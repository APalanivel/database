SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
   
 dbo.Client_Commodity_Sel_By_Client_Commodity_Service_Level  
  
DESCRIPTION:  
  
Used to select the commodity,hier level code,bucket aggresstion and allow for de  
  
INPUT PARAMETERS:  
 Name      DataType  Default  Description  
------------------------------------------------------------------------------------  
 @Client_id      int  
 @Commodity_Service_Value VARCHAR(25)     Invoice/GHG  
 @Hier_Level_Cd_Value  VARCHAR(25)     Account/Site  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
DECLARE @Client_Ids Client_Ids  
  
INSERT INTO @Client_Ids  
VALUES (12068),(11278)  
  
EXEC dbo.Common_Commodity_Sel_For_Client_Ids @Client_Ids  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RK      Raghu Kalvapudi 
  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 RK         12/06/2013 Created  
******/  
  
CREATE PROCEDURE [dbo].[Common_Commodity_Sel_For_Client_Ids]
      (
       @Client_Ids dbo.Client_Ids READONLY )
AS
BEGIN  
    
      SET NOCOUNT ON;  

      DECLARE @Client_Count INT  
  
  
      SELECT
            @Client_Count = count(1)
      FROM
            @Client_Ids  
  
  
      SELECT
            cc.Commodity_Id
           ,com.Commodity_Name
      FROM
            Core.Client_Commodity cc
            INNER JOIN @Client_Ids cl
                  ON cc.Client_Id = cl.Client_Id
            INNER JOIN dbo.Code c
                  ON cc.Commodity_Service_Cd = c.Code_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_id = cc.Commodity_id
      WHERE
            c.Code_Value = 'Invoice'
      GROUP BY
            cc.Commodity_Id
           ,com.Commodity_Name
      HAVING
            count(1) = @Client_Count
      ORDER BY
            ( case WHEN com.Commodity_Name = 'Electric Power' THEN 1
                   WHEN com.Commodity_Name = 'Natural Gas' THEN 2
                   ELSE 3
              END ) ASC
           ,com.Commodity_Name ASC  
  
END  
;
GO
GRANT EXECUTE ON  [dbo].[Common_Commodity_Sel_For_Client_Ids] TO [CBMSApplication]
GO
