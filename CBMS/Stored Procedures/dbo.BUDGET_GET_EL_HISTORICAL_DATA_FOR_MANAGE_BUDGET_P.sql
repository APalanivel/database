SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: dbo.BUDGET_GET_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P

DESCRIPTION:
	SP to get Usage data for EP,for a Given Budget

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@user_id					VARCHAR(10)     
    @session_id					VARCHAR(20)
    @budgetId					INT
    @from_month					DATETIME
    @to_month					DATETIME

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
exec BUDGET_GET_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P '','',865, '01/01/2008', '12/01/2008'  

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AKR         Ashok Kumar Raju
	
MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	AKR         2012-07-09     Modified as a part of Additional Data		
******/  
    
CREATE  PROCEDURE dbo.BUDGET_GET_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@budgetId INT
      ,@from_month DATETIME
      ,@to_month DATETIME )
AS 
BEGIN  
      SET NOCOUNT ON  
      
      DECLARE
            @EL_Kwh_Unit INT
           ,@Bucket_Master_Id INT
      
      SELECT
            @EL_Kwh_Unit = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'Kwh'
            AND e.ENTITY_DESCRIPTION = 'Unit for electricity'
            
      SELECT
            @Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Commodity c
            INNER JOIN dbo.Bucket_Master bm
                  ON c.Commodity_Id = bm.Commodity_Id
      WHERE
            c.Commodity_Name = 'Electric Power'
            AND bm.Bucket_Name = 'Total Usage'
            
   
      SELECT
            cuad.Service_Month
           ,( cuad.Bucket_Value * cuc.CONVERSION_FACTOR ) AS usage_value
           ,ba.BUDGET_ACCOUNT_ID
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON cuad.ACCOUNT_ID = ba.ACCOUNT_ID
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                  ON cuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                     AND cuc.CONVERTED_UNIT_ID = @EL_Kwh_Unit
      WHERE
            ba.BUDGET_ID = @budgetId
            AND cuad.Service_Month BETWEEN @from_month
                                   AND     @to_month
            AND cuad.Bucket_Master_Id = @Bucket_Master_Id
        
END  
  
  
;
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_EL_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
