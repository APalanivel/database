SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec GET_DEAL_TICKET_VOLUME_DETAILS_FOR_INDIVIDUAL_TRIGGERS_P 1,1,331,100177,1708,3,264,0,0,0,0,0

CREATE  PROCEDURE dbo.GET_DEAL_TICKET_VOLUME_DETAILS_FOR_INDIVIDUAL_TRIGGERS_SCENARIO_P
@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@dealTicketId integer,
@dealTicketDetailsId integer,
@currencyUnit integer,
@consumptionUnit integer,
@hedgeTypeId integer,
@hedgeLevelTypeId integer,
@divisionId integer,
@siteId integer,
@contractId integer

AS

	set nocount on	

if @currencyUnit>0 And @consumptionUnit>0

BEGIN

if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)=@currencyUnit OR (select count(*) from RM_CURRENCY_UNIT_CONVERSION where client_id=@clientId)=0)

	BEGIN
	

	if @clientId>0 and @divisionId=0 and @siteId=0

		BEGIN
	--print 'inside client 1 '

		SELECT 
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*1/consumption.CONVERSION_FACTOR) trigger_price,
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			1.00 conversion_factor,
			rmdtd.hedge_price*1/consumption.CONVERSION_FACTOR,
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor

		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			--RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE 
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			onboard.site_id=rmdtvd.site_id 
			
		END


	else if @clientId>0 and @divisionId>0 and @siteId=0

		BEGIN

		SELECT 
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*1/consumption.CONVERSION_FACTOR) trigger_price,
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			1.00 conversion_factor,
			rmdtd.hedge_price*1/consumption.CONVERSION_FACTOR,
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor


		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			--RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE 
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			onboard.site_id=rmdtvd.site_id 
		END

	else if @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0

		BEGIN

		if @contractId>0

		BEGIN

				SELECT 
					rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
					(rmdtd.trigger_price*1/consumption.CONVERSION_FACTOR) trigger_price,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
					1.00 conversion_factor,
					rmdtd.hedge_price*1/consumption.CONVERSION_FACTOR,
					consumption.CONVERSION_FACTOR Volume_Conversion_Factor

				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET rmdt,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					CONSUMPTION_UNIT_CONVERSION consumption,
					RM_ONBOARD_HEDGE_SETUP onboard

				WHERE 
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID= @dealTicketDetailsId AND
					rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id= @dealTicketId)AND
					consumption.CONVERTED_UNIT_ID= @consumptionUnit AND
					rmdtvd.site_id=onboard.site_id AND
					rmdtvd.site_id = @siteId AND
					rmdt.CONTRACT_ID= @contractId AND
					rmdt.RM_DEAL_TICKET_ID= @dealTicketId
		END

		else

		BEGIN
				SELECT 
					rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
					(rmdtd.trigger_price*1/consumption.CONVERSION_FACTOR) trigger_price,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
					1.00 conversion_factor,
					rmdtd.hedge_price*1/consumption.CONVERSION_FACTOR,
					consumption.CONVERSION_FACTOR Volume_Conversion_Factor

				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					--RM_CURRENCY_UNIT_CONVERSION currency,
					CONSUMPTION_UNIT_CONVERSION consumption,
					RM_ONBOARD_HEDGE_SETUP onboard

				WHERE 
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
					rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					rmdtvd.site_id=onboard.site_id AND
					rmdtvd.site_id =@siteId
		END

		END
	END

	

else if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id= @dealTicketId)!= @currencyUnit)

	BEGIN--6TH

		if((select CURRENCY_UNIT_NAME from CURRENCY_UNIT where CURRENCY_UNIT_ID= @currencyUnit)!='CAN')
	BEGIN--7TH


	if @clientId>0 and @divisionId=0 and @siteId=0

		BEGIN


		SELECT
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			1/currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))AND
			onboard.site_id=rmdtvd.site_id 
		END
	
		ELSE IF  @clientId>0 and @divisionId>0 and @siteId=0

		BEGIN

			select
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			1/currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,

			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
			onboard.site_id=rmdtvd.site_id 
		END

	else if @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0

		BEGIN

		  IF @contractId>0 
		         BEGIN
				select
				rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
				(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
				DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
				DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
				rmdtvd.site_id,
				rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
				1/currency.CONVERSION_FACTOR,
				(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
				consumption.CONVERSION_FACTOR Volume_Conversion_Factor

			    FROM
				RM_DEAL_TICKET rmdt,
				RM_DEAL_TICKET_DETAILS rmdtd,
				RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
				RM_CURRENCY_UNIT_CONVERSION currency,
				CONSUMPTION_UNIT_CONVERSION consumption,
				RM_ONBOARD_HEDGE_SETUP onboard
			    WHERE
				rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
				rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
				rmdt.contract_id=@contractId AND
				rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
				consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
				consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
				RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
				where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
				rmdtvd.site_id=onboard.site_id and
				rmdtvd.site_id =@siteId 
			  END

		  ELSE
			  BEGIN

				select
				rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
				(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
				DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
				DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
				rmdtvd.site_id,
				rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
				1/currency.CONVERSION_FACTOR,
				(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
				consumption.CONVERSION_FACTOR Volume_Conversion_Factor

			  FROM
				RM_DEAL_TICKET_DETAILS rmdtd,
				RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
				RM_CURRENCY_UNIT_CONVERSION currency,
				CONSUMPTION_UNIT_CONVERSION consumption,
				RM_ONBOARD_HEDGE_SETUP onboard
			  WHERE
				rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
				rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
				consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
				consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
				RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
				where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
				rmdtvd.site_id=onboard.site_id and
				rmdtvd.site_id =@siteId 
			   END
		END
	   END
		
	ELSE
		
		if @clientId>0 and @divisionId=0 and @siteId=0

		BEGIN


		SELECT
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,

			(rmdtd.trigger_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))AND
			onboard.site_id=rmdtvd.site_id 
		END
	
		ELSE IF  @clientId>0 and @divisionId>0 and @siteId=0

		BEGIN

			select
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			rmdtd.trigger_price*currency.CONVERSION_FACTOR trigger_price,
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

		FROM
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		WHERE
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
			onboard.site_id=rmdtvd.site_id 
		END

		ELSE IF @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0

		BEGIN

			if @contractId>0

			begin
				
			select
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

			from
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET rmdt,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

			where
			rmdt.RM_DEAL_TICKET_ID= @dealTicketId AND
			rmdt.contract_id= @contractId AND
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_ID= @dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
			rmdtvd.site_id=onboard.site_id and
			rmdtvd.site_id =@siteId 
		END

			else

			begin

			select
			rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedge_volume,
			(rmdtd.trigger_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
			DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
			DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
			rmdtvd.site_id,
			rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
			currency.CONVERSION_FACTOR,
			(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
			consumption.CONVERSION_FACTOR Volume_Conversion_Factor
			
			

		from
			RM_DEAL_TICKET_DETAILS rmdtd,
			RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
			RM_CURRENCY_UNIT_CONVERSION currency,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard

		where
			rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId AND
			rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
			consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
			consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
			RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
			where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER))and
			rmdtvd.site_id=onboard.site_id and
			rmdtvd.site_id =@siteId 
			end
		END

	
END

else if @currencyUnit=0 And @consumptionUnit=0

	BEGIN

	select 

		rmdtvd.hedge_volume,
		rmdtd.trigger_price,
		DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
		DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
		rmdtvd.site_id
	
	
	from 
		rm_deal_ticket_details rmdtd,
		RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
		RM_ONBOARD_HEDGE_SETUP onboard
		

	where 
		rmdtvd.RM_DEAL_TICKET_DETAILS_ID=@dealTicketDetailsId and
		rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID and
		rmdtvd.site_id=onboard.site_id and
		onboard.hedge_type_id=@hedgeTypeId
		
		
	END
END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_VOLUME_DETAILS_FOR_INDIVIDUAL_TRIGGERS_SCENARIO_P] TO [CBMSApplication]
GO
