SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   dbo.Account_Consolidated_Billing_Vendor_Sel  
             
DESCRIPTION:               
   To get consolidated billing configurations  
     
INPUT PARAMETERS:              
 Name    DataType Default  Description    
---------------------------------------------------------------------------------    
 @Account_Id   INT  
  
  
OUTPUT PARAMETERS:  
 Name        DataType  Default  Description    
---------------------------------------------------------------------------------    
  
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------    
 SELECT TOP 10 * FROM dbo.Account_Consolidated_Billing_Vendor  
              
  SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 1269921  
  EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 1269921  
  EXEC dbo.Account_DMO_Config_Sel @Account_Id = 1269921  
  EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1269921  
    
  SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 1269922  
  EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 1269922  
  EXEC dbo.Account_DMO_Config_Sel @Account_Id = 1269922  
  EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1269922  
    
  EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 29085  
  EXEC dbo.Account_DMO_Config_Sel @Account_Id = 29085  
  EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 29085  
   
   
EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 455299
EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1656186
EXEC dbo.Account_Consolidated_Billing_Vendor_Sel 1656230


 AUTHOR INITIALS:              
 Initials	Name              
-------------------------------------------------------------              
 RR			Raghu Reddy  
 NR			Narayana Reddy
  
 MODIFICATIONS:  
 Initials	Date			Modification  
------------------------------------------------------------  
 RR			2017-01-19		Contract placeholder - CP-50 Created  
 NR			2019-09-26		Add Contract  - Added Contract and Contract number.
 NR			2019-12-26		MAINT-9591 - Now Geting contract details based on consolidated account .
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Sel]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        SELECT
            cbv.Account_Consolidated_Billing_Vendor_Id
            , cbv.Account_Id
            , iv.ENTITY_NAME AS Invoice_Vendor
            , ISNULL(ven.VENDOR_NAME, 'Contracted Vendor') AS Vendor_Name
            , cbv.Billing_Start_Dt
            , cbv.Billing_End_Dt
            , cbv.Invoice_Vendor_Type_Id
            , cbv.Supplier_Vendor_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_User
            , cbv.Last_Change_Ts
            , LEFT(cm.ED_CONTRACT_NUMBER, LEN(cm.ED_CONTRACT_NUMBER) - 1) AS ED_CONTRACT_NUMBER
        FROM
            dbo.Account_Consolidated_Billing_Vendor cbv
            INNER JOIN Core.Client_Hier_Account cha
                ON cbv.Account_Id = cha.Account_Id
            INNER JOIN dbo.ENTITY iv
                ON cbv.Invoice_Vendor_Type_Id = iv.ENTITY_ID
            LEFT JOIN dbo.VENDOR ven
                ON cbv.Supplier_Vendor_Id = ven.VENDOR_ID
            INNER JOIN dbo.USER_INFO ui
                ON cbv.Updated_User_Id = ui.USER_INFO_ID
            CROSS APPLY
        (   SELECT
                CAST(sup_cha.Supplier_Contract_ID AS VARCHAR(100)) + '|'
                + CASE WHEN sup_cha.Supplier_Contract_ID = -1 THEN 'Missing Contract'
                      ELSE c.ED_CONTRACT_NUMBER
                  END + ','
            FROM
                Core.Client_Hier_Account u_cha
                INNER JOIN Core.Client_Hier_Account sup_cha
                    ON u_cha.Client_Hier_Id = sup_cha.Client_Hier_Id
                       AND  u_cha.Meter_Id = sup_cha.Meter_Id
                       AND  sup_cha.Account_Type = 'Supplier'
                       AND  u_cha.Account_Type = 'Utility'
                LEFT JOIN dbo.CONTRACT c
                    ON c.CONTRACT_ID = sup_cha.Supplier_Contract_ID
            WHERE
                cha.Client_Hier_Id = sup_cha.Client_Hier_Id
                AND cha.Account_Id = u_cha.Account_Id
                AND (   cbv.Billing_Start_Dt BETWEEN sup_cha.Supplier_Account_begin_Dt
                                             AND     sup_cha.Supplier_Account_End_Dt
                        OR  ISNULL(cbv.Billing_End_Dt, '2099-12-31') BETWEEN sup_cha.Supplier_Account_begin_Dt
                                                                     AND     sup_cha.Supplier_Account_End_Dt
                        OR  sup_cha.Supplier_Account_begin_Dt BETWEEN cbv.Billing_Start_Dt
                                                              AND     ISNULL(cbv.Billing_End_Dt, '2099-12-31')
                        OR  sup_cha.Supplier_Account_End_Dt BETWEEN cbv.Billing_Start_Dt
                                                            AND     ISNULL(cbv.Billing_End_Dt, '2099-12-31'))
            GROUP BY
                CAST(sup_cha.Supplier_Contract_ID AS VARCHAR(100)) + '|'
                + CASE WHEN sup_cha.Supplier_Contract_ID = -1 THEN 'Missing Contract'
                      ELSE c.ED_CONTRACT_NUMBER
                  END
            FOR XML PATH('')) cm(ED_CONTRACT_NUMBER)
        WHERE
            cbv.Account_Id = @Account_Id
            AND cha.Account_Type = 'Utility'
        GROUP BY
            cbv.Account_Consolidated_Billing_Vendor_Id
            , cbv.Account_Id
            , iv.ENTITY_NAME
            , ISNULL(ven.VENDOR_NAME, 'Contracted Vendor')
            , cbv.Billing_Start_Dt
            , cbv.Billing_End_Dt
            , cbv.Invoice_Vendor_Type_Id
            , cbv.Supplier_Vendor_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , cbv.Last_Change_Ts
            , LEFT(cm.ED_CONTRACT_NUMBER, LEN(cm.ED_CONTRACT_NUMBER) - 1)
        ORDER BY
            cbv.Last_Change_Ts;



    END;
    ;





GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Sel] TO [CBMSApplication]
GO
