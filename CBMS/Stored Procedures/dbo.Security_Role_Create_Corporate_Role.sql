SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   Security_Role_Create_Corporate_Role

    
DESCRIPTION:          
	Adds the default Corporate security role and Client_Hier records. 
	Can be done for a client or for all cleints that do not have a corporate role
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          
@Client_Id		INT			NULL		Client to add corporate role for. If null
											,corporate roles are created for all 
											clients that do not have them
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC Security_Role_Create_Corporate_Role

EXEC Security_Role_Create_Corporate_Role 100
       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
HG			Harihara Suthan G     
     
MODIFICATIONS           
Initials	Date		Modification          
------------------------------------------------------------          
CMH			06/01/2010	Created
HG			04/01/2011	Script modified to remove Role_Name column from Security_Role table as it is not used by application anywhere.

*****/ 
CREATE PROCEDURE dbo.Security_Role_Create_Corporate_Role
(	@Client_Id	INT = NULL	) 
AS 
BEGIN

	SET NOCOUNT ON; 

	DECLARE 
		@Client_Name VARCHAR(200)
		,@Security_Role_Id INT


	DECLARE  cClient INSENSITIVE CURSOR
	FOR (	SELECT 
				c.Client_Id
				,c.Client_Name
			FROM 
				dbo.Client c
				LEFT JOIN dbo.Security_Role sr 
					ON c.Client_id = sr.Client_Id
			WHERE 
				sr.Security_Role_id IS NULL
				AND c.Client_Id = ISNULL(@Client_Id, c.Client_Id ) ) 
			
	OPEN cClient
	FETCH NEXT FROM cClient INTO @Client_Id, @Client_Name

	WHILE @@fetch_status = 0 
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION 
				-- Add Corporate Role
				INSERT dbo.Security_Role
				(	Client_Id
					,Is_Corporate) 
				VALUES 
				(	@Client_Id
					,1	)
					
				SET @Security_Role_Id = SCOPE_IDENTITY() 
			
				-- Add Client, Division, and Site records
				INSERT dbo.Security_Role_Client_Hier
				(	Security_Role_Id
					,Client_Hier_Id )
				SELECT 
					@Security_Role_Id
					,ch.Client_Hier_Id 
				FROM 
					dbo.Security_Role sr
					INNER JOIN Core.Client_Hier ch 
						ON sr.Client_Id = ch.Client_Id
					INNER JOIN Code sgCode
						ON ch.Sitegroup_Type_cd = sgCode.Code_Id
				WHERE
					 ch.Client_Id = @Client_Id
						AND ( ch.Sitegroup_Type_cd IS NULL  -- Client, Site
							OR sgCode.Code_Value = 'Division') -- Divisions (Exclude Sitegroups) 

			COMMIT TRANSACTION
			PRINT 'Corporate group Added for ' + @Client_Name + ' (' + CONVERT(VARCHAR, @Client_Id) + ')'
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			DECLARE @ErrMsg VARCHAR(500) 
			SET @ErrMsg = 'Error creating Corporate group for ' + @Client_Name + ' (' + CONVERT(VARCHAR, @Client_Id) + ')'
		END CATCH

		FETCH NEXT FROM cClient INTO @Client_Id, @Client_Name
	END
	CLOSE cClient
	DEALLOCATE cClient
END

		

		
GO
GRANT EXECUTE ON  [dbo].[Security_Role_Create_Corporate_Role] TO [CBMSApplication]
GO
