SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.[Invoice_Collection_Issue_Event_Upd_Invoice_Collection_Issue_Event_Id]       
                        
 DESCRIPTION:                        
			To get Invoice_Collection_Issue_Event_desc_Upd      
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Issue_Event_Id INT

                             
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

          exec [dbo].[Invoice_Collection_Issue_Event_Upd_Invoice_Collection_Issue_Event_Id] 1                    
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
RKV						Ravi kumar vegesna                         

 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 RKV                   2018-02-03       SE2017-273,Created         
                      
******/    

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Event_Upd_Invoice_Collection_Issue_Event_Id]
      ( 
       @New_Event_Desc NVARCHAR(MAX)
      ,@Invoice_Collection_Issue_Event_Ids VARCHAR(MAX)
      ,@User_Info_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;  


      DECLARE @Invoice_Collection_Issue_Event_Id TABLE
            ( 
             Invoice_Collection_Issue_Event_Id INT )
      
      INSERT      INTO @Invoice_Collection_Issue_Event_Id
                  ( 
                   Invoice_Collection_Issue_Event_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Invoice_Collection_Issue_Event_Ids, ',') us
                  
                  
      
      
      
      
        
	 	
      UPDATE
            ie
      SET   
            Event_Desc = @New_Event_Desc
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      FROM
            Invoice_Collection_Issue_Event ie
            INNER JOIN @Invoice_Collection_Issue_Event_Id icie
                  ON icie.Invoice_Collection_Issue_Event_Id = ie.Invoice_Collection_Issue_Event_Id
      	


END;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Event_Upd_Invoice_Collection_Issue_Event_Id] TO [CBMSApplication]
GO
