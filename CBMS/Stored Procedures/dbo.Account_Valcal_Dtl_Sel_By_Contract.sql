SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Account_Valcal_Dtl_Sel_By_Contract                     
                          
 DESCRIPTION: To raise the Valcal Exception
       
		              
                          
 INPUT PARAMETERS:      
                         
 Name                  DataType          Default       Description      
--------------------------------------------------------------------
 @Contract_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                  DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                            


EXEC dbo.Account_Valcal_Dtl_Sel_By_Contract
    @Account_Id = 163963
    , @Start_Dt = '2019-01-01'  
	,@End_Dt =  '2019-01-31' 
    
	                   
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/
CREATE PROCEDURE [dbo].[Account_Valcal_Dtl_Sel_By_Contract]
    (
        @Account_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE @Is_Config_Complete BIT = 0;

        SELECT
            @Is_Config_Complete = 1
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Valcon_Account_Config vac
                ON vac.Account_Id = cha.Account_Id
                   AND  cha.Supplier_Contract_ID = vac.Contract_Id
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
            AND vac.Is_Config_Complete = 1
            AND (   @Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                              AND     ISNULL(cha.Supplier_Account_End_Dt, '9999-12-31')
                    OR  @End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                AND     ISNULL(cha.Supplier_Account_End_Dt, '9999-12-31')
                    OR  cha.Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                    OR  ISNULL(cha.Supplier_Account_End_Dt, '9999-12-31') BETWEEN @Start_Dt
                                                                          AND     @End_Dt);


        SELECT  @Is_Config_Complete AS Is_Config_Complete;
    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Valcal_Dtl_Sel_By_Contract] TO [CBMSApplication]
GO
