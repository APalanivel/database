SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: Dbo.Sr_Load_Profile_Determinant_Del  
     
DESCRIPTION: 
	It Deletes	Sr Load Profile Determinant for Selected Sr Load Profile Determinant Id.
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------------------         
	@Sr_Load_Profile_Determinant_Id		INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Sr_Load_Profile_Determinant_Del  8382
		
		
	ROLLBACK TRAN

		SELECT  * FROM dbo.Sr_Load_Profile_Determinant 
					   WHERE SR_LOAD_PROFILE_DETERMINANT_ID = 8382



AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			24-August-10	CREATED

*/
CREATE PROCEDURE dbo.Sr_Load_Profile_Determinant_Del
    (
      @Sr_Load_Profile_Determinant_Id	INT
     )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Sr_Load_profile_determinant
	WHERE
		Sr_Load_Profile_Determinant_Id = @Sr_Load_Profile_Determinant_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Load_Profile_Determinant_Del] TO [CBMSApplication]
GO
