SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                          
 NAME: dbo.Report_DE_Cost_Usage_Flint_Group              
                          
 DESCRIPTION:                          
                      
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
@Client_Name				VARCHAR(200)	 'Flint Group'
@Currency_Unit				VARCHAR(200)	  'USD'
@Begin_Date						DATE		 '2012-02-01'
@End_Date						DATE			NULL		will get previous month 1st date.
                                
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                                
 
 EXEC [dbo].[Report_DE_Cost_Usage_Flint_Group] 

                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 AP                    Ashok Pothuri  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 AP                    2015-01-28       Created                  
                         
******/                     
                  
CREATE PROCEDURE [dbo].[Report_DE_Cost_Usage_Flint_Group]
      ( 
       @Client_Name VARCHAR(200) = 'Flint Group'
      ,@Currency_Unit VARCHAR(200) = 'USD'
      ,@Begin_Date DATE = '2012-02-01'
      ,@End_Date DATE = NULL )
AS 
BEGIN                  
      SET NOCOUNT ON;    
 
      SELECT
            @End_Date = ISNULL(@End_Date, DATEADD(mm, -1, DATEADD(dd, -DATEPART(dd, GETDATE()) + 1, GETDATE())))
     
              
      DECLARE
            @Client_Id INT
           ,@Currency_Unit_Id INT
           ,@NG_Consumption_Unit_Id INT
           ,@EP_Consumption_Unit_Id INT
           ,@EP_Commodity_Id INT
           ,@NG_Commodity_Id INT
           ,@Region_Id INT
           ,@Utility_Entity_Id INT

      SELECT
            @Client_Id = Client_Id
      FROM
            Core.Client_Hier ch
      WHERE
            Client_Name = @Client_Name
            AND ch.Sitegroup_Id = 0
      
      SELECT
            @Currency_Unit_Id = cu.CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_NAME = 'USD'
      
      SELECT
            @NG_Consumption_Unit_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Dth'
            AND ENTITY_DESCRIPTION = 'Unit for Gas'


      SELECT
            @EP_Consumption_Unit_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'kWh'
            AND ENTITY_DESCRIPTION = 'Unit for electricity'
      
            
      SELECT
            @EP_Commodity_Id = MAX(CASE WHEN Commodity_Name = 'Electric Power' THEN Commodity_Id
                                   END)
           ,@NG_Commodity_Id = MAX(CASE WHEN Commodity_Name = 'Natural Gas' THEN Commodity_Id
                                   END)
      FROM
            dbo.Commodity
      WHERE
            Commodity_Name IN ( 'Electric Power', 'Natural Gas' )  
  
      SELECT
            @Region_Id = re.region_Id
      FROM
            dbo.REGION re
      WHERE
            re.REGION_NAME = 'EUR'
       
       
      SELECT
            @Utility_Entity_Id = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'Utility'
            AND en.ENTITY_DESCRIPTION = 'Account Type'
  
      SELECT DISTINCT
            Site_name AS 'Site'
           ,Sitegroup_Name AS 'Division'
           ,Service_Month AS 'ServiceMonth'
           ,averagebdep AS '# days in service period EP'
           ,EPUsage AS 'TotalEPUsage(kWh)'
           ,ELCost AS 'TotalEPCost(USD$)'
           ,'UnitEPCost_$/kWh' = CASE WHEN EPUsage <> '0'
                                           AND ELCost <> '0' THEN ELCost / EPUsage
                                      ELSE '0'
                                 END
           ,EPComplete
           ,averagebdng AS '# days in service period NG'
           ,NGUsage AS 'TotalNGUsage(Dth)'
           ,NGCost AS 'TotalNGCost(USD$)'
           ,'UnitNGCost_$/Dth' = CASE WHEN NGUsage <> '0'
                                           AND NGCost <> '0' THEN NGCost / NGUsage
                                      ELSE '0'
                                 END
           ,NGComplete
      FROM
            ( SELECT DISTINCT
                  Site_name
                 ,Service_Month
                 ,averagebdng
                 ,averagebdep
                 ,Sitegroup_Name
                 ,City
                 ,SUM(ngindusage) NGUsage
                 ,SUM(elindusage) EPUsage
                 ,SUM(ng_ind_cost) NGCost
                 ,SUM(el_ind_cost) ELCost
                 ,SUM(ip_ind_rcvd) IPRcvd
                 ,SUM(ip_ind_exp) IPExp
                 ,ng_is_complete NGComplete
                 ,el_is_complete EPComplete
              FROM
                  ( SELECT DISTINCT
                        Client_Name
                       ,Service_Month
                       ,Sitegroup_Name
                       ,Country_Name
                       ,State_Name
                       ,City
                       ,Site_name
                       ,ngindusage
                       ,elindusage
                       ,ng_ind_cost
                       ,el_ind_cost
                       ,ip_ind_exp = CAST (ip_ind_exp AS DECIMAL(32, 16))
                       ,ip_ind_rcvd = CAST (ip_ind_rcvd AS DECIMAL(32, 16))
                       ,ng_is_complete
                       ,el_is_complete
                       ,averagebdng
                       ,averagebdep
                    FROM
                        ( SELECT DISTINCT
                              ch.Client_Name
                             ,cus.Service_Month
                             ,ch.Sitegroup_Name
                             ,ch.Country_Name
                             ,ch.State_Name
                             ,ch.City
                             ,ch.Site_name
                             ,ngindusage = CASE WHEN cus.Bucket_Value <> '0'
                                                     AND bmm.Commodity_Id = @NG_Commodity_Id
                                                     AND bmm.Bucket_Name = 'total usage' THEN ( cus.Bucket_Value * cuc.CONVERSION_FACTOR )
                                                ELSE '0'
                                           END
                             ,elindusage = CASE WHEN cus.Bucket_Value <> '0'
                                                     AND bmm.Commodity_Id = @EP_Commodity_Id
                                                     AND bmm.Bucket_Name = 'total usage' THEN ( cus.Bucket_Value * cucep.CONVERSION_FACTOR )
                                                ELSE '0'
                                           END
                             ,ng_ind_cost = CASE WHEN bmm.Commodity_Id = @NG_Commodity_Id
                                                      AND bmm.Bucket_Name = 'total cost' THEN CAST(cucUSD.CONVERSION_FACTOR * ( cus.Bucket_Value ) AS DECIMAL(32, 16))
                                            END
                             ,el_ind_cost = CASE WHEN bmm.Commodity_Id = @EP_Commodity_Id
                                                      AND bmm.Bucket_Name = 'total cost' THEN CAST(cucUSD.CONVERSION_FACTOR * ( cus.Bucket_Value ) AS DECIMAL(32, 16))
                                            END
                             ,ip_ind_exp = ips.EXPECTED_COUNT
                             ,ip_ind_rcvd = ips.RECEIVED_COUNT
                             ,ng_is_complete = CASE WHEN ips.NG_IS_COMPLETE = '1' THEN 'Yes'
                                                    ELSE 'No'
                                               END
                             ,el_is_complete = CASE WHEN ips.EL_IS_COMPLETE = '1' THEN 'Yes'
                                                    ELSE 'No'
                                               END
                             ,z.averagebdng
                             ,z.averagebdep
                          FROM
                              Cost_Usage_Site_Dtl cus
                              LEFT JOIN Core.Client_Hier ch
                                    ON ch.Client_Hier_Id = cus.Client_Hier_Id
                              LEFT JOIN SITE s
                                    ON s.SITE_ID = ch.Site_Id
                              LEFT JOIN Bucket_Master bmm
                                    ON bmm.Bucket_Master_Id = cus.Bucket_Master_Id
                              LEFT JOIN ENTITY uom
                                    ON uom.ENTITY_ID = cus.UOM_Type_Id
                              LEFT JOIN CONSUMPTION_UNIT_CONVERSION cuc
                                    ON cuc.BASE_UNIT_ID = cus.UOM_Type_Id
                                       AND cuc.CONVERTED_UNIT_ID = @NG_Consumption_Unit_Id
                              LEFT JOIN CONSUMPTION_UNIT_CONVERSION cucep
                                    ON cucep.BASE_UNIT_ID = cus.UOM_Type_Id
                                       AND cucep.CONVERTED_UNIT_ID = @EP_Consumption_Unit_Id
                              LEFT JOIN ACCOUNT acct
                                    ON acct.SITE_ID = s.SITE_ID
                              LEFT JOIN VENDOR v
                                    ON v.VENDOR_ID = acct.VENDOR_ID
                              JOIN VENDOR_COMMODITY_MAP vcm
                                    ON vcm.VENDOR_ID = v.VENDOR_ID
                              LEFT JOIN CURRENCY_UNIT_CONVERSION cucUSD
                                    ON ( cucUSD.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
                                         AND cucUSD.CONVERSION_DATE = cus.Service_Month
                                         AND cucUSD.CONVERTED_UNIT_ID = @Currency_Unit_Id
                                         AND cucUSD.BASE_UNIT_ID = cus.CURRENCY_UNIT_ID )
                              LEFT JOIN INVOICE_PARTICIPATION_SITE ips
                                    ON ips.SITE_ID = s.SITE_ID
                                       AND ips.SERVICE_MONTH = cus.Service_Month
                              LEFT JOIN ( SELECT
                                                SITE_ID
                                               ,SUM(abdng) averagebdng
                                               ,SUM(abdep) averagebdep
                                               ,SERVICE_MONTH
                                          FROM
                                                ( SELECT
                                                      SITE_ID
                                                     ,AVG(abdng) abdng
                                                     ,'0' abdep
                                                     ,SERVICE_MONTH
                                                  FROM
                                                      ( SELECT
                                                            s.SITE_ID
                                                           ,AVG(cuism.Billing_Days) abdng
                                                           ,'0' abdep
                                                           ,SERVICE_MONTH
                                                        FROM
                                                            CU_INVOICE cui
                                                            JOIN CU_INVOICE_SERVICE_MONTH cuism
                                                                  ON cuism.CU_INVOICE_ID = cui.CU_INVOICE_ID
                                                            JOIN core.Client_Hier_Account cha
                                                                  ON cuism.Account_ID = cha.Account_Id
                                                            JOIN core.Client_Hier s
                                                                  ON cha.Client_Hier_Id = s.Client_Hier_Id
                                                        WHERE
                                                            cui.IS_REPORTED = 1
                                                            AND cui.IS_PROCESSED = 1
                                                            AND IS_DUPLICATE <> 1
                                                            AND cui.CLIENT_ID = @Client_Id
                                                            AND cha.Account_Type = 'Utility'
                                                            AND cuism.SERVICE_MONTH BETWEEN @Begin_Date
                                                                                    AND     @End_Date
                                                            AND cha.Commodity_Id = @NG_Commodity_Id
                                                        GROUP BY
                                                            s.SITE_ID
                                                           ,SERVICE_MONTH
                                                        UNION
                                                        SELECT
                                                            s.SITE_ID
                                                           ,AVG(cuism.Billing_Days) abdng
                                                           ,'0' abdep
                                                           ,SERVICE_MONTH
                                                        FROM
                                                            CU_INVOICE cui
                                                            JOIN CU_INVOICE_SERVICE_MONTH cuism
                                                                  ON cuism.CU_INVOICE_ID = cui.CU_INVOICE_ID
                                                            JOIN core.Client_Hier_Account supplier
                                                                  ON cuism.Account_ID = supplier.Account_Id
                                                                     AND supplier.Account_Type = 'supplier'
                                                            JOIN core.Client_Hier_Account utility
                                                                  ON supplier.Meter_Id = utility.Meter_Id
                                                                     AND supplier.Account_Type = 'utility'
                                                            INNER JOIN core.Client_Hier s
                                                                  ON utility.Client_Hier_Id = s.Client_Hier_Id
                                                        WHERE
                                                            cui.IS_REPORTED = 1
                                                            AND cui.IS_PROCESSED = 1
                                                            AND cui.IS_DUPLICATE <> 1
                                                            AND cui.CLIENT_ID = @Client_Id
                                                            AND cuism.SERVICE_MONTH BETWEEN @Begin_Date
                                                                                    AND     @End_Date
                                                            AND utility.Commodity_Id = @NG_Commodity_Id
                                                        GROUP BY
                                                            s.SITE_ID
                                                           ,SERVICE_MONTH ) y
                                                  GROUP BY
                                                      SITE_ID
                                                     ,SERVICE_MONTH
                                                  UNION
                                                  SELECT
                                                      SITE_ID
                                                     ,'0' abdng
                                                     ,AVG(abdep) abdep
                                                     ,SERVICE_MONTH
                                                  FROM
                                                      ( SELECT
                                                            s.SITE_ID
                                                           ,'0' abdng
                                                           ,AVG(cuism.Billing_Days) abdep
                                                           ,SERVICE_MONTH
                                                        FROM
                                                            CU_INVOICE cui
                                                            JOIN CU_INVOICE_SERVICE_MONTH cuism
                                                                  ON cuism.CU_INVOICE_ID = cui.CU_INVOICE_ID
                                                            JOIN core.Client_Hier_Account cha
                                                                  ON cuism.Account_ID = cha.Account_Id
                                                            JOIN core.Client_Hier s
                                                                  ON cha.Client_Hier_Id = s.Client_Hier_Id
                                                        WHERE
                                                            cui.IS_REPORTED = 1
                                                            AND cui.IS_PROCESSED = 1
                                                            AND IS_DUPLICATE <> 1
                                                            AND cui.CLIENT_ID = @Client_Id
                                                            AND cha.Account_Type = 'Utility'
                                                            AND cuism.SERVICE_MONTH BETWEEN @Begin_Date
                                                                                    AND     @End_Date
                                                            AND cha.Commodity_Id = @EP_Commodity_Id
                                                        GROUP BY
                                                            s.SITE_ID
                                                           ,SERVICE_MONTH
                                                        UNION
                                                        SELECT
                                                            s.SITE_ID
                                                           ,'0' abdng
                                                           ,AVG(cuism.Billing_Days) abdep
                                                           ,SERVICE_MONTH
                                                        FROM
                                                            CU_INVOICE cui
                                                            JOIN CU_INVOICE_SERVICE_MONTH cuism
                                                                  ON cuism.CU_INVOICE_ID = cui.CU_INVOICE_ID
                                                            JOIN core.Client_Hier_Account supplier
                                                                  ON cuism.Account_ID = supplier.Account_Id
                                                                     AND supplier.Account_Type = 'supplier'
                                                            JOIN core.Client_Hier_Account utility
                                                                  ON supplier.Meter_Id = utility.Meter_Id
                                                                     AND supplier.Account_Type = 'utility'
                                                            INNER JOIN core.Client_Hier s
                                                                  ON utility.Client_Hier_Id = s.Client_Hier_Id
                                                        WHERE
                                                            cui.IS_REPORTED = 1
                                                            AND cui.IS_PROCESSED = 1
                                                            AND cui.IS_DUPLICATE <> 1
                                                            AND cui.CLIENT_ID = @Client_Id
                                                            AND cuism.SERVICE_MONTH BETWEEN @Begin_Date
                                                                                    AND     @End_Date
                                                            AND utility.Commodity_Id = @EP_Commodity_Id
                                                        GROUP BY
                                                            s.SITE_ID
                                                           ,SERVICE_MONTH ) z
                                                  GROUP BY
                                                      SITE_ID
                                                     ,SERVICE_MONTH ) a
                                          GROUP BY
                                                SITE_ID
                                               ,SERVICE_MONTH ) z
                                    ON z.SITE_ID = s.SITE_ID
                                       AND z.SERVICE_MONTH = cus.Service_Month
                          WHERE
                              ch.Client_Id = @Client_Id
                              AND cus.Service_Month BETWEEN @Begin_Date
                                                    AND     @End_Date
                              AND bmm.Bucket_Name IN ( 'total cost', 'total usage' )
                              AND s.NOT_MANAGED != 1
                              AND s.CLOSED != 1
                              AND ch.Region_ID != @Region_Id
                              AND acct.ACCOUNT_TYPE_ID = @Utility_Entity_Id ) y ) x
              GROUP BY
                  Client_Name
                 ,Sitegroup_Name
                 ,Country_Name
                 ,State_Name
                 ,City
                 ,Site_name
                 ,Service_Month
                 ,averagebdng
                 ,averagebdep
                 ,ng_is_complete
                 ,el_is_complete ) y    
             
          
END          
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Flint_Group] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Flint_Group] TO [CBMSApplication]
GO
