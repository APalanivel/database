SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:      
 dbo.cbmsUtilityDetailAnalystMap_Save    
     
DESCRIPTION:      
 Used to insert data into utility_detail_analyst_map    
      
INPUT PARAMETERS:      
 Name      DataType Default Description      
------------------------------------------------------------      
@MyAccountId int
	, @utility_detail_id int
	, @group_info_id int
	, @vendor_id int
	, @analyst_id int   
                
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:      
------------------------------------------------------------    

    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
KH   Kevin Horton      
 
     
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
KH	 11/5/2009			Created    
KH   11/5/2009			Removed vendor_id param
******/

CREATE PROCEDURE [dbo].[cbmsUtilityDetailAnalystMap_Save]
	( @MyAccountId int
	, @utility_detail_id int
	, @group_info_id int
	, @analyst_id int = null
	
	
	)
AS
BEGIN

	set nocount on

	  declare @this_id int		
	

	   select @this_id = utility_detail_analyst_map_id
	     from utility_detail_analyst_map
	    where utility_detail_id = @utility_detail_id
			and group_info_id = @group_info_id
	
		if @this_id is null
		begin	
			insert into utility_detail_analyst_map
				( utility_detail_id
				, group_info_id
				, analyst_id				
				)
				values
				( @utility_detail_id
				, @group_info_id
				, @analyst_id
				)
	
			set @this_id = SCOPE_IDENTITY()	
		end
		else
		begin
			update utility_detail_analyst_map
				set analyst_id = @analyst_id
			where utility_detail_analyst_map_id = @this_id
		end
			
	exec cbmsUtilityDetailAnalystMap_Get @MyAccountId, @utility_detail_id, @group_info_id
	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsUtilityDetailAnalystMap_Save] TO [CBMSApplication]
GO
