SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  [dbo].[Check_Duplicate_ACT_Commodity_Name]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType  Default      Description    
---------------------------------------------------------------------------                  
  @ACTCommmodityID   VARCHAR(255)
  @ACTCommmodityName VARCHAR(250)
  @CBMSCommodityID   VARCHAR(MAX)
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
 



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		   Naga Jyothi
   
MODIFICATIONS    
    
 Initials	Date		Modification    
---------------------------------------------------------------------------------------    
 NJ		   2019-07-06	 Created for SE2017-733 ACT Commodity Mapping within CBMS
******/
CREATE PROCEDURE [dbo].[Check_Duplicate_ACT_Commodity_Name]
    (
        @ACT_Commodity_Id VARCHAR(255)
        , @ACT_Commodity_Name VARCHAR(255)
        , @CBMSCommodity VARCHAR(MAX)
    )
AS
    BEGIN

        SET @ACT_Commodity_Id = ISNULL(@ACT_Commodity_Id, 0);
        SET @ACT_Commodity_Name = ISNULL(@ACT_Commodity_Name, 0);

        DECLARE @count INT;

        SET NOCOUNT ON;
        IF @ACT_Commodity_Id = 0
            BEGIN
                SELECT
                    @count = COUNT(*)
                FROM
                    dbo.ACT_Commodity ac
                WHERE
                    ac.ACT_Commodity_XName = @ACT_Commodity_Name;

            END;
        ELSE
            BEGIN
                SELECT
                    @count = COUNT(*)
                FROM
                    dbo.ACT_Commodity ac
                WHERE
                    ac.ACT_Commodity_Id != @ACT_Commodity_Id
                    AND ac.ACT_Commodity_XName = @ACT_Commodity_Name;

            END;

        IF (@count > 0)
            BEGIN
                IF @ACT_Commodity_Id = 0
                    BEGIN

                        SELECT
                            'ACT XML Commodity is already exists'
                        FROM
                            dbo.ACT_Commodity ac
                        WHERE
                            ac.ACT_Commodity_XName = @ACT_Commodity_Name;
                    END;
                ELSE
                    BEGIN

                        SELECT
                            'ACT XML Commodity is already exists'
                        FROM
                            dbo.ACT_Commodity ac
                        WHERE
                            ac.ACT_Commodity_Id != @ACT_Commodity_Id
                            AND ac.ACT_Commodity_XName = @ACT_Commodity_Name;

                    END;
                RETURN;
            END;

        DECLARE @name NVARCHAR(255);
        DECLARE @pos INT;
        DECLARE @csv TABLE
              (
                  Name VARCHAR(MAX)
              );
        WHILE CHARINDEX(',', @CBMSCommodity) > 0
            BEGIN
                SELECT  @pos = CHARINDEX(',', @CBMSCommodity);
                SELECT  @name = SUBSTRING(@CBMSCommodity, 1, @pos - 1);

                INSERT INTO @csv SELECT @name;

                SELECT
                    @CBMSCommodity = SUBSTRING(@CBMSCommodity, @pos + 1, LEN(@CBMSCommodity) - @pos);
            END;

        INSERT INTO @csv SELECT @CBMSCommodity;

        IF @ACT_Commodity_Id = 0
            BEGIN
                SELECT
                    ISNULL('CBMS Commodity ' + STUFF((   SELECT
                                                                ',' + c.Commodity_Name
                                                         FROM
                                                                dbo.Commodity_ACT_Commodity_Map cacm
                                                                JOIN dbo.Commodity c
                                                                    ON cacm.Commodity_Id = c.Commodity_Id
                                                                JOIN @csv cs
                                                                    ON cs.Name = c.Commodity_Name
                                                         FOR XML PATH('')), 1, 1, '') + ' already mapped', '') AS Val;


                RETURN;
            END;
        ELSE
            BEGIN

                SELECT
                    ISNULL('CBMS Commodity ' + STUFF((   SELECT
                                                                ',' + c.Commodity_Name
                                                         FROM
                                                                dbo.Commodity_ACT_Commodity_Map cacm
                                                                JOIN dbo.Commodity c
                                                                    ON cacm.Commodity_Id = c.Commodity_Id
                                                                JOIN @csv cs
                                                                    ON cs.Name = c.Commodity_Name
                                                         WHERE
                                                             cacm.ACT_Commodity_Id != @ACT_Commodity_Id
                                                         FOR XML PATH('')), 1, 1, '') + ' already mapped', '') AS Val;

                RETURN;
            END;


    END;

GO
GRANT EXECUTE ON  [dbo].[Check_Duplicate_ACT_Commodity_Name] TO [CBMSApplication]
GO
