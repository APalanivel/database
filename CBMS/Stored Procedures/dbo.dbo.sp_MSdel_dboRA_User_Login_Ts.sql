SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSdel_dboRA_User_Login_Ts]
		@pkc1 int
as
begin  
	delete [dbo].[RA_User_Login_Ts]
where [User_Login_Ts_Id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end  
GO
