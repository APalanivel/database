SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Country_Commodity_Uom_Sel]
           
DESCRIPTION:             
			To get country and commodity specific default UOM
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL
    @StartIndex		INT			1
    @EndIndex		INT			2147483647


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	EXEC dbo.Country_Commodity_Uom_Sel null,null,1,100
	EXEC dbo.Country_Commodity_Uom_Sel null,100163


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-09		Global Sourcing - Created
								
******/
CREATE PROCEDURE [dbo].[Country_Commodity_Uom_Sel]
      ( 
       @Country_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      WITH  Cte_CC_UOM
              AS ( SELECT
                        ccu.COUNTRY_ID
                       ,con.COUNTRY_NAME
                       ,ccu.Commodity_Id
                       ,com.Commodity_Name
                       ,ccu.Default_Uom_Type_Id
                       ,default_Uom.ENTITY_NAME AS Default_Uom
                       ,Row_Num = row_number() OVER ( ORDER BY com.Commodity_Name, con.COUNTRY_NAME )
                       ,Total = count(1) OVER ( )
                   FROM
                        dbo.Country_Commodity_Uom ccu
                        INNER JOIN dbo.COUNTRY con
                              ON ccu.COUNTRY_ID = con.COUNTRY_ID
                        INNER JOIN dbo.Commodity com
                              ON ccu.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.ENTITY default_Uom
                              ON ccu.Default_Uom_Type_Id = default_Uom.ENTITY_ID
                   WHERE
                        ( @Country_Id IS NULL
                          OR ccu.COUNTRY_ID = @Country_Id )
                        AND ( @Commodity_Id IS NULL
                              OR ccu.Commodity_Id = @Commodity_Id ))
            SELECT
                  ccu.COUNTRY_ID
                 ,ccu.COUNTRY_NAME
                 ,ccu.Commodity_Id
                 ,ccu.Commodity_Name
                 ,ccu.Default_Uom_Type_Id
                 ,ccu.Default_Uom
                 ,ccu.Row_Num
                 ,ccu.Total
            FROM
                  Cte_CC_UOM ccu
            WHERE
                  ccu.Row_Num BETWEEN @StartIndex AND @EndIndex
            ORDER BY
                  ccu.Row_Num
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Country_Commodity_Uom_Sel] TO [CBMSApplication]
GO
