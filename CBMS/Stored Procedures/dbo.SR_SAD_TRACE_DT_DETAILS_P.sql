SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_TRACE_DT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	
	@toWhom        	int       	          	
	@dealStatusTypeId	int       	          	
	@updateDate    	datetime  	          	
	@traceOrderNumber	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_SAD_TRACE_DT_DETAILS_P 824
--select * from SR_DT_TRANSPORT_TERMS_CONDITIONS where sr_deal_ticket_id = 824

CREATE   PROCEDURE dbo.SR_SAD_TRACE_DT_DETAILS_P

@srDealTicketId int,
@toWhom int,
@dealStatusTypeId int,
@updateDate datetime,
@traceOrderNumber int


AS
set nocount on
	DECLARE @fromWhom int

	select @fromWhom=WITH_WHOM from SR_DEAL_TICKET_DETAILS where SR_DEAL_TICKET_ID=@srDealTicketId

	insert into SR_DEAL_TICKET_TRACE_LOG
		(SR_DEAL_TICKET_ID, FROM_WHOM, TO_WHOM, DEAL_STATUS_TYPE_ID, UPDATE_DATE, TRACING_ORDER_NUMBER)

	values
		(@srDealTicketId, @fromWhom, @toWhom, @dealStatusTypeId, @updateDate, @traceOrderNumber)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_TRACE_DT_DETAILS_P] TO [CBMSApplication]
GO
