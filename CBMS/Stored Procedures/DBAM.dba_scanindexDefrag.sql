SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [DBAM].[dba_scanindexDefrag]
      @database VARCHAR(128)
     ,@tableName VARCHAR(128) = NULL
     ,@scanMode VARCHAR(10) = N'LIMITED'
     ,@minFragmentation FLOAT = 10.0
     ,@minPageCount INT = 8
     ,@maxPageCount INT = NULL
AS /*********************************************************************************
    Name:       [dba_scanindexDefrag]

    Purpose:    Scan Index Fragmentation details and store into the table
    @database			Name of the database
    @tableName			Name of the table , by default it's null , it will scan the all tables in the database
    @scanMode          Specifies which scan mode to use to determine fragmentation levels.  Options are:
                            LIMITED - scans the parent level; quickest mode,
                                      recommended for most cases.
                            SAMPLED - samples 1% of all data pages; if less than
                                      10k pages, performs a DETAILED scan.
                            DETAILED - scans all data pages.  Use great care with
                                       this mode, AS it can cause performance issues.
	@minPageCount         Specifies how many pages must exist in an index in order to be considered for a defrag.  Defaulted to 8 pages, AS 
                            Microsoft recommends only defragging indexes with more than 1 extent (8 pages).  
    @minFragmentation   defaulted to 10%, will not defrag if fragmentation is less than that
    
*********************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET QUOTED_IDENTIFIER ON;
BEGIN
      BEGIN TRY

            DECLARE
                  @databaseid INT
                 ,@debugMessage NVARCHAR(4000) 

            SELECT
                  @databaseid = database_id
            FROM
                  sys.databases
            WHERE
                  [name] = @database
            TRUNCATE TABLE [DBAManagement].[dbo].[dba_indexDefragStatus]
            INSERT      INTO [DBAManagement].[dbo].[dba_indexDefragStatus]
                        ( 
                         databaseID
                        ,databaseName
                        ,objectID
                        ,indexID
                        ,partitionNumber
                        ,fragmentation
                        ,page_count
                        ,range_scan_count
                        ,scanDate )
                        SELECT
                              ps.database_id AS 'databaseID'
                             ,quotename(db_name(ps.database_id)) AS 'databaseName'
                             ,ps.[object_id] AS 'objectID'
                             ,ps.index_id AS 'indexID'
                             ,ps.partition_number AS 'partitionNumber'
                             ,sum(ps.avg_fragmentation_in_percent) AS 'fragmentation'
                             ,sum(ps.page_count) AS 'page_count'
                             ,os.range_scan_count
                             ,getdate() AS 'scanDate'
                        FROM
                              sys.dm_db_index_physical_stats(@databaseID, object_id(@tableName), NULL, NULL, @scanMode) AS ps
                              JOIN sys.dm_db_index_operational_stats(@databaseID, object_id(@tableName), NULL, NULL) AS os
                                    ON ps.database_id = os.database_id
                                       AND ps.[object_id] = os.[object_id]
                                       AND ps.index_id = os.index_id
                                       AND ps.partition_number = os.partition_number
                        WHERE
                              avg_fragmentation_in_percent >= @minFragmentation
                              AND ps.index_id > 0 -- ignore heaps
                              AND ps.page_count BETWEEN @minPageCount
                                                AND     isnull(@maxPageCount, page_count) 
                              --AND ps.page_count > @minPageCount
                              AND ps.index_level = 0 -- leaf-level nodes only, supports @scanMode
                        GROUP BY
                              ps.database_id
                             ,quotename(db_name(ps.database_id))
                             ,ps.[object_id]
                             ,ps.index_id
                             ,ps.partition_number
                             ,os.range_scan_count
            OPTION
                              ( MAXDOP 2 )


      END TRY
      BEGIN CATCH

            SET @debugMessage = error_message() + ' (Line Number: ' + cast(error_line() AS VARCHAR(10)) + ')';
            PRINT @debugMessage;
      END CATCH;
END

GO
