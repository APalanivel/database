SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsCuInvoiceServiceMonth_Merge

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------ 	          	
	@cu_invoice_id INT 
	@service_month DATETIME
	@Account_ID INT
	@Begin_Dt DATETIME
	@End_Dt DATETIME
	@Billing_Days INT 
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC cbmsCuInvoiceServiceMonth_Merge 3723755,'1 JAN 2010',1726,'1 JAN 2010','31 JAN 2010',30
SELECT * FROM CU_INVOICE_SERVICE_MONTH WHERE CU_INVOICE_ID = 3723755 --2269248


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	SKA			Shobhit Kumar Agrawal
	HG			Harihara Suthan G
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			06/07/2010	Created
	HG			11/23/2010	Script modified to fix #21664
								- As in some scenario application passing the time stamp value along with the @Service_Month converted the value to date before saving it to table.
******/

CREATE PROCEDURE [dbo].[cbmsCuInvoiceServiceMonth_Merge]
(
       @cu_invoice_id	INT
      ,@service_month	DATETIME
      ,@Account_ID		INT
      ,@Begin_Dt		DATETIME
      ,@End_Dt			DATETIME
      ,@Billing_Days	INT
)
AS
BEGIN

      SET NOCOUNT ON ;

      SET @service_month = CAST(@Service_Month AS DATE)

      MERGE INTO cu_invoice_service_month AS Tgt USING ( SELECT
                                                            @cu_invoice_id AS Cu_Invoice_ID
                                                           ,@service_month AS Service_Month
                                                           ,@Account_ID AS Account_Id
                                                           ,@Begin_Dt AS Begin_Dt
                                                           ,@End_Dt AS End_Dt
                                                           ,@Billing_Days AS Billing_Days ) Src
			ON ( Tgt.cu_invoice_id = Src.cu_invoice_id
                       AND Tgt.Account_ID = Src.Account_ID
                       AND Src.service_month = CASE WHEN Tgt.service_month IS NULL THEN Src.service_month
                                                    ELSE Tgt.service_month
                                               END ) 
            WHEN NOT MATCHED THEN INSERT ( cu_invoice_id, service_month, Account_ID, Begin_Dt, End_Dt, Billing_Days )
				VALUES
				(
				 Src.cu_invoice_id
				,Src.service_month
				,Src.Account_ID
				,Src.Begin_Dt
				,Src.End_Dt
				,Src.Billing_Days 
				) 
            WHEN MATCHED THEN UPDATE
				SET   
					tgt.Service_Month = src.Service_Month;
                
END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceServiceMonth_Merge] TO [CBMSApplication]
GO
