SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
----------------------------------------------------------------------------------------------------------------------  
/******  
NAME:  
 [DBO].[SUPPLIER_ACCOUNT_PROCESSING_INSTRUCTIONS_UPD]  
  
DESCRIPTION:  
 IF "APPLY THIS ON ALL EXISTING" IS SELECTED THEN UPDATE dbo.ALL THE VALUES FOR SELECTED CONTRACT  
   
  
  
INPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
@PROCESSINGINSTRUCTIONS VARCHAR(500), 
@CONTRACTID  INT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC SUPPLIER_ACCOUNT_DETAILS_UPD  .....  
  
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
NK  NAGESWARA RAO KOSURI  
  
MODIFICATIONS  
INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
 
NK	11/17/2009	CREATED    
  
*/  
  
CREATE PROCEDURE [DBO].[SUPPLIER_ACCOUNT_CONSUMPTION_LEVEL_UPD]  
  
 @CONSUMPTION_LEVEL_ID INT, 
 @CONTRACTID   INT   
 
AS  
BEGIN  
   
 SET NOCOUNT ON  
  
 UPDATE acc  
  SET acc.Variance_Consumption_Level_Id =   @CONSUMPTION_LEVEL_ID
	                  
 FROM  
  dbo.Account_Variance_Consumption_Level acc  
  JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP Samm ON samm.Account_id = acc.Account_Id  
 WHERE  
  samm.CONTRACT_ID = @CONTRACTID  
  
   
END



GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_CONSUMPTION_LEVEL_UPD] TO [CBMSApplication]
GO
