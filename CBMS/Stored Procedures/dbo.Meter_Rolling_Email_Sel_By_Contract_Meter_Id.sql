SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id         
                            
 DESCRIPTION:                            
   To get Supplier Account Details                           
                            
 INPUT PARAMETERS:              
                         
 Name       DataType         Default       Description            
------------------------------------------------------------------------------         
@User_Info_Id     INT    
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------         
     
EXEC dbo.Sourcing_Dashboard_Is_Rolling_Meter_Sel_By_User_Info_Id    
     @Contract_Id =1
         , @Meter_Id =1 
  
   
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
------------------------------------------------------------------------------         
 NR      Narayana Reddy    
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
------------------------------------------------------------------------------         
 NR                    2019-05-29       Created for - Add Contract.    
                           
******/
CREATE PROCEDURE [dbo].[Meter_Rolling_Email_Sel_By_Contract_Meter_Id]
    (
        @Contract_Id INT
        , @Meter_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            meterMap.Contract_ID
            , meterMap.METER_ID
            , ui.EMAIL_ADDRESS
        FROM
            Core.Client_Hier_Account utilacc
            INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP AS meterMap
                ON meterMap.METER_ID = utilacc.Meter_Id
            LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                ON utilacc.Account_Vendor_Id = vcm.VENDOR_ID
                   AND  utilacc.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = vcam.Analyst_Id
        WHERE
            utilacc.Account_Type = 'Utility'
            AND utilacc.Account_Not_Managed = 0
            AND meterMap.IS_HISTORY = 0
            AND meterMap.Is_Rolling_Meter = 1
            AND meterMap.Contract_ID = @Contract_Id
            AND meterMap.METER_ID = @Meter_Id
        GROUP BY
            meterMap.Contract_ID
            , meterMap.METER_ID
            , ui.EMAIL_ADDRESS;


    END;

GO
GRANT EXECUTE ON  [dbo].[Meter_Rolling_Email_Sel_By_Contract_Meter_Id] TO [CBMSApplication]
GO
