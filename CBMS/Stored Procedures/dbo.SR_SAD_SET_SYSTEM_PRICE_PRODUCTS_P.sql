
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_SAD_SET_SYSTEM_PRICE_PRODUCTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                    DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId                 varchar
	@sessionId              varchar
	@commodityId            int
	@productName            varchar(100)
	@productDescription     varchar(4000)
	@Countrty_Id_List		VARCHAR(MAX)
                          
                           
OUTPUT PARAMETERS:         
	Name		DataType    Default		Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION  
		EXEC dbo.SR_SAD_SET_SYSTEM_PRICE_PRODUCTS_P 1,1,290,'Pricing Product Global Sourcing - Phase2','Pricing Product Global Sourcing - Phase2','1,2,3,4,5'
		SELECT * FROM dbo.SR_PRICING_PRODUCT spp JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
			WHERE spp.SR_PRICING_PRODUCT_ID = ident_current('dbo.SR_PRICING_PRODUCT')
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-09-22	Global Sourcing - Phase2 - Added input parameter @Countrty_Id_List

******/
CREATE PROCEDURE [dbo].[SR_SAD_SET_SYSTEM_PRICE_PRODUCTS_P]
      (
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@commodityId INT
      ,@productName VARCHAR(100)
      ,@productDescription VARCHAR(4000)
      ,@Countrty_Id_List VARCHAR(MAX) = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @SR_PRICING_PRODUCT_ID INT;

      INSERT      INTO dbo.SR_PRICING_PRODUCT
                  (COMMODITY_TYPE_ID
                  ,PRODUCT_NAME
                  ,PRODUCT_DESCRIPTION )
      VALUES
                  (@commodityId
                  ,@productName
                  ,@productDescription );
                  
      SET @SR_PRICING_PRODUCT_ID = SCOPE_IDENTITY();
      
      EXEC dbo.Country_Sr_Pricing_Product_Ins_Upd
            @SR_PRICING_PRODUCT_ID = @SR_PRICING_PRODUCT_ID
           ,@Countrty_Id_List = @Countrty_Id_List
           ,@User_Info_Id = @userId;
      
      
END;

GO

GRANT EXECUTE ON  [dbo].[SR_SAD_SET_SYSTEM_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
