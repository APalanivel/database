SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketData_GetForPricePointAndSource]
	( @price_point varchar(200) = null
	, @source varchar(200) = null
	)

AS
BEGIN

	   select price_point_id
		, price_point
		, source
	     from el_market_data with (nolock)
	    where price_point = @price_point 
	      and source = @source

END
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketData_GetForPricePointAndSource] TO [CBMSApplication]
GO
