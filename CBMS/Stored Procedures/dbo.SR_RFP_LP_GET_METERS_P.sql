SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_RFP_LP_GET_METERS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@site_id int,
	@rfp_id int
	AS
	set nocount on
	select count(sr_rfp_account_meter_map_id) as meters 
	from 	sr_rfp_account_meter_map map(nolock),
		sr_rfp_account rfp_account(nolock),
		site s(nolock), 
		account a(nolock)


	where 	rfp_account.sr_rfp_id = @rfp_id
		and map.sr_rfp_account_id = rfp_account.sr_rfp_account_id 
		and a.account_id = rfp_account.account_id
		and rfp_account.is_deleted = 0
		and s.site_id = a.site_id
		and s.site_id = @site_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_METERS_P] TO [CBMSApplication]
GO
