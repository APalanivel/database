SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE  PROCEDURE dbo.BUDGET_SAVE_CONTRACT_BUDGET_MONTHS_P
	@budget_contract_id int,
	@month_identifier datetime,
	@contract_budget_month_id int out
	AS

	insert into budget_contract_budget_months(budget_contract_budget_id, month_identifier) values(@budget_contract_id, @month_identifier)
	select @contract_budget_month_id = scope_identity()









GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_CONTRACT_BUDGET_MONTHS_P] TO [CBMSApplication]
GO
