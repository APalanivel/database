SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
      dbo.Cu_Invoice_Cnt_Sel_By_Account_Id
   
 DESCRIPTION:   
      Gets the Test details based on commodity.
      
 INPUT PARAMETERS:  
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 @Account_Id		INT

 OUTPUT PARAMETERS:
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 USAGE EXAMPLES:  
---------------------------------------------------------------------------- 

EXEC dbo.Cu_Invoice_Cnt_Sel_By_Account_Id @Account_Id = 152169

Exec Cu_Invoice_Cnt_Sel_By_Account_Id 213987, '2017-02-01'


AUTHOR INITIALS:
Initials	Name
---------------------------------------------------------------------------- 
 NR			Narayana Reddy
 

 MODIFICATIONS   
 Initials	Date			Modification
----------------------------------------------------------------------------
 NR        2019-03-28		Created for Data Quality.

******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Cnt_Sel_By_Account_Id]
    (
        @Account_Id INT
        , @Service_Month DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Invoice_Count INT
            , @IS_REPORTED BIT;

        SELECT
            @Invoice_Count = COUNT(DISTINCT cism.CU_INVOICE_ID)
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.SERVICE_MONTH IS NOT NULL
            AND cism.Account_ID = @Account_Id
            AND (   @Service_Month IS NULL
                    OR  cism.SERVICE_MONTH = @Service_Month);


        SELECT
            @IS_REPORTED = ci.IS_REPORTED
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            cism.Account_ID = @Account_Id
            AND cism.SERVICE_MONTH = DATEADD(MM, -1, @Service_Month)
        ORDER BY
            ci.IS_REPORTED DESC;

        SELECT
            @Invoice_Count AS Invoice_Count
            , ISNULL(@IS_REPORTED, 0) AS Last_Month_Invoice_Is_Reported;

    END;





GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Cnt_Sel_By_Account_Id] TO [CBMSApplication]
GO
