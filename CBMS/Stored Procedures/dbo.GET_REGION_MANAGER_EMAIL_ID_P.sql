SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_REGION_MANAGER_EMAIL_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@regionId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE [dbo].[GET_REGION_MANAGER_EMAIL_ID_P] 
@userId varchar(10),
@sessionId varchar(20),
@regionId int
AS
set nocount on
SELECT 	
	EMAIL_ADDRESS 
FROM 
	USER_INFO 
WHERE 
	USER_INFO_ID IN (select user_info_id from REGION_MANAGER_MAP where region_id = @regionId)
GO
GRANT EXECUTE ON  [dbo].[GET_REGION_MANAGER_EMAIL_ID_P] TO [CBMSApplication]
GO
