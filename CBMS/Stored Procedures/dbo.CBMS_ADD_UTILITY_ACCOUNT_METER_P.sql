SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                 
NAME:                
 dbo.CBMS_ADD_UTILITY_ACCOUNT_METER_P                
            
DESCRIPTION:  This procedure used to insert the records in Meter table            
            
INPUT PARAMETERS:                
    Name                        DataType        Default     Description                
---------------------------------------------------------------------------                
 @rateId                     int            
 @purchaseMethodTypeId       int            
 @accountId     int            
 @addressId                  int            
 @meterNumber                varchar(50)            
 @taxExemptStatus            numeric(3216)            
 @taxExemptionStatusId       int            
 @expirationDate             datetime            
 @comments                   varchar(4000)            
 @Meter_Type_Cd    INT    NULL            
                
OUTPUT PARAMETERS:                
    Name            DataType          Default     Description                
----------------------------------------------------------------------                
 @meterId        int             
                
                
USAGE EXAMPLES:                
----------------------------------------------------------------------                
            
 EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'Meter Type'            
            
 BEGIN TRANSACTION            
  DECLARE @Meter_Id INT, @meterNumber VARCHAR(50) = 'Testing New - '+cast(getdate() AS VARCHAR(50))            
  EXEC dbo.CBMS_ADD_UTILITY_ACCOUNT_METER_P @rateId =8233,@purchaseMethodTypeId =241,@accountId =122169,@addressId =1,            
    @meterNumber = @meterNumber,@taxExemptStatus = null, @taxExemptionStatusId =null,@expirationDate =null,            
    @comments  = 'Testing New',@meterId = @Meter_Id OUT, @Meter_Type_Cd = 102041            
  SELECT * FROM dbo.METER WHERE METER_ID = @Meter_Id            
 ROLLBACK TRANSACTION            
            
AUTHOR INITIALS:            
 Initials Name                
------------------------------------------------------------                
 DR   Deana Ritter            
 NK   Nageswara Rao Kosuri            
 RR   Raghu Reddy            
            
MODIFICATIONS:            
 Initials Date  Modification                
------------------------------------------------------------                
 DR   8/4/2009 Removed Linked Server Updates            
 NK   01/08/2010 Changed the @meterNumber column size from 40 to 50 characters            
 RR   2015-10-06 Global Sourcing - Phase2 - Added new input type Meter Type            
 SC   2020-01-05 Added Primary Meter Merge Exec Sproc to capture the Primary meter flag for Budget 2.o change  
*******/
CREATE PROCEDURE [dbo].[CBMS_ADD_UTILITY_ACCOUNT_METER_P]
    (
        @rateId INT
        , @purchaseMethodTypeId INT
        , @accountId INT
        , @addressId INT
        , @meterNumber VARCHAR(50)
        , @taxExemptStatus NUMERIC(32, 16)
        , @taxExemptionStatusId INT
        , @meterId INT OUTPUT
        , @expirationDate DATETIME
        , @comments VARCHAR(4000)
        , @Meter_Type_Cd INT = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        DECLARE @Commodity_Id INT;
        SELECT
            @Commodity_Id = c.Commodity_Id
        FROM
            dbo.Commodity c
            INNER JOIN dbo.RATE r
                ON r.COMMODITY_TYPE_ID = c.Commodity_Id
        WHERE
            r.RATE_ID = @rateId;

        INSERT INTO dbo.METER
             (
                 RATE_ID
                 , PURCHASE_METHOD_TYPE_ID
                 , ACCOUNT_ID
                 , ADDRESS_ID
                 , METER_NUMBER
                 , TAX_EXEMPT_STATUS
                 , TAX_EXEMPTION_ID
                 , EXPIRATION_DATE
                 , COMMENTS
                 , Meter_Type_Cd
             )
        VALUES
            (@rateId
             , @purchaseMethodTypeId
             , @accountId
             , @addressId
             , @meterNumber
             , @taxExemptStatus
             , @taxExemptionStatusId
             , @expirationDate
             , @comments
             , @Meter_Type_Cd);

        SELECT  @meterId = SCOPE_IDENTITY();

        DECLARE @Commodity_Meter_Id_In NVARCHAR(MAX);
        SELECT
            @Commodity_Meter_Id_In = CAST(@Commodity_Id AS VARCHAR(200)) + N'|' + CAST(@meterId AS VARCHAR(200));

        IF NOT EXISTS (   SELECT
                                1
                          FROM
                                Budget.Account_Commodity_Primary_Meter
                          WHERE
                                Account_Id = @accountId
                                AND Commodity_Id = @Commodity_Id)
            BEGIN
                EXEC Budget.Account_Commodity_Primary_Meter_Merge
                    @Account_Id = @accountId
                    , @Commodity_Meter_Id = @Commodity_Meter_Id_In
                    , @User_Info_Id = @User_Info_Id;
            END;
        SELECT  @meterId;

    END;
GO

GRANT EXECUTE ON  [dbo].[CBMS_ADD_UTILITY_ACCOUNT_METER_P] TO [CBMSApplication]
GO
