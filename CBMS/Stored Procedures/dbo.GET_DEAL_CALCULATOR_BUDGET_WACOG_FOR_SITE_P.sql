SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  

NAME:  
 dbo.Get_Deal_Calculator_Budget_Wacog_For_Site_P
   
DESCRIPTION:   

INPUT PARAMETERS:  
	Name			DataType	Default Description  
------------------------------------------------------------  
	@userId VARCHAR(10)
    @sessionId VARCHAR(20)
	@clientId		INT
	@siteId			INT
	@startDate		DATETIME
	@endDate		DATETIME
	@startYear		INT
	@endYear		INT  
 
         
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------
		
		EXEC dbo.Get_Deal_Calculator_Budget_Wacog_For_Site_P 1043,1639,'1-1-2005','12-1-2005',2005,2005    
		EXEC dbo.Get_Deal_Calculator_Budget_Wacog_For_Site_P 10015,10348,'1-1-2005','12-1-2005',2005,2005    
		EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITE_P 49,'585CFC7D8E7BC2C42C5AA341CC700AA3',11610,258266,'01/01/2011','12/01/2011',2011,2011

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 
 
 MODIFICATIONS
 Initials	Date		Modification  
------------------------------------------------------------
 SKA		12/02/2010	Removed one tmp tables which is not required to get the data for SiteLevel Bug#21455
						This sp is modified to fix one of the bug only, not did any more chnages included removing of unused parameter
******/ 


CREATE PROCEDURE dbo.GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITE_P
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@clientId INT
     ,@siteId INT
     ,@startDate DATETIME
     ,@endDate DATETIME
     ,@startYear INT
     ,@endYear INT
AS 
BEGIN    
    
 --FOR SITE      
      SET NOCOUNT ON    
    
      DECLARE @ResultSet TABLE
            (
             priceVolume DECIMAL(32, 16)
            ,VOLUME DECIMAL(32, 16)
            ,FORECAST_ID INT
            ,SITEID INT
            ,TARGETPRICE DECIMAL(32, 16)
            ,TARGETID INT
            ,DEALMONTHYEAR DATETIME
            ,MI2 DATETIME
            ,MONTH_IDENTIFIER VARCHAR(12) )    
    
      DECLARE @tblbase TABLE
            (
             RM_TARGET_BUDGET_ID INT
            ,DIVISION_ID INT
            ,Site_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,BUDGET_TARGET DECIMAL(32, 16) )    
     
      DECLARE @tblIDColln1 TABLE
            (
             RM_TARGET_BUDGET_ID INT
            ,DIVISION_ID INT
            ,Site_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,BUDGET_TARGET DECIMAL(32, 16) )    
      DECLARE @tblIDColln2 TABLE
            (
             RM_TARGET_BUDGET_ID INT
            ,DIVISION_ID INT
            ,Site_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,BUDGET_TARGET DECIMAL(32, 16) )    
      DECLARE @tblIDColln3 TABLE
            (
             RM_TARGET_BUDGET_ID INT
            ,DIVISION_ID INT
            ,Site_ID INT
            ,MONTH_IDENTIFIER DATETIME
            ,BUDGET_TARGET DECIMAL(32, 16) )    
    
      DECLARE
            @Forecast_As_Of_Date_Startyear DATETIME
           ,@Forecast_As_Of_Date_Endyear DATETIME
           ,@InsertedCount INT
           ,@divisionId INT    
    
      SELECT
            @Forecast_As_Of_Date_Startyear = MAX(FORECAST_AS_OF_DATE)
      FROM
            dbo.RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @startYear
            AND CLIENT_ID = @clientId    
      SELECT
            @Forecast_As_Of_Date_Endyear = MAX(FORECAST_AS_OF_DATE)
      FROM
            dbo.RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @endYear
            AND CLIENT_ID = @clientId    
    
      SELECT
            @divisionId = division_id
      FROM
            dbo.site
      WHERE
            site_id = @siteId    
     
      INSERT INTO
            @tblBase
            SELECT DISTINCT
                  RM_TARGET_BUDGET_ID
                 ,rm_bdg.DIVISION_ID
                 ,rm_bdg.Site_ID
                 ,RM_TARGET_BUDGET.MONTH_IDENTIFIER
                 ,RM_TARGET_BUDGET.BUDGET_TARGET
            FROM
                  dbo.RM_TARGET_BUDGET
                  JOIN dbo.RM_BUDGET rm_bdg ON rm_bdg.RM_BUDGET_ID = RM_TARGET_BUDGET.RM_BUDGET_ID
                  JOIN dbo.RM_BUDGET rm_bdg_in ON rm_bdg_in.RM_BUDGET_ID = rm_bdg.RM_BUDGET_ID
            WHERE
                  rm_bdg.CLIENT_ID = @clientId
                  AND rm_bdg.IS_BUDGET_APPROVED = 1
                  AND RM_TARGET_BUDGET.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                        AND     CONVERT(VARCHAR(12), @endDate, 101)
                  AND ( ( CONVERT(VARCHAR(12), @startDate, 101) BETWEEN rm_bdg_in.BUDGET_START_MONTH
                                                                AND     rm_bdg_in.BUDGET_END_MONTH )
                        OR ( CONVERT(VARCHAR(12), @endDate, 101) BETWEEN rm_bdg_in.BUDGET_START_MONTH
                                                                 AND     rm_bdg_in.BUDGET_END_MONTH )
                        OR ( rm_bdg_in.BUDGET_START_MONTH BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                          AND     CONVERT(VARCHAR(12), @endDate, 101) )
                        OR ( rm_bdg_in.BUDGET_END_MONTH BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                        AND     CONVERT(VARCHAR(12), @endDate, 101) ) )    

    
      INSERT INTO
            @tblIDColln1
            SELECT
                  RM_TARGET_BUDGET_ID
                 ,DIVISION_ID
                 ,Site_ID
                 ,MONTH_IDENTIFIER
                 ,BUDGET_TARGET
            FROM
                  @tblBase
            WHERE
                  DIVISION_ID = @divisionId
                  AND SITE_ID IS NULL    
    
      INSERT INTO
            @tblIDColln2
            SELECT
                  b.RM_TARGET_BUDGET_ID
                 ,b.DIVISION_ID
                 ,b.Site_ID
                 ,b.MONTH_IDENTIFIER
                 ,b.BUDGET_TARGET
            FROM
                  @tblBase b
                  JOIN Site sit ON sit.Site_ID = b.Site_ID
            WHERE
                  sit.division_id = @divisionId    
    
      INSERT INTO
            @tblIDColln3
            SELECT
                  RM_TARGET_BUDGET_ID
                 ,DIVISION_ID
                 ,Site_ID
                 ,MONTH_IDENTIFIER
                 ,BUDGET_TARGET
            FROM
                  @tblBase
            WHERE
                  DIVISION_ID IS NULL
                  AND SITE_ID IS NULL    
    
      INSERT INTO
            @ResultSet
            SELECT
                  volumedetails.VOLUME * tblSite.BUDGET_TARGET priceVolume
                 ,volumedetails.VOLUME VOLUME
                 ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID
                 ,volumedetails.SITE_ID SITEID
                 ,tblSite.BUDGET_TARGET TARGETPRICE
                 ,tblSite.RM_TARGET_BUDGET_ID TARGETID
                 ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR
                 ,tblSite.MONTH_IDENTIFIER MI2
                 ,CONVERT (VARCHAR(12), tblSite.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
            FROM
                  RM_FORECAST_VOLUME_DETAILS volumedetails
                  JOIN RM_FORECAST_VOLUME volume ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                  JOIN RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id
                  JOIN @tblBase tblSite ON tblSite.Site_ID = volumedetails.SITE_ID -- Evaluate whether this join is required    
                                           and tblSite.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER
            WHERE
                  volume.CLIENT_ID = @clientId
                  AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                     AND     CONVERT(VARCHAR(12), @endDate, 101)
                  AND tblSite.SITE_ID = @siteId    
    
 --division      
      IF @@ROWCOUNT = 0 
            BEGIN    
      
                  INSERT INTO
                        @ResultSet
                        SELECT
                              volumedetails.VOLUME * TblTrgBdg.BUDGET_TARGET priceVolume
                             ,volumedetails.VOLUME VOLUME
                             ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID
                             ,volumedetails.SITE_ID SITEID
                             ,TblTrgBdg.BUDGET_TARGET TARGETPRICE
                             ,TblTrgBdg.RM_TARGET_BUDGET_ID TARGETID
                             ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR
                             ,TblTrgBdg.MONTH_IDENTIFIER MI2
                             ,CONVERT (VARCHAR(12), TblTrgBdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                        FROM
                              RM_FORECAST_VOLUME_DETAILS volumedetails
                              JOIN RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID
                              JOIN RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id
                              JOIN @tblIDColln1 TblTrgBdg ON TblTrgBdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER
                              JOIN Site sit ON sit.Site_ID = volumedetails.Site_ID
                                               AND sit.Division_ID = TblTrgBdg.Division_ID
                              LEFT OUTER JOIN @tblIDColln2 tblSite ON tblSite.Site_ID = volumedetails.SITE_ID
                        WHERE
                              volume.CLIENT_ID = @clientId
                              AND volume.FORECAST_AS_OF_DATE IN ( @Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear )
                              AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                                 AND     CONVERT(VARCHAR(12), @endDate, 101)
                              AND tblSite.Site_ID IS NULL    
       
                  SET @InsertedCount = @@ROWCOUNT    
    
            END    
    
 --client      
      IF @InsertedCount = 0 
            BEGIN    
    
                  INSERT INTO
                        @ResultSet
                        SELECT
                              volumedetails.VOLUME * TblTrgBdg.BUDGET_TARGET priceVolume
                             ,volumedetails.VOLUME VOLUME
                             ,volumedetails.RM_FORECAST_VOLUME_DETAILS_ID FORECAST_ID
                             ,volumedetails.SITE_ID SITEID
                             ,TblTrgBdg.BUDGET_TARGET TARGETPRICE
                             ,TblTrgBdg.RM_TARGET_BUDGET_ID TARGETID
                             ,volumedetails.MONTH_IDENTIFIER DEALMONTHYEAR
                             ,TblTrgBdg.MONTH_IDENTIFIER MI2
                             ,CONVERT (VARCHAR(12), TblTrgBdg.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                        FROM
                              RM_FORECAST_VOLUME_DETAILS volumedetails
                              JOIN RM_FORECAST_VOLUME volume ON volume.RM_FORECAST_VOLUME_ID = volumedetails.RM_FORECAST_VOLUME_ID
                              JOIN RM_BUDGET_SITE_MAP ON RM_BUDGET_SITE_MAP.SITE_ID = volumedetails.site_id
                              JOIN @tblIDColln3 TblTrgBdg ON TblTrgBdg.MONTH_IDENTIFIER = volumedetails.MONTH_IDENTIFIER
                              JOIN Site sit ON sit.Site_ID = volumedetails.Site_ID
                              JOIN Division div ON div.Division_ID = sit.Division_ID
                        WHERE
                              volume.CLIENT_ID = @clientId
                              AND volume.FORECAST_AS_OF_DATE IN ( @Forecast_As_Of_Date_Startyear, @Forecast_As_Of_Date_Endyear )
                              AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                                                 AND     CONVERT(VARCHAR(12), @endDate, 101)
                              AND div.CLIENT_ID = @clientId      
            END    
    
      SELECT
            SUM(A.priceVolume) priceVolume
           ,SUM(A.VOLUME) volume
           ,CASE SUM(A.VOLUME)
              WHEN 0 THEN 0.0
              ELSE SUM(A.priceVolume) / SUM(A.VOLUME)
            END AS BudgetWACOGPRICE
           ,DATEPART(MONTH, A.DEALMONTHYEAR) dealTicketMonth
           ,DATEPART(YEAR, A.DEALMONTHYEAR) dealTicketYear
           ,A.MONTH_IDENTIFIER MONTH_IDENTIFIER
      FROM
            @ResultSet a
      GROUP BY
            DATEPART(MONTH, a.DEALMONTHYEAR)
           ,DATEPART(YEAR, a.DEALMONTHYEAR)
           ,a.MONTH_IDENTIFIER     
    
END 

GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITE_P] TO [CBMSApplication]
GO
