
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*******************        
Name:

    dbo.Report_DE_GFEJF

Description:

Input Parameters:        
 Name     DataType  Default Description        
---------------------------------------------------------------        
         
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
EXEC dbo.Report_DE_GFEJF         
         
AUTHOR INITIALS:        
Initials	Name        
------------------------------------------------------------        
AKR         Ashok Kumar Raju        
AJ			Ajay Chejarla            
HG			Harihara Suthan G    
RR			Raghu Reddy
        
MODIFICATIONS:        
Initials	Date		Modification        
------------------------------------------------------------        
AKR			2012-05-28	Cloned from lec_GFEJF
AJ			12/17/2012	Added two columns Utility Account ID, Supplier Account ID to the select list
AKR			2013-01-28	Added the Analyst Name and Broker Fee columns
HG			2013-05-02	Added INNER JOIN with code table to filter only the Cost & usages services mapped to the client in Core.Client_Commodity
AKR			2013-09-04	Added the Sourcing Analyst Mappings for Client, Account and Site
RR			2017-03-10	Added new columns
						SupplierAccountStartDate
						SupplierAccountEndDate when null will be shown as Unspecified and date is casted to varchar as to show Unspecified when null.
						SupplierAccountCreatedBy - show username similar to the existing column "ContractCreatedBy" in the Joined File today.
						SupplierAccountCreatedDate  - show date and timestamp similar to the existing column "AccountCreated" in the Joined File today.

*******************/
CREATE  PROCEDURE [dbo].[Report_DE_GFEJF]
AS 
BEGIN    
    
      SET NOCOUNT ON;        
      DECLARE @Client_Type_Id INT        
        
      SELECT
            @Client_Type_Id = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_DESCRIPTION = 'Client Type'
            AND en.ENTITY_NAME = 'GfE'        
      
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Account_Table INT
           
      SELECT
            @Account_Table = MAX(CASE WHEN ENTITY_NAME = 'Account_Table' THEN ENTITY_ID
                                 END)
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION IN ( 'Table_Type', 'Table Type', 'Contract Type', 'Client Type' );   
                         
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type';      
    
      WITH  cte_Account_List
              AS ( SELECT
                        ch.client_name AS ClientName
                       ,ch.client_id AS ClientID
                       ,ClientNotManaged = CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                                                ELSE 'No'
                                           END
                       ,ch.Sitegroup_Name AS DivisionName
                       ,ch.Sitegroup_Id AS DivisionID
                       ,DivisionNotManaged = CASE WHEN ch.Division_Not_Managed = 1 THEN 'Yes'
                                                  ELSE 'No'
                                             END
                       ,ch.Site_name AS SiteName
                       ,ch.Site_id AS SiteID
                       ,SiteCreated = eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER AS SiteRef
                       ,rg.group_name AS SiteRMGroup
                       ,SiteNotManaged = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                                              ELSE 'No'
                                         END
                       ,SiteClosed = CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                                          ELSE 'No'
                                     END
                       ,ch.Site_Type_Name AS SiteType
                       ,ch.Site_Address_Line1 AS AddressLine1
                       ,ch.Site_Address_Line2 AS AddressLine2
                       ,ch.city AS City
                       ,ch.state_name AS State_Province
                       ,ch.zipcode AS ZipCode
                       ,ch.country_name AS Country
                       ,ch.region_name AS Region
                       ,utility.account_number AS UtilityAccountNumber
                       ,esvl.entity_name AS UtilityAccountServiceLevel
                       ,eaacct.modified_date AS AccountCreated
                       ,utility.Account_Not_Expected AS UtilityAccountInvoiceNotExpected
                       ,UtilityAccountNotManaged = CASE WHEN utility.Account_Not_Managed = 1 THEN 'Yes'
                                                        ELSE 'No'
                                                   END
                       ,euacctso.entity_name AS UtilityAccountInvoiceSource
                       ,utility.Account_Vendor_Name AS Utility
                       ,ecomm.entity_name AS CommodityType
                       ,utility.Rate_Name AS Rate
                       ,utility.Meter_Number AS MeterNumber
                       ,PurchaseMethod = CASE WHEN fmet.meter_id IS NOT NULL THEN 'OnFutureSupplyContract'
                                              ELSE 'NotOnFutureSupplyContract'
                                         END
                       ,samm.Supplier_Contract_ID AS ContractID
                       ,c.BASE_CONTRACT_ID AS BaseContractID
                       ,c.ED_CONTRACT_NUMBER AS ContractNumber
                       ,ContractPricingStatus = CASE WHEN lps.load_profile_specification_id IS NULL THEN 'NotBuilt'
                                                     ELSE 'Built'
                                                END
                       ,ecty.Entity_name AS ContractType
                       ,utility.Account_Vendor_Name AS ContractedVendor
                       ,c.CONTRACT_START_DATE AS ContractStartDate
                       ,c.CONTRACT_END_DATE AS ContractEndDate
                       ,NotificationDate = c.CONTRACT_END_DATE - ( c.NOTIFICATION_DAYS + 15 )
                       ,ContractTriggerRights = ISNULL(CASE c.IS_CONTRACT_TRIGGER_RIGHTS
                                                         WHEN 1 THEN 'Yes'
                                                         ELSE 'No'
                                                       END, 'Null')
                       ,FullRequirements = ISNULL(CASE c.IS_CONTRACT_FULL_REQUIREMENT
                                                    WHEN 1 THEN 'Yes'
                                                    ELSE 'No'
                                                  END, 'Null')
                       ,c.CONTRACT_PRICING_SUMMARY AS Contract_Pricing_Summary
                       ,c.CONTRACT_COMMENTS AS ContractComments
                       ,curr.currency_unit_name AS Currency
                       ,eren.ENTITY_NAME AS RenewalType
                       ,samm.Account_Number AS SupplierAccountNumber
                       ,esuasv.ENTITY_NAME AS SupplierAccountServiceLevel
                       ,esacctso.ENTITY_NAME AS SupplierAccountInvoiceSource
                       ,ContractCreatedBy = ui.first_name + SPACE(1) + ui.last_name
                       ,eac.modified_date AS ContractCreatedDate
                       ,UtilityGroupBillStatus = CASE WHEN utility.Account_Group_ID IS NOT NULL THEN 'PartofGroupBill'
                                                      ELSE 'No'
                                                 END
                       ,ContractRecalcType = ereca.ENTITY_NAME
                       ,ContractClassification = eclas.ENTITY_NAME
                       ,utility.Account_Id [Utility Account ID]
                       ,samm.Account_Id [Supplier Account ID]
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,COALESCE(Utility.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
                       ,Is_Broker_Account = CASE Utility.Account_Is_Broker
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME BrokerFee_CurrencyUnit
                       ,euom.ENTITY_NAME BrokerFee_UomName
                       ,samm.Supplier_Account_begin_Dt AS SupplierAccountStartDate
                       ,samm.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui1.USERNAME AS SupplierAccountCreatedBy
                       ,ea1.MODIFIED_DATE AS SupplierAccountCreatedDate
                   FROM
                        --Contract Section         
                        dbo.contract c
                        JOIN CORE.Client_Hier_Account samm
                              ON c.CONTRACT_ID = samm.Supplier_Contract_ID
                                 AND samm.Account_Type = 'Supplier'
                                 AND ( samm.Supplier_Meter_Disassociation_Date IS NULL
                                       OR samm.Supplier_Meter_Disassociation_Date >= c.CONTRACT_START_DATE )
                        JOIN CORE.Client_Hier_Account Utility
                              ON samm.meter_Id = utility.Meter_Id
                                 AND Utility.Account_Type = 'Utility'
                        LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON Utility.Account_Id = acbf.Account_Id
                                 AND Utility.Commodity_Id = acbf.Commodity_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                              ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
                        LEFT OUTER JOIN dbo.ENTITY euom
                              ON euom.ENTITY_ID = acbf.UOM_Entity_Id
                        JOIN Core.Client_Hier CH
                              ON ch.Client_Hier_Id = Utility.Client_Hier_Id
                        JOIN dbo.ENTITY eren
                              ON eren.entity_id = c.renewal_type_id
                        JOIN dbo.ENTITY esuasv
                              ON esuasv.entity_id = samm.Account_Service_level_Cd
                        LEFT OUTER JOIN dbo.ENTITY esacctso
                              ON esacctso.entity_id = samm.Account_Invoice_Source_Cd
                        JOIN dbo.ENTITY_audit eac
                              ON eac.entity_identifier = c.contract_id
                                 AND eac.entity_id = 494
                                 AND eac.audit_type = 1
                        LEFT OUTER JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                              ON lps.contract_id = c.contract_id
                        JOIN dbo.ENTITY ecty
                              ON ecty.entity_id = c.contract_type_id
                        JOIN dbo.CURRENCY_UNIT curr
                              ON curr.currency_unit_id = c.currency_unit_id
                        JOIN dbo.USER_INFO ui
                              ON ui.user_info_id = eac.user_info_id
                        JOIN dbo.ENTITY ereca
                              ON ereca.entity_id = c.CONTRACT_RECALC_TYPE_ID
                        JOIN dbo.ENTITY eclas
                              ON eclas.entity_id = c.Contract_Classification_Cd
                        JOIN dbo.SITE s
                              ON s.SITE_ID = CH.Site_Id
                        JOIN dbo.ENTITY ecomm
                              ON ecomm.entity_id = samm.Commodity_Id
                        LEFT OUTER JOIN dbo.ENTITY esvl
                              ON esvl.entity_id = Utility.Account_Service_level_Cd
                        JOIN dbo.ENTITY_AUDIT eaacct
                              ON eaacct.entity_identifier = Utility.account_id
                                 AND eaacct.entity_id = 491
                                 AND eaacct.audit_type = 1
                        LEFT OUTER JOIN dbo.ENTITY euacctso
                              ON euacctso.entity_id = Utility.Account_Invoice_Source_Cd
                        LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgsm
                              ON rgsm.site_id = ch.site_id
                        LEFT OUTER JOIN dbo.RM_GROUP rg
                              ON rg.rm_group_id = rgsm.rm_group_id
                        JOIN dbo.ENTITY_AUDIT eas
                              ON eas.entity_identifier = ch.site_id
                                 AND eas.entity_id = 506
                                 AND eas.audit_type = 1
                        LEFT OUTER JOIN ( SELECT
                                                sam.meter_id
                                          FROM
                                                core.Client_Hier_Account sam
                                                JOIN contract c
                                                      ON c.contract_id = sam.Supplier_Contract_ID
                                          WHERE
                                                sam.Account_Type = 'Supplier'
                                                AND sam.Supplier_Meter_Disassociation_Date IS NULL
                                                AND c.contract_type_id = 153
                                                AND c.contract_end_date > GETDATE() ) fmet
                              ON fmet.Meter_Id = samm.meter_id
                        LEFT JOIN dbo.ENTITY_AUDIT ea1
                              ON ea1.ENTITY_IDENTIFIER = samm.ACCOUNT_ID
                                 AND ea1.ENTITY_ID = @Account_Table --494                      
                                 AND ea1.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui1
                              ON ui1.USER_INFO_ID = ea1.USER_INFO_ID
                   WHERE
                        ch.client_type_id = @Client_Type_Id
                        OR ch.client_name = 'Cytec Industries Inc.'
                   GROUP BY
                        ch.client_name
                       ,ch.client_id
                       ,CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,ch.Division_Not_Managed
                       ,ch.Site_name
                       ,ch.Site_id
                       ,eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER
                       ,rg.group_name
                       ,CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,ch.Site_Type_Name
                       ,ch.Site_Address_Line1
                       ,ch.Site_Address_Line2
                       ,ch.city
                       ,ch.state_name
                       ,ch.zipcode
                       ,ch.country_name
                       ,ch.region_name
                       ,utility.account_number
                       ,esvl.entity_name
                       ,eaacct.modified_date
                       ,utility.Account_Not_Expected
                       ,CASE WHEN utility.Account_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,euacctso.entity_name
                       ,utility.Account_Vendor_Name
                       ,ecomm.entity_name
                       ,utility.Rate_Name
                       ,utility.Meter_Number
                       ,CASE WHEN fmet.meter_id IS NOT NULL THEN 'OnFutureSupplyContract'
                             ELSE 'NotOnFutureSupplyContract'
                        END
                       ,samm.Supplier_Contract_ID
                       ,c.BASE_CONTRACT_ID
                       ,c.ED_CONTRACT_NUMBER
                       ,CASE WHEN lps.load_profile_specification_id IS NULL THEN 'NotBuilt'
                             ELSE 'Built'
                        END
                       ,ecty.Entity_name
                       ,utility.Account_Vendor_Name
                       ,c.CONTRACT_START_DATE
                       ,c.CONTRACT_END_DATE
                       ,c.CONTRACT_END_DATE - ( c.NOTIFICATION_DAYS + 15 )
                       ,ISNULL(CASE c.IS_CONTRACT_TRIGGER_RIGHTS
                                 WHEN 1 THEN 'Yes'
                                 ELSE 'No'
                               END, 'Null')
                       ,ISNULL(CASE c.IS_CONTRACT_FULL_REQUIREMENT
                                 WHEN 1 THEN 'Yes'
                                 ELSE 'No'
                               END, 'Null')
                       ,c.CONTRACT_PRICING_SUMMARY
                       ,c.CONTRACT_COMMENTS
                       ,curr.currency_unit_name
                       ,eren.ENTITY_NAME
                       ,samm.Account_Number
                       ,esuasv.ENTITY_NAME
                       ,esacctso.ENTITY_NAME
                       ,ui.first_name + SPACE(1) + ui.last_name
                       ,eac.modified_date
                       ,CASE WHEN utility.Account_Group_ID IS NOT NULL THEN 'PartofGroupBill'
                             ELSE 'No'
                        END
                       ,ereca.ENTITY_NAME
                       ,eclas.ENTITY_NAME
                       ,utility.Account_Id
                       ,samm.Account_Id
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,Utility.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,CASE Utility.Account_Is_Broker
                          WHEN 1 THEN 'Yes'
                          ELSE 'No'
                        END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME
                       ,euom.ENTITY_NAME
                       ,samm.Supplier_Account_begin_Dt
                       ,samm.Supplier_Account_End_Dt
                       ,ui1.USERNAME
                       ,ea1.MODIFIED_DATE
                   UNION         
---2        
--SitesWithAccountNoContracts          
                   SELECT
                        ch.client_name AS ClientName
                       ,ch.client_id AS ClientID
                       ,ClientNotManaged = CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                                                ELSE 'No'
                                           END
                       ,ch.Sitegroup_Name AS DivisionName
                       ,ch.Sitegroup_Id AS DivisionID
                       ,DivisionNotManaged = CASE WHEN ch.Division_Not_Managed = 1 THEN 'Yes'
                                                  ELSE 'No'
                                             END
                       ,ch.Site_name AS SiteName
                       ,ch.Site_id AS SiteID
                       ,SiteCreated = eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER AS SiteRef
                       ,rg.group_name AS SiteRMGroup
                       ,SiteNotManaged = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                                              ELSE 'No'
                                         END
                       ,SiteClosed = CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                                          ELSE 'No'
                                     END
                       ,ch.Site_Type_Name AS SiteType
                       ,ch.Site_Address_Line1 AS AddressLine1
                       ,ch.Site_Address_Line2 AS AddressLine2
                       ,ch.city AS City
                       ,ch.state_name AS State_Province
                       ,ch.zipcode AS ZipCode
                       ,ch.country_name AS Country
                       ,ch.region_name AS Region
                       ,utility.account_number AS UtilityAccountNumber
                       ,esvl.entity_name AS UtilityAccountServiceLevel
                       ,eaacct.modified_date AS AccountCreated
                       ,utility.Account_Not_Expected AS UtilityAccountInvoiceNotExpected
                       ,UtilityAccountNotManaged = CASE WHEN utility.Account_Not_Managed = 1 THEN 'Yes'
                                                        ELSE 'No'
                                                   END
                       ,euacctso.ENTITY_NAME AS UtilityAccountInvoiceSource
                       ,utility.Account_Vendor_Name AS Utility
                       ,ecomm.Entity_name AS CommodityType
                       ,utility.Rate_Name AS Rate
                       ,utility.Meter_Number AS MeterNumber
                       ,NULL AS PurchaseMethod
                       ,NULL AS ContractID
                       ,NULL AS BaseContractID
                       ,NULL AS ContractNumber
                       ,NULL AS ContractPricingStatus
                       ,NULL AS ContractType
                       ,NULL AS ContractedVendor
                       ,NULL AS ContractStartDate
                       ,NULL AS ContractEndDate
                       ,NULL AS NotificationDate
                       ,NULL AS ContractTriggerRights
                       ,NULL AS FullRequirements
                       ,NULL AS Contract_Pricing_Summary
                       ,NULL AS ContractComments
                       ,NULL AS Currency
                       ,NULL AS RenewalType
                       ,NULL AS SupplierAccountNumber
                       ,NULL AS SupplierAccountServiceLevel
                       ,NULL AS SupplierAccountInvoiceSource
                       ,NULL AS ContractCreatedBy
                       ,NULL AS ContractCreatedDate
                       ,NULL AS UtilityGroupBillStatus
                       ,NULL AS ContractRecalcType
                       ,NULL AS ContractClassification
                       ,utility.Account_Id [Utility Account ID]
                       ,NULL [Supplier Account ID]
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,COALESCE(Utility.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
                       ,Is_Broker_Account = CASE Utility.Account_Is_Broker
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME BrokerFee_CurrencyUnit
                       ,euom.ENTITY_NAME BrokerFee_UomName
                       ,NULL AS SupplierAccountStartDate
                       ,NULL AS SupplierAccountEndDate
                       ,NULL AS SupplierAccountCreatedBy
                       ,NULL AS SupplierAccountCreatedDate
                   FROM
                        core.client_hier ch
                        JOIN dbo.SITE s
                              ON s.SITE_ID = ch.Site_Id
                        JOIN core.Client_Hier_Account utility
                              ON ch.Client_Hier_Id = utility.Client_Hier_Id
                                 AND Utility.Account_Type = 'Utility'
                        LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON Utility.Account_Id = acbf.Account_Id
                                 AND Utility.Commodity_Id = acbf.Commodity_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                              ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
                        LEFT OUTER JOIN dbo.ENTITY euom
                              ON euom.ENTITY_ID = acbf.UOM_Entity_Id
                        LEFT OUTER JOIN core.Client_Hier_Account supp
                              ON supp.Client_Hier_Id = utility.Client_Hier_Id
                                 AND supp.Account_Type = 'Supplier'
                                 AND supp.Meter_Id = utility.Meter_Id
                        LEFT   JOIN dbo.rm_group_site_map rgsm
                              ON rgsm.site_id = ch.site_id
                        LEFT   JOIN dbo.rm_group rg
                              ON rg.rm_group_id = rgsm.rm_group_id
                        JOIN dbo.entity_audit eas
                              ON eas.entity_identifier = ch.site_id
                                 AND eas.entity_id = 506
                                 AND eas.audit_type = 1
                        JOIN entity ecomm
                              ON ecomm.entity_id = utility.Commodity_Id
                        LEFT OUTER JOIN dbo.entity esvl
                              ON esvl.entity_id = utility.Account_Service_level_Cd
                        JOIN dbo.entity_audit eaacct
                              ON eaacct.entity_identifier = utility.account_id
                                 AND eaacct.entity_id = 491
                                 AND eaacct.audit_type = 1
                        LEFT   JOIN entity euacctso
                              ON euacctso.entity_id = utility.Account_Invoice_Source_Cd
                   WHERE
                        ( supp.meter_id IS NULL
                          OR ( supp.Supplier_Meter_Disassociation_Date IS NOT NULL
                               AND utility.meter_id NOT IN ( SELECT
                                                                  meter_id
                                                             FROM
                                                                  core.Client_Hier_Account
                                                             WHERE
                                                                  Supplier_Meter_Disassociation_Date IS NULL
                                                                  AND Account_Type = 'Supplier' ) ) )
                        AND ch.client_type_id = @Client_Type_Id
                        OR ch.client_name = 'Cytec Industries Inc.'
                   GROUP BY
                        ch.client_name
                       ,ch.client_id
                       ,ch.Client_Not_Managed
                       ,ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,ch.Division_Not_Managed
                       ,ch.Site_name
                       ,ch.Site_id
                       ,eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER
                       ,rg.group_name
                       ,ch.Site_Not_Managed
                       ,ch.Site_Closed
                       ,ch.Site_Type_Name
                       ,ch.Site_Address_Line1
                       ,ch.Site_Address_Line2
                       ,ch.city
                       ,ch.state_name
                       ,ch.zipcode
                       ,ch.country_name
                       ,ch.region_name
                       ,utility.account_number
                       ,esvl.entity_name
                       ,eaacct.modified_date
                       ,utility.Account_Not_Expected
                       ,utility.Account_Not_Managed
                       ,euacctso.ENTITY_NAME
                       ,utility.Account_Vendor_Name
                       ,ecomm.Entity_name
                       ,utility.Rate_Name
                       ,utility.Meter_Number
                       ,utility.Account_Id
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,Utility.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,CASE Utility.Account_Is_Broker
                          WHEN 1 THEN 'Yes'
                          ELSE 'No'
                        END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME
                       ,euom.ENTITY_NAME
                   UNION         
----3        
--SitesWithNoAccounts        
                   SELECT
                        ch.Client_Name AS ClientName
                       ,ch.Client_Id AS ClientID
                       ,ClientNotManaged = CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                                                ELSE 'No'
                                           END
                       ,ch.Sitegroup_Name AS DivisionName
                       ,ch.Sitegroup_Id AS DivisionID
                       ,DivisionNotManaged = CASE WHEN ch.Division_Not_Managed = 1 THEN 'Yes'
                                                  ELSE 'No'
                                             END
                       ,ch.Site_name AS SiteName
                       ,ch.Site_Id AS SiteID
                       ,ea.modified_date AS SiteCreated
                       ,s.site_reference_number AS SiteRef
                       ,rg.group_name AS SiteRMGroup
                       ,SiteNotManaged = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                                              ELSE 'No'
                                         END
                       ,SiteClosed = CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                                          ELSE 'No'
                                     END
                       ,ch.Site_Type_Name AS SiteType
                       ,addr.ADDRESS_LINE1 AS AddressLine1
                       ,addr.ADDRESS_LINE2 AS AddressLine2
                       ,ch.City
                       ,ch.State_name AS State_Province
                       ,ch.ZipCode AS ZipCode
                       ,ch.Country_Name AS Country
                       ,ch.Region_Name AS Region
                       ,NULL AS UtilityAccountNumber
                       ,NULL AS UtilityAccountServiceLevel
                       ,NULL AS AccountCreated
                       ,NULL AS UtilityAccountInvoiceNotExpected
                       ,NULL AS UtilityAccountNotManaged
                       ,NULL AS UtilityAccountInvoiceSource
                       ,NULL AS Utility
                       ,NULL AS CommodityType
                       ,NULL AS Rate
                       ,NULL AS MeterNumber
                       ,NULL AS PurchaseMethod
                       ,NULL AS ContractID
                       ,NULL AS BaseContractID
                       ,NULL AS ContractNumber
                       ,NULL AS ContractPricingStatus
                       ,NULL AS ContractType
                       ,NULL AS ContractedVendor
                       ,NULL AS ContractStartDate
                       ,NULL AS ContractEndDate
                       ,NULL AS NotificationDate
                       ,NULL AS ContractTriggerRights
                       ,NULL AS FullRequirements
                       ,NULL AS Contract_Pricing_Summary
                       ,NULL AS ContractComments
                       ,NULL AS Currency
                       ,NULL AS RenewalType
                       ,NULL AS SupplierAccountNumber
                       ,NULL AS SupplierAccountServiceLevel
                       ,NULL AS SupplierAccountInvoiceSource
                       ,NULL AS ContractCreatedBy
                       ,NULL AS ContractCreatedDate
                       ,NULL AS UtilityGroupBillStatus
                       ,NULL AS ContractRecalcType
                       ,NULL AS ContractClassification
                       ,NULL [Utility Account ID]
                       ,NULL [Supplier Account ID]
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL AS SupplierAccountStartDate
                       ,NULL AS SupplierAccountEndDate
                       ,NULL AS SupplierAccountCreatedBy
                       ,NULL AS SupplierAccountCreatedDate
                   FROM
                        Core.Client_Hier ch
                        INNER JOIN site s
                              ON ch.site_Id = s.SITE_ID
                        INNER JOIN ADDRESS addr
                              ON addr.ADDRESS_PARENT_ID = s.SITE_ID
                        LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgsm
                              ON ch.Site_Id = rgsm.SITE_ID
                        LEFT OUTER JOIN dbo.RM_GROUP rg
                              ON rgsm.RM_GROUP_ID = rg.RM_GROUP_ID
                        JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = ch.Site_Id
                                 AND ea.entity_id = 506
                                 AND ea.audit_type = 1
                        LEFT OUTER JOIN core.Client_Hier_Account maddr
                              ON maddr.Meter_Address_ID = addr.ADDRESS_ID
                   WHERE
                        maddr.Meter_ID IS NULL
                        AND ch.Client_Type_Id = @Client_Type_Id
                        OR ch.Client_Name = 'Cytec Industries Inc.'
                   GROUP BY
                        ch.Client_Name
                       ,ch.Client_Id
                       ,ch.Client_Not_Managed
                       ,ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,ch.Division_Not_Managed
                       ,ch.Site_name
                       ,ch.Site_Id
                       ,ea.MODIFIED_DATE
                       ,s.site_reference_number
                       ,rg.group_name
                       ,ch.Site_Not_Managed
                       ,ch.Site_Closed
                       ,addr.ADDRESS_LINE1
                       ,addr.ADDRESS_LINE2
                       ,ch.City
                       ,ch.State_name
                       ,ch.ZipCode
                       ,ch.Country_Name
                       ,ch.Site_Type_Name
                       ,ch.Region_Name
                   UNION 
---For DMO Supplier accounts
                   SELECT
                        ch.client_name AS ClientName
                       ,ch.client_id AS ClientID
                       ,ClientNotManaged = CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                                                ELSE 'No'
                                           END
                       ,ch.Sitegroup_Name AS DivisionName
                       ,ch.Sitegroup_Id AS DivisionID
                       ,DivisionNotManaged = CASE WHEN ch.Division_Not_Managed = 1 THEN 'Yes'
                                                  ELSE 'No'
                                             END
                       ,ch.Site_name AS SiteName
                       ,ch.Site_id AS SiteID
                       ,SiteCreated = eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER AS SiteRef
                       ,rg.group_name AS SiteRMGroup
                       ,SiteNotManaged = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                                              ELSE 'No'
                                         END
                       ,SiteClosed = CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                                          ELSE 'No'
                                     END
                       ,ch.Site_Type_Name AS SiteType
                       ,ch.Site_Address_Line1 AS AddressLine1
                       ,ch.Site_Address_Line2 AS AddressLine2
                       ,ch.city AS City
                       ,ch.state_name AS State_Province
                       ,ch.zipcode AS ZipCode
                       ,ch.country_name AS Country
                       ,ch.region_name AS Region
                       ,utility.account_number AS UtilityAccountNumber
                       ,esvl.entity_name AS UtilityAccountServiceLevel
                       ,eaacct.modified_date AS AccountCreated
                       ,utility.Account_Not_Expected AS UtilityAccountInvoiceNotExpected
                       ,UtilityAccountNotManaged = CASE WHEN utility.Account_Not_Managed = 1 THEN 'Yes'
                                                        ELSE 'No'
                                                   END
                       ,euacctso.entity_name AS UtilityAccountInvoiceSource
                       ,utility.Account_Vendor_Name AS Utility
                       ,ecomm.entity_name AS CommodityType
                       ,utility.Rate_Name AS Rate
                       ,utility.Meter_Number AS MeterNumber
                       ,PurchaseMethod = NULL
                       ,samm.Supplier_Contract_ID AS ContractID
                       ,NULL AS BaseContractID
                       ,NULL AS ContractNumber
                       ,NULL AS ContractPricingStatus
                       ,NULL AS ContractType
                       ,utility.Account_Vendor_Name AS ContractedVendor
                       ,NULL AS ContractStartDate
                       ,NULL AS ContractEndDate
                       ,NULL NotificationDate
                       ,NULL ContractTriggerRights
                       ,NULL FullRequirements
                       ,NULL Contract_Pricing_Summary
                       ,NULL ContractComments
                       ,NULL Currency
                       ,NULL RenewalType
                       ,samm.Account_Number AS SupplierAccountNumber
                       ,esuasv.ENTITY_NAME AS SupplierAccountServiceLevel
                       ,esacctso.ENTITY_NAME AS SupplierAccountInvoiceSource
                       ,NULL ContractCreatedBy
                       ,NULL ContractCreatedDate
                       ,UtilityGroupBillStatus = CASE WHEN utility.Account_Group_ID IS NOT NULL THEN 'PartofGroupBill'
                                                      ELSE 'No'
                                                 END
                       ,NULL AS ContractRecalcType
                       ,NULL ContractClassification
                       ,utility.Account_Id [Utility Account ID]
                       ,samm.Account_Id [Supplier Account ID]
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,COALESCE(Utility.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
                       ,Is_Broker_Account = CASE Utility.Account_Is_Broker
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME BrokerFee_CurrencyUnit
                       ,euom.ENTITY_NAME BrokerFee_UomName
                       ,samm.Supplier_Account_begin_Dt AS SupplierAccountStartDate
                       ,samm.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui1.USERNAME AS SupplierAccountCreatedBy
                       ,ea1.MODIFIED_DATE AS SupplierAccountCreatedDate
                   FROM
                        CORE.Client_Hier_Account samm
                        JOIN CORE.Client_Hier_Account Utility
                              ON samm.meter_Id = utility.Meter_Id
                                 AND Utility.Account_Type = 'Utility'
                        LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON Utility.Account_Id = acbf.Account_Id
                                 AND Utility.Commodity_Id = acbf.Commodity_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                              ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
                        LEFT OUTER JOIN dbo.ENTITY euom
                              ON euom.ENTITY_ID = acbf.UOM_Entity_Id
                        JOIN Core.Client_Hier CH
                              ON ch.Client_Hier_Id = Utility.Client_Hier_Id
                        JOIN dbo.ENTITY esuasv
                              ON esuasv.entity_id = samm.Account_Service_level_Cd
                        LEFT OUTER JOIN dbo.ENTITY esacctso
                              ON esacctso.entity_id = samm.Account_Invoice_Source_Cd
                        JOIN dbo.SITE s
                              ON s.SITE_ID = CH.Site_Id
                        JOIN dbo.ENTITY ecomm
                              ON ecomm.entity_id = samm.Commodity_Id
                        LEFT OUTER JOIN dbo.ENTITY esvl
                              ON esvl.entity_id = Utility.Account_Service_level_Cd
                        JOIN dbo.ENTITY_AUDIT eaacct
                              ON eaacct.entity_identifier = Utility.account_id
                                 AND eaacct.entity_id = 491
                                 AND eaacct.audit_type = 1
                        LEFT OUTER JOIN dbo.ENTITY euacctso
                              ON euacctso.entity_id = Utility.Account_Invoice_Source_Cd
                        LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgsm
                              ON rgsm.site_id = ch.site_id
                        LEFT OUTER JOIN dbo.RM_GROUP rg
                              ON rg.rm_group_id = rgsm.rm_group_id
                        JOIN dbo.ENTITY_AUDIT eas
                              ON eas.entity_identifier = ch.site_id
                                 AND eas.entity_id = 506
                                 AND eas.audit_type = 1
                        LEFT JOIN dbo.ENTITY_AUDIT ea1
                              ON ea1.ENTITY_IDENTIFIER = samm.ACCOUNT_ID
                                 AND ea1.ENTITY_ID = @Account_Table --494                      
                                 AND ea1.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui1
                              ON ui1.USER_INFO_ID = ea1.USER_INFO_ID
                   WHERE
                        samm.Account_Type = 'Supplier'
                        AND samm.Supplier_Contract_ID = -1
                        AND ( ch.client_type_id = @Client_Type_Id
                              OR ch.client_name = 'Cytec Industries Inc.' )
                   GROUP BY
                        ch.client_name
                       ,ch.client_id
                       ,CASE WHEN ch.Client_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,ch.Division_Not_Managed
                       ,ch.Site_name
                       ,ch.Site_id
                       ,eas.modified_date
                       ,s.SITE_REFERENCE_NUMBER
                       ,rg.group_name
                       ,CASE WHEN ch.Site_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,CASE WHEN ch.Site_Closed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,ch.Site_Type_Name
                       ,ch.Site_Address_Line1
                       ,ch.Site_Address_Line2
                       ,ch.city
                       ,ch.state_name
                       ,ch.zipcode
                       ,ch.country_name
                       ,ch.region_name
                       ,utility.account_number
                       ,esvl.entity_name
                       ,eaacct.modified_date
                       ,utility.Account_Not_Expected
                       ,CASE WHEN utility.Account_Not_Managed = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,euacctso.entity_name
                       ,utility.Account_Vendor_Name
                       ,ecomm.entity_name
                       ,utility.Rate_Name
                       ,utility.Meter_Number
                       ,samm.Supplier_Contract_ID
                       ,utility.Account_Vendor_Name
                       ,samm.Account_Number
                       ,esuasv.ENTITY_NAME
                       ,esacctso.ENTITY_NAME
                       ,CASE WHEN utility.Account_Group_ID IS NOT NULL THEN 'PartofGroupBill'
                             ELSE 'No'
                        END
                       ,utility.Account_Id
                       ,samm.Account_Id
                       ,utility.Commodity_Id
                       ,utility.Account_Vendor_Id
                       ,Utility.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,CASE Utility.Account_Is_Broker
                          WHEN 1 THEN 'Yes'
                          ELSE 'No'
                        END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME
                       ,euom.ENTITY_NAME
                       ,samm.Supplier_Account_begin_Dt
                       ,samm.Supplier_Account_End_Dt
                       ,ui1.USERNAME
                       ,ea1.MODIFIED_DATE )
            SELECT DISTINCT
                  acc.ClientName
                 ,acc.ClientID
                 ,acc.ClientNotManaged
                 ,acc.DivisionName
                 ,acc.DivisionID
                 ,acc.DivisionNotManaged
                 ,acc.SiteName
                 ,acc.SiteID
                 ,acc.SiteCreated
                 ,acc.SiteRef
                 ,acc.SiteRMGroup
                 ,acc.SiteNotManaged
                 ,acc.SiteClosed
                 ,acc.SiteType
                 ,acc.AddressLine1
                 ,acc.AddressLine2
                 ,acc.City
                 ,acc.State_Province
                 ,acc.ZipCode
                 ,acc.Country
                 ,acc.Region
                 ,acc.UtilityAccountNumber
                 ,acc.UtilityAccountServiceLevel
                 ,acc.AccountCreated
                 ,acc.UtilityAccountInvoiceNotExpected
                 ,acc.UtilityAccountNotManaged
                 ,acc.UtilityAccountInvoiceSource
                 ,acc.Utility
                 ,acc.CommodityType
                 ,acc.Rate
                 ,acc.MeterNumber
                 ,acc.PurchaseMethod
                 ,acc.ContractID
                 ,acc.BaseContractID
                 --,acc.ContractNumber
                 ,CASE WHEN acc.ContractID = -1 THEN 'DMO'
                       ELSE acc.ContractNumber
                  END AS ContractNumber
                 ,acc.ContractPricingStatus
                 ,acc.ContractType
                 ,acc.ContractedVendor
                 ,acc.ContractStartDate
                 ,acc.ContractEndDate
                 ,acc.NotificationDate
                 ,acc.ContractTriggerRights
                 ,acc.FullRequirements
                 ,acc.Contract_Pricing_Summary
                 ,acc.ContractComments
                 ,acc.Currency
                 ,acc.RenewalType
                 ,acc.SupplierAccountNumber
                 ,acc.SupplierAccountServiceLevel
                 ,acc.SupplierAccountInvoiceSource
                 ,acc.ContractCreatedBy
                 ,acc.ContractCreatedDate
                 ,acc.UtilityGroupBillStatus
                 ,acc.ContractRecalcType
                 ,acc.ContractClassification
                 ,acc.[Utility Account ID]
                 ,acc.[Supplier Account ID]
                 ,acc.Commodity_Id
                 ,acc.Account_Vendor_Id
                 ,acc.Is_Broker_Account [Broker Account]
                 ,CAST(acc.Broker_fee AS VARCHAR) + ' ' + acc.BrokerFee_CurrencyUnit + '/' + acc.BrokerFee_UomName AS [Broker Fee]
                 ,CASE WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN ui.first_name + SPACE(1) + ui.last_name
                       WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.first_name + SPACE(1) + uia.last_name, uis.first_name + SPACE(1) + uis.last_name, uic.first_name + SPACE(1) + uic.last_name)
                  END AS [Sourcing Analyst]
                 ,cacc.Code_Value [Client Sourcing Mapping]
                 ,sacc.Code_Value [Site Sourcing Mapping]
                 ,aacc.Code_Value [Account Sourcing Mapping]
                 ,acc.SupplierAccountStartDate
                 ,acc.SupplierAccountEndDate
                 ,acc.SupplierAccountCreatedBy
                 ,acc.SupplierAccountCreatedDate
            FROM
                  cte_Account_List acc
                  LEFT JOIN Code cacc
                        ON cacc.Code_Id = acc.Client_Analyst_Mapping_Cd
                  LEFT JOIN Code sacc
                        ON sacc.Code_Id = acc.Site_Analyst_Mapping_Cd
                  LEFT JOIN Code aacc
                        ON aacc.Code_Id = acc.Analyst_Mapping_Cd
                  LEFT OUTER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                        ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                           AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                  LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                        ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                  LEFT OUTER JOIN dbo.USER_INFO ui
                        ON vcam.Analyst_Id = ui.USER_INFO_ID
                  LEFT OUTER JOIN ( core.Client_Commodity ccc
                                    INNER JOIN dbo.Code cd
                                          ON cd.Code_Id = ccc.Commodity_Service_Cd
                                             AND cd.Code_Value = 'Invoice' )
                                    ON ccc.Client_Id = acc.ClientId
                                       AND ccc.Commodity_Id = acc.COMMODITY_ID
                  LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca
                        ON aca.Account_Id = acc.[Utility Account ID]
                           AND aca.Commodity_Id = acc.COMMODITY_ID
                  LEFT OUTER JOIN dbo.USER_INFO uia
                        ON aca.Analyst_User_Info_Id = uia.USER_INFO_ID
                  LEFT OUTER JOIN dbo.SITE_Commodity_Analyst sca
                        ON sca.Site_Id = acc.SiteId
                           AND sca.Commodity_Id = acc.COMMODITY_ID
                  LEFT OUTER JOIN dbo.USER_INFO uis
                        ON sca.Analyst_User_Info_Id = uis.USER_INFO_ID
                  LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca
                        ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                  LEFT OUTER JOIN dbo.USER_INFO uic
                        ON cca.Analyst_User_Info_Id = uic.USER_INFO_ID
            ORDER BY
                  acc.ClientName    
          
END;

;
GO





GRANT EXECUTE ON  [dbo].[Report_DE_GFEJF] TO [CBMS_SSRS_Reports]
GO
