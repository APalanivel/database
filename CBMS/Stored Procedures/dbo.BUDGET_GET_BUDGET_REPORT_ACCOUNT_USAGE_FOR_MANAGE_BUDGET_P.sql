SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













-- exec BUDGET_GET_BUDGET_REPORT_ACCOUNT_USAGE_FOR_MANAGE_BUDGET_P '','',1259, '01/01/2007', '12/01/2007',10003,null,null,291
CREATE            PROCEDURE dbo.BUDGET_GET_BUDGET_REPORT_ACCOUNT_USAGE_FOR_MANAGE_BUDGET_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int,
	@from_month datetime, 
	@to_month datetime,
	@clientId int,
	@divisionId int,
	@siteId int ,
	@commodityId int , 
	@unitId int
	AS
	begin
	set nocount on

		if ( @commodityId = 291 ) --//Natural Gas
		begin
			select	budget_usage.month_identifier,
				(budget_usage.volume * conv.conversion_factor)as budget_usage,
				budget_account.budget_account_id	
		 
			from 	budget_account
				join budget_usage on budget_usage.account_id = budget_account.account_id
				and budget_account.budget_id = @budget_id
				and budget_usage.commodity_type_id = 291
				and budget_usage.month_identifier between @from_month and @to_month				
				join consumption_unit_conversion conv on conv.base_unit_id = budget_usage.volume_unit_type_id
				and conv.converted_unit_id = @unitId
				join entity budget_usage_type on budget_usage_type.entity_id = budget_usage.budget_usage_type_id				
				join account acc on budget_account.account_id = acc.account_id
				JOIN SITE S WITH(NOLOCK) ON (acc.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
				JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
				JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))
	
		end else --//Electirc Power
		begin
			select	budget_usage.month_identifier,
				(budget_usage.volume * conv.conversion_factor)as budget_usage,
				--budget_usage_type.entity_name 
				budget_account.budget_account_id
		 
			from 	budget_account
				join budget_usage on budget_usage.account_id = budget_account.account_id
				and budget_account.budget_id = @budget_id
				and budget_usage.commodity_type_id = 290
				and budget_usage.month_identifier between @from_month and @to_month
				join consumption_unit_conversion conv on conv.base_unit_id = budget_usage.volume_unit_type_id
				and conv.converted_unit_id = 12 --// kWh
				join entity budget_usage_type on budget_usage_type.entity_id = budget_usage.budget_usage_type_id				
				join account acc on budget_account.account_id = acc.account_id
				JOIN SITE S WITH(NOLOCK) ON (acc.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
				JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
				JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))
		
		end

		
	end
























GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_REPORT_ACCOUNT_USAGE_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
