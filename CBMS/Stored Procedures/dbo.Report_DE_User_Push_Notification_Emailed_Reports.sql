SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
  
NAME:      
dbo.Report_DE_User_Push_Notification_Emailed_Reports
  
DESCRIPTION:      
Report to show which users are set up to receive emailed reports

INPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
 
  
            
OUTPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------    
  
EXEC dbo.Report_DE_User_Push_Notification_Emailed_Reports 
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
LEC Lynn Cox      
  
MODIFICATIONS       
Initials	Date		Modification      
------------------------------------------------------------      
LEC		 2016-06-09     Created
  
******/  

CREATE PROCEDURE [dbo].[Report_DE_User_Push_Notification_Emailed_Reports]
AS
BEGIN    
    
      SET NOCOUNT ON;     

      SELECT
            c.CLIENT_NAME [Client]
           ,ui.FIRST_NAME [First Name]
           ,ui.LAST_NAME [Last Name]
           ,ui.USERNAME [User Name]
           ,cd.Code_Value [Emailed Reports]
      FROM
            USER_INFO ui
            JOIN CLIENT c
                  ON c.CLIENT_ID = ui.CLIENT_ID
            JOIN dbo.User_Push_Notification upn
                  ON upn.User_Info_Id = ui.USER_INFO_ID
            JOIN Code cd
                  ON cd.Code_Id = upn.Notification_Type_Cd
            JOIN ENTITY e
                  ON e.ENTITY_ID = c.CLIENT_TYPE_ID
      WHERE
            e.ENTITY_NAME != 'demo'-- All Demo type client should be excluded from this report
            AND c.NOT_MANAGED = 0
            AND c.CLIENT_NAME != 'blackstone' -- Blackstone is an aggregate client and should be excluded to avoid duplicated site data
            AND ui.IS_HISTORY = 0; -- Remove is history users (CBMS DB)
  
END;

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_User_Push_Notification_Emailed_Reports] TO [CBMSApplication]
GO
