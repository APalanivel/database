SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
        
NAME:        
  dbo.cbmsCuException_SearchIncomingPrivateForQueue
     
DESCRIPTION:
     
INPUT PARAMETERS:
 Name				    DataType		Default			Description        
------------------------------------------------------------                
 @queue_id			    int				0                  
 @client_name	     	varchar(200)	null               
 @city			     	varchar(200)	null               
 @state_name		    varchar(50)		null               
 @vendor_name		    varchar(200)	null               
 @account_number    	varchar(200)	null    
 @Site_Name		    	varchar(200)	null
 @Cu_Exception_Type_Id  int             null
      
OUTPUT PARAMETERS:
 Name   DataType  Default Description
----------------------------------------------------------        
USAGE EXAMPLES:
------------------------------------------------------------        
USE CBMS_TK3 
      
  EXEC dbo.cbmsCuException_SearchIncomingPrivateForQueue 19997
  
  EXEC dbo.cbmsCuException_SearchIncomingPrivateForQueue
	   @queue_id=49
	  ,@client_name=null
	  ,@city=null
	  ,@vendor_name=null
	  ,@account_number=null
	  ,@Cu_Exception_Type_Id =null

  EXEC dbo.cbmsCuException_SearchIncomingPrivateForQueue
	   @queue_id=49
	  ,@client_name=null
	  ,@city=null
	  ,@vendor_name='oneo'
	  ,@account_number=null
      ,@Cu_Exception_Type_Id =null



  EXEC dbo.cbmsCuException_SearchIncomingPrivateForQueue
	   @queue_id=10
	  ,@client_name=null
	  ,@city=null
	  ,@vendor_name=null
	  ,@account_number=null
	  ,@Cu_Exception_Type_Id =null

EXEC cbmsCuException_SearchIncomingPrivateForQueue
      @queue_id = 69626	  
      
AUTHOR INITIALS:
INITIALS NAME  
------------------------------------------------------------  
SKA		 Shobhit Kumar Agrawal
PNR		 Pandarinath
SP		 Sandeep Pigilam.
  
MODIFICATION:
INITIALS DATE		MODIFICATION
------------------------------------------------------------  
SKA		 10/21/2010 Created
SKA		 01/13/2011 Added the search condition for Account Number Bug#20733.
PNR		 03/09/2011	Added Left join with cu_invoice_label to fetch vendor_name when the invoice is not resolved to an account and if the value saved there(MAINT-495)
SP		 2014-05-26 For Data Operations Enhancement 4.2.3 added @Site_Name,@Cu_Exception_Type_Id as input parameters.
						-- Added condition to exclude the failed recalc type exceptions
SSP		2017-05-17  small Enhancement,SE2017-124 added filters	@State_Id and removed @State_Name,@Country_Id INT = NULL,@Commodity_Id INT = NULL,@Supplier_Name VARCHAR(500) = NULL
HG		2018-04-09	Optimized the sproc for performance, Supplier vendor filter was causing the performance issue to solve that saving the required details in a temp table and used in the main query to filter the records.
******/
CREATE PROCEDURE [dbo].[cbmsCuException_SearchIncomingPrivateForQueue]
      (
       @queue_id INT = 0
      ,@client_name VARCHAR(200) = NULL
      ,@city VARCHAR(200) = NULL
      ,@vendor_name VARCHAR(200) = NULL
      ,@account_number VARCHAR(200) = NULL
      ,@Site_Name VARCHAR(200) = NULL
      ,@Cu_Exception_Type_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Supplier_Name VARCHAR(500) = NULL
      ,@State_Id INT = NULL )
AS
BEGIN      

      SET NOCOUNT ON;  
      SET @client_name = REPLACE(@client_name, '''', '''''')            
      SET @city = REPLACE(@city, '''', '''''')            
      SET @vendor_name = REPLACE(@vendor_name, '''', '''''')            
      SET @account_number = REPLACE(@account_number, '''', '''''')            
      SET @account_number = REPLACE(@account_number, SPACE(1), SPACE(0))               
      SET @account_number = REPLACE(@account_number, '-', SPACE(0))    
      SET @account_number = REPLACE(@account_number, '/', SPACE(0))    
      SET @Site_Name = REPLACE(@Site_Name, '''', '''''');

      SELECT
            cuex.CBMS_IMAGE_ID
           ,cuex.CBMS_DOC_ID
           ,cuex.CU_INVOICE_ID
           ,cuex.SERVICE_MONTH
           ,cuex.QUEUE_ID
           ,cuex.EXCEPTION_TYPE
           ,cuex.EXCEPTION_STATUS_TYPE
           ,cuex.UBM_Account_Number
           ,cuex.Client_Hier_ID
           ,cuex.Account_ID
           ,cuex.DATE_IN_QUEUE
           ,cuex.SORT_ORDER
           ,cuex.UBM_Client_Name
           ,cuex.UBM_Site_Name
           ,cuex.UBM_City
           ,cuex.UBM_State_Name
           ,ci.ACCOUNT_GROUP_ID
           ,ag.VENDOR_ID AS Account_Group_Vendor_Id
           ,ag.GROUP_BILLING_NUMBER
           ,CAST(NULL AS VARCHAR(200)) AS Group_Account_Vendor_Name
           ,CAST(NULL AS VARCHAR(200)) AS Group_Account_Vendor_Type
      INTO
            #Cu_Exception_Denorm
      FROM
            dbo.CU_EXCEPTION_DENORM cuex
            INNER JOIN dbo.CU_INVOICE ci
                  ON ci.CU_INVOICE_ID = cuex.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP ag
                  ON ag.ACCOUNT_GROUP_ID = ci.ACCOUNT_GROUP_ID
            LEFT  JOIN dbo.CU_EXCEPTION_TYPE cet
                  ON cuex.EXCEPTION_TYPE = cet.EXCEPTION_TYPE
      WHERE
            cuex.QUEUE_ID = @queue_id
            AND cuex.IS_MANUAL = 1
            AND cuex.EXCEPTION_TYPE <> 'Failed Recalc'
            AND ( @Cu_Exception_Type_Id IS NULL
                  OR cet.CU_EXCEPTION_TYPE_ID = @Cu_Exception_Type_Id )      

      CREATE CLUSTERED INDEX cix_#Cu_Exception_Denorm ON #Cu_Exception_Denorm (CU_INVOICE_ID)
      CREATE INDEX ix_#Cu_Exception_Denorm_CH ON #Cu_Exception_Denorm (Client_Hier_ID,Account_ID)
      CREATE INDEX ix_#Cu_Exception_Denorm_Vendor ON #Cu_Exception_Denorm (Account_Group_Vendor_Id)

      UPDATE
            ced
      SET
            ced.Group_Account_Vendor_Name = v.VENDOR_NAME
           ,ced.Group_Account_Vendor_Type = vt.ENTITY_NAME
      FROM
            #Cu_Exception_Denorm ced
            JOIN dbo.VENDOR v
                  ON v.VENDOR_ID = ced.Account_Group_Vendor_Id
            JOIN dbo.ENTITY vt
                  ON vt.ENTITY_ID = v.VENDOR_TYPE_ID

      SELECT
            ced.CU_INVOICE_ID
           ,ced.Client_Hier_ID
           ,ced.Account_ID
           ,scha.Account_Vendor_Name
      INTO
            #SCHA
      FROM
            #Cu_Exception_Denorm ced
            INNER JOIN Core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ced.Client_Hier_ID
                     AND cha.Account_Id = ced.Account_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH sm
                  ON sm.CU_INVOICE_ID = ced.CU_INVOICE_ID
                     AND sm.Account_ID = ced.Account_ID
            INNER JOIN Core.Client_Hier_Account scha
                  ON cha.Client_Hier_Id = scha.Client_Hier_Id
                     AND scha.Meter_Id = cha.Meter_Id
                     AND sm.SERVICE_MONTH BETWEEN scha.Supplier_Account_begin_Dt
                                          AND     scha.Supplier_Account_End_Dt
      WHERE
            cha.Account_Type = 'Utility'
            AND scha.Account_Type = 'Supplier'
      GROUP BY
            ced.CU_INVOICE_ID
           ,ced.Client_Hier_ID
           ,ced.Account_ID
           ,scha.Account_Vendor_Name

      SELECT
            cuex.CBMS_IMAGE_ID
           ,cuex.CBMS_DOC_ID
           ,cuex.CU_INVOICE_ID
           ,cuex.QUEUE_ID
           ,cuex.EXCEPTION_TYPE
           ,cuex.EXCEPTION_STATUS_TYPE
           ,ISNULL(ISNULL(cuex.GROUP_BILLING_NUMBER, cha.Account_Number), cuex.UBM_Account_Number) AS account_number
           ,cuex.SERVICE_MONTH
           ,CASE WHEN cuex.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                 ELSE cuex.UBM_Client_Name
            END Client_Name
           ,CASE WHEN cuex.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                 ELSE cuex.UBM_Site_Name
            END site_name
           ,MAX(COALESCE(CASE WHEN cuex.Group_Account_Vendor_Type = 'Utility' THEN cuex.Group_Account_Vendor_Name
                         END, CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                   WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                              END, Inv_Lbl.VENDOR_NAME)) AS Utility_Vendor_Name
           ,cuex.DATE_IN_QUEUE
           ,cuex.SORT_ORDER
           ,com.Commodity_Name
           ,cuex.Client_Hier_ID
           ,MAX(ISNULL(COALESCE(CASE WHEN cuex.Group_Account_Vendor_Type = 'Supplier' THEN cuex.Group_Account_Vendor_Name
                                END, CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                                     END), scha.Account_Vendor_Name)) AS Supplier_Vendor_Name
      FROM
            #Cu_Exception_Denorm cuex
            LEFT JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = cuex.Client_Hier_ID
            LEFT JOIN Core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = cuex.Client_Hier_ID
                     AND cha.Account_Id = cuex.Account_ID
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.CU_INVOICE_LABEL Inv_Lbl
                  ON cuex.CU_INVOICE_ID = Inv_Lbl.CU_INVOICE_ID
            LEFT JOIN #SCHA scha
                  ON scha.CU_INVOICE_ID = cuex.CU_INVOICE_ID
                     AND scha.Client_Hier_ID = cuex.Client_Hier_ID
                     AND scha.Account_ID = cuex.Account_ID
            OUTER APPLY ( SELECT
                              uac.Account_Vendor_Name + ', '
                          FROM
                              Core.Client_Hier_Account uac
                              INNER JOIN Core.Client_Hier_Account sac
                                    ON sac.Meter_Id = uac.Meter_Id
                          WHERE
                              uac.Account_Type = 'Utility'
                              AND sac.Account_Type = 'Supplier'
                              AND cha.Account_Type = 'Supplier'
                              AND sac.Account_Id = cha.Account_Id
                          GROUP BY
                              uac.Account_Vendor_Name
            FOR
                          XML PATH('') ) UV ( Vendor_Name )
      WHERE
            ( ( @client_name IS NULL )
              OR ( ISNULL(ch.Client_Name, cuex.UBM_Client_Name) LIKE '%' + @client_name + '%' ) )
            AND ( ( @city IS NULL )
                  OR ( ISNULL(ch.City, cuex.UBM_City) LIKE '%' + @city + '%' ) )
            AND ( ( @State_Id IS NULL )
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.STATE s
                              WHERE
                                    s.STATE_ID = @State_Id
                                    AND ( ISNULL(ch.State_Name, cuex.UBM_State_Name) = s.STATE_NAME ) ) )
            AND ( @vendor_name IS NULL
                  OR ( COALESCE(CASE WHEN cuex.Group_Account_Vendor_Type = 'Utility' THEN cuex.Group_Account_Vendor_Name
                                END, CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                          WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                                     END, Inv_Lbl.VENDOR_NAME) LIKE '%' + @vendor_name + '%' ) )
            AND ( ( @account_number IS NULL )
                  OR ( ( LEN(@account_number) >= 4 )
                       AND ( REPLACE(REPLACE(REPLACE(ISNULL(cuex.GROUP_BILLING_NUMBER, ISNULL(cha.Account_Number, cuex.UBM_Account_Number)), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) LIKE '%' + @account_number + '%' ) )
                  OR ( ( LEN(@account_number) <= 4 )
                       AND ( REPLACE(REPLACE(REPLACE(ISNULL(cuex.GROUP_BILLING_NUMBER, ISNULL(cha.Account_Number, cuex.UBM_Account_Number)), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) = @account_number ) ) )
            AND ( ( @Site_Name IS NULL )
                  OR ( ISNULL(ch.Site_name, cuex.UBM_Site_Name) LIKE '%' + @Site_Name + '%' ) )
            AND ( @Country_Id IS NULL
                  OR ch.Country_Id = @Country_Id )
            AND ( @Commodity_Id IS NULL
                  OR cha.Commodity_Id = @Commodity_Id )
            AND ( @Supplier_Name IS NULL
                  OR ( COALESCE(CASE WHEN cuex.Group_Account_Vendor_Type = 'Supplier' THEN cuex.Group_Account_Vendor_Name
                                END, CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                                     END, scha.Account_Vendor_Name) LIKE '%' + @Supplier_Name + '%' ) )
      GROUP BY
            cuex.CBMS_IMAGE_ID
           ,cuex.CBMS_DOC_ID
           ,cuex.CU_INVOICE_ID
           ,cuex.QUEUE_ID
           ,cuex.EXCEPTION_TYPE
           ,cuex.EXCEPTION_STATUS_TYPE
           ,ISNULL(ISNULL(cuex.GROUP_BILLING_NUMBER, cha.Account_Number), cuex.UBM_Account_Number)
           ,cuex.SERVICE_MONTH
           ,cuex.Client_Hier_ID
           ,CASE WHEN cuex.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                 ELSE cuex.UBM_Client_Name
            END
           ,CASE WHEN cuex.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                 ELSE cuex.UBM_Site_Name
            END
           ,cuex.DATE_IN_QUEUE
           ,cuex.SORT_ORDER
           ,com.Commodity_Name
           ,cuex.Client_Hier_ID
      ORDER BY
            cuex.DATE_IN_QUEUE


      DROP TABLE #Cu_Exception_Denorm
      DROP TABLE #SCHA

END;
GO



GRANT EXECUTE ON  [dbo].[cbmsCuException_SearchIncomingPrivateForQueue] TO [CBMSApplication]
GO
