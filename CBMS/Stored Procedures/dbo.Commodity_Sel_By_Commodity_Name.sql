SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	dbo.Commodity_Sel_By_Commodity_Name

DESCRIPTION:    
	To get the data from COMMODITY

INPUT PARAMETERS:    
Name			DataType		Default Description    
------------------------------------------------------------    
@Commodity_Name	VARCHAR(50)
          
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------  

	EXEC dbo.Commodity_Sel_By_Commodity_Name 'Natural Gas'

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date			Modification    
------------------------------------------------------------    
GB						Created  
SKA		14/07/2009		Modified as per coding standard
SKA		23/07/2009		Removed unused parameters

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Commodity_Sel_By_Commodity_Name]
	@Commodity_Name VARCHAR(50)
AS  

BEGIN  
SET NOCOUNT ON;  
  
  SELECT COmmodity_ID, COMMODITY_NAME FROM COMMODITY 
  WHERE COMMODITY_NAME= @Commodity_Name 
  
  
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_By_Commodity_Name] TO [CBMSApplication]
GO
