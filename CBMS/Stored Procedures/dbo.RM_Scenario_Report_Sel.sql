SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Report_Sel           
                        
 DESCRIPTION:                        
			To get the RM_Scenario_Report              
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Report_Id		   INT                  
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[RM_Scenario_Report_Sel] 
 @RM_Scenario_Report_Id=1
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-06       Created                
                       
******/   
       
                
CREATE PROCEDURE [dbo].[RM_Scenario_Report_Sel]
      ( 
       @RM_Scenario_Report_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      SELECT
            rsr.Client_Id
           ,rsr.Client_Hier_Id
           ,ch.Sitegroup_Id
           ,ch.Site_Id
           ,rsr.Start_Service_Month
           ,rsr.Is_Include_Trigger
           ,rsr.Currency_Unit_Id
           ,rsr.Uom_Type_Id
           ,rsr.Scenario_Name
           ,e.ENTITY_NAME AS UOM
           ,cu.CURRENCY_UNIT_NAME
      FROM
            dbo.RM_Scenario_Report rsr
            INNER JOIN core.Client_Hier ch
                  ON rsr.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CURRENCY_UNIT cu
                  ON rsr.Currency_Unit_Id = cu.CURRENCY_UNIT_ID
            INNER JOIN dbo.ENTITY e
                  ON rsr.Uom_Type_Id = e.ENTITY_ID
      WHERE
            rsr.RM_Scenario_Report_Id = @RM_Scenario_Report_Id                   
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Report_Sel] TO [CBMSApplication]
GO
