SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:    
              
Description:              
			
                           
 Input Parameters:              
    Name					DataType		Default			Description                
----------------------------------------------------------------------------------------                
                        
 
 Output Parameters:                    
    Name					DataType		Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

	Exec cbmsRmDealTicket_GetForContract 1,75496	
	Exec cbmsRmDealTicket_GetForContract 1,54924
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RR				Raghu Reddy
 
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RR				2019-05-28		GRM - Modified to refer new schema
******/ 
CREATE PROCEDURE [dbo].[cbmsRmDealTicket_GetForContract]
      ( 
       @MyAccountId INT
      ,@contract_id INT )
AS 
BEGIN

      SELECT
           DISTINCT Deal_Ticket_Id AS rm_deal_ticket_id
      FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl
      WHERE
            Contract_Id = @contract_id

END

GO
GRANT EXECUTE ON  [dbo].[cbmsRmDealTicket_GetForContract] TO [CBMSApplication]
GO
