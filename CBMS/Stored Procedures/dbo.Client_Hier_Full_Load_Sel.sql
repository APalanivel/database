SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/*********              
NAME:              
            
 dbo.Client_Hier_Full_Load_Sel        
              
DESCRIPTION:            
       
 This procedure is used to load data of client heir data from the base tables          
                                
INPUT PARAMETERS:                                
 Name                DataType            Default  Description                                
---------------------------------------------------------------------                                
           
OUTPUT PARAMETERS:            
            
 Name                DataType            Default  Description                                
---------------------------------------------------------------------         
                             
 USAGE EXAMPLES:            
---------------------------------------------------------------------         
      
 EXEC  dbo.Client_Hier_Full_Load_Sel          
           
AUTHOR INITIALS:                                
Initials        Name                                
---------------------------------------------------------------------         
 RKV            Ravi Kumar Vegesna                           
MODIFICATIONS:                            
            
Initials        Date       Modification                            
---------------------------------------------------------------------         
 RKV           2014-09-09  Created         
******/            
CREATE PROCEDURE [dbo].[Client_Hier_Full_Load_Sel]
AS 
BEGIN          
      SET NOCOUNT ON;   
      DECLARE
            @ClientHierCd INT
           ,@sgHierCd INT
           ,@divHierCd INT
           ,@siteHierCd INT          
          
      CREATE TABLE #Client_Hier_Full_Load
            ( 
             [Client_Hier_Full_Load_Id] [int] NOT NULL
                                              IDENTITY(1, 1)
            ,[Hier_level_Cd] [int] NOT NULL
            ,[Client_Id] [int] NOT NULL
            ,[Client_Name] [varchar](200) NOT NULL
            ,[Client_Currency_Group_Id] [int] NOT NULL
            ,[Sitegroup_Id] [int] NOT NULL
                                  DEFAULT ( (0) )
            ,[Sitegroup_Name] [varchar](200) NOT NULL
                                             DEFAULT ( '' )
            ,[Site_Id] [int] NOT NULL
                             DEFAULT ( (0) )
            ,[Site_name] [varchar](200) NOT NULL
                                        DEFAULT ( '' )
            ,[Site_Address_Line1] [varchar](200) NULL
            ,[Site_Address_Line2] [varchar](200) NULL
            ,[City] [varchar](200) NULL
            ,[ZipCode] [varchar](30) NULL
            ,[State_Id] [int] NULL
            ,[State_Name] [varchar](200) NULL
            ,[Country_Id] [int] NULL
            ,[Country_Name] [varchar](200) NULL
            ,[Geo_Lat] [decimal](32, 16) NULL
            ,[Geo_Long] [decimal](32, 16) NULL
            ,[Site_Not_Managed] [bit] NOT NULL
                                      DEFAULT ( (0) )
            ,[Site_Closed] [bit] NOT NULL
                                 DEFAULT ( (0) )
            ,[Site_Closed_Dt] [date] NOT NULL
                                     DEFAULT ( '12/31/2099' )
            ,[Region_ID] [int] NOT NULL
                               DEFAULT ( (0) )
            ,[Region_Name] [varchar](200) NULL
            ,[Client_Not_Managed] [bit] NOT NULL
                                        DEFAULT ( (0) )
            ,[Division_Not_Managed] [bit] NOT NULL
                                          DEFAULT ( (0) )
            ,[Sitegroup_Type_cd] [int] NULL
            ,[Sitegroup_Type_Name] [varchar](255) NULL
            ,[Last_Change_TS] [datetime] NOT NULL
                                         DEFAULT ( GETDATE() )
            ,[User_Passcode_Expiration_Duration] [smallint] NULL
            ,[User_Max_Failed_Login_Attempts] [smallint] NULL
            ,[User_PassCode_Reuse_Limit] [smallint] NULL
            ,[Client_Fiscal_Offset] [int] NULL
            ,[Client_Type_Id] [int] NULL
            ,[Report_Frequency_Type_Id] [int] NULL
            ,[Fiscalyear_Startmonth_Type_Id] [int] NULL
            ,[Ubm_Service_Id] [int] NULL
            ,[Dsm_Strategy] [bit] NULL
            ,[Is_Sep_Issued] [bit] NULL
            ,[Sep_Issue_Date] [datetime] NULL
            ,[Sitegroup_Owner_User_id] [int] NULL
            ,[Client_Hier_Created_Ts] [datetime] NULL
                                                 DEFAULT ( GETDATE() )
            ,[Portfolio_Client_Id] [int] NULL
            ,[Site_Type_Name] [varchar](200) NULL
            ,[Client_Analyst_Mapping_Cd] [int] NOT NULL
                                               DEFAULT ( (0) )
            ,[Site_Analyst_Mapping_Cd] [int] NULL
            ,[Weather_Station_Code] [varchar](10) NULL
            ,[Site_Search_Combo] AS ( ( ( ( ( ( ( ( ( ( ISNULL([SITE_NAME], '') + ' | ' ) + ISNULL([Site_ADDRESS_LINE1], '') ) + ' | ' ) + ISNULL([City], '') ) + ' | ' ) + ISNULL([State_Name], '') ) + ' | ' ) + ISNULL([ZIPCODE], '') ) + ' | ' ) + ISNULL([COUNTRY_NAME], '') ) PERSISTED
                                                                                                                                                                                                                                                                                    NOT NULL
            ,[Site_Reference_Number] [varchar](30) NULL
            ,[Doing_Business_As] [varchar](200) NULL
            ,[Portfolio_Client_Hier_Reference_Number] [int] NULL )          
      ALTER TABLE #Client_Hier_Full_Load ADD CONSTRAINT [pk_Client_Hier_Full_Load] PRIMARY KEY CLUSTERED  ([Client_Hier_Full_Load_Id]) WITH (FILLFACTOR=90) ON [Primary]          
      ALTER TABLE #Client_Hier_Full_Load ADD CONSTRAINT [unc_Client_Hier_Full_Load_Client_Id_Sitegroup_Id_Site_Id] UNIQUE NONCLUSTERED  ([Client_Id], [Site_Id], [Sitegroup_Id]) WITH (FILLFACTOR=90) ON [Primary]          
          
          
      -- CORPORATE LEVEL INSEERTION          
             
      SELECT
            @ClientHierCd = Code_Id
      FROM
            CBMS.dbo.Codeset cs
            JOIN CBMS.dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Corporate'                
                           
      INSERT      INTO #Client_Hier_Full_Load
                  ( 
                   Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_Managed
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Client_Analyst_Mapping_Cd )
                  SELECT
                        @ClientHierCd
                       ,cl.Client_Id
                       ,cl.Client_Name
                       ,cl.Currency_Group_Id
                       ,cl.Not_Managed
                       ,cl.User_Passcode_Expiration_Duration
                       ,cl.User_Max_Failed_Login_Attempts
                       ,cl.User_PassCode_Reuse_Limit
                       ,CASE fsm.ENTITY_NAME
                          WHEN 'January' THEN 0
                          WHEN 'February' THEN -11
                          WHEN 'March' THEN -10
                          WHEN 'April' THEN -9
                          WHEN 'May' THEN -8
                          WHEN 'June' THEN -7
                          WHEN 'July' THEN -6
                          WHEN 'August' THEN -5
                          WHEN 'September' THEN -4
                          WHEN 'October' THEN -3
                          WHEN 'November' THEN -2
                          WHEN 'December' THEN -1
                        END
                       ,cl.Client_Type_Id
                       ,cl.Report_Frequency_Type_Id
                       ,cl.Fiscalyear_Startmonth_Type_Id
                       ,cl.Ubm_Service_Id
                       ,cl.Dsm_Strategy
                       ,cl.Is_Sep_Issued
                       ,cl.Sep_Issue_Date
                       ,cl.Portfolio_Client_Id
                       ,cl.Analyst_Mapping_Cd
                  FROM
                        cbms.dbo.CLIENT cl
                        JOIN cbms.dbo.Entity fsm
                              ON fsm.Entity_id = cl.FiscalYear_StartMonth_Type_id          
                                        
                               
                               
                               
             --SITEGROUP LEVEL INSERTION                  
                               
      SELECT
            @sgHierCd = Code_Id
      FROM
            CBMS.dbo.Codeset cs
            INNER JOIN CBMS.dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Site group'              
            
      SELECT
            @divHierCd = Code_Id
      FROM
            CBMS.dbo.Codeset cs
            INNER JOIN CBMS.dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Division'            
                               
                            
      INSERT      #Client_Hier_Full_Load
                  ( 
                   Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_managed
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Division_Not_Managed
                  ,Sitegroup_Type_cd
                  ,Sitegroup_Type_Name
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Sitegroup_Owner_User_Id
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Client_Analyst_Mapping_Cd
                  ,Portfolio_Client_Hier_Reference_Number )
                  SELECT
                        CASE WHEN sgt.Code_Value = 'Division' THEN @divHierCd
                             ELSE @sgHierCd
                        END AS Hier_level_Cd
                       ,ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Not_managed
                       ,sg.Sitegroup_Id
                       ,sg.Sitegroup_Name
                       ,0 --div.Not_Managed                    
                       ,sg.Sitegroup_Type_Cd
                       ,sgt.Code_Value
                       ,ch.User_Passcode_Expiration_Duration
                       ,ch.User_Max_Failed_Login_Attempts
                       ,ch.User_PassCode_Reuse_Limit
                       ,ch.Client_Fiscal_Offset
                       ,sg.Owner_User_Id
                       ,ch.Client_Type_Id
                       ,ch.Report_Frequency_Type_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,ch.Ubm_Service_Id
                       ,ch.Dsm_Strategy
                       ,ch.Is_Sep_Issued
                       ,ch.Sep_Issue_Date
                       ,ch.Portfolio_Client_Id
                       ,ch.Client_Analyst_Mapping_Cd
                       ,sg.Portfolio_Client_Hier_Reference_Number
                  FROM
                        CBMS.dbo.Sitegroup sg
                        INNER JOIN #Client_Hier_Full_Load ch
                              ON sg.Client_Id = ch.Client_Id
                        INNER JOIN CBMS.dbo.Code sgt
                              ON sgt.Code_id = sg.Sitegroup_Type_Cd
                  WHERE
                        ch.Sitegroup_id = 0
                        AND sg.Is_Complete = 1            
            
                            
            --- SITE LEVEL INSERTION            
                      
      SELECT
            @siteHierCd = Code_Id
      FROM
            CBMS.dbo.Codeset cs
            INNER JOIN CBMS.dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Site'              
            
            
      INSERT      #Client_Hier_Full_Load
                  ( 
                   Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_managed
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Division_Not_Managed
                  ,Sitegroup_Type_cd
                  ,Sitegroup_Type_Name
                  ,Site_Id
                  ,Site_name
                  ,Site_Address_Line1
                  ,Site_Address_Line2
                  ,City
                  ,ZipCode
                  ,State_Id
                  ,State_Name
                  ,Country_Id
                  ,Country_Name
                  ,Geo_Lat
                  ,Geo_Long
                  ,Site_Not_Managed
                  ,Site_Closed
                  ,Site_Closed_Dt
                  ,Region_ID
                  ,Region_Name
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Site_Type_Name
                  ,Client_Analyst_Mapping_Cd
                  ,Site_Analyst_Mapping_Cd
                  ,Weather_Station_Code
                  ,Site_Reference_Number
                  ,Portfolio_Client_Hier_Reference_Number )
                  SELECT
                        @SiteHierCd
                       ,ch.CLIENT_ID
                       ,ch.CLIENT_NAME
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Not_managed
                       ,s.DIVISION_ID
                       ,sg.Sitegroup_Name
                       ,div.Not_Managed
                       ,sg.Sitegroup_Type_cd
                       ,SgType.Code_Value
                       ,s.SITE_ID
                       ,s.SITE_NAME
                       ,ad.ADDRESS_LINE1
                       ,ad.ADDRESS_LINE2
                       ,ad.CITY
                       ,ad.ZipCode
                       ,st.STATE_ID
                       ,st.STATE_NAME
                       ,st.COUNTRY_ID
                       ,cnt.Country_Name
                       ,ad.GEO_LAT
                       ,ad.GEO_LONG
                       ,s.NOT_MANAGED
                       ,s.CLOSED
                       ,ISNULL(s.CLOSED_DATE, '1900-01-01')
                       ,ISNULL(rg.REGION_ID, 0)
                       ,rg.REGION_NAME
                       ,ch.User_Passcode_Expiration_Duration
                       ,ch.User_Max_Failed_Login_Attempts
                       ,ch.User_PassCode_Reuse_Limit
                       ,ch.Client_Fiscal_Offset
                       ,ch.Client_Type_Id
                       ,ch.Report_Frequency_Type_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,ch.Ubm_Service_Id
                       ,ch.Dsm_Strategy
                       ,ch.Is_Sep_Issued
                       ,ch.Sep_Issue_Date
                       ,ch.Portfolio_Client_Id
                       ,stype.Entity_Name
                       ,ch.Client_Analyst_Mapping_Cd
                       ,s.Analyst_Mapping_Cd
                       ,s.Weather_Station_Code
                       ,s.Site_Reference_Number
                       ,s.Portfolio_Client_Hier_Reference_Number
                  FROM
                        CBMS.dbo.[Site] s
                        INNER JOIN #Client_Hier_Full_Load ch
                              ON s.Client_ID = ch.Client_ID
                                 AND s.DIVISION_ID = ch.Sitegroup_Id
                        INNER JOIN CBMS.dbo.SITEGROUP sg
                              ON sg.SITEGROUP_ID = s.DIVISION_ID
                        INNER JOIN CBMS.dbo.DIVISION_DTL div
                              ON div.SITEGROUP_ID = sg.SITEGROUP_ID
                        INNER JOIN CBMS.dbo.Code SgType
                              ON sgType.Code_Id = sg.Sitegroup_Type_Cd
                        LEFT JOIN CBMS.dbo.[ADDRESS] ad
                              ON ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
                        LEFT JOIN CBMS.dbo.[STATE] st
                              ON st.STATE_ID = ad.STATE_ID
                        LEFT JOIN CBMS.dbo.REGION rg
                              ON rg.REGION_ID = st.REGION_ID
                        LEFT JOIN CBMS.dbo.COUNTRY cnt
                              ON cnt.COUNTRY_ID = st.COUNTRY_ID
                        INNER JOIN CBMS.dbo.Entity stype
                              ON stype.Entity_Id = s.Site_Type_Id
                  WHERE
                        ch.SITE_ID = 0
                        AND ch.Sitegroup_Id > 0                  
                            
      SELECT
            Hier_level_Cd
           ,Client_Id
           ,Client_Name
           ,Client_Currency_Group_Id
           ,Sitegroup_Id
           ,Sitegroup_Name
           ,Site_Id
           ,Site_name
           ,Site_Address_Line1
           ,Site_Address_Line2
           ,City
           ,ZipCode
           ,State_Id
           ,State_Name
           ,Country_Id
           ,Country_Name
           ,Geo_Lat
           ,Geo_Long
           ,Site_Not_Managed
           ,Site_Closed
           ,Site_Closed_Dt
           ,Region_ID
           ,Region_Name
           ,Client_Not_Managed
           ,Division_Not_Managed
           ,Sitegroup_Type_cd
           ,Sitegroup_Type_Name
           ,Last_Change_TS
           ,User_Passcode_Expiration_Duration
           ,User_Max_Failed_Login_Attempts
           ,User_PassCode_Reuse_Limit
           ,Client_Fiscal_Offset
           ,Client_Type_Id
           ,Report_Frequency_Type_Id
           ,Fiscalyear_Startmonth_Type_Id
           ,Ubm_Service_Id
           ,Dsm_Strategy
           ,Is_Sep_Issued
           ,Sep_Issue_Date
           ,Sitegroup_Owner_User_id
           ,Client_Hier_Created_Ts
           ,Portfolio_Client_Id
           ,Site_Type_Name
           ,Client_Analyst_Mapping_Cd
           ,Site_Analyst_Mapping_Cd
           ,Weather_Station_Code
           ,Site_Reference_Number
           ,Doing_Business_As
           ,Portfolio_Client_Hier_Reference_Number
      FROM
            #Client_Hier_Full_Load         
                
                                        
      DROP TABLE #Client_Hier_Full_Load          
                                        
                                        
END 
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_Full_Load_Sel] TO [CBMSApplication]
GO
