SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                 
NAME:  dbo.Report_DE_Cost_Usage_Account_Category_Sel_By_Client_Commodity       
 	 		               
DESCRIPTION:            
 	 		              
INPUT PARAMETERS:                  
Name                DataType          Default     Description                  
---------------------------------------------------------------------------------------                  
@Client_Name            VARCHAR(200) 
@Commodity_Name         VARCHAR(max)              Comma separated Service name 
 	 	 	            
OUTPUT PARAMETERS:                  
Name              DataType          Default     Description                  
---------------------------------------------------------------------------------------                  
 	 	 	                  
USAGE EXAMPLES:
 	 	 	 
Exec dbo.Report_DE_Cost_Usage_Account_Category_Sel_By_Client_Commodity
 	 	@Client_Name = 'AT&T Services, Inc.'
 	 	,@Commodity_Name = 'SPPA - Lighting'
 	 	 	 
---------------------------------------------------------------------------------------
 	 	 	 
AUTHOR INITIALS:
Initials        Name
---------------------------------------------------------------------------------------
NR                      Narayana Reddy
 	 	 	              
Initials        Date                    Modification
---------------------------------------------------------------------------------------
NR                      2016-01-11  Created for DDCR-16
RKV						2020-04-29	changed the code to get the ubm account code from account to Account_Ubm_Account_Code_Map as part of   
      Multi Account UBM Changes
******/
CREATE PROCEDURE [dbo].[Report_DE_Cost_Usage_Account_Category_Sel_By_Client_Commodity]
     (
         @Client_Name VARCHAR(200)
         , @Commodity_Name VARCHAR(MAX)
     )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Client_Hier_Account
             (
                 Client_Hier_Id INT
                 , Client_Name VARCHAR(200)
                 , Sitegroup_Name VARCHAR(200)
                 , Site_Name VARCHAR(200)
                 , Site_Id INT
                 , Site_Address_Line1 VARCHAR(200)
                 , City VARCHAR(200)
                 , State_Name VARCHAR(200)
                 , ZipCode VARCHAR(30)
                 , Account_Vendor_Name VARCHAR(200)
                 , Account_Number VARCHAR(1000)
                 , Account_Type VARCHAR(200)
                 , Account_Ubm_Account_Code VARCHAR(200)
                 , Consumption_Level_Desc VARCHAR(200)
                 , Account_Id INT
                 , PRIMARY KEY CLUSTERED
                   (
                       Client_Hier_Id
                       , Account_Id
                   )
             );

        DECLARE @Commodity_List TABLE
              (
                  Commodity_Id INT
              );

        DECLARE @Category TABLE
              (
                  Bucket_Master_Id INT
                  , Bucket_Name VARCHAR(200)
                  , Bucket_Type VARCHAR(200)
                  , Default_Uom_Type_Id INT
              );

        DECLARE
            @Currency_Unit_Id INT
            , @Client_Id INT
            , @Client_Currency_Group_Id INT;

        SELECT
            @Currency_Unit_Id = cu.CURRENCY_UNIT_ID
        FROM
            dbo.CURRENCY_UNIT cu
        WHERE
            CURRENCY_UNIT_NAME = 'USD';

        SELECT
            @Client_Id = CLIENT_ID
            , @Client_Currency_Group_Id = CURRENCY_GROUP_ID
        FROM
            dbo.CLIENT
        WHERE
            CLIENT_NAME = @Client_Name;

        INSERT INTO @Commodity_List
             (
                 Commodity_Id
             )
        SELECT
            com.Commodity_Id
        FROM
            dbo.ufn_split(@Commodity_Name, ',') cnl
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Name = cnl.Segments
        GROUP BY
            com.Commodity_Id;

        INSERT INTO @Category
             (
                 Bucket_Master_Id
                 , Bucket_Name
                 , Bucket_Type
                 , Default_Uom_Type_Id
             )
        SELECT
            bm.Bucket_Master_Id
            , bm.Bucket_Name
            , bt.Code_Value
            , bm.Default_Uom_Type_Id
        FROM
            dbo.Bucket_Master bm
            INNER JOIN @Commodity_List cl
                ON cl.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.Code bt
                ON bm.Bucket_Type_Cd = bt.Code_Id;

        INSERT INTO #Client_Hier_Account
             (
                 Client_Hier_Id
                 , Client_Name
                 , Sitegroup_Name
                 , Site_Name
                 , Site_Id
                 , Site_Address_Line1
                 , City
                 , State_Name
                 , ZipCode
                 , Account_Vendor_Name
                 , Account_Number
                 , Account_Type
                 , Account_Ubm_Account_Code
                 , Consumption_Level_Desc
                 , Account_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Client_Name
            , ch.Sitegroup_Name
            , ch.Site_name
            , ch.Site_Id
            , ch.Site_Address_Line1
            , ch.City
            , ch.State_Name
            , ch.ZipCode
            , ca.Account_Vendor_Name
            , ca.Display_Account_Number
            , ca.Account_Type
            , auacm.Ubm_Account_Code Account_UBM_Account_Code
            , vcl.Consumption_Level_Desc
            , ca.Account_Id
        FROM
            Core.Client_Hier_Account ca
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = ca.Client_Hier_Id
                   AND  ch.Client_Hier_Id = ca.Client_Hier_Id
            LEFT OUTER JOIN dbo.Account_Ubm_Account_Code_Map auacm  
				ON auacm.Account_Id = ca.Account_Id
			INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = ca.Commodity_Id
            INNER JOIN @Commodity_List cl
                ON cl.Commodity_Id = com.Commodity_Id
            LEFT OUTER JOIN(dbo.Account_Variance_Consumption_Level avcl
                            INNER JOIN Variance_Consumption_Level vcl
                                ON vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id)
                ON avcl.ACCOUNT_ID = ca.Account_Id
                   AND  vcl.Commodity_Id = com.Commodity_Id
        WHERE
            ch.Client_Id = @Client_Id
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Name
            , ch.Sitegroup_Name
            , ch.Site_name
            , ch.Site_Id
            , ch.Site_Address_Line1
            , ch.City
            , ch.State_Name
            , ch.ZipCode
            , ca.Account_Vendor_Name
            , ca.Display_Account_Number
            , ca.Account_Type
            , auacm.Ubm_Account_Code
            , vcl.Consumption_Level_Desc
            , ca.Account_Id;

        SELECT
            ac.Client_Name AS [Client]
            , ac.Sitegroup_Name AS [Division]
            , ac.Site_Name AS [Site]
            , ac.Site_Id AS [Site ID]
            , ac.Client_Hier_Id AS [Client Hier ID]
            , ac.Site_Address_Line1 AS [Site Address]
            , ac.City
            , ac.State_Name AS [State]
            , ac.ZipCode AS [Zip]
            , ac.Account_Vendor_Name AS [Vendor]
            , ac.Account_Number AS [Account number]
            , ac.Account_Type [Account Type]
            , ac.Account_Ubm_Account_Code AS [UBM Account Code]
            , ac.Consumption_Level_Desc AS [Consumption Level]
            , bm.Bucket_Name [Category]
            , [Bucket Value] = CASE WHEN bm.Bucket_Type = 'Determinant' THEN cuad.Bucket_Value * cuc.CONVERSION_FACTOR
                                   WHEN bm.Bucket_Type = 'Charge' THEN cuad.Bucket_Value * cur.CONVERSION_FACTOR
                                   ELSE NULL
                               END
            , cuad.Service_Month [Month]
            , [UOM] = CASE WHEN bm.Bucket_Type = 'Determinant' THEN e.ENTITY_NAME
                          ELSE 'USD'
                      END
        FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN #Client_Hier_Account ac
                ON ac.Client_Hier_Id = cuad.Client_Hier_ID
                   AND  ac.Account_Id = cuad.ACCOUNT_ID
            INNER JOIN @Category bm
                ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cur
                ON cur.BASE_UNIT_ID = cuad.CURRENCY_UNIT_ID
                   AND  cur.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cur.CONVERSION_DATE = cuad.Service_Month
                   AND  cur.CONVERTED_UNIT_ID = @Currency_Unit_Id
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                   AND  cuc.CONVERTED_UNIT_ID = bm.Default_Uom_Type_Id
            LEFT OUTER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = bm.Default_Uom_Type_Id
        ORDER BY
            ac.Site_Name
            , ac.Account_Number
            , cuad.Service_Month
            , bm.Bucket_Name;

        DROP TABLE #Client_Hier_Account;

    END;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Account_Category_Sel_By_Client_Commodity] TO [CBMS_SSRS_Reports]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Account_Category_Sel_By_Client_Commodity] TO [CBMSReports]
GO
