SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	
	@dealTicketStatusType	int       	          	
	@dealTicketStatus	varchar(200)	          	
	@cbmsImageId   	int       	          	
	@comments      	varchar(4000)	          	
	@dealTransactionDate	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE dbo.CREATE_DEAL_TICKET_STATUS_P  

@userId varchar,
@sessionId varchar,
@dealTicketId int, 
@dealTicketStatusType int, 
@dealTicketStatus varchar(200), 
@cbmsImageId int, 
@comments varchar(4000), 
@dealTransactionDate datetime

AS
set nocount on
DECLARE @entityId int 
DECLARE @pkey int

select @entityId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@dealTicketStatusType AND 
ENTITY_NAME=@dealTicketStatus

select @pkey = RM_DEAL_TICKET_TRANSACTION_STATUS_ID
		FROM RM_DEAL_TICKET_TRANSACTION_STATUS 
		WHERE RM_DEAL_TICKET_ID = @dealTicketId AND 
		DEAL_TRANSACTION_STATUS_TYPE_ID = @entityId

IF @pkey > 0
	BEGIN
		UPDATE RM_DEAL_TICKET_TRANSACTION_STATUS 
		SET	COMMENTS = @comments 
		WHERE RM_DEAL_TICKET_TRANSACTION_STATUS_ID = @pkey
	END
ELSE
	BEGIN
		INSERT INTO RM_DEAL_TICKET_TRANSACTION_STATUS 
			(RM_DEAL_TICKET_ID, DEAL_TRANSACTION_STATUS_TYPE_ID, 
			DEAL_TRANSACTION_DATE, CBMS_IMAGE_ID, COMMENTS)
		VALUES(@dealTicketId, @entityId, @dealTransactionDate, 
			@cbmsImageId, @comments)
	END
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_STATUS_P] TO [CBMSApplication]
GO
