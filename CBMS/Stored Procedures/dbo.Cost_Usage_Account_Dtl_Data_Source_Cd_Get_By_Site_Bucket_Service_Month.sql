SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
    dbo.Cost_Usage_Account_Dtl_Data_Source_Cd_Get_By_Site_Bucket_Service_Month
 
 Description:  
	Used for returning the Data Source Cd for the given selection.
 
 Input Parameters:  
    Name			DataType    Default Description
-------------------------------------------------------------------------------------
    @Client_Hier_Id		INT
    @Bucket_Master_Id	INT
    @Service_Month		DATE
 
 Output Parameters:
    Name			Datatype  Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------  
    EXECUTE dbo.Cost_Usage_Account_Dtl_Data_Source_Cd_Get_By_Site_Bucket_Service_Month 
      @Client_Hier_Id = 2586
     ,@Bucket_Master_Id = 100154
     ,@Service_Month = '12/01/2010'
      
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 
 Modifications :  
 Initials	Date			Modification  
------------------------------------------------------------  
 AP			2012-01-08		Created SP
 AP			2012-03-26		Addnl Data Changes
							-- Replaced @Site_Id parameter with @Client_Hier_Id
							-- Removed CH table reference from the join
DMR			2014-01-09		Added WITH RECOMPILE statement to deal with erratic performance.
HG			2020-01-09		MAINT-9751, Simplified the code to improve the performance
								- Bucket_Master table join was removed as it is not necessary
								- Saving the data in a table variable and returns the required results queried from the table variable
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Data_Source_Cd_Get_By_Site_Bucket_Service_Month]
    (
        @Client_Hier_Id   INT
       ,@Bucket_Master_Id INT
       ,@Service_Month    DATE
    )
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE
        @CBMS_Data_Source_Cd INT
       ,@Data_Source_Count   INT
       ,@Data_Source_Cd      INT;

    DECLARE @Data_Source TABLE
        (
            Data_Source_Cd INT
        );

    SELECT @CBMS_Data_Source_Cd = cd.Code_Id
    FROM
        dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE
        cs.Std_Column_Name = 'Data_Source_Cd'
        AND cd.Code_Value = 'CBMS';

    INSERT INTO @Data_Source
    (
        Data_Source_Cd
    )
    SELECT cuad.Data_Source_Cd
    FROM
        dbo.Cost_Usage_Account_Dtl cuad
    WHERE
        cuad.Client_Hier_ID = @Client_Hier_Id
        AND cuad.Bucket_Master_Id = @Bucket_Master_Id
        AND cuad.Service_Month = @Service_Month;

    SELECT
        @Data_Source_Count = COUNT(DISTINCT cuad.Data_Source_Cd)
       ,@Data_Source_Cd = MAX(cuad.Data_Source_Cd)
    FROM
        @Data_Source cuad;

    SELECT (CASE
                WHEN @Data_Source_Count > 1 THEN
                    @CBMS_Data_Source_Cd
                ELSE
                    @Data_Source_Cd
            END
           ) AS Data_Source_Cd;

END;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Data_Source_Cd_Get_By_Site_Bucket_Service_Month] TO [CBMSApplication]
GO
