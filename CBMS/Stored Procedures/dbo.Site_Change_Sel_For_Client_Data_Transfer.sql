SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Change_Sel_For_Client_Data_Transfer]   

DESCRIPTION: This procedure used select divisions for   

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------

	DECLARE @tvp_CH_Filter AS [tvp_Client_Data_Transfer_CH_Filter]

	INSERT INTO @tvp_CH_Filter 
	VALUES	(1,1,1228,100016,0)
			,(1,1,32399,100015,1)

	EXEC [Site_Change_Sel_For_Client_Data_Transfer] 
		@From_Client_Id = 109,
		@Last_Version_Id = 2547028598,
		@tvp_CH_Filter = @tvp_CH_Filter 

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			8 Jul 2019		Created 	
 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Site_Change_Sel_For_Client_Data_Transfer]
	(
		@From_Client_Id INT,
		@Last_Version_Id BIGINT,
		@tvp_CH_Filter [tvp_Client_Data_Transfer_CH_Filter] READONLY
	)
AS
BEGIN
	  
    SET NOCOUNT ON 

	DECLARE @tvp_Count INT

	SELECT @tvp_Count = COUNT(1)
	FROM @tvp_CH_Filter

	SELECT	ch.Client_Hier_Id,
			ch.Hier_Level_Cd,
			ch2.Client_Hier_Id AS Division_Client_Hier_Id
	FROM CHANGETABLE(CHANGES Core.Client_Hier, @Last_Version_Id) ct
	INNER JOIN Core.Client_Hier ch 
		ON ch.Client_Hier_Id = ct.Client_Hier_Id
			AND ch.Site_Id > 0 --Sites
	LEFT JOIN Core.Client_Hier ch2
		ON	ch2.Site_Id = 0 
			AND ch2.Sitegroup_Id = ch.Sitegroup_Id
	LEFT JOIN @tvp_CH_Filter chf 
		ON chf.Client_Hier_Id = CASE	WHEN chf.Hier_Level_Cd = 100016 --Site
											THEN ch.Client_Hier_Id
										WHEN chf.Hier_Level_Cd IN (100015,100017) --Division, Sitegroup
											AND chf.Is_Copy_All_Site = 1
											THEN ch2.Client_Hier_Id END
	WHERE ch.Client_Id = @From_Client_Id
	AND ct.SYS_CHANGE_VERSION <= CHANGE_TRACKING_CURRENT_VERSION()
	AND (@tvp_Count = 0
			OR chf.Client_Hier_Id IS NOT NULL)	
	GROUP BY	ch.Client_Hier_Id,
				ch.Hier_Level_Cd,
				ch2.Client_Hier_Id

END
GO
GRANT EXECUTE ON  [dbo].[Site_Change_Sel_For_Client_Data_Transfer] TO [CBMSApplication]
GO
