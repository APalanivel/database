SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- dbo.GET_FISCAL_YEAR_ENTITY_DETAILS_P 158

CREATE   PROCEDURE dbo.GET_FISCAL_YEAR_ENTITY_DETAILS_P
	@entityType int
	AS
	begin
		set nocount on

		select 	entity_id, 
			entity_name 
		from 	entity 
		where 	entity_type = @entityType 
		order by  
			CASE entity_name 
			WHEN 'January' THEN 1 
			WHEN 'February' THEN 2 
			WHEN 'March' THEN 3 
			WHEN 'April' THEN 4 
			WHEN 'May' THEN 5  
			WHEN 'June' THEN 6 
			WHEN 'July' THEN 7 
			WHEN 'August' THEN 8 
			WHEN 'September' THEN 9  
			WHEN 'October' THEN 10  
			WHEN 'November' THEN 11 
			WHEN 'December' THEN 12 
			ELSE               0 
			END

	end 



GO
GRANT EXECUTE ON  [dbo].[GET_FISCAL_YEAR_ENTITY_DETAILS_P] TO [CBMSApplication]
GO
