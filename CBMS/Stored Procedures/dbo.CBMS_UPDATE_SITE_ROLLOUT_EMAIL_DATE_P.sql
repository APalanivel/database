SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE   PROCEDURE dbo.CBMS_UPDATE_SITE_ROLLOUT_EMAIL_DATE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@site_id int
	AS

	update site set rollout_email_date = getdate(), rollout_email_sent_by = @user_id  where site_id = @site_id





GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_SITE_ROLLOUT_EMAIL_DATE_P] TO [CBMSApplication]
GO
