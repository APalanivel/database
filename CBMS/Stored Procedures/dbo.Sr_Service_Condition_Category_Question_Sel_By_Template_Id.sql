SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Category_Question_Sel_By_Template_Id]
           
DESCRIPTION:             
			To get given template's questions and categories
			
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Language_CD						INT				NULL

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
        EXEC dbo.Sr_Service_Condition_Category_Question_Sel_By_Template_Id 15
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-31	Global Sourcing - Phase3 - GCS-602 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Category_Question_Sel_By_Template_Id]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage'
            
      SELECT
            ssct.Sr_Service_Condition_Template_Id
           ,ssctcm.Sr_Service_Condition_Category_Id
           ,ssctcm.Display_Seq AS Category_Order
           ,ssctqm.Sr_Service_Condition_Question_Id
           ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
           ,ssctqm.Display_Seq AS Question_Order
      FROM
            dbo.Sr_Service_Condition_Template ssct
            INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm
                  ON ssct.Sr_Service_Condition_Template_Id = ssctcm.Sr_Service_Condition_Template_Id
            INNER JOIN dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                  ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = ssctqm.Sr_Service_Condition_Template_Category_Map_Id
            INNER JOIN dbo.Sr_Service_Condition_Question sscq
                  ON ssctqm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
            LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                  ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                     AND sscqlv.Language_Cd = @Language_CD
      WHERE
            ssct.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
      ORDER BY
            ssctcm.Display_Seq
           ,ssctqm.Display_Seq
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Category_Question_Sel_By_Template_Id] TO [CBMSApplication]
GO
