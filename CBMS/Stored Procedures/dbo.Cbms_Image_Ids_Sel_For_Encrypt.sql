SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME: dbo.Cbms_Image_Ids_Sel_For_Encrypt  
  
DESCRIPTION:   
  Procedure will return non Encrypted CBMS Images

INPUT PARAMETERS:      
      Name				DataType       Default   Description      
------------------------------------------------------------------------------------------------------------------------      
  @Client_Id			INT
      
OUTPUT PARAMETERS:      
      Name                DataType          Default     Description      
------------------------------------------------------------------------------------------------------------------------     
	CBMS_IMAGE_ID			INT

USAGE EXAMPLES:  
------------------------------------------------------------------------------------------------------------------------  
	EXEC Cbms_Image_Ids_Sel_For_Encrypt 11278
  
AUTHOR INITIALS:  
 Initials	Name
------------------------------------------------------------------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------------------------------------------------------------------
	KVK		10/16/2019		Created
******/
CREATE PROCEDURE [dbo].[Cbms_Image_Ids_Sel_For_Encrypt]
(@Client_Id INT)
AS
BEGIN

    SET NOCOUNT ON;

    SELECT cu.CBMS_IMAGE_ID
    FROM dbo.CU_INVOICE cu WITH (NOLOCK)
        LEFT JOIN dbo.Cbms_Image_Encrypt id WITH (NOLOCK)
            ON id.CBMS_IMAGE_ID = cu.CBMS_IMAGE_ID
    WHERE cu.CLIENT_ID = @Client_Id
          AND id.CBMS_IMAGE_ID IS NULL
    GROUP BY cu.CBMS_IMAGE_ID;

END;


GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Ids_Sel_For_Encrypt] TO [CBMSApplication]
GO
