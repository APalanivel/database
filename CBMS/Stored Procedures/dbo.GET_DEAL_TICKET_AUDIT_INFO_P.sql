SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_AUDIT_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE           PROCEDURE dbo.GET_DEAL_TICKET_AUDIT_INFO_P

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int

AS
set nocount on
	SELECT 
	cemui.USERNAME AS DEAL_INITIATED_BY, 
	rmdt.DEAL_INITIATED_DATE, 
	rmaui1.USERNAME AS ORDER_PLACED_BY, 
	rmdt.ORDER_PLACED_DATE, 
	rmaui2.USERNAME AS VERBAL_CONFIRMATION_BY, 
	rmdt.VERBAL_CONFIRMATION_DATE, 
	cemui.FIRST_NAME + ' ' + cemui.LAST_NAME AS DEAL_INITIATED_BY_NAME, 
	rmaui1.FIRST_NAME + ' ' + rmaui1.LAST_NAME AS ORDER_PLACED_BY_NAME, 
	rmaui2.FIRST_NAME + ' ' + rmaui2.LAST_NAME AS VERBAL_CONFIRMATION_BY_NAME,  
	cemui.EMAIL_ADDRESS AS DEAL_INITIATED_BY_EMAIL, 
	rmaui1.EMAIL_ADDRESS AS ORDER_PLACED_BY_EMAIL, 
	rmaui2.EMAIL_ADDRESS AS VERBAL_CONFIRMATION_BY_EMAIL  
FROM 
	RM_DEAL_TICKET rmdt 
		LEFT JOIN USER_INFO rmaui1 ON 
			(rmdt.ORDER_PLACED_BY = rmaui1.USER_INFO_ID)
		LEFT JOIN USER_INFO rmaui2 ON 
			(rmdt.VERBAL_CONFIRMATION_BY = rmaui2.USER_INFO_ID), 
	USER_INFO cemui
WHERE 
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.DEAL_INITIATED_BY = cemui.USER_INFO_ID
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_AUDIT_INFO_P] TO [CBMSApplication]
GO
