SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Contract_Cu_Invoice_Map_Del_By_Contract_Account_List 
  
DESCRIPTION:  
 It is used when recalc is deleted then all recalc should be show in un associated list.
  
INPUT PARAMETERS:  
 Name						DataType			Default					Description  
----------------------------------------------------------------------------------------  
 @Account_Id				INT   
 @Contract_Id				INT					  
 @User_Info_Id				INT
  
OUTPUT PARAMETERS:  
 Name					DataType			Default					Description  
----------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
----------------------------------------------------------------------------------------
BEGIN TRAN

EXEC [dbo].[Contract_Cu_Invoice_Map_Del_By_Contract_Account_List]
    @Contract_Id = 1
    , @Account_Id_List = '1'
  
 ROLLBACK TRAN
 
AUTHOR INITIALS:  
 Initials		Name  
----------------------------------------------------------------------------------------  
 NR				Narayana Reddy
    
MODIFICATIONS  
 Initials			Date				Modification  
----------------------------------------------------------------------------------------  
  NR				2019-10-03			Created For Add Contract.  

******/

CREATE PROCEDURE [dbo].[Contract_Cu_Invoice_Map_Del_By_Contract_Account_List]
    (
        @Contract_Id INT
        , @Account_Id_List NVARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DELETE
        ccim
        FROM
            dbo.Contract_Cu_Invoice_Map ccim
        WHERE
            ccim.Contract_Id = @Contract_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Account_Id_List, ',') us
                           WHERE
                                us.Segments = ccim.Account_Id);


    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Contract_Cu_Invoice_Map_Del_By_Contract_Account_List] TO [CBMSApplication]
GO
