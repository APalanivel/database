SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Contract_Meter_Volume_Del]  

DESCRIPTION: It Deletes Contract Meter Volume for Selected Contract Id.     
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Contract_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Contract_Meter_Volume_Del 20140
  Rollback Tran

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	RR			Raghu Reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			16-JUN-10	CREATED
	RR			2020-04-30	GRM - Contract volumes enhancement - Modified to delete data from dbo.Contract_Meter_Volume_Dtl

*/

CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Del]
    (
        @Contract_Meter_Volume_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE  FROM
        dbo.Contract_Meter_Volume_Dtl
        WHERE
            CONTRACT_METER_VOLUME_ID = @Contract_Meter_Volume_Id;

        DELETE  FROM
        dbo.CONTRACT_METER_VOLUME
        WHERE
            CONTRACT_METER_VOLUME_ID = @Contract_Meter_Volume_Id;

    END;

GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Del] TO [CBMSApplication]
GO
