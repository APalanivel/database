SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_CONTRACT_NUMBER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contractNumber	varchar(150)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec dbo.SR_RFP_CHECK_CONTRACT_NUMBER_P 10866 - 001
CREATE   PROCEDURE dbo.SR_RFP_CHECK_CONTRACT_NUMBER_P

@contractNumber varchar(150)
	AS
set nocount on
	select COUNT(CONTRACT_ID) from contract where ED_CONTRACT_NUMBER = @contractNumber
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_CONTRACT_NUMBER_P] TO [CBMSApplication]
GO
