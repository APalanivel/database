SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                          
NAME: dbo.Variance_Extraction_Service_Param_sel                           
                        
DESCRIPTION:                             
                        
 To get the CalcVals based on the cu_invoice_id, account_id,Commodity_Id.                        
                         
INPUT PARAMETERS:                            
Name     DataType Default   Description                            
-------------------------------------------------------------------------                            
@Cu_Invoice_Id     INT                             
@Account_Id     INT                              
@Commodity_Id    INT                             
                        
OUTPUT PARAMETERS:                            
Name     DataType Default   Description                            
-------------------------------------------------------------------------                            
                        
USAGE EXAMPLES:                            
------------------------------------------------------------                            
							
     exec Variance_Extraction_Service_Param_sel                   
                          
                        
AUTHOR INITIALS:                            
Initials Name                            
------------------------------------------------------------                            
RKV   Ravi Kumar Vegesna                        
                       
                         
MODIFICATIONS                             
Initials Date  Modification                            
------------------------------------------------------------                            
RKV   2020-01-03 Created                        
                          
******/
CREATE PROCEDURE [dbo].[Variance_Extraction_Service_Param_sel]
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            Variance_Extraction_Service_Param_Id
            , Param_Name
            , Data_Type_Cd
            , Created_Ts
            , Last_Change_Ts
        FROM
            dbo.Variance_Extraction_Service_Param;
    END;

GO
GRANT EXECUTE ON  [dbo].[Variance_Extraction_Service_Param_sel] TO [CBMSApplication]
GO
