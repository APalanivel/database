SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:    [Workflow].[Workflow_Queue_I_Save_FilterQuery]    
     
DESCRIPTION: Inserting into Saved filter data to Workflow.Workflow_queue_saved_filter_query &Workflow.Workflow_queue_saved_filter_query_Value    
        
 INPUT PARAMETERS:        
 Name   DataType  Default Description        
 @int_Id      INT            
 @int_Module_Id     INT            
 @nvarchar_Custom_Filter_Name NVARCHAR(MAX)            
 @datetime_Expires_On   DATETIME           
 @bit_No_Expiration_Date  BIT            
 @int_Created_User_Id   INT            
 --@int_Last_Modified_User_Id INT            
 @bit_Is_Public_Filter   BIT        
 @TVP       Save_FilterQuery_SearchFilterValues READONLY            
 @TVPIdValues     Save_FilterQuery_SearchFilterValues READONLY          
 @bit_IsActive     BIT     
 @SubQueueName     NVARCHAR(MAX)     
------------------------------------------------------------        
     
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:        
 EXEC Workflow.Workflow_Queue_I_Save_FilterQuery @int_Id = 0,                                  -- int    
                                                @int_Module_Id = 0,                           -- int    
                                                @nvarchar_Custom_Filter_Name = N'',           -- nvarchar(max)    
                                                @datetime_Expires_On = '2019-09-10 09:40:54', -- datetime    
                                                @bit_No_Expiration_Date = NULL,               -- bit    
                                                @int_Created_User_Id = 0,                     -- int    
                                                @bit_Is_Public_Filter = NULL,                 -- bit    
                                                @TVP = NULL,                                  -- Save_FilterQuery_SearchFilterValues    
                                                @TVPIdValues = NULL,                          -- Save_FilterQuery_SearchFilterValues    
                                                @bit_IsActive = NULL,                         -- bit    
                                                @SubQueueName = N''                           -- nvarchar(max)    
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name         
------------------------------------------------------------        
TRK   Ramakrishna Thummala Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification  
------------------------------------------------------------        
  SEP-2019  Ramakrishna Thummala   Initial Creation      
    
******/ 
  
CREATE PROCEDURE [Workflow].[Workflow_Queue_I_Save_FilterQuery]                                 
(                
 @int_Id      AS INT,                
 @int_Module_Id     AS INT,                
 @nvarchar_Custom_Filter_Name AS NVARCHAR(MAX),                
 @datetime_Expires_On   AS DATETIME=NULL,               
 @bit_No_Expiration_Date  AS BIT ,               
 @int_Created_User_Id   AS INT ,                              
 @bit_Is_Public_Filter   AS BIT,            
 @TVP       AS Save_FilterQuery_SearchFilterValues READONLY,                 
 @TVPIdValues     AS Save_FilterQuery_SearchFilterValues READONLY,              
 @bit_IsActive     AS BIT,         
 @SubQueueName     AS NVARCHAR(MAX)     --saved filter, my exception          
                
)                
AS                
BEGIN                
                
  DECLARE  @subQueue AS int          
          
  IF @datetime_Expires_On IS NULL         
   BEGIN         
   SET @datetime_Expires_On='12/31/9999'         
   END        
 ELSE         
  SET @datetime_Expires_On=@datetime_Expires_On        
        
 SELECT @subQueue = Workflow_Sub_Queue_Id  FROM  Workflow.Workflow_sub_queue             
 WHERE Workflow_Queue_Id = @int_Module_Id and Workflow_Sub_Queue_Name= @SubQueueName                 
             
          
 SELECT SF.Workflow_Queue_Search_Filter_Id AS SearchFilters_Id,T.SearchFilterValue [Key] ,TV.SearchFilterValue  [Value]              
 INTO #tvp  FROM  Workflow.workflow_queue_search_filter SF                   
 JOIN Workflow.search_filter MSF  ON SF.Search_Filter_Id = MSF.Search_Filter_Id                 
 JOIN  @TVP T ON T.SearchFilter_Name = MSF.Filter_Name        
 JOIN  @TVPIdValues TV ON TV.SearchFilter_Name = MSF.Filter_Name                
 WHERE SF.Workflow_Queue_Id = @int_Module_Id           
          
          
 DECLARE @Code_Id INT        
 SELECT @Code_Id=C.Code_Id FROM dbo.Code AS C         
 INNER JOIN dbo.Codeset AS C2 ON C.CodeSet_ID=C2.CodeSet_ID AND C2.CodeSet_Name ='SavedFilter' AND C.Code_Value='UserDefinedFilter'        
 SELECT @Code_Id        
        
 INSERT INTO Workflow.Workflow_queue_saved_filter_query                 
 (          
  Workflow_Sub_Queue_Id          
  ,Saved_Filter_Name          
  ,Saved_Filter_Type_Cd          
  ,Owner_User_Id          
  ,Expiration_Dt             
  ,Display_Seq          
  ,Created_User_Id          
  ,Created_Ts          
  ,Updated_User_Id          
  ,Last_Change_Ts          
  ,Is_Shared          
           
 )                
 SELECT               
  @subQueue  ,          
  @nvarchar_Custom_Filter_Name,          
  @Code_Id,          
  @int_Created_User_Id,         
  CASE WHEN @bit_No_Expiration_Date = 0 THEN @datetime_Expires_On  ELSE '12/31/9999' END,         
  1,             
  @int_Created_User_Id,          
  GETDATE(),          
  @int_Created_User_Id,          
  GETDATE(),          
  @bit_Is_Public_Filter         
     
                  
                  
 SELECT @int_Id = SCOPE_IDENTITY()                
                   
                
 INSERT INTO Workflow.workflow_queue_saved_filter_query_value               
   (          
    Workflow_Queue_Saved_Filter_Query_Id          
    ,Workflow_Queue_Search_Filter_Id          
    ,Selected_Value          
    ,Selected_KeyValue          
    ,Created_User_Id          
    ,Created_Ts          
    ,Updated_User_Id          
    ,Last_Change_Ts        
 )              
          
 SELECT             
     @int_Id,            
     v.SearchFilters_Id,             
     [Key] ,            
     [Value],            
     @int_Created_User_Id,            
     GETDATE(),            
     @int_Created_User_Id,            
     GETDATE()           
 FROM #TVP v       
                   
               
               
 SELECT @int_Id AS ID                
 DROP TABLE #tvp               
                
 --RETURN                  
END         
        
  
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_I_Save_FilterQuery] TO [CBMSApplication]
GO
