SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.Cu_Invoice_Process_contract_recalc_Update    
		
 DESCRIPTION:     
 
 INPUT PARAMETERS:    
 Name				DataType			Default				Description    
----------------------------------------------------------------------------                 
 
 OUTPUT PARAMETERS:    
 Name				DataType			Default				Description    
----------------------------------------------------------------------------     
            
 USAGE EXAMPLES:    
----------------------------------------------------------------------------     
	Begin Transaction
	SELECT contract_Recalc FROM Cu_Invoice_Process   where cu_invoice_id = 250
	EXEC Cu_Invoice_Process_contract_recalc_Update 250,1
	SELECT contract_Recalc FROM Cu_Invoice_Process   where cu_invoice_id = 250
	Rollback Transaction
  
   
  AUTHOR INITIALS:    
 Initials		Name    
----------------------------------------------------------------------------     
	RKV				Ravi Kumar Vegesna 
 
 MODIFICATIONS:    
 Initials		Date				Modification    
----------------------------------------------------------------------------     
	RKV			2015-12-01			Created as Part of AS400-PII
    
******/    
CREATE PROCEDURE [dbo].[Cu_Invoice_Process_contract_recalc_Update]
      ( 
       @cu_invoice_id INT
      ,@contract_recalc BIT )
AS 
BEGIN

      SET NOCOUNT ON

   
      UPDATE
            Cu_Invoice_Process
      SET   
            contract_recalc = @contract_recalc
      WHERE
            cu_invoice_id = @cu_invoice_id

        

END

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Process_contract_recalc_Update] TO [CBMSApplication]
GO
