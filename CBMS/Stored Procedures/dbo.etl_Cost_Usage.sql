
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.etl_Cost_Usage

DESCRIPTION:
This object is used to populate the cost&Usage site level info to the reportinjg tables at DV side.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MinDBTS		BIGINT 
	@MaxDBTS		BIGINT   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @MinDBts BIGINT
		,@MaxDbts BIGINT
	
	SELECT @MinDBts = @@Dbts - 100000
	SELECT @MaxDbts = @@Dbts

	EXEC dbo.etl_Cost_Usage @MinDBts, @MaxDbts


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SKA			Shobhit Kumar Agrawal
	AP			Athmaram Pabbathi
	BCH			Balaraju
	
MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
SKA	   11/FEB/2010	Modified the scripts for ther services
	   03/02/2010	Removed NOLOCK HINTS	
					Removed the reference of Client table, Division view as we can get the info from Site table itself

HG	   04/23/2010	Sometimes we would have cost but no usage as well as usage, but no cost, but these informations should be replicated to DV reporting tables.
					As we have constraints on DV2 reporting tables did the following changes in the query.
					Condition added to convert the null cost/usage values to 0
					Commodity table added to get the default if UOM is not available as ETL tables can't have NULL value
					Currencty unit id prepopulated in a variable and used the value of currency_unit is not availble for other services.

HG	   04/26/2010	In the query which pulls other services cost & usage ISNULL function used to convert the NULL cost/usage moved to Subselect query as we have to filter the records based on these columns

DMR	   09/10/2010	Modified for Quoted_Identifier
AP	   08/17/2011	Removed Union of select statments for EP & NG and added 'Total Usage' bucket in the sub query.
AP	   08/22/2011	Added qualifiers to all objects with the owner name.
BCH	   2012-03-19	Used core.client_hier table instead of site table.

******/
CREATE PROCEDURE [dbo].[etl_Cost_Usage]
      ( 
       @MinDBTS BIGINT
      ,@MaxDBTS BIGINT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Currency_Unit_ID INT

      SELECT
            @Currency_Unit_ID = Currency_Unit_Id
      FROM
            dbo.Currency_Unit
      WHERE
            Currency_Unit_Name = 'USD'
	

      SELECT
            cu.Client_Id
           ,cu.Site_Id
           ,cu.Commodity_Id
           ,cu.Service_Date
           ,cu.Service_Year
           ,cu.Service_Month
           ,isnull(cu.UOM_Type_Cd, com.Default_UOM_Entity_Type_Id) AS UOM_Type_Cd
           ,isnull(cu.Currency_Id, @Currency_Unit_ID) AS Currency_ID
           ,cu.Total_Cost
           ,cu.Total_Usage
      FROM
            ( SELECT
                  ch.Client_ID
                 ,ch.Site_Id
                 ,bm.Commodity_Id AS Commodity_Id
                 ,cusd.Service_Month AS Service_Date
                 ,year(cusd.Service_Month) AS Service_Year
                 ,month(cusd.service_month) AS Service_Month
                 ,max(case WHEN bm.Bucket_Name IN ( 'VOLUME', 'Total Usage' ) THEN cusd.UOM_Type_Id
                      END) AS UOM_Type_Cd
                 ,max(case WHEN bm.Bucket_Name = 'Total Cost' THEN cusd.Currency_Unit_ID
                      END) AS Currency_Id
                 ,isnull(sum(case WHEN bm.Bucket_Name = 'Total Cost' THEN cusd.Bucket_Value
                             END), 0) AS Total_Cost
                 ,isnull(sum(case WHEN bm.Bucket_Name IN ( 'VOLUME', 'Total Usage' ) THEN cusd.Bucket_Value
                             END), 0) AS Total_Usage
              FROM
                  dbo.Cost_Usage_Site_Dtl cusd
                  INNER JOIN core.Client_Hier ch
                        ON ch.Client_Hier_Id = cusd.Client_Hier_Id
                  INNER JOIN dbo.Bucket_Master bm
                        ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
              WHERE
                  ch.Site_Id > 0
                  AND bm.Bucket_Name IN ( 'Total Cost', 'VOLUME', 'Total Usage' )
                  AND cusd.Row_Version BETWEEN @MinDBTS AND @MaxDBTS
              GROUP BY
                  ch.Client_ID
                 ,ch.Site_Id
                 ,bm.Commodity_Id
                 ,cusd.Service_Month ) cu
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = cu.Commodity_Id
      WHERE
            ( cu.Total_Cost != 0
              OR cu.Total_Usage != 0 )

END
;
GO

GRANT EXECUTE ON  [dbo].[etl_Cost_Usage] TO [CBMSApplication]
GO
