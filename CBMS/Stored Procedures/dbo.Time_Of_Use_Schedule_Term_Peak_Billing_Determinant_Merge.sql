SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant_Merge

DESCRIPTION:
	Inserts mapping between the determinant and the peaks

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Billing_Determinant_Id					INT
	@Time_Of_Use_Schedule_Term_Peak_Ids		VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------
					
Exec dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant_Merge 175212, '5,9'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------------------------
	CPE			2012-07-25		Created
******/ 

CREATE PROCEDURE dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant_Merge
( 
 @Billing_Determinant_Id INT
,@Time_Of_Use_Schedule_Term_Peak_Ids VARCHAR(MAX) )
AS 
BEGIN
	
      SET NOCOUNT ON

      MERGE INTO dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant tgt 
      USING ( SELECT
                  Segments AS Time_Of_Use_Schedule_Term_Peak_Id
            FROM
                  dbo.ufn_split(@Time_Of_Use_Schedule_Term_Peak_Ids, ',') ) src
		ON tgt.BILLING_DETERMINANT_ID = @BILLING_DETERMINANT_ID
            AND tgt.Time_Of_Use_Schedule_Term_Peak_Id = src.Time_Of_Use_Schedule_Term_Peak_Id 
      WHEN NOT MATCHED THEN
		INSERT ( BILLING_DETERMINANT_ID, Time_Of_Use_Schedule_Term_Peak_Id )
		VALUES
            (
             @Billing_Determinant_Id
            ,src.Time_Of_Use_Schedule_Term_Peak_Id ) 
      WHEN NOT MATCHED BY SOURCE AND BILLING_DETERMINANT_ID = @Billing_Determinant_Id THEN 
		DELETE ;
END

;
GO
GRANT EXECUTE ON  [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant_Merge] TO [CBMSApplication]
GO
