SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Batch_Map_Del  

DESCRIPTION: It Deletes Rm Deal Ticket Batch Map for Selected Rm Deal Ticket Details Id,Batch_Process_Type_Id,Batch_Generation_Date.
      
INPUT PARAMETERS:          
	NAME						DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Id			INT
    @Batch_Process_Type_Id		INT
    @Batch_Generation_Date		DATETIME

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
   BEGIN TRAN
		
		EXEC dbo.Rm_Deal_Ticket_Batch_Map_del 101635,693,'2004-11-23 00:00:31.827'
		
	ROLLBACK TRAN

	SELECT
		 rm_deal_ticket_id,Batch_Process_Type_Id,Batch_Generation_Date
	FROM
		dbo.Rm_Deal_Ticket_Batch_Map
	WHERE
		rm_deal_ticket_id = 101635

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Batch_Map_Del
    (
       @Rm_Deal_Ticket_Id		INT
      ,@Batch_Process_Type_Id	INT
      ,@Batch_Generation_Date	DATETIME
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Deal_Ticket_Batch_Map
	WHERE
		Rm_Deal_Ticket_Id = @Rm_Deal_Ticket_Id
		AND Batch_Process_Type_Id = @Batch_Process_Type_Id
		AND Batch_Generation_Date = @Batch_Generation_Date
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Batch_Map_Del] TO [CBMSApplication]
GO
