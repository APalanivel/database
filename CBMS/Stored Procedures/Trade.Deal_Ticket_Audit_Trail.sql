SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Audit_Trail
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Audit_Trail  23
	Exec Trade.Deal_Ticket_Audit_Trail  21
	Exec Trade.Deal_Ticket_Audit_Trail  291
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
     
    
******/  

CREATE PROCEDURE [Trade].[Deal_Ticket_Audit_Trail] ( @Deal_Ticket_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;	
      
      SELECT
            dt.Deal_Ticket_Id
           ,'Initiated By' AS Audit_Action
           ,initiated.USERNAME AS Username
           ,REPLACE(CONVERT(VARCHAR(20), dt.Created_Ts, 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), dt.Created_Ts, 100), 7)) + ' EST' AS Action_Date
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.USER_INFO initiated
                  ON dt.Created_User_Id = initiated.USER_INFO_ID
      WHERE
            DT.Deal_Ticket_Id = @Deal_Ticket_Id
            
         
                   
  
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Audit_Trail] TO [CBMSApplication]
GO
