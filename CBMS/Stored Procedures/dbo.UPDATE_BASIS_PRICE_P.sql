SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_BASIS_PRICE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@indexId       	int       	          	
	@pricePointName	varchar(200)	          	
	@basisPrice    	decimal(32,16)	          	
	@monthidentifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.UPDATE_BASIS_PRICE_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@indexId INT,
	@pricePointName VARCHAR(200),
	@basisPrice decimal(32,16),
	@monthidentifier DATETIME

AS
DECLARE @count INT
	, @forecastId INT
	, @priceIndex INT
	, @entityId INT
	, @a INT
	, @counter INT
BEGIN

	SET NOCOUNT ON

	SELECT @priceIndex=price_index_id FROM price_index
	WHERE index_id=	@indexId
		AND pricing_point=@pricePointName

	SELECT @forecastId=RM_FORECAST_ID FROM RM_FORECAST
	WHERE price_index_id= (@priceIndex)

	
	IF @forecastId IS NULL
	 BEGIN
		SELECT @entityId = ENTITY_ID FROM ENTITY
		WHERE ENTITY_TYPE=272 AND ENTITY_NAME='Basis'
	
		INSERT INTO RM_FORECAST (forecast_type_id,price_index_id,forecast_from_date)
		VALUES(@entityId,@priceIndex,'01/01/2002')
	
		SET @a=SCOPE_IDENTITY()

		INSERT INTO RM_FORECAST_DETAILS (RM_FORECAST_ID,MONTH_IDENTIFIER, BASIS_PRICE)
		VALUES(@a,@monthidentifier,@basisPrice)

	 END
	ELSE
	 BEGIN
			UPDATE RM_FORECAST_DETAILS
			SET RM_FORECAST_ID=@forecastId
				, MONTH_IDENTIFIER=@monthidentifier
				, BASIS_PRICE=@basisPrice
			WHERE RM_FORECAST_ID=@forecastId
				AND month_identifier=@monthidentifier


			-- If update fails insert the record
			IF @@ROWCOUNT=0
			 BEGIN

				INSERT INTO RM_FORECAST_DETAILS (RM_FORECAST_ID, MONTH_IDENTIFIER, BASIS_PRICE)
				VALUES(@forecastId,@monthidentifier,@basisPrice)

			 END
	 END
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_BASIS_PRICE_P] TO [CBMSApplication]
GO
