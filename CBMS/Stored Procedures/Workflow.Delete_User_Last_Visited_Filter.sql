SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********                       
NAME:                      
   Workflow.Delete_User_Last_Visited_Filter                   
                  
DESCRIPTION:                      
  This procedure used to delete the data from Workflow.User_Last_Visited_Filter table                    
                  
INPUT PARAMETERS:                      
                   
Name									DataType		Default Description                      
-----------------------------------------------------------------------------                   
@User_Info_Id			INT
@Session_Id				INT                
                           
OUTPUT PARAMETERS:                      
Name    DataType  Default Description                      
------------------------------------------------------------                      
                      
USAGE EXAMPLES:                      
------------------------------------------------------------                      
EXEC Workflow.Delete_User_Last_Visited_Filter                  
             
AUTHOR INITIALS:                      
Initials  Name                      
------------------------------------------------------------                      
PRV    Pramod Reddy V                   
                  
                  
MODIFICATIONS                       
 Initials Date   Modification                      
------------------------------------------------------------                      
PRV   2019-09-03  SP Created                  
                  
******/

CREATE PROCEDURE [Workflow].[Delete_User_Last_Visited_Filter]
(
    @User_Info_Id INT,
    @Session_Id INT
)
AS
BEGIN

    IF EXISTS
    (
        SELECT 1
        FROM Workflow.User_Last_Visited_Filter
        WHERE User_Info_Id = @User_Info_Id
              AND Session_Id = @Session_Id
    )
    BEGIN

        DELETE FROM Workflow.User_Last_Visited_Filter
        WHERE User_Info_Id = @User_Info_Id
              AND Session_Id = @Session_Id;


    END;

END;
GO
GRANT EXECUTE ON  [Workflow].[Delete_User_Last_Visited_Filter] TO [CBMSApplication]
GO
