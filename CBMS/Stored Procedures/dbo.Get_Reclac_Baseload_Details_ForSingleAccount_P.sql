SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.Get_Reclac_Baseload_Details_ForSingleAccount_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@monthId		DATETIME
	@accountId		INT
	@type			VARCHAR( 20 )       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	exec dbo.Get_Reclac_Baseload_Details_ForSingleAccount_P '1/1/2005',16207,'BaseLoad'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	15/Feb/2010	Created
	HG			03/12/2010	GROUP BY clause added to fetch the distinct records

******/

CREATE PROCEDURE DBO.Get_Reclac_Baseload_Details_ForSingleAccount_P
	@monthId AS DATETIME
	,@accountId AS INT
	,@type AS VARCHAR(20)
AS 
BEGIN
 
	SET NOCOUNT ON   

	SELECT
		cm.charge_master_id,
		cm.charge_display_id,
		ch.charge_id,
		ch.currency_unit_id,
		ch.charge_calculation_type_id,
		ch.charge_name,
		ch.is_prorate_monthly,
		lps.lps_unit_type_id,
		lps.lps_frequency_type_id,
		ch.charge_expression,
		bld.baseload_id,
		bld.baseload_volume,
		bld.baseload_low_tolerance,
		bld.baseload_high_tolerance,
		lps.lps_fixed_price,
		piv.index_value,
		con.is_contract_full_requirement,
		calcType.entity_name
	FROM
		dbo.charge_master cm
		INNER JOIN dbo.charge ch
			ON cm.charge_master_id = ch.charge_master_id
		INNER JOIN dbo.baseload_details bld
			ON ch.charge_parent_id = bld.baseload_id
				AND cm.charge_parent_id = bld.baseload_id
		INNER JOIN dbo.load_profile_specification lps
			ON bld.load_profile_specification_id = lps.load_profile_specification_id
		LEFT JOIN dbo.price_index_value piv
			ON (lps.price_index_id = piv.price_index_id
				AND MONTH(piv.index_month) = MONTH(@monthId) 
				AND YEAR(piv.index_month) = YEAR(@monthId)
				)
		INNER JOIN dbo.entity type
			ON cm.charge_type_id = type.entity_id
		INNER JOIN dbo.entity calcType
			ON calcType.entity_id = ch.charge_calculation_type_id
		INNER JOIN dbo.contract con
			ON lps.contract_id = con.contract_id
		INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samp
			ON samp.Contract_ID = con.CONTRACT_ID
	WHERE 
		type.entity_name = @type   
		AND MONTH(lps.month_identifier) = MONTH(@monthId) AND YEAR(lps.month_identifier) = YEAR(@monthId) 
		AND samp.account_id = @accountId  
	GROUP BY
		cm.charge_master_id,
		cm.charge_display_id,
		ch.charge_id,
		ch.currency_unit_id,
		ch.charge_calculation_type_id,
		ch.charge_name,
		ch.is_prorate_monthly,
		lps.lps_unit_type_id,
		lps.lps_frequency_type_id,
		ch.charge_expression,
		bld.baseload_id,
		bld.baseload_volume,
		bld.baseload_low_tolerance,
		bld.baseload_high_tolerance,
		lps.lps_fixed_price,
		piv.index_value,
		con.is_contract_full_requirement,
		calcType.entity_name
	  
END
GO
GRANT EXECUTE ON  [dbo].[Get_Reclac_Baseload_Details_ForSingleAccount_P] TO [CBMSApplication]
GO
