SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE dbo.GET_DIVISION_SITES_P
	@divisionID int
	AS
	begin
		set nocount on

		select  site_id,
			site_name 
		from	site 
		where 	division_id = @divisionID

	end
GO
GRANT EXECUTE ON  [dbo].[GET_DIVISION_SITES_P] TO [CBMSApplication]
GO
