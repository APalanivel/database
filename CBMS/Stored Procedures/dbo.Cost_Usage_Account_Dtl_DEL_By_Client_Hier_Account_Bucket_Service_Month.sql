SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month

DESCRIPTION:
	Used to delete data in Cost_Usage_Account_Dtl table for the given Client_Hier_Id, Account_Id, Bucket_Master_Id, Service_Month

INPUT PARAMETERS:
    Name			    DataType		Default	Description
------------------------------------------------------------
    @Client_Hier_Id	    INT
    @Account_Id	    INT
    @Bucket_Master_Id   INT
    @Service_Month	    DATE
      
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT * FROM dbo.Cost_Usage_Account_Dtl WHERE Client_Hier_Id = 1204 AND Account_Id = 440819 AND Bucket_Master_Id = 100244 AND Service_Month = '01/01/2010'

BEGIN TRANSACTION

EXECUTE dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month 
      @Client_Hier_Id = 1204
     ,@Account_Id = 440819
     ,@Bucket_Master_Id = 100244
     ,@Service_Month = '01/01/2010'
      
ROLLBACK


AUTHOR INITIALS:
	Initials	 Name
------------------------------------------------------------
	AP		 Athmaram Pabbathi
	
MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
    AP		 2012-03-19  Created to Replace dbo.Cost_Usage_Account_Dtl_Del_By_Cost_Usage_Account_Dtl_Id and to use Clustered Index

******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month
      ( 
       @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Month DATE )
AS 
BEGIN
	
      SET NOCOUNT ON

      DELETE
            dbo.Cost_Usage_Account_Dtl
      WHERE
            Account_Id = @Account_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Month = @Service_Month
            AND Client_Hier_Id = @Client_Hier_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month] TO [CBMSApplication]
GO
