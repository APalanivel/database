SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsNYISO_GetZonesForSheet

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[cbmsNYISO_GetZonesForSheet]

AS
BEGIN

	   select zone_id	
		, zone_name
		, zone_sheet_name
	     from ny_iso
	    where zone_sheet_name is not null
	 order by zone_sheet_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_GetZonesForSheet] TO [CBMSApplication]
GO
