SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 Cu_Invoice_SDE_Sub_Bucket_Mapping_Header_GetForCuInvoice  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------           
 @cu_invoice_id  int          
 @Account_ID  INT    NULL               
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 EXEC Cu_Invoice_SDE_Sub_Bucket_Mapping_Header_GetForCuInvoice 92963239
   
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
   
 RKV         Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
  RKV        2020-06-08 Create as part Data purple  
         
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_SDE_Sub_Bucket_Mapping_Header_GetForCuInvoice]
(
	@cu_invoice_id	INT
	, @Account_ID	INT = NULL
	, @Commodity_Id INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		-1 Cu_Exception_Id
		, @cu_invoice_id Cu_Invoice_Id
		, MAX(cisde.QUEUE_ID)
		, MIN(cisde.Created_Ts) date_in_queue
		, MIN(ISNULL(cl.CLIENT_NAME, 'Unknown')) client_name
		, CASE WHEN COUNT(DISTINCT s.SITE_ID) > 1 THEN
				   'Multiple'
			  ELSE
				  MIN(ISNULL((RTRIM(ad.city) + ', ' + st.STATE_NAME + ' (' + s.SITE_NAME + ')'), 'Unknown'))
		  END site_name
		, CASE WHEN COUNT(DISTINCT cisded.Account_Id) > 1 THEN
				   'Multiple'
			  ELSE
				  MIN(ISNULL(a.ACCOUNT_NUMBER, 'Unknown'))
		  END account_number
		, CASE WHEN COUNT(DISTINCT cisded.Account_Id) > 1 THEN
				   'unknown'
			  WHEN MIN(a.ACCOUNT_ID) IS NULL THEN
				  'Unknown'
			  WHEN MIN(a.ACCOUNT_GROUP_ID) IS NULL THEN
				  'Not Part of Group Bill'
			  ELSE
				  MIN(ISNULL(ag.GROUP_BILLING_NUMBER, 'Group Bill Number Not Defined'))
		  END AS group_bill_number
		, MAX(ui.FIRST_NAME) + ' ' + MAX(ui.LAST_NAME) AS queue_name
	FROM
		dbo.Cu_Invoice_Standing_Data_Exception cisde
		INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl cisded
			ON cisded.Cu_Invoice_Standing_Data_Exception_Id = cisde.Cu_Invoice_Standing_Data_Exception_Id
		INNER JOIN Code status_Cd
			ON status_Cd.Code_Id = cisde.Exception_Status_Cd
		INNER JOIN dbo.Code Type_Cd
			ON Type_Cd.Code_Id = cisde.Exception_Type_Cd
		INNER JOIN dbo.ACCOUNT a
			ON a.ACCOUNT_ID = cisded.Account_Id
		INNER JOIN dbo.USER_INFO ui
			ON ui.QUEUE_ID = cisde.QUEUE_ID
		LEFT JOIN(dbo.ADDRESS adr
				  JOIN(
						  SELECT
							  m.ACCOUNT_ID
							  , m.ADDRESS_ID
						  FROM
							  dbo.METER m
						  UNION
						  SELECT
							  map.ACCOUNT_ID
							  , m.ADDRESS_ID
						  FROM
							  dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
							  JOIN dbo.ACCOUNT a
								  ON a.ACCOUNT_ID = map.ACCOUNT_ID
							  JOIN dbo.METER m
								  ON m.METER_ID = map.METER_ID
					  ) AS x
					  ON x.ADDRESS_ID = adr.ADDRESS_ID)
			ON x.ACCOUNT_ID = cisded.Account_Id
		LEFT OUTER JOIN(dbo.SITE s
						JOIN dbo.ADDRESS ad
							ON ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
						JOIN dbo.State st
							ON st.STATE_ID = ad.state_id)
			ON s.SITE_ID = adr.ADDRESS_PARENT_ID
		LEFT JOIN dbo.CLIENT cl
			ON cl.CLIENT_ID = s.CLIENT_ID
		LEFT JOIN dbo.ACCOUNT_GROUP ag
			ON ag.ACCOUNT_GROUP_ID = a.ACCOUNT_GROUP_ID
	WHERE
		cisde.Cu_Invoice_Id = @cu_invoice_id
		AND status_Cd.Code_Value IN ( 'New', 'In Progress', 'Archived' )
		AND Type_Cd.Code_Value IN ( 'Sub-Bkt Determnt Mapping', 'Sub-Bkt Charge Mapping' )
		AND (
				@Account_ID IS NULL OR cisded.Account_Id = @Account_ID
			)
		AND (
				@Commodity_Id IS NULL OR cisded.Commodity_Id = @Commodity_Id
			);
END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_SDE_Sub_Bucket_Mapping_Header_GetForCuInvoice] TO [CBMSApplication]
GO
