SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_DELETE_FEED_FREQUENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@frequencyId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.UBM_DELETE_FEED_FREQUENCY_P
@userId varchar(10),
@sessionId varchar(20),

@frequencyId int


AS
set nocount on
	DELETE FROM UBM_FEED_FREQUENCY WHERE UBM_FEED_FREQUENCY_ID=@frequencyId
GO
GRANT EXECUTE ON  [dbo].[UBM_DELETE_FEED_FREQUENCY_P] TO [CBMSApplication]
GO
