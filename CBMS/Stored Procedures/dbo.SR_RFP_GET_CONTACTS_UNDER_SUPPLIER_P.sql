SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTACTS_UNDER_SUPPLIER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_RFP_GET_CONTACTS_UNDER_SUPPLIER_P 
@vendorId int

as
set nocount on
select	--userInfo.USER_INFO_ID, 
	scInfo.SR_SUPPLIER_CONTACT_INFO_ID,
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from	USER_INFO userInfo,	
	SR_SUPPLIER_CONTACT_INFO scInfo, 
	SR_SUPPLIER_CONTACT_VENDOR_MAP  cvMap

where	
	cvMap.vendor_id = @vendorId
	and scInfo.SR_SUPPLIER_CONTACT_INFO_ID = cvMap.SR_SUPPLIER_CONTACT_INFO_ID
	and scInfo.USER_INFO_ID = userInfo.USER_INFO_ID
	and userInfo.is_History = 0
	

order by userInfo.USERNAME
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTACTS_UNDER_SUPPLIER_P] TO [CBMSApplication]
GO
