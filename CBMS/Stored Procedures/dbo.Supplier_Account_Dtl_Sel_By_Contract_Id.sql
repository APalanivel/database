
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Supplier_Account_Dtl_Sel_By_Contract_Id]  
     
DESCRIPTION: 
	To Get Supplier Account Information for Selected Contract Id with pagination.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT						
	@Start_Index	INT			1
	@End_Index		INT			2147483647			
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Supplier_Account_Dtl_Sel_By_Contract_Id  79943,1,10

	EXEC Supplier_Account_Dtl_Sel_By_Contract_Id  12057,1,5
	
	EXEC Supplier_Account_Dtl_Sel_By_Contract_Id 89077, 1, 25
	 
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
    BCH			Balaraju      
    SP			Sandeep Pigilam
    
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			03-JUNE-10	CREATED
	BCH			2012-02-07  Added Site_Id column to Select list for Delete Tool requirement.
	SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
	
*/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtl_Sel_By_Contract_Id]
      ( 
       @Contract_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
            
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Quality_Assurance';
      WITH  Cte_Contracts
              AS ( SELECT
                        acct.VENDOR_ID Supp_Vendor_Id
                       ,ua.SITE_ID
                       ,ua.VENDOR_ID
                       ,acct.account_number
                       ,acct.ACCOUNT_ID
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY acct.account_number )
                       ,Total_Rows = COUNT(1) OVER ( )
                   FROM
                        dbo.supplier_account_meter_map samm
                        JOIN dbo.account acct
                              ON acct.account_id = samm.account_id
                        JOIN dbo.meter AS m
                              ON m.meter_id = samm.meter_id
                        JOIN dbo.account AS ua
                              ON ua.account_id = m.account_id
                   WHERE
                        samm.CONTRACT_ID = @Contract_Id
                   GROUP BY
                        acct.ACCOUNT_ID
                       ,acct.account_number
                       ,acct.VENDOR_ID
                       ,ua.SITE_ID
                       ,ua.VENDOR_ID)
            SELECT
                  cl.CLIENT_NAME
                 ,cont.SITE_ID
                 ,RTRIM(ad.city) + ', ' + st.state_name + ' (' + RTRIM(s.site_name) + ')' Site_Name
                 ,uv.vendor_name Utility_Name
                 ,sv.vendor_name Supplier_Name
                 ,ISNULL(cont.account_number, 'Not Yet Assigned') Account_Number
                 ,cont.Account_Id
                 ,un.FIRST_NAME + SPACE(1) + un.LAST_NAME Managed_By
                 ,cont.Total_Rows
            FROM
                  Cte_Contracts cont
                  JOIN dbo.vendor AS sv
                        ON sv.vendor_id = cont.Supp_Vendor_Id
                  JOIN dbo.Site AS s
                        ON s.site_id = cont.site_id
                  JOIN dbo.Address ad
                        ON ad.address_id = s.primary_address_id
                  JOIN dbo.State st
                        ON st.state_id = ad.state_id
                  JOIN dbo.Client AS cl
                        ON cl.client_id = s.client_id
                  JOIN dbo.Vendor AS uv
                        ON uv.vendor_id = cont.vendor_id
                  JOIN dbo.Utility_Detail ud
                        ON ud.vendor_id = uv.Vendor_id
                  JOIN dbo.Utility_Detail_Analyst_Map UDAM
                        ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                  JOIN dbo.GROUP_INFO GI
                        ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID
                  INNER JOIN @Group_Legacy_Name gil
                        ON GI.GROUP_INFO_ID = gil.GROUP_INFO_ID
                  JOIN dbo.USER_INFO un
                        ON un.user_info_id = udam.Analyst_ID
            WHERE
                  cont.Row_Num BETWEEN @Start_Index AND @End_Index

END
;
GO


GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtl_Sel_By_Contract_Id] TO [CBMSApplication]
GO
