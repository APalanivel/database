SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME:        
 cbms_prod.dbo.METERS_FOR_CONTRACT_Sel         
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
@contract_id	INT
@meter_id		VARCHAR(MAX) 
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
	EXEC METERS_FOR_CONTRACT_Sel @contract_id = 12048,@meter_id = '183,321,953'
	EXEC METERS_FOR_CONTRACT_Sel @contract_id = 79531,@meter_id = '47211,47212,47213,48939,50834,98580,98589'

        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
SKA		Shobhit Kumar Agrawal

MODIFICATIONS                
 Initials  Date   Modification        
------------------------------------------------------------        
SKA		06/03/2009  Created    
SKA		06/09/2010	Used cast feature for Segments column of ufn_Split as comparing with integer value
  
 
******/     

CREATE PROCEDURE [dbo].[METERS_FOR_CONTRACT_Sel]
      @contract_id INT
     ,@meter_id VARCHAR(MAX)
AS 
BEGIN
      SET NOCOUNT ON ;    
	
      SELECT
            samm.ACCOUNT_ID
           ,samm.METER_ID
           ,samm.METER_ASSOCIATION_DATE
           ,samm.METER_DISASSOCIATION_DATE
      FROM
            SUPPLIER_ACCOUNT_METER_MAP samm
            INNER JOIN ufn_Split(@meter_id, ',') segm ON CAST(segm.Segments AS INT) = samm.METER_ID
      WHERE
            samm.Contract_ID = @contract_id
            		
END


GO
GRANT EXECUTE ON  [dbo].[METERS_FOR_CONTRACT_Sel] TO [CBMSApplication]
GO
