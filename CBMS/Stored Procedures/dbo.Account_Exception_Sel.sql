SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Exception_Sel       
              
Description:              
			This sproc to get the exception  details for a Given id's.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Queue_Id							INT					NULL
    @Exception_Type						INT					NULL
    @Account_Number						VARCHAR(50)			NULL
    @Site_Name							INT					NULL
    @City								VARCHAR(200)		NULL
    @State_Id							INT					NULL
    @Vendor_Name						INT					NULL
    @Sort_Column_Name					VARCHAR(255)		'Date_In_Queue DESC'
    @Start_Index						INT					1                        
    @End_Index							INT					2147483647  
    @Total_Count						INT					0     
    @Country_Id							INT					NULL                  
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   EXEC dbo.Account_Exception_Sel 

   EXEC Account_Exception_Sel_1
   
   EXEC dbo.Account_Exception_Sel @Sort_Column_Name='Client_Name asc'
   
   EXEC dbo.Account_Exception_Sel @Start_Index=1,@End_Index=3

	exec Account_Exception_Sel 29507
	exec Account_Exception_Sel 70560,102298
	EXEC Account_Exception_Sel 114196
	SELECT * FRoM USER_INFO where Queue_Id=29507 

	SELECT * FROM ACCOUNT_EXCEPTION ORDER BY ACCOUNT_EXCEPTION_ID desc
	exec Account_Exception_Sel 49 

	EXEC Account_Exception_Sel 25503

	SELECT * FROM user_info WHERE username ='pkancherla'
	EXEC Account_Exception_Sel 114196
	EXEC cbmsQueueItem_GetCount 115427

	Select * from user_info where queue_id=71929

	EXEC Account_Exception_Sel 71929

	SET Statistics IO ON 
	EXEC Account_Exception_Sel 9315
	EXEC Account_Exception_Sel1 @Queue_Id = 9315, @Cu_invoice_Id = 93946164

	EXEC Account_Exception_Sel1 @Queue_Id = 60181
	EXEC Account_Exception_Sel1 @Queue_Id = 52958



Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
	RKV				Ravi Kumar Vegesna 
	SP				Sandeep Pigilam              
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.
    RKV             2015-09-04      Modified the join filter on Client_Hier_Account as part of AS400-PII
    SP				2016-12-08		Invoice tracking replaced state_name with state_id and added country_id as optional.
								     Made meter and commodity multiple for invoice collection exception
								     ADDED @Is_Missing_IC_Included INT = NULL
    SP				2017-05-17		 small Enhancement,SE2017-124 added filters	@Country_Id INT = NULL,@Commodity_Id INT = NULL,@Supplier_Name VARCHAR(500) = NULL																
	RKV				2018-10-10      Maint-7824,for the column Date_In_Queue Modified the code to get the exception date for all the exception types.
	RKV				2018-11-16		Maint-7992,removed filter on Invoice_Collection_Account_Config of table Invoice_Collection_Account_Contact in CTE and added in on clause.  
    NR				2019-08-08		Add Contract - Added Contract level Exception.  
	HG				2019-11-25		MAINT-9579 , @Commodity filter applied on Contract level exception.      
	NR				2020-02-21		MAINT-9885 - Removed left joins and added temp tables.
******/
CREATE PROCEDURE [dbo].[Account_Exception_Sel]
(
	@Queue_Id				  INT
	, @Exception_Type		  INT		   = NULL
	, @Account_Number		  VARCHAR(500) = NULL
	, @Client_Name			  VARCHAR(200) = NULL
	, @Site_Name			  VARCHAR(200) = NULL
	, @City					  VARCHAR(200) = NULL
	, @State_Id				  INT		   = NULL
	, @Vendor_Name			  VARCHAR(200) = NULL
	, @Sort_Column_Name		  VARCHAR(255) = 'Date_In_Queue ASC'
	, @Start_Index			  INT		   = 1
	, @End_Index			  INT		   = 2147483647
	, @Total_Count			  INT		   = 0
	, @Country_Id			  INT		   = NULL
	, @Is_Missing_IC_Included INT		   = NULL
	, @Commodity_Id			  INT		   = NULL
	, @Supplier_Name		  VARCHAR(500) = NULL
	, @Cu_Invoice_Id		  INT		   = NULL
	, @Exception_Status_Cd	  INT		   = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	CREATE TABLE #Account_Exception
	(
		Account_Exception_Id					INT
		, Account_Id							INT
		, Meter_Number							NVARCHAR(MAX)
		, Date_In_Queue							DATETIME
		, Exception_Type						VARCHAR(255)
		, Commodity_Name						NVARCHAR(MAX)
		, Client_Name							NVARCHAR(MAX)
		, Site_Name								NVARCHAR(MAX)
		, Account_Number						NVARCHAR(MAX)
		, Country_Name							NVARCHAR(MAX)
		, Account_Vendor_Name					NVARCHAR(MAX)
		, Supplier_Vendor_Name					NVARCHAR(MAX)
		, Site_Id								INT
		, State_Id								INT
		, State_Name							VARCHAR(255)
		, Client_Id								INT
		, Contract_Id							INT
		, Contract_Exception_Id					INT
		, Exception_Status						VARCHAR(25)
		, Cu_Invoice_Id							INT NULL
		, Cu_Invoice_Standing_Data_Exception_Id INT NULL
	);

	DECLARE @Invoice_SDE TABLE
	(
		Cu_Invoice_Standing_Data_Exception_Id INT		   NOT NULL PRIMARY KEY CLUSTERED
		, Cu_Invoice_Id						  INT		   NOT NULL
		, Client_Name						  VARCHAR(255) NOT NULL
		, Client_Id							  INT
		, Exception_Type					  VARCHAR(255) NOT NULL
		, Exception_Status					  VARCHAR(25)  NOT NULL
		, Date_In_Queue						  DATE		   NOT NULL
	);

	DECLARE
		@Exception_Type_Cd INT
		, @Missing_Contract_Type_Cd INT
		, @Outside_Contract_Term_Contract_Type_Cd INT;

	SELECT
		@Exception_Type_Cd = c.Code_Id
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON c.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Exception Type' AND c.Code_Value = 'Missing IC Data';

	SELECT
		@Missing_Contract_Type_Cd = c.Code_Id
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON c.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Exception Type' AND c.Code_Value = 'Missing Contract';

	SELECT
		@Outside_Contract_Term_Contract_Type_Cd = c.Code_Id
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON c.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'Exception Type' AND c.Code_Value = 'Outside Contract Term';



	DECLARE @Contact_Level_Vendor_Cd INT;

	SELECT
		@Contact_Level_Vendor_Cd = c.Code_Id
	FROM
		dbo.Code c
		INNER JOIN dbo.Codeset cs
			ON c.Codeset_Id = cs.Codeset_Id
	WHERE
		cs.Codeset_Name = 'ContactLevel' AND c.Code_Value = 'Vendor';


	SELECT
		Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
									   scha.Account_Vendor_Name
								  WHEN v.VENDOR_NAME IS NOT NULL THEN
									  v.VENDOR_NAME
								  WHEN icav.VENDOR_NAME IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
									  icav.VENDOR_NAME
								  ELSE
									  ucha.Account_Vendor_Name
							  END
		, Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
										 scha.Account_Type
									WHEN v.VENDOR_NAME IS NOT NULL THEN
										e.ENTITY_NAME
									WHEN icav.VENDOR_NAME IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
										'Supplier'
									ELSE
										ucha.Account_Type
								END
		, ucha.Account_Id
		, Invoice_Collection_Account_Config_Id = MAX(	CASE WHEN scha.Account_Vendor_Name IS NOT NULL
																  AND asbv1.Account_Id IS NOT NULL THEN
																 NULL
															WHEN v.VENDOR_NAME IS NOT NULL THEN
																NULL
															WHEN icav.VENDOR_NAME IS NOT NULL
																 AND asbv1.Account_Id IS NOT NULL THEN
																icc.Invoice_Collection_Account_Config_Id
															ELSE
																NULL
														END
													)
	INTO
		#Vendor_Dtls
	FROM
		Core.Client_Hier_Account ucha
		INNER JOIN Core.Client_Hier ch
			ON ucha.Client_Hier_Id = ch.Client_Hier_Id
		LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config icac
			ON icac.Account_Id = ucha.Account_Id
		LEFT OUTER JOIN Core.Client_Hier_Account scha
			ON ucha.Meter_Id = scha.Meter_Id
			   AND scha.Account_Type = 'Supplier'
			   AND icac.Invoice_Collection_Service_End_Dt
			   BETWEEN scha.Supplier_Account_begin_Dt AND scha.Supplier_Account_End_Dt
		LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
						INNER JOIN dbo.ENTITY e
							ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID AND e.ENTITY_NAME = 'Supplier'
						LEFT OUTER JOIN dbo.VENDOR v
							ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
			ON asbv.Account_Id = ucha.Account_Id
			   AND icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt AND asbv.Billing_End_Dt
		LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
						INNER JOIN dbo.ENTITY e1
							ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID AND e1.ENTITY_NAME = 'Supplier'
						LEFT OUTER JOIN dbo.VENDOR v1
							ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
			ON asbv1.Account_Id = ucha.Account_Id
		LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
						INNER JOIN dbo.Contact_Info ci
							ON ci.Contact_Info_Id = icc.Contact_Info_Id
							   AND icc.Is_Primary = 1
							   AND ci.Contact_Level_Cd = @Contact_Level_Vendor_Cd
						INNER JOIN dbo.Vendor_Contact_Map vcm
							ON ci.Contact_Info_Id = vcm.Contact_Info_Id
						INNER JOIN dbo.VENDOR icav
							ON icav.VENDOR_ID = vcm.VENDOR_ID)
			ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
	WHERE
		ucha.Account_Type = 'Utility'
		AND EXISTS (
					   SELECT
						   1
					   FROM
						   dbo.Account_Exception ae
						   INNER JOIN dbo.Code cde
							   ON ae.Exception_Status_Cd = cde.Code_Id
					   WHERE
						   ae.Account_Id = ucha.Account_Id
						   AND (ae.Queue_Id = @Queue_Id)
						   AND (
								   @Exception_Type IS NULL OR cde.Code_Id = @Exception_Type
							   )
						   AND (
								   (
									   @Exception_Status_Cd IS NULL AND cde.Code_Value IN ( 'New', 'In Progress' )
								   )
								   OR (
										  ae.Exception_Status_Cd = @Exception_Status_Cd
										  AND cde.Code_Value IN ( 'New', 'In Progress', 'Archived' )
									  )
							   )
				   )
	GROUP BY
		CASE WHEN scha.Account_Vendor_Name IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
				 scha.Account_Vendor_Name
			WHEN v.VENDOR_NAME IS NOT NULL THEN
				v.VENDOR_NAME
			WHEN icav.VENDOR_NAME IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
				icav.VENDOR_NAME
			ELSE
				ucha.Account_Vendor_Name
		END
		, CASE WHEN scha.Account_Vendor_Name IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
				   scha.Account_Type
			  WHEN v.VENDOR_NAME IS NOT NULL THEN
				  e.ENTITY_NAME
			  WHEN icav.VENDOR_NAME IS NOT NULL AND asbv1.Account_Id IS NOT NULL THEN
				  'Supplier'
			  ELSE
				  ucha.Account_Type
		  END
		, ucha.Account_Id;


	SELECT
		CASE WHEN ae.Meter_Id = -1 AND @Exception_Type_Cd = ae.Exception_Type_Cd THEN
				 'Multiple'
			WHEN ae.Meter_Id = -1 AND @Exception_Type_Cd <> ae.Exception_Type_Cd THEN
				''
			WHEN (
					 @Missing_Contract_Type_Cd = ae.Exception_Type_Cd
					 OR @Outside_Contract_Term_Contract_Type_Cd = ae.Exception_Type_Cd
				 )
				 AND COUNT(DISTINCT Meter_Cnt.Meter_Id) > 1 THEN
				'Multiple'
			ELSE
				cha1.Meter_Number
		END Meter_Number
		, cha1.Display_Account_Number
		, cha1.Meter_Country_Name
		, cha1.Account_Vendor_Name
		, ae.Account_Exception_Id
		, ae.Account_Id
		, ae.Exception_Created_Ts AS Exception_Created_Ts
		, ae.Exception_Type_Cd
		, ae.Exception_Status_Cd
		, MAX(cha1.Client_Hier_Id) AS Client_Hier_Id
		, (CASE WHEN @Exception_Type_Cd = ae.Exception_Type_Cd THEN
					ae.Commodity_Id
			   ELSE
				   cha1.Commodity_Id
		   END
		  ) Commodity_Id
		, ae.Queue_Id
		, ae.Commodity_Id AS Exception_Commodity_Id
		, ic.Invoice_Collection_Account_Config_Id
		, cha1.Account_Type
		, ch.Site_Id
		, ch.State_Id
		, ch.State_Name
		, ch.Client_Id
		, ae.Contract_Id
		, ch.Site_name
		, ch.Client_Name
	INTO
		#CHA_Dtls
	FROM
		Core.Client_Hier ch
		INNER JOIN Core.Client_Hier_Account cha1
			ON ch.Client_Hier_Id = cha1.Client_Hier_Id
		INNER JOIN dbo.Account_Exception ae
			ON ae.Account_Id = cha1.Account_Id
			   AND (
					   ae.Commodity_Id = cha1.Commodity_Id OR ae.Commodity_Id = -1
				   )
			   AND (
					   ae.Meter_Id = cha1.Meter_Id OR ae.Meter_Id = -1
				   )
		INNER JOIN dbo.Code cde
			ON cde.Code_Id = ae.Exception_Status_Cd
		INNER JOIN Core.Client_Hier_Account Meter_Cnt
			ON Meter_Cnt.Account_Id = ae.Account_Id
		LEFT JOIN(
					 SELECT
						 ic.Account_Id
						 , MAX(ic.Created_Ts) AS IC_Created_Ts
						 , MAX(ic.Invoice_Collection_Account_Config_Id) AS Invoice_Collection_Account_Config_Id
					 FROM
						 dbo.Invoice_Collection_Account_Config ic
						 INNER JOIN dbo.USER_INFO ui
							 ON ic.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID
					 WHERE
						 ui.QUEUE_ID = @Queue_Id
					 GROUP BY
						 ic.Account_Id
				 ) ic
			ON ic.Account_Id = cha1.Account_Id AND ae.Exception_Type_Cd = @Exception_Type_Cd
	WHERE
		(
			@Is_Missing_IC_Included IS NULL
			OR (
				   @Is_Missing_IC_Included = 1 AND ae.Exception_Type_Cd = @Exception_Type_Cd
			   )
			OR (
				   @Is_Missing_IC_Included = 0 AND ae.Exception_Type_Cd <> @Exception_Type_Cd
			   )
		)
		AND (ae.Queue_Id = @Queue_Id)
		AND (
				@Exception_Type IS NULL OR ae.Exception_Type_Cd = @Exception_Type
			)
		AND (
				(
					@Exception_Status_Cd IS NULL AND cde.Code_Value IN ( 'New', 'In Progress' )
				)
				OR (
					   ae.Exception_Status_Cd = @Exception_Status_Cd
					   AND cde.Code_Value IN ( 'New', 'In Progress', 'Archived' )
				   )
			)
	GROUP BY
		ae.Meter_Id
		, cha1.Meter_Number
		, cha1.Display_Account_Number
		, cha1.Meter_Country_Name
		, cha1.Account_Vendor_Name
		, ae.Account_Exception_Id
		, ae.Account_Id
		, ae.Exception_Created_Ts
		, ae.Exception_Type_Cd
		, ae.Exception_Status_Cd
		, (CASE WHEN @Exception_Type_Cd = ae.Exception_Type_Cd THEN
					ae.Commodity_Id
			   ELSE
				   cha1.Commodity_Id
		   END
		  )
		, ae.Queue_Id
		, ae.Commodity_Id
		, ic.Invoice_Collection_Account_Config_Id
		, cha1.Account_Type
		, ch.Site_Id
		, ch.State_Id
		, ch.State_Name
		, ch.Client_Id
		, ae.Contract_Id
		, ch.Site_name
		, ch.Client_Name;

	INSERT INTO #Account_Exception
		(
			Account_Exception_Id
			, Account_Id
			, Meter_Number
			, Date_In_Queue
			, Exception_Type
			, Commodity_Name
			, Client_Name
			, Site_Name
			, Account_Number
			, Country_Name
			, Account_Vendor_Name
			, Supplier_Vendor_Name
			, Site_Id
			, State_Id
			, State_Name
			, Client_Id
			, Contract_Id
			, Contract_Exception_Id
			, Exception_Status
		)
	SELECT
		cha.Account_Exception_Id
		, cha.Account_Id
		, cha.Meter_Number
		, cha.Exception_Created_Ts AS Date_In_Queue
		, TypeCd.Code_Dsc AS Exception_Type
		, CASE WHEN c.Commodity_Name = 'Not Applicable' AND @Exception_Type_Cd = cha.Exception_Type_Cd THEN
				   'Multiple'
			  ELSE
				  c.Commodity_Name
		  END AS Commodity_Name
		, cha.Client_Name
		, CASE WHEN COUNT(DISTINCT cha.Site_name) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(cha.Site_name)
		  END AS Site_Name
		, cha.Display_Account_Number AS Account_Number
		, cha.Meter_Country_Name AS Country_Name
		, CASE WHEN vd.Account_Vendor_Type = 'Utility' THEN
				   vd.Account_Vendor_Name
			  WHEN cha.Account_Type = 'Utility' THEN
				  cha.Account_Vendor_Name
		  END Account_Vendor_Name
		, CASE WHEN vd.Account_Vendor_Type = 'Supplier' THEN
				   vd.Account_Vendor_Name
			  WHEN cha.Account_Type = 'Supplier' THEN
				  cha.Account_Vendor_Name
		  END AS Supplier_Vendor_Name
		, cha.Site_Id
		, cha.State_Id
		, cha.State_Name
		, cha.Client_Id
		, cha.Contract_Id
		, -1 AS Contract_Exception_Id
		, StatusCd.Code_Value AS Exception_Status
	FROM
		#CHA_Dtls cha
		LEFT OUTER JOIN #Vendor_Dtls vd
			ON vd.Account_Id = cha.Account_Id
			   AND vd.Invoice_Collection_Account_Config_Id = cha.Invoice_Collection_Account_Config_Id
		INNER JOIN dbo.Code TypeCd
			ON TypeCd.Code_Id = cha.Exception_Type_Cd
		INNER JOIN dbo.Commodity c
			ON c.Commodity_Id = (CASE WHEN @Exception_Type_Cd = cha.Exception_Type_Cd THEN
										  cha.Exception_Commodity_Id
									 ELSE
										 cha.Commodity_Id
								 END
								)
		INNER JOIN dbo.Code StatusCd
			ON StatusCd.Code_Id = cha.Exception_Status_Cd
	WHERE
		(cha.Queue_Id = @Queue_Id)
		AND (
				@Exception_Type IS NULL OR TypeCd.Code_Id = @Exception_Type
			)
		AND (
				@Account_Number IS NULL OR cha.Display_Account_Number LIKE '%' + @Account_Number + '%'
			)
		AND (
				@Vendor_Name IS NULL
				OR (
					   vd.Account_Vendor_Name LIKE '%' + @Vendor_Name + '%' AND vd.Account_Vendor_Type = 'Utility'
				   )
				OR (
					   cha.Account_Vendor_Name LIKE '%' + @Vendor_Name + '%' AND cha.Account_Type = 'Utility'
				   )
			)
		AND (
				@Supplier_Name IS NULL
				OR (
					   vd.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%' AND vd.Account_Vendor_Type = 'Supplier'
				   )
				OR (
					   cha.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%' AND cha.Account_Type = 'Supplier'
				   )
			)
		AND EXISTS (
					   SELECT
						   1
					   FROM
						   Core.Client_Hier ch
						   INNER JOIN Core.Client_Hier_Account cha1
							   ON ch.Client_Hier_Id = cha1.Client_Hier_Id
					   WHERE
						   cha.Account_Id = cha1.Account_Id
						   AND (
								   @Client_Name IS NULL OR ch.Client_Name LIKE '%' + @Client_Name + '%'
							   )
						   AND (
								   @Site_Name IS NULL OR ch.Site_name LIKE '%' + @Site_Name + '%'
							   )
						   AND (
								   @City IS NULL OR ch.City LIKE '%' + @City + '%'
							   )
						   AND (
								   @State_Id IS NULL OR ch.State_Id = @State_Id
							   )
						   AND (
								   @Country_Id IS NULL OR ch.Country_Id = @Country_Id
							   )
						   AND (
								   @Commodity_Id IS NULL OR (CASE WHEN @Exception_Type_Cd = cha.Exception_Type_Cd THEN
																	  cha.Exception_Commodity_Id
																 ELSE
																	 cha.Commodity_Id
															 END
															) = @Commodity_Id
							   )
				   )
		AND (
				@Cu_Invoice_Id IS NULL
				OR EXISTS (
							  SELECT
								  1
							  FROM
								  dbo.Account_Exception_Cu_Invoice_Map aecim
							  WHERE
								  aecim.Cu_Invoice_Id = @Cu_Invoice_Id
								  AND aecim.Account_Exception_Id = cha.Account_Exception_Id
						  )
			)
		AND (
				(
					@Exception_Status_Cd IS NULL AND StatusCd.Code_Value IN ( 'New', 'In Progress' )
				)
				OR (
					   cha.Exception_Status_Cd = @Exception_Status_Cd
					   AND StatusCd.Code_Value IN ( 'New', 'In Progress', 'Archived' )
				   )
			)
	GROUP BY
		cha.Account_Exception_Id
		, cha.Account_Id
		, cha.Meter_Number
		, cha.Exception_Created_Ts
		, TypeCd.Code_Dsc
		, CASE WHEN c.Commodity_Name = 'Not Applicable' AND @Exception_Type_Cd = cha.Exception_Type_Cd THEN
				   'Multiple'
			  ELSE
				  c.Commodity_Name
		  END
		, cha.Client_Name
		, cha.Display_Account_Number
		, cha.Meter_Country_Name
		, CASE WHEN vd.Account_Vendor_Type = 'Utility' THEN
				   vd.Account_Vendor_Name
			  WHEN cha.Account_Type = 'Utility' THEN
				  cha.Account_Vendor_Name
		  END
		, CASE WHEN vd.Account_Vendor_Type = 'Supplier' THEN
				   vd.Account_Vendor_Name
			  WHEN cha.Account_Type = 'Supplier' THEN
				  cha.Account_Vendor_Name
		  END
		, cha.Site_Id
		, cha.State_Id
		, cha.State_Name
		, cha.Client_Id
		, cha.Contract_Id
		, StatusCd.Code_Value;

	INSERT INTO #Account_Exception
		(
			Account_Exception_Id
			, Account_Id
			, Meter_Number
			, Date_In_Queue
			, Exception_Type
			, Commodity_Name
			, Client_Name
			, Site_Name
			, Account_Number
			, Country_Name
			, Account_Vendor_Name
			, Supplier_Vendor_Name
			, Site_Id
			, State_Id
			, State_Name
			, Client_Id
			, Contract_Id
			, Contract_Exception_Id
			, Exception_Status
		)
	SELECT
		-1 AS Account_Exception_Id
		, -1 AS Account_Id
		, CASE WHEN COUNT(DISTINCT sup_Acc.Meter_Id) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(sup_Acc.Meter_Number)
		  END
		, ae.Created_Ts
		, TypeCd.Code_Dsc
		, c.Commodity_Name
		, ch.Client_Name
		, CASE WHEN COUNT(DISTINCT ch.Site_name) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(ch.Site_name)
		  END AS Site_Name
		, con.ED_CONTRACT_NUMBER
		, sup_Acc.Meter_Country_Name AS Country_Name
		, CASE WHEN COUNT(DISTINCT uti_cha.Account_Vendor_Name) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(uti_cha.Account_Vendor_Name)
		  END
		, CASE WHEN COUNT(DISTINCT sup_Acc.Account_Vendor_Name) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(sup_Acc.Account_Vendor_Name)
		  END
		, CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1 ELSE MAX(ch.Site_Id)END
		, CASE WHEN COUNT(DISTINCT ch.State_Id) > 1 THEN -1 ELSE MAX(ch.State_Id)END
		, CASE WHEN COUNT(DISTINCT ch.State_Id) > 1 THEN
				   'Multiple'
			  ELSE
				  MAX(ch.State_Name)
		  END
		, ch.Client_Id
		, ae.Contract_Id
		, ae.Contract_Exception_Id
		, StatusCd.Code_Value AS Exception_Status
	FROM
		Core.Client_Hier ch
		INNER JOIN Core.Client_Hier_Account uti_cha
			ON uti_cha.Client_Hier_Id = ch.Client_Hier_Id
		INNER JOIN Core.Client_Hier_Account sup_Acc
			ON uti_cha.Meter_Id = sup_Acc.Meter_Id AND uti_cha.Client_Hier_Id = sup_Acc.Client_Hier_Id
		INNER JOIN dbo.Contract_Exception ae
			ON ae.Contract_Id = sup_Acc.Supplier_Contract_ID
		INNER JOIN dbo.CONTRACT con
			ON con.CONTRACT_ID = ae.Contract_Id
		INNER JOIN dbo.Code TypeCd
			ON TypeCd.Code_Id = ae.Exception_Type_Cd
		INNER JOIN dbo.Commodity c
			ON c.Commodity_Id = con.COMMODITY_TYPE_ID
		INNER JOIN dbo.Code StatusCd
			ON StatusCd.Code_Id = ae.Exception_Status_Cd
	WHERE
		ae.Queue_Id = @Queue_Id
		AND (
				@Exception_Type IS NULL OR TypeCd.Code_Id = @Exception_Type
			)
		AND uti_cha.Account_Type = 'Utility'
		AND sup_Acc.Account_Type = 'Supplier'
		AND (
				@Commodity_Id IS NULL OR c.Commodity_Id = @Commodity_Id
			)
		AND (
				@Client_Name IS NULL OR ch.Client_Name LIKE '%' + @Client_Name + '%'
			)
		AND (
				@Site_Name IS NULL OR ch.Site_name LIKE '%' + @Site_Name + '%'
			)
		AND (
				@City IS NULL OR ch.City LIKE '%' + @City + '%'
			)
		AND (
				@State_Id IS NULL OR ch.State_Id = @State_Id
			)
		AND (
				@Country_Id IS NULL OR ch.Country_Id = @Country_Id
			)
		AND (
				@Vendor_Name IS NULL
				OR (
					   uti_cha.Account_Vendor_Name LIKE '%' + @Vendor_Name + '%' AND uti_cha.Account_Type = 'Utility'
				   )
			)
		AND (
				@Supplier_Name IS NULL
				OR (
					   sup_Acc.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%'
					   AND sup_Acc.Account_Type = 'Supplier'
				   )
			)
		AND (
				(
					@Exception_Status_Cd IS NULL AND StatusCd.Code_Value IN ( 'New', 'In Progress' )
				)
				OR (
					   ae.Exception_Status_Cd = @Exception_Status_Cd
					   AND StatusCd.Code_Value IN ( 'New', 'In Progress', 'Archived' )
				   )
			)
	GROUP BY
		ae.Contract_Exception_Id
		, ae.Created_Ts
		, c.Commodity_Name
		, con.ED_CONTRACT_NUMBER
		, sup_Acc.Meter_Country_Name
		, TypeCd.Code_Dsc
		, ch.Client_Id
		, ae.Contract_Id
		, ae.Contract_Exception_Id
		, StatusCd.Code_Value
		, ch.Client_Name;


	INSERT INTO @Invoice_SDE
		(
			Cu_Invoice_Standing_Data_Exception_Id
			, Cu_Invoice_Id
			, Client_Name
			, Client_Id
			, Exception_Type
			, Exception_Status
			, Date_In_Queue
		)
	SELECT
		sde.Cu_Invoice_Standing_Data_Exception_Id
		, sde.Cu_Invoice_Id
		, ch.Client_Name
		, ch.Client_Id
		, extyp.Code_Dsc AS Exception_Type
		, StatusCd.Code_Value AS Exception_Status
		, sde.Last_Change_Ts
	FROM
		dbo.Cu_Invoice_Standing_Data_Exception sde
		INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl sded
			ON sded.Cu_Invoice_Standing_Data_Exception_Id = sde.Cu_Invoice_Standing_Data_Exception_Id
		INNER JOIN dbo.Code extyp
			ON extyp.Code_Id = sde.Exception_Type_Cd
		INNER JOIN dbo.Code StatusCd
			ON StatusCd.Code_Id = sde.Exception_Status_Cd
		INNER JOIN Core.Client_Hier_Account cha
			ON cha.Account_Id = sded.Account_Id AND cha.Commodity_Id = sded.Commodity_Id
		INNER JOIN Core.Client_Hier ch
			ON ch.Client_Hier_Id = cha.Client_Hier_Id
	WHERE
		sde.Queue_Id = @Queue_Id
		AND (
				@Cu_Invoice_Id IS NULL OR sde.Cu_Invoice_Id = @Cu_Invoice_Id
			)
		AND (
				@Exception_Type IS NULL OR extyp.Code_Id = @Exception_Type
			)
		AND (
				@Commodity_Id IS NULL OR sded.Commodity_Id = @Commodity_Id
			)
		AND (
				@Client_Name IS NULL OR ch.Client_Name LIKE '%' + @Client_Name + '%'
			)
		AND (
				@Site_Name IS NULL OR ch.Site_name LIKE '%' + @Site_Name + '%'
			)
		AND (
				@City IS NULL OR ch.City LIKE '%' + @City + '%'
			)
		AND (
				@Account_Number IS NULL OR cha.Display_Account_Number LIKE '%' + @Account_Number + '%'
			)
		AND (
				@State_Id IS NULL OR ch.State_Id = @State_Id
			)
		AND (
				@Country_Id IS NULL OR ch.Country_Id = @Country_Id
			)
		AND (
				@Vendor_Name IS NULL
				OR (
					   cha.Account_Vendor_Name LIKE '%' + @Vendor_Name + '%' AND cha.Account_Type = 'Utility'
				   )
			)
		AND (
				@Supplier_Name IS NULL
				OR (
					   cha.Account_Vendor_Name LIKE '%' + @Supplier_Name + '%' AND cha.Account_Type = 'Supplier'
				   )
			)
		AND (
				(
					@Exception_Status_Cd IS NULL AND StatusCd.Code_Value IN ( 'New', 'In Progress' )
				)
				OR (
					   sde.Exception_Status_Cd = @Exception_Status_Cd
					   AND StatusCd.Code_Value IN ( 'New', 'In Progress', 'Archived' )
				   )
			)
	GROUP BY
		sde.Cu_Invoice_Standing_Data_Exception_Id
		, sde.Cu_Invoice_Id
		, ch.Client_Name
		, ch.Client_Id
		, extyp.Code_Dsc
		, StatusCd.Code_Value
		, sde.Last_Change_Ts
		, sded.Account_Id;

	INSERT INTO #Account_Exception
		(
			Account_Exception_Id
			, Date_In_Queue
			, Exception_Type
			, Client_Name
			, Client_Id
			, Account_Id
			, Account_Number
			, Exception_Status
			, Cu_Invoice_Id
			, Cu_Invoice_Standing_Data_Exception_Id
		)
	SELECT
		-1
		, isde.Date_In_Queue
		, isde.Exception_Type
		, isde.Client_Name
		, isde.Client_Id
		, -1
		, isde.Cu_Invoice_Id
		, isde.Exception_Status
		, isde.Cu_Invoice_Id
		, isde.Cu_Invoice_Standing_Data_Exception_Id
	FROM
		@Invoice_SDE isde;

	UPDATE
		ae
	SET
		ae.Site_Name = CASE WHEN x.Site_Cnt > 1 THEN 'Multiple' ELSE x.Site_Name END
		, ae.Site_Id = x.Site_Id
		, ae.Account_Vendor_Name = x.Account_Vendor_Name
		, ae.Supplier_Vendor_Name = x.Supplier
		, ae.Country_Name = x.Country_Name
		, ae.State_Id = x.State_Id
		, ae.State_Name = CASE WHEN x.State_Cnt > 1 THEN 'Multiple' ELSE x.State_Name END
		, ae.Commodity_Name = CASE WHEN x.Commodity_Cnt > 1 THEN 'Multiple' ELSE x.Commodity_Name END
		, ae.Meter_Number = CASE WHEN x.Meter_Cnt > 1 THEN 'Multiple' ELSE x.Meter_Number END
	FROM
		#Account_Exception ae
		CROSS APPLY(
					   SELECT
						   MIN(ch.Site_name) AS Site_Name
						   , MIN(ch.Site_Id) AS Site_Id
						   , COUNT(DISTINCT ch.Client_Hier_Id) AS Site_Cnt
						   , MIN(ch.State_Id) AS State_Id
						   , MIN(ch.State_Name) AS State_Name
						   , COUNT(DISTINCT ch.State_Id) AS State_Cnt
						   , MIN(cha.Meter_Number) AS Meter_Number
						   , COUNT(DISTINCT cha.Meter_Id) AS Meter_Cnt
						   , MIN(CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name END) AS Account_Vendor_Name
						   , MIN(CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name END) AS Supplier
						   , MIN(ch.Country_Name) AS Country_Name
						   , MIN(com.Commodity_Name) AS Commodity_Name
						   , MIN(cha.Account_Number) AS Account_Number
						   , COUNT(DISTINCT com.Commodity_Id) AS Commodity_Cnt
					   FROM
						   Core.Client_Hier ch
						   INNER JOIN Core.Client_Hier_Account cha
							   ON cha.Client_Hier_Id = ch.Client_Hier_Id
						   INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl ed
							   ON ed.Account_Id = cha.Account_Id AND ed.Commodity_Id = cha.Commodity_Id
						   INNER JOIN dbo.Commodity com
							   ON com.Commodity_Id = ed.Commodity_Id
					   WHERE
						   ed.Cu_Invoice_Standing_Data_Exception_Id = ae.Cu_Invoice_Standing_Data_Exception_Id
				   ) x
	WHERE
		ae.Cu_Invoice_Standing_Data_Exception_Id IS NOT NULL;

	IF @Total_Count = 0 BEGIN
SELECT @Total_Count = COUNT(1) OVER ()FROM #Account_Exception;
	END;

	WITH cte_Exception_List
	AS (   SELECT
			   ae.Account_Exception_Id
			   , ae.Account_Id
			   , ae.Meter_Number
			   , ae.Date_In_Queue
			   , ae.Exception_Type
			   , ae.Commodity_Name
			   , ae.Client_Name
			   , ae.Site_Name
			   , ae.Account_Number
			   , ae.Country_Name
			   , ae.Account_Vendor_Name
			   , ae.Supplier_Vendor_Name
			   , ae.Site_Id
			   , ae.State_Id
			   , ae.State_Name
			   , ae.Client_Id
			   , ae.Contract_Id
			   , ae.Contract_Exception_Id
			   , ae.Exception_Status
			   , ae.Cu_Invoice_Standing_Data_Exception_Id
			   , ae.Cu_Invoice_Id
			   , ROW_NUMBER() OVER (ORDER BY(CASE WHEN @Sort_Column_Name = 'Exception_Type ASC' THEN
													  ae.Exception_Type
												 WHEN @Sort_Column_Name = 'Commodity_Name ASC' THEN
													 ae.Commodity_Name
												 WHEN @Sort_Column_Name = 'Client_Name ASC' THEN
													 ae.Client_Name
												 WHEN @Sort_Column_Name = 'Site_name ASC' THEN
													 ae.Site_Name
												 WHEN @Sort_Column_Name = 'Country_Name ASC' THEN
													 ae.Country_Name
												 WHEN @Sort_Column_Name = 'Account_Vendor_Name ASC' THEN
													 ae.Account_Vendor_Name
											 END
											) ASC
											, (CASE WHEN @Sort_Column_Name = 'Exception_Type DESC' THEN
														ae.Exception_Type
												   WHEN @Sort_Column_Name = 'Commodity_Name DESC' THEN
													   ae.Commodity_Name
												   WHEN @Sort_Column_Name = 'Client_Name DESC' THEN
													   ae.Client_Name
												   WHEN @Sort_Column_Name = 'Site_name DESC' THEN
													   ae.Site_Name
												   WHEN @Sort_Column_Name = 'Country_Name DESC' THEN
													   ae.Country_Name
												   WHEN @Sort_Column_Name = 'Account_Vendor_Name DESC' THEN
													   ae.Account_Vendor_Name
											   END
											  ) DESC
											, (CASE WHEN @Sort_Column_Name = 'Date_In_Queue ASC' THEN ae.Date_In_Queue END) ASC
											, (CASE WHEN @Sort_Column_Name = 'Date_In_Queue DESC' THEN ae.Date_In_Queue END) DESC
								   ) AS Row_Num
		   FROM
			   #Account_Exception ae)
	SELECT
		ce.Account_Exception_Id
		, ce.Account_Id
		, ce.Meter_Number
		, ce.Date_In_Queue
		, CASE WHEN ce.Exception_Type = 'Missing Contract' THEN
				   ce.Exception_Type + ' - ' + ce.Exception_Status
			  ELSE
				  ce.Exception_Type
		  END AS Exception_Type
		, ce.Commodity_Name
		, ce.Client_Name
		, ce.Site_Name
		, ce.Account_Number
		, ce.Country_Name
		, ce.Account_Vendor_Name AS Utility_Vendor_Name
		, @Total_Count AS Total_Rows
		, ce.Row_Num
		, Supplier_Vendor_Name
		, ce.Site_Id
		, ce.State_Id
		, ce.State_Name
		, ce.Client_Id
		, ce.Contract_Id
		, ce.Contract_Exception_Id
		, ce.Cu_Invoice_Id
		, ce.Cu_Invoice_Standing_Data_Exception_Id
	FROM
		cte_Exception_List ce
	WHERE
		ce.Row_Num BETWEEN @Start_Index AND @End_Index
	ORDER BY
		Row_Num;

	DROP TABLE #Account_Exception;
	DROP TABLE #Vendor_Dtls;
	DROP TABLE #CHA_Dtls;

END;
GO



GRANT EXECUTE ON  [dbo].[Account_Exception_Sel] TO [CBMSApplication]
GO
