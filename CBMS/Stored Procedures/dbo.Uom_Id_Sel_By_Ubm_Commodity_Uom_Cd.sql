SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Uom_Id_Sel_By_Ubm_Commodity_Uom_Cd]  
     
DESCRIPTION: 
	To Get UNIT_OF_MEASURE_TYPE_ID from UBM_UNIT_OF_MEASURE_MAP for the given pass UBM_ID, Commodity_Type_Id and UBM_UNIT_OF_MEASURE_CODE
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Ubm_Id			INT
@ubm_unit_of_measure_code		VARCHAR(200)
@commodity_type_id	INT

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  

	EXEC Uom_Id_Sel_By_Ubm_Commodity_Uom_Cd  1,'ccf',67
	EXEC Uom_Id_Sel_By_Ubm_Commodity_Uom_Cd  7,'kWh/m� (n)',291
    
    SELECT
		*
	FROM
		dbo.UBM_UNIT_OF_MEASURE_MAP
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
HG			08/19/2010	Created
SKA			08/31/2010	Renamed the input parameters
*/

CREATE PROCEDURE dbo.Uom_Id_Sel_By_Ubm_Commodity_Uom_Cd
	@Ubm_Id			INT
	,@ubm_unit_of_measure_code	VARCHAR(200)
	,@commodity_type_id	INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT
		UNIT_OF_MEASURE_TYPE_ID
	FROM
		dbo.UBM_UNIT_OF_MEASURE_MAP
	WHERE
		UBM_ID = @Ubm_Id
		AND UBM_UNIT_OF_MEASURE_CODE = @ubm_unit_of_measure_code
		AND COMMODITY_TYPE_ID = @commodity_type_id

END
GO
GRANT EXECUTE ON  [dbo].[Uom_Id_Sel_By_Ubm_Commodity_Uom_Cd] TO [CBMSApplication]
GO
