
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_DELETE_STATUS_FOR_ACCOUNT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@accountId           int,
	@rfp_id              int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- exec SR_RFP_UPDATE_DELETE_STATUS_FOR_ACCOUNT_P 16210 , 499

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR		Deana Ritter
    AKR     Ashok Kumar Raju
    
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
DR     08/04/2009	   Removed Linked Server Updates

 DMR	  09/10/2010 Modified for Quoted_Identifier
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project

******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_DELETE_STATUS_FOR_ACCOUNT_P
      ( 
       @accountId INT
      ,@rfp_id INT )
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            sr_rfp_account
      SET   
            is_deleted = 1
      WHERE
            account_id = @accountId
            AND sr_rfp_id = @rfp_id

	--added by prasad 

      DECLARE
            @projectid INT
           ,@siteid INT
           ,@totalaccountcount INT
           ,@smruploadedaccountcount INT
           ,@approvedacccount INT
           ,@closedacccount INT
           ,@postedcount AS INT		

      SET @siteid = ( SELECT DISTINCT
                        account.site_id
                      FROM
                        sr_rfp_account
                       ,account
                      WHERE
                        sr_rfp_account.account_id = @accountId
                        AND account.account_id = @accountId
                        AND sr_rfp_account.sr_rfp_id = @rfp_id )

      IF ( SELECT
            count(*)
           FROM
            RFP_SITE_PROJECT
           WHERE
            RFP_ID = @rfp_id
            AND SITE_ID = @siteId ) > 0 
            BEGIN
                  SET @projectId = ( SELECT
                                          SSO_PROJECT_ID
                                     FROM
                                          RFP_SITE_PROJECT
                                     WHERE
                                          RFP_ID = @rfp_id
                                          AND SITE_ID = @siteid )

                  SET @totalaccountcount = ( SELECT
                                                count(sr_rfp_account_id)
                                             FROM
                                                sr_rfp_account
                                               ,account
                                             WHERE
                                                sr_rfp_account.sr_rfp_id = @rfp_id
                                                AND sr_rfp_account.account_id = account.account_id
                                                AND account.site_id = @siteId
                                                AND sr_rfp_account.is_deleted = 0 )
                  IF ( @totalaccountcount ) = 0 
                        BEGIN
                              DELETE FROM
                                    SSO_PROJECT_STEP
                              WHERE
                                    SSO_PROJECT_ID = @projectId
                              DELETE FROM
                                    SSO_PROJECT_ACTIVITY
                              WHERE
                                    SSO_PROJECT_ID = @projectId
                              DELETE FROM
                                    SSO_PROJECT_OWNER_MAP
                              WHERE
                                    SSO_PROJECT_ID = @projectId
                              DELETE FROM
                                    RFP_SITE_PROJECT
                              WHERE
                                    SSO_PROJECT_ID = @projectId
                              DELETE FROM
                                    SSO_PROJECT
                              WHERE
                                    SSO_PROJECT_ID = @projectId
                        END	
                  ELSE 
                        BEGIN
--query to select all send to Rfp  accounts for that rfp id and siteid
                              SET @postedcount = 0
                              SELECT
                                    @postedcount = count(sr_rfp_send_supplier_id)
                              FROM
                                    sr_rfp_send_supplier supp
                                    JOIN sr_rfp_account rfpacc
                                          ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                             AND is_bid_group = 0
                                             AND status_type_id = 1090 --for posted
                                             AND rfpacc.sr_rfp_id = @rfp_id
                                             AND rfpacc.is_deleted = 0
                                    JOIN account
                                          ON rfpacc.account_id = account.account_id
                                             AND account.site_id = @siteId
                              SELECT
                                    @postedcount = @postedcount + count(sr_rfp_send_supplier_id)
                              FROM
                                    sr_rfp_send_supplier supp
                                    JOIN sr_rfp_account rfpacc
                                          ON supp.sr_account_group_id = rfpacc.sr_rfp_account_id
                                             AND is_bid_group = 1
                                             AND status_type_id = 1090 --for posted
                                             AND rfpacc.sr_rfp_id = @rfp_id
                                             AND rfpacc.is_deleted = 0
                                    JOIN account
                                          ON rfpacc.account_id = account.account_id
                                             AND account.site_id = @siteId
				--if(@postedcount)>0

--query to select all smr aploaded  accounts for that rfp id and siteid		

                              DECLARE @account AS INT
                              SET @account = 0

                              SELECT
                                    @account = count(DISTINCT sr_rfp_account_id)
                              FROM
                                    sr_rfp_sop_smr
                                   ,sr_rfp_account
                                   ,account
                                   ,cbms_image img
                              WHERE
                                    sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                    AND sr_rfp_account.sr_rfp_id = @rfp_id
                                    AND is_bid_group = 0
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                    AND img.cbms_image_type_id = 1200 --//for smr upload.
                                    AND account.site_id = @siteId

                              SELECT
                                    @account = @account + count(DISTINCT sr_rfp_bid_group_id)
                              FROM
                                    sr_rfp_sop_smr
                                   ,sr_rfp_account
                                   ,account
                                   ,cbms_image img
                              WHERE
                                    sr_rfp_sop_smr.sr_account_group_id = sr_rfp_account.sr_rfp_bid_group_id
                                    AND sr_rfp_account.sr_rfp_id = @rfp_id
                                    AND is_bid_group = 1
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND sr_rfp_sop_smr.cbms_image_id = img.cbms_image_id
                                    AND img.cbms_image_type_id = 1200 --//for smr upload.
                                    AND account.site_id = @siteId

                              SET @smruploadedaccountcount = ( SELECT @account )
						 
--query to select all CLIENT APPROVED accounts for that rfp id and siteid

                              SET @account = 0

                              SELECT
                                    @account = count(DISTINCT sr_rfp_account_id)
                              FROM
                                    sr_rfp_sop_client_approval
                                   ,sr_rfp_account
                                   ,account
                              WHERE
                                    sr_rfp_sop_client_approval.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                    AND sr_rfp_account.sr_rfp_id = @rfp_id
                                    AND is_bid_group = 0
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND account.site_id = @siteId

                              SELECT
                                    @account = @account + count(DISTINCT sr_rfp_bid_group_id)
                              FROM
                                    sr_rfp_sop_client_approval
                                   ,sr_rfp_account
                                   ,account
                              WHERE
                                    sr_rfp_sop_client_approval.sr_account_group_id = sr_rfp_account.sr_rfp_bid_group_id
                                    AND sr_rfp_account.sr_rfp_id = @rfp_id
                                    AND is_bid_group = 1
                                    AND sr_rfp_account.is_deleted = 0
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND account.site_id = @siteId

                              SELECT
                                    @approvedacccount = ( SELECT @account )
						
--query to select all closed accounts for that rfp id and siteid
		               
                              SELECT
                                    @closedacccount = count(sr_rfp_account_id)
                              FROM
                                    sr_rfp_closure
                                   ,sr_rfp_account
                                   ,account
                              WHERE
                                    sr_rfp_closure.sr_account_group_id = sr_rfp_account.sr_rfp_account_id
                                    AND sr_rfp_account.sr_rfp_id = @rfp_id
                                    AND sr_rfp_account.account_id = account.account_id
                                    AND account.site_id = @siteid
                                    AND sr_rfp_account.is_deleted = 0 

                              IF ( @postedcount ) > 0 
                                    BEGIN
                                          IF ( @totalaccountcount = @smruploadedaccountcount ) 
                                                BEGIN
                                                      IF ( @totalaccountcount = @approvedacccount ) 
                                                            BEGIN
                                                                  IF ( @totalaccountcount = @closedacccount ) 
                                                                        BEGIN
                                                                              UPDATE
                                                                                    sso_project_step
                                                                              SET   
                                                                                    is_active = 0
                                                                                   ,is_complete = 1
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND ( step_no = 5
                                                                                          OR step_no = 4
                                                                                          OR step_no = 3 )
								--added for 18499
                                                                              UPDATE
                                                                                    sso_project
                                                                              SET   
                                                                                    project_status_type_id = ( SELECT
                                                                                                                  entity_id
                                                                                                               FROM
                                                                                                                  entity
                                                                                                               WHERE
                                                                                                                  entity_type = 601
                                                                                                                  AND entity_name = 'Completed' )
                                                                              WHERE
                                                                                    sso_project_id = @projectid 

                                                                        END
                                                                  ELSE 
                                                                        BEGIN
                                                                              UPDATE
                                                                                    sso_project_step
                                                                              SET   
                                                                                    is_active = 1
                                                                                   ,is_complete = 0
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND step_no = 5
								--added for 18499
                                                                              UPDATE
                                                                                    sso_project
                                                                              SET   
                                                                                    project_status_type_id = ( SELECT
                                                                                                                  entity_id
                                                                                                               FROM
                                                                                                                  entity
                                                                                                               WHERE
                                                                                                                  entity_type = 601
                                                                                                                  AND entity_name = 'Open' )
                                                                              WHERE
                                                                                    sso_project_id = @projectid 

                                                                        END
                                                            END
                                                      ELSE 
                                                            IF ( @approvedacccount > 0 ) 
                                                                  BEGIN
                                                                        UPDATE
                                                                              sso_project_step
                                                                        SET   
                                                                              is_active = 1
                                                                             ,is_complete = 0
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 4
                                                                        IF ( @totalaccountcount = @closedacccount ) 
                                                                              BEGIN
                                                                                    UPDATE
                                                                                          sso_project_step
                                                                                    SET   
                                                                                          is_active = 0
                                                                                         ,is_complete = 1
                                                                                    WHERE
                                                                                          sso_project_id = @projectid
                                                                                          AND step_no = 5
								--added for 18499
                                                                                    UPDATE
                                                                                          sso_project
                                                                                    SET   
                                                                                          project_status_type_id = ( SELECT
                                                                                                                        entity_id
                                                                                                                     FROM
                                                                                                                        entity
                                                                                                                     WHERE
                                                                                                                        entity_type = 601
                                                                                                                        AND entity_name = 'Completed' )
                                                                                    WHERE
                                                                                          sso_project_id = @projectid 

                                                                              END
                                                                        ELSE 
                                                                              BEGIN 
                                                                                    UPDATE
                                                                                          sso_project_step
                                                                                    SET   
                                                                                          is_active = 1
                                                                                         ,is_complete = 0
                                                                                    WHERE
                                                                                          sso_project_id = @projectid
                                                                                          AND step_no = 5
								--added for 18499
                                                                                    UPDATE
                                                                                          sso_project
                                                                                    SET   
                                                                                          project_status_type_id = ( SELECT
                                                                                                                        entity_id
                                                                                                                     FROM
                                                                                                                        entity
                                                                                                                     WHERE
                                                                                                                        entity_type = 601
                                                                                                                        AND entity_name = 'Open' )
                                                                                    WHERE
                                                                                          sso_project_id = @projectid 


                                                                              END
														
                                                                  END
                                                            ELSE 
                                                                  IF ( @closedacccount = 0 ) 
                                                                        BEGIN
                                                                              DELETE FROM
                                                                                    sso_project_step
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND step_no = 5	
                                                                              DELETE FROM
                                                                                    sso_project_activity
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'
							
							--added for 18499
                                                                              UPDATE
                                                                                    sso_project
                                                                              SET   
                                                                                    project_status_type_id = ( SELECT
                                                                                                                  entity_id
                                                                                                               FROM
                                                                                                                  entity
                                                                                                               WHERE
                                                                                                                  entity_type = 601
                                                                                                                  AND entity_name = 'Open' )
                                                                              WHERE
                                                                                    sso_project_id = @projectid 
					
                                                                        END
                                                END
                                          ELSE 
                                                IF ( @smruploadedaccountcount > 0 ) 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 3
                                                      END
                                                ELSE 
                                                      BEGIN
                                                            DELETE FROM
                                                                  sso_project_step
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 3
                                                            DELETE FROM
                                                                  sso_project_activity
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND activity_description = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review'

						
                                                      END
                                    END
                              ELSE 
                                    IF ( @smruploadedaccountcount ) > 0 
                                          BEGIN
                                                IF ( @totalaccountcount = @smruploadedaccountcount ) 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 0
                                                                 ,is_complete = 1
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 3 
					
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4 
					
                                                      END
                                                ELSE 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 3 
                                                            IF ( @approvedacccount ) = 0 
                                                                  BEGIN
                                                                        DELETE FROM
                                                                              sso_project_step
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 4 
                                                                        DELETE FROM
                                                                              sso_project_activity
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND activity_description = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.'

                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        UPDATE
                                                                              sso_project_step
                                                                        SET   
                                                                              is_active = 1
                                                                             ,is_complete = 0
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 4 

                                                                  END
                                                            IF @closedacccount = 0
                                                                  AND @approvedacccount = 0 
                                                                  BEGIN
                                                                        DELETE FROM
                                                                              sso_project_step
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5 
                                                                        DELETE FROM
                                                                              sso_project_activity
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'
							--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Open' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 

                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        UPDATE
                                                                              sso_project_step
                                                                        SET   
                                                                              is_active = 1
                                                                             ,is_complete = 0
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5 
							--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Open' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 

                                                                  END
                                                      END
                                                IF ( @totalaccountcount = @approvedacccount ) 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 0
                                                                 ,is_complete = 1
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4 
					
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 5
						--added for 18499
                                                            UPDATE
                                                                  sso_project
                                                            SET   
                                                                  project_status_type_id = ( SELECT
                                                                                                entity_id
                                                                                             FROM
                                                                                                entity
                                                                                             WHERE
                                                                                                entity_type = 601
                                                                                                AND entity_name = 'Open' )
                                                            WHERE
                                                                  sso_project_id = @projectid 

					
                                                      END
                                                ELSE 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4 
                                                            IF ( @closedacccount ) = 0 
                                                                  BEGIN
                                                                        DELETE FROM
                                                                              sso_project_step
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5 
                                                                        DELETE FROM
                                                                              sso_project_activity
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'

							--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Open' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 

                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        UPDATE
                                                                              sso_project_step
                                                                        SET   
                                                                              is_active = 1
                                                                             ,is_complete = 0
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5 
							--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Open' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 

                                                                  END
                                                      END
                                                IF ( @totalaccountcount = @closedacccount ) 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 0
                                                                 ,is_complete = 1
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 5 
						--added for 18499
                                                            UPDATE
                                                                  sso_project
                                                            SET   
                                                                  project_status_type_id = ( SELECT
                                                                                                entity_id
                                                                                             FROM
                                                                                                entity
                                                                                             WHERE
                                                                                                entity_type = 601
                                                                                                AND entity_name = 'Completed' )
                                                            WHERE
                                                                  sso_project_id = @projectid 

				
                                                      END
                                                ELSE 
                                                      BEGIN
                                                            UPDATE
                                                                  sso_project_step
                                                            SET   
                                                                  is_active = 1
                                                                 ,is_complete = 0
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 5 
						--added for 18499
                                                            UPDATE
                                                                  sso_project
                                                            SET   
                                                                  project_status_type_id = ( SELECT
                                                                                                entity_id
                                                                                             FROM
                                                                                                entity
                                                                                             WHERE
                                                                                                entity_type = 601
                                                                                                AND entity_name = 'Open' )
                                                            WHERE
                                                                  sso_project_id = @projectid 

                                                            IF ( @closedacccount ) = 0 
                                                                  BEGIN
                                                                        DELETE FROM
                                                                              sso_project_step
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5 
                                                                        DELETE FROM
                                                                              sso_project_activity
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'

							--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Open' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 

                                                                  END
                                                      END
                                          END
                                    ELSE 
                                          IF ( @approvedacccount ) > 0 
                                                BEGIN
                                                      DELETE FROM
                                                            sso_project_step
                                                      WHERE
                                                            sso_project_id = @projectid
                                                            AND step_no = 3 
                                                      DELETE FROM
                                                            sso_project_activity
                                                      WHERE
                                                            sso_project_id = @projectid
                                                            AND activity_description = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review'

                                                      IF ( @totalaccountcount = @approvedacccount ) 
                                                            BEGIN
                                                                  UPDATE
                                                                        sso_project_step
                                                                  SET   
                                                                        is_active = 0
                                                                       ,is_complete = 1
                                                                  WHERE
                                                                        sso_project_id = @projectid
                                                                        AND step_no = 4 
					
                                                                  UPDATE
                                                                        sso_project_step
                                                                  SET   
                                                                        is_active = 1
                                                                       ,is_complete = 0
                                                                  WHERE
                                                                        sso_project_id = @projectid
                                                                        AND step_no = 5
						--added for 18499
                                                                  UPDATE
                                                                        sso_project
                                                                  SET   
                                                                        project_status_type_id = ( SELECT
                                                                                                      entity_id
                                                                                                   FROM
                                                                                                      entity
                                                                                                   WHERE
                                                                                                      entity_type = 601
                                                                                                      AND entity_name = 'Open' )
                                                                  WHERE
                                                                        sso_project_id = @projectid 

					
                                                            END
                                                      ELSE 
                                                            BEGIN
                                                                  UPDATE
                                                                        sso_project_step
                                                                  SET   
                                                                        is_active = 1
                                                                       ,is_complete = 0
                                                                  WHERE
                                                                        sso_project_id = @projectid
                                                                        AND step_no = 4 
                                                                  IF ( @closedacccount ) = 0 
                                                                        BEGIN
                                                                              DELETE FROM
                                                                                    sso_project_step
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND step_no = 5 
                                                                              DELETE FROM
                                                                                    sso_project_activity
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'
							--added for 18499
                                                                              UPDATE
                                                                                    sso_project
                                                                              SET   
                                                                                    project_status_type_id = ( SELECT
                                                                                                                  entity_id
                                                                                                               FROM
                                                                                                                  entity
                                                                                                               WHERE
                                                                                                                  entity_type = 601
                                                                                                                  AND entity_name = 'Open' )
                                                                              WHERE
                                                                                    sso_project_id = @projectid 

                                                                        END
                                                                  ELSE 
                                                                        BEGIN
                                                                              UPDATE
                                                                                    sso_project_step
                                                                              SET   
                                                                                    is_active = 1
                                                                                   ,is_complete = 0
                                                                              WHERE
                                                                                    sso_project_id = @projectid
                                                                                    AND step_no = 5 
							--added for 18499
                                                                              UPDATE
                                                                                    sso_project
                                                                              SET   
                                                                                    project_status_type_id = ( SELECT
                                                                                                                  entity_id
                                                                                                               FROM
                                                                                                                  entity
                                                                                                               WHERE
                                                                                                                  entity_type = 601
                                                                                                                  AND entity_name = 'Open' )
                                                                              WHERE
                                                                                    sso_project_id = @projectid 

                                                                        END
                                                            END
                                                END
                                          ELSE 
                                                IF ( @closedacccount ) > 0 
                                                      BEGIN
                                                            DELETE FROM
                                                                  sso_project_step
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4 
                                                            DELETE FROM
                                                                  sso_project_activity
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND activity_description = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.'

                                                            IF ( @totalaccountcount = @closedacccount ) 
                                                                  BEGIN
					
                                                                        UPDATE
                                                                              sso_project_step
                                                                        SET   
                                                                              is_active = 0
                                                                             ,is_complete = 1
                                                                        WHERE
                                                                              sso_project_id = @projectid
                                                                              AND step_no = 5
						--added for 18499
                                                                        UPDATE
                                                                              sso_project
                                                                        SET   
                                                                              project_status_type_id = ( SELECT
                                                                                                            entity_id
                                                                                                         FROM
                                                                                                            entity
                                                                                                         WHERE
                                                                                                            entity_type = 601
                                                                                                            AND entity_name = 'Completed' )
                                                                        WHERE
                                                                              sso_project_id = @projectid 


					
                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        IF ( @closedacccount ) = 0 
                                                                              BEGIN
                                                                                    DELETE FROM
                                                                                          sso_project_step
                                                                                    WHERE
                                                                                          sso_project_id = @projectid
                                                                                          AND step_no = 5
                                                                                    DELETE FROM
                                                                                          sso_project_activity
                                                                                    WHERE
                                                                                          sso_project_id = @projectid
                                                                                          AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'
 							--added for 18499
                                                                                    UPDATE
                                                                                          sso_project
                                                                                    SET   
                                                                                          project_status_type_id = ( SELECT
                                                                                                                        entity_id
                                                                                                                     FROM
                                                                                                                        entity
                                                                                                                     WHERE
                                                                                                                        entity_type = 601
                                                                                                                        AND entity_name = 'Open' )
                                                                                    WHERE
                                                                                          sso_project_id = @projectid 

                                                                              END
                                                                        ELSE 
                                                                              BEGIN
                                                                                    UPDATE
                                                                                          sso_project_step
                                                                                    SET   
                                                                                          is_active = 1
                                                                                         ,is_complete = 0
                                                                                    WHERE
                                                                                          sso_project_id = @projectid
                                                                                          AND step_no = 5 
							--added for 18499
                                                                                    UPDATE
                                                                                          sso_project
                                                                                    SET   
                                                                                          project_status_type_id = ( SELECT
                                                                                                                        entity_id
                                                                                                                     FROM
                                                                                                                        entity
                                                                                                                     WHERE
                                                                                                                        entity_type = 601
                                                                                                                        AND entity_name = 'Open' )
                                                                                    WHERE
                                                                                          sso_project_id = @projectid 

                                                                              END
                                                                  END

                                                      END
                                                ELSE 
                                                      BEGIN
                                                            DELETE FROM
                                                                  sso_project_step
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 3 
                                                            DELETE FROM
                                                                  sso_project_activity
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND activity_description = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and formulating its recommendation for your review'

                                                            DELETE FROM
                                                                  sso_project_step
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 4 
                                                            DELETE FROM
                                                                  sso_project_activity
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND activity_description = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.'

                                                            DELETE FROM
                                                                  sso_project_step
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND step_no = 5 
                                                            DELETE FROM
                                                                  sso_project_activity
                                                            WHERE
                                                                  sso_project_id = @projectid
                                                                  AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'

					--added for 18499
                                                            UPDATE
                                                                  sso_project
                                                            SET   
                                                                  project_status_type_id = ( SELECT
                                                                                                entity_id
                                                                                             FROM
                                                                                                entity
                                                                                             WHERE
                                                                                                entity_type = 601
                                                                                                AND entity_name = 'Open' )
                                                            WHERE
                                                                  sso_project_id = @projectid 


                                                      END


		
                        END -- else	
            END
END
	
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_DELETE_STATUS_FOR_ACCOUNT_P] TO [CBMSApplication]
GO
