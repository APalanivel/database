SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.CU_Invoice_Account_Info_SEL        
       
DESCRIPTION:  This will fetch account information for the cu_invoice_id  
      
INPUT PARAMETERS:          
 Name              DataType          Default     Description          
------------------------------------------------------------          
 @cu_invoice_id		int  
       
          
OUTPUT PARAMETERS:          
Name              DataType          Default     Description          
------------------------------------------------------------          
USAGE EXAMPLES: 

	EXEC CU_Invoice_Account_Info_SEL 915

	EXEC CU_Invoice_Account_Info_SEL 3971245

------------------------------------------------------------        
AUTHOR INITIALS:        
Initials	Name        
------------------------------------------------------------        
NK			Nageswara Rao Kosuri    
SSR			Sharad Srivastava	      
      
Initials	Date		Modification        
------------------------------------------------------------        
NK			02/19/2010  Created          
SSR			03/17/2010	Added Account_group_ID in Where Clause As NULL and NOT NULL	
SSR			03/18/2010	Removed Cu_invoice_account_map(Account_id) with cu_invoice_service_month(Account_id)
SSR			04/17/2010	USed CTE Concept to get the Supplier & Utility Account
SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)

******/
CREATE PROCEDURE dbo.CU_Invoice_Account_Info_SEL
	@cu_invoice_id INT
AS
BEGIN
 
	SET NOCOUNT ON;

	WITH CTE_Supplier_Account_Det
	AS
	(
		SELECT
			 a.Supplier_Account_Begin_Dt
			,a.Supplier_Account_End_Dt
			,a.ACCOUNT_ID
			,a.VENDOR_ID
			,a.account_type_id
			,a.ACCOUNT_NUMBER
			,cism.CU_INVOICE_ID
		FROM
			dbo.cu_invoice_service_month cism
			JOIN dbo.account AS a
				ON cism.account_id = a.account_id
			WHERE
				cism.Cu_invoice_id = @cu_invoice_id
	),
	CTE_Account_Det
	AS
		(
		SELECT
			m.ACCOUNT_ID
			,m.METER_ID
			,cte1.Supplier_Account_Begin_Dt
			,cte1.Supplier_Account_End_Dt
			,cte1.vendor_id
			,cte1.ACCOUNT_TYPE_ID
			,cte1.ACCOUNT_NUMBER
			,cte1.CU_INVOICE_ID
		 FROM
			dbo.METER m
			JOIN CTE_Supplier_Account_Det cte1
				ON cte1.ACCOUNT_ID = m.ACCOUNT_ID
		 UNION

		 SELECT
			map.ACCOUNT_ID
			,map.METER_ID
			,cte2.Supplier_Account_Begin_Dt
			,cte2.Supplier_Account_End_Dt
			,cte2.VENDOR_ID
			,cte2.ACCOUNT_TYPE_ID
			,cte2.ACCOUNT_NUMBER
			,cte2.CU_INVOICE_ID
		 FROM
			dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
			JOIN CTE_Supplier_Account_Det cte2
				ON cte2.ACCOUNT_ID = map.ACCOUNT_ID
		)
		SELECT
			cl.client_id
			, cl.client_name
			, ISNULL(AcctGrp_Vendor.vendor_id,v.vendor_id ) AS vendor_id
			, ISNULL(AcctGrp_Vendor.vendor_name ,v.vendor_name) AS vendor_name
			, ISNULL(accountGroup.group_billing_number,x.account_number) AS account_number
		FROM
			CTE_Account_Det x
			JOIN dbo.METER m
				ON m.METER_ID = x.METER_ID
			JOIN dbo.ADDRESS ad
				ON ad.ADDRESS_ID = m.ADDRESS_ID
			JOIN dbo.SITE s
				ON s.SITE_ID = ad.ADDRESS_PARENT_ID
			JOIN dbo.client AS cl
				ON cl.client_id = s.client_id
			JOIN dbo.cu_invoice inv
				ON inv.cu_invoice_id = x.cu_invoice_id
			JOIN dbo.vendor AS v
				ON v.vendor_id = x.vendor_id
			LEFT JOIN dbo.ACCOUNT_GROUP accountGroup
				ON inv.ACCOUNT_GROUP_ID = accountGroup.ACCOUNT_GROUP_ID
			LEFT JOIN dbo.Vendor AcctGrp_Vendor
				ON AcctGrp_Vendor.VENDOR_ID = accountGroup.VENDOR_ID
		WHERE
			x.cu_invoice_id = @cu_invoice_id
		GROUP BY
			  cl.client_id
			, cl.client_name
			, ISNULL(AcctGrp_Vendor.vendor_id,v.vendor_id)
			, ISNULL(AcctGrp_Vendor.vendor_name ,v.vendor_name)
			, ISNULL(accountGroup.group_billing_number,x.account_number)

END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Account_Info_SEL] TO [CBMSApplication]
GO
