SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_SITE_VOLUME_ALLOCATION_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec DBO.GET_DEAL_TICKET_SITE_VOLUME_ALLOCATION_INFO_P 1,1,100158

CREATE     PROCEDURE DBO.GET_DEAL_TICKET_SITE_VOLUME_ALLOCATION_INFO_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int 

AS
set nocount on
	SELECT 
	rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
	rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID, 
	rmdtd.TOTAL_VOLUME, 
	rmdtvd.SITE_ID,
	rmdtvd.HEDGE_VOLUME
FROM 
	RM_DEAL_TICKET rmdt, 
	RM_DEAL_TICKET_DETAILS rmdtd, 
	RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd 
WHERE 
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.RM_DEAL_TICKET_ID = rmdtd.RM_DEAL_TICKET_ID AND 
	rmdtd.RM_DEAL_TICKET_DETAILS_ID = rmdtvd.RM_DEAL_TICKET_DETAILS_ID 
ORDER BY rmdtd.RM_DEAL_TICKET_DETAILS_ID
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_SITE_VOLUME_ALLOCATION_INFO_P] TO [CBMSApplication]
GO
