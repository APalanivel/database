SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_DELETE_RATE_COMPARISON_DOCUMENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@rateComparisonId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE DBO.RC_DELETE_RATE_COMPARISON_DOCUMENT_P 

@userId varchar(20),
@sessionId varchar(20),
@rateComparisonId int

AS
set nocount on
	update RC_RATE_COMPARISON
	
	set	STATUS_TYPE_ID=(select ENTITY_ID from ENTITY where ENTITY_TYPE=701 AND ENTITY_NAME='Not Reviewed'),
		RESULT_TYPE_ID=(select ENTITY_ID from ENTITY where ENTITY_TYPE=700 AND ENTITY_NAME='Rate Comparison Document not set up'),
		REVIEWED_BY=null ,
		RATE_COMPARISON_DOCUMENT_XML='-1'
	
	where RC_RATE_COMPARISON_ID=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[RC_DELETE_RATE_COMPARISON_DOCUMENT_P] TO [CBMSApplication]
GO
