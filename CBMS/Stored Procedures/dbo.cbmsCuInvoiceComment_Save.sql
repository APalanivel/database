SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsCuInvoiceComment_Save]
	( @MyAccountId int
	, @cu_invoice_id int
	, @comment_by_id int = null
	, @comment_date datetime = null
	, @image_comment varchar(4000)
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	if @comment_by_id is null
	begin
		set @comment_by_id = @MyAccountId
	end

	if @comment_date is null
	begin
		set @comment_date = getdate()
	end


	insert into cu_invoice_comment
		( cu_invoice_id, comment_by_id, comment_date, image_comment
		)
	values
		( @cu_invoice_id, @comment_by_id, @comment_date, @image_comment
		)

	set @this_id = @@IDENTITY

--	set nocount off

	exec cbmsCuInvoiceComment_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceComment_Save] TO [CBMSApplication]
GO
