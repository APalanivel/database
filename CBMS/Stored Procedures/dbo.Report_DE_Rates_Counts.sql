
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/*****
        
NAME: dbo.Report_DE_Rates_Counts      
    
DESCRIPTION:        
	used to get the rates
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC Report_DE_Rates_Counts      
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
AKR   Ashok Kumar Raju
SP	  Sandeep Pigilam           

     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  AKR    2012-05-01  Cloned from View "dbo.lec_RatesCounts"
  SP	 2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
  
******/

CREATE PROCEDURE [dbo].[Report_DE_Rates_Counts]
AS 
BEGIN        
    
      SET NOCOUNT ON;    
      
      DECLARE
            @EP_ID INT
           ,@NG_ID INT
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
            
      SELECT
            @EP_ID = MAX(CASE WHEN com.commodity_Name = 'Electric Power' THEN com.commodity_Id
                         END)
           ,@NG_ID = MAX(CASE WHEN com.commodity_Name = 'Natural Gas' THEN com.commodity_Id
                         END)
      FROM
            dbo.Commodity com 
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market';
      WITH  account_ven_rate_cte
              AS ( SELECT
                        ca.Account_Number
                       ,ca.account_id
                       ,ca.account_vendor_id [vendor_id]
                       ,ca.Account_Vendor_Name
                       ,ca.Rate_Id
                       ,Commodity = CASE WHEN ( EP_Count = 1
                                                AND NG_Count = 1 ) THEN 'EP-NG'
                                         WHEN EP_Count = 1
                                              AND Others_Count = 0 THEN 'EP'
                                         WHEN NG_Count = 1
                                              AND Others_Count = 0 THEN 'NG'
                                         ELSE 'OtherServicesOnly'
                                    END
                   FROM
                        Core.Client_Hier_Account ca
                        CROSS APPLY ( SELECT
                                          COUNT(DISTINCT cha.Commodity_Id)
                                      FROM
                                          Core.Client_Hier_Account cha
                                      WHERE
                                          cha.Account_Id = ca.Account_Id
                                          AND cha.Commodity_Id = @EP_ID
                        FOR
                                      XML PATH('') ) EP ( EP_Count )
                        CROSS APPLY ( SELECT
                                          COUNT(DISTINCT chaa.Commodity_Id)
                                      FROM
                                          Core.Client_Hier_Account chaa
                                      WHERE
                                          chaa.Account_Id = ca.Account_Id
                                          AND chaa.Commodity_Id = @NG_ID
                        FOR
                                      XML PATH('') ) NG ( NG_Count )
                        CROSS APPLY ( SELECT
                                          COUNT(DISTINCT chaa.Commodity_Id)
                                      FROM
                                          Core.Client_Hier_Account chaa
                                      WHERE
                                          chaa.Account_Id = ca.Account_Id
                                          AND chaa.Commodity_Id NOT IN ( @EP_ID, @NG_ID )
                        FOR
                                      XML PATH('') ) oth ( Others_Count )
                   WHERE
                        ca.Account_Type = 'Utility'
                        AND ca.Account_Not_Managed = 0
                   GROUP BY
                        ca.Account_Number
                       ,ca.account_id
                       ,ca.account_vendor_id
                       ,ca.Account_Vendor_Name
                       ,ca.Rate_Id
                       ,CASE WHEN ( EP_Count = 1
                                    AND NG_Count = 1 ) THEN 'EP-NG'
                             WHEN EP_Count = 1
                                  AND Others_Count = 0 THEN 'EP'
                             WHEN NG_Count = 1
                                  AND Others_Count = 0 THEN 'NG'
                             ELSE 'OtherServicesOnly'
                        END),
            Vendor_Analyst_CTE
              AS ( SELECT
                        v.vendor_id
                       ,v.VENDOR_NAME
                       ,ui1.FIRST_NAME + ' ' + ui1.LAST_NAME [Manager]
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME [ManagingAnalyst]
                       ,STATE_NAME [State]
                       ,REGION_NAME [Region]
                   FROM
                        vendor v
                        JOIN utility_detail ud
                              ON ud.vendor_id = v.vendor_id
                        JOIN utility_detail_analyst_map udam
                              ON udam.utility_detail_id = ud.utility_detail_id
                        JOIN user_info ui
                              ON ui.USER_INFO_ID = udam.Analyst_ID
                        JOIN GROUP_INFO gi
                              ON gi.GROUP_INFO_ID = udam.Group_Info_ID
                        INNER JOIN @Group_Legacy_Name gil
                              ON gi.GROUP_INFO_ID = gil.Group_Info_ID
                        JOIN VENDOR_STATE_MAP vsm
                              ON vsm.VENDOR_ID = v.VENDOR_ID
                        JOIN STATE st
                              ON st.STATE_ID = vsm.STATE_ID
                        JOIN REGION reg
                              ON reg.REGION_ID = st.REGION_ID
                        JOIN REGION_MANAGER_MAP rmm
                              ON rmm.REGION_ID = reg.REGION_ID
                        JOIN user_info ui1
                              ON ui1.USER_INFO_ID = rmm.USER_INFO_ID)
            SELECT
                  ac.Account_vendor_name [Utility]
                 ,COUNT(DISTINCT ac.rate_id) [Count of Rates by Utility]
                 ,COUNT(DISTINCT ac.account_id) [Count of Accounts by Utility]
                 ,vc.ManagingAnalyst
                 ,vc.Manager
                 ,vc.State
                 ,vc.Region
                 ,ac.Commodity
            FROM
                  account_ven_rate_cte ac
                  JOIN Vendor_Analyst_CTE vc
                        ON vc.VENDOR_ID = ac.vendor_id
            GROUP BY
                  ac.Account_Vendor_Name
                 ,ac.Commodity
                 ,vc.Manager
                 ,vc.ManagingAnalyst
                 ,vc.State
                 ,vc.Region
            ORDER BY
                  ac.Account_Vendor_Name
                 ,ac.Commodity
                 ,vc.Manager
                 ,vc.ManagingAnalyst
                 ,vc.State
                 ,vc.Region  
            
END;
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Rates_Counts] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Rates_Counts] TO [CBMSApplication]
GO
