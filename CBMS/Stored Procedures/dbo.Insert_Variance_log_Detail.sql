SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*********      
NAME:   
       
DESCRIPTION: This procedure is created to insert the variance logging entry into log db   
      
INPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
              
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:         
      
 EXEC dbo.Master_Variance_Test_SEL   1  

 EXEC dbo.Master_Variance_Test_SEL   2   
      
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
Ap		Arunkumar Palanivel
      
Initials Date  Modification        
------------------------------------------------------------        
AP	Feb 10,2020	Created new procedure   
******/

CREATE PROCEDURE [dbo].[Insert_Variance_log_Detail]
      (
      @Client_Hier_Id             INT
    , @Account_Id                 INT
    , @Commodity_Id               INT
    , @Service_Month              DATE
	, @Start_Time DATETIME = null 
   -- , @Error_Desc                 INT
    , @TVP_Variance_log_Detail AS TVP_Variance_log_Detail READONLY )
AS
      BEGIN

            SET NOCOUNT ON;


            DECLARE @identity INT;

			SET @Start_Time = isnull(@Start_Time, getdate())


            INSERT INTO logdb.dbo.Variance_Extraction_Service_Log (
                                                                  Client_Hier_Id
                                                                , Account_Id
                                                                , Commodity_Id
                                                                , Service_Month
                                                               -- , Error_Desc
                                                                , Created_Ts
                                                                , Last_Change_Ts
                                                            )
            VALUES
                 ( @Client_Hier_Id  -- Client_Hier_Id - int
                 , @Account_Id      -- Account_Id - int
                 , @Commodity_Id    -- Commodity_Id - int
                 , @Service_Month   -- Service_Month - date
              --   , @Error_Desc      -- Error_Desc - nvarchar(max)
                 , @Start_Time        -- Created_Ts - datetime
                 , getdate()        -- Last_Change_Ts - datetime
                  );


            SET @identity = scope_identity();

            INSERT INTO logdb.dbo.Variance_Extraction_Service_Log_Dtl (
                                                                      Variance_Extraction_Service_Log_Id 
                                                                    , Request_Data
                                                                    , Response_Data
                                                                    , Error_Desc
                                                                    , Created_Ts
                                                                    , Last_Change_Ts
                                                                )
                        SELECT
                              @identity 
                            , tvp.Request_data
                            , tvp.Response_data
                            , tvp.Error_desc
                            , getdate()
                            , getdate()
                        FROM  @TVP_Variance_log_Detail tvp;

      END;
GO
GRANT EXECUTE ON  [dbo].[Insert_Variance_log_Detail] TO [CBMSApplication]
GO
