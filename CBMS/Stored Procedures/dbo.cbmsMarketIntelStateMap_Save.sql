SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE      procedure [dbo].[cbmsMarketIntelStateMap_Save]
	( @MyAccountId int
	, @MarketIntelId int
	, @state_id int
	)
AS
BEGIN

	set nocount on
	
	declare @RecordCount int		

	select @RecordCount = count(*)
	from market_intel_state_map
	where market_intel_id = @MarketIntelId
	and state_id = @state_id
	
	if @RecordCount = 0
	begin
		insert into market_intel_state_map
			( market_intel_id
			, state_id
			)
		values
			(@MarketIntelId
			,@state_id
			)
	end

--	set nocount off

End
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntelStateMap_Save] TO [CBMSApplication]
GO
