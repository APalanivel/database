SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SR_CREATE_MASTER_BATCH_P
	@batch_start_time datetime,
	@master_batch_log_id int out
	AS
	set nocount on	
	insert into sr_batch_master_log (batch_start_date)
	values(@batch_start_time)
	
	set @master_batch_log_id = @@identity
	
	return
GO
GRANT EXECUTE ON  [dbo].[SR_CREATE_MASTER_BATCH_P] TO [CBMSApplication]
GO
