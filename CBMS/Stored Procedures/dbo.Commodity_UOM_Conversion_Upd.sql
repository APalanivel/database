SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
  

NAME:        
dbo.Commodity_UOM_Conversion_Upd       

DESCRIPTION:        
	Used to insert/update UOM conversion into COMMODITY_UOM_CONVERSION table      

INPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
  @Base_Commodity_Id AS INT
, @Base_UOM_Cd AS INT
, @Converted_Commodity_Id AS INT
, @Converted_UOM_Cd AS INT
, @Conversion_Factor AS DECIMAL(   32, 16 )    
      
OUTPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC dbo.Commodity_UOM_Conversion_Upd 57,102,1373,96,0.984206527611061


AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA Shobhit Kr Agrawal    

MODIFICATIONS         
Initials Date		Modification        
------------------------------------------------------------        
GB		Created      
SKA		08/07/09	Modified    

GB		14/07/09	In Entity Table the Unit for Description is different for Different Commodity,   
					It does not follow standard pattern for electriv power, Natural gas and Alternative Natural Gas  
					Hence those are treated separately in the case  
SS		17/09/09	Added @Is_Updated variable for getting update status, CASE WHEN for few cases to ensure when Conversion factor is not passed
					SP also returns Outvariable results for use in Application.
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Commodity_UOM_Conversion_Upd]
  @Base_Commodity_Id AS INT
, @Base_UOM_Cd AS INT
, @Converted_Commodity_Id AS INT
, @Converted_UOM_Cd AS INT
, @Conversion_Factor AS DECIMAL(   32, 16 ) 
, @Is_Updated BIT =0 OUT

AS 

BEGIN
    SET  NOCOUNT ON;

    DECLARE  @TConversion_Factor AS DECIMAL(   32, 16 )
    
    SELECT   @TConversion_Factor = CASE WHEN @Conversion_Factor IS NULL THEN NULL 
												WHEN @Conversion_Factor = 0 THEN 0
												ELSE  1 / @Conversion_Factor END
		        
    BEGIN TRY
		BEGIN TRANSACTION
    		    
			UPDATE     dbo.COMMODITY_UOM_CONVERSION 
				SET  Conversion_Factor = CASE WHEN @Conversion_Factor IS NOT NULL THEN @Conversion_Factor 
											  ELSE Conversion_Factor 
										  END	 
					, Is_Active = 1
				WHERE       Base_Commodity_Id = @Base_Commodity_Id AND
							Base_UOM_Cd = @Base_UOM_Cd AND
							Converted_Commodity_Id = @Converted_Commodity_Id AND
							Converted_UOM_Cd = @Converted_UOM_Cd
							
			SELECT @Is_Updated = CASE WHEN @@ROWCOUNT > 0  THEN  1 ELSE 0 END 			
			-- Insert or UPDATE Converted Commodity one level with respect to base commodity conversion factor        
		    			
			UPDATE
				dbo.COMMODITY_UOM_CONVERSION 
				SET Conversion_Factor = CASE 
											WHEN @TConversion_Factor IS NOT NULL THEN @TConversion_Factor
											ELSE Conversion_Factor 
										END
										, Is_Active = 1
			WHERE
				Base_Commodity_Id = @Converted_Commodity_Id AND
				Base_UOM_Cd = @Converted_UOM_Cd AND
				Converted_Commodity_Id = @Base_Commodity_Id AND
				Converted_UOM_Cd = @Base_UOM_Cd

		    SELECT @Is_Updated = CASE WHEN @@ROWCOUNT > 0 OR @Is_Updated > 0  THEN  1 ELSE 0 END
		    SELECT @Is_Updated AS IS_UPDATED
		    
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		EXEC dbo.usp_RethrowError
	END CATCH
END
GO
GRANT EXECUTE ON  [dbo].[Commodity_UOM_Conversion_Upd] TO [CBMSApplication]
GO
