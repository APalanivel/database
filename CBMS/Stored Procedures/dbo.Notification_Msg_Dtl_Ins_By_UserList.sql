SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   [dbo].[Notification_Msg_Dtl_Ins]  
             
DESCRIPTION:               
			To insert notification msg for each entity 
             
INPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
@Notification_Msg_Queue_Id		INT
@Recipient_User_Id				INT  
@Recipient_Email_Address		NVARCHAR(200)  
@Msg_Delivery_Status			VARCHAR(25)		'Pending'

 OUTPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
    
           
 USAGE EXAMPLES:              
------------------------------------------------------------------------
BEGIN TRANSACTION
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=1 AND Recipient_User_Id IN (60740,60741)
	EXEC dbo.Notification_Msg_Dtl_Ins_By_UserList   
		@Notification_Msg_Queue_Id = 8140
		,@Recipient_User_List = '3526,3571,3572,5082,5083'
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=1 AND Recipient_User_Id IN (60740,60741)
ROLLBACK TRANSACTION

SELECT TOP 5 USER_INFO_ID FROM dbo.SR_SUPPLIER_CONTACT_INFO WHERE Termination_Contact_Email_Address IS NOT NULL
	AND Termination_Contact_Email_Address <>'' AND Registration_Contact_Email_Address IS NOT NULL
	AND Registration_Contact_Email_Address<>''
	
SELECT TOP 5 nmq.Notification_Msg_Queue_Id FROM dbo.Notification_Msg_Queue nmq 
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' ) AND cs.Codeset_Name = 'Notification Type'

SELECT TOP 5 nmq.Notification_Msg_Queue_Id FROM dbo.Notification_Msg_Queue nmq 
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Registration Notification' ) AND cs.Codeset_Name = 'Notification Type'


BEGIN TRANSACTION
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8140 AND Recipient_User_Id IN (3526,3571,3572,5082,5083)
	EXEC dbo.Notification_Msg_Dtl_Ins_By_UserList   
		@Notification_Msg_Queue_Id = 8140
		,@Recipient_User_List = '3526,3571,3572,5082,5083'
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8140 AND Recipient_User_Id IN (3526,3571,3572,5082,5083)
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8149 AND Recipient_User_Id IN (3526,3571,3572,5082,5083)
	EXEC dbo.Notification_Msg_Dtl_Ins_By_UserList   
		@Notification_Msg_Queue_Id = 8149
		,@Recipient_User_List = '3526,3571,3572,5082,5083'
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8149 AND Recipient_User_Id IN (3526,3571,3572,5082,5083)
ROLLBACK TRANSACTION 

BEGIN TRANSACTION
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8322
	EXEC dbo.Notification_Msg_Dtl_Ins_By_UserList   
		@Notification_Msg_Queue_Id = 8322
		,@Recipient_User_List = '7537,26958'
	SELECT * FROM dbo.Notification_Msg_Dtl nmd WHERE Notification_Msg_Queue_Id=8322
ROLLBACK TRANSACTION 

 
AUTHOR INITIALS:              
	Initials	Name              
------------------------------------------------------------------------
	RR			Raghu Reddy  
             
 MODIFICATIONS:               
	Initials	Date		Modification              
------------------------------------------------------------------------  
	RR			2014-01-23  Created for GCS Phase-5

******/
CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Ins_By_UserList]
      ( 
       @Notification_Msg_Queue_Id INT
      ,@Recipient_User_List NVARCHAR(MAX)
      ,@Msg_Delivery_Status VARCHAR(25) = 'Pending' )
AS 
BEGIN  
      SET NOCOUNT ON;
      
      DECLARE
            @Msg_Delivery_Status_Cd INT
           ,@Flow_Type VARCHAR(25)
      
      
      DECLARE @User_Info_Ids TABLE ( User_Info_Id INT )
      DECLARE @User_Email_Ids TABLE
            ( 
             User_Info_Id INT
            ,Email_Address VARCHAR(150) )
            
  
      SELECT
            @Msg_Delivery_Status_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            INNER JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = @Msg_Delivery_Status
            AND cs.Codeset_Name = 'Message Delivery Status'

  
      SELECT
            @Flow_Type = cd.Code_Value
      FROM
            dbo.Notification_Msg_Queue nmq
            INNER JOIN dbo.Notification_Template nt
                  ON nmq.Notification_Template_Id = nt.Notification_Template_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Notification Type'
            AND nmq.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id
      
      
      
      INSERT      INTO @User_Info_Ids
                  ( 
                   User_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Recipient_User_List, ',') ufn
                  GROUP BY
                        ufn.Segments  
                        
                        
      INSERT      INTO @User_Email_Ids
                  ( 
                   User_Info_Id
                  ,Email_Address )
                  SELECT
                        ssci.USER_INFO_ID
                       ,CASE WHEN @Flow_Type = 'Registration Notification' THEN ssci.Registration_Contact_Email_Address
                             WHEN @Flow_Type = 'Termination Vendor' THEN ssci.Termination_Contact_Email_Address
                             WHEN @Flow_Type = 'Termination Analyst' THEN ssci.Termination_Contact_Email_Address
                        END
                  FROM
                        dbo.SR_SUPPLIER_CONTACT_INFO ssci
                        INNER JOIN @User_Info_Ids uis
                              ON ssci.USER_INFO_ID = uis.User_Info_Id
                  WHERE
                        ( ( @Flow_Type = 'Registration Notification'
                            AND ssci.Registration_Contact_Email_Address IS NOT NULL
                            AND ssci.Registration_Contact_Email_Address <> '' )
                          OR ( @Flow_Type = 'Termination Vendor'
                               AND ssci.Termination_Contact_Email_Address IS NOT NULL
                               AND ssci.Termination_Contact_Email_Address <> '' ) )
                        
      INSERT      INTO @User_Email_Ids
                  ( 
                   User_Info_Id
                  ,Email_Address )
                  SELECT
                        recp.USER_INFO_ID
                       ,recp.EMAIL_ADDRESS
                  FROM
                        dbo.USER_INFO recp
                        INNER JOIN @User_Info_Ids uis
                              ON recp.USER_INFO_ID = uis.User_Info_Id
                  WHERE
                        @Flow_Type = 'Termination Analyst'
                                     
      BEGIN TRY  
            BEGIN TRANSACTION 
    
            INSERT      INTO dbo.Notification_Msg_Dtl
                        ( 
                         Notification_Msg_Queue_Id
                        ,Recipient_User_Id
                        ,Recipient_Email_Address
                        ,Msg_Delivery_Status_Cd
                        ,Created_Ts
                        ,Last_Change_Ts )
                        SELECT
                              @Notification_Msg_Queue_Id
                             ,uei.User_Info_Id
                             ,uei.Email_Address
                             ,@Msg_Delivery_Status_Cd
                             ,GETDATE()
                             ,GETDATE()
                        FROM
                              @User_Email_Ids uei 
                              
            COMMIT  TRANSACTION
            
      END TRY  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRANSACTION 
            EXEC dbo.usp_RethrowError   
   
      END CATCH  
   
END;





;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Ins_By_UserList] TO [CBMSApplication]
GO
