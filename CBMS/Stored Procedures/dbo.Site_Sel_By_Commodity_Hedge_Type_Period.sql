SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
NAME:                                  
    dbo.Site_Sel_By_Commodity_Hedge_Type_Period                                  
                                  
DESCRIPTION:                                  
                             
                                  
INPUT PARAMETERS:                                  
 Name    DataType  Default Description                                  
---------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Start_Dt   DATE  
    @End_Dt    DATE  
    @Hedge_Type   INT  
                            
                         
OUTPUT PARAMETERS:                                  
 Name   DataType  Default Description                                  
---------------------------------------------------------------      
  
                               
 EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 11236  
  
 EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
 EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-12-30',@Hedge_Type=586  
 EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-03-30',@Hedge_Type=586  
 EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2019-01-01',@End_Dt='2019-12-30',@Hedge_Type=586  
 EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2017-01-01',@End_Dt='2017-01-01',@Hedge_Type=586  
   
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2012-01-01','2023-01-01',586  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2017-01-01','2017-01-01',586,'Granite'  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2019-01-01','2019-01-01',586,'Toluca A 44944'  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2017-01-01','2017-01-01',586,'Valtierrilla'  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2017-01-01','2017-01-01',586,'Salt Lake City'  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2017-01-01','2017-01-01',586,'NC'  
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2017-01-01','2017-01-01',586,'50246' 
	EXEC dbo.Site_Sel_By_Commodity_Hedge_Type_Period 10003,291,'2018-01-01','2019-12-01',586,'pbc',1,25
                  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy  
                     
MODIFICATIONS  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2018-10-25  Created For Risk Management  
	RR			2019-09-26  GRM-1185 - Added sorting
******/
CREATE PROCEDURE [dbo].[Site_Sel_By_Commodity_Hedge_Type_Period]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Hedge_Type INT
        , @Keyword VARCHAR(MAX) = NULL
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Hedge_Type_Input VARCHAR(200);
        DECLARE @Search_Str VARCHAR(MAX);

        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        SELECT  @Search_Str = '%' + @Keyword + '%';

        WITH CTE_Sites
        AS (
               SELECT
                    ch.Site_Id
                    , ch.Site_name
                    , ch.Site_Address_Line1 + ' ' + ch.Site_Address_Line2 AS Site_Address
                    , ch.City
                    , ch.State_Name
                    , ch.ZipCode
                    , ch.Site_Not_Managed
                    , ROW_NUMBER() OVER (ORDER BY
                                             ch.Site_name) AS row_Num
               FROM
                    Core.Client_Hier ch
               WHERE
                    ch.Client_Id = @Client_Id
                    AND (   EXISTS (   SELECT
                                            1
                                       FROM
                                            Trade.RM_Client_Hier_Onboard siteob
                                            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                                ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                            INNER JOIN dbo.ENTITY et
                                                ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                       WHERE
                                            siteob.Client_Hier_Id = ch.Client_Hier_Id
                                            AND siteob.Commodity_Id = @Commodity_Id
                                            AND (   (   @Hedge_Type_Input = 'Physical'
                                                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                                                        AND EXISTS (   SELECT
                                                                            1
                                                                       FROM
                                                                            Core.Client_Hier_Account suppacc
                                                                            INNER JOIN dbo.CONTRACT con
                                                                                ON suppacc.Supplier_Contract_ID = con.CONTRACT_ID
                                                                       WHERE
                                                                            ch.Client_Hier_Id = suppacc.Client_Hier_Id
                                                                            AND con.COMMODITY_TYPE_ID = @Commodity_Id
                                                                            AND (   con.CONTRACT_START_DATE BETWEEN @Start_Dt
                                                                                                            AND     @End_Dt
                                                                                    OR  con.CONTRACT_END_DATE BETWEEN @Start_Dt
                                                                                                              AND     @End_Dt
                                                                                    OR  @Start_Dt BETWEEN con.CONTRACT_START_DATE
                                                                                                  AND     con.CONTRACT_END_DATE
                                                                                    OR  @End_Dt BETWEEN con.CONTRACT_START_DATE
                                                                                                AND     con.CONTRACT_END_DATE)))
                                                    OR  (   @Hedge_Type_Input = 'Financial'
                                                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
                                            AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                         AND     @End_Dt
                                                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                           AND     @End_Dt
                                                    OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                                  AND     chhc.Config_End_Dt
                                                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                                AND     chhc.Config_End_Dt))
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                Trade.RM_Client_Hier_Onboard clntob
                                                INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                                    ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                                INNER JOIN dbo.ENTITY et
                                                    ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                                INNER JOIN Core.Client_Hier clch
                                                    ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                           WHERE
                                                clch.Sitegroup_Id = 0
                                                AND clch.Client_Id = ch.Client_Id
                                                AND clntob.Country_Id = ch.Country_Id
                                                AND clntob.Commodity_Id = @Commodity_Id
                                                AND (   (   @Hedge_Type_Input = 'Physical'
                                                            AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                                                            AND EXISTS (   SELECT
                                                                                1
                                                                           FROM
                                                                                Core.Client_Hier_Account suppacc
                                                                                INNER JOIN dbo.CONTRACT con
                                                                                    ON suppacc.Supplier_Contract_ID = con.CONTRACT_ID
                                                                           WHERE
                                                                                ch.Client_Hier_Id = suppacc.Client_Hier_Id
                                                                                AND con.COMMODITY_TYPE_ID = @Commodity_Id
                                                                                AND (   con.CONTRACT_START_DATE BETWEEN @Start_Dt
                                                                                                                AND     @End_Dt
                                                                                        OR  con.CONTRACT_END_DATE BETWEEN @Start_Dt
                                                                                                                  AND     @End_Dt
                                                                                        OR  @Start_Dt BETWEEN con.CONTRACT_START_DATE
                                                                                                      AND     con.CONTRACT_END_DATE
                                                                                        OR  @End_Dt BETWEEN con.CONTRACT_START_DATE
                                                                                                    AND     con.CONTRACT_END_DATE)))
                                                        OR  (   @Hedge_Type_Input = 'Financial'
                                                                AND et.ENTITY_NAME IN ( 'Financial'
                                                                                        , 'Physical & Financial' )))
                                                AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                             AND     @End_Dt
                                                        OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                               AND     @End_Dt
                                                        OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                                      AND     chhc.Config_End_Dt
                                                        OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                                    AND     chhc.Config_End_Dt)
                                                AND NOT EXISTS (   SELECT
                                                                        1
                                                                   FROM
                                                                        Trade.RM_Client_Hier_Onboard siteob1
                                                                   WHERE
                                                                        siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                                        AND siteob1.Commodity_Id = @Commodity_Id)))
                    AND ch.Site_Id > 0
                    AND (   @Keyword IS NULL
                            OR  (   ch.Site_name LIKE @Search_Str
                                    OR  ch.Site_Address_Line1 LIKE @Search_Str
                                    OR  ch.Site_Address_Line2 LIKE @Search_Str
                                    OR  ch.City LIKE @Search_Str
                                    OR  ch.State_Name LIKE @Search_Str
                                    OR  ch.ZipCode LIKE @Search_Str))
               GROUP BY
                   ch.Site_Id
                    --,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')'  
                   , ch.Site_name
                   , ch.Site_Address_Line1 + ' ' + ch.Site_Address_Line2
                   , ch.City
                   , ch.State_Name
                   , ch.ZipCode
                   , ch.Site_Not_Managed
           )
        SELECT
            Site_Id
            , Site_name
            , Site_Address
            , City
            , State_Name
            , ZipCode
            , Site_Not_Managed
        FROM
            CTE_Sites
        WHERE
            row_Num BETWEEN @StartIndex
                    AND     @EndIndex
        ORDER BY
            row_Num;


    END;






GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Commodity_Hedge_Type_Period] TO [CBMSApplication]
GO
