SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Account_Audit_Del]  
     
DESCRIPTION: 
	To Delete Account Audit Information for selected Account Audit Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@ACCOUNT_AUDIT_ID		INT				Utility Account
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Account_Audit_Del  101527

	EXEC Account_Audit_Del  116867
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
PNR			25-MAY-10	CREATED
*/

CREATE PROCEDURE dbo.Account_Audit_Del
    (
      @ACCOUNT_AUDIT_ID INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
	FROM	
		dbo.ACCOUNT_AUDIT 
	WHERE 
		ACCOUNT_AUDIT_ID=@ACCOUNT_AUDIT_ID
			
END
GO
GRANT EXECUTE ON  [dbo].[Account_Audit_Del] TO [CBMSApplication]
GO
