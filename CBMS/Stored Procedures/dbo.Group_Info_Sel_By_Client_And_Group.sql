SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
 NAME: [dbo].[Group_Info_Sel_By_Client_And_Group]    
                    
 DESCRIPTION:                      
    To return the Groupinfo id if the client has Easy pass setup option                   
                      
 INPUT PARAMETERS:                      
                     
 Name                              DataType        Default       Description                      
-------------------------------------------------------------------------------------------------------------                    
 @Client_Id                        INT      NULL               

              
                      
 OUTPUT PARAMETERS:                            
 Name                              DataType        Default       Description                      
-------------------------------------------------------------------------------------------------------------                    
                      
 USAGE EXAMPLES:                          
-------------------------------------------------------------------------------------------------------------                     
                     
    EXEC Group_Info_Sel_By_Client_And_Group
      @Client_Id = 14598 ,@Group_Name ='Easy Pass Setup'

	                       
    EXEC Group_Info_Sel_By_Client_And_Group
      @Client_Id = 11278 ,@Group_Name ='Easy Pass Setup'
                     
     


 AUTHOR INITIALS:                     
                     
 Initials               Name                      
-------------------------------------------------------------------------------------------------------------                    
 SM                     Smitha P A                
                       
 MODIFICATIONS:                     
                      
 Initials               Date            Modification
-------------------------------------------------------------------------------------------------------------
 SM                      2019-06-04     Created for RA Admin user management

******/
CREATE PROCEDURE  [dbo].[Group_Info_Sel_By_Client_And_Group]
	  ( 
       @Client_Id INT,
	   @Group_Name NVARCHAR(200) = NULL
	   )
AS 
BEGIN             
      
      SET NOCOUNT ON; 
	  SELECT gf.Group_Name, cgm.Group_Info_Id 
	  FROM  dbo.Group_Info gf inner join dbo.Client_Group_Info_Map cgm 
		ON gf.GROUP_INFO_ID = cgm.Group_Info_Id       
	  WHERE cgm.Client_Id = @Client_Id  AND (@Group_Name IS NULL
                              OR gf.group_name LIKE '%' + @Group_Name + '%'
                              OR gf.GROUP_DESCRIPTION LIKE '%' + @Group_Name + '%' )

    
END
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Sel_By_Client_And_Group] TO [CBMSApplication]
GO
