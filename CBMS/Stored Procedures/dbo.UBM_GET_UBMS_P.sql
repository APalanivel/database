SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                  
Name:                  
    dbo.UBM_GET_UBMS_P                 
                 
Description:                  
    
Input Parameters:                  
    Name	 DataType			Default				Description                  
------------------------------------------------------------------------                  
             
                 
Output Parameters:                  
    Name	 DataType			Default				Description                  
------------------------------------------------------------------------                  
                 
Usage Examples:                
------------------------------------------------------------------------                  
               
 EXEC dbo.UBM_GET_UBMS_P 1,1,0         
                 
Author Initials:                  
	Initials   Name                
------------------------------------------------------------------------                  
	NR          Narayana Reddy
	       
 Modifications :                  
 Initials		Date			Modification                  
------------------------------------------------------------------------  
NR				01-09-2017		PAyment process- Added Is_Genric as param.              
******/



CREATE PROCEDURE [dbo].[UBM_GET_UBMS_P]
    @userId VARCHAR(20)
    , @sessionId VARCHAR(20)
    , @Is_Generic BIT = NULL
AS
    SET NOCOUNT ON;
    SELECT
        UBM_ID
        , UBM_NAME
        , Is_Generic
    FROM
        UBM
    WHERE
        (   @Is_Generic IS NULL
            OR  (   @Is_Generic = 0
                    AND Is_Generic IS NULL))
        OR  (Is_Generic = @Is_Generic);
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_UBMS_P] TO [CBMSApplication]
GO
