SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











/******                              
  
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	@return_value = [dbo].[Variance_Rule_Decider_Bucket_Based]
		@Account_Id =1407581 , --1407581 1405819
		@Commodity_Id = 290,
		@Service_Month = N'2017-12-01'

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	 1.Find the buckets associated to the account for the commodity and service month.
	 2.If the bucket is Total cost, then we have to give total cost.
		For total cost , we have to give additional info as per the above rule.
	3.check 12 of 18 data points to decide linear analogous

	Method Identification - 12 of 18 Rule
If the selected account has USAGE values for 12 or more months out of the last 18 months (start counting backwards from/not including the service month of the invoice start date), the query service will select Linear Regression variance method and continue with history query.

Note that 12 out of 18 rule does not require that the values be in 12 consecutive months. Just require 12 of 18 months to have usage values.

A value of 0 counts as a value and should be counted toward 12 of 18 rule.

Months marked as "No Data This Period" do NOT count as values toward the 12 of 18 rule.

Estimated usage values (either created by Gap-Filling procedure or entered by a user) will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months with manually-entered values will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months that were manually excluded as anomalous usage (see Anomalous Month section within Review Variance page) will NOT count as values toward the 12 of 18 rule.

If the selected account has usage values for less than 12 months out of the last 18 months, the query service will select Analogous Account variance method.
	4.It is possible that for same account /commodity one category can be linear and another one can be analogus. in that case, we have to send two request.
	however SP will give only on result set. 
	5.
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					Jan 21 2020			New procedure to get data points for variance testing (as per the requirement mentioned above)         
AP					Jun 24,2010			Maint 10466 to filter out DEO account  
******/
CREATE PROCEDURE [dbo].[Variance_Rule_Decider_Bucket_Based]
      (
      @Account_Id    INT
    , @Commodity_Id  INT
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;



            DECLARE @Bucket_List TABLE
                  ( Bucket_Master_Id INT          PRIMARY KEY CLUSTERED
                  , Bucket_Name      VARCHAR(255)
                  , Variance_Decider VARCHAR(100) DEFAULT 'Rule Based Variance Test' );

            INSERT INTO @Bucket_List (
                                           Bucket_Master_Id
                                         , Bucket_Name
                                     )
                        SELECT      DISTINCT
                                    bm.Bucket_Master_Id
                                  , bm.Bucket_Name
                        FROM        dbo.Cost_Usage_Account_Dtl cuad
                                    JOIN
                                    dbo.Bucket_Master bm
                                          ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
                        WHERE       cuad.ACCOUNT_ID = @Account_Id
                                    AND   cuad.Service_Month = @Service_Month
                                    AND   bm.Commodity_Id = @Commodity_Id;



            --check for current account and delete the buckets which doesnt have current entry




            --THIS WILL HAVE THE CURRENT ACCOUNT SERVICE MONTH BUCKETS ONLY

            IF EXISTS
                  (     SELECT
                              1
                        FROM  Core.Client_Hier ch
                              JOIN
                              Core.Client_Hier_Account cha
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        WHERE cha.Account_Id = @Account_Id
                              AND   cha.Commodity_Id = @Commodity_Id
                              AND   cha.Account_Type = 'utility' )
                  BEGIN
                        UPDATE
                              BL
                        SET
                              Variance_Decider = 'Advanced Variance Test'
                        FROM  @Bucket_List BL
                              JOIN
                              dbo.Variance_category_Bucket_Map vcbm
                                    ON vcbm.Bucket_master_Id = BL.Bucket_Master_Id;
                  END;

            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.ACCOUNT
                        WHERE ACCOUNT_ID = @Account_Id
                              AND   Is_Data_Entry_Only <> 1 )
                  BEGIN
                        SELECT
                              Bucket_Master_Id
                            , Bucket_Name
                            , Variance_Decider
                        FROM  @Bucket_List;
                  END;
      END;

GO
GRANT EXECUTE ON  [dbo].[Variance_Rule_Decider_Bucket_Based] TO [CBMSApplication]
GO
