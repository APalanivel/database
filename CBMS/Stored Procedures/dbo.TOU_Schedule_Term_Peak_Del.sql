SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.TOU_Schedule_Term_Peak_Del

DESCRIPTION:
	Deletes the dtl table based on the peak dtl ids.
	If there are no entries in the dtl table for a particular peak, the peak is also deleted

INPUT PARAMETERS:
	Name										DataType		Default	Description
-------------------------------------------------------------------------------
	@Time_Of_Use_Schedule_Term_Peak_Id			INT
	@Time_Of_Use_Schedule_Term_Peak_Dtl_Ids		VARCHAR(MAX)


OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------

BEGIN TRAN
Exec dbo.TOU_Schedule_Term_Peak_Del 1,'16,20'
ROLLBACK TRAN
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	BCH			Balaraju Chalumuri

MODIFICATIONS

	Initials	Date		 Modification
------------------------------------------------------------------------------
	CPE			2012-07-25  Created
	BCH			2012-09-06	Given working example.
******/ 
CREATE PROCEDURE dbo.TOU_Schedule_Term_Peak_Del
      ( 
       @Time_Of_Use_Schedule_Term_Peak_Id INT
      ,@Time_Of_Use_Schedule_Term_Peak_Dtl_Ids VARCHAR(MAX) )
AS 
BEGIN
      SET NOCOUNT ON;
	
      DECLARE @Peak_Dtl_Ids TABLE ( Dtl_Id INT )

      INSERT      @Peak_Dtl_Ids
                  SELECT
                        Segments
                  FROM
                        ufn_Split(@Time_Of_Use_Schedule_Term_Peak_Dtl_Ids, ',')                   

      BEGIN TRY
            BEGIN TRAN

            DELETE
                  TOUSTPD
            FROM
                  dbo.Time_of_Use_Schedule_Term_Peak_Dtl TOUSTPD
                  JOIN @Peak_Dtl_Ids PDI
                        ON TOUSTPD.Time_Of_Use_Schedule_Term_Peak_Dtl_Id = PDI.Dtl_Id
	  
            DELETE
                  TOUSTP
            FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak TOUSTP
            WHERE
                  TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id = @Time_Of_Use_Schedule_Term_Peak_Id
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    dbo.Time_of_Use_Schedule_Term_Peak_Dtl
                                   WHERE
                                    Time_Of_Use_Schedule_Term_Peak_Id = TOUSTP.Time_Of_Use_Schedule_Term_Peak_Id )
	  
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC dbo.usp_RethrowError
      END CATCH
END

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Peak_Del] TO [CBMSApplication]
GO
