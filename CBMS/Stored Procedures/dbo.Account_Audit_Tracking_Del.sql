SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******          

NAME: [DBO].[ Account_Audit_Tracking_Del]  
     
DESCRIPTION: 
	To Delete Account Audit Tracking Information for selected Tracking Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@TRACKING_ID	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN  
		EXEC Account_Audit_Tracking_Del  1140
	ROLLBACK TRAN
	
	EXEC  Account_Audit_Tracking_Del  116867
	

	SELECT * FROM Account_Audit_Tracking
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			26-MAY-10	CREATED     
*/  

CREATE PROCEDURE dbo.Account_Audit_Tracking_Del
    (
      @TRACKING_ID INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
   	FROM	
		dbo.Account_Audit_Tracking
	WHERE 
		TRACKING_ID = @TRACKING_ID
			
END
GO
GRANT EXECUTE ON  [dbo].[Account_Audit_Tracking_Del] TO [CBMSApplication]
GO
