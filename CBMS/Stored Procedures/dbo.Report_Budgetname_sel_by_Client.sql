SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_Budgetname_sel_by_Client
	
DESCRIPTION:
	This report is to get All the Budget Name  by Client Filter
	
INPUT PARAMETERS:
Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
@Client_id_List	VARCHAR(MAX)
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_Budgetname_sel_by_Client '10012'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   08/19/2010	Created
*/
CREATE PROC [dbo].[Report_Budgetname_sel_by_Client]
    @Client_id_List VARCHAR(MAX)
AS 
    BEGIN   
  
        SET NOCOUNT ON   
          
        SELECT
            bd.BUDGET_ID
          , bd.BUDGET_NAME
        FROM
            dbo.BUDGET bd
            JOIN dbo.ufn_split(@Client_id_List, ',') ufn_budget_tmp
                ON CAST(ufn_budget_tmp.Segments AS INT) = bd.CLIENT_ID
        ORDER BY
            bd.BUDGET_ID            
       
  
    END


GO
GRANT EXECUTE ON  [dbo].[Report_Budgetname_sel_by_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Budgetname_sel_by_Client] TO [CBMSApplication]
GO
