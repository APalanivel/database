SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_SAD_DELETE_CONTACT_INFO_NON_PRIMARY_VENDOR_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                    DataType          Default     Description    
---------------------------------------------------------------------------------    
@vendormapId                  int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec DBO.GET_UPDATE_SITE_INFO_P_M 
--@site_id = 29

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_SAD_DELETE_CONTACT_INFO_NON_PRIMARY_VENDOR_P

@vendormapId int

AS
	
SET NOCOUNT ON

	DELETE 
		SR_SUPPLIER_CONTACT_VENDOR_MAP
	WHERE   
		SR_SUPPLIER_CONTACT_VENDOR_MAP_id = @vendormapId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_CONTACT_INFO_NON_PRIMARY_VENDOR_P] TO [CBMSApplication]
GO
