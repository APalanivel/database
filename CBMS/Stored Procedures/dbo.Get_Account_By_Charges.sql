SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 Get_Account_By_Charges
   
DESCRIPTION:  
  Get Account Number by charges, based on regulated / contract charges passed.
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @Regulated_Charge		BIT ,
 @Contract_Charge		BIT ,
 @Client_Id				INT	,
 @Site_Id				INT ,
 @Contract_ID			INT 
                                       
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 SET STATISTICS IO ON  
 EXEC dbo.Get_Account_By_Charges 0,1 ,102 
 
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
    PRG   Prasanna Raghavendra G
  
MODIFICATIONS:  
 Initials Date		Modification  
------------------------------------------------------------  
PRG		08-APR-2019	Created For Budget 2.0  
RKV     2019-05-27  Added New parameter Rate_ID
******/

CREATE PROC [dbo].[Get_Account_By_Charges]
     (
         @Regulated_Charge SMALLINT
         , @Contract_Charge SMALLINT
         , @Client_Id INT
         , @Site_Id INT = NULL
         , @Contract_ID INT = NULL
         , @Rate_ID INT = NULL

     )
AS
    BEGIN

        SELECT
            cha.Account_Id
            , cha.Display_Account_Number Account_Number
        FROM
            Core.Client_Hier AS ch
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN dbo.CONTRACT AS c
                ON cha.Supplier_Contract_ID = c.CONTRACT_ID
        WHERE
            (   (   ch.Client_Id = @Client_Id
                    AND ch.Site_Id = @Site_Id)
                OR  (   ch.Client_Id = @Client_Id
                        AND c.CONTRACT_ID = @Contract_ID))
            AND Account_Type = CASE WHEN (   (   @Regulated_Charge = 1
                                                 AND COALESCE(@Contract_Charge, 0) = 0)
                                             OR (   @Regulated_Charge = 1
                                                    AND @Contract_Charge = 1)) THEN 'Utility'
                                   WHEN (   @Contract_Charge = 1
                                            AND COALESCE(@Regulated_Charge, 0) = 0) THEN 'Supplier'
                               END
            AND (   @Rate_ID IS NULL
                    OR  cha.Rate_Id = @Rate_ID)
        GROUP BY
            cha.Account_Id
            , cha.Display_Account_Number
        ORDER BY
            cha.Display_Account_Number;
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Account_By_Charges] TO [CBMSApplication]
GO
