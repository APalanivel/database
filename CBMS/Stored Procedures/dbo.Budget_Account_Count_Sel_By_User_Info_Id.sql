
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********

NAME:                         
     dbo.Budget_Account_Count_Sel_By_User_Info_Id

DESCRIPTION:                                        

 INPUT PARAMETERS:                                        
	Name            DataType         Default       Description                                      
------------------------------------------------------------------
	@User_Info_Id   INT                              

 OUTPUT PARAMETERS:                                        
	Name            DataType         Default       Description                                      
------------------------------------------------------------------

USAGE EXAMPLES:                                                          
------------------------------------------------------------------

	SELECT TOP 10 * FROM dbo.Budget_Account_Queue baq 
	INNER JOIN dbo.USER_INFO ui ON baq.Queue_Id = ui.QUEUE_ID
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 60681
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 60681
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 60681
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 20713
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 20713
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 20713
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 15
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 15
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 15
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 7090
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 7090
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 7090
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 75
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 75
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 75
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 1094
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 1094
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 1094
	
	EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 26958
	EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 26958
	EXEC dbo.Budget_Account_Count_Sel_By_User_Info_Id 26958


AUTHOR INITIALS:                                      
	Initials	Name                                      
------------------------------------------------------------------
	RR			Raghu Reddy	  
 
 MODIFICATIONS:                                    
	Initials	Date        Modification                                    
---------------------------------------------------------------------------------------------------------------                                    
	RR			2017-05-26	SE2017-26 - Created to get count active budget accounts assigned to user queue
	RR			2017-05-26	MAINT-5550 - C/D and tariff accounts do not display in user queue, the script should exclude this type of accounts, so
							used the same scripts used in dashboard to get the count as these scripts have already this logic. C/D and tariff accounts
							will be removed from queue if the user moved to history.
*********/

CREATE PROCEDURE [dbo].[Budget_Account_Count_Sel_By_User_Info_Id] ( @User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Budget_Account_Cnt INT
      
      DECLARE @Tbl_RA AS TABLE
            ( 
             ToatlAccounts INT
            ,DueDateAccounts INT
            ,ReviewAccounts INT
            ,ReviewDueAccounts INT )
      DECLARE @Tbl_SA AS TABLE
            ( 
             ToatlAccounts INT
            ,DueDateAccounts INT
            ,DistibutionAccounts INT
            ,DistibutionDueAccounts INT )
      
      --SELECT
      --      @Budget_Account_Cnt = COUNT(baq.Budget_Account_Id)
      --FROM
      --      dbo.BUDGET_ACCOUNT ba
      --      INNER JOIN dbo.Budget_Account_Queue baq
      --            ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
      --      INNER JOIN dbo.USER_INFO ui
      --            ON baq.Queue_Id = ui.QUEUE_ID
      --WHERE
      --      ui.USER_INFO_ID = @User_Info_Id
      --      AND ba.IS_DELETED = 0
      
      
      INSERT      INTO @Tbl_RA
                  ( 
                   ToatlAccounts
                  ,DueDateAccounts
                  ,ReviewAccounts
                  ,ReviewDueAccounts )
                  EXEC dbo.BUDGET_GET_RA_BUDGET_QUEUE_DETAILS_P 
                        @User_Info_Id
      
      INSERT      INTO @Tbl_SA
                  ( 
                   ToatlAccounts
                  ,DueDateAccounts
                  ,DistibutionAccounts
                  ,DistibutionDueAccounts )
                  EXEC dbo.BUDGET_GET_SA_BUDGET_QUEUE_DETAILS_P 
                        @User_Info_Id
            
      SELECT
            @Budget_Account_Cnt = ISNULL(@Budget_Account_Cnt, 0) + ISNULL(ToatlAccounts, 0) + ISNULL(ReviewAccounts, 0)
      FROM
            @Tbl_RA
      SELECT
            @Budget_Account_Cnt = ISNULL(@Budget_Account_Cnt, 0) + ISNULL(ToatlAccounts, 0) + ISNULL(DistibutionAccounts, 0)
      FROM
            @Tbl_SA
      
      SELECT
            ISNULL(@Budget_Account_Cnt, 0) AS Budget_Account_Count
      
      
END

;
GO

GRANT EXECUTE ON  [dbo].[Budget_Account_Count_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
