SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******            
NAME:            
 [DBO].[SUPPLIER_ACCOUNTS_SEL]    
       
DESCRIPTION:            
 TO GET THE SUPPLIER_ACCOUNT DETAILS FOR THE SELECTED CONTRACT ID  
        
INPUT PARAMETERS:            
NAME					DATATYPE		 DEFAULT				DESCRIPTION            
---------------------------------------------------------------------------------            
@CONTRACT_ID			INT  
  
                  
OUTPUT PARAMETERS:            
NAME					DATATYPE		 DEFAULT				DESCRIPTION            
---------------------------------------------------------------------------------            
USAGE EXAMPLES:            
---------------------------------------------------------------------------------            
    
 EXEC SUPPLIER_ACCOUNTS_SEL  36608  

 EXEC SUPPLIER_ACCOUNTS_SEL 172906 
       
AUTHOR INITIALS:            
INITIALS		NAME            
---------------------------------------------------------------------------------            
MGB				BHASKARAN GOPALAKRISHNAN    
DMR				Deana Ritter  
            
MODIFICATIONS             
INITIALS	DATE			MODIFICATION            
---------------------------------------------------------------------------------            
MGB			21-AUG-09		CREATED       
DMR			10/9/2009		Removed Contract table and remapped all Contract ID joins to SAMM  
MGB			10/30/2009		included meterid in select statement  
MGB			03-Nov-09		included RA_WATCH_LIST while selecting records.
NR			209-07-03		Add Contract - Changed Inner to left on Entity.SERVICE_LEVEL_TYPE_ID. 
   
*/
CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNTS_SEL]
    (
        @CONTRACT_ID INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            MAP.ACCOUNT_ID ACCOUNT_ID
            , ACC.ACCOUNT_NUMBER SUPP_ACC_NUMBER
            , M.METER_NUMBER METER_NUMBER
            , M.METER_ID METER_ID
            , CONVERT(VARCHAR(10), ACC.Supplier_Account_Begin_Dt, 101) Supplier_Account_Begin_Dt
            , CONVERT(VARCHAR(10), ACC.Supplier_Account_End_Dt, 101) Supplier_Account_End_Dt
            , UTILACC.ACCOUNT_NUMBER UTIL_ACCOUNT_NUMBER
            , ACC.RA_WATCH_LIST RA_WATCH_LIST
            , ACC.DM_WATCH_LIST DM_WATCH_LIST
            , ACC.NOT_MANAGED NOT_MANAGED
            , ACC.NOT_EXPECTED NOT_EXPECTED
            , ACC.SERVICE_LEVEL_TYPE_ID SERVICE_LEVEL_TYPE_ID
            , ENT.ENTITY_NAME ENTITY_NAME
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP MAP
            JOIN dbo.ACCOUNT ACC
                ON ACC.ACCOUNT_ID = MAP.ACCOUNT_ID
            JOIN dbo.METER M
                ON MAP.METER_ID = M.METER_ID
            JOIN dbo.ACCOUNT UTILACC
                ON M.ACCOUNT_ID = UTILACC.ACCOUNT_ID
            LEFT JOIN dbo.ENTITY ENT
                ON ACC.SERVICE_LEVEL_TYPE_ID = ENT.ENTITY_ID
        WHERE
            MAP.Contract_ID = @CONTRACT_ID
            AND MAP.IS_HISTORY = 0
        ORDER BY
            MAP.ACCOUNT_ID;

    END;
GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNTS_SEL] TO [CBMSApplication]
GO
