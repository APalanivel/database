SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_SITES_UNDER_CLIENT_FOR_INITIATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE dbo.BUDGET_GET_SITES_UNDER_CLIENT_FOR_INITIATE_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
select	site.SITE_ID,
	viewSite.SITE_NAME

from	SITE site,	
	DIVISION division ,
	VWSITENAME viewSite,
	client c

where 	c.CLIENT_ID= @clientId 
	AND site.DIVISION_ID=division.DIVISION_ID 	
	and site.SITE_ID = viewSite.SITE_ID
	and site.closed=0 and site.not_managed=0
	and division.client_id = c.client_id

order by viewSite.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SITES_UNDER_CLIENT_FOR_INITIATE_P] TO [CBMSApplication]
GO
