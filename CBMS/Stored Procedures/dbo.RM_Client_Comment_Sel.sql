SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	
		dbo.RM_Client_Comment_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.RM_Client_Comment_Sel  10003
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-28  Global Risk Management - Created
     
    
******/  

CREATE PROCEDURE [dbo].[RM_Client_Comment_Sel] ( @Client_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;	
      
      SELECT
            comm.Comment_Text
           ,REPLACE(CONVERT(VARCHAR(20), rcc.Last_Change_Ts, 106), ' ', '-') AS Comment_Date
           ,REPLACE(CONVERT(VARCHAR(20), rcc.Last_Change_Ts, 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), rcc.Last_Change_Ts, 100), 7)) + ' EST' AS Comment_Date_Time
           ,ui.USERNAME AS Comment_By
           ,rcc.Cbms_Image_Id
           ,ci.CBMS_DOC_ID
      FROM
            dbo.RM_Client_Comment rcc
            INNER JOIN dbo.Comment comm
                  ON rcc.Comment_Id = comm.Comment_ID
            INNER JOIN dbo.USER_INFO ui
                  ON rcc.Updated_User_Id = ui.USER_INFO_ID
            LEFT JOIN dbo.cbms_image ci
                  ON rcc.Cbms_Image_Id = ci.CBMS_IMAGE_ID
      WHERE
            rcc.Client_Id = @Client_Id
      ORDER BY
            rcc.Last_Change_Ts DESC
  
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Comment_Sel] TO [CBMSApplication]
GO
