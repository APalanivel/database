SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                            
Name:   dbo.EC_Calc_Val_Sel                                     
                                            
Description:                                            
   This sproc to get the calc details for a given id                                    
                                                         
 Input Parameters:                                            
    Name    DataType     Default   Description                                              
------------------------------------------------------------------------------------------------------------                              
 @Country_Id   INT          NULL                              
    @State_Id   INT          NULL                              
    @Commodity_Id       INT          NULL                                 
    @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_EC_Calc_Val_Meter_Attribute_Value                               
    @Start_Index       INT           1                                                      
    @End_Index     INT           2147483647                                
    @Total_Count       INT           0                                                     
                               
 Output Parameters:                                                  
    Name        DataType   Default   Description                                              
------------------------------------------------------------------------------------------------------------                                             
                                            
 Usage Examples:                                                
------------------------------------------------------------------------------------------------------------                              
                              
 SELECT * FROM dbo.EC_Calc_Val a JOIN dbo.EC_Calc_Val_Meter_Attribute b ON a.EC_Calc_Val_Id = b.EC_Calc_Val_Id                              
                              
 DECLARE  @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_EC_Calc_Val_Meter_Attribute_Value                              
 EXEC dbo.EC_Calc_Val_Sel NULL,124,290,@tvp_EC_Calc_Val_Meter_Attribute_Value                              
                               
 DECLARE  @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_EC_Calc_Val_Meter_Attribute_Value                              
 INSERT INTO @tvp_EC_Calc_Val_Meter_Attribute_Value VALUES (61,115),(60,133)                              
     EXEC dbo.EC_Calc_Val_Sel NULL,25,290,@tvp_EC_Calc_Val_Meter_Attribute_Value                              
                                   
                               
                              
                                 
Author Initials:                         
                                           
Initials  Name                                            
----------------------------------------------------------------------------------------                                              
 NR    Narayana Reddy                               
 RKV    Ravi Kumar Vegesna                         
 VRV    Venkat Reddy vanga      
 NM  Nagaraju Muppa                        
                                             
 Modifications:                                            
    Initials        Date  Modification                                            
----------------------------------------------------------------------------------------                                              
    NR  2015-05-18  Created For AS400.                                       
    RKV 2015-10-01  Added new join Ec_Calc_Val_Bucket_Map as part of AS400-PII                               
    RKV 2016-11-21  MAINT 4563  Added Group_Type_Name in the result set                            
    VRV 2019-09-23  Maint-9297 removed the row_number from group by clause              
	VRV 2019-11-11  SE2017-875 Add the new logic to get the null meterattribute calc vals in result set      
	NM  2020-03-17 MAINT-10024 added the EC_Meter_Attribute table to get the null meterattribute calc vals as column.
	NM	2020-03-25 MAINT-9943  Added new bit varaible which will store the tvp null values like if all the values are null it  will retrun 1 
								then we will show all the ec_calc_vals for that country  state commodity else it will work as it is and return the CalcVals when Meter Attributes set as True.                
                              
                                      
******/

CREATE PROCEDURE [dbo].[EC_Calc_Val_Sel]
(
    @Country_Id INT = NULL,
    @State_Id INT = NULL,
    @Commodity_Id INT = NULL,
    @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_EC_Calc_Val_Meter_Attribute_Value READONLY,
    @Start_Index INT = 1,
    @End_Index INT = 2147483647,
    @Total_Count INT = 0
)
AS
BEGIN
    SET NOCOUNT ON;



    DECLARE @EC_Meter_Attribute_Ids VARCHAR(MAX) = '',
            @EC_Meter_Attribute_Ids_cols VARCHAR(MAX) = '',
            @SQLString NVARCHAR(MAX),
            @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt INT,
            @tvp_EC_Calc_Val_Meter_Attribute_Value_NOTNULL_cnt INT,
	/**** MAINT-9943 Added new bit varaible which will store the tvp null values like if all the values are null it  will retrun 1 ***/
            @Is_All_EC_Meter_Attribute_Value_Null BIT = 1;


    CREATE TABLE #EC_Calc_Val_Id
    (
        Ec_Calc_Val_Id INT,
        EC_Meter_Attribute_Id INT
    );




    CREATE TABLE #Mathced_EC_Calc_Val_Id
    (
        Ec_Calc_Val_Id INT
    );

    SELECT @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt = COUNT(1)
    FROM @tvp_EC_Calc_Val_Meter_Attribute_Value;

    SELECT @tvp_EC_Calc_Val_Meter_Attribute_Value_NOTNULL_cnt = COUNT(1)
    FROM @tvp_EC_Calc_Val_Meter_Attribute_Value
    WHERE EC_Meter_Attribute_Value IS NOT NULL;

/**** MAINT-9943 Added ***/
    SELECT @Is_All_EC_Meter_Attribute_Value_Null = 0
    FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tecvmav
    WHERE tecvmav.EC_Meter_Attribute_Value IS NOT NULL;

    SELECT @EC_Meter_Attribute_Ids = STUFF(
    (
        SELECT ',[' + CAST(EMA.EC_Meter_Attribute_Id AS NVARCHAR(30)) + ']'
        FROM dbo.EC_Meter_Attribute EMA
            LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                ON EMA.EC_Meter_Attribute_Id = ecvma.EC_Meter_Attribute_Id
            LEFT JOIN EC_Calc_Val ecv
                ON ecv.EC_Calc_Val_Id = ecvma.EC_Calc_Val_Id
            INNER JOIN dbo.STATE s
                ON EMA.State_Id = s.STATE_ID
            INNER JOIN dbo.COUNTRY C
                ON s.COUNTRY_ID = C.COUNTRY_ID
            INNER JOIN dbo.Commodity com
                ON EMA.Commodity_Id = com.Commodity_Id
        WHERE (
                  @Country_Id IS NULL
                  OR C.COUNTRY_ID = @Country_Id
              )
              AND
              (
                  @State_Id IS NULL
                  OR s.STATE_ID = @State_Id
              )
              AND
              (
                  @Commodity_Id IS NULL
                  OR com.Commodity_Id = @Commodity_Id
              )
              AND
              (
                  @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt = 0
                  OR EXISTS
    (
        SELECT 1
        FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_ecvma
        WHERE EMA.EC_Meter_Attribute_Id = tvp_ecvma.EC_Meter_Attribute_Id
              AND
              (
                  tvp_ecvma.EC_Meter_Attribute_Value IS NULL
                  OR ecvma.EC_Meter_Attribute_Value = tvp_ecvma.EC_Meter_Attribute_Value
              )
    )
              )
        GROUP BY EMA.EC_Meter_Attribute_Id
        FOR XML PATH(''), TYPE
    ).value('.', 'NVARCHAR(MAX)'),
    1   ,
    1   ,
    ' '
                                          );





    SELECT @EC_Meter_Attribute_Ids_cols
        = STUFF(
    (
        SELECT ',[' + CAST(EMA.EC_Meter_Attribute_Id AS NVARCHAR(30)) + '] AS ID_'
               + CAST(EMA.EC_Meter_Attribute_Id AS NVARCHAR(30))
        FROM dbo.EC_Meter_Attribute EMA
            LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                ON EMA.EC_Meter_Attribute_Id = ecvma.EC_Meter_Attribute_Id
            LEFT JOIN EC_Calc_Val ecv
                ON ecv.EC_Calc_Val_Id = ecvma.EC_Calc_Val_Id
            INNER JOIN dbo.STATE s
                ON EMA.State_Id = s.STATE_ID
            INNER JOIN dbo.COUNTRY C
                ON s.COUNTRY_ID = C.COUNTRY_ID
            INNER JOIN dbo.Commodity com
                ON EMA.Commodity_Id = com.Commodity_Id
        WHERE (
                  @Country_Id IS NULL
                  OR C.COUNTRY_ID = @Country_Id
              )
              AND
              (
                  @State_Id IS NULL
                  OR s.STATE_ID = @State_Id
              )
              AND
              (
                  @Commodity_Id IS NULL
                  OR com.Commodity_Id = @Commodity_Id
              )
              AND
              (
                  @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt = 0
                  OR EXISTS
    (
        SELECT 1
        FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_ecvma
        WHERE EMA.EC_Meter_Attribute_Id = tvp_ecvma.EC_Meter_Attribute_Id
              AND
              (
                  tvp_ecvma.EC_Meter_Attribute_Value IS NULL
                  OR ecvma.EC_Meter_Attribute_Value = tvp_ecvma.EC_Meter_Attribute_Value
              )
    )
              )
        GROUP BY EMA.EC_Meter_Attribute_Id
        FOR XML PATH(''), TYPE
    ).value('.', 'NVARCHAR(MAX)'),
    1   ,
    1   ,
    ' '
               );




    INSERT INTO #Mathced_EC_Calc_Val_Id
    SELECT ecvma.EC_Calc_Val_Id
    FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_ecvma
        INNER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
            ON ecvma.EC_Meter_Attribute_Value = tvp_ecvma.EC_Meter_Attribute_Value
               AND tvp_ecvma.EC_Meter_Attribute_Id = ecvma.EC_Meter_Attribute_Id
    GROUP BY ecvma.EC_Calc_Val_Id
    HAVING COUNT(1) = @tvp_EC_Calc_Val_Meter_Attribute_Value_NOTNULL_cnt;



    INSERT INTO #Mathced_EC_Calc_Val_Id
    SELECT ecvma.EC_Calc_Val_Id
    FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_ecvma
        INNER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
            ON tvp_ecvma.EC_Meter_Attribute_Value IS NULL
               AND tvp_ecvma.EC_Meter_Attribute_Id = ecvma.EC_Meter_Attribute_Id
    WHERE (
              EXISTS
    (
        SELECT 1
        FROM #Mathced_EC_Calc_Val_Id mecvi
        WHERE ecvma.EC_Calc_Val_Id = mecvi.Ec_Calc_Val_Id
    )
              OR @tvp_EC_Calc_Val_Meter_Attribute_Value_NOTNULL_cnt = 0
          );

    INSERT INTO #EC_Calc_Val_Id
    (
        Ec_Calc_Val_Id,
        EC_Meter_Attribute_Id
    )
    SELECT ecv.EC_Calc_Val_Id,
           ecvma.EC_Meter_Attribute_Id
    FROM dbo.EC_Calc_Val ecv
        LEFT OUTER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
            ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
        LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
            ON ecv.EC_Calc_Val_Id = ecvma.EC_Calc_Val_Id
        INNER JOIN dbo.STATE s
            ON ecv.State_Id = s.STATE_ID
        INNER JOIN dbo.COUNTRY C
            ON s.COUNTRY_ID = C.COUNTRY_ID
        LEFT OUTER JOIN dbo.Bucket_Master bm
            ON bm.Bucket_Master_Id = ecvbm.Bucket_Master_Id
        INNER JOIN dbo.Commodity com
            ON ecv.Commodity_Id = com.Commodity_Id
    WHERE (
              @Country_Id IS NULL
              OR C.COUNTRY_ID = @Country_Id
          )
          AND
          (
              @State_Id IS NULL
              OR s.STATE_ID = @State_Id
          )
          AND
          (
              @Commodity_Id IS NULL
              OR com.Commodity_Id = @Commodity_Id
          )
          AND
          (
              @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt = 0
              OR EXISTS
    (
        SELECT 1
        FROM @tvp_EC_Calc_Val_Meter_Attribute_Value tvp_ecvma
        WHERE (
                  tvp_ecvma.EC_Meter_Attribute_Value IS NULL
                  OR ecvma.EC_Meter_Attribute_Id = tvp_ecvma.EC_Meter_Attribute_Id
              )
              AND
              (
                  tvp_ecvma.EC_Meter_Attribute_Value IS NULL
                  OR ecvma.EC_Meter_Attribute_Value = tvp_ecvma.EC_Meter_Attribute_Value
              )
    )
          )
    GROUP BY ecv.EC_Calc_Val_Id,
             ecvma.EC_Meter_Attribute_Id;

    IF @Total_Count = 0
    BEGIN


        SELECT @Total_Count = COUNT(1)
        FROM dbo.EC_Calc_Val ecv
            LEFT OUTER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm
                ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id
            LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                ON ecv.EC_Calc_Val_Id = ecvma.EC_Calc_Val_Id
            INNER JOIN dbo.STATE s
                ON ecv.State_Id = s.STATE_ID
            INNER JOIN dbo.COUNTRY C
                ON s.COUNTRY_ID = C.COUNTRY_ID
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = ecvbm.Bucket_Master_Id
            INNER JOIN dbo.Commodity com
                ON ecv.Commodity_Id = com.Commodity_Id
            CROSS APPLY
        (
            SELECT eisbm.Sub_Bucket_Name + ', '
            FROM dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                    ON ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
            WHERE ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id
            GROUP BY eisbm.Sub_Bucket_Name
            FOR XML PATH('')
        ) cvsb(cvsb_list)
        WHERE (
                  @Country_Id IS NULL
                  OR C.COUNTRY_ID = @Country_Id
              )
              AND
              (
                  @State_Id IS NULL
                  OR s.STATE_ID = @State_Id
              )
              AND
              (
                  @Commodity_Id IS NULL
                  OR com.Commodity_Id = @Commodity_Id
              )
              AND
              (
                  @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt = 0
                  OR EXISTS
        (
            SELECT 1
            FROM #EC_Calc_Val_Id ecvi
                INNER JOIN #Mathced_EC_Calc_Val_Id mecvi
                    ON mecvi.Ec_Calc_Val_Id = ecvi.Ec_Calc_Val_Id
            WHERE ecvma.EC_Calc_Val_Id = ecvi.Ec_Calc_Val_Id
                  AND ecvma.EC_Meter_Attribute_Id = ecvi.EC_Meter_Attribute_Id
        )
              );


    END;




    SET @SQLString
        = N'                              
      ;WITH  Cte_Calc_Val                              
              AS ( SELECT                       
                        ecv.EC_Calc_Val_Id                              
                       ,ecv.Calc_Value_Name                              
                       ,SP_Cd.Code_Value + '' '' + SPOP_Cd.Code_Value + '' ''  + CAST(ecv.Starting_Period_Operand AS VARCHAR(10)) AS Start_Month                              
                       ,EP_Cd.Code_Value + '' ''  + EPOP_Cd.Code_Value + '' ''  + CAST(ecv.End_Period_Operand AS VARCHAR(10)) AS End_Month                              
                       ,bm.Bucket_Name                              
                       ,[Sub_Bucket_Name] = LEFT(cvsb.cvsb_list, LEN(cvsb.cvsb_list) - 1)                              
                       ,Aggr_Cd.Code_Value AS Logic                
                       ,c.COUNTRY_NAME                              
                       ,s.STATE_NAME                              
                       ,com.Commodity_Name                              
                       ,ecvma.EC_Meter_Attribute_Id                              
                       ,ecma.EC_Meter_Attribute_Name                              
                       ,ecvma.EC_Meter_Attribute_Value                              
                       ,ecv.Ec_Account_Group_Type_Id                              
                       ,ROW_NUMBER() OVER ( ORDER BY ecv.Calc_Value_Name) AS Row_Num                              
                   FROM                              
                        dbo.EC_Calc_Val ecv                              
                       LEFT OUTER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm                      
                        ON ecv.EC_Calc_Val_Id = ecvbm.Ec_Calc_Val_Id                              
                       LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma                              
                              ON ecv.EC_Calc_Val_Id = ecvma.EC_Calc_Val_Id                              
                        LEFT OUTER JOIN dbo.EC_Meter_Attribute ecma                              
                              ON ecvma.EC_Meter_Attribute_Id = ecma.EC_Meter_Attribute_Id                              
                        INNER JOIN dbo.STATE s                              
                              ON ecv.State_Id = s.STATE_ID                              
                        INNER JOIN dbo.COUNTRY c                              
                              ON s.COUNTRY_ID = c.COUNTRY_ID                              
                        LEFT OUTER JOIN dbo.Bucket_Master bm                              
                              ON bm.Bucket_Master_Id =ecvbm.Bucket_Master_Id                              
                        INNER JOIN dbo.Commodity com                              
                              ON ecv.Commodity_Id = com.Commodity_Id                              
                        INNER JOIN dbo.Code SP_Cd                              
                              ON SP_Cd.Code_Id = ecv.Starting_Period_Cd                              
        INNER JOIN dbo.Code SPOP_Cd                              
                              ON SPOP_Cd.Code_Id = ecv.Starting_Period_Operator_Cd                              
                        INNER JOIN code EP_Cd                      
                              ON EP_Cd.Code_Id = ecv.End_Period_Cd                              
                        INNER JOIN dbo.Code EPOP_Cd                              
                              ON EPOP_Cd.Code_Id = ecv.End_Period_Operator_Cd                              
                        INNER JOIN dbo.Code Aggr_Cd                              
         ON Aggr_Cd.Code_Id = ecv.Aggregation_Cd                              
                   CROSS APPLY ( SELECT                              
            eisbm.Sub_Bucket_Name + '', ''                              
                               FROM                              
                                                                                
                                             dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm                              
             INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm                              
             ON ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id                              
            WHERE                              
              ecvbsbm.Ec_Calc_Val_Bucket_Map_Id = ecvbm.Ec_Calc_Val_Bucket_Map_Id                              
                                                     
                                           
                                                                                    
                    GROUP BY                              
                                                      eisbm.Sub_Bucket_Name                              
           FOR                              
                                                  XML PATH('''') ) cvsb ( cvsb_list )                              
                   WHERE                              
                        ( @Country_Id_D IS NULL                              
                          OR c.COUNTRY_ID = @Country_Id_D )                              
                        AND ( @State_Id_D IS NULL                              
                              OR s.STATE_ID = @State_Id_D )                              
                        AND ( @Commodity_Id_D IS NULL                              
                              OR com.Commodity_Id = @Commodity_Id_D )                              
                        AND ((' + CAST(@Is_All_EC_Meter_Attribute_Value_Null AS CHAR(1))
          + N' = 1 or @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt_D = 0)                              
                              OR EXISTS ( SELECT                              
                                                1                              
                  FROM                              
                                                #EC_Calc_Val_Id ecvi                              
                                       INNER JOIN #Mathced_EC_Calc_Val_Id mecvi                              
               ON mecvi.EC_Calc_Val_Id  = ecvi.EC_Calc_Val_Id     
       
                                 
                                                                             
                                          WHERE                              
            ecvma.EC_Calc_Val_Id = ecvi.Ec_Calc_Val_Id                               
                                                AND ecvma.EC_Meter_Attribute_Id = ecvi.EC_Meter_Attribute_Id                               
                                   ) )                              
                                                
                        )                              
                                         
            SELECT                              
                  EC_Calc_Val_Id              
                 ,Calc_Value_Name                              
                 ,Start_Month                              
                 ,End_Month                              
                 ,Bucket_Name                              
                 ,Sub_Bucket_Name                              
                 ,Logic                              
                 ,COUNTRY_NAME                              
                 ,STATE_NAME                              
                 ,Commodity_Name                              
                 ,@Total_Count_D AS Total_Rows                           
     ,Group_Type_Name ' + CASE
                              WHEN @EC_Meter_Attribute_Ids_cols IS NOT NULL
                                   AND LEN(@EC_Meter_Attribute_Ids_cols) > 0 THEN
                                  +'                              
                 ,'               + @EC_Meter_Attribute_Ids_cols
                              ELSE
                                  +''
                          END
          + N'                               
            FROM                              
                  ( SELECT                              
                        ccv.EC_Calc_Val_Id                              
                       ,ccv.Calc_Value_Name                              
        ,ccv.Start_Month                              
                       ,ccv.End_Month                              
          ,ccv.Bucket_Name                              
                       ,ccv.Sub_Bucket_Name                              
                       ,ccv.Logic                              
                       ,ccv.COUNTRY_NAME                              
                       ,ccv.STATE_NAME                              
                       ,ccv.Commodity_Name                              
                       ,ccv.EC_Meter_Attribute_Id                              
                       ,ccv.EC_Meter_Attribute_Value                              
                       ,@Total_Count_D AS Total_Rows                               
                       ,eagt.Group_Type_Name                              
                    FROM                              
                        Cte_Calc_Val ccv                        
                        LEFT OUTER JOIN Ec_Account_Group_Type eagt                              
                        ON eagt.Ec_Account_Group_Type_Id = ccv.Ec_Account_Group_Type_Id                              
                                                      
                    WHERE                              
                        ccv.Row_Num BETWEEN @Start_Index_D AND @End_Index_D ) up '
          + CASE
                WHEN @EC_Meter_Attribute_Ids IS NOT NULL
                     AND LEN(@EC_Meter_Attribute_Ids) > 0 THEN
                    +'PIVOT ( MAX(EC_Meter_Attribute_Value) FOR EC_Meter_Attribute_ID IN ( ' + @EC_Meter_Attribute_Ids
                    + '))                              
                              
                       AS pvt'
                ELSE
                    +' GROUP BY  EC_Calc_Val_Id                              
                            ,Calc_Value_Name                              
                            ,Start_Month                              
                            ,End_Month                              
                            ,Bucket_Name                              
                            ,Sub_Bucket_Name                              
                            ,Logic                              
                            ,COUNTRY_NAME                              
                            ,STATE_NAME                              
                            ,Commodity_Name                              
                                                 
       ,Group_Type_Name'
            END;

    EXEC sp_executesql @SQLString,
                       N'@tvp_EC_Calc_Val_Meter_Attribute_Value_D tvp_EC_Calc_Val_Meter_Attribute_Value READONLY                              
               ,@Total_Count_D INT, @Start_Index_D INT, @End_Index_D INT, @Country_Id_D INT,@State_Id_D INT,@Commodity_Id_D INT             
               ,@tvp_EC_Calc_Val_Meter_Attribute_Value_cnt_D INT  ',
                       @Total_Count_D = @Total_Count,
                       @Start_Index_D = @Start_Index,
                       @End_Index_D = @End_Index,
                       @Country_Id_D = @Country_Id,
                       @State_Id_D = @State_Id,
                       @Commodity_Id_D = @Commodity_Id,
                       @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt_D = @tvp_EC_Calc_Val_Meter_Attribute_Value_cnt;

    DROP TABLE #Mathced_EC_Calc_Val_Id,
               #EC_Calc_Val_Id;
END;;;;
GO


GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Sel] TO [CBMSApplication]
GO
