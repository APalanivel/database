SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: Dbo.Sr_Load_Profile_Default_Setup_Del  
     
DESCRIPTION: 
	It Deletes	Sr Load Profile Default Setup for Selected Sr Load Profile Default Setup Id. 
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------------------         
	@Sr_Load_Profile_Default_Setup_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Sr_Load_Profile_Default_Setup_Del  288

	ROLLBACK TRAN
	
		
		SELECT  * FROM dbo.sr_load_profile_default_setup
					   WHERE Sr_Load_Profile_Default_Setup_Id = 288

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			24-August-10	CREATED

*/
CREATE PROCEDURE dbo.Sr_Load_Profile_Default_Setup_Del
    (
      @Sr_Load_Profile_Default_Setup_Id	INT
     )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Sr_Load_Profile_Default_Setup
	WHERE
		Sr_Load_Profile_Default_Setup_Id = @Sr_Load_Profile_Default_Setup_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Load_Profile_Default_Setup_Del] TO [CBMSApplication]
GO
