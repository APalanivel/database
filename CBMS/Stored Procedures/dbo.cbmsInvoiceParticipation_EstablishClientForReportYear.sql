SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE     PROCEDURE [dbo].[cbmsInvoiceParticipation_EstablishClientForReportYear]
	( @MyAccountId int
	, @client_id int
	, @report_year int
	)
AS
BEGIN

	set nocount on

	  declare @start_month int
		, @begin_date datetime
		, @end_date datetime
		, @reference_date datetime
		, @working_date datetime
		, @account_id int
		, @site_id int
		, @is_expected bit

	   select @start_month = start_month
	     from vwClientFiscalYearStartMonth
	    where client_id = @client_id

	if @start_month is null
	begin
		set @start_month = 1
	end


	if @start_month = 1
	begin
	      set @begin_date 	= convert(datetime, '1/1/' + convert(varchar, @report_year))
	      set @end_date 	= convert(datetime, '12/1/' + convert(varchar, @report_year))
	end
	else
	begin


	      select convert(datetime, convert(varchar,5) + '/1/' + convert(varchar, null - 1))


	      set @begin_date 	= convert(datetime, convert(varchar,@start_month) + '/1/' + convert(varchar, @report_year - 1))
	      set @end_date 	= dateadd(m, -1, convert(varchar,@start_month) + '/1/' + convert(varchar, @report_year))
	end

	set @reference_date = dateadd(m, -1, @begin_date)

/*
	print @begin_date
	print @end_date
	print @reference_date
*/

/*

	--Normal

		   select distinct ip.account_id
			, ip.site_id
			, ip.is_expected
		     from invoice_participation ip with (nolock)
		     join account a with (nolock) on a.account_id = ip.account_id
		     join site s with (nolock) on s.site_id = ip.site_id
		     join division d on d.division_id = s.division_id
		    where d.client_id = @client_id
		      and ip.service_month = @reference_date
		      and ip.is_expected = 1


	-- commercial

		   select distinct ip.account_id
			, ip.site_id
			, 1 is_expected
		     from vwSiteName vws 
		     join vwCbmsAccountSite ip on ip.site_id = vws.site_id
		    where vws.client_id = @client_id
		      and ip.account_type_id = 38

*/

	
	declare curUtility cursor
	read_only
	for
		   select distinct ip.account_id
			, ip.site_id
			, ip.is_expected
		     from invoice_participation ip with (nolock)
		     join account a with (nolock) on a.account_id = ip.account_id
		     join site s with (nolock) on s.site_id = ip.site_id
		     join division d on d.division_id = s.division_id
		    where d.client_id = @client_id
		      and ip.service_month = @reference_date
		      and ip.is_expected = 1
		      and a.account_type_id = 38

	
	OPEN curUtility
	
	FETCH NEXT FROM curUtility INTO @account_id, @site_id, @is_expected
	WHILE (@@fetch_status <> -1)
	BEGIN
	
		set @working_date = @begin_date
	
		while @working_date <= @end_date
		begin

			EXEC cbmsInvoiceParticipationQueue_Save
				  @MyAccountId
				, 9 -- Make Expected
				, null
				, null
				, @site_id 
				, @account_id 
				, @working_date
				, 1

			set @working_date = dateadd("m", 1, @working_date)

		end
	
		FETCH NEXT FROM curUtility INTO @account_id, @site_id, @is_expected
	END
	
	CLOSE curUtility 
	DEALLOCATE curUtility 


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_EstablishClientForReportYear] TO [CBMSApplication]
GO
