SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_HEDGE_DETAIL_P
	@hedge_volume DECIMAL(32,16), 
	@hedge_price DECIMAL(32,16),
	@hedge_detail_id INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.hedge_detail
	SET	hedge_volume=@hedge_volume,
		hedge_price=@hedge_price
	WHERE hedge_detail_id=@hedge_detail_id

END

GO
GRANT EXECUTE ON  [dbo].[UPDATE_HEDGE_DETAIL_P] TO [CBMSApplication]
GO
