SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



































/******                              
 NAME: dbo.[[Get_Variance_Process_Engine_By_Category]]                  
                              
 DESCRIPTION:   
                            
Rules:				  
				  Electric Power:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Demand:
                                Historical Usage
                                Historical Demand
                Billed Demand:
                                Historical Usage
                                Historical Billed Demand
                Cost:
                                Historical Usage
                                (Strongly encouraged) Historical Billed Demand
                                (Alternative) Historical Demand
                                Historical Cost

Natural Gas:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost

Water:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost

Waste Water:
                Usage:
                                Cooling Degree Days
                                Heating Degree Days
                                Historical Total Usage
                Cost:
                                Historical Usage
                                Historical Cost
          
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	@return_value = [dbo].[Get_Variance_Data_Analagous_Extraction_service_Account_Commodity_ID]
		@Account_Id =1407581 , --1407581 1405819
		@Commodity_Id = 290,
		@Service_Month = N'2017-12-01'

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	 1.Find the buckets associated to the account for the commodity and service month.
	 2.If the bucket is Total cost, then we have to give total cost.
		For total cost , we have to give additional info as per the above rule.
	3.check 12 of 18 data points to decide linear analogous

	Method Identification - 12 of 18 Rule
If the selected account has USAGE values for 12 or more months out of the last 18 months (start counting backwards from/not including the service month of the invoice start date), the query service will select Linear Regression variance method and continue with history query.

Note that 12 out of 18 rule does not require that the values be in 12 consecutive months. Just require 12 of 18 months to have usage values.

A value of 0 counts as a value and should be counted toward 12 of 18 rule.

Months marked as "No Data This Period" do NOT count as values toward the 12 of 18 rule.

Estimated usage values (either created by Gap-Filling procedure or entered by a user) will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months with manually-entered values will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months that were manually excluded as anomalous usage (see Anomalous Month section within Review Variance page) will NOT count as values toward the 12 of 18 rule.

If the selected account has usage values for less than 12 months out of the last 18 months, the query service will select Analogous Account variance method.
	4.It is possible that for same account /commodity one category can be linear and another one can be analogus. in that case, we have to send two request.
	however SP will give only on result set. 
	5.
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					Jan 21 2020			New procedure to get data points for variance testing (as per the requirement mentioned above)  
AP					Feb 19,2020			Modified procedure to exclude anomalous month data       
   
******/
CREATE PROCEDURE [dbo].[Get_Variance_Data_Analagous_Extraction_service_Account_Commodity_ID]
      (
      @Account_Id    INT
    , @Commodity_Id  INT
    , @Service_Month DATE
	,@Category_Service_Type AS tvp_Extraction_Service READONLY )
AS
      BEGIN

            SET NOCOUNT ON;

			DECLARE @start_date_performance DATETIME2 = GETDATE()

			
            DECLARE
                  @Client_Hier_Id                      INT
                , @currency_Group_Id                   INT
                , @Extraction_Service_History_Rule_Use INT
                , @Extraction_Service_History_Rule_Of  INT
                , @Second_Previous_Month               DATE
                , @Min_Account                         INT
                , @Max_Account                         INT
                , @Variance_Consumption_Level_id       INT
                , @Analogous_Distance                  INT
                , @Start_index                         INT        = 1
                , @count                               INT
                , @Bucket_master_id                    INT
                , @Variance_category_cd                INT
                , @Linear_Regression_Sigma_value       VARCHAR(10)
                , @Error_metric                        VARCHAR(30)
                , @Rate_id                             INT
                , @Unique_Value                        INT;


            SELECT
                  @Variance_Consumption_Level_id = vcl.Variance_Consumption_Level_Id
            FROM  dbo.Variance_Consumption_Level vcl
                  JOIN
                  dbo.Account_Variance_Consumption_Level avcl
                        ON avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
            WHERE vcl.Commodity_Id = @Commodity_Id
                  AND   avcl.ACCOUNT_ID = @Account_Id;

            CREATE TABLE #Bucket_Values -- This table will use to store the Final Value
                  ( Source_Account_Id             INT
                  , Service_Month                 DATE
                  , Billing_Start_Date            DATE
                  , Billing_End_Date              DATE
                  , Bucket_Master_id              INT
                  , Bucket_Name                   VARCHAR(255)
                  , Bucket_Type                   VARCHAR(25)
                  , Bucket_Value                  DECIMAL(28, 10)
                  , uom_cd                        INT
                  , uom_Name                      VARCHAR(25)
                  , Target_Account_ID             INT
                  , Target_Account_Client_Hier_Id INT
                  , Service_Type                  VARCHAR(25)
                  , Rate_Id                       INT
                  , [Perform_Test]                VARCHAR(10)    DEFAULT 'False'
                  , ThresholdSigma                VARCHAR(10)
                  , [Test Calculation Type]       VARCHAR(30)
                  , category_Name                 VARCHAR(20)
				  ,row_num INT
				  , BillingDays int); 

            DECLARE @Bucket_master_id_TVP AS tvp_Bucket_Master_IDs 

			DECLARE @Category_Service_Type1 TABLE
            (row_num INT IDENTITY (1,1),
			Variance_category_Cd INT,
			Bucket_master_id INT)

			INSERT INTO @Category_Service_Type1 (
			                                          Variance_category_Cd
			                                        , Bucket_master_id
			                                    )
			SELECT DISTINCT Variance_category_cd, Bucket_Master_Id FROM @Category_Service_Type


            SELECT
                  @Rate_id = cha.Rate_Id
            FROM  Core.Client_Hier_Account cha
            WHERE cha.Account_Id = @Account_Id;


            DECLARE
                 @Previous_Month     DATE = dateadd(DAY, - ( day(dateadd(MONTH, -1, @Service_Month))) + 1, dateadd(MONTH, -1, @Service_Month)) 

            SELECT
                  @Second_Previous_Month = dateadd(DAY, - ( day(dateadd(MONTH, -1, @Previous_Month))) + 1, dateadd(MONTH, -1, @Previous_Month));


            SELECT
                  @Client_Hier_Id = cha.Client_Hier_Id
            FROM  Core.Client_Hier_Account cha
            WHERE cha.Account_Id = @Account_Id
                  AND   cha.Commodity_Id = @Commodity_Id;

            SELECT
                  @currency_Group_Id = ch.Client_Currency_Group_Id
            FROM  Core.Client_Hier ch
            WHERE ch.Client_Hier_Id = @Client_Hier_Id; 

 

            --	SELECT @count

            /*TO IMPROVE THE PERFORMANCE ADDED ANOTHER LOGIC*/

    

	SET @count = (SELECT count (1) FROM @Category_Service_Type1)

                        --EXEC [dbo].[Get_Variance_Analog_Params_Consumption_ID_Based]
                        --      @Variance_Consumption_Level_id = @Variance_Consumption_Level_id        -- int
                        --    , @Unique_Value = @Unique_Value OUTPUT;     -- int

						--	SELECT @Unique_Value

						 --	SELECT @Unique_Value, @count,@Start_index

						 
                        IF ( @Unique_Value = 1 )
                              BEGIN
 
                                    INSERT INTO @Bucket_master_id_TVP (
                                                                            Bucket_Master_Id
                                                                      )
                                                SELECT      DISTINCT
                                                            t.Bucket_Master_Id
                                                FROM        @Category_Service_Type t

                                    SELECT
                                          @Min_Account = cast(VCESPV.Param_Value AS INT)
                                    FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                          JOIN
                                          dbo.Variance_Extraction_Service_Param VESP
                                                ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                    WHERE VESP.Param_Name = 'Analogous_Account_Min_Account_Required'
                                          AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;



                                    SELECT
                                          @Max_Account = cast(VCESPV.Param_Value AS INT)
                                    FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                          JOIN
                                          dbo.Variance_Extraction_Service_Param VESP
                                                ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                    WHERE VESP.Param_Name = 'Analogous_Account_Max_Account_Required'
                                          AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;


                                    SELECT
                                          @Analogous_Distance = VCESPV.Param_Value
                                    FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                          JOIN
                                          dbo.Variance_Extraction_Service_Param VESP
                                                ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                    WHERE VESP.Param_Name = 'Analogous_Account_Distance_value'
                                          AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;


                                    SELECT
                                          @Linear_Regression_Sigma_value = vcespv.Param_Value
                                    FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                                          JOIN
                                          dbo.Variance_Extraction_Service_Param vcesp
                                                ON vcesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                                    WHERE vcesp.Param_Name = 'Linear_Regression_Sigma_value'
                                          AND   vcespv.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;

                                    SELECT
                                          @Error_metric = c.Code_Value
                                    FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                                          JOIN
                                          dbo.Variance_Extraction_Service_Param vcesp
                                                ON vcesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                                          JOIN
                                          dbo.Code c
                                                ON c.Code_Id = vcespv.Param_Value
                                    WHERE vcesp.Param_Name = 'Error_Metric'
                                          AND   vcespv.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id;  

                                    INSERT INTO #Bucket_Values (
                                                                     Source_Account_Id
                                                                   , Service_Month
                                                                   , Billing_Start_Date
                                                                   , Billing_End_Date
                                                                   , Bucket_Name
                                                                   , Bucket_Type
                                                                   , Bucket_Value
                                                                   , uom_cd
                                                                   , uom_Name
                                                                   , Target_Account_ID
                                                                   , Target_Account_Client_Hier_Id
                                                                   , Service_Type
                                                                   , Rate_Id
                                                                   , Bucket_Master_id
                                                                   , ThresholdSigma
                                                                   , [Test Calculation Type]
                                                                   , category_Name
																   , BillingDays 
                                                               )

															
                                    EXEC dbo.Get_Analagous_Accounts_Account_Id_Commodity_Base 
                                    
                                          @Account_Id = @Account_Id           -- int
                                        , @Commodity_Id = @Commodity_Id       -- int
                                        , @Previous_Month = @Previous_Month   -- datetime
                                        , @Current_Service_Month = @Service_Month
                                        , @Second_Previous_Month = @Second_Previous_Month
                                        , @Bucket_master_id = @Bucket_master_id_TVP  
                                        , @Min_Account = @Min_Account
                                        , @Max_Account = @Max_Account
                                        , @Analogous_Distance = @Analogous_Distance
                                        , @Variance_Consumption_Level_id = @Variance_Consumption_Level_id
                                        , @Linear_Regression_Sigma_value = @Linear_Regression_Sigma_value
                                        , @error_metric = @Error_metric;

										 DELETE      b1
                                                FROM        #Bucket_Values b1
                                                WHERE       Service_Type = 'ANALAGOUS'
                                                            AND   b1.Service_Month = @Second_Previous_Month
                                                            AND   EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  #Bucket_Values B2
                                                            WHERE B2.Target_Account_ID = B1.Target_Account_ID
                                                                  AND   B2.Service_Month = @Previous_Month
                                                                  AND   B2.Service_Type = 'ANALAGOUS' );


                                                UPDATE
                                                      B
                                                SET
                                                      B.Perform_Test = 'True'
                                                FROM  #Bucket_Values B
                                                      JOIN
                                                      @Category_Service_Type Bl
                                                            ON B.Bucket_Master_id = Bl.Bucket_Master_Id
                                                WHERE B.Target_Account_ID = @Account_Id
                                                      AND   B.Service_Type = 'ANALAGOUS'
                                                      AND   B.Service_Month = @Service_Month;

                              END;

                        ELSE
                              BEGIN

                                    WHILE ( @Start_index <= @count )
                                          BEGIN

								--		  SELECT 'arun'

                                                SELECT
                                                      @Variance_category_cd = TBC.Variance_Category_Cd
                                                    , @Bucket_master_id = TBC.Bucket_Master_Id
                                                FROM  @Category_Service_Type1 TBC
                                                WHERE row_num = @Start_index;


                                                -- SELECT @commodity_id, @Account_Id, @Variance_category_cd

                                                SELECT
                                                      @Min_Account = cast(VCESPV.Param_Value AS INT)
                                                FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                                      JOIN
                                                      dbo.Variance_Extraction_Service_Param VESP
                                                            ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                                WHERE VESP.Param_Name = 'Analogous_Account_Min_Account_Required'
                                                      AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id
                                                      AND   VCESPV.Variance_Category_Cd = @Variance_category_cd;


                                                SELECT
                                                      @Max_Account = cast(VCESPV.Param_Value AS INT)
                                                FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                                      JOIN
                                                      dbo.Variance_Extraction_Service_Param VESP
                                                            ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                                WHERE VESP.Param_Name = 'Analogous_Account_Max_Account_Required'
                                                      AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id
                                                      AND   VCESPV.Variance_Category_Cd = @Variance_category_cd;

                                                SELECT
                                                      @Analogous_Distance = VCESPV.Param_Value
                                                FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value VCESPV
                                                      JOIN
                                                      dbo.Variance_Extraction_Service_Param VESP
                                                            ON VESP.Variance_Extraction_Service_Param_Id = VCESPV.Variance_Extraction_Service_Param_Id
                                                WHERE VESP.Param_Name = 'Analogous_Account_Distance_value'
                                                      AND   VCESPV.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id
                                                      AND   VCESPV.Variance_Category_Cd = @Variance_category_cd;



                                                SELECT
                                                      @Linear_Regression_Sigma_value = vcespv.Param_Value
                                                FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                                                      JOIN
                                                      dbo.Variance_Extraction_Service_Param vcesp
                                                            ON vcesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                                                WHERE vcesp.Param_Name = 'Linear_Regression_Sigma_value'
                                                      AND   vcespv.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id
                                                      AND   vcespv.Variance_Category_Cd = @Variance_category_cd;

                                                SELECT
                                                      @Error_metric = c.Code_Value
                                                FROM  dbo.Variance_Consumption_Extraction_Service_Param_Value vcespv
                                                      JOIN
                                                      dbo.Variance_Extraction_Service_Param vcesp
                                                            ON vcesp.Variance_Extraction_Service_Param_Id = vcespv.Variance_Extraction_Service_Param_Id
                                                      JOIN
                                                      dbo.Code c
                                                            ON c.Code_Id = vcespv.Param_Value
                                                WHERE vcesp.Param_Name = 'Error_Metric'
                                                      AND   vcespv.Variance_Consumption_Level_Id = @Variance_Consumption_Level_id
                                                      AND   vcespv.Variance_Category_Cd = @Variance_category_cd;


                                                --								--SELECT @Analogous_Distance
                                                IF ( @Analogous_Distance IS NOT NULL )
                                                      BEGIN
													  PRINT @Bucket_master_id

                                                            INSERT INTO #Bucket_Values (
                                                                                             Source_Account_Id
                                                                                           , Service_Month
                                                                                           , Billing_Start_Date
                                                                                           , Billing_End_Date
                                                                                           , Bucket_Name
                                                                                           , Bucket_Type
                                                                                           , Bucket_Value
                                                                                           , uom_cd
                                                                                           , uom_Name
                                                                                           , Target_Account_ID
                                                                                           , Target_Account_Client_Hier_Id
                                                                                           , Service_Type
                                                                                           , Rate_Id
                                                                                           , Bucket_Master_id
                                                                                           , ThresholdSigma
                                                                                           , [Test Calculation Type]
                                                                                           , category_Name
																						   , BillingDays 
                                                                                       )
                                                            EXEC dbo.Get_Analagous_Accounts_Account_Id_Commodity_Base_Category
                                                                  @Account_Id = @Account_Id           -- int
                                                                , @Commodity_Id = @Commodity_Id       -- int
                                                                , @Previous_Month = @Previous_Month   -- datetime
                                                                , @Current_Service_Month = @Service_Month
                                                                , @Second_Previous_Month = @Second_Previous_Month
                                                                , @Bucket_master_id = @Bucket_master_id
                                                                , @Min_Account = @Min_Account
                                                                , @Max_Account = @Max_Account
                                                                , @Analogous_Distance = @Analogous_Distance
                                                                , @Variance_Consumption_Level_id = @Variance_Consumption_Level_id
                                                                , @Linear_Regression_Sigma_value = @Linear_Regression_Sigma_value
                                                                , @error_metric = @Error_metric;

                                                      END;
                                                SET @Start_index = @Start_index + 1;

                                                -- FOR ANALOGOUS IF PREVIOUS MONTH HAVE DATA, DELETE SECOND PREV MONTH DATA. DONT WANT TO SEND TOO MUCH INFO TO PHYTHON SERVICE

                                                DELETE      b1
                                                FROM        #Bucket_Values b1
                                                WHERE       Service_Type = 'ANALAGOUS'
                                                            AND   b1.Service_Month = @Second_Previous_Month
                                                            AND   EXISTS
                                                      (     SELECT
                                                                  1
                                                            FROM  #Bucket_Values B2
                                                            WHERE B2.Target_Account_ID = B1.Target_Account_ID
                                                                  AND   B2.Service_Month = @Previous_Month
                                                                  AND   B2.Service_Type = 'ANALAGOUS' );


                                                UPDATE
                                                      B
                                                SET
                                                      B.Perform_Test = 'True'
                                                FROM  #Bucket_Values B
                                                      JOIN
                                                      @Category_Service_Type1 Bl
                                                            ON B.Bucket_Master_id = Bl.Bucket_Master_Id
                                                WHERE B.Target_Account_ID = @Account_Id
                                                      AND   B.Service_Type = 'ANALAGOUS'
                                                      AND   B.Service_Month = @Service_Month;


                                          END;
                              END;
                  
				  
				  
				




            SELECT      DISTINCT
                        uom_Name
                      , uom_cd
                      , ThresholdSigma
                      , bv.[Test Calculation Type]
                      , Target_Account_ID
                      , Target_Account_Client_Hier_Id
                      , Source_Account_Id
                      , Service_Type
                      , Service_Month
                      , Rate_Id
                      , Perform_Test
                      , category_Name AS Bucket_Name
                      , Bucket_Value
                      , Bucket_Type
                      , Bucket_Master_id
                      , Billing_Start_Date
                      , Billing_End_Date
					  , bv.BillingDays
            FROM        #Bucket_Values bv;


            SELECT      DISTINCT
                        bv.Target_Account_ID
                      , bv.Target_Account_Client_Hier_Id
                      , bv.Billing_Start_Date
                      , bv.Billing_End_Date
                      , bv.Service_Month
            FROM        #Bucket_Values bv
			WHERE bv.Service_Type ='linear';
			 
            DROP TABLE #Bucket_Values 
			;

			--INSERT INTO dbo.performance_test_variance (
			--                                                 Account_id
			--                                               , commodity_id
			--                                               , service_month
			--                                               , starttime
			--                                               , endtime
			--                                           )
			--VALUES
			--     ( @Account_Id                -- Account_id - int
			--     , @Commodity_Id               -- commodity_id - int
			--     , @Service_Month        -- service_month - date
			--     , @start_date_performance    -- starttime - datetime2(7)
			--     , sysdatetime()    -- endtime - datetime2(7)
			--      )

      END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Data_Analagous_Extraction_service_Account_Commodity_ID] TO [CBMSApplication]
GO
