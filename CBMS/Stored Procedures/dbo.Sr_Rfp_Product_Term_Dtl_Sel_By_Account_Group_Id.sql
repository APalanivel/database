SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Sr_Rfp_Product_Term_Dtl_Sel_By_Account_Group_Id

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default				Description
------------------------------------------------------------------------
	@rfpAccountId  	int       	          	
	@isBidGroup    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default				Description
------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------


exec Sr_Rfp_Product_Term_Dtl_Sel_By_Account_Group_Id 10012029,1

exec Sr_Rfp_Product_Term_Dtl_Sel_By_Account_Group_Id 2,0


AUTHOR INITIALS:
	Initials			Name
------------------------------------------------------------------------
	NR					Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------------------
 NR				30-03-2016	Global Sourcing Phase-3 Created for GCS-582
 
******/
CREATE  PROCEDURE [dbo].[Sr_Rfp_Product_Term_Dtl_Sel_By_Account_Group_Id]
      ( 
       @rfpAccountId INT
      ,@isBidGroup INT )
AS 
BEGIN

      SET NOCOUNT ON

      CREATE TABLE #Product_Detail
            ( 
             PRODUCT_ID INT NOT NULL
            ,SR_RFP_SELECTED_PRODUCTS_ID INT NOT NULL
            ,PRODUCT_NAME VARCHAR(100) NOT NULL
            ,SR_RFP_ACCOUNT_TERM_ID INT NOT NULL
            ,FROM_MONTH DATETIME NOT NULL
            ,TO_MONTH DATETIME NOT NULL
            ,NO_OF_MONTHS INT NOT NULL
            ,SR_SUPPLIER_CONTACT_INFO_ID INT NOT NULL )
      
      INSERT      INTO #Product_Detail
                  ( 
                   PRODUCT_ID
                  ,SR_RFP_SELECTED_PRODUCTS_ID
                  ,PRODUCT_NAME
                  ,SR_RFP_ACCOUNT_TERM_ID
                  ,FROM_MONTH
                  ,TO_MONTH
                  ,NO_OF_MONTHS
                  ,SR_SUPPLIER_CONTACT_INFO_ID )
                  SELECT
                        spp.SR_PRICING_PRODUCT_ID PRODUCT_ID
                       ,srtpf.SR_RFP_SELECTED_PRODUCTS_ID
                       ,spp.PRODUCT_NAME PRODUCT_NAME
                       ,srat.SR_RFP_ACCOUNT_TERM_ID
                       ,srat.FROM_MONTH
                       ,srat.TO_MONTH
                       ,srat.NO_OF_MONTHS
                       ,srscvm.SR_SUPPLIER_CONTACT_INFO_ID AS SR_SUPPLIER_CONTACT_INFO_ID
                  FROM
                        SR_RFP_ACCOUNT_TERM srat
                        INNER JOIN SR_RFP_TERM_PRODUCT_MAP srtpf
                              ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpf.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN SR_RFP_SELECTED_PRODUCTS srsp
                              ON srtpf.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN SR_PRICING_PRODUCT spp
                              ON srsp.PRICING_PRODUCT_ID = spp.SR_PRICING_PRODUCT_ID
                        INNER JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srscvm
                              ON srtpf.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srscvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN SR_SUPPLIER_CONTACT_INFO ssci
                              ON srscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN VENDOR v
                              ON srscvm.VENDOR_ID = v.VENDOR_ID
                        INNER JOIN USER_INFO ui
                              ON ssci.USER_INFO_ID = ui.USER_INFO_ID
                  WHERE
                        srtpf.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
                        AND srsp.IS_SYSTEM_PRODUCT = 1
                        AND srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
                        AND srat.IS_BID_GROUP = @isBidGroup
                  UNION
                  SELECT
                        spp.SR_RFP_PRICING_PRODUCT_ID PRODUCT_ID
                       ,srtpf.SR_RFP_SELECTED_PRODUCTS_ID
                       ,spp.PRODUCT_NAME PRODUCT_NAME
                       ,srat.SR_RFP_ACCOUNT_TERM_ID
                       ,srat.FROM_MONTH
                       ,srat.TO_MONTH
                       ,srat.NO_OF_MONTHS
                       ,srscvm.SR_SUPPLIER_CONTACT_INFO_ID AS SR_SUPPLIER_CONTACT_INFO_ID
                  FROM
                        SR_RFP_ACCOUNT_TERM srat
                        INNER JOIN SR_RFP_TERM_PRODUCT_MAP srtpf
                              ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpf.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN SR_RFP_SELECTED_PRODUCTS srsp
                              ON srtpf.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN SR_RFP_PRICING_PRODUCT spp
                              ON srsp.PRICING_PRODUCT_ID = spp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srscvm
                              ON srtpf.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srscvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN SR_SUPPLIER_CONTACT_INFO ssci
                              ON srscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN VENDOR v
                              ON srscvm.VENDOR_ID = v.VENDOR_ID
                        INNER JOIN USER_INFO ui
                              ON ssci.USER_INFO_ID = ui.USER_INFO_ID
                  WHERE
                        srtpf.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
                        AND srsp.IS_SYSTEM_PRODUCT IS NULL
                        AND srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
                        AND srat.IS_BID_GROUP = @isBidGroup
            
                  
      SELECT
            PRODUCT_ID
           ,SR_RFP_SELECTED_PRODUCTS_ID
           ,PRODUCT_NAME
           ,SR_RFP_ACCOUNT_TERM_ID
           ,FROM_MONTH
           ,TO_MONTH
           ,NO_OF_MONTHS
           ,SR_SUPPLIER_CONTACT_INFO_ID
      FROM
            #Product_Detail
      ORDER BY
            PRODUCT_ID
           ,SR_RFP_SELECTED_PRODUCTS_ID
           ,PRODUCT_NAME
           ,SR_RFP_ACCOUNT_TERM_ID
           ,FROM_MONTH
           ,TO_MONTH
           ,NO_OF_MONTHS
      DROP TABLE #Product_Detail
       
END 
	

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Product_Term_Dtl_Sel_By_Account_Group_Id] TO [CBMSApplication]
GO
