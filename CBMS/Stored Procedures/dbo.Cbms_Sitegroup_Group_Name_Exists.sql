SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
dbo.Cbms_Sitegroup_Group_Name_Exists
 
 DESCRIPTION:   
 
	This procedure used to insert the Non metric(Division/State/Country/Supplier/Vendor..) rules of a smart group
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Rule_Filter_Id INT	
	, @Filter_Condition_Id INT
	, @Filter_Value VARCHAR(50)
	, @Is_Inclusive BIT
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  
	

	EXEC dbo.Cbms_Sitegroup_Group_Name_Upd
    @Cbms_Sitegroup_Id = 1
    , @Group_Name = 'XXX'
   

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2018-09-27	Created for GRM.
  
******/

CREATE PROC [dbo].[Cbms_Sitegroup_Group_Name_Exists]
      ( 
       @Client_Id INT
      ,@Group_Name NVARCHAR(255)
      ,@Module_Cd INT
      ,@Cbms_Sitegroup_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE @IS_Group_Name_Exist BIT = 0;

      SELECT
            @IS_Group_Name_Exist = 1
      FROM
            dbo.Cbms_Sitegroup cs
      WHERE
            cs.Group_Name = @Group_Name
            AND cs.Client_Id = @Client_Id
            AND cs.Module_Cd = @Module_Cd
            AND ( @Cbms_Sitegroup_Id IS NULL
                  OR cs.Cbms_Sitegroup_Id <> @Cbms_Sitegroup_Id );


      SELECT
            @IS_Group_Name_Exist AS IS_Group_Name_Exist;


END;

GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Group_Name_Exists] TO [CBMSApplication]
GO
