SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Pricing_Product_Sel_By_Country_Commodity]
           
DESCRIPTION:             
			To get SR pricing products
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	EXEC dbo.Sr_Pricing_Product_Sel_By_Country_Commodity null,null
	EXEC dbo.Sr_Pricing_Product_Sel_By_Country_Commodity null,290
	EXEC dbo.Sr_Pricing_Product_Sel_By_Country_Commodity null,291
	EXEC dbo.Sr_Pricing_Product_Sel_By_Country_Commodity 1,null


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-09-16	Global Sourcing - Phase2 -Created
								
******/
CREATE PROCEDURE [dbo].[Sr_Pricing_Product_Sel_By_Country_Commodity]
      (
       @Country_Id INT = NULL
      ,@Commodity_Id INT = NULL )
AS
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            srpr.SR_PRICING_PRODUCT_ID
           ,srpr.PRODUCT_NAME
      FROM
            dbo.SR_PRICING_PRODUCT srpr
            LEFT JOIN dbo.SR_Pricing_Product_Country_Map csrpr
                  ON srpr.SR_PRICING_PRODUCT_ID = csrpr.SR_PRICING_PRODUCT_ID
      WHERE
            ( @Commodity_Id IS NULL
              OR srpr.COMMODITY_TYPE_ID = @Commodity_Id )
            AND ( @Country_Id IS NULL
                  OR csrpr.Country_ID = @Country_Id );
            
                         
END;
GO
GRANT EXECUTE ON  [dbo].[Sr_Pricing_Product_Sel_By_Country_Commodity] TO [CBMSApplication]
GO
