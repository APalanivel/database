SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUbmInvoice_GetSixMonthPKReceivedStat]
	( @MyAccountId int )
AS
BEGIN

	set nocount on

	declare @start_date datetime
	set @start_date = dateadd(m, -6, convert(datetime, convert(varchar, month(getdate())) + '/1/' + convert(varchar, year(getdate()))))

	   select month(l.start_date) month_no
		, datename(mm, l.start_date) month_name
		, count(*)
	     from ubm_batch_master_log l
	     join ubm_invoice ui on ui.ubm_batch_master_log_id = l.ubm_batch_master_log_id
	    where l.ubm_id = 7
	      and l.start_date >= @start_date
	 group by month(l.start_date)
		, datename(mm, l.start_date) 
	 order by month(l.start_date)


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetSixMonthPKReceivedStat] TO [CBMSApplication]
GO
