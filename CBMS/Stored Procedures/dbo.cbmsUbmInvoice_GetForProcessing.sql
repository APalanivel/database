
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsUbmInvoice_GetForProcessing

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@ubm_invoice_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC cbmsUbmInvoice_GetForProcessing 
      @MyAccountId = 49
     ,@ubm_invoice_id = 23044364 
     
AUTHOR INITIALS:
	Initials		Name
------------------------------------------------------------
	SP				Sandeep Pigilam
	NR				Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	SP			2014-08-13  Data Operations Enhancement Phase III,Added Invoice_Type_Cd	     
	NR			2016-12-23	MAINT-4737 replaced Invoice_Type_Cd with Meter_Read_Type_Cd.
	HG			2017-09-24	Added Invoice_Type column for bulk invoice load enhancement
******/
CREATE PROCEDURE [dbo].[cbmsUbmInvoice_GetForProcessing]
      (
       @MyAccountId INT
      ,@ubm_invoice_id INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Meter_Read_Type_Actual INT
           ,@Meter_Read_Type_Estimated INT
           ,@Invoice_Type_Adjustment INT

      SELECT
            @Meter_Read_Type_Actual = MAX(CASE WHEN cd.Code_Value = 'Actual' THEN cd.Code_Id
                                          END)
           ,@Meter_Read_Type_Estimated = MAX(CASE WHEN cd.Code_Value = 'Estimated' THEN cd.Code_Id
                                             END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Meter Read Type'

      SELECT
            @Invoice_Type_Adjustment = MAX(CASE WHEN cd.Code_Value = 'Adjustment' THEN cd.Code_Id
                                           END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Invoice Type'


      SELECT
            ml.UBM_ID
           ,UBM.UBM_NAME
           ,ml.UBM_BATCH_MASTER_LOG_ID
           ,i.UBM_INVOICE_ID
           ,i.INVOICE_IDENTIFIER
           ,NULL feed_file_name
           ,i.CBMS_IMAGE_ID
           ,i.UBM_ACCOUNT_ID ubm_account_code
           ,i.UBM_ACCOUNT_NUMBER
           ,i.UBM_VENDOR_ID ubm_vendor_code
           ,i.UBM_VENDOR_NAME
           ,vm.VENDOR_ID
           ,i.UBM_CLIENT_ID ubm_client_code
           ,cm.CLIENT_ID
           ,i.CITY ubm_city
           ,i.STATE ubm_state_code
           ,cur.CURRENCY_UNIT_ID
           ,i.CURRENCY ubm_currency_code
           ,MIN(det.SERVICE_START_DATE) begin_date
           ,MAX(det.SERVICE_END_DATE) end_date
           ,MAX(det.BILLING_DAYS) billing_days
           ,COUNT(i.UBM_INVOICE_ID) AS inv_count
           ,NULL 'inv_count'
           ,i.CURRENT_CHARGES
           ,CASE WHEN UBM.UBM_NAME IN ( 'Avista', 'Cass', 'Generic ETL' )
                      AND MAX(CASE WHEN det.ESTIMATE_FLAG = 'N' THEN 1
                                   ELSE 0
                              END) = 1 THEN @Meter_Read_Type_Actual
                 WHEN UBM.UBM_NAME IN ( 'Avista', 'Cass', 'Generic ETL' )
                      AND MAX(CASE WHEN det.ESTIMATE_FLAG = 'Y' THEN 1
                                   ELSE 0
                              END) = 1 THEN @Meter_Read_Type_Estimated
            END AS Meter_Read_Type_Cd
           ,it.Code_Id AS Invoice_Type_Cd
      FROM
            dbo.UBM_BATCH_MASTER_LOG ml
            JOIN dbo.UBM_INVOICE i
                  ON i.UBM_BATCH_MASTER_LOG_ID = ml.UBM_BATCH_MASTER_LOG_ID
            LEFT OUTER JOIN dbo.UBM_INVOICE_DETAILS det
                  ON det.UBM_INVOICE_ID = i.UBM_INVOICE_ID
            JOIN dbo.UBM
                  ON UBM.UBM_ID = ml.UBM_ID
            LEFT OUTER JOIN dbo.UBM_CLIENT_MAP cm
                  ON cm.UBM_ID = ml.UBM_ID
                     AND cm.UBM_CLIENT_CODE = i.UBM_CLIENT_ID
            LEFT OUTER JOIN dbo.UBM_VENDOR_MAP vm
                  ON vm.UBM_ID = ml.UBM_ID
                     AND vm.UBM_VENDOR_CODE = i.UBM_VENDOR_ID
            LEFT OUTER JOIN dbo.UBM_CURRENCY_MAP cur
                  ON cur.UBM_ID = ml.UBM_ID
                     AND cur.UBM_CURRENCY_CODE = i.CURRENCY
            LEFT OUTER JOIN ( dbo.Codeset cs
                              INNER JOIN dbo.Code it
                                    ON it.Codeset_Id = cs.Codeset_Id )
                  ON it.Code_Value = CAST(i.Invoice_Type AS VARCHAR(25))
                     AND cs.Codeset_Name = 'Invoice Type'
      WHERE
            i.UBM_INVOICE_ID = @ubm_invoice_id
      GROUP BY
            ml.UBM_ID
           ,UBM.UBM_NAME
           ,ml.UBM_BATCH_MASTER_LOG_ID
           ,i.UBM_INVOICE_ID
           ,i.INVOICE_IDENTIFIER
           ,i.CBMS_IMAGE_ID
           ,i.UBM_ACCOUNT_ID
           ,i.UBM_ACCOUNT_NUMBER
           ,i.UBM_VENDOR_ID
           ,i.UBM_VENDOR_NAME
           ,vm.VENDOR_ID
           ,i.UBM_CLIENT_ID
           ,cm.CLIENT_ID
           ,i.CITY
           ,i.STATE
           ,cur.CURRENCY_UNIT_ID
           ,i.CURRENCY
           ,i.CURRENT_CHARGES
           ,it.Code_Id
END;
;
GO



GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetForProcessing] TO [CBMSApplication]
GO
