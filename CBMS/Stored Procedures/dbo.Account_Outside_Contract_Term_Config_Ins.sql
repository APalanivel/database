SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Outside_Contract_Term_Config_Ins          
              
Description:              
        To insert Data into OCT account level table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Id						INT
     @Commodity_Id						INT
     @Config_Start_Dt							DATE
     @Config_End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Account_Outside_Contract_Term_Config WHERE Account_Id =1148520
    EXEC dbo.Account_Outside_Contract_Term_Config_Ins 
      @Account_Id =1148520
     ,@OCT_Parameter_Cd = 1
	 ,@OCT_Tolerance_Date_Cd = 1
	 ,@OCT_Dt_Range_Cd = 1
     ,@Config_Start_Dt = '2015-01-01'
     ,@Config_End_Dt= NULL   
     ,@User_Info_Id = 49
	 ,@Contract_Id =1
    
	SELECT * FROM Account_Outside_Contract_Term_Config WHERE Account_Id =1148520

	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/

CREATE PROCEDURE [dbo].[Account_Outside_Contract_Term_Config_Ins]
    (
        @Account_Id INT
        , @OCT_Parameter_Cd INT
        , @OCT_Tolerance_Date_Cd INT
        , @OCT_Dt_Range_Cd INT
        , @Config_Start_Dt DATE
        , @Config_End_Dt DATE = NULL
        , @User_Info_Id INT
        , @Contract_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        INSERT INTO dbo.Account_Outside_Contract_Term_Config
             (
                 Account_Id
                 , Contract_Id
                 , OCT_Parameter_Cd
                 , OCT_Tolerance_Date_Cd
                 , OCT_Dt_Range_Cd
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Account_Id
            , @Contract_Id
            , @OCT_Parameter_Cd
            , @OCT_Tolerance_Date_Cd
            , @OCT_Dt_Range_Cd
            , @Config_Start_Dt
            , @Config_End_Dt
            , GETDATE()
            , @User_Info_Id
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Outside_Contract_Term_Config aoc
                           WHERE
                                aoc.Account_Id = @Account_Id
                                AND aoc.Contract_Id = @Contract_Id
                                AND aoc.OCT_Parameter_Cd = @OCT_Parameter_Cd);






    END;





GO
GRANT EXECUTE ON  [dbo].[Account_Outside_Contract_Term_Config_Ins] TO [CBMSApplication]
GO
