SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seStripHeader_GetTempForAccount
	(
		@AccountId int
	)
AS
BEGIN

	set nocount on
	
	declare @ThisId int
	
	select @ThisId = StripHeaderId
	  from seStripHeader
	 where AccountId = @AccountId
	   and StripName is null

	if @ThisId is null
	begin
	
		insert into seStripHeader
			( AccountId
			, UniformVolume
			)
			values
			( @AccountId
			, 0
			)
			
		set @ThisId = @@IDENTITY
	
	end

	exec seStripHeader_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seStripHeader_GetTempForAccount] TO [CBMSApplication]
GO
