SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Budget_Account_Queue_Route_To_User

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType		Default	Description
----------------------------------------------------------------
	@Budget_Account_Queue_Ids	VARCHAR(MAX)
    @Route_To_User_Id			INT
    @Routed_By_User_Id			INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-06-01	SE2017-26 - Created
	
******/

CREATE PROCEDURE [dbo].[Budget_Account_Queue_Route_To_User]
      ( 
       @Budget_Account_Queue_Ids VARCHAR(MAX)
      ,@Route_To_User_Id INT
      ,@Routed_By_User_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Queue_Id INT
      DECLARE @Tbl_Budget_Account_Queue TABLE
            ( 
             Budget_Account_Queue_Id INT )
      
      SELECT
            @Queue_Id = QUEUE_ID
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @Route_To_User_Id
            
      INSERT      INTO @Tbl_Budget_Account_Queue
                  ( 
                   Budget_Account_Queue_Id )
                  SELECT
                        CONVERT(INT, Segments)
                  FROM
                        dbo.ufn_split(@Budget_Account_Queue_Ids, ',')
                        
      UPDATE
            baq
      SET   
            Queue_Id = @Queue_Id
           ,Routed_By_User_Id = @Routed_By_User_Id
           ,Routed_Ts = GETDATE()
           ,Last_Change_Ts = GETDATE()
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN @Tbl_Budget_Account_Queue baq1
                  ON baq.Budget_Account_Queue_Id = baq1.Budget_Account_Queue_Id

      
      
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Budget_Account_Queue_Route_To_User] TO [CBMSApplication]
GO
