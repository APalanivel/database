SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Vendor_Commodity_Analyst_Map_Ins

DESCRIPTION:
	Used to insert data into vendor_commodity_analyst_map table.
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
   @Analyst_ID				 INT 
   @Vendor_commodity_Map_id  INT 
   
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Vendor_Commodity_Analyst_Map_Ins 64,4539

	SELECT * FROM Vendor_Commodity_Analyst_Map WHERE vendor_commodity_map_id = 4539
	SELECT * FROM vendor_commodity_map where vendor_commodity_map_id = 4539

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SSR       	02/10/2010	Created
******/

CREATE PROCEDURE dbo.Vendor_Commodity_Analyst_Map_Ins
	 @Analyst_ID INT 
	,@Vendor_commodity_Map_id INT 

AS
BEGIN

	SET NOCOUNT ON
	
	INSERT INTO dbo.Vendor_Commodity_Analyst_Map
			(
			 Analyst_ID
			,Vendor_commodity_Map_id
			)
	VALUES(
			@Analyst_ID
		   ,@Vendor_commodity_Map_id
		)

	SELECT SCOPE_IDENTITY() Vendor_Commodity_Analyst_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Vendor_Commodity_Analyst_Map_Ins] TO [CBMSApplication]
GO
