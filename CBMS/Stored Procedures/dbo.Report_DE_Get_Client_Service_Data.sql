
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****  
  
NAME:  
 [dbo].[Report_DE_Get_Client_Service_Data] 
  
DESCRIPTION:  
      Procedure will select all client Commodities.

INPUT PARAMETERS:  
      Name     DataType          Default     Description  
------------------------------------------------------------------------------------------
   
OUTPUT PARAMETERS:  
      Name              DataType          Default     Description  
--------------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------------------
 EXEC  [dbo].[Report_DE_Get_Client_Service_Data]
      
AUTHOR INITIALS:  
 Initials		Name     
------------------------------------------------------------------------------------------
  KVK			Vinay K
	              
MODIFICATIONS:  
 Initials    Date		Modification  
------------------------------------------------------------------------------------------
  KVK		3/11/2014	Created
  LEC		11/29/2016  Modified for removal of variance columns in client commodity table

******/   
CREATE PROCEDURE [dbo].[Report_DE_Get_Client_Service_Data]
AS 
BEGIN  
  
      SET NOCOUNT ON;  
      DECLARE @Invoice_Cd INT = ( SELECT
                                    c.Code_Id
                                  FROM
                                    dbo.Code AS c
                                    JOIN dbo.Codeset AS c2
                                          ON c.Codeset_Id = c2.Codeset_Id
                                  WHERE
                                    c2.Codeset_Name = 'CommodityServiceSource'
                                    AND c.Code_Value = 'Invoice' )

      SELECT
            ch.client_name [Client]
           ,com.Commodity_Name [Commodity]
           ,[Variance Effective Status] = CASE WHEN ccf.Client_Commodity_Id IS NULL THEN 'No'
                                               ELSE 'Yes'
                                          END
           ,[Variance Test Type] = cd.Code_Value
           ,ccf.Start_Dt [Variance Start Date]
           ,ccf.End_Dt [Variance End Date]
      FROM
            core.Client_Hier ch
            JOIN Core.Client_Commodity cc
                  ON cc.client_id = ch.client_id
            JOIN commodity com
                  ON com.commodity_id = cc.commodity_id
            LEFT JOIN client_commodity_variance_test_config ccf
                  ON ccf.Client_Commodity_Id = cc.Client_Commodity_Id
            LEFT JOIN code cd
                  ON cd.Code_Id = ccf.Variance_Test_Type_Cd
      WHERE
            ch.Site_Id = 0
            AND ch.Sitegroup_Id = 0
            AND cc.Commodity_Service_Cd = @Invoice_Cd
      ORDER BY
            ch.client_name
           ,com.Commodity_Name 
			
END;

;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Get_Client_Service_Data] TO [CBMS_SSRS_Reports]
GO
