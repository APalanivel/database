SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME: dbo.Contract_Notification_Log_Ins 

DESCRIPTION: 
				To update the status of the message

INPUT PARAMETERS: 
Name						DataType	Default		Description 
------------------------------------------------------------------------------- 
@Contract_Id				INT
@Meter_Id					INT
@Notification_Msg_Queue_Id	INT
@Last_Change_Ts				INT 


OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
------------------------------------------------------------------------------- 

USAGE EXAMPLES:   
-------------------------------------------------------------------------------   

SELECT TOP 5 nmq.Notification_Msg_Queue_Id FROM dbo.Notification_Msg_Queue nmq 
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' ) AND cs.Codeset_Name = 'Notification Type'

BEGIN TRANSACTION
	Select * from Contract_Notification_Log where Contract_Id = 116655 
	EXEC dbo.Contract_Notification_Log_Ins 
		  @Contract_Id = 116655
		 ,@Meter_Id_List = '316440,327232,327220,316443,713289'
		 ,@Notification_Msg_Queue_Id = 8140
	Select * from Contract_Notification_Log a join dbo.Contract_Notification_Log_Meter_Dtl b
		on a.Contract_Notification_Log_Id = b.Contract_Notification_Log_Id where Contract_Id = 116655 
	SELECT DISTINCT Supplier_Contract_ID,Meter_Id,Supplier_Meter_Association_Date,Supplier_Meter_Disassociation_Date
		FROM Core.Client_Hier_Account WHERE Supplier_Contract_ID=116655 AND Meter_Id	IN (316440,327232,327220,316443,713289)
ROLLBACK TRANSACTION


SELECT TOP 5 nmq.Notification_Msg_Queue_Id FROM dbo.Notification_Msg_Queue nmq 
	INNER JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = nt.Notification_Type_Cd
    INNER JOIN dbo.Codeset cs ON cd.Codeset_Id = cd.Codeset_Id
WHERE cd.Code_Value IN ( 'Registration Notification' ) AND cs.Codeset_Name = 'Notification Type'


BEGIN TRANSACTION
	Select * from Contract_Notification_Log where Contract_Id = 116655 
	EXEC dbo.Contract_Notification_Log_Ins 
		  @Contract_Id = 116655
		 ,@Meter_Id_List = '316440,327232,327220,316443,713289'
		 ,@Notification_Msg_Queue_Id = 8149
	Select * from Contract_Notification_Log a join dbo.Contract_Notification_Log_Meter_Dtl b
		on a.Contract_Notification_Log_Id = b.Contract_Notification_Log_Id where Contract_Id = 116655 
	SELECT DISTINCT Supplier_Contract_ID,Meter_Id,Supplier_Meter_Association_Date,Supplier_Meter_Disassociation_Date
		FROM Core.Client_Hier_Account WHERE Supplier_Contract_ID=116655 AND Meter_Id	IN (316440,327232,327220,316443,713289)
ROLLBACK TRANSACTION 

AUTHOR INITIALS:   
Initials	Name   
-------------------------------------------------------------------------------   
RR			Raghu Reddy


MODIFICATIONS:  
Initials	Date		Modification 
-------------------------------------------------------------------------------   
RR			2016-05-30  Created  For GCS Phase- 5.	 		 
******/
CREATE PROCEDURE [dbo].[Contract_Notification_Log_Ins]
      ( 
       @Contract_Id INT
      ,@Meter_Id_List NVARCHAR(MAX)
      ,@Notification_Msg_Queue_Id INT
      ,@Is_On_Demand BIT = 0 )
AS 
BEGIN 
      SET NOCOUNT ON;
      
      DECLARE @Contract_Notification_Log_Id INT;
      
      DECLARE @Meter_Ids TABLE ( Meter_Id INT );  
      
      DECLARE
            @Flow_Verification VARCHAR(25)
           ,@Advance_Notofication_Days INT;
      
      SELECT
            @Flow_Verification = Code_Value
      FROM
            dbo.Notification_Msg_Queue nmq
            INNER JOIN dbo.Notification_Template nt
                  ON nmq.Notification_Template_Id = nt.Notification_Template_Id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = nt.Notification_Type_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Notification Type'
            AND nmq.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id
            
      SELECT
            @Advance_Notofication_Days = CAST(ISNULL(con.NOTIFICATION_DAYS, 0) + ISNULL(con.Advance_Termination_Notofication_Days, 0) AS INT)
      FROM
            dbo.CONTRACT con
      WHERE
            con.CONTRACT_ID = @Contract_Id;
              
      INSERT      INTO @Meter_Ids
                  ( 
                   Meter_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Meter_Id_List, ',') ufn;    


      INSERT      INTO dbo.Contract_Notification_Log
                  ( 
                   Contract_Id
                  ,Notification_Msg_Queue_Id
                  ,Last_Change_Ts
                  ,Meter_Term_Dt
                  ,Is_On_Demand
                  ,Advance_Notofication_Days )
                  SELECT
                        @Contract_Id
                       ,@Notification_Msg_Queue_Id
                       ,GETDATE()
                       ,CASE WHEN @Flow_Verification = 'Registration Notification' THEN cha.Supplier_Meter_Association_Date
                             WHEN @Flow_Verification = 'Termination Vendor' THEN cha.Supplier_Meter_Disassociation_Date
                             WHEN @Flow_Verification = 'Termination Analyst' THEN cha.Supplier_Meter_Disassociation_Date
                        END
                       ,@Is_On_Demand
                       ,@Advance_Notofication_Days
                  FROM
                        Core.Client_Hier_Account cha
                        JOIN @Meter_Ids mtr
                              ON cha.Meter_Id = mtr.Meter_Id
                  WHERE
                        cha.Supplier_Contract_ID = @Contract_Id
                  GROUP BY
                        CASE WHEN @Flow_Verification = 'Registration Notification' THEN cha.Supplier_Meter_Association_Date
                             WHEN @Flow_Verification = 'Termination Vendor' THEN cha.Supplier_Meter_Disassociation_Date
                             WHEN @Flow_Verification = 'Termination Analyst' THEN cha.Supplier_Meter_Disassociation_Date
                        END;

      SET @Contract_Notification_Log_Id = SCOPE_IDENTITY();
      
      INSERT      INTO dbo.Contract_Notification_Log_Meter_Dtl
                  ( 
                   Contract_Notification_Log_Id
                  ,Meter_Id )
                  SELECT
                        @Contract_Notification_Log_Id
                       ,mtr.Meter_Id
                  FROM
                        @Meter_Ids mtr;
                              
                 
END;



;
GO
GRANT EXECUTE ON  [dbo].[Contract_Notification_Log_Ins] TO [CBMSApplication]
GO
