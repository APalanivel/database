
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:  dbo.BUDGET_GET_FORECAST_TOTAL_VOLUME_P

 DESCRIPTION:
 
 INPUT PARAMETERS:
 Name					DataType	Default Description
------------------------------------------------------------
@userId					VARCHAR(10)
@sessionId				VARCHAR(20)
@clientId				INT
@divisionId				INT
@siteId					INT
@fromDate				DATETIME
@toDate					DATETIME

 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  



 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 HG			Hariharasuthan Ganesan
 
 MODIFICATIONS:  
 Initials	Date		Modification  
------------------------------------------------------------  
 HG			2017-03-28	MAINT-5087, modified the logic to save the most recent forecast as of date specific to a site/division/client in a variable and use it in the other query.
									Earlier it was failing if the RM_FORECAST_VOLUME_ID specific site/division is different from other volume ids added for that client.
*********/
CREATE PROCEDURE [dbo].[BUDGET_GET_FORECAST_TOTAL_VOLUME_P]
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@clientId INT
     ,@divisionId INT
     ,@siteId INT
     ,@fromDate DATETIME
     ,@toDate DATETIME
AS
BEGIN
      SET NOCOUNT ON
	
      DECLARE @Most_Recent_Forecast_As_Of_Dt DATETIME 
	
      IF @siteId > 0
            BEGIN
            
                  SELECT
                        @Most_Recent_Forecast_As_Of_Dt = MAX(FORECAST_AS_OF_DATE)
                  FROM
                        RM_FORECAST_VOLUME rfv
                        INNER JOIN dbo.RM_FORECAST_VOLUME_DETAILS vd
                              ON vd.RM_FORECAST_VOLUME_ID = rfv.RM_FORECAST_VOLUME_ID
                  WHERE
                        rfv.CLIENT_ID = @clientId
                        AND vd.SITE_ID = @siteId
            
                  SELECT
                        sit.SITE_ID
                       ,details.MONTH_IDENTIFIER
                       ,CAST(SUM(details.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) volume
                  FROM
                        RM_FORECAST_VOLUME forecast_volume
                       ,RM_FORECAST_VOLUME_DETAILS details
                       ,SITE sit
                       ,CONSUMPTION_UNIT_CONVERSION consumption
                       ,RM_ONBOARD_HEDGE_SETUP onboard
                  WHERE
                        details.SITE_ID = sit.SITE_ID
                        AND details.RM_FORECAST_VOLUME_ID = forecast_volume.RM_FORECAST_VOLUME_ID
                        AND forecast_volume.FORECAST_AS_OF_DATE = @Most_Recent_Forecast_As_Of_Dt
                        AND forecast_volume.CLIENT_ID = @clientId
                        AND details.MONTH_IDENTIFIER BETWEEN @fromDate
                                                     AND     @toDate
                        AND sit.SITE_ID = @siteId
                        AND onboard.SITE_ID = sit.SITE_ID
                        AND onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                        AND consumption.CONVERTED_UNIT_ID = 25
                  GROUP BY
                        details.MONTH_IDENTIFIER
                       ,sit.SITE_ID
                       ,consumption.CONVERSION_FACTOR
            END
      ELSE
            IF @divisionId > 0
                  BEGIN
                  
                        SELECT
                              @Most_Recent_Forecast_As_Of_Dt = MAX(FORECAST_AS_OF_DATE)
                        FROM
                              RM_FORECAST_VOLUME rfv
                              INNER JOIN dbo.RM_FORECAST_VOLUME_DETAILS vd
                                    ON vd.RM_FORECAST_VOLUME_ID = rfv.RM_FORECAST_VOLUME_ID
                              INNER JOIN dbo.SITE sit
                                    ON sit.SITE_ID = vd.SITE_ID
                        WHERE
                              rfv.CLIENT_ID = @clientId
                              AND sit.DIVISION_ID = @divisionId

                        SELECT
                              sit.SITE_ID
                             ,details.MONTH_IDENTIFIER
                             ,CAST(SUM(details.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) volume
                        FROM
                              RM_FORECAST_VOLUME forecast_volume
                             ,RM_FORECAST_VOLUME_DETAILS details
                             ,SITE sit
                             ,CONSUMPTION_UNIT_CONVERSION consumption
                             ,RM_ONBOARD_HEDGE_SETUP onboard
                        WHERE
                              details.SITE_ID = sit.SITE_ID
                              AND details.RM_FORECAST_VOLUME_ID = forecast_volume.RM_FORECAST_VOLUME_ID
                              AND forecast_volume.FORECAST_AS_OF_DATE = @Most_Recent_Forecast_As_Of_Dt
                              AND forecast_volume.CLIENT_ID = @clientId
                              AND details.MONTH_IDENTIFIER BETWEEN @fromDate
                                                           AND     @toDate
                              AND sit.DIVISION_ID = @divisionId
                              AND onboard.SITE_ID = sit.SITE_ID
                              AND onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                              AND consumption.CONVERTED_UNIT_ID = 25
                        GROUP BY
                              details.MONTH_IDENTIFIER
                             ,sit.SITE_ID
                             ,consumption.CONVERSION_FACTOR

                  END
            ELSE --//Client level
                  BEGIN
                  
                        SELECT
                              @Most_Recent_Forecast_As_Of_Dt = MAX(FORECAST_AS_OF_DATE)
                        FROM
                              RM_FORECAST_VOLUME
                        WHERE
                              CLIENT_ID = @clientId 

                        SELECT
                              sit.SITE_ID
                             ,details.MONTH_IDENTIFIER
                             ,CAST(SUM(details.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) volume
                        FROM
                              RM_FORECAST_VOLUME forecast_volume
                             ,RM_FORECAST_VOLUME_DETAILS details
                             ,SITE sit
                             ,CONSUMPTION_UNIT_CONVERSION consumption
                             ,RM_ONBOARD_HEDGE_SETUP onboard
                        WHERE
                              details.SITE_ID = sit.SITE_ID
                              AND details.RM_FORECAST_VOLUME_ID = forecast_volume.RM_FORECAST_VOLUME_ID
                              AND forecast_volume.FORECAST_AS_OF_DATE = @Most_Recent_Forecast_As_Of_Dt
                              AND forecast_volume.CLIENT_ID = @clientId
                              AND details.MONTH_IDENTIFIER BETWEEN @fromDate
                                                           AND     @toDate
                              AND onboard.SITE_ID = sit.SITE_ID
                              AND onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                              AND consumption.CONVERTED_UNIT_ID = 25
                        GROUP BY
                              details.MONTH_IDENTIFIER
                             ,sit.SITE_ID
                             ,consumption.CONVERSION_FACTOR

                  END
END











;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_FORECAST_TOTAL_VOLUME_P] TO [CBMSApplication]
GO
