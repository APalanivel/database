
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME: dbo.UBM_CASS_CONTROL_BULK_INSERT_P
      
DESCRIPTION:      
      
	To bulk load the control file received from UBM

INPUT PARAMETERS:         
    Name			DataType				Default     Description
-------------------------------------------------------------------            
	@dirName		VARCHAR(100)
	@fileName		VARCHAR(100)
	@server			VARCHAR(100)

OUTPUT PARAMETERS:
      Name			DataType          Default     Description
----------------------------------------------------------------    

USAGE EXAMPLES:
----------------------------------------------------------------

	EXEC dbo.UBM_CASS_CONTROL_BULK_INSERT_P  16, -1

AUTHOR INITIALS:
Initials	Name     
------------------------------------------------------------        
HG			Harihara Suthan G

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
HG			2014-09-24	Comments header added and modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma

******/
CREATE PROCEDURE [dbo].[UBM_CASS_CONTROL_BULK_INSERT_P]
	@dirName VARCHAR(100),
	@fileName VARCHAR(100),
	@server VARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @bulkInsert VARCHAR(8000)

	IF (@fileName LIKE '%summit_%' ) -- *** Schneider Electric
	 BEGIN
	 
		SET @bulkInsert = 'BULK INSERT UBM_CASS_IMAGES_CONTROL ' +
			' FROM ' + '''' + '\\'+ @server + '\SchneiderExtract\image\' + @dirName + '\' + @fileName + '''' +
			' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\cass_imagecount.fmt' + '''' + ')'

	 END
	ELSE IF (@fileName LIKE '%prokarma_%' ) -- *** Schneider Electric
	 BEGIN
	 
		SET @bulkInsert = 'BULK INSERT UBM_CASS_IMAGES_CONTROL ' +
			' FROM ' + '''' + '\\'+ @server + '\ProkarmaExtract\image\' + @dirName + '\' + @fileName + '''' +
			' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\cass_imagecount.fmt' + '''' + ')'

	 END
	ELSE IF (@fileName LIKE 'cass%') -- *** CASS
	 BEGIN
	 
		SET @bulkInsert = ' BULK INSERT UBM_CASS_IMAGES_CONTROL ' + 
			' FROM ' + '''' +  '\\' + @server + '\CassExtract\image\' + @dirName + '\' + @fileName + '''' +
			' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\cass_imagecount.fmt' + '''' + ')'
	 END
	ELSE IF (@fileName LIKE 'gfe%') -- *** GFE
	 BEGIN
		
		SET @bulkInsert = ' BULK INSERT UBM_CASS_IMAGES_CONTROL ' +
			' FROM '+ '''' + '\\' + @server + '\GfeExtract\image\' + @dirName + '\' + @fileName + '''' +
			' WITH (MAXERRORS=0, TABLOCK, KEEPNULLS, FORMATFILE =' + '''' + 'C:\UBMFormatFiles\gfe_imagecount.fmt' + '''' + ')'
		
	 END

	EXEC(@bulkInsert)

END

;
GO

GRANT EXECUTE ON  [dbo].[UBM_CASS_CONTROL_BULK_INSERT_P] TO [CBMSApplication]
GO
