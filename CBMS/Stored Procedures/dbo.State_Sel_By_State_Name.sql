SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.State_Sel_By_State_Name            
                        
 DESCRIPTION:                        
			To get the State_Id based on State_Name.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@State_Name					VARCHAR(200)                     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
     EXEC [dbo].[State_Sel_By_State_Name] 
		@State_Name='CA'    
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-12-08       Created                
                       
******/                 
                
CREATE PROCEDURE [dbo].[State_Sel_By_State_Name]
      ( 
       @State_Name VARCHAR(200) )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      SELECT
            STATE_ID
      FROM
            [dbo].[STATE]
      WHERE
            STATE_NAME = @State_Name
                        
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[State_Sel_By_State_Name] TO [CBMSApplication]
GO
