SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:		Change_Control_Process_Check_Thresholds

DESCRIPTION: Checks change_Table_Threshold to see if any tables have hit the threshold. 
	Procedure is executed by the Change_Control_Queue When a Check_Threshold mesage is sent
	
	-- If a time or change Count threshold is hit; the stored procedure specified in 
		Change_Control_Threshold.Table_Change_Procedure is executed
	
	-- If no changes are made for longer than the Change Tracking Retention period the 
		Change_Control_Threshold is updated to the current_Change_tracking_Version()


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message				XML								 Contains a list of tables to check thresholds for
	,@Conversation_Handle	UNIQUEIDENTIFIER	NULL  		Conversation Handle may be null if called outside of service broker 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
Change_Control_Process_Check_Thresholds 'Core.Client_Hier'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			10/01/2011	Created
	CMH			10/19/2011  Added code to get Current_Version_Id from Change_Control_Threshold table
	CMH			11/04/2011	Renamed Change_Control_Check_Table_Thresholds for consistancy
	CMH			11/30/2011  Combined the Change_Control_Check_Thresholds and Change_Control_Process_Check_Thresholds
								Replaced @Table_name paramter with @message and @Conversation_Handle
******/
CREATE PROCEDURE [dbo].[Change_Control_Process_Check_Thresholds]
(	@Message				XML									-- Contains a list of tables to check thresholds for
	,@Conversation_Handle	UNIQUEIDENTIFIER	= NULL  )		-- Conversation Handle may be null if called outside of service broker 
AS 
BEGIN
	
	-- Get the tables from the message
	DECLARE 
		@i INT = 1 
		,@MaxI INT  = 1
		,@Param_Definition NVARCHAR(255)= N'@Last_Version_Id BIGINT, @Current_Version_Id BIGINT'
		,@MessageTableCnt INT = 0 
	
	DECLARE @InTableList TABLE (Table_Name Varchar(255))
		
	DECLARE @Table_List TABLE 
	(	Id							INT	identity NOT NULL
		,Table_Name					VARCHAR(255) NOT NULL
		,Table_Change_Procedure		VARCHAR(255) NOT NULL
		,Last_Version_Id			BIGINT		 NOT NULL	) 

	-- Get the list list from the message if tehy were included
	INSERT @InTableList 
	(	Table_Name)
	SELECT 
		cng.c.query('.').value('Table_Name[1]', 'Varchar(255)')
	FROM 
		@message.nodes('/Check_Threshold/Table_Name') cng(c)
		
	SET @MessageTableCnt = (SELECT count(1) FROM @InTableList WHERE Table_Name != '')


	/* Protect against change tracking retention periods running out. 
			If There have been no changes for longer than the change tracking retention, 
				then reset the Last_Version_Id */
	UPDATE cct
	SET Last_Version_Id = Change_Tracking_Current_Version() 
	FROM 
		Change_Control_Threshold cct
	WHERE 
		cct.Current_Change_Count = 0 
		AND ( @MessageTableCnt = 0 
				OR EXISTS (SELECT 1 FROM @InTableList t1 WHERE t1.Table_Name = cct.Table_Name))
		AND cct.Last_Version_Id <= CHANGE_TRACKING_MIN_VALID_VERSION(object_id(cct.Table_Name))


	-- Get a list of tables where the threshold has been met (based on input parameter) 
	INSERT @Table_List
    (	Table_Name
		,Table_Change_Procedure
		,Last_Version_Id)
	SELECT 
		Table_Name
		,Table_Change_Procedure
		,Last_Version_Id
	FROM 
		Change_Control_Threshold cct
	WHERE
		( @MessageTableCnt = 0 
				OR EXISTS (SELECT 1 FROM @InTableList t1 WHERE t1.Table_Name = cct.Table_Name) )
		AND ( Current_Change_Count >= Change_Count_Threshold
				OR datediff(mi, Last_Data_Push_ts, getdate()) >= Time_Threshold_Mins )
	
	SET @MaxI = scope_identity()

	-- Loop through each procedure			
	WHILE ( @i <= @MaxI )
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION 
				DECLARE 
					@Curr_Table VARCHAR(255)
					,@Curr_Procedure NVARCHAR(255)
					,@Param_Version_Id BIGINT
					,@param_Current_Version_Id BIGINT = Change_Tracking_Current_Version() 
					
				-- Get the Saved Info
				SELECT 
					@Curr_Table = Table_Name
					,@Curr_Procedure = Table_Change_Procedure
					,@Param_Version_Id = Last_Version_Id
				FROM 
					 @Table_List
				WHERE 
					Id = @I

				-- Execute the procedure to send information
				DECLARE @Stmt NVARCHAR(1000) = N'EXEC ' + @Curr_Procedure + ' @Last_Version_Id, @Current_Version_Id'
				EXEC sp_EXECUTESQL 
					@Statement = @Stmt
					,@params = @Param_Definition
					,@Last_Version_Id = @Param_Version_Id
					,@Current_Version_Id = @Param_Current_Version_Id
				
				-- Reset the threshold values 
				UPDATE Change_Control_Threshold
				SET 
					Current_Change_Count = 0 
					,Last_Data_Push_ts = getdate() 
					,Last_Version_Id = @Param_Current_Version_Id
				WHERE 
					Table_Name = @Curr_Table
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH 
			ROLLBACK TRANSACTION 
			EXEC usp_RethrowError
		END CATCH
		SET @i = @I + 1 
	END
	
	IF @Conversation_Handle IS NOT NULL
	BEGIN
		END CONVERSATION @Conversation_Handle 
	END 
END

GO
GRANT EXECUTE ON  [dbo].[Change_Control_Process_Check_Thresholds] TO [CBMSApplication]
GO
