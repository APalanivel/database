SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_BUDGET_NAME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec BUDGET_GET_ACCOUNTS_P '','',1052, 290

CREATE  PROCEDURE dbo.BUDGET_GET_BUDGET_NAME_P
	@user_id varchar(10),
	@session_id varchar(20),
	@clientId int
	AS
begin
	set nocount on
		select budget_name from budget where client_id=@clientId	
	
	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_NAME_P] TO [CBMSApplication]
GO
