
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default			Description
-----------------------------------------------------------------------
	@rfpAccountId  	int       	          	
	@isBidGroup    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default			Description
-----------------------------------------------------------------------

USAGE EXAMPLES:
-----------------------------------------------------------------------

EXEC SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P 10002412,1

EXEC SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P 12012606,0

EXEC SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P 3100,1

EXEC SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P 12060960,0


AUTHOR INITIALS:
	Initials		Name
-----------------------------------------------------------------------
	NR				Narayana Reddy	

MODIFICATIONS

 Initials		Date			Modification
-----------------------------------------------------------------------
	        	9/21/2010		Modify Quoted Identifier
 DMR		    09/10/2010		Modified for Quoted_Identifier
 NR				2016-07-15		GCS-1183 - Added Local_Cd Values. 


******/

CREATE  PROCEDURE [dbo].[SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P]
      ( 
       @rfpAccountId INT
      ,@isBidGroup INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            v.vendor_id AS supplierId
           ,v.VENDOR_NAME AS supplierName
           ,ssci.SR_SUPPLIER_CONTACT_INFO_ID AS contactId
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME AS contactName
           ,srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID AS supplierContactVendorMapId
           ,cd.Code_Id AS Locale_Cd
           ,cd.Code_Value AS Locale_Value
      FROM
            SR_RFP_ACCOUNT_TERM srat
            INNER JOIN SR_RFP_TERM_PRODUCT_MAP srtpm
                  ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN SR_RFP_SELECTED_PRODUCTS srsp
                  ON srtpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN SR_PRICING_PRODUCT srp
                  ON srsp.PRICING_PRODUCT_ID = srp.SR_PRICING_PRODUCT_ID
            INNER JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srscvm
                  ON srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srscvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            INNER JOIN SR_SUPPLIER_CONTACT_INFO ssci
                  ON srscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN VENDOR v
                  ON srscvm.VENDOR_ID = v.VENDOR_ID
            INNER JOIN USER_INFO userInfo
                  ON ssci.USER_INFO_ID = userInfo.USER_INFO_ID
            INNER JOIN dbo.Code cd
                  ON cd.Code_Value = userInfo.locale_code
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
            AND srsp.IS_SYSTEM_PRODUCT = 1
            AND srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
            AND srat.IS_BID_GROUP = @isBidGroup
            AND cs.Codeset_Name = 'LocalizationLanguage'
      UNION
      SELECT
            v.vendor_id AS supplierId
           ,v.VENDOR_NAME AS supplierName
           ,ssci.SR_SUPPLIER_CONTACT_INFO_ID AS contactId
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME AS contactName
           ,srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID AS supplierContactVendorMapId
           ,cd.Code_Id AS Locale_Cd
           ,cd.Code_Value AS Locale_Value
      FROM
            SR_RFP_ACCOUNT_TERM srat
            INNER JOIN SR_RFP_TERM_PRODUCT_MAP srtpm
                  ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN SR_RFP_SELECTED_PRODUCTS srsp
                  ON srtpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN SR_RFP_PRICING_PRODUCT srp
                  ON srsp.PRICING_PRODUCT_ID = srp.SR_RFP_PRICING_PRODUCT_ID
            INNER JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srscvm
                  ON srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srscvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            INNER JOIN SR_SUPPLIER_CONTACT_INFO ssci
                  ON srscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN VENDOR v
                  ON srscvm.VENDOR_ID = v.VENDOR_ID
            INNER JOIN USER_INFO userInfo
                  ON ssci.USER_INFO_ID = userInfo.USER_INFO_ID
            INNER JOIN dbo.Code cd
                  ON cd.Code_Value = userInfo.locale_code
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
            AND srsp.IS_SYSTEM_PRODUCT IS NULL
            AND srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
            AND srat.IS_BID_GROUP = @isBidGroup
            AND cs.Codeset_Name = 'LocalizationLanguage'

END

;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SHORTLIST_SUPPLIER_CONTACTS_P] TO [CBMSApplication]
GO
