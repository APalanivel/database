SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Contract_Recalc_Type_SEL  
 
DESCRIPTION:  Used to select account watch list group  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Contract_Recalc_Type_SEL 4719

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/21/2009  Created


******/  


CREATE PROCEDURE dbo.Contract_Recalc_Type_SEL
	@Account_Id Int	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT con.Contract_Recalc_Type_Id, cd.Code_Dsc as RecalcType
	
	FROM SUPPLIER_ACCOUNT_METER_MAP MAP
	JOIN CONTRACT con ON MAP.Contract_Id = con.CONTRACT_ID
	JOIN Code cd ON cd.Code_Id = con.Contract_Recalc_Type_Id
	JOIN CodeSet Cs ON Cs.CodeSet_Id = Cd.CodeSet_Id
	
	WHERE MAP.Account_Id = @Account_Id
		AND Cs.CodeSet_Name ='Recalculation_Type'	
	
	
END
GO
GRANT EXECUTE ON  [dbo].[Contract_Recalc_Type_SEL] TO [CBMSApplication]
GO
