SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  
	
	dbo.CuInvoice_Image_Sel_By_AccountList_Start_End_Date

DESCRIPTION:
	
	Cu Invoice Images select by given client hier id, account list and date ranges

INPUT PARAMETERS:
	Name					   DataType          Default     Description
-------------------------------------------------------------------------
	@Client_Hier_Id				INT
	@Account_Id					VARCHAR(MAX)  
	@Start_Dt					DATE
	@End_Dt						DATE

        
OUTPUT PARAMETERS:
      Name						DataType          Default     Description
-------------------------------------------------------------------------

USAGE EXAMPLES:

EXEC  dbo.CuInvoice_Image_Sel_By_AccountList_Start_End_Date 16995,'74605','2012-01-01','2012-12-01'
EXEC  dbo.CuInvoice_Image_Sel_By_AccountList_Start_End_Date 8736,'11648,11650','2011-01-01','2012-12-01'
Exec  dbo.CuInvoice_Image_Sel_By_AccountList_Start_End_Date 16996,'74654,74658','2012-01-01','2012-12-01'

--------------------------------------------------------------------------

AUTHOR INITIALS:
Initials	Name   
--------------------------------------------------------------------------
RR			Raghu Reddy

Initials Date			Modification
--------------------------------------------------------------------------
RR		  2014-09-04	Created for Data Operations Revamp.

******/
CREATE PROCEDURE [dbo].[CuInvoice_Image_Sel_By_AccountList_Start_End_Date]
      ( 
       @Client_Hier_Id INT
      ,@Account_Id VARCHAR(MAX)
      ,@Start_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE @Account_List TABLE
            ( 
             Account_Id INT PRIMARY KEY )

      INSERT      INTO @Account_List
                  ( 
                   Account_Id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Account_Id, ',')
      SELECT
            cha.Client_Hier_Id
           ,cism.Account_ID
           ,cha.Display_Account_Number Account_Number
           ,ci.CU_INVOICE_ID
           ,cism.SERVICE_MONTH
           ,imag.CBMS_IMAGE_ID
           ,imag.CBMS_DOC_ID
      FROM
            @Account_List acc_list
            INNER JOIN core.Client_Hier_Account cha
                  ON acc_list.Account_Id = cha.Account_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON acc_list.Account_Id = cism.Account_ID
            INNER JOIN dbo.CU_INVOICE ci
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
            INNER JOIN dbo.Cbms_Image imag
                  ON ci.CBMS_IMAGE_ID = imag.CBMS_IMAGE_ID
      WHERE
            cha.Client_Hier_Id = @Client_Hier_Id
            AND cism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
            AND ci.IS_REPORTED = 1
            AND ci.IS_PROCESSED = 1
            AND ci.IS_DNT = 0
            AND ci.IS_DUPLICATE = 0
      GROUP BY
            cha.Client_Hier_Id
           ,cism.Account_ID
           ,cha.Display_Account_Number
           ,ci.CU_INVOICE_ID
           ,cism.SERVICE_MONTH
           ,imag.CBMS_IMAGE_ID
           ,imag.CBMS_DOC_ID
      ORDER BY
            cha.Display_Account_Number
           ,cism.SERVICE_MONTH 

END;


;
GO
GRANT EXECUTE ON  [dbo].[CuInvoice_Image_Sel_By_AccountList_Start_End_Date] TO [CBMSApplication]
GO
