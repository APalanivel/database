SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure dbo.seStorage_GetAll
As
BEGIN
	set nocount on
	 select StorageId
				, ReportDate
				, WeekEnding
				, WeekOfYear
				, Volume
				, Change
		 from seStorage
 order by ReportDate desc
END
GO
GRANT EXECUTE ON  [dbo].[seStorage_GetAll] TO [CBMSApplication]
GO
