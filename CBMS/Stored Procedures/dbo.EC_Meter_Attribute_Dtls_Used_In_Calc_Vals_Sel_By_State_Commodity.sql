SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity         
                
Description:                
   This sproc get the attribute details for the given state and commodity.                 
                
 Input Parameters:                
    Name					DataType			Default			Description                  
----------------------------------------------------------------------------------------                  
    @Commodity_Id			INT  
    @State_Id				INT    
      
 Output Parameters:                      
    Name					DataType			Default			Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity 236,290  
   Exec dbo.EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity 136,290  
   Exec dbo.EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity 73,290      
  
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy            
	     
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2015-04-22		Created For AS400.           
               
******/   
  
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity]
      ( 
       @State_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
              
      SELECT
            ema.EC_Meter_Attribute_Id
           ,ema.EC_Meter_Attribute_Name
           ,c.Code_Value AS Attribute_Type
           ,emav.EC_Meter_Attribute_Value_Id
           ,emav.EC_Meter_Attribute_Value
      FROM
            dbo.EC_Meter_Attribute ema
            INNER JOIN dbo.Code c
                  ON c.Code_Id = ema.Attribute_Type_Cd
            INNER JOIN dbo.EC_Meter_Attribute_Value emav
                  ON ema.EC_Meter_Attribute_Id = emav.EC_Meter_Attribute_Id
      WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id
            AND ema.Is_Used_In_Calc_Vals = 1
             
              
END 

;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Dtls_Used_In_Calc_Vals_Sel_By_State_Commodity] TO [CBMSApplication]
GO
