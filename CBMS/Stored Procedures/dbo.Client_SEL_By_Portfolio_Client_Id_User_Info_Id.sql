SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   dbo.Client_SEL_By_Portfolio_Client_Id_User_Info_Id

DESCRIPTION:  
Gets all the clients related to that Portfolio_client_Id with a flag to denote on which side the clients should be shown.
	-->	If the user has access to all the clients under the portfolio, Show_On_Left will be 1 for all the clients
	-->	If the user doesn't have access to any client under the portfolio, Show_On_Left will be 1 for all the clients
	-->	If the user has access to only few clients, Show_On_Left will be 0 for all the clients to which the user has access
		and it will be 1 for all the clients to which the user doesn't have access.
	
	-->	Is_Portfolio_User flag will be 1 if the user has access to atleast one client under the portfolio
	
	-->	If there is no resultset coming from this SP, the bottom section should not be shown at all as this is not a 
		portfolio client
	

INPUT PARAMETERS:          
Name                       DataType     Default Description          
------------------------------------------------------------          
@User_Info_Id               INT
@Portfolio_client_Id        INT             

OUTPUT PARAMETERS:          
Name                    DataType        Default         Description          
------------------------------------------------------------ 


USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC dbo.Client_SEL_By_Portfolio_Client_Id_User_Info_Id 49,11725

AUTHOR INITIALS:          
Initials    Name          
------------------------------------------------------------          
CPE			Chaitanya Panduga Eshwar

MODIFICATIONS           
Initials          Date			Modification          
------------------------------------------------------------          
CPE              2012-03-15		Created

*****/

CREATE PROC dbo.Client_SEL_By_Portfolio_Client_Id_User_Info_Id
      @User_Info_Id INT
     ,@Portfolio_client_Id INT
AS 
BEGIN 

      SET NOCOUNT ON ;

      DECLARE @Result TABLE
		( 
		 Client_Id INT
		,Client_Name VARCHAR(500)
		,Show_Client_Access INT )

      INSERT
            @Result
            ( 
             Client_Id
            ,Client_Name
            ,Show_Client_Access )
            SELECT
                  CH.Client_Id
                 ,CH.Client_Name
                 ,CASE WHEN USR.Security_Role_Id IS NULL THEN 1
                       ELSE 0
                  END AS Show_Client_Access -- If user doesn't have access, show_one_left = 1
            FROM
                  Core.Client_Hier CH
                  JOIN dbo.Security_Role SR
                        ON CH.Client_Id = SR.Client_Id
                  LEFT JOIN dbo.User_Security_Role USR
                        ON SR.Security_Role_Id = USR.Security_Role_Id
                           AND USR.User_Info_Id = @User_Info_Id
            WHERE
                  CH.Portfolio_Client_Id = @Portfolio_Client_Id
                  AND CH.Sitegroup_Id = 0
                  AND SR.Is_Corporate = 1

	  -- If there is atleast 1 record with Show_Client_Access = 0 i.e. user has access to atleast one client under the
	  -- portfolio client, Is_Portfolio_user will be 1
      
      SELECT
            Is_Portfolio_User = CASE WHEN MIN(Show_Client_Access) = 0 THEN 1
                                     ELSE 0
                                END
      FROM
            @Result
      
	  -- If there are no records with Show_Client_Access = 1, i.e. the user has access to all the clients under the
	  -- portfolio, set the show_one_left to 1.
      UPDATE
            @Result
      SET   Show_Client_Access = 1
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @Result
                         WHERE
                              Show_Client_Access = 1 )

      SELECT
            Client_Id
           ,Client_Name
           ,Show_Client_Access
           ,@Portfolio_client_Id AS Portfolio_client_Id
      FROM
            @Result
      ORDER BY
            Client_Name
END
GO
GRANT EXECUTE ON  [dbo].[Client_SEL_By_Portfolio_Client_Id_User_Info_Id] TO [CBMSApplication]
GO
