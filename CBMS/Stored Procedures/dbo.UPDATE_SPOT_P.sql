SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_SPOT_P
	@spot_from_date DATETIME, 	
	@spot_to_date DATETIME,
	@spot_volume DECIMAL(32,16),
	@deal_type_id INT,
	@spot_price DECIMAL(32,16),
	@spot_unit_type_id INT,
	@spot_transaction_date DATETIME,
	@transaction_by_type_id INT,
	@spot_comments VARCHAR(4000),
	@spot_name VARCHAR(200),
	@spot_id INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.spot
	SET	spot_from_date=@spot_from_date,
		spot_to_date=@spot_to_date,
		spot_volume=@spot_volume,
		deal_type_id=@deal_type_id,
		spot_price=@spot_price,
		spot_unit_type_id=@spot_unit_type_id,
		spot_transaction_date=@spot_transaction_date,
		transaction_by_type_id=@transaction_by_type_id,
		spot_comments=@spot_comments,
		spot_name=@spot_name 
	WHERE spot_id=@spot_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SPOT_P] TO [CBMSApplication]
GO
