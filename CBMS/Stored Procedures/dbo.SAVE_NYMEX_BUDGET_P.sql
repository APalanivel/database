SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from entity where entity_description like '%BUDGET%'

--select * from rm_budget

--select entity_id from entity	where entity_type=259 and entity_name='Site'

CREATE     PROCEDURE dbo.SAVE_NYMEX_BUDGET_P
@userId varchar(10),
@sessionId varchar(20),

@divisionId int,
@siteId int,
@clientId int,
@depricatedType int,
@budgetModeId int,
@levelName varchar(20),
@creationDate dateTime,
@startMonth dateTime,
@endMonth dateTime,
@budgetName Varchar(1000),
@budgetXML text,
@approveTypeId int,
@approveByCem int,
@approvedDate dateTime,
@clientGenerated int,
@unitTypeId int,
@currencyId int

as
	set nocount on
	declare @budgetTypeId int, @budgetLevel int, @approveId  int

	Select @budgetTypeId=entity_id from entity
	where entity_type=257 and entity_name='Nymex Budget'

	select @budgetLevel=entity_id from entity
	where entity_type=259 and entity_name=@levelName

	select @approveId=entity_id from entity
	where entity_type=260 and entity_name ='Conservative Budget'
	

	insert into rm_budget 
		(division_id,site_id,
		 client_id,budget_type_id,
		 budget_level_type_id,
		 budget_creation_date,budget_start_month,
		 budget_end_month,budget_name,budget_xml,
		 APPROVED_TYPE_ID,
		 is_budget_approved,
		 approved_date,is_client_generated)
		values
		(
		 @divisionId,@siteId,@clientId,@budgetTypeId,
		 @budgetLevel,@creationDate,
		 @startMonth,@endMonth,@budgetName,@budgetXML,@approveId,
		@clientGenerated,
		 @approvedDate, @clientGenerated
		)
GO
GRANT EXECUTE ON  [dbo].[SAVE_NYMEX_BUDGET_P] TO [CBMSApplication]
GO
