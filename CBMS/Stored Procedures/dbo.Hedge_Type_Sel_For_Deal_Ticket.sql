SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
NAME:                                
    dbo.Hedge_Type_Sel_For_Deal_Ticket                                
                                
DESCRIPTION:                                
                           
                                
INPUT PARAMETERS:                                
	Name				DataType		Default Description                                
---------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT
    @Start_Dt			DATE
    @End_Dt				DATE
    @Hedge_Type			INT
                          
                       
OUTPUT PARAMETERS:                                
 Name   DataType  Default Description                                
---------------------------------------------------------------    


	EXEC dbo.Hedge_Type_Sel_For_Deal_Ticket
	               
 
USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
                   
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-10-25  Created For Risk Management

******/
CREATE PROCEDURE [dbo].[Hedge_Type_Sel_For_Deal_Ticket]
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            ent.ENTITY_ID
           ,ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_TYPE = 273
            AND ENTITY_NAME IN ( 'Physical', 'Financial' )
      ORDER BY
            ent.DISPLAY_ORDER

END;

GO
GRANT EXECUTE ON  [dbo].[Hedge_Type_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
