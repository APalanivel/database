SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE           PROCEDURE [dbo].[cbmsInvoice_GetForAccountMonth]
	( @MyAccountId int
	, @account_id int
	, @service_month datetime
	)
AS
BEGIN

	   select i.invoice_id
		, i.cbms_image_id
		, i.invoice_for_month 		service_month
		, i.is_invoice_effective 	is_default
		, i.invoice_from_date		begin_date
		, i.invoice_to_date		end_date
		, i.invoice_grand_total
		, ea.user_info_id 		created_by_id
		, ui.username 			created_by
		, i.invoice_creation_date 	created_date
	     from invoice i with (nolock)
	     join entity_audit ea with (nolock) on ea.entity_identifier = i.invoice_id and ea.entity_id = 501
	     join user_info ui with (nolock) on ui.user_info_id = ea.user_info_id
	    where i.account_id = @account_id
	      and i.invoice_for_month = @service_month
	 order by i.invoice_creation_date asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoice_GetForAccountMonth] TO [CBMSApplication]
GO
