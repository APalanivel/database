SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.Commodity_Sel_All_Invoice_Services

DESCRIPTION:

	Used to get EP, NG and other services commodities for client service map.

INPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  
       
OUTPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.Commodity_Sel_All_Invoice_Services 1,'Natural',1,100
	EXEC dbo.Commodity_Sel_All_Invoice_Services 0

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
HG			Hari
SP			Sandeep Pigilam

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
HG			03/25/2010	Created
SP			2015-08-07  Data Source Management Phase II,Added Is_Client_Specific in order by clause after Is_Alternate_Fuel column.

******/

CREATE PROCEDURE [dbo].[Commodity_Sel_All_Invoice_Services]
     (
         @Is_Active INT = 1
         , @Keyword NVARCHAR(MAX) = NULL
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647

     )
AS
    BEGIN

        SET NOCOUNT ON;
        WITH Cte_Commodity
        AS (
               SELECT
                    Commodity_Id
                    , Commodity_Name
                    , Default_UOM_Entity_Type_Id
                    , UOM_Entity_Type
                    , Is_Alternate_Fuel
                    , Is_Sustainable
                    , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                       c.Commodity_Id)
                    , Total_Rows = COUNT(1) OVER ()
               FROM
                    dbo.Commodity c
               WHERE
                    Is_Active = @Is_Active
                    AND (   Is_Alternate_Fuel = 1
                            OR  Commodity_Name IN ( 'Electric Power', 'Natural Gas' ))
                    AND Commodity_Id > 0
                    AND (   @Keyword IS NULL
                            OR  c.Commodity_Name LIKE '%' + @Keyword + '%')
               GROUP BY
                   Commodity_Id
                   , Commodity_Name
                   , Default_UOM_Entity_Type_Id
                   , UOM_Entity_Type
                   , Is_Alternate_Fuel
                   , Is_Sustainable
           )
        SELECT
            com.Commodity_Id
            , com.Commodity_Name
            , com.Default_UOM_Entity_Type_Id
            , com.UOM_Entity_Type
            , com.Is_Alternate_Fuel
            , com.Is_Sustainable
            , com.Row_Num
            , com.Total_Rows
        FROM
            Cte_Commodity com
        WHERE
            com.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            com.Is_Alternate_Fuel
            , com.Commodity_Name;
    END;

GO

GRANT EXECUTE ON  [dbo].[Commodity_Sel_All_Invoice_Services] TO [CBMSApplication]
GO
