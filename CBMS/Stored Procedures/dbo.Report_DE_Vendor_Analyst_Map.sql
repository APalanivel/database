
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Report_DE_Vendor_Analyst_Map         
  
      
DESCRIPTION:   
 Gets all Vendor, Analyst details
   
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
EXEC dbo.Report_DE_Vendor_Analyst_Map  
         
       
AUTHOR INITIALS:            
Initials	Name            
------------------------------------------------------------            
AKR			Ashok Kumar Raju       
       
MODIFICATIONS             
Initials	Date		Modification            
------------------------------------------------------------            
AKR			2012-09-06	Created  
AKR			2012-10-09	Modified the Join on VENDOR_STATE_MAP
AKR			2014-09-10  Modified the code remove Utility_Analyst_Map View
HG			2014-10-01	Group_Name replaced by code_value of group_legacy_name
*****/ 

CREATE PROCEDURE [dbo].[Report_DE_Vendor_Analyst_Map]
AS 
BEGIN 

      DECLARE @vendor_Type INT

      DECLARE @Utility_Commodity_Cnt TABLE
            ( 
             Vendor_Id INT
            ,Commodity_Cnt INT )
      DECLARE @Supplier_Commodity_Cnt TABLE
            ( 
             Vendor_Id INT
            ,Commodity_Cnt INT )
      
      CREATE TABLE #Utility_Analyst_Map
            ( 
             Utility_Detail_ID INT
            ,Regulated_Analyst_ID INT
            ,Deregulated_Analyst_ID INT
            ,Gas_Analyst_ID INT
            ,Power_Analyst_ID INT
            ,Sourcing_Ops_Analyst_ID INT )
            
      SELECT
            @vendor_Type = e.Entity_Id
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'Utility'
            AND e.ENTITY_DESCRIPTION = 'Vendor'

      INSERT      INTO @Utility_Commodity_Cnt
                  ( 
                   Vendor_Id
                  ,Commodity_Cnt )
                  SELECT
                        v.vendor_id
                       ,COUNT(DISTINCT vcm.commodity_type_id)
                  FROM
                        vendor v
                        JOIN vendor_commodity_map vcm
                              ON vcm.vendor_id = v.vendor_id
                  GROUP BY
                        v.vendor_id
                       ,v.vendor_type_id
                  HAVING
                        COUNT(DISTINCT vcm.commodity_type_id) > 1
                        AND v.vendor_type_id = @vendor_Type

      INSERT      INTO @Supplier_Commodity_Cnt
                  ( 
                   Vendor_Id
                  ,Commodity_Cnt )
                  SELECT
                        cha.Account_Vendor_Id
                       ,COUNT(DISTINCT cha.commodity_id)
                  FROM
                        core.Client_Hier_Account cha
                        INNER JOIN dbo.CONTRACT con
                              ON con.CONTRACT_ID = cha.Supplier_Contract_ID
                  WHERE
                        con.contract_end_date > '2005-12-31'
                        AND cha.Supplier_Meter_Disassociation_Date > con.CONTRACT_START_DATE
                  GROUP BY
                        cha.Account_Vendor_Id
                  HAVING
                        COUNT(DISTINCT cha.commodity_id) > 1
        
      INSERT      INTO #Utility_Analyst_Map
                  ( 
                   Utility_Detail_ID
                  ,Regulated_Analyst_ID
                  ,Deregulated_Analyst_ID
                  ,Gas_Analyst_ID
                  ,Power_Analyst_ID
                  ,Sourcing_Ops_Analyst_ID )
                  SELECT
                        Utility_Detail_ID
                       ,[Regulated_Market] AS Regulated_Analyst_ID
                       ,[Quality_Assurance] AS Deregulated_Analyst_ID
                       ,[Natural Gas] AS Gas_Analyst_ID
                       ,[Electric Power] AS Power_Analyst_ID
                       ,[Invoice_Control] AS Sourcing_Ops_Analyst_ID
                  FROM
                        ( SELECT
                              udam.Utility_Detail_ID
                             ,gln.Code_Value AS Group_Name
                             ,udam.Analyst_Id
                          FROM
                              dbo.Utility_Detail_Analyst_Map UDAM
                              INNER JOIN dbo.GROUP_INFO GI
                                    ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID
                              INNER JOIN dbo.Code gln
                                    ON gln.Code_Id = gi.Group_Legacy_Name_Cd
                          WHERE
                              gln.Code_Value IN ( 'Regulated_Market', 'Quality_Assurance', 'Invoice_Control' )
                          UNION
                          SELECT
                              ud.UTILITY_DETAIL_ID
                             ,com.Commodity_Name
                             ,vcam.Analyst_Id
                          FROM
                              dbo.VENDOR_COMMODITY_MAP vcm
                              INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                                    ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                              INNER JOIN dbo.UTILITY_DETAIL ud
                                    ON ud.VENDOR_ID = vcm.VENDOR_ID
                              INNER JOIN dbo.Commodity com
                                    ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
                          WHERE
                              com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' ) ) AS p PIVOT ( MAX(Analyst_Id) FOR Group_Name IN ( [Regulated_Market], [Quality_Assurance], [Invoice_Control], [Electric Power], [Natural Gas] ) ) AS pvt  
   
      CREATE CLUSTERED INDEX ix_#Utility_Analyst_Map ON #Utility_Analyst_Map(Utility_Detail_ID)   
      
      SELECT
            vendor_name Vendor
           ,commodity = CASE WHEN uti.Commodity_Cnt IS NOT NULL
                                  OR sup.Commodity_Cnt IS NOT NULL THEN 'Both'
                             ELSE commoditytype
                        END
           ,vendortype VendorType
           ,country_name
           ,state_name State
           ,RegulatedAnalyst RegAnalyst
           ,DeregulatedAnalyst DeregAnalyst
           ,SOPSAnalyst SOPSAnalyst
           ,GasAnalyst GasAnalyst
           ,PowerAnalyst PowerAnalyst
      FROM
            ( SELECT
                  v.vendor_name
                 ,v.vendor_id
                 ,st.state_name
                 ,ui.first_name + ' ' + ui.last_name RegulatedAnalyst
                 ,ui1.first_name + ' ' + ui1.last_name DeregulatedAnalyst
                 ,ui2.first_name + ' ' + ui2.last_name SOPSAnalyst
                 ,ui3.first_name + ' ' + ui3.last_name GasAnalyst
                 ,ui4.first_name + ' ' + ui4.last_name PowerAnalyst
                 ,e.entity_name AS VendorType
                 ,e1.entity_name AS CommodityType
                 ,ctry.country_name
              FROM
                  dbo.vendor v
                  JOIN dbo.vendor_state_map vsm
                        ON vsm.vendor_id = v.vendor_id
                  JOIN dbo.utility_detail ud
                        ON ud.vendor_id = v.vendor_id
                  LEFT  JOIN #Utility_Analyst_Map uam
                        ON uam.utility_detail_id = ud.utility_detail_id
                  LEFT JOIN dbo.user_info ui
                        ON ui.user_info_id = uam.regulated_analyst_id
                  LEFT JOIN dbo.user_info ui1
                        ON ui1.user_info_id = uam.deregulated_analyst_id
                  LEFT JOIN dbo.user_info ui2
                        ON ui2.user_info_id = uam.sourcing_ops_analyst_id
                  LEFT JOIN dbo.user_info ui3
                        ON ui3.user_info_id = uam.gas_analyst_id
                  LEFT JOIN dbo.user_info ui4
                        ON ui4.user_info_id = uam.power_analyst_id
                  JOIN dbo.state st
                        ON st.state_id = vsm.state_id
                  JOIN dbo.entity e
                        ON e.entity_id = v.vendor_type_id
                  JOIN dbo.vendor_commodity_map vcm
                        ON vcm.vendor_id = v.vendor_id
                  JOIN dbo.entity e1
                        ON e1.entity_id = vcm.commodity_type_id
                  JOIN dbo.country ctry
                        ON ctry.country_id = st.country_id
              WHERE
                  v.is_history != 1
              GROUP BY
                  v.vendor_name
                 ,v.vendor_id
                 ,st.state_name
                 ,ui.first_name + ' ' + ui.last_name
                 ,ui1.first_name + ' ' + ui1.last_name
                 ,ui2.first_name + ' ' + ui2.last_name
                 ,ui3.first_name + ' ' + ui3.last_name
                 ,ui4.first_name + ' ' + ui4.last_name
                 ,e.entity_name
                 ,e1.entity_name
                 ,ctry.country_name
              UNION
              SELECT
                  cha.Account_Vendor_Name
                 ,cha.Account_Vendor_Id
                 ,s.STATE_NAME
                 ,ui.first_name + ' ' + ui.last_name RegulatedAnalyst
                 ,ui1.first_name + ' ' + ui1.last_name DeregulatedAnalyst
                 ,ui2.first_name + ' ' + ui2.last_name SOPSAnalyst
                 ,ui3.first_name + ' ' + ui3.last_name GasAnalyst
                 ,ui4.first_name + ' ' + ui4.last_name PowerAnalyst
                 ,e.entity_name AS VendorType
                 ,e1.entity_name AS CommodityType
                 ,ctry.country_name
              FROM
                  core.Client_Hier_Account cha
                  INNER JOIN dbo.CONTRACT con
                        ON con.CONTRACT_ID = cha.Supplier_Contract_ID
                  INNER JOIN dbo.VENDOR_STATE_MAP vsm
                        ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                  INNER JOIN dbo.STATE s
                        ON s.STATE_ID = vsm.STATE_ID
                  INNER JOIN dbo.COUNTRY ctry
                        ON ctry.COUNTRY_ID = s.COUNTRY_ID
                  INNER JOIN dbo.ENTITY e
                        ON e.ENTITY_ID = cha.Account_Vendor_Type_ID
                  INNER JOIN dbo.ENTITY e1
                        ON e1.ENTITY_ID = cha.Commodity_Id
                  JOIN dbo.utility_detail ud
                        ON ud.vendor_id = cha.Account_Vendor_Id
                  LEFT JOIN #Utility_Analyst_Map uam
                        ON uam.utility_detail_id = ud.utility_detail_id
                  LEFT JOIN dbo.user_info ui
                        ON ui.user_info_id = uam.regulated_analyst_id
                  LEFT JOIN dbo.user_info ui1
                        ON ui1.user_info_id = uam.deregulated_analyst_id
                  LEFT JOIN dbo.user_info ui2
                        ON ui2.user_info_id = uam.sourcing_ops_analyst_id
                  LEFT JOIN dbo.user_info ui3
                        ON ui3.user_info_id = uam.gas_analyst_id
                  LEFT JOIN dbo.user_info ui4
                        ON ui4.user_info_id = uam.power_analyst_id
              WHERE
                  con.contract_end_date > '2005-12-31'
                  AND cha.Supplier_Meter_Disassociation_Date > con.CONTRACT_START_DATE
              GROUP BY
                  cha.Account_Vendor_Name
                 ,cha.Account_Vendor_Id
                 ,s.STATE_NAME
                 ,ui.first_name + ' ' + ui.last_name
                 ,ui1.first_name + ' ' + ui1.last_name
                 ,ui2.first_name + ' ' + ui2.last_name
                 ,ui3.first_name + ' ' + ui3.last_name
                 ,ui4.first_name + ' ' + ui4.last_name
                 ,e.entity_name
                 ,e1.entity_name
                 ,ctry.country_name ) x
            LEFT JOIN @Utility_Commodity_Cnt uti
                  ON uti.vendor_id = x.vendor_id
            LEFT JOIN @Supplier_Commodity_Cnt sup
                  ON sup.vendor_id = x.vendor_id
      GROUP BY
            vendor_name
           ,CASE WHEN uti.Commodity_Cnt IS NOT NULL
                      OR sup.Commodity_Cnt IS NOT NULL THEN 'Both'
                 ELSE commoditytype
            END
           ,vendortype
           ,country_name
           ,state_name
           ,regulatedanalyst
           ,deregulatedanalyst
           ,sopsanalyst
           ,gasanalyst
           ,poweranalyst 

END;

;
GO



GRANT EXECUTE ON  [dbo].[Report_DE_Vendor_Analyst_Map] TO [CBMSApplication]
GO
