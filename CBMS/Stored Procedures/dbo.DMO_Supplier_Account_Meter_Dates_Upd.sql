SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Meter_Dates_Upd
           
DESCRIPTION:             
			To update DMO supplier account end date
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id					INT
	@Meter_Association_Dt		DATETIME
    @Meter_Disassociation_Dt	DATETIME
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		SELECT TOP 10 * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID = -1
	BEGIN TRANSACTION
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149234
		EXEC dbo.DMO_Supplier_Account_Meter_Dates_Upd 1149234,'2017-03-01 00:00:00.000',NULL
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149234
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149235
		EXEC dbo.DMO_Supplier_Account_Meter_Dates_Upd 1149235,NULL,'2017-12-01 00:00:00.000',NULL
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149235
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-07	Contract placeholder - CP-8 Created
	RR			2017-09-18	MAINT-5902 - Null should be allowed for meter disassocitaion date.
	NR			2019-11-09	Add Contract - Added @Supplier_Account_Config_Id parameter.
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Meter_Dates_Upd]
    (
        @Account_Id INT
        , @Meter_Association_Dt DATETIME
        , @Meter_Disassociation_Dt DATETIME
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN


        SET NOCOUNT ON;

        UPDATE
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        SET
            METER_ASSOCIATION_DATE = @Meter_Association_Dt
            , METER_DISASSOCIATION_DATE = @Meter_Disassociation_Dt
        WHERE
            ACCOUNT_ID = @Account_Id
            AND Contract_ID = -1
            AND Supplier_Account_Config_Id = @Supplier_Account_Config_Id;


    END;

    ;
GO

GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Meter_Dates_Upd] TO [CBMSApplication]
GO
