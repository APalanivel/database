SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.CU_Invoice_Determinant_Account_INS 
     
     
DESCRIPTION:  
	This object is used to insert the record during stage-3 process for CU_INVOICE_Determinant_ACCOUNT

    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
@CU_Invoice_Id	int
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
     
	CU_Invoice_Determinant_Account_INS 7102632
    
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
          
SKA	Shobhit Kumar Agrawal    
    
Initials	Date		Modification      
------------------------------------------------------------      
SKA		06/07/2010	Created    


******/      

CREATE PROCEDURE dbo.CU_Invoice_Determinant_Account_INS
      @CU_Invoice_Id INT
AS 
BEGIN      
      SET NOCOUNT ON ;

	
      INSERT INTO
            CU_Invoice_Determinant_Account
            (
             CU_INVOICE_DETERMINANT_ID
            )
            SELECT
                  CU_INVOICE_DETERMINANT_ID
            FROM
                  dbo.CU_Invoice_Determinant
            WHERE
                  CU_INVOICE_ID = @CU_Invoice_Id
END    


    


GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Account_INS] TO [CBMSApplication]
GO
