SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.GET_DEALTICKET_DETAILS_FOR_MARK_TO_MARKET_INDEX_REPORT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	VARCHAR(1)	          	
	@sessionId     	VARCHAR(1)	          	
	@fromDate      	VARCHAR(12)	          	
	@toDate        	VARCHAR(12)	          	
	@clientId      	int       	          	
	@siteId        	VARCHAR(1000)	          	
	@unitId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	exec GET_DEALTICKET_DETAILS_FOR_MARK_TO_MARKET_INDEX_REPORT_P '-1','-1','12/01/2005','11/01/2006',224,'2512',25

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	NK          NagaKiran

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script	
	HG	        8/24/2009	Non Ansi join converted to Ansi type.	
	NK          09/18/2009  Modified the query for multiple sites and included the logic if @Max_Conversion_Yr is null or empty then assigining Zero value.
	                                 

******/

CREATE PROCEDURE dbo.GET_DEALTICKET_DETAILS_FOR_MARK_TO_MARKET_INDEX_REPORT_P
	@userId VARCHAR,
	@sessionId VARCHAR,
	@fromDate VARCHAR(12),
	@toDate VARCHAR(12),
	@clientId INT,
	@siteId VARCHAR(1000),
	@unitId INT
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @selectQuery1 VARCHAR(3000)
	DECLARE @fromQuery1 VARCHAR(3000)
	DECLARE @whereQuery1 VARCHAR(8000)

	DECLARE @selectQuery2 VARCHAR(3000)
	DECLARE @fromQuery2 VARCHAR(3000)
	DECLARE @whereQuery2 VARCHAR(8000)

	DECLARE @selectQuery3 VARCHAR(3000)
	DECLARE @fromQuery3 VARCHAR(3000)
	DECLARE @whereQuery3 VARCHAR(8000)

	DECLARE @selectQuery4 VARCHAR(3000)
	DECLARE @fromQuery4 VARCHAR(3000)
	DECLARE @whereQuery4 VARCHAR(8000)

	DECLARE @selectQuery5 VARCHAR(3000)
	DECLARE @fromQuery5 VARCHAR(3000)
	DECLARE @whereQuery5 VARCHAR(8000)

	DECLARE @selectQuery6 VARCHAR(3000)
	DECLARE @fromQuery6 VARCHAR(3000)
	DECLARE @whereQuery6 VARCHAR(8000)

	DECLARE @finalQuery1 VARCHAR(8000)
	DECLARE @finalQuery2 VARCHAR(8000)
	DECLARE @finalQuery3 VARCHAR(8000)
	DECLARE @finalQuery4 VARCHAR(8000)
	DECLARE @finalQuery5 VARCHAR(8000)
	DECLARE @finalQuery6 VARCHAR(8000)
	DECLARE @Max_Conversion_Yr INT
	
	IF @siteId = '' or @siteId = null
	 BEGIN
		SET @siteId = '0'	 
	 END
	ELSE
	 BEGIN
		SET @siteId = @siteId
	 END
	 
	 SELECT @Max_Conversion_Yr = MAX(CONVERSION_YEAR) FROM dbo.RM_CURRENCY_UNIT_CONVERSION WHERE CLIENT_ID= @clientId

	 SET  @Max_Conversion_Yr = ISNULL(@Max_Conversion_Yr , '0')    
	 
	SET @selectQuery1 = ' SELECT dealticket.RM_DEAL_TICKET_ID,
		CAST(dealdetails.HEDGE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		-1 PREMIUM,CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,
		' + '''' + '-1' + '''' + 'OPTION_NAME,' + '''' + 'financial' + '''' + '  HEDGE_TYPE,entity3.ENTITY_NAME  HEDGE_MODE_TYPE,			
		isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),' + '''' + '-1' + '''' + ') MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),' +'''' + '1.000'+ ''''+ ' ) CONVERSION_FACTOR,	
		isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),' +'''' + '1.000'+ ''''+ ' ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,CASE entity1.ENTITY_NAME	
			 WHEN ' +'''' + 'Daily' + '''' + ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' +'''' + 'Monthly' + '''' + 'THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME'
			 
	SET @fromQuery1 = ' FROM RM_DEAL_TICKET  dealticket
				JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID
				JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
				JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID
				LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID = onboard.CLIENT_ID
				JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
				JOIN ENTITY entity1	ON dealticket.FREQUENCY_TYPE_ID = entity1.ENTITY_ID		
				JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID=entity3.ENTITY_ID
				LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID = conversion.CLIENT_ID
					AND conversion.CONVERSION_YEAR = ' + STR(@Max_Conversion_Yr)

 	
	SET @whereQuery1 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=268 AND ENTITY_NAME like ' + '''' + 'fixed price' + '''' + ') AND	
    	       		dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from  ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ' + '''' + 'financial' + '''' + ') AND	
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET	where CONVERT(VARCHAR(12),'+ '''' + @fromDate+ '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
  				UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), ' + '''' +@toDate  + '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' + @fromDate+ '''' + ', 101)  AND CONVERT(VARCHAR(12),' + '''' +@toDate + '''' + ', 101)
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' +@fromDate+ '''' + ', 101)
			AND CONVERT(VARCHAR(12),' + '''' + @toDate + '''' + ', 101)) AND 
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from ENTITY where   ENTITY_TYPE=286 AND
			ENTITY_NAME IN('+ '''' + 'order executed' + '''' + ')) AND dealticket.CLIENT_ID=' +str(@clientId) + ' AND 
			dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge where hedge.SITE_ID in (' + @siteId + ') AND hedge.INCLUDE_IN_REPORTS=1) AND				
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), '+ '''' + @fromDate+''''+ ', 101) AND CONVERT(VARCHAR(12), '+ '''' + @toDate+ '''' + ', 101) AND  consumption.CONVERTED_UNIT_ID=' + str(@unitId) 
			

	SET @selectQuery2 = ' SELECT dealticket.RM_DEAL_TICKET_ID,CAST(dealdetails.HEDGE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		-1 PREMIUM,CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,' + '''' + '-1' + '''' + ' OPTION_NAME,
		' + '''' + 'financial'  + '''' + ' HEDGE_TYPE,entity3.ENTITY_NAME HEDGE_MODE_TYPE,isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),-1 ) MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),' + '''' + '1.000' + '''' + ') CONVERSION_FACTOR,	
		isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),' + '''' + '1.000' + '''' + ' ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,CASE entity1.ENTITY_NAME	
			 WHEN ' +'''' + 'Daily' + '''' + ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' + '''' + 'Monthly' + '''' + ' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME '

	SET @fromQuery2 = ' FROM RM_DEAL_TICKET  dealticket
			JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
			LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID = onboard.CLIENT_ID
			JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
			JOIN ENTITY entity1 ON  dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID			
			JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID = entity3.ENTITY_ID
			LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID = conversion.CLIENT_ID
				AND conversion.CONVERSION_YEAR=  ' + STR(@Max_Conversion_Yr)

	SET @whereQuery2 = ' WHERE dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID from  ENTITY where  ENTITY_TYPE=274 AND ENTITY_NAME IN(' + '''' + 'weighted strip price' + '''' + ',' + '''' + 'individual month pricing' + '''' + ')) AND	
			   dealticket.DEAL_TYPE_ID=( select ENTITY_ID  from ENTITY where  ENTITY_TYPE=268 AND ENTITY_NAME like ' + '''' + 'trigger' + '''' + ')AND 
			dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from  ENTITY   where  ENTITY_TYPE=273 AND  ENTITY_NAME like ' + '''' + 'financial' + '''' + ') AND 
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET
			where CONVERT(VARCHAR(12),' + '''' + @fromDate + '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH	
  				UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), ' + '''' + @toDate + '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' + @fromDate+ '''' + ', 101)  AND CONVERT(VARCHAR(12),' + '''' + @toDate+ '''' + ', 101)
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' + @fromDate+ '''' + ', 101) 
			AND CONVERT(VARCHAR(12),' + '''' + @toDate + '''' + ', 101)) AND
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from ENTITY where   ENTITY_TYPE=286 AND
			ENTITY_NAME IN(' + '''' + 'order executed' + '''' + ')) 
			AND 
			dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID from    ENTITY
			where   ENTITY_TYPE=287 AND ENTITY_NAME IN(' + '''' + 'fixed' + '''' + ',' + '''' + 'closed' + '''' + '))
			AND
			dealticket.CLIENT_ID=' +str(@clientId) + '  AND			
			dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
 				where hedge.SITE_ID in (' + @siteId +') AND hedge.INCLUDE_IN_REPORTS=1) AND	
			
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), ' + '''' +  + @fromDate + '''' + ', 101) 
			AND CONVERT(VARCHAR(12), ' + '''' + @toDate+ '''' +', 101) AND 			
			consumption.CONVERTED_UNIT_ID=' + str(@unitId)

	SET @selectQuery3 = ' SELECT dealticket.RM_DEAL_TICKET_ID,
		CAST(dealoption.STRIKE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		CAST(dealoption.PREMIUM_PRICE as decimal(8,3) ) PREMIUM,	
		CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,
		entity1.ENTITY_NAME OPTION_NAME	,' + '''' + 'financial' + '''' + ' HEDGE_TYPE,entity3.ENTITY_NAME  HEDGE_MODE_TYPE,			
		isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),-1 ) MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),1.000 ) CONVERSION_FACTOR,	
		isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),1.000 ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,	
		CASE entity2.ENTITY_NAME	
			 WHEN ' + '''' + 'Daily' + '''' + ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' + '''' + 'Monthly' + '''' + ' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME'
			 
	SET @fromQuery3 = ' FROM RM_DEAL_TICKET  dealticket
			JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON dealTicket.RM_DEAL_TICKET_ID = dealstatus.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_OPTION_DETAILS dealoption ON dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID
			JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID
			JOIN ENTITY entity1 ON dealoption.OPTION_TYPE_ID=entity1.ENTITY_ID
			LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID = onboard.CLIENT_ID
			JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
			JOIN ENTITY entity2 ON dealticket.FREQUENCY_TYPE_ID=entity2.ENTITY_ID
			JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID=entity3.ENTITY_ID
			LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID = conversion.CLIENT_ID
				AND conversion.CONVERSION_YEAR = ' + STR(@Max_Conversion_Yr)
				

	SET @whereQuery3 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID  from ENTITY
				where  ENTITY_TYPE=268 AND ENTITY_NAME like ' + '''' + 'options' + '''' + ') AND	
			dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ' + '''' + 'financial' + '''' + ') AND
			 dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET
			where CONVERT(VARCHAR(12),' + '''' + @fromDate+ ''''+ ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
  				UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), '+ '''' +@toDate+ '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' +@fromDate+ '''' + ', 101)  AND CONVERT(VARCHAR(12),' + '''' +@toDate+ '''' + ', 101)
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' +@fromDate+ '''' +', 101) 
			AND CONVERT(VARCHAR(12),'+ ''''+@toDate+ '''' + ', 101))  AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from    ENTITY
			where   ENTITY_TYPE=286 AND ENTITY_NAME IN('+ '''' + 'order executed' + '''' + ')) AND
			dealticket.CLIENT_ID=' +str(@clientId)+' AND	
			dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID  from RM_ONBOARD_HEDGE_SETUP hedge
 				where hedge.SITE_ID in(' + @siteId + ') AND hedge.INCLUDE_IN_REPORTS=1) AND 
				
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), ' + '''' + @fromDate + '''' + ', 101) AND CONVERT(VARCHAR(12), '+ '''' + @toDate + '''' +', 101) AND	
			consumption.CONVERTED_UNIT_ID=' +str(@unitId)		

	SET @selectQuery4 = ' SELECT dealticket.RM_DEAL_TICKET_ID,CAST(dealdetails.HEDGE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		-1 PREMIUM,CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,
		-1 OPTION_NAME,' + '''' + 'Physical' + '''' + ' HEDGE_TYPE,entity3.ENTITY_NAME  HEDGE_MODE_TYPE,
		isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),-1 ) MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),1.000 ) CONVERSION_FACTOR,	
		isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),1.000 ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,	
		CASE entity1.ENTITY_NAME	
			 WHEN ' + '''' + 'Daily' + '''' + ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' + '''' + 'Monthly' + '''' +' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME'
			 
	SET @fromQuery4 = ' FROM RM_DEAL_TICKET dealticket
			JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON  dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON  dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID
			LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID = onboard.CLIENT_ID
			JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID
			JOIN ENTITY entity1 ON dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID			
			JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID = entity3.ENTITY_ID
			LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID=conversion.CLIENT_ID
				AND conversion.CONVERSION_YEAR= ' + STR(@Max_Conversion_Yr)

			
	SET @whereQuery4 = ' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID from ENTITY
			 where  ENTITY_TYPE=268 AND ENTITY_NAME like ' + '''' + 'fixed price' + '''' + ') AND	
    				dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from  ENTITY where  ENTITY_TYPE=273 AND ENTITY_NAME like ' + '''' + 'Physical' + '''' + ') AND	
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET	where CONVERT(VARCHAR(12),' + '''' +@fromDate+ '''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
  				UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), '+ ''''+@toDate+'''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' +@fromDate+'''' + ', 101)  AND CONVERT(VARCHAR(12),'+ ''''+@toDate+'''' +', 101)
         		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),'+ ''''+@fromDate+'''' + ', 101) 
			AND CONVERT(VARCHAR(12),'+ '''' +@toDate+'''' + ', 101))  AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from ENTITY where   ENTITY_TYPE=286 AND
			ENTITY_NAME IN(' + '''' + 'order executed' + '''' + ')) AND dealticket.CLIENT_ID=' +str(@clientId)+ ' AND
			dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
 				where hedge.SITE_ID in (' + @siteId +') AND hedge.INCLUDE_IN_REPORTS=1) AND				
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), ' + '''' + @fromDate+ '''' + ', 101) 
			AND CONVERT(VARCHAR(12), ' + '''' +@toDate+ '''' + ', 101)  AND
			 consumption.CONVERTED_UNIT_ID= '+str(@unitId)
			 
	SET @selectQuery5 = ' SELECT dealticket.RM_DEAL_TICKET_ID,CAST(dealdetails.HEDGE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		-1 PREMIUM,CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,-1 OPTION_NAME,
		' + '''' + 'Physical'+'''' + '  HEDGE_TYPE,entity3.ENTITY_NAME  HEDGE_MODE_TYPE,isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),-1 ) MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),1.000 ) CONVERSION_FACTOR,isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),1.000 ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,CASE entity1.ENTITY_NAME	
			 WHEN ' + '''' + 'Daily' + '''' +  ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' + '''' + 'Monthly' + '''' + ' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME'
	
	SET @fromQuery5 = ' FROM RM_DEAL_TICKET dealticket
			JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID
			LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID = onboard.CLIENT_ID				
			JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID
			JOIN ENTITY entity1 ON dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID
			JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID=entity3.ENTITY_ID
			LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID=conversion.CLIENT_ID
				AND conversion.CONVERSION_YEAR = ' + STR(@Max_Conversion_Yr)

		
	SET @whereQuery5 = ' WHERE dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
  		   from  ENTITY where  ENTITY_TYPE=274 AND ENTITY_NAME IN(' + '''' + 'weighted strip price' + '''' + ',' + '''' + 'individual month pricing' + '''' + ')) AND	
		   dealticket.DEAL_TYPE_ID=( select ENTITY_ID  from	 ENTITY	  where  ENTITY_TYPE=268 AND   ENTITY_NAME like ' + '''' + 'trigger' + '''' + ') AND
		dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID  from  ENTITY where  ENTITY_TYPE=273 AND  ENTITY_NAME like ' + '''' + 'Physical' + '''' + ') AND	
		dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET
		where CONVERT(VARCHAR(12),' + '''' + @fromDate+'''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
  		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), ' + ''''+@toDate+'''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
       		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' + '''' +@fromDate+ '''' + ', 101)  AND CONVERT(VARCHAR(12),' + '''' +@toDate+ '''' + ', 101)
			UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),'+ ''''+@fromDate+'''' + ', 101) 
		AND CONVERT(VARCHAR(12),'+'''' +@toDate+'''' + ', 101)) AND  	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from ENTITY where   ENTITY_TYPE=286 AND
			ENTITY_NAME IN(' + '''' + 'order executed' + '''' + ')) 
			and dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID from    ENTITY
			where   ENTITY_TYPE=287 AND ENTITY_NAME IN(' + '''' + 'fixed' + '''' + ',' + '''' + 'closed' + '''' + '))

		AND dealticket.CLIENT_ID='+str(@clientId)+' AND		
		dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID from RM_ONBOARD_HEDGE_SETUP hedge
 		where hedge.SITE_ID in ('+ @siteId +') AND hedge.INCLUDE_IN_REPORTS=1) AND	
		dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), '+ '''' + +@fromDate+ '''' + ', 101) AND CONVERT(VARCHAR(12), '+ '''' + @toDate+ '''' + ', 101)  AND
		 consumption.CONVERTED_UNIT_ID=' + str(@unitId)

	SET @selectQuery6 = ' SELECT dealticket.RM_DEAL_TICKET_ID,CAST(dealoption.STRIKE_PRICE as decimal(8,3) ) HEDGE_PRICE,	
		CAST(dealoption.PREMIUM_PRICE as decimal(8,3) ) PREMIUM,CONVERT (VARCHAR(12), dealdetails.MONTH_IDENTIFIER, 101)  MONTH_IDENTIFIER,
		entity1.ENTITY_NAME OPTION_NAME	,' + '''' + 'Physical' + '''' + ' HEDGE_TYPE,entity3.ENTITY_NAME  HEDGE_MODE_TYPE,			
		isnull(CAST(onboard.MAX_HEDGE_PERCENT as decimal(8,3)),-1 ) MAX_HEDGE_PERCENT	,
		isnull(CAST(conversion.CONVERSION_FACTOR as decimal(8,6)),1.000 ) CONVERSION_FACTOR,	
		isnull(CAST(consumption.CONVERSION_FACTOR as decimal(8,6)),1.000 ) VOLUME_CONVERSION_FACTOR,	
		dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID,CASE entity2.ENTITY_NAME	
			 WHEN ' + '''' + 'Daily' + '''' + ' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,2))
			 WHEN ' + '''' + 'Monthly' + '''' + ' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,2))		
			 ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,2))	END HEDGE_VOLUME'
			 
	SET @fromQuery6 = ' FROM RM_DEAL_TICKET dealticket
			JOIN RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus ON  dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_DETAILS dealdetails ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
			JOIN RM_DEAL_TICKET_OPTION_DETAILS dealoption ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealoption.RM_DEAL_TICKET_DETAILS_ID
			JOIN RM_DEAL_TICKET_VOLUME_DETAILS dealvolume ON dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID
			JOIN ENTITY entity1 ON dealoption.OPTION_TYPE_ID=entity1.ENTITY_ID
			LEFT OUTER JOIN RM_ONBOARD_CLIENT onboard ON dealticket.CLIENT_ID=onboard.CLIENT_ID
			JOIN CONSUMPTION_UNIT_CONVERSION consumption ON dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID
			JOIN ENTITY entity2 ON dealticket.FREQUENCY_TYPE_ID=entity2.ENTITY_ID
			JOIN ENTITY entity3 ON dealticket.HEDGE_MODE_TYPE_ID=entity3.ENTITY_ID
			LEFT OUTER JOIN RM_CURRENCY_UNIT_CONVERSION conversion ON dealticket.CLIENT_ID=conversion.CLIENT_ID
				AND	conversion.CONVERSION_YEAR= ' + STR(@Max_Conversion_Yr)

	SET @whereQuery6 =' WHERE dealticket.DEAL_TYPE_ID=( select ENTITY_ID  from ENTITY where  ENTITY_TYPE=268 AND ENTITY_NAME like ' + '''' +'options' + '''' + ') AND	
		dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID from ENTITY where ENTITY_TYPE=273 AND ENTITY_NAME like ' + '''' + 'Physical' + '''' + ') AND
		dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID  from RM_DEAL_TICKET where CONVERT(VARCHAR(12),' + '''' +@fromDate+'''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
  		UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where CONVERT(VARCHAR(12), ' + '''' +@toDate+'''' + ', 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
			UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_START_MONTH BETWEEN  CONVERT(VARCHAR(12),' +'''' +@fromDate+ '''' + ', 101)  AND CONVERT(VARCHAR(12),' + '''' +@toDate+ '''' + ', 101)
			UNION select RM_DEAL_TICKET_ID from RM_DEAL_TICKET where HEDGE_END_MONTH BETWEEN  CONVERT(VARCHAR(12),'+ '''' + @fromDate+ '''' + ', 101) 
		AND CONVERT(VARCHAR(12),' + '''' + @toDate + '''' + ', 101))  AND
		dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID from ENTITY where ENTITY_TYPE=286 AND ENTITY_NAME IN(' + '''' + 'order executed' + '''' + '))
		AND dealticket.CLIENT_ID=' + str(@clientId)+ ' AND
		 dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID  from RM_ONBOARD_HEDGE_SETUP hedge
 		where hedge.SITE_ID in(' + @siteId + ') AND hedge.INCLUDE_IN_REPORTS=1) AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(VARCHAR(12), '+ '''' +@fromDate + '''' + ', 101) AND CONVERT(VARCHAR(12), ' + '''' + @toDate+ '''' + ', 101) AND
		
		consumption.CONVERTED_UNIT_ID= ' +str(@unitId)

	select @finalQuery1= @selectQuery1+@fromQuery1+@whereQuery1+' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealdetails.HEDGE_PRICE,dealdetails.MONTH_IDENTIFIER,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,entity3.ENTITY_NAME '
			    
	select @finalQuery2 = @selectQuery2+@fromQuery2+@whereQuery2+' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealdetails.HEDGE_PRICE,dealdetails.MONTH_IDENTIFIER,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,entity3.ENTITY_NAME '

	select @finalQuery3 = @selectQuery3+@fromQuery3+@whereQuery3+ ' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealoption.STRIKE_PRICE,dealoption.PREMIUM_PRICE,dealdetails.MONTH_IDENTIFIER,entity1.ENTITY_NAME,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity2.ENTITY_NAME,entity3.ENTITY_NAME '

	select @finalQuery4 = @selectQuery4+@fromQuery4+@whereQuery4 + ' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealdetails.HEDGE_PRICE,dealdetails.MONTH_IDENTIFIER,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,entity3.ENTITY_NAME '

	select @finalQuery5 = @selectQuery5+@fromQuery5+@whereQuery5+ ' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealdetails.HEDGE_PRICE,dealdetails.MONTH_IDENTIFIER,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity1.ENTITY_NAME,entity3.ENTITY_NAME '

	select @finalQuery6 = @selectQuery6+@fromQuery6+@whereQuery6 + ' GROUP BY dealticket.RM_DEAL_TICKET_ID,dealoption.STRIKE_PRICE,dealoption.PREMIUM_PRICE,dealdetails.MONTH_IDENTIFIER,entity1.ENTITY_NAME,onboard.MAX_HEDGE_PERCENT,conversion.CONVERSION_FACTOR,consumption.CONVERSION_FACTOR,dealticket.CURRENCY_TYPE_ID,entity2.ENTITY_NAME,entity3.ENTITY_NAME '

	exec (@finalQuery1 + ' UNION ' +  @finalQuery2 + ' UNION ' + @finalQuery3 + ' UNION ' +@finalQuery4 + ' UNION ' + @finalQuery5+ ' UNION ' + @finalQuery6)


END
GO
GRANT EXECUTE ON  [dbo].[GET_DEALTICKET_DETAILS_FOR_MARK_TO_MARKET_INDEX_REPORT_P] TO [CBMSApplication]
GO
