SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








create PROCEDURE [dbo].[cbmsBudgets_GetForClientCommodityYearAndReportDate]
	( @myaccountid int = null
	, @client_id int = null
	, @commodity_type_id int = null
	, @year int = null
	, @report_date datetime
	)
AS
BEGIN

	   select budget_id
		, client_id
		, budget_title
		, date_created
		, budget_start_year
		, budget_start_month
		, user_info_id
		, commodity_type_id
		, cbms_image_id
	     from budgets
	    where client_id = @client_id
	      and commodity_type_id = @commodity_type_id
	      and budget_start_year = @year
	      and report_date = @report_date

END







GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_GetForClientCommodityYearAndReportDate] TO [CBMSApplication]
GO
