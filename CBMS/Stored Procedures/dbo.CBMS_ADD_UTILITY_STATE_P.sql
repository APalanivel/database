SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_ADD_UTILITY_STATE_P]


DESCRIPTION: Inserts data into the VENDOR_STATE_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_id              int, 
@state_id               int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec [CBMS_ADD_UTILITY_STATE_P]
--@vendor_id = 3051,
--@state_id = 14


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [DBO].[CBMS_ADD_UTILITY_STATE_P] 
@vendor_id int, 
@state_id int

AS


INSERT INTO VENDOR_STATE_MAP (
								VENDOR_ID, 
								STATE_ID, 
								IS_HISTORY
							 ) 
					VALUES   (
								@vendor_id, 
								@state_id, 
								0
							  )
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_UTILITY_STATE_P] TO [CBMSApplication]
GO
