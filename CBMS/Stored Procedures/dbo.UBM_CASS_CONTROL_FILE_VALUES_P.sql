SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_CASS_CONTROL_FILE_VALUES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec UBM_CASS_CONTROL_FILE_VALUES_P 288

CREATE    PROCEDURE dbo.UBM_CASS_CONTROL_FILE_VALUES_P 
@masterLogId int

AS
set nocount on
SELECT CASS_TABLENAME,EXPECTED_DATA FROM 
UBM_CASS_TABLE_COUNT WHERE UBM_BATCH_MASTER_LOG_ID =@masterLogId
order by cass_tablename
GO
GRANT EXECUTE ON  [dbo].[UBM_CASS_CONTROL_FILE_VALUES_P] TO [CBMSApplication]
GO
