SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Account_Forecast_Volume_Merge                      
                        
Description:                        
        To get global site groups  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Group_Name   VARCHAR(100)  
    @Client_Id   INT  
    @RM_Group_Type_Cd INT  
    @User_Info_Id  INT     
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
 @Cbms_Sitegroup_Id INT    
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
 BEGIN TRANSACTION  
  DECLARE @Cbms_Sitegroup_Id int  
  EXEC dbo.RM_Account_Forecast_Volume_Merge 'Global RM Test',235,102559,291,49,@Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id OUT  
  SELECT @Cbms_Sitegroup_Id  
  SELECT * FROM dbo.RM_GROUP WHERE RM_GROUP_ID = @Cbms_Sitegroup_Id  
 ROLLBACK TRANSACTION       
                       
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-30-07     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [dbo].[RM_Account_Forecast_Volume_Merge]
    (
        @Client_Hier_Id INT
        , @Account_Id INT
        , @Commodity_Id INT
        , @Service_Month DATE
        , @Forecast_Volume DECIMAL
        , @Uom_Id INT
        , @Volume_Source_Cd INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Data_Change_Level_Cd INT
            , @Site_Forecast_Volume DECIMAL(28, 0)
            , @Site_Uom_Id INT
            , @Site_Volume_Source_Cd INT;

        SELECT
            @Data_Change_Level_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Data Change Level'
            AND cd.Code_Value = 'Account';



        MERGE INTO Trade.RM_Account_Forecast_Volume AS tgt
        USING
        (   SELECT
                @Client_Hier_Id AS Client_Hier_Id
                , @Account_Id AS Account_Id
                , @Commodity_Id AS Commodity_Id
                , @Service_Month AS Service_Month) AS src
        ON tgt.Client_Hier_Id = src.Client_Hier_Id
           AND  tgt.Account_Id = src.Account_Id
           AND  tgt.Commodity_Id = src.Commodity_Id
           AND  tgt.Service_Month = src.Service_Month
        WHEN MATCHED THEN UPDATE SET
                              tgt.Forecast_Volume = @Forecast_Volume
                              , tgt.Uom_Id = @Uom_Id
                              , tgt.Volume_Source_Cd = @Volume_Source_Cd
                              , tgt.Data_Change_Level_Cd = @Data_Change_Level_Cd
                              , tgt.Updated_User_Id = @User_Info_Id
                              , tgt.Last_Change_Ts = GETDATE()
        WHEN NOT MATCHED BY TARGET THEN INSERT (Client_Hier_Id
                                                , Account_Id
                                                , Commodity_Id
                                                , Service_Month
                                                , Forecast_Volume
                                                , Uom_Id
                                                , Volume_Source_Cd
                                                , Data_Change_Level_Cd
                                                , Created_User_Id
                                                , Created_Ts
                                                , Updated_User_Id
                                                , Last_Change_Ts)
                                        VALUES
                                            (@Client_Hier_Id
                                             , @Account_Id
                                             , @Commodity_Id
                                             , @Service_Month
                                             , @Forecast_Volume
                                             , @Uom_Id
                                             , @Volume_Source_Cd
                                             , @Data_Change_Level_Cd
                                             , @User_Info_Id
                                             , GETDATE()
                                             , @User_Info_Id
                                             , GETDATE());

        SELECT
            @Site_Forecast_Volume = SUM(ISNULL(afv.Forecast_Volume, 0))
            , @Site_Uom_Id = MAX(ISNULL(afv.Uom_Id, 0))
            , @Site_Volume_Source_Cd = MAX(ISNULL(afv.Volume_Source_Cd, 0))
        FROM
            Trade.RM_Account_Forecast_Volume afv
        WHERE
            afv.Client_Hier_Id = @Client_Hier_Id
            AND afv.Commodity_Id = @Commodity_Id
            AND afv.Service_Month = @Service_Month;



        --UPDATE
        --    cfv
        --SET
        --    cfv.Forecast_Volume = @Site_Forecast_Volume
        --    , cfv.Uom_Id = @Uom_Id
        --    , cfv.Volume_Source_Cd = @Volume_Source_Cd
        --    , cfv.Data_Change_Level_Cd = @Data_Change_Level_Cd
        --    , cfv.Updated_User_Id = @User_Info_Id
        --    , cfv.Last_Change_Ts = GETDATE()
        --FROM
        --    Trade.RM_Account_Forecast_Volume afv
        --    INNER JOIN Trade.RM_Client_Hier_Forecast_Volume cfv
        --        ON afv.Client_Hier_Id = cfv.Client_Hier_Id
        --           AND  afv.Commodity_Id = cfv.Commodity_Id
        --           AND  afv.Service_Month = cfv.Service_Month
        --WHERE
        --    afv.Client_Hier_Id = @Client_Hier_Id
        --    AND afv.Commodity_Id = @Commodity_Id
        --    AND afv.Service_Month = @Service_Month;

        MERGE Trade.RM_Client_Hier_Forecast_Volume AS tgt
        USING
        (   SELECT
                @Site_Forecast_Volume AS Forecast_Volume
                , @Uom_Id AS Uom_Id
                , @Volume_Source_Cd AS Volume_Source_Cd
                , @Client_Hier_Id AS Client_Hier_Id
                , @Service_Month AS Service_Month
                , @Commodity_Id AS Commodity_Id) src
        ON tgt.Client_Hier_Id = src.Client_Hier_Id
           AND  tgt.Service_Month = src.Service_Month
           AND  tgt.Commodity_Id = src.Commodity_Id
        WHEN MATCHED THEN UPDATE SET
                              tgt.Forecast_Volume = src.Forecast_Volume
                              , tgt.Uom_Id = src.Uom_Id
                              , tgt.Volume_Source_Cd = src.Volume_Source_Cd
                              , tgt.Updated_User_Id = @User_Info_Id
                              , tgt.Last_Change_Ts = GETDATE()
                              , tgt.Data_Change_Level_Cd = @Data_Change_Level_Cd
        WHEN NOT MATCHED THEN INSERT (Client_Hier_Id
                                      , Commodity_Id
                                      , Service_Month
                                      , Forecast_Volume
                                      , Uom_Id
                                      , Volume_Source_Cd
                                      , Data_Change_Level_Cd
                                      , Created_User_Id
                                      , Created_Ts
                                      , Updated_User_Id
                                      , Last_Change_Ts)
                              VALUES
                                  (src.Client_Hier_Id
                                   , @Commodity_Id
                                   , src.Service_Month
                                   , src.Forecast_Volume
                                   , src.Uom_Id
                                   , src.Volume_Source_Cd
                                   , @Data_Change_Level_Cd
                                   , @User_Info_Id
                                   , GETDATE()
                                   , @User_Info_Id
                                   , GETDATE());


    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Account_Forecast_Volume_Merge] TO [CBMSApplication]
GO
