SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
    dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P 12038 
    
    dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P 12048 
    
    dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P 12056 
    
    dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P 11765 
    
    dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P 13477 
    


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	09/01/2009  Used contract_id to join contract with supplier_account_meter_map instead of account_id
	 
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE          PROCEDURE dbo.SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P
@contractId as int  
  
AS  

BEGIN
SET NOCOUNT ON  
  
  
select cancelRenegotiation.cbms_image_id,  
 cbmsImage.cbms_doc_id ,   
 cbmsImage.content_type,   
 cli.client_name,    
 vwSite.site_id,  
 vwSite.site_name,  
 utilacc.account_id,   
 utilacc.account_number,  
 con.contract_id,  
 con.ed_contract_number,   
 util.vendor_name utility_name,   
 con.contract_start_date as termFromMonth,  
 con.contract_end_date as termToMonth  
from dbo.client cli (nolock)
 join dbo.vwSiteName vwSite(nolock)on cli.client_id = vwSite.client_id  
 join dbo.account utilacc (nolock)  on utilacc.site_id = vwSite.site_id  
 left join dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT cancelRenegotiation(nolock) on (cancelRenegotiation.account_id = utilacc.account_id )    
 and cancelRenegotiation.contract_id = @contractId  
 left join dbo.cbms_image cbmsImage on (cbmsImage.cbms_image_id = cancelRenegotiation.cbms_image_id)    
 left join dbo.SR_RFP_ACCOUNT rfp_account(NOLOCK)on rfp_account.account_id = utilacc.account_id   
 and rfp_account.is_deleted = 0  
 left join dbo.SR_RFP_ACCOUNT_TERM (NOLOCK) on  rfp_account.SR_RFP_ACCOUNT_ID=SR_RFP_ACCOUNT_TERM.SR_ACCOUNT_GROUP_ID and SR_RFP_ACCOUNT_TERM.is_bid_group=0
 join dbo.meter met (nolock) on met.account_id = utilacc.account_id    
 join dbo.supplier_account_meter_map map (nolock) on map.meter_id = met.meter_id   
 join dbo.contract con (nolock) on con.contract_id = map.contract_id
 join dbo.vendor util (nolock)  on utilacc.vendor_id = util.vendor_id  
where con.contract_id = @contractId   
   
group by cancelRenegotiation.cbms_image_id,  
 cbmsImage.cbms_doc_id ,   
 cbmsImage.content_type,   
 cli.client_name,    
 vwSite.site_id,  
 vwSite.site_name,  
 utilacc.account_id,   
 utilacc.account_number,  
 con.ed_contract_number,   
 util.vendor_name,  
 con.contract_id    
 ,con.contract_start_date,con.contract_end_date  
   
       
  
union  
  
select cancelRenegotiation.cbms_image_id,  
 cbmsImage.cbms_doc_id ,   
 cbmsImage.content_type,   
 cli.client_name,    
 vwSite.site_id,  
 vwSite.site_name,  
 utilacc.account_id,   
 utilacc.account_number,  
 con.contract_id,  
 con.ed_contract_number,   
 util.vendor_name utility_name,   
 con.contract_start_date as termFromMonth,  
 con.contract_end_date as termToMonth  
   
from   dbo.client cli (nolock)
 join dbo.vwSiteName vwSite(nolock) on cli.client_id = vwSite.client_id   
 join dbo.account utilacc (nolock) on utilacc.site_id = vwSite.site_id  
 join dbo.meter met (nolock) on met.account_id = utilacc.account_id    
 join dbo.supplier_account_meter_map map (nolock) on map.meter_id = met.meter_id   
  
 left join dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT cancelRenegotiation(nolock) on (cancelRenegotiation.account_id = utilacc.account_id )    
 and cancelRenegotiation.contract_id = @contractId  
 left join dbo.cbms_image cbmsImage on (cbmsImage.cbms_image_id = cancelRenegotiation.cbms_image_id)  
 left join dbo.SR_RFP_ACCOUNT rfp_account(NOLOCK)on rfp_account.account_id = utilacc.account_id   
 and rfp_account.is_deleted = 0  
 left join dbo.SR_RFP_ACCOUNT_TERM (NOLOCK) on  rfp_account.SR_RFP_ACCOUNT_ID=SR_RFP_ACCOUNT_TERM.SR_ACCOUNT_GROUP_ID and SR_RFP_ACCOUNT_TERM.is_bid_group=1   
 join dbo.contract con(nolock) on con.contract_id = map.contract_id  
 join dbo.vendor util (nolock) on utilacc.vendor_id = util.vendor_id  
 join dbo.sr_rfp_bid_group bid_group(nolock) on bid_group.sr_rfp_bid_group_id = rfp_account.sr_rfp_bid_group_id   
   
WHERE con.contract_id = @contractId   
      
GROUP BY cancelRenegotiation.cbms_image_id,  
 cbmsImage.cbms_doc_id ,   
 cbmsImage.content_type,   
 cli.client_name,    
 vwSite.site_id,  
 vwSite.site_name,  
 utilacc.account_id,   
 utilacc.account_number,  
 con.ed_contract_number,   
 util.vendor_name,  
 con.contract_id ,
 con.contract_start_date , 
 con.contract_end_date  
 
 END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_CANCEL_RENOGOTIATION_ACCOUNTS_INFO_P] TO [CBMSApplication]
GO
