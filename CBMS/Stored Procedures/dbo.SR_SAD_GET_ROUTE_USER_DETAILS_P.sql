SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_ROUTE_USER_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_SAD_GET_ROUTE_USER_DETAILS_P
@userId varchar(10),
@sessionId varchar(20)
AS
set nocount on

select 	userInfo.user_info_id ,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from 	user_info userInfo, user_info_group_info_map map, group_info groupInfo

where 	map.group_info_id = groupInfo.group_info_id
	and map.user_info_id = userInfo.user_info_id
	and groupInfo.group_name in ('supply', 'cem')

order by userInfo.FIRST_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ROUTE_USER_DETAILS_P] TO [CBMSApplication]
GO
