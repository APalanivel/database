SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CreateTempTablesForIPJob

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--Mark McCallon
--12-7-07
--startup job to create temp tables in TEMP database

CREATE PROCEDURE dbo.CreateTempTablesForIPJob
 AS
begin

if exists (select * from dbo.sysobjects where id = object_id(N'tempdb.[dbo].[DV2_ACCOUNT_SITE_DELTA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table tempdb.[dbo].[DV2_ACCOUNT_SITE_DELTA]

CREATE TABLE tempdb.[dbo].[DV2_ACCOUNT_SITE_DELTA] (
	[SITE_ID] [int] NOT NULL ,
	[ACCOUNT_ID] [int] NOT NULL ,
	[ACCOUNT_TYPE_ID] [int] NOT NULL ,
	[ACCOUNT_NUMBER] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COMMODITY_TYPE_ID] [int] NOT NULL ,
	[CONTRACT_START_DATE] [smalldatetime] NULL ,
	[CONTRACT_END_DATE] [smalldatetime] NULL ,
	CONSTRAINT [PK_DV2_ACCOUNT_SITE_DELTA] PRIMARY KEY  CLUSTERED 
	(
		[SITE_ID],
		[ACCOUNT_ID],
		[ACCOUNT_NUMBER],
		[COMMODITY_TYPE_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]

if exists (select * from dbo.sysobjects where id = object_id(N'tempdb.[dbo].[DV2_IP_DELTA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table tempdb.[dbo].[DV2_IP_DELTA]

CREATE TABLE tempdb.[dbo].[DV2_IP_DELTA] (
	[DIVISION_ID] [int] NOT NULL ,
	[SITE_ID] [int] NOT NULL ,
	[SITE_NOT_MANAGED] [bit] NOT NULL ,
	[VENDOR_TYPE_ID] [int] NOT NULL ,
	[VENDOR_ID] [int] NOT NULL ,
	[ACCOUNT_ID] [int] NOT NULL ,
	[COMMODITY_TYPE_ID] [int] NOT NULL ,
	[COUNTRY_ID] [int] NOT NULL ,
	[STATE_ID] [int] NOT NULL ,
	[SERVICE_MONTH_ID] [smallint] NOT NULL ,
	[IS_EXPECTED] [bit] NOT NULL ,
	[IS_RECEIVED] [bit] NOT NULL ,
	[INVOICE_COUNT] [smallint] NOT NULL ,
	[CBMS_IMAGE_ID] [int] NULL ,
	CONSTRAINT [PK_DV2_IP_DELTA] PRIMARY KEY  CLUSTERED 
	(
		[SITE_ID],
		[ACCOUNT_ID],
		[COMMODITY_TYPE_ID],
		[SERVICE_MONTH_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]

end
GO
GRANT EXECUTE ON  [dbo].[CreateTempTablesForIPJob] TO [CBMSApplication]
GO
