
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******          

NAME: [DBO].[Cost_Usage_Sel_By_Contract_Id]  
     
DESCRIPTION: 
	To Get Cost Usage Information which are having system_row = 0 for Selected Contract Id with pagination.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT			  		
	@Start_Index	INT			1
	@End_Index		INT			2147483647		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	EXEC Cost_Usage_Sel_By_Contract_Id 26314,1,2
	EXEC Cost_Usage_Sel_By_Contract_Id 53284,1,10
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR		PANDARINATH
     AP		Athmaram Pabbathi
               
MODIFICATIONS:           
	INITIALS	DATE		   MODIFICATION          
------------------------------------------------------------          
	PNR		16-JUN-10	   CREATED   
     AP		08/08/2011   Removed the Select statement that is using Cost_Usage table from the Union
     AP		08/19/2011   Used Client_Hier, Client_Hier_Account tables and removed Supplier_Account_Meter_Map, Meter, Account tables
     AP		09/19/2011   Removed Cte_service_month CTE and added Row_Num and Total_Rows logic to main CTE
*/  

CREATE PROCEDURE dbo.Cost_Usage_Sel_By_Contract_Id
      ( 
       @Contract_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON ;
          
      DECLARE @Contract_Accounts TABLE
            ( 
             Account_Id INT
            ,Client_Hier_Id INT PRIMARY KEY CLUSTERED ( Account_id, Client_Hier_Id )
            ,Site_Name VARCHAR(1000) )
            
      INSERT      INTO @Contract_Accounts
                  ( 
                   Account_Id
                  ,Client_Hier_Id
                  ,Site_Name )
                  SELECT
                        CHA.Account_id
                       ,CH.Client_Hier_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' AS Site_Name
                  FROM
                        Core.Client_Hier CH
                        INNER JOIN Core.Client_Hier_Account CHA
                              ON CHA.Client_Hier_Id = CH.Client_Hier_Id
                  WHERE
                        CHA.Supplier_Contract_ID = @Contract_Id
                  GROUP BY
                        CHA.Account_id
                       ,CH.Client_Hier_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' ;

      WITH  Cte_Service_month_List
              AS ( SELECT
                        ca.Client_Hier_Id
                       ,ca.Site_Name
                       ,acdtl.Service_Month
                       ,row_number() OVER ( ORDER BY acdtl.Service_Month ) AS Row_Num
                       ,count(1) OVER ( ) AS Total_Rows
                   FROM
                        dbo.Cost_Usage_Account_Dtl acdtl
                        JOIN @Contract_Accounts ca
                              ON ca.Account_Id = acdtl.Account_Id
                                 AND ca.Client_Hier_Id = acdtl.Client_Hier_Id
                   GROUP BY
                        ca.Client_Hier_Id
                       ,ca.Site_Name
                       ,acdtl.Service_Month)
            SELECT
                  sm.Site_Name
                 ,sm.Service_Month
                 ,sm.Total_Rows
            FROM
                  Cte_Service_month_List sm
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index

END
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Sel_By_Contract_Id] TO [CBMSApplication]
GO
