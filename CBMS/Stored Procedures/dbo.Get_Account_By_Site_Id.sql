SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: [dbo].[Get_Account_By_Site_Id]

DESCRIPTION: 
	To get the Account details of given site id 

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Site_Id	INT

OUTPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

[Get_Account_By_Site_Id] 26514

AUTHOR INITIALS:
INITIALS		NAME
------------------------------------------------------------

RKV				Ravi Kumar Vegesna

MODIFICATIONS
INITIALS	DATE				MODIFICATION
------------------------------------------------------------
RKV			2019-10-03			Created to get the Account based upon Site Id
*/

CREATE PROCEDURE [dbo].[Get_Account_By_Site_Id]
      ( 
       @Site_Id INT
       )
AS 
BEGIN

      SET NOCOUNT ON;

     

 SELECT
            cha.Account_Id
           ,MAX(cha.Display_Account_Number) AS Account_Number         
      FROM
            core.Client_Hier_Account cha
			JOIN core.Client_Hier ch
			ON ch.Client_Hier_Id = cha.Client_Hier_Id
		INNER JOIN dbo.Invoice_Collection_Account_Config icac
		ON icac.Account_Id = cha.Account_Id
		INNER JOIN dbo.Invoice_Collection_Queue icq
		ON icac.Invoice_Collection_Account_Config_Id=icq.Invoice_Collection_Account_Config_Id
		INNER JOIN dbo.Invoice_Collection_Issue_Log icil
		ON icil.Invoice_Collection_Queue_Id=icq.Invoice_Collection_Queue_Id
		
      WHERE
            ch.Site_id = @Site_Id 
			GROUP BY cha.Account_Id

END;
GO
GRANT EXECUTE ON  [dbo].[Get_Account_By_Site_Id] TO [CBMSApplication]
GO
