SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 	dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------  
	@rfpId          INT     
         
   
OUTPUT PARAMETERS:  
	 Name		DataType	Default		Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
	EXEC dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp 12349
	EXEC dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp 13258
	EXEC dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp 10071
	EXEC dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp 8810
	EXEC dbo.Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp 7452
    
   
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
   
MODIFICATIONS  
  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2016-03-09	Global Sourcing - Phase3 - GCS-522 Created
							Used the function dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS to check whether the RFP posted to supplier or not, the same
							function used in dbo.SR_RFP_GET_VIEW_BIDS_SOP_INFO_P, if value exists in ALL_ACCOUNT_TERMS_ID coulmn then enables
							the view bids link
	RR			2016-04-25	GCS-798 Modified the script to return accounts as per manage SSR page, using dbo.SR_RFP_GET_VIEW_BIDS_SOP_INFO_P in 
							that manage SSR page
  
******/  
CREATE PROCEDURE [dbo].[Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp] @rfpId INT
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
      DECLARE @ENTITY_ID INT  
  
      SELECT
            @ENTITY_ID = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Closed'
            AND ENTITY_DESCRIPTION = 'BID_STATUS';  
            
      WITH  Cte_RFP_Mtrs
              AS ( SELECT
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Site_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + '(' + ch.Site_name + ')' AS Site_Name
                       ,srbg.GROUP_NAME
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,cha.Account_Id
                       ,cha.Display_Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,mtrtyp.Code_Value AS Meter_Type
                       ,case WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END AS Is_Bid_Group
                   FROM
                        dbo.SR_RFP rfp
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON rfp.SR_RFP_ID = rfpacc.SR_RFP_ID
                        LEFT JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON rfpacc.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND rfp.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        LEFT JOIN dbo.Code mtrtyp
                              ON cha.Meter_Type_Cd = mtrtyp.Code_Id
                   WHERE
                        rfpacc.is_deleted = 0
                        AND rfpacc.SR_RFP_ID = @rfpId
                        AND rfpacc.SR_RFP_BID_GROUP_ID IS NULL
                        AND rfpacc.BID_STATUS_TYPE_ID != @ENTITY_ID
                   UNION
                   SELECT
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Site_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + '(' + ch.Site_name + ')' AS Site_Name
                       ,srbg.GROUP_NAME
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,cha.Account_Id
                       ,cha.Display_Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,mtrtyp.Code_Value AS Meter_Type
                       ,case WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END AS Is_Bid_Group
                   FROM
                        dbo.SR_RFP rfp
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON rfp.SR_RFP_ID = rfpacc.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT sraa
                              ON rfpacc.SR_RFP_ACCOUNT_ID = sraa.SR_RFP_ACCOUNT_ID
                        LEFT JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON rfpacc.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND rfp.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        LEFT JOIN dbo.Code mtrtyp
                              ON cha.Meter_Type_Cd = mtrtyp.Code_Id
                   WHERE
                        rfpacc.is_deleted = 0
                        AND rfpacc.SR_RFP_ID = @rfpId
                        AND rfpacc.SR_RFP_BID_GROUP_ID IS NULL
                        AND rfpacc.BID_STATUS_TYPE_ID = @ENTITY_ID
                   UNION
                   SELECT
                        ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Site_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + '(' + ch.Site_name + ')' AS Site_Name
                       ,srbg.GROUP_NAME
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,cha.Account_Id
                       ,cha.Display_Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,mtrtyp.Code_Value AS Meter_Type
                       ,case WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END AS Is_Bid_Group
                   FROM
                        dbo.SR_RFP rfp
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON rfp.SR_RFP_ID = rfpacc.SR_RFP_ID
                        LEFT JOIN dbo.SR_RFP_BID_GROUP srbg
                              ON rfpacc.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND rfp.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        LEFT JOIN dbo.Code mtrtyp
                              ON cha.Meter_Type_Cd = mtrtyp.Code_Id
                   WHERE
                        rfpacc.is_deleted = 0
                        AND rfpacc.SR_RFP_ID = @rfpId
                        AND rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL )
            SELECT
                  Client_Id
                 ,Client_Name
                 ,Site_Id
                 ,Site_Name
                 ,GROUP_NAME
                 ,case WHEN SR_RFP_BID_GROUP_ID IS NOT NULL THEN SR_RFP_BID_GROUP_ID
                       ELSE SR_RFP_ACCOUNT_ID
                  END AS SR_RFP_ACCOUNT_GROUP_ID
                 ,Account_Id
                 ,Display_Account_Number
                 ,Alternate_Account_Number
                 ,left(mtrnum.numbers, len(mtrnum.numbers) - 1) AS Meter_Number
                 ,left(mtrtyp.mtrtyps, len(mtrtyp.mtrtyps) - 1) AS Meter_Type
                 ,left(posted.accterm, len(posted.accterm) - 1) AS Is_Posted
            FROM
                  Cte_RFP_Mtrs accs
                  CROSS APPLY ( SELECT
                                    mtrs.Meter_Number + ','
                                FROM
                                    Cte_RFP_Mtrs mtrs
                                WHERE
                                    mtrs.Account_Id = accs.Account_Id
                                GROUP BY
                                    mtrs.Meter_Number
                  FOR
                                XML PATH('') ) mtrnum ( numbers )
                  CROSS APPLY ( SELECT
                                    mtrs.Meter_Type + ','
                                FROM
                                    Cte_RFP_Mtrs mtrs
                                WHERE
                                    mtrs.Account_Id = accs.Account_Id
                                    AND mtrs.Meter_Type IS NOT NULL
                                GROUP BY
                                    mtrs.Meter_Type
                  FOR
                                XML PATH('') ) mtrtyp ( mtrtyps )
                  CROSS APPLY ( SELECT DISTINCT
                                    convert(VARCHAR(50), AllAccountTermsId) + ', '
                                FROM
                                    dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS(isnull(SR_RFP_BID_GROUP_ID, SR_RFP_ACCOUNT_ID), Is_Bid_Group)
                  FOR
                                XML PATH('') ) posted ( accterm )
            GROUP BY
                  Client_Id
                 ,Client_Name
                 ,Site_Id
                 ,Site_Name
                 ,GROUP_NAME
                 ,case WHEN SR_RFP_BID_GROUP_ID IS NOT NULL THEN SR_RFP_BID_GROUP_ID
                       ELSE SR_RFP_ACCOUNT_ID
                  END
                 ,Account_Id
                 ,Display_Account_Number
                 ,Alternate_Account_Number
                 ,left(mtrnum.numbers, len(mtrnum.numbers) - 1)
                 ,left(mtrtyp.mtrtyps, len(mtrtyp.mtrtyps) - 1)
                 ,left(posted.accterm, len(posted.accterm) - 1)
                 
    
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Account_Meter_Dtls_Sel_By_Rfp] TO [CBMSApplication]
GO
