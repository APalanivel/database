SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- exec BUDGET_GET_UTILITY_CONTRACTS_P '','',85592,'01/01/2008', '12/01/2008'  
-- exec BUDGET_GET_UTILITY_CONTRACTS_P'','',87451,'01/01/2007', '12/01/2007'  
  
CREATE   PROCEDURE dbo.BUDGET_GET_UTILITY_CONTRACTS_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@budget_account_id INT,
	@budget_start_month DATETIME,
	@budget_end_month DATETIME
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @contStartDt DATETIME
		, @contEndDt DATETIME

	SELECT @contStartDt = (DATEADD(MONTH, 1, @budget_end_month) - 1 )
		, @contEndDt = GETDATE()
  
	SELECT bucVw.contract_id
		, bucVw.ed_contract_number
	FROM budget bdg
		INNER JOIN budget_account bdg_acc ON bdg.budget_id = bdg_acc.budget_id
		INNER JOIN BUDGET_UTILITY_CONTRACT_VW bucVw ON bucVw.account_id = bdg_acc.account_id
			AND bucVw.commodity_type_id = bdg.commodity_type_id
	WHERE bdg_acc.budget_account_id = @budget_account_id
		AND bucVw.contract_start_date <= @contStartDt
		--AND bucVw.contract_end_date > @contEndDt
		AND bucVw.contract_end_date >= @budget_start_month
 
END




GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_UTILITY_CONTRACTS_P] TO [CBMSApplication]
GO
