SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ALL_BID_DOCUMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpbidid      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec SR_RFP_GET_ALL_BID_DOCUMENTS_P 8517

CREATE            PROCEDURE dbo.SR_RFP_GET_ALL_BID_DOCUMENTS_P 

@rfpbidid int
AS
set nocount on

if @rfpbidid > 0 or @rfpbidid = -1

	begin 
		select CBMS_DOC_ID AS NAME, CBMS_IMAGE_SIZE AS CBMS_IMAGE_SIZE, 
		CONTENT_TYPE AS CONTENT_TYPE,cbms_image_id AS CBMS_IMAGE_ID FROM CBMS_IMAGE(nolock) where cbms_image_id in 
		(
			select distinct CBMS_IMAGE_ID from SR_RFP_BID_DOCUMENTS(nolock) INNER JOIN
			 SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP(nolock) on SR_RFP_BID_DOCUMENTS.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID =
			 SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,
			 SR_SUPPLIER_CONTACT_INFO suppcont(nolock)
			where 
			--SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_ID = @rfpid and 
			SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_SUPPLIER_CONTACT_INFO_ID = suppcont.SR_SUPPLIER_CONTACT_INFO_ID 
			--AND suppcont.USER_INFO_ID = @userid 
			--AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID= @userid
			AND  SR_RFP_BID_DOCUMENTS.SR_RFP_BID_ID = @rfpbidid
		)
	end

else
	begin
		select CBMS_DOC_ID AS NAME, CBMS_IMAGE_SIZE AS CBMS_IMAGE_SIZE, 
		CONTENT_TYPE AS CONTENT_TYPE,cbms_image_id AS CBMS_IMAGE_ID FROM CBMS_IMAGE(nolock) where cbms_image_id in 
		(
			select distinct CBMS_IMAGE_ID from SR_RFP_BID_DOCUMENTS(nolock) INNER JOIN
			 SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP(nolock) on SR_RFP_BID_DOCUMENTS.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID =
			 SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,
			 SR_SUPPLIER_CONTACT_INFO suppcont(nolock)
			where 
			--SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_ID = @rfpid  and 
			SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_SUPPLIER_CONTACT_INFO_ID = suppcont.SR_SUPPLIER_CONTACT_INFO_ID 
			--AND suppcont.USER_INFO_ID = @userid 
			--AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID= @userid
		)

	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_BID_DOCUMENTS_P] TO [CBMSApplication]
GO
