SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  

NAME:  
	dbo.cbmsSSOSavings_GetYtdTotalByCatForClient  
  
DESCRIPTION:
  
  INPUT PARAMETERS:  
  Name				DataType  Default Description
------------------------------------------------------------  
  @MyAccountId		INT
  @client_id		INT
  @report_year		INT                       
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
  
	EXEC dbo.cbmsSSOSavings_GetYtdTotalByCatForClient 49,10069,2009
   
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
 PNR	  04/07/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 
******/

CREATE PROCEDURE dbo.cbmsSSOSavings_GetYtdTotalByCatForClient
( 
 @MyAccountId INT
,@client_id INT
,@report_year INT )
AS 
BEGIN
  
      SET NOCOUNT ON ;

      DECLARE
            @session_uid UNIQUEIDENTIFIER
           ,@begin_date DATETIME
           ,@end_date DATETIME
           ,@current_month DATETIME  

      SET @current_month = GETDATE()

      SELECT
            @current_month = First_Day_Of_Month_D
      FROM
            meta.Date_Dim dd
      WHERE
            month_num = MONTH(@current_month)
            AND Year_Num = YEAR(@current_month)
            
      SET @session_uid = NEWID()

      EXEC dbo.cbmsRpt_ServiceMonth_LoadForReportYear @MyAccountId, @session_uid, @report_year, @client_id  

      SELECT
            @begin_date = MIN(service_month)
           ,@end_date = MAX(service_month)
      FROM
            dbo.rpt_service_month
      WHERE
            session_uid = @session_uid  

      SELECT
            @end_date = @current_month
      WHERE
            @end_date > @current_month 

      SELECT
            e.entity_name savings_category_type
           ,SUM(sd.estimated_savings_value) AS total_estimated_savings
      FROM
            dbo.sso_savings s
            JOIN dbo.sso_savings_detail sd
                  ON sd.sso_savings_id = s.sso_savings_id
            JOIN dbo.entity e
                  ON s.savings_category_type_id = e.entity_id
      WHERE
            sd.service_month BETWEEN @begin_date
                             AND     @end_date
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.SSO_SAVINGS_OWNER_MAP ssom
                              JOIN Core.Client_Hier ch
                                    ON ch.Client_Hier_Id = ssom.Client_Hier_Id
                         WHERE
                              ch.client_id = @client_id
                              AND s.sso_savings_id = ssom.sso_savings_id )
      GROUP BY
            e.entity_name
		
END

GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetYtdTotalByCatForClient] TO [CBMSApplication]
GO
GO