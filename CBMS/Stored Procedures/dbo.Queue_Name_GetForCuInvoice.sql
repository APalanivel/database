SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	Queue_Name_GetForCuInvoice

DESCRIPTION:
This sp will return the current queue from the exception denorm table.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------        	
	@cu_invoice_id 	int       	
	@Account_ID		INT				NULL 
	@Commodity_Id   INT             NULL	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	Queue_Name_GetForCuInvoice 3722703
	Queue_Name_GetForCuInvoice 3725217,15726
	Queue_Name_GetForCuInvoice 3721798,101590


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV         Ravi Kumar Vegesna
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV			2016-09-19  MAINT 4193:Created as part of AS400 Recalc Detail Page Enhancement
******/

CREATE PROCEDURE [dbo].[Queue_Name_GetForCuInvoice]
      ( 
       @cu_invoice_id INT
      ,@Account_ID INT = NULL 
      ,@Commodity_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            ced.cu_invoice_id
           ,ced.queue_id
           ,vq.queue_name
      FROM
            dbo.CU_EXCEPTION_DENORM ced
            INNER JOIN dbo.vwCbmsQueueName vq
                  ON vq.queue_id = ced.queue_id
      WHERE
            ced.cu_invoice_id = @cu_invoice_id
            AND ( @Account_ID IS NULL
                  OR ced.Account_ID = @Account_ID )
            AND ( @Commodity_Id IS NULL
                  OR ced.Commodity_Id = @Commodity_Id )
      GROUP BY
            ced.cu_invoice_id
           ,ced.queue_id
           ,vq.queue_name
           	
				
END;

;
GO
GRANT EXECUTE ON  [dbo].[Queue_Name_GetForCuInvoice] TO [CBMSApplication]
GO
