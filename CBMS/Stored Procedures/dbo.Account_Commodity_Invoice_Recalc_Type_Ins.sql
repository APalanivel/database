SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Commodity_Invoice_Recalc_Type_Ins          
              
Description:              
        To insert Data into Account_Commodity_Invoice_Recalc_Type table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Id						INT
     @Commodity_Id						INT
     @Start_Dt							DATE
     @End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @Account_Commodity_Invoice_Recalc_Type_Id			INT
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520
    EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Ins 
      @Account_Id =1148520
     ,@Commodity_Id = 290
     ,@Start_Dt = '2015-01-01'
     ,@End_Dt= NULL
     ,@Invoice_Recalc_Type_Cd = 102030
     ,@User_Info_Id = 49
     ,@Determinant_Source_Cd = 102536
     
	
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-08-07		Created For AS400-II.   
    NR				2016-08-24		MAINT-4064 modifide the code to save the entity audit type as "update". 
    NR				2018-02-08		Interval Data Recalcs - IDR-5 - Added @Determinant_Source_Cd as parameter.   
    NR				2019-07-19		Add Contract - Added Is_Config_Complete,Contract_Id.         
******/
CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Ins]
    (
        @Account_Id INT
        , @Commodity_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE = NULL
        , @Invoice_Recalc_Type_Cd INT
        , @User_Info_Id INT
        , @Determinant_Source_Cd INT
        , @Source_Cd INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @New_Exception_Status_Cd INT
            , @Closed_Exception_Status_Cd INT
            , @Exception_Type_Cd INT;

        SELECT
            @New_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';

        SELECT
            @Closed_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'Closed';


        SELECT
            @Exception_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cd.Code_Value = 'Missing Recalc Type';


        INSERT INTO dbo.Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Id
                 , Commodity_ID
                 , Start_Dt
                 , End_Dt
                 , Invoice_Recalc_Type_Cd
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
                 , Determinant_Source_Cd
                 , Source_Cd
             )
        SELECT
            @Account_Id
            , @Commodity_Id
            , @Start_Dt
            , NULLIF(@End_Dt, '9999-12-31')
            , @Invoice_Recalc_Type_Cd
            , GETDATE()
            , @User_Info_Id
            , @User_Info_Id
            , GETDATE()
            , @Determinant_Source_Cd
            , @Source_Cd
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Commodity_Invoice_Recalc_Type acr
                           WHERE
                                acr.Account_Id = @Account_Id
                                AND acr.Commodity_ID = @Commodity_Id
                                AND acr.Start_Dt = @Start_Dt
                                AND ISNULL(acr.End_Dt, '9999-12-31') = ISNULL(@End_Dt, '9999-12-31'));



        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
            @entity_identifier = @Account_Id
            , @user_info_id = @User_Info_Id
            , @audit_type = 2
            , @entity_name = 'ACCOUNT_TABLE'
            , @entity_type = 500;


        UPDATE
            ae
        SET
            ae.Exception_Status_Cd = @Closed_Exception_Status_Cd
            , ae.Closed_By_User_Id = @User_Info_Id
            , ae.Closed_Ts = GETDATE()
            , ae.Last_Change_Ts = GETDATE()
            , ae.Updated_User_Id = @User_Info_Id
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Account_Id = @Account_Id
            AND ae.Commodity_Id = @Commodity_Id
            AND ae.Exception_Type_Cd = @Exception_Type_Cd
            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd;




    END;
    ;

    ;








GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Ins] TO [CBMSApplication]
GO
