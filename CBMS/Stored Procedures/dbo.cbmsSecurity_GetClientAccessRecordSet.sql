SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE        procedure [dbo].[cbmsSecurity_GetClientAccessRecordSet]
	( @user_info_id int
	, @client_id int = null 
	, @division_id int = null 
	, @site_id int = null 
	)
AS
BEGIN

	set nocount on

	  declare @working_user_info_id int
		, @working_client_id int
		, @working_division_id int
		, @working_site_id int
		, @access_level int
		, @my_client_id int
		, @my_division_id int
		, @my_site_id int
		, @division_count int

	   select @working_user_info_id = user_info_id
		, @access_level = access_level
		, @my_client_id = client_id
		, @my_division_id = division_id
		, @my_site_id = site_id
	     from user_info
	    where user_info_id = @user_info_id


	-- THIS SPROC WILL CONFIRM USER IS ONLY SEEING CLIENT/DIVISION/SITE
	--	HE SHOULD SEE


	-- IF USER_INFO_IS IS NULL, ZERO-OUT EVERYTHING SO USER SEES NOTHING
	if @working_user_info_id is null
	begin

		set @working_client_id = 0
		set @working_division_id = 0
		set @working_site_id = 0

	end
	else
	begin

		if @access_level = 0
		begin
			-- INTERNAL USER, CAN SEE ANYTHING
			-- DEFAULT TO PROVIDED CLIENT/DIVISION/SITE

			set @working_client_id 		= @client_id
			set @working_division_id 	= @division_id
			set @working_site_id 		= @site_id

			-- IF WE HAVE A DIVISION BUT NOT A SITE, CHECK IF THIS IS THE ONLY DIVISION
			-- 	IF IT IS THE ONLY DIVISION, REMOVE IT FROM THE CRITERIA
			if @working_division_id is not null and @working_site_id is null
			begin

				   select @division_count = count(*)
				     from division
				    where client_id = @working_client_id

				--if @division_count = 1
				--begin
					--set @working_division_id = null
				--end
			end


			-- IF CLIENT_ID IS NULL, NULL-OUT EVERYTHING ELSE (CLEAN-UP/VERIFY)
			if @working_client_id is null
			begin
				set @working_division_id = null
				set @working_site_id = null
			end
			else
			begin
	
				-- VERIFY THIS DIVISION IN THIS CLIENT		
				if not exists(select division_id 
						from division 
					       where division_id = @working_division_id 
					         and client_id = @working_client_id)
				begin
					set @working_division_id = null
				end
						
				-- IF NOT DIVISION, CLEAR SITE
				if @working_division_id is null
				begin
					set @working_site_id = null
				end
				else
				begin
	
					-- VERIFY THIS SITE IN THIS DIVISION
					if not exists(select site_id 
							from site 
						       where site_id = @working_site_id 
							 and division_id = @working_division_id)
					begin
						set @working_site_id = null
					end
		
				end
		
			end -- END CLEAN-UP/VERFIY

		end
		else
		begin


			-- EXTERNAL USER, RESTRICTED ACCESS
			--	USE DEFAULT OPTIONS
			

			--
			--	CLIENT
			--
			-- IGNORE PROVIDED CLIENT, ALL EXTERNAL USERS HAVE A CLIENT
			set @working_client_id = @my_client_id


			--
			--	DIVISION
			--
			-- IF USER HAS A DIVISION, DEFAULT TO THAT DIVISION
			if @my_division_id is not null
			begin
				set @working_division_id = @my_division_id
			end
			else if @my_division_id is null and @division_id is not null
			begin
	
				-- IF NO ASSIGNED DIVISION AND DIVISION PROVIDED, 
				--	VERIFY DIVISION IN CLIENT

				   select @working_division_id = division_id
				     from division
				    where client_id = @working_client_id
				      and division_id = @division_id
	

				-- THIS IS NOT A DIVISION USER BUT A DIVISION WAS PROVIDED
				--	IF A SITE WAS NOT PROVIDED, REMOVE DIVISION FILTER
				--	IF CLIENT HAS ONLY ONE DIVISION
				if @working_division_id is not null and @site_id is null
				begin
	
					   select @division_count = count(*)
					     from division
					    where client_id = @working_client_id
	
					--if @division_count = 1
					--begin
					--	set @working_division_id = null
					--end
				end


				if @working_division_id is null
				begin
					-- WE WERE GIVEN A DIVISION THAT WAS NOT VALID
					--	KILL THE SITE TOO
					set @site_id = null
				end
			end
			else
			begin
				-- NO ASSIGNED DIVISION AND NONE PROVIDED
				-- 	SET OUTPUT TO NULL
				set @working_division_id = null
			end -- END DIVISION



			--
			--	SITE
			--
			-- IF USER HAS A SITE, DEFAULT TO THAT SITE
			if @my_site_id is not null
			begin
				set @working_site_id = @my_site_id
			end
			else if @my_site_id is null and @site_id is not null
			begin
	
				-- IF NO ASSIGNED SITE AND SITE PROVIDED, 
				--	VERIFY SITE IN DIVISION
				   select @working_site_id = site_id
				     from division d
				     join site s on s.division_id = d.division_id
				    where d.division_id = @working_division_id
				      and s.site_id = @site_id

			end
			else
			begin
				-- NO ASSIGNED SITE AND NONE PROVIDED
				-- 	SET OUTPUT TO NULL
				set @working_site_id = null
			end -- END SITE

		end -- END ACCESS LEVEL

	end -- END HAVE USER_INFO_ID


	select @working_client_id as client_id
		, @working_division_id as division_id 
		, @working_site_id as site_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSecurity_GetClientAccessRecordSet] TO [CBMSApplication]
GO
