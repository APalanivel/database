SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec GET_REPORT_DATE_FOR_CLIENT_P 1, 1, 211, 'Hedge Report'


CREATE       PROCEDURE dbo.GET_REPORT_DATE_FOR_CLIENT_P
@userId varchar,
@sessionId varchar,
@clientId int,
@reportName varchar(50)

as
	set nocount on
declare @temp_table table(LOG_TIME datetime)

insert into @temp_table

select distinct CONVERT(datetime, CONVERT (Varchar(12),batchlog.LOG_TIME, 101)) as LOG_TIME
from	REPORT_LIST list,
	RM_REPORT report,
	RM_REPORT_BATCH_LOG batchlog
where	list.REPORT_NAME like @reportName AND
	list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
	report.RM_REPORT_ID=batchlog.RM_REPORT_ID AND
	batchlog.CLIENT_ID =@clientId 
order by LOG_TIME desc

select CONVERT (Varchar(12),LOG_TIME, 101)  LOG_TIME from @temp_table
GO
GRANT EXECUTE ON  [dbo].[GET_REPORT_DATE_FOR_CLIENT_P] TO [CBMSApplication]
GO
