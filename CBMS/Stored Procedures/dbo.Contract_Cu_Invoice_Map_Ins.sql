SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Contract_Cu_Invoice_Map_Ins                     
                          
 DESCRIPTION:      
		  To Map the   contract and invoice data for associated.                        
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
----------------------------------------------------------------------------------
                          
 USAGE EXAMPLES:                              
----------------------------------------------------------------------------------
               
	 BEGIN TRAN 
	        
	 SELECT * FROM dbo.Contract_Cu_Invoice_Map where  Contract_Id = 1   
	                
	 EXEC dbo.Contract_Cu_Invoice_Map_Ins
	      @Contract_Id =1
        , @Cu_Invoice_Id  = 1
		,@Account_Id = 1
        , @User_Info_Id =49  

	 SELECT * FROM dbo.Contract_Cu_Invoice_Map where  Contract_Id = 1   
	   
	 ROLLBACK   
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
----------------------------------------------------------------------------------
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
----------------------------------------------------------------------------------
 NR                     2013-12-30      Created for - Add contract.                        
                         
******/
CREATE PROCEDURE [dbo].[Contract_Cu_Invoice_Map_Ins]
    (
        @Contract_Id INT
        , @Cu_Invoice_Id INT
        , @Account_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        BEGIN TRY
            BEGIN TRAN;


            INSERT INTO dbo.Contract_Cu_Invoice_Map
                 (
                     Contract_Id
                     , Cu_Invoice_Id
                     , Account_Id
                     , Created_User_Id
                     , Created_Ts
                 )
            SELECT
                @Contract_Id
                , @Cu_Invoice_Id
                , @Account_Id
                , @User_Info_Id
                , GETDATE()
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Contract_Cu_Invoice_Map sac
                               WHERE
                                    sac.Cu_Invoice_Id = @Cu_Invoice_Id
                                    AND sac.Contract_Id = @Contract_Id
                                    AND sac.Account_Id = @Account_Id);






            COMMIT TRAN;

        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError 'Error In Update';

        END CATCH;
    END;



GO
GRANT EXECUTE ON  [dbo].[Contract_Cu_Invoice_Map_Ins] TO [CBMSApplication]
GO
