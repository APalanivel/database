SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_Queuedata

DESCRIPTION:

 This procedure is to fetch private and public Queue

INPUT PARAMETERS:
Name				DataType		Default	Description
----------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
----------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------
EXEC Report_Queuedata 

AUTHOR INITIALS:
	Initials	Name
----------------------------------------------------------
	SSR			Sharad srivastava

MODIFICATIONS:
	Initials	Date		Modification
----------------------------------------------------------
	SSR        	06/22/2010	Created
******/  
CREATE PROC [dbo].[Report_Queuedata]
AS 
    BEGIN
     
        SET NOCOUNT ON 
     
        DECLARE @Public_id INT,
            @Private_Id INT
			
        SELECT  @Public_id = ent.ENTITY_ID
        FROM    ENTITY ent
        WHERE   ent.ENTITY_NAME = 'Public'
                AND ent.ENTITY_DESCRIPTION = 'Queue'
			
        SELECT  @Private_Id = ent.ENTITY_ID
        FROM    ENTITY ent
        WHERE   ent.ENTITY_NAME = 'Private'
                AND ent.ENTITY_DESCRIPTION = 'Queue' ;
                
        WITH    CTE_Queue
                  AS ( SELECT   q.queue_id,
                                q.queue_type_id,
                                qt.entity_name queue_type,
                                ui.first_name + SPACE(1) + ui.last_name queue_name
                       FROM     queue q
                                JOIN user_info ui ON ui.queue_id = q.queue_id
                                JOIN entity qt ON qt.entity_id = q.queue_type_id
                       WHERE    q.queue_type_id = @Private_Id
                       UNION ALL
                       SELECT   q.queue_id,
                                q.queue_type_id,
                                qt.entity_name queue_type,
                                gi.group_name queue_name
                       FROM     queue q
                                JOIN entity qt ON qt.entity_id = q.queue_type_id
                                JOIN group_info gi ON gi.queue_id = q.queue_id
                       WHERE    q.queue_type_id = @Public_id
                     )
            SELECT  cte_f.QUEUE_ID,
                    cte_f.QUEUE_TYPE_ID,
                    cte_f.queue_type,
                    cte_f.queue_name
            FROM    CTE_Queue cte_f
                

    END


GO
GRANT EXECUTE ON  [dbo].[Report_Queuedata] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Queuedata] TO [CBMSApplication]
GO
