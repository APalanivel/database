SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_MASTER_LOGIDS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@masterLogId   	int       	          	
	@expectedData  	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec UBM_GET_MASTER_LOGIDS_P 463,9.0

CREATE      PROCEDURE dbo.UBM_GET_MASTER_LOGIDS_P 
@masterLogId int,
@expectedData decimal(32,16)

AS
set nocount on
declare @tableName varchar(30)
declare @ubmName varchar (30)


select @ubmName=ubm_name from ubm_batch_master_log masterLog, ubm where ubm_batch_master_log_id=463
and masterLog.ubm_id=ubm.ubm_id

if (@ubmName='Cass')
begin
	set @tableName='cass_account'
end
else
begin
	set @tableName='summit_account'
end


-- this is to get the duplicate control file values 


SELECT UBM_BATCH_MASTER_LOG_ID FROM UBM_CASS_TABLE_COUNT WHERE 
UBM_BATCH_MASTER_LOG_ID <>@masterLogId
and CASS_TABLENAME= @tableName AND EXPECTED_DATA = @expectedData
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_MASTER_LOGIDS_P] TO [CBMSApplication]
GO
