SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[RM_DEAL_TICKET_Sel_By_Division_Id]  
     
DESCRIPTION:

	To get the deal ticket details for the given Division_Id
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Sitegroup_Id	INT		
	@Start_Index	INT			1
    @End_Index		INT			2147483647
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.RM_DEAL_TICKET_Sel_By_Division_Id 8,1,10
	EXEC dbo.RM_DEAL_TICKET_Sel_By_Division_Id 259,1,5
	EXEC dbo.RM_DEAL_TICKET_Sel_By_Division_Id 22,1,5
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	RR			Raghu Reddy			
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	RR			08/05/2011	Created for the requirement of MAINT-787 to show the deail ticket no in Delete division page.
*/  

CREATE PROCEDURE dbo.RM_DEAL_TICKET_Sel_By_Division_Id
      ( 
       @Sitegroup_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN
      DECLARE @HedgeLevelType INT
      
      SET NOCOUNT ON ;
   		
      SELECT
            @HedgeLevelType = ENTITY_ID
      FROM
            ENTITY
      WHERE
            Entity_Name = 'Division'
            AND ENTITY_DESCRIPTION = 'Hedge_Level_Type' ;
      
      WITH  Cte_DealTickets
              AS ( SELECT
                        dt.DEAL_TICKET_NUMBER
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY dt.RM_DEAL_TICKET_ID )
                       ,Total_Rows = count(1) OVER ( )
                   FROM
                        dbo.RM_DEAL_TICKET dt
                   WHERE
                        dt.HEDGE_LEVEL_TYPE_ID = @HedgeLevelType
                        AND dt.DIVISION_ID = @Sitegroup_Id)
            SELECT
                  DEAL_TICKET_NUMBER
                 ,Total_Rows
            FROM
                  Cte_DealTickets
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index

END
GO
GRANT EXECUTE ON  [dbo].[RM_DEAL_TICKET_Sel_By_Division_Id] TO [CBMSApplication]
GO
