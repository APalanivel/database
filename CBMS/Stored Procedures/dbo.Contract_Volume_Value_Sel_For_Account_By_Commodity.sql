SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 [dbo].Contract_Volume_value_Sel_For_Account_By_Commodity  
  
DESCRIPTION:  
    
INPUT PARAMETERS:  
 Name       DataType  Default Description  
------------------------------------------------------------  
 @Account_Id     INT  
 @Site_Client_Hier_Id   INT  
 @Commodity_id     INT  
 @Begin_Dt      DATE  
 @End_Dt      DATE  
 @Currency_Unit_Id    INT  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
  EXEC dbo.Contract_Volume_Value_Sel_For_Account_By_Commodity   
        @Account_Id = 577985  
       ,@Site_Client_Hier_Id = 294001  
       ,@Begin_Dt = '2013-01-01'  
       ,@End_Dt = '2013-05-01'  
       ,@Currency_Unit_Id = 3  
       ,@Commodity_id = 291  
       
   
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 SP  Sandeep Pigilam  
   
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------   
 SP   2016-17-10  Variance Test Enhancements Phase II. For Contract Volume variance test created new sproc. 
   
******/  
CREATE PROCEDURE [dbo].[Contract_Volume_Value_Sel_For_Account_By_Commodity]
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT )
      WITH RECOMPILE
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      DECLARE
            @Country_Id INT
           ,@Country_Default_Uom_Type_Id INT               
  
      DECLARE @Contract_Volume_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200)
            ,Frequency INT )  
  
      CREATE TABLE #Contract_Meter_Base_Loads
            ( 
             Bucket_Value NUMERIC(32, 16)
            ,Service_Month DATETIME
            ,UOM_Type_Id INT
            ,Frequency INT )   
        
              
      SELECT
            @Country_Id = ch.Country_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id  
     
     
      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id  
                    
  
      INSERT      INTO #Contract_Meter_Base_Loads
                  ( 
                   Bucket_Value
                  ,Service_Month
                  ,UOM_Type_Id
                  ,Frequency )
                  SELECT
                        SUM(cmv.volume) AS Bucket_Value
                       ,cmv.month_identifier AS Service_Month
                       ,cmv.unit_type_id UOM_Type_Id
                       ,CASE WHEN frq.ENTITY_NAME = '30-day Cycle' THEN 30
                             WHEN frq.ENTITY_NAME = 'Daily' THEN 1
                             WHEN frq.ENTITY_NAME = 'Month' THEN DATEPART(dd, DATEADD(dd, -DATEPART(dd, cmv.month_identifier), DATEADD(mm, 1, cmv.month_identifier)))
                             WHEN frq.ENTITY_NAME = 'Contract Term' THEN DATEDIFF(dd, c.CONTRACT_START_DATE, c.CONTRACT_END_DATE)
                        END AS Frequency
                  FROM
                        dbo.contract_meter_volume cmv
                        INNER JOIN dbo.CONTRACT c
                              ON cmv.CONTRACT_ID = c.CONTRACT_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Meter_Id = cmv.METER_ID
                                 AND c.CONTRACT_ID = cha.Supplier_Contract_ID
                        LEFT JOIN dbo.ENTITY frq
                              ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
                  WHERE
                        ( c.CONTRACT_START_DATE BETWEEN @Begin_Dt
                                                AND     @End_Dt
                          OR c.CONTRACT_END_DATE BETWEEN @Begin_Dt AND @End_Dt
                          OR @Begin_Dt BETWEEN c.CONTRACT_START_DATE
                                       AND     c.CONTRACT_END_DATE
                          OR @End_Dt BETWEEN c.CONTRACT_START_DATE
                                     AND     c.CONTRACT_END_DATE )
                        AND cha.Account_Id = @Account_Id
                        AND cmv.month_identifier BETWEEN @Begin_Dt
                                                 AND     @End_Dt
                  GROUP BY
                        cmv.month_identifier
                       ,cmv.unit_type_id
                       ,frq.ENTITY_NAME
                       ,c.CONTRACT_START_DATE
                       ,c.CONTRACT_END_DATE
                       
                       
                                
      INSERT      INTO @Contract_Volume_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,UOM_Id
                  ,UOM_Name
                  ,Frequency )
                  SELECT
                        cua.Service_Month
                       ,@Account_Id Account_ID
                       ,'Determinant' Bucket_Type
                       ,'Total Usage' AS Bucket_Name
                       ,( cua.Bucket_Value * uc.Conversion_factor ) AS Bucket_Value
                       ,NULL AS Total_Cost
                       ,( cua.Bucket_Value * uc.Conversion_factor ) AS Volume
                       ,@Country_Default_Uom_Type_Id AS UOM_Id
                       ,en.ENTITY_NAME AS UOM_Name
                       ,cua.Frequency
                  FROM
                        #Contract_Meter_Base_Loads cua
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_ID = cua.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = @Country_Default_Uom_Type_Id
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = @Country_Default_Uom_Type_Id
                  WHERE
                        cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt         
                                       
  
      SELECT
            cu.Account_Id
           ,cu.Bucket_Name
           ,cu.Bucket_Type
           ,cu.Bucket_value
           ,cu.UOM_Id
           ,cu.UOM_Name
           ,Service_Month AS Service_Month
           ,Frequency
      FROM
            @Contract_Volume_Dtl cu  
  
      DROP TABLE #Contract_Meter_Base_Loads  
END;  





;
GO
GRANT EXECUTE ON  [dbo].[Contract_Volume_Value_Sel_For_Account_By_Commodity] TO [CBMSApplication]
GO
