SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_VENDOR_COMMODITY_LIST_P
	@utilityID INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT VENDOR_ID,
		COMMODITY_TYPE_ID
	FROM dbo.VENDOR_COMMODITY_MAP
	WHERE VENDOR_ID=@utilityID

END
GO
GRANT EXECUTE ON  [dbo].[GET_VENDOR_COMMODITY_LIST_P] TO [CBMSApplication]
GO
