SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Update_Account_Group_By_Account_ID

DESCRIPTION:
			To update Account_Group with new vendor

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Meter_Id      	INT 
	@Rate_ID      	INT          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT TOP 10 acc.ACCOUNT_ID,acc.VENDOR_ID
		FROM dbo.ACCOUNT_GROUP ag JOIN dbo.ACCOUNT acc ON ag.ACCOUNT_GROUP_ID = acc.ACCOUNT_GROUP_ID 
	
	BEGIN TRANSACTION
		SELECT ag.ACCOUNT_GROUP_ID,ag.VENDOR_ID,acc.ACCOUNT_ID 
			FROM dbo.ACCOUNT_GROUP ag JOIN dbo.ACCOUNT acc ON ag.ACCOUNT_GROUP_ID = acc.ACCOUNT_GROUP_ID WHERE acc.ACCOUNT_ID = 455183
		--EXEC dbo.Update_Account_Group_By_Account_ID 413450,796,385
		--EXEC dbo.Update_Account_Group_By_Account_ID 149348,796,385
		EXEC dbo.Update_Account_Group_By_Account_ID 455183,7109,796
		SELECT ag.ACCOUNT_GROUP_ID,ag.VENDOR_ID,acc.ACCOUNT_ID 
			FROM dbo.ACCOUNT_GROUP ag JOIN dbo.ACCOUNT acc ON ag.ACCOUNT_GROUP_ID = acc.ACCOUNT_GROUP_ID WHERE acc.ACCOUNT_ID = 455183
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2013-10-21	MAINT-2127 Created

******/
CREATE PROCEDURE dbo.Update_Account_Group_By_Account_ID
      ( 
       @Account_ID INT
      ,@Old_Vendor_Id INT
      ,@New_Vendor_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            ag
      SET   
            ag.VENDOR_ID = @New_Vendor_Id
      FROM
            dbo.ACCOUNT_GROUP ag
            JOIN dbo.ACCOUNT acc
                  ON ag.ACCOUNT_GROUP_ID = acc.ACCOUNT_GROUP_ID
      WHERE
            acc.ACCOUNT_ID = @Account_ID
            AND acc.VENDOR_ID = @Old_Vendor_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Update_Account_Group_By_Account_ID] TO [CBMSApplication]
GO
