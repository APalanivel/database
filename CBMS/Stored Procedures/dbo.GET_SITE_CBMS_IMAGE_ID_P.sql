SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_SITE_CBMS_IMAGE_ID_P
	@siteID int
	AS
	begin
		set nocount on

		select cbms_Image_Id 
		from   site 
		where  site_id = @siteID
	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_CBMS_IMAGE_ID_P] TO [CBMSApplication]
GO
