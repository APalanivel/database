SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SHORTLIST_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpAccountGroupId	int       	          	
	@isBidGroup    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_SHORTLIST_ID_P 
@rfpAccountGroupId int,
@isBidGroup int


as
set nocount on
DECLARE @sopShortListId int
SELECT @sopShortListId = (select SR_RFP_SOP_SHORTLIST_ID 
			  from 	SR_RFP_SOP_SHORTLIST
			  Where	SR_ACCOUNT_GROUP_ID = @rfpAccountGroupId
			  AND IS_BID_GROUP    =	 @isBidGroup)



RETURN  @sopShortListId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SHORTLIST_ID_P] TO [CBMSApplication]
GO
