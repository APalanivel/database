
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**********  
NAME: [dbo].[Report_DE_Credit_Balance_PriorHistory]
       
DESCRIPTION:   
   
        
INPUT PARAMETERS:            
NAME							DATATYPE			DEFAULT				DESCRIPTION            
----------------------------------------------------------------------------------------            
@Client_Id						INT
@Batch_Processed_Start_Date		DATE  
                
OUTPUT PARAMETERS:            
NAME							DATATYPE			DEFAULT				DESCRIPTION            
----------------------------------------------------------------------------------------  
          
USAGE EXAMPLES:            
----------------------------------------------------------------------------------------  
    
EXEC Report_DE_Credit_Balance_PriorHistory 108,'2015-02-01'  
  
 EXEC Report_DE_Credit_Balance_PriorHistory  11231,'2016-04-21'  
 
       
AUTHOR INITIALS:          
INITIALS		NAME            
----------------------------------------------------------------------------------------  
NR				Narayana Reddy
  
MODIFICATIONS  
  
INITIALS		DATE				MODIFICATION  
----------------------------------------------------------------------------------------  
NR				2016-02-03			Created For REPTMGR-46. 
lec				2016-07-06			Revised for site reference number and state. User requested commodity, but we are not at charge/determinant level and 
									commodity would've produced extraneous records.

**********/  

CREATE PROCEDURE [dbo].[Report_DE_Credit_Balance_PriorHistory]
      ( 
       @Client_Id INT
      ,@Batch_Processed_Start_Date DATE )
AS 
BEGIN  
  
      SET NOCOUNT ON
      
      DECLARE
            @ClientId INT = @Client_Id
           ,@Batch_Processed_Start_Dt DATE = @Batch_Processed_Start_Date

      CREATE TABLE #Accounts ( ACCOUNT_ID INT )
      CREATE INDEX #IDX_Accounts_Account_Id ON #Accounts(ACCOUNT_ID)

      INSERT      INTO #Accounts
                  ( 
                   ACCOUNT_ID )
                  SELECT DISTINCT
                        cism.ACCOUNT_ID
                  FROM
                        dbo.ubm_invoice ui
                        INNER JOIN dbo.cu_INVOICE ci
                              ON ci.UBM_INVOICE_id = ui.UBM_INVOICE_ID
                                 AND ui.CBMS_IMAGE_ID = ui.CBMS_IMAGE_ID
                        INNER JOIN dbo.UBM_BATCH_MASTER_LOG ubml
                              ON ui.UBM_BATCH_MASTER_LOG_ID = ubml.UBM_BATCH_MASTER_LOG_ID
                        LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                  WHERE
                        ci.CLIENT_ID = @ClientId
                        AND ui.PREVIOUS_BALANCE < 0
                        AND ubml.START_DATE >= @Batch_Processed_Start_Dt
                        AND ui.AMOUNT_DUE <= 0
                       
      SELECT
            c.CLIENT_NAME
           ,ci.CU_INVOICE_ID
           ,ui.UBM_CLIENT_NAME
           ,ui.UBM_CLIENT_ID
           ,ca.ACCOUNT_NUMBER
           ,ca.account_VENDOR_NAME
           ,ubml.START_DATE
           ,cism.SERVICE_MONTH
           ,ui.PREVIOUS_BALANCE
           ,ui.AMOUNT_DUE
           ,ui.VENDOR_BILLING_DATE
           ,ch.state_name 
           ,ch.Site_Reference_Number
           --,com.Commodity_Name
      FROM
            dbo.ubm_invoice ui
            INNER JOIN dbo.cu_INVOICE ci
                  ON ci.UBM_INVOICE_id = ui.UBM_INVOICE_ID
                     AND ui.CBMS_IMAGE_ID = ui.CBMS_IMAGE_ID
            INNER JOIN dbo.CLIENT c
                  ON ci.CLIENT_ID = c.CLIENT_ID
            INNER JOIN dbo.UBM_BATCH_MASTER_LOG ubml
                  ON ui.UBM_BATCH_MASTER_LOG_ID = ubml.UBM_BATCH_MASTER_LOG_ID
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
            LEFT JOIN Core.Client_Hier_Account ca
                  ON ca.account_id = cism.account_id
            LEFT JOIN core.Client_Hier ch ON ch.Client_Hier_Id = ca.Client_Hier_Id
            --LEFT JOIN dbo.Commodity com ON com.Commodity_Id = ca.Commodity_Id
            INNER JOIN #Accounts cc
                  ON cc.ACCOUNT_ID = cism.Account_ID
      WHERE
            cism.SERVICE_MONTH BETWEEN DATEADD(month, -24, @Batch_Processed_Start_Dt)
                               AND     @Batch_Processed_Start_Dt
      GROUP BY
            c.CLIENT_NAME
           ,ci.CU_INVOICE_ID
           ,ui.UBM_CLIENT_NAME
           ,ui.UBM_CLIENT_ID
           ,ca.ACCOUNT_NUMBER
           ,ca.account_VENDOR_NAME
           ,ubml.START_DATE
           ,cism.SERVICE_MONTH
           ,ui.PREVIOUS_BALANCE
           ,ui.AMOUNT_DUE
           ,ci.CURRENT_CHARGES
           ,ui.PAYMENT_DUE_DATE
           ,ui.VENDOR_BILLING_DATE
           ,ch.state_name 
           ,ch.Site_Reference_Number
           --,com.Commodity_Name
      ORDER BY
            ca.ACCOUNT_NUMBER
           ,ca.account_VENDOR_NAME
           ,ui.VENDOR_BILLING_DATE DESC  
             
      DROP TABLE #Accounts

END

;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Credit_Balance_PriorHistory] TO [CBMS_SSRS_Reports]
GO
