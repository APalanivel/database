
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

    dbo.cbmsInvoiceParticipation_SearchForClient

DESCRIPTION:       

INPUT PARAMETERS:
 Name                DataType  Default Description
------------------------------------------------------------
@report_year            INT     
@commodity_type_id	    INT     
@region_id              INT     
@country_id             INT     
@state_id               INT     
@client_id              INT     
@division_id            INT     
@site_id                INT     
@account_id             INT          
@vendor_type_id         INT     
@vendor_id              INT     
@utility_id             INT     
@analyst_id             INT     
@not_managed            BIT                      

OUTPUT PARAMETERS:      
 Name				DataType  Default Description      
------------------------------------------------------------      

USAGE EXAMPLES:      
------------------------------------------------------------      

	SET STATISTICS IO ON

	EXEC cbmsInvoiceParticipation_SearchForClient 2010, null, null, null,null, 10069, 570, NULL, NULL,null,null,null,null

	EXEC cbmsInvoiceParticipation_SearchForClient
			@Report_Year = 2009
			,@Commodity_Type_Id = 290
			,@Client_id =  218


	EXEC cbmsInvoiceParticipation_SearchForClient
			@Report_Year = 2009
			,@Commodity_Type_Id = 290
			,@Client_id =  218
			,@Site_Id = 12681

	EXEC cbmsInvoiceParticipation_SearchForClient
			@Report_Year = 2009
			,@Commodity_Type_Id = 290
			,@Client_id =  218
			,@Account_id = 18819

	SELECT * FROM core.Client_Hier_Account WHERE Account_id = 18819

	SELECT * FROM Invoice_participation WHERE Account_id = 18819


AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------      
  DMR		Deana Ritter  
    
 MODIFICATIONS       
 Initials	Date		Modification      
------------------------------------------------------------      
			9/23/2009	For adding Supplier Account start and end dates ,   
			9/24/2009	Added Schema owners, reviewed SP.  
 HG			10/29/2009  Division view replaced by Division_Dtl table.  
 SSR		03/04/2010	Removed Division View AND Site View
 SKA	    03/18/2010  Used Meta.Date_Dim rather than creating dates using cbmsRpt_Service_Month
			     		Removed unused input parameter @MyAccountId
			     		vwAccountSite replaced by base tables.
					    Pivot will not be help here as all columns are based on different case conditions 
 SKA		04/27/2010  Removed CU_invoice(Account_id) with Cu_invoice_service_month(account_id)								
 SKA		05/27/2010	Refer METER_DISASSOCIATION_DATE condition instead of Is_History
 SSR		07/08/2010  Removed the logic of fetching data from SAMM table
							(map.meter_disassociation_date IS NULL)
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
 HG			01/31/2011	Script modified to fix #22195
						for Mo11 instead of returning a count of invoices the SP is summing the CBMS_Image_ID records corrected it to return the count of images.
						Replaced Client,Rate,Vendor,account tables are removed with client_hier_account and client_hier tables as well removed union part the same taken into main query.

 NR			2015-07-02	MAINT-3573 Added Client_Id, sitegroup_id,Supplier_Account_End_Dt and ClientHier_Id in the result set.
 TO DO
 Initials	Date			Modification
------------------------------------------------------------
 DMR		10/9/2009		This needs to be reviewed and potentially modified as pivot table.

******/  
CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_SearchForClient]
      ( 
       @report_year AS INT
      ,@commodity_type_id AS INT = NULL
      ,@region_id AS INT = NULL
      ,@country_id AS INT = NULL
      ,@state_id AS INT = NULL
      ,@client_id AS INT = NULL
      ,@division_id AS INT = NULL
      ,@site_id AS INT = NULL
      ,@account_id AS INT = NULL
      ,@vendor_type_id AS INT = NULL
      ,@vendor_id AS INT = NULL
      ,@utility_id AS INT = NULL
      ,@analyst_id AS INT = NULL
      ,@not_managed AS BIT = NULL )
AS 
BEGIN
  
      SET  NOCOUNT ON  

      DECLARE
            @begin_date DATETIME
           ,@end_date DATETIME
           ,@Utility_Account_Type_Id INT
           ,@Supplier_Account_Type_Id INT

      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Num SMALLINT )

      SELECT
            @Utility_Account_Type_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Utility'  

      SELECT
            @Supplier_Account_Type_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Supplier'

      SELECT
            @begin_date = ( CASE WHEN fy.start_month = 1 THEN dd.Date_D
                                 ELSE DATEADD(M, -12, dd.Date_D)
                            END )
           ,@end_date = ( CASE WHEN fy.start_month = 1 THEN DATEADD(M, 12, dd.Date_D)
                               ELSE dd.Date_D
                          END )
      FROM
            meta.Date_Dim dd
            JOIN dbo.vwClientFiscalYearStartMonth fy
                  ON dd.Month_Num = fy.start_month
      WHERE
            dd.Year_Num = @report_year
            AND fy.client_id = @Client_Id;

      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Num )
                  SELECT
                        Date_D AS service_month
                       ,Month_Num = ROW_NUMBER() OVER ( ORDER BY Date_D )
                  FROM
                        meta.Date_Dim
                  WHERE
                        Date_D BETWEEN @begin_date
                               AND     DATEADD(M, -1, @end_date);

      WITH  Cte_Client_Hier_Data
              AS ( SELECT
                        ch.Client_Name
                       ,ch.Site_id
                       ,RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,CASE WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
                             ELSE @Supplier_Account_Type_Id
                        END AS Account_Type_Id
                       ,cha.Account_type
                       ,cha.Commodity_Id AS commodity_type_id
                       ,com.Commodity_Name AS commodity_type
                       ,cha.Account_Invoice_Source_Cd AS invoice_source_type_id
                       ,InvSource.entity_name AS invoice_source
                       ,cha.Account_Vendor_Id AS Vendor_Id
                       ,cha.Account_Vendor_Name AS Vendor_Name
                       ,cha.Account_id
                       ,Account_Number = CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Number
                                              ELSE ISNULL(cha.Account_Number, 'Blank for ' + cha.Account_Vendor_Name) + ' (' + CASE WHEN MIN(ISNULL(cha.Supplier_Meter_Association_Date, cha.Supplier_Account_Begin_Dt)) < cha.Supplier_Account_Begin_Dt THEN CONVERT(VARCHAR(12), cha.Supplier_Account_Begin_Dt)
                                                                                                                                    ELSE CONVERT(VARCHAR, MIN(ISNULL(cha.Supplier_Meter_Association_Date, cha.Supplier_Account_Begin_Dt)), 101)
                                                                                                                               END + '-' + CASE WHEN MAX(ISNULL(cha.Supplier_Meter_Disassociation_Date, cha.Supplier_Account_End_Dt)) > cha.Supplier_Account_End_Dt THEN CONVERT(VARCHAR(12), cha.Supplier_Account_End_Dt)
                                                                                                                                                ELSE CONVERT(VARCHAR, MAX(ISNULL(cha.Supplier_Meter_Disassociation_Date, cha.Supplier_Account_End_Dt)), 101)
                                                                                                                                           END + ')'
                                         END
                       ,cha.Account_Not_Managed AS Not_managed
                       ,CASE WHEN cha.Account_Not_managed = 1 THEN 'Inactive'
                             ELSE 'Active'
                        END AS Active
                       ,CASE WHEN MIN(ISNULL(cha.Supplier_Meter_Association_Date, cha.Supplier_Account_Begin_Dt)) < cha.Supplier_Account_Begin_Dt THEN cha.Supplier_Account_Begin_Dt
                             ELSE MIN(ISNULL(cha.Supplier_Meter_Association_Date, cha.Supplier_Account_Begin_Dt))
                        END AS Supplier_Account_Begin_Dt
                       ,CASE WHEN MAX(ISNULL(cha.Supplier_Meter_Disassociation_Date, cha.Supplier_Account_End_Dt)) > cha.Supplier_Account_End_Dt THEN cha.Supplier_Account_End_Dt
                             ELSE MAX(ISNULL(cha.Supplier_Meter_Disassociation_Date, cha.Supplier_Account_End_Dt))
                        END AS Supplier_Account_End_Dt
                       ,ch.Client_Id
                       ,ch.Client_Hier_Id
                       ,ch.Sitegroup_Id
                   FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON com.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN dbo.entity AS InvSource
                              ON InvSource.entity_id = cha.Account_Invoice_Source_Cd
                   WHERE
                        ch.Client_id = @client_id
                        AND ( @division_id IS NULL
                              OR ch.Sitegroup_id = @division_id )
                        AND ( @site_id IS NULL
                              OR ch.Site_id = @site_id )
                        AND ( @account_id IS NULL
                              OR cha.Account_id = @account_id )
                        AND ( @not_managed IS NULL
                              OR cha.Account_Not_Managed = @not_managed )
                        AND ( @commodity_type_id IS NULL
                              OR cha.commodity_id = @commodity_type_id )
                        AND ( @vendor_id IS NULL
                              OR cha.Account_vendor_id = @vendor_id )
                        AND ( @vendor_type_id IS NULL
                              OR cha.Account_Vendor_Type_id = @vendor_type_id )
                        AND ( @state_id IS NULL
                              OR ch.State_id = @state_id )
                        AND ( @country_id IS NULL
                              OR ch.country_id = @country_id )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Invoice_Participation ip1
                                     WHERE
                                          ip1.Site_Id = ch.SITE_ID
                                          AND ip1.Account_Id = cha.Account_Id
                                          AND ( ip1.IS_EXPECTED = 1
                                                OR ip1.IS_RECEIVED = 1 ) )
                   GROUP BY
                        ch.client_name
                       ,ch.site_id
                       ,ch.city
                       ,ch.state_name
                       ,ch.site_name
                       ,cha.Account_Type
                       ,cha.Account_Invoice_Source_Cd
                       ,InvSource.entity_name
                       ,cha.commodity_id
                       ,com.Commodity_Name
                       ,cha.Account_Vendor_id
                       ,cha.Account_Vendor_name
                       ,cha.Account_id
                       ,CHA.Account_number
                       ,cha.Account_Not_managed
                       ,cha.Supplier_Account_Begin_Dt
                       ,cha.Supplier_Account_End_Dt
                       ,ch.Client_Id
                       ,ch.Client_Hier_Id
                       ,ch.Sitegroup_Id)
            SELECT
                  cha.Client_Name
                 ,cha.Site_id
                 ,cha.Site_Name
                 ,cha.Account_Type_Id
                 ,cha.Account_type
                 ,cha.Commodity_type_id
                 ,cha.commodity_type
                 ,cha.Invoice_Source_Type_id
                 ,cha.Invoice_Source
                 ,cha.Vendor_Id
                 ,cha.Vendor_Name
                 ,cha.Account_id
                 ,cha.Account_Number
                 ,cha.Not_managed
                 ,cha.Active
                 ,cha.Supplier_Account_Begin_Dt
                 ,month1_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 1 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month1_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 1 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month1_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 1 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month1_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 1 THEN 1
                                                ELSE 0
                                           END)
                 ,month2_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 2 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month2_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 2 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month2_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 2 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month2_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 2 THEN 1
                                                ELSE 0
                                           END)
                 ,month3_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 3 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month3_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 3 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month3_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 3 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month3_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 3 THEN 1
                                                ELSE 0
                                           END)
                 ,month4_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 4 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month4_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 4 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month4_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 4 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month4_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 4 THEN 1
                                                ELSE 0
                                           END)
                 ,month5_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 5 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month5_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 5 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month5_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 5 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month5_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 5 THEN 1
                                                ELSE 0
                                           END)
                 ,month6_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 6 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month6_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 6 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month6_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 6 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month6_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 6 THEN 1
                                                ELSE 0
                                           END)
                 ,month7_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 7 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month7_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 7 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month7_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 7 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month7_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 7 THEN 1
                                                ELSE 0
                                           END)
                 ,month8_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 8 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month8_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 8 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month8_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 8 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month8_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 8 THEN 1
                                                ELSE 0
                                           END)
                 ,month9_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 9 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month9_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                  AND tmptbl.Month_Num = 9 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                     ELSE 0
                                                                                END
                                             ELSE 0
                                        END)
                 ,month9_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                               AND tmptbl.Month_Num = 9 THEN cu_i.cbms_image_id
                                          ELSE NULL
                                     END)
                 ,month9_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                     AND tmptbl.Month_Num = 9 THEN 1
                                                ELSE 0
                                           END)
                 ,month10_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 10 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month10_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 10 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month10_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                AND tmptbl.Month_Num = 10 THEN cu_i.cbms_image_id
                                           ELSE NULL
                                      END)
                 ,month10_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                      AND tmptbl.Month_Num = 10 THEN 1
                                                 ELSE 0
                                            END)
                 ,month11_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 11 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month11_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 11 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month11_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                AND tmptbl.Month_Num = 11 THEN cu_i.cbms_image_id
                                           ELSE NULL
                                      END)
                 ,month11_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                      AND tmptbl.Month_Num = 11 THEN 1
                                                 ELSE 0
                                            END)
                 ,month12_expected = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 12 THEN CASE WHEN ip.is_expected = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month12_received = MAX(CASE WHEN ip.service_month = tmptbl.service_month
                                                   AND tmptbl.Month_Num = 12 THEN CASE WHEN ip.is_received = 1 THEN 1
                                                                                       ELSE 0
                                                                                  END
                                              ELSE 0
                                         END)
                 ,month12_image = MAX(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                AND tmptbl.Month_Num = 12 THEN cu_i.cbms_image_id
                                           ELSE NULL
                                      END)
                 ,month12_image_count = SUM(CASE WHEN cu_im.service_month = tmptbl.service_month
                                                      AND tmptbl.Month_Num = 12 THEN 1
                                                 ELSE 0
                                            END)
                 ,cha.Client_Id
                 ,cha.Client_Hier_Id
                 ,cha.Sitegroup_Id
                 ,cha.Supplier_Account_End_Dt
            FROM
                  Cte_Client_Hier_Data cha
                  CROSS APPLY @Service_Month tmptbl
                  LEFT OUTER JOIN dbo.invoice_participation ip
                        ON ip.account_id = cha.account_id
                           AND ip.site_id = cha.site_id
                           AND ip.SERVICE_MONTH = tmptbl.service_month
                  LEFT OUTER JOIN ( dbo.cu_invoice_service_month cu_im
                                    INNER JOIN dbo.cu_invoice cu_i
                                          ON cu_im.cu_invoice_id = cu_i.cu_invoice_id )
                                    ON cu_im.ACCOUNT_id = ip.ACCOUNT_id
                                       AND cu_im.SERVICE_MONTH = tmptbl.service_month
            WHERE
                  ( cha.Account_Type = 'Utility'
                    OR cha.Supplier_Account_Begin_Dt < @End_Date )
                  AND ( cha.Account_Type = 'Utility'
                        OR cha.Supplier_Account_End_Dt >= @Begin_Date )
            GROUP BY
                  cha.client_name
                 ,cha.site_id
                 ,cha.site_name
                 ,cha.Account_Type_Id
                 ,cha.Account_Type
                 ,cha.Invoice_Source_Type_Id
                 ,cha.Invoice_Source
                 ,cha.commodity_Type_id
                 ,cha.Commodity_Type
                 ,cha.Vendor_id
                 ,cha.Vendor_name
                 ,cha.Account_id
                 ,CHA.Account_number
                 ,cha.Active
                 ,cha.Supplier_Account_Begin_Dt
                 ,cha.Not_managed
                 ,cha.Client_Id
                 ,cha.Client_Hier_Id
                 ,cha.Sitegroup_Id
                 ,cha.Supplier_Account_End_Dt
            ORDER BY
                  cha.Site_Name
                 ,cha.Account_number
                 ,cha.Supplier_Account_Begin_Dt

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_SearchForClient] TO [CBMSApplication]
GO
