SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_UPLOADED_RC_CLIENT_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_RFP_GET_UPLOADED_RC_CLIENT_LIST_P
@userId varchar(10),
@sessionId varchar(20)

AS
set nocount on
select c.client_id, c.client_name 

from	client c(nolock), 
	vwSiteName vwSite(nolock), 	
	account a(nolock),			
	sr_rc_contract_document_accounts_map accMap (nolock)
	
where  	 vwSite.site_id = a.site_id
	and c.client_id = vwSite.client_id	
	and accMap.sr_rc_contract_document_id is not null
	and accMap.account_id = a.account_id

group by c.client_id , c.client_name

order by c.client_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UPLOADED_RC_CLIENT_LIST_P] TO [CBMSApplication]
GO
