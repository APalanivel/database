
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******       
 NAME:  dbo.[Report_DE_RM_Hedge]        
 DESCRIPTION:   select distinct dealdetails.MONTH_IDENTIFIER from        
 RM_DEAL_TICKET_DETAILS        
 INPUT PARAMETERS:        
 Name   DataType   Default Description       
 -----------------------------------------------------------        
 @fromDate VARCHAR(12)        
 @toDate VARCHAR(12)        
 @clientId INT        
 @divisionId INT=0        
 @siteId INT        
 @unitId INT        
 @groupId INT=0                          
       
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description      
  ------------------------------------------------------------      
         
  USAGE EXAMPLES:       
  ------------------------------------------------------------            
   EXEC [dbo].[Report_DE_RM_Hedge]         
     @fromDate = '09/01/2012'        
    ,@toDate = '08/30/2013'        
    ,@clientId = 11660        
    ,@divisionId = 0        
    ,@unitId = 25        
    ,@groupId = 0        
      
AUTHOR INITIALS:        
Initials Name       
------------------------------------------------------------        
SP  Sandeep Pigilam        
      
 MODIFICATIONS         
 Initials Date  Modification       
 ------------------------------------------------------------         
 SP   2013-10-21  Created        
 AKR  2014-08-01  Modified to comment the Nymex filter.
   ******/      
     
CREATE PROCEDURE [dbo].[Report_DE_RM_Hedge]
      ( 
       @fromDate DATE
      ,@toDate DATE
      ,@clientId INT
      ,@divisionId INT = 0
      ,@unitId INT
      ,@groupId INT = 0 )
AS 
BEGIN      
      SET NOCOUNT ON      
      DECLARE
            @Fixed_Price_Entity INT
           ,@Nymex_Entity INT
           ,@Order_Executed_Entity INT
           ,@Trigger_Entity INT
           ,@weighted_strip_price_Entity INT
           ,@individual_month_pricing_Entity INT
           ,@Fixed_Entity INT
           ,@Closed_Entity INT
           ,@Options_Entity INT
           ,@Hedge_Type_Financial INT
           ,@Hedge_Type_Physical INT
           ,@Max_Conversion_Yr SMALLINT      
      CREATE TABLE #RM_DEAL_TICKET_ID
            ( 
             RM_DEAL_TICKET_ID INT PRIMARY KEY )      
      DECLARE @Sites TABLE
            ( 
             Site_Id INT NOT NULL
                         PRIMARY KEY )      
      INSERT      INTO @Sites
                  ( 
                   Site_Id )
                  SELECT
                        s.Site_Id
                  FROM
                        dbo.Site s
                  WHERE
                        s.Client_ID = @clientId
                        AND @divisionId = 0
                        AND @groupId = 0
                  UNION ALL
                  SELECT
                        s.Site_Id
                  FROM
                        dbo.Site s
                  WHERE
                        s.Client_ID = @clientId
                        AND DIVISION_ID = @divisionId
                        AND @divisionId > 0
                  UNION ALL
                  SELECT
                        s.Site_Id
                  FROM
                        dbo.Site s
                        INNER JOIN dbo.RM_GROUP_SITE_MAP groupmap
                              ON s.SITE_ID = groupmap.SITE_ID
                  WHERE
                        groupmap.RM_GROUP_ID = @GroupId
                        AND @GroupId > 0      
      SELECT
            @Hedge_Type_Financial = MAX(CASE WHEN ENTITY_NAME = 'financial' THEN ENTITY_ID
                                        END)
           ,@Hedge_Type_Physical = MAX(CASE WHEN ENTITY_NAME = 'Physical' THEN ENTITY_ID
                                       END)
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 273
            AND ENTITY_NAME IN ( 'financial', 'Physical' )      
      SELECT
            @Nymex_Entity = ENTITY_ID
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 263
            AND ENTITY_NAME LIKE 'nymex'         
         
      SELECT
            @Order_Executed_Entity = ENTITY_ID
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 286
            AND ENTITY_NAME IN ( 'order executed' )         
                   
      SELECT
            @Trigger_Entity = MAX(CASE WHEN ENTITY_NAME = 'trigger' THEN ENTITY_ID
                                  END)
           ,@Options_Entity = MAX(CASE WHEN ENTITY_NAME = 'options' THEN ENTITY_ID
                                  END)
           ,@Fixed_Price_Entity = MAX(CASE WHEN ENTITY_NAME = 'Fixed Price' THEN ENTITY_ID
                                      END)
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 268
            AND ENTITY_NAME IN ( 'trigger', 'options', 'Fixed Price' )        
        
         
      SELECT
            @weighted_strip_price_Entity = MAX(CASE WHEN ENTITY_NAME = 'weighted strip price' THEN ENTITY_ID
                                               END)
           ,@individual_month_pricing_Entity = MAX(CASE WHEN ENTITY_NAME = 'individual month pricing' THEN ENTITY_ID
                                                   END)
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 274
            AND ENTITY_NAME IN ( 'weighted strip price', 'individual month pricing' )        
         
      SELECT
            @Fixed_Entity = MAX(CASE WHEN ENTITY_NAME = 'fixed' THEN ENTITY_ID
                                END)
           ,@Closed_Entity = MAX(CASE WHEN ENTITY_NAME = 'closed' THEN ENTITY_ID
                                 END)
      FROM
            ENTITY
      WHERE
            ENTITY_TYPE = 287
            AND ENTITY_NAME IN ( 'fixed', 'closed' )      
      INSERT      INTO #RM_DEAL_TICKET_ID
                  ( 
                   RM_DEAL_TICKET_ID )
                  SELECT
                        RD.RM_DEAL_TICKET_ID
                  FROM
                        RM_DEAL_TICKET RD
                  WHERE
                        RD.CLIENT_ID = @ClientId
                        AND ( ( @fromDate BETWEEN RD.HEDGE_START_MONTH
                                          AND     RD.HEDGE_END_MONTH )
                              OR ( @toDate BETWEEN RD.HEDGE_START_MONTH
                                           AND     RD.HEDGE_END_MONTH )
                              OR ( RD.HEDGE_START_MONTH BETWEEN @fromDate
                                                        AND     @toDate )
                              OR ( RD.HEDGE_END_MONTH BETWEEN @fromDate AND @toDate ) )
                  GROUP BY
                        RD.RM_DEAL_TICKET_ID      
      SELECT
            @Max_Conversion_Yr = MAX(CONVERSION_YEAR)
      FROM
            dbo.RM_CURRENCY_UNIT_CONVERSION
      WHERE
            CLIENT_ID = @clientId         
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealdetails.HEDGE_PRICE AS DECIMAL(16, 12)) HEDGE_PRICE
           ,-1 PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,'-1' OPTION_NAME
           ,'financial' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity1.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus
                  ON dealTicket.RM_DEAL_TICKET_ID = dealstatus.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity1
                  ON dealticket.FREQUENCY_TYPE_ID = entity1.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.DEAL_TYPE_ID = @Fixed_Price_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Financial  
           -- AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID = @Order_Executed_Entity
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealdetails.HEDGE_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity1.ENTITY_NAME
      UNION
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealdetails.HEDGE_PRICE AS DECIMAL(8, 3)) HEDGE_PRICE
           ,-1 PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,'-1' OPTION_NAME
           ,'financial' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity1.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity1
                  ON dealticket.FREQUENCY_TYPE_ID = entity1.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.PRICING_REQUEST_TYPE_ID IN ( @weighted_strip_price_Entity, @individual_month_pricing_Entity )
            AND dealticket.DEAL_TYPE_ID = @Trigger_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Financial  
            --AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealdetails.TRIGGER_STATUS_TYPE_ID IN ( @Fixed_Entity, @Closed_Entity )
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealdetails.HEDGE_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity1.ENTITY_NAME
      UNION
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealoption.STRIKE_PRICE AS DECIMAL(8, 3)) HEDGE_PRICE
           ,CAST(dealoption.PREMIUM_PRICE AS DECIMAL(8, 3)) PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,entity1.ENTITY_NAME OPTION_NAME
           ,'financial' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity2.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus
                  ON dealTicket.RM_DEAL_TICKET_ID = dealstatus.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_OPTION_DETAILS dealoption
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealoption.RM_DEAL_TICKET_DETAILS_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            JOIN dbo.ENTITY entity1
                  ON dealoption.OPTION_TYPE_ID = entity1.ENTITY_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity2
                  ON dealticket.FREQUENCY_TYPE_ID = entity2.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.DEAL_TYPE_ID = @Options_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Financial  
            --AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID = @Order_Executed_Entity
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealoption.STRIKE_PRICE
           ,dealoption.PREMIUM_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,entity1.ENTITY_NAME
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity2.ENTITY_NAME
      UNION
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealdetails.HEDGE_PRICE AS DECIMAL(8, 3)) HEDGE_PRICE
           ,-1 PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,'-1' OPTION_NAME
           ,'Physical' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity1.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus
                  ON dealTicket.RM_DEAL_TICKET_ID = dealstatus.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity1
                  ON dealticket.FREQUENCY_TYPE_ID = entity1.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.DEAL_TYPE_ID = @Fixed_Price_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Physical  
            --AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID = @Order_Executed_Entity
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealdetails.HEDGE_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity1.ENTITY_NAME
      UNION
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealdetails.HEDGE_PRICE AS DECIMAL(8, 3)) HEDGE_PRICE
           ,-1 PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,'-1' OPTION_NAME
           ,'Physical' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity1.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity1
                  ON dealticket.FREQUENCY_TYPE_ID = entity1.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.PRICING_REQUEST_TYPE_ID IN ( @weighted_strip_price_Entity, @individual_month_pricing_Entity )
            AND dealticket.DEAL_TYPE_ID = @Trigger_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Physical  
            --AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealdetails.TRIGGER_STATUS_TYPE_ID IN ( @Fixed_Entity, @Closed_Entity )
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealdetails.HEDGE_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity1.ENTITY_NAME
      UNION
      SELECT
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,CAST(dealoption.STRIKE_PRICE AS DECIMAL(8, 3)) HEDGE_PRICE
           ,CAST(dealoption.PREMIUM_PRICE AS DECIMAL(8, 3)) PREMIUM
           ,dealdetails.MONTH_IDENTIFIER MONTH_IDENTIFIER
           ,entity1.ENTITY_NAME OPTION_NAME
           ,'Physical' HEDGE_TYPE
           ,ISNULL(CAST(onboard.MAX_HEDGE_PERCENT AS DECIMAL(8, 3)), '-1') MAX_HEDGE_PERCENT
           ,ISNULL(CAST(conversion.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') CONVERSION_FACTOR
           ,ISNULL(CAST(consumption.CONVERSION_FACTOR AS DECIMAL(10, 6)), '1.000') VOLUME_CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID CURRENCY_TYPE_ID
           ,CASE entity2.ENTITY_NAME
              WHEN 'Daily' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR * DATEPART(dd, DATEADD(dd, -( DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)) ), DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER)))) AS DECIMAL(20, 2))
              WHEN 'Monthly' THEN CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
              ELSE CAST(SUM(dealvolume.HEDGE_VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(20, 2))
            END HEDGE_VOLUME
      FROM
            dbo.RM_DEAL_TICKET dealticket
            JOIN #RM_DEAL_TICKET_ID Tickets
                  ON dealticket.RM_DEAL_TICKET_ID = Tickets.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus
                  ON dealTicket.RM_DEAL_TICKET_ID = dealstatus.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_DETAILS dealdetails
                  ON dealTicket.RM_DEAL_TICKET_ID = dealdetails.RM_DEAL_TICKET_ID
            JOIN dbo.RM_DEAL_TICKET_OPTION_DETAILS dealoption
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealoption.RM_DEAL_TICKET_DETAILS_ID
            JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
                  ON dealdetails.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
            JOIN dbo.ENTITY entity1
                  ON dealoption.OPTION_TYPE_ID = entity1.ENTITY_ID
            LEFT OUTER JOIN dbo.RM_ONBOARD_CLIENT onboard
                  ON dealticket.CLIENT_ID = onboard.CLIENT_ID
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                  ON dealticket.UNIT_TYPE_ID = consumption.BASE_UNIT_ID
            JOIN dbo.ENTITY entity2
                  ON dealticket.FREQUENCY_TYPE_ID = entity2.ENTITY_ID
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                  ON dealticket.CLIENT_ID = conversion.CLIENT_ID
                     AND conversion.CONVERSION_YEAR = @Max_Conversion_Yr
      WHERE
            dealticket.DEAL_TYPE_ID = @Options_Entity
            AND dealTicket.HEDGE_TYPE_ID = @Hedge_Type_Physical  
            --AND dealTicket.HEDGE_MODE_TYPE_ID = @Nymex_Entity  
            AND dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID = @Order_Executed_Entity
            AND dealticket.CLIENT_ID = @clientId
            AND EXISTS ( SELECT DISTINCT
                              hedge.SITE_ID
                         FROM
                              RM_ONBOARD_HEDGE_SETUP hedge
                              INNER JOIN @Sites s
                                    ON s.SITE_ID = hedge.SITE_ID
                         WHERE
                              hedge.SITE_ID = dealvolume.SITE_ID
                              AND hedge.INCLUDE_IN_REPORTS = 1 )
            AND dealdetails.MONTH_IDENTIFIER BETWEEN @fromDate
                                             AND     @toDate
            AND consumption.CONVERTED_UNIT_ID = @unitId
      GROUP BY
            dealticket.RM_DEAL_TICKET_ID
           ,dealvolume.SITE_ID
           ,dealoption.STRIKE_PRICE
           ,dealoption.PREMIUM_PRICE
           ,dealdetails.MONTH_IDENTIFIER
           ,entity1.ENTITY_NAME
           ,onboard.MAX_HEDGE_PERCENT
           ,conversion.CONVERSION_FACTOR
           ,consumption.CONVERSION_FACTOR
           ,dealticket.CURRENCY_TYPE_ID
           ,entity2.ENTITY_NAME      
END; 
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_RM_Hedge] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_RM_Hedge] TO [CBMSApplication]
GO
