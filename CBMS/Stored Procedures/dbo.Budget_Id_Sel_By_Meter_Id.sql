SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Id_Sel_By_Meter_Id]  
     
DESCRIPTION: 

	This procedure will fetch the budgets associated with the utility account of the given meter and the commodity which the meter associated.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC Budget_Id_Sel_By_Meter_Id  62461,1,5
	

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR		PANDARINATH

MODIFICATIONS
INITIALS	DATE			MODIFICATION
------------------------------------------------------------
PNR			03-June-10		CREATED

*/

CREATE PROCEDURE dbo.Budget_Id_Sel_By_Meter_Id
(
	@Meter_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Budget_List AS
	(
		SELECT
			 ba.BUDGET_ID
			 ,ROW_NUMBER() OVER(ORDER BY ba.BUDGET_ID) Row_Num
			 ,COUNT(1) OVER() Total_Rows
		FROM
			dbo.METER m
			JOIN dbo.RATE r
				 ON m.RATE_ID=r.RATE_ID
			JOIN dbo.BUDGET_ACCOUNT ba
				 ON m.ACCOUNT_ID = ba.ACCOUNT_ID
			JOIN dbo.BUDGET bg
				 ON ba.BUDGET_ID = bg.BUDGET_ID
				 AND bg.COMMODITY_TYPE_ID = r.COMMODITY_TYPE_ID
		WHERE
			m.METER_ID = @Meter_Id
			AND ba.IS_DELETED = 0
	)
	SELECT
		 Budget_id
		 ,Total_Rows
	FROM
		 Cte_Budget_List
	WHERE
		 Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Id_Sel_By_Meter_Id] TO [CBMSApplication]
GO
