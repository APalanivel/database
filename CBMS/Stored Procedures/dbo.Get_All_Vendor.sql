SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Get_All_Vendor   
    
DESCRIPTION:    
	To get all the Vendor names along with the State and Country it is mapped to for Pam data export	
	    
INPUT PARAMETERS:    
 Name					DataType	Default		Description
------------------------------------------------------------    
 @Vendor_Status_Filter	SMALLINT	2			1 - For all, 2 - Active, 3 - Inactive

OUTPUT PARAMETERS:    
 Name					DataType	Default		Description    
------------------------------------------------------------    

    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 HG			Harihara Suthan G    
    
MODIFICATIONS    
 Initials	Date		Modification    
------------------------------------------------------------    
 HG			2015-09-17	Created
******/    
CREATE PROCEDURE [dbo].[Get_All_Vendor]
      (
       @Vendor_Status_Filter SMALLINT = 2 )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            v.Vendor_Name [CBMS Vendor Name]
           ,v.Vendor_Id [CBMS Vendor Id]
           ,e.Entity_Name [CBMS Vendor Type]
           ,st.State_Name [CBMS Vendor State]
           ,ctry.Country_Name [CBMS Vendor Country]
      FROM
            dbo.Vendor v
            JOIN dbo.ENTITY e
                  ON e.Entity_Id = v.Vendor_TYPE_Id
            JOIN dbo.Vendor_State_Map vsm
                  ON vsm.Vendor_Id = v.Vendor_Id
            JOIN dbo.State st
                  ON st.State_Id = vsm.State_Id
            JOIN dbo.Country ctry
                  ON ctry.Country_Id = st.Country_Id
      WHERE
            ( @Vendor_Status_Filter = 1
              OR ( @Vendor_Status_Filter = 2
                   AND v.Is_History = 0 )
              OR ( @Vendor_Status_Filter = 3
                   AND v.Is_History = 1 ) );

END;
;
GO
GRANT EXECUTE ON  [dbo].[Get_All_Vendor] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Get_All_Vendor] TO [CBMSReports]
GO
