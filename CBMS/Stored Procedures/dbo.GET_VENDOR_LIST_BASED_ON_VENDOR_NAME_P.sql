
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.GET_VENDOR_LIST_BASED_ON_VENDOR_NAME_P  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name			DataType		Default Description  
------------------------------------------------------------  
 @entityName	VARCHAR(200)  
 @vendorType	INT  
 @Vendor_Name	VARCHAR(200)	NULL   
 @Country_Id	INT				NULL  
 @State_Id		INT				NULL  
 @Commodity_Id	INT				NULL  
 @EndIndex		INT  
 @StartIndex	INT  
  
OUTPUT PARAMETERS:  
 Name			DataType		Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
       
exec dbo.GET_VENDOR_LIST_BASED_ON_VENDOR_NAME_P 'utility', 155, 'souther',null,null,null  
exec dbo.GET_VENDOR_LIST_BASED_ON_VENDOR_NAME_P 'supplier', 155, 'souther',null,null,null  

         
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 BCH	  Balaraju Chalumuri  
  
MODIFICATIONS  
  
 Initials	Date		Modification  
-----------------------------------------------------------  

 BCH		2012-07-17  Added Country_Id,State_Id and Commodity_Id as optional paramenters,  
						And added Country_Id,country_name,State_Id ,state_name and Commodity_Name to select list For Rate Engine project.  
 BCH		2012-10-22 Supplier vendor filter logic changed to return the results irrespective of comma seperated commodity value
							- country and state join modified to return the results only for Utility Vendors

******/   
CREATE PROCEDURE dbo.GET_VENDOR_LIST_BASED_ON_VENDOR_NAME_P
      ( @entityName VARCHAR(200)
      ,@vendorType INT
      ,@vendorName VARCHAR(200)
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN  
      SET NOCOUNT ON;  
  
      WITH  GET_VENDOR_LIST_P
              AS ( SELECT
                        v.VENDOR_ID
                       ,v.VENDOR_NAME
                       ,cn.COUNTRY_ID
                       ,cn.COUNTRY_NAME
                       ,st.STATE_ID
                       ,st.STATE_NAME
                       ,left(SL.LIST, len(SL.LIST) - 1) AS COMMODITY_NAME
                       ,count(1) OVER ( ) AS Total
                       ,row_number() OVER ( ORDER BY v.VENDOR_NAME ) AS rowNum
                   FROM
                        dbo.VENDOR v
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = v.VENDOR_TYPE_ID
                        LEFT JOIN ( dbo.VENDOR_STATE_MAP vsm
                                    JOIN dbo.STATE st
                                          ON vsm.STATE_ID = st.STATE_ID
                                    JOIN dbo.COUNTRY cn
                                          ON st.COUNTRY_ID = cn.COUNTRY_ID )
                                    ON v.VENDOR_ID = vsm.VENDOR_ID
                                       AND @entityName = 'Utility'
                        OUTER APPLY ( SELECT
                                          com.Commodity_Name + ','
                                      FROM
                                          dbo.VENDOR_COMMODITY_MAP vcm
                                          JOIN dbo.Commodity com
                                                ON vcm.COMMODITY_TYPE_ID = com.Commodity_Id
                                      WHERE
                                          VCM.VENDOR_ID = v.VENDOR_ID
                                          AND ( com.Commodity_Id = @Commodity_Id
                                                OR @Commodity_Id IS NULL )
                                          AND @entityName = 'Utility'
                                      GROUP BY
                                          com.Commodity_Name
                        FOR
                                      XML PATH('') ) SL ( LIST )
                   WHERE
                        e.ENTITY_NAME = @entityName
                        AND e.ENTITY_TYPE = @vendorType
                        AND v.IS_HISTORY = 0
                        AND v.VENDOR_NAME LIKE '%' + @vendorName + '%'
                        AND ( cn.COUNTRY_ID = @Country_Id
                              OR @Country_Id IS NULL )
                        AND ( st.STATE_ID = @State_Id
                              OR @State_Id IS NULL )
                        AND ( @entityName = 'Supplier'
                              OR left(SL.LIST, len(SL.LIST) - 1) IS NOT NULL )
                   GROUP BY
                        v.VENDOR_ID
                       ,v.VENDOR_NAME
                       ,cn.COUNTRY_ID
                       ,cn.COUNTRY_NAME
                       ,st.STATE_ID
                       ,st.STATE_NAME
                       ,SL.LIST)
            SELECT
                  VENDOR_ID
                 ,VENDOR_NAME
                 ,cte.COUNTRY_ID
                 ,cte.COUNTRY_NAME
                 ,cte.STATE_ID
                 ,cte.STATE_NAME
                 ,cte.COMMODITY_NAME
                 ,cte.Total
                 ,cte.RowNum
            FROM
                  GET_VENDOR_LIST_P cte
            WHERE
                  rowNum BETWEEN @StartIndex AND @EndIndex
            ORDER BY
                  cte.VENDOR_NAME  
  
         
END

;
GO

GRANT EXECUTE ON  [dbo].[GET_VENDOR_LIST_BASED_ON_VENDOR_NAME_P] TO [CBMSApplication]
GO
