SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id        
                
Description:                
		This sproc to get the client details based state commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@State_Id					INT
	@Commodity_Id				INT
	@Ec_Account_Group_Type_Id   INT
	@Start_Dt					DATE
	@End_Dt						DATE
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

   Exec dbo.Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id   124,290,10985,50,null 
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id]
      ( 
       @State_Id INT
      ,@Commodity_Id INT
      ,@Client_Id INT
      ,@Ec_Account_Group_Type_Id INT = NULL
      ,@Ec_Client_Account_Group_Id INT = NULL
      ,@Start_Dt DATE = NULL
      ,@End_Dt DATE = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
      
       CREATE TABLE #Accounts
            ( 
             Account_Id INT
            ,Start_Dt DATE
            ,End_Dt DATE
            ,Account_Group_Name NVARCHAR(60)
            ,Ec_Client_Account_Group_Id INT )
      
      
      INSERT      INTO #Accounts
                  ( 
                   Account_Id
                  ,Start_Dt
                  ,End_Dt
                  ,Account_Group_Name
                  ,Ec_Client_Account_Group_Id )
                  SELECT
                        Account_Id
                       ,ecag.Start_Dt
                       ,ecag.End_Dt
                       ,Account_Group_Name
                       ,ecag.Ec_Client_Account_Group_Id
                  FROM
                        dbo.Ec_Client_Account_Group ecag
                        INNER JOIN dbo.Ec_Client_Account_Group_Account ecaga
                              ON ecag.Ec_Client_Account_Group_Id = ecaga.Ec_Client_Account_Group_Id
                  WHERE
                        ecag.Client_Id = @Client_Id
                        AND ( @Ec_Account_Group_Type_Id IS NULL
                              OR ecag.Ec_Account_Group_Type_Id = @Ec_Account_Group_Type_Id )
                        AND ( ( @Start_Dt IS NULL )
                              OR ( ecag.Start_Dt BETWEEN ISNULL(@Start_Dt, '1900-01-01')
                                                 AND     ISNULL(@End_Dt, '2099-01-01')
                                   OR @Start_Dt BETWEEN ecag.Start_Dt
                                                AND     ISNULL(ecag.End_Dt, '2099-01-01') ) )
                        AND ( ( @End_Dt IS NULL )
                              OR ( ecag.End_Dt BETWEEN ISNULL(@Start_Dt, '1900-01-01')
                                               AND     ISNULL(@End_Dt, '2099-01-01')
                                   OR @End_Dt BETWEEN ecag.Start_Dt
                                              AND     ISNULL(ecag.End_Dt, '2099-01-01') ) )
                  GROUP BY
                        Account_Id
                       ,ecag.Start_Dt
                       ,ecag.End_Dt
                       ,Account_Group_Name
                       ,ecag.Ec_Client_Account_Group_Id


     
      SELECT
            ch.Client_Id
           ,ch.Client_Name
           ,ch.Site_Id
           ,ch.Site_name
           ,ch.City
           ,ch.State_Id
           ,ch.State_Name
           ,cha.Account_Id
           ,cha.Account_Number
           ,a.Start_Dt Start_Dt
           ,a.End_Dt End_Dt
           ,CASE WHEN a.Account_Id IS NOT NULL THEN 1
                 ELSE 0
            END AS Is_Account_Added
           ,a.Account_Group_Name
           ,a.Ec_Client_Account_Group_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN #Accounts a
                  ON a.Account_Id = cha.Account_Id
      WHERE
            ch.Site_Id > 0
            AND cha.Account_Type = 'Utility'
            AND ch.Client_Id = @Client_Id
            AND cha.Account_Not_Managed = 0
            AND ch.State_Id = @State_Id
            AND cha.Commodity_Id = @Commodity_Id
      GROUP BY
            ch.Client_Id
           ,ch.Client_Name
           ,ch.Site_Id
           ,ch.Site_name
           ,ch.City
           ,ch.State_Id
           ,ch.State_Name
           ,cha.Account_Id
           ,cha.Account_Number
           ,a.Start_Dt
           ,a.End_Dt
           ,CASE WHEN a.Account_Id IS NOT NULL THEN 1
                 ELSE 0
            END
           ,a.Account_Group_Name
           ,a.Ec_Client_Account_Group_Id
      ORDER BY
            ch.Client_Name
           ,ch.Site_name
            
      
END
      




;
GO
GRANT EXECUTE ON  [dbo].[Group_Type_Client_Dtl_Sel_By_State_Commodity_Group_Id] TO [CBMSApplication]
GO
