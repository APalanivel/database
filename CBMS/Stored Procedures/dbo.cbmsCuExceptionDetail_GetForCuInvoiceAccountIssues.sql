SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE          PROCEDURE [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceAccountIssues]
	( @MyAccountId int
	, @cu_invoice_id int
	)
AS
BEGIN

	set nocount on

	declare @exception_group_type_id int

	   select @exception_group_type_id = entity_id
	     from entity with (nolock)
	    where entity_type = 712
	      and entity_name = 'Account Issues'

	set nocount off

	   select ced.cu_exception_detail_id
		, ced.cu_exception_id
		, eg.entity_id exception_group_type_id
		, eg.entity_name exception_group_type
		, ced.exception_type_id
		, et.exception_type
		, ced.exception_status_type_id
		, es.entity_name exception_status_type
		, ced.opened_date
		, ced.is_closed
		, ced.closed_reason_type_id
		, cr.entity_name closed_reason_type
		, ced.closed_by_id
		, ui.first_name + ' ' + ui.last_name closed_by
		, ced.closed_date
	     from cu_exception ce  with (nolock)
	     join cu_exception_detail ced with (nolock) on ced.cu_exception_id = ce.cu_exception_id
  left outer join cu_exception_type et with (nolock) on et.cu_exception_type_id = ced.exception_type_id
  left outer join entity eg with (nolock) on eg.entity_id = et.exception_group_type_id
  left outer join entity es with (nolock) on es.entity_id = ced.exception_status_type_id
  left outer join entity cr with (nolock) on cr.entity_id = ced.closed_reason_type_id
  left outer join user_info ui with (nolock) on ui.user_info_id = ced.closed_by_id
	    where ce.cu_invoice_id = @cu_invoice_id
	      and et.exception_group_type_id = @exception_group_type_id
	 order by ced.is_closed 
		, ced.opened_date desc


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceAccountIssues] TO [CBMSApplication]
GO
