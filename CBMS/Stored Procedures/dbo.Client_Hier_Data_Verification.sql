
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME: CBMS.dbo.Client_Hier_Data_Verification

 DESCRIPTION:

	Used to compare the Client_Hier table data with the base table values

 INPUT PARAMETERS:
 Name				DataType		Default Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 
 USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Client_Hier_Data_Verification

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G
 RC         Rao Chejarla

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 HG			01/04/2011		Created to compare the Client_Hier table data with base table values.
 HG			01/13/2011		Sitegroup_Type_Dsc column renamed to Sitegroup_Type_Name
 SKA		01/21/2011		Removed the sub selects from scripts
 HG			10/21/2011		MAINT-868 , Added code to compare the following list of columns
							Client_Type_Id
							Report_Frequency_Type_Id
							Fiscalyear_Startmonth_Type_Id
							Ubm_Service_Id
							Dsm_Strategy
							Is_Sep_Issued
							Sep_Issue_Date
							Sitegroup_Owner_User_Id
 RC          04/11/2012     Removed refernces to su.GHG_Region_* tables							
******/

CREATE PROCEDURE [dbo].[Client_Hier_Data_Verification]
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Client_Hier_Level_Cd INT
           ,@Sitegroup_Hier_Level_Cd INT
           ,@Division_Hier_Level_Cd INT
           ,@Site_Hier_Level_Cd INT
           ,@Sitegroup_Type_Cd_Division INT
			
      DECLARE @Client_Hier_Data_Mismatch TABLE
            ( 
             Column_Name VARCHAR(60)
            ,Client_Hier_Id INT
            ,Source_Value VARCHAR(200)
            ,Target_Value VARCHAR(200) )


      SELECT
            @Client_Hier_Level_Cd = Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_Level_Cd'
            AND cd.Code_Value = 'Corporate'

      SELECT
            @Sitegroup_Hier_Level_Cd = Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_Level_Cd'
            AND cd.Code_Value = 'Sitegroup'

      SELECT
            @Division_Hier_Level_Cd = Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_Level_Cd'
            AND cd.Code_Value = 'Division'

      SELECT
            @Site_Hier_Level_Cd = Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_Level_Cd'
            AND cd.Code_Value = 'Site'

      SELECT
            @Sitegroup_Type_Cd_Division = Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.CodeSet cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Sitegroup_Type_Cd'
            AND cd.Code_Value = 'Division'

		-- Hier_Level_Cd
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Hier_Level_Cd' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,case WHEN ch.Sitegroup_Id = 0 THEN @Client_Hier_Level_Cd
                             WHEN ch.Sitegroup_id > 0
                                  AND ch.Site_Id = 0
                                  AND ch.Sitegroup_Type_Cd = @Sitegroup_Type_Cd_Division THEN @Division_Hier_Level_Cd
                             WHEN ch.Sitegroup_id > 0
                                  AND ch.Site_Id = 0
                                  AND ch.Sitegroup_Type_Cd != @Sitegroup_Type_Cd_Division THEN @Sitegroup_Hier_Level_Cd
                             WHEN ch.Site_Id > 0 THEN @Site_Hier_Level_Cd
                        END AS Source_Value
                       ,ch.Hier_Level_Cd AS Target_Value
                  FROM
                        Core.Client_hier ch
                        JOIN dbo.code cd
                              ON cd.Code_Id = ch.Hier_level_Cd
                  WHERE
                        ( cd.Code_Value = 'Corporate'
                          AND ( ch.Sitegroup_Id > 0
                                OR ch.Site_Id > 0 ) )
                        OR ( cd.Code_Value IN ( 'Sitegroup', 'Division' )
                             AND ( ch.Site_Id > 0 ) )
                        OR ( cd.Code_Value IN ( 'Site' )
                             AND ( ch.Site_Id = 0 ) )							


		-- Client_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,'No record in Source' AS Source_Value
                       ,ch.Client_Id AS Target_Value
                  FROM
                        core.Client_Hier ch
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Client cl
                                     WHERE
                                          ch.Client_Id = cl.CLIENT_ID )
							
		-- Client_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cl.Client_Name AS Source_Value
                       ,ch.Client_Name AS Targe_Value
                  FROM
                        Client cl
                        JOIN core.Client_hier ch
                              ON ch.Client_Id = cl.Client_id
                  WHERE
                        ch.Client_name != cl.Client_Name
			 

		-- Client_Currency_Group_id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Currency_Group_id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cl.CURRENCY_GROUP_ID AS Source_Value
                       ,ch.Client_Currency_Group_Id AS Target_Value
                  FROM
                        Client cl
                        JOIN core.Client_hier ch
                              ON ch.Client_Id = cl.Client_id
                  WHERE
                        isnull(cl.Currency_Group_id, 0) != isnull(ch.CLient_Currency_Group_Id, 0)	
				

		-- Sitegroup_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sitegroup_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,'No Record in Source' AS Source_Value
                       ,ch.Sitegroup_Id AS Target_Value
                  FROM
                        core.Client_Hier ch
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Sitegroup sg
                                     WHERE
                                          ( sg.Is_Complete = 1
                                            OR sg.Is_Edited = 1 ) -- Valid Sitegroups
                                          AND ch.Client_id != sg.Client_Id )


      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sitegroup_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,'Inactive Sitegroup' AS Source_Value
                       ,ch.Sitegroup_Id AS Target_Value
                  FROM
                        core.Client_Hier ch
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.Sitegroup sg
                                 WHERE
                                    ( sg.Is_Complete = 0
                                      AND sg.Is_Edited = 0 ) -- Save process not yet completed Sitegroups
                                    AND sg.Sitegroup_id = ch.Sitegroup_Id )

		-- Sitegroup_Name	
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sitegroup_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,sg.Sitegroup_Name AS Source_Value
                       ,ch.Sitegroup_Name AS Target_Value
                  FROM
                        Sitegroup sg
                        JOIN core.Client_Hier ch
                              ON ch.Sitegroup_id = sg.Sitegroup_id
                  WHERE
                        sg.Sitegroup_Name != ch.Sitegroup_Name

		-- Site_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,'Not found in Source' AS Source_Value
                       ,ch.Site_Id AS Target_Value
                  FROM
                        core.Client_Hier ch
                  WHERE
                        ch.Site_Id > 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Site s
                                         WHERE
                                          s.Site_Id = ch.Site_id )
							
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,s.Client_Id AS Source_Value
                       ,ch.Client_Id AS Target_Value
                  FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.Site s
                              ON s.Site_id = ch.Site_Id
                  WHERE
                        s.Client_Id != ch.Client_Id

		-- Site_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,s.Site_Name AS Source_Value
                       ,ch.Site_Name AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                  WHERE
                        s.Site_Name != ch.Site_Name		
				

		-- Site_Address_Line1
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Address_Line1' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.ADDRESS_LINE1 AS Source_Value
                       ,ch.Site_Address_Line1 AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN dbo.Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        ad.ADDRESS_LINE1 ! = isnull(ch.Site_Address_Line1, '')
			

		-- Site_Address_Line2
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Address_Line2' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.ADDRESS_LINE2 AS Source_Value
                       ,ch.Site_Address_Line2 AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        isnull(ad.ADDRESS_LINE2, '') ! = isnull(ch.Site_Address_Line2, '')

		-- City
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'City' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.CITY AS Source_Value
                       ,ch.City AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        ad.City ! = ch.City	


		-- ZipCode
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'ZipCode' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.ZIPCODE AS Source_Value
                       ,ch.ZipCode AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        ad.ZipCode ! = ch.ZipCode

		-- State_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'State_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.State_Id AS Source_Value
                       ,ch.State_Id AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        ad.State_Id ! = isnull(ch.State_Id, 0)

		-- State_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'State_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,st.State_Name AS Source_Value
                       ,ch.State_Name AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                        JOIN State st
                              ON st.State_Id = ad.State_Id
                  WHERE
                        st.STATE_NAME ! = isnull(ch.State_Name, '')			

		-- Country_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Country_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,st.COUNTRY_ID AS Source_Value
                       ,ch.Country_Id AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                        JOIN State st
                              ON st.State_Id = ad.State_id
                  WHERE
                        st.Country_id ! = ch.Country_id					
			
		-- Country_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Country_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cy.Country_Name AS Source_Value
                       ,ch.COUNTRY_Name AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                        JOIN State st
                              ON st.State_Id = ad.State_id
                        JOIN Country cy
                              ON cy.Country_Id = st.Country_id
                  WHERE
                        cy.COUNTRY_Name ! = isnull(ch.COUNTRY_Name, '')

		-- GEO_LAT
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Geo_Lat' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,ad.Geo_Lat AS Source_Value
                       ,ch.Geo_Lat AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        isnull(ad.Geo_lat, 0) != isnull(ad.Geo_lat, 0)

		-- GEO_LONG
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Geo_Long' AS Column_Name
                       ,ch.Client_Hier_id
                       ,ad.Geo_Long AS Source_Value
                       ,ch.Geo_Long AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                  WHERE
                        isnull(ad.Geo_long, 0) != isnull(ad.Geo_long, 0)

		-- Site_Not_Managed
		
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Not_Managed' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,s.NOT_MANAGED AS Source_Value
                       ,ch.Site_Not_Managed AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                  WHERE
                        s.Not_Managed ! = ch.Site_Not_Managed


		-- Site_Closed
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Closed' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,s.Closed AS Source_Value
                       ,ch.Site_closed AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                  WHERE
                        s.Closed ! = ch.Site_Closed			
				

		-- Site_Closed_Dt
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Site_Closed_Dt' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,s.Closed_Date AS Source_Value
                       ,ch.Site_closed_Dt AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                  WHERE
                        isnull(convert(DATE, s.CLOSED_DATE), '1/1/1900') ! = isnull(ch.Site_Closed_Dt, '1/1/1900')
				

		-- Region_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Region_Id' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,st.Region_id AS Source_Value
                       ,ch.Region_Id AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                        JOIN State st
                              ON st.State_Id = ad.State_id
                        JOIN Region rg
                              ON rg.Region_id = st.Region_id
                  WHERE
                        st.Region_id != ch.Region_id


		-- Region_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Region_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,rg.Region_Name AS Source_Value
                       ,ch.Region_Name AS Target_Value
                  FROM
                        Site s
                        JOIN core.Client_Hier ch
                              ON ch.Site_id = s.Site_id
                        JOIN Address ad
                              ON ad.Address_Id = s.Primary_Address_id
                        JOIN State st
                              ON st.State_Id = ad.State_id
                        JOIN Region rg
                              ON rg.Region_id = st.Region_id
                  WHERE
                        rg.Region_Name != isnull(ch.Region_Name, '')
				

		-- Client_Not_Managed
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Not_Managed' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,c.Not_Managed AS Source_Value
                       ,ch.Client_Not_managed AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        JOIN client c
                              ON c.Client_Id = ch.Client_id
                  WHERE
                        c.Not_Managed != ch.Client_Not_Managed


		-- Division_Not_Managed
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Division_Not_Managed' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,sg.Not_Managed AS Source_Value
                       ,ch.Division_Not_Managed AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        JOIN Division_Dtl sg
                              ON sg.Sitegroup_Id = ch.Sitegroup_id
                  WHERE
                        sg.Not_Managed != ch.Division_Not_Managed

		-- Sitegroup_Type_Cd
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sitegroup_Type_Cd' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,sg.Sitegroup_Type_Cd AS Source_Value
                       ,ch.Sitegroup_Type_Cd AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        JOIN Sitegroup sg
                              ON sg.Sitegroup_id = ch.Sitegroup_id
                  WHERE
                        isnull(ch.Sitegroup_Type_Cd, 0) != sg.Sitegroup_Type_Cd
				
		-- Sitegroup_Type_Name
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sitegroup_Type_Name' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cd.Code_Value AS Source_Value
                       ,ch.Sitegroup_Type_Name AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        JOIN Sitegroup sg
                              ON sg.Sitegroup_id = ch.Sitegroup_id
                        JOIN Code cd
                              ON cd.Code_id = sg.Sitegroup_Type_Cd
                  WHERE
                        isnull(ch.Sitegroup_Type_Name, '') != cd.Code_Value


		-- User_Passcode_Expiration_Duration
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'User_Passcode_Expiration_Duration' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cl.User_Passcode_Expiration_Duration AS Source_Value
                       ,ch.User_Passcode_Expiration_Duration AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        cl.User_Passcode_Expiration_Duration != isnull(ch.User_Passcode_Expiration_Duration, 0)


		-- User_Max_Failed_Login_Attempts
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'User_Max_Failed_Login_Attempts' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cl.User_Max_Failed_Login_Attempts AS Source_Value
                       ,ch.User_Max_Failed_Login_Attempts AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        cl.User_Max_Failed_Login_Attempts != isnull(ch.User_Max_Failed_Login_Attempts, 0)			


		-- User_PassCode_Reuse_Limit
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'User_PassCode_Reuse_Limit' AS Column_Name
                       ,ch.Client_Hier_Id
                       ,cl.User_PassCode_Reuse_Limit AS Source_Value
                       ,ch.User_PassCode_Reuse_Limit AS Target_Value
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        cl.User_PassCode_Reuse_Limit != isnull(ch.User_PassCode_Reuse_Limit, 0)				
				
	
		--Client_Fiscal_Offset --(sub select is required as the column is derived)
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        Column_Name
                       ,Client_Hier_id
                       ,Source_Value
                       ,Target_Value
                  FROM
                        ( SELECT
                              'Client_Fiscal_Offset' AS Column_Name
                             ,Client_Hier_Id
                             ,case fsm.ENTITY_NAME
                                WHEN 'January' THEN 0
                                WHEN 'February' THEN -11
                                WHEN 'March' THEN -10
                                WHEN 'April' THEN -9
                                WHEN 'May' THEN -8
                                WHEN 'June' THEN -7
                                WHEN 'July' THEN -6
                                WHEN 'August' THEN -5
                                WHEN 'September' THEN -4
                                WHEN 'October' THEN -3
                                WHEN 'November' THEN -2
                                WHEN 'December' THEN -1
                              END AS Source_Value
                             ,isnull(ch.Client_Fiscal_Offset, 2147483647) AS Target_Value
                          FROM
                              Core.Client_Hier ch
                              JOIN dbo.Client cl
                                    ON cl.CLient_id = ch.Client_Id
                              JOIN dbo.Entity fsm
                                    ON fsm.Entity_id = cl.FiscalYear_StartMonth_Type_id ) x
                  WHERE
                        x.Source_Value != x.Target_Value
		
		-- Client_Type_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Client_Type_Id'
                       ,ch.Client_Hier_Id
                       ,ch.Client_Type_id
                       ,cl.Client_Type_id
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Client_Type_id,0) != cl.Client_Type_Id

		-- Report_Frequency_Type_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  ( 
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Report_Frequency_Type_Id'
                       ,ch.Client_Hier_Id
                       ,ch.Report_Frequency_Type_Id
                       ,cl.Report_Frequency_Type_Id
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Report_Frequency_Type_Id,0) != cl.Report_Frequency_Type_Id

		-- Fiscalyear_Startmonth_Type_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Fiscalyear_Startmonth_Type_Id'
                       ,ch.Client_Hier_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,cl.Fiscalyear_Startmonth_Type_Id
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Fiscalyear_Startmonth_Type_Id,0) != cl.Fiscalyear_Startmonth_Type_Id

		-- Ubm_Service_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Ubm_Service_Id'
                       ,ch.Client_Hier_Id
                       ,ch.Ubm_Service_Id
                       ,cl.Ubm_Service_Id
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Ubm_Service_Id,0) != isnull(cl.Ubm_Service_Id, 0)

		-- Dsm_Strategy
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Dsm_Strategy'
                       ,ch.Client_Hier_Id
                       ,ch.Dsm_Strategy
                       ,cl.Dsm_Strategy
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Dsm_Strategy,0) != isnull(cl.Dsm_Strategy, 0)

		-- Is_Sep_Issued
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Is_Sep_Issued'
                       ,ch.Client_Hier_Id
                       ,ch.Is_Sep_Issued
                       ,cl.Is_Sep_Issued
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Is_Sep_Issued,0) != isnull(cl.Is_Sep_Issued, 0)


		-- Sep_Issue_Date
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                        'Sep_Issue_Date'
                       ,ch.Client_Hier_Id
                       ,ch.Sep_Issue_Date
                       ,cl.Sep_Issue_Date
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Client cl
                              ON cl.Client_Id = ch.Client_id
                  WHERE
                        isnull(ch.Sep_Issue_Date,'1/1/1900') != isnull(cl.Sep_Issue_Date, '1/1/1900')

		-- Sitegroup_Owner_User_Id
      INSERT      INTO @Client_Hier_Data_Mismatch
                  (
                   Column_Name
                  ,Client_Hier_Id
                  ,Source_Value
                  ,Target_Value )
                  SELECT
                       'Sitegroup_Owner_User_Id'
                       ,ch.Client_Hier_Id
                       ,ch.Sitegroup_Owner_User_Id
                       ,sg.Owner_User_Id
                  FROM
                        core.Client_Hier ch
                        JOIN dbo.Sitegroup sg
                              ON sg.Sitegroup_Id = ch.Sitegroup_id
                  WHERE
                        ch.Sitegroup_Type_Name IN ('Global','User')
                        AND isnull(ch.Sitegroup_Owner_User_Id,0) != sg.Owner_User_Id


      SELECT
            Column_Name
           ,Client_Hier_Id
           ,Source_Value
           ,Target_Value
      FROM
            @Client_Hier_Data_Mismatch

END
;
GO


GRANT EXECUTE ON  [dbo].[Client_Hier_Data_Verification] TO [CBMSApplication]
GO
