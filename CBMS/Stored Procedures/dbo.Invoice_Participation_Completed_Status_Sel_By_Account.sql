SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 CBMS.dbo.Invoice_Participation_Completed_Status_Sel_By_Account
 
 Description:  
 
 Input Parameters:  
    Name			    DataType	 Default Description  
------------------------------------------------------------------------  
    @Report_Year	    INT		 NULL
    @Client_Id		    INT		 NULL
    @Division_Id	    INT		 NULL
    @Site_Client_Hier_Id INT		 NULL
    @RegionId		    INT		 NULL
    @Country_Id	    INT		 NULL
    @Site_Not_Managed   INT		 NULL
    @Start_Index	    INT		 1
    @End_Index		    INT		 2147483647 
 
 Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------  
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Account 2011, 10069, NULL, NULL, NULL, NULL, NULL, NULL, 1, 10
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Account 2011, 170
EXECUTE dbo.Invoice_Participation_Completed_Status_Sel_By_Account 2008, 170, NULL, 2195, NULL, NULL, NULL

 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 
 Modifications :  
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP		  2012-03-13  Created and replaced flowlling SPs as a part of Addtl Data changes
					   - dbo.cbmsCostUsage_LoadForAccount
 AP		  2012-04-18  Replaced @Site_Id parameter with @Site_Client_Hier_Id
 NR		  2020-07-01	SE2017-512 - Added @Rate_Id input parameter.

******/
CREATE PROCEDURE [dbo].[Invoice_Participation_Completed_Status_Sel_By_Account]
    (
        @Report_Year INT
        , @Client_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Client_Hier_Id INT = NULL
        , @Region_Id INT = NULL
        , @State_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Site_Type_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Rate_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Start_Month DATE
            , @End_Month DATE
            , @Usd_Currency_Unit_Id INT
            , @EP_Commodity_Id INT
            , @NG_Commodity_Id INT
            , @Site_Type_Name VARCHAR(200);

        CREATE TABLE #Account_Dtl
             (
                 Client_Name VARCHAR(200)
                 , Sitegroup_Name VARCHAR(200)
                 , Site_Id INT
                 , Client_Hier_Id INT
                 , City VARCHAR(200)
                 , State_Name VARCHAR(20)
                 , Region_Name VARCHAR(200)
                 , Address_Line1 VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(200)
                 , Vendor_Name VARCHAR(200)
                 , Client_Currency_Group_Id INT
                 , Commodity_Id INT
                 , Total_Rows INT
                 , Rate_Name VARCHAR(200)
             );

        CREATE CLUSTERED INDEX IDX_Account_Dtl_Account_Id
            ON #Account_Dtl
        (
            Site_Id
            , Account_Id
            , Commodity_Id
        )   ;

        DECLARE @Service_Months TABLE
              (
                  Service_Month DATE PRIMARY KEY CLUSTERED
                  , Month_Num SMALLINT
              );


        SELECT
            @Usd_Currency_Unit_Id = CU.CURRENCY_UNIT_ID
        FROM
            dbo.CURRENCY_UNIT CU
        WHERE
            CU.SYMBOL = 'USD';

        SELECT
            @EP_Commodity_Id = MAX(CASE WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                   END)
            , @NG_Commodity_Id = MAX(CASE WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                     END)
        FROM
            dbo.Commodity com
        WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' );

        SELECT
            @Site_Type_Name = st.ENTITY_NAME
        FROM
            dbo.ENTITY st
        WHERE
            st.ENTITY_ID = @Site_Type_Id;

        INSERT INTO @Service_Months
             (
                 Service_Month
                 , Month_Num
             )
        SELECT
            dd.DATE_D
            , ROW_NUMBER() OVER (ORDER BY
                                     dd.DATE_D)
        FROM    (   SELECT
                        DATEADD(m, ch.Client_Fiscal_Offset, CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                    FROM
                        Core.Client_Hier ch
                    WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                    UNION ALL
                    SELECT
                        CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS VARCHAR(10))
                    WHERE
                        @Client_Id IS NULL) X
                CROSS JOIN meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN X.Start_month
                      AND     DATEADD(MONTH, -1, (DATEADD(YEAR, 1, X.Start_month)));

        SELECT
            @Start_Month = MIN(sm.Service_Month)
            , @End_Month = MAX(sm.Service_Month)
        FROM
            @Service_Months sm;

        WITH CTE_Account_Dtl
        AS (
               SELECT
                    CH.Client_Name
                    , CH.Sitegroup_Name
                    , CH.Site_Id
                    , CH.Client_Hier_Id
                    , CH.City
                    , CH.State_Name
                    , CH.Region_Name
                    , CH.Site_Address_Line1
                    , CHA.Account_Id
                    , CHA.Display_Account_Number AS Account_Number
                    , CHA.Account_Vendor_Name
                    , CH.Client_Currency_Group_Id
                    , CHA.Commodity_Id
                    , ROW_NUMBER() OVER (ORDER BY
                                             CH.Client_Name
                                             , CH.Sitegroup_Name
                                             , CH.City
                                             , CHA.Account_Id ASC) AS Row_Num
                    , COUNT(1) OVER () AS Total_Rows
                    , CHA.Rate_Name
               FROM
                    Core.Client_Hier CH
                    INNER JOIN Core.Client_Hier_Account CHA
                        ON CH.Client_Hier_Id = CHA.Client_Hier_Id
               WHERE
                    (   @Client_Id IS NULL
                        OR  CH.Client_Id = @Client_Id)
                    AND (   @Division_Id IS NULL
                            OR  CH.Sitegroup_Id = @Division_Id)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  CH.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND (   @Site_Type_Id IS NULL
                            OR  CH.Site_Type_Name = @Site_Type_Name)
                    AND (   @State_Id IS NULL
                            OR  CH.State_Id = @State_Id)
                    AND (   @Region_Id IS NULL
                            OR  CH.Region_ID = @Region_Id)
                    AND (   @Vendor_Id IS NULL
                            OR  CHA.Account_Vendor_Id = @Vendor_Id)
                    AND CH.Client_Not_Managed = 0
                    AND CH.Site_Not_Managed = 0
                    AND CH.Site_Closed = 0
                    AND CH.Division_Not_Managed = 0
                    AND CHA.Account_Not_Managed = 0
                    AND (   CHA.Supplier_Account_begin_Dt IS NULL
                            OR  CHA.Supplier_Account_begin_Dt <= @End_Month)
                    AND (   CHA.Supplier_Account_End_Dt IS NULL
                            OR  CHA.Supplier_Account_End_Dt >= @Start_Month)
                    AND (   @Rate_Id IS NULL
                            OR  CHA.Rate_Id = @Rate_Id)
               GROUP BY
                   CH.Client_Name
                   , CH.Sitegroup_Name
                   , CH.Site_Id
                   , CH.Client_Hier_Id
                   , CH.City
                   , CH.State_Name
                   , CH.Region_Name
                   , CH.Site_Address_Line1
                   , CHA.Account_Id
                   , CHA.Display_Account_Number
                   , CHA.Account_Vendor_Name
                   , CH.Client_Currency_Group_Id
                   , CHA.Commodity_Id
                   , CHA.Rate_Name
           )
        INSERT INTO #Account_Dtl
             (
                 Client_Name
                 , Sitegroup_Name
                 , Site_Id
                 , Client_Hier_Id
                 , City
                 , State_Name
                 , Region_Name
                 , Address_Line1
                 , Account_Id
                 , Account_Number
                 , Vendor_Name
                 , Client_Currency_Group_Id
                 , Commodity_Id
                 , Total_Rows
                 , Rate_Name
             )
        SELECT
            ad.Client_Name
            , ad.Sitegroup_Name
            , ad.Site_Id
            , ad.Client_Hier_Id
            , ad.City
            , ad.State_Name
            , ad.Region_Name
            , ad.Site_Address_Line1
            , ad.Account_Id
            , ad.Account_Number
            , ad.Account_Vendor_Name
            , ad.Client_Currency_Group_Id
            , ad.Commodity_Id
            , ad.Total_Rows
            , ad.Rate_Name
        FROM
            CTE_Account_Dtl ad
        WHERE
            ad.Row_Num BETWEEN @Start_Index
                       AND     @End_Index;


        SELECT
            AD.Client_Name
            , AD.Sitegroup_Name AS Division_Name
            , AD.City
            , AD.State_Name
            , AD.Region_Name
            , AD.City AS Site_Name
            , AD.Address_Line1
            , AD.Account_Id
            , AD.Account_Number
            , AD.Vendor_Name
            , AD.Site_Id
            , @Start_Month AS Fiscal_Start
            , @End_Month AS Fiscal_End
            , sm.Service_Month
            , (CASE WHEN SUM(CAST(IP.IS_EXPECTED AS INT)) = SUM(CAST(IP.IS_RECEIVED AS INT)) THEN 'Yes'
                   WHEN SUM(CAST(IP.IS_EXPECTED AS INT)) != SUM(CAST(IP.IS_RECEIVED AS INT)) THEN 'No'
               END) AS Complete
            , AD.Total_Rows
            , AD.Rate_Name
        FROM
            #Account_Dtl AD
            CROSS JOIN @Service_Months sm
            LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION IP
                ON IP.SITE_ID = AD.Site_Id
                   AND  IP.ACCOUNT_ID = AD.Account_Id
                   AND  IP.SERVICE_MONTH = sm.Service_Month
                   AND  AD.Commodity_Id IN ( @EP_Commodity_Id, @NG_Commodity_Id )
        GROUP BY
            AD.Client_Name
            , AD.Sitegroup_Name
            , AD.City
            , AD.State_Name
            , AD.Region_Name
            , AD.Address_Line1
            , AD.Account_Id
            , AD.Account_Number
            , AD.Vendor_Name
            , AD.Site_Id
            , sm.Service_Month
            , AD.Total_Rows
            , AD.Rate_Name
        ORDER BY
            AD.Client_Name
            , AD.Sitegroup_Name
            , AD.City;

        DROP TABLE #Account_Dtl;
    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Completed_Status_Sel_By_Account] TO [CBMSApplication]
GO
