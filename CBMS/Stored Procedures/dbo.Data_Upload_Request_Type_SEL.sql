SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************      
NAME : dbo.Data_Upload_Request_Type_SEL  
     
DESCRIPTION:   
Stored Procedure is used to get the data from Data Upload Audit Log.
 
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   
@RequestType			VARCHAR

 OUTPUT PARAMETERS:      
 Name			DataType				Default			Description      
--------------------------------------------------------------------      
DATA_UPLOAD_REQUEST_TYPE_ID		INT
REQUEST_HANDLER_URL				VARCHAR
  
  USAGE EXAMPLES:      
--------------------------------------------------------------------      
EXEC Data_Upload_Request_Type_SEL 'C&U'
    
AUTHOR INITIALS:      
 Initials Name      
-------------------------------------------------------------------      
Romy Thomas
     
 MODIFICATIONS       
 Initials Date  Modification      
--------------------------------------------------------------------
RT		 07/13/2011 Created
******************************************************************************************************/  

CREATE PROCEDURE [dbo].[Data_Upload_Request_Type_SEL]
      ( 
       @RequestType VARCHAR(200) )
AS 
BEGIN
      SET NOCOUNT ON ;
     
      SELECT
            DATA_UPLOAD_REQUEST_TYPE_ID
           ,REQUEST_HANDLER_URL
           ,HANDLE_IMMEDIATE
      FROM
            DATA_UPLOAD_REQUEST_TYPE
      WHERE
            REQUEST_NAME = @RequestType
		
END
GO
GRANT EXECUTE ON  [dbo].[Data_Upload_Request_Type_SEL] TO [CBMSApplication]
GO
