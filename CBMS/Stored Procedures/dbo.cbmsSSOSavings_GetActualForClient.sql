SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME:
	dbo.cbmsSSOSavings_GetActualForClient

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    @client_id		INT    	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSSOSavings_GetActualForClient 207
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			03/31/2011	Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
							GROUP BY Clause added instead of Distinct Clause in sub query select.
******/


CREATE PROCEDURE dbo.cbmsSSOSavings_GetActualForClient
( 
 @client_id INT )
AS 
BEGIN
    
      SELECT
            ssd.sso_savings_id
           ,ssd.sso_savings_detail_id
           ,ssd.service_month
           ,ssd.estimated_savings_value
           ,ssd.actual_savings_value
      FROM
            dbo.sso_savings_detail ssd
      WHERE
            ssd.actual_savings_value > 0
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.SSO_SAVINGS_OWNER_MAP ssom
                              JOIN Core.Client_Hier ch
                                    ON ch.Client_Hier_Id = ssom.Client_Hier_Id
                         WHERE
                              ch.client_id = @client_id
                              AND ssd.SSO_SAVINGS_ID = ssom.SSO_SAVINGS_ID ) 
                  

END


GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetActualForClient] TO [CBMSApplication]
GO
