
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                              
                                    
NAME: dbo.Cu_Invoice_Recalc_Response_Data_Transfer                                  
                                    
DESCRIPTION:                                     
      Select the changed Account,commodity,service month Level data from Cu_Invoice_Recalc_Response 
      tables(using change tracking) to transfer it to Hub                                    
                                          
INPUT PARAMETERS:                                              
NAME			DATATYPE	DEFAULT			DESCRIPTION                                              
------------------------------------------------------------                                              
 @Min_CT_Ver	BIGINT                            
 @Max_CT_Ver	BIGINT                                 
                                                    
OUTPUT PARAMETERS:                                              
NAME			DATATYPE	DEFAULT			DESCRIPTION                                              
------------------------------------------------------------                                              
      
USAGE EXAMPLES:                                              
------------------------------------------------------------                                    
 select top 10 * from  Cu_Invoice_Recalc_Response                      
 DECLARE @Current_Version BIGINT                        
 SELECT @Current_Version = CHANGE_TRACKING_CURRENT_VERSION()                        
 --EXEC dbo.Cu_Invoice_Recalc_Response_Data_Transfer_1 0,@Current_Version                        
 EXEC dbo.Cu_Invoice_Recalc_Response_Data_Transfer 0,@Current_Version                        
                        
AUTHOR INITIALS:                                              
INITIALS	NAME                        
------------------------------------------------------------                                              
RKV			Ravi Kumar Vegesna    
      
MODIFICATIONS                                    
INITIALS	DATE		MODIFICATION                                    
------------------------------------------------------------                                    
RKV			2016-02-26  Created for AS400
*/
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Data_Transfer]
      (
       @Min_CT_Ver BIGINT
      ,@Max_CT_Ver BIGINT )
AS
BEGIN

      SET NOCOUNT ON;

	/*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/   
      IF 1 = 2
            BEGIN

                  SELECT
                        CAST(NULL AS INT) AS Client_Hier_Id
                       ,CAST(NULL AS INT) AS ACCOUNT_ID
                       ,CAST(NULL AS INT) AS Commodity_Id
                       ,CAST(NULL AS DATE) AS SERVICE_MONTH
                       ,CAST(NULL AS INT) AS Cu_Invoice_Recalc_Response_Id
                       ,CAST(NULL AS NVARCHAR(200)) AS Charge_Name
                       ,CAST(NULL AS INT) AS Bucket_Master_Id
                       ,CAST(NULL AS BIT) AS Is_Hidden_On_RA
                       ,CAST(NULL AS DECIMAL(28, 10)) AS Net_Amount
                       ,CAST(NULL AS INT) AS Net_Amt_Currency_Unit_Id
                       ,CAST(NULL AS INT) AS Updated_User_Id
                       ,CAST(NULL AS CHAR(1)) AS Sys_Change_Operation;
                       
            END;

      CREATE TABLE #Recalc_Data
            (
             Client_Hier_Id INT NULL
            ,ACCOUNT_ID INT NULL
            ,Commodity_Id INT NULL
            ,SERVICE_MONTH DATE NULL
            ,cu_Invoice_id INT NULL
            ,Cu_Invoice_Recalc_Response_Id INT NOT NULL
            ,Charge_Name NVARCHAR(200) NULL
            ,Bucket_Master_Id INT NULL
            ,Calculator_Working NVARCHAR(MAX) NULL
            ,Created_User_Id INT NULL
            ,Determinant_Unit NVARCHAR(200) NULL
            ,Determinant_Value DECIMAL(28, 10) NULL
            ,Is_Hidden_On_RA BIT NULL
            ,Net_Amount DECIMAL(28, 10) NULL
            ,Net_Amt_Currency_Unit_Id INT NULL
            ,Rate_Amount DECIMAL(28, 10) NULL
            ,Rate_Currency NVARCHAR(200) NULL
            ,Rate_Currency_Unit_Id INT NULL
            ,Rate_Unit NVARCHAR(200) NULL
            ,Rate_Uom_Type_Id INT NULL
            ,Recalc_Response_Source_Cd INT NULL
            ,Updated_User_Id INT NULL
            ,Sys_Change_Operation CHAR(1) NOT NULL );
      CREATE CLUSTERED INDEX #cx#Recalc_Data ON #Recalc_Data(Cu_Invoice_Recalc_Response_Id);

      CREATE TABLE #Month_Cnt
            (
             Cu_Invoice_Recalc_Response_Id INT
            ,Month_Cnt INT );

      CREATE TABLE #Cu_Invoice_Recalc_Response_Change_Operation
            (
             Cu_Invoice_Recalc_Response_Id INT
            ,Sys_Change_Operation CHAR(1)
            ,Sys_Change_Version BIGINT );

      INSERT      INTO #Cu_Invoice_Recalc_Response_Change_Operation
                  (Cu_Invoice_Recalc_Response_Id
                  ,Sys_Change_Operation
                  ,Sys_Change_Version )
                  SELECT
                        ct.Cu_Invoice_Recalc_Response_Id
                       ,ct.Sys_Change_Operation
                       ,ct.Sys_Change_Version
                  FROM
                        CHANGETABLE(CHANGES dbo.Cu_Invoice_Recalc_Response, @Min_CT_Ver) ct
                  WHERE
                        ct.Sys_Change_Version <= @Max_CT_Ver;
    
      INSERT      INTO #Recalc_Data
                  (Client_Hier_Id
                  ,ACCOUNT_ID
                  ,Commodity_Id
                  ,SERVICE_MONTH
                  ,cu_Invoice_id
                  ,Cu_Invoice_Recalc_Response_Id
                  ,Charge_Name
                  ,Bucket_Master_Id
                  ,Calculator_Working
                  ,Created_User_Id
                  ,Determinant_Unit
                  ,Determinant_Value
                  ,Is_Hidden_On_RA
                  ,Net_Amount
                  ,Net_Amt_Currency_Unit_Id
                  ,Rate_Amount
                  ,Rate_Currency
                  ,Rate_Currency_Unit_Id
                  ,Rate_Unit
                  ,Rate_Uom_Type_Id
                  ,Recalc_Response_Source_Cd
                  ,Updated_User_Id
                  ,Sys_Change_Operation )
                  SELECT
                        cha.Client_Hier_Id
                       ,rh.ACCOUNT_ID
                       ,rh.Commodity_Id
                       ,cism.SERVICE_MONTH
                       ,cism.CU_INVOICE_ID
                       ,cirr.Cu_Invoice_Recalc_Response_Id
                       ,cirr.Charge_Name
                       ,cirr.Bucket_Master_Id
                       ,cirr.Calculator_Working
                       ,cirr.Created_User_Id
                       ,cirr.Determinant_Unit
                       ,cirr.Determinant_Value
                       ,cirr.Is_Hidden_On_RA
                       ,cirr.Net_Amount
                       ,cirr.Net_Amt_Currency_Unit_Id
                       ,cirr.Rate_Amount
                       ,cirr.Rate_Currency
                       ,cirr.Rate_Currency_Unit_Id
                       ,cirr.Rate_Unit
                       ,cirr.Rate_Uom_Type_Id
                       ,cirr.Recalc_Response_Source_Cd
                       ,ISNULL(cirr.Updated_User_Id, cirr.Created_User_Id) Updated_User_Id
                       ,ct.Sys_Change_Operation
                  FROM
                        #Cu_Invoice_Recalc_Response_Change_Operation ct
                        INNER JOIN dbo.Cu_Invoice_Recalc_Response cirr
                              ON cirr.Cu_Invoice_Recalc_Response_Id = ct.Cu_Invoice_Recalc_Response_Id
                        INNER JOIN dbo.RECALC_HEADER rh
                              ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                              ON rh.CU_INVOICE_ID = cism.CU_INVOICE_ID
                                 AND rh.ACCOUNT_ID = cism.Account_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Account_Id = cism.Account_ID
                  WHERE
                        ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                        AND cism.SERVICE_MONTH IS NOT NULL
                        AND ct.Sys_Change_Operation IN ( 'I', 'U' )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.CU_EXCEPTION_DENORM ced
                                         WHERE
                                          ced.CU_INVOICE_ID = rh.CU_INVOICE_ID
                                          AND ced.Account_ID = rh.ACCOUNT_ID
                                          AND ced.Commodity_Id = rh.Commodity_Id
                                          AND ced.EXCEPTION_TYPE = 'Failed Recalc' )
                  GROUP BY
                        cha.Client_Hier_Id
                       ,rh.ACCOUNT_ID
                       ,rh.Commodity_Id
                       ,cism.SERVICE_MONTH
                       ,cirr.Cu_Invoice_Recalc_Response_Id
                       ,cirr.Charge_Name
                       ,cirr.Bucket_Master_Id
                       ,cirr.Calculator_Working
                       ,cirr.Created_User_Id
                       ,cirr.Determinant_Unit
                       ,cirr.Determinant_Value
                       ,cirr.Is_Hidden_On_RA
                       ,cirr.Net_Amount
                       ,cirr.Net_Amt_Currency_Unit_Id
                       ,cirr.Rate_Amount
                       ,cirr.Rate_Currency
                       ,cirr.Rate_Currency_Unit_Id
                       ,cirr.Rate_Unit
                       ,cirr.Rate_Uom_Type_Id
                       ,cirr.Recalc_Response_Source_Cd
                       ,cism.CU_INVOICE_ID
                       ,cirr.Updated_User_Id
                       ,ct.Sys_Change_Operation;


      INSERT      INTO #Month_Cnt
                  (Cu_Invoice_Recalc_Response_Id
                  ,Month_Cnt )
                  SELECT
                        Cu_Invoice_Recalc_Response_Id
                       ,NULLIF(COUNT(DISTINCT SERVICE_MONTH), 0)
                  FROM
                        #Recalc_Data
                  GROUP BY
                        Cu_Invoice_Recalc_Response_Id;

      UPDATE
            rd
      SET
            rd.Net_Amount = rd.Net_Amount / mc.Month_Cnt
      FROM
            #Recalc_Data rd
            INNER JOIN #Month_Cnt mc
                  ON mc.Cu_Invoice_Recalc_Response_Id = rd.Cu_Invoice_Recalc_Response_Id;

      INSERT      INTO #Recalc_Data
                  (Cu_Invoice_Recalc_Response_Id
                  ,Sys_Change_Operation )
                  SELECT
                        ct.Cu_Invoice_Recalc_Response_Id
                       ,ct.Sys_Change_Operation
                  FROM
                        #Cu_Invoice_Recalc_Response_Change_Operation ct
                  WHERE
                        ct.Sys_Change_Version <= @Max_CT_Ver
                        AND ct.Sys_Change_Operation = 'D'
                  GROUP BY
                        ct.Cu_Invoice_Recalc_Response_Id
                       ,ct.Sys_Change_Operation;
      SELECT
            Client_Hier_Id
           ,ACCOUNT_ID
           ,Commodity_Id
           ,SERVICE_MONTH
           ,Cu_Invoice_Recalc_Response_Id
           ,Charge_Name
           ,Bucket_Master_Id
           ,Is_Hidden_On_RA
           ,Net_Amount
           ,Net_Amt_Currency_Unit_Id
           ,Updated_User_Id
           ,Sys_Change_Operation
      FROM
            #Recalc_Data; 
      
      DROP TABLE #Recalc_Data;
      DROP TABLE #Cu_Invoice_Recalc_Response_Change_Operation;
      DROP TABLE #Month_Cnt;

END;      
;
;
;

GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Data_Transfer] TO [CBMSApplication]
GO
