SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Nymex_Forecast_Del]  
     
DESCRIPTION: 
	It Deletes Budget Nymex Forecast Detail for Selected Budget Nymex Forecast Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------          
@Budget_Nymex_Forecast_Id		INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN
		EXEC Budget_Nymex_Forecast_Del 239089
	ROLLBACK TRAN
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			26-MAY-10		CREATED     

*/  

CREATE PROCEDURE dbo.Budget_Nymex_Forecast_Del
    (
      @Budget_Nymex_Forecast_Id INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
	DELETE 
	
	FROM 
		dbo.BUDGET_NYMEX_FORECAST
	WHERE
		BUDGET_NYMEX_FORECAST_ID = @Budget_Nymex_Forecast_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Nymex_Forecast_Del] TO [CBMSApplication]
GO
