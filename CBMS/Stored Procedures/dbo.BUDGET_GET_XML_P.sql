SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE dbo.BUDGET_GET_XML_P
	@budget_xml_id int
	AS
	begin
		set nocount on

		select budget_account_id, budget_xml 
		from budget_detail_snap_shot	
		where budget_detail_snap_shot_id = @budget_xml_id
	end








GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_XML_P] TO [CBMSApplication]
GO
