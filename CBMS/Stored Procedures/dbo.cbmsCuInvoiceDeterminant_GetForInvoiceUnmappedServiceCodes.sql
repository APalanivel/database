SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:  
	dbo.cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes

DESCRIPTION:
	Used to return all the unmapped services from either determinant or charges of the gives invoice id.
	
INPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------
@Cu_invoice_id	INT

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes	6918866
	EXEC dbo.cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes	6793409
	EXEC dbo.cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes	3912353

	SELECT TOP 10 * FROM dbo.Cu_Invoice_Charge  chg
	WHERE
        chg.ubm_service_code IS NOT NULL
          AND chg.ubm_service_type_id IS NULL
          AND commodity_Type_id >0 AND commodity_Type_id <290
	ORDER BY Cu_invoice_id DESC

	SELECT TOP 10 * FROM dbo.Cu_Invoice_determinant  chg
	WHERE
        chg.ubm_service_code IS NOT NULL
          AND chg.ubm_service_type_id IS NULL
          AND commodity_Type_id >0 AND commodity_Type_id <290
	ORDER BY Cu_invoice_id DESC
	
	SELECT * FROM Cu_Invoice WHERE Cu_Invoice_iD IN (6918866, 6793409, 3912353)

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
HG			Hari

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
HG			05/16/2011	Comments header added
						-- MAINT-547 fixes the code to return un mapped services from determinant / charges
						-- DISTINCT clause replaced by GROUP BY clause
						-- NOLOCK hints removed
						-- unused @MyAccountId parameter has been removed

******/
CREATE PROCEDURE [dbo].[cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes]
( @cu_invoice_id INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Unmapped_Services TABLE
		( Ubm_Service_Type_Id INT
		,Ubm_Service_Code VARCHAR(200) )

      INSERT INTO
            @Unmapped_Services
            (
             Ubm_Service_Type_Id
            ,Ubm_Service_Code )
            SELECT
                  cd.ubm_service_type_id
                 ,cd.ubm_service_code
            FROM
                  dbo.CU_Invoice_Determinant cd
            WHERE
                  cd.cu_invoice_id = @cu_invoice_id
                  AND cd.ubm_service_code IS NOT NULL
                  AND cd.ubm_service_type_id IS NULL
			GROUP BY
                  cd.ubm_service_type_id
                 ,cd.ubm_service_code

      INSERT INTO
            @Unmapped_Services
            (
             Ubm_Service_Type_Id
            ,Ubm_Service_Code )
            SELECT
                  chg.ubm_service_type_id
                 ,chg.ubm_service_code
            FROM
                  dbo.CU_INVOICE_CHARGE chg
            WHERE
                  chg.cu_invoice_id = @cu_invoice_id
                  AND chg.ubm_service_code IS NOT NULL
                  AND chg.ubm_service_type_id IS NULL
			GROUP BY
                  chg.ubm_service_type_id
                 ,chg.ubm_service_code
            ORDER BY
                  chg.ubm_service_code

		SELECT
			Ubm_Service_Type_id
			,Ubm_Service_Code
		FROM
			@Unmapped_Services
		GROUP BY
			Ubm_Service_Type_id
			,Ubm_Service_Code		
		ORDER BY
			Ubm_Service_Code

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceDeterminant_GetForInvoiceUnmappedServiceCodes] TO [CBMSApplication]
GO