SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******                        
Name:                        
        Trade.Deal_Total_Hedge_Volume_Merge                      
                        
Description:                        
          
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Group_Name   VARCHAR(100)  
    @Client_Id   INT  
    @RM_Group_Type_Cd INT  
    @User_Info_Id  INT     
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
   
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
       
                       
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-11-16     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [Trade].[Deal_Total_Hedge_Volume_Merge]
     (
         @Deal_Ticket_Id INT
         , @Trade_tvp_Deal_Total_Hedge_Volume AS Trade.tvp_Deal_Total_Hedge_Volume READONLY
         , @Uom_Id INT
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        MERGE INTO Trade.Deal_Total_Hedge_Volume AS tgt
        USING
        (   SELECT
                @Deal_Ticket_Id AS Deal_Ticket_Id
                , Deal_Month
                , Total_Volume
                , @Uom_Id AS Uom_Type_Id
                , Client_Generated_Price
                , Trigger_Target_Price
                , Client_Generated_Fixed_Dt
            FROM
                @Trade_tvp_Deal_Total_Hedge_Volume) AS src
        ON tgt.Deal_Ticket_Id = src.Deal_Ticket_Id
           AND  tgt.Deal_Month = src.Deal_Month
        WHEN MATCHED THEN UPDATE SET
                              tgt.Total_Volume = src.Total_Volume
                              , tgt.Uom_Type_Id = src.Uom_Type_Id
                              , tgt.Updated_User_Id = @User_Info_Id
                              , tgt.Last_Change_Ts = GETDATE()
                              , tgt.Client_Generated_Price = src.Client_Generated_Price
                              , tgt.Trigger_Target_Price = src.Trigger_Target_Price
                              , tgt.Client_Generated_Fixed_Dt = src.Client_Generated_Fixed_Dt
        WHEN NOT MATCHED BY TARGET THEN INSERT (Deal_Ticket_Id
                                                , Deal_Month
                                                , Total_Volume
                                                , Uom_Type_Id
                                                , Created_User_Id
                                                , Created_Ts
                                                , Updated_User_Id
                                                , Last_Change_Ts
                                                , Client_Generated_Price
                                                , Trigger_Target_Price
                                                , Client_Generated_Fixed_Dt)
                                        VALUES
                                            (@Deal_Ticket_Id
                                             , src.Deal_Month
                                             , src.Total_Volume
                                             , src.Uom_Type_Id
                                             , @User_Info_Id
                                             , GETDATE()
                                             , @User_Info_Id
                                             , GETDATE()
                                             , src.Client_Generated_Price
                                             , src.Trigger_Target_Price
                                             , src.Client_Generated_Fixed_Dt);


    END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Total_Hedge_Volume_Merge] TO [CBMSApplication]
GO
