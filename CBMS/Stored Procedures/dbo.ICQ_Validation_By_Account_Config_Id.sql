SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
Name:      
 dbo.ICQ_Validation_By_Account_Config_Id    
     
Description:      
     
Input Parameters:      
    Name								DataType       Default    Description      
-----------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_id	INT
@Log_Message							NVARCHAR(500)
@User_Info_Id							INT 
     
 Output Parameters:      
 Name      Datatype Default   Description      
-------------------------------------------------------------------------      
     
 Usage Examples:    
------------------------------------------------------------      
 EXEC dbo.ICQ_Validation_By_Account_Config_Id 553   
  
        
Author Initials:      
 Initials Name    
------------------------------------------------------------    
 PR  Pradip Rajput
     
 Modifications :      
 Initials   Date				Modification      
------------------------------------------------------------      
 PR			2017-03-30			Created SP  
 
******/
CREATE PROCEDURE [dbo].[ICQ_Validation_By_Account_Config_Id]
    (
        @Invoice_Collection_Account_Config_id INT
        , @Sort_Column VARCHAR(50) = 'ICQ_Id'
        , @Sort_Order VARCHAR(10) = 'Desc'
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @SQL_Stat VARCHAR(8000);

        DECLARE
            @id INT = @Invoice_Collection_Account_Config_id
            , @st_dt DATE
            , @end_dt DATE;


        SELECT
            @st_dt = Invoice_Collection_Service_Start_Dt
        FROM
            Invoice_Collection_Account_Config
        WHERE
            Invoice_Collection_Account_Config_Id = @id;


        SELECT
            @end_dt = Invoice_Collection_Service_End_Dt
        FROM
            Invoice_Collection_Account_Config
        WHERE
            Invoice_Collection_Account_Config_Id = @id;


        SELECT
            @SQL_Stat = '
      SELECT
            map.Invoice_Collection_Queue_Id AS ICQ_Id
           ,CONVERT(VARCHAR, ( q.Collection_Start_Dt ), 23) AS Start_dt
           ,CONVERT(VARCHAR, ( q.Collection_End_Dt ), 23) AS End_dt
           ,c.Code_Value AS ICQ_Type
           ,cc.Code_Value AS ICE_reason
           ,mon.Seq_No
           ,cs.Code_Value AS current_status
           ,q.Created_Ts
           ,q.Last_Change_Ts
      FROM
            Invoice_Collection_Queue_Month_Map map
            INNER JOIN Invoice_Collection_Queue q
                  ON q.Invoice_Collection_Queue_Id = map.Invoice_Collection_Queue_Id
            INNER JOIN Account_Invoice_Collection_Month mon
                  ON mon.Account_Invoice_Collection_Month_Id = map.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN Code c
                  ON c.Code_Id = q.Invoice_Collection_Queue_Type_Cd
            LEFT OUTER JOIN code cc
                  ON cc.Code_Id = q.Invoice_Collection_Exception_Type_Cd
            LEFT OUTER JOIN code cs
                  ON cs.Code_Id = q.Status_Cd
      WHERE
            q.Invoice_Collection_Account_Config_Id = ' + CAST(@id AS VARCHAR(50))+ '
            AND mon.Invoice_Collection_Account_Config_Id = ' + CAST(@id AS VARCHAR(50)) + '
      GROUP BY
            map.Invoice_Collection_Queue_Id
           ,mon.Seq_No
           ,q.Collection_Start_Dt
           ,q.Collection_End_Dt
           ,c.Code_Value
           ,cc.Code_Value
           ,cs.Code_Value
           ,q.Created_Ts
           ,q.Last_Change_Ts order by ' + @Sort_Column + ' ' + @Sort_Order;

        EXEC (@SQL_Stat);

        SELECT
            m.Account_Invoice_Collection_Month_Id AS IC_month_id
            , m.Service_Month
            , m.Seq_No
            , c.Code_Value
        FROM
            Account_Invoice_Collection_Month m
            JOIN Account_Invoice_Collection_Frequency f
                ON m.Account_Invoice_Collection_Frequency_Id = f.Account_Invoice_Collection_Frequency_Id
            JOIN Code c
                ON c.Code_Id = f.Invoice_Frequency_Cd
        WHERE
            m.Invoice_Collection_Account_Config_Id = @id
        ORDER BY
            m.Seq_No
            , m.Service_Month;

        --------------------------------------  


        SELECT
            cism.CU_INVOICE_ID
            , CONVERT(VARCHAR, MIN(cism.SERVICE_MONTH), 23) + ' To ' + CONVERT(VARCHAR, MAX(cism.SERVICE_MONTH), 23) service_months
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON icac.Account_Id = cism.Account_ID
            INNER JOIN dbo.CU_INVOICE ci
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_id
            AND ISNULL(cism.SERVICE_MONTH, '') <> ''
            AND (   (cism.Begin_Dt BETWEEN @st_dt
                                   AND     @end_dt)
                    OR  (cism.End_Dt BETWEEN @st_dt
                                     AND     @end_dt))
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED
        ORDER BY
            cism.Begin_Dt
            , cism.End_Dt
            , cism.CU_INVOICE_ID;


        SELECT
            *
        FROM
            Account_Invoice_Collection_Month_Cu_Invoice_Map map
            JOIN Account_Invoice_Collection_Month mon
                ON mon.Account_Invoice_Collection_Month_Id = map.Account_Invoice_Collection_Month_Id
        WHERE
            mon.Invoice_Collection_Account_Config_Id = @id;






    END;

    ;
GO
GRANT EXECUTE ON  [dbo].[ICQ_Validation_By_Account_Config_Id] TO [CBMSApplication]
GO
