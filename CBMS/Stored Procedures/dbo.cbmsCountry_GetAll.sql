SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 Get_Clients_By_State_Vendor
   
DESCRIPTION:  
  Get Client names based on State / Vendor
  
INPUT PARAMETERS:  
 Name			DataType  Default Description  
------------------------------------------------------------  
 @MyAccountId		INT
 @CountryId			INT 
 @CountryName	  VARCHAR(200)
 @Start_Index		INT
 @End_Index			INT           
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  

 EXEC dbo.cbmsCountry_GetAll 7347,@CountryName='U',@Start_Index=1,@End_Index=20

 
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
  
  
MODIFICATIONS:  
 Initials Date		Modification  
------------------------------------------------------------  

******/
CREATE PROCEDURE [dbo].[cbmsCountry_GetAll]
    (
        @MyAccountId INT
        , @CountryId INT = NULL
        , @CountryName VARCHAR(200) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN;
        WITH Cte_County
        AS (
               SELECT
                    COUNTRY_ID
                    , COUNTRY_NAME
                    , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                       c.COUNTRY_ID)
                    , Total_Rows = COUNT(1) OVER ()
               FROM
                    COUNTRY c
               WHERE
                    COUNTRY_ID = ISNULL(@CountryId, COUNTRY_ID)
                    AND COUNTRY_NAME LIKE '%' + ISNULL(@CountryName, COUNTRY_NAME) + '%'
           )
        SELECT
            cntry.COUNTRY_ID
            , cntry.COUNTRY_NAME
        FROM
            Cte_County cntry
        WHERE
            cntry.Row_Num BETWEEN @Start_Index
                          AND     @End_Index
        ORDER BY
            cntry.COUNTRY_NAME;

    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsCountry_GetAll] TO [CBMSApplication]
GO
