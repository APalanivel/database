SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[RM_Deal_Ticket_Volume_Sel_By_Site_Id]  
     
DESCRIPTION: 
	To Get Sum of Rm Deal Ticket Hedge Volume for Given Site Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Site_Id		INT			  		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
    
	EXEC dbo.RM_Deal_Ticket_Volume_Sel_By_Site_Id  128
	
	EXEC dbo.RM_Deal_Ticket_Volume_Sel_By_Site_Id  17256

AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			29-JUN-10	CREATED 
	RR			27-05-2019	GRM - Modified scripts to refer new schema  
  
*/  

CREATE PROCEDURE [dbo].[RM_Deal_Ticket_Volume_Sel_By_Site_Id] ( @Site_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            vol.Deal_Ticket_Id
           ,SUM(vol.Total_Volume) AS Hedge_Volume
      FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
            INNER JOIN Core.Client_Hier ch
                  ON vol.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            ch.Site_Id = @Site_Id
      GROUP BY
            vol.Deal_Ticket_Id
      HAVING
            SUM(vol.Total_Volume) > 0

END

GO
GRANT EXECUTE ON  [dbo].[RM_Deal_Ticket_Volume_Sel_By_Site_Id] TO [CBMSApplication]
GO
