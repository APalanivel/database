SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
            
Name:   
	dbo.EC_Calc_Val_Meter_Attribute_Upd   
              
Description:              
			To update  Calc Val meter attribute configuration
              
 Input Parameters:              
    Name						DataType		Default		Description                
---------------------------------------------------------------------------------
	@EC_Calc_Val_Id				INT
    @EC_Meter_Attribute_Id		INT
    @EC_Meter_Attribute_Value	NVARCHAR(255)
    @User_Info_Id				INT
    
Output Parameters:                    
    Name						DataType		Default		Description                
---------------------------------------------------------------------------------
              
Usage Examples:                  
---------------------------------------------------------------------------------

	SELECT TOP 10 a.EC_Invoice_Sub_Bucket_Master_Id,a.Bucket_Master_Id,a.State_Id,b.Commodity_Id
	FROM dbo.EC_Invoice_Sub_Bucket_Master a JOIN dbo.Bucket_Master b ON a.Bucket_Master_Id = b.Bucket_Master_Id
	SELECT TOP 10 * FROM dbo.EC_Meter_Attribute
	
	BEGIN TRANSACTION
		DECLARE @EC_Calc_Val_Id INT,@EC_Calc_Val_Id_Out INT 
		SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'
		EXEC dbo.EC_Calc_Val_Ins  73,290,'Test_Calc_Val_Ins',329,15,101031,101017,5,101030,101018,5,100957,16,@EC_Calc_Val_Id_Out output
		SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'
		SELECT @EC_Calc_Val_Id = EC_Calc_Val_Id FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'
		SELECT * FROM dbo.EC_Calc_Val_Meter_Attribute WHERE EC_Calc_Val_Id = @EC_Calc_Val_Id
		EXEC dbo.EC_Calc_Val_Meter_Attribute_Ins @EC_Calc_Val_Id, 29, 'Test_Calc_Val_Meter_Attribute_Ins', 16
		SELECT * FROM dbo.EC_Calc_Val_Meter_Attribute WHERE EC_Calc_Val_Id = @EC_Calc_Val_Id
		EXEC dbo.EC_Calc_Val_Meter_Attribute_Upd @EC_Calc_Val_Id, 29, 'Test_Calc_Val_Meter_Attribute_Updated', 16
		SELECT * FROM dbo.EC_Calc_Val_Meter_Attribute WHERE EC_Calc_Val_Id = @EC_Calc_Val_Id
	ROLLBACK TRANSACTION
   
	
Author Initials:              
    Initials	Name              
---------------------------------------------------------------------------------
	NR			Narayana Reddy               
 
Modifications:              
	Initials    Date		Modification              
---------------------------------------------------------------------------------
    NR			2015-05-13	Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Calc_Val_Meter_Attribute_Upd]
      ( 
       @EC_Calc_Val_Id INT
      ,@EC_Meter_Attribute_Id INT
      ,@EC_Meter_Attribute_Value NVARCHAR(255)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      UPDATE
            ecvma
      SET   
            EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
           ,Update_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      FROM
            dbo.EC_Calc_Val_Meter_Attribute ecvma
      WHERE
            ecvma.EC_Calc_Val_Id = @EC_Calc_Val_Id
            AND ecvma.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
                  
END;


;
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Meter_Attribute_Upd] TO [CBMSApplication]
GO
