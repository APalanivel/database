SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsClient_GetForCEM

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE [dbo].[cbmsClient_GetForCEM]
( 
	@MyAccountId int 
)
AS
BEGIN

--set @MyAccountId = 48

	select c.client_id
		,c.client_name
	from client c
	join client_cem_map ccm on ccm.client_id = c.client_id
	where user_info_id = @MyAccountId
	and c.not_managed = 0
	 order by c.client_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsClient_GetForCEM] TO [CBMSApplication]
GO
