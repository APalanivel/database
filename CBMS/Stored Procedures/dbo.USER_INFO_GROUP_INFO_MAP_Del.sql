SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******    
                       
 NAME: dbo.USER_INFO_GROUP_INFO_MAP_Del              
                        
 DESCRIPTION:    
     To delete colums from  USER_INFO_GROUP_INFO_MAP table.                        
                        
 INPUT PARAMETERS:    
                       
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
 @GROUP_INFO_ID                     INT      
 @User_Info_Id                      INT
                      
 OUTPUT PARAMETERS:    
                             
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------    
             
 BEGIN TRAN      
 SELECT * FROM dbo.USER_INFO_GROUP_INFO_MAP  where GROUP_INFO_ID=223 and  User_Info_Id=39134               
 EXEC  dbo.USER_INFO_GROUP_INFO_MAP_Del @GROUP_INFO_ID=223,@User_Info_Id=39134   
 SELECT * FROM dbo.USER_INFO_GROUP_INFO_MAP where GROUP_INFO_ID=223 and User_Info_Id=39134    
 ROLLBACK               
 
                       
 AUTHOR INITIALS:  
     
 Initials               Name    
---------------------------------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                          
                         
 MODIFICATIONS:  
                         
 Initials               Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 NR                     2014-01-07      Created for RA Admin user management                      
                       
******/ 
 CREATE PROCEDURE [dbo].[USER_INFO_GROUP_INFO_MAP_Del]
      ( 
       @GROUP_INFO_ID INT
      ,@User_Info_Id INT )
 AS 
 BEGIN                      
      SET NOCOUNT ON;            
               
      DELETE FROM
            dbo.USER_INFO_GROUP_INFO_MAP
      WHERE
            GROUP_INFO_ID = @GROUP_INFO_ID
            AND User_Info_Id = @User_Info_Id
                      
 END   


;
GO
GRANT EXECUTE ON  [dbo].[USER_INFO_GROUP_INFO_MAP_Del] TO [CBMSApplication]
GO
