SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_NYMEX_FEED_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@closePrice    	decimal(10,3)	          	
	@highPrice     	decimal(10,3)	          	
	@lowPrice      	decimal(10,3)	          	
	@rmNymexDataId 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE DBO.UPDATE_NYMEX_FEED_P 

@userId varchar(10),
@sessionId varchar(20),
@closePrice decimal(10,3), 
@highPrice decimal(10,3), 
@lowPrice decimal(10,3),
@rmNymexDataId int

AS
set nocount on
update rm_nymex_data
set close_price = @closePrice, high_price = @highPrice, low_price = @lowPrice
where rm_nymex_data_id = @rmNymexDataId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_NYMEX_FEED_P] TO [CBMSApplication]
GO
