
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_USAGE_SEARCH_RESULTS_P
    
Description:
    

Input Parameters:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Account_ID              INT
	@Commodity_Type_ID       INT
	@Unit_Type_ID            INT
	@From_Month            DATETIME 
	@To_Month              DATETIME
	@Not_Managed             BIT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec BUDGET_GET_USAGE_SEARCH_RESULTS_P 158109,291,25,'1/1/2011','12/1/2011'
	exec BUDGET_GET_USAGE_SEARCH_RESULTS_P 158108,290,12,'1/1/2011','12/1/2011'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    RK		Rajesh Kasoju
    AP		Athmaram Pabbathi

MODIFICATIONS:
Initials	Date		    Modification
----------------------------------------------------------------
        	09/21/2010    Modify Quoted Identifier
RK		07/27/2011    Removed @User_ID & @Session_ID parameters and 
				    Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master tables
AP	     08/30/2011    Removed If Else section & select statement for finding commodity_id since its no longer needed and 
				    modifed left outer join to inner join on consumption_unit_conversion table.
AP		09/09/2011    Modified SP to return column Service_Month instead of ServiceMonth
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_USAGE_SEARCH_RESULTS_P]
      ( 
       @Account_ID INT
      ,@Commodity_Type_ID INT
      ,@Unit_Type_ID INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME )
AS 
BEGIN
      SET NOCOUNT ON

	
      SELECT
            cha.Account_ID AS AccountID
           ,cha.Account_Number AS AccountNumber
           ,CUAD.Service_Month
           ,sum(isnull(CUAD.Bucket_Value * CNUC.Conversion_Factor, 0)) AS Usage_Value
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN ( SELECT
                              ch.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Number
                         FROM
                              Core.Client_Hier ch
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                         WHERE
                              cha.Account_Id = @Account_Id
                              AND ch.Site_Not_Managed = 0
                         GROUP BY
                              ch.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Number ) cha
                  ON cuad.Client_Hier_Id = cha.Client_Hier_Id
                     AND cuad.Account_Id = cha.Account_Id
            INNER JOIN dbo.Consumption_Unit_Conversion CNUC
                  ON CNUC.Base_Unit_ID = CUAD.UOM_Type_Id
                     AND CNUC.Converted_Unit_ID = @Unit_Type_ID
      WHERE
            CUAD.Account_ID = @Account_ID
            AND CUAD.Service_Month BETWEEN @From_Month
                                   AND     @To_Month
            AND BM.Commodity_Id = @Commodity_Type_ID
            AND bm.Bucket_Name = 'Total Usage'
      GROUP BY
            cha.Account_ID
           ,cha.Account_Number
           ,CUAD.Service_Month 
							 
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_USAGE_SEARCH_RESULTS_P] TO [CBMSApplication]
GO
