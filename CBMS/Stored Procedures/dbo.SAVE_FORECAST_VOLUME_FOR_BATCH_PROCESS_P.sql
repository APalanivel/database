SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SAVE_FORECAST_VOLUME_FOR_BATCH_PROCESS_P]
   @userId varchar(10)
 , @sessionId varchar(50)
 , @clientId int
 , @forecast_year int
AS 
   set nocount on
   declare @cnt int
    , @asOfDate datetime
	
   select   @cnt = count(1)
   from     rm_forecast_volume 
   where    forecast_year = @forecast_year
            and client_id = @clientId
	
   if ( @cnt = 0 ) 
      begin
         select   @asOfDate = forecast_as_of_date
         from     rm_forecast_volume 
         where    client_id = @clientId

         insert   into rm_forecast_volume 
                  (
                    client_id
                  , forecast_year
                  , forecast_as_of_date
                  )
         values   (
                    @clientId
                  , @forecast_year
                  , @asOfDate
                  )
      end
GO
GRANT EXECUTE ON  [dbo].[SAVE_FORECAST_VOLUME_FOR_BATCH_PROCESS_P] TO [CBMSApplication]
GO
