SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Trade_Comment_Sel
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Trade_Comment_Sel  131852
	Exec Trade.Deal_Ticket_Trade_Comment_Sel  131454
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-04-25  Global Risk Management - Created
	RR			2020-03-02	GRM-1589 Modified to excluded commented user from Comment_Date_Time, as it wont available for client
							generated deals   
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Trade_Comment_Sel]
     (
         @Deal_Ticket_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Tbl_Comments AS TABLE
              (
                  Comment_ID INT
                  , Comment_Text VARCHAR(MAX)
                  , Comment_Date DATETIME
                  , Created_Ts DATETIME
                  , Trade_Id INT
                  , Comment_By VARCHAR(50)
              );

        INSERT INTO @Tbl_Comments
             (
                 Comment_ID
                 , Comment_Text
                 , Comment_Date
                 , Trade_Id
                 , Comment_By
             )
        SELECT
            -1 AS Comment_ID
            , chws.Workflow_Status_Comment AS Comment_Text
            , chws.Last_Change_Ts
            , txns.Cbms_Trade_Number AS Trade_Id
            , txns.Assignee
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status txns
                ON chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = txns.Deal_Ticket_Client_Hier_Workflow_Status_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Workflow_Status_Comment IS NOT NULL
            AND chws.Workflow_Status_Comment <> ''
            AND chws.Trade_Month IS NOT NULL
        GROUP BY
            chws.Workflow_Status_Comment
            , chws.Last_Change_Ts
            , txns.Cbms_Trade_Number
            , txns.Assignee;



        SELECT
            Comment_ID
            , Comment_Text
            --,Comment_Date
            , CASE WHEN Comment_By IS NOT NULL THEN
                       REPLACE(CONVERT(VARCHAR(20), Comment_Date, 106), ' ', '-') + ' at '
                       + LTRIM(RIGHT(CONVERT(VARCHAR(20), Comment_Date, 100), 7)) + ' EST by ' + Comment_By
                  WHEN Comment_By IS NULL THEN
                      REPLACE(CONVERT(VARCHAR(20), Comment_Date, 106), ' ', '-') + ' at '
                      + LTRIM(RIGHT(CONVERT(VARCHAR(20), Comment_Date, 100), 7)) + ' EST '
              END AS Comment_Date_Time
            , REPLACE(CONVERT(VARCHAR(20), Comment_Date, 106), ' ', '-') AS Comment_Date
            , Trade_Id
            , Comment_Date AS Comment_Ts
        FROM
            @Tbl_Comments
        ORDER BY
            CAST(Comment_Date AS DATETIME2) DESC;

    END;



GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Trade_Comment_Sel] TO [CBMSApplication]
GO
