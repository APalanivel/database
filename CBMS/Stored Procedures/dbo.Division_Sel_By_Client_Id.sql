SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
Name:  
    dbo.Division_Sel_By_Client_Id 
 
Description:  
    Used for returning the list of divisions for the given filter criteria.
 
Input Parameters:  
    Name			    DataType	 Default Description  
------------------------------------------------------------------------  
    @Client_Id		    INT				
    @Divisions			Varchar(200)		 NULL
    @splitSign			Varchar(10)			 
 
Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
Usage Examples:
------------------------------------------------------------  
    EXECUTE dbo.Division_Sel_By_Client_Id  
      @Client_Id = 170
     ,@Divisions = 'Corporate Offices,Automotive'
     ,@splitSign = ',' 
 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AS		  Arun Skaria
 
 Modifications :  
 Initials		Date			Modification  
------------------------------------------------------------  
 AS				13-Mar-2014		Created
******/
CREATE PROCEDURE dbo.Division_Sel_By_Client_Id
      (
       @Client_Id INT
      ,@Divisions VARCHAR(200) = NULL
      ,@splitSign VARCHAR(10) = ',' )
AS
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE @Sitegroup_Type_cd INT

      SELECT
            @Sitegroup_Type_cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cd.Code_Value = 'Division'
            AND cs.Codeset_Name = 'SiteGroup_Type'
  
  
      SELECT
            sgrp.Sitegroup_Id
           ,segments
      FROM
            ufn_split(@Divisions, @splitSign) div
            LEFT JOIN dbo.Sitegroup sgrp
                  ON div.segments = sgrp.Sitegroup_Name
                     AND sgrp.Client_ID = @Client_Id
                     AND sgrp.Sitegroup_Type_cd = @Sitegroup_Type_cd
      WHERE
            div.segments != ''  
 
  
END






;
GO
GRANT EXECUTE ON  [dbo].[Division_Sel_By_Client_Id] TO [CBMSApplication]
GO
