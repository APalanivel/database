SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE       PROCEDURE [dbo].[cbmsBudgets_Save]
	( @MyAccountID int = null
	, @budget_id int = null
	, @client_id int = null
	, @budget_title varchar(50) = null
	, @date_created datetime = null
	, @budget_start_year int = null
	, @budget_start_month int = null
	, @user_info_id int = null
	, @commodity_type_id int = null
	, @cd_account_factor decimal (32,16) = null
	, @cbms_image_id int = null
	, @conversion_factor_chf decimal (32,16) = null
	, @conversion_factor_can decimal (32,16) = null
	, @conversion_factor_eur decimal (32,16) = null
	, @conversion_factor_gbp decimal (32,16) = null
	, @conversion_factor_pesos decimal (32,16) = null
	, @report_date datetime = null

	)

AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @budget_id


	if @this_id is null
	begin

		insert into budgets
			( client_id
			, budget_title
			, date_created
			, budget_start_year
			, budget_start_month
			, user_info_id
			, commodity_type_id
			, cd_account_factor
			, cbms_image_id
			, report_date
			)
		 values
			( @client_id
			, @budget_title
			, @date_created
			, @budget_start_year
			, @budget_start_month
			, @user_info_id
			, @commodity_type_id
			, @cd_account_factor
			, @cbms_image_id
			, @report_date
			)

		set @this_id = @@IDENTITY
		select @@IDENTITY as budget_id
		
		insert into budget_currency_map	(budget_id, currency_unit_id, conversion_factor)
		SELECT @this_id, currency_unit_id, @conversion_factor_chf  FROM currency_unit where currency_unit_name = 'CHF'

		insert into budget_currency_map	(budget_id, currency_unit_id, conversion_factor)
		SELECT @this_id, currency_unit_id, @conversion_factor_can  FROM currency_unit where currency_unit_name = 'CAN'

		insert into budget_currency_map	(budget_id, currency_unit_id, conversion_factor)
		SELECT @this_id, currency_unit_id, @conversion_factor_eur  FROM currency_unit where currency_unit_name = 'EUR'

		insert into budget_currency_map	(budget_id, currency_unit_id, conversion_factor)
		SELECT @this_id, currency_unit_id, @conversion_factor_gbp  FROM currency_unit where currency_unit_name = 'GBP'

		insert into budget_currency_map	(budget_id, currency_unit_id, conversion_factor)
		SELECT @this_id, currency_unit_id, @conversion_factor_pesos  FROM currency_unit where currency_unit_name = 'PESOS'



	end
	else
	begin

		   update budgets
		      set client_id = @client_id
			, budget_title = @budget_title
			, date_created = @date_created
			, user_info_id = @user_info_id
			, budget_start_year = @budget_start_year 
			, budget_start_month = @budget_start_year 
			, cd_account_factor = @cd_account_factor
			, cbms_image_id = @cbms_image_id
			, report_date = @report_date
		    where budget_id = @this_id

	end

	set nocount off

	exec cbmsBudgets_Get @this_id


END


SET QUOTED_IDENTIFIER ON






GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_Save] TO [CBMSApplication]
GO
