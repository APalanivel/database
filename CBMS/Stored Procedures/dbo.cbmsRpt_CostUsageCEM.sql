SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsRpt_CostUsageCEM

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsRpt_CostUsageCEM]
(
	@MyAccountId int
)
AS
begin

	select distinct ui.username, ui.user_info_id, ui.first_name + ' ' + ui.last_name as [name]
	from client c
	join division d on d.client_id = c.client_id
	join site s on s.division_id = d.division_id
	join client_cem_map ccm on ccm.client_id = c.client_id
	join user_info ui on (ui.user_info_id = ccm.user_info_id and ui.is_history = 0)

end
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_CostUsageCEM] TO [CBMSApplication]
GO
