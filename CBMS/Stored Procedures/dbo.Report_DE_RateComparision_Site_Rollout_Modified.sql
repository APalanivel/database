SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME: [DBO].[Report_DE_RateComparision_Site_Rollout__Modified]    
       
DESCRIPTION:  
  
 New site Rollout Query - Built to meet request from Rates Managers    
    to track new site rollouts and when rollout rate comparisons are performed  by analysts.    
   
  
INPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
  
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION     
------------------------------------------------------------            
  
  
USAGE EXAMPLES:       
------------------------------------------------------------    
  
EXEC Report_DE_RateComparision_Site_Rollout_Modified  
   
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
AKR   Ashok Kumar Raju  
KVK   vinay K 
SP	  Sandeep Pigilam
  
MODIFICATIONS  
INITIALS DATE   MODIFICATION  
------------------------------------------------------------            
AKR   2012-05-30   Cloned from View dbo.lec_ratecomparisons_new_site_rollout  
AKR   2012-10-09   MOdified the logic to get RCDone for no rate comparisons at all  
AKR   2014-09-10   Modified the code remove Utility_Analyst_Map View
KVK	  2014-12-15   Modifeid to have new documents schema
sp	  2015-02-13   Enhance-213,Added Order by to the final select statement
LEC	  2017-06-29   Modified report to exclude duplications of inaccurate entity_audit.modified_date records.
lec   2018-03-26   Modified report - removed Commercial Type exclusion from filter. 
				   Modified to exclude Demo client types.
				   Re-arranged columns, and added site_name client_id, rate_id, vendor_id, rate comparison created by
				   Re-named several columns
lec   2018-04-23   Added Meter_ID
				   
*/  
CREATE PROCEDURE [dbo].[Report_DE_RateComparision_Site_Rollout_Modified]
AS 
BEGIN  
  
      SET NOCOUNT ON;         
          
      DECLARE
            @SSO_Document_Table_Type_Id INT
           ,@Account_Table_Type_Id INT
           ,@Rate_Com_Document_Category_Id INT
           ,@Commercial_Type_Id INT
           ,@Central_Plains INT
           ,@Midwest INT
           ,@Northeast INT
           ,@Southeast INT
           ,@West INT        
      
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )  
                    
      CREATE TABLE #Meters
            ( 
             Meter_Id INT PRIMARY KEY )        
              
      SELECT
            @SSO_Document_Table_Type_Id = MAX(CASE WHEN en.ENTITY_NAME = 'SSO_DOCUMENT'
                                                        AND en.ENTITY_DESCRIPTION = 'Table_Type' THEN en.ENTITY_ID
                                              END)
           ,@Account_Table_Type_Id = MAX(CASE WHEN en.ENTITY_NAME = 'ACCOUNT_TABLE'
                                                   AND en.ENTITY_DESCRIPTION = 'Table_Type' THEN en.ENTITY_ID
                                         END)
           ,@Commercial_Type_Id = MAX(CASE WHEN en.ENTITY_NAME = 'Commercial'
                                                AND en.ENTITY_DESCRIPTION = 'Client Type' THEN en.ENTITY_ID
                                      END)
      FROM
            dbo.ENTITY en
            
      SELECT
            @Rate_Com_Document_Category_Id = cdc.Client_Document_Category_Id
      FROM
            dbo.Client_Document_Category AS cdc
      WHERE
            cdc.Client_Id = -1 --global category
            AND cdc.Category_Name = 'Rate Comparison'
	  
	  
	  
	  
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market'        
         
      SELECT
            @Central_Plains = MAX(CASE WHEN reg.REGION_NAME = 'CP' THEN reg.REGION_ID
                                  END)
           ,@Midwest = MAX(CASE WHEN reg.REGION_NAME = 'MW' THEN reg.REGION_ID
                           END)
           ,@Northeast = MAX(CASE WHEN reg.REGION_NAME = 'NE' THEN reg.REGION_ID
                             END)
           ,@Southeast = MAX(CASE WHEN reg.REGION_NAME = 'SE' THEN reg.REGION_ID
                             END)
           ,@West = MAX(CASE WHEN reg.REGION_NAME = 'W' THEN reg.REGION_ID
                        END)
      FROM
            dbo.REGION reg
      
      
      INSERT      INTO #Meters
                  ( 
                   Meter_Id )
                  SELECT
                        cha.Meter_Id
                  FROM
                        dbo.rc_rate_comparison rc
                        INNER JOIN core.Client_Hier_Account cha
                              ON rc.METER_ID = cha.Meter_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                       INNER JOIN entity ct ON ct.ENTITY_ID = ch.Client_Type_Id
                        INNER JOIN dbo.SITE s
                              ON s.SITE_ID = ch.Site_Id
                        LEFT JOIN dbo.entity e1
                              ON cha.Account_Service_level_Cd = e1.entity_id
                        LEFT JOIN dbo.entity e2
                              ON cha.Account_Invoice_Source_Cd = e2.entity_id
                        JOIN dbo.vendor v
                              ON cha.Account_Vendor_Id = v.vendor_id
                        LEFT JOIN dbo.user_info ui
                              ON rc.created_by = ui.user_info_id
                        LEFT JOIN dbo.user_info ui1
                              ON rc.reviewed_by = ui1.user_info_id
                        LEFT JOIN dbo.utility_detail ud
                              ON v.vendor_id = ud.vendor_id
                        LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                                    INNER JOIN @Group_Legacy_Name GI
                                          ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                                    ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
                        LEFT JOIN dbo.user_info ui2
                              ON udam.Analyst_id = ui2.user_info_id
                        LEFT JOIN dbo.entity e3
                              ON e3.entity_id = rc.status_type_id
                        LEFT JOIN dbo.entity e4
                              ON e4.entity_id = rc.result_type_id
                        LEFT JOIN dbo.sso_document ssodoc
                              ON ssodoc.cbms_image_id = rc.cbms_image_id
                        LEFT JOIN dbo.Client_Document_Category_Map AS cdcm
                              ON cdcm.SSO_DOCUMENT_ID = ssodoc.SSO_DOCUMENT_ID
                        LEFT JOIN dbo.Client_Document_Category AS cdc
                              ON cdc.Client_Document_Category_Id = cdcm.Client_Document_Category_Id
                        LEFT JOIN dbo.entity_audit ea1
                              ON ea1.entity_identifier = ssodoc.sso_document_id
                        LEFT JOIN dbo.user_info ui4
                              ON ui4.user_info_id = ea1.user_info_id
                        LEFT JOIN dbo.entity_audit ea2
                              ON ea2.entity_identifier = cha.Account_Id
                        LEFT JOIN user_info uic ON uic.USER_INFO_ID = rc.CREATED_BY
                  WHERE
                        ( cdcm.Client_Document_Category_Id = @Rate_Com_Document_Category_Id
                          AND ( ea1.audit_type = 1
                                AND ea1.entity_id = @SSO_Document_Table_Type_Id ) )
                        AND ( ea2.audit_type = 1
                              AND ea2.entity_id = @Account_Table_Type_Id )
                        AND cha.account_Not_Managed = 0
                        --AND ch.Client_Type_Id <> @Commercial_Type_Id
                        AND ct.ENTITY_NAME != 'demo'
                        AND ch.Region_ID IN ( @Central_Plains, @Midwest, @Northeast, @Southeast, @West )
                  GROUP BY
                        cha.Meter_Id  
                          
      SELECT
           
            CAST(( CONVERT (VARCHAR(10), MIN(ea2.modified_date), 101) ) AS DATETIME) AS [Account Creation Date]
           ,CAST (( CONVERT(CHAR(10), s.rollout_email_date, 101) ) AS DATETIME) AS [Rollout Date]
           ,ch.client_name AS [Client]
           ,ct.ENTITY_NAME AS [ClientType]
           ,ch.Site_Type_Name AS [SiteType]
           ,ch.Region_Name AS [Region]
            ,cha.Meter_State_Name AS [State]
           ,cha.Meter_City AS [City]
		   ,ch.site_name [Site Name]
		   ,com.Commodity_Name AS [Commodity]
			,v.VENDOR_NAME AS [Vendor name]
           ,cha.Rate_Name AS [Rate name]
           ,cha.Display_Account_Number AS [Account Number]
           ,cha.Meter_Number AS [Meter Number]
           ,e1.entity_name AS [Service Level]
           ,ch.client_id [Client ID]
           ,cha.Account_Vendor_Id [Vendor ID]
           ,cha.Rate_Id [Rate ID]
           ,cha.Account_Id AS [Account ID]
           ,cha.Meter_Id AS [Meter_ID]
			,ui2.first_name + SPACE(1) + ui2.last_name AS [Responsible]
           ,CAST(( CONVERT (CHAR(10), ( rollout_email_date + 42 ), 101) ) AS DATETIME) AS [RC Review Deadline]
            ,'yes' AS [RC Done In CBMS?]
             ,CAST (( CONVERT (CHAR(10), rc.creation_date, 101) ) AS DATETIME) AS [CBMS RC Creation Date]
           ,e4.entity_name AS [CBMS Rate Comparison Result]
           --,m.comments AS [CBMS Comments]
           ,uic.FIRST_NAME + ' '+ uic.LAST_NAME [CBMS RC Created By]
          --,rc.RATE_COMPARISON_NAME [CBMS Comparison Name]
      FROM
            dbo.rc_rate_comparison rc
            INNER JOIN core.Client_Hier_Account cha
                  ON rc.METER_ID = cha.Meter_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.METER m
                  ON m.METER_ID = cha.Meter_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY ct
                  ON ch.Client_Type_Id = ct.ENTITY_ID
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.Site_Id
            LEFT JOIN dbo.entity e1
                  ON cha.Account_Service_level_Cd = e1.entity_id
            LEFT JOIN dbo.entity e2
                  ON cha.Account_Invoice_Source_Cd = e2.entity_id
            JOIN dbo.vendor v
                  ON cha.Account_Vendor_Id = v.vendor_id
            LEFT JOIN dbo.user_info ui
                  ON rc.created_by = ui.user_info_id
            LEFT JOIN dbo.user_info ui1
                  ON rc.reviewed_by = ui1.user_info_id
            LEFT JOIN dbo.utility_detail ud
                  ON v.vendor_id = ud.vendor_id
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            LEFT JOIN dbo.user_info ui2
                  ON udam.Analyst_id = ui2.user_info_id
            LEFT JOIN dbo.entity e3
                  ON e3.entity_id = rc.status_type_id
            LEFT JOIN dbo.entity e4
                  ON e4.entity_id = rc.result_type_id
            LEFT JOIN dbo.sso_document ssodoc
                  ON ssodoc.cbms_image_id = rc.cbms_image_id
            LEFT JOIN dbo.Client_Document_Category_Map AS cdcm
                  ON cdcm.SSO_DOCUMENT_ID = ssodoc.SSO_DOCUMENT_ID
            LEFT JOIN dbo.Client_Document_Category AS cdc
                  ON cdc.Client_Document_Category_Id = cdcm.Client_Document_Category_Id
            LEFT JOIN dbo.entity_audit ea1
                  ON ea1.entity_identifier = ssodoc.sso_document_id
            LEFT JOIN dbo.user_info ui4
                  ON ui4.user_info_id = ea1.user_info_id
            LEFT JOIN dbo.entity_audit ea2
                  ON ea2.entity_identifier = cha.Account_Id
                  LEFT JOIN user_info uic ON uic.USER_INFO_ID = rc.CREATED_BY
      WHERE
            ( cdcm.Client_Document_Category_Id = @Rate_Com_Document_Category_Id
              AND ( ea1.audit_type = 1
                    AND ea1.entity_id = @SSO_Document_Table_Type_Id ) )
            AND ( ea2.audit_type = 1
                  AND ea2.entity_id = @Account_Table_Type_Id )
            AND cha.account_Not_Managed = 0
           -- AND ch.Client_Type_Id <> @Commercial_Type_Id
           AND ct.ENTITY_NAME != 'demo'
            AND ch.Region_ID IN ( @Central_Plains, @Midwest, @Northeast, @Southeast, @West )
      GROUP BY
            --MIN(ea2.modified_date)
           
           s.rollout_email_date
           ,cha.Account_Vendor_Id
           ,cha.Rate_Id
           ,ch.site_name
           ,ch.client_name
           ,ch.Client_Id
           ,ct.ENTITY_NAME
           ,ch.Site_Type_Name
           ,cha.Meter_City
           ,cha.Meter_State_Name
           ,ch.Region_Name
           ,com.Commodity_Name
           ,cha.Display_Account_Number
           ,cha.Account_Id
           ,cha.Meter_Id
           ,cha.Meter_Number
           ,v.VENDOR_NAME
           ,cha.Rate_Name
           ,e1.entity_name
           ,ui2.first_name
           ,ui2.last_name
           ,rollout_email_date
           ,e4.entity_name
           --,m.comments
           ,CONVERT (CHAR(10), rc.creation_date, 101)
            ,uic.FIRST_NAME + ' '+ uic.LAST_NAME 
             --,rc.RATE_COMPARISON_NAME 
      UNION
      SELECT
            
            CAST(( CONVERT (VARCHAR(10), MIN(ea2.modified_date), 101) ) AS DATETIME) AS [Account Creation Date]
           ,CAST (( CONVERT(CHAR(10), s.rollout_email_date, 101) ) AS DATETIME) AS [Rollout Date]
           ,ch.client_name AS [Client]
           ,ct.ENTITY_NAME AS [ClientType]
           ,ch.Site_Type_Name AS [SiteType]
           ,ch.Region_Name AS [Region]
            ,cha.Meter_State_Name AS [State]
           ,cha.Meter_City AS [City]
		   ,ch.site_name [Site Name]
		   ,com.Commodity_Name AS [Commodity]
			,v.VENDOR_NAME AS [Vendor name]
           ,cha.Rate_Name AS [Rate name]
           ,cha.Display_Account_Number AS [Account Number]
           ,cha.Meter_Number AS [Meter Number]
           ,e1.entity_name AS [Service Level]
           ,ch.client_id [Client ID]
           ,cha.Account_Vendor_Id [Vendor ID]
           ,cha.Rate_Id [Rate ID]
           ,cha.Account_Id AS [Account ID]
           ,cha.meter_id AS [Meter_ID]
			,ui2.first_name + SPACE(1) + ui2.last_name AS [Responsible]
           ,CAST(( CONVERT (CHAR(10), ( rollout_email_date + 42 ), 101) ) AS DATETIME) AS [RC Review Deadline]
            ,'yes' AS [RC Done In CBMS?]
             ,CAST (( CONVERT (CHAR(10), rc.creation_date, 101) ) AS DATETIME) AS [CBMS RC Creation Date]
           ,e4.entity_name AS [CBMS Rate Comparison Result]
           --,m.comments AS [CBMS Comments]
            ,uic.FIRST_NAME + ' '+ uic.LAST_NAME [CBMS RC Created By]
             --,rc.RATE_COMPARISON_NAME [CBMS Comparison Name]
      FROM
            dbo.rc_rate_comparison rc
            INNER JOIN core.Client_Hier_Account cha
                  ON rc.METER_ID = cha.Meter_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.METER m
                  ON m.METER_ID = cha.Meter_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY ct
                  ON ch.Client_Type_Id = ct.ENTITY_ID
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.Site_Id
            LEFT JOIN dbo.entity e1
                  ON cha.Account_Service_level_Cd = e1.entity_id
            LEFT JOIN dbo.entity e2
                  ON cha.Account_Invoice_Source_Cd = e2.entity_id
            JOIN dbo.vendor v
                  ON cha.Account_Vendor_Id = v.vendor_id
            LEFT JOIN dbo.user_info ui
                  ON rc.created_by = ui.user_info_id
            LEFT JOIN dbo.user_info ui1
                  ON rc.reviewed_by = ui1.user_info_id
            LEFT JOIN dbo.utility_detail ud
                  ON v.vendor_id = ud.vendor_id
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            LEFT JOIN dbo.user_info ui2
                  ON udam.Analyst_id = ui2.user_info_id
            LEFT JOIN dbo.entity e3
                  ON e3.entity_id = rc.status_type_id
            LEFT JOIN dbo.entity e4
                  ON e4.entity_id = rc.result_type_id
            LEFT JOIN dbo.sso_document ssodoc
                  ON ssodoc.cbms_image_id = rc.cbms_image_id
            LEFT JOIN dbo.Client_Document_Category_Map AS cdcm
                  ON cdcm.SSO_DOCUMENT_ID = ssodoc.SSO_DOCUMENT_ID
            LEFT JOIN dbo.Client_Document_Category AS cdc
                  ON cdc.Client_Document_Category_Id = cdcm.Client_Document_Category_Id
            LEFT JOIN dbo.entity_audit ea1
                  ON ea1.entity_identifier = ssodoc.sso_document_id
            LEFT JOIN dbo.user_info ui4
                  ON ui4.user_info_id = ea1.user_info_id
            LEFT JOIN dbo.entity_audit ea2
                  ON ea2.entity_identifier = cha.Account_Id
                  LEFT JOIN user_info uic ON uic.USER_INFO_ID = rc.CREATED_BY
                 
      WHERE
            cdcm.Client_Document_Category_Id = @Rate_Com_Document_Category_Id
            AND ( ea2.audit_type = 1
                  AND ea2.entity_id = @Account_Table_Type_Id )
            AND cha.account_Not_Managed = 0
           -- AND ch.Client_Type_Id <> @Commercial_Type_Id
           AND ct.ENTITY_NAME != 'demo'
            AND ch.Region_ID IN ( @Central_Plains, @Midwest, @Northeast, @Southeast, @West )
            AND NOT EXISTS ( SELECT
                              Meter_Id
                             FROM
                              #Meters m1
                             WHERE
                              m.meter_Id = m1.Meter_Id )
      GROUP BY
            --MIN(ea2.modified_date)
           s.rollout_email_date
           ,cha.Account_Vendor_Id
           ,cha.Rate_Id
           ,ch.client_name
           ,ch.client_id
           ,ch.site_name
           ,ct.ENTITY_NAME
           ,ch.Site_Type_Name
           ,cha.Meter_City
           ,cha.Meter_State_Name
           ,ch.Region_Name
           ,com.Commodity_Name
           ,cha.display_account_number
           ,cha.Account_Id
           ,cha.Meter_Id
           ,cha.Meter_Number
           ,v.VENDOR_NAME
           ,cha.Rate_Name
           ,ui2.first_name + SPACE(1) + ui2.last_name
           ,e1.entity_name
           ,rollout_email_date
           ,e4.entity_name
           --,m.comments
           ,CONVERT (CHAR(10), rc.creation_date, 101)
            ,uic.FIRST_NAME + ' '+ uic.LAST_NAME 
             --,rc.RATE_COMPARISON_NAME 
      UNION
      SELECT
            
            CAST(( CONVERT (VARCHAR(10), MIN(ea2.modified_date), 101) ) AS DATETIME) AS [Account Creation Date]
           ,CAST (( CONVERT(CHAR(10), s.rollout_email_date, 101) ) AS DATETIME) AS [Rollout Date]
           ,ch.client_name AS [Client]
           ,ct.ENTITY_NAME AS [ClientType]
           ,ch.Site_Type_Name AS [SiteType]
           ,ch.Region_Name AS [Region]
            ,cha.Meter_State_Name AS [State]
           ,cha.Meter_City AS [City]
		   ,ch.site_name [Site Name]
		   ,com.Commodity_Name AS [Commodity]
			,v.VENDOR_NAME AS [Vendor name]
           ,cha.Rate_Name AS [Rate name]
           ,cha.Display_Account_Number AS [Account Number]
           ,cha.Meter_Number AS [Meter Number]
           ,e1.entity_name AS [Service Level]
           ,ch.client_id [Client ID]
           ,cha.Account_Vendor_Id [Vendor ID]
           ,cha.Rate_Id [Rate ID]
           ,cha.Account_Id AS [Account ID]
           ,cha.meter_id AS [Meter_ID]
			,ui2.first_name + SPACE(1) + ui2.last_name AS [Responsible]
           ,CAST(( CONVERT (CHAR(10), ( rollout_email_date + 42 ), 101) ) AS DATETIME) AS [RC Review Deadline]
            ,'yes' AS [RC Done In CBMS?]
             ,CAST (( CONVERT (CHAR(10), rc.creation_date, 101) ) AS DATETIME) AS [CBMS RC Creation Date]
           ,e4.entity_name AS [CBMS Rate Comparison Result]
           --,m.comments AS [CBMS Comments]
            ,uic.FIRST_NAME + ' '+ uic.LAST_NAME [CBMS RC Created By]
             --,rc.RATE_COMPARISON_NAME [CBMS Comparison Name]
      FROM
            dbo.rc_rate_comparison rc
            INNER JOIN core.Client_Hier_Account cha
                  ON rc.METER_ID = cha.Meter_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.METER m
                  ON m.METER_ID = cha.Meter_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY ct
                  ON ch.Client_Type_Id = ct.ENTITY_ID
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.Site_Id
            LEFT JOIN dbo.entity e1
                  ON cha.Account_Service_level_Cd = e1.entity_id
            LEFT JOIN dbo.entity e2
                  ON cha.Account_Invoice_Source_Cd = e2.entity_id
            JOIN dbo.vendor v
                  ON cha.Account_Vendor_Id = v.vendor_id
            LEFT JOIN dbo.user_info ui
                  ON rc.created_by = ui.user_info_id
            LEFT JOIN dbo.user_info ui1
                  ON rc.reviewed_by = ui1.user_info_id
            LEFT JOIN dbo.utility_detail ud
                  ON v.vendor_id = ud.vendor_id
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            LEFT JOIN dbo.user_info ui2
                  ON udam.Analyst_id = ui2.user_info_id
            LEFT JOIN dbo.entity e3
                  ON e3.entity_id = rc.status_type_id
            LEFT JOIN dbo.entity e4
                  ON e4.entity_id = rc.result_type_id
            LEFT JOIN dbo.sso_document ssodoc
                  ON ssodoc.cbms_image_id = rc.cbms_image_id
            LEFT JOIN dbo.entity_audit ea1
                  ON ea1.entity_identifier = ssodoc.sso_document_id
            LEFT JOIN dbo.user_info ui4
                  ON ui4.user_info_id = ea1.user_info_id
            LEFT JOIN dbo.entity_audit ea2
                  ON ea2.entity_identifier = cha.Account_Id
                  LEFT JOIN user_info uic ON uic.USER_INFO_ID = rc.CREATED_BY
      WHERE
            ssodoc.sso_document_id IS NULL
            AND ( ea2.audit_type = 1
                  AND ea2.entity_id = @Account_Table_Type_Id )
            AND cha.account_Not_Managed = 0
            --AND ch.Client_Type_Id <> @Commercial_Type_Id
            AND ct.ENTITY_NAME != 'demo'
            AND ch.Region_ID IN ( @Central_Plains, @Midwest, @Northeast, @Southeast, @West )
      GROUP BY
            --MIN(ea2.modified_date)
           s.rollout_email_date
           ,cha.Account_Vendor_Id
           ,cha.Rate_Id
           ,ch.client_name
           ,ch.client_id
           ,ch.site_name
           ,ct.ENTITY_NAME
           ,ch.Site_Type_Name
           ,cha.Meter_City
           ,cha.Meter_State_Name
           ,ch.Region_Name
           ,com.Commodity_Name
           ,cha.display_account_number
           ,cha.Account_Id
           ,cha.Meter_Id
           ,cha.Meter_Number
           ,v.VENDOR_NAME
           ,cha.Rate_Name
           ,ui2.first_name + SPACE(1) + ui2.last_name
           ,e1.entity_name
           ,rollout_email_date
           ,e4.entity_name
           --,m.comments
           ,CONVERT (CHAR(10), rc.creation_date, 101)
            ,uic.FIRST_NAME + ' '+ uic.LAST_NAME
             --,rc.RATE_COMPARISON_NAME
      UNION  
    
--********************************no rate comparisons at all    
      SELECT
            
            CAST(( CONVERT (VARCHAR(10), MIN(ea2.modified_date), 101) ) AS DATETIME) AS [Account Creation Date]
           ,CAST (( CONVERT(CHAR(10), s.rollout_email_date, 101) ) AS DATETIME) AS [Rollout Date]
           ,ch.client_name AS [Client]
           ,ct.ENTITY_NAME AS [ClientType]
           ,ch.Site_Type_Name AS [SiteType]
           ,ch.Region_Name AS [Region]
            ,cha.Meter_State_Name AS [State]
           ,cha.Meter_City AS [City]
		   ,ch.site_name [Site Name]
		   ,com.Commodity_Name AS [Commodity]
			,v.VENDOR_NAME AS [Vendor name]
           ,cha.Rate_Name AS [Rate name]
           ,cha.Display_Account_Number AS [Account Number]
           ,cha.Meter_Number AS [Meter Number]
           ,e1.entity_name AS [Service Level]
           ,ch.client_id [Client ID]
           ,cha.Account_Vendor_Id [Vendor ID]
           ,cha.Rate_Id [Rate ID]
           ,cha.Account_Id AS [Account ID]
           ,cha.Meter_Id AS [Meter_ID]
			,ui2.first_name + SPACE(1) + ui2.last_name AS [Responsible]
           ,CAST(( CONVERT (CHAR(10), ( rollout_email_date + 42 ), 101) ) AS DATETIME) AS [RC Review Deadline]
            ,CASE WHEN cha.account_id IN ( SELECT
                                                account_id
                                          FROM
                                                rc_rate_comparison ) THEN 'OtherMeter'
                 ELSE 'No'
            END AS [RC Done in CBMS?]
             ,null AS [CBMS RC Creation Date]
           ,NULL AS [CBMS Rate Comparison Result]
           --,m.comments AS [CBMS Comments]
            ,null [CBMS RC Created By]
             --,rc.RATE_COMPARISON_NAME [CBMS Comparison Name]
           
          
      FROM
            core.Client_Hier_Account cha
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.METER m
                  ON m.METER_ID = cha.Meter_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY ct
                  ON ch.Client_Type_Id = ct.ENTITY_ID
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.Site_Id
            LEFT JOIN dbo.entity e1
                  ON cha.Account_Service_level_Cd = e1.entity_id
            LEFT JOIN dbo.entity e2
                  ON cha.Account_Invoice_Source_Cd = e2.entity_id
            JOIN dbo.vendor v
                  ON cha.Account_Vendor_Id = v.vendor_id
            LEFT JOIN dbo.utility_detail ud
                  ON v.vendor_id = ud.vendor_id
            LEFT JOIN ( dbo.Utility_Detail_Analyst_Map UDAM
                        INNER JOIN @Group_Legacy_Name GI
                              ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID )
                        ON ud.UTILITY_DETAIL_ID = udam.UTILITY_DETAIL_ID
            LEFT JOIN dbo.user_info ui2
                  ON udam.Analyst_id = ui2.user_info_id
            LEFT JOIN dbo.rc_rate_comparison rc
                  ON rc.meter_Id = m.meter_Id
            LEFT JOIN dbo.entity_audit ea2
                  ON ea2.entity_identifier = cha.Account_Id
                  LEFT JOIN user_info uic ON uic.USER_INFO_ID = rc.CREATED_BY
      WHERE
            ( e1.entity_name = 'a'
              OR e1.entity_name = 'b' )
            AND rc.meter_id IS NULL
            AND ( ea2.entity_id = @Account_Table_Type_Id
                  AND ea2.audit_type = 1 )
            AND cha.Account_Not_Managed = 0
            AND ch.Region_ID IN ( @Central_Plains, @Midwest, @Northeast, @Southeast, @West )
           -- AND ch.client_type_id != @Commercial_Type_Id
           AND ct.ENTITY_NAME != 'demo'
      GROUP BY
           -- MIN(ea2.modified_date)
           s.rollout_email_date
           ,cha.Account_Vendor_Id
           ,cha.Rate_Id
           ,ch.client_name
           ,ch.client_id
           ,ch.site_name
           ,ct.ENTITY_NAME
           ,ch.Site_Type_Name
           ,cha.Meter_City
           ,cha.Meter_State_Name
           ,ch.Region_Name
           ,com.Commodity_Name
           ,cha.Display_Account_Number
           ,cha.Account_Id
           ,cha.Meter_Id
           ,cha.Meter_Number
           ,v.VENDOR_NAME
           ,cha.Rate_Name
           ,ui2.first_name + SPACE(1) + ui2.last_name
           ,e1.entity_name
           ,rollout_email_date
           --,m.comments
           ,rc.Account_Id
             --,rc.RATE_COMPARISON_NAME 
      ORDER BY
            [Account Creation Date]
           ,[Client]
           ,cha.Account_Id
           ,[Meter Number]          
END
;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_RateComparision_Site_Rollout_Modified] TO [CBMSApplication]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_RateComparision_Site_Rollout_Modified] TO [CBMSReports]
GO
