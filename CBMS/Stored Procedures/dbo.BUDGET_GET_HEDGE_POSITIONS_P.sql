SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--exec BUDGET_GET_HEDGE_POSITIONS_P '','',190,0,0,'01/01/2007','12/01/2007'
--exec BUDGET_GET_HEDGE_POSITIONS_P '','',190,473,0,'01/01/2007','12/01/2007'
--exec BUDGET_GET_HEDGE_POSITIONS_P '','',190,0,1644,'01/01/2007','12/01/2007'

CREATE   PROCEDURE dbo.BUDGET_GET_HEDGE_POSITIONS_P
	@userId varchar(10),
	@sessionId varchar(20),
	@clientId int,
	@divisionId int,
	@siteId int,
	@fromDate datetime,
	@toDate datetime
	AS
	begin
		set nocount on
	
		if @siteId > 0
		begin
		
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			then CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0			
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			vwSiteName site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=559 --//for entity 'fixed price'
			AND dealTicket.HEDGE_MODE_TYPE_ID=550 --//for entity 'nymex'
			AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID =610 --//for entity 'order executed'
			AND dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = @siteId AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			and site.site_id=dealvolume.SITE_ID AND
	
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	--------------------------- second ---------------
		UNION ALL
	
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			VWSITENAME site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
		  				     from   ENTITY
					  	     where  ENTITY_TYPE=274 AND
					  	 	    ENTITY_NAME IN('weighted strip price','individual month pricing')
					) AND	
	
			dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'trigger'
						) AND	
	
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	
			dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
	
							       from    ENTITY
							       where   ENTITY_TYPE=287 AND
								       ENTITY_NAME IN('fixed','closed')
							    ) AND
	
			dealticket.CLIENT_ID=@clientId AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = @siteId AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			AND dealvolume.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
		
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
	--------------------------- third ---------------
	
		UNION ALL
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_OPTION_DETAILS dealoption,
			ENTITY entity,
			SITE site,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'options'
						) AND	
		
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
	
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
								       from    ENTITY
								       where   ENTITY_TYPE=286 AND
								               ENTITY_NAME IN('order executed')
								    ) AND
			dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID AND
			dealoption.SITE_ID = @siteId AND
			dealoption.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealoption.OPTION_TYPE_ID=entity.ENTITY_ID AND	
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			AND dealoption.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
		end
		else if @divisionId > 0
		begin
			select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			vwSiteName site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=559 --//for entity 'fixed price'
			AND dealTicket.HEDGE_MODE_TYPE_ID=550 --//for entity 'nymex'
			AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID =610 --//for entity 'order executed'
			AND dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			and site.division_id = @divisionId
			and site.site_id=dealvolume.SITE_ID AND
	
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	--------------------------- second ---------------
		UNION ALL
	
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			VWSITENAME site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
		  				     from   ENTITY
					  	     where  ENTITY_TYPE=274 AND
					  	 	    ENTITY_NAME IN('weighted strip price','individual month pricing')
					) AND	
	
			dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'trigger'
						) AND	
	
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	
			dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
	
							       from    ENTITY
							       where   ENTITY_TYPE=287 AND
								       ENTITY_NAME IN('fixed','closed')
							    ) AND
	
			dealticket.CLIENT_ID=@clientId AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			and site.division_id = @divisionId
			AND dealvolume.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
		
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
	--------------------------- third ---------------
	
		UNION ALL
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_OPTION_DETAILS dealoption,
			ENTITY entity,
			SITE site,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'options'
						) AND	
		
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
	
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
								       from    ENTITY
								       where   ENTITY_TYPE=286 AND
								               ENTITY_NAME IN('order executed')
								    ) AND
			dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID AND
			dealoption.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealoption.OPTION_TYPE_ID=entity.ENTITY_ID AND	
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			and site.division_id = @divisionId
			AND dealoption.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
		end
		else --//Client level
		begin
			select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			vwSiteName site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=559 --//for entity 'fixed price'
			AND dealTicket.HEDGE_MODE_TYPE_ID=550 --//for entity 'nymex'
			AND dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID =610 --//for entity 'order executed'
			AND dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			and site.site_id=dealvolume.SITE_ID AND
	
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	--------------------------- second ---------------
		UNION ALL
	
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			VWSITENAME site,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
		  				     from   ENTITY
					  	     where  ENTITY_TYPE=274 AND
					  	 	    ENTITY_NAME IN('weighted strip price','individual month pricing')
					) AND	
	
			dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'trigger'
						) AND	
	
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	
			dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
	
							       from    ENTITY
							       where   ENTITY_TYPE=287 AND
								       ENTITY_NAME IN('fixed','closed')
							    ) AND
	
			dealticket.CLIENT_ID=@clientId AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
			dealvolume.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			AND dealvolume.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
		
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
	--------------------------- third ---------------
	
		UNION ALL
	
		select 	site.site_id,
			dealdetails.MONTH_IDENTIFIER,
			CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			END HEDGE_VOLUME,
			case when SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) <> 0
			THEN CASE entity1.ENTITY_NAME	
			     WHEN 'Daily' THEN CAST( SUM (dealdetails.HEDGE_PRICE *dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR*DATEPART(dd, DATEADD(dd, -(DATEPART(dd, DATEADD(mm, 1, dealdetails.MONTH_IDENTIFIER))),dateadd(mm, 1, dealdetails.MONTH_IDENTIFIER))) ) as decimal(20,3))
			     WHEN 'Monthly' THEN CAST(SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR ) as decimal(20,3))		
			     ELSE CAST( SUM (dealdetails.HEDGE_PRICE * dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )/SUM (dealvolume.HEDGE_VOLUME*consumption.CONVERSION_FACTOR )  as decimal(20,3))
			     END
			ELSE 0	
			END HEDGE_PRICE
		
		from	RM_DEAL_TICKET  dealticket
			left join RM_DEAL_TICKET_DETAILS dealdetails on (dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID )
			left join RM_CURRENCY_UNIT_CONVERSION conversion on( dealticket.CLIENT_ID=conversion.CLIENT_ID) and (conversion.CONVERSION_YEAR=DATEPART(yyyy,dealdetails.MONTH_IDENTIFIER)),
			RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
			RM_DEAL_TICKET_OPTION_DETAILS dealoption,
			ENTITY entity,
			SITE site,
			RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
			CONSUMPTION_UNIT_CONVERSION consumption,
			ENTITY entity1,
			RM_ONBOARD_HEDGE_SETUP hedge
		
		where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
						  from	 ENTITY
						  where  ENTITY_TYPE=268 AND
						  	 ENTITY_NAME like 'options'
						) AND	
		
			dealTicket.HEDGE_MODE_TYPE_ID=(   select ENTITY_ID
							  from	 ENTITY
							  where  ENTITY_TYPE=263 AND
							  	 ENTITY_NAME like 'nymex'
						) AND	
	
			
			dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
		 					  from	RM_DEAL_TICKET
							  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH
	
							  UNION 	
	
							  select RM_DEAL_TICKET_ID 
	
		 					  from	RM_DEAL_TICKET
							  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
		
							  UNION										
	
							  select RM_DEAL_TICKET_ID 					
							  from	RM_DEAL_TICKET					
							  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)
	
							) AND
		
			dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
			dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
								       from    ENTITY
								       where   ENTITY_TYPE=286 AND
								               ENTITY_NAME IN('order executed')
								    ) AND
			dealticket.CLIENT_ID=@clientId AND
			dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
			dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealoption.RM_DEAL_TICKET_DETAILS_ID AND
			dealoption.SITE_ID = hedge.SITE_ID AND
			hedge.INCLUDE_IN_REPORTS = 1 AND
			dealoption.OPTION_TYPE_ID=entity.ENTITY_ID AND	
			dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
			AND dealoption.SITE_ID=site.site_id AND
			dealticket.UNIT_TYPE_ID=consumption.BASE_UNIT_ID AND
			consumption.CONVERTED_UNIT_ID=25 AND
			dealticket.FREQUENCY_TYPE_ID=entity1.ENTITY_ID	
	
		GROUP BY  site.site_id, dealdetails.MONTH_IDENTIFIER, entity1.ENTITY_NAME
	
		end
	end










GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_HEDGE_POSITIONS_P] TO [CBMSApplication]
GO
