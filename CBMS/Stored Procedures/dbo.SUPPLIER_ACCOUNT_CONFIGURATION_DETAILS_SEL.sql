SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/********          
NAME:            
   [DBO].[SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL] 
       
DESCRIPTION:            
		TO GET THE SUPPLIER_ACCOUNT DETAILS FOR THE SELECTED CONTRACT ID AND ACCOUNT ID  
        
INPUT PARAMETERS:            
NAME					DATATYPE				DEFAULT				 DESCRIPTION            
------------------------------------------------------------------------------------------            
@CONTRACT_ID			INT  
@ACCOUNT_ID				INT						NULL  
                  
OUTPUT PARAMETERS:            
NAME					DATATYPE				DEFAULT				 DESCRIPTION            
------------------------------------------------------------------------------------------            
USAGE EXAMPLES:            
------------------------------------------------------------------------------------------            
 
    
 EXEC dbo.SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL   13212,5309   
 EXEC dbo.SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL   87257,456452
 EXEC dbo.SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL  -1,1364017
 EXEC dbo.SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL  -1,1740992      

  EXEC dbo.SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL  -1,1740992 
  
  EXEC SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL 172938,1741066 

  EXEC SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL 94187,537203 

  EXEC SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL 172938,1741066 

  EXEC SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL 172040,1713063 

       
AUTHOR INITIALS:            
INITIALS		NAME     
------------------------------------------------------------------------------------------            
MGB				BHASKARAN GOPALAKRISHNAN    
DMR				Deana Ritter  
HG				Hari
SP				Sandeep Pigilam   
RR				Raghu Reddy         

MODIFICATIONS             
INITIALS	DATE		MODIFICATION            
------------------------------------------------------------------------------------------            
MGB			25-AUG-09	CREATED       
DMR			10/9/2009	Removed Contract table and remapped all Contract ID INNER JOINs to SAMM  
MGB			03-Nov-09	included RA_WATCH_LIST while selecting records   
HG			11/05/2009	Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity  
						Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd   
HG			10/11/2009	Added filter clause condition map.Is_history = 0.  
NK			11/11/2009	Incorporated Watchlist group and Is Data Entry Only
SP			2014-04-29	Data Operations Enhancement-2,Added Client_Id,Division_Id,Utility_Account_Id,Utility_Invoice_Source_Id,Utility_Invoice_Source_Name in Select list 
						and replaced Distinct with Group By.
SP			2014-08-05	Data Operations Enhancemet Phase III,Added Is_Invoice_Posting_Blocked in Select list.	
RR			2015-07-20	Added Alternate_Account_Number in output list
SP			2016-12-15	Invoice tracking,@Is_Client_Config_Exists,@Is_Active,CONSOLIDATED_BILLING_POSTED_TO_UTILITY are added as select list.
RR			2017-02-20	CP-8 Added supplier vendor id, name and comodity details
SP			2017-03-17	Contract Placeholder project change,Replaced CONSOLIDATED_BILLING_POSTED_TO_UTILITY with new table Account_Consolidated_Billing_Vendor. 
NR			2018-02-22	Recalc Data Interval - Add  Supplier_Account_Determinant_Source in Output List.	
NR			2019-04-03	Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.		
NR			2019-08-20	Add contract - Added config dates
NR			2019-12-19	MAINT-9670 - Added Contract level Recacl when copy to contract to account to show the exact recalc type.
 RKV  2019-12-30	D20-1762  REmoved ubm details
 RKV  2020-02-28  Added space between first name and lastname
********/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL]
     (
         @CONTRACT_ID INT
         , @ACCOUNT_ID INT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;


        CREATE TABLE #Final_Account_List
             (
                 Account_Id INT
                 , Commodity_Id INT
                 , DMO_Start_Dt DATE
                 , DMO_End_Dt DATE
             );

        DECLARE @Site_Id INT;


        DECLARE
            @Is_Client_Config_Exists BIT = 0
            , @Is_Active BIT = 0;

        SELECT
            @Is_Client_Config_Exists = 1
        FROM
            dbo.Invoice_Collection_Client_Config icc
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Core.Client_Hier_Account cha
                            INNER JOIN Core.Client_Hier ch
                                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                       WHERE
                            cha.Account_Id = @ACCOUNT_ID
                            AND icc.Client_Id = ch.Client_Id);

        SELECT
            @Is_Active = 1
        FROM
            dbo.Invoice_Collection_Account_Config ICA
        WHERE
            ICA.Account_Id = @ACCOUNT_ID
            AND Is_Chase_Activated = 1;




        SELECT
            @Site_Id = ch.Site_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @ACCOUNT_ID;


        INSERT INTO #Final_Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        EXEC dbo.DMO_Config_Sel_By_Site_Id @Site_Id = @Site_Id;





        SELECT
            MAP.ACCOUNT_ID
            , Suppacc.ACCOUNT_NUMBER SUPP_ACC_NUMBER
            , M.METER_NUMBER
            , M.METER_ID
            , CONVERT(VARCHAR(10), sac.Supplier_Account_Begin_Dt, 101) SUPPLIER_ACCOUNT_BEGIN_DT
            , CONVERT(VARCHAR(10), sac.Supplier_Account_End_Dt, 101) SUPPLIER_ACCOUNT_END_DT
            , UTILACC.ACCOUNT_NUMBER UTIL_ACCOUNT_NUMBER
            , Suppacc.RA_WATCH_LIST
            , Suppacc.DM_WATCH_LIST
            , Suppacc.NOT_MANAGED
            , Suppacc.NOT_EXPECTED
            , Suppacc.SERVICE_LEVEL_TYPE_ID
            , RATE.RATE_NAME RATENAME
            , Utltven.VENDOR_ID UTILITY_ID
            , Utltven.VENDOR_NAME UTILITY
            , S.SITE_ID
            , S.SITE_NAME SITENAME
            , ADR.CITY
            , ST.STATE_ID
            , ST.STATE_NAME STATE
            , Suppacc.INVOICE_SOURCE_TYPE_ID INVOICE_SOURCE_TYPE_ID
            , ENT.ENTITY_NAME ENTITY_NAME
            , CONVERT(VARCHAR(10), Suppacc.NOT_EXPECTED_DATE, 101) NOT_EXPECTED_DATE
            , Suppacc.Supplier_Account_Recalc_Type_Cd
            , Suppacc.Supplier_Account_Is_Default_Setting
            , Suppacc.Is_Data_Entry_Only
            , S.Client_ID
            , S.DIVISION_ID
            , UTILACC.ACCOUNT_ID AS Utility_Account_Id
            , UTILACC.INVOICE_SOURCE_TYPE_ID AS Utility_Invoice_Source_Id
            , invsrc.ENTITY_NAME AS Utility_Invoice_Source_Name
            , Suppacc.Template_Cu_Invoice_Id
            , Suppacc.Template_Cu_Invoice_Updated_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Template_Cu_Invoice_Updated_User_Name
            , Suppacc.Template_Cu_Invoice_Last_Change_Ts
            , Suppacc.Is_Invoice_Posting_Blocked
            , Suppacc.Alternate_Account_Number
            , @Is_Client_Config_Exists AS Is_Client_Config_Exists
            , @Is_Active AS Is_Active
            , CAST((CASE WHEN cb.Account_Id IS NOT NULL THEN 1
                        ELSE 0
                    END) AS BIT) CONSOLIDATED_BILLING_POSTED_TO_UTILITY
            , Suppven.VENDOR_ID AS Supplier_Vendor_Id
            , Suppven.VENDOR_NAME AS Supplier_Vendor_Name
            , comm.Commodity_Id
            , comm.Commodity_Name
            , Suppacc.Supplier_Account_Determinant_Source_Cd
            , Determinant_Source.Code_Value AS Supplier_Account_Determinant_Source
            , CASE WHEN (   fal.Account_Id IS NOT NULL
                            AND fal.Commodity_Id IS NOT NULL
                            AND (   MAP.Contract_ID = -1
                                    OR  MAP.Contract_ID IS NULL)) THEN 'DMO'
                  WHEN (   MAP.Contract_ID = -1
                           OR   MAP.Contract_ID IS NULL) THEN 'Missing Contract'
                  ELSE c.ED_CONTRACT_NUMBER
              END AS Contract_Label
            , MAP.Contract_ID
            , CASE WHEN LEN(rt.Recalc_Type) > 1 THEN LEFT(rt.Recalc_Type, LEN(rt.Recalc_Type) - 1)
                  ELSE rt.Recalc_Type
              END AS Recalc_Type
            , sac.Supplier_Account_Config_Id
			, UI2. FIRST_NAME + ' ' + UI2.LAST_NAME AS Last_Change_By
			, APBAT.Last_Change_Ts 
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP MAP
            INNER JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = MAP.ACCOUNT_ID
                   AND  sac.Contract_Id = MAP.Contract_ID
                   AND  sac.Supplier_Account_Config_Id = MAP.Supplier_Account_Config_Id
            INNER JOIN dbo.ACCOUNT Suppacc
                ON Suppacc.ACCOUNT_ID = MAP.ACCOUNT_ID
            INNER JOIN dbo.VENDOR Suppven
                ON Suppven.VENDOR_ID = Suppacc.VENDOR_ID
            INNER JOIN dbo.METER M
                ON MAP.METER_ID = M.METER_ID
            INNER JOIN dbo.ACCOUNT UTILACC
                ON M.ACCOUNT_ID = UTILACC.ACCOUNT_ID
            INNER JOIN dbo.RATE RATE
                ON RATE.RATE_ID = M.RATE_ID
            INNER JOIN dbo.Commodity comm
                ON comm.Commodity_Id = RATE.COMMODITY_TYPE_ID
            INNER JOIN dbo.VENDOR Utltven
                ON UTILACC.VENDOR_ID = Utltven.VENDOR_ID
            INNER JOIN dbo.SITE S
                ON UTILACC.SITE_ID = S.SITE_ID
            INNER JOIN dbo.ADDRESS ADR
                ON ADR.ADDRESS_ID = S.PRIMARY_ADDRESS_ID
            INNER JOIN dbo.STATE ST
                ON ST.STATE_ID = ADR.STATE_ID
            LEFT JOIN dbo.ENTITY ENT
                ON Suppacc.SERVICE_LEVEL_TYPE_ID = ENT.ENTITY_ID
            LEFT JOIN dbo.ENTITY invsrc
                ON UTILACC.INVOICE_SOURCE_TYPE_ID = invsrc.ENTITY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON Suppacc.Template_Cu_Invoice_Updated_User_Id = ui.USER_INFO_ID
			LEFT JOIN dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING AS APBAT 
				ON Suppacc.ACCOUNT_ID=APBAT.ACCOUNT_ID 
			LEFT JOIN dbo.USER_INFO AS UI2 ON APBAT.Updated_User_Id=UI2.USER_INFO_ID
            LEFT JOIN (   SELECT
                                acbv.Account_Id
                          FROM
                                dbo.Account_Consolidated_Billing_Vendor acbv
                          GROUP BY
                              acbv.Account_Id) cb
                ON cb.Account_Id = UTILACC.ACCOUNT_ID
            LEFT JOIN dbo.Code Determinant_Source
                ON Suppacc.Supplier_Account_Determinant_Source_Cd = Determinant_Source.Code_Id
            LEFT JOIN #Final_Account_List fal
                ON fal.Account_Id = MAP.ACCOUNT_ID
                   AND  fal.Commodity_Id = comm.Commodity_Id
                   AND  (   sac.Supplier_Account_Begin_Dt BETWEEN fal.DMO_Start_Dt
                                                          AND     fal.DMO_End_Dt
                            AND ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31') BETWEEN fal.DMO_Start_Dt
                                                                                  AND     fal.DMO_End_Dt)
            LEFT JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = MAP.Contract_ID
            CROSS APPLY (   SELECT
                                ISNULL(
                                    Sup_Recalc.Code_Value
                                    , CASE WHEN clsrt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                                                AND clsrt.Invoice_Recalc_Type_Cd IS NULL THEN Con_Recalc.Code_Value
                                      END) + ','
                            FROM
                                dbo.Account_Commodity_Invoice_Recalc_Type clsrt
                                LEFT JOIN dbo.Code Sup_Recalc
                                    ON Sup_Recalc.Code_Id = clsrt.Invoice_Recalc_Type_Cd
                                LEFT JOIN dbo.Contract_Recalc_Config crc
                                    ON crc.Contract_Id = @CONTRACT_ID
                                       AND  crc.Commodity_Id = comm.Commodity_Id
                                       AND  crc.Is_Consolidated_Contract_Config = 0
                                       AND  (   Suppacc.Supplier_Account_Begin_Dt BETWEEN clsrt.Start_Dt
                                                                                  AND     ISNULL(
                                                                                              clsrt.End_Dt
                                                                                              , '9999-12-31')
                                                OR  Suppacc.Supplier_Account_End_Dt BETWEEN clsrt.Start_Dt
                                                                                    AND     ISNULL(
                                                                                                clsrt.End_Dt
                                                                                                , '9999-12-31')
                                                OR  clsrt.Start_Dt BETWEEN Suppacc.Supplier_Account_Begin_Dt
                                                                   AND     Suppacc.Supplier_Account_End_Dt
                                                OR  ISNULL(clsrt.End_Dt, '9999-12-31') BETWEEN Suppacc.Supplier_Account_Begin_Dt
                                                                                       AND     Suppacc.Supplier_Account_End_Dt)
                                LEFT JOIN dbo.Code Con_Recalc
                                    ON Con_Recalc.Code_Id = crc.Supplier_Recalc_Type_Cd
                            WHERE
                                clsrt.Account_Id = MAP.ACCOUNT_ID
                                AND clsrt.Commodity_ID = comm.Commodity_Id
                                AND (   Suppacc.Supplier_Account_Begin_Dt BETWEEN clsrt.Start_Dt
                                                                          AND     ISNULL(clsrt.End_Dt, '9999-12-31')
                                        OR  Suppacc.Supplier_Account_End_Dt BETWEEN clsrt.Start_Dt
                                                                            AND     ISNULL(clsrt.End_Dt, '9999-12-31')
                                        OR  clsrt.Start_Dt BETWEEN Suppacc.Supplier_Account_Begin_Dt
                                                           AND     Suppacc.Supplier_Account_End_Dt
                                        OR  ISNULL(clsrt.End_Dt, '9999-12-31') BETWEEN Suppacc.Supplier_Account_Begin_Dt
                                                                               AND     Suppacc.Supplier_Account_End_Dt)
                            GROUP BY
                                ISNULL(
                                    Sup_Recalc.Code_Value
                                    , CASE WHEN clsrt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                                                AND clsrt.Invoice_Recalc_Type_Cd IS NULL THEN Con_Recalc.Code_Value
                                      END)
                            FOR XML PATH('')) rt(Recalc_Type)
        WHERE
            MAP.Contract_ID = @CONTRACT_ID
            AND (   @ACCOUNT_ID IS NULL
                    OR  Suppacc.ACCOUNT_ID = @ACCOUNT_ID)
            AND MAP.IS_HISTORY = 0
        GROUP BY
            MAP.ACCOUNT_ID
            , Suppacc.ACCOUNT_NUMBER
            , M.METER_NUMBER
            , M.METER_ID
            , sac.Supplier_Account_Begin_Dt
            , sac.Supplier_Account_End_Dt
            , UTILACC.ACCOUNT_NUMBER
            , Suppacc.RA_WATCH_LIST
            , Suppacc.DM_WATCH_LIST
            , Suppacc.NOT_MANAGED
            , Suppacc.NOT_EXPECTED
            , Suppacc.SERVICE_LEVEL_TYPE_ID
            , RATE.RATE_NAME
            , Utltven.VENDOR_ID
            , Utltven.VENDOR_NAME
            , S.SITE_ID
            , S.SITE_NAME
            , ADR.CITY
            , ST.STATE_ID
            , ST.STATE_NAME
            , Suppacc.INVOICE_SOURCE_TYPE_ID
            , ENT.ENTITY_NAME
            , Suppacc.NOT_EXPECTED_DATE
            , Suppacc.Supplier_Account_Recalc_Type_Cd
            , Suppacc.Supplier_Account_Is_Default_Setting
            , Suppacc.Is_Data_Entry_Only
            , S.Client_ID
            , S.DIVISION_ID
            , UTILACC.ACCOUNT_ID
            , UTILACC.INVOICE_SOURCE_TYPE_ID
            , invsrc.ENTITY_NAME
            , Suppacc.Template_Cu_Invoice_Id
            , Suppacc.Template_Cu_Invoice_Updated_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , Suppacc.Template_Cu_Invoice_Last_Change_Ts
            , Suppacc.Is_Invoice_Posting_Blocked
            , Suppacc.Alternate_Account_Number
            , CAST((CASE WHEN cb.Account_Id IS NOT NULL THEN 1
                        ELSE 0
                    END) AS BIT)
            , Suppven.VENDOR_ID
            , Suppven.VENDOR_NAME
            , comm.Commodity_Id
            , comm.Commodity_Name
            , Suppacc.Supplier_Account_Determinant_Source_Cd
            , Determinant_Source.Code_Value
            , CASE WHEN (   fal.Account_Id IS NOT NULL
                            AND fal.Commodity_Id IS NOT NULL
                            AND (   MAP.Contract_ID = -1
                                    OR  MAP.Contract_ID IS NULL)) THEN 'DMO'
                  WHEN (   MAP.Contract_ID = -1
                           OR   MAP.Contract_ID IS NULL) THEN 'Missing Contract'
                  ELSE c.ED_CONTRACT_NUMBER
              END
            , MAP.Contract_ID
            , CASE WHEN LEN(rt.Recalc_Type) > 1 THEN LEFT(rt.Recalc_Type, LEN(rt.Recalc_Type) - 1)
                  ELSE rt.Recalc_Type
              END
            , sac.Supplier_Account_Config_Id
			,UI2. FIRST_NAME 
			,UI2.LAST_NAME
			,APBAT.Last_Change_Ts 

        ORDER BY
            MAP.ACCOUNT_ID;

        DROP TABLE #Final_Account_List;
    END;
    ;

    ;


















GO




GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_CONFIGURATION_DETAILS_SEL] TO [CBMSApplication]
GO
