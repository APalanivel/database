SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_FORECAST_VOLUMES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@forecast_as_of_date	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE dbo.DELETE_FORECAST_VOLUMES_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@forecast_as_of_date datetime
 AS
delete RM_FORECAST_VOLUME_DETAILS where RM_FORECAST_VOLUME_ID 
	in (select RM_FORECAST_VOLUME_ID from RM_FORECAST_VOLUME
		 where forecast_as_of_date=@forecast_as_of_date and client_id = @clientId)

delete RM_FORECAST_VOLUME where forecast_as_of_date=@forecast_as_of_date  and  client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[DELETE_FORECAST_VOLUMES_P] TO [CBMSApplication]
GO
