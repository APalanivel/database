SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Contract_Geography_Dtls_Sel_By_Contract_Id]
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name					DataType		Default		Description  
---------------------------------------------------------------------------------  
    @Meter_Id				VARCHAR(MAX)
    @Contract_Start_Date	DATETIME
    @Contract_End_Date		DATETIME


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		
		SELECT DISTINCT TOP 10 Contract_ID FROM dbo.SUPPLIER_ACCOUNT_METER_MAP
		SELECT cha.Supplier_Contract_ID FROM core.Client_Hier_Account cha
			GROUP BY cha.Supplier_Contract_ID HAVING count(DISTINCT cha.Meter_State_Id)>1
		
		EXEC  dbo.Contract_Geography_Dtls_Sel_By_Contract_Id 11603
		EXEC  dbo.Contract_Geography_Dtls_Sel_By_Contract_Id 12048
		EXEC  dbo.Contract_Geography_Dtls_Sel_By_Contract_Id 12056
		EXEC  dbo.Contract_Geography_Dtls_Sel_By_Contract_Id 117979
		EXEC  dbo.Contract_Geography_Dtls_Sel_By_Contract_Id 125326


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-11-03	Global Sourcing - Phase2 -Created
								
******/

CREATE PROCEDURE [dbo].[Contract_Geography_Dtls_Sel_By_Contract_Id] ( @Contract_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            cha.Meter_State_Id
           ,cha.Meter_State_Name
           ,cha.Meter_Country_Id
           ,cha.Meter_Country_Name
      FROM
            core.Client_Hier_Account cha
      WHERE
            cha.Supplier_Contract_ID = @Contract_Id
      GROUP BY
            cha.Meter_State_Id
           ,cha.Meter_State_Name
           ,cha.Meter_Country_Id
           ,cha.Meter_Country_Name

	

END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Geography_Dtls_Sel_By_Contract_Id] TO [CBMSApplication]
GO
