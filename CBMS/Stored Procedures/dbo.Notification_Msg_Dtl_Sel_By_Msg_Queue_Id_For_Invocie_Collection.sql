
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection 

DESCRIPTION: 
				To get full details of notification deliveried

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Notification_Msg_Queue_Id	INT

OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   
SELECT * FROM dbo.USER_INFO WHERE USER_INFO_ID = 16

EXEC dbo.Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection 10558
EXEC dbo.Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection 8311
EXEC dbo.Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection 8319


AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	NR			Narayana Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	NR			2016-06-03  Created invoice Collection
	NR			2017-06-20	MAINT-5500 - Replaced xml type to accepcted the & value.
	 	 		 
******/
CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection]
      ( 
       @Notification_Msg_Queue_Id INT )
AS 
BEGIN 

      SET NOCOUNT ON;
      
      SELECT
            nmd.Recipient_Email_Address AS To_User_Email
           ,nmq.Email_Subject
           ,nmq.Email_Body
           ,CASE WHEN LEN(attachment.attachments) > 0 THEN LEFT(attachment.attachments, LEN(attachment.attachments) - 1)
                 ELSE NULL
            END AS Attachments
           ,CASE WHEN LEN(recpmail.recpmails) > 0 THEN LEFT(recpmail.recpmails, LEN(recpmail.recpmails) - 1)
                 ELSE NULL
            END AS CC_Email_Address
      FROM
            dbo.Notification_Msg_Dtl nmd
            INNER JOIN dbo.Notification_Msg_Queue nmq
                  ON nmd.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
            CROSS APPLY ( SELECT (
                              SELECT
                                    nmd.CC_Email_Address + ';'
                              FROM
                                    dbo.Notification_Msg_Dtl nmd
                              WHERE
                                    nmq.Notification_Msg_Queue_Id = nmd.Notification_Msg_Queue_Id
                              GROUP BY
                                    nmd.CC_Email_Address
                          FOR
                              XML PATH('')
                                 ,TYPE).value('.', 'NVARCHAR(MAX)') recpmail ) recpmail ( recpmails )
            CROSS APPLY ( SELECT (
                              SELECT
                                    ci.CBMS_DOC_ID + '|' + CAST(ci.CBMS_IMAGE_ID AS VARCHAR(50)) + ';'
                              FROM
                                    dbo.Notification_Msg_Attachment nma
                                    INNER JOIN dbo.cbms_image ci
                                          ON nma.Cbms_Image_Id = ci.CBMS_IMAGE_ID
                              WHERE
                                    nma.Notification_Msg_Queue_Id = nmd.Notification_Msg_Queue_Id
                              GROUP BY
                                    ci.CBMS_DOC_ID
                                   ,ci.CBMS_IMAGE_ID
                          FOR
                              XML PATH('')
                                 ,TYPE).value('.', 'VARCHAR(MAX)') attachment ) attachment ( attachments )
      WHERE
            nmq.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id;
      
END;



;

;
GO

GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Sel_By_Msg_Queue_Id_For_Invocie_Collection] TO [CBMSApplication]
GO
