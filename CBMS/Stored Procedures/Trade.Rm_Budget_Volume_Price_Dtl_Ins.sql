SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Volume_Price_Dtl_Ins                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Volume_Price_Dtl_Ins  584,'2019-01-01','2019-12-01'  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-02-24	GRM-1769 Modified insert to merge               
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Volume_Price_Dtl_Ins]
    (
        @Rm_Budget_Id INT
        , @tvp_Rm_Budget_Volume_Price_Dtl AS [Trade].[tvp_Rm_Budget_Volume_Price_Dtl] READONLY
    )
AS
    BEGIN

        SET NOCOUNT ON;

        --INSERT INTO Trade.Rm_Budget_Volume_Price_Dtl
        --     (
        --         Rm_Budget_Id
        --         , Client_Hier_Id
        --         , Service_Month
        --         , Forecast_Volume
        --         , Total_Volume_Hedged
        --         , WACOG_Hedge_Price
        --         , Budget_Target_Price
        --     )
        --SELECT
        --    @Rm_Budget_Id
        --    , Client_Hier_Id
        --    , Service_Month
        --    , Forecast_Volume
        --    , Total_Volume_Hedged
        --    , WACOG_Hedge_Price
        --    , Budget_Target_Price
        --FROM
        --    @tvp_Rm_Budget_Volume_Price_Dtl;


        MERGE INTO Trade.Rm_Budget_Volume_Price_Dtl AS tgt
        USING
        (   SELECT
                @Rm_Budget_Id AS Rm_Budget_Id
                , Client_Hier_Id
                , Service_Month
                , Forecast_Volume
                , Total_Volume_Hedged
                , WACOG_Hedge_Price
                , Budget_Target_Price
            FROM
                @tvp_Rm_Budget_Volume_Price_Dtl) AS src
        ON tgt.Rm_Budget_Id = src.Rm_Budget_Id
           AND  tgt.Client_Hier_Id = src.Client_Hier_Id
           AND  tgt.Service_Month = src.Service_Month
        WHEN MATCHED THEN UPDATE SET
                              tgt.Forecast_Volume = src.Forecast_Volume
                              , tgt.Total_Volume_Hedged = src.Total_Volume_Hedged
                              , tgt.WACOG_Hedge_Price = src.WACOG_Hedge_Price
                              , tgt.Budget_Target_Price = src.Budget_Target_Price
        WHEN NOT MATCHED THEN INSERT (Rm_Budget_Id
                                      , Client_Hier_Id
                                      , Service_Month
                                      , Forecast_Volume
                                      , Total_Volume_Hedged
                                      , WACOG_Hedge_Price
                                      , Budget_Target_Price)
                              VALUES
                                  (@Rm_Budget_Id
                                   , src.Client_Hier_Id
                                   , src.Service_Month
                                   , src.Forecast_Volume
                                   , src.Total_Volume_Hedged
                                   , src.WACOG_Hedge_Price
                                   , src.Budget_Target_Price);


    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Volume_Price_Dtl_Ins] TO [CBMSApplication]
GO
