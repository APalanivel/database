SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ONBOARD_SITE_VOLUME_UNITS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.GET_ONBOARD_SITE_VOLUME_UNITS_P
@userId varchar(10),
@sessionId varchar(20),
@clientId integer
AS
set nocount on
	select hedge.site_id, hedge.volume_units_type_id 
	from RM_ONBOARD_HEDGE_SETUP hedge(NOLOCK), site sit(NOLOCK), division div(NOLOCK) , client cli(NOLOCK) 
	where hedge.site_id = sit.site_id
	and hedge.division_id = div.division_id
	and div.division_id = sit.division_id  
	and div.client_id = cli.client_id
	and cli.client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_SITE_VOLUME_UNITS_P] TO [CBMSApplication]
GO
