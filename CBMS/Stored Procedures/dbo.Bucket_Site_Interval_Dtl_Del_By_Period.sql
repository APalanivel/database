SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
NAME: Bucket_Site_Interval_Dtl_Del_By_Period  
      
DESCRIPTION:                    
                    
 To Delete the data in Bucket_Site_Interval_Dtl  based on the selected  Date Interval    
      
INPUT PARAMETERS:                              
NAME    DATATYPE DEFAULT  DESCRIPTION                              
------------------------------------------------------------                              
@Client_Hier_Id  INT    
@Account_Id   INT    
@Service_Start_Dt DATE    
@Service_End_Dt  DATE    
@Data_Source_Cd  INT    
      
OUTPUT PARAMETERS:  
NAME    DATATYPE DEFAULT  DESCRIPTION                    
                           
------------------------------------------------------------                              
  
USAGE EXAMPLES:                              
------------------------------------------------------------                      
   
   
DECLARE @Client_Hier_Id INT,@Account_Id INT,@Service_Start_Dt DATE,@Service_End_Dt DATE,@Data_Source_Cd INT
SELECT TOP 1 @Client_Hier_Id=a.Client_Hier_Id,@Account_Id=b.Account_Id,@Service_Start_Dt=a.Service_Start_Dt,
	@Service_End_Dt=a.Service_End_Dt,@Data_Source_Cd=a.Data_Source_Cd 
	FROM dbo.Bucket_Site_Interval_Dtl a JOIN Core.Client_Hier_Account b ON a.Client_Hier_Id = b.Client_Hier_Id 

BEGIN TRAN  
	SELECT  count(1) from dbo.Bucket_Site_Interval_Dtl where  Client_Hier_Id=@Client_Hier_Id AND
		Service_Start_Dt=@Service_Start_Dt AND Service_End_Dt=@Service_End_Dt AND Data_Source_Cd=@Data_Source_Cd
	Exec dbo.Bucket_Site_Interval_Dtl_Del_By_Period @Client_Hier_Id=@Client_Hier_Id,@Account_Id=@Account_Id,
		@Service_Start_Dt=@Service_Start_Dt,@Service_End_Dt=@Service_End_Dt,@Data_Source_Cd=@Data_Source_Cd 
	SELECT  count(1) from dbo.Bucket_Site_Interval_Dtl where  Client_Hier_Id=@Client_Hier_Id AND
		Service_Start_Dt=@Service_Start_Dt AND Service_End_Dt=@Service_End_Dt AND Data_Source_Cd=@Data_Source_Cd
ROLLBACK TRAN                   
       
          
AUTHOR INITIALS:                              
INITIALS NAME      
------------------------------------------------------------                              
PKY   Pavan K Yadalam
NM	Nagaraju MUppa                    
      
MODIFICATIONS      
INITIALS DATE  MODIFICATION      
------------------------------------------------------------      
PKY   2013-03-14 Created for Calendarization requirement      
NM		2020-06-03 Maint-10283	Added table variable instead of using exists clause with Client_Hier_Account table to get the bucket_master details based on Account to improve the sp performance
****/
CREATE PROCEDURE [dbo].[Bucket_Site_Interval_Dtl_Del_By_Period]
(
	@Client_Hier_Id		INT
	, @Account_Id		INT
	, @Service_Start_Dt DATE
	, @Service_End_Dt	DATE
	, @Data_Source_Cd	INT
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Account_Bucket_Master TABLE
	(
		Bucket_Master_Id INT NOT NULL
	);

	INSERT INTO @Account_Bucket_Master
		(
			Bucket_Master_Id
		)
	SELECT
		bm.Bucket_Master_Id
	FROM
		dbo.Bucket_Master bm
	WHERE
		EXISTS (
				   SELECT
					   1
				   FROM
					   Core.Client_Hier_Account cha
				   WHERE
					   cha.Commodity_Id = bm.Commodity_Id
					   AND cha.Account_Id = @Account_Id
					   AND cha.Client_Hier_Id = @Client_Hier_Id
			   );


	DELETE
		bsid
	FROM
		dbo.Bucket_Site_Interval_Dtl bsid
		INNER JOIN @Account_Bucket_Master ABM
			ON ABM.Bucket_Master_Id = bsid.Bucket_Master_Id
	WHERE
		bsid.Client_Hier_Id = @Client_Hier_Id
		AND (
				@Service_Start_Dt BETWEEN bsid.Service_Start_Dt AND bsid.Service_End_Dt
				OR @Service_End_Dt BETWEEN bsid.Service_Start_Dt AND bsid.Service_End_Dt
				OR bsid.Service_Start_Dt BETWEEN @Service_Start_Dt AND @Service_End_Dt
				OR bsid.Service_End_Dt BETWEEN @Service_Start_Dt AND @Service_End_Dt
			)
		AND bsid.Data_Source_Cd = @Data_Source_Cd;

END;

GO
GRANT EXECUTE ON  [dbo].[Bucket_Site_Interval_Dtl_Del_By_Period] TO [CBMSApplication]
GO
