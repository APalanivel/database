SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Sel_By_Client                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
    @User_Info_Id					INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	EXEC Trade.Deal_Ticket_Sel_By_Client 11236
	
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-14     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Sel_By_Client] ( @Client_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

       
      SELECT
            dt.Deal_Ticket_Id
           ,dt.Workflow_Id
           ,wf.Workflow_Name
      FROM
            trade.Deal_Ticket dt
            INNER JOIN dbo.Workflow wf
                  ON dt.Workflow_Id = wf.Workflow_Id
      WHERE
            Client_Id = @Client_Id
      GROUP BY
            dt.Deal_Ticket_Id
           ,dt.Workflow_Id
           ,wf.Workflow_Name


END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Sel_By_Client] TO [CBMSApplication]
GO
