
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************    
NAME :	dbo.Get_New_Users 
   
DESCRIPTION: This procedure used to get the new users which are created and .
   
 INPUT PARAMETERS:    
 Name				DataType				Default      Description    
--------------------------------------------------------------------      
 @Threshold_Mins	INT						30			Determines which users should be considered for email notification
    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------    
  USAGE EXAMPLES:    
--------------------------------------------------------------------    

	EXEC Get_New_Users
  
AUTHOR INITIALS:    
 Initials		Name    
-------------------------------------------------------------------     
 KVK			K Vinay Kumar
 KH				Kevin Horton
 HG				Harihara Suthan Ganesan
  
 MODIFICATIONS
 Initials	Date		Modification
--------------------------------------------------------------------
KVK			12/21/2010  ?User_Info table has 4 types of users:
						o	Internal users (access_level = 0, Is_Demo_User = 0)
						o	External DV Users (access_level = 1, Is_Demo_User = 0)
						o	SV Users (access_leve = null, Is_Demo_User = 0)
						o	Demo users (access_level = 1 and Is_Demo_User = 1)
						send email only to External DV Users

RC			12/29/2010  Removed PASSCODE selection
KH			01/30/2013	Joined Single_SignOn_User_Map (emails should not be sent to external SSO Users)
HG			2014-01-31	RA Admin UM changes
							- Email_Body, Email_Subject and notification_msg_Dtl_id column added
							- Cut off time added to pick only the users created 30 minutes before the batch run time
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Get_New_Users]
      ( @Threshold_Mins INT = 30 )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Notification_Template_Id INT
           ,@Msg_Delivery_Status_Cd INT
           ,@Cut_Off_Ts DATETIME= DATEADD(MINUTE, -@Threshold_Mins, GETDATE())

      SELECT
            @Msg_Delivery_Status_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            INNER JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Pending'
            AND cs.Codeset_Name = 'Message Delivery Status'

      SELECT
            @Notification_Template_Id = nt.Notification_Template_Id
      FROM
            dbo.Notification_Template nt
            INNER JOIN dbo.Code AS c
                  ON c.Code_Id = nt.Notification_Type_Cd
      WHERE
            c.Code_Value = 'New User Confirmation'

      SELECT
            ui.user_info_id
           ,ui.access_level
           ,ui.FIRST_NAME + SPACE(1) + LAST_NAME AS Full_Name
           ,ui.username
           ,ui.EMAIL_ADDRESS
           ,mq.Email_Subject
           ,mq.Email_Body
           ,md.Notification_Msg_Dtl_Id
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.Notification_Msg_Dtl md
                  ON md.Recipient_User_Id = ui.USER_INFO_ID
            INNER JOIN dbo.Notification_Msg_Queue mq
                  ON mq.Notification_Msg_Queue_Id = md.Notification_Msg_Queue_Id
      WHERE
            md.Msg_Delivery_Status_Cd = @Msg_Delivery_Status_Cd
            AND mq.Notification_Template_Id = @Notification_Template_Id
            AND md.Created_Ts <= @Cut_Off_Ts
            AND ui.Access_Level = 1
            AND ui.Is_Demo_User = 0
            AND ui.Is_history = 0
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Single_Signon_User_Map um
                             WHERE
                              um.Internal_User_ID = ui.USER_INFO_ID )

END;
;
GO


GRANT EXECUTE ON  [dbo].[Get_New_Users] TO [CBMSApplication]
GO
