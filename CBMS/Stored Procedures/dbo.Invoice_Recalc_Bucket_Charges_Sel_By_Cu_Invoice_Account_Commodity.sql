SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Recalc_Bucket_Charges_Sel_By_Cu_Invoice_Account_Commodity             
    
Description:                
        To insert Data to Cu_Invoice_Account_Commodity_Recalc_Lock table.                

 Input Parameters:                
    Name				DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
   @Cu_Invoice_Id       INT  
   @Account_Id			INT  
   @Commodity_Id		INT  

 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  

 Usage Examples:                    
----------------------------------------------------------------------------------------                  

EXEC Invoice_Recalc_Bucket_Charges_Sel_By_Cu_Invoice_Account_Commodity
      23647880
     ,660968
     ,291  

EXEC Invoice_Recalc_Bucket_Charges_Sel_By_Cu_Invoice_Account_Commodity
      28400518
     ,53795
     ,291  

Author Initials:                
	Initials	Name                
----------------------------------------------------------------------------------------                  
	RKV			Ravi Kumar Vegesna  
	HG			Harihara Suthan G  

 Modifications:                
    Initials        Date		Modification                
----------------------------------------------------------------------------------------                  
    RKV				2016-09-01  Maint-4109: Created For AS400-Enhancement.
    RKV             2016-10-05  MAINT-4409: Excluded the Late Fee from both Recalc Total and Invoice Total Charge
    HG              2016-10-05  MAINT-4410: Federal Tax Included in the Tax Bucket Instead of showing as a separate bucket. 
    HG				2016-10-11  Modified to take the charge value from CU_INVOICE_CHARGE when Cu_Invoice_Charge_Account.Charge value is also null
	RKV             2018-04-27  MAINT-7139 changed the logic to get the buckets for the given account commodity.
	RKV				2018-06-07	divide by zero wasn't handled in the place where we calculate No Other Commodity, corrected the code to fix that
******/
CREATE PROCEDURE [dbo].[Invoice_Recalc_Bucket_Charges_Sel_By_Cu_Invoice_Account_Commodity]
     (
         @Cu_Invoice_Id INT
         , @Account_Id INT
         , @Commodity_Id INT
     )
AS
    BEGIN





        SET NOCOUNT ON;



        DECLARE
            @Total_Cost_Selected_Commodity DECIMAL(28, 10)
            , @Commodity_Cnt INT
            , @Tax_Bucket_Master_Id INT
            , @Invoice_Total_Cost DECIMAL(28, 10)
            , @Federal_Tax_Bucket_Master_Id INT;


        DECLARE @Tax_Calc_Invoice_Charges TABLE
              (
                  Bucket_Master_Id INT
                  , Bucket_Name VARCHAR(255)
                  , Bucket_Value DECIMAL(28, 10)
              );

        DECLARE @No_Other_Commodity_Buckets TABLE
              (
                  Bucket_Master_Id INT
                  , Bucket_Name VARCHAR(255)
              );

        DECLARE @Account_Commodities TABLE
              (
                  Commodity_Id INT
              );




        CREATE TABLE #Bucket_Invoice_Recalc_Charges
             (
                 Bucket_Master_Id INT
                 , Bucket_Name VARCHAR(255)
                 , Invoice_Total_Charges DECIMAL(28, 10)
                 , Recalc_Total_Charges DECIMAL(28, 10)
             );

        CREATE TABLE #Bucket_Recalc_Charges
             (
                 Bucket_Master_Id INT
                 , Bucket_Name VARCHAR(255)
                 , Recalc_Total_Charges DECIMAL(28, 10)
             );

        CREATE TABLE #Bucket_Invoice_Charges
             (
                 Bucket_Master_Id INT
                 , Bucket_Name VARCHAR(255)
                 , Invoice_Total_Charges DECIMAL(28, 10)
             );

        CREATE TABLE #Child_Bucket_Names
             (
                 Bucket_Master_Id INT
                 , bucket_Name VARCHAR(255)
                 , Commodity_Id INT
             );

        SELECT
            @Commodity_Cnt = COUNT(DISTINCT cha.Commodity_Id)
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id;

        SELECT
            @Tax_Bucket_Master_Id = MAX(CASE WHEN bm.Bucket_Name = 'Tax' THEN bm.Bucket_Master_Id
                                        END)
            , @Federal_Tax_Bucket_Master_Id = MAX(CASE WHEN bm.Bucket_Name = 'Federal Tax' THEN bm.Bucket_Master_Id
                                                  END)
        FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bt
                ON bt.Code_Id = bm.Bucket_Type_Cd
        WHERE
            bm.Bucket_Name IN ( 'Tax', 'Federal Tax' )
            AND bm.Commodity_Id = @Commodity_Id
            AND bt.Code_Value = 'Charge';




        SELECT
            @Tax_Bucket_Master_Id = ISNULL(@Tax_Bucket_Master_Id, 0)
            , @Federal_Tax_Bucket_Master_Id = ISNULL(@Federal_Tax_Bucket_Master_Id, 0);


        INSERT INTO @Account_Commodities
             (
                 Commodity_Id
             )
        SELECT  DISTINCT
                Commodity_Id
        FROM
            Core.Client_Hier_Account
        WHERE
            Account_Id = @Account_Id;


        INSERT INTO @No_Other_Commodity_Buckets
             (
                 Bucket_Master_Id
                 , Bucket_Name
             )
        SELECT
            Bucket_Master_Id
            , Bucket_Name
        FROM
            dbo.Bucket_Master
        WHERE
            Commodity_Id = -1;


        INSERT INTO #Child_Bucket_Names
             (
                 Bucket_Master_Id
                 , bucket_Name
                 , Commodity_Id
             )
        SELECT
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id
        FROM
            dbo.Bucket_Category_Rule bcr
            INNER JOIN dbo.Bucket_Master catbm
                ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master chldbm
                ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code lvl
                ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Bucket_Master tcbm
                ON tcbm.Bucket_Name = 'Total Cost'
                   AND  tcbm.Commodity_Id = catbm.Commodity_Id
            INNER JOIN dbo.Code tcbt
                ON tcbt.Code_Id = tcbm.Bucket_Type_Cd
            INNER JOIN @Account_Commodities ac
                ON ac.Commodity_Id = chldbm.Commodity_Id
        WHERE
            lvl.Code_Value = 'Invoice'
            AND tcbt.Code_Value = 'Charge'
            AND catbm.Bucket_Name NOT IN ( 'Late Fees' )
            AND catbm.Is_Active = 1
            AND chldbm.Is_Active = 1
            AND (   catbm.Bucket_Name <> 'Load Factor'
                    OR  (   catbm.Bucket_Name = 'Load Factor'
                            AND bcr.Priority_Order = 1))
        GROUP BY
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id;

        INSERT INTO #Child_Bucket_Names
             (
                 Bucket_Master_Id
                 , bucket_Name
                 , Commodity_Id
             )
        SELECT
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id
        FROM
            dbo.Bucket_Category_Rule bcr
            INNER JOIN dbo.Bucket_Master catbm
                ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master chldbm
                ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code lvl
                ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Bucket_Master tcbm
                ON tcbm.Bucket_Name = 'Total Cost'
                   AND  tcbm.Commodity_Id = catbm.Commodity_Id
            INNER JOIN dbo.Code tcbt
                ON tcbt.Code_Id = tcbm.Bucket_Type_Cd
            INNER JOIN @Account_Commodities ac
                ON ac.Commodity_Id = chldbm.Commodity_Id
        WHERE
            lvl.Code_Value = 'Invoice'
            AND tcbt.Code_Value = 'Charge'
            AND catbm.Bucket_Name = 'Taxes'
            AND catbm.Is_Active = 1
            AND chldbm.Is_Active = 1
            AND (   catbm.Bucket_Name <> 'Load Factor' -- This was done to exclude the load factor calculation based on the total usage.
                    OR  (   catbm.Bucket_Name = 'Load Factor'
                            AND bcr.Priority_Order = 1))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Child_Bucket_Names cbn
                               WHERE
                                    chldbm.Bucket_Master_Id = cbn.Bucket_Master_Id)
        GROUP BY
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id;



        INSERT INTO #Child_Bucket_Names
             (
                 Bucket_Master_Id
                 , bucket_Name
                 , Commodity_Id
             )
        SELECT
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id
        FROM
            dbo.Bucket_Category_Rule bcr
            INNER JOIN dbo.Bucket_Master catbm
                ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master chldbm
                ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code lvl
                ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Bucket_Master tcbm
                ON tcbm.Bucket_Name = 'Total Cost'
                   AND  tcbm.Commodity_Id = catbm.Commodity_Id
            INNER JOIN dbo.Code tcbt
                ON tcbt.Code_Id = tcbm.Bucket_Type_Cd
            INNER JOIN @Account_Commodities ac
                ON ac.Commodity_Id = chldbm.Commodity_Id
        WHERE
            lvl.Code_Value = 'Invoice'
            AND tcbt.Code_Value = 'Charge'
            AND chldbm.Bucket_Name = 'Penalty'
            AND catbm.Is_Active = 1
            AND chldbm.Is_Active = 1
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Child_Bucket_Names cbn
                               WHERE
                                    chldbm.Bucket_Master_Id = cbn.Bucket_Master_Id)
        GROUP BY
            chldbm.Bucket_Master_Id
            , chldbm.Bucket_Name
            , chldbm.Commodity_Id;


        SELECT
            @Total_Cost_Selected_Commodity = SUM(CASE WHEN (   NULLIF(cica.Charge_Expression, '') IS NULL
                                                               OR   cica.Charge_Value IS NULL) THEN
                                                 (CONVERT(
                                                      DECIMAL(28, 10)
                                                      , REPLACE(
                                                            REPLACE(
                                                                REPLACE(
                                                                    REPLACE(
                                                                        REPLACE(
                                                                            REPLACE(
                                                                                (CASE WHEN (PATINDEX(
                                                                                                '%[0-9]-'
                                                                                                , cic.CHARGE_VALUE)) > 0 THEN
                                                                                          STUFF(
                                                                                              cic.CHARGE_VALUE
                                                                                              , PATINDEX(
                                                                                                    '%[0-9]-'
                                                                                                    , cic.CHARGE_VALUE)
                                                                                                + 1, 1, '')
                                                                                     ELSE cic.CHARGE_VALUE
                                                                                 END), ',', ''), '0-', 0), '+', '')
                                                                    , '$', ''), ')', ''), '(', '')))
                                                     ELSE cica.Charge_Value
                                                 END)
        FROM
            dbo.CU_INVOICE_CHARGE cic
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
            INNER JOIN #Child_Bucket_Names bn
                ON bn.Bucket_Master_Id = cic.Bucket_Master_Id
        WHERE
            bn.Commodity_Id = @Commodity_Id
            AND cica.ACCOUNT_ID = @Account_Id
            AND cic.CU_INVOICE_ID = @Cu_Invoice_Id;



        SELECT
            @Invoice_Total_Cost = SUM(CASE WHEN (   NULLIF(cica.Charge_Expression, '') IS NULL
                                                    OR  cica.Charge_Value IS NULL) THEN
                                      (CONVERT(
                                           DECIMAL(28, 10)
                                           , REPLACE(
                                                 REPLACE(
                                                     REPLACE(
                                                         REPLACE(
                                                             REPLACE(
                                                                 REPLACE(
                                                                     (CASE WHEN (PATINDEX('%[0-9]-', cic.CHARGE_VALUE)) > 0 THEN
                                                                               STUFF(
                                                                                   cic.CHARGE_VALUE
                                                                                   , PATINDEX(
                                                                                         '%[0-9]-', cic.CHARGE_VALUE)
                                                                                     + 1, 1, '')
                                                                          ELSE cic.CHARGE_VALUE
                                                                      END), ',', ''), '0-', 0), '+', ''), '$', ''), ')'
                                                     , ''), '(', '')))
                                          ELSE cica.Charge_Value
                                      END)
        FROM
            dbo.CU_INVOICE_CHARGE cic
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
            INNER JOIN #Child_Bucket_Names bn
                ON bn.Bucket_Master_Id = cic.Bucket_Master_Id
        WHERE
            cica.ACCOUNT_ID = @Account_Id
            AND cic.CU_INVOICE_ID = @Cu_Invoice_Id;



        --No Other Commodity Calculations  





        INSERT INTO @Tax_Calc_Invoice_Charges
             (
                 Bucket_Master_Id
                 , Bucket_Name
                 , Bucket_Value
             )
        SELECT
            -1
            , 'No/Other Commodity'
            , (@Total_Cost_Selected_Commodity
               * (CASE WHEN (   NULLIF(cica.Charge_Expression, '') IS NULL
                                OR   cica.Charge_Value IS NULL) THEN
            (CONVERT(
                 DECIMAL(28, 10)
                 , REPLACE(
                       REPLACE(
                           REPLACE(
                               REPLACE(
                                   REPLACE(
                                       REPLACE(
                                           (CASE WHEN (PATINDEX('%[0-9]-', cc.CHARGE_VALUE)) > 0 THEN
                                                     STUFF(
                                                         cc.CHARGE_VALUE, PATINDEX('%[0-9]-', cc.CHARGE_VALUE) + 1, 1
                                                         , '')
                                                ELSE cc.CHARGE_VALUE
                                            END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')))
                      ELSE cica.Charge_Value
                  END)) / NULLIF(@Invoice_Total_Cost, 0) Charge_Value
        FROM
            @No_Other_Commodity_Buckets bm
            INNER JOIN dbo.CU_INVOICE_CHARGE cc
                ON bm.Bucket_Master_Id = cc.Bucket_Master_Id
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                ON cica.CU_INVOICE_CHARGE_ID = cc.CU_INVOICE_CHARGE_ID
        WHERE
            cc.CU_INVOICE_ID = @Cu_Invoice_Id
            AND cica.ACCOUNT_ID = @Account_Id
            AND bm.Bucket_Name IN ( 'Tax - Misc', 'Fees' );



        INSERT INTO #Bucket_Invoice_Charges
        SELECT
            Bucket_Master_Id
            , Bucket_Name
            , SUM(Bucket_Value)
        FROM
            @Tax_Calc_Invoice_Charges
        GROUP BY
            Bucket_Master_Id
            , Bucket_Name;



        --No other Commodities Ends here.

        INSERT INTO #Bucket_Invoice_Charges
             (
                 Bucket_Master_Id
                 , Bucket_Name
                 , Invoice_Total_Charges
             )
        SELECT
            bm.Bucket_Master_Id
            , bm.bucket_Name
            , SUM(CASE WHEN (   NULLIF(cica.Charge_Expression, '') IS NULL
                                OR  cica.Charge_Value IS NULL) THEN
                  (CONVERT(
                       DECIMAL(28, 10)
                       , REPLACE(
                             REPLACE(
                                 REPLACE(
                                     REPLACE(
                                         REPLACE(
                                             REPLACE(
                                                 (CASE WHEN (PATINDEX('%[0-9]-', cc.CHARGE_VALUE)) > 0 THEN
                                                           STUFF(
                                                               cc.CHARGE_VALUE
                                                               , PATINDEX('%[0-9]-', cc.CHARGE_VALUE) + 1, 1, '')
                                                      ELSE cc.CHARGE_VALUE
                                                  END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')))
                      ELSE cica.Charge_Value
                  END) Charge_Value
        FROM
            #Child_Bucket_Names bm
            INNER JOIN dbo.CU_INVOICE_CHARGE cc
                ON bm.Bucket_Master_Id = cc.Bucket_Master_Id
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                ON cica.CU_INVOICE_CHARGE_ID = cc.CU_INVOICE_CHARGE_ID
        WHERE
            cc.CU_INVOICE_ID = @Cu_Invoice_Id
            AND cica.ACCOUNT_ID = @Account_Id
            AND bm.Commodity_Id = @Commodity_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Bucket_Invoice_Charges bic
                               WHERE
                                    bic.Bucket_Master_Id = bm.Bucket_Master_Id)
        GROUP BY
            bm.Bucket_Master_Id
            , bm.bucket_Name;

        INSERT INTO #Bucket_Recalc_Charges
             (
                 Bucket_Master_Id
                 , Bucket_Name
                 , Recalc_Total_Charges
             )
        SELECT
            CASE WHEN bm.Bucket_Master_Id = @Federal_Tax_Bucket_Master_Id THEN @Tax_Bucket_Master_Id
                ELSE bm.Bucket_Master_Id
            END
            , CASE WHEN bm.Bucket_Master_Id = @Federal_Tax_Bucket_Master_Id THEN 'Tax'
                  ELSE bm.Bucket_Name
              END
            , SUM(cirr.Net_Amount) Net_Amount
        FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Cu_Invoice_Recalc_Response cirr
                ON cirr.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.RECALC_HEADER rh
                ON rh.RECALC_HEADER_ID = cirr.Recalc_Header_Id
                   AND  rh.Commodity_Id = bm.Commodity_Id
            INNER JOIN #Child_Bucket_Names bn
                ON bn.Bucket_Master_Id = cirr.Bucket_Master_Id
        WHERE
            rh.CU_INVOICE_ID = @Cu_Invoice_Id
            AND rh.ACCOUNT_ID = @Account_Id
            AND bm.Commodity_Id = @Commodity_Id
        GROUP BY
            CASE WHEN bm.Bucket_Master_Id = @Federal_Tax_Bucket_Master_Id THEN @Tax_Bucket_Master_Id
                ELSE bm.Bucket_Master_Id
            END
            , CASE WHEN bm.Bucket_Master_Id = @Federal_Tax_Bucket_Master_Id THEN 'Tax'
                  ELSE bm.Bucket_Name
              END;

        INSERT INTO #Bucket_Invoice_Recalc_Charges
        SELECT
            ISNULL(bic.Bucket_Master_Id, brc.Bucket_Master_Id)
            , ISNULL(bic.Bucket_Name, brc.Bucket_Name)
            , bic.Invoice_Total_Charges
            , brc.Recalc_Total_Charges
        FROM
            #Bucket_Invoice_Charges bic
            FULL OUTER JOIN #Bucket_Recalc_Charges brc
                ON bic.Bucket_Master_Id = brc.Bucket_Master_Id;

        SELECT
            Bucket_Name
            , (Invoice_Total_Charges) Invoice_Total_Charges
            , (Recalc_Total_Charges) Recalc_Total_Charges
        FROM
            #Bucket_Invoice_Recalc_Charges
        ORDER BY
            Bucket_Name;

        DROP TABLE #Bucket_Invoice_Recalc_Charges;
        DROP TABLE #Bucket_Recalc_Charges;
        DROP TABLE
            #Bucket_Invoice_Charges
            , #Child_Bucket_Names;

    END;







GO
GRANT EXECUTE ON  [dbo].[Invoice_Recalc_Bucket_Charges_Sel_By_Cu_Invoice_Account_Commodity] TO [CBMSApplication]
GO
