
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Sel]
           
DESCRIPTION:             
			To get mapped countries to SR pricing product
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Template_Name	INT				NULL
    @Commodity_Id						INT				NULL
    @Country_Id							VARCHAR(MAX)	NULL
    @SR_PRICING_PRODUCT_ID				INT				NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT * FROM dbo.Sr_Service_Condition_Template
	
	EXEC dbo.Sr_Service_Condition_Template_Sel 'Service Condition Template'
	EXEC dbo.Sr_Service_Condition_Template_Sel NULL,NULL,'1',NULL
	EXEC dbo.Sr_Service_Condition_Template_Sel NULL,NULL,'1,4',NULL
	EXEC dbo.Sr_Service_Condition_Template_Sel NULL,NULL,4,NULL
	EXEC dbo.Sr_Service_Condition_Template_Sel NULL,NULL,4,'155,156'
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-11	Global Sourcing - Phase3 - GCS-448 Created
				2016-05-18	GCS-981 Added input parameter Sr_Service_Condition_Template_Id
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Sel]
      ( 
       @Template_Name NVARCHAR(255) = NULL
      ,@Commodity_Id INT = NULL
      ,@Country_Id VARCHAR(MAX) = NULL
      ,@SR_PRICING_PRODUCT_ID VARCHAR(MAX) = NULL
      ,@Sr_Service_Condition_Template_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      WITH  Cte_Templates
              AS ( SELECT
                        ssct.Sr_Service_Condition_Template_Id
                       ,ssct.Template_Name
                       ,spp.COMMODITY_TYPE_ID
                       ,comm.Commodity_Name
                       ,sppcm.SR_Pricing_Product_Id
                       ,spp.PRODUCT_NAME
                       ,sppcm.Country_Id
                       ,coun.COUNTRY_NAME
                   FROM
                        dbo.Sr_Service_Condition_Template ssct
                        LEFT JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctmp
                              ON ssct.Sr_Service_Condition_Template_Id = ssctmp.Sr_Service_Condition_Template_Id
                        LEFT JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON ssctmp.SR_Pricing_Product_Country_Map_Id = sppcm.SR_Pricing_Product_Country_Map_Id
                        LEFT JOIN dbo.SR_PRICING_PRODUCT spp
                              ON sppcm.SR_Pricing_Product_Id = spp.SR_PRICING_PRODUCT_ID
                        LEFT JOIN dbo.Commodity comm
                              ON spp.COMMODITY_TYPE_ID = comm.Commodity_Id
                        LEFT JOIN dbo.COUNTRY coun
                              ON sppcm.Country_Id = coun.COUNTRY_ID
                   WHERE
                        ( @Template_Name IS NULL
                          OR ssct.Template_Name = @Template_Name )
                        AND ( @Commodity_Id IS NULL
                              OR spp.COMMODITY_TYPE_ID = @Commodity_Id )
                        AND ( @Country_Id IS NULL
                              OR EXISTS ( SELECT
                                                cast(Segments AS INT)
                                          FROM
                                                dbo.ufn_split(@Country_Id, ',')
                                          WHERE
                                                cast(Segments AS INT) = sppcm.Country_Id ) )
                        AND ( @SR_PRICING_PRODUCT_ID IS NULL
                              OR EXISTS ( SELECT
                                                cast(Segments AS INT)
                                          FROM
                                                dbo.ufn_split(@SR_PRICING_PRODUCT_ID, ',')
                                          WHERE
                                                cast(Segments AS INT) = sppcm.SR_Pricing_Product_Id ) )
                        AND ( @Sr_Service_Condition_Template_Id IS NULL
                              OR ssct.Sr_Service_Condition_Template_Id <> @Sr_Service_Condition_Template_Id ))
            SELECT
                  cte.Sr_Service_Condition_Template_Id
                 ,cte.Template_Name
                 ,cte.COMMODITY_TYPE_ID
                 ,cte.Commodity_Name
                 ,left(prdids.ProductIds, len(prdids.ProductIds) - 1) AS SR_Pricing_Product_Id
                 ,left(prds.Products, len(prds.Products) - 1) AS PRODUCT_NAME
                 ,left(contry.Countries, len(contry.Countries) - 1) AS COUNTRY
                 ,left(contry_id.Country_ids, len(contry_id.Country_ids) - 1) AS COUNTRY_ID
            FROM
                  Cte_Templates cte
                  CROSS APPLY ( SELECT
                                    cte1.COUNTRY_NAME + ','
                                FROM
                                    Cte_Templates cte1
                                WHERE
                                    cte.Sr_Service_Condition_Template_Id = cte1.Sr_Service_Condition_Template_Id
                                    AND cte1.Country_Id IS NOT NULL
                                GROUP BY
                                    cte1.COUNTRY_NAME
                  FOR
                                XML PATH('') ) contry ( Countries )
                  CROSS APPLY ( SELECT
                                    cte1.PRODUCT_NAME + ','
                                FROM
                                    Cte_Templates cte1
                                WHERE
                                    cte.Sr_Service_Condition_Template_Id = cte1.Sr_Service_Condition_Template_Id
                                    AND cte1.PRODUCT_NAME IS NOT NULL
                                GROUP BY
                                    cte1.PRODUCT_NAME
                  FOR
                                XML PATH('') ) prds ( Products )
                  CROSS APPLY ( SELECT
                                    cast(cte1.SR_Pricing_Product_Id AS VARCHAR(10)) + ','
                                FROM
                                    Cte_Templates cte1
                                WHERE
                                    cte.Sr_Service_Condition_Template_Id = cte1.Sr_Service_Condition_Template_Id
                                    AND cte1.SR_Pricing_Product_Id IS NOT NULL
                                GROUP BY
                                    cte1.SR_Pricing_Product_Id
                  FOR
                                XML PATH('') ) prdids ( ProductIds )
                  CROSS APPLY ( SELECT
                                    cast(cte1.Country_Id AS VARCHAR(10)) + ','
                                FROM
                                    Cte_Templates cte1
                                WHERE
                                    cte.Sr_Service_Condition_Template_Id = cte1.Sr_Service_Condition_Template_Id
                                    AND cte1.Country_Id IS NOT NULL
                                GROUP BY
                                    cte1.Country_Id
                  FOR
                                XML PATH('') ) contry_id ( Country_ids )
            GROUP BY
                  cte.Sr_Service_Condition_Template_Id
                 ,cte.Template_Name
                 ,cte.COMMODITY_TYPE_ID
                 ,cte.Commodity_Name
                 ,left(prdids.ProductIds, len(prdids.ProductIds) - 1)
                 ,left(prds.Products, len(prds.Products) - 1)
                 ,left(contry.Countries, len(contry.Countries) - 1)
                 ,left(contry_id.Country_ids, len(contry_id.Country_ids) - 1)
            
END;
;
GO

GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Sel] TO [CBMSApplication]
GO
