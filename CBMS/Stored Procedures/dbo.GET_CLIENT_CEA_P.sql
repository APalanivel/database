SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CLIENT_CEA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientID      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE dbo.GET_CLIENT_CEA_P
	@clientID INT
AS
BEGIN

	SET NOCOUNT ON
  
	SELECT userInfo.LAST_NAME
		, userInfo.FIRST_NAME
		, userInfo.user_info_id
	FROM dbo.client_cea_map cea
		INNER JOIN dbo.USER_INFO userInfo ON userInfo.user_info_id = cea.user_info_id
	WHERE userInfo.is_history = 0
		AND cea.client_id = @clientID
    
END
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_CEA_P] TO [CBMSApplication]
GO
