SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
NAME:  dbo.Sr_Rfp_Site_Utility_Dtls_Sel_By_Sr_Account_Group_Id                        
                              
DESCRIPTION:  
			To get account utillities and site info for the account RFP initiated
                               
                              
INPUT PARAMETERS:              
	Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
	@Sr_Account_Group_Id INT
    @Is_Bid_Group INT
                                          
OUTPUT PARAMETERS:                   
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                              

USAGE EXAMPLES:              
-----------------------------------------------------------------------------------  
  
	EXEC dbo.Sr_Rfp_Site_Utility_Dtls_Sel_By_Sr_Account_Group_Id 12103458,0
	EXEC dbo.Sr_Rfp_Site_Utility_Dtls_Sel_By_Sr_Account_Group_Id 10011924,1
  
  
 AUTHOR INITIALS:              
             
	Initials	Name              
-----------------------------------------------------------------------------------  
	RR			Raghu Reddy                                    
                               
 MODIFICATIONS:            
             
	Initials	Date        Modification            
-----------------------------------------------------------------------------------  
	RR			2016-04-15	Global Sourcing - Phase3 - GCS-530 Created
                             
******/ 

CREATE PROCEDURE [dbo].[Sr_Rfp_Site_Utility_Dtls_Sel_By_Sr_Account_Group_Id]
      ( 
       @Sr_Account_Group_Id INT
      ,@Is_Bid_Group INT )
AS 
BEGIN  
      SET NOCOUNT ON;
      DECLARE @Lpsize BIGINT
      
      SELECT
            @Lpsize = dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(@Sr_Account_Group_Id, @Is_Bid_Group);
      
      WITH  Cte_Utility
              AS ( SELECT
                        ch.Client_Name
                       ,rtrim(ch.City) + ', ' + ch.State_Name + '(' + ch.Site_name + ')' AS Site_Name
                       ,cha.Account_Vendor_Name
                       ,com.Commodity_Name
                       ,uom.ENTITY_NAME AS UOM
                   FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc
                              ON cha.Account_Id = rfpacc.ACCOUNT_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
                                 AND rfp.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN dbo.Commodity com
                              ON rfp.COMMODITY_TYPE_ID = com.Commodity_Id
                        INNER JOIN dbo.ENTITY uom
                              ON com.Default_UOM_Entity_Type_Id = uom.ENTITY_ID
                   WHERE
                        ( @Is_Bid_Group = 1
                          AND rfpacc.SR_RFP_BID_GROUP_ID = @Sr_Account_Group_Id )
                        OR ( @Is_Bid_Group = 0
                             AND rfpacc.SR_RFP_ACCOUNT_ID = @Sr_Account_Group_Id )
                   GROUP BY
                        ch.Client_Name
                       ,rtrim(ch.City) + ', ' + ch.State_Name + '(' + ch.Site_name + ')'
                       ,cha.Account_Vendor_Name
                       ,com.Commodity_Name
                       ,uom.ENTITY_NAME)
            SELECT
                  cu.Client_Name
                 ,replace(left(Site_Name.Site_Names, len(Site_Name.Site_Names) - 1), '&amp;', '&') AS Site_Name
                 ,replace(left(Utility.Utilities, len(Utility.Utilities) - 1), '&amp;', '&') AS Utility_Vendor
                 ,cast(@Lpsize AS VARCHAR(50)) Volume
                 ,UOM
            FROM
                  Cte_Utility cu
                  CROSS APPLY ( SELECT
                                    cu1.Account_Vendor_Name + ', '
                                FROM
                                    Cte_Utility cu1
                                WHERE
                                    cu1.Account_Vendor_Name IS NOT NULL
                                GROUP BY
                                    cu1.Account_Vendor_Name
                  FOR
                                XML PATH('') ) Utility ( Utilities )
                  CROSS APPLY ( SELECT
                                    cu2.Site_Name + ', '
                                FROM
                                    Cte_Utility cu2
                                WHERE
                                    cu2.Site_Name IS NOT NULL
                                GROUP BY
                                    cu2.Site_Name
                  FOR
                                XML PATH('') ) Site_Name ( Site_Names )
            GROUP BY
                  cu.Client_Name
                 ,replace(left(Site_Name.Site_Names, len(Site_Name.Site_Names) - 1), '&amp;', '&')
                 ,replace(left(Utility.Utilities, len(Utility.Utilities) - 1), '&amp;', '&')
                 ,cu.Commodity_Name
                 ,UOM
      
   
END  
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Site_Utility_Dtls_Sel_By_Sr_Account_Group_Id] TO [CBMSApplication]
GO
