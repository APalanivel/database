SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seContent_Get
	(
		@ContentId int
	)
As
BEGIN
	set nocount on
	 select ContentId
				, ContentLocaleId
				, ContentPlacementId
				, Version
				, CurrentStatusId
				, ContentText
		 from seContent
		where ContentId = @ContentId

END
GO
GRANT EXECUTE ON  [dbo].[seContent_Get] TO [CBMSApplication]
GO
