SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_NYMEX_FORECAST_DATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId        		varchar(10)	          	
	@sessionId     		varchar(20)	          	
	@forecastYear  		int       	          	
	@forecastType  		int       	          	
	@forecastTypeName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2019-11-29	RM-Budgets Enahancement - Added RM_FORECAST_ID to result set

******/

CREATE PROCEDURE [dbo].[GET_NYMEX_FORECAST_DATES_P]
    @userId VARCHAR(10)
    , @sessionId VARCHAR(20)
    , @forecastYear INT
    , @forecastType INT
    , @forecastTypeName VARCHAR(200)
AS
    SET NOCOUNT ON;
    IF @forecastYear = -1
        BEGIN

            SELECT
                FORECAST_AS_OF_DATE AS_OF_DATE
                , RM_FORECAST_ID
            FROM
                RM_FORECAST
            WHERE
                FORECAST_TYPE_ID = (   SELECT
                                            ENTITY_ID
                                       FROM
                                            ENTITY
                                       WHERE
                                            ENTITY_TYPE = @forecastType
                                            AND ENTITY_NAME = @forecastTypeName)
            ORDER BY
                FORECAST_AS_OF_DATE DESC;
        END;
    ELSE
        SELECT
            FORECAST_FROM_DATE
            , FORECAST_TO_DATE
            , RM_FORECAST_ID
        FROM
            RM_FORECAST
        WHERE /*datepart( YEAR, FORECAST_FROM_DATE )= @forecastYear and*/
            FORECAST_TYPE_ID = (   SELECT
                                        ENTITY_ID
                                   FROM
                                        ENTITY
                                   WHERE
                                        ENTITY_TYPE = @forecastType
                                        AND ENTITY_NAME = @forecastTypeName)
        ORDER BY
            FORECAST_FROM_DATE DESC;

GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_FORECAST_DATES_P] TO [CBMSApplication]
GO
