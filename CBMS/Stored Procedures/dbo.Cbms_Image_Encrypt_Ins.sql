SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******  
NAME: dbo.Cbms_Image_Encrypt_Ins  
  
DESCRIPTION:   
  Procedure is to insert CBMS_Image_Ids Encrypted values

INPUT PARAMETERS:      
      Name				DataType       Default   Description      
------------------------------------------------------------------------------------------------------------------------      
  @Client_Id			INT
      
OUTPUT PARAMETERS:      
      Name                DataType          Default     Description      
------------------------------------------------------------------------------------------------------------------------     
	CBMS_IMAGE_ID			INT

USAGE EXAMPLES:  
------------------------------------------------------------------------------------------------------------------------  

  
AUTHOR INITIALS:  
 Initials	Name
------------------------------------------------------------------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------------------------------------------------------------------
	KVK		10/16/2019		Created
******/
CREATE PROCEDURE [dbo].[Cbms_Image_Encrypt_Ins]
(
    @tvp_Cbms_Image_Id_Encrypt dbo.tvp_Cbms_Image_Id_Encrypt READONLY
)
AS
BEGIN

    SET NOCOUNT ON;

    INSERT INTO dbo.Cbms_Image_Encrypt
    (
        CBMS_IMAGE_ID,
        Encrypt_Cbms_Image_Id
    )
    SELECT CBMS_IMAGE_ID,
           Encrypt_Cbms_Image_Id
    FROM @tvp_Cbms_Image_Id_Encrypt;


END;

GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Encrypt_Ins] TO [CBMSApplication]
GO
