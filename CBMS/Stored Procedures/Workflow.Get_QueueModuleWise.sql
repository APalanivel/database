SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:    [Workflow].[Get_QueueModuleWise]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @Workflow_Queue_Category_Cd int 
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:   EXEC [Workflow].[Get_QueueModuleWise]  
Workflow_Queue_Category_Cd =49---int 
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AP	Arunkumar Palanivel 
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AP			OCT 17 2019 
  
******/  

 
CREATE PROCedure [Workflow].[Get_QueueModuleWise]        
   (        
  @Workflow_Queue_Category_name varchar(50)        
   )        
AS        
BEGIN        
      
  SELECT wq.Workflow_Queue_Id as Module_Id, wq.Queue_Display_Name Module_Name, CONVERT(BIT,1) Is_Default  , null NavUrl    
  FROM Workflow.Workflow_Queue wq
  join code C 
on wq.Workflow_Queue_Category_Cd = c.Code_Id 
where c.Code_value =@Workflow_Queue_Category_name 
and wq.is_active = 1   
      
END 

GO
GRANT EXECUTE ON  [Workflow].[Get_QueueModuleWise] TO [CBMSApplication]
GO
