SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_CEMS_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
select	rcem.CEM_USER_ID,
	userinfo.LAST_NAME,
	userinfo.FIRST_NAME,
	entity.ENTITY_ID,
	entity.ENTITY_NAME
	
from	CLIENT_CEM_MAP cemMap,
	RM_CEM_TEAM rcem,
	USER_INFO userinfo,
	ENTITY entity
	
where	cemMap.CLIENT_ID =@clientId AND 
	cemMap.USER_INFO_ID = rcem.CEM_USER_ID AND	
	rcem.CEM_USER_ID=userinfo.USER_INFO_ID  AND
	rcem.CEM_TEAM_TYPE_ID=entity.ENTITY_TYPE
GO
GRANT EXECUTE ON  [dbo].[GET_CEMS_UNDER_CLIENT_P] TO [CBMSApplication]
GO
