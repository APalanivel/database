SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_CLIENT_NAMES_P
	@clientID int
	as
	begin
		set nocount on

		select	client_name 
		from	client 
		where	client_id = @clientID

	end


GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_NAMES_P] TO [CBMSApplication]
GO
