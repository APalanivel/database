
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******              
    
NAME: [DBO].[Report_DE_Get_Demand_Billing_Info]      
         
DESCRIPTION:     
     
          
INPUT PARAMETERS:              
 NAME   DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
 @Client_Id  VARCHAR(MAX)  
 @Country_Id VARCHAR(MAX)  
 @EP_Selected BIT   NULL  
 @EP_UOM  INT  
 @NG_Selected BIT   NULL  
 @NG_UOM  INT  
 @Other_Service VARCHAR(1000)  
 @Other_Service_UOM INT  NULL  
 @Currency_Unit_Id INT  
 @EL_Demand_Uom INT   NULL  
 @Service_Month DATE   
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            
      
 EXEC Report_DE_Get_Demand_Billing_Info  '235',1,12,1,16,NULL,NULL,3,12,'2012-01-01'    
    
AUTHOR INITIALS:              
INITIALS NAME              
------------------------------------------------------------    
AKR   Ashok Kumar Raju    
    
MODIFICATIONS    
    
INITIALS DATE  MODIFICATION    
------------------------------------------------------------              
AKR   2012-08-14 CREATED    
AKR   2012-12-10 Added Contract Start and End Dates to the select list. Added Country to the parameters list  
AKR   2014-08-08 Modifed the code to include Site_Reference_Number column.
--This sproc is being refernced by 2 SSRS Reports, changes are to be considered to the reports accordingly
Cost_Usage_Demand_Billing_Days.rdl, Cost_Usage_Demand_Billing_Days_With_Site_Number.rdl
*/    
    
CREATE PROCEDURE dbo.Report_DE_Get_Demand_Billing_Info
      ( 
       @Client_Id VARCHAR(MAX)
      ,@Country_Id VARCHAR(MAX)
      ,@EP_Selected BIT = NULL
      ,@EP_UOM INT
      ,@NG_Selected BIT = NULL
      ,@NG_UOM INT
      ,@Other_Service VARCHAR(1000)
      ,@Other_Service_UOM INT = NULL
      ,@Currency_Unit_Id INT
      ,@EL_Demand_Uom INT = NULL
      ,@Service_Month DATE )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
       
      DECLARE @Client_Id_List TABLE
            ( 
             Client_Id INT PRIMARY KEY )   
                
      DECLARE @Country_Id_List TABLE
            ( 
             Country_Id INT PRIMARY KEY )   
               
      INSERT      INTO @Country_Id_List
                  ( 
                   Country_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Country_Id, ',') us    
                          
      INSERT      INTO @Client_Id_List
                  ( 
                   Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Client_Id, ',') us                     
       
      DECLARE @Other_Service_List TABLE
            ( 
             Commodity_Id INT PRIMARY KEY )    
                 
      DECLARE @Currency VARCHAR(10)    
         
      SELECT
            @Currency = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id    
                
    
      INSERT      INTO @Other_Service_List
                  ( 
                   Commodity_Id )
                  SELECT
                        segments
                  FROM
                        ( SELECT
                              us.Segments
                          FROM
                              dbo.ufn_split(@Other_Service, ',') us
                          UNION
                          SELECT
                              Commodity_Id
                          FROM
                              dbo.Commodity c
                          WHERE
                              Commodity_Name = 'Electric Power'
                              AND @EP_Selected = 1
                          UNION
                          SELECT
                              Commodity_Id
                          FROM
                              dbo.Commodity c
                          WHERE
                              Commodity_Name = 'Natural Gas'
                              AND @NG_Selected = 1 ) k    
              
                                         
      CREATE TABLE #CUDEmand
            ( 
             [Account ID] INT
            ,[Flow Period] DATE
            ,Client_Name VARCHAR(200)
            ,[Site Name] VARCHAR(200)
            ,[Commodity] VARCHAR(50)
            ,[Vendor] VARCHAR(200)
            ,[Vendor Account Number] VARCHAR(500)
            ,[Consumption] DECIMAL(28, 10)
            ,[Consumption UOM] VARCHAR(200)
            ,[Actual UOM] VARCHAR(200)
            ,[Total Charges] DECIMAL(28, 10)
            ,[ActualDemand] DECIMAL(28, 10)
            ,[BillingDemand] DECIMAL(28, 10)
            ,[Account Type] CHAR(8)
            ,[Consumptions Units] VARCHAR(200)
            ,[Demand Units] VARCHAR(200)
            ,Site_Reference_Number VARCHAR(30) )    
       
       
      CREATE TABLE #BillingInfo
            ( 
             [Account Number] VARCHAR(500)
            ,[account id] INT
            ,[Service Days] INT
            ,[Service Start] DATE
            ,[Service End] DATE
            ,service_month DATE
            ,account_type CHAR(8)
            ,CONTRACT_ID INT )    
       
       
      INSERT      INTO #CUDEmand
                  ( 
                   [Account ID]
                  ,[Flow Period]
                  ,Client_Name
                  ,[Site Name]
                  ,Commodity
                  ,Vendor
                  ,[Vendor Account Number]
                  ,Consumption
                  ,[Consumption UOM]
                  ,[Actual UOM]
                  ,[Total Charges]
                  ,ActualDemand
                  ,BillingDemand
                  ,[Account Type]
                  ,[Consumptions Units]
                  ,[Demand Units]
                  ,Site_Reference_Number )
                  SELECT
                        cuad.ACCOUNT_ID [Account ID]
                       ,cuad.Service_Month [Flow Period]
                       ,ch.Client_Name Client_Name
                       ,ch.site_name [Site Name]
                       ,com.Commodity_Name [Commodity]
                       ,cha.Account_Vendor_Name [Vendor]
                       ,cha.Display_Account_Number [Vendor Account Number]
                       ,CASE WHEN Bucket_Name = 'Total Usage'
                                  AND com.Commodity_Name = 'Electric Power' THEN ( cuad.Bucket_Value * cuus.conversion_factor )
                             WHEN Bucket_Name = 'Total Usage'
                                  AND com.Commodity_Name = 'Natural Gas' THEN ( cuad.Bucket_Value * cung.conversion_factor )
                             WHEN Bucket_Name = 'Volume' THEN ( cuad.Bucket_Value * cugp.conversion_factor )
                        END [Consumption]
                       ,CASE WHEN Bucket_Name = 'Total Usage'
                                  AND com.Commodity_Name = 'Electric Power' THEN ( eel.entity_name )
                             WHEN Bucket_Name = 'Total Usage'
                                  AND com.Commodity_Name = 'Natural Gas' THEN ( eng.entity_name )
                             WHEN Bucket_Name = 'Volume' THEN ( eot.entity_name )
                        END [Consumption UOM]
                       ,e.entity_Name [Actual UOM]
                       ,CASE WHEN Bucket_Name = 'Total Cost' THEN cuad.Bucket_Value * cur.conversion_factor
                        END [Total Charges]
                       ,CASE WHEN Bucket_Name = 'Demand'
                                  AND c.Code_Value = 'Determinant'
                                  AND com.Commodity_Name = 'Electric Power' THEN cuad.Bucket_Value * cudem.conversion_factor
                        END [ActualDemand]
                       ,CASE WHEN Bucket_Name = 'Billed Demand'
                                  AND c.Code_Value = 'Determinant'
                                  AND com.Commodity_Name = 'Electric Power' THEN cuad.Bucket_Value * cudem.conversion_factor
                        END [BillingDemand]
                       ,cha.Account_Type [Account Type]
                       ,CASE WHEN Bucket_Name IN ( 'Total Usage', 'Volume' ) THEN e.entity_name
                        END [Consumptions Units]
                       ,CASE WHEN Bucket_Name = 'Demand'
                                  AND c.Code_Value = 'Determinant' THEN e.entity_name
                        END [Demand Units]
                       ,ch.Site_Reference_Number
                  FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN @Client_Id_List cl
                              ON ch.Client_Id = cl.client_Id
                        INNER JOIN @Country_Id_List cou
                              ON ch.Country_Id = cou.Country_Id
                        INNER JOIN dbo.Cost_Usage_Account_Dtl cuad
                              ON cha.Account_Id = cuad.ACCOUNT_ID
                                 AND cha.Client_Hier_Id = cuad.Client_Hier_Id
                        INNER JOIN dbo.Bucket_Master bm
                              ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
                        INNER JOIN dbo.Commodity com
                              ON bm.Commodity_Id = com.Commodity_Id
                        INNER JOIN @Other_Service_List osl
                              ON osl.commodity_id = com.commodity_Id
                        INNER JOIN dbo.Code c
                              ON c.Code_Id = bm.Bucket_Type_Cd
                        LEFT JOIN entity e
                              ON e.entity_id = cuad.UOM_Type_Id
                        LEFT JOIN entity e1
                              ON e1.entity_id = cuad.UOM_Type_Id
                        LEFT JOIN consumption_unit_conversion cuus
                              ON cuus.base_unit_id = cuad.UOM_Type_Id
                                 AND cuus.converted_unit_id = @EP_UOM --kwh    
                        LEFT JOIN entity eel
                              ON eel.entity_id = @EP_UOM
                        LEFT JOIN consumption_unit_conversion cugp
                              ON cugp.base_unit_id = cuad.UOM_Type_Id
                                 AND cugp.converted_unit_id = com.Default_UOM_Entity_Type_Id --kwh    
                        LEFT JOIN entity eot
                              ON eot.entity_id = com.Default_UOM_Entity_Type_Id
                        LEFT JOIN consumption_unit_conversion cung
                              ON cung.base_unit_id = cuad.UOM_Type_Id
                                 AND cung.converted_unit_id = @NG_UOM--THerm    
                        LEFT JOIN entity eng
                              ON eng.entity_id = @NG_UOM
                        LEFT JOIN currency_unit_conversion cur
                              ON cur.base_unit_id = cuad.CURRENCY_UNIT_ID
                                 AND cur.converted_unit_id = @Currency_Unit_Id -- usd    
                                 AND cur.conversion_date = cuad.Service_Month
                                 AND cur.currency_group_id = ch.client_currency_group_id
                        LEFT JOIN consumption_unit_conversion cudem
                              ON cudem.base_unit_id = cuad.UOM_Type_Id
                                 AND cudem.converted_unit_id = @EL_Demand_Uom --kw    
                  WHERE
                        Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Demand', 'Billed Demand', 'Volume' )
                        AND cuad.Service_Month >= @Service_Month
                        AND ch.Site_Not_Managed = 0
                        AND cha.Account_Not_Managed = 0
                  GROUP BY
                        cuad.ACCOUNT_ID
                       ,cuad.Service_Month
                       ,ch.Client_Name
                       ,ch.site_name
                       ,com.Commodity_Name
                       ,cha.Account_Vendor_Name
                       ,cha.Display_Account_Number
                       ,Bucket_Name
                       ,Bucket_Value
                       ,cuus.conversion_factor
                       ,cung.conversion_factor
                       ,cur.conversion_factor
                       ,cudem.conversion_factor
                       ,cugp.conversion_factor
                       ,c.Code_Value
                       ,cha.Account_Type
                       ,e.entity_name
                       ,eel.entity_name
                       ,eng.entity_name
                       ,eot.entity_name
                       ,e1.entity_name --BillingInfo   
                       ,ch.Site_Reference_Number 
                     
      CREATE CLUSTERED INDEX ix_#CUDEmand_Account_Number_Account_Id_Flow_Period_Account_Type ON #CUDEmand([Vendor Account Number],[Account ID], [Flow Period],[Account Type])                    
                     
      INSERT      INTO #BillingInfo
                  ( 
                   [Account Number]
                  ,[account id]
                  ,[Service Days]
                  ,[Service Start]
                  ,[Service End]
                  ,service_month
                  ,account_type
                  ,CONTRACT_ID )
                  SELECT
                        cha.Display_Account_Number [Account Number]
                       ,cuabd.account_id [account id]
                       ,cuabd.billing_days [Service Days]
                       ,cuabd.Billing_Start_Dt [Service Start]
                       ,cuabd.Billing_End_Dt [Service End]
                       ,cuabd.service_month
                       ,cha.account_type
                       ,cha.Supplier_Contract_ID
                  FROM
                        dbo.Cost_Usage_Account_Billing_Dtl cuabd
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cuabd.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Client_Id_List cl
                              ON cl.client_id = ch.CLIENT_ID
                        INNER JOIN @Country_Id_List cou
                              ON ch.Country_Id = cou.Country_Id
                  WHERE
                        cuabd.SERVICE_MONTH >= @Service_Month
                  GROUP BY
                        cha.Display_Account_Number
                       ,cuabd.account_id
                       ,cuabd.billing_days
                       ,cuabd.Billing_Start_Dt
                       ,cuabd.Billing_End_Dt
                       ,cuabd.service_month
                       ,cha.account_type
                       ,cha.Supplier_Contract_ID  
  
                           
      CREATE CLUSTERED INDEX ix_#BillingInfo_Account_Number_Account_Id_Flow_Period_Account_Type ON #BillingInfo([Account Number],[Account ID], service_month,account_type)    
                     
      SELECT
            cc.[Account ID]
           ,CONVERT(DATE, cc.[Flow Period]) [Flow Period]
           ,cc.Client_Name
           ,cc.[Site Name]
           ,cc.[Commodity]
           ,cc.[Vendor]
           ,cc.[Vendor Account Number]
           ,bc.[Service Days]
           ,CONVERT(DATE, bc.[Service Start]) [Service Start]
           ,CONVERT(DATE, bc.[Service End]) [Service End]
           ,SUM(cc.[Consumption]) [Consumption]
           ,MAX(cc.[Consumption UOM]) [Consumption UOM]
           ,MAX(cc.[Actual UOM]) [Actual UOM]
           ,SUM(cc.[Total Charges]) [Total Charges]
           ,@Currency CurrencyUOM
           ,SUM(cc.[ActualDemand]) [ActualDemand]
           ,SUM(cc.[BillingDemand]) [BillingDemand]
           ,MAX([Demand Units]) [Demand Units]
           ,cc.[Account Type]
           ,con.CONTRACT_START_DATE
           ,con.CONTRACT_END_DATE
           ,cc.Site_Reference_Number
      FROM
            #CUDEmand cc
            LEFT JOIN #BillingInfo bc
                  ON bc.[account number] = cc.[vendor account number]
                     AND bc.[account id] = cc.[account id]
                     AND bc.service_month = cc.[flow period]
                     AND bc.account_type = cc.[Account Type]
            LEFT JOIN dbo.CONTRACT con
                  ON con.CONTRACT_ID = bc.Contract_ID
      GROUP BY
            cc.[Account ID]
           ,cc.[Flow Period]
           ,cc.Client_Name
           ,bc.[Service Start]
           ,bc.[Service End]
           ,cc.[Site Name]
           ,cc.[Commodity]
           ,cc.[Vendor]
           ,cc.[Vendor Account Number]
           ,bc.[Service Days]
           ,cc.[Account Type]
           ,con.CONTRACT_START_DATE
           ,con.CONTRACT_END_DATE
           ,cc.Site_Reference_Number
      ORDER BY
            cc.Client_Name
           ,[Site Name]
           ,[Account ID]
           ,[Vendor Account Number]
           ,[Account Type]
           ,[Commodity]
           ,[Flow Period]    
               
END;  
;
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Get_Demand_Billing_Info] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Get_Demand_Billing_Info] TO [CBMSApplication]
GO
