
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.SR_RFP_CREATE_SOP_P    
    
DESCRIPTION:     
    
    
INPUT PARAMETERS:        
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------        
 @userId VARCHAR,      
 @sessionId VARCHAR,      
 @supplierContactMap VARCHAR(200),      
 @supplierContactId INT,      
 @accountTermId INT,      
 @supplierBOCId INT,      
 @productName VARCHAR(200),      
 @accountGroupId INT,      
 @isBidGroup INT,      
 @priceComments VARCHAR(4000)      
                              
                               
OUTPUT PARAMETERS:             
      Name              DataType          Default     Description        
------------------------------------------------------------        
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
-- Test Insert     
-- exec SR_RFP_CREATE_SOP_P -1,-1,'Hess Corporation~nationalaccounts',31280,185861,22574,'Fixed Price',18530,0,'commentes'      
    
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
  DR  Deana Ritter    
  AKR Ashok Kumar Raju  
MODIFICATIONS    
    
	Initials	Date		Modification    
------------------------------------------------------------    
	DR			08/04/2009  Removed Linked Server Updates    
	DMR			09/10/2010  Modified for Quoted_Identifier    
	AKR			2012-11-29  Modified the Code to Include Is_Broker_Type_Included.
	RR			2016-05-10	GCS-887 Saving Pricing_Comments to SOP bid    
******/    
CREATE PROCEDURE [dbo].[SR_RFP_CREATE_SOP_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@supplierContactMap VARCHAR(200)
      ,@supplierContactId INT
      ,@accountTermId INT
      ,@supplierBOCId INT
      ,@productName VARCHAR(200)
      ,@accountGroupId INT
      ,@isBidGroup INT
      ,@priceComments VARCHAR(4000) )
AS 
BEGIN      
      
      SET NOCOUNT ON;      
      
      DECLARE
            @SOPSummaryId INT
           ,@SOPId INT
           ,@SOPAccountTermId INT
           ,@termId INT
           ,@SOPTermId INT
           ,@SOPBidId INT      
      
      DECLARE
            @summitBidId INT
           ,@supplierCommentsId INT
           ,@supplierServiceId INT      
      
      DECLARE
            @deliveryFuelId INT
           ,@balancingToleranceId INT
           ,@riskManagementId INT
           ,@alternateFuelId INT
           ,@pricingScopeId INT
           ,@miscPowerId INT
           ,@newProductName VARCHAR(200)
           ,@newIsSubProduct BIT      
      
      DECLARE
            @newSummitBidId INT
           ,@newSupplierCommentsId INT
           ,@newSupplierServiceId INT      
      
      DECLARE
            @newDeliveryFuelId INT
           ,@newBalancingToleranceId INT
           ,@newRiskManagementId INT
           ,@newAlternateFuelId INT
           ,@newPricingScopeId INT
           ,@newMiscPowerId INT
           ,@newBidId INT
           ,@is_sdp INT      
      
      DECLARE
            @SOP_TERM_ID INT
           ,@SOP_FROM_MONTH DATETIME
           ,@SOP_TO_MONTH DATETIME
           ,@SOP_NO_OF_MONTHS INT      
      
     
      SELECT
            @SOPSummaryId = SR_RFP_SOP_SUMMARY_ID
      FROM
            dbo.SR_RFP_SOP_SUMMARY
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND IS_BID_GROUP = @isBidGroup      
      
      SELECT
            @SOPId = SR_RFP_SOP_ID
      FROM
            dbo.SR_RFP_SOP
      WHERE
            SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
            AND VENDOR_NAME = @supplierContactMap
            AND SR_RFP_SOP_SUMMARY_ID = @SOPSummaryId      
      
      SELECT
            @termId = SR_RFP_TERM_ID
      FROM
            dbo.SR_RFP_ACCOUNT_TERM
      WHERE
            SR_RFP_ACCOUNT_TERM_ID = @accountTermId
            AND sr_account_group_id = @accountGroupId
            AND is_bid_group = @isBidGroup
            AND is_sop IS NULL      
      
      SELECT
            @SOPTermId = SR_RFP_TERM_ID
      FROM
            dbo.SR_RFP_ACCOUNT_TERM
      WHERE
            SR_RFP_TERM_ID = @termId
            AND SR_RFP_ACCOUNT_TERM.IS_SOP = 1
            AND sr_account_group_id = @accountGroupId
            AND is_bid_group = @isBidGroup      
      
      SELECT
            @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
           ,@supplierCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
           ,@supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID
           ,@newProductName = PRODUCT_NAME
           ,@newIsSubProduct = IS_SUB_PRODUCT
      FROM
            dbo.SR_RFP_BID
      WHERE
            SR_RFP_BID_ID = @supplierBOCId      
    
      SELECT
            @deliveryFuelId = SR_RFP_DELIVERY_FUEL_ID
           ,@balancingToleranceId = SR_RFP_BALANCING_TOLERANCE_ID
           ,@riskManagementId = SR_RFP_RISK_MANAGEMENT_ID
           ,@alternateFuelId = SR_RFP_ALTERNATE_FUEL_ID
           ,@pricingScopeId = SR_RFP_PRICING_SCOPE_ID
           ,@miscPowerId = SR_RFP_MISCELLANEOUS_POWER_ID
      FROM
            dbo.SR_RFP_SUPPLIER_SERVICE
      WHERE
            SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId      
      
      IF @SOPSummaryId = ''
            OR @SOPSummaryId IS NULL 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_SOP_SUMMARY
                              ( 
                               SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP )
                  VALUES
                              ( 
                               @accountGroupId
                              ,@isBidGroup )      
        
                  SELECT
                        @SOPSummaryId = scope_identity()        
            END      
      
      IF @SOPId = ''
            OR @SOPId IS NULL 
            BEGIN      
       
                  INSERT      INTO dbo.SR_RFP_SOP
                              ( 
                               SR_RFP_SOP_SUMMARY_ID
                              ,SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              ,VENDOR_NAME
                              ,COMMENTS )
                  VALUES
                              ( 
                               @SOPSummaryId
                              ,@supplierContactId
                              ,@supplierContactMap
                              ,NULL )      
      
                  SELECT
                        @SOPId = scope_identity()      
      
            END      
      
      
      IF @SOPTermId IS NOT NULL
            AND @SOPTermId > 0 
            BEGIN      
        
                  SELECT
                        @SOPAccountTermId = sr_rfp_account_term_id
                  FROM
                        dbo.sr_rfp_account_term
                  WHERE
                        SR_RFP_TERM_ID = @SOPTermId
                        AND sr_account_group_id = @accountGroupId
                        AND is_bid_group = @isBidGroup
                        AND is_sop = 1      
            END      
      ELSE 
            BEGIN      
        
                  SELECT
                        @is_sdp = is_sdp
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        sr_account_group_id = @accountGroupId
                        AND is_bid_group = @isBidGroup
                        AND SR_RFP_ACCOUNT_TERM_ID = @accountTermId      
        
                  INSERT      INTO dbo.sr_rfp_account_term
                              ( 
                               SR_RFP_TERM_ID
                              ,SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,IS_SOP
                              ,FROM_MONTH
                              ,TO_MONTH
                              ,NO_OF_MONTHS
                              ,IS_SDP )
                              SELECT
                                    SR_RFP_TERM_ID
                                   ,SR_ACCOUNT_GROUP_ID
                                   ,IS_BID_GROUP
                                   ,1
                                   ,FROM_MONTH
                                   ,TO_MONTH
                                   ,NO_OF_MONTHS
                                   ,@is_sdp
                              FROM
                                    dbo.sr_rfp_account_term
                              WHERE
                                    sr_rfp_account_term_id = @accountTermId      
      
                  SELECT
                        @SOPAccountTermId = scope_identity()      
      
     
                  SELECT TOP 1
                        @SOP_TERM_ID = SR_RFP_TERM_ID
                       ,@SOP_FROM_MONTH = FROM_MONTH
                       ,@SOP_TO_MONTH = TO_MONTH
                       ,@SOP_NO_OF_MONTHS = NO_OF_MONTHS
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        SR_RFP_ACCOUNT_TERM_ID = @accountTermId      
      
     
            END      
       
 -- second INSERT INTO SR_RFP_BID_REQUIREMENTS table      
       
      INSERT      INTO dbo.SR_RFP_BID_REQUIREMENTS
                  ( 
                   DELIVERY_POINT
                  ,TRANSPORTATION_LEVEL_TYPE_ID
                  ,NOMINATION_TYPE_ID
                  ,BALANCING_TYPE_ID
                  ,COMMENTS
                  ,DELIVERY_POINT_POWER_TYPE_ID
                  ,SERVICE_LEVEL_POWER_TYPE_ID )
                  SELECT
                        DELIVERY_POINT
                       ,TRANSPORTATION_LEVEL_TYPE_ID
                       ,NOMINATION_TYPE_ID
                       ,BALANCING_TYPE_ID
                       ,COMMENTS
                       ,DELIVERY_POINT_POWER_TYPE_ID
                       ,SERVICE_LEVEL_POWER_TYPE_ID
                  FROM
                        SR_RFP_BID_REQUIREMENTS
                  WHERE
                        SR_RFP_BID_REQUIREMENTS_ID = @summitBidId      
      
      SELECT
            @newSummitBidId = scope_identity()      
       
 -- third INSERT INTO SR_RFP_SUPPLIER_PRICE_COMMENTS table      
      IF @supplierCommentsId != ''
            AND @supplierCommentsId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              ( 
                               IS_CREDIT_APPROVAL
                              ,NO_CREDIT_COMMENTS
                              ,PRICE_RESPONSE_COMMENTS
                              ,Broker_Included_Type_Id
                              ,Pricing_Comments )
                              SELECT
                                    IS_CREDIT_APPROVAL
                                   ,NO_CREDIT_COMMENTS
                                   ,PRICE_RESPONSE_COMMENTS
                                   ,Broker_Included_Type_Id
                                   ,Pricing_Comments
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId      
      
                  SELECT
                        @newSupplierCommentsId = scope_identity()      
      
  -- Added by Nag for archiving price comments      
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              ( 
                               PRICE_RESPONSE_COMMENTS
                              ,ARCHIVED_ON_DATE
                              ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID )
                              SELECT
                                    PRICE_RESPONSE_COMMENTS
                                   ,ARCHIVED_ON_DATE
                                   ,@newSupplierCommentsId
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId      
      
            END      
      
 -- fourth INSERT INTO SR_RFP_DELIVERY_FUEL table      
      IF @deliveryFuelId != ''
            OR @deliveryFuelId IS NOT NULL
            AND @deliveryFuelId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_DELIVERY_FUEL
                              ( 
                               DELIVERY_PIPELINE_COMMENTS
                              ,FUEL_IN_BID_TYPE_ID
                              ,FUEL_IN_BID_VALUE
                              ,TRANSPORT_IN_BID_TYPE_ID
                              ,TRANSPORT_IN_BID_VALUE )
                              SELECT
                                    DELIVERY_PIPELINE_COMMENTS
                                   ,FUEL_IN_BID_TYPE_ID
                                   ,FUEL_IN_BID_VALUE
                                   ,TRANSPORT_IN_BID_TYPE_ID
                                   ,TRANSPORT_IN_BID_VALUE
                              FROM
                                    dbo.SR_RFP_DELIVERY_FUEL
                              WHERE
                                    SR_RFP_DELIVERY_FUEL_ID = @deliveryFuelId      
      
                  SELECT
                        @newDeliveryFuelId = scope_identity()      
      
            END      
      
      IF @balancingToleranceId != ''
            OR @balancingToleranceId IS NOT NULL
            AND @balancingToleranceId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_BALANCING_TOLERANCE
                              ( 
                               IS_PRICE_VARYING
                              ,VOLUME_TOLERANCE
                              ,ABOVE_TOLERANCE_COMMENTS
                              ,BELOW_TOLERANCE_COMMENTS
                              ,BALANCING_FREQUENCY_TYPE_ID
                              ,TOLERANCE_COMMENTS )
                              SELECT
                                    IS_PRICE_VARYING
                                   ,VOLUME_TOLERANCE
                                   ,ABOVE_TOLERANCE_COMMENTS
                                   ,BELOW_TOLERANCE_COMMENTS
                                   ,BALANCING_FREQUENCY_TYPE_ID
                                   ,TOLERANCE_COMMENTS
                              FROM
                                    dbo.SR_RFP_BALANCING_TOLERANCE
                              WHERE
                                    SR_RFP_BALANCING_TOLERANCE_ID = @balancingToleranceId      
      
                  SELECT
                        @newBalancingToleranceId = scope_identity()      
      
            END      
      
      IF @riskManagementId = ''
            OR @riskManagementId IS NOT NULL
            AND @riskManagementId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_RISK_MANAGEMENT
                              ( 
                               SWAP_INDEX_NYMEX_TYPE_ID
                              ,SWAP_INDEX_NYMEX_COMMENTS
                              ,SWAP_CONTRACT_FIXED_TYPE_ID
                              ,SWAP_CONTRACT_FIXED_COMMENTS
                              ,EXPECTED_VARIANCE
                              ,FIXED_PRICE_TRIGGER_TYPE_ID
                              ,FIXED_PRICE_TRIGGER_COMMENTS )
                              SELECT
                                    SWAP_INDEX_NYMEX_TYPE_ID
                                   ,SWAP_INDEX_NYMEX_COMMENTS
                                   ,SWAP_CONTRACT_FIXED_TYPE_ID
                                   ,SWAP_CONTRACT_FIXED_COMMENTS
                                   ,EXPECTED_VARIANCE
                                   ,FIXED_PRICE_TRIGGER_TYPE_ID
                                   ,FIXED_PRICE_TRIGGER_COMMENTS
                              FROM
                                    dbo.SR_RFP_RISK_MANAGEMENT
                              WHERE
                                    SR_RFP_RISK_MANAGEMENT_ID = @riskManagementId      
      
                  SELECT
                        @newRiskManagementId = scope_identity()      
      
            END      
       
      IF @alternateFuelId != ''
            OR @alternateFuelId IS NOT NULL
            AND @alternateFuelId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_ALTERNATE_FUEL
                              ( 
                               BUYER_LIQUIDATE_TYPE_ID
                              ,COMMENTS )
                              SELECT
                                    BUYER_LIQUIDATE_TYPE_ID
                                   ,COMMENTS
                              FROM
                                    dbo.SR_RFP_ALTERNATE_FUEL
                              WHERE
                                    SR_RFP_ALTERNATE_FUEL_ID = @alternateFuelId      
      
                  SELECT
                        @newAlternateFuelId = scope_identity()      
      
            END      
      
      IF @pricingScopeId != ''
            OR @pricingScopeId IS NOT NULL
            AND @pricingScopeId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_PRICING_SCOPE
                              ( 
                               PRICE_INCLUDE_LOSES_TYPE_ID
                              ,LOSS_PERCENT
                              ,PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID
                              ,ANCILIARY_SERVICE_ESTIMATED_COST
                              ,PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID
                              ,CAPACITY_CHARGES_ESTIMATED_COST
                              ,PRICE_INCLUDE_ISO_CHARGES_TYPE_ID
                              ,ISO_CHARGES_ESTIMATED_COST
                              ,PRICE_INCLUDE_TRANSMISSION_TYPE_ID
                              ,TRANSMISSION_ESTIMATED_COST
                              ,PRICE_INCLUDE_TAXES_TYPE_ID
                              ,TAXES_ESTIMATED_COST
                              ,OTHER_CHARGES
                              ,DEMAND_CAP_APPLICABLE_TYPE_ID
                              ,CAP_FLOOR_PRICE )
                              SELECT
                                    PRICE_INCLUDE_LOSES_TYPE_ID
                                   ,LOSS_PERCENT
                                   ,PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID
                                   ,ANCILIARY_SERVICE_ESTIMATED_COST
                                   ,PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID
                                   ,CAPACITY_CHARGES_ESTIMATED_COST
                                   ,PRICE_INCLUDE_ISO_CHARGES_TYPE_ID
                                   ,ISO_CHARGES_ESTIMATED_COST
                                   ,PRICE_INCLUDE_TRANSMISSION_TYPE_ID
                                   ,TRANSMISSION_ESTIMATED_COST
                                   ,PRICE_INCLUDE_TAXES_TYPE_ID
                                   ,TAXES_ESTIMATED_COST
                                   ,OTHER_CHARGES
                                   ,DEMAND_CAP_APPLICABLE_TYPE_ID
                                   ,CAP_FLOOR_PRICE
                              FROM
                                    dbo.SR_RFP_PRICING_SCOPE
                              WHERE
                                    SR_RFP_PRICING_SCOPE_ID = @pricingScopeId      
      
                  SELECT
                        @newPricingScopeId = scope_identity()      
      
            END      
      
      IF @miscPowerId != ''
            OR @miscPowerId IS NOT NULL
            AND @miscPowerId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_MISCELLANEOUS_POWER
                              ( 
                               BLOCK_SIZES
                              ,PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID
                              ,PERCENT_GREEN_ENERGY
                              ,CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID
                              ,CONVERT_PRODUCT_FIXED_PRICE_COMMENTS
                              ,BUYER_BLOCK_HEDGES_TYPE_ID
                              ,BUYER_BLOCK_HEDGES_COMMENTS
                              ,HEAT_PRICING_TRIGGERS_TYPE_ID
                              ,HEAT_PRICING_TRIGGERS_COMMENTS )
                              SELECT
                                    BLOCK_SIZES
                                   ,PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID
                                   ,PERCENT_GREEN_ENERGY
                                   ,CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID
                                   ,CONVERT_PRODUCT_FIXED_PRICE_COMMENTS
                                   ,BUYER_BLOCK_HEDGES_TYPE_ID
                                   ,BUYER_BLOCK_HEDGES_COMMENTS
                                   ,HEAT_PRICING_TRIGGERS_TYPE_ID
                                   ,HEAT_PRICING_TRIGGERS_COMMENTS
                              FROM
                                    dbo.SR_RFP_MISCELLANEOUS_POWER
                              WHERE
                                    SR_RFP_MISCELLANEOUS_POWER_ID = @miscPowerId      
      
                  SELECT
                        @newMiscPowerId = scope_identity()      
      
            END      
       
      IF @newDeliveryFuelId = ''
            OR @newDeliveryFuelId IS NULL 
            BEGIN      
      
                  SELECT
                        @newDeliveryFuelId = NULL      
      
            END      
      
      IF @newBalancingToleranceId = ''
            OR @newBalancingToleranceId IS NULL 
            BEGIN      
      
                  SELECT
                        @newBalancingToleranceId = NULL      
      
            END      
      
      IF @newRiskManagementId = ''
            OR @newRiskManagementId IS NULL 
            BEGIN      
      
                  SELECT
                        @newRiskManagementId = NULL      
      
            END      
      
      IF @newAlternateFuelId = ''
            OR @newAlternateFuelId IS NULL 
            BEGIN      
      
                  SELECT
                        @newAlternateFuelId = NULL      
      
            END      
      
      IF @newPricingScopeId = ''
            OR @newPricingScopeId IS NULL 
            BEGIN      
      
                  SELECT
                        @newPricingScopeId = NULL      
      
            END      
      
      IF @newMiscPowerId = ''
            OR @newMiscPowerId IS NULL 
            BEGIN      
      
                  SELECT
                        @newMiscPowerId = NULL      
            END      
      
      IF @newDeliveryFuelId IS NOT NULL
            OR @newBalancingToleranceId IS NOT NULL
            OR @newRiskManagementId IS NOT NULL
            OR @newAlternateFuelId IS NOT NULL
            OR @newPricingScopeId IS NOT NULL
            OR @newMiscPowerId IS NOT NULL 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_SERVICE
                              ( 
                               SR_RFP_DELIVERY_FUEL_ID
                              ,SR_RFP_BALANCING_TOLERANCE_ID
                              ,SR_RFP_RISK_MANAGEMENT_ID
                              ,SR_RFP_ALTERNATE_FUEL_ID
                              ,SR_RFP_PRICING_SCOPE_ID
                              ,SR_RFP_MISCELLANEOUS_POWER_ID )
                  VALUES
                              ( 
                               @newDeliveryFuelId
                              ,@newBalancingToleranceId
                              ,@newRiskManagementId
                              ,@newAlternateFuelId
                              ,@newPricingScopeId
                              ,@newMiscPowerId )      
      
                  SELECT
                        @newSupplierServiceId = scope_identity()      
      
            END      
      
      IF @newSupplierServiceId = ''
            OR @newSupplierServiceId IS NULL 
            BEGIN      
      
                  SELECT
                        @newSupplierServiceId = NULL      
  
            END      
      
      INSERT      INTO dbo.SR_RFP_BID
                  ( 
                   PRODUCT_NAME
                  ,SR_RFP_BID_REQUIREMENTS_ID
                  ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  ,SR_RFP_SUPPLIER_SERVICE_ID
                  ,IS_SUB_PRODUCT )
      VALUES
                  ( 
                   @newProductName
                  ,@newSummitBidId
                  ,@newSupplierCommentsId
                  ,@newSupplierServiceId
                  ,@newIsSubProduct )      
      
      SELECT
            @newBidId = scope_identity()      
      
      INSERT      INTO dbo.SR_RFP_SOP_DETAILS
                  ( 
                   SR_RFP_SOP_ID
                  ,SR_RFP_ACCOUNT_TERM_ID
                  ,PRODUCT_NAME
                  ,SR_RFP_BID_ID
                  ,PRICE_RESPONSE_COMMENTS
                  ,IS_RECOMMENDED )
      VALUES
                  ( 
                   @SOPId
                  ,@SOPAccountTermId
                  ,@productName
                  ,@newBidId
                  ,@priceComments
                  ,0 )      
      
END;

;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_SOP_P] TO [CBMSApplication]
GO
