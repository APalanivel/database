SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[p2p_i_cbms_image14185353070538331998]
		@c1 int,
		@c2 int,
		@c3 varchar(200),
		@c4 datetime,
		@c5 smallint,
		@c6 decimal(18,0),
		@c7 varchar(200),
		@c8 int,
		@c9 bit,
		@c10 varchar(255),
		@c11 int,
		@c12 varchar(255),
		@c13 varchar(255),
		@c14 int,
		@c15 datetime,
		@c16 uniqueidentifier
	,@MSp2pPostVersion varbinary(32) 
as
begin  
begin try
	insert into [dbo].[cbms_image](
		[CBMS_IMAGE_ID],
		[CBMS_IMAGE_TYPE_ID],
		[CBMS_DOC_ID],
		[DATE_IMAGED],
		[BILLING_DAYS_ADJUSTMENT],
		[CBMS_IMAGE_SIZE],
		[CONTENT_TYPE],
		[INV_SOURCED_IMAGE_ID],
		[is_reported],
		[Cbms_Image_Path],
		[App_ConfigID],
		[CBMS_Image_Directory],
		[CBMS_Image_FileName],
		[CBMS_Image_Location_Id],
		[Last_Change_Ts],
		[msrepl_tran_version],
		$sys_p2p_cd_id
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16,
		@MSp2pPostVersion	) 
end try
begin catch
if @@error in (2627, 2601) 
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 5
		,@conflict_type_txt nvarchar(20) = N'Insert-Insert'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[cbms_image] 
where [CBMS_IMAGE_ID] = @c1
	if @@rowcount = 0  
			exec sys.sp_replrethrow
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[cbms_image]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22825,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22824,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_cbms_image] (
		[CBMS_IMAGE_ID],
		[CBMS_IMAGE_TYPE_ID],
		[CBMS_DOC_ID],
		[DATE_IMAGED],
		[BILLING_DAYS_ADJUSTMENT],
		[CBMS_IMAGE_SIZE],
		[CONTENT_TYPE],
		[INV_SOURCED_IMAGE_ID],
		[is_reported],
		[Cbms_Image_Path],
		[App_ConfigID],
		[CBMS_Image_Directory],
		[CBMS_Image_FileName],
		[CBMS_Image_Location_Id],
		[Last_Change_Ts],
		[msrepl_tran_version]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,NULL
			,NULL
		insert [dbo].[conflict_dbo_cbms_image] (

		[CBMS_IMAGE_ID],
		[CBMS_IMAGE_TYPE_ID],
		[CBMS_DOC_ID],
		[DATE_IMAGED],
		[BILLING_DAYS_ADJUSTMENT],
		[CBMS_IMAGE_SIZE],
		[CONTENT_TYPE],
		[INV_SOURCED_IMAGE_ID],
		[is_reported],
		[Cbms_Image_Path],
		[App_ConfigID],
		[CBMS_Image_Directory],
		[CBMS_Image_FileName],
		[CBMS_Image_Location_Id],
		[Last_Change_Ts],
		[msrepl_tran_version]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[CBMS_IMAGE_ID],
		[CBMS_IMAGE_TYPE_ID],
		[CBMS_DOC_ID],
		[DATE_IMAGED],
		[BILLING_DAYS_ADJUSTMENT],
		[CBMS_IMAGE_SIZE],
		[CONTENT_TYPE],
		[INV_SOURCED_IMAGE_ID],
		[is_reported],
		[Cbms_Image_Path],
		[App_ConfigID],
		[CBMS_Image_Directory],
		[CBMS_Image_FileName],
		[CBMS_Image_Location_Id],
		[Last_Change_Ts],
		[msrepl_tran_version]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[cbms_image] 

where [CBMS_IMAGE_ID] = @c1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[cbms_image] set
		[CBMS_IMAGE_TYPE_ID] = @c2,
		[CBMS_DOC_ID] = @c3,
		[DATE_IMAGED] = @c4,
		[BILLING_DAYS_ADJUSTMENT] = @c5,
		[CBMS_IMAGE_SIZE] = @c6,
		[CONTENT_TYPE] = @c7,
		[INV_SOURCED_IMAGE_ID] = @c8,
		[is_reported] = @c9,
		[Cbms_Image_Path] = @c10,
		[App_ConfigID] = @c11,
		[CBMS_Image_Directory] = @c12,
		[CBMS_Image_FileName] = @c13,
		[CBMS_Image_Location_Id] = @c14,
		[Last_Change_Ts] = @c15,
		[msrepl_tran_version] = @c16		,$sys_p2p_cd_id = @MSp2pPostVersion

where [CBMS_IMAGE_ID] = @c1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[cbms_image]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
else  
	EXEC sys.sp_replrethrow
end catch 
end  
GO
