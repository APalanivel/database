SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[cbmsAddress_Get]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN
	
	   select a.address_line1
		, a.address_line2
		, a.city
		, st.state_name
		, a.zipcode
		, a.is_primary_address
		from address a
		join state st on st.state_id = a.state_id
		where a.address_parent_id = @site_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsAddress_Get] TO [CBMSApplication]
GO
