SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*********      
NAME:  dbo.Master_Variance_Test_SEL        
       
DESCRIPTION:  Used to select all the Master level variance data        
      
INPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
              
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:         
      
 EXEC dbo.Master_Variance_Test_SEL   1  

 EXEC dbo.Master_Variance_Test_SEL   2   
      
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
NK   Nageswara Rao Kosuri 
SCH  Satish Chadarajupalli              
      
      
Initials Date  Modification        
------------------------------------------------------------        
NK  10/13/2009  Created      
HG  02/05/2010 Removed the hard coded commodity_id and pivot, logic moved to application as we will get more commodity added as a part of other fuel to do variance test.      
     Commodity table added to get the commodity name.   
SCH	01/03/2020    
******/

CREATE PROCEDURE [dbo].[Master_Variance_Test_SEL]
      (
      @variance_process_Engine_id INT,
	  @start_index INT =NULL,
	  @end_index INT = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

			SET @start_index = isnull (@start_index,1)
			SET @end_index = isnull (@end_index,2147483647)



            IF EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Processing_Engine VP
                              JOIN
                              dbo.Code C
                                    ON VP.Variance_Engine_Cd = C.Code_Id
                        WHERE C.Code_Dsc = 'Advanced Variance Test'
                              AND   VP.Variance_Processing_Engine_Id = @variance_process_Engine_id )
                  BEGIN

				  WITH cte AS (

				  SELECT row_number() OVER (ORDER BY A.category) AS row_num,* FROM 

				  (

                        SELECT      DISTINCT
						
                                    C.Code_Value AS category
									,  C.Code_Id AS category_id
                                  , 'Linear Regression/Analogous Account Backup' AS Description
                                  , VMAP.Commodity_Id
                                  , CM.Commodity_Name 
                        FROM        dbo.Variance_Parameter_Commodity_Map VMAP
                                    JOIN
                                    dbo.Commodity CM
                                          ON CM.Commodity_Id = VMAP.Commodity_Id
                                    JOIN
                                    dbo.Variance_Parameter VP
                                          ON VP.Variance_Parameter_Id = VMAP.Variance_Parameter_Id
                                    JOIN
                                    dbo.Code C
                                          ON C.Code_Id = VP.Variance_Category_Cd
                        WHERE       VMAP.variance_process_Engine_id = @variance_process_Engine_id)A)


						SELECT cte.category,
						cte.category_id,
						cte.Description,
						cte.Commodity_Id,
						cte.Commodity_Name FROM cte 
						WHERE row_num BETWEEN @start_index AND @end_index

                  END;

            ELSE
                  BEGIN

                        SELECT
		vt.variance_test_id
		,vt.is_inclusive
		,vt.is_multiple_condition
		,vr.Variance_Rule_Id
		,cd.code_id AS Category_Id
		,cd.code_value AS Category
		,vp.Variance_Parameter_Id
        ,vp.Parameter
        ,cdo.code_id AS Operator_Id
        ,cdo.code_value AS Operator
        ,vr.Positive_Negative
        ,vpbmap.variance_baseline_id AS BaseLine_Id
        ,cdb.code_Dsc AS BaseLine
        ,cmap.commodity_id
        ,com.Commodity_Name
	FROM
		dbo.Variance_Test_Master vt
		INNER JOIN Variance_Test_Commodity_Map cmap
			ON cmap.Variance_Test_id  = vt.Variance_test_id
		INNER JOIN Variance_Rule vr
			ON vr.Variance_Test_id = cmap.Variance_test_id
		INNER JOIN Variance_Parameter_Baseline_Map vpbmap
			ON vr.Variance_Baseline_id = vpbmap.Variance_Baseline_id
		INNER JOIN Variance_Parameter vp
			ON vp.Variance_Parameter_id = vpbmap.Variance_Parameter_id
		INNER JOIN Code cd
			ON cd.code_id = vp.Variance_Category_cd
		INNER JOIN Code cdo
			ON cdo.code_id = vr.Operator_cd
		INNER JOIN Code cdb
			ON cdb.code_id = vpbmap.Baseline_cd
		INNER JOIN dbo.Commodity com
			ON com.Commodity_Id = cmap.Commodity_Id		
			WHERE cd.code_value NOT IN  ('Demand', 'Billed Demand')	
			AND vp.Parameter <> 'Commodity Unit Cost'
	GROUP BY
		vt.variance_test_id,vt.is_inclusive
		,vt.is_multiple_condition
		,vr.Variance_Rule_Id
		,cd.code_id
		,cd.code_value
		,vp.Variance_Parameter_Id
        ,vp.Parameter
        ,cdo.code_id
        ,cdo.code_value
        ,vr.Positive_Negative
        ,vpbmap.variance_baseline_id
        ,cdb.code_Dsc
        ,cmap.commodity_id
        ,com.Commodity_Name
	ORDER BY
		variance_test_Id

                  END;
      END;
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_SEL] TO [CBMSApplication]
GO
