
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME: [DBO].User_Sel_By_Division_Id    
       
DESCRIPTION:   
   To get User Detail for selected Division Id.  
        
INPUT PARAMETERS:            
 NAME					DATATYPE		DEFAULT				DESCRIPTION            
-----------------------------------------------------------------------------            
 @Division_Id			INT   
 @Start_Index			INT				1  
 @End_Index				INT				2147483647        
                 
OUTPUT PARAMETERS:            
 NAME					DATATYPE		DEFAULT				DESCRIPTION            
-----------------------------------------------------------------------------
USAGE EXAMPLES:
-----------------------------------------------------------------------------

 EXEC dbo.User_Sel_By_Division_Id 12241155,1  

 EXEC dbo.User_Sel_By_Division_Id 12241157,0
   
AUTHOR INITIALS:            
 INITIALS		NAME            
-----------------------------------------------------------------------------            
 PNR			PANDARINATH 
 NR				Narayana Reddy 
            
MODIFICATIONS:             
 INITIALS		DATE				MODIFICATION            
-----------------------------------------------------------------------------            
 PNR			25-JULY-10			CREATED     
 HG				09/27/2010			Added @Is_History parameter to filter the records based on the requirement  
									@Is_History = 0 - will fetch active users(users who are not moved to histor)  
									@Is_History = 1 - Will fetch inactive users  
									@Is_History = NULL will fetch all the users associated with the site.  
 NR				2015-11-05			MAINT-3681 Removed the User_Division_Map table and replaced with Security role,client_hier table and select only the non corporate role users
******/    
 
CREATE PROCEDURE [dbo].[User_Sel_By_Division_Id]
      (
       @Division_Id INT
      ,@Is_History BIT = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS
BEGIN  
  
      SET NOCOUNT ON;
      
      DECLARE
            @Hier_Level_Cd INT
           ,@Corporate_Role_Id INT;
      
      SELECT
            @Corporate_Role_Id = sr.Security_Role_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Security_Role sr
                  ON sr.Client_Id = ch.Client_Id
      WHERE
            sr.Is_Corporate = 1
            AND ch.Sitegroup_Id = @Division_Id
            AND ch.Site_Id = 0;

      SELECT
            @Hier_Level_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND Code_Value = 'Division';  

      WITH  Cte_User_Names
              AS ( SELECT
                        ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS UserName
                       ,ROW_NUMBER() OVER ( ORDER BY ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME ) AS Row_Num
                       ,COUNT(1) OVER ( ) AS Total_Rows
                   FROM
                        dbo.Security_Role_Client_Hier srch
                        INNER JOIN Core.Client_Hier ch
                              ON srch.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.User_Security_Role usr
                              ON usr.Security_Role_Id = srch.Security_Role_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = usr.User_Info_Id
                   WHERE
                        ch.Sitegroup_Id = @Division_Id
                        AND ( @Is_History IS NULL
                              OR ui.IS_HISTORY = @Is_History )
                        AND ch.Hier_level_Cd = @Hier_Level_Cd
                        AND ui.ACCESS_LEVEL = 1
                        AND usr.Security_Role_Id <> @Corporate_Role_Id -- To exclude the corporate role users
                   GROUP BY
                        ui.FIRST_NAME
                       ,ui.LAST_NAME)
            SELECT
                  UserName
                 ,Total_Rows
            FROM
                  Cte_User_Names
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index;

END;
;
GO

GRANT EXECUTE ON  [dbo].[User_Sel_By_Division_Id] TO [CBMSApplication]
GO
