SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.Outside_Contract_Term_Invoice_Se_By_Account_Contract_Id                  
                            
 DESCRIPTION:        
				TO get the roles information for clint app acess role table .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Account_Id				            INT          
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
       
 Exec  dbo.Outside_Contract_Term_Invoice_Se_By_Account_Contract_Id 11         
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2020-04-20      Created for MAINT-10168.                       
                           
******/

CREATE PROC [dbo].[Outside_Contract_Term_Invoice_Se_By_Account_Contract_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Invoice TABLE
              (
                  Cu_invoice_Id INT
              );


        DECLARE
            @OCT_Exception_Type_Cd INT
            , @New_Exception_Status_Cd INT
            , @In_Progress_Status_Cd INT;

        SELECT
            @OCT_Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Outside Contract Term'
            AND cs.Codeset_Name = 'Exception Type';



        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @In_Progress_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                           END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value IN ( 'New', 'In Progress' )
            AND cs.Codeset_Name = 'Exception Status';


        INSERT INTO @Invoice
             (
                 Cu_invoice_Id
             )
        SELECT
            aecim.Cu_Invoice_Id
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aecim
                ON aecim.Account_Exception_Id = ae.Account_Exception_Id
        WHERE
            ae.Account_Id = @Account_Id
            AND ae.Exception_Type_Cd = @OCT_Exception_Type_Cd
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Status_Cd );



        SELECT
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            EXISTS (SELECT  1 FROM  @Invoice i WHERE i.Cu_invoice_Id = cism.CU_INVOICE_ID)
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED;





    END;


GO
GRANT EXECUTE ON  [dbo].[Outside_Contract_Term_Invoice_Se_By_Account_Contract_Id] TO [CBMSApplication]
GO
