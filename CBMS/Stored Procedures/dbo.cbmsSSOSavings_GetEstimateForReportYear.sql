SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

NAME:  
	dbo.cbmsSSOSavings_GetEstimateForReportYear  
  
DESCRIPTION:
  
 INPUT PARAMETERS:  
 Name				DataType  Default Description  
------------------------------------------------------------  
 @MyAccountId		int                     
 @report_year		int                     
 @client_id			int                     
 @division_id		int        null         
 @site_id			int        null         
 @currency_unit_id	int                     
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
  
 EXEC dbo.cbmsSSOSavings_GetEstimateForReportYear 49,2009,10069,NULL,NULL,3
 EXEC dbo.cbmsSSOSavings_GetEstimateForReportYear 49,2013,10036,317,267553,3

   
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
 CPE		Chaitanya Panduga Eshwar
 RR			Raghu Reddy
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
          7/20/2009  Autogenerated script  
 SSR	  03/29/2010 Replaced Entity(entity_id) with Commodity(commodity_id)  
 SSR	  04/05/2010 Added Recurrsive CTE and removed Sub select Query of View vwCbmsSSOSavingsOwnerFlat     
 DMR	  09/10/2010 Modified for Quoted_Identifier
 PNR	  04/05/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 CPE	  04/08/2011 Modified the SP to replace vwClientFiscalYearStartMonth and the CTE with Date_Dim
 RR		  2013-08-13 MAINT-2101 Script modified to pull the values in the order of client fiscal year
  
******/  

CREATE PROCEDURE dbo.cbmsSSOSavings_GetEstimateForReportYear
      ( 
       @MyAccountId INT
      ,@report_year INT
      ,@client_id INT
      ,@division_id INT = NULL
      ,@site_id INT = NULL
      ,@currency_unit_id INT )
AS 
BEGIN
  
      SET NOCOUNT ON;
    
      EXEC dbo.cbmsSecurity_GetClientAccess 
            @MyAccountId
           ,@client_id OUTPUT
           ,@division_id OUTPUT
           ,@site_id OUTPUT  

      DECLARE
            @StartDate DATETIME
           ,@EndDate DATETIME
           ,@OffSet INT
      
      DECLARE @Months_Order TABLE
            ( 
             Row_Num INT IDENTITY(1, 1)
            ,Month_Num INT
            ,Month_Name VARCHAR(10) )

      SELECT
            @OffSet = Client_Fiscal_Offset
      FROM
            Core.Client_Hier
      WHERE
            Client_Id = @client_id
            AND Sitegroup_Id = 0

      SELECT
            @StartDate = dateadd(mm, @OffSet, Date_D)
           ,@EndDate = dateadd(mm, 11, @StartDate)
      FROM
            meta.Date_Dim
      WHERE
            Year_Num = @report_year
            AND Month_Num = 1;
            
      INSERT      @Months_Order
                  SELECT
                        DD.MONTH_NUM
                       ,DD.MONTH_NAME
                  FROM
                        META.DATE_DIM DD
                  WHERE
                        DD.DATE_D BETWEEN @StartDate AND @EndDate
                  ORDER BY
                        DD.DATE_D
      SELECT
            SS.SSO_SAVINGS_ID
           ,SS.SAVINGS_TITLE
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS Owner_Id
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS Owner_Name
           ,Cd.Display_Seq AS Owner_Sort
           ,SS.COMMODITY_TYPE_ID
           ,com.Commodity_Name commodity_type
           ,@report_year report_year
           ,CU.CURRENCY_UNIT_NAME currency_type
           ,CH.Client_Name
           ,max(case WHEN mo.Row_Num = 1 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month1_Value
           ,max(case WHEN mo.Row_Num = 2 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month2_Value
           ,max(case WHEN mo.Row_Num = 3 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month3_Value
           ,max(case WHEN mo.Row_Num = 4 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month4_Value
           ,max(case WHEN mo.Row_Num = 5 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month5_Value
           ,max(case WHEN mo.Row_Num = 6 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month6_Value
           ,max(case WHEN mo.Row_Num = 7 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month7_Value
           ,max(case WHEN mo.Row_Num = 8 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month8_Value
           ,max(case WHEN mo.Row_Num = 9 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month9_Value
           ,max(case WHEN mo.Row_Num = 10 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month10_Value
           ,max(case WHEN mo.Row_Num = 11 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month11_Value
           ,max(case WHEN mo.Row_Num = 12 THEN SSD.ESTIMATED_SAVINGS_VALUE * CUC.CONVERSION_FACTOR
                     ELSE 0
                END) AS Month12_Value
      FROM
            DBO.SSO_SAVINGS SS
            JOIN DBO.SSO_SAVINGS_DETAIL SSD
                  ON SS.SSO_SAVINGS_ID = SSD.SSO_SAVINGS_ID
            JOIN dbo.SSO_SAVINGS_OWNER_MAP SSOM
                  ON SS.SSO_SAVINGS_ID = SSOM.SSO_SAVINGS_ID
            JOIN Core.Client_Hier CH
                  ON CH.Client_Hier_Id = SSOM.Client_Hier_Id
            JOIN meta.Date_Dim DD
                  ON DD.Date_D = SSD.SERVICE_MONTH
            JOIN @Months_Order mo
                  ON mo.Month_Num = DD.MONTH_NUM
            JOIN dbo.CURRENCY_UNIT_CONVERSION CUC
                  ON CUC.CURRENCY_GROUP_ID = CH.Client_Currency_Group_Id
                     AND CUC.BASE_UNIT_ID = SS.CURRENCY_UNIT_ID
                     AND CUC.CONVERTED_UNIT_ID = @currency_unit_id
            JOIN DBO.CURRENCY_UNIT CU
                  ON CU.CURRENCY_UNIT_ID = SS.CURRENCY_UNIT_ID
            JOIN dbo.Code Cd
                  ON CH.Hier_level_Cd = Cd.Code_Id
            LEFT OUTER JOIN dbo.Commodity com
                  ON SS.COMMODITY_TYPE_ID = com.Commodity_Id
      WHERE
            ( @client_id IS NULL
              OR CH.Client_Id = @client_id )
            AND ( @division_id IS NULL
                  OR CH.Sitegroup_Id = @division_id )
            AND ( @site_id IS NULL
                  OR CH.Site_Id = @site_id )
            AND DD.Date_D BETWEEN @StartDate AND @EndDate
      GROUP BY
            SS.SSO_SAVINGS_ID
           ,SS.SAVINGS_TITLE
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END
           ,Cd.Display_Seq
           ,SS.COMMODITY_TYPE_ID
           ,com.Commodity_Name
           ,CU.CURRENCY_UNIT_NAME
           ,CH.Client_Name
          
END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetEstimateForReportYear] TO [CBMSApplication]
GO
GO