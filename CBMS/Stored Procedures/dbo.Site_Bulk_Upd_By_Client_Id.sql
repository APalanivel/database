
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Site_Bulk_Upd_By_Client_Id]  
     
DESCRIPTION: 

	To update Site properties across the given client id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Client_Id					INT
@User_Info_Id				INT
@Lp_Contact_First_Name		VARCHAR(40)		NULL
@Lp_Contact_Last_Name		VARCHAR(40)		NULL
@Lp_Contact_Email_Address	VARCHAR(150)	NULL
@Lp_Contact_Cc				VARCHAR(1500)	NULL
@Is_Preference_By_Email		BIT				NULL
@Site_Product_Service		VARCHAR(2000)	NULL
@Production_Schedule		VARCHAR(2000)	NULL
@Shutdown_Schedule			VARCHAR(2000)	NULL
@Contracting_Entity			VARCHAR(200)	NULL
@Client_Legal_Structure		VARCHAR(4000)	NULL
@Naics_Code					VARCHAR(30)		NULL
@TAX_NUMBER					VARCHAR(30)		NULL
@Duns_Number				VARCHAR(30)		NULL
@Doing_Business_As			VARCHAR(200)	NULL
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:          
------------------------------------------------------------

	begin tran
select top 10 * from site where client_id=10069  and Lp_Contact_First_Name='bulk upd test'
	EXEC Site_Bulk_Upd_By_Client_Id_balaraju
		@Client_Id = 10069
		,@User_Info_Id = 49
		,@Lp_Contact_First_Name = 'bulk upd test'
		select top 10 * from site where client_id=10069 and Lp_Contact_First_Name='bulk upd test'

rollback tran

	EXEC Site_Bulk_Upd_By_Client_Id
		@Client_Id = 10069
		,@User_Info_Id = 49
		,@Lp_Contact_First_Name = 'bulk upd test123'
		,@IS_PREFERENCE_BY_DV = 1
	
	EXEC Site_Bulk_Upd_By_Client_Id
		@Client_Id = 10069
		,@User_Info_Id = 49
		,@Lp_Contact_First_Name = 'bulk upd test123'
		,@IS_PREFERENCE_BY_DV = 1
		,@Shutdown_Schedule = 'Test'

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G
SKA			Shobhit Kumar
BCH			Balaraju Chalumuri
RKV         Ravi Kumar Vegesna 


MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2/12/2010	Created for Site bulk update enhancement
SKA			12/14/2010	Add input parameter for @IS_PREFERENCE_BY_DV
HG			1/11/2011	Converted to Dynamic SQL using SP_ExecuteSql as it is decided to update only the Column modified by the user
SKA			01/20/2011	Fein_Number has been chaged to Tax_Number as we do not have this column in site table.
BCH			2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
RKV         2013-11-05  Maint-1964 Parameters to hashbytes is converted to xml raw

*/

CREATE PROCEDURE dbo.Site_Bulk_Upd_By_Client_Id
      @Client_Id INT
     ,@User_Info_Id INT
     ,@Lp_Contact_First_Name VARCHAR(40) = NULL
     ,@Lp_Contact_Last_Name VARCHAR(40) = NULL
     ,@Lp_Contact_Email_Address VARCHAR(150) = NULL
     ,@Lp_Contact_Cc VARCHAR(1500) = NULL
     ,@Is_Preference_By_Email BIT = NULL
     ,@Site_Product_Service VARCHAR(2000) = NULL
     ,@Production_Schedule VARCHAR(2000) = NULL
     ,@Shutdown_Schedule VARCHAR(2000) = NULL
     ,@Contracting_Entity VARCHAR(200) = NULL
     ,@Client_Legal_Structure VARCHAR(4000) = NULL
     ,@Naics_Code VARCHAR(30) = NULL
     ,@TAX_NUMBER VARCHAR(30) = NULL
     ,@Duns_Number VARCHAR(30) = NULL
     ,@Doing_Business_As VARCHAR(200) = NULL
     ,@IS_PREFERENCE_BY_DV BIT = NULL
AS 
BEGIN

      SET NOCOUNT ON;

	-- To hold the Sites get updated
      CREATE TABLE #Updated_Sites ( Site_Id INT )
		
      DECLARE @Column_Value TABLE
            ( 
             Column_Name VARCHAR(60)
            ,Column_Data_Type VARCHAR(20)
            ,Column_Value VARCHAR(4000)
            ,Is_Quoted BIT
            ,Is_Filter_Column BIT )

      DECLARE
            @SqlString NVARCHAR(MAX)
           ,@Where_Clause NVARCHAR(200)
           ,@Param_Definition NVARCHAR(MAX)
           ,@Binary_Check_Sum_Column NVARCHAR(1000)
           ,@Binary_Check_Sum_Param NVARCHAR(1000)


      DECLARE
            @Site_Table_Entity_Id INT
           ,@Current_Ts DATETIME
           ,@Audit_Type INT = 2

      SELECT
            @Site_Table_Entity_Id = Entity_Id
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Site_Table'
            AND ENTITY_DESCRIPTION = 'Table_Type'

	-- Insert only the Numeric column parameters		
      INSERT      INTO @Column_Value
                  ( Column_Name, Column_Data_Type, Column_Value, Is_Quoted, Is_Filter_Column )
      VALUES
                  ( 'Client_Id', 'INT', @Client_Id, 0, 1 )
		,         ( 'Is_Preference_By_Email', 'BIT', @Is_Preference_By_Email, 0, 0 )
		,         ( 'Is_Preference_By_Dv', 'BIT', @IS_PREFERENCE_BY_DV, 0, 0 )

	-- Insert Varchar column parameters
      INSERT      INTO @Column_Value
                  ( Column_Name, Column_Data_Type, Column_Value, Is_Quoted, Is_Filter_Column )
      VALUES
                  ( 'Lp_Contact_First_Name', 'VARCHAR(40)', @Lp_Contact_First_Name, 1, 0 )
		,         ( 'Lp_Contact_Last_Name', 'VARCHAR(40)', @Lp_Contact_Last_Name, 1, 0 )
		,         ( 'Lp_Contact_Email_Address', 'VARCHAR(150)', @Lp_Contact_Email_Address, 1, 0 )
		,         ( 'Lp_Contact_Cc', 'VARCHAR(1500)', @Lp_Contact_Cc, 1, 0 )
		,         ( 'Site_Product_Service', 'VARCHAR(2000)', @Site_Product_Service, 1, 0 )
		,         ( 'Production_Schedule', 'VARCHAR(2000)', @Production_Schedule, 1, 0 )
		,         ( 'Shutdown_Schedule', 'VARCHAR(2000)', @Shutdown_Schedule, 1, 0 )
		,         ( 'Contracting_Entity', 'VARCHAR(2000)', @Contracting_Entity, 1, 0 )
		,         ( 'Client_Legal_Structure', 'VARCHAR(4000)', @Client_Legal_Structure, 1, 0 )
		,         ( 'Naics_Code', 'VARCHAR(30)', @Naics_Code, 1, 0 )
		,         ( 'TAX_NUMBER', 'VARCHAR(30)', @TAX_NUMBER, 1, 0 )
		,         ( 'Duns_Number', 'VARCHAR(30)', @Duns_Number, 1, 0 )
		,         ( 'Doing_Business_As', 'VARCHAR(200)', @Doing_Business_As, 1, 0 )

      SELECT
            @SqlString = COALESCE(@SqlString + ', ' + Column_Name + ' = @' + Column_Name, Column_Name + ' = @' + Column_Name)
      FROM
            @Column_Value
      WHERE
            Column_Value IS NOT NULL
            AND Is_Filter_Column = 0

      SELECT
            @Binary_Check_Sum_Column = COALESCE(@Binary_Check_Sum_Column + ', ' + Column_Name, Column_Name)
           ,@Binary_Check_Sum_Param = COALESCE(@Binary_Check_Sum_Param + ', ' + '@' + Column_Name + ' ' + Column_Name , '@' + Column_Name + ' ' + Column_Name)
      FROM
            @Column_Value
      WHERE
            Column_Value IS NOT NULL
            AND Is_Filter_Column = 0
      ORDER BY
            Column_Name

      SELECT
            @Where_Clause = COALESCE(@Where_Clause + ' AND ' + Column_Name + ' = @' + Column_Name, Column_Name + ' = @' + Column_Name)
      FROM
            @Column_Value
      WHERE
            Is_Filter_Column = 1

      SELECT
            @Param_Definition = COALESCE(@Param_Definition + ', ' + '@' + Column_Name + ' ' + Column_Data_Type, '@' + Column_Name + ' ' + Column_Data_Type)
      FROM
            @Column_Value

      SET @SqlString = 'UPDATE dbo.Site
						SET ' + @SqlString + ' OUTPUT inserted.Site_Id INTO #Updated_Sites' + +' WHERE ' + @Where_Clause + ' AND ' + ' HASHBYTES(' + '''SHA1''' + ', ( SELECT ' + @Binary_Check_Sum_Column + ' FOR XML RAW )) != HASHBYTES(' + '''SHA1''' + ', + ( SELECT ' + @Binary_Check_Sum_Param + ' FOR XML RAW ))'				 

	-- Converting Zero length strings to NULL value before updating in to DB
      SELECT
            @Lp_Contact_First_Name = NULLIF(@Lp_Contact_First_Name, '')
           ,@Lp_Contact_Last_Name = NULLIF(@Lp_Contact_Last_Name, '')
           ,@Lp_Contact_Email_Address = NULLIF(@Lp_Contact_Email_Address, '')
           ,@Lp_Contact_Cc = NULLIF(@Lp_Contact_Cc, '')
           ,@Site_Product_Service = NULLIF(@Site_Product_Service, '')
           ,@Production_Schedule = NULLIF(@Production_Schedule, '')
           ,@Shutdown_Schedule = NULLIF(@Shutdown_Schedule, '')
           ,@Contracting_Entity = NULLIF(@Contracting_Entity, '')
           ,@Client_Legal_Structure = NULLIF(@Client_Legal_Structure, '')
           ,@Naics_Code = NULLIF(@Naics_Code, '')
           ,@TAX_NUMBER = NULLIF(@TAX_NUMBER, '')
           ,@Duns_Number = NULLIF(@Duns_Number, '')
           ,@Doing_Business_As = NULLIF(@Doing_Business_As, '')
	
      BEGIN TRY
            BEGIN TRAN
	
            EXEC SP_EXECUTESQL 
                  @SqlString
                 ,@Param_Definition
                 ,@Client_Id = @Client_Id
                 ,@Lp_Contact_First_Name = @Lp_Contact_First_Name
                 ,@Lp_Contact_Last_Name = @Lp_Contact_Last_Name
                 ,@Lp_Contact_Email_Address = @Lp_Contact_Email_Address
                 ,@Lp_Contact_Cc = @Lp_Contact_Cc
                 ,@Site_Product_Service = @Site_Product_Service
                 ,@Production_Schedule = @Production_Schedule
                 ,@Shutdown_Schedule = @Shutdown_Schedule
                 ,@Contracting_Entity = @Contracting_Entity
                 ,@Client_Legal_Structure = @Client_Legal_Structure
                 ,@Naics_Code = @Naics_Code
                 ,@TAX_NUMBER = @TAX_NUMBER
                 ,@Duns_Number = @Duns_Number
                 ,@Doing_Business_As = @Doing_Business_As
                 ,@Is_Preference_By_Email = @Is_Preference_By_Email
                 ,@IS_PREFERENCE_BY_DV = @IS_PREFERENCE_BY_DV

            SET @Current_Ts = GETDATE()

            INSERT      INTO dbo.ENTITY_AUDIT
                        ( 
                         Entity_ID
                        ,Entity_Identifier
                        ,User_Info_Id
                        ,Audit_Type
                        ,modified_date )
                        SELECT
                              @Site_Table_Entity_Id AS Entity_Id
                             ,SITE_ID AS Site_Id
                             ,@User_Info_Id AS User_Info_Id
                             ,@Audit_Type AS Audit_Type
                             ,@Current_Ts AS Current_Ts
                        FROM
                              #Updated_Sites

            COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END
            EXEC dbo.usp_RethrowError 
                  'Error in Site Bulk update'

      END CATCH

      DROP TABLE #Updated_Sites

END



;
GO


GRANT EXECUTE ON  [dbo].[Site_Bulk_Upd_By_Client_Id] TO [CBMSApplication]
GO
