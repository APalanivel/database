SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.TOU_Schedule_Term_Sel_By_Rate_Schedule_Id

DESCRIPTION:
	Returns the TOU Schedule Term by given Rate Schedule id.

INPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------
	@Rate_Schedule_Id						INT


OUTPUT PARAMETERS:
	Name									DataType		Default	Description
-------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------
					
Exec dbo.TOU_Schedule_Term_Sel_By_Rate_Schedule_Id 32917  
Exec dbo.TOU_Schedule_Term_Sel_By_Rate_Schedule_Id 33040  
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------

	BCH			Balaraju Chalumuri

MODIFICATIONS

	Initials	Date		 Modification
------------------------------------------------------------------------------
	BCH			2012-07-017  Created
******/ 
CREATE PROCEDURE dbo.TOU_Schedule_Term_Sel_By_Rate_Schedule_Id
      @Rate_Schedule_Id INT
AS 
BEGIN
      SET NOCOUNT ON ;

      SELECT
            TOUST.Time_Of_Use_Schedule_Term_Id
           ,TOUS.Time_Of_Use_Schedule_Id
           ,TOUS.Schedule_Name
           ,CONVERT(DATE, TOUST.Start_Dt) AS Start_Dt
           ,CASE WHEN TOUST.End_Dt = '2099-12-31' THEN NULL
                 ELSE CONVERT(DATE, TOUST.End_Dt)
            END AS End_Dt
      FROM
            dbo.RATE_SCHEDULE rs
            JOIN dbo.Time_Of_Use_Schedule_Term TOUST
                  ON rs.Time_Of_Use_Schedule_Term_Id = TOUST.Time_Of_Use_Schedule_Term_Id
            JOIN dbo.Time_Of_Use_Schedule TOUS
                  ON TOUST.Time_Of_Use_Schedule_Id = TOUS.Time_Of_Use_Schedule_Id
      WHERE
            rs.RATE_SCHEDULE_ID = @Rate_Schedule_Id
END

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Sel_By_Rate_Schedule_Id] TO [CBMSApplication]
GO
