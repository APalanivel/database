SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO














CREATE         PROCEDURE [dbo].[cbmsCPIndexDetail_Save]
	( @clearport_index_detail_id int = null
	, @clearport_index_month_id int = null
	, @index_detail_value decimal (32,16) = null
	, @index_detail_date datetime = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @clearport_index_detail_id 


	if @this_id is null
	begin

		insert into clearport_index_detail
			( clearport_index_month_id
			, index_detail_value
			, index_detail_date
			)
		 values
			( @clearport_index_month_id
			, @index_detail_value
			, @index_detail_date
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as clearport_index_detail_id

	end
	else
	begin

		   update clearport_index_detail with (rowlock)
		      set clearport_index_month_id = @clearport_index_month_id
			, index_detail_value = @index_detail_value
			, index_detail_date = @index_detail_date
		    where clearport_index_detail_id = @this_id

	end

	set nocount off



END












SET QUOTED_IDENTIFIER ON 













GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexDetail_Save] TO [CBMSApplication]
GO
