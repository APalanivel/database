SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.Cbms_Sitegroup_Participant_Merge                    
                      
Description:                      
        To get global site groups
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Group_Name			VARCHAR(100)
    @Client_Id			INT
    @RM_Group_Type_Cd	INT
    @User_Info_Id		INT   
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@Cbms_Sitegroup_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		DECLARE	@Cbms_Sitegroup_Id int
		EXEC dbo.Cbms_Sitegroup_Participant_Merge 'Global RM Test',235,102559,291,49,@Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id OUT
		SELECT @Cbms_Sitegroup_Id
		SELECT * FROM dbo.RM_GROUP WHERE RM_GROUP_ID = @Cbms_Sitegroup_Id
	ROLLBACK TRANSACTION     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-30-07     Global Risk Management - Created                    
                     
******/       
CREATE PROCEDURE [dbo].[Cbms_Sitegroup_Participant_Merge]
      ( 
       @Cbms_Sitegroup_Id INT
      ,@Tvp_RM_Group_Dtl dbo.Tvp_RM_Group_Dtl READONLY
      ,@Include_New_Contract_Extensions BIT
      ,@User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;    
      
                   
      MERGE INTO dbo.Cbms_Sitegroup_Participant AS tgt
            USING 
                  ( SELECT
                        @Cbms_Sitegroup_Id AS RM_Group_Id
                       ,RM_Group_Participant_Id
                       ,RM_Group_Participant_Type_Cd
                    FROM
                        @Tvp_RM_Group_Dtl ) AS src
            ON tgt.Cbms_Sitegroup_Id = src.RM_Group_Id
                  AND tgt.Group_Participant_Id = src.RM_Group_Participant_Id
                  AND tgt.Group_Participant_Type_Cd = src.RM_Group_Participant_Type_Cd
            WHEN NOT MATCHED BY SOURCE AND  tgt.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
                  THEN
							DELETE
            WHEN NOT MATCHED BY TARGET 
                  THEN
                   INSERT
                        ( 
                         Cbms_Sitegroup_Id
                        ,Group_Participant_Id
                        ,Group_Participant_Type_Cd
                        ,Include_New_Contract_Extensions
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         @Cbms_Sitegroup_Id
                        ,src.RM_Group_Participant_Id
                        ,src.RM_Group_Participant_Type_Cd
                        ,@Include_New_Contract_Extensions
                        ,@User_Info_Id
                        ,GETDATE()
                        ,@User_Info_Id
                        ,GETDATE() );  
		
                                   
END;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Participant_Merge] TO [CBMSApplication]
GO
