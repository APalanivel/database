SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsUbmClientMap_Save]
	( @MyAccountId int
	, @ubm_client_map_id int = null
	, @ubm_id int
	, @ubm_client_code varchar(200)
	, @client_id int
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	set @this_id = @ubm_client_map_id

	if @this_id is null
	begin

	   select @this_id = ubm_client_map_id
	     from ubm_client_map
	    where ubm_id = @ubm_id
	      and ubm_client_code = @ubm_client_code
	      and client_id = @client_id

	end

	if @this_id is null
	begin

		insert into ubm_client_map
			( ubm_id
			, ubm_client_code
			, client_id
			, is_processed
			)
		values
			( @ubm_id
			, @ubm_client_code
			, @client_id
			, 1
			)

		set @this_id = @@IDENTITY


	   update cu_invoice
	      set client_id = @client_id
	    where ubm_client_code = @ubm_client_code
	      and client_id is null

	end

	exec cbmsUbmClientMap_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmClientMap_Save] TO [CBMSApplication]
GO
