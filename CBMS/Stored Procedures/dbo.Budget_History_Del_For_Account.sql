SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_History_Del_For_Account]  
     
DESCRIPTION: 

	To delete budget history associated with the given Account Id.
		- If the account marked as deleted(Is_Deleted = 1 in Budget_Account) then all it history has to be removed.
		- If the deleted the account is the only account of that budget then budget also will be removed

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	SET STATISTICS IO ON    
	BEGIN TRAN 
		EXEC Budget_History_Del_For_Account  23127
	ROLLBACK TRAN

	BEGIN TRAN 
		EXEC Budget_History_Del_For_Account  31765
	ROLLBACK TRAN

	BEGIN TRAN
		EXEC Budget_History_Del_For_Account  15518
	ROLLBACK TRAN

	SELECT * FROM Budget_Account WHERE account_id IN ( 31765, 23127, 15518)

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED
HG			21-JUL-10	Removed the if condition to check if record exist in @budget_account_list to populate other tables as we can have entry in Budget_Nymex_Setup for the account though there is no budget created for that account.
*/

CREATE PROCEDURE dbo.Budget_History_Del_For_Account
	@Account_Id		INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @Budget_Usage_Id						INT
			,@Budget_Nymex_Forecast_Id				INT
			,@Budget_Detail_Snap_Shot_Id			INT
			,@Budget_Detail_Comments_Owner_Map_Id	INT
			,@Budget_Detail_Comments_Id				INT
			,@Budget_Detail_Id						INT
			,@Budget_Account_Id						INT
			,@Budget_Currency_Map_Id				INT
			,@Budget_Id								INT

	DECLARE @Budget_Usage_List TABLE(Budget_Usage_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Budget_Nymex_Forecast_List TABLE(Budget_Nymex_Forecast_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Budget_Account_List TABLE(Budget_Account_Id INT PRIMARY KEY CLUSTERED, Budget_Id INT);
	DECLARE @Budget_Detail_Snap_Shot_List TABLE(Budget_Detail_Snap_Shot_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Budget_Detail_Comments_Owner_Map_List TABLE(Budget_Detail_Comments_Owner_Map_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Budget_Detail_Comments_List TABLE(Budget_Detail_Comments_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Budget_Details_List TABLE(Budget_Detail_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Orphan_Budget_List TABLE(Budget_Id INT PRIMARY KEY CLUSTERED);
	DECLARE @Budget_Currency_Map_List TABLE(Budget_Currency_Map_Id INT PRIMARY KEY CLUSTERED);


	INSERT INTO @Budget_Account_List
	(
		Budget_Account_Id
		,Budget_Id
	)
	SELECT
		Budget_Account_id
		,Budget_Id
	FROM
		dbo.Budget_Account ba
	WHERE
		ACCOUNT_ID = @Account_Id
		AND Is_Deleted = 1
		AND NOT EXISTS	(
							SELECT
								1
							FROM
								dbo.Budget_Account ba1
							WHERE
								ba1.ACCOUNT_ID = @Account_Id
								AND IS_DELETED = 0
						)
	
	INSERT INTO @Budget_Usage_List
	(
		Budget_Usage_Id
	)
	SELECT
		Budget_Usage_id
	FROM
		dbo.Budget_Usage
	WHERE
		ACCOUNT_ID = @Account_Id

	INSERT INTO @Budget_Nymex_Forecast_List
	(
		Budget_Nymex_Forecast_Id
	)
	SELECT
		Budget_Nymex_Forecast_Id
	FROM
		dbo.BUDGET_NYMEX_FORECAST
	WHERE
		ACCOUNT_ID = @Account_Id

	INSERT INTO @Budget_Detail_Snap_Shot_List
	(
		Budget_Detail_Snap_Shot_Id
	)
	SELECT
		bdss.BUDGET_DETAIL_SNAP_SHOT_ID
	FROM
		dbo.BUDGET_DETAIL_SNAP_SHOT bdss
		JOIN @Budget_Account_List bal
			ON bal.Budget_Account_Id = bdss.BUDGET_ACCOUNT_ID
			
	INSERT INTO @Budget_Detail_Comments_Owner_Map_List
	(
		Budget_Detail_Comments_Owner_Map_Id
	)
	SELECT
		map.Budget_Detail_Comments_Owner_Map_Id
	FROM
		dbo.Budget_Detail_Comments_Owner_Map map
		JOIN @Budget_Account_List bal
			ON bal.Budget_Account_Id = map.BUDGET_ACCOUNT_ID

	INSERT INTO @Budget_Detail_Comments_List
	(
		Budget_Detail_Comments_Id
	)
	SELECT
		bdc.BUDGET_DETAIL_COMMENTS_ID
	FROM
		dbo.Budget_Detail_Comments bdc
		JOIN @Budget_Account_List bal
			ON bal.Budget_Account_Id = bdc.BUDGET_ACCOUNT_ID

	INSERT INTO @Budget_Details_List
	(
		Budget_Detail_Id
	)
	SELECT
		bd.BUDGET_DETAIL_ID
	FROM
		dbo.Budget_Details bd
		JOIN @Budget_Account_List bal
			ON bal.Budget_Account_Id = bd.BUDGET_ACCOUNT_ID

	
	INSERT INTO @Orphan_Budget_List
	(
		Budget_Id
	)
	SELECT
		bal.Budget_Id
	FROM
		@Budget_Account_List bal
	WHERE
		NOT EXISTS(SELECT
						1
					FROM
						dbo.BUDGET_ACCOUNT ba
					WHERE
						ba.BUDGET_ID = bal.Budget_Id
				   )
	GROUP BY
		bal.Budget_Id

	INSERT INTO @Budget_Currency_Map_List
	(
		Budget_Currency_Map_Id
	)
	SELECT
		Budget_Currency_Map_Id
	FROM
		dbo.BUDGET_CURRENCY_MAP bcm
		JOIN @Orphan_Budget_List bl
			ON bl.Budget_Id = bcm.BUDGET_ID

	BEGIN TRY
		BEGIN TRAN

			WHILE EXISTS(SELECT 1 FROM @Budget_Usage_List)
				BEGIN

					SET @Budget_Usage_Id = (SELECT TOP 1 Budget_Usage_Id FROM @Budget_Usage_List)

					EXEC dbo.Budget_Usage_Del @Budget_Usage_Id

					DELETE
						@Budget_Usage_List
					WHERE
						Budget_Usage_Id = @Budget_Usage_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Nymex_Forecast_List)
				BEGIN

					SET @Budget_Nymex_Forecast_Id = (SELECT TOP 1 Budget_Nymex_Forecast_Id FROM @Budget_Nymex_Forecast_List)

					EXEC dbo.Budget_Nymex_Forecast_Del @Budget_Nymex_Forecast_Id

					DELETE
						@Budget_Nymex_Forecast_List
					WHERE
						Budget_Nymex_Forecast_Id = @Budget_Nymex_Forecast_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Detail_Snap_Shot_List)
				BEGIN

					SET @Budget_Detail_Snap_Shot_Id = (SELECT TOP 1 Budget_Detail_Snap_Shot_Id FROM @Budget_Detail_Snap_Shot_List)

					EXEC dbo.Budget_Detail_Snap_Shot_Del @Budget_Detail_Snap_Shot_Id

					DELETE
						@Budget_Detail_Snap_Shot_List
					WHERE
						Budget_Detail_Snap_Shot_Id = @Budget_Detail_Snap_Shot_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Detail_Comments_Owner_Map_List)
				BEGIN

					SET @Budget_Detail_Comments_Owner_Map_Id = (SELECT TOP 1 Budget_Detail_Comments_Owner_Map_Id FROM @Budget_Detail_Comments_Owner_Map_List)

					EXEC dbo.Budget_Detail_Comments_Owner_Map_Del @Budget_Detail_Snap_Shot_Id

					DELETE
						@Budget_Detail_Comments_Owner_Map_List
					WHERE
						Budget_Detail_Comments_Owner_Map_Id = @Budget_Detail_Comments_Owner_Map_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Detail_Comments_List)
				BEGIN

					SET @Budget_Detail_Comments_Id = (SELECT TOP 1 Budget_Detail_Comments_Id FROM @Budget_Detail_Comments_List)

					EXEC dbo.Budget_Detail_Comments_Del @Budget_Detail_Comments_Id

					DELETE
						@Budget_Detail_Comments_List
					WHERE
						Budget_Detail_Comments_Id = @Budget_Detail_Comments_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Details_List)
				BEGIN

					SET @Budget_Detail_Id = (SELECT TOP 1 Budget_Detail_Id FROM @Budget_Details_List)

					EXEC dbo.Budget_Details_Del @Budget_Detail_Id

					DELETE
						@Budget_Details_List
					WHERE
						Budget_Detail_Id = @Budget_Detail_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Account_List)
				BEGIN

					SET @Budget_Account_Id = (SELECT TOP 1 Budget_Account_Id FROM @Budget_Account_List)

					EXEC dbo.Budget_Account_Del @Budget_Account_Id

					DELETE
						@Budget_Account_List
					WHERE
						Budget_Account_Id = @Budget_Account_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Currency_Map_List)
				BEGIN

					SET @Budget_Currency_Map_Id = (SELECT TOP 1 Budget_Currency_Map_Id FROM @Budget_Currency_Map_List)

					EXEC dbo.Budget_Currency_Map_Del @Budget_Currency_Map_Id

					DELETE
						@Budget_Currency_Map_List
					WHERE
						Budget_Currency_Map_Id = @Budget_Currency_Map_Id

				END				

			WHILE EXISTS(SELECT 1 FROM @Orphan_Budget_List)
				BEGIN

					SET @Budget_Id = (SELECT TOP 1 Budget_Id FROM @Orphan_Budget_List)

					EXEC dbo.Budget_Del @Budget_Id

					DELETE
						@Orphan_Budget_List
					WHERE
						Budget_Id = @Budget_Id

				END
				
		COMMIT TRAN
	END TRY
	BEGIN CATCH
	 IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END

		EXEC dbo.usp_RethrowError
	
	END CATCH


END
GO
GRANT EXECUTE ON  [dbo].[Budget_History_Del_For_Account] TO [CBMSApplication]
GO
