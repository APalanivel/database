SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 CBMS.Budget.Get_Invoices_By_Account_By_TimeStamp  
    
DESCRIPTION:    
  Script to get Invoices for that account's for a given time period   
    
INPUT PARAMETERS:    
 Name     DataType  Default Description    
---------------------------------------------------------------    
 @Cu_Invoice_Id     INT    
 @dtStart_Date      Date  
 @dtEnd_Date  Date  
    
OUTPUT PARAMETERS:    
 Name     DataType  Default Description    
------------------------------------------------------------    
     
USAGE EXAMPLES:    
------------------------------------------------------------    
   --priority implementation 
 EXEC Budget.[Get_Invoices_By_Account_By_TimeStamp] 1234937,'2017-01-01','2017-10-01',12    
   
 --For SubBuckets  
EXEC Budget.Get_Invoices_By_Account_By_TimeStamp @intAccount_ID = 26654,@dtStart_Date = '2001-02-01',@dtEnd_Date = '2019-03-03', @Converted_uom_Id = 12 ;  
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 PRG  Prasanna R Gachinmani  
  
     
MODIFICATIONS    
 Initials Date   Modification    
------------------------------------------------------------    
 PRG      2019-04-10 Initial Development  
    
******/
CREATE PROC [Budget].[Get_Invoices_By_Account_By_TimeStamp]
    (
      @intAccount_ID INT
        , @dtStart_Date DATETIME
        , @dtEnd_Date DATETIME
        , @Converted_uom_Id INT = NULL
    )
AS
    BEGIN
        DECLARE @Total_Usage_Buckets TABLE
              (
                  SERVICE_MONTH DATE
                  , CU_Invoice_Id INT
                  , Bucket_Master_Id INT
                  , Priority_Order INT
                  , Row_Num INT
              );

        INSERT INTO @Total_Usage_Buckets
             (
                 SERVICE_MONTH
                 , CU_Invoice_Id
                 , Bucket_Master_Id
                 , Priority_Order
                 , Row_Num
             )
        SELECT
            cism.SERVICE_MONTH
            , cism.CU_INVOICE_ID
            , bm.Bucket_Master_Id
            , bcr.Priority_Order
            , RANK() OVER (PARTITION BY
                               cism.SERVICE_MONTH
                               , cism.CU_INVOICE_ID
                           ORDER BY
                               bcr.Priority_Order) AS RN
        FROM
            dbo.CU_INVOICE_DETERMINANT cid
            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH AS cism
                ON cism.Account_ID = cida.ACCOUNT_ID
                   AND  cism.CU_INVOICE_ID = cid.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cid.Bucket_Master_Id
                   AND  bm.Commodity_Id = cid.COMMODITY_TYPE_ID
            LEFT OUTER JOIN dbo.Bucket_Category_Rule AS bcr
                ON bcr.Child_Bucket_Master_Id = bm.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Bucket_Master AS bm2
                ON bm2.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON cid.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
        WHERE
            cism.Account_ID = @intAccount_ID
            AND c.Code_Value = 'Determinant'
            AND cism.SERVICE_MONTH BETWEEN @dtStart_Date
                                   AND     @dtEnd_Date
            AND bm2.Bucket_Name = 'Total Usage'
            AND cid.DETERMINANT_VALUE <> '0'
        GROUP BY
            cism.SERVICE_MONTH
            , cism.CU_INVOICE_ID
            , bm.Bucket_Master_Id
            , bcr.Priority_Order;



        SELECT
            cism.SERVICE_MONTH
      , LEFT(rn.CU_Invoice_Id, LEN(rn.CU_Invoice_Id) - 1) AS CU_Invoice_Id
            , LEFT(rn_image.CBMS_Image_Id, LEN(rn_image.CBMS_Image_Id) - 1) AS CBMS_Image_Id
            , SUM(CONVERT(
                      DECIMAL(28, 10)
                      , REPLACE(
                            REPLACE(
                                REPLACE(
                                    REPLACE(
                                        REPLACE(
                                            REPLACE(
                                                (CASE WHEN (PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE)) > 0 THEN
                                                          STUFF(
                                                              cid.DETERMINANT_VALUE
                                                              , PATINDEX('%[0-9]-', cid.DETERMINANT_VALUE) + 1, 1, '')
                                                     ELSE cid.DETERMINANT_VALUE
                                                 END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', ''))
                  * ISNULL(cuc.CONVERSION_FACTOR, 1)) AS DETERMINANT_VALUE
        INTO
            #DETERMINANT
        FROM
            dbo.CU_INVOICE_DETERMINANT cid
            INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH AS cism
                ON cism.Account_ID = cida.ACCOUNT_ID
                   AND  cism.CU_INVOICE_ID = cid.CU_INVOICE_ID
            INNER JOIN @Total_Usage_Buckets AS tub
                ON tub.Bucket_Master_Id = cid.Bucket_Master_Id
                   AND  tub.CU_Invoice_Id = cid.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cid.Bucket_Master_Id
                   AND  bm.Commodity_Id = cid.COMMODITY_TYPE_ID
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON cid.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION AS cuc
                ON cuc.BASE_UNIT_ID = cid.UNIT_OF_MEASURE_TYPE_ID
                   AND  cuc.CONVERTED_UNIT_ID = @Converted_uom_Id
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, csm.CU_INVOICE_ID) + ','
            FROM
                dbo.CU_INVOICE_SERVICE_MONTH csm
            WHERE
                cism.SERVICE_MONTH = csm.SERVICE_MONTH
                AND cism.Account_ID = csm.Account_ID
            FOR XML PATH('')) rn(CU_Invoice_Id)
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, ci.CBMS_IMAGE_ID) + ','
            FROM
                dbo.CU_INVOICE ci
                JOIN dbo.CU_INVOICE_SERVICE_MONTH csm
                    ON ci.CU_INVOICE_ID = csm.CU_INVOICE_ID
            WHERE
                csm.SERVICE_MONTH = cism.SERVICE_MONTH
                AND cism.Account_ID = csm.Account_ID
            FOR XML PATH('')) rn_image(CBMS_Image_Id)
        WHERE
            cism.Account_ID = @intAccount_ID
            AND c.Code_Value = 'Determinant'
            AND cism.SERVICE_MONTH BETWEEN @dtStart_Date
                                   AND     @dtEnd_Date
            AND tub.Row_Num = 1
        GROUP BY
            cism.SERVICE_MONTH
            , LEFT(rn.CU_Invoice_Id, LEN(rn.CU_Invoice_Id) - 1)
            , LEFT(rn_image.CBMS_Image_Id, LEN(rn_image.CBMS_Image_Id) - 1);




        SELECT
            cism.SERVICE_MONTH
            , LEFT(rn.CU_Invoice_Id, LEN(rn.CU_Invoice_Id) - 1) AS CU_Invoice_Id
            , LEFT(rn_image.CBMS_Image_Id, LEN(rn_image.CBMS_Image_Id) - 1) AS CBMS_Image_Id
            , SUM(CONVERT(
                      DECIMAL(28, 10)
                      , REPLACE(
  REPLACE(
                          REPLACE(
                                    REPLACE(
                                        REPLACE(
                                            REPLACE(
                                                (CASE WHEN (PATINDEX('%[0-9]-', CIC.CHARGE_VALUE)) > 0 THEN
                                                          STUFF(
                                                              CIC.CHARGE_VALUE
                                                              , PATINDEX('%[0-9]-', CIC.CHARGE_VALUE) + 1, 1, '')
                                                     ELSE CIC.CHARGE_VALUE
                                                 END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', ''))) AS CHARGE_VALUE
        INTO
            #CHARGE
        FROM
            dbo.CU_INVOICE_CHARGE CIC
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT CICA
                ON CICA.CU_INVOICE_CHARGE_ID = CIC.CU_INVOICE_CHARGE_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH AS cism
                ON cism.Account_ID = CICA.ACCOUNT_ID
                   AND  cism.CU_INVOICE_ID = CIC.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = CIC.Bucket_Master_Id
                   AND  bm.Commodity_Id = CIC.COMMODITY_TYPE_ID
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON CIC.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, csm.CU_INVOICE_ID) + ','
            FROM
                dbo.CU_INVOICE_SERVICE_MONTH csm
            WHERE
                cism.SERVICE_MONTH = csm.SERVICE_MONTH
                AND cism.Account_ID = csm.Account_ID
            FOR XML PATH('')) rn(CU_Invoice_Id)
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, ci.CBMS_IMAGE_ID) + ','
            FROM
                dbo.CU_INVOICE ci
                JOIN dbo.CU_INVOICE_SERVICE_MONTH csm
                    ON ci.CU_INVOICE_ID = csm.CU_INVOICE_ID
            WHERE
                csm.SERVICE_MONTH = cism.SERVICE_MONTH
                AND cism.Account_ID = csm.Account_ID
            FOR XML PATH('')) rn_image(CBMS_Image_Id)
        WHERE
            cism.Account_ID = @intAccount_ID
            AND c.Code_Value = 'Charge'
            AND cism.SERVICE_MONTH BETWEEN @dtStart_Date
                                   AND     @dtEnd_Date
        GROUP BY
            cism.SERVICE_MONTH
            , rn.CU_Invoice_Id
            , rn_image.CBMS_Image_Id;

        SELECT
            c.SERVICE_MONTH
            , c.CU_Invoice_Id
            , c.CBMS_Image_Id
            , c.CHARGE_VALUE
            , d.DETERMINANT_VALUE
        FROM
            #CHARGE c
            LEFT JOIN #DETERMINANT d
                ON d.CBMS_Image_Id = c.CBMS_Image_Id
                   AND  d.CU_Invoice_Id = c.CU_Invoice_Id
                   AND  d.SERVICE_MONTH = c.SERVICE_MONTH;

        DROP TABLE
            #DETERMINANT
            , #CHARGE;


    END;
GO
GRANT EXECUTE ON  [Budget].[Get_Invoices_By_Account_By_TimeStamp] TO [CBMSApplication]
GO
