SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.DV2GetAccountSiteDeltaInserts

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	MM			Mark
	HG			Hari
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	MM			11-19-07	delta insert for dv2_account_site
	HG			10/30/2009	Script modified to incorporate the change Contract_Start_date and Contract_End_date column renamed to Supplier_Account_Begin_dt and Supplier_Account_End_dt in DV2_ACCOUNT_SITE table.

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.DV2GetAccountSiteDeltaInserts
AS

BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.DV2_ACCOUNT_SITE(SITE_ID, ACCOUNT_ID, ACCOUNT_TYPE_ID, ACCOUNT_NUMBER, COMMODITY_TYPE_ID, Supplier_Account_Begin_dt,
		Supplier_Account_End_dt)
	SELECT
		b.SITE_ID, b.ACCOUNT_ID, b.ACCOUNT_TYPE_ID, b.ACCOUNT_NUMBER, b.COMMODITY_TYPE_ID
		, b.Supplier_Account_Begin_Dt, b.Supplier_Account_End_Dt
	FROM
		tempdb.dbo.DV2_ACCOUNT_SITE_DELTA b
		LEFT OUTER JOIN dbo.dv2_account_site a ON a.site_id = b.site_id
			AND a.account_id = b.account_id
			AND a.account_number = b.account_number
			AND a.commodity_type_id=b.commodity_type_id
	WHERE
		a.site_id IS NULL

END
GO
GRANT EXECUTE ON  [dbo].[DV2GetAccountSiteDeltaInserts] TO [CBMSApplication]
GO
