SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*********   
NAME:  dbo.Consumption_Level_Variance_Test_SAVE  
 
DESCRIPTION:  Used to update all the consumption level data  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Variance_Rule_Id			Int	
@Variance_Rule_Dtl_Id		Int  
@Consumption_Level_Id		int
@is_Data_Entry_Only			bit
@is_Active					bit
@Default_Tolerance			decimal(16,4)
@Test_Description			varchar(255)    
    

OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    

USAGE EXAMPLES:   
------------------------------------------------------------  


AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
NK			Nageswara Rao Kosuri         
RR			RAGHUREDDY
AP			Arunkumar Palanivel

Initials	Date			Modification  
------------------------------------------------------------  
NK			10/13/2009		Created
RR			13-JUNE-11		MAINT-703 Removed the GRANT permission TO "[Deb],[Nagakiran],[Nageswara],[Pandarinath]"
AP			Feb 24,2020		Added exclusion condition in procedure to avoid adding avt rules at account level


******/


CREATE PROCEDURE [dbo].[Consumption_Level_Variance_Test_SAVE]
      @Variance_Rule_Dtl_Id INT
    , @Consumption_Level_Id INT
    , @is_Data_Entry_Only   BIT
    , @is_Active            BIT
    , @Default_Tolerance    DECIMAL(16, 4) = NULL
    , @Test_Description     VARCHAR(255)   = NULL
AS
      BEGIN

            SET NOCOUNT ON;

            IF ( @is_Active = 0 )
                  BEGIN
                        DELETE      vrdac
                        FROM        dbo.Variance_Rule_Dtl_Account_Override vrdac
                                    JOIN
                                    dbo.Account_Variance_Consumption_Level avcl
                                          ON avcl.ACCOUNT_ID = vrdac.ACCOUNT_ID
                        WHERE       avcl.Variance_Consumption_Level_Id = @Consumption_Level_Id
                                    AND   vrdac.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                    AND   vrdac.Tolerance IS NULL;

                        UPDATE
                              dbo.Variance_Rule_Dtl
                        SET
                              IS_Active = @is_Active
                            , Is_Data_Entry_Only = @is_Data_Entry_Only
                        WHERE Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;

                  END;
            ELSE IF ( @is_Active = 1 )
                       BEGIN

                             INSERT INTO  dbo.Variance_Rule_Dtl_Account_Override (
                                                                                   Variance_Rule_Dtl_Id
                                                                                 , ACCOUNT_ID
                                                                             )
                                          SELECT
                                                @Variance_Rule_Dtl_Id
                                              , avcl.ACCOUNT_ID
                                          FROM  dbo.Account_Variance_Consumption_Level avcl
                                                LEFT JOIN
                                                dbo.Variance_Rule_Dtl_Account_Override vrdac
                                                      ON avcl.ACCOUNT_ID = vrdac.ACCOUNT_ID
                                                         AND vrdac.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                                          WHERE avcl.Variance_Consumption_Level_Id = @Consumption_Level_Id
                                                AND   vrdac.ACCOUNT_ID IS NULL
                                                  AND   NOT EXISTS
                                                            (     SELECT
                                                                        1
                                                                  FROM  Core.Client_Hier_Account cha
                                                                        JOIN
                                                                        dbo.Account_Variance_Consumption_Level avcl1
                                                                              ON avcl1.ACCOUNT_ID = cha.Account_Id
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Level vcl
                                                                              ON vcl.Variance_Consumption_Level_Id = avcl1.Variance_Consumption_Level_Id
                                                                                 AND vcl.Commodity_Id = cha.Commodity_Id
                                                                        JOIN
                                                                        dbo.Bucket_Master bm
                                                                              ON bm.Commodity_Id = vcl.Commodity_Id
                                                                        JOIN
                                                                        dbo.Variance_category_Bucket_Map vcbm
                                                                              ON vcbm.Bucket_master_Id = bm.Bucket_Master_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter vp
                                                                              ON vp.Variance_Category_Cd = vcbm.Variance_Category_Cd
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Baseline_Map vpbm
                                                                              ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule vr
                                                                              ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                                                        JOIN
                                                                        dbo.Variance_Rule_Dtl vrd
                                                                              ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                                                                        JOIN
                                                                        dbo.Variance_Parameter_Commodity_Map vpcm
                                                                              ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                                                        JOIN
                                                                        dbo.Variance_Processing_Engine vpe
                                                                              ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                                                        JOIN
                                                                        dbo.Code vpcat
                                                                              ON vpcat.Code_Id = vpe.Variance_Engine_Cd
                                                                        JOIN
                                                                        dbo.Variance_Consumption_Extraction_Service_Param_Value vesp
                                                                              ON vesp.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                                                  WHERE cha.Account_Id = avcl.ACCOUNT_ID
                                                                        AND   cha.Account_Type = 'Utility'
                                                                        AND   vpcat.Code_Value = 'Advanced Variance Test'
                                                                        AND   vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
																		AND vcl.Variance_Consumption_Level_Id = @Consumption_Level_Id );


                             UPDATE
                                    dbo.Variance_Rule_Dtl
                             SET
                                    IS_Active = @is_Active
                                  , Default_Tolerance = @Default_Tolerance
                                  , Test_Description = @Test_Description
                                  , Is_Data_Entry_Only = @is_Data_Entry_Only
                             WHERE  Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;

                       END;
      END;

GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_SAVE] TO [CBMSApplication]
GO