SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Contract_Meter_Volume_Sel_By_Contract_Id 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
	Exec dbo.Contract_Meter_Volume_Sel_By_Contract_Id @Contract_Id = 185074, @ED_CONTRACT_NUMBER = NULL
	Exec dbo.Contract_Meter_Volume_Sel_By_Contract_Id @Contract_Id = NULL, @ED_CONTRACT_NUMBER = '185074-0001'
	Exec dbo.Contract_Meter_Volume_Sel_By_Contract_Id @Contract_Id = 185232
   
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-01	GRM - Contract volumes enhancement - Created
******/
CREATE PROC [dbo].[Contract_Meter_Volume_Sel_By_Contract_Id]
    (
        @Contract_Id INT = NULL
        , @ED_CONTRACT_NUMBER VARCHAR(150) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Account_Vols AS TABLE
              (
                  Account_Id INT
                  , Meter_Id INT
                  , Service_Month DATE
                  , Determinant_Type VARCHAR(50)
                  , Volume DECIMAL(28, 12)
                  , Bucket_Master_Id INT
                  , Account_Number VARCHAR(50)
                  , Meter_Number VARCHAR(50)
                  , Is_Edited BIT
              );

        DECLARE
            @Uom_Type_Id INT
            , @Uom_Name VARCHAR(200)
            , @Frequency_Type_Id INT
            , @Frequency_Name VARCHAR(200)
            , @Commodity_Id INT
            , @On_Peak_Usage_Bucket_Master_Id INT
            , @Off_Peak_Usage_Bucket_Master_Id INT;

        SELECT
            @Contract_Id = ISNULL(@Contract_Id, c.CONTRACT_ID)
        FROM
            dbo.CONTRACT c
        WHERE
            c.ED_CONTRACT_NUMBER = @ED_CONTRACT_NUMBER;

        SELECT
            @Commodity_Id = c.COMMODITY_TYPE_ID
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;

        SELECT
            @Uom_Type_Id = uom.ENTITY_ID
            , @Uom_Name = uom.ENTITY_NAME
            , @Frequency_Type_Id = frq.ENTITY_ID
            , @Frequency_Name = frq.ENTITY_NAME
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
            INNER JOIN dbo.ENTITY uom
                ON uom.ENTITY_ID = cmv.UNIT_TYPE_ID
            INNER JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
        WHERE
            cmv.CONTRACT_ID = @Contract_Id;

        SELECT
            @On_Peak_Usage_Bucket_Master_Id = bm.Bucket_Master_Id
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'On-Peak Usage' )
            AND bm.Is_Category = 1;

        SELECT
            @Off_Peak_Usage_Bucket_Master_Id = bm.Bucket_Master_Id
        FROM
            dbo.Bucket_Master bm
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'Off-Peak Usage' )
            AND bm.Is_Category = 1;


        INSERT INTO @Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Account_Number
                 , Meter_Number
                 , Is_Edited
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Total'
            , cmv.VOLUME
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmv.Is_Edited, 0)
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Meter_Id = cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = chasupp.Supplier_Contract_ID
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cmv.METER_ID = chasupp.Meter_Id
                   AND  cmv.CONTRACT_ID = chasupp.Supplier_Contract_ID
                   AND  cmv.MONTH_IDENTIFIER = dd.DATE_D
        WHERE
            chasupp.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Type = 'Utility'
            AND chasupp.Account_Type = 'Supplier'
            AND dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, c.CONTRACT_START_DATE) + 1, c.CONTRACT_START_DATE)
                          AND     c.CONTRACT_END_DATE
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cmv.VOLUME
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmv.Is_Edited, 0);

        INSERT INTO @Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Account_Number
                 , Meter_Number
                 , Is_Edited
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'On Peak'
            , cmvd.Volume
            , ISNULL(cmvd.Bucket_Master_Id, @On_Peak_Usage_Bucket_Master_Id)
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmvd.Is_Edited, 0)
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Meter_Id = cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = chasupp.Supplier_Contract_ID
            INNER JOIN dbo.Code c2
                ON c2.Code_Id = c.Contract_Meter_Volume_Type_Cd
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cmv.METER_ID = chasupp.Meter_Id
                   AND  cmv.CONTRACT_ID = chasupp.Supplier_Contract_ID
                   AND  cmv.MONTH_IDENTIFIER = dd.DATE_D
            LEFT JOIN dbo.Contract_Meter_Volume_Dtl cmvd
                ON cmvd.CONTRACT_METER_VOLUME_ID = cmv.CONTRACT_METER_VOLUME_ID
                   AND  cmvd.Bucket_Master_Id = @On_Peak_Usage_Bucket_Master_Id
        WHERE
            chasupp.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Type = 'Utility'
            AND chasupp.Account_Type = 'Supplier'
            AND dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, c.CONTRACT_START_DATE) + 1, c.CONTRACT_START_DATE)
                          AND     c.CONTRACT_END_DATE
            AND c2.Code_Value = 'On/Off Peak'
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cmvd.Volume
            , ISNULL(cmvd.Bucket_Master_Id, @On_Peak_Usage_Bucket_Master_Id)
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmvd.Is_Edited, 0);

        INSERT INTO @Account_Vols
             (
                 Account_Id
                 , Meter_Id
                 , Service_Month
                 , Determinant_Type
                 , Volume
                 , Bucket_Master_Id
                 , Account_Number
                 , Meter_Number
                 , Is_Edited
             )
        SELECT
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , 'Off Peak'
            , cmvd.Volume
            , ISNULL(cmvd.Bucket_Master_Id, @Off_Peak_Usage_Bucket_Master_Id)
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmvd.Is_Edited, 0)
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Meter_Id = cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = chasupp.Supplier_Contract_ID
            INNER JOIN dbo.Code c2
                ON c2.Code_Id = c.Contract_Meter_Volume_Type_Cd
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cmv.METER_ID = chasupp.Meter_Id
                   AND  cmv.CONTRACT_ID = chasupp.Supplier_Contract_ID
                   AND  cmv.MONTH_IDENTIFIER = dd.DATE_D
            LEFT JOIN dbo.Contract_Meter_Volume_Dtl cmvd
                ON cmvd.CONTRACT_METER_VOLUME_ID = cmv.CONTRACT_METER_VOLUME_ID
                   AND  cmvd.Bucket_Master_Id = @Off_Peak_Usage_Bucket_Master_Id
        WHERE
            chasupp.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Type = 'Utility'
            AND chasupp.Account_Type = 'Supplier'
            AND dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, c.CONTRACT_START_DATE) + 1, c.CONTRACT_START_DATE)
                          AND     c.CONTRACT_END_DATE
            AND c2.Code_Value = 'On/Off Peak'
        GROUP BY
            cha.Account_Id
            , cha.Meter_Id
            , dd.DATE_D
            , cmvd.Volume
            , ISNULL(cmvd.Bucket_Master_Id, @Off_Peak_Usage_Bucket_Master_Id)
            , cha.Account_Number
            , cha.Meter_Number
            , ISNULL(cmvd.Is_Edited, 0);



        SELECT
            av.Account_Id
            , av.Meter_Id
            , av.Service_Month
            , av.Determinant_Type
            , av.Volume
            , @Uom_Type_Id AS Uom_Type_Id
            , @Uom_Name AS Uom_Name
            , @Frequency_Type_Id AS Frequency_Type_Id
            , @Frequency_Name AS Frequency_Name
            , av.Bucket_Master_Id
            , av.Account_Number
            , av.Meter_Number
            , av.Is_Edited
        FROM
            @Account_Vols av
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = av.Account_Id
                   AND  cha.Meter_Id = av.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Type = 'Utility'
            AND cha.Commodity_Id = @Commodity_Id
        ORDER BY
            cha.Account_Vendor_Name
            , ch.City
            , ch.Site_name
            , cha.Account_Number
            , av.Account_Id
            , av.Service_Month
            , av.Determinant_Type;

    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Sel_By_Contract_Id] TO [CBMSApplication]
GO
