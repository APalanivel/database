SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DealTicket_details] 
@Start_dt Datetime, 
@Enddt datetime,
@Ticket_id Int
AS
--DECLARE 	@Ticket_id	Int
BEGIN
	set nocount on
--	DECLARE C1 CURSOR
--	FOR 	SELECT 13

--SELECT RM_DEAL_TICKET.RM_DEAL_TICKET_ID
--	FROM 	RM_DEAL_TICKET
--	WHERE	RM_DEAL_TICKET.CLIENT_ID=1

--	OPEN 	C1
--	FETCH NEXT FROM C1 INTO @Ticket_id

--	WHILE @@FETCH_STATUS = 0
--	BEGIN
	if  (	select 	e.entity_name as deal_type 
		from 	rm_deal_ticket rmdt, 
			entity e 
		where 	rmdt.rm_deal_ticket_id = 13
		and 	rmdt.deal_type_id = e.entity_id) = 'fixed price' 
	begin
		print 'testing 122223223'
		IF (	select 	top 1 e.entity_name as deal_transaction_status
			from 	rm_deal_ticket_transaction_status rmdtts, 
				entity e
			where 	rmdtts.rm_deal_ticket_id = 13
			and 	rmdtts.deal_transaction_status_type_id = e.entity_id
			order by deal_transaction_date desc) = 'order placed'
		begin
			print 'testing 33333333333333'
--			SELECT * FROM Test(@Ticket_id, @Start_dt, @Enddt)
		select 	rmdtd.month_identifier, 
			sum(rmdtvd.hedge_volume) as TOTAL_HEDGE_VOLUME
		from 	rm_deal_ticket_details rmdtd, 
			rm_deal_ticket_volume_details rmdtvd 
		where 	rmdtd.rm_deal_ticket_id = 13 and 
			rmdtd.month_identifier between @Start_dt and @Enddt
		and 	rmdtd.rm_deal_ticket_details_id = rmdtvd.rm_deal_ticket_details_id
		group by rmdtd.month_identifier		

		end
		print 'testing 0000000000'
	end
--	FETCH NEXT FROM C1 INTO @Ticket_id
--   	END
--   CLOSE C1
--   DEALLOCATE C1
END
GO
GRANT EXECUTE ON  [dbo].[DealTicket_details] TO [CBMSApplication]
GO
