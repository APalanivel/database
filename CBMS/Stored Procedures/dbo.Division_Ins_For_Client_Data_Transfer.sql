SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Ins_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to transfer sites from a source client to destination client.  

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------      
	BEGIN TRAN
		DECLARE @Client_Hier_Id int
		EXEC [Division_Ins_For_Client_Data_Transfer] 
			@From_Division_Client_Hier_Id = 32395,
			@To_Client_Id = 109,
			@Client_Hier_Id = @Client_Hier_Id out
		SELECT @Client_Hier_Id
	ROLLBACK TRAN

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			19 Jul 2019		Created 	
 MSV			19 Aug 2019		ICDT-10 - Division Not Managed copy

*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Division_Ins_For_Client_Data_Transfer]
	(
		@From_Division_Client_Hier_Id INT,
		@To_Client_Id INT,
		@Client_Hier_Id INT OUT
	)
AS
BEGIN
	
    SET NOCOUNT ON

    DECLARE
        @From_Division_Id INT
		,@Division_Id INT
        ,@User_Info_Id INT
		,@Hier_level_Cd INT
		
    SELECT
        @User_Info_Id = ui.User_Info_Id
    FROM
        dbo.USER_INFO ui
    WHERE
        ui.username = 'conversion'
	
	SELECT	@From_Division_Id = ch.Sitegroup_Id
			,@Hier_level_Cd = ch.Hier_level_Cd
	FROM Core.Client_Hier ch 
	WHERE ch.Client_Hier_Id = @From_Division_Client_Hier_Id   

	BEGIN TRY
		BEGIN TRAN;

		IF @Hier_level_Cd = 100015
		BEGIN
		--Division Insert

			DECLARE @Savings_Category_Type_Table TABLE
			(
				Id INT IDENTITY(1, 1)
				,Savings_Category_Type_Id INT 
			)  

			DECLARE
				@sbaTypeId INT
				,@priceIndexId INT
				,@termPreferredTypeId INT
				,@contractReviewerTypeid INT
				,@decisionMakerTypeid INT
				,@signatoryTypeId INT
				,@divisionName VARCHAR(200)
				,@isInterestMinoritySuppliers INT
				,@isCorporateHedge INT
				,@naicsCode VARCHAR(30)
				,@taxNumber VARCHAR(30)
				,@dunsNumber VARCHAR(30)
				,@isCorporateDivision INT
				,@triggerRights INT
				,@miscComments VARCHAR(4000)
				,@notManaged INT
				,@contractingEntity VARCHAR(200)
				,@clientLegalStructure VARCHAR(4000)
				,@Savings_Category_Type_Id INT
				,@Audit_Type INT = 1 -- Add operation
				,@Entity_Name VARCHAR(MAX) = 'DIVISION_TABLE'
				,@Entity_Type INT = 500
			
			SELECT
				@sbaTypeId = sba_Type_Id
				,@priceIndexId = price_Index_Id
				,@termPreferredTypeId = term_Preferred_Type_Id
				,@contractReviewerTypeid = contract_Reviewer_Type_id
				,@decisionMakerTypeid = decision_Maker_Type_id
				,@signatoryTypeId = signatory_Type_Id
				,@divisionName = s.Sitegroup_Name
				,@isInterestMinoritySuppliers = is_Interest_Minority_Suppliers
				,@isCorporateHedge = is_Corporate_Hedge
				,@naicsCode = naics_Code
				,@taxNumber = tax_Number
				,@dunsNumber = duns_Number
				,@isCorporateDivision = is_Corporate_Division
				,@triggerRights = trigger_Rights
				,@miscComments = misc_Comments
				,@notManaged = not_Managed
				,@contractingEntity = contracting_Entity
				,@clientLegalStructure = client_Legal_Structure
			FROM
				dbo.Sitegroup AS s
				INNER JOIN dbo.Division_Dtl AS dd
						ON s.Sitegroup_Id = dd.SiteGroup_Id
			WHERE
				s.Sitegroup_Id = @From_Division_Id
			
			EXEC CBMS_ADD_DIVISION_P
				@sbaTypeId
				,@priceIndexId
				,@To_Client_Id
				,@termPreferredTypeId
				,@contractReviewerTypeid
				,@decisionMakerTypeid
				,@signatoryTypeId
				,@divisionName
				,@isInterestMinoritySuppliers
				,@isCorporateHedge
				,@naicsCode
				,@taxNumber
				,@dunsNumber
				,@isCorporateDivision
				,@triggerRights
				,@miscComments
				,0
				,@contractingEntity
				,@clientLegalStructure
				,@Division_Id OUTPUT
			
			--Not Managed Update
			IF @notManaged = 1
			BEGIN
				UPDATE dbo.Division_Dtl
				SET
				    NOT_MANAGED = 1
				WHERE SiteGroup_Id = @Division_Id
			END
			
			INSERT INTO @Savings_Category_Type_Table
				(
					Savings_Category_Type_Id 
				)
			SELECT
				Savings_Category_Type_Id
			FROM
				SAVINGS_CATEGORY_DIVISION_MAP
			WHERE
				DIVISION_ID = @From_Division_Id
			
			
			WHILE EXISTS ( SELECT
							1
							FROM
							@Savings_Category_Type_Table )
			BEGIN 

					SELECT TOP 1
						@Savings_Category_Type_Id = Savings_Category_Type_Id
					FROM
						@Savings_Category_Type_Table

					EXEC ADD_SAVINGS_DIVISION_MAP_P
						@Savings_Category_Type_Id
						,@Division_Id

					DELETE
						@Savings_Category_Type_Table
					WHERE
						Savings_Category_Type_Id = @Savings_Category_Type_Id

			END
		
			EXEC ADD_ENTITY_AUDIT_ITEM_P
				@Division_Id
				,@User_Info_Id
				,@Audit_Type
				,@Entity_Name
				,@Entity_Type 

			EXEC Add_Div_To_Corp_Role
				@Division_Id
			
		END
		ELSE
		BEGIN
		--Sitegroup Insert
		
			DECLARE
				@Sitegroup_Name VARCHAR(200)
				,@Sitegroup_Type_Cd INT
				,@Is_Smart_Group BIT
				,@Owner_User_Id INT
				,@Is_Locked BIT = 0
				,@Is_Complete BIT

			DECLARE @Division_Id_Out AS TABLE
			(Division_Id INT)

			SELECT @Owner_User_Id = ui.USER_INFO_ID
			FROM dbo.User_Info ui
            WHERE ui.USERNAME = 'GlobalSiteGroup'
			
			SELECT
				@Sitegroup_Name = s.Sitegroup_Name
				,@Sitegroup_Type_Cd = s.Sitegroup_Type_Cd
				,@Is_Smart_Group = s.Is_Smart_Group
				,@Is_Locked = s.Is_Locked
				,@Is_Complete = s.Is_Complete
			FROM
				dbo.Sitegroup s
			WHERE
				SiteGroup_Id = @From_Division_Id	
							
			INSERT INTO @Division_Id_Out
			(
				Division_Id
			)
			EXEC SiteGroup_INS
				@Sitegroup_Name 
				,@Sitegroup_Type_Cd
				,@To_Client_Id
				,@Is_Smart_Group
				,@Owner_User_Id
				,@User_Info_Id
				,@Is_Locked
				,@Is_Complete
			
			SELECT @Division_Id = Division_Id
			FROM @Division_Id_Out
			
		END

		--Output Client_Hier_Id
		SELECT @Client_Hier_Id = ch.Client_Hier_Id
		FROM Core.Client_Hier ch
		WHERE ch.Sitegroup_Id = @Division_Id
		AND ch.Site_Id = 0

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[Division_Ins_For_Client_Data_Transfer] TO [CBMSApplication]
GO
