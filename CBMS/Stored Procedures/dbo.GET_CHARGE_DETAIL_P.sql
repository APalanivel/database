SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_CHARGE_DETAIL_P
	@parentChargeId INT,
	@parentMasterId INT,
	@parentDisplayId CHAR(4),
	@parentRateId INT
AS
BEGIN

	SET NOCOUNT ON

		SELECT c.charge_id , 
		c.charge_master_id,
		cm.charge_display_id,
		rs.rate_id, 
		c.charge_parent_id rs_id   
	FROM dbo.rate_schedule rs JOIN dbo.charge c ON c.charge_parent_id = rs.rate_schedule_id			 
		INNER JOIN dbo.charge_master cm ON cm.charge_master_id = c.charge_master_id
		INNER JOIN dbo.entity ent1 ON ent1.entity_id = cm.charge_parent_type_id			
		INNER JOIN dbo.entity ent2 ON ent2.entity_id = cm.charge_parent_type_id			
	WHERE cm.charge_display_id = @parentDisplayId
		AND ent1.entity_name = 'Rate'
		AND ent1.entity_type = 120			
		AND ent2.entity_name = 'Rate Schedule'
		AND ent2.entity_type = 120
		AND c.charge_master_id = @parentMasterId
		AND c.charge_id <> @parentChargeId
		AND c.charge_id > @parentChargeId
		AND rs.rate_id <> @parentRateId
		
END
GO
GRANT EXECUTE ON  [dbo].[GET_CHARGE_DETAIL_P] TO [CBMSApplication]
GO
