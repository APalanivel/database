SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Ins                    
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Id								INT
 @Invoice_Processing_Config_Type_Cd			INT
 @Config_Start_Dt							DATE
 @Config_End_Dt								DATE			 NULL
 @Processing_Instruction_Category_Cd		INT
 @Instruction								NVARCHAR(MAX)
 @User_Info_Id								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'ProcessingInstruction'  ---> Processing_Instruction_Category_Cd


BEGIN TRAN


SELECT
    *
FROM
    [dbo].[Account_Invoice_Processing_Config]
where
    Account_Id = 325423 and Config_Start_Dt = '2019-11-01'


SELECT
    *
FROM
    [dbo].[Account_Invoice_Processing_Config_Log] a
    INNER JOIN [dbo].[Account_Invoice_Processing_Config] b
        On a.Account_Invoice_Processing_Config_Id = b.Account_Invoice_Processing_Config_Id
where
    b.account_id = 325423
	And b.Config_Start_Dt = '2019-11-01'


EXEC dbo.Account_Invoice_Processing_Config_Ins
    @Account_Id = 325423
    , @Processing_Instruction_Category_Cd = 100237
    , @Config_Start_Dt = '2019-11-01'
    , @Config_End_Dt = '2018-12-31'
    , @Processing_Instruction = 'Hello Narayana'    
    , @User_Info_Id = 49

SELECT
    *
FROM
    [dbo].[Account_Invoice_Processing_Config]
where
    Account_Id = 325423 and Config_Start_Dt = '2019-11-01'

SELECT
    *
FROM
    [dbo].[Account_Invoice_Processing_Config_Log] a
    INNER JOIN [dbo].[Account_Invoice_Processing_Config] b
        On a.Account_Invoice_Processing_Config_Id = b.Account_Invoice_Processing_Config_Id
where
    b.account_id = 325423
	And b.Config_Start_Dt = '2019-11-01'


ROLLBACK TRAN
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Ins]
    (
        @Account_Id INT
        , @Processing_Instruction_Category_Cd INT
        , @Config_Start_Dt DATE
        , @Config_End_Dt DATE = NULL
        , @Processing_Instruction NVARCHAR(MAX)
        , @User_Info_Id INT
        , @Is_Updated_Using_Apply_All BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Account_Invoice_Processing_Config_Id INT
            , @Current_Value NVARCHAR(MAX)
            , @Instruction_Category VARCHAR(25)
            , @PI_Field_Name NVARCHAR(255);


        SELECT
            @Instruction_Category = c.Code_Value
        FROM
            dbo.Code c
        WHERE
            c.Code_Id = @Processing_Instruction_Category_Cd;


        BEGIN TRY
            BEGIN TRAN;

            INSERT INTO dbo.Account_Invoice_Processing_Config
                 (
                     Account_Id
                     , Processing_Instruction_Category_Cd
                     , Config_Start_Dt
                     , Config_End_Dt
                     , Processing_Instruction
                     , Watch_List_Group_Info_Id
                     , Is_Active
                     , Created_User_Id
                     , Created_Ts
                     , Updated_User_Id
                     , Last_Change_Ts
                 )
            SELECT
                @Account_Id
                , @Processing_Instruction_Category_Cd
                , @Config_Start_Dt
                , @Config_End_Dt
                , @Processing_Instruction
                , NULL
                , 1
                , @User_Info_Id
                , GETDATE()
                , @User_Info_Id
                , GETDATE();

            SELECT  @Account_Invoice_Processing_Config_Id = SCOPE_IDENTITY();



            SET @PI_Field_Name = @Instruction_Category + N' ' + N'Processing Instructions';

            SET @Current_Value = CONVERT(VARCHAR(20), @Config_Start_Dt, 101) + N' - '
                                 + ISNULL(CONVERT(VARCHAR(20), @Config_End_Dt, 101), ' Open End Date');


            EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                , @Field_Name = @PI_Field_Name
                , @Change_Type = 'Created'
                , @Previous_Value = NULL
                , @Current_Value = @Processing_Instruction
                , @Is_Updated_Using_Apply_All = @Is_Updated_Using_Apply_All
                , @Event_By_User_Id = @User_Info_Id;


            EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                , @Field_Name = 'Configuration Dates'
                , @Change_Type = 'Created'
                , @Previous_Value = NULL
                , @Current_Value = @Current_Value
                , @Is_Updated_Using_Apply_All = @Is_Updated_Using_Apply_All
                , @Event_By_User_Id = @User_Info_Id;



            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;







GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Ins] TO [CBMSApplication]
GO
