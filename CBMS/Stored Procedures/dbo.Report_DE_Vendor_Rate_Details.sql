SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/*****
        
NAME: dbo.Report_DE_Vendor_Rate_Details      
    
DESCRIPTION:        
	used to get the details of vendors
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC Report_DE_Vendor_Rate_Details      
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
AKR   Ashok Kumar Raju
     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  AKR    2012-05-01  Cloned from select statement of Job "2008_RateVendorData"
******/

CREATE PROCEDURE dbo.Report_DE_Vendor_Rate_Details
AS 
BEGIN        
    
      SET NOCOUNT ON ;    
      
      DECLARE @Vendor_Type INT
      
      SELECT
            @Vendor_Type = e.Entity_Id
      FROM
            dbo.ENTITY e
      WHERE
            ENTITY_NAME = 'Utility'
            AND ENTITY_DESCRIPTION = 'Vendor'
      
      SELECT
            V.vendor_Id [Vendor ID]
           ,r.rate_id [Rate ID]
           ,v.vendor_name [Vendor Name]
           ,r.rate_name [Rate Name]
           ,st.state_name [State]
           ,Com.commodity_name [Commodity]
      FROM
            dbo.vendor v
            JOIN dbo.rate r
                  ON r.vendor_id = v.vendor_id
            JOIN dbo.vendor_state_map vsm
                  ON vsm.vendor_id = v.vendor_id
            JOIN dbo.Commodity com
                  ON com.commodity_id = r.commodity_type_id
            JOIN dbo.state st
                  ON st.state_id = vsm.state_id
      WHERE
            v.is_history != 1
            AND v.vendor_type_id = @Vendor_Type
    
END



;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Vendor_Rate_Details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Vendor_Rate_Details] TO [CBMSApplication]
GO
