SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                              
NAME:                              
  [dbo].[Report_DE_JoinedFile_All]                   
                        
DESCRIPTION:                         
 Client Info, Accounts, Meters, Contract, Utility , Rate data                      
                        
                        
INPUT PARAMETERS:                            
      Name              DataType          Default     Description                            
------------------------------------------------------------                            
  @region_name  VARCHAR(10)   NULL                      
                      
                      
OUTPUT PARAMETERS:                      
      Name              DataType          Default     Description                      
------------------------------------------------------------                       
                      
                      
USAGE EXAMPLES:                      
------------------------------------------------------------                      
                  
Stress /Prod                      
exec [dbo].[Report_DE_JoinedFile_All] 'ne'                      
exec [dbo].[Report_DE_JoinedFile_All] 'ch'                      
                      
                      
AUTHOR INITIALS:                      
                       
Initials Name          
------------------------------------------------------------          
AKR   Ashok Kumar Raju
SP	  Sandeep Pigilam    
PS    Patchava Srinivasarao      
      
Initials Date  Modification          
------------------------------------------------------------          
AKR   2012-06-07 Created.      
AKR   2014-11-25 Modified to be used by excel exporter with tab delimited.   
KVK	  12/03/2014 replace new line and carraige return with empty
KVK	  4/2/2015  added @ symbol infornt of Accountnumber      
SP	  2015-05-20 Removed the where clause which INCLUDES client types of ?Commercial? and ?Industrial? 
				 to EXCLUDE  Client Types ?Demo?, ?GFE?, ?Shared Savings?, ?Software? 
SP	  2017-03-10 Added new columns
				 SupplierAccountStartDate
				 SupplierAccountEndDate when null will be shown as Unspecified and date is casted to varchar as to show Unspecified when null.
				 SupplierAccountCreatedBy - show username similar to the existing column "ContractCreatedBy" in the Joined File today.
				 SupplierAccountCreatedDate  - show date and timestamp similar to the existing column "AccountCreated" in the Joined File today.
				 Displaying Contract# as DMO Label For DMO suppplier account. 
lec   2018-02-01 Added new Contract Product Type column
PS    2018-08-22 Added new Meter_Id column
lec	  2020-01-27 Modified to Left join Supplier Service level for Supplier Accounts. No longer required.  Missing supplier accounts in joined file.
lec	  2020-03-24 Added Rate_ID , s.CONTRACTING_ENTITY [SiteContractingEntity], regmarketsanalyst
lec	  2020-04-07 Modified Site Contracting Entity to pull with replace to accomodate in line carriage return
lec	  2020-06-15 Modified to add alternate account number, rolling meter for contracts, original contract end date
*/      
      
CREATE  PROCEDURE [dbo].[Report_DE_JoinedFile_All]
      ( 
       @region_name VARCHAR(10) = NULL )
AS 
BEGIN      
      
      SET NOCOUNT ON;      
      
      DECLARE
            @Supplier_Contract_Type_Id INT
           ,@Utility_Contract_Type_Id INT
           ,@Contract_End_Date DATE = '2011-12-31'
           ,@Account_Table INT
           ,@Contract_Table INT
           ,@Site_Table INT
           ,@Client_Type_Id_Demo INT
           ,@Client_Type_Id_GfE INT
           ,@Client_Type_Id_Shared_Savings INT
           ,@Client_Type_Id_Software INT
           ,@Client_Id INT                       
          
      SET @Contract_End_Date = CAST(DATEPART(yy, GETDATE()) - 3 AS VARCHAR) + '-12-31'    
          
      CREATE TABLE #UtilityAccts_CTE
            ( 
             Account_Number VARCHAR(50)
            ,Account_Id INT
            ,UtilityAccountServiceLevel VARCHAR(200)
            ,AccountCreated DATETIME
            ,AccountInvoiceNotExpected VARCHAR(10)
            ,AccountNotManaged VARCHAR(10)
            ,UtilityAccountInvoiceSource VARCHAR(200)
            ,Utility VARCHAR(200)
            ,CommodityType VARCHAR(50)
            ,Rate VARCHAR(200)
			,Rate_ID INT 
            ,PurchaseMethod VARCHAR(50)
            ,Site_Id INT
            ,Client_Hier_Id INT
            ,ADDRESS_ID INT
            ,MeterNumber VARCHAR(50)
            ,Meter_Id INT
            ,AccountCreatedBy VARCHAR(30)
            ,Consumption_Level_Desc VARCHAR(100)
            ,IsDataEntryOnly VARCHAR(10)
            ,Is_Broker_Account VARCHAR(5)
            ,Broker_fee DECIMAL(28, 12)
            ,BrokerFee_Currency_Unit VARCHAR(10)
            ,BrokerFee_Uom_Unit VARCHAR(25)
            ,Analyst_Id INT
            ,Account_Analyst_Mapping_Cd INT
            ,Site_Analyst_Mapping_Cd INT
            ,Client_Analyst_Mapping_Cd INT 
			,RegMarketsAnalyst varchar(100)
			,AlternateAccountNumber varchar(200))                          
             
      CREATE TABLE #Contracts_CTE
            ( 
             ContractId INT
            ,Account_Id INT
            ,BaseContractId INT
            ,ContractNumber VARCHAR(150)
            ,ContractPricingStatus VARCHAR(10)
            ,ContractType VARCHAR(200)
            ,ContractedVendor VARCHAR(200)
            ,ContractStartDate DATETIME
            ,ContractEndDate DATETIME
            ,NotificationDays DECIMAL(12, 2)
            ,NotificationDate DATETIME
            ,ContractTriggerRights VARCHAR(10)
            ,FullRequirements VARCHAR(10)
            ,CONTRACT_PRICING_SUMMARY VARCHAR(1000)
            ,ContractComments VARCHAR(4000)
            ,Currency VARCHAR(200)
            ,RenewalType VARCHAR(200)
            ,SupplierAccountNumber VARCHAR(50)
            ,SupplierAccountServiceLevel VARCHAR(200)
            ,SupplierAccountInvoiceSource VARCHAR(200)
            ,ContractCreatedBy VARCHAR(30)
            ,ContractCreatedDate DATETIME
            ,ContractRecalcType VARCHAR(30)
            ,Meter_Id INT
            ,SupplierAccountStartDate DATETIME
            ,SupplierAccountEndDate DATETIME
            ,SupplierAccountCreatedBy VARCHAR(30)
            ,SupplierAccountCreatedDate DATETIME
            ,ContractProductType VARCHAR(200)
			,SupplierAltAccount VARCHAR(200)
			,is_Rolling_meter bit
			,Original_contract_end_date datetime)                      
          
          
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT                   
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'            
                  
      SELECT
            @Client_Id = C.CLIENT_ID
      FROM
            dbo.CLIENT C
      WHERE
            C.CLIENT_NAME = 'AT&T Services, Inc.'   
                     
                                 
                           
                               
      SELECT
            @Supplier_Contract_Type_Id = MAX(CASE WHEN ENTITY_NAME = 'Supplier' THEN ENTITY_ID
                                             END)
           ,@Utility_Contract_Type_Id = MAX(CASE WHEN ENTITY_NAME = 'Utility' THEN ENTITY_ID
                                            END)
           ,@Account_Table = MAX(CASE WHEN ENTITY_NAME = 'Account_Table' THEN ENTITY_ID
                                 END)
           ,@Contract_Table = MAX(CASE WHEN ENTITY_NAME = 'CONTRACT_TABLE' THEN ENTITY_ID
                                  END)
           ,@Site_Table = MAX(CASE WHEN ENTITY_NAME = 'Site_Table' THEN ENTITY_ID
                              END)
           ,@Client_Type_Id_Demo = MAX(CASE WHEN ENTITY_NAME = 'Demo'
                                                 AND ENTITY_DESCRIPTION = 'Client Type' THEN ENTITY_ID
                                       END)
           ,@Client_Type_Id_GfE = MAX(CASE WHEN ENTITY_NAME = 'GfE'
                                                AND ENTITY_DESCRIPTION = 'Client Type' THEN ENTITY_ID
                                      END)
           ,@Client_Type_Id_Shared_Savings = MAX(CASE WHEN ENTITY_NAME = 'Shared Savings'
                                                           AND ENTITY_DESCRIPTION = 'Client Type' THEN ENTITY_ID
                                                 END)
           ,@Client_Type_Id_Software = MAX(CASE WHEN ENTITY_NAME = 'Software'
                                                     AND ENTITY_DESCRIPTION = 'Client Type' THEN ENTITY_ID
                                           END)
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION IN ( 'Table_Type', 'Table Type', 'Contract Type', 'Client Type' );                      
                 
--Utility Accounts                      
                 
                  
      ;            
      WITH  
	  
	  regmarkets_analyst_CTE as
	  (
		 Select distinct 
		v.vendor_id [VendorID]
		,v.vendor_name [Vendor]
		,ui.first_name + ' ' + ui.LAST_NAME [RegMarketsAnalyst]
		,st.state_name [State]
		from vendor v 
		join VENDOR_COMMODITY_MAP vcm on vcm.VENDOR_ID = v.VENDOR_ID
		join VENDOR_STATE_MAP vsm on vsm.VENDOR_ID = v.VENDOR_ID
		join state st on st.STATE_ID = vsm.STATE_ID
		join UTILITY_DETAIL ud on ud.vendor_id = v.vendor_id
		join Utility_Detail_Analyst_Map udam on udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
		join user_info ui on ui.USER_INFO_ID = udam.Analyst_ID
		join GROUP_INFO gi on gi.GROUP_INFO_ID= udam.Group_Info_ID
		where
		gi.GROUP_INFO_ID = 133
)
	  ,
	  cte_Accounts_List
              AS ( SELECT
                        cha.Account_Number Account_Number
                       ,cha.Account_Id Account_Id
                       ,svl.entity_Name UtilityAccountServiceLevel
                       ,ea.MODIFIED_DATE AccountCreated
                       ,AccountInvoiceNotExpected = CASE cha.Account_Not_Expected
                                                      WHEN 1 THEN 'Yes'
                                                      ELSE 'No'
                                                    END
                       ,AccountNotManaged = CASE cha.Account_Not_Managed
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,istyp.ENTITY_NAME UtilityAccountInvoiceSource
                       ,cha.Account_Vendor_Name Utility
                       ,comm.Commodity_Name CommodityType
                       ,cha.Rate_Name Rate
					   ,cha.Rate_Id Rate_ID
                       ,'NotApplicable' PurchaseMethod
                       ,ch.Site_Id
                       ,cha.client_hier_Id
                       ,cha.Meter_Address_ID ADDRESS_ID
                       ,cha.Meter_Number MeterNumber
                       ,cha.Meter_Id
                       ,ui.username AccountCreatedBy
                       ,vcl.Consumption_Level_Desc
                       ,IsDataEntryOnly = CASE cha.Account_Is_Data_Entry_Only
                                            WHEN 1 THEN 'Yes'
                                            ELSE 'No'
                                          END
                       ,Is_Broker_Account = CASE cha.Account_Is_Broker
                                              WHEN 1 THEN 'Yes'
                                              ELSE 'No'
                                            END
                       ,acbf.Broker_Fee
                       ,ecur.CURRENCY_UNIT_NAME BrokerFee_CurrencyUnit
                       ,euom.ENTITY_NAME BrokerFee_UomName
                       ,ch.Client_Id
                       ,cha.Commodity_Id
                       ,cha.Account_Vendor_Id
                       ,COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
					   ,rc.RegMarketsAnalyst
					   ,cha.Alternate_Account_Number AlternateAccountNumber 
                   FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN dbo.Entity svl
                              ON svl.entity_id = cha.Account_Service_level_Cd
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ea.ENTITY_ID = @Account_Table
                                 AND ea.AUDIT_TYPE = 1
                        LEFT OUTER JOIN dbo.ENTITY istyp
                              ON istyp.ENTITY_ID = cha.Account_Invoice_Source_Cd
                        INNER JOIN dbo.Commodity comm
                              ON comm.commodity_ID = cha.Commodity_Id
                        INNER JOIN dbo.user_info ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        INNER JOIN Account_Variance_Consumption_Level av
                              ON av.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                                 AND vcl.Commodity_Id = cha.Commodity_Id
                        LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON cha.Account_Id = acbf.Account_Id
                                 AND cha.Commodity_Id = acbf.Commodity_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                              ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
                        LEFT OUTER JOIN dbo.ENTITY euom
                              ON euom.ENTITY_ID = acbf.UOM_Entity_Id
						left outer join regmarkets_analyst_CTE rc on rc.VendorID = cha.Account_Vendor_Id
                   WHERE
                        cha.Account_Type = 'Utility'
                        AND ch.client_type_id NOT IN ( @Client_Type_Id_Demo, @Client_Type_Id_GfE, @Client_Type_Id_Shared_Savings, @Client_Type_Id_Software )
                        AND ch.CLIENT_ID != @Client_Id)
            INSERT      INTO #UtilityAccts_CTE
                        ( 
                         Account_Number
                        ,Account_Id
                        ,UtilityAccountServiceLevel
                        ,AccountCreated
                        ,AccountInvoiceNotExpected
                        ,AccountNotManaged
                        ,UtilityAccountInvoiceSource
                        ,Utility
                        ,CommodityType
                        ,Rate
						,Rate_ID
                        ,PurchaseMethod
                        ,Site_Id
                        ,Client_Hier_Id
                        ,ADDRESS_ID
                        ,MeterNumber
                        ,Meter_Id
                        ,AccountCreatedBy
                        ,Consumption_Level_Desc
                        ,IsDataEntryOnly
                        ,Is_Broker_Account
                        ,Broker_fee
                        ,BrokerFee_Currency_Unit
                        ,BrokerFee_Uom_Unit
                        ,Analyst_Id
                        ,Account_Analyst_Mapping_Cd
                        ,Site_Analyst_Mapping_Cd
                        ,Client_Analyst_Mapping_Cd 
						,RegMarketsAnalyst
						,AlternateAccountNumber )
                        SELECT
                              acc.Account_Number
                             ,acc.Account_Id
                             ,acc.UtilityAccountServiceLevel
                             ,acc.AccountCreated
                             ,acc.AccountInvoiceNotExpected
                             ,acc.AccountNotManaged
                             ,acc.UtilityAccountInvoiceSource
                             ,acc.Utility
                             ,acc.CommodityType
                             ,acc.Rate
							 ,acc.Rate_ID
                             ,acc.PurchaseMethod
                             ,acc.Site_Id
                             ,acc.Client_Hier_Id
                             ,acc.ADDRESS_ID
                             ,acc.MeterNumber
                             ,acc.Meter_Id
                             ,acc.AccountCreatedBy
                             ,acc.Consumption_Level_Desc
                             ,acc.IsDataEntryOnly
                             ,acc.Is_Broker_Account
                             ,acc.Broker_Fee
                             ,acc.BrokerFee_CurrencyUnit
                             ,acc.BrokerFee_UomName
                             ,CASE WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                                   WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                              END AS Analyst_Id
                             ,acc.Analyst_Mapping_Cd
                             ,acc.Site_Analyst_Mapping_Cd
                             ,acc.Client_Analyst_Mapping_Cd
							 ,RegMarketsAnalyst
							 ,AlternateAccountNumber 
                        FROM
                              cte_Accounts_List acc
                              LEFT OUTER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                                    ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                       AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                              LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                                    ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                              LEFT OUTER JOIN ( core.Client_Commodity ccc
                                                INNER JOIN dbo.Code cd
                                                      ON cd.Code_Id = ccc.Commodity_Service_Cd
                                                         AND cd.Code_Value = 'Invoice' )
                                                ON ccc.Client_Id = acc.Client_Id
                                                   AND ccc.Commodity_Id = acc.COMMODITY_ID
                              LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca
                                    ON aca.Account_Id = acc.Account_Id
                                       AND aca.Commodity_Id = acc.COMMODITY_ID
                              LEFT OUTER JOIN dbo.SITE_Commodity_Analyst sca
                                    ON sca.Site_Id = acc.Site_Id
                                       AND sca.Commodity_Id = acc.COMMODITY_ID
                              LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca
                                    ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id          
          
      CREATE CLUSTERED INDEX [ix_#UtilityAccts_CTE__Site_Id__Address_ID] ON [#UtilityAccts_CTE] ([Site_Id],[ADDRESS_ID])                   
      
  
--Contracts          
           
      INSERT      INTO #Contracts_CTE
                  ( 
                   ContractId
                  ,Account_Id
                  ,BaseContractId
                  ,ContractNumber
                  ,ContractPricingStatus
                  ,ContractType
                  ,ContractedVendor
                  ,ContractStartDate
                  ,ContractEndDate
                  ,NotificationDays
                  ,NotificationDate
                  ,ContractTriggerRights
                  ,FullRequirements
                  ,CONTRACT_PRICING_SUMMARY
                  ,ContractComments
                  ,Currency
                  ,RenewalType
                  ,SupplierAccountNumber
                  ,SupplierAccountServiceLevel
                  ,SupplierAccountInvoiceSource
                  ,ContractCreatedBy
                  ,ContractCreatedDate
                  ,ContractRecalcType
                  ,Meter_Id
                  ,SupplierAccountStartDate
                  ,SupplierAccountEndDate
                  ,SupplierAccountCreatedBy
                  ,SupplierAccountCreatedDate
                  ,ContractProductType 
				  ,SupplierAltAccount
				  ,Is_Rolling_Meter
				  ,Original_contract_end_date)
                  SELECT
                        c.Contract_Id ContractId
                       ,cha.Account_Id Account_Id
                       ,c.BASE_CONTRACT_ID BaseContractId
                       ,c.ED_CONTRACT_NUMBER ContractNumber
                       ,ContractPricingStatus = CASE WHEN lps.CONTRACT_ID IS NULL THEN 'NotBuilt'
                                                     ELSE 'Built'
                                                END
                       ,e.ENTITY_NAME ContractType
                       ,cha.Account_Vendor_Name ContractedVendor
                       ,c.CONTRACT_START_DATE ContractStartDate
                       ,c.CONTRACT_END_DATE ContractEndDate
                       ,c.NOTIFICATION_DAYS NotificationDays
                       ,NotificationDate = contract_end_date - ( Notification_Days + 15 )
                       ,ContractTriggerRights = CASE c.is_contract_trigger_rights
                                                  WHEN 1 THEN 'Yes'
                                                  ELSE 'No'
                                                END
                       ,FullRequirements = CASE c.is_contract_full_requirement
                                             WHEN 1 THEN 'Yes'
                                             ELSE 'No'
                                           END
                       ,c.CONTRACT_PRICING_SUMMARY
                       ,c.CONTRACT_COMMENTS ContractComments
                       ,cu.CURRENCY_UNIT_NAME Currency
                       ,e1.ENTITY_NAME RenewalType
                       ,cha.Account_Number SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,ui.USERNAME ContractCreatedBy
                       ,ea.MODIFIED_DATE ContractCreatedDate
                       ,e4.ENTITY_NAME ContractRecalcType
                       ,cha.Meter_Id
                       ,cha.Supplier_Account_Begin_Dt AS SupplierAccountStartDate
                       ,cha.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui1.USERNAME SupplierAccountCreatedBy
                       ,ea1.MODIFIED_DATE SupplierAccountCreatedDate
                       ,prodtype.code_value ContractProductType
					   ,cha.Alternate_Account_Number [SupplierAltAccount]
					   ,samm.Is_Rolling_Meter
					   ,c.Original_contract_end_date
                  FROM
                        dbo.Contract c
                        INNER JOIN Core.Client_Hier_Account cha
                              ON c.CONTRACT_ID = cha.Supplier_Contract_ID
							  inner join SUPPLIER_ACCOUNT_METER_MAP samm on samm.Contract_ID = c.contract_id
							  and samm.METER_ID = cha.meter_id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
                        INNER JOIN dbo.CURRENCY_UNIT cu
                              ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
                        INNER JOIN dbo.ENTITY e1
                              ON e1.ENTITY_ID = c.RENEWAL_TYPE_ID
                        --INNER JOIN dbo.ENTITY e2
                        --      ON e2.ENTITY_ID = cha.Account_Service_level_Cd
						left outer  JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = cha.Account_Service_level_Cd
                        LEFT OUTER JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = cha.Account_Invoice_Source_Cd
                        LEFT OUTER JOIN ENTITY e4
                              ON e4.ENTITY_ID = c.CONTRACT_RECALC_TYPE_ID
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
                                 AND ea.ENTITY_ID = @Contract_Table --494                      
                                 AND ea.AUDIT_TYPE = 1
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        LEFT OUTER JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                              ON lps.CONTRACT_ID = c.CONTRACT_ID
                        LEFT JOIN dbo.ENTITY_AUDIT ea1
                              ON ea1.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ea1.ENTITY_ID = @Account_Table --494                      
                                 AND ea1.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui1
                              ON ui1.USER_INFO_ID = ea1.USER_INFO_ID
                        LEFT JOIN code prodtype
                              ON prodtype.code_id = c.Contract_Product_Type_Cd
                  WHERE
                        ( c.CONTRACT_TYPE_ID = @Utility_Contract_Type_Id
                          OR ( c.CONTRACT_TYPE_ID = @Supplier_Contract_Type_Id
                               AND c.CONTRACT_END_DATE > @Contract_End_Date ) )
                        AND cha.Account_Type = 'Supplier'
                        AND ch.Client_Type_Id NOT IN ( @Client_Type_Id_Demo, @Client_Type_Id_GfE, @Client_Type_Id_Shared_Savings, @Client_Type_Id_Software )
                        AND ch.CLIENT_ID != @Client_Id  
                        
                        
                        
      INSERT      INTO #Contracts_CTE
                  ( 
                   ContractId
                  ,Account_Id
                  ,BaseContractId
                  ,ContractNumber
                  ,ContractPricingStatus
                  ,ContractType
                  ,ContractedVendor
                  ,ContractStartDate
                  ,ContractEndDate
                  ,NotificationDays
                  ,NotificationDate
                  ,ContractTriggerRights
                  ,FullRequirements
                  ,CONTRACT_PRICING_SUMMARY
                  ,ContractComments
                  ,Currency
                  ,RenewalType
                  ,SupplierAccountNumber
                  ,SupplierAccountServiceLevel
                  ,SupplierAccountInvoiceSource
                  ,ContractCreatedBy
                  ,ContractCreatedDate
                  ,ContractRecalcType
                  ,Meter_Id
                  ,SupplierAccountStartDate
                  ,SupplierAccountEndDate
                  ,SupplierAccountCreatedBy
                  ,SupplierAccountCreatedDate
                  ,ContractProductType
				  ,SupplierAltAccount
				  ,Is_Rolling_Meter
				  ,Original_contract_end_date)
                  SELECT
                        -1 ContractId
                       ,cha.Account_Id Account_Id
                       ,NULL BaseContractId
                       ,NULL ContractNumber
                       ,NULL ContractPricingStatus
                       ,NULL ContractType
                       ,NULL ContractedVendor
                       ,NULL ContractStartDate
                       ,NULL ContractEndDate
                       ,NULL NotificationDays
                       ,NULL NotificationDate
                       ,NULL ContractTriggerRights
                       ,NULL FullRequirements
                       ,NULL CONTRACT_PRICING_SUMMARY
                       ,NULL ContractComments
                       ,NULL Currency
                       ,NULL RenewalType
                       ,suppacc.Account_Number SupplierAccountNumber
                       ,e2.ENTITY_NAME SupplierAccountServiceLevel
                       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
                       ,NULL ContractCreatedBy
                       ,NULL ContractCreatedDate
                       ,NULL ContractRecalcType
                       ,cha.Meter_Id
                       ,suppacc.Supplier_Account_Begin_Dt AS SupplierAccountStartDate
                       ,suppacc.Supplier_Account_End_Dt AS SupplierAccountEndDate
                       ,ui.USERNAME SupplierAccountCreatedBy
                       ,ea.MODIFIED_DATE SupplierAccountCreatedDate
                       ,NULL AS ContractProductType
					   ,suppacc.Alternate_Account_Number
					   ,Is_Rolling_Meter
					   ,null as Original_contract_end_date
                  FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        INNER JOIN dbo.ACCOUNT suppacc
                              ON samm.ACCOUNT_ID = suppacc.Account_Id
                        LEFT JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = suppacc.Account_Id
                                 AND ea.ENTITY_ID = @Account_Table --494                      
                                 AND ea.AUDIT_TYPE = 1
                        LEFT JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = ea.USER_INFO_ID
                        left outer JOIN dbo.ENTITY e2
                              ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
						--INNER JOIN dbo.ENTITY e2
      --                        ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID
                        LEFT OUTER JOIN dbo.ENTITY e3
                              ON e3.ENTITY_ID = suppacc.INVOICE_SOURCE_TYPE_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON samm.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        samm.Contract_ID = -1
                        AND ( suppacc.Supplier_Account_End_Dt IS NULL
                              OR suppacc.Supplier_Account_End_Dt > @Contract_End_Date )
                        AND ch.Client_Type_Id NOT IN ( @Client_Type_Id_Demo, @Client_Type_Id_GfE, @Client_Type_Id_Shared_Savings, @Client_Type_Id_Software )
                        AND ch.CLIENT_ID != @Client_Id  
                             
                                          
                                         
                                              
            --SiteList                      
                                  
                                  
      CREATE CLUSTERED INDEX [ix_#Contracts_CTE__Meter_Id] ON [#Contracts_CTE] ([Meter_Id])                                    
            
            
      SELECT
            REPLACE(REPLACE(REPLACE(ch.Client_Name, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [ClientName]
           ,ch.Client_Id [ClientID]
           ,ClientNotManaged = CASE ch.Client_Not_Managed
                                 WHEN 1 THEN 'Yes'
                                 ELSE 'No'
                               END
           ,REPLACE(REPLACE(REPLACE(ch.Sitegroup_Name, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [DivisionName]
           ,ch.Sitegroup_Id [DivisionID]
           ,DivisionNotManaged = CASE ch.Site_Not_Managed
                                   WHEN 1 THEN 'Yes'
                                   ELSE 'No'
                                 END
           ,REPLACE(REPLACE(REPLACE(ch.Site_name, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [SiteName]
		   
           ,ch.Site_Id [SiteID]
           ,ea.MODIFIED_DATE SiteCreated
           ,REPLACE(REPLACE(REPLACE(s.SITE_REFERENCE_NUMBER, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [SiteRef.]
           ,REPLACE(REPLACE(REPLACE(rg.GROUP_NAME, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') SiteRMGroup
           ,SiteNotManaged = CASE ch.Site_Not_Managed
                               WHEN 1 THEN 'Yes'
                               ELSE 'No'
                             END
           ,SiteClosed = CASE ch.Site_Closed
                           WHEN 1 THEN 'Yes'
                           ELSE 'No'
                         END
           ,ch.Site_Type_Name SiteType
           ,REPLACE(REPLACE(REPLACE(addr.ADDRESS_LINE1, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [AddressLine1]
           ,REPLACE(REPLACE(REPLACE(addr.ADDRESS_LINE2, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [AddressLine2]
           ,REPLACE(REPLACE(REPLACE(addr.CITY, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') City
           ,REPLACE(REPLACE(REPLACE(st.STATE_NAME, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [State_Province]
           ,REPLACE(REPLACE(REPLACE(addr.ZIPCODE, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [ZipCode]
           ,REPLACE(REPLACE(REPLACE(Ctry.COUNTRY_NAME, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [Country]
           ,REPLACE(REPLACE(REPLACE(reg.REGION_NAme, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [Region]
           ,'@' + REPLACE(REPLACE(REPLACE(uacte.ACCOUNT_NUMBER, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [AccountNumber]
           ,uacte.UtilityAccountServiceLevel
           ,uacte.AccountCreated
           ,uacte.AccountInvoiceNotExpected
           ,uacte.AccountNotManaged
           ,uacte.UtilityAccountInvoiceSource
           ,REPLACE(REPLACE(REPLACE(uacte.Utility, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') Utility
           ,uacte.CommodityType
           ,REPLACE(REPLACE(REPLACE(uacte.Rate, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') Rate
		  
           ,REPLACE(REPLACE(REPLACE(uacte.MeterNumber, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') MeterNumber
           ,uacte.PurchaseMethod
           ,cc.ContractID
           ,BaseContractId
           ,CASE WHEN cc.ContractID = -1 THEN 'DMO'
                 ELSE cc.ContractNumber
            END AS ContractNumber
           ,REPLACE(REPLACE(REPLACE(cc.ContractPricingStatus, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') ContractPricingStatus
           ,cc.ContractType
           ,REPLACE(REPLACE(REPLACE(cc.ContractedVendor, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') ContractedVendor
           ,cc.ContractStartDate
           ,cc.ContractEndDate
           ,cc.NotificationDays
           ,cc.NotificationDate
           ,cc.ContractTriggerRights
           ,cc.FullRequirements
           ,REPLACE(REPLACE(REPLACE(cc.Contract_Pricing_Summary, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') Contract_Pricing_Summary
           ,REPLACE(REPLACE(REPLACE(cc.ContractComments, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') ContractComments
           ,cc.Currency
           ,cc.RenewalType
           ,REPLACE(REPLACE(REPLACE(cc.SupplierAccountNumber, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') SupplierAccountNumber
           ,cc.SupplierAccountServiceLevel
           ,cc.SupplierAccountInvoiceSource
           ,cc.ContractCreatedBy
           ,cc.ContractCreatedDate
           ,cc.ContractRecalcType
           ,uacte.AccountCreatedBy
           ,uacte.Consumption_Level_Desc
           ,uacte.IsDataEntryOnly
           ,uacte.Account_Id [Utility Account ID]
           ,cc.Account_Id [Supplier Account ID]
           ,uacte.Is_Broker_Account [Broker Account]
           ,CAST(uacte.Broker_fee AS VARCHAR) + ' ' + uacte.BrokerFee_Currency_Unit + '/' + uacte.BrokerFee_Uom_Unit AS [Broker Fee]
           ,uia.first_name + SPACE(1) + uia.last_name [Sourcing Analyst]
           ,cacc.Code_Value [Client Sourcing Mapping]
           ,sacc.Code_Value [Site Sourcing Mapping]
           ,aacc.Code_Value [Account Sourcing Mapping]
           ,cc.SupplierAccountStartDate
           ,CASE WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN 'Unspecified'
                 ELSE CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
            END AS SupplierAccountEndDate
           ,cc.SupplierAccountCreatedBy
           ,cc.SupplierAccountCreatedDate
           ,cc.ContractProductType
           ,uacte.Meter_Id AS Meter_Id
		   ,uacte.Rate_ID
		   --,s.CONTRACTING_ENTITY [SiteContractingEntity]
		   ,REPLACE(REPLACE(REPLACE(s.CONTRACTING_ENTITY, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [SiteContractingEntity]
		   ,RegMarketsAnalyst
		   ,AlternateAccountNumber [Utility Alternate Acct#]
		   ,SupplierAltAccount [Supplier Alternate Acct#]
		   ,[Is_Rolling_Meter] = case when is_Rolling_meter = 1 then 'Yes' when is_rolling_meter = 0 then  'No' else null  end 
		   ,Original_contract_end_date
      FROM
            Core.Client_Hier ch
            JOIN dbo.Site s
                  ON ch.Site_Id = s.SITE_ID
            JOIN dbo.Address addr
                  ON addr.Address_Parent_Id = ch.Site_Id
            JOIN dbo.State st
                  ON st.State_Id = addr.State_Id
            JOIN dbo.Country Ctry
                  ON Ctry.Country_Id = st.Country_Id
            JOIN dbo.Region reg
                  ON reg.Region_Id = st.Region_Id
            JOIN dbo.ENTITY_AUDIT ea
                  ON ea.ENTITY_IDENTIFIER = ch.Site_Id
                     AND ea.ENTITY_ID = @Site_Table-- 506                      
                     AND ea.AUDIT_TYPE = 1
            LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgm
                  ON rgm.SITE_ID = ch.Site_Id
            LEFT OUTER JOIN dbo.RM_GROUP rg
                  ON rg.RM_GROUP_ID = rgm.rm_group_id
            LEFT OUTER JOIN #UtilityAccts_CTE uacte
                  ON uacte.SITE_ID = ch.Site_Id
                     AND uacte.ADDRESS_ID = addr.ADDRESS_ID
            LEFT JOIN Code cacc
                  ON cacc.Code_Id = uacte.Client_Analyst_Mapping_Cd
            LEFT JOIN Code sacc
                  ON sacc.Code_Id = uacte.Site_Analyst_Mapping_Cd
            LEFT JOIN Code aacc
                  ON aacc.Code_Id = uacte.Account_Analyst_Mapping_Cd
            LEFT OUTER JOIN #Contracts_CTE cc
                  ON cc.meter_id = uacte.meter_id
            LEFT OUTER JOIN dbo.USER_INFO uia
                  ON uacte.Analyst_Id = uia.USER_INFO_ID
			
      WHERE
            ( REG.REGION_NAME = @region_name
              OR @region_name IS NULL )
            AND Client_Type_Id NOT IN ( @Client_Type_Id_Demo, @Client_Type_Id_GfE, @Client_Type_Id_Shared_Savings, @Client_Type_Id_Software )
            AND ch.CLIENT_ID != @Client_Id
            AND ch.Site_Not_Managed != 1
      GROUP BY
            ch.Client_Name
           ,ch.Client_Id
           ,ch.Client_Not_Managed
           ,ch.Sitegroup_Name
           ,ch.Sitegroup_Id
           ,ch.Division_Not_Managed
           ,ch.Site_name
           ,ch.Site_Id
           ,ea.MODIFIED_DATE
           ,s.SITE_REFERENCE_NUMBER
           ,rg.GROUP_NAME
           ,ch.Site_Not_Managed
           ,ch.Site_Closed
           ,ch.Site_Type_Name
           ,addr.ADDRESS_LINE1
           ,addr.ADDRESS_LINE2
           ,addr.CITY
           ,st.STATE_NAME
           ,addr.ZIPCODE
           ,Ctry.COUNTRY_NAME
           ,reg.REGION_NAME
           ,uacte.ACCOUNT_NUMBER
           ,uacte.UtilityAccountServiceLevel
           ,uacte.AccountCreated
           ,uacte.AccountInvoiceNotExpected
           ,uacte.AccountNotManaged
           ,uacte.UtilityAccountInvoiceSource
           ,uacte.Utility
           ,uacte.CommodityType
           ,uacte.Rate
		   ,uacte.Rate_ID
           ,uacte.MeterNumber
           ,uacte.PurchaseMethod
           ,cc.ContractID
           ,BaseContractId
           ,CASE WHEN cc.ContractID = -1 THEN 'DMO'
                 ELSE cc.ContractNumber
            END
           ,cc.ContractPricingStatus
           ,cc.ContractType
           ,cc.ContractedVendor
           ,cc.ContractStartDate
           ,cc.ContractEndDate
           ,cc.NotificationDays
           ,cc.NotificationDate
           ,cc.ContractTriggerRights
           ,cc.FullRequirements
           ,cc.Contract_Pricing_Summary
           ,cc.ContractComments
           ,cc.Currency
           ,cc.RenewalType
           ,cc.SupplierAccountNumber
           ,cc.SupplierAccountServiceLevel
           ,cc.SupplierAccountInvoiceSource
           ,cc.ContractCreatedBy
           ,cc.ContractCreatedDate
           ,addr.ADDRESS_ID
           ,uacte.AccountCreatedBy
           ,cc.ContractRecalcType
           ,uacte.Consumption_Level_Desc
           ,uacte.IsDataEntryOnly
           ,uacte.Account_Id
           ,cc.Account_Id
           ,uacte.Is_Broker_Account
           ,uacte.Broker_fee
           ,uacte.BrokerFee_Currency_Unit
           ,uacte.BrokerFee_Uom_Unit
           ,uia.first_name
           ,uia.last_name
           ,cacc.Code_Value
           ,sacc.Code_Value
           ,aacc.Code_Value
           ,cc.SupplierAccountStartDate
           ,CASE WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN 'Unspecified'
                 ELSE CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
            END
           ,cc.SupplierAccountCreatedBy
           ,cc.SupplierAccountCreatedDate
           ,cc.ContractProductType
           ,uacte.Meter_Id
		   ,uacte.Rate_ID
		   ,s.CONTRACTING_ENTITY 
		   ,RegMarketsAnalyst
		   ,AlternateAccountNumber 
		   ,SupplierAltAccount
		   ,Is_Rolling_Meter
		   ,Original_contract_end_date
                     
END;
;
GO




GRANT EXECUTE ON  [dbo].[Report_DE_JoinedFile_All] TO [CBMS_SSRS_Reports]
GO
