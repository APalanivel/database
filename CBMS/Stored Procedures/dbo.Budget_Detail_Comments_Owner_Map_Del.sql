SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Detail_Comments_Owner_Map_Del]  
     
DESCRIPTION: 
	It Deletes Budget Detail comments ownermap for Selected Budget_Detail_Comments_Owner_Map_Id
      
INPUT PARAMETERS:          
NAME									DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------------------             
@Budget_Detail_Comments_Owner_Map_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	BEGIN TRAN
		EXEC Budget_Detail_Comments_Owner_Map_Del 80
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH
          
MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Detail_Comments_Owner_Map_Del
    (
      @Budget_Detail_Comments_Owner_Map_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.Budget_Detail_Comments_Owner_Map		
	WHERE
		Budget_Detail_Comments_Owner_Map_Id = @Budget_Detail_Comments_Owner_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Detail_Comments_Owner_Map_Del] TO [CBMSApplication]
GO
