SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 dbo.Report_DE_Currency_Unit_Conversion 
  
DESCRIPTION:  
	Created to get the conversion for all the base unit's selected, from SSRS report.
  
INPUT PARAMETERS:  
 Name				DataType		Default			Description  
------------------------------------------------------------  
@CurrencyGroupId	INT
@BaseUnitId			VARCHAR(2000)
@ConvertedUnitId	INT
@Year				INT                    
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
AKR      Ashok Kumar Raju  
  
MODIFICATIONS  
  
 Initials	Date			Modification  
------------------------------------------------------------  
 AKR     2012-07-25        Created the Sproc
 
******/  
  
CREATE PROCEDURE dbo.Report_DE_Currency_Unit_Conversion
      ( 
       @CurrencyGroupId INT
      ,@BaseUnitId VARCHAR(2000)
      ,@ConvertedUnitId INT
      ,@Year INT )
AS 
BEGIN  
      DECLARE
            @beginDate DATETIME
           ,@endDate DATETIME  
      DECLARE @Base_Unit_Id_List TABLE ( Base_Unit_Id INT )
  
      SET @beginDate = '1/1/' + cast(@year AS VARCHAR(4))  
      SET @endDate = dateadd(year, 1, @begindate)  
  
      INSERT      INTO @Base_Unit_Id_List
                  ( 
                   Base_Unit_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@BaseUnitId, ',') us
      
  
      SELECT
            cu.CURRENCY_UNIT_NAME 
           ,cuc.conversion_factor
           ,cuc.conversion_date
      FROM
            dbo.currency_unit_conversion cuc
            INNER JOIN @Base_Unit_Id_List buil
                  ON cuc.BASE_UNIT_ID = buil.Base_Unit_Id
            INNER JOIN dbo.CURRENCY_UNIT cu
            ON buil.Base_Unit_Id = cu.CURRENCY_UNIT_ID
      WHERE
            cuc.currency_group_id = @CurrencyGroupId
            AND cuc.converted_unit_id = @ConvertedUnitId
            AND cuc.conversion_date >= @beginDate
            AND cuc.conversion_date < @endDate
      ORDER BY
            cu.CURRENCY_UNIT_NAME
           ,conversion_date ASC  
END  

;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Currency_Unit_Conversion] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Currency_Unit_Conversion] TO [CBMSApplication]
GO
