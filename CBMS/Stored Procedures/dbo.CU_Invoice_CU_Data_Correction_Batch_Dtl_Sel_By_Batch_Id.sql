SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******         
                     
 NAME: 
		[dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Sel_By_Batch_Id]                  
                        
 DESCRIPTION:                         
        To get batch details based on batch_Id.                        
                        
 INPUT PARAMETERS:        
                       
 Name										DataType           Default       Description        
--------------------------------------------------------------------------------------- 
 @CU_Invoice_CU_Data_Correction_Batch_Id	INT
                        
 OUTPUT PARAMETERS:             
                        
 Name										DataType           Default       Description        
--------------------------------------------------------------------------------------- 
                        
 USAGE EXAMPLES:        
--------------------------------------------------------------------------------------- 

            
   EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Sel_By_Batch_Id 
      @CU_Invoice_CU_Data_Correction_Batch_Id = 10
 
  EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl_Sel_By_Batch_Id 
      @CU_Invoice_CU_Data_Correction_Batch_Id = 1
      ,@Status_Value='Completed'
 
                     
 AUTHOR INITIALS:        
       
 Initials               Name        
--------------------------------------------------------------------------------------- 
 NR                     Narayana Reddy
                         
 MODIFICATIONS:      
       
 Initials               Date             Modification      
--------------------------------------------------------------------------------------- 
 NR                     2017-03-09      Created for Contract Placeholder.                     
                       
******/ 
CREATE PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Sel_By_Batch_Id]
      ( 
       @CU_Invoice_CU_Data_Correction_Batch_Id INT
      ,@Status_Value VARCHAR(25) = 'Pending'--'Completed'
       )
AS 
BEGIN 
                       
      SET NOCOUNT ON;  
      
      SELECT
            cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Dtl_Id
           ,cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Id
           ,cicdcbd.From_Account_Id
           ,cicdcbd.To_Account_Id
           ,NULLIF(cicdcbd.Cu_Invoice_Id, -1) AS Cu_Invoice_Id
           ,action_Cd.Code_Value AS Action_Type
           ,cicdcb.Contract_Id
           ,cicdcb.Is_Notification_Required
           ,cd.Code_Value
           ,ch.Client_Name AS Client
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,cha.Display_Account_Number AS DMO_Supplier_Account
           ,com.Commodity_Name
           ,SUBSTRING(DATENAME(M, ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month)), 1, 3) + '-' + RIGHT(CAST(DATEPART(YYYY, ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month)) AS VARCHAR), 2) AS Month_Posted
           ,cha.Commodity_Id
           ,cha.Client_Hier_Id
           ,ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month) AS Service_Month
           ,ch.Client_Currency_Group_Id
           ,ch.Client_Id
           ,ch.Site_Id
           ,sr.Security_Role_Id
      FROM
            dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl cicdcbd
            INNER JOIN dbo.CU_Invoice_CU_Data_Correction_Batch cicdcb
                  ON cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Id = cicdcb.CU_Invoice_CU_Data_Correction_Batch_Id
            INNER JOIN dbo.Code action_Cd
                  ON action_Cd.Code_Id = cicdcbd.Action_Cd
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = cicdcbd.Status_Cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cicdcbd.Cu_Invoice_Id = cism.CU_INVOICE_ID
            INNER JOIN core.Client_Hier_Account cha
                  ON cicdcbd.From_Account_Id = cha.Account_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                  ON cha.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.Security_Role sr
                  ON ch.Client_Id = sr.Client_Id
                     AND sr.Is_Corporate = 1
      WHERE
            cs.Codeset_Name = 'Request Status'
            AND cd.Code_Value = @Status_Value
            AND cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
      GROUP BY
            cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Dtl_Id
           ,cicdcbd.CU_Invoice_CU_Data_Correction_Batch_Id
           ,cicdcbd.From_Account_Id
           ,cicdcbd.To_Account_Id
           ,cicdcbd.Cu_Invoice_Id
           ,action_Cd.Code_Value
           ,cicdcb.Contract_Id
           ,cicdcb.Is_Notification_Required
           ,cd.Code_Value
           ,ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,cha.Display_Account_Number
           ,com.Commodity_Name
           ,SUBSTRING(DATENAME(M, ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month)), 1, 3) + ' ' + RIGHT(CAST(DATEPART(YYYY, ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month)) AS VARCHAR), 2)
           ,cha.Commodity_Id
           ,cha.Client_Hier_Id
           ,ISNULL(cism.SERVICE_MONTH, cicdcbd.Service_Month)
           ,ch.Client_Currency_Group_Id
           ,ch.Client_Id
           ,ch.Site_Id
           ,sr.Security_Role_Id
     
      

END



;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl_Sel_By_Batch_Id] TO [CBMSApplication]
GO
