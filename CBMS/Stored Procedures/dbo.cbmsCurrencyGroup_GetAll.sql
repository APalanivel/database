SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCurrencyGroup_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsCurrencyGroup_GetAll]
	( @MyAccountId int )
AS
BEGIN

	   select currency_group_id
		,currency_group_name
	     from currency_group
	 order by currency_group_name asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyGroup_GetAll] TO [CBMSApplication]
GO
