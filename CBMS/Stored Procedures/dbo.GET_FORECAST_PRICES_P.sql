SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_PRICES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@fromDate      	datetime  	          	
	@toDate        	datetime  	          	
	@forecastType  	int       	          	
	@forecastTypeName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- this procedure is for Recommandation Report

CREATE      PROCEDURE dbo.GET_FORECAST_PRICES_P

@userId varchar(10),
@sessionId varchar(20),
@fromDate  Datetime,
@toDate  Datetime,
@forecastType int, 
@forecastTypeName varchar(200)

as
set nocount on
DECLARE @entityId int

select @entityId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@forecastType AND 
ENTITY_NAME=@forecastTypeName

	select 
	aggressive_price aggressive, conservative_price conservative,
	 moderate_price moderate, 
	month_identifier identifier
	from RM_FORECAST a,RM_FORECAST_DETAILS b
	where a.RM_FORECAST_ID=b.RM_FORECAST_ID and  a.RM_FORECAST_ID=
	(
		select RM_FORECAST_ID from RM_FORECAST where 
		forecast_as_of_date=(
					SELECT MAX(FORECAST_FROM_DATE)
					FROM RM_FORECAST WHERE forecast_type_id=@entityId

			)
	)
	and @fromDate<=month_identifier and  @toDate>=month_identifier
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_PRICES_P] TO [CBMSApplication]
GO
