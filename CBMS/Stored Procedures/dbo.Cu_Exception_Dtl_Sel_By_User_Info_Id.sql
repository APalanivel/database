SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Cu_Exception_Dtl_Sel_By_User_Info_Id

DESCRIPTION:
			User is move to history  This excpetions alo mandetory.

INPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	@User_Info_Id					INT
   

OUTPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
---------------------------------------------------------------------------------------------

EXEC dbo.Cu_Exception_Dtl_Sel_By_User_Info_Id
    @User_Info_Id =49
   

AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	SP			Srinivas Patchava
	
MODIFICATIONS
	Initials	Date			Modification
---------------------------------------------------------------------------------------------
	NR       	2018-10-04		D20-227 -  Created to show the user associated exceptions.
	SP          2019-07-09      SE2017-729 should show the IC exception name under Exception Types User is Associate

******/

CREATE PROCEDURE [dbo].[Cu_Exception_Dtl_Sel_By_User_Info_Id]
    (
        @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cet.EXCEPTION_TYPE
        FROM
            dbo.CU_EXCEPTION_TYPE cet
        WHERE
            cet.Is_Active = 1
            AND cet.Managed_By_User_Info_Id = @User_Info_Id
        UNION
        SELECT
            c.Code_Value AS EXCEPTION_TYPE
        FROM
            dbo.Invoice_Collection_Exception_Type_User_Info_Map icetuim
            INNER JOIN dbo.Code c
                ON c.Code_Id = icetuim.Invoice_Collection_Exception_Type_Cd
        WHERE
            icetuim.User_Info_Id = @User_Info_Id;
    END;





GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Dtl_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
