SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Usage_Del]  
     
DESCRIPTION: 
	It Deletes Budget Usage Details for Selected Budget Usage Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
@Budget_Usage_Id				INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Budget_Usage_Del 239089
	ROLLBACK TRAN
	
	SELECT * FROM Budget_Usage WHERE Budget_Usage_Id = 239089

AUTHOR INITIALS:          
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Usage_Del
    (
      @Budget_Usage_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.BUDGET_USAGE
	WHERE
		BUDGET_USAGE_ID = @Budget_Usage_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Usage_Del] TO [CBMSApplication]
GO
