SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.[Meter_Dtl_Sel_By_Account_Site_Invoice_Begin_End_Dt  
  
DESCRIPTION:  
 It is used to get Utility/Supplier account meter informaiton by given account and site.  
  
INPUT PARAMETERS:  
 Name				DataType		 Default				Description  
--------------------------------------------------------------------------------------  
 @account_id     INT   
 @site_id        INT         NULL  
  
OUTPUT PARAMETERS:  
 Name				DataType		 Default				Description  
--------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
Supplier account

select * from cu_invoice_Service_month where account_id =1120197
  
 EXEC dbo.[Meter_Dtl_Sel_By_Account_Site_Invoice_Begin_End_Dt 
     @Account_Id = 1120197
    , @Site_Id = 390657
    , @Commodity_Id = NULL
    , @Cu_Invoice_Id = 74895813
     
   

   
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy
   
MODIFICATIONS  
  Initials		Date				Modification  
------------------------------------------------------------  
	NR			2019-10-26			Add Contract - Created
******/
CREATE PROCEDURE [dbo].[Meter_Dtl_Sel_By_Account_Site_Invoice_Begin_End_Dt]
    (
        @Account_Id INT
        , @Site_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Cu_Invoice_Id INT
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Supplier_Account_Type_Id INT
            , @Utility_Account_Type_Id INT;

        SELECT
            @Supplier_Account_Type_Id = MAX(CASE WHEN at.ENTITY_NAME = 'Supplier' THEN at.ENTITY_ID
                                            END)
            , @Utility_Account_Type_Id = MAX(CASE WHEN at.ENTITY_NAME = 'Utility' THEN at.ENTITY_ID
                                             END)
        FROM
            dbo.ENTITY at
        WHERE
            at.ENTITY_NAME IN ( 'Utility', 'Supplier' )
            AND at.ENTITY_DESCRIPTION = 'Account Type';

        SELECT
            cha.Meter_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Id
              END AS Rate_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Name
              END AS Rate_Name
            , cha.Commodity_Id AS Commodity_Type_Id
            , com.Commodity_Name AS Commodity_Type
            , cha.Account_Vendor_Id AS Utility_Id
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Id
                  ELSE ucha.Account_Vendor_Id
              END AS Supplier_Id
            , cha.Account_Vendor_Name AS Utility_Name
            , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                  ELSE ucha.Account_Vendor_Name
              END AS Supplier_Name
            , m.PURCHASE_METHOD_TYPE_ID
            , pm.ENTITY_NAME AS Purchase_Method_Type
            , cha.Account_Id AS Utility_Account_Id
            , cha.Account_Number AS Utility_Account_Number
            , cha.Meter_Address_ID AS Address_Id
            , cha.Meter_Address_Line_1 AS Address_Line1
            , cha.Meter_Address_Line_2 AS Address_Line2
            , ch.City
            , ch.Client_Hier_Id
            , ch.State_Id
            , ch.State_Name
            , cha.Meter_Number
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , ch.Site_Id
            , ch.Site_name
            , ch.Site_Not_Managed AS Not_Managed
            , cha.Account_Type AS Entity_Name
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , (CASE WHEN cha.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                   WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
               END) AS Account_Type_Id
            , cont.ED_CONTRACT_NUMBER
            , cha.Rate_Id AS Supplier_RateId
            , cha.Rate_Name AS Supplier_RateName
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS SiteView_Name
            , CASE WHEN cha.Account_Type = 'SUPPLIER' THEN ucha.Display_Account_Number
                  ELSE cha.Display_Account_Number
              END AS Supp_Utility_Account_Number
            , cha.Account_Invoice_Source_Cd invoice_source_type_id
            , invScr.ENTITY_NAME invoice_source_type
            , ch.Client_Id
            , m.ACCOUNT_ID meter_account_id
        FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN(Core.Client_Hier_Account scha
                      JOIN Core.Client_Hier_Account ucha
                          ON ucha.Account_Id = scha.Account_Id)
                ON scha.Meter_Id = cha.Meter_Id
                   AND  cha.Account_Type = 'Supplier'
                   AND  ucha.Account_Type = 'Utility'
            LEFT JOIN dbo.METER m
                ON cha.Meter_Id = m.METER_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            LEFT JOIN dbo.ENTITY pm
                ON pm.ENTITY_ID = m.PURCHASE_METHOD_TYPE_ID
            LEFT JOIN dbo.[CONTRACT] cont
                ON cont.CONTRACT_ID = cha.Supplier_Contract_ID
            LEFT JOIN dbo.ENTITY invScr
                ON cha.Account_Invoice_Source_Cd = invScr.ENTITY_ID
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = cha.Account_Id
        WHERE
            ch.Site_Not_Managed = 0
            AND cha.Account_Id = @Account_Id
            AND (   @Site_Id IS NULL
                    OR  ch.Site_Id = @Site_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND (   cism.SERVICE_MONTH IS NULL
                    OR  ((   cha.Account_Type = 'Utility'
                             OR (   cha.Account_Type = 'Supplier'
                                    AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                          AND     cha.Supplier_Account_End_Dt
                                            OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                            AND     cha.Supplier_Account_End_Dt
                                            OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                              AND     cism.End_Dt
                                            OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                            AND     cism.End_Dt)))))
        GROUP BY
            cha.Meter_Id
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Id
              END
            , CASE WHEN cha.Supplier_Contract_ID IS NULL THEN cha.Rate_Name
              END
            , cha.Commodity_Id
            , com.Commodity_Name
            , cha.Account_Vendor_Id
            , ucha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , ucha.Account_Vendor_Name
            , m.PURCHASE_METHOD_TYPE_ID
            , pm.ENTITY_NAME
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Address_ID
            , cha.Meter_Address_Line_1
            , cha.Meter_Address_Line_2
            , ch.City
            , ch.Client_Hier_Id
            , ch.State_Id
            , ch.State_Name
            , cha.Meter_Number
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , ch.Site_Id
            , ch.Site_name
            , ch.Site_Not_Managed
            , cha.Account_Type
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cont.ED_CONTRACT_NUMBER
            , cha.Rate_Id
            , cha.Rate_Name
            , ucha.Display_Account_Number
            , cha.Display_Account_Number
            , cha.Account_Invoice_Source_Cd
            , invScr.ENTITY_NAME
            , ch.Client_Id
            , m.ACCOUNT_ID;
    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Meter_Dtl_Sel_By_Account_Site_Invoice_Begin_End_Dt] TO [CBMSApplication]
GO
