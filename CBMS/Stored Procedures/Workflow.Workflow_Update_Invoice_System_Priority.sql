SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/******                        
 NAME: [Workflow].[Workflow_Update_Invoice_System_Priority]           
                        
 DESCRIPTION:                        
			This procedure will be used to remove system priority for the invoice
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 
                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
  exec [Workflow].[Workflow_Update_Invoice_System_Priority] 
                   
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
AP						Arunkumar Palanivel        
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
AP						Dec 18,2019		Created procedure based on the requirement mentioned below

Scenario:

Client is marked dynamic priority on Nov 15

Invoice 123 is marked as priority on Nov 15 due to the dynamic priority being checked in

Invoice 123 is routed to client team on Nov 16 (still marked as a priority)

Client dynamic priority is removed on Nov 20

Invoice 123 remains marked as a priority because it was open on Nov 15 when priority was set and is still open

Invoice 123 is routed back to the data team on Feb 20

Because the invoice was three months old when it was routed back to the data team in February and the dynamic priority was no longer checked in for the client, we do not want to see the invoice still marked as a priority.  If the client is no longer marked as a priority and the invoices are still open in a queue two weeks after the checkbox has been unchecked, the priority items for that client should drop the flag and no longer be marked as a priority.  										
										
										       
                  
******/


CREATE PROCEDURE [Workflow].[Workflow_Update_Invoice_System_Priority]
AS
      BEGIN

            /*      
   1) collect all system priority invoice into temp table.      
   2) delete invoices marked as client priority.      
   3) delete invoices which are less then 20 days.      
   4) update invoices to non priority.      
  */

  SET NOCOUNT ON ;
            DECLARE @Days INT,
			@user_id INT ,
			@Userprioritymanual INT

			SET @user_id =
			(SELECT TOP (1) QUEUE_ID FROM dbo.USER_INFO 
WHERE USERNAME ='conversion')

  SELECT @Userprioritymanual = Code_Id      
    FROM dbo.Code      
    WHERE Code_Value = 'Userprioritymanual';    

            SELECT
                  @Days =
                  (     SELECT
                              cast(App_Config_Value AS INT)
                        FROM  dbo.App_Config
                        WHERE App_Config_Cd = 'RemoveSystemPriorityForDays' );

            SELECT
                  cia.CU_INVOICE_ID
                , ci.CLIENT_ID
            INTO
                  #tempClientInvoice
            FROM  Workflow.Cu_Invoice_Attribute cia
                  JOIN
                  dbo.CU_INVOICE ci
                        ON cia.CU_INVOICE_ID = ci.CU_INVOICE_ID
                  JOIN
                  dbo.Code c
                        ON cia.Source_Cd = c.Code_Id
                  JOIN
                  dbo.Client_Attribute ca
                        ON ca.Client_Id = ci.CLIENT_ID
            WHERE cia.Is_Priority = 1
                  AND   c.Code_Value = 'Systempriorityclient'
                  AND   ci.CLIENT_ID IS NOT NULL
                  AND   ca.Updated_Ts < getdate() - @Days
                  AND   ca.Mark_Invoice_Exception_As_Priority = 0;



            UPDATE
                  Workflow.Cu_Invoice_Attribute
            SET
                  Is_Priority = 0
                , Updated_User_Id = isnull( @user_id,16)
                , Last_Change_Ts = getdate()
				,Source_Cd = @Userprioritymanual
            WHERE CU_INVOICE_ID IN
                        (     SELECT
                                    CU_INVOICE_ID
                              FROM  #tempClientInvoice );


DROP TABLE #tempClientInvoice 
      END;
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Update_Invoice_System_Priority] TO [CBMSApplication]
GO
