SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.EC_Contract_Attribute_Sel_By_EC_Contract_Attribute_Id         
                
Description:                
		This sproc to get the attribute details for a Given id.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@EC_Contract_Attribute_Id	INT  
      
 Output Parameters:                      
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.EC_Contract_Attribute_Sel_By_EC_Contract_Attribute_Id   7  
     
   Exec dbo.EC_Contract_Attribute_Sel_By_EC_Contract_Attribute_Id  9         
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2015-04-22		Created For AS400.           
               
******/   
  
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Sel_By_EC_Contract_Attribute_Id]
      ( 
       @EC_Contract_Attribute_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
              
      SELECT
            eca.EC_Contract_Attribute_Id
           ,eca.State_Id
           ,eca.Commodity_Id
           ,eca.EC_Contract_Attribute_Name
           ,eca.Attribute_Type_Cd
           ,s.COUNTRY_ID
           ,s.STATE_ID
           ,s.STATE_NAME
      FROM
            dbo.EC_Contract_Attribute eca
            INNER JOIN dbo.STATE s
                  ON s.STATE_ID = eca.State_Id
      WHERE
            eca.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id  
                  
END  
            

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Sel_By_EC_Contract_Attribute_Id] TO [CBMSApplication]
GO
