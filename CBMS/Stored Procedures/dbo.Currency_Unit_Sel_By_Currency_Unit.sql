SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Currency_Unit_Sel_By_Currency_Unit

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
   @CurrencyId int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     
USAGE EXAMPLES:
------------------------------------------------------------
-- Exec dbo.Currency_Unit_Sel_By_Currency_Unit  3


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------            	 	
	KC          31-Aug-2009   Created new SProc to get currency Id and currency unit name			
								        	
******/


CREATE PROCEDURE dbo.Currency_Unit_Sel_By_Currency_Unit  
 ( 
	@Currency_Unit_id INT
 )
AS  
BEGIN  
  
	SET NOCOUNT ON

	SELECT
		Currency_unit_id  
		, Currency_unit_name  
	FROM 
		dbo.currency_unit  
	WHERE 
		Currency_Unit_id = @Currency_Unit_id 
	ORDER BY
		sort_order  
		, currency_unit_name   
  
END  
GO
GRANT EXECUTE ON  [dbo].[Currency_Unit_Sel_By_Currency_Unit] TO [CBMSApplication]
GO
