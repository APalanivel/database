
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	[dbo].[Cost_Usage_Min_value_Sel_For_Account_By_Commodity]

DESCRIPTION:

		Used to select minimum value for all the buckets in between the given date, this procedure used by the variance page
		As application don't have Client_id information on this page getting the currency group id in a variable and using it in main query.
		
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Account_Id				INT
	@Site_Client_Hier_Id	INT
	@Commodity_id			INT
	@Begin_Dt				DATE
	@End_Dt					DATE
	@Currency_Unit_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Min_value_Sel_For_Account_By_Commodity 38009, 12592, 291, '2/1/2007','12/1/2007',3
	EXEC dbo.Cost_Usage_Min_value_Sel_For_Account_By_Commodity 38009, 12592, 290, '2/1/2007','12/1/2007',3

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	SSR			Sharad Srivastava
	AP			Athmaram Pabbathi
	RR			Raghu Reddy
	KVK			Vinay k
	SP			Sandeep Pigilam
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG			02/05/2010  Created
	SSR			03/04/2010	Removed Input parameter @Client_ID and joined Account & SUPPLIER_ACCOUNT_METER_MAP to get currency group id
	HG			03/29/2010  ISNULL function used to determine the conversion factor removed and using bucket type to determine the conversion factors.
	SSR			04/14/2010  Replaced LEFT JOIN of Cost_usage_Account_dtl with INNER JOIN of Bucket_master
	HG			04/29/2010  Unit Cost added as one of the row with other bucket values as per the variance page requirement
    DMR			09/10/2010  Modified for Quoted_Identifier
	AP			08/09/2011  Removed @UOM_Type_ID parameter and modified the conv logic to use default_uom_type_id from bucket_master. 
								Replaced script for @currency_group_id with client_heir & client_hier_account tables; included "Total Usage" bucket for volume
								and added additional filters for Is_Required_For_Variance_Test = 1 and Is_Active = 1 on bucket_master
    AP			08/30/2011  Removed CTE and UNION statment used for Unit_Cost calculation, since it is handled in application
    AP			09/09/2011  Added UNION statement back to the script
	AP			09/12/2011  Replaced CTE_Cu_Account_Dtl with @Cost_Usage_Account_Dtl table variable
	AP			03/27/2012	Added @Site_Client_Hier_Id as a parameter and modified SELECT for currency_group_id and removed CHA table
	RR			2012-06-04	Corrected usage examples
	HG			2012-07-09	MAINT-1355, modified to fix the Unit cost calculation to consider only Total Usage/ Volume determinant.
	AKR         2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name							
	KVK			10/01/2013  Added recompile statement in the procedure definition
	SP			2014-05-09  Data Operations Enhancement,Added	EXISTS clause to add Service_Month in final select list 
	SP			2016-13-10	Variance Test Enhancements,Used CURRENCY_UNIT_COUNTRY_MAP for default currency_id and Country_Commodity_Uom for default uom_id. 	
	
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Min_value_Sel_For_Account_By_Commodity]
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT )
      WITH RECOMPILE
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Currency_Group_Id INT
           ,@Currency_Name VARCHAR(200)
           ,@Usage_Bucket_Stream_Cd INT
           ,@Country_Id INT
           ,@Country_Default_Uom_Type_Id INT;

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) )

      DECLARE @tbl_1 TABLE
            ( 
             Account_Id INT
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Type VARCHAR(25)
            ,Bucket_value DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) ) 
      DECLARE @tbl_2 TABLE
            ( 
             Account_Id INT
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Type VARCHAR(25)
            ,Bucket_value DECIMAL(28, 10)
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) )              


      SELECT
            @Usage_Bucket_Stream_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'BucketStream'
            AND cd.Code_Value = 'Usage';

      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Country_Id = ch.Country_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id
                        
      SELECT
            @Currency_Unit_Id = MAX(cucm.CURRENCY_UNIT_ID)
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
      WHERE
            cucm.Country_Id = @Country_Id
	  
      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id	  
                     
      SELECT
            @Currency_Name = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id   

                                 
      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        cua.Service_Month
                       ,@Account_Id ACCOUNT_ID
                       ,bkt_cd.Code_Value Bucket_Type
                       ,bm.Bucket_Name
                       ,( cua.Bucket_Value * ( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                                    WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                               END ) ) Bucket_Value
                       ,( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cua.Bucket_Value * cc.Conversion_Factor
                          END ) AS Total_Cost
                       ,( CASE WHEN bkt_cd.Code_Value = 'Determinant' THEN cua.Bucket_Value * uc.Conversion_Factor
                          END ) AS Volume
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Unit_Id
                             ELSE ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                         ELSE bm.Default_Uom_Type_Id
                                    END )
                        END UOM_Id
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Name
                             ELSE en.ENTITY_NAME
                        END UOM_Name
                  FROM
                        dbo.Cost_Usage_Account_Dtl cua
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_ID = cua.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                                   ELSE bm.Default_Uom_Type_Id
                                                              END )
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                       ELSE bm.Default_Uom_Type_Id
                                                  END )
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON cc.currency_group_id = @Currency_Group_Id
                                 AND cc.base_unit_id = cua.CURRENCY_UNIT_ID
                                 AND cc.converted_unit_id = @Currency_unit_id
                                 AND cc.conversion_date = cua.Service_month
                  WHERE
                        cua.ACCOUNT_ID = @Account_Id
                        AND cua.Client_Hier_Id = @Site_Client_Hier_Id
                        AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                        AND bm.Commodity_Id = @Commodity_id
                        AND BM.Is_Required_For_Variance_Test = 1
                        AND BM.Is_Active = 1

      INSERT      INTO @tbl_1
                  ( 
                   Account_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Bucket_value
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        CU.Account_Id
                       ,CU.Bucket_Name
                       ,CU.Bucket_Type
                       ,MIN(CU.Bucket_value) Bucket_value
                       ,cu.UOM_Id
                       ,cu.UOM_Name
                  FROM
                        @Cost_Usage_Account_Dtl CU
                  GROUP BY
                        CU.Account_Id
                       ,CU.Bucket_Name
                       ,CU.Bucket_Type
                       ,cu.UOM_Id
                       ,cu.UOM_Name
           
           
           
      INSERT      INTO @tbl_2
                  ( 
                   Account_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Bucket_value
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        UnitCost.Account_Id
                       ,UnitCost.Bucket_Name
                       ,UnitCost.Bucket_Type
                       ,MIN(UnitCost.Unit_Cost)
                       ,@Currency_Unit_Id UOM_Id
                       ,@Currency_Name UOM_Name
                  FROM
                        ( SELECT
                              CU.Account_id
                             ,CU.Service_Month
                             ,'Unit Cost' Bucket_Name
                             ,'Charge' Bucket_Type
                             ,SUM(CU.Total_Cost) / NULLIF(SUM(CU.Volume), 0) Unit_Cost
                          FROM
                              @Cost_Usage_Account_Dtl CU
                          WHERE
                              cu.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
                          GROUP BY
                              CU.Account_id
                             ,CU.Service_Month ) UnitCost
                  GROUP BY
                        UnitCost.Account_Id
                       ,UnitCost.Bucket_Name
                       ,UnitCost.Bucket_Type;
      WITH  CTE_Unitcost
              AS ( SELECT
                        CU.Account_id
                       ,CU.Service_Month
                       ,'Unit Cost' Bucket_Name
                       ,'Charge' Bucket_Type
                       ,SUM(CU.Total_Cost) / NULLIF(SUM(CU.Volume), 0) Unit_Cost
                       ,@Currency_Unit_Id UOM_Id
                       ,@Currency_Name UOM_Name
                   FROM
                        @Cost_Usage_Account_Dtl CU
                   WHERE
                        cu.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
                   GROUP BY
                        CU.Account_id
                       ,CU.Service_Month)
            SELECT
                  maintbl.Account_Id
                 ,maintbl.Bucket_Name
                 ,maintbl.Bucket_Type
                 ,maintbl.Bucket_value
                 ,maintbl.UOM_Id
                 ,maintbl.UOM_Name
                 ,MIN(maintbl.Service_Month) AS Service_Month
            FROM
                  @tbl_1 tbl_1
                  INNER JOIN @Cost_Usage_Account_Dtl maintbl
                        ON maintbl.Account_Id = tbl_1.Account_Id
                           AND maintbl.Bucket_Name = tbl_1.Bucket_Name
                           AND maintbl.Bucket_Type = tbl_1.Bucket_Type
                           AND maintbl.UOM_Id = tbl_1.UOM_Id
                           AND maintbl.UOM_Name = tbl_1.UOM_Name
                           AND maintbl.Bucket_value = tbl_1.Bucket_value
            GROUP BY
                  maintbl.Account_Id
                 ,maintbl.Bucket_Name
                 ,maintbl.Bucket_Type
                 ,maintbl.Bucket_value
                 ,maintbl.UOM_Id
                 ,maintbl.UOM_Name
            UNION ALL
            SELECT
                  maintbl.Account_Id
                 ,maintbl.Bucket_Name
                 ,maintbl.Bucket_Type
                 ,maintbl.Unit_Cost
                 ,maintbl.UOM_Id
                 ,maintbl.UOM_Name
                 ,MIN(maintbl.Service_Month) AS Service_Month
            FROM
                  @tbl_2 tbl_2
                  INNER JOIN CTE_Unitcost maintbl
                        ON maintbl.Account_Id = tbl_2.Account_Id
                           AND maintbl.Bucket_Name = tbl_2.Bucket_Name
                           AND maintbl.Bucket_Type = tbl_2.Bucket_Type
                           AND maintbl.UOM_Id = tbl_2.UOM_Id
                           AND maintbl.UOM_Name = tbl_2.UOM_Name
                           AND maintbl.Unit_Cost = tbl_2.Bucket_value
            GROUP BY
                  maintbl.Account_Id
                 ,maintbl.Bucket_Name
                 ,maintbl.Bucket_Type
                 ,maintbl.Unit_Cost
                 ,maintbl.UOM_Id
                 ,maintbl.UOM_Name
            ORDER BY
                  Bucket_Type
                 ,Bucket_Name	


END;
;


;
GO






GRANT EXECUTE ON  [dbo].[Cost_Usage_Min_value_Sel_For_Account_By_Commodity] TO [CBMSApplication]
GO
