SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******              
                                 
 NAME:  [dbo].[User_Role_Cnt_Sel_For_Group_By_Client]                              
                                  
 DESCRIPTION:              
    TO get the group information for group info  table .                                  
                                  
 INPUT PARAMETERS:              
                                 
 Name                                   DataType        Default       Description              
---------------------------------------------------------------------------------------------------------------            
 @Group_Info_Id                         INT                
 @Client_Id                             INT      
                                 
 OUTPUT PARAMETERS:              
                                       
 Name                                   DataType        Default       Description              
---------------------------------------------------------------------------------------------------------------            
                                  
 USAGE EXAMPLES:                                      
---------------------------------------------------------------------------------------------------------------           
             
 Exec  dbo.User_Role_Cnt_Sel_For_Group_By_Client 219,235              
 Exec  dbo.User_Role_Cnt_Sel_For_Group_By_Client 349,235                 
                                 
 AUTHOR INITIALS:            
               
 Initials               Name              
---------------------------------------------------------------------------------------------------------------            
 NR                     Narayana Reddy                                    
                                   
 MODIFICATIONS:            
                                   
 Initials               Date            Modification            
---------------------------------------------------------------------------------------------------------------
 NR                     2014-01-01      Created for RA Admin user management
******/
 CREATE PROCEDURE dbo.User_Role_Cnt_Sel_For_Group_By_Client
      ( 
       @GROUP_INFO_ID INT
      ,@Client_Id INT )
 AS 
 BEGIN

      SET NOCOUNT ON

      DECLARE @User_Count INT

      SELECT
            @User_Count = COUNT(uc.User_Info_Id)
      FROM
            ( SELECT
                  map.USER_INFO_ID
              FROM
                  dbo.User_Info_Group_Info_Map map
                  INNER JOIN dbo.USER_INFO ui
                        ON map.User_Info_Id = ui.USER_INFO_ID
              WHERE
                  ui.CLIENT_ID = @Client_Id
                  AND map.GROUP_INFO_ID = @GROUP_INFO_ID
                  AND ui.ACCESS_LEVEL = 1
                  AND ui.IS_HISTORY = 0
              UNION
              SELECT
                  urm.USER_INFO_ID
              FROM
                  dbo.Client_App_Access_Role_Group_Info_Map caarg
                  INNER JOIN dbo.Client_App_Access_Role caar
                        ON caarg.Client_App_Access_Role_Id = caar.Client_App_Access_Role_Id
                  INNER JOIN dbo.User_Info_Client_App_Access_Role_Map urm
                        ON caar.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                        INNER JOIN dbo.USER_INFO ui
							ON urm.User_Info_Id = ui.USER_INFO_ID
              WHERE
                  caarg.GROUP_INFO_ID = @GROUP_INFO_ID
                  AND caar.Client_Id = @Client_Id 
                  AND ui.ACCESS_LEVEL = 1
                  AND ui.IS_HISTORY = 0
                  ) uc

      SELECT
            gi.group_info_id
           ,gi.group_name
           ,gi.group_description
           ,@User_Count AS User_Count
           ,Roles.Role_Cnt AS Role_Count
      FROM
            dbo.group_info gi
            LEFT OUTER JOIN ( SELECT
                                    caarg.GROUP_INFO_ID
                                   ,COUNT(caarg.Client_App_Access_Role_Id) Role_Cnt
                              FROM
                                    dbo.Client_App_Access_Role_Group_Info_Map caarg
                                    JOIN dbo.Client_App_Access_Role caar
                                          ON caarg.Client_App_Access_Role_Id = caar.Client_App_Access_Role_Id
                              WHERE
                                    caar.Client_Id = @Client_Id
                              GROUP BY
                                    caarg.GROUP_INFO_ID ) AS Roles
                  ON Roles.GROUP_INFO_ID = gi.GROUP_INFO_ID
      WHERE
            gi.GROUP_INFO_ID = @GROUP_INFO_ID  
 END;


;
GO
GRANT EXECUTE ON  [dbo].[User_Role_Cnt_Sel_For_Group_By_Client] TO [CBMSApplication]
GO
