SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******  
NAME:  
 
 dbo.UBM_BATCH_MASTER_LOG_INS
 
 DESCRIPTION:   
 
 Inserts record in the UBM_BATCH_MASTER_LOG table and returns
 the UBM_BATCH_MASTER_LOG_ID.  This procedure was copied
 from the UBM_SET_UBM_MASTER_LOG_P.  The difference between
 this procedure and UBM_SET_UBM_MASTER_LOG_P is that this
 procedure returns the UBM_BATCH_MASTER_LOG_ID.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@ubmName varchar(20)

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------
@UBM_BATCH_MASTER_LOG_ID INT

  USAGE EXAMPLES:  
------------------------------------------------------------  
	EXEC UBM_BATCH_MASTER_LOG_INS 'CASS'

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page	10/2/2009
 
    
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    

******/


CREATE   PROCEDURE [dbo].[UBM_BATCH_MASTER_LOG_INS]
(
	@ubmName varchar(20)
)
AS
BEGIN

	set nocount on
	DECLARE @ubmId int
	DECLARE @statusTypeId int
	
	select @ubmId=(select ubm_id from ubm where ubm_name=@ubmName)

	select @statusTypeId=(select entity_id from entity where entity_name='In process' and ENTITY_DESCRIPTION = 'UBM_BATCH_PROCESS_STATUS_TYPE')

	insert into UBM_BATCH_MASTER_LOG (ubm_id, status_type_id, start_date) values (@ubmId, @statusTypeId, getdate())

	SELECT @@IDENTITY AS UBM_BATCH_MASTER_LOG_ID
	
	RETURN
	
END



GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_MASTER_LOG_INS] TO [CBMSApplication]
GO
