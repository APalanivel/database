SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_UTILITIES_UNDER_STATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@stateId       	varchar(25)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.SR_SAD_GET_UTILITIES_UNDER_STATE_P
@stateId varchar(25)


AS
set nocount on
Select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v,	
	VENDOR_STATE_MAP vsm ,	
	entity venType 

where 	v.VENDOR_ID = vsm.VENDOR_ID 
	and v.vendor_type_id = venType.entity_id 
	and venType.entity_type = 155
	and venType.entity_name = 'Utility' 
	AND vsm.STATE_ID = @stateId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_UTILITIES_UNDER_STATE_P] TO [CBMSApplication]
GO
