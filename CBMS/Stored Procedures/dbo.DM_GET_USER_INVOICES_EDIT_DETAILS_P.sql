SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DM_GET_USER_INVOICES_EDIT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@fromDate      	varchar(20)	          	
	@toDate        	varchar(20)	          	
	@userId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DM_GET_USER_INVOICES_EDIT_DETAILS_P '01/01/2010','01/31/2010',0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
	
******/

CREATE PROCEDURE dbo.DM_GET_USER_INVOICES_EDIT_DETAILS_P
    @fromDate VARCHAR(20),
    @toDate	  VARCHAR(20),
    @userId	  INT
AS 
BEGIN

	SET NOCOUNT ON ;

    DECLARE  @selectClause  VARCHAR(8000)
			,@fromClause	VARCHAR(8000)
			,@whereClause   VARCHAR(8000)
			,@groupByClause VARCHAR(8000)
			,@SQLStatement  VARCHAR(8000)
			
	SELECT  @selectClause = ' userinfo.USER_INFO_ID AS USER_INFO_ID,COUNT(distinct invoice.UBM_INVOICE_ID) TOTAL_EXCEPTIONS,
				userInfo.FIRST_NAME + '' '' + userInfo.LAST_NAME AS USER_INFO_NAME '


    SELECT  @fromClause = 'UBM_INVOICE invoice,
				UBM_BATCH_MASTER_LOG masterLog,
				UBM ubm,
				CU_INVOICE_CHANGE_LOG cuInvoiceChangeLog ,
				CU_INVOICE cuInvoice,
				USER_INFO userinfo'

    SELECT  @whereClause = 'invoice.UBM_BATCH_MASTER_LOG_ID = masterLog.UBM_BATCH_MASTER_LOG_ID AND
				masterLog.UBM_ID = ubm.UBM_ID AND
				invoice.IS_QUARTERLY = 0 AND
				cuInvoice.UBM_INVOICE_ID = invoice.UBM_INVOICE_ID AND 
				cuInvoice.CBMS_IMAGE_ID = invoice.CBMS_IMAGE_ID AND
				cuInvoiceChangeLog.CU_INVOICE_ID=cuInvoice.CU_INVOICE_ID AND
				cuInvoice.UPDATED_BY_ID = userinfo.USER_INFO_ID '

    SELECT  @groupByClause = 'userinfo.USER_INFO_ID,userInfo.FIRST_NAME + '' '' + userInfo.LAST_NAME '

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            IF ( @fromDate = @toDate ) 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND CONVERT(Varchar(12), masterLog.END_DATE,101) = '
                            +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''
                END
            ELSE 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND masterLog.END_DATE BETWEEN '
                            +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''
                            + ' AND ' +''''+ CONVERT(VARCHAR(12), @toDate, 101) + ''''
                END
        END 

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NULL
              OR @toDate = ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE >= '
                    +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        END 

    IF ( @fromDate IS NULL
         OR @fromDate = ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE <= '
                    + '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        END 

    IF @userId > 0 
        BEGIN
            SELECT  @whereClause = @whereClause
                    + ' AND userinfo.USER_INFO_ID = ' + STR(@userId) 
        END 

    SELECT  @SQLStatement = 'SELECT ' + @selectClause + ' FROM ' + @fromClause
            + ' WHERE ' + @whereClause + ' GROUP BY ' + @groupByClause

    EXEC(@SQLStatement)

END
GO
GRANT EXECUTE ON  [dbo].[DM_GET_USER_INVOICES_EDIT_DETAILS_P] TO [CBMSApplication]
GO
