SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  

NAME:  
 dbo.cbmsSsoSavings_GetTotalByCategory  
  
DESCRIPTION:
  
  INPUT PARAMETERS:  
  Name				DataType	Default Description  
------------------------------------------------------------  
  @MyAccountId		INT
  @currency_unit_id INT			NULL
  @client_id		INT			NULL
  @division_id		INT			NULL
  @site_id			INT			NULL               
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
  
  EXEC dbo.cbmsSsoSavings_GetTotalByCategory 49,3,10069,NULL,NULL
  
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
 PNR	  04/05/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 
******/ 

CREATE PROCEDURE dbo.cbmsSsoSavings_GetTotalByCategory
( 
 @MyAccountId INT
,@currency_unit_id INT = NULL
,@client_id INT = NULL
,@division_id INT = NULL
,@site_id INT = NULL )
AS 
BEGIN
  
      SET NOCOUNT ON ;

      EXEC dbo.cbmsSecurity_GetClientAccess @MyAccountId, @client_id OUTPUT, @division_id OUTPUT, @site_id OUTPUT  

      DECLARE @convert_date DATETIME  
    
      SET @convert_date = GETDATE()

      SELECT
            @convert_date = First_Day_Of_Month_D
      FROM
            meta.Date_Dim dd
      WHERE
            month_num = MONTH(@convert_date)
            AND Year_Num = YEAR(@convert_date)

      SELECT
            x.savings_category_type_id
           ,x.savings_category_type
           ,SUM(x.total_estimated_savings) total_estimated_savings
      FROM
            ( SELECT DISTINCT
                  s.sso_savings_id
                 ,s.savings_category_type_id
                 ,sc.entity_name savings_category_type
                 ,( s.total_estimated_savings * cuc.conversion_factor ) AS total_estimated_savings
              FROM
                  dbo.sso_savings s
                  JOIN dbo.entity sc
                        ON sc.entity_id = s.savings_category_type_id
                  JOIN dbo.SSO_SAVINGS_OWNER_MAP own
                        ON own.sso_savings_id = s.sso_savings_id
                  JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = own.Client_Hier_Id
                  LEFT OUTER JOIN dbo.currency_unit_conversion cuc
                        ON cuc.currency_group_id = ch.Client_Currency_Group_Id
                           AND cuc.base_unit_id = s.currency_unit_id
                           AND cuc.converted_unit_id = @currency_unit_id
                           AND cuc.conversion_date = @convert_date
              WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              dbo.SSO_SAVINGS_OWNER_MAP ssom
                              JOIN Core.Client_Hier chi
                                    ON chi.Client_Hier_Id = ssom.Client_Hier_Id
                           WHERE
                              ( @client_id IS NULL
                                OR chi.Client_Id = @client_id )
                              AND ( @division_id IS NULL
                                    OR chi.Sitegroup_Id = @division_id )
                              AND ( @site_id IS NULL
                                    OR chi.Site_Id = @site_id )
                              AND ssom.sso_savings_id = s.sso_savings_id ) ) x
      GROUP BY
            x.savings_category_type_id
           ,x.savings_category_type  
  
END

GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavings_GetTotalByCategory] TO [CBMSApplication]
GO
GO