SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id      
              
Description:              
			This sproc is to get the Contact Details For the Final_Review log id
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Invoice_Collection_Activity_Id 		INT					
                           
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id 1143
   
   Exec Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id 1040
   
   Exec Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id 1276
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna 
	SLP				Sri Lakshmi Pallikonda
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2019-08-21		Created For IC.
	SLP			2019-11-07		Concatenated the Invoice_Request_Type to the Chase Period
            
******/
CREATE   PROCEDURE [dbo].[Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id]
      ( 
       @Invoice_Collection_Final_Review_Log_Id INT
       ,@Invoice_Collection_Officer_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
      
      CREATE TABLE #Vendor_Dtls
            ( 
             Account_Vendor_Name VARCHAR(200)
            ,Account_Vendor_Type CHAR(8)
            ,Invoice_Collection_Account_Config_Id INT )

      DECLARE
            @Source_Type_Client INT
           ,@Source_Type_Account INT
           ,@Source_Type_Vendor INT
           ,@Contact_Level_Client INT
           ,@Contact_Level_Account INT
           ,@Contact_Level_Vendor INT;
 
 
      SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
           ,@Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                       END)
           ,@Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                      END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' )
            
   
      SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
           ,@Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                         END)
           ,@Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                        END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactLevel'
      
      INSERT      INTO #Vendor_Dtls
                  ( 
                   Account_Vendor_Name
                  ,Account_Vendor_Type
                  ,Invoice_Collection_Account_Config_Id )
                  SELECT
                        Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                                   WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                                   WHEN icav.VENDOR_NAME IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                                                   ELSE cha.Account_Vendor_Name
                                              END
                       ,Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                                   WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                                                   WHEN icav.VENDOR_NAME IS NOT NULL
                                                        AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                                                   ELSE cha.Account_Type
                                              END
                       ,icac.Invoice_Collection_Account_Config_Id
                  FROM
                        dbo.Invoice_Collection_Final_Review_Log icccm
                        INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icccmqm
                              ON icccm.Invoice_Collection_Final_Review_Log_Id = icccmqm.Invoice_Collection_Final_Review_Log_Id
                        INNER JOIN Invoice_Collection_Queue icq
                              ON icccmqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                        INNER JOIN Invoice_Collection_Account_Config icac
                              ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                        INNER JOIN core.client_Hier_Account cha
                              ON cha.Account_Id = icac.Account_id
                        INNER JOIN dbo.Code cpc
                              ON cpc.Code_Id = icac.Chase_Priority_Cd
                         LEFT OUTER JOIN ( core.Client_Hier_Account scha
                                          INNER JOIN core.Client_Hier_Account ucha1
                                                ON ucha1.Meter_Id = scha.Meter_Id
                                                   AND ucha1.Account_Type = 'Utility'
                                          INNER JOIN dbo.CONTRACT c
                                                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
                                          INNER JOIN dbo.ENTITY ce
                                                ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
                                                   AND ce.ENTITY_NAME = 'Supplier' )
                                          ON cha.Account_Id = ucha1.Account_Id
                                             AND scha.Account_Type = 'Supplier'
                                             AND ( CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                                             OR icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                                                        ELSE icac.Invoice_Collection_Service_End_Dt
                                                   END ) BETWEEN scha.Supplier_Account_begin_Dt
                                                         AND     scha.Supplier_Account_End_Dt
                        --LEFT OUTER JOIN core.Client_Hier_Account scha
                        --      ON cha.Meter_Id = scha.Meter_Id
                        --         AND scha.Account_Type = 'Supplier'
                        --         --AND icac.Invoice_Collection_Service_End_Dt BETWEEN scha.Supplier_Account_begin_Dt
                        --         --                                           AND     scha.Supplier_Account_End_Dt
                        --         AND ( CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                        --                         OR icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                        --                    ELSE icac.Invoice_Collection_Service_End_Dt
                        --               END ) BETWEEN scha.Supplier_Account_begin_Dt
                        --                     AND     scha.Supplier_Account_End_Dt
                        LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv
                                          INNER JOIN dbo.ENTITY e
                                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                                   AND e.ENTITY_NAME = 'Supplier'
                                          LEFT OUTER JOIN dbo.VENDOR v
                                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id )
                                          ON asbv.Account_Id = cha.Account_Id
                                             --AND icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt
                                             --                                           AND     asbv.Billing_End_Dt
                                             AND ( CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                                             OR asbv.Billing_End_Dt > GETDATE() THEN GETDATE()
                                                        ELSE icac.Invoice_Collection_Service_End_Dt
                                                   END ) BETWEEN asbv.Billing_Start_Dt
                                                         AND     ISNULL(asbv.Billing_End_Dt, '9999-12-31')
                        LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv1
                                          INNER JOIN dbo.ENTITY e1
                                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                                   AND e1.ENTITY_NAME = 'Supplier'
                                          LEFT OUTER JOIN dbo.VENDOR v1
                                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id )
                                          ON asbv1.Account_Id = cha.Account_Id
                        LEFT OUTER JOIN ( dbo.Invoice_Collection_Account_Contact icc
                                          INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                          INNER JOIN Contact_Info ci
                                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                                   AND icc.Is_Primary = 1
                                                   AND ( ( @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                           AND @Contact_Level_Client = ci.Contact_Level_Cd )
                                                         OR ( @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                              AND @Contact_Level_Account = ci.Contact_Level_Cd )
                                                         OR ( @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                              AND @Contact_Level_Vendor = ci.Contact_Level_Cd ) )
                                          INNER JOIN dbo.Vendor_Contact_Map vcm
                                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                                          INNER JOIN dbo.VENDOR icav
                                                ON icav.VENDOR_ID = vcm.Vendor_Id )
                                          ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                  WHERE
                        cha.Account_Type = 'Utility'
                        AND icccm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
                        AND ( @Invoice_Collection_Officer_Id IS NULL
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id )
                  GROUP BY
                        CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                             WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                             WHEN icav.VENDOR_NAME IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                             ELSE cha.Account_Vendor_Name
                        END
                       ,CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                             WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                             WHEN icav.VENDOR_NAME IS NOT NULL
                                  AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                             ELSE cha.Account_Type
                        END
                       ,icac.Invoice_Collection_Account_Config_Id
   
      SELECT
            ch.Client_name
           ,ch.Site_name
           ,cha.Account_Number
           ,icac.Invoice_Collection_Alternative_Account_Number AS Alternate_Account_Number
           ,ch.Country_Name
           ,ch.State_Name
           ,ISNULL(vd.Account_Vendor_Type, cha.Account_Type) Account_Type
           ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                 ELSE Commodities.Commodity_Name
            END AS Commodity_Name
           ,ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) Account_Vendor_Name
           ,ISNULL(ifc.code_value, icgc.Code_Value) Invoice_Frequency
           ,icq.Collection_Start_Dt
           ,icq.Collection_End_Dt
           ,ci.Email_Address Contact_Email
           ,ci.First_Name + ' ' + ci.Last_Name Username
           ,ci.Phone_Number Contact_Phone
           ,ch.Client_Id
           ,aic.Code_Value source_Type
           ,icac.Invoice_Collection_Account_Config_Id
           ,icccm.Invoice_Final_Review_Comment
           ,icccmqm.Invoice_Collection_Queue_Id
		   ,icmrt.Code_Value AS Invoice_Request_Type
      INTO
            #Invoice_Collection_Final_Review_Log
      FROM
            dbo.Invoice_Collection_Final_Review_Log icccm
            INNER JOIN dbo.Invoice_Collection_Final_Review_Log_Queue_Map icccmqm
                  ON icccm.Invoice_Collection_Final_Review_Log_Id = icccmqm.Invoice_Collection_Final_Review_Log_Id
            INNER JOIN Invoice_Collection_Queue icq
                  ON icccmqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN Invoice_Collection_Account_Config icac
                  ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN core.client_Hier_Account cha
                  ON cha.Account_Id = icac.Account_id
            INNER JOIN core.client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN user_info ui
                  ON ui.user_info_id = icac.Invoice_Collection_Officer_User_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                  ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN ( dbo.Account_Invoice_Collection_Month aicm
                         LEFT OUTER JOIN Account_Invoice_Collection_Frequency aicfe
                              ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                         LEFT OUTER JOIN Code ifc
                              ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                         LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                              ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                         LEFT OUTER JOIN dbo.Code icgc
                              ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd )
                         ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN dbo.Code cpc
                  ON cpc.Code_Id = icac.Chase_Priority_Cd
            LEFT OUTER JOIN dbo.Code icmrt
                  ON icmrt.Code_Id = icq.Invoice_Request_Type_Cd
            LEFT OUTER JOIN ( dbo.Invoice_Collection_Account_Contact icc
                              INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                    ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                       AND aics.Is_Primary = 1
                              INNER JOIN Contact_Info ci
                                    ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                       AND icc.Is_Primary = 1
                                       AND ( ( @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                               AND @Contact_Level_Client = ci.Contact_Level_Cd )
                                             OR ( @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                  AND @Contact_Level_Account = ci.Contact_Level_Cd )
                                             OR ( @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                  AND @Contact_Level_Vendor = ci.Contact_Level_Cd ) ) )
                              ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Code aic
                  ON aic.Code_Id = aics.Invoice_Source_Type_Cd
            CROSS APPLY ( SELECT
                              c.Commodity_Name + ','
                          FROM
                              Invoice_Collection_Queue icqe
                              INNER JOIN Invoice_Collection_Account_Config icac1
                                    ON icqe.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
                              INNER JOIN core.Client_Hier_Account cha
                                    ON cha.Account_Id = icac1.Account_Id
                              INNER JOIN dbo.Commodity c
                                    ON c.Commodity_Id = cha.Commodity_Id
                          WHERE
                              icq.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
                          GROUP BY
                              c.Commodity_Name
            FOR
                          XML PATH('') ) Commodities ( Commodity_Name )
            LEFT OUTER JOIN #Vendor_Dtls vd
                  ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
      WHERE
            icccm.Invoice_Collection_Final_Review_Log_Id = @Invoice_Collection_Final_Review_Log_Id
            AND ( @Invoice_Collection_Officer_Id IS NULL
                              OR icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id )
      GROUP BY
            ch.Client_name
           ,ch.Site_name
           ,cha.Account_Number
           ,icac.Invoice_Collection_Alternative_Account_Number
           ,ch.Country_Name
           ,ch.State_Name
           ,ISNULL(vd.Account_Vendor_Type, cha.Account_Type)
           ,CASE WHEN LEN(Commodities.Commodity_Name) > 0 THEN LEFT(Commodities.Commodity_Name, LEN(Commodities.Commodity_Name) - 1)
                 ELSE Commodities.Commodity_Name
            END
           ,ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
           ,ifc.code_value
           ,icgc.Code_Value
           ,icq.Collection_Start_Dt
           ,icq.Collection_End_Dt
           ,ci.Email_Address
           ,ci.Phone_Number
           ,ch.Client_Id
           ,aic.Code_Value
           ,icac.Invoice_Collection_Account_Config_Id
           ,ci.First_Name
           ,ci.Last_Name
           ,icccm.Invoice_Final_Review_Comment
           ,icccmqm.Invoice_Collection_Queue_Id
           ,icmrt.Code_Value

     
      SELECT
            Client_name
           ,Site_name
           ,Account_Number
           ,Alternate_Account_Number
           ,Country_Name
           ,State_Name
           ,Account_Type
           ,Commodity_Name
           ,Account_Vendor_Name
           ,Invoice_Frequency
           ,CASE WHEN LEN(Periods.Period_to_Final_Review) > 0 THEN LEFT(Periods.Period_to_Final_Review, LEN(Periods.Period_to_Final_Review) - 1)
                 ELSE Periods.Period_to_Final_Review
            END AS Period_to_Final_Review
           ,Contact_Email
           ,Username
           ,Contact_Phone
           ,Client_Id
           ,source_Type
           ,Invoice_Collection_Account_Config_Id
           ,'' AS Invoice_Final_Review_Comment
      FROM
            #Invoice_Collection_Final_Review_Log iccl
            CROSS APPLY ( SELECT
                              CONVERT(VARCHAR(12), iccle.Collection_Start_Dt, 105) + ' # ' + CONVERT(VARCHAR(12), iccle.Collection_End_Dt, 105) 
							  +'/'+ LEFT(ISNULL(iccle.Invoice_Request_Type, ''), 1)
							  + ','
                          FROM
                              #Invoice_Collection_Final_Review_Log iccle
                          WHERE
                              iccl.Invoice_Collection_Account_Config_Id = iccle.Invoice_Collection_Account_Config_Id
                              AND ISNULL(iccl.Invoice_Frequency, 'm') = ISNULL(iccle.Invoice_Frequency, 'm')
                              AND iccl.Account_Vendor_Name = iccle.Account_Vendor_Name
                          ORDER BY
                              iccle.Collection_Start_Dt
            FOR
                          XML PATH('') ) Periods ( Period_to_Final_Review )
      GROUP BY
            Client_name
           ,Site_name
           ,Account_Number
           ,Alternate_Account_Number
           ,Country_Name
           ,State_Name
           ,Account_Type
           ,Commodity_Name
           ,Account_Vendor_Name
           ,Invoice_Frequency
           ,CASE WHEN LEN(Periods.Period_to_Final_Review) > 0 THEN LEFT(Periods.Period_to_Final_Review, LEN(Periods.Period_to_Final_Review) - 1)
                 ELSE Periods.Period_to_Final_Review
            END
           ,Contact_Email
           ,Username
           ,Contact_Phone
           ,Client_Id
           ,source_Type
           ,Invoice_Collection_Account_Config_Id
     
     

      DROP TABLE #Vendor_Dtls
      DROP TABLE #Invoice_Collection_Final_Review_Log
END;






;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Final_Review_Mail_Attachment_Sel_By_Invoice_Collection_Final_Review_Log_Id] TO [CBMSApplication]
GO
