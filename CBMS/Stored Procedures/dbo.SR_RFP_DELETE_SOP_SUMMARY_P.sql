
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_DELETE_SOP_SUMMARY_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                    DataType          Default     Description    
---------------------------------------------------------------------------------    
	@accountGroupId			int
	@isBidGroupId			int
	@minSOPDetailsId		int
	@maxSOPDetailsId		int
	@mode					varchar(10)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_DELETE_SOP_SUMMARY_P
--@accountGroupId int,
--@isBidGroupId int,
--@minSOPDetailsId = 73,
--@maxSOPDetailsId = 74,
--@mode varchar(10)




AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-05-09	GCS-887 Deleting references from dbo.Sr_Rfp_Sop_Service_Condition_Response

******/
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_SOP_SUMMARY_P]
      ( 
       @accountGroupId INT
      ,@isBidGroupId INT
      ,@minSOPDetailsId INT
      ,@maxSOPDetailsId INT
      ,@mode VARCHAR(10) )
AS 
BEGIN
      SET nocount ON
      DECLARE
            @oldAccountTermId INT
           ,@oldRFPBidId INT
           ,@sopDetailsId INT
      DECLARE @summitBidId INT
      DECLARE @supplierCommentsId INT
      DECLARE @supplierServiceId INT
	
      DECLARE @deliveryFuelId INT

      DECLARE @balancingToleranceId INT
      DECLARE @riskManagementId INT
      DECLARE @alternateFuelId INT
      DECLARE @pricingScopeId INT
      DECLARE @miscPowerId INT
      DECLARE @deleteTermId INT

	
      IF @mode = 'View_SOP' 
            BEGIN	
	
	
                  DECLARE DELETE_SAVE_SOP CURSOR FAST_FORWARD
                  FOR
                  SELECT
                        details.sr_rfp_account_term_id
                       ,details.sr_rfp_bid_id
                       ,details.sr_rfp_sop_details_id
                  FROM
                        sr_rfp_sop_details details
                  WHERE
                        sr_rfp_sop_details_id BETWEEN @minSOPDetailsId
                                              AND     @maxSOPDetailsId

                  OPEN DELETE_SAVE_SOP
                  FETCH NEXT FROM DELETE_SAVE_SOP INTO @oldAccountTermId, @oldRFPBidId, @sopDetailsId

	

                  WHILE ( @@fetch_status <> -1 ) 
                        BEGIN
                              IF ( @@fetch_status <> -2 ) 
                                    BEGIN
			
                                          IF @oldRFPBidId != ''
                                                OR @oldRFPBidId IS NOT NULL
                                                AND @oldRFPBidId > 0 
                                                SELECT
                                                      @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
                                                FROM
                                                      SR_RFP_BID
                                                WHERE
                                                      SR_RFP_BID_ID = @oldRFPBidId
                                          SELECT
                                                @supplierCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                                          FROM
                                                SR_RFP_BID
                                          WHERE
                                                SR_RFP_BID_ID = @oldRFPBidId
                                          SELECT
                                                @supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID
                                          FROM
                                                SR_RFP_BID
                                          WHERE
                                                SR_RFP_BID_ID = @oldRFPBidId
                                          SELECT
                                                @deliveryFuelId = SR_RFP_DELIVERY_FUEL_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                          SELECT
                                                @balancingToleranceId = SR_RFP_BALANCING_TOLERANCE_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                          SELECT
                                                @riskManagementId = SR_RFP_RISK_MANAGEMENT_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                          SELECT
                                                @alternateFuelId = SR_RFP_ALTERNATE_FUEL_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                          SELECT
                                                @pricingScopeId = SR_RFP_PRICING_SCOPE_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                          SELECT
                                                @miscPowerId = SR_RFP_MISCELLANEOUS_POWER_ID
                                          FROM
                                                SR_RFP_SUPPLIER_SERVICE
                                          WHERE
                                                SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
				
                                          PRINT '@oldRFPBidId ' + str(@oldRFPBidId)
                                          PRINT '@deliveryFuelId ' + str(@deliveryFuelId)
                                          PRINT '@supplierServiceId ' + str(@supplierServiceId)	
                                          PRINT '@balancingToleranceId ' + str(@balancingToleranceId)
                                          PRINT '@riskManagementId ' + str(@riskManagementId)
                                          PRINT '@alternateFuelId ' + str(@alternateFuelId)
                                          PRINT '@pricingScopeId ' + str(@pricingScopeId)
                                          PRINT '@miscPowerId ' + str(@miscPowerId)

                                          IF @sopDetailsId != ''
                                                OR @sopDetailsId IS NOT NULL
                                                AND @sopDetailsId > 0 
                                                BEGIN
                                                      DELETE FROM
                                                            dbo.Sr_Rfp_Sop_Service_Condition_Response
                                                      WHERE
                                                            Sr_Rfp_Sop_Details_Id = @sopDetailsId
                                                      DELETE FROM
                                                            SR_RFP_SOP_DETAILS
                                                      WHERE
                                                            SR_RFP_SOP_DETAILS_ID = @sopDetailsId
                                                      IF ( SELECT
                                                            count(*)
                                                           FROM
                                                            SR_RFP_SOP_DETAILS
                                                           WHERE
                                                            sr_rfp_account_term_id = @oldAccountTermId ) = 0 
                                                            BEGIN
                                                                  DELETE FROM
                                                                        SR_RFP_ACCOUNT_TERM
                                                                  WHERE
                                                                        SR_RFP_ACCOUNT_TERM_ID = @oldAccountTermId
                                                            END

                                                      DELETE FROM
                                                            SR_RFP_BID
                                                      WHERE
                                                            SR_RFP_BID_ID = @oldRFPBidId
                                                END


                                          IF @supplierServiceId != ''
                                                OR @supplierServiceId IS NOT NULL
                                                AND @supplierServiceId > 0 
                                                BEGIN
                                                      DELETE FROM
                                                            SR_RFP_SUPPLIER_SERVICE
                                                      WHERE
                                                            SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId

                                                      IF @deliveryFuelId != ''
                                                            OR @deliveryFuelId IS NOT NULL
                                                            AND @deliveryFuelId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @deliveryFuelId '
                                                                  DELETE FROM
                                                                        SR_RFP_DELIVERY_FUEL
                                                                  WHERE
                                                                        SR_RFP_DELIVERY_FUEL_ID = @deliveryFuelId
                                                            END
					
                                                      IF @balancingToleranceId != ''
                                                            OR @balancingToleranceId IS NOT NULL
                                                            AND @balancingToleranceId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @balancingToleranceId '
                                                                  DELETE FROM
                                                                        SR_RFP_BALANCING_TOLERANCE
                                                                  WHERE
                                                                        SR_RFP_BALANCING_TOLERANCE_ID = @balancingToleranceId
                                                            END
	
                                                      IF @riskManagementId != ''
                                                            OR @riskManagementId IS NOT NULL
                                                            AND @riskManagementId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @riskManagementId '
                                                                  DELETE FROM
                                                                        SR_RFP_RISK_MANAGEMENT
                                                                  WHERE
                                                                        SR_RFP_RISK_MANAGEMENT_ID = @riskManagementId
                                                            END
	
                                                      IF @alternateFuelId != ''
                                                            OR @alternateFuelId IS NOT NULL
                                                            AND @alternateFuelId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @alternateFuelId '
                                                                  DELETE FROM
                                                                        SR_RFP_ALTERNATE_FUEL
                                                                  WHERE
                                                                        SR_RFP_ALTERNATE_FUEL_ID = @alternateFuelId
                                                            END
	
                                                      IF @pricingScopeId != ''
                                                            OR @pricingScopeId IS NOT NULL
                                                            AND @pricingScopeId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @pricingScopeId '
                                                                  DELETE FROM
                                                                        SR_RFP_PRICING_SCOPE
                                                                  WHERE
                                                                        SR_RFP_PRICING_SCOPE_ID = @pricingScopeId
                                                            END
	
                                                      IF @miscPowerId != ''
                                                            OR @miscPowerId IS NOT NULL
                                                            AND @miscPowerId > 0 
                                                            BEGIN
                                                                  PRINT 'DELETING @miscPowerId '
                                                                  DELETE FROM
                                                                        SR_RFP_MISCELLANEOUS_POWER
                                                                  WHERE
                                                                        SR_RFP_MISCELLANEOUS_POWER_ID = @miscPowerId
                                                            END
	
                                                END

                                          IF @supplierCommentsId != ''
                                                OR @supplierCommentsId IS NOT NULL
                                                AND @supplierCommentsId > 0 
                                                BEGIN
                                                      PRINT 'DELETING @supplierCommentsId '
                                                      DELETE FROM
                                                            SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                                                      WHERE
                                                            SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId
                                                      DELETE FROM
                                                            SR_RFP_SUPPLIER_PRICE_COMMENTS
                                                      WHERE
                                                            SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId
                                                END

                                          IF @summitBidId != ''
                                                OR @summitBidId IS NOT NULL
                                                AND @summitBidId > 0 
                                                BEGIN
                                                      PRINT 'DELETING @summitBidId '
                                                      DELETE FROM
                                                            SR_RFP_BID_REQUIREMENTS
                                                      WHERE
                                                            SR_RFP_BID_REQUIREMENTS_ID = @summitBidId
                                                END
				
	
					
				

                                    END

                              FETCH NEXT FROM DELETE_SAVE_SOP INTO @oldAccountTermId, @oldRFPBidId, @sopDetailsId
                        END

	
                  CLOSE DELETE_SAVE_SOP
                  DEALLOCATE DELETE_SAVE_SOP

            END


      ELSE 
            IF @mode = 'Reset_SOP' 
                  BEGIN

                        DECLARE DELETE_SAVE_SOP CURSOR FAST_FORWARD
                        FOR
                        SELECT
                              details.sr_rfp_account_term_id
                             ,details.sr_rfp_bid_id
                             ,details.sr_rfp_sop_details_id
                        FROM
                              sr_rfp_sop_summary summary
                             ,sr_rfp_sop sop
                             ,sr_rfp_account_term term
                             ,sr_rfp_sop_details details
                             ,sr_rfp_bid bid
                        WHERE
                              summary.sr_account_group_id = @accountGroupId
                              AND summary.is_bid_group = @isBidGroupId
                              AND summary.sr_rfp_sop_summary_id = sop.sr_rfp_sop_summary_id
                              AND sop.sr_rfp_sop_id = details.sr_rfp_sop_id
                              AND term.sr_rfp_account_term_id = details.sr_rfp_account_term_id
                              AND term.is_sop = 1
                              AND details.sr_rfp_bid_id = bid.sr_rfp_bid_id

                        OPEN DELETE_SAVE_SOP
                        FETCH NEXT FROM DELETE_SAVE_SOP INTO @oldAccountTermId, @oldRFPBidId, @sopDetailsId

	

                        WHILE ( @@fetch_status <> -1 ) 
                              BEGIN
                                    IF ( @@fetch_status <> -2 ) 
                                          BEGIN
			
                                                IF @oldRFPBidId != ''
                                                      OR @oldRFPBidId IS NOT NULL
                                                      AND @oldRFPBidId > 0 
                                                      SELECT
                                                            @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
                                                      FROM
                                                            SR_RFP_BID
                                                      WHERE
                                                            SR_RFP_BID_ID = @oldRFPBidId
                                                SELECT
                                                      @supplierCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                                                FROM
                                                      SR_RFP_BID
                                                WHERE
                                                      SR_RFP_BID_ID = @oldRFPBidId
                                                SELECT
                                                      @supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID
                                                FROM
                                                      SR_RFP_BID
                                                WHERE
                                                      SR_RFP_BID_ID = @oldRFPBidId
                                                SELECT
                                                      @deliveryFuelId = SR_RFP_DELIVERY_FUEL_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                                SELECT
                                                      @balancingToleranceId = SR_RFP_BALANCING_TOLERANCE_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                                SELECT
                                                      @riskManagementId = SR_RFP_RISK_MANAGEMENT_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                                SELECT
                                                      @alternateFuelId = SR_RFP_ALTERNATE_FUEL_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                                SELECT
                                                      @pricingScopeId = SR_RFP_PRICING_SCOPE_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
                                                SELECT
                                                      @miscPowerId = SR_RFP_MISCELLANEOUS_POWER_ID
                                                FROM
                                                      SR_RFP_SUPPLIER_SERVICE
                                                WHERE
                                                      SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
				
                                                PRINT '@oldRFPBidId ' + str(@oldRFPBidId)
                                                PRINT '@deliveryFuelId ' + str(@deliveryFuelId)
                                                PRINT '@supplierServiceId ' + str(@supplierServiceId)	
                                                PRINT '@balancingToleranceId ' + str(@balancingToleranceId)
                                                PRINT '@riskManagementId ' + str(@riskManagementId)
                                                PRINT '@alternateFuelId ' + str(@alternateFuelId)
                                                PRINT '@pricingScopeId ' + str(@pricingScopeId)
                                                PRINT '@miscPowerId ' + str(@miscPowerId)

                                                IF @sopDetailsId != ''
                                                      OR @sopDetailsId IS NOT NULL
                                                      AND @sopDetailsId > 0 
                                                      BEGIN
                                                            DELETE FROM
                                                                  dbo.Sr_Rfp_Sop_Service_Condition_Response
                                                            WHERE
                                                                  Sr_Rfp_Sop_Details_Id = @sopDetailsId
                                                            DELETE FROM
                                                                  SR_RFP_SOP_DETAILS
                                                            WHERE
                                                                  SR_RFP_SOP_DETAILS_ID = @sopDetailsId
                                                            DELETE FROM
                                                                  SR_RFP_BID
                                                            WHERE
                                                                  SR_RFP_BID_ID = @oldRFPBidId
                                                      END

				

                                                IF @supplierServiceId != ''
                                                      OR @supplierServiceId IS NOT NULL
                                                      AND @supplierServiceId > 0 
                                                      BEGIN
                                                            DELETE FROM
                                                                  SR_RFP_SUPPLIER_SERVICE
                                                            WHERE
                                                                  SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId

                                                            IF @deliveryFuelId != ''
                                                                  OR @deliveryFuelId IS NOT NULL
                                                                  AND @deliveryFuelId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @deliveryFuelId '
                                                                        DELETE FROM
                                                                              SR_RFP_DELIVERY_FUEL
                                                                        WHERE
                                                                              SR_RFP_DELIVERY_FUEL_ID = @deliveryFuelId
                                                                  END
					
                                                            IF @balancingToleranceId != ''
                                                                  OR @balancingToleranceId IS NOT NULL
                                                                  AND @balancingToleranceId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @balancingToleranceId '
                                                                        DELETE FROM
                                                                              SR_RFP_BALANCING_TOLERANCE
                                                                        WHERE
                                                                              SR_RFP_BALANCING_TOLERANCE_ID = @balancingToleranceId
                                                                  END
	
                                                            IF @riskManagementId != ''
                                                                  OR @riskManagementId IS NOT NULL
                                                                  AND @riskManagementId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @riskManagementId '
                                                                        DELETE FROM
                                                                              SR_RFP_RISK_MANAGEMENT
                                                                        WHERE
                                                                              SR_RFP_RISK_MANAGEMENT_ID = @riskManagementId
                                                                  END
	
                                                            IF @alternateFuelId != ''
                                                                  OR @alternateFuelId IS NOT NULL
                                                                  AND @alternateFuelId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @alternateFuelId '
                                                                        DELETE FROM
                                                                              SR_RFP_ALTERNATE_FUEL
                                                                        WHERE
                                                                              SR_RFP_ALTERNATE_FUEL_ID = @alternateFuelId
                                                                  END
	
                                                            IF @pricingScopeId != ''
                                                                  OR @pricingScopeId IS NOT NULL
                                                                  AND @pricingScopeId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @pricingScopeId '
                                                                        DELETE FROM
                                                                              SR_RFP_PRICING_SCOPE
                                                                        WHERE
                                                                              SR_RFP_PRICING_SCOPE_ID = @pricingScopeId
                                                                  END
	
                                                            IF @miscPowerId != ''
                                                                  OR @miscPowerId IS NOT NULL
                                                                  AND @miscPowerId > 0 
                                                                  BEGIN
                                                                        PRINT 'DELETING @miscPowerId '
                                                                        DELETE FROM
                                                                              SR_RFP_MISCELLANEOUS_POWER
                                                                        WHERE
                                                                              SR_RFP_MISCELLANEOUS_POWER_ID = @miscPowerId
                                                                  END
	
                                                      END

                                                IF @supplierCommentsId != ''
                                                      OR @supplierCommentsId IS NOT NULL
                                                      AND @supplierCommentsId > 0 
                                                      BEGIN
                                                            PRINT 'DELETING @supplierCommentsId '
                                                            DELETE FROM
                                                                  SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                                                            WHERE
                                                                  SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId
                                                            DELETE FROM
                                                                  SR_RFP_SUPPLIER_PRICE_COMMENTS
                                                            WHERE
                                                                  SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId
                                                      END

                                                IF @summitBidId != ''
                                                      OR @summitBidId IS NOT NULL
                                                      AND @summitBidId > 0 
                                                      BEGIN
                                                            PRINT 'DELETING @summitBidId '
                                                            DELETE FROM
                                                                  SR_RFP_BID_REQUIREMENTS
                                                            WHERE
                                                                  SR_RFP_BID_REQUIREMENTS_ID = @summitBidId
                                                      END
				
	
					
				

                                          END

                                    FETCH NEXT FROM DELETE_SAVE_SOP INTO @oldAccountTermId, @oldRFPBidId, @sopDetailsId
                              END

                        DELETE FROM
                              SR_RFP_ACCOUNT_TERM
                        WHERE
                              SR_ACCOUNT_GROUP_ID = @accountGroupId
                              AND IS_BID_GROUP = @isBidGroupId
                              AND is_sop = 1

                        DELETE FROM
                              SR_RFP_SOP
                        WHERE
                              SR_RFP_SOP_SUMMARY_ID IN ( SELECT
                                                            SR_RFP_SOP_SUMMARY_ID
                                                         FROM
                                                            SR_RFP_SOP_SUMMARY
                                                         WHERE
                                                            SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                            AND IS_BID_GROUP = @isBidGroupId )

                        CLOSE DELETE_SAVE_SOP
                        DEALLOCATE DELETE_SAVE_SOP

                  END

END	
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SOP_SUMMARY_P] TO [CBMSApplication]
GO
