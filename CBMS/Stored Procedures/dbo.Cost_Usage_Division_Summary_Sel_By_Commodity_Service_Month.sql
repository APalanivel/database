
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
 dbo.Cost_Usage_Division_Summary_Sel_By_Commodity_Service_Month    
   
 DESCRIPTION:    
 Used to select cost usage data rolled up to the division level.  
   
 INPUT PARAMETERS:    
 Name   DataType Default  Description    
------------------------------------------------------------    
 @Client_Id  INT  
 @Commodity_Id  INT  
 @Begin_Dt  DATE  
 @End_Dt   DATE  
 @Currency_Unit_Id INT  
 @UOM_Id   INT  
 @Sitegroup_Id  INT  NULL  
 @Site_Id   INT  NULL  
 @Site_Not_Managed INT  NULL  
 @Country_Id  INT  NULL  
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  
   
 EXEC Cost_Usage_Division_Summary_Sel_By_Commodity_Service_Month 235, 67, '1/1/2010','12/1/2010', 3, 1409  
 EXEC Cost_Usage_Division_Summary_Sel_By_Commodity_Service_Month 10069, 291, '1/1/2011','12/1/2011', 3, 12  

   
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 SSR		Sharad srivastava  
 BCH		Balaraju
 RR			Raghu Reddy  
   
 MODIFICATIONS     
 Initials	Date	    Modification    
------------------------------------------------------------    
 SSR		02/08/2010  Created  
 SSR		03/11/2010  Unit_Cost Column With Decimal (32,16) Added in Resultset  
 SKA		03/12/2010  Used Left join for Cost_Usage_Site_Dtl instead of Inner join to get all the rows even if there is no entries in cu.  
 SSR		03/12/2010  Changed default value of @Site_Not_Managed from 0 to NULL  
 HG			25/03/2010  Script modified to return only the Total cost and volume buckets.  
						Conversion factors determined based on  bucket type.  
 HG			04/01/2010  cost & usage data rolled up to division level , direct join of Invoice_participation and Cost_Usage_Site_Dtl will give wrong results as we have multiple entries for the same site with different account.  
						Invoice participation logic moved to seperate cte and used that results in the final select.  
 AP			09/12/2011  Following changes made as a part of Addl Data Project  
						- Removed hardcoded buckets and used table variable and used dbo.Cost_usage_Bucket_Sel_By_Commodity SP to load buckets.  
						- Removed @Client_Sites table variable and used #Client_Sites temp table instead  
 BCH		2012-03-19  Removed site ,sitegorup,address and state tables used core.client_hier table instead.and replaced client_hier_id intead site_id logic.  
 AP			2012-03-20  Replaced @Site_Id parameter with @Client_Hier_Id and modified logic to remove multiple CTEs
 RR			2012-07-09  Removed the script replcing null values with zeros for Total_Cost, Volume, Unit_Cost
******/  
CREATE PROCEDURE dbo.Cost_Usage_Division_Summary_Sel_By_Commodity_Service_Month
      ( 
       @Client_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_unit_id INT
      ,@UOM_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Client_Hier_Id INT = NULL
      ,@Site_Not_Managed INT = NULL
      ,@Country_id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )  
            
      CREATE TABLE #Client_Sites
            ( 
             Client_Hier_Id INT
            ,Site_Id INT
            ,Division_Id INT
            ,Sitegroup_Name VARCHAR(200)
            ,Currency_Group_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) )

      INSERT      INTO #Client_Sites
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Division_Id
                  ,Sitegroup_Name
                  ,Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Sitegroup_Id AS Division_Id
                       ,ch.Sitegroup_Name
                       ,ch.Client_Currency_Group_Id
                  FROM
                        core.Client_Hier ch
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Site_Id > 0
                        AND ( @Client_Hier_Id IS NULL
                              OR ch.Client_Hier_Id = @Client_Hier_Id )
                        AND ( @Sitegroup_Id IS NULL
                              OR ch.Sitegroup_Id = @Sitegroup_Id )
                        AND ( @Site_Not_Managed IS NULL
                              OR ch.site_Not_Managed = @Site_Not_Managed )
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )  
                    
  
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id ;  
    
      WITH  Cte_Invoice_Participation
              AS ( SELECT
                        s.Division_Id
                       ,( max(case WHEN ip.Is_Expected = 1
                                        AND ip.Is_Received = 1 THEN 1
                                   ELSE 0
                              END) ) Is_Complete
                       ,( max(convert(INT, ip.Is_Received)) ) Is_published
                       ,( max(case WHEN ip.Recalc_Under_Review = 1
                                        OR ip.Variance_Under_Review = 1 THEN 1
                                   ELSE 0
                              END) ) Is_Under_Review
                   FROM
                        dbo.Invoice_Participation ip
                        JOIN #Client_Sites s
                              ON s.Site_Id = ip.SITE_ID
                   WHERE
                        ip.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                   GROUP BY
                        s.Division_Id)
            SELECT
                  s.Division_Id
                 ,s.Sitegroup_Name AS Division_Name
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cc.Conversion_Factor
                           ELSE 0
                      END) AS Total_Cost
                 ,sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * uc.Conversion_Factor
                           ELSE 0
                      END) AS Volume
                 ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cc.Conversion_Factor
                           ELSE 0
                      END) / nullif(sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * uc.Conversion_Factor
                                             ELSE 0
                                        END), 0) AS Unit_Cost
                 ,isnull(max(ip.Is_Complete), 0) Is_Complete
                 ,isnull(max(ip.Is_published), 0) Is_published
                 ,isnull(max(ip.Is_Under_Review), 0) Is_Under_Review
            FROM
                  #Client_Sites s
                  LEFT OUTER JOIN dbo.Cost_Usage_Site_Dtl cusd
                        ON s.Client_Hier_Id = cusd.Client_Hier_Id
                           AND cusd.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                  LEFT OUTER JOIN @Cost_Usage_Bucket_Id CUB
                        ON CUB.Bucket_Master_Id = cusd.Bucket_Master_Id
                  LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uc
                        ON uc.Base_Unit_ID = cusd.UOM_Type_Id
                           AND uc.Converted_Unit_ID = @UOM_Id
                  LEFT JOIN dbo.Currency_Unit_Conversion cc
                        ON cc.Currency_Group_Id = s.Currency_Group_Id
                           AND cc.Base_Unit_Id = cusd.Currency_Unit_ID
                           AND cc.Converted_Unit_Id = @Currency_Unit_Id
                           AND cc.Conversion_Date = cusd.Service_Month
                  LEFT OUTER JOIN Cte_Invoice_Participation ip
                        ON ip.Division_Id = s.Division_Id
            GROUP BY
                  s.Division_ID
                 ,s.Sitegroup_Name
            ORDER BY
                  Division_Name
                       

      DROP TABLE #Client_Sites
  
END  
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Division_Summary_Sel_By_Commodity_Service_Month] TO [CBMSApplication]
GO
