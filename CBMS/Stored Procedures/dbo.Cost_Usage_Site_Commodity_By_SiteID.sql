
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Cost_Usage_Site_Commodity_By_SiteID

DESCRIPTION:
	Used to Select the data from Cost_Usage_Account_Dtl table for the given Site_id , period and commodity
	
INPUT PARAMETERS:
    Name			DataType		Default	Description
------------------------------------------------------------
    @Client_Hier_Id	INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Site_Commodity_By_SiteID 1204
	EXEC dbo.Cost_Usage_Site_Commodity_By_SiteID 1227

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	SKA		Shobhit Kumar Agrawal
	AP		Athmaram Pabbathi

MODIFICATIONS
    Initials	 Date	   Modification
------------------------------------------------------------
    SKA		 03/04/2010  Created
    DMR		 09/10/2010  Modified for Quoted_Identifier
    AP		 03/17/2012  Replaced @Site_Id parameter with @Client_Hier_Id
******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Commodity_By_SiteID ( @Client_Hier_Id INT )
AS 
BEGIN
      SET NOCOUNT ON

      SELECT
            cusd.Client_Hier_Id
           ,cusd.UOM_Type_Id
           ,cusd.CURRENCY_UNIT_ID
           ,bm.Commodity_Id
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
            JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
      WHERE
            cusd.Client_Hier_Id = @Client_Hier_Id
      GROUP BY
            cusd.Client_Hier_Id
           ,cusd.UOM_Type_Id
           ,cusd.CURRENCY_UNIT_ID
           ,bm.Commodity_Id

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Commodity_By_SiteID] TO [CBMSApplication]
GO
