SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.IS_MARKET_FORECAST_DATE_VALID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@forecastDate  	datetime  	          	
	@marketName    	varchar(30)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.IS_MARKET_FORECAST_DATE_VALID_P  

@userId varchar(10),
@sessionId varchar(20),
@forecastDate Datetime,
@marketName varchar(30)


AS
set nocount on

SELECT asofdate FROM rm_market_forecast forecast,rm_market market
                 WHERE forecast.asofdate>=@forecastDate 
                  and forecast.rm_market_id = market.rm_market_id
                  and market.rm_market_name = @marketName
GO
GRANT EXECUTE ON  [dbo].[IS_MARKET_FORECAST_DATE_VALID_P] TO [CBMSApplication]
GO
