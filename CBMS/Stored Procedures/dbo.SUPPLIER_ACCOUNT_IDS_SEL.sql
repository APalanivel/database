SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 [DBO].[SUPPLIER_ACCOUNT_IDS_SEL]  
  
DESCRIPTION:  
 GETS ALL THE SUPPLIER ACCOUNT IDS FOR THE METER IDS AND FOR THE SELECTED CONTRACT - ADDDING METERS
  
  
INPUT PARAMETERS:  
NAME			DATATYPE	DEFAULT		DESCRIPTION  
------------------------------------------------------------  
@CONTRACT_ID	INT,
@METER_ID		INT
  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  

  
USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC SUPPLIER_ACCOUNT_IDS_SEL 36608,12345
  
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
MGB  BHASKARAN GOPALAKRISHNAN  
HG	 Hari  
MODIFICATIONS  
INITIALS	DATE		MODIFICATION  
------------------------------------------------------------  
MGB			07-OCT-09	CREATED  
HG			25-OCT-09	Contract table join removed as contract_id exists in Supplier_Account_meter_map table.
HG			28-OCT-09	MAP.IS_HISTORY = 0 condition added.
*/
CREATE PROCEDURE  [DBO].[SUPPLIER_ACCOUNT_IDS_SEL]
	@CONTRACT_ID INT,
	@METER_ID INT
AS

BEGIN 
	SET NOCOUNT ON
	
	SELECT 
		MAP.ACCOUNT_ID ACCOUNT_ID 
	FROM		
		dbo.SUPPLIER_ACCOUNT_METER_MAP MAP
		JOIN dbo.METER M
			ON M.METER_ID = MAP.METER_ID
	WHERE
		map.CONTRACT_ID = @CONTRACT_ID 
		AND M.METER_ID = @METER_ID
		AND MAP.IS_HISTORY = 0
END
GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_IDS_SEL] TO [CBMSApplication]
GO
