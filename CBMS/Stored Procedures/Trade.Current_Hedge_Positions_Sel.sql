SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.GET_CURRENT_HEDGE_POSITIONS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@startDate      	VARCHAR(12)
	@endDate        	VARCHAR(12)
	@clientId      	int	          	
	@divisionOrSiteId	int
	@displayStatus 	int
	@unitId        	int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Trade.Current_Hedge_Positions_Sel '2019-01-01','2019-12-01',NULL,Null,'2030,2372',Null,25
	EXEC Trade.Current_Hedge_Positions_Sel '2017-01-01','2017-12-01',1005,Null,Null,Null,25
	EXEC Trade.Current_Hedge_Positions_Sel '2019-01-01','2019-12-01',NULL,Null,'5738,5788',Null,25
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2019-05-31	GRM - Created		

******/

CREATE PROCEDURE [Trade].[Current_Hedge_Positions_Sel]
    (
        @startDate VARCHAR(12)
        , @endDate VARCHAR(12)
        , @clientId INT
        , @divisionId INT = NULL
        , @siteId VARCHAR(MAX) = NULL
        , @groupId INT = NULL
        , @unitId INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , Site_Name VARCHAR(200)
             );



        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Name
             )
        EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id
            @Cbms_Sitegroup_Id = @groupId;


        DECLARE @Hedge_Position_Report TABLE
              (
                  Site_Name VARCHAR(200)
                  , Site_Id INT
                  , RM_Deal_Ticket_Id INT
                  , HEDGE_VOLUME DECIMAL(32, 16)
                  , CONVERSION_FACTOR DECIMAL(32, 16)
                  , Volume_Conversion_Factor DECIMAL(32, 16)
                  , Currency_Type_Id INT
                  , Hedge_Price DECIMAL(32, 16)
                  , Premium DECIMAL(32, 16)
                  , MONTH_IDENTIFIER DATE
                  , Option_Name VARCHAR(200)
              );



        INSERT INTO @Hedge_Position_Report
        SELECT
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
            , ch.Site_Id
            , NULL AS RM_DEAL_TICKET_ID
            , SUM((CASE WHEN frq.Code_Value = 'Monthly' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN dtchvd.Total_Volume
                       WHEN trdact.Code_Value = 'Sell' THEN -dtchvd.Total_Volume
                   END)
                       WHEN frq.Code_Value = 'Daily' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN dtchvd.Total_Volume
                       WHEN trdact.Code_Value = 'Sell' THEN -dtchvd.Total_Volume
                   END) * dd.DAYS_IN_MONTH_NUM
                   END) * consumption.CONVERSION_FACTOR) AS HEDGE_VOLUME
            , ISNULL(conversion.CONVERSION_FACTOR, '1.000') CONVERSION_FACTOR
            , ISNULL(consumption.CONVERSION_FACTOR, '1.000') VOLUME_CONVERSION_FACTOR
            , dt2.Currency_Unit_Id AS CURRENCY_TYPE_ID
            , CAST(NULL AS DECIMAL(28, 3)) AS HEDGE_PRICE
            , -1 PREMIUM
            , dtchvd.Deal_Month MONTH_IDENTIFIER
            , '-1' OPTION_NAME
        FROM
            Trade.Deal_Ticket dt2
            INNER JOIN dbo.Code trdact
                ON dt2.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt2.Deal_Ticket_Frequency_Cd
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Deal_Ticket_Id = dt2.Deal_Ticket_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtchvd.Deal_Month
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = dtchvd.Trade_Price_Id
            LEFT JOIN dbo.RM_CURRENCY_UNIT_CONVERSION conversion
                ON (dt2.Client_Id = conversion.CLIENT_ID)
                   AND  (conversion.CONVERSION_YEAR = DATEPART(yyyy, GETDATE()))
            JOIN Core.Client_Hier ch
                ON dtchvd.Client_Hier_Id = ch.Client_Hier_Id
            JOIN dbo.CONSUMPTION_UNIT_CONVERSION consumption
                ON dtchvd.Uom_Type_Id = consumption.BASE_UNIT_ID
            JOIN dbo.Code c
                ON dt2.Deal_Ticket_Frequency_Cd = c.Code_Id
            INNER JOIN dbo.Code c2
                ON dt2.Deal_Ticket_Type_Cd = c2.Code_Id
            INNER JOIN dbo.ENTITY e
                ON dt2.Hedge_Mode_Type_Id = e.ENTITY_ID
            INNER JOIN dbo.PRICE_INDEX pi
                ON pi.PRICE_INDEX_ID = dt2.Price_Index_Id
            INNER JOIN dbo.ENTITY e2
                ON e2.ENTITY_ID = pi.INDEX_ID
        WHERE
            e2.ENTITY_NAME = 'NYMEX'
            AND (   CONVERT(VARCHAR(12), @startDate, 101) BETWEEN dt2.Hedge_Start_Dt
                                                          AND     dt2.Hedge_End_Dt
                    OR  CONVERT(VARCHAR(12), @endDate, 101) BETWEEN dt2.Hedge_Start_Dt
                                                            AND     dt2.Hedge_End_Dt
                    OR  dt2.Hedge_Start_Dt BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                           AND     CONVERT(VARCHAR(12), @endDate, 101)
                    OR  dt2.Hedge_End_Dt BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                         AND     CONVERT(VARCHAR(12), @endDate, 101))
            AND tp.Trade_Price IS NOT NULL
            AND (   @clientId IS NULL
                    OR  ch.Client_Id = @clientId)
            AND (   @divisionId IS NULL
                    OR  ch.Sitegroup_Id = @divisionId)
            AND (   @siteId IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@siteId, ',') us
                                   WHERE
                                        CAST(us.Segments AS INT) = ch.Client_Hier_Id))
            AND dtchvd.Deal_Month BETWEEN CONVERT(VARCHAR(12), @startDate, 101)
                                  AND     CONVERT(VARCHAR(12), @endDate, 101)
            AND consumption.CONVERTED_UNIT_ID = @unitId
            AND ch.Site_Closed = 0
            AND ch.Site_Not_Managed = 0
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                                    INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                        ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                               WHERE
                                    siteob.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob.Commodity_Id = dt2.Commodity_Id
                                    AND chhc.Include_In_Reports = 1
                                    AND (   chhc.Config_Start_Dt BETWEEN @startDate
                                                                 AND     @endDate
                                            OR  chhc.Config_End_Dt BETWEEN @startDate
                                                                   AND     @endDate
                                            OR  @startDate BETWEEN chhc.Config_Start_Dt
                                                           AND     chhc.Config_End_Dt
                                            OR  @endDate BETWEEN chhc.Config_Start_Dt
                                                         AND     chhc.Config_End_Dt))
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard clntob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                        INNER JOIN Core.Client_Hier clch
                                            ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                   WHERE
                                        clch.Sitegroup_Id = 0
                                        AND clch.Client_Id = ch.Client_Id
                                        AND clntob.Country_Id = ch.Country_Id
                                        AND clntob.Commodity_Id = dt2.Commodity_Id
                                        AND chhc.Include_In_Reports = 1
                                        AND (   chhc.Config_Start_Dt BETWEEN @startDate
                                                                     AND     @endDate
                                                OR  chhc.Config_End_Dt BETWEEN @startDate
                                                                       AND     @endDate
                                                OR  @startDate BETWEEN chhc.Config_Start_Dt
                                                               AND     chhc.Config_End_Dt
                                                OR  @endDate BETWEEN chhc.Config_Start_Dt
                                                             AND     chhc.Config_End_Dt)
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Trade.RM_Client_Hier_Onboard siteob1
                                                           WHERE
                                                                siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                                AND siteob1.Commodity_Id = dt2.Commodity_Id)))
            AND (   @groupId IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , ch.Site_Id
            , dtchvd.Deal_Month
            --, entity1.ENTITY_NAME
            , conversion.CONVERSION_FACTOR
            , consumption.CONVERSION_FACTOR
            , dt2.Currency_Unit_Id
            , trdact.Code_Value
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM;
        WITH Cte_Hedge_Position_Report
        AS (
               SELECT
                    Site_Name
                    , Site_Id
                    , RM_Deal_Ticket_Id
                    , HEDGE_VOLUME
                    , CONVERSION_FACTOR
                    , Volume_Conversion_Factor
                    , Currency_Type_Id
                    , Hedge_Price
                    , Premium
                    , CONVERT(VARCHAR(12), MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                    , Option_Name
                    , Row_Num = DENSE_RANK() OVER (ORDER BY
                                                       Site_Name)
               FROM
                    @Hedge_Position_Report
           )
        SELECT
            Site_Name
            , Site_Id AS Site_Id
            , RM_Deal_Ticket_Id
            , HEDGE_VOLUME AS Total_Volume
            , CONVERSION_FACTOR
            , Volume_Conversion_Factor
            , Currency_Type_Id
            , Hedge_Price
            , Premium
            , MONTH_IDENTIFIER AS Service_Month
            , Option_Name
            , tot.Total AS Total_Rows
        FROM
            Cte_Hedge_Position_Report hpr
            CROSS JOIN
            (   SELECT
                    COUNT(DISTINCT Site_Id) Total
                FROM
                    @Hedge_Position_Report) tot
        WHERE
            hpr.Row_Num BETWEEN @StartIndex
                        AND     @EndIndex;

    END;

GO
GRANT EXECUTE ON  [Trade].[Current_Hedge_Positions_Sel] TO [CBMSApplication]
GO
