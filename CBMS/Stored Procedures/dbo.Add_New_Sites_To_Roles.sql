SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    dbo.Add_Missing_Sites_To_Roles
 
DESCRIPTION: 
		StoreProcedure is used to assign the new Site to the roles that are having Division level access.
 Note: there is a Similar logic procedure i.e.Add_Site_To_Roles. but that will do site at a time.
    
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 Client_Id	INT		Not NULL

 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
	NA
 
 USAGE EXAMPLES:    
------------------------------------------------------------    
    
	DECLARE
      @SIte_List [dbo].[Site_Ids]
     ,@Client_Id INT = 11231

    INSERT  INTO @SIte_List
            ( Site_Id )
            SELECT
                  Site_id
            FROM
                  Core.CLient_Hier ch JOIN dbo.Security_Role_Client_Hier AS srch
				  ON ch.Client_Hier_Id = srch.Client_Hier_Id
            WHERE
                  site_id > 0
                  AND ch.client_id = @Client_Id
                  AND srch.Client_Hier_Id IS NULL

	EXEC dbo.Add_New_Sites_To_Roles @SIte_List
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
	KVK		Vinay K
 
 MODIFICATIONS     
 
 Initials Date   Modification    
------------------------------------------------------------    
 KVK	  10/12/2011 Created

******/
CREATE PROCEDURE [dbo].[Add_New_Sites_To_Roles]
      ( 
       @Site_List [dbo].[Site_Ids] READONLY )
AS 
BEGIN
	
      SET NOCOUNT ON;

      INSERT      INTO dbo.Security_Role_Client_Hier
                  ( 
                   Security_Role_Id
                  ,Client_Hier_Id )
                  SELECT
                        srch.Security_Role_Id
                       ,ch_site.Client_Hier_Id
                  FROM
                        Core.Client_Hier AS ch_site
                        JOIN @SIte_List s
                              ON ch_site.site_id = s.[Site_Id]
                        JOIN Core.Client_Hier AS ch_Div
                              ON ch_Div.Sitegroup_Id = ch_site.Sitegroup_Id
                        JOIN dbo.Security_Role_Client_Hier AS srch
                              ON srch.Client_Hier_Id = ch_Div.Client_Hier_Id
                  WHERE
                        ch_site.site_id > 0
                        AND ch_Div.Site_Id = 0 


END
;
GO
GRANT EXECUTE ON  [dbo].[Add_New_Sites_To_Roles] TO [CBMSApplication]
GO
