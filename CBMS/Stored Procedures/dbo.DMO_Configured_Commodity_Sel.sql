SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Configured_Commodity_Sel
           
DESCRIPTION:             
			To select DMO configured commodities
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Site_Id		INT			NULL
    @Account_Id		INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	EXEC dbo.Client_Hier_DMO_Config_Sel 12066
	EXEC dbo.DMO_Configured_Commodity_Sel 24336,NULL
	
	EXEC dbo.Account_DMO_Config_Sel 70260
	EXEC dbo.DMO_Configured_Commodity_Sel NULL,70260
	
	EXEC dbo.Client_Hier_DMO_Config_Sel 12066
	EXEC dbo.DMO_Configured_Commodity_Sel 24336,NULL
	
	EXEC dbo.Account_DMO_Config_Sel 70260
	EXEC dbo.DMO_Configured_Commodity_Sel NULL,70260
	
	EXEC dbo.Account_DMO_Config_Sel 96047
	EXEC dbo.DMO_Configured_Commodity_Sel 24047,NULL
	
	EXEC dbo.DMO_Configured_Commodity_Sel 27918
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-27	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Configured_Commodity_Sel]
      ( 
       @Site_Id INT = NULL
      ,@Account_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Commodity_Id INT
            ,Commodity_Name VARCHAR(50) )
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Client_Hier_Id INT
           ,@Account_Hier_level_Cd INT
      
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
      FROM
            Core.Client_Hier ch
      WHERE
            ( @Site_Id IS NULL
              OR ch.Site_Id = @Site_Id )
            AND ( @Account_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    Core.Client_Hier_Account cha
                              WHERE
                                    ch.Client_Hier_Id = cha.Client_Hier_Id
                                    AND cha.Account_Id = @Account_Id ) )
            
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND @Site_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Commodity_Id
                  ,Commodity_Name )
                  SELECT
                        NULL AS Client_Hier_DMO_Config_Id
                       ,adc.Commodity_Id
                       ,com.Commodity_Name
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN dbo.Commodity com
                              ON adc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON adc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        ( @Account_Id IS NULL
                          OR adc.Account_Id = @Account_Id )
                        AND ( @Site_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                Core.Client_Hier_Account cha
                                                INNER JOIN Core.Client_Hier ch
                                                      ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                          WHERE
                                                ch.Site_Id = @Site_Id
                                                AND cha.Account_Id = adc.Account_Id ) )
      
      SELECT
            tc.Commodity_Id
           ,tc.Commodity_Name
      FROM
            @Tbl_Config tc
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                         WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND @Sitegroup_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND @Sitegroup_Id IS NOT NULL
                              AND @Site_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = @Site_Id )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Account_Not_Applicable_DMO_Config na
                             WHERE
                              @Account_Id IS NOT NULL
                              AND na.Account_Id = @Account_Id
                              AND na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id )
      GROUP BY
            tc.Commodity_Id
           ,tc.Commodity_Name
      ORDER BY
            CASE WHEN tc.Commodity_Name = 'Electric Power' THEN 1
                 WHEN tc.Commodity_Name = 'Natural Gas' THEN 2
                 ELSE 3
            END
           ,tc.Commodity_Name
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[DMO_Configured_Commodity_Sel] TO [CBMSApplication]
GO
