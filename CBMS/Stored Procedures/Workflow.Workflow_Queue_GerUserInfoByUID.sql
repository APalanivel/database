SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Workflow_Queue_GerUserInfoByUID]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description 
 @UserInfoId INT       
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-2019  Created

******/
CREATE PROCEDURE [Workflow].[Workflow_Queue_GerUserInfoByUID]         
(          
	@UserInfoId INT          
)          
AS          
BEGIN          
               
     SELECT   UI.FIRST_NAME+' '+UI.LAST_NAME as USERNAME,QUEUE_ID FROM  USER_INFO UI WHERE USER_INFO_ID=@UserInfoId      
      
END; 

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_GerUserInfoByUID] TO [CBMSApplication]
GO
