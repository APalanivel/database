SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
 cbms_prod.dbo.Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month
 
 DESCRIPTION:
 
	Procedure used to select utility account count received for the given supplier account and given month.
	  
 INPUT PARAMETERS:    
 Name			DataType	Default Description    
------------------------------------------------------------    
 @Account_id     int                       
 @Service_Month  date                  
 
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------

 EXEC Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month 115134, '4/1/2009'
 EXEC Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month 124734, '2009-05-01'
 EXEC Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month 10053, '2004-06-09'
 EXEC Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month 158106, '03/01/2010'
 

AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
 Initials	Date		Modification
------------------------------------------------------------
 HG			04/06/2010	Created
 
******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month
(
	@Account_Id		Int
	,@Service_Month	Date
)
AS
BEGIN

	SET NOCOUNT ON;

	WITH Cte_Account_Dtl AS
	(
		SELECT
			a.account_id
			,map.METER_ID
			,Supplier_Account_Begin_Dt = CASE
												WHEN MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt)) < a.Supplier_Account_Begin_Dt
													THEN a.Supplier_Account_Begin_Dt
												ELSE MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt))
											END

			, Supplier_Account_End_Dt = CASE
											WHEN MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt)) > a.Supplier_Account_End_Dt
												THEN a.Supplier_Account_End_Dt
											ELSE MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt))
										END
		FROM
			dbo.account a
			JOIN dbo.supplier_account_meter_map map
				on map.account_id = a.account_id
		WHERE
			a.ACCOUNT_ID = @Account_Id
			AND map.is_history = 0
		GROUP BY
			a.account_id
			,map.METER_ID
			, a.Supplier_Account_Begin_Dt
			, a.Supplier_Account_End_Dt
	)
	SELECT
		COUNT(DISTINCT uacc.Account_Id) Site_Count
		,COUNT(DISTINCT cua.Account_Id) Site_Count_Received
	FROM
		Cte_Account_Dtl vas
		JOIN dbo.METER m
			ON m.METER_ID = vas.METER_ID
		JOIN dbo.Account uacc
			ON uacc.ACCOUNT_ID = m.ACCOUNT_ID
		LEFT JOIN dbo.Cost_Usage_Account_Dtl cua
			ON cua.ACCOUNT_ID = uacc.account_id
				AND cua.Service_Month = @Service_Month
	WHERE
		@service_month BETWEEN vas.Supplier_Account_Begin_Dt AND vas.Supplier_Account_End_Dt
		AND uacc.NOT_MANAGED = 0

END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Utility_Account_Count_For_Supplier_Account_By_Month] TO [CBMSApplication]
GO
