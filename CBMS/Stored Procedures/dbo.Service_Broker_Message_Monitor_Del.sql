SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   

	dbo.Service_Broker_Message_Monitor_Del
	
DESCRIPTION:          
	
	This is for rollup message monitor purge process
      
      
INPUT PARAMETERS:          
	Name						DataType	Default		Description          
------------------------------------------------------------          
	@@Number_Of_Days					INT

       
OUTPUT PARAMETERS:          
Name						DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------

-- dbo.Service_Broker_Message_Monitor_Del 30

AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
TP			Anoop  
     

MODIFICATIONS           
Initials	Date			Modification          
------------------------------------------------------------          
TP			2014-01-13		Created

****************/
CREATE PROC [dbo].[Service_Broker_Message_Monitor_Del] 
	( 
		@Number_Of_Days INT 
	)
AS 
BEGIN

      DELETE FROM
            dbo.Service_Broker_Message_Monitor
      WHERE
            DateDiff(DAY, Message_Received_Ts, GetDate()) > @Number_Of_Days
			AND Message_Status ='Completed'
END
;
GO
