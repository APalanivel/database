SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_End_Dt_Upd
           
DESCRIPTION:             
			To update DMO supplier account end date
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id					INT
    @Supplier_Account_End_Dt	DATETIME
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-07	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_End_Dt_Upd]
      ( 
       @Account_Id INT
      ,@Supplier_Account_End_Dt DATETIME )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.ACCOUNT
      SET   
            Supplier_Account_End_Dt = @Supplier_Account_End_Dt
      WHERE
            ACCOUNT_ID = @Account_Id
      UPDATE
            dbo.SUPPLIER_ACCOUNT_METER_MAP
      SET   
            METER_DISASSOCIATION_DATE = @Supplier_Account_End_Dt
      WHERE
            ACCOUNT_ID = @Account_Id
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_End_Dt_Upd] TO [CBMSApplication]
GO
