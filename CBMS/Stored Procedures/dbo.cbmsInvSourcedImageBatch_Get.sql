SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsInvSourcedImageBatch_Get]
	( @MyAccountId int )
AS
BEGIN

	declare @this_id int

	set nocount on

	insert into inv_sourced_image_batch ( batch_date ) values ( getdate() )

	set @this_id = @@IDENTITY

--	set nocount off

	   select inv_sourced_image_batch_id
		, batch_date
	     from inv_sourced_image_batch with (nolock)
	    where inv_sourced_image_batch_id = @this_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageBatch_Get] TO [CBMSApplication]
GO
