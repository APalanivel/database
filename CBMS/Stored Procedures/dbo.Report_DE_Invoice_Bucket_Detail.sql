
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Report_DE_Invoice_Bucket_Detail                     
                          
 DESCRIPTION:      
		  To get the invocie details.                       
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Client_Name						VARCHAR(200)
 @Start_Dt							DATETIME		 NULL
 @End_Dt							DATETIME		 NULL          
                 
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
--------------------------------------------------------------------------------------------------------------- 
 
	BEGIN TRAN
		SELECT * FROM REPTMGR.dbo.Client_Report_Config
		EXEC Report_DE_Invoice_Bucket_Detail
		  @Client_Name = 'Royal Philips|Philips Lighting|New Lumileds'
		SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      

	BEGIN TRAN
		SELECT * FROM REPTMGR.dbo.Client_Report_Config
        EXEC Report_DE_Invoice_Bucket_Detail
            @Client_Name = 'Philips'
            ,@Sitegroup_Name = '1'
           ,@Start_Dt = '2014-03-01'
           ,@End_Dt = '2014-09-01'
		SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      

	SELECT * FROM Client WHERE Client_id = 12344

 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 HG							Harihara Suthan                           
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 HG                     2016-01-08      Created for calling this sproc from some other .net tool to export 
										the output in to FTP/SFTP location.
 HG						2016-03-23		Modified to return the date fields in the format of MM/DD/YYYY HH:MM:SS             
 HG						2016-04-19		Date format changed again to include the time stamp.
 HG						2016-05-18		Since the Philips client is splitted to 3 different client Royal Philips|Philips Lighting|New Lumileds changed the input to considered the Pipe separated client name and getting all the clients data together.		
 HG						2016-08-17		Added optional @Sitegroup_Name parameter to filter the data if sitegroup name is passed 
******/
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Bucket_Detail]
      (
       @Client_Name VARCHAR(MAX)
      ,@Sitegroup_Name VARCHAR(MAX) = NULL
      ,@Start_Dt DATETIME = NULL
      ,@End_Dt DATETIME = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Client_Accounts
            (
             Account_Id INT PRIMARY KEY CLUSTERED );

      CREATE TABLE #Client_Invoice
            (
             CU_Invoice_Id INT PRIMARY KEY CLUSTERED
            ,Currency_Unit VARCHAR(200) );

      CREATE TABLE #Invoice_Charges_Determinants_Dtl
            (
             Account_Id INT
            ,Service_Month DATE
            ,Bucket VARCHAR(255)
            ,Name VARCHAR(200)
            ,Value VARCHAR(50)
            ,Type VARCHAR(50)
            ,Code VARCHAR(10)
            ,Commodity VARCHAR(255)
            ,Unit VARCHAR(200)
            ,Cu_Invoice_Id INT
            ,Commodity_Type_Id INT
            ,Begin_Date DATE
            ,End_Date DATE
            ,Determinant_Charge_Unique_Id INT );      
      CREATE NONCLUSTERED INDEX #ix_#Invoice_Charges_Determinants_Dtl ON #Invoice_Charges_Determinants_Dtl(Account_Id, Commodity_Type_Id);

      DECLARE @Client_List TABLE ( Client_Id INT );
      DECLARE @Sitegroup_List TABLE ( Sitegroup_Id INT );

      DECLARE
            @Last_Run_Ts DATETIME
           ,@Current_Ts DATETIME;	-- Uses the maximum Cu_Invoice Updated time stamp available in Copy database

      SET @Start_Dt = ISNULL(@Start_Dt, DATEADD(yy, -1, GETDATE())); 
      SET @End_Dt = ISNULL(@End_Dt, GETDATE());  

      SELECT
            @Current_Ts = MAX(UPDATED_DATE)
      FROM
            dbo.CU_INVOICE; 

      INSERT      INTO @Client_List
                  ( Client_Id )
                  SELECT
                        cl.CLIENT_ID
                  FROM
                        dbo.ufn_split(@Client_Name, '|') cn
                        INNER JOIN dbo.CLIENT cl
                              ON cl.CLIENT_NAME = cn.Segments
                  GROUP BY
                        cl.CLIENT_ID;

      INSERT      INTO @Sitegroup_List
                  ( Sitegroup_Id )
                  SELECT
                        sg.Sitegroup_Id
                  FROM
                        dbo.ufn_split(@Sitegroup_Name, '|') sn
                        INNER JOIN Core.Client_Hier sg
                              ON sg.Sitegroup_Name = sn.Segments
                  WHERE
                        sg.Site_Id = 0
                        AND sg.Sitegroup_Id > 0
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @Client_List cl
                                     WHERE
                                          cl.Client_Id = sg.Client_Id );

      SELECT
            @Last_Run_Ts = crc.Last_Run_Ts
      FROM
            REPTMGR.dbo.Client_Report_Config crc
      WHERE
            crc.Report_Type = 'Invoice Detail'
            AND EXISTS ( SELECT
                              1
                         FROM
                              @Client_List cl
                         WHERE
                              cl.Client_Id = crc.Client_Id );

      INSERT      INTO #Client_Accounts
                  (Account_Id )
                  SELECT
                        cha.Account_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    @Client_List cl
                                 WHERE
                                    cl.Client_Id = ch.Client_Id )
                        AND ( @Sitegroup_Name IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Sitegroup_Site sgs
                                                INNER JOIN @Sitegroup_List sl
                                                      ON sl.Sitegroup_Id = sgs.Sitegroup_id
                                          WHERE
                                                sgs.Site_id = ch.Site_Id ) )
                  GROUP BY
                        cha.Account_Id;

      INSERT      INTO #Client_Invoice
                  (CU_Invoice_Id
                  ,Currency_Unit )
                  SELECT
                        cism.CU_INVOICE_ID
                       ,ISNULL(cu.CURRENCY_UNIT_NAME, 'USD')
                  FROM
                        #Client_Accounts ca
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                              ON cism.Account_ID = ca.Account_Id
                        INNER JOIN dbo.CU_INVOICE i
                              ON i.CU_INVOICE_ID = cism.CU_INVOICE_ID
                        LEFT JOIN CURRENCY_UNIT cu
                              ON cu.CURRENCY_UNIT_ID = i.CURRENCY_UNIT_ID
                  WHERE
                        cism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                        AND i.UPDATED_DATE BETWEEN @Last_Run_Ts
                                           AND     @Current_Ts
                        AND i.IS_DNT = 0
                        AND i.IS_DUPLICATE = 0
                        AND i.IS_REPORTED = 1
                        AND i.IS_PROCESSED = 1
                  GROUP BY
                        cism.CU_INVOICE_ID
                       ,ISNULL(cu.CURRENCY_UNIT_NAME, 'USD');

      INSERT      INTO #Invoice_Charges_Determinants_Dtl
                  (Account_Id
                  ,Service_Month
                  ,Bucket
                  ,Name
                  ,Value
                  ,Type
                  ,Code
                  ,Commodity
                  ,Unit
                  ,Cu_Invoice_Id
                  ,Commodity_Type_Id
                  ,Begin_Date
                  ,End_Date
                  ,Determinant_Charge_Unique_Id )
                  SELECT
                        cuica.ACCOUNT_ID
                       ,[Service Month] = CASE WHEN smcount.CU_INVOICE_ID IS NOT NULL THEN NULL
                                               ELSE cuism.SERVICE_MONTH
                                          END
                       ,bm.Bucket_Name
                       ,cuic.CHARGE_NAME
                       ,( CASE WHEN NULLIF(cuica.Charge_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cuic.CHARGE_VALUE) ) > 0 THEN STUFF(cuic.CHARGE_VALUE, PATINDEX('%[0-9]-', cuic.CHARGE_VALUE) + 1, 1, '')
                                                                                                                                                                       ELSE cuic.CHARGE_VALUE
                                                                                                                                                                  END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                               ELSE cuica.Charge_Value
                          END ) AS VALUE
                       ,'Charge'
                       ,cuic.CU_DETERMINANT_CODE
                       ,com.Commodity_Name
                       ,cui.Currency_Unit
                       ,cuic.CU_INVOICE_ID
                       ,cuic.COMMODITY_TYPE_ID
                       ,cuism.Begin_Dt
                       ,cuism.End_Dt
                       ,cuic.CU_INVOICE_CHARGE_ID
                  FROM
                        #Client_Invoice cui
                        INNER JOIN CU_INVOICE_SERVICE_MONTH cuism
                              ON cui.CU_Invoice_Id = cuism.CU_INVOICE_ID
                        INNER JOIN CU_INVOICE_CHARGE cuic
                              ON cuic.CU_INVOICE_ID = cui.CU_Invoice_Id
                        INNER JOIN CU_INVOICE_CHARGE_ACCOUNT cuica
                              ON cuica.CU_INVOICE_CHARGE_ID = cuic.CU_INVOICE_CHARGE_ID
                                 AND cuica.ACCOUNT_ID = cuism.Account_ID
                        INNER JOIN Bucket_Master bm
                              ON bm.Bucket_Master_Id = cuic.Bucket_Master_Id
                        INNER JOIN Commodity com
                              ON com.Commodity_Id = cuic.COMMODITY_TYPE_ID
                        LEFT OUTER JOIN ( SELECT
                                                CU_INVOICE_ID
                                          FROM
                                                dbo.CU_INVOICE_SERVICE_MONTH
                                          GROUP BY
                                                CU_INVOICE_ID
                                          HAVING
                                                COUNT(DISTINCT SERVICE_MONTH) > 1 ) smcount
                              ON cui.CU_Invoice_Id = smcount.CU_INVOICE_ID
                  WHERE
                        cuism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                  GROUP BY
                        cuica.ACCOUNT_ID
                       ,bm.Bucket_Name
                       ,cuic.CHARGE_NAME
                       ,cuic.CHARGE_VALUE
                       ,cuic.CU_DETERMINANT_CODE
                       ,com.Commodity_Name
                       ,cui.Currency_Unit
                       ,cuic.CU_INVOICE_ID
                       ,cuic.COMMODITY_TYPE_ID
                       ,cuism.Begin_Dt
                       ,cuism.End_Dt
                       ,CASE WHEN smcount.CU_INVOICE_ID IS NOT NULL THEN NULL
                             ELSE cuism.SERVICE_MONTH
                        END
                       ,CASE WHEN NULLIF(cuica.Charge_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cuic.CHARGE_VALUE) ) > 0 THEN STUFF(cuic.CHARGE_VALUE, PATINDEX('%[0-9]-', cuic.CHARGE_VALUE) + 1, 1, '')
                                                                                                                                                                     ELSE cuic.CHARGE_VALUE
                                                                                                                                                                END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                             ELSE cuica.Charge_Value
                        END
                       ,cuic.CU_INVOICE_CHARGE_ID;

      INSERT      INTO #Invoice_Charges_Determinants_Dtl
                  (Account_Id
                  ,Service_Month
                  ,Bucket
                  ,Name
                  ,Value
                  ,Type
                  ,Code
                  ,Commodity
                  ,Unit
                  ,Cu_Invoice_Id
                  ,Commodity_Type_Id
                  ,Begin_Date
                  ,End_Date
                  ,Determinant_Charge_Unique_Id )
                  SELECT
                        cuica.ACCOUNT_ID
                       ,[Service Month] = CASE WHEN smcount.CU_INVOICE_ID IS NOT NULL THEN NULL
                                               ELSE cuism.SERVICE_MONTH
                                          END
                       ,bm.Bucket_Name AS [Bucket]
                       ,cuic.DETERMINANT_NAME [Name]
                       ,( CASE WHEN NULLIF(cuica.Determinant_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cuic.DETERMINANT_VALUE) ) > 0 THEN STUFF(cuic.DETERMINANT_VALUE, PATINDEX('%[0-9]-', cuic.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                                                                                            ELSE cuic.DETERMINANT_VALUE
                                                                                                                                                                       END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                               ELSE cuica.Determinant_Value
                          END ) AS VALUE
                       ,'Determinant' AS [Type]
                       ,cuic.CU_DETERMINANT_CODE [Code]
                       ,com.Commodity_Name [Commodity]
                       ,e.ENTITY_NAME [Unit]
                       ,cuic.CU_INVOICE_ID
                       ,cuic.COMMODITY_TYPE_ID
                       ,cuism.Begin_Dt
                       ,cuism.End_Dt
                       ,cuic.CU_INVOICE_DETERMINANT_ID
                  FROM
                        #Client_Invoice cui
                        INNER JOIN CU_INVOICE_SERVICE_MONTH cuism
                              ON cui.CU_Invoice_Id = cuism.CU_INVOICE_ID
                        LEFT OUTER JOIN ( SELECT
                                                CU_INVOICE_ID
                                          FROM
                                                dbo.CU_INVOICE_SERVICE_MONTH
                                          GROUP BY
                                                CU_INVOICE_ID
                                          HAVING
                                                COUNT(DISTINCT SERVICE_MONTH) > 1 ) smcount
                              ON cui.CU_Invoice_Id = smcount.CU_INVOICE_ID
                        INNER JOIN CU_INVOICE_DETERMINANT cuic
                              ON cuic.CU_INVOICE_ID = cui.CU_Invoice_Id
                        INNER JOIN CU_INVOICE_DETERMINANT_ACCOUNT cuica
                              ON cuica.CU_INVOICE_DETERMINANT_ID = cuic.CU_INVOICE_DETERMINANT_ID
                                 AND cuica.ACCOUNT_ID = cuism.Account_ID
                        INNER JOIN Bucket_Master bm
                              ON bm.Bucket_Master_Id = cuic.Bucket_Master_Id
                        INNER JOIN Commodity com
                              ON com.Commodity_Id = cuic.COMMODITY_TYPE_ID
                        LEFT JOIN ( dbo.CONSUMPTION_UNIT_CONVERSION cuc
                                    LEFT JOIN ENTITY e
                                          ON e.ENTITY_ID = cuc.BASE_UNIT_ID )
                              ON cuc.BASE_UNIT_ID = cuic.UNIT_OF_MEASURE_TYPE_ID
                  WHERE
                        cuism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                  GROUP BY
                        cuica.ACCOUNT_ID
                       ,bm.Bucket_Name
                       ,cuic.DETERMINANT_NAME
                       ,cuic.DETERMINANT_VALUE
                       ,cuic.CU_DETERMINANT_CODE
                       ,com.Commodity_Name
                       ,e.ENTITY_NAME
                       ,cuic.CU_INVOICE_ID
                       ,cuic.COMMODITY_TYPE_ID
                       ,cuism.Begin_Dt
                       ,cuism.End_Dt
                       ,CASE WHEN smcount.CU_INVOICE_ID IS NOT NULL THEN NULL
                             ELSE cuism.SERVICE_MONTH
                        END
                       ,CASE WHEN NULLIF(cuica.Determinant_Expression, '') IS NULL THEN ( CONVERT(DECIMAL(28, 10), REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(( CASE WHEN ( PATINDEX('%[0-9]-', cuic.DETERMINANT_VALUE) ) > 0 THEN STUFF(cuic.DETERMINANT_VALUE, PATINDEX('%[0-9]-', cuic.DETERMINANT_VALUE) + 1, 1, '')
                                                                                                                                                                          ELSE cuic.DETERMINANT_VALUE
                                                                                                                                                                     END ), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')) )
                             ELSE cuica.Determinant_Value
                        END
                       ,cuic.CU_INVOICE_DETERMINANT_ID;

      SELECT
            ch.Client_Name [Client]
           ,ch.Sitegroup_Name [Division]
           ,ch.Site_name [Site]
           ,ch.City [City]
           ,ch.State_Name [State]
           ,ch.Site_Address_Line1 [Primary Address]
           ,ch.Site_Address_Line2 [Address 2]
           ,ca.Account_Vendor_Name [Vendor]
           ,ca.Account_Number [Account number]
           ,ca.Account_Type [Account Type]
           ,[Commodity]
           ,Cu_Invoice_Id [Invoice ID]
           ,CONVERT(VARCHAR(10), Service_Month, 101) + ' 12:00:00 AM' [Service Month]
           ,CONVERT(VARCHAR(10), icd.Begin_Date, 101) + ' 12:00:00 AM' [Invoice begin date]
           ,CONVERT(VARCHAR(10), icd.End_Date, 101) + ' 12:00:00 AM' [Invoice end date]
           ,[Type]
           ,Bucket
           ,Name
           ,Value
           ,[Unit]
      FROM
            Core.Client_Hier_Account ca
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = ca.Client_Hier_Id
            INNER JOIN #Invoice_Charges_Determinants_Dtl icd
                  ON icd.Account_Id = ca.Account_Id
                     AND icd.Commodity_Type_Id = ca.Commodity_Id
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @Client_List cl
                     WHERE
                        cl.Client_Id = ch.Client_Id )
      GROUP BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,ch.City
           ,ch.State_Name
           ,ch.Site_Address_Line1
           ,ch.Site_Address_Line2
           ,ca.Account_Vendor_Name
           ,ca.Account_Number
           ,ca.Account_Type
           ,Commodity
           ,Cu_Invoice_Id
           ,Service_Month
           ,Begin_Date
           ,End_Date
           ,Type
           ,Bucket
           ,Name
           ,Value
           ,Unit
           ,icd.Determinant_Charge_Unique_Id
      ORDER BY
            ch.Client_Name
           ,ch.Site_name
           ,ca.Account_Number
           ,[Service Month];

      UPDATE
            cnf
      SET
            Last_Run_Ts = @Current_Ts
      FROM
            REPTMGR.dbo.Client_Report_Config cnf
      WHERE
            cnf.Report_Type = 'Invoice Detail'
            AND EXISTS ( SELECT
                              1
                         FROM
                              @Client_List cl
                         WHERE
                              cl.Client_Id = cnf.Client_Id );

      DROP TABLE #Client_Accounts;
      DROP TABLE #Client_Invoice;
      DROP TABLE #Invoice_Charges_Determinants_Dtl;

END;
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Bucket_Detail] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Bucket_Detail] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Bucket_Detail] TO [CBMSReports]
GO
