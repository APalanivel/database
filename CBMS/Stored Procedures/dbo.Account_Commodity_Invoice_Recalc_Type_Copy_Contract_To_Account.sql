SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account

DESCRIPTION:

INPUT PARAMETERS:
Name						DataType		Default			Description
-------------------------------------------------------------------------------
 @Account_Id				INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
USE CBMS

select Max(Account_Commodity_Invoice_Recalc_Type_Id)
 from dbo.Account_Commodity_Invoice_Recalc_Type  --> 1692975
 select top 1 *
 from dbo.Account_Commodity_Invoice_Recalc_Type  order by Account_Commodity_Invoice_Recalc_Type_Id desc 

Begin Tran 
EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account
    @Account_Id = 1
	,@Contract_Id = 1
	, @User_Info_Id =49

ROllback tran

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials		Date				Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract. 
	NR				2020-02-26		MAINT-9935 - Added OCT rules also  When new accounts are created they inheriting the Contract Level ValCons.

******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Valcon_Account_Config_Type_cd INT
            , @Contract_Source_Cd INT
            , @Valcon_Source_Type_Cd INT
            , @Valcon_Contract_Config_Id_Exists BIT = 0
            , @Account_Recalc_Exist BIT = 0
            , @Contract_Recalc_Exist BIT = 0;
        SELECT
            @Valcon_Account_Config_Type_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Supplier Account'
            AND cs.Codeset_Name = 'Valcon Config Type';

        SELECT
            @Valcon_Source_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Contract'
            AND cs.Codeset_Name = 'Valcon Source Type';

        SELECT  TOP 1
                @Valcon_Contract_Config_Id_Exists = 1
        FROM
            dbo.Valcon_Contract_Config vcc
        WHERE
            vcc.Contract_Id = @Contract_Id
            AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_cd
            AND vcc.Is_Config_Complete = 1;

        SELECT  TOP 1
                @Account_Recalc_Exist = 1
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            acirt.Account_Id = @Account_Id;

        SELECT  TOP 1
                @Contract_Recalc_Exist = 1
        FROM
            dbo.Contract_Recalc_Config crc
        WHERE
            crc.Contract_Id = @Contract_Id
            AND crc.Is_Consolidated_Contract_Config = 0;


        IF (   @Account_Recalc_Exist = 0
               AND  @Contract_Recalc_Exist = 1
               AND  @Valcon_Contract_Config_Id_Exists = 1)
            BEGIN

                EXEC dbo.Valcon_Account_Config_Ins_Upd
                    @Account_Id = @Account_Id
                    , @Contract_Id = @Contract_Id
                    , @Source_Cd = @Valcon_Source_Type_Cd
                    , @Is_Config_Complete = 1
                    , @Comment_Id = NULL
                    , @User_Info_Id = @User_Info_Id;


                INSERT INTO dbo.Account_Commodity_Invoice_Recalc_Type
                     (
                         Account_Id
                         , Commodity_ID
                         , Start_Dt
                         , End_Dt
                         , Invoice_Recalc_Type_Cd
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                         , Determinant_Source_Cd
                         , Source_Cd
                     )
                SELECT
                    @Account_Id
                    , crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , NULL
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                    , crc.Determinant_Source_Cd
                    , @Valcon_Source_Type_Cd
                FROM
                    dbo.Contract_Recalc_Config crc
                WHERE
                    crc.Is_Consolidated_Contract_Config = 0
                    AND crc.Contract_Id = @Contract_Id
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acr
                                       WHERE
                                            acr.Account_Id = @Account_Id
                                            AND acr.Commodity_ID = crc.Commodity_Id
                                            AND acr.Start_Dt = crc.Start_Dt
                                            AND ISNULL(acr.End_Dt, '9999-12-31') = ISNULL(crc.End_Dt, '9999-12-31'))
                GROUP BY
                    crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Determinant_Source_Cd;



                INSERT INTO dbo.Account_Outside_Contract_Term_Config
                     (
                         Account_Id
                         , Contract_Id
                         , OCT_Parameter_Cd
                         , OCT_Tolerance_Date_Cd
                         , OCT_Dt_Range_Cd
                         , Config_Start_Dt
                         , Config_End_Dt
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                SELECT
                    @Account_Id
                    , @Contract_Id
                    , coctc.OCT_Parameter_Cd
                    , coctc.OCT_Tolerance_Date_Cd
                    , coctc.OCT_Dt_Range_Cd
                    , coctc.Config_Start_Dt
                    , coctc.Config_End_Dt
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                FROM
                    dbo.Contract_Outside_Contract_Term_Config coctc
                    INNER JOIN dbo.Valcon_Contract_Config vcc
                        ON vcc.Valcon_Contract_Config_Id = coctc.Valcon_Contract_Config_Id
                WHERE
                    vcc.Contract_Id = @Contract_Id
                    AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_cd
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Outside_Contract_Term_Config aoc
                                       WHERE
                                            aoc.Account_Id = @Account_Id
                                            AND aoc.Contract_Id = @Contract_Id
                                            AND aoc.OCT_Parameter_Cd = coctc.OCT_Parameter_Cd);




            END;





    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account] TO [CBMSApplication]
GO
