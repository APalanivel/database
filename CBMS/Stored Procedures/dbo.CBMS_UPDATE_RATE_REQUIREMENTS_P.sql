SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME: dbo.[CBMS_UPDATE_RATE_REQUIREMENTS_P]      
      
      
DESCRIPTION: Updates information rate information      
      
INPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------------          
@rate_name              varchar(200),      
@rate_requirements      varchar(4000),      
@rate_id                int      
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
--EXEC [dbo].[CBMS_UPDATE_RATE_REQUIREMENTS_P]       
--@rate_name ='QBC120',  -- ORIGINAL VALUE = 'QBC12'      
--@rate_requirements ='Developed for the Quebecor, Fairfield, PA site. ',      
--@rate_id = 1      
      
      
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
  DR Deana Ritter      
  SC Sreenivasulu Cheerala    
MODIFICATIONS      
      
 Initials Date  Modification      
------------------------------------------------------------      
 DR     08/04/2009    Removed Linked Server Updates      
 DMR    09/10/2010 Modified for Quoted_Identifier      
 SC     2020-05-15 Added @Budget_Calc_Type_Id as a input parameter.   
******/
CREATE PROCEDURE [dbo].[CBMS_UPDATE_RATE_REQUIREMENTS_P]
    @rate_name VARCHAR(200)
    , @rate_requirements VARCHAR(4000)
    , @rate_id INT
    , @Budget_Calc_Type_Id INT
AS
    UPDATE
        RATE
    SET
        RATE_NAME = @rate_name
        , RATE_REQUIREMENTS = @rate_requirements
        , Budget_Calc_Type_Cd = @Budget_Calc_Type_Id
    WHERE
        RATE_ID = @rate_id;

GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_RATE_REQUIREMENTS_P] TO [CBMSApplication]
GO
