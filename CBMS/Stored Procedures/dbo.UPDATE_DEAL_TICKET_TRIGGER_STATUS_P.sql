SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_TRIGGER_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@triggerStatusType	int       	          	
	@triggerStatusName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE dbo.UPDATE_DEAL_TICKET_TRIGGER_STATUS_P  

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int, 
@triggerStatusType int, 
@triggerStatusName varchar(200)

AS
set nocount on
	DECLARE @triggerStatusId int
	DECLARE @finalStatus varchar(200)
	DECLARE @statusCheck int
	DECLARE @cancelCheck int
	
	SELECT @statusCheck = COUNT(1)
	FROM 
		RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts, 
		ENTITY e
	WHERE 
		rmdtts.RM_DEAL_TICKET_ID = @dealTicketId AND 
		rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID AND 
		e.ENTITY_NAME IN ('CLIENT CONFIRMATION', 'COUNTERPARTY CONFIRMATION')
		

	SELECT @cancelCheck = COUNT(1)
	FROM 
		RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts, 
		ENTITY e
	WHERE 
		rmdtts.RM_DEAL_TICKET_ID = @dealTicketId AND 
		rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID AND 
		e.ENTITY_NAME IN ('CANCELLED')

	--default trigger status
	SELECT @finalStatus = @triggerStatusName

	--change the trigger status based on following conditions
	IF @cancelCheck > 0
				SELECT @finalStatus = 'Cancelled'
	ELSE 
		IF @statusCheck = 1 
			BEGIN
				SELECT @finalStatus = 'Fixed'
			END
		ELSE IF @statusCheck = 2 
			BEGIN
				SELECT @finalStatus = 'Closed'
			END

	SELECT @triggerStatusId = (SELECT ENTITY_ID FROM ENTITY 
		WHERE 	ENTITY_TYPE = @triggerStatusType AND 
			ENTITY_NAME = @finalStatus)
	
	UPDATE 
		RM_DEAL_TICKET_DETAILS 
	SET 
		TRIGGER_STATUS_TYPE_ID = @triggerStatusId 
	WHERE 
		RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_TRIGGER_STATUS_P] TO [CBMSApplication]
GO
