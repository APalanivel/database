SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FORECAST_VOLUME_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@siteId        	int       	          	
	@month_identifier	datetime  	          	
	@forecastYear  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.GET_FORECAST_VOLUME_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@siteId int,
@month_identifier datetime,
--@asOfDate datetime,
@forecastYear int


 AS
set nocount on
select   
	details.volume , volumeType.entity_name
from  
	rm_forecast_volume_details details(NOLOCK), site sit(NOLOCK),entity volumeType WITH (NOLOCK)
where
	volumeType.entity_id = details.volume_type_id
	and details.site_id = sit.site_id
	and sit.site_id = @siteId
	and details.month_identifier= @month_identifier
	and details.rm_forecast_volume_id =
 					(select rm_forecast_volume_id from rm_forecast_volume WITH (NOLOCK)
				 	 where  forecast_year=@forecastYear 
						--and forecast_as_of_date= @asOfDate 
						and client_id = @clientId)
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
