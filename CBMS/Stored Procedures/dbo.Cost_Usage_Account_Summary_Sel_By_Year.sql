SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	 dbo.Cost_Usage_Account_Summary_Sel_By_Year
	 
DESCRIPTION:  
	This procedure accepts @bucket_list as a TVP and returns the bucket value 
	   and invoice participation details for the given account id and report year.
	 
INPUT PARAMETERS:  
    Name			    DataType		    Default Description  
------------------------------------------------------------  
	@Client_Hier_Id		INT
    @Account_Id			INT
    @Report_Year	    INT
    @Bucket_List	    Cost_Usage_Bucket		  READONLY - Table Value Parameter

 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------ 

    DECLARE @Bucket_List1 AS Cost_Usage_Bucket

    INSERT INTO @Bucket_List1
    VALUES (290, 'Total Cost','Charge', 3)
	   ,(290, 'Total Usage', 'Determinant' ,12)
    EXECUTE dbo.Cost_Usage_Account_Summary_Sel_By_Year 12592, 38009, 2011, @Bucket_List1



	DECLARE @Bucket_List1 AS Cost_Usage_Bucket

	INSERT      INTO @Bucket_List1
	VALUES
				( 291, 'Total Cost', 'Charge', 3 )
	,           ( 291, 'Total Usage', 'Determinant', 25 )


	EXEC Cost_Usage_Account_Summary_Sel_By_Year 
		  10917
		 ,24208
		 ,2004
		 ,@Bucket_List1

	EXEC Cost_Usage_Account_Summary_Sel_By_Year 
		  284798
		 ,24208
		 ,2004
		 ,@Bucket_List1

    
    SELECT * FROM Core.Client_Hier_Account WHERE Account_id = 24208

 AUTHOR INITIALS:  
 Initials		Name  
------------------------------------------------------------  
 AP			Athmaram Pabbathi
  
 MODIFICATIONS
 Initials	Date		Modification
------------------------------------------------------------
 AP		09/28/2011		Created
 HG		10/03/2011		Modifed the query to take the default_uom_type_id in Bucket_Master table if no UOM id passed in @Bucket_List for determinant type buckets
 AP		03/19/2012		Added Data_Source_Code in the result set
 HG		2012-05-18		Client_hier_id added as a parameter.
 ******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Summary_Sel_By_Year
      ( 
      @Client_Hier_Id INT
      ,@Account_Id INT
      ,@Report_Year INT
      ,@Bucket_List AS dbo.Cost_Usage_Bucket READONLY )
AS 
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE
            @Currency_Group_id INT
           ,@Client_Id INT
           ,@Client_Fiscal_Offset INT

      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT
            ,PRIMARY KEY CLUSTERED ( Service_Month ) )
            

      SELECT TOP 1
            @Client_Id = CH.Client_Id
           ,@Currency_Group_id = CH.Client_Currency_Group_Id
           ,@Client_Fiscal_Offset = ch.Client_Fiscal_Offset
      FROM
            Core.Client_Hier CH
            INNER JOIN Core.Client_Hier_Account CHA
                  ON CHA.Client_Hier_Id = CH.Client_Hier_Id
      WHERE
            CHA.Account_Id = @Account_Id

	/* Load the Months into table variable*/
      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Number )
                  SELECT
                        dd.Date_D AS Service_Month
                       ,row_number() OVER ( ORDER BY dd.Date_D ) Month_Number
                  FROM
                        meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN dateadd(m, @Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))
                                  AND     dateadd(mm, 11, dateadd(m, @Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))) ;
                                  

      WITH  Cte_SiteResult
              AS ( SELECT
                        CUAD.Account_Id
                       ,CUAD.Service_Month
                       ,BM.Bucket_Name
                       ,bkt_cd.Code_Value AS Bucket_Type
                       ,sum(case WHEN bkt_cd.Code_Value = 'Determinant' THEN CUAD.Bucket_Value * UC.Conversion_Factor
                                 WHEN bkt_cd.Code_Value = 'Charge' THEN CUAD.Bucket_Value * CC.Conversion_Factor
                                 ELSE 0
                            END) AS Bucket_Value
                       ,COM.Commodity_Name
                       ,dsc.Code_Value AS Data_Source_Code
                   FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN @Service_Month sm
                              ON sm.Service_Month = cuad.Service_Month
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = CUAD.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        INNER JOIN @Bucket_List BM_Id
                              ON bm_id.Commodity_Id = bm.Commodity_Id
                                 AND BM_Id.Bucket_Name = BM.Bucket_Name
                                 AND bm_id.Bucket_Type = bkt_cd.Code_Value
                        INNER JOIN dbo.Commodity COM
                              ON COM.Commodity_Id = BM.Commodity_Id
                        INNER JOIN dbo.Code dsc
                              ON dsc.Code_Id = cuad.Data_Source_Cd
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_Id = cuad.UOM_Type_Id
                                 AND uc.Converted_Unit_Id = isnull(BM_Id.Bucket_Uom_Id, bm.Default_Uom_Type_Id)
                                 AND bm_id.Bucket_Type = 'Determinant'
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON cc.Currency_Group_Id = @Currency_Group_Id
                                 AND cc.Base_Unit_Id = cuad.Currency_Unit_Id
                                 AND cc.Converted_Unit_Id = bm_id.Bucket_Uom_Id
                                 AND bm_id.Bucket_Type = 'Charge'
                                 AND cc.Conversion_Date = cuad.Service_Month
                   WHERE
                        CUAD.Account_Id = @Account_Id
                        AND cuad.Client_Hier_Id = @Client_Hier_Id
                   GROUP BY
                        CUAD.Account_Id
                       ,CUAD.Service_Month
                       ,BM.Bucket_Name
                       ,bkt_cd.Code_Value
                       ,COM.Commodity_Name
                       ,dsc.Code_Value)
            SELECT
                  fy.Service_month
                 ,fy.Month_Number
                 ,x.Commodity_Name
                 ,x.Bucket_Name
                 ,X.Bucket_Type
                 ,x.Bucket_Value
                 ,x.Data_Source_Code
                 ,isnull(max(case WHEN ip.Is_Expected = 1
                                       AND ip.Is_Received = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Complete
                 ,isnull(max(convert(INT, ip.Is_Received)), 0) AS Is_published
                 ,isnull(max(case WHEN ip.Recalc_Under_Review = 1
                                       OR ip.Variance_Under_Review = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Under_Review
            FROM
                  @Service_Month fy
                  LEFT OUTER JOIN Cte_SiteResult x
                        ON x.service_month = fy.Service_Month
                  LEFT JOIN dbo.Invoice_Participation ip
                        ON ip.Account_Id = x.Account_Id
                           AND ip.Service_Month = x.Service_Month
            GROUP BY
                  fy.Service_month
                 ,fy.Month_Number
                 ,x.Commodity_Name
                 ,x.Bucket_Name
                 ,X.Bucket_Type
                 ,x.Bucket_Value
                 ,x.Data_Source_Code
            ORDER BY
                  fy.Service_Month


END

;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Summary_Sel_By_Year] TO [CBMSApplication]
GO
