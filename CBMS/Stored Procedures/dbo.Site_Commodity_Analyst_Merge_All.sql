SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
      dbo.Site_Commodity_Analyst_Merge_All  
     
 DESCRIPTION:     
      Updates the Analyst for Site level. created to use for FIll all dropdown  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
  @Client_Id  INT   
  @Sitegroup_Id  INT   NULL  
  @Site_Id   INT   NULL  
  @Commodity_Id  INT   
  @Analyst_User_Info_Id INT     
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
    Begin TRAN  
 EXEC Site_Commodity_Analyst_Merge_All 235,NULL,NULL,290,31443  
 End tran  
   
  Begin TRAN  
 EXEC Site_Commodity_Analyst_Merge_All 10866,1450,NULL,290,150  
 End tran  
   
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------   
 AKR  Ashok Kumar Raju  
   
  
 MODIFICATIONS     
 Initials Date  Modification  
------------------------------------------------------------  
 AKR        2012-09-28  Created for POCO  
  
******/  
  
CREATE  PROCEDURE dbo.Site_Commodity_Analyst_Merge_All
      ( 
       @Client_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Commodity_Id INT
      ,@Analyst_User_Info_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON  
        
      DECLARE
            @Client_Analyst INT
           ,@Commodity_Service_Cd INT  
      CREATE TABLE #Site_Analyst_List
            ( 
             Site_Id INT
            ,Site_Analyst_User_Info_Id INT )  
     
      SELECT
            @Commodity_Service_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice'  
              
      SELECT
            @Client_Analyst = isnull(cca.Analyst_User_Info_Id, 0)
      FROM
            core.Client_Commodity cc
            LEFT JOIN dbo.Client_Commodity_Analyst cca
                  ON cc.Client_Commodity_Id = cca.Client_Commodity_Id
      WHERE
            cc.Client_Id = @Client_Id
            AND cc.Commodity_Id = @Commodity_Id
            AND cc.Commodity_Service_Cd = @Commodity_Service_Cd  
              
  
      INSERT      INTO #Site_Analyst_List
                  ( 
                   Site_Id
                  ,Site_Analyst_User_Info_Id )
                  SELECT
                        ss.Site_Id
                       ,sca.Analyst_User_Info_Id Site_Analyst_User_Info_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Commodity cc
                              ON ch.Client_Id = cc.Client_Id
                                 AND cc.Commodity_Id = @Commodity_Id
                                 AND cc.Commodity_Service_Cd = @Commodity_Service_Cd
                        INNER JOIN Core.Client_Hier ss
                              ON ch.Sitegroup_Id = ss.Sitegroup_id
                                 AND ss.site_id > 0
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON ss.Site_id = sca.Site_Id
                                 AND sca.Commodity_Id = @Commodity_Id
                  WHERE
                        ( ch.CLIENT_ID = @client_id )
                        AND ( @Sitegroup_Id IS NULL
                              OR ch.Sitegroup_Id = @Sitegroup_Id )
                        AND ( @Site_Id IS NULL
                              OR ss.Site_Id = @Site_Id )
                        AND ch.Site_Id = 0
                        AND ch.Sitegroup_Id > 0  
                        
      BEGIN TRY                
            BEGIN TRAN  

                        
            DELETE
                  aca
            FROM
                  dbo.Account_Commodity_Analyst aca
                  INNER JOIN dbo.ACCOUNT acc
                        ON aca.Account_Id = acc.ACCOUNT_ID
                  INNER JOIN #Site_Analyst_List sal
                        ON acc.Site_Id = sal.Site_Id
                           AND aca.Commodity_Id = @Commodity_Id     
                                
            IF ( @Analyst_User_Info_Id = @Client_Analyst
                 OR @Analyst_User_Info_Id IS NULL ) 
                  BEGIN  
    
                        DELETE
                              sca
                        FROM
                              dbo.Site_Commodity_Analyst sca
                              INNER JOIN #Site_Analyst_List sal
                                    ON sca.Site_Id = sal.Site_Id
                                       AND sca.Commodity_Id = @Commodity_Id  
     
                  END  
   
            ELSE 
                  BEGIN  
     
  
                        MERGE INTO dbo.Site_Commodity_Analyst AS tgt
                              USING 
                                    ( SELECT
                                          Site_Id
                                         ,Site_Analyst_User_Info_Id
                                      FROM
                                          #Site_Analyst_List ) AS src
                              ON tgt.Site_Id = src.Site_Id
                                    AND tgt.Commodity_Id = @Commodity_Id
                              WHEN MATCHED 
                                    THEN  
       UPDATE                           SET
                                          tgt.Analyst_User_Info_Id = @Analyst_User_Info_Id
                              WHEN NOT MATCHED 
                                    THEN  
     
    INSERT
                                          ( 
                                           Site_Id
                                          ,Commodity_Id
                                          ,Analyst_User_Info_Id )
                                        VALUES
                                          ( 
                                           src.Site_Id
                                          ,@Commodity_Id
                                          ,@Analyst_User_Info_Id ) ;  
                  END    
            

            COMMIT TRAN                
      END TRY                
      BEGIN CATCH                
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                
                
            EXEC dbo.usp_RethrowError                
                
      END CATCH                
      
      
     
END  
;
GO
GRANT EXECUTE ON  [dbo].[Site_Commodity_Analyst_Merge_All] TO [CBMSApplication]
GO
