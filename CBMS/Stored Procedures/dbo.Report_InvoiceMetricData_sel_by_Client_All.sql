
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
          
  dbo.Report_InvoiceMetricData_sel_by_Client_All          
 DESCRIPTION:  To fetch Invoice Event Details for Client\Clients          
 INPUT PARAMETERS:          
Name				DataType		Default			Description          
------------------------------------------------------------          
@Client_Id_List		VARCHAR(MAX)          
@begindate			DATETIME          
@enddate			DATETIME  

OUTPUT PARAMETERS:          
Name				DataType		Default			Description          
------------------------------------------------------------          
 USAGE EXAMPLES:          
------------------------------------------------------------         
EXEC dbo.Report_InvoiceMetricData_sel_by_Client_All '11017','2010-01-16','2010-02-01'             
EXEC dbo.Report_InvoiceMetricData_sel_by_Client_All '-1','2010-01-01','2010-02-01'         
EXEC dbo.Report_InvoiceMetricData_sel_by_Client_All '-1,11017','2010-01-01','2010-02-01' 
            
 AUTHOR INITIALS:          
Initials	Name          
-----------------------------------------------------------          
SSR			Sharad Srivastava       
CPE			Chaitanya Panduga Eshwar         
AKR			Ashok Kumar Raju 
NR			Narayana Reddy                   
     
      
MODIFICATIONS           
Initials	Date			Modification          
------------------------------------------------------------          
SSR         06/14/2010		Created          
SSR			06/21/2010		Renamed parameter @Client_name_sel to @Client_Id_list          
							passing Client_id_list to ufn_split function instead of Client_name          
CPE			08/05/2011		Made changes as per Issue# REPTMGR-24          
CPE			09/14/2011		Made changes as per Issue# REPTMGR-34 and 35          
CPE			2014-01-27		Optimized the script for performance                
AKR			2014-02-14		Modified the code to run for all clients and past 3 months when NUll value is passed for all the Parameters   
AKR			2014-09-02		Modified the code to include Invoice Type         
AKR			2014-10-07		Modified the sproc for Failed Recalc for Utility Accounts  
NR			2016-01-20		MAINT-4737 Replaced Invoice_Type_Cd with Meter_Read_Type.                    

******/          
          
CREATE PROCEDURE [dbo].[Report_InvoiceMetricData_sel_by_Client_All]
      (
       @Client_Id_List VARCHAR(MAX) = NULL
      ,@begindate DATETIME = NULL
      ,@enddate DATETIME = NULL )
AS
BEGIN          
          
      SET NOCOUNT ON;          
              
      IF 1 = 2
            BEGIN          
                  SELECT
                        CAST(NULL AS INT) AS 'CuInvoiceID'
                       ,CAST(NULL AS VARCHAR(200)) AS 'Client Name'
                       ,CAST(NULL AS DATETIME) AS 'InsertEventDate'
                       ,CAST(NULL AS VARCHAR(4000)) AS 'Inserted'
                       ,CAST(NULL AS VARCHAR(30)) AS 'USERNAME'
                       ,CAST(NULL AS VARCHAR(4000)) AS 'ResolvedToMonth'
                       ,CAST(NULL AS DATETIME) AS 'ResolvedToMonthEventDate'
                       ,CAST(NULL AS VARCHAR(3)) AS 'IsProcessed'
                       ,CAST(NULL AS VARCHAR(3)) AS 'IS_REPORTED'
                       ,CAST(NULL AS VARCHAR(3)) AS 'IsManual'
                       ,CAST(NULL AS VARCHAR(4000)) AS 'ClosedManual'
                       ,CAST(NULL AS DATETIME) AS 'ClosedManualDate'
                       ,CAST(NULL AS DATETIME) AS 'OverallExceptionOpen'
                       ,CAST(NULL AS DATETIME) AS 'ExceptionClosed'
                       ,CAST(NULL AS VARCHAR(3)) AS 'IsClosed'
                       ,CAST(NULL AS INT) AS 'Queue_ID'
                       ,CAST(NULL AS VARCHAR(50)) AS 'Account Number'
                       ,CAST(NULL AS VARCHAR(200)) AS 'Site'
                       ,CAST(NULL AS VARCHAR(200)) AS 'City'
                       ,CAST(NULL AS VARCHAR(200)) AS 'State'
                       ,CAST(NULL AS VARCHAR(200)) AS 'Country'
                       ,CAST(NULL AS VARCHAR(200)) AS 'Vendor'
                       ,CAST(NULL AS VARCHAR(200)) AS 'UBM'
                       ,CAST(NULL AS VARCHAR(MAX)) AS 'AllExceptions'
                       ,CAST(NULL AS VARCHAR(MAX)) AS 'AllUsers'
                       ,CAST(NULL AS VARCHAR(MAX)) AS 'Meter_Read_Type';           
            
            END;            
            
   
              
              
      CREATE TABLE #ClientIds
            (
             Client_Id INT PRIMARY KEY );          
                     
        
      IF @Client_Id_List IS NOT NULL
            BEGIN          
                  INSERT      #ClientIds
                              SELECT
                                    Segments
                              FROM
                                    dbo.ufn_split(@Client_Id_List, ',');          
            END;        
      ELSE
            BEGIN        
                  INSERT      #ClientIds
                              SELECT
                                    CLIENT_ID
                              FROM
                                    ( SELECT
                                          -1 CLIENT_ID
                                      UNION ALL
                                      SELECT
                                          CLIENT_ID
                                      FROM
                                          CLIENT ) k;        
            END;        
                    
      SELECT
            @enddate = ISNULL(@enddate, CAST(DATEADD(DAY, -DATEPART(DAY, GETDATE()) + 1, GETDATE()) AS DATE));        
              
              
      SELECT
            @begindate = ISNULL(@begindate, DATEADD(MM, -3, @enddate));        
                    
      CREATE TABLE #CU_INVOICE_EVENT_BASE
            (
             CU_INVOICE_ID INT
            ,EVENT_DATE DATETIME
            ,EVENT_BY_ID INT
            ,EVENT_DESCRIPTION VARCHAR(4000) );          
          
      CREATE CLUSTERED INDEX CIX_CU_INVOICE_EVENT_BASE ON #CU_INVOICE_EVENT_BASE(CU_INVOICE_ID);          
          
      CREATE TABLE #CU_INVOICE_EVENT
            (
             CU_INVOICE_ID INT NOT NULL
            ,EVENT_DATE DATETIME
            ,EVENT_BY_ID INT
            ,EVENT_DESCRIPTION VARCHAR(4000)
            ,UBM VARCHAR(200)
            ,IS_PROCESSED BIT
            ,IS_REPORTED BIT
            ,IS_MANUAL BIT
            ,CLIENT_ID INT
            ,Meter_Read_Type VARCHAR(25) );           
          
      CREATE TABLE #Cu_Invoice_Event_Whole
            (
             CU_INVOICE_ID INT
            ,EVENT_DESCRIPTION VARCHAR(4000)
            ,EVENT_DATE DATETIME
            ,Flag BIT );          
          
      CREATE CLUSTERED INDEX CIX_CU_INVOICE_EVENT_Whole ON #Cu_Invoice_Event_Whole(CU_INVOICE_ID);          
          
      CREATE TABLE #CU_Invoice_Accounts
            (
             CU_INVOICE_ID INT
            ,Account_Number VARCHAR(50)
            ,Account_Vendor_Name VARCHAR(200)
            ,Client_Hier_Id INT );          
          
      CREATE CLUSTERED INDEX CIX_CU_INVOICE_Accounts ON #CU_Invoice_Accounts(CU_INVOICE_ID, Client_Hier_Id);          
          
      CREATE TABLE #CUException
            (
             CU_INVOICE_ID INT
            ,CU_EXCEPTION_ID INT
            ,OPENED_DATE DATETIME
            ,CLOSED_DATE DATETIME
            ,IS_CLOSED BIT
            ,QUEUE_ID INT );          
          
      CREATE CLUSTERED INDEX CIX_CUEXCEPTION ON #CUException(CU_INVOICE_ID, CU_EXCEPTION_ID);          
          
      INSERT      #CU_INVOICE_EVENT_BASE
                  (CU_INVOICE_ID
                  ,EVENT_DATE
                  ,EVENT_BY_ID
                  ,EVENT_DESCRIPTION )
                  SELECT
                        CIE.CU_INVOICE_ID
                       ,CIE.EVENT_DATE
                       ,CIE.EVENT_BY_ID
                       ,CIE.EVENT_DESCRIPTION
                  FROM
                        dbo.CU_INVOICE_EVENT CIE
                  WHERE
                        CIE.EVENT_DATE BETWEEN @begindate AND @enddate
                        AND CIE.EVENT_DESCRIPTION LIKE '%insert%'
                        AND CIE.CU_INVOICE_ID IS NOT NULL;          
          
      INSERT      #CU_INVOICE_EVENT
                  (CU_INVOICE_ID
                  ,EVENT_DATE
                  ,EVENT_BY_ID
                  ,EVENT_DESCRIPTION
                  ,UBM
                  ,IS_PROCESSED
                  ,IS_REPORTED
                  ,IS_MANUAL
                  ,CLIENT_ID
                  ,Meter_Read_Type )
                  SELECT
                        CIE.CU_INVOICE_ID
                       ,CIE.EVENT_DATE
                       ,CIE.EVENT_BY_ID
                       ,CIE.EVENT_DESCRIPTION
                       ,UBM.UBM_NAME
                       ,CI.IS_PROCESSED
                       ,CI.IS_REPORTED
                       ,CI.IS_MANUAL
                       ,CI.CLIENT_ID
                       ,itc.Code_Value
                  FROM
                        #CU_INVOICE_EVENT_BASE CIE
                        JOIN dbo.CU_INVOICE CI
                              ON CIE.CU_INVOICE_ID = CI.CU_INVOICE_ID
                        LEFT JOIN dbo.UBM
                              ON CI.UBM_ID = UBM.UBM_ID
                        JOIN #ClientIds C
                              ON CI.CLIENT_ID = C.Client_Id
                        LEFT JOIN dbo.Code itc
                              ON itc.Code_Id = CI.Meter_Read_Type_Cd
                  WHERE
                        CI.IS_DNT = 0
                        AND CI.IS_DUPLICATE = 0
                  GROUP BY
                        CIE.CU_INVOICE_ID
                       ,CIE.EVENT_DATE
                       ,CIE.EVENT_BY_ID
                       ,CIE.EVENT_DESCRIPTION
                       ,UBM.UBM_NAME
                       ,CI.IS_PROCESSED
                       ,CI.IS_REPORTED
                       ,CI.IS_MANUAL
                       ,CI.CLIENT_ID
                       ,itc.Code_Value;          
                     
      IF CHARINDEX('-1', @Client_Id_List, 1) > 0
            INSERT      #CU_INVOICE_EVENT
                        (CU_INVOICE_ID
                        ,EVENT_DATE
                        ,EVENT_BY_ID
                        ,EVENT_DESCRIPTION
                        ,UBM
                        ,IS_PROCESSED
                        ,IS_REPORTED
                        ,IS_MANUAL
                        ,CLIENT_ID
                        ,Meter_Read_Type )
                        SELECT
                              CIE.CU_INVOICE_ID
                             ,CIE.EVENT_DATE
                             ,CIE.EVENT_BY_ID
                             ,CIE.EVENT_DESCRIPTION
                             ,UBM.UBM_NAME
                             ,CI.IS_PROCESSED
                             ,CI.IS_REPORTED
                             ,CI.IS_MANUAL
                             ,NULL AS CLIENT_ID
                             ,itc.Code_Value
                        FROM
                              #CU_INVOICE_EVENT_BASE CIE
                              JOIN dbo.CU_INVOICE CI
                                    ON CIE.CU_INVOICE_ID = CI.CU_INVOICE_ID
                              LEFT JOIN dbo.UBM
                                    ON CI.UBM_ID = UBM.UBM_ID
                              LEFT JOIN dbo.Code itc
                                    ON itc.Code_Id = CI.Meter_Read_Type_Cd
                        WHERE
                              CI.CLIENT_ID IS NULL
                              AND CI.IS_DNT = 0
                              AND CI.IS_DUPLICATE = 0
                        GROUP BY
                              CIE.CU_INVOICE_ID
                             ,CIE.EVENT_DATE
                             ,CIE.EVENT_BY_ID
                             ,CIE.EVENT_DESCRIPTION
                             ,UBM.UBM_NAME
                             ,CI.IS_PROCESSED
                             ,CI.IS_REPORTED
                             ,CI.IS_MANUAL
                             ,itc.Code_Value;       
          
      CREATE CLUSTERED INDEX IX_TCU_INVOICE_EVENT ON #CU_INVOICE_EVENT ( CU_INVOICE_ID );           
          
      TRUNCATE TABLE #CU_INVOICE_EVENT_BASE;          
          
      INSERT      #CU_INVOICE_EVENT_BASE
                  (CU_INVOICE_ID
                  ,EVENT_DATE
                  ,EVENT_BY_ID
                  ,EVENT_DESCRIPTION )
                  SELECT
                        CIE2.CU_INVOICE_ID
                       ,CIE2.EVENT_DATE
                       ,CIE2.EVENT_BY_ID
                       ,CIE2.EVENT_DESCRIPTION
                  FROM
                        #CU_INVOICE_EVENT CIE
                        INNER JOIN dbo.CU_INVOICE_EVENT CIE2
                              ON CIE.CU_INVOICE_ID = CIE2.CU_INVOICE_ID
                  ORDER BY
                        CIE2.CU_INVOICE_ID;          
             
      INSERT      #Cu_Invoice_Event_Whole
                  (CU_INVOICE_ID
                  ,EVENT_DESCRIPTION
                  ,EVENT_DATE
                  ,Flag )
                  SELECT
                        CIE2.CU_INVOICE_ID
                       ,CIE2.EVENT_DESCRIPTION
                       ,CIE2.EVENT_DATE
                       ,CASE WHEN CIE2.EVENT_DESCRIPTION = 'Invoice Closed via Manual Close/Stop Processing' THEN 1
                             ELSE 0
                        END Flag
                  FROM
                        #CU_INVOICE_EVENT_BASE CIE2
                  WHERE
                        ( CIE2.EVENT_DESCRIPTION LIKE '%passed resolve to month%'
                          OR CIE2.EVENT_DESCRIPTION LIKE '%resolved to month%'
                          OR CIE2.EVENT_DESCRIPTION = 'Invoice Closed via Manual Close/Stop Processing' );          
      WITH  CTE_Accounts
              AS ( SELECT
                        CIE.CU_INVOICE_ID
                       ,CISM.Account_ID
                   FROM
                        #CU_INVOICE_EVENT CIE
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH CISM
                              ON CIE.CU_INVOICE_ID = CISM.CU_INVOICE_ID
                   GROUP BY
                        CIE.CU_INVOICE_ID
                       ,CISM.Account_ID)
            INSERT      #CU_Invoice_Accounts
                        (CU_INVOICE_ID
                        ,Account_Number
                        ,Account_Vendor_Name
                        ,Client_Hier_Id )
                        SELECT
                              cte.CU_INVOICE_ID
                             ,cha.Account_Number
                             ,cha.Account_Vendor_Name
                             ,cha.Client_Hier_Id
                        FROM
                              CTE_Accounts cte
                              INNER JOIN Core.Client_Hier_Account AS cha
                                    ON cte.Account_ID = cha.Account_Id
                        GROUP BY
                              cte.CU_INVOICE_ID
                             ,cha.Account_Number
                             ,cha.Account_Vendor_Name
                             ,cha.Client_Hier_Id;          
          
      INSERT      #CUException
                  (CU_INVOICE_ID
                  ,CU_EXCEPTION_ID
                  ,OPENED_DATE
                  ,CLOSED_DATE
                  ,IS_CLOSED
                  ,QUEUE_ID )
                  SELECT
                        CE.CU_INVOICE_ID
                       ,CE.CU_EXCEPTION_ID
                       ,CE.OPENED_DATE
                       ,CE.CLOSED_DATE
                       ,CE.IS_CLOSED
                       ,CE.QUEUE_ID
                  FROM
                        #CU_INVOICE_EVENT CIE
                        JOIN dbo.CU_EXCEPTION CE
                              ON CIE.CU_INVOICE_ID = CE.CU_INVOICE_ID;          
                
      SELECT
            CIE.CU_INVOICE_ID CuInvoiceID
           ,ISNULL(ch.Client_Name, 'No Client') AS [Client Name]
           ,CIE.EVENT_DATE InsertEventDate
           ,CIE.EVENT_DESCRIPTION Inserted
           ,UI1.USERNAME
           ,CRC.EVENT_DESCRIPTION AS ResolvedToMonth
           ,CRC.EVENT_DATE AS ResolvedToMonthEventDate
           ,CASE CIE.IS_PROCESSED
              WHEN 1 THEN 'Yes'
              WHEN 0 THEN 'No'
              ELSE NULL
            END AS IsProcessed
           ,CASE CIE.IS_REPORTED
              WHEN 1 THEN 'Yes'
              WHEN 0 THEN 'No'
              ELSE NULL
            END AS IsReported
           ,CASE CIE.IS_MANUAL
              WHEN 1 THEN 'Yes'
              WHEN 0 THEN 'No'
              ELSE NULL
            END AS IsManual
           ,IC.EVENT_DESCRIPTION AS ClosedManual
           ,CONVERT(VARCHAR, IC.EVENT_DATE, 120) AS ClosedManualDate
           ,CONVERT(VARCHAR, ce.OPENED_DATE, 120) OverallExceptionOpen
           ,CONVERT(VARCHAR, ce.CLOSED_DATE, 120) ExceptionClosed      
           --,IC.EVENT_DATE AS ClosedManualDate      
           --,CE.OPENED_DATE OverallExceptionOpen      
           --,CE.CLOSED_DATE ExceptionClosed      
           ,CASE ce.IS_CLOSED
              WHEN 1 THEN 'Yes'
              WHEN 0 THEN 'No'
              ELSE NULL
            END IsClosed
           ,ce.QUEUE_ID Queue_ID
           ,ca.Account_Number AS [Account Number]
           ,ch.Site_name AS Site
           ,ch.City
           ,ch.State_Name AS State
           ,ch.Country_Name AS Country
           ,ca.Account_Vendor_Name AS Vendor
           ,CIE.UBM
           ,LEFT(CEET.EXCEPTION_TYPE, LEN(CEET.EXCEPTION_TYPE) - 1) AS AllExceptions
           ,LEFT(UN.UserName, LEN(UN.UserName) - 1) AS AllUsers
           ,CIE.Meter_Read_Type
      FROM
            #CU_INVOICE_EVENT CIE
            INNER JOIN dbo.USER_INFO UI1
                  ON CIE.EVENT_BY_ID = UI1.USER_INFO_ID
            LEFT JOIN ( #CU_Invoice_Accounts ca
                        INNER JOIN Core.Client_Hier ch
                              ON ca.Client_Hier_Id = ch.Client_Hier_Id )
                  ON CIE.CU_INVOICE_ID = ca.CU_INVOICE_ID
            LEFT JOIN #Cu_Invoice_Event_Whole IC
                  ON CIE.CU_INVOICE_ID = IC.CU_INVOICE_ID
                     AND IC.Flag = 1
            LEFT JOIN #Cu_Invoice_Event_Whole CRC
                  ON CIE.CU_INVOICE_ID = CRC.CU_INVOICE_ID
                     AND CRC.Flag = 0
            LEFT JOIN #CUException ce
                  ON CIE.CU_INVOICE_ID = ce.CU_INVOICE_ID
            CROSS APPLY ( SELECT TOP 15
                              UN.UserName + ', '
                          FROM
                              #CU_INVOICE_EVENT_BASE CIEB
                              INNER JOIN dbo.USER_INFO AS UN
                                    ON CIEB.EVENT_BY_ID = UN.USER_INFO_ID
                          WHERE
                              CIEB.CU_INVOICE_ID = CIE.CU_INVOICE_ID
                          GROUP BY
                              UN.UserName
                          ORDER BY
                              UN.UserName
            FOR
                          XML PATH('') ) UN ( UserName )
            CROSS APPLY ( SELECT TOP 15
                              CASE WHEN CET.EXCEPTION_TYPE = 'Failed Recalc'
                                        AND ca.Account_Type = 'Utility' THEN 'Failed - Utility Recalc'
                                   WHEN CET.EXCEPTION_TYPE = 'Failed Recalc' THEN 'Failed - ' + rt.Code_Value
                                   ELSE CET.EXCEPTION_TYPE
                              END + ', '
                          FROM
                              dbo.CU_EXCEPTION_DETAIL CED
                              JOIN dbo.CU_EXCEPTION_TYPE CET
                                    ON CED.EXCEPTION_TYPE_ID = CET.CU_EXCEPTION_TYPE_ID
                              JOIN dbo.CU_EXCEPTION ce
                                    ON ce.CU_EXCEPTION_ID = CED.CU_EXCEPTION_ID
                              LEFT JOIN CU_INVOICE_SERVICE_MONTH cuism
                                    ON cuism.CU_INVOICE_ID = ce.CU_INVOICE_ID
                              LEFT JOIN Core.Client_Hier_Account ca
                                    ON ca.Account_Id = cuism.Account_ID
                              LEFT OUTER JOIN dbo.Code rt
                                    ON rt.Code_Id = ca.Supplier_Account_Recalc_Type_Cd
                          WHERE
                              CED.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
                          ORDER BY
                              CED.OPENED_DATE
            FOR
                          XML PATH('') ) CEET ( EXCEPTION_TYPE )
      ORDER BY
            [Client Name];          
          
      DROP TABLE #ClientIds;          
      DROP TABLE #CU_INVOICE_EVENT_BASE;          
      DROP TABLE #CU_INVOICE_EVENT;          
      DROP TABLE #Cu_Invoice_Event_Whole;          
      DROP TABLE #CU_Invoice_Accounts;          
      DROP TABLE #CUException;          
       
END;
;
GO







GRANT EXECUTE ON  [dbo].[Report_InvoiceMetricData_sel_by_Client_All] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_InvoiceMetricData_sel_by_Client_All] TO [CBMSApplication]
GO
