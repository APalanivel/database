SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[cbmsCodeSet_GetForCodeSetName]
	( 
		@CodeSet_Name varchar(25) = null
	)
AS
BEGIN

   select Codeset_Id
		, Codeset_Name
		, codeset_Dsc
		, Std_Column_Name
     from Codeset
    where Codeset_Name = @CodeSet_Name


END


GO
GRANT EXECUTE ON  [dbo].[cbmsCodeSet_GetForCodeSetName] TO [CBMSApplication]
GO
