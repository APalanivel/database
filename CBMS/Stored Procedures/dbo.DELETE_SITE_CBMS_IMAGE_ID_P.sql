SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_SITE_CBMS_IMAGE_ID_P
	@siteID int
	AS
	begin
		set nocount on

		update site 
		set    cbms_image_id = 0, 
		       is_alternate_gas = 0 
		where  site_id = @siteID
	end


GO
GRANT EXECUTE ON  [dbo].[DELETE_SITE_CBMS_IMAGE_ID_P] TO [CBMSApplication]
GO
