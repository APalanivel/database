SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/********        
NAME:        
  dbo.cbmsCuException_GetForDenorm        
        
DESCRIPTION:        
 to get the details from base tables on the basis of cu_invoice_id        
        
INPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 @cu_invoice_id    int                           
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
        
 EXEC dbo.cbmsCuException_GetForDenorm 71132529          
 EXEC dbo.cbmsCuException_GetForDenorm 2030579        
 EXEC dbo.cbmsCuException_GetForDenorm  75250445 
    
        
 SELECT * FROM Cu_Exception Where Cu_invoice_id = 5562469        
        
 SELECT        
  *        
 FROM        
  Cu_Exception_Detail        
 WHERE        
  Cu_Exception_Id  = 3230856        
        
  SELECT TOP 10        
    *        
  FROM        
    cu_Exception ce        
  WHERE        
    is_closed = 1        
    AND EXISTS ( SELECT        
       1        
        FROM        
       Cu_Exception_Detail ced        
        WHERE        
       ced.Cu_Exception_Id = ce.Cu_Exception_id        
       AND ( ced.IS_CLOSED = 1        
          AND ced.CLOSED_REASON_TYPE_ID IS NULL )        
          )        
  ORDER BY ce.Cu_Invoice_Id DESC        
        
AUTHOR INITIALS:        
INITIALS  NAME        
------------------------------------------------------------        
SKA    Shobhit Kumar Agrawal        
PNR    PANDARINATH        
RKV    Ravi Kumra vegesna        
NR    Narayana Reddy      
AP    ARUNKUMAR PALANIVEL    
SP    Srinivas Patchava  
        
MODIFICATION        
INITIALS DATE  MODIFICATION        
------------------------------------------------------------        
SKA   10/21/2010 Created        
SKA   01/13/2011 Added the where condition for ed.IS_CLOSED AND ed.CLOSED_REASON_TYPE_ID (Bug#21854)        
PNR   01/20/2011 Added the where condition for ed.IS_CLOSED AND ed.CLOSED_REASON_TYPE_ID (Bug#21854) at Sub query of join        
HG   02/18/2011 Removed the filter condition Is_Closed = 0 on Cu_Exception as per the requirement of #21854        
      (Requirement is to show all the closed exceptions if closed reason type is not available)        
HG   03/14/2011 As per the request reverted the changes made to show the invoice exception which are in closed status but the closed reason type id is null        
      (MAINT-534)        
RKV   2015-10-30 added two optional parameters account_id and commodity_id as part of AS400-PII        
HG   2016-05-15 AS400 post prodn release hot fix - Returning Commodity_id      
NR   2018-10-08 Data2.0_Phase-2 D20 - 164- Replaced "Multiple" with "Mapping Required" For Exception_Type.      
RKV   2018-11-29 Maint-8018 -  Updated UBM_Client_Name column as per logic.      
AP   SEP 3,2019 ADDED NEW COLUMN DATE IN CBMS AND EXTRACTED THE SAME IN RESULT
SP    21-10-2019 Modify the script exception_status_type for Maint-9395     
*/
CREATE PROCEDURE [dbo].[cbmsCuException_GetForDenorm]
    (
        @cu_invoice_id INT
        , @Account_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Has_Mapping_Exceptions BIT = 0;


        SELECT
            @Has_Mapping_Exceptions = 1
        FROM
            dbo.CU_EXCEPTION ce
            INNER JOIN dbo.CU_EXCEPTION_DETAIL ced
                ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
            INNER JOIN dbo.CU_EXCEPTION_TYPE t
                ON t.CU_EXCEPTION_TYPE_ID = ced.EXCEPTION_TYPE_ID
        WHERE
            ced.IS_CLOSED = 0
            AND ce.IS_CLOSED = 0
            AND ce.CU_INVOICE_ID = @cu_invoice_id
            AND t.EXCEPTION_TYPE IN ( 'Mapping Required - Charge Bucket Not Mapped'
                                      , 'Mapping Required - Determinant Bucket Not Mapped'
                                      , 'Mapping Required - Unit of Measure Not Mapped' );


        SELECT  DISTINCT
                ci.CBMS_IMAGE_ID
                , ci.CBMS_DOC_ID
                , i.CU_INVOICE_ID
                , ex.QUEUE_ID
                , 'exception_type' = CASE WHEN x.exception_type_count = 1 THEN et.EXCEPTION_TYPE
                                         WHEN x.exception_type_count > 1
                                              AND   @Has_Mapping_Exceptions = 1 THEN 'Mapping Required - Multiple'
                                         ELSE 'Multiple'
                                     END
                , 'exception_status_type' = CASE WHEN ex.ROUTED_REASON_TYPE_ID IS NOT NULL THEN rr.ENTITY_NAME
                                                ELSE es.ENTITY_NAME
                                            END
                , 'UBM_Account_Number' = isnull(isnull(a.Account_Number, l.ACCOUNT_NUMBER), i.UBM_ACCOUNT_NUMBER)
                , 'Service_Month' = CASE WHEN z.service_month_count = 1 THEN convert(VARCHAR, cism.SERVICE_MONTH, 101)
                                        WHEN z.service_month_count IS NULL
                                             OR z.service_month_count = 0 THEN NULL
                                        ELSE 'Multiple'
                                    END
                , 'UBM_Client_Name' = CASE WHEN et.EXCEPTION_TYPE = 'Mapping Required - Client Code Not Mapped' THEN
                                               coalesce(ui.UBM_CLIENT_NAME, l.CLIENT_NAME, chcl.Client_Name)
                                          WHEN cism.Account_ID IS NULL THEN
                                              coalesce(chcl.Client_Name, ui.UBM_CLIENT_NAME, l.CLIENT_NAME)
                                          ELSE isnull(chc.Client_Name, l.CLIENT_NAME)
                                      END
                , 'UBM_Site_Name' = CASE WHEN cism.Account_ID IS NOT NULL THEN
                                             rtrim(ad.CITY) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                                        WHEN l.CITY IS NOT NULL THEN l.CITY + ', ' + l.STATE_NAME
                                        WHEN i.UBM_CITY IS NOT NULL THEN i.UBM_CITY + ', ' + i.UBM_STATE_CODE
                                        ELSE NULL
                                    END
                , 'UBM_State_Name' = isnull(isnull(ch.State_Name, l.STATE_NAME), i.UBM_STATE_CODE)
                , 'UBM_City' = isnull(isnull(ad.CITY, l.CITY), i.UBM_CITY)
                , i.IS_MANUAL
                , CASE WHEN et.EXCEPTION_TYPE = 'Failed Recalc' THEN ed.OPENED_DATE
                      ELSE ex.OPENED_DATE
                  END date_in_queue
                , et.SORT_ORDER
                , ch.Client_Hier_Id
                , vam.account_id
                , ed.Commodity_Id
                , (   SELECT    TOP 1
                                Date_In_CBMS
                      FROM
                            CU_EXCEPTION_DENORM CUD
                      WHERE
                          CUD.CU_INVOICE_ID = i.CU_INVOICE_ID) AS date_in_CBMS
        FROM
            dbo.CU_EXCEPTION ex
            JOIN dbo.CU_EXCEPTION_DETAIL ed
                ON ed.CU_EXCEPTION_ID = ex.CU_EXCEPTION_ID
            JOIN dbo.CU_EXCEPTION_TYPE et
                ON et.CU_EXCEPTION_TYPE_ID = ed.EXCEPTION_TYPE_ID
            JOIN dbo.ENTITY es
                ON es.ENTITY_ID = ed.EXCEPTION_STATUS_TYPE_ID
            JOIN dbo.CU_INVOICE i
                ON i.CU_INVOICE_ID = ex.CU_INVOICE_ID
            JOIN dbo.cbms_image ci
                ON ci.CBMS_IMAGE_ID = i.CBMS_IMAGE_ID
            LEFT OUTER JOIN dbo.ENTITY rr
                ON rr.ENTITY_ID = ex.ROUTED_REASON_TYPE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = i.CU_INVOICE_ID
                   AND  isnull(ed.Account_ID, 0) = (CASE WHEN et.EXCEPTION_TYPE = 'Failed Recalc' THEN cism.Account_ID
                                                        ELSE 0
                                                    END)
            LEFT OUTER JOIN dbo.CU_INVOICE_LABEL l
                ON l.CU_INVOICE_ID = ex.CU_INVOICE_ID
            LEFT OUTER JOIN Core.Client_Hier_Account a
                ON a.Account_Id = cism.Account_ID
            LEFT OUTER JOIN Core.Client_Hier chc
                ON chc.Client_Hier_Id = a.Client_Hier_Id
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = i.VENDOR_ID
            LEFT OUTER JOIN Core.Client_Hier chcl
                ON i.CLIENT_ID = chcl.Client_Id
                   AND  chcl.Site_Id = 0
                   AND  chcl.Sitegroup_Id = 0
            LEFT OUTER JOIN dbo.vwAccountMeter vam
                ON vam.account_id = cism.Account_ID
            LEFT OUTER JOIN dbo.METER me
                ON me.METER_ID = vam.meter_id
            LEFT OUTER JOIN dbo.ADDRESS ad
                ON ad.ADDRESS_ID = me.ADDRESS_ID
            LEFT OUTER JOIN Core.Client_Hier ch
                ON vam.site_id = ch.Site_Id
                   AND  ch.Site_Id > 0
            JOIN
            (   SELECT
                    ex.CU_INVOICE_ID
                    , count(DISTINCT ed.EXCEPTION_TYPE_ID) exception_type_count
                FROM
                    dbo.CU_EXCEPTION ex
                    JOIN dbo.CU_EXCEPTION_DETAIL ed
                        ON ed.CU_EXCEPTION_ID = ex.CU_EXCEPTION_ID
                WHERE
                    ed.IS_CLOSED = 0
                    AND ex.IS_CLOSED = 0
                GROUP BY
                    ex.CU_INVOICE_ID) x
                ON x.CU_INVOICE_ID = ex.CU_INVOICE_ID
            JOIN
            (   SELECT
                    ex.CU_INVOICE_ID
                    , count(DISTINCT ed.EXCEPTION_STATUS_TYPE_ID) exception_status_type_count
                FROM
                    dbo.CU_EXCEPTION ex
                    JOIN dbo.CU_EXCEPTION_DETAIL ed
                        ON ed.CU_EXCEPTION_ID = ex.CU_EXCEPTION_ID
                WHERE
                    ed.IS_CLOSED = 0
                    AND ex.IS_CLOSED = 0
                GROUP BY
                    ex.CU_INVOICE_ID) y
                ON y.CU_INVOICE_ID = ex.CU_INVOICE_ID
            LEFT OUTER JOIN
            (   SELECT
                    CU_INVOICE_ID
                    , count(DISTINCT SERVICE_MONTH) service_month_count
                FROM
                    dbo.CU_INVOICE_SERVICE_MONTH
                GROUP BY
                    CU_INVOICE_ID) z
                ON z.CU_INVOICE_ID = i.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.UBM_INVOICE ui
                ON ui.UBM_INVOICE_ID = i.UBM_INVOICE_ID
        WHERE
            ed.IS_CLOSED = 0
            AND ex.IS_CLOSED = 0
            AND i.CU_INVOICE_ID = @cu_invoice_id
            AND (   @Account_Id IS NULL
                    OR  ed.Account_ID = @Account_Id)
            AND (   @Commodity_Id IS NULL
                    OR  ed.Commodity_Id = @Commodity_Id);


    END;

GO


GRANT EXECUTE ON  [dbo].[cbmsCuException_GetForDenorm] TO [CBMSApplication]
GO
