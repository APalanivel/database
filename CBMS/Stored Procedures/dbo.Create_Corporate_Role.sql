SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Create_Corporate_Role  
  
DESCRIPTION: Creates a corporate role in SECURITY_ROLE for the given client
  
INPUT PARAMETERS:      
 Name			DataType          Default     Description      
------------------------------------------------------------------      
 @client_Id     INT  
      
OUTPUT PARAMETERS:
 Name				DataType          Default     Description      
------------------------------------------------------------      
 @Corporate_Role_Id	INT
   
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Create_Corporate_Role 11613,@Corporate_Role_Id	OUT
	
	SELECT @Corporate_Role_Id
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 PNR	  Pandarinath  
 HG			Harihara Suthan G     
 
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 PNR		11/19/2010	Created as part of project : Security Roles Administration
 HG			04/01/2011	Script modified to remove Role_Name column from Security_Role table as it is not used by application anywhere.

******/  

CREATE PROCEDURE dbo.Create_Corporate_Role
    (
        @Client_Id			INT
       ,@Corporate_Role_Id	INT OUT
    )
AS
BEGIN

	SET NOCOUNT ON ;

	DECLARE @CorpRole_Id	INT

    INSERT  dbo.Security_role
	(
		 Client_Id
		,Show_Large_Datasets
		,Is_Corporate
    )
   VALUES
	(
		 @Client_Id
		,1    
		,1
	)

    SET @CorpRole_Id = SCOPE_IDENTITY()  
    
    INSERT INTO dbo.Security_Role_Client_Hier
	(
		 Security_Role_Id
		,Client_Hier_Id
    )
	SELECT 
		 @CorpRole_Id
		,ch.Client_Hier_Id 
	FROM
		Core.Client_Hier ch
		JOIN dbo.Code c 
			 ON ch.Hier_level_Cd = c.Code_Id
		JOIN dbo.Codeset cs 
			 ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		ch.Client_Id = @Client_Id
		AND cs.Codeset_Name = 'HierLevel'
		AND c.Code_Value in ('Corporate', 'Division','Site')

	SET @Corporate_Role_Id = @CorpRole_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Create_Corporate_Role] TO [CBMSApplication]
GO
