SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Charge_Season_Map_Del]  

DESCRIPTION: It Deletes Charge Season Map for Selected Charge Season Map Id.     
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Charge_Season_Map_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Charge_Season_Map_Del 891
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Charge_Season_Map_Del
    (
      @Charge_Season_Map_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.CHARGE_SEASON_MAP
	WHERE
		CHARGE_SEASON_MAP_ID = @Charge_Season_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Charge_Season_Map_Del] TO [CBMSApplication]
GO
