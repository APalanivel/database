SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_ADD_SUPPLIER_P    

DESCRIPTION:  This procedure used to insert the records in vendor table 

IINPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@vendor_name            varchar(200)
@vendor_type            int

    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
 @vendor_id int out
    
USAGE EXAMPLES:    
------------------------------------------------------------    
exec [dbo].[CBMS_ADD_SUPPLIER_P] @vendor_name = 'Test Data', @vendor_type = 110, @vendor_id = null


Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    

SKA	8/4/009  Removed linked server updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
*********/
CREATE PROCEDURE [dbo].[CBMS_ADD_SUPPLIER_P] 
@vendor_name varchar(200),
@vendor_type int, @vendor_id int out

AS



INSERT INTO VENDOR (
					VENDOR_TYPE_ID, 
					VENDOR_NAME,
					IS_HISTORY
					) 
					SELECT 
						ENTITY_ID,@vendor_name,0 
					FROM 
						ENTITY 
					WHERE 
						ENTITY_TYPE=@vendor_type AND 
						ENTITY_NAME='Supplier'


select @vendor_id = SCOPE_IDENTITY()

RETURN
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_SUPPLIER_P] TO [CBMSApplication]
GO
