SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_STATES_NET_P

AS

begin

set nocount on

 

select state_id, state_name from state

 

end


GO
GRANT EXECUTE ON  [dbo].[GET_STATES_NET_P] TO [CBMSApplication]
GO
