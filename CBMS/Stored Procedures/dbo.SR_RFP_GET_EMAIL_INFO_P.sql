SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_EMAIL_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@sr_rfp_email_log_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_RFP_GET_EMAIL_INFO_P
	@sr_rfp_email_log_id int
	AS
set nocount on
	select 	from_email_address,
		to_email_address,	
		cc_email_address,
		subject,
		content,
		generation_date
	from 	sr_rfp_email_log
	where	sr_rfp_email_log_id = @sr_rfp_email_log_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_EMAIL_INFO_P] TO [CBMSApplication]
GO
