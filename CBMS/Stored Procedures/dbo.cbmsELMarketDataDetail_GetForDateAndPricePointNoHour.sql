SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketDataDetail_GetForDateAndPricePointNoHour]
	( @price_point_id varchar(200) = null
	, @price_point_date datetime = null
	)

AS
BEGIN

	   select dd.price_point_detail_id
		, dd.price_point_value	
		, dd.price_point_date
		, dd.price_point_id
		, d.price_point
		, d.source
	     from el_market_data_detail dd with (nolock)
	     join el_market_Data d with (nolock) on d.price_point_id = dd.price_point_id
	    where dd.price_point_id = @price_point_id 
	      and convert(varchar(10), dd.price_point_date ,101) = @price_point_date 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketDataDetail_GetForDateAndPricePointNoHour] TO [CBMSApplication]
GO
