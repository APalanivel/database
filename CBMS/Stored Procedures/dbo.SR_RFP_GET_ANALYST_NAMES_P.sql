
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ANALYST_NAMES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.SR_RFP_GET_ANALYST_NAMES_P 1,1
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-07-24	Global Sourcing - Added IS_HISTORY filter


******/

CREATE PROCEDURE [dbo].[SR_RFP_GET_ANALYST_NAMES_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20) )
AS 
BEGIN
      SET NOCOUNT ON;
      SELECT
            ui.user_info_id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME USER_INFO_NAME
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP map
                  ON map.user_info_id = ui.user_info_id
            INNER JOIN dbo.GROUP_INFO gi
                  ON map.group_info_id = gi.group_info_id
      WHERE
            gi.group_name = 'supply'
            AND ui.IS_HISTORY = 0
      ORDER BY
            ui.FIRST_NAME
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ANALYST_NAMES_P] TO [CBMSApplication]
GO
