SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SAVE_CLIENT_APPROVE_BUDGET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@budgetId      	int       	          	
	@monthIdentifier	datetime  	          	
	@volume        	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE      PROCEDURE dbo.SAVE_CLIENT_APPROVE_BUDGET_P

@userId varchar(10),
@sessionId varchar(20),

@budgetId int,
@monthIdentifier datetime,
@volume decimal (32,16)


as
set nocount on
	insert into rm_target_budget 
	(
		rm_budget_id, month_identifier,
		budget_target
	)
	values
	(
		@budgetId, @monthIdentifier,
		@volume
	 )
GO
GRANT EXECUTE ON  [dbo].[SAVE_CLIENT_APPROVE_BUDGET_P] TO [CBMSApplication]
GO
