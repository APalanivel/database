SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Forecast_Volume_Batch_Dtl_Status_Upd                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name								DataType    Default     Description                        
--------------------------------------------------------------------------------
	@RM_Forecast_Volume_Batch_Dtl_Id	INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-27     Global Risk Management - Created 
                     
******/
CREATE  PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Dtl_Status_Upd]
     (
         @RM_Forecast_Volume_Batch_Dtl_Id INT
         , @Batch_Status_Cd INT
         , @Error_Description NVARCHAR(MAX) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            Trade.RM_Forecast_Volume_Batch_Dtl
        SET
            Status_Cd = @Batch_Status_Cd
            , Error_Description = @Error_Description
            , Last_Change_Ts = GETDATE()
        WHERE
            RM_Forecast_Volume_Batch_Dtl_Id = @RM_Forecast_Volume_Batch_Dtl_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Dtl_Status_Upd] TO [CBMSApplication]
GO
