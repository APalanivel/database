SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoSavingsBreakdown_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_savings_detail_id	int       	          	
	@savings_id    	int       	          	
	@service_month 	datetime  	          	
	@actual_savings_value	decimal(32,16)	          	
	@estimated_savings_value	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsSsoSavingsBreakdown_Save]

	( @MyAccountId int
	, @sso_savings_detail_id int
	, @savings_id int
	, @service_month datetime
	, @actual_savings_value decimal (32,16)
	, @estimated_savings_value decimal (32,16)
	)
AS
BEGIN
	set nocount on

	  declare @this_id int

	      set @this_id = @sso_savings_detail_id

	if @this_id is null
	begin
		insert into sso_savings_detail
			( 
			sso_savings_id
			, service_month
			, actual_savings_value
			, estimated_savings_value
			
			 )
		 values
			(  
			@savings_id
			, @service_month
			, @actual_savings_value
			, @estimated_savings_value
			)

		set @this_id = @@IDENTITY

	end
	else
	begin
		   update sso_savings_detail
		      set sso_savings_id = @savings_id
			, service_month = @service_month
			, actual_savings_value = @actual_savings_value
			, estimated_savings_value = @estimated_savings_value
		    where sso_savings_detail_id = @this_id

	end
--	exec cbmsSsoSavings_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavingsBreakdown_Save] TO [CBMSApplication]
GO
