SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
 NAME: dbo.Utility_Detail_Analyst_Map_Sel_All                  
                               
 DESCRIPTION:                              
			Search of all utility vendors in the system.  
			
			Pivot implemented in this Stored Procedure itself due to Performance Issue in Application Side Pivot.
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
--------------------------------------------------------------------------------------------------------------- 
                              
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  

 --Get all for Excel Download     
 EXEC [dbo].[Utility_Detail_Analyst_Map_Sel_All]       

                            
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 SP                    Sandeep Pigilam                
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SP                    2014-02-24			Created                      
                             
******/   
CREATE  PROCEDURE dbo.Utility_Detail_Analyst_Map_Sel_All
AS 
BEGIN  
      SET NOCOUNT ON;
      
      DECLARE
            @QueryString VARCHAR(MAX)
           ,@SelectQueue VARCHAR(MAX)= ''
           ,@PivotQueue VARCHAR(MAX)= ''
      SELECT
            @SelectQueue = 'MAX([' + gi.GROUP_NAME + ']) AS [' + gi.GROUP_NAME + '],' + @SelectQueue
           ,@PivotQueue = '[' + gi.GROUP_NAME + '],' + @PivotQueue
      FROM
            dbo.GROUP_INFO gi
            JOIN dbo.QUEUE q
                  ON q.queue_id = gi.QUEUE_ID
            JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = q.QUEUE_TYPE_ID
      WHERE
            cs.Codeset_Name = 'InvoiceProcessingTeam'
      ORDER BY
            gi.GROUP_INFO_ID DESC
              
      SET @SelectQueue = LEFT(@SelectQueue, LEN(@SelectQueue) - 1)  
      SET @PivotQueue = LEFT(@PivotQueue, LEN(@PivotQueue) - 1)  
           
      SET @QueryString = '
      SELECT
             Vendor_Name as [Vendor Name]
            ,Country_Name as [Country Name]
            ,State_Name as [State Name]
            ,Commodity_Name as Service
           ,' + @SelectQueue + '
      FROM
            ( SELECT
                  v.Vendor_Name
                 ,ui.first_name + '' '' + ui.last_name Username
                 ,LEFT(temp.Commodity_Name, LEN(temp.Commodity_Name) - 1) AS Commodity_Name
                 ,st.State_Name
                 ,gi.Group_Info_Id
                 ,gi.Group_Name
                 ,cou.Country_Name
                 
              FROM
                  dbo.vendor v
                  JOIN vendor_state_map stmap
                        ON stmap.vendor_id = v.vendor_id
                  JOIN dbo.state st
                        ON st.state_id = stmap.state_id
                  JOIN dbo.COUNTRY cou
                        ON cou.COUNTRY_ID = st.COUNTRY_ID
                  JOIN dbo.utility_detail ud
                        ON ud.vendor_id = v.vendor_id
                  LEFT OUTER JOIN dbo.utility_detail_analyst_map map
                        ON map.utility_detail_id = ud.utility_detail_id
                  LEFT OUTER JOIN dbo.user_info ui
                        ON ui.user_info_id = map.Analyst_ID
                  LEFT JOIN dbo.GROUP_INFO gi
                        ON map.Group_Info_ID = gi.GROUP_INFO_ID
                  CROSS APPLY ( SELECT
                                    CAST(c.Commodity_Name AS VARCHAR(MAX)) + '', ''
                                FROM
                                    dbo.Commodity c
                                    INNER JOIN dbo.VENDOR_COMMODITY_MAP vc
                                          ON c.Commodity_Id = vc.COMMODITY_TYPE_ID
                                WHERE
                                    vc.VENDOR_ID = v.VENDOR_ID
                                ORDER BY
                                    ( CASE WHEN c.Commodity_Name = ''Electric Power'' THEN 1
                                           WHEN c.Commodity_Name = ''Natural Gas'' THEN 2
                                           ELSE 3
                                      END )
                                   ,c.Commodity_Name
                  FOR
                                XML PATH('''') ) temp ( Commodity_Name )
               
                  ) DataTable PIVOT
			( MAX(username) FOR GROUP_NAME IN (' + @PivotQueue + '  ) ) PivotTable
      GROUP BY
            Vendor_Name
           ,Country_Name
           ,State_Name
           ,Commodity_Name'
           

      EXEC (@QueryString)
	  PRINT @QueryString
END 
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Analyst_Map_Sel_All] TO [CBMSApplication]
GO
