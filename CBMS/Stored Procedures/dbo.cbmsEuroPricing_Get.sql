SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsEuroPricing_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@euro_pricing_id	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsEuroPricing_Get]
	( @euro_pricing_id int = null
	)
AS
BEGIN

	   select euro_pricing_id
		, detail_date
		, price_type
		, year
		, interval
		, value
		, price_point
	     from price_point with (nolock)
	    where euro_pricing_id = @euro_pricing_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsEuroPricing_Get] TO [CBMSApplication]
GO
