SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Comment_Ins                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Comment_Ins  1005
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Comment_Ins]
    (
        @Rm_Budget_Id INT
        , @Comment_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        INSERT INTO Trade.Rm_Budget_Comment
             (
                 Rm_Budget_Id
                 , Comment_Id
                 , Created_User_Id
                 , Created_Ts
             )
        VALUES
            (@Rm_Budget_Id
             , @Comment_Id
             , @User_Info_Id
             , GETDATE());

        UPDATE
            Trade.Rm_Budget
        SET
            Last_Change_Ts = GETDATE()
        WHERE
            Rm_Budget_Id = @Rm_Budget_Id;
    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Comment_Ins] TO [CBMSApplication]
GO
