SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.EC_Meter_Attribute_Ins               
                  
Description:                  
        To insert Data to EC_Meter_Attribute table.                  
                  
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
    @State_Id       INT    
    @Commodity_Id      INT    
    @EC_Meter_Attribute_Name   NVARCHAR    
    @Attribute_Type_Cd     INT    
    @Is_Used_In_Calc_Vals    BIT    
    @User_Info_Id      INT        
    @Vendor_Type_Cd 
	        
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
    @EC_Meter_Attribute_Id   INT    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------                    
 DECLARE  @EC_Meter_Attribute_Id_Out INT     
 BEGIN TRAN      
 SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400'    
    EXEC dbo.EC_Meter_Attribute_Ins     
      @State_Id = 1    
     ,@Commodity_Id = 2    
     ,@EC_Meter_Attribute_Name = 'Test_AS400'    
     ,@Attribute_Type_Cd = 3    
     ,@Is_Used_In_Calc_Vals=1    
     ,@User_Info_Id = 100    
     ,@EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out OUTPUT    
 SELECT @EC_Meter_Attribute_Id_Out AS EC_Meter_Attribute_Id     
 SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400'    
 ROLLBACK TRAN                 
                 
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 NR     Narayana Reddy    
 RKV    Ravi Kumar Vegesna    
 SC		Sreenivasulu Cheerala                  
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    NR    2015-04-22  Created For AS400.     
    RKV             2016-11-02      Maint-4317 Added New Parameter @Attribute_Use_Cd           
    SC		2020-05-15  Added @Vendor_Type_Cd int the Parameter section to capture (Supplier, Distributor, Both).
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Ins]
     (
         @State_Id INT
         , @Commodity_Id INT
         , @EC_Meter_Attribute_Name NVARCHAR(200)
         , @Attribute_Type_Cd INT
         , @Is_Used_In_Calc_Vals BIT
         , @User_Info_Id INT
         , @EC_Meter_Attribute_Id INT OUTPUT
         , @Attribute_Use_Cd INT
         , @Vendor_Type_Cd INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        INSERT INTO dbo.EC_Meter_Attribute
             (
                 State_Id
                 , Commodity_Id
                 , EC_Meter_Attribute_Name
                 , Attribute_Type_Cd
                 , Is_Used_In_Calc_Vals
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , Attribute_Use_Cd
                 , Vendor_Type_Cd
             )
        VALUES
            (@State_Id
             , @Commodity_Id
             , @EC_Meter_Attribute_Name
             , @Attribute_Type_Cd
             , @Is_Used_In_Calc_Vals
             , @User_Info_Id
             , GETDATE()
             , @User_Info_Id
             , GETDATE()
             , @Attribute_Use_Cd
             , @Vendor_Type_Cd);

        SET @EC_Meter_Attribute_Id = SCOPE_IDENTITY();

    END;
    ;
GO

GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Ins] TO [CBMSApplication]
GO
