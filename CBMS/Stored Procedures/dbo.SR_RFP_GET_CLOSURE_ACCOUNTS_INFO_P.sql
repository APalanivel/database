
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@RFP_ID        	INT       	          	
    @StartIndex		INT				1 
    @EndIndex		INT				2147483647
    @SortColumn		VARCHAR	
    @SortIndex		VARCHAR
    

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  206,1,50,'CLIENT_NAME','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  139,1,50,'SITE_NAME','DESC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  206,1,50,'ACCOUNT_NUMBER','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  139,1,50,'ACTION','DESC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  139,1,50,'VENDOR_NAME','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  139,1,50,'ED_CONTRACT_NUMBER','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  5,1,50,'GENERAL_COMMENTS','ASC'
	 
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  139,1,2147483647,'GENERAL_COMMENTS','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  13255,1,50,'SITE_NAME','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  13056,1,50,'SITE_NAME','ASC'
	EXEC dbo.SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P  12995,1,50,'SITE_NAME','ASC'
	 
	 
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	PNR			Pandarinath
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SK			07/13/2010  Added Pagination	        	
							Division View Removed as No Use,VWSiteName View replaced by Based table.
	PNR			08/04/2010	Converted static sql to dynamic sql for sorting purpose 
							and also added parameters @SortColumn & @SortIndex for user choice.	
	RR			2015-12-21	Global Sourcing - Phase3 - GCS-425 Added SR_RFP_BID_GROUP_ID, BID_GROUP_NAME, Meter_Number to select list
							GCS-684 Added RFP_Account_Bid_Status, Close action type in Select list
******/

CREATE PROCEDURE [dbo].[SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P]
      @RFP_ID INT
     ,@StartIndex INT = 1
     ,@EndIndex INT = 2147483647
     ,@SortColumn VARCHAR(200)
     ,@SortIndex VARCHAR(10)
AS 
BEGIN

      SET NOCOUNT ON;
  
      DECLARE @Sql VARCHAR(8000)
	  
      SET @SortColumn = case WHEN @SortColumn = 'CLIENT_NAME' THEN 'ch.CLIENT_NAME'
                             WHEN @SortColumn = 'SITE_NAME' THEN 'RTRIM(ch.city) + '', '' + ch.state_name + '' ('' + ch.site_name + '')'''
                             WHEN @SortColumn = 'ACCOUNT_NUMBER' THEN 'cha.ACCOUNT_NUMBER'
                             WHEN @SortColumn = 'ACTION' THEN 'E.ENTITY_NAME'
                             WHEN @SortColumn = 'VENDOR_NAME' THEN 'V.VENDOR_NAME'
                             WHEN @SortColumn = 'ED_CONTRACT_NUMBER' THEN 'CON.ED_CONTRACT_NUMBER'
                             WHEN @SortColumn = 'GENERAL_COMMENTS' THEN 'CL.GENERAL_COMMENTS'
                             ELSE @SortColumn
                        END
						
      SET @Sql = '
      
      ;WITH  SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P_PAG
       AS
       (
					 SELECT
						  ch.CLIENT_ID
						 ,ch.CLIENT_NAME
						 ,ch.SITE_ID
						 ,RTRIM(ch.city) + '', '' + ch.state_name + '' ('' + ch.site_name + '')'' SITE_NAME
						 ,cha.ACCOUNT_NUMBER
						 ,CL.CLOSE_ACTION_TYPE_ID AS ACTION_ID
						 ,E.ENTITY_NAME AS ACTION
						 ,V.VENDOR_ID
						 ,V.VENDOR_NAME
						 ,CON.CONTRACT_ID
						 ,CON.ED_CONTRACT_NUMBER
						 ,CL.BENCHMARK_INDEX_ADDER
						 ,CL.BENCHMARK_FIXED_WHOLESALE
						 ,CL.SAVINGS
						 ,CL.SAVINGS_COMMENTS
						 ,CL.IS_AWARDED_EMAIL_SENT AS AWARDED_EMAIL
						 ,RFP_ACCOUNT.SR_RFP_ACCOUNT_ID AS ACCOUNT_GROUP_ID
						 ,CL.SR_SUPPLIER_CONTACT_INFO_ID AS SUPPLIERCONTACTID
						 ,CL.GENERAL_COMMENTS
						 ,CL.BENCHMARK_COMMENTS
						 ,count(1) OVER ( ) AS TOTAL
						 ,ROW_NUMBER() OVER ( ORDER BY ' + @SortColumn + ' ' + @SortIndex + ' ) AS Row_Num
						 ,bidgroup.SR_RFP_BID_GROUP_ID
                         ,bidgroup.GROUP_NAME AS BID_GROUP_NAME
                         ,cha.Account_Id
                         ,Bid_Status.ENTITY_NAME As Rfp_Account_Bid_Status
                         ,action.ENTITY_NAME AS CLOSE_ACTION_TYPE
					FROM
						  dbo.SR_RFP_ACCOUNT RFP_ACCOUNT
						  INNER JOIN Core.Client_Hier_Account cha
								ON RFP_ACCOUNT.ACCOUNT_ID = cha.Account_Id
						  INNER JOIN Core.Client_Hier ch
								ON cha.Client_Hier_Id = ch.Client_Hier_Id
						  LEFT JOIN dbo.SR_RFP_CLOSURE CL
								ON CL.SR_ACCOUNT_GROUP_ID = RFP_ACCOUNT.SR_RFP_ACCOUNT_ID
						  LEFT JOIN dbo.CONTRACT CON
								ON CL.CONTRACT_ID = CON.CONTRACT_ID
						  LEFT JOIN dbo.VENDOR V
								ON CL.VENDOR_ID = V.VENDOR_ID
						  LEFT JOIN dbo.ENTITY E
								ON CL.CLOSE_ACTION_TYPE_ID = E.ENTITY_ID
						  LEFT JOIN dbo.SR_RFP_BID_GROUP bidgroup
                                ON RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = bidgroup.SR_RFP_BID_GROUP_ID
						  INNER JOIN dbo.ENTITY Bid_Status
                                ON RFP_ACCOUNT.BID_STATUS_TYPE_ID = Bid_Status.ENTITY_ID
                          LEFT JOIN dbo.ENTITY action
								ON CL.CLOSE_ACTION_TYPE_ID = action.ENTITY_ID
                   WHERE
                        RFP_ACCOUNT.IS_DELETED = 0
                        AND RFP_ACCOUNT.SR_RFP_ID = ' + cast(@RFP_ID AS VARCHAR)  
                        
      SET @Sql = @Sql + ' GROUP BY ch.CLIENT_ID
							 ,ch.CLIENT_NAME
							 ,ch.SITE_ID
							 ,RTRIM(ch.city) + '', '' + ch.state_name + '' ('' + ch.site_name + '')''
							 ,cha.ACCOUNT_NUMBER
							 ,CL.CLOSE_ACTION_TYPE_ID 
							 ,E.ENTITY_NAME 
							 ,V.VENDOR_ID
							 ,V.VENDOR_NAME
							 ,CON.CONTRACT_ID
							 ,CON.ED_CONTRACT_NUMBER
							 ,CL.BENCHMARK_INDEX_ADDER
							 ,CL.BENCHMARK_FIXED_WHOLESALE
							 ,CL.SAVINGS
							 ,CL.SAVINGS_COMMENTS
							 ,CL.IS_AWARDED_EMAIL_SENT
							 ,RFP_ACCOUNT.SR_RFP_ACCOUNT_ID
							 ,CL.SR_SUPPLIER_CONTACT_INFO_ID
							 ,CL.GENERAL_COMMENTS
							 ,CL.BENCHMARK_COMMENTS
							 ,bidgroup.SR_RFP_BID_GROUP_ID
							 ,bidgroup.GROUP_NAME
							 ,cha.Account_Id
							 ,Bid_Status.ENTITY_NAME
							,action.ENTITY_NAME
                        ' + ')'
            
      SET @Sql = @Sql + '
            
            SELECT
                  rfpacc.CLIENT_ID
                 ,rfpacc.CLIENT_NAME
                 ,rfpacc.SITE_ID
                 ,rfpacc.SITE_NAME
                 ,rfpacc.ACCOUNT_NUMBER
                 ,rfpacc.ACTION_ID
                 ,rfpacc.ACTION
                 ,rfpacc.VENDOR_ID
                 ,rfpacc.VENDOR_NAME
                 ,rfpacc.CONTRACT_ID
                 ,rfpacc.ED_CONTRACT_NUMBER
                 ,rfpacc.BENCHMARK_INDEX_ADDER
                 ,rfpacc.BENCHMARK_FIXED_WHOLESALE
                 ,rfpacc.SAVINGS
                 ,rfpacc.SAVINGS_COMMENTS
                 ,rfpacc.AWARDED_EMAIL
                 ,rfpacc.ACCOUNT_GROUP_ID
                 ,rfpacc.SUPPLIERCONTACTID
                 ,rfpacc.GENERAL_COMMENTS
                 ,rfpacc.BENCHMARK_COMMENTS
                 ,rfpacc.TOTAL
                 ,rfpacc.SR_RFP_BID_GROUP_ID
				 ,rfpacc.BID_GROUP_NAME
				 ,Meter_Number = left(mtrnum.mtrnum_list, len(mtrnum.mtrnum_list) - 1)
				 ,rfpacc.Rfp_Account_Bid_Status
				 ,rfpacc.CLOSE_ACTION_TYPE
            FROM
                  SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P_PAG rfpacc
                  CROSS APPLY ( SELECT
                                      mtrnum.Meter_Number + '', ''
                                  FROM
                                      Core.Client_Hier_Account mtrnum
                                  WHERE
                                      rfpacc.Account_Id = mtrnum.Account_Id
                                  GROUP BY
                                      mtrnum.Meter_Number
							FOR
                                    XML PATH('''') ) mtrnum ( mtrnum_list )
            WHERE
                  Row_Num BETWEEN ' + cast(@StartIndex AS VARCHAR) + ' AND ' + cast(@EndIndex AS VARCHAR)
            
      EXEC ( @Sql )

END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CLOSURE_ACCOUNTS_INFO_P] TO [CBMSApplication]
GO
