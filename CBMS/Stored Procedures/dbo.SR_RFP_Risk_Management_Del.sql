SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Risk_Management_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Risk Management for Selected 
						SR RFP Risk Management Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------
@SR_RFP_Risk_Management_Id		INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	Begin Tran
		EXEC SR_RFP_Risk_Management_Del  1652
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    31-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Risk_Management_Del
   (
    @SR_RFP_Risk_Management_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_RISK_MANAGEMENT
	WHERE
		SR_RFP_RISK_MANAGEMENT_ID = @SR_RFP_Risk_Management_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Risk_Management_Del] TO [CBMSApplication]
GO
