SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Outside_Contract_Term_Config_Sel          
              
Description:              
        To insert Data into  table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Id						INT
    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	

    EXEC dbo.Account_Outside_Contract_Term_Config_Sel 
      @Account_Id =1148520    ,@Contract_Id =1
          
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/
CREATE PROCEDURE [dbo].[Account_Outside_Contract_Term_Config_Sel]
    (
        @Account_Id INT
        , @Contract_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            'Account Level' AS Level_Configured
            , aloct.Account_Outside_Contract_Term_Config_Id
            , aloct.OCT_Parameter_Cd
            , c.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , c2.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , c3.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
        FROM
            dbo.Account_Outside_Contract_Term_Config aloct
            INNER JOIN dbo.Code c
                ON c.Code_Id = aloct.OCT_Parameter_Cd
            INNER JOIN dbo.Code c2
                ON c2.Code_Id = aloct.OCT_Tolerance_Date_Cd
            INNER JOIN dbo.Code c3
                ON c3.Code_Id = aloct.OCT_Dt_Range_Cd
        WHERE
            aloct.Account_Id = @Account_Id
            AND aloct.Contract_Id = @Contract_Id;
    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Outside_Contract_Term_Config_Sel] TO [CBMSApplication]
GO
