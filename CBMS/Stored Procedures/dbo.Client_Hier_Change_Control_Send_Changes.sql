
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.Client_Hier_Change_Control_Send_Changes    
    
DESCRIPTION:    
 packages changes between @Last_Version_Id     
 and @Current_Version_Id into an XML document and sends them to the     
 Target services listed in Change Control Target    
    
    
    
INPUT PARAMETERS:    
 Name      DataType  Default Description    
------------------------------------------------------------    
@Last_Version_Id   BIGINT  The minimum Change tracking version Id to get     
@Current_Version_Id   BIGINT  The most current Change tracking Version Id    
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 CMH  Chad Hattabaugh    
 HG  Harihara Suthan G    
 AP  Athmaram Pabbathi    
 KVK  Vinay K    
 DJ  Dhanya Jose    
 CPE Chaitanya Panduga Eshwar    
 AKR Ashok Kumar Raju   
     
MODIFICATIONS    
    
 Initials Date     Modification    
------------------------------------------------------------    
 CMH  10/01/2011   Created    
 HG  01/01/2012   Included Client_Hier_Created_Ts in the select statement to save it in @Message    
 AP  03/08/2012   Included Portfolio_Client_Id in the select statement to save it in @Message; as a part of Blackstone project    
 AP  03/16/2012   Addnl Data Changes     
        --Included Site_Type_Name in the Select statement to save it in @Message    
 KVK  04/22/2012   Removed 2 columns(GHG_Region_Key & GHG_Region_Name) from Client Hier Table    
 DJ  06/12/2012    Modified the SELECT for SYS_CHANGE_OPERATION to cng.Sys_Change_Operation    
 AKR  2012-10-31   Included Client_Analyst_Mapping_Cd and Site_Analyst_Mapping_Cd Columns as a part of POCO  
 CPE  2012-10-31   Included Weather_Station_Code column  
******/    
CREATE PROCEDURE [dbo].[Client_Hier_Change_Control_Send_Changes]
      ( 
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT )
AS 
BEGIN     
      SET NOCOUNT ON ;     
     
      DECLARE
            @Message XML
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255)    
     
 -- Validate that the changes are continious with last execution    
      IF CHANGE_TRACKING_MIN_VALID_VERSION(object_id('Core.Client_Hier')) > @Last_Version_id 
            RAISERROR('Changes for Core.Client_Hier is not concurrent', 16, 1) WITH LOG, NOWAIT    
     
 -- Force Sys_Change_Operation to Delete if the base record does not exists     
      SET @Message = ( SELECT
                        cng.Sys_Change_Operation AS SYS_CHANGE_OPERATION
                       ,cng.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Client_Currency_Group_Id
                       ,ch.Sitegroup_Id
                       ,ch.Sitegroup_Name
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,ch.Site_Address_Line1
                       ,ch.Site_Address_Line2
                       ,ch.City
                       ,ch.ZipCode
                       ,ch.State_Id
                       ,ch.State_Name
                       ,ch.Country_Id
                       ,ch.Country_Name
                       ,ch.Geo_Lat
                       ,ch.Geo_Long
                       ,ch.Site_Not_Managed
                       ,ch.Site_Closed
                       ,ch.Site_Closed_Dt
                       ,ch.Region_ID
                       ,ch.Region_Name
                       ,ch.Client_Not_Managed
                       ,ch.Division_Not_Managed
                       ,ch.Sitegroup_Type_cd
                       ,ch.Sitegroup_Type_Name
                       ,ch.Site_Search_Combo
                       ,ch.Last_Change_TS
                       ,ch.User_Passcode_Expiration_Duration
                       ,ch.User_Max_Failed_Login_Attempts
                       ,ch.User_PassCode_Reuse_Limit
                       ,ch.Client_Fiscal_Offset
                       ,ch.Client_Type_Id
                       ,ch.Report_Frequency_Type_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,ch.Ubm_Service_Id
                       ,ch.Dsm_Strategy
                       ,ch.Is_Sep_Issued
                       ,ch.Sep_Issue_Date
                       ,ch.Sitegroup_Owner_User_Id
                       ,ch.Client_Hier_Created_Ts
                       ,ch.Portfolio_Client_Id
                       ,ch.Site_Type_Name
                       ,ch.Client_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Weather_Station_Code
                       FROM
                        Changetable(CHANGES Core.Client_Hier, @Last_Version_Id) cng
                        LEFT JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cng.Client_Hier_Id
                       WHERE
                        cng.SYS_Change_Version <= @Current_Version_Id
            FOR
                       XML PATH('Client_Hier_Change')
                          ,ELEMENTS
                          ,ROOT('Client_Hier_Changes') )     
    
 -- MAke sure there is really data to send before we try sending anything    
      IF @Message.exist('/Client_Hier_Changes') = 1 
            BEGIN    
  -- Cyce through the targets in Change_Control Targt and sent themessage to each    
                  DECLARE cDialog CURSOR    
                  FOR    
                  ( SELECT    
                  Target_Service    
                  ,Target_Contract    
                  FROM    
                  Service_Broker_Target    
                  WHERE    
                  Table_Name = 'Core.Client_Hier')     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
      
                  WHILE @@Fetch_Status = 0 
                        BEGIN     
                              DECLARE @Conversation_Handle UNIQUEIDENTIFIER ;    
                              BEGIN DIALOG CONVERSATION @Conversation_Handle    
   FROM SERVICE [//Change_Control/Service/CBMS/Change_Control]    
   TO SERVICE @Target_Service    
   ON CONTRACT @Target_Contract ;    
                              SEND ON CONVERSATION @Conversation_Handle    
    MESSAGE TYPE [//Change_Control/Message/Client_Hier_Changes] (@Message)    
      
                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog    
            END -- IF     
END ;  
  
;
GO
