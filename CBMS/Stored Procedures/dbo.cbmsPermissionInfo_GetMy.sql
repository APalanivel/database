SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  procedure [dbo].[cbmsPermissionInfo_GetMy]
	( @MyAccountId int
	, @group_info_id int )
AS
BEGIN

	   select pm.permission_info_id
		, IsChecked = case
			when map.permission_info_id is null then 0
			else 1
			end
		, pm.permission_name
		, pm.permission_description
	     from permission_info pm
  left outer join (select permission_info_id
			, group_info_id
	             from group_info_permission_info_map map 
		    where map.group_info_id = @group_info_id
		  ) map on map.permission_info_id = pm.permission_info_id
	 order by pm.permission_name asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPermissionInfo_GetMy] TO [CBMSApplication]
GO
