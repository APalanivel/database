SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.CBMS_SSO_User_Map_Ins                      
                        
Description:                        
        
                        
Input Parameters:                        
    Name			DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@App_Name		NVARCHAR(255)
    @User_Info_Id	INT
    @SSO_XUser_Id	NVARCHAR(255)
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC dbo.CBMS_SSO_User_Map_Ins  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          21-02-2019  Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[CBMS_SSO_User_Map_Ins]
      ( 
       @App_Name NVARCHAR(255)
      ,@User_Info_Id INT
      ,@SSO_XUser_Id NVARCHAR(255) )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @CBMS_SSO_App_Id INT
      
      SELECT
            @CBMS_SSO_App_Id = CBMS_SSO_App_Id
      FROM
            dbo.CBMS_SSO_App
      WHERE
            App_Name = @App_Name
      

      MERGE dbo.CBMS_SSO_User_Map AS TGT
            USING 
                  ( SELECT
                        @User_Info_Id AS User_Info_Id
                       ,@CBMS_SSO_App_Id AS CBMS_SSO_App_Id
                       ,@SSO_XUser_Id AS SSO_XUser_Id ) AS SRC
            ON TGT.CBMS_SSO_App_Id = SRC.CBMS_SSO_App_Id
                  AND TGT.User_Info_Id = SRC.User_Info_Id
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        TGT.SSO_XUser_Id = SRC.SSO_XUser_Id
                       ,TGT.Updated_User_Id = @User_Info_Id
                       ,TGT.Last_Change_Ts = GETDATE()
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         User_Info_Id
                        ,CBMS_SSO_App_Id
                        ,SSO_XUser_Id
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         SRC.User_Info_Id
                        ,SRC.CBMS_SSO_App_Id
                        ,SRC.SSO_XUser_Id
                        ,@User_Info_Id
                        ,GETDATE()
                        ,@User_Info_Id
                        ,GETDATE() );

END;
GO
GRANT EXECUTE ON  [dbo].[CBMS_SSO_User_Map_Ins] TO [CBMSApplication]
GO
