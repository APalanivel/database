SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*********       
NAME:  dbo.Variance_Tests_LoadAll      
     
DESCRIPTION:  This will retrieve all the variance tests to run for the account based on consumption level and master/account level configurations    
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	  @Account_Id		Int    
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   

exec Variance_Tests_LoadAll
      502571

exec Variance_Tests_LoadAll 37704  
exec Variance_Tests_LoadAll 10820
exec Variance_Tests_LoadAll 10645
exec Variance_Tests_LoadAll 1148793

-- SET STATISTICS IO ON
exec Variance_Tests_LoadAll
      502571


------------------------------------------------------------  
    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK		 Nageswara Rao Kosuri
BCH		 Balaraju     
SP		 Sandeep Pigilam   
AP		Arunkumar Palanivel   
    
Initials Date		Modification      
------------------------------------------------------------      
NK		10/29/2009  Created    
NK		01/08/2010  Modified baseline code_value to code_dsc
NK		01/20/2010  Order by clause added to sort the records on vcl.Commodity_Id, vt.Variance_test_Id
BCH		2012-08-27  Replaced base tables(Account, Supplier_Account_Meter_Map, Meter, Rate and Commodity) with core.Client_Hier_Account.
				    (For Performance Issue)
SP		2014-04-29	For Data Operations Enhancement,added BaseLine_Cd,BaseLine_Desc,Service_Month in select list.
SP		2016-11-10	Variance Enhancement Phase II VTE-24,Added Account_Level_Is_Data_Entry_Only. 				    
HG		2016-12-06	Modified to return the tolerance value converted to local uom/currency for the tests configured with absolute value. Test description will still be the defauat test desc updated in consumption level.
Ap		Feb 24 2020 Modified procedure to exclude AVT rules
AP		June 11,2020  https://summit.jira.com/browse/SE2017-996 Tax exclusion

******/
CREATE PROCEDURE [dbo].[Variance_Tests_LoadAll]
      (
      @Account_id INT )
AS
      BEGIN
            SET NOCOUNT ON;

            DECLARE @Account_Variance_Rule_Dtl_Override TABLE
                  ( Variance_Rule_Dtl_Id INT
                  , Account_Id           INT
                  , Tolerance            DECIMAL(16, 4)
                  , Test_Description     VARCHAR(255)
                  , PRIMARY KEY CLUSTERED (
                          Variance_Rule_Dtl_Id
                        , Account_Id ));

            DECLARE
                  @Site_Country_Id            INT
                , @USA_Country_Id             INT
                , @Variance_Process_Engine_Id INT;

            DECLARE @tbl_Account_Commodity TABLE
                  ( Account_id   INT
                  , Commodity_Id INT );

            DECLARE @Account_Variance_Rule_Dtl_tvp Account_Variance_Rule_Dtl;


            SELECT
                  @Variance_Process_Engine_Id = VPE.Variance_Processing_Engine_Id
            FROM  dbo.Variance_Processing_Engine VPE
                  JOIN
                  dbo.Code C
                        ON VPE.Variance_Engine_Cd = C.Code_Id
            WHERE C.Code_Dsc = 'Advanced Variance Test';

            SELECT      TOP ( 1 )
                        @Site_Country_Id = ch.Country_Id
            FROM        Core.Client_Hier_Account cha
                        INNER JOIN
                        Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
            WHERE       cha.Account_Id = @Account_id
            ORDER BY    ch.Country_Name;

            SELECT
                  @USA_Country_Id = COUNTRY_ID
            FROM  dbo.COUNTRY
            WHERE COUNTRY_NAME = 'USA';

            INSERT INTO @tbl_Account_Commodity (
                                                     Account_id
                                                   , Commodity_Id
                                               )
                        SELECT
                                    Account_Id
                                  , Commodity_Id
                        FROM        Core.Client_Hier_Account
                        WHERE       Account_id = @Account_id
                        GROUP BY    Account_Id
                                  , Commodity_Id;

            IF @Site_Country_Id <> @USA_Country_Id
                  BEGIN

                        INSERT INTO @Account_Variance_Rule_Dtl_tvp (
                                                                         Account_Id
                                                                       , Variance_Rule_Dtl_Id
                                                                   )
                                    SELECT
                                          ACCOUNT_ID
                                        , Variance_Rule_Dtl_Id
                                    FROM  dbo.Variance_Rule_Dtl_Account_Override vrdao
                                    WHERE vrdao.ACCOUNT_ID = @Account_id;

                        INSERT INTO @Account_Variance_Rule_Dtl_Override (
                                                                              Variance_Rule_Dtl_Id
                                                                            , Account_Id
                                                                            , Tolerance
                                                                            , Test_Description
                                                                        )
                        EXEC dbo.Get_Account_Variance_Tolerance_Value_Converted_To_Local_Currency_Uom
                              @Account_Variance_Rule_Dtl_tvp = @Account_Variance_Rule_Dtl_tvp;
                  END;

            SELECT
                        vt.Variance_Test_Id
                      , vt.Is_Inclusive
                      , vt.Is_Multiple_Condition
                      , vrd.IS_Active
                      , cd.Code_Value AS Category
                      , vp.Parameter
                      , cdo.Code_Value AS Operator
                      , vr.Positive_Negative
                      , cdb.Code_Dsc AS BaseLine
                      , vcl.Commodity_Id
                      , co.Commodity_Name
                      , vcl.Consumption_Level_Desc AS Consumption_Level
                      , CASE WHEN vrdac.Tolerance IS NOT NULL
                                   THEN vrdac.Tolerance
                             WHEN @USA_Country_Id <> @Site_Country_Id
                                   THEN isnull(ntv.Tolerance, vrd.Default_Tolerance)
                             ELSE  vrd.Default_Tolerance
                        END AS Tolerance
                      , vrd.Is_Data_Entry_Only
                      , vcl.Variance_Consumption_Level_Id
                      , acc.ACCOUNT_TYPE_ID
                      , vrd.Variance_Rule_Dtl_Id
                      , isnull(vrdac.Test_Description, vrd.Test_Description) AS Test_Description
                      , cdb.Code_Id AS BaseLine_Cd
                      , cdb.Code_Dsc AS BaseLine_Desc
                      , NULL AS Service_Month
                      , acc.Is_Data_Entry_Only AS Account_Level_Is_Data_Entry_Only
            FROM        dbo.Variance_Rule_Dtl vrd
                        INNER JOIN
                        dbo.Variance_Rule_Dtl_Account_Override vrdac
                              ON vrdac.Variance_Rule_Dtl_Id = vrd.Variance_Rule_Dtl_Id
                                 AND vrdac.ACCOUNT_ID = @Account_id
                        INNER JOIN
                        dbo.Variance_Rule vr
                              ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                        INNER JOIN
                        dbo.Variance_Test_Master vt
                              ON vt.Variance_Test_Id = vr.Variance_Test_Id
                        INNER JOIN
                        dbo.Variance_Test_Commodity_Map map
                              ON map.Variance_Test_Id = vt.Variance_Test_Id
                        INNER JOIN
                        dbo.Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = vrd.Variance_Consumption_Level_Id
                        INNER JOIN
                        dbo.Account_Variance_Consumption_Level avcl
                              ON avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                 AND avcl.ACCOUNT_ID = vrdac.ACCOUNT_ID
                        INNER JOIN
                        dbo.ACCOUNT acc
                              ON acc.ACCOUNT_ID = avcl.ACCOUNT_ID
                        INNER JOIN
                        dbo.Variance_Parameter_Baseline_Map vpbmap
                              ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                        INNER JOIN
                        dbo.Variance_Parameter vp
                              ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                        INNER JOIN
                        dbo.Code cd
                              ON cd.Code_Id = vp.Variance_Category_Cd
                        INNER JOIN
                        dbo.Code cdo
                              ON cdo.Code_Id = vr.Operator_Cd
                        INNER JOIN
                        dbo.Code cdb
                              ON cdb.Code_Id = vpbmap.Baseline_Cd
                        INNER JOIN
                        dbo.Commodity co
                              ON co.Commodity_Id = vcl.Commodity_Id
                                 AND map.Commodity_Id = vcl.Commodity_Id
                        INNER JOIN
                        @tbl_Account_Commodity cha
                              ON cha.Commodity_Id = co.Commodity_Id
                        LEFT OUTER JOIN
                        @Account_Variance_Rule_Dtl_Override ntv
                              ON ntv.Variance_Rule_Dtl_Id = vrdac.Variance_Rule_Dtl_Id
                                 AND ntv.Account_Id = vrdac.ACCOUNT_ID
            WHERE       vrdac.ACCOUNT_ID = @Account_id
                        AND   NOT EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Rules_Exclusion vr
                        WHERE vr.Country_id = @Site_Country_Id
                              AND   vr.Baseline_Cd = cdb.Code_Id
                              AND   vr.Category_id = cd.Code_Id )
                        AND   NOT EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Rule vr
                              INNER JOIN
                              dbo.Variance_Rule_Dtl vrdtl
                                    ON vrdtl.Variance_Rule_Id = vr.Variance_Rule_Id
                                       AND vrdac.Variance_Rule_Dtl_Id = vrdtl.Variance_Rule_Dtl_Id
                              INNER JOIN
                              dbo.Variance_Parameter_Baseline_Map vpbmap
                                    ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                              INNER JOIN
                              dbo.Variance_Parameter vp1
                                    ON vp1.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                              INNER JOIN
                              dbo.Variance_Parameter_Commodity_Map VPCM
                                    ON VPCM.Variance_Parameter_Id = vp1.Variance_Parameter_Id
                              JOIN
                              dbo.Commodity CM
                                    ON CM.Commodity_Id = VPCM.Commodity_Id
                              JOIN
                              Core.Client_Hier_Account cha
                                    ON cha.Commodity_Id = CM.Commodity_Id
                        WHERE avcl.Variance_Consumption_Level_Id = vrdtl.Variance_Consumption_Level_Id
                              AND   VPCM.Commodity_Id = vcl.Commodity_Id
                              AND   cha.Account_Id = @Account_id
                              AND   cha.Account_Type = 'utility'
                              AND   VPCM.variance_process_Engine_id = @Variance_Process_Engine_Id
                              AND   vp.Variance_Parameter_Id = vp1.Variance_Parameter_Id )
            ORDER BY    vcl.Commodity_Id
                      , vt.Variance_Test_Id;

      END;

      ;
GO



GRANT EXECUTE ON  [dbo].[Variance_Tests_LoadAll] TO [CBMSApplication]
GO
