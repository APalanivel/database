SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoPublication_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_publication_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    procedure [dbo].[cbmsSsoPublication_Delete]
	( @MyAccountId int
	, @sso_publication_id int
	)
AS
BEGIN

	set nocount on

	delete sso_publications where sso_publication_id = @sso_publication_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPublication_Delete] TO [CBMSApplication]
GO
