SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 [cbmsCuInvoiceCharge_GetForInvoiceUnmappedBuckets_NewPage]  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @cu_invoice_id  int                     
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.[cbmsCuInvoiceCharge_GetForInvoiceUnmappedSubBuckets] 123424  
 EXEC dbo.[cbmsCuInvoiceCharge_GetForInvoiceUnmappedSubBuckets] 123448  
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
HKT    Harish Kumar Tirumandyam 
  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 2020-05-27     Same logic as cbmsCuInvoiceCharge_GetForInvoiceUnmappedBuckets including sub buckets logic 
  
******/
CREATE PROCEDURE [dbo].[cbmsCuInvoiceCharge_GetForInvoiceUnmappedSubBuckets]
(@cu_invoice_id INT)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @State_Id INT;

	SELECT TOP 1
		   @State_Id = cha.Meter_State_Id
	FROM
		   dbo.CU_INVOICE_SERVICE_MONTH cism
		   INNER JOIN Core.Client_Hier_Account cha
			   ON cism.Account_ID = cha.Account_Id
	WHERE
		   cism.CU_INVOICE_ID = @cu_invoice_id;



	SELECT
		cc.COMMODITY_TYPE_ID
		, com.Commodity_Name commodity_type
		, cc.Bucket_Master_Id bucket_type_id
		, cc.UBM_BUCKET_CODE
		, cc.EC_Invoice_Sub_Bucket_Master_Id
		, cc.Ubm_Sub_Bucket_Code
		, ubcm.Bucket_Master_Id mapped_Bucket_Master_Id
		, ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id mapped_EC_Invoice_Sub_Bucket_Master_Id
	FROM
		dbo.CU_INVOICE_CHARGE cc
		INNER JOIN dbo.CU_INVOICE ci
			ON ci.CU_INVOICE_ID = cc.CU_INVOICE_ID
		JOIN dbo.Commodity com
			ON com.Commodity_Id = cc.COMMODITY_TYPE_ID
		LEFT OUTER JOIN dbo.UBM_BUCKET_CHARGE_MAP ubcm
			ON ubcm.COMMODITY_TYPE_ID = cc.COMMODITY_TYPE_ID
			   AND ubcm.UBM_BUCKET_CODE = cc.UBM_BUCKET_CODE
			   AND ubcm.UBM_ID = ci.UBM_ID
		LEFT OUTER JOIN dbo.Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map ubdeisbm
			ON ubdeisbm.Ubm_Bucket_Charge_Map_Id = ubcm.UBM_BUCKET_CHARGE_MAP_ID
			   AND ubdeisbm.Ubm_Sub_Bucket_Code = cc.Ubm_Sub_Bucket_Code
			   AND ubdeisbm.State_Id = @State_Id
	WHERE
		cc.CU_INVOICE_ID = @cu_invoice_id
		AND cc.UBM_BUCKET_CODE IS NOT NULL
		AND (
				cc.Bucket_Master_Id IS NOT NULL
				AND cc.Ubm_Sub_Bucket_Code IS NOT NULL
				AND cc.EC_Invoice_Sub_Bucket_Master_Id IS NULL
				AND EXISTS (
							   SELECT
								   1
							   FROM
								   dbo.EC_Invoice_Sub_Bucket_Master eisbm
								   INNER JOIN dbo.Bucket_Master bm
									   ON bm.Bucket_Master_Id = eisbm.Bucket_Master_Id
							   WHERE
								   bm.Commodity_Id = cc.COMMODITY_TYPE_ID AND eisbm.State_Id = @State_Id
						   )
			)
	GROUP BY
		cc.COMMODITY_TYPE_ID
		, com.Commodity_Name
		, cc.Bucket_Master_Id
		, cc.UBM_BUCKET_CODE
		, cc.EC_Invoice_Sub_Bucket_Master_Id
		, cc.Ubm_Sub_Bucket_Code
		, ubcm.Bucket_Master_Id
		, ubdeisbm.EC_Invoice_Sub_Bucket_Master_Id
	ORDER BY
		com.Commodity_Name
		, cc.UBM_BUCKET_CODE ASC;


END;


GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceCharge_GetForInvoiceUnmappedSubBuckets] TO [CBMSApplication]
GO
