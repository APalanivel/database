SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_DELETE_SUPPLY_INFO_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_DELETE_SUPPLY_INFO_DETAILS_P

@srDealTicketId int

AS
set nocount on
delete SR_DT_SUPPLY_INFO where sr_deal_ticket_id = @srDealTicketId
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_SUPPLY_INFO_DETAILS_P] TO [CBMSApplication]
GO
