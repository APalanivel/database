SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
  

NAME:        
dbo.Consumption_Unit_Conversion_Ins       

DESCRIPTION:        
	Used to insert/update UOM conversion into COMMODITY_UOM_CONVERSION table      

INPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
  @BaseUnitId AS INT
, @ConvertedUnitId AS INT
, @Conversion_Factor AS DECIMAL(   32 , 16 )     
      
OUTPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC dbo.Consumption_Unit_Conversion_Ins 87,102,10


AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA Shobhit Kr Agrawal    

MODIFICATIONS         
Initials Date Modification        
------------------------------------------------------------        
GB    Created      
SKA  08/07/09 Modified    

GB  14/07/09 In Entity Table the Unit for Description is different for Different Commodity,   
			It does not follow standard pattern for electriv power, Natural gas and Alternative Natural Gas  
			Hence those are treated separately in the case  

    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Consumption_Unit_Conversion_Ins]
  @BaseUnitId AS INT
, @ConvertedUnitId AS INT
, @Conversion_Factor AS DECIMAL(   32 , 16 )

AS 

BEGIN
    SET  NOCOUNT ON;
    
    DECLARE  @TConversion_Factor AS DECIMAL(32, 16)
    BEGIN TRY
		BEGIN TRANSACTION
    

			IF NOT EXISTS (SELECT 1 FROM dbo.CONSUMPTION_UNIT_CONVERSION 
						WHERE  BASE_UNIT_ID = @BaseUnitId AND
							CONVERTED_UNIT_ID = @ConvertedUnitId )
			 BEGIN
			 				
				INSERT INTO dbo.CONSUMPTION_UNIT_CONVERSION(   BASE_UNIT_ID
					, CONVERTED_UNIT_ID
					, CONVERSION_FACTOR )
				VALUES  ( @BaseUnitId
					, @ConvertedUnitId
					, @Conversion_Factor)
			 END	   

			SET @TConversion_Factor = 1 / @Conversion_Factor

			IF NOT EXISTS (SELECT 1 FROM dbo.CONSUMPTION_UNIT_CONVERSION 
							WHERE BASE_UNIT_ID = @ConvertedUnitId AND
								CONVERTED_UNIT_ID = @BaseUnitId)
			 BEGIN
			 
				INSERT INTO dbo.CONSUMPTION_UNIT_CONVERSION(   BASE_UNIT_ID
					, CONVERTED_UNIT_ID
					, CONVERSION_FACTOR )
				VALUES(@ConvertedUnitId
					, @BaseUnitId
					, @TConversion_Factor)
			 END

		COMMIT TRAN

	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		EXEC dbo.usp_RethrowError
	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Consumption_Unit_Conversion_Ins] TO [CBMSApplication]
GO
