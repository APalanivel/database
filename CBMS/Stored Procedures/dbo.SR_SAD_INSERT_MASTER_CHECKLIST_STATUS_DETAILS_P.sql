SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_INSERT_MASTER_CHECKLIST_STATUS_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountId     	int       	          	
	@year          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_INSERT_MASTER_CHECKLIST_STATUS_DETAILS_P
@accountId int,
@year int

as
set nocount on

insert into SR_MASTER_CHECKLIST_REVIEW_STATUS
	(ACCOUNT_ID, 
	IS_REVIEWED,
	CHECKLIST_YEAR 
	)

values (
	@accountId,
	1,
	@year 
	)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_INSERT_MASTER_CHECKLIST_STATUS_DETAILS_P] TO [CBMSApplication]
GO
