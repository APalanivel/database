SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsCurrencyUnitConversion_Save]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@myAccountId               int
	@currencyGroupId           int
	@baseUnitId                int
	@convertedUnitId           int
	@conversionFactor          decimal(32,16)
	@startDate                 datetime
	@yearsForward              int               0
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsCurrencyUnitConversion_Save
--	@myAccountId = 1
--	,@currencyGroupId = 1
--	,@baseUnitId = 1
--	,@convertedUnitId =1
--	,@conversionFactor = 1.0
--	,@startDate = '2009-04-23 09:38:44.100'
--	,@yearsForward = 0



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

******/



create procedure [dbo].[cbmsCurrencyUnitConversion_Save]
(
	@myAccountId int
	,@currencyGroupId int
	,@baseUnitId int
	,@convertedUnitId int
	,@conversionFactor decimal(32,16)
	,@startDate datetime
	,@yearsForward int = 0
)
as

begin

	set nocount on
	if @yearsForward is null
		set @yearsForward = 0
	begin tran
		if @yearsForward = 0
			begin
			-- delete single row where conversion_date = @startDate
			delete
			from currency_unit_conversion
			where currency_group_id = @currencyGroupId
			and base_unit_id = @baseUnitId
			and converted_unit_id = @convertedUnitId
			and conversion_date = @startDate
			end
		else
			begin
			-- delete all rows where conversion_date >= @startDate
			delete
			from currency_unit_conversion
			where currency_group_id = @currencyGroupId
			and base_unit_id = @baseUnitId
			and converted_unit_id = @convertedUnitId
			and conversion_date >= @startDate
			end		

		declare @endDate datetime
		if @yearsForward = 0
			set @endDate = @startDate + 1
		else
			set @endDate = dateadd(year,@yearsForward,getdate())
	
		declare @currentDate datetime
		set @currentDate = @startDate
	
		while (@currentDate < @endDate)
			begin
				insert into currency_unit_conversion
				(
					currency_group_id
					,base_unit_id
					,converted_unit_id
					,conversion_factor
					,conversion_date
				)
				values
				(
					 @currencyGroupId
					,@baseUnitId
					,@convertedUnitId
					,@conversionFactor
					,@currentDate
				)
			set @currentDate = dateadd(month,1,@currentDate)
				
			end
			
		
	if @@error <> 0
		rollback tran
	else
		commit tran
--	set nocount off
end

GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnitConversion_Save] TO [CBMSApplication]
GO
