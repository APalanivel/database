SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name       DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Cbms_Sitegroup_Id    INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
  SELECT TOP 10 * FROM dbo.Cbms_Sitegroup  
  EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id 29  
                      
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-09-29     Global Risk Management - Created   
                       
******/
CREATE PROCEDURE [dbo].[Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id] (@Cbms_Sitegroup_Id INT)
AS
BEGIN
    SET NOCOUNT ON;

    CREATE TABLE #RM_Group_Sites
    (
        [Site_Id] INT
    );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = rgd.Group_Participant_Id
        INNER JOIN Core.Client_Hier_Account chasupp
            ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier_Account chautil
            ON chasupp.Meter_Id = chautil.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
    WHERE (
              rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
              AND grp.Code_Value = 'Contract'
              AND chasupp.Account_Type = 'Supplier'
              AND chautil.Account_Type = 'Utility'
          );

    INSERT INTO #RM_Group_Sites
    (
        Site_Id
    )
    SELECT ch.Site_Id
    FROM dbo.Cbms_Sitegroup_Participant rgd
        INNER JOIN dbo.Code grp
            ON grp.Code_Id = rgd.Group_Participant_Type_Cd
        INNER JOIN Core.Client_Hier ch
            ON ch.Site_Id = rgd.Group_Participant_Id
    WHERE (
              rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
              AND grp.Code_Value = 'Site'
          );

    SELECT ch.Site_Id,
           ch.Client_Id,
           ch.Sitegroup_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
    FROM #RM_Group_Sites rgs
        INNER JOIN Core.Client_Hier ch
            ON rgs.Site_Id = ch.Site_Id
    GROUP BY ch.Site_Id,
             ch.Client_Id,
             ch.Sitegroup_Id,
             RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')';



END;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id] TO [CBMSApplication]
GO
