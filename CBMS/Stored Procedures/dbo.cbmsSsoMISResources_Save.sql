SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoMISResources_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_resources_id	int       	          	
	@resources_title	varchar(200)	          	
	@resources_url 	varchar(1000)	null      	
	@resources_category_type_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       PROCEDURE [dbo].[cbmsSsoMISResources_Save]
	( @MyAccountId int
	, @sso_resources_id int
	, @resources_title varchar(200)
	, @resources_url varchar(1000)=null
	, @resources_category_type_id int
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @sso_resources_id

	if @this_id is null
	begin

		insert into sso_resourcess
			( 
			resources_title
			, resources_url
			, resources_category_type_id
			 )
		 values
			(  
			@resources_title
			, @resources_url
			, @resources_category_type_id
			)

		set @this_id = @@IDENTITY

	end
	else
	begin

		   update sso_misresourcess
		      set resources_title = @resources_title
			, resources_url = @resources_url
			, resources_category_type_id = @resources_category_type_id
			
		    where sso_resources_id = @this_id

	end

--	exec cbmsSsoMISresources_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoMISResources_Save] TO [CBMSApplication]
GO
