SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
 NAME: [dbo].[User_Info_Group_Info_Map_Sel_By_User]      
      
 DESCRIPTION:       
       get groups for user.        
           
 INPUT PARAMETERS:        
       
 Name                               DataType            Default          Description        
---------------------------------------------------------------------------------------------------------------      
 @User_info_id                      INT      
        
 OUTPUT PARAMETERS:              
 Name                               DataType            Default          Description        
---------------------------------------------------------------------------------------------------------------      
        
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------       
       
 EXEC dbo.User_Info_Group_Info_Map_Sel_By_User 235   
 
 EXEC User_Info_Group_Info_Map_Sel_By_User 41453
   

 AUTHOR INITIALS:       
       
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy          
         
 MODIFICATIONS:       
        
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2013-12-18      Created for RA Admin user management      
       
******/      
      
CREATE PROCEDURE [dbo].[User_Info_Group_Info_Map_Sel_By_User] ( @user_info_id INT )
AS 
BEGIN      
      SET NOCOUNT ON        
        
      SELECT
            gi.group_info_id
           ,gi.group_name
           ,gi.group_description
           ,gi.Is_Required_For_RA_Login
      FROM
            dbo.group_info gi
            INNER JOIN dbo.User_Info_Group_Info_Map map
                  ON map.group_info_id = gi.group_info_id
                     AND map.User_Info_Id = @User_info_id
      WHERE
            ( gi.Is_Required_For_RA_Login = 0
              OR NOT EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.User_Info_Client_App_Access_Role_Map urm
                                    INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map argm
                                          ON argm.Client_App_Access_Role_Id = urm.Client_App_Access_Role_Id
                              WHERE
                                    urm.User_Info_Id = @user_info_id
                                    AND argm.GROUP_INFO_ID = gi.GROUP_INFO_ID ) )
      ORDER BY
            gi.group_name ASC        
END;



;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Group_Info_Map_Sel_By_User] TO [CBMSApplication]
GO
