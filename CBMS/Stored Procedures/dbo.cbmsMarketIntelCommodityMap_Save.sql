SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      procedure [dbo].[cbmsMarketIntelCommodityMap_Save]
	( @MyAccountId int
	, @MarketIntelId int
	, @commodity_type_id int
	)
AS
BEGIN

	set nocount on

	declare @RecordCount int

	select @RecordCount = count(*)
	from market_intel_commodity_map
	where market_intel_id = @MarketIntelId
	and commodity_type_id = @commodity_type_id

	if @RecordCount = 0
	begin
		insert into market_intel_commodity_map
			( market_intel_id
			, commodity_type_id
			)
		values
			(@MarketIntelId
			,@commodity_type_id
			)
	end
	
		
--set nocount off

End
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntelCommodityMap_Save] TO [CBMSApplication]
GO
