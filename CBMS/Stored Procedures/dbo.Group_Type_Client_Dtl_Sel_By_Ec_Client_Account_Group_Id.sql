SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Group_Type_Client_Dtl_Sel_By_Ec_Client_Account_Group_Id        
                
Description:                
		This sproc to get the client details based state commodity.        
                             
 Input Parameters:                
    Name							DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Ec_Client_Account_Group_Id		INT
	
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

   Exec dbo.Group_Type_Client_Dtl_Sel_By_Ec_Client_Account_Group_Id   38
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Group_Type_Client_Dtl_Sel_By_Ec_Client_Account_Group_Id]
      ( 
       @Ec_Client_Account_Group_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            ch.Country_Id
           ,ch.Country_Name
           ,ch.State_Id
           ,ch.State_Name
           ,c.Commodity_Id
           ,c.Commodity_Name
           ,ch.Client_Id
           ,ch.Client_Name
           ,eagt.Group_Type_Name
           ,ecag.Account_Group_Name
           ,ecag.Start_Dt
           ,ecag.End_Dt
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN dbo.Ec_Client_Account_Group ecag
                  ON eagt.Ec_Account_Group_Type_Id = ecag.Ec_Account_Group_Type_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Id = ecag.Client_Id
                     AND eagt.State_Id = ch.State_Id
            INNER JOIN dbo.Commodity c
                  ON c.Commodity_Id = eagt.Commodity_Id
      WHERE
            ecag.Ec_Client_Account_Group_Id = @Ec_Client_Account_Group_Id
      GROUP BY
            ch.Country_Id
           ,ch.Country_Name
           ,ch.State_Id
           ,ch.State_Name
           ,c.Commodity_Id
           ,c.Commodity_Name
           ,ch.Client_Id
           ,ch.Client_Name
           ,eagt.Group_Type_Name
           ,ecag.Account_Group_Name
           ,ecag.Start_Dt
           ,ecag.End_Dt
            
      
END
      
;
GO
GRANT EXECUTE ON  [dbo].[Group_Type_Client_Dtl_Sel_By_Ec_Client_Account_Group_Id] TO [CBMSApplication]
GO
