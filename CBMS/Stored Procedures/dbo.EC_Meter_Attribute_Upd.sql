SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.EC_Meter_Attribute_Upd             
                
Description:                
        To update Data to EC_Meter_Attribute table.                
                
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
    @EC_Meter_Attribute_Id    INT  
    @State_Id       INT  
    @Commodity_Id      INT  
    @EC_Meter_Attribute_Name   NVARCHAR(200)  
    @Attribute_Type_Cd     INT  
    @Is_Used_In_Calc_Vals    BIT  
    @User_Info_Id      INT      
          
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------                  
 DECLARE  @EC_Meter_Attribute_Id_Out INT   
 BEGIN TRAN    
 SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400'  
    EXEC dbo.EC_Meter_Attribute_Ins   
      @State_Id = 1  
     ,@Commodity_Id = 2  
     ,@EC_Meter_Attribute_Name = 'Test_AS400'  
     ,@Attribute_Type_Cd = 3  
     ,@Is_Used_In_Calc_Vals=1  
     ,@User_Info_Id = 100  
     ,@EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out OUTPUT  
 SELECT @EC_Meter_Attribute_Id_Out AS EC_Meter_Attribute_Id   
 SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Name='Test_AS400'  
   
 EXEC dbo.EC_Meter_Attribute_Upd   
      @EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out  
     ,@State_Id = 1  
     ,@Commodity_Id = 2  
     ,@EC_Meter_Attribute_Name = 'Test_AS400_Updated'  
     ,@Attribute_Type_Cd = 3  
     ,@Is_Used_In_Calc_Vals=1  
     ,@User_Info_Id = 100  
       
 SELECT * FROM dbo.EC_Meter_Attribute WHERE EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_Out  
 ROLLBACK TRAN    
             
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 NR     Narayana Reddy    
 RKV    Ravi Kumar Vegesna  
 SC		Sreenivasulu Cheerala              
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    NR    2015-04-22  Created For AS400.           
    RKV             2016-11-02      Maint-4317 Added New Parameter @Attribute_Use_Cd         
    SC		2020-05-15  Added @Vednor_Type_Cd as input parameter           
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Upd]
    (
        @EC_Meter_Attribute_Id INT
        , @State_Id INT
        , @Commodity_Id INT
        , @EC_Meter_Attribute_Name NVARCHAR(200)
        , @Attribute_Type_Cd INT
        , @Is_Used_In_Calc_Vals BIT
        , @User_Info_Id INT
        , @Attribute_Use_Cd INT
        , @Vendor_Type_Cd INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            dbo.EC_Meter_Attribute
        SET
            State_Id = @State_Id
            , Commodity_Id = @Commodity_Id
            , EC_Meter_Attribute_Name = @EC_Meter_Attribute_Name
            , Attribute_Type_Cd = @Attribute_Type_Cd
            , Is_Used_In_Calc_Vals = @Is_Used_In_Calc_Vals
            , Updated_User_Id = @User_Info_Id
            , Attribute_Use_Cd = @Attribute_Use_Cd
            , Last_Change_Ts = GETDATE()
            , Vendor_Type_Cd = @Vendor_Type_Cd
        WHERE
            EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id;
    END;
GO

GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Upd] TO [CBMSApplication]
GO
