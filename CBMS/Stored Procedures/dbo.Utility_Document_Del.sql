SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Document_Del

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Utility_Document_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Utility_Document_Del 20
	ROLLBACK TRAN
		
SELECT * FROM Utility_Document

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
    
CREATE PROC dbo.Utility_Document_Del   
      (    
       @Utility_Document_Id INT    
 )    
AS     
BEGIN    
     
      SET nocount ON ;    
    
      DELETE FROM     
            Utility_Document    
      WHERE    
            Utility_Document_Id = @Utility_Document_Id
END 

GO
GRANT EXECUTE ON  [dbo].[Utility_Document_Del] TO [CBMSApplication]
GO
