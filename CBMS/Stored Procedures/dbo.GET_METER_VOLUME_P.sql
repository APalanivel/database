SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.GET_METER_VOLUME_P 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
	SELECT * FROM  dbo.CONTRACT_METER_VOLUME cmv WHERE cmv.CONTRACT_ID = 185074
	Exec dbo.GET_METER_VOLUME_P 185074, 3082
  
  
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-05-09	Added header
							GRMUER-81 - Added
******/
CREATE PROCEDURE [dbo].[GET_METER_VOLUME_P]
    (
        @contract_id INT
        , @meter_id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cmv.VOLUME
            , cmv.MONTH_IDENTIFIER
            , ent1.ENTITY_ID unit_id
            , ent1.ENTITY_NAME unit_name
            , ent2.ENTITY_ID frequency_id
            , ent2.ENTITY_NAME frequency_name
            , cmv.Is_Edited
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
            INNER JOIN dbo.ENTITY ent1
                ON ent1.ENTITY_ID = cmv.UNIT_TYPE_ID
            INNER JOIN dbo.ENTITY ent2
                ON ent2.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
        WHERE
            cmv.CONTRACT_ID = @contract_id
            AND cmv.METER_ID = @meter_id
        GROUP BY
            cmv.VOLUME
            , cmv.MONTH_IDENTIFIER
            , ent1.ENTITY_ID
            , ent1.ENTITY_NAME
            , ent2.ENTITY_ID
            , ent2.ENTITY_NAME
            , cmv.Is_Edited;

    END;

GO
GRANT EXECUTE ON  [dbo].[GET_METER_VOLUME_P] TO [CBMSApplication]
GO
