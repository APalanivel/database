SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   procedure [dbo].[cbmsLookup_Save]
	( @MyAccountId int
	, @entity_id int = null
	, @entity_name varchar(200)
	, @entity_type int
	, @entity_description varchar(1000) = null
	)
AS
BEGIN

	set nocount on

	declare @ThisId int
	set @ThisId = @entity_id
	
	if @ThisId is null
	begin


		insert into entity
			( entity_name
			, entity_type
			, entity_description
			)
		values
			( @entity_name
			, @entity_type
			, @entity_description
			)
	
		set @ThisId = @@IDENTITY

		--exec cbmsEntityAudit_Log @MyAccountId, 'group_info', @ThisId, 'Insert'

	end
	else
	begin
	
	   update entity_info
	      set entity_name = @entity_name
		, entity_type = @entity_type
		, entity_description = @entity_description
	    where entity_id = @ThisId

	     --exec cbmsEntityAudit_Log @MyAccountId, 'user_info', @ThisId, 'Edit'
	
	end
	
--	set nocount off

	exec cbmsLookup_Get @MyAccountId, @ThisId


END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_Save] TO [CBMSApplication]
GO
