
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.UBM_GET_BATCHLOG_FILES_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@BatchLogId		varchar(8)			null      
	@UbmName		varchar(20)			null      
	@StartDate		datetime			null       
	@EndDate		datetime			null      
	@Status			varchar(20)			null  
	@FailureReason	varchar(500)		null
	@StartIndex		INT					1
	@EndIndex		INT					2147483647	            	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.UBM_GET_BATCHLOG_FILES_P       
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  5803,null,null,nul     
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,'prokarma',null,null,null,null,1,25     
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,'2009-08-19 04:00:02.533',null,null,null,1,25     
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,null,'2009-08-12 03:40:04.490',null,null,1,25  
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,null,null,'success',null,1,25    
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,null,null,null,'prokarma ftp',1,25 
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,null,null,null,'prokarma ftp',1,25
EXEC dbo.UBM_GET_BATCHLOG_FILES_P
      null
     ,null
     ,null
     ,null
     ,null
     ,null
     ,1
     ,250
     ,'TEST2'
EXEC dbo.UBM_GET_BATCHLOG_FILES_P  null,null,null,null,null,null,1,250,'SWDE2221'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------	        	
	NKOSURI		19-Aug-2009	Added serach criteria
	NKOSURI		09-Sep-2009	Modified for date issue
	NKOSURI		10-Sep-2009	Modified to fetch detail failure reason
	PNR			17-Aug-2010	Added Pagination apart from this added @StartIndex,@EndIndex Params.
	SKA			08/25/2010	Renamed the total_rows column to total
								        	
	DMR			09/10/2010	Modified for Quoted_Identifier
	NR			2017-08-17	Generic ETL - Payment File Processing - Added Group key column in Output List.
	RR			2017-09-20	Generic ETL - Added new optional inputs @Group_Key & @File_Name
******/
CREATE PROCEDURE [dbo].[UBM_GET_BATCHLOG_FILES_P]
      (
       @BatchLogId INT = NULL
      ,@UbmName VARCHAR(200) = NULL
      ,@StartDate DATETIME = NULL
      ,@EndDate DATETIME = NULL
      ,@Status VARCHAR(200) = NULL
      ,@FailureReason VARCHAR(500) = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647
      ,@Group_Key NVARCHAR(200) = NULL
      ,@File_Name NVARCHAR(255) = NULL )
AS
BEGIN  
  
      SET NOCOUNT ON;
      
      SELECT
            @EndDate = CASE WHEN @EndDate IS NOT NULL THEN DATEADD(SS, -1, DATEADD(D, 1, CAST(CONVERT(VARCHAR(12), @EndDate, 101) AS DATETIME)))
                            ELSE @EndDate
                       END;  
  
      WITH  UBM_GET_BATCHLOG_FILES
              AS ( SELECT
                        master_log.UBM_BATCH_MASTER_LOG_ID BATCH_ID
                       ,ubm.UBM_NAME UBM_NAME
                       ,master_log.START_DATE
                       ,master_log.END_DATE
                       ,Batch_Status.ENTITY_NAME STATUS
                       ,master_log.EXPECTED_IMAGE_RECORDS
                       ,master_log.ACTUAL_IMAGE_RECORDS
                       ,master_log.BATCH_FAILURE_REASON
                       ,master_log.DETAIL_FAILURE_REASON
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY master_log.UBM_BATCH_MASTER_LOG_ID DESC )
                       ,Total = COUNT(1) OVER ( )
                       ,usc.Group_Key
                   FROM
                        dbo.UBM_BATCH_MASTER_LOG master_log
                        JOIN dbo.UBM ubm
                              ON master_log.UBM_ID = ubm.UBM_ID
                        JOIN dbo.ENTITY Batch_Status
                              ON Batch_Status.ENTITY_ID = master_log.STATUS_TYPE_ID
                        LEFT JOIN UBM_Generic.dbo.UBM_Bill_Batch ubmbb
                              ON master_log.UBM_BATCH_MASTER_LOG_ID = ubmbb.UBM_Batch_Master_Log_Id
                        LEFT JOIN UBM_Generic.dbo.UBM_Source_Config usc
                              ON usc.UBM_Source_Config_Id = ubmbb.UBM_Source_Config_Id
                   WHERE
                        ( ( @UbmName IS NULL )
                          OR ( ubm.UBM_NAME = @UbmName ) )
                        AND ( ( @Status IS NULL )
                              OR ( Batch_Status.ENTITY_NAME = @Status ) )
                        AND ( ( @BatchLogId IS NULL )
                              OR ( master_log.UBM_BATCH_MASTER_LOG_ID = @BatchLogId ) )
                        AND ( ( @StartDate IS NULL )
                              OR ( master_log.START_DATE >= @StartDate ) )
                        AND ( ( @EndDate IS NULL )
                              OR ( master_log.END_DATE <= @EndDate ) )
                        AND ( ( @FailureReason IS NULL )
                              OR ( master_log.BATCH_FAILURE_REASON LIKE +'%' + @FailureReason + '%' ) )
                        AND ( @Group_Key IS NULL
                              OR usc.Group_Key LIKE +'%' + @Group_Key + '%' )
                        AND ( @File_Name IS NULL
                              OR ( EXISTS ( SELECT
                                                1
                                            FROM
                                                dbo.UBM_BATCH_LOG ubl
                                            WHERE
                                                ubl.ZIP_FILE_NAME LIKE +'%' + @File_Name + '%'
                                                AND master_log.UBM_BATCH_MASTER_LOG_ID = ubl.UBM_BATCH_MASTER_LOG_ID )
                                   OR EXISTS ( SELECT
                                                1
                                               FROM
                                                dbo.UBM_FEED_FILE_MAP ffm
                                               WHERE
                                                ffm.FEED_FILE_NAME LIKE +'%' + @File_Name + '%'
                                                AND master_log.UBM_BATCH_MASTER_LOG_ID = ffm.UBM_BATCH_MASTER_LOG_ID )
                                   OR EXISTS ( SELECT
                                                1
                                               FROM
                                                UBM_Generic.dbo.UBM_Bill_Batch ubb
                                               WHERE
                                                ubb.FTS_Input_File_Name LIKE +'%' + @File_Name + '%'
                                                AND master_log.UBM_BATCH_MASTER_LOG_ID = ubb.UBM_Batch_Master_Log_Id ) ) ))
            SELECT
                  blf.BATCH_ID
                 ,blf.UBM_NAME
                 ,blf.START_DATE
                 ,blf.END_DATE
                 ,blf.STATUS
                 ,blf.EXPECTED_IMAGE_RECORDS
                 ,blf.ACTUAL_IMAGE_RECORDS
                 ,blf.BATCH_FAILURE_REASON
                 ,blf.DETAIL_FAILURE_REASON
                 ,blf.Total
                 ,blf.Group_Key
                 ,ubb.File_UNC_Path
                 ,ubb.FTS_Input_File_Name
            FROM
                  UBM_GET_BATCHLOG_FILES blf
                  LEFT JOIN UBM_Generic.dbo.UBM_Bill_Batch ubb
                        ON ubb.UBM_Batch_Master_Log_Id = blf.BATCH_ID
            WHERE
                  blf.Row_Num BETWEEN @StartIndex AND @EndIndex
            GROUP BY
                  blf.BATCH_ID
                 ,blf.UBM_NAME
                 ,blf.START_DATE
                 ,blf.END_DATE
                 ,blf.STATUS
                 ,blf.EXPECTED_IMAGE_RECORDS
                 ,blf.ACTUAL_IMAGE_RECORDS
                 ,blf.BATCH_FAILURE_REASON
                 ,blf.DETAIL_FAILURE_REASON
                 ,blf.Total
                 ,blf.Group_Key
                 ,ubb.File_UNC_Path
                 ,ubb.FTS_Input_File_Name
                 ,blf.Row_Num
            ORDER BY
                  blf.Row_Num
     
END

;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_BATCHLOG_FILES_P] TO [CBMSApplication]
GO
