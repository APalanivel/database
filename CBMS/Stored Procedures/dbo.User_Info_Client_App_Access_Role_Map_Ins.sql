SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
          
 NAME: [dbo].[User_Info_Client_App_Access_Role_Map_Ins]        
          
 DESCRIPTION:          
		To insert the values into dbo.User_Info_Client_App_Access_Role_Map  table.          
          
 INPUT PARAMETERS:          
         
 Name                               DataType            Default      Description          
---------------------------------------------------------------------------------------------------------------       
 @User_Info_Id                      INT
 @Assigned_User_Id                  INT
 @Client_App_Access_Role_Id         INT   
           
 OUTPUT PARAMETERS:         
                
 Name                               DataType            Default      Description          
---------------------------------------------------------------------------------------------------------------        
          
 USAGE EXAMPLES:              
---------------------------------------------------------------------------------------------------------------        
	 BEGIN TRAN  
	 EXEC dbo.User_Info_Client_App_Access_Role_Map_Ins  
			 @User_Info_Id=39151
			,@Client_App_Access_Role_Id =34
			,@Assigned_User_Id = 39151
	 select * from dbo.User_Info_Client_App_Access_Role_Map where  User_Info_Id=39184    
	 ROLLBACK TRAN          
         
 AUTHOR INITIALS:        
          
 Initials               Name          
---------------------------------------------------------------------------------------------------------------        
 NR                     Narayana Reddy            
           
 MODIFICATIONS:         
         
 Initials               Date            Modification        
---------------------------------------------------------------------------------------------------------------        
 NR                     2014-01-01      Created for RA Admin user management        
         
******/          
        
CREATE PROCEDURE [dbo].[User_Info_Client_App_Access_Role_Map_Ins]
      ( 
       @User_Info_Id INT
      ,@Client_App_Access_Role_Id INT
      ,@Assigned_User_Id INT )
AS 
BEGIN        
      SET NOCOUNT ON;        
         
      INSERT      INTO dbo.User_Info_Client_App_Access_Role_Map
                  ( 
                   User_Info_Id
                  ,Client_App_Access_Role_Id
                  ,Assigned_User_Id
                  ,Assigned_Ts )
      VALUES
                  ( 
                   @User_Info_Id
                  ,@Client_App_Access_Role_Id
                  ,@Assigned_User_Id
                  ,GETDATE() )        
  
                   
END 



;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Client_App_Access_Role_Map_Ins] TO [CBMSApplication]
GO
