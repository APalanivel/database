SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********           
NAME:  dbo.Account_Level_Variance_Test_SEL          
         
DESCRIPTION:  Used to select all the Account level data  for account configuration screen        
        
INPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
@Account_Id    Int        
@Commodity_Id   Int                 
            
OUTPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
            
USAGE EXAMPLES:          
         
 exec Account_Level_Variance_Test_SEL 68430,291,100175        
        
------------------------------------------------------------          
AUTHOR INITIALS:          
	Initials	Name          
------------------------------------------------------------          
	NK			Nageswara Rao Kosuri   
	AP			Arunkumar Palanivel
        
        
	Initials	Date		Modification          
------------------------------------------------------------          
	NK			10/13/2009  Created        
	HG			11/30/2009	Left join with Account_Variance_Consumption_Level and Variance_Consumption_Level tables is not required in the main query hence removed.        
	NK			12/28/2009	Order by Clause added to sort the record based on Variance_Test_Id.        
	AKR			2012-05-16	Added Variance_Category_Cd parameter. 
	RR			2016-11-30	VTE-56 Added optional input parameter @Is_DEO
	AP			Jan 17,2020	Changed procedure for Advance Variance Test Project. Account level variance test is only for rule based variance (legacy). so need to filter out only those records.
				added new declared parameter to filter out only rule based variance test
	AP			Jan 30,2020	Modified the procedure to support only utility accounts
******/

CREATE PROCEDURE [dbo].[Account_Level_Variance_Test_SEL]
      (
      @Account_Id           INT
    , @Commodity_Id         INT
    , @Variance_Category_Cd INT = NULL
    , @Is_DEO               INT = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE @Is_Data_Entry_Only BIT;
            DECLARE
                  @consumptionLevelId         INT
                , @Variance_Process_Engine_Id INT
                , @Account_Type               VARCHAR(20);

            SELECT
                  @Variance_Process_Engine_Id = VPE.Variance_Processing_Engine_Id
            FROM  dbo.Variance_Processing_Engine VPE
                  JOIN
                  dbo.Code C
                        ON VPE.Variance_Engine_Cd = C.Code_Id
            WHERE C.Code_Dsc = 'Rule Based Variance Test';

            SELECT
                  @Account_Type = cha.Account_Type
            FROM  Core.Client_Hier_Account cha
            WHERE cha.Account_Id = @Account_Id
                  AND   cha.Commodity_Id = @Commodity_Id;

            SELECT
                  @Is_Data_Entry_Only = isnull(Is_Data_Entry_Only, 0)
            FROM  dbo.ACCOUNT
            WHERE ACCOUNT_ID = @Account_Id;

            SELECT
                  @consumptionLevelId = avcl.Variance_Consumption_Level_Id
            FROM  dbo.Account_Variance_Consumption_Level avcl
                  JOIN
                  dbo.Variance_Consumption_Level vcl
                        ON avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
            WHERE avcl.ACCOUNT_ID = @Account_Id
                  AND   vcl.Commodity_Id = @Commodity_Id;

            IF ( @Account_Type = 'Utility' )
                  BEGIN

                        SELECT
                                    vt.Is_Inclusive
                                  , vt.Is_Multiple_Condition
                                  , vt.Variance_Test_Id
                                  , vr.Variance_Rule_Id
                                  , vrdtl.Variance_Rule_Dtl_Id
                                  , cd.Code_Value AS Category
                                  , vp.Parameter
                                  , cdo.Code_Value AS Operator
                                  , vr.Positive_Negative
                                  , cdb.Code_Dsc AS BaseLine
                                  , vrdtl.Default_Tolerance
                                  , vrdac.Tolerance AS Custom_Tolerance
                                  , map.Commodity_Id
                                  , vrdtl.Test_Description AS cons_level_Test_Description
                                  , isnull(vrdac.Test_Description, vrdtl.Test_Description) AS Test_Description
                                  , vrdtl.Is_Data_Entry_Only
                                  , Manual_Overriden = CASE WHEN ( vrdac.Tolerance IS NOT NULL )
                                                                 OR
                                                                       (     vrdac.Variance_Rule_Dtl_Id IS NULL
                                                                             AND   vrdtl.Variance_Rule_Dtl_Id IS NOT NULL
                                                                             AND   vrdtl.IS_Active = 1 )
                                                                 OR
                                                                       (     vrdtl.IS_Active = 0
                                                                             AND   vrdac.Variance_Rule_Dtl_Id IS NOT NULL
                                                                             AND   vrdac.Tolerance IS NOT NULL )
                                                                  THEN 1
                                                            ELSE  CASE WHEN (     vrdtl.IS_Active = 0
                                                                                  AND     vrdac.Variance_Rule_Dtl_Id IS NULL )
                                                                            OR
                                                                                  (     vrdtl.IS_Active = 1
                                                                                        AND   vrdac.Tolerance IS NULL )
                                                                             THEN 0
                                                                       ELSE  0
                                                                  END
                                                       END
                                  , IS_Active = CASE WHEN (     vrdac.Variance_Rule_Dtl_Id IS NULL
                                                                AND     vrdtl.Variance_Rule_Dtl_Id IS NOT NULL
                                                                AND     vrdtl.IS_Active = 1 )
                                                          OR
                                                                (     vrdtl.IS_Active = 0
                                                                      AND   vrdac.Variance_Rule_Dtl_Id IS NULL )
                                                           THEN 0
                                                     ELSE  CASE WHEN (     vrdtl.IS_Active = 1
                                                                           AND vrdac.Tolerance IS NULL )
                                                                     OR ( vrdac.Tolerance IS NOT NULL )
                                                                     OR
                                                                           (     vrdtl.IS_Active = 0
                                                                                 AND   vrdac.Variance_Rule_Dtl_Id IS NOT NULL
                                                                                 AND   vrdac.Tolerance IS NOT NULL )
                                                                      THEN 1
                                                                ELSE  0
                                                           END
                                                END
                        FROM        dbo.Variance_Test_Master vt
                                    INNER JOIN
                                    dbo.Variance_Test_Commodity_Map map
                                          ON map.Variance_Test_Id = vt.Variance_Test_Id
                                    INNER JOIN
                                    dbo.Variance_Rule vr
                                          ON vr.Variance_Test_Id = vt.Variance_Test_Id
                                    INNER JOIN
                                    dbo.Variance_Rule_Dtl vrdtl
                                          ON vrdtl.Variance_Rule_Id = vr.Variance_Rule_Id
                                    LEFT JOIN
                                    dbo.Variance_Rule_Dtl_Account_Override vrdac
                                          ON vrdac.Variance_Rule_Dtl_Id = vrdtl.Variance_Rule_Dtl_Id
                                             AND vrdac.ACCOUNT_ID = @Account_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter_Baseline_Map vpbmap
                                          ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter vp
                                          ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter_Commodity_Map VPCM
                                          ON VPCM.Commodity_Id = map.Commodity_Id
                                             AND VPCM.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                    INNER JOIN
                                    dbo.Code cd
                                          ON cd.Code_Id = vp.Variance_Category_Cd
                                    INNER JOIN
                                    dbo.Code cdo
                                          ON cdo.Code_Id = vr.Operator_Cd
                                    INNER JOIN
                                    dbo.Code cdb
                                          ON cdb.Code_Id = vpbmap.Baseline_Cd
                        WHERE       map.Commodity_Id = @Commodity_Id
                                    AND   VPCM.variance_process_Engine_id = @Variance_Process_Engine_Id
                                    AND
                                          (     (     @Is_Data_Entry_Only = 0
                                                      AND   vrdtl.Is_Data_Entry_Only = isnull(@Is_DEO, @Is_Data_Entry_Only))
                                                OR
                                                      (     @Is_Data_Entry_Only = 1
                                                            AND   vrdtl.Is_Data_Entry_Only = @Is_Data_Entry_Only
                                                            AND   vrdtl.Is_Data_Entry_Only = isnull(@Is_DEO, @Is_Data_Entry_Only)))
                                    AND   vrdtl.Variance_Consumption_Level_Id = @consumptionLevelId
                                    AND
                                          (     @Variance_Category_Cd IS NULL
                                                OR    vp.Variance_Category_Cd = @Variance_Category_Cd )
                        ORDER BY    vt.Variance_Test_Id;
                  END;


            ELSE
                  BEGIN

                        SELECT
                                    vt.Is_Inclusive
                                  , vt.Is_Multiple_Condition
                                  , vt.Variance_Test_Id
                                  , vr.Variance_Rule_Id
                                  , vrdtl.Variance_Rule_Dtl_Id
                                  , cd.Code_Value AS Category
                                  , vp.Parameter
                                  , cdo.Code_Value AS Operator
                                  , vr.Positive_Negative
                                  , cdb.Code_Dsc AS BaseLine
                                  , vrdtl.Default_Tolerance
                                  , vrdac.Tolerance AS Custom_Tolerance
                                  , map.Commodity_Id
                                  , vrdtl.Test_Description AS cons_level_Test_Description
                                  , isnull(vrdac.Test_Description, vrdtl.Test_Description) AS Test_Description
                                  , vrdtl.Is_Data_Entry_Only
                                  , Manual_Overriden = CASE WHEN ( vrdac.Tolerance IS NOT NULL )
                                                                 OR
                                                                       (     vrdac.Variance_Rule_Dtl_Id IS NULL
                                                                             AND   vrdtl.Variance_Rule_Dtl_Id IS NOT NULL
                                                                             AND   vrdtl.IS_Active = 1 )
                                                                 OR
                                                                       (     vrdtl.IS_Active = 0
                                                                             AND   vrdac.Variance_Rule_Dtl_Id IS NOT NULL
                                                                             AND   vrdac.Tolerance IS NOT NULL )
                                                                  THEN 1
                                                            ELSE  CASE WHEN (     vrdtl.IS_Active = 0
                                                                                  AND     vrdac.Variance_Rule_Dtl_Id IS NULL )
                                                                            OR
                                                                                  (     vrdtl.IS_Active = 1
                                                                                        AND   vrdac.Tolerance IS NULL )
                                                                             THEN 0
                                                                       ELSE  0
                                                                  END
                                                       END
                                  , IS_Active = CASE WHEN (     vrdac.Variance_Rule_Dtl_Id IS NULL
                                                                AND     vrdtl.Variance_Rule_Dtl_Id IS NOT NULL
                                                                AND     vrdtl.IS_Active = 1 )
                                                          OR
                                                                (     vrdtl.IS_Active = 0
                                                                      AND   vrdac.Variance_Rule_Dtl_Id IS NULL )
                                                           THEN 0
                                                     ELSE  CASE WHEN (     vrdtl.IS_Active = 1
                                                                           AND vrdac.Tolerance IS NULL )
                                                                     OR ( vrdac.Tolerance IS NOT NULL )
                                                                     OR
                                                                           (     vrdtl.IS_Active = 0
                                                                                 AND   vrdac.Variance_Rule_Dtl_Id IS NOT NULL
                                                                                 AND   vrdac.Tolerance IS NOT NULL )
                                                                      THEN 1
                                                                ELSE  0
                                                           END
                                                END
                        FROM        dbo.Variance_Test_Master vt
                                    INNER JOIN
                                    dbo.Variance_Test_Commodity_Map map
                                          ON map.Variance_Test_Id = vt.Variance_Test_Id
                                    INNER JOIN
                                    dbo.Variance_Rule vr
                                          ON vr.Variance_Test_Id = vt.Variance_Test_Id
                                    INNER JOIN
                                    dbo.Variance_Rule_Dtl vrdtl
                                          ON vrdtl.Variance_Rule_Id = vr.Variance_Rule_Id
                                    LEFT JOIN
                                    dbo.Variance_Rule_Dtl_Account_Override vrdac
                                          ON vrdac.Variance_Rule_Dtl_Id = vrdtl.Variance_Rule_Dtl_Id
                                             AND vrdac.ACCOUNT_ID = @Account_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter_Baseline_Map vpbmap
                                          ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                    INNER JOIN
                                    dbo.Variance_Parameter vp
                                          ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                    INNER JOIN
                                    dbo.Code cd
                                          ON cd.Code_Id = vp.Variance_Category_Cd
                                    INNER JOIN
                                    dbo.Code cdo
                                          ON cdo.Code_Id = vr.Operator_Cd
                                    INNER JOIN
                                    dbo.Code cdb
                                          ON cdb.Code_Id = vpbmap.Baseline_Cd
                        WHERE       map.Commodity_Id = @Commodity_Id
                                    AND
                                          (     (     @Is_Data_Entry_Only = 0
                                                      AND   vrdtl.Is_Data_Entry_Only = isnull(@Is_DEO, @Is_Data_Entry_Only))
                                                OR
                                                      (     @Is_Data_Entry_Only = 1
                                                            AND   vrdtl.Is_Data_Entry_Only = @Is_Data_Entry_Only
                                                            AND   vrdtl.Is_Data_Entry_Only = isnull(@Is_DEO, @Is_Data_Entry_Only)))
                                    AND   vrdtl.Variance_Consumption_Level_Id = @consumptionLevelId
                                    AND
                                          (     @Variance_Category_Cd IS NULL
                                                OR    vp.Variance_Category_Cd = @Variance_Category_Cd )
                        ORDER BY    vt.Variance_Test_Id;
                  END;
      END;


      ;
GO


GRANT EXECUTE ON  [dbo].[Account_Level_Variance_Test_SEL] TO [CBMSApplication]
GO
