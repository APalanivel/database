SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:                
      [dbo].[Notification_Template_Sel_By_Notification_Type]              
                
Description:                
				To return Default email subject, email body  from Notification_Template      
                
 Input Parameters:                
    Name                    DataType                Default Description                  
-----------------------------------------------------------------------------------------------                  
	@Notification_Type      VARCHAR(25)      
                 
Output Parameters:                      
 Name     Datatype  Default Description                      
------------------------------------------------------------                
                
Usage Examples:                    
------------------------------------------------------------  
                    
  EXEC [dbo].[Notification_Template_Sel_By_Notification_Type]  @Notification_Type='Create New User'

               
Author Initials:                
    Initials  Name                
------------------------------------------------------------                
    RR        Raghu Reddu     
             
Modifications:                
    Initials	Date        Modification                
------------------------------------------------------------                
    RR			2014-01-27    Created            
          
          
             
******/     
CREATE PROCEDURE [dbo].[Notification_Template_Sel_By_Notification_Type]
      ( 
       @Notification_Type VARCHAR(25) )
AS 
BEGIN    
      SET NOCOUNT ON    
     
      SELECT
            nt.Default_Email_Subject
           ,nt.Default_Email_Body
      FROM
            dbo.Notification_Template nt
            INNER JOIN dbo.Code c
                  ON nt.Notification_Type_Cd = c.Code_Id
      WHERE
            c.Code_Value = @Notification_Type
            
               
END 

;
GO
GRANT EXECUTE ON  [dbo].[Notification_Template_Sel_By_Notification_Type] TO [CBMSApplication]
GO
