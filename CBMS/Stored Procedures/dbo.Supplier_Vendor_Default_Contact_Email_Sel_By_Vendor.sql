SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  

NAME: 
	dbo.Supplier_Vendor_Default_Contact_Email_Sel_By_Vendor  
  
DESCRIPTION:   
  
INPUT PARAMETERS:      
	Name            DataType          Default     Description      
---------------------------------------------------------------------------------      
	@Vendor_Id		INT
                             
OUTPUT PARAMETERS:           
      Name						DataType          Default     Description      
---------------------------------------------------------------------------------      
	  @srSupplierContactInfoId	INT				  
  
  
USAGE EXAMPLES:  
--------------------------------------------------------------------------------- 
 
		SELECT DISTINCT VENDOR_ID FROM dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP
		
		SELECT DISTINCT ssci.Is_Default_Termination_Contact_Email_For_Vendor,ssci.Termination_Contact_Email_Address
				,ssci.Is_Default_Registration_Contact_Email_For_Vendor,ssci.Registration_Contact_Email_Address
		FROM dbo.SR_SUPPLIER_CONTACT_INFO ssci INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm 
		ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = scvm.SR_SUPPLIER_CONTACT_INFO_ID
		WHERE scvm.VENDOR_ID = 261
		
		EXEC dbo.Supplier_Vendor_Default_Contact_Email_Sel_By_Vendor 261
  
AUTHOR INITIALS:  
	Initials	Name  
--------------------------------------------------------------------------------- 
	RR			Raghu Reddy

MODIFICATIONS  
  
	Initials	Date		Modification  
--------------------------------------------------------------------------------- 
	RR			2015-10-19	Global Sourcing - Phase2 - Created

******/
CREATE PROCEDURE [dbo].[Supplier_Vendor_Default_Contact_Email_Sel_By_Vendor] ( @Vendor_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Termination_Contact_Email_Address NVARCHAR(MAX)
           ,@Registration_Contact_Email_Address NVARCHAR(MAX)
            
                 
      SELECT
            @Termination_Contact_Email_Address = ssci.Termination_Contact_Email_Address
      FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
      WHERE
            scvm.VENDOR_ID = @Vendor_Id
            AND ssci.Is_Default_Termination_Contact_Email_For_Vendor = 1
            
      SELECT
            @Registration_Contact_Email_Address = ssci.Registration_Contact_Email_Address
      FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
      WHERE
            scvm.VENDOR_ID = @Vendor_Id
            AND ssci.Is_Default_Registration_Contact_Email_For_Vendor = 1
            
      SELECT
            @Termination_Contact_Email_Address AS Default_Termination_Contact_Email_Address
           ,@Registration_Contact_Email_Address AS Default_Registration_Contact_Email_Address
      

END;
GO
GRANT EXECUTE ON  [dbo].[Supplier_Vendor_Default_Contact_Email_Sel_By_Vendor] TO [CBMSApplication]
GO
