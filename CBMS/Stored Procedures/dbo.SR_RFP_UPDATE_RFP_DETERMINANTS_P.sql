SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_RFP_DETERMINANTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int,
	@determinant_name varchar(50),
	@determinant_unit_type_id int,
	@is_checked bit,
	@vendor_id int,
	@rfp_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- select * from sr_rfp_load_profile_determinant(nolock) where sr_rfp_load_profile_determinant_id = 59
-- update sr_rfp_load_profile_determinant set UPDATED_DETERMINANT_UNIT_TYPE_ID = DETERMINANT_UNIT_TYPE_ID, UPDATED_IS_CHECKED = IS_CHECKED

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_UPDATE_RFP_DETERMINANTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int,
	@determinant_name varchar(50),
	@determinant_unit_type_id int,
	@is_checked bit,
	@vendor_id int,
	@rfp_id int
	AS
	
set nocount on
	
	Declare @sr_rfp_load_profile_determinant_id int
	if(@determinant_id > 0)
	begin
		declare @old_determinant_name varchar(50), @updated_is_checked bit, @updated_unit_type_id int

		select  @old_determinant_name = determinant_name,
			@updated_is_checked =  updated_is_checked,
			@updated_unit_type_id =  updated_determinant_unit_type_id
		from sr_rfp_load_profile_determinant(nolock) 
		where sr_rfp_load_profile_determinant_id = @determinant_id
		
		if(@determinant_name != @old_determinant_name) OR (@updated_is_checked != @is_checked) OR (@updated_unit_type_id != @determinant_unit_type_id)
		begin
			update	sr_rfp_load_profile_determinant 
			set 	determinant_name = @determinant_name,
				updated_determinant_unit_type_id = @determinant_unit_type_id,
				updated_is_checked = @is_checked
			
			from	
				sr_rfp_load_profile_determinant determinant(nolock),
				sr_rfp_load_profile_setup setup(nolock),
				sr_rfp_account rfp_acc(nolock),
				account acc(nolock),
				vendor ven(nolock)
			
			where 	setup.sr_rfp_id = @rfp_id
				and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
				and determinant.determinant_name = @old_determinant_name
				and rfp_acc.sr_rfp_id = setup.sr_rfp_id
				and rfp_acc.sr_rfp_account_id = setup.sr_rfp_account_id
				and acc.account_id = rfp_acc.account_id
				and ven.vendor_id = acc.vendor_id
				and acc.vendor_id = @vendor_id

		end

	end
	else --//insert new determinants  for all accounts belong to the utility for the RFP
	begin
		declare @determinant_type_id int
		select @determinant_type_id = entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'New'
		DECLARE C_LP_SETUP CURSOR FAST_FORWARD
		FOR
	
		select 	setup.sr_rfp_load_profile_setup_id
		from	
			sr_rfp_load_profile_setup setup(nolock),
			sr_rfp_account rfp_acc(nolock),
			account acc(nolock),
			vendor ven(nolock)
		
		where 	setup.sr_rfp_id = @rfp_id
			and rfp_acc.sr_rfp_id = setup.sr_rfp_id
			and rfp_acc.sr_rfp_account_id = setup.sr_rfp_account_id
			and acc.account_id = rfp_acc.account_id
			and rfp_acc.is_deleted= 0
			and ven.vendor_id = acc.vendor_id
			and acc.vendor_id = @vendor_id

		declare @sr_rfp_load_profile_setup_id int

		OPEN C_LP_SETUP
		
		FETCH NEXT FROM C_LP_SETUP INTO @sr_rfp_load_profile_setup_id
		WHILE (@@fetch_status <> -1)
		BEGIN
			IF (@@fetch_status <> -2)
			BEGIN
				insert into sr_rfp_load_profile_determinant
				(sr_rfp_load_profile_setup_id, determinant_name, determinant_unit_type_id, updated_determinant_unit_type_id,  updated_is_checked,determinant_type_id) 
			 	values(@sr_rfp_load_profile_setup_id, @determinant_name,@determinant_unit_type_id,@determinant_unit_type_id,  @is_checked, @determinant_type_id)
			END
			FETCH NEXT FROM C_LP_SETUP INTO @sr_rfp_load_profile_setup_id
		END
		CLOSE C_LP_SETUP
		DEALLOCATE C_LP_SETUP

	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_RFP_DETERMINANTS_P] TO [CBMSApplication]
GO
