SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Contract_Budget_History_Del_For_Contract]  
     
DESCRIPTION: 

	To Deletes  Budget Contract Budget History associated with the given Contract Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Contract_Id	INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN
		EXEC Budget_Contract_Budget_History_Del_For_Contract  13349
	ROLLBACK TRAN

	BEGIN TRAN
		EXEC Budget_Contract_Budget_History_Del_For_Contract  12435
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			06/22/2010	Created

*/

CREATE PROCEDURE dbo.Budget_Contract_Budget_History_Del_For_Contract
	 @Contract_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @Budget_Contract_Budget_Id			INT
			,@Budget_Contract_Budget_Month_Id	INT
			,@Budget_Contract_Budget_Detail_Id	INT
			,@Budget_Contract_Defaults_Id		INT
	
	DECLARE @Budget_Contract_Budget_List TABLE(Budget_Contract_Budget_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Budget_Contract_Budget_Months_List TABLE (Budget_Contract_Budget_Month_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Budget_Contract_Budget_Detail_List TABLE(Budget_Contract_Budget_Detail_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Budget_Contract_Defaults_List TABLE(Budget_Contract_Defaults_Id INT PRIMARY KEY CLUSTERED)
	
	INSERT INTO @Budget_Contract_Budget_List(Budget_Contract_Budget_Id)
	SELECT
		Budget_Contract_Budget_Id
	FROM
		dbo.Budget_Contract_Budget
	WHERE
		Contract_Id =  @Contract_Id
		
	INSERT INTO @Budget_Contract_Budget_Months_List(Budget_Contract_Budget_Month_Id)
	SELECT
		bcbm.Budget_Contract_Budget_Month_Id
	FROM 
		dbo.Budget_Contract_Budget_Months bcbm
		JOIN @Budget_Contract_Budget_List bcbl
			 ON bcbm.Budget_Contract_Budget_Id = bcbl.Budget_Contract_Budget_Id
		
	INSERT INTO @Budget_Contract_Budget_Detail_List(Budget_Contract_Budget_Detail_Id)
	SELECT 
		bcbd.Budget_Contract_Budget_Detail_Id
	FROM 
		dbo.Budget_Contract_Budget_Detail bcbd
		JOIN @Budget_Contract_Budget_Months_List bcbm
			 ON bcbd.Budget_Contract_Budget_Month_Id =	bcbm.Budget_Contract_Budget_Month_Id
		
	INSERT INTO @Budget_Contract_Defaults_List(Budget_Contract_Defaults_Id)		
	SELECT 
		bcd.Budget_Contract_Defaults_Id
	FROM 
		dbo.Budget_Contract_Defaults bcd
		JOIN @Budget_Contract_Budget_List bcbl
			 ON bcd.Budget_Contract_Budget_Id = bcbl.Budget_Contract_Budget_Id
	
	BEGIN TRY
		BEGIN TRAN 
		
			WHILE EXISTS(SELECT 1 FROM @Budget_Contract_Budget_Detail_List)
				BEGIN

					SET @Budget_Contract_Budget_Detail_Id = (SELECT TOP 1 Budget_Contract_Budget_Detail_Id FROM @Budget_Contract_Budget_Detail_List)

					EXEC dbo.Budget_Contract_Budget_Detail_Del @Budget_Contract_Budget_Detail_Id

					DELETE
						@Budget_Contract_Budget_Detail_List
					WHERE
						Budget_Contract_Budget_Detail_Id = @Budget_Contract_Budget_Detail_Id

				END		

			WHILE EXISTS(SELECT 1 FROM @Budget_Contract_Budget_Months_List)
				BEGIN

					SET @Budget_Contract_Budget_Month_Id = (SELECT TOP 1 Budget_Contract_Budget_Month_Id FROM @Budget_Contract_Budget_Months_List)

					EXEC dbo.Budget_Contract_Budget_Months_Del @Budget_Contract_Budget_Month_Id

					DELETE
						@Budget_Contract_Budget_Months_List
					WHERE
						Budget_Contract_Budget_Month_Id = @Budget_Contract_Budget_Month_Id
				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Contract_Defaults_List)
				BEGIN

					SET @Budget_Contract_Defaults_Id = (SELECT TOP 1  Budget_Contract_Defaults_Id FROM @Budget_Contract_Defaults_List)

					EXEC dbo.Budget_Contract_Defaults_Del @Budget_Contract_Defaults_Id

					DELETE
						@Budget_Contract_Defaults_List
					WHERE
						Budget_Contract_Defaults_Id = @Budget_Contract_Defaults_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Budget_Contract_Budget_List)
				BEGIN

					SET @Budget_Contract_Budget_Id = (SELECT TOP 1 Budget_Contract_Budget_Id FROM @Budget_Contract_Budget_List)

					EXEC dbo.Budget_Contract_Budget_Del @Budget_Contract_Budget_Id

					DELETE
						@Budget_Contract_Budget_List
					WHERE
						Budget_Contract_Budget_Id = @Budget_Contract_Budget_Id

				END

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END

		EXEC dbo.usp_RethrowError

	END CATCH
END
GO
GRANT EXECUTE ON  [dbo].[Budget_Contract_Budget_History_Del_For_Contract] TO [CBMSApplication]
GO
