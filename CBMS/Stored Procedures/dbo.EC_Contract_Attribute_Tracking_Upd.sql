SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.EC_Contract_Attribute_Tracking_Upd

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Account_Id				INT
	@Comment_Type			VARCHAR(25)
	@Comment_User_Info_Id	INT
	@Comment_Dt				DATE
	@Comment_Text			VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	
	BEGIN TRANSACTION
		
		DECLARE @Tracking_Id INT
		SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE Contract_Id = 10866
		EXEC dbo.EC_Contract_Attribute_Tracking_Ins 10866, 2, '2017-01-01', '2017-12-31', 'Test_1_Test', 16
		SELECT @Tracking_Id = EC_Contract_Attribute_Tracking_Id FROM dbo.EC_Contract_Attribute_Tracking WHERE Contract_Id = 10866 
				AND EC_Contract_Attribute_Id = 2 AND EC_Contract_Attribute_Value = 'Test_1_Test'
		SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Tracking_Id = @Tracking_Id
		EXEC dbo.EC_Contract_Attribute_Tracking_Upd @Tracking_Id, 2, 'Test_2_Test', '2017-01-01', '2017-12-31', 16
		SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Tracking_Id = @Tracking_Id
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV			Ravi kumar vegesna
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RKV       	2015-09-16	Created for AS400-PII

******/

CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Tracking_Upd]
      ( 
       @EC_Contract_Attribute_Tracking_Id INT
      ,@EC_Contract_Attribute_Id INT
      ,@EC_Contract_Attribute_Value NVARCHAR(255)
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            mat
      SET   
            EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
           ,EC_Contract_Attribute_Value = @EC_Contract_Attribute_Value
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = getdate()
      FROM
            dbo.EC_Contract_Attribute_Tracking mat
      WHERE
            mat.EC_Contract_Attribute_Tracking_Id = @EC_Contract_Attribute_Tracking_Id
      
                  
END;
;
;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Tracking_Upd] TO [CBMSApplication]
GO
