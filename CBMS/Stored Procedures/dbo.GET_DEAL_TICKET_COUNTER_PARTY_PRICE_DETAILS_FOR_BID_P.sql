SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS_FOR_BID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealTicketId  	int       	          	
	@counterpartyID	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS_FOR_BID_P 106940, 33


CREATE   PROCEDURE DBO.GET_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS_FOR_BID_P 

@dealTicketId int,
@counterpartyID int

AS
set nocount on
	SELECT *  FROM RM_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS
	WHERE RM_DEAL_TICKET_ID = @dealTicketId AND
	RM_COUNTERPARTY_ID =  @counterpartyID AND
	IS_SELECTED_COUNTERPARTY = 1
	order by month_identifier
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS_FOR_BID_P] TO [CBMSApplication]
GO
