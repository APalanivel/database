
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: dbo.Budget_Usage_Ins_Upd
    
DESCRIPTION:    
    
     This procedure will be used to save the Budget Usage data.     
    
INPUT PARAMETERS:    
      NAME					DATATYPE				DEFAULT           DESCRIPTION    
------------------------------------------------------------------------------------------------------------------------	
    @Tvp_Budget_Usage		tvp_Budget_Usage	
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
                       
USAGE EXAMPLES:              
------------------------------------------------------------------------------------------------------------------------          
      
    BEGIN TRAN 
        	
	DECLARE @Tvp_Budget_Usage tvp_Budget_Usage
	INSERT INTO @Tvp_Budget_Usage VALUES (533046,'2014-03-01',2000)
	EXEC dbo.Budget_Usage_Ins_Upd @Tvp_Budget_Usage, 'Electric Power','kWh'
	
	ROLLBACK TRAN   
       
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------------------------------------------------------------------             
      RMG         Rani Mary George
	  KVK		  Vinay Kacham
	  TP		  Anoop
              
MODIFICATIONS:    
      INITIALS    DATE   MODIFICATION              
------------------------------------------------------------------------------------------------------------------------              
      RMG 		  03/07/2013	 CREATED
	  KVK		  05/10/2013	 modifed the queries to have performance
	  RMG         05/28/2013     modified to add a condition to check whether volume IS NULL in Tvp or in Budget_Usage table
	  TP		  2016-11-03	 MAINT-4569 - added tgt.VOLUME_UNIT_TYPE_ID in merge update query
******/  

CREATE PROCEDURE [dbo].[Budget_Usage_Ins_Upd]
      ( 
       @Tvp_Budget_Usage tvp_Budget_Usage READONLY
      ,@Commodity_Name VARCHAR(50)
      ,@Volume_Unit_Name VARCHAR(25) )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE @Manual_Budget_Usage_Type_Id INT
      DECLARE @LastYear_Budget_Usage_Type_Id INT
      DECLARE @2YearsAgo_Budget_Usage_Type_Id INT
      DECLARE @3Years_Ago_Budget_Usage_Type_Id INT    
      
      DECLARE @Commodity_Id INT
      DECLARE @Volume_Unit_Type_Id INT

      DECLARE @Bucket_Master_Id INT	

		--To get Budget usage type Id
      SELECT
            @Manual_Budget_Usage_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = 'Manual'
            AND enty.ENTITY_DESCRIPTION = 'BUDGET_USAGE_TYPE' 

      SELECT
            @LastYear_Budget_Usage_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = 'Last Year'
            AND enty.ENTITY_DESCRIPTION = 'BUDGET_USAGE_TYPE' 


      SELECT
            @2YearsAgo_Budget_Usage_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = '2 Years Ago'
            AND enty.ENTITY_DESCRIPTION = 'BUDGET_USAGE_TYPE' 

      SELECT
            @3Years_Ago_Budget_Usage_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = '3 Years Ago '
            AND enty.ENTITY_DESCRIPTION = 'BUDGET_USAGE_TYPE' 


		--To get Commodity Id	
      SELECT
            @Commodity_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = @Commodity_Name
            AND enty.ENTITY_DESCRIPTION = 'Commodity'
        
            		
		--To get Volume unit type id based on the Commodity name
      SELECT
            @Volume_Unit_Type_Id = enty.ENTITY_ID
      FROM
            dbo.ENTITY enty
      WHERE
            enty.ENTITY_NAME = @Volume_Unit_Name
            AND enty.ENTITY_DESCRIPTION = case @Commodity_Name
                                            WHEN 'Electric Power' THEN 'Unit for electricity'
                                            WHEN 'Natural Gas' THEN 'Unit for Gas'
                                          END;

      SELECT
            @Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Bucket_Master AS bm
      WHERE
            bm.Commodity_Id = @commodity_id
            AND bm.Bucket_Name = 'Total Usage'

      DECLARE
            @start_dt DATE
           ,@end_dt DATE
  
      SET @start_dt = ( SELECT
                              dateadd(year, -3, min(Month_Identifier))
                        FROM
                              @Tvp_Budget_Usage )  
      SET @end_dt = ( SELECT
                        dateadd(year, -1, max(Month_Identifier))
                      FROM
                        @Tvp_Budget_Usage )  
	
	-- get distinct AccountId's
      DECLARE @Accounts AS TABLE ( Account_Id INT )
      INSERT      INTO @Accounts
                  ( 
                   Account_Id )
                  SELECT
                        Account_Id
                  FROM
                        @Tvp_Budget_Usage
                  GROUP BY
                        Account_Id

      DECLARE @Final_Result AS TABLE
            ( 
             Account_Id INT
            ,Month_Identifier SMALLDATETIME
            ,Volume DECIMAL(32, 16)
            ,Budget_Usage_Type_Id INT )


	  --check is there any difference in volume
      INSERT      INTO @Final_Result
                  ( 
                   Account_Id
                  ,Month_Identifier
                  ,Volume
                  ,Budget_Usage_Type_Id )
                  SELECT
                        tbu.Account_Id
                       ,tbu.Month_Identifier
                       ,tbu.Volume
                       ,@Manual_Budget_Usage_Type_Id
                  FROM
                        @Tvp_Budget_Usage tbu
                        INNER JOIN dbo.BUDGET_USAGE AS bu
                              ON tbu.Account_Id = bu.ACCOUNT_ID
                                 AND tbu.Month_Identifier = bu.MONTH_IDENTIFIER
                                 AND bu.COMMODITY_TYPE_ID = @Commodity_Id
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = bu.VOLUME_UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = @Volume_Unit_Type_Id
                  WHERE
                        tbu.Volume != [dbo].[ufn_Decimal_Format_Half_Even](bu.VOLUME * cuc.CONVERSION_FACTOR, 0)
                        OR tbu.Volume IS NULL
                        OR bu.Volume IS NULL
  

      CREATE TABLE #Account_3year_Usage
            ( 
             Account_Id INT
            ,Month_Identifier SMALLDATETIME
            ,Volume DECIMAL(32, 16) )
     
	 
      INSERT      INTO #Account_3year_Usage
                  ( 
                   Account_Id
                  ,Month_Identifier
                  ,Volume )
                  SELECT
                        cuad.ACCOUNT_ID
                       ,cuad.Service_Month
                       ,[dbo].[ufn_Decimal_Format_Half_Even](cuad.Bucket_Value * cuc.Conversion_Factor, 0)
                  FROM
                        dbo.Cost_Usage_Account_Dtl AS cuad
                        INNER JOIN ( SELECT
                                          cua.Account_Id
                                         ,month(cua.Service_Month) [smonth]
                                         ,max(cua.Service_Month) Service_Month
                                     FROM
                                          @Accounts tvp
                                          INNER JOIN dbo.Cost_Usage_Account_Dtl cua
                                                ON cua.ACCOUNT_ID = tvp.Account_Id
                                                   AND cua.Service_Month BETWEEN @start_dt AND @end_dt
                                          LEFT JOIN @Final_Result fr
                                                ON fr.Account_Id = cua.Account_Id
                                                   AND fr.Month_Identifier = cua.Service_Month
                                     WHERE
                                          fr.Account_Id IS NULL
                                          AND cua.Bucket_Master_Id = @Bucket_Master_Id
                                     GROUP BY
                                          cua.Account_Id
                                         ,month(cua.Service_Month) ) cu_acc
                              ON cuad.ACCOUNT_ID = cu_acc.ACCOUNT_ID
                                 AND cuad.Service_Month = cu_acc.Service_Month
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                                 AND cuc.CONVERTED_UNIT_ID = @Volume_Unit_Type_Id
                  WHERE
                        cuad.Bucket_Master_Id = @Bucket_Master_Id


      INSERT      INTO @Final_Result
                  ( 
                   Account_Id
                  ,Month_Identifier
                  ,Volume
                  ,Budget_Usage_Type_Id )
                  SELECT
                        tbu.account_id
                       ,tbu.month_identifier
                       ,tbu.volume
                       ,case WHEN dateadd(year, -1, tbu.month_identifier) = au.month_identifier
                                  AND tbu.volume = au.Volume THEN @LastYear_Budget_Usage_Type_Id
                             WHEN dateadd(year, -2, tbu.month_identifier) = au.month_identifier
                                  AND tbu.volume = au.Volume THEN @2YearsAgo_Budget_Usage_Type_Id
                             WHEN dateadd(year, -3, tbu.month_identifier) = au.month_identifier
                                  AND tbu.volume = au.Volume THEN @3Years_Ago_Budget_Usage_Type_Id
                             ELSE @Manual_Budget_Usage_Type_Id
                        END AS Budget_Usage_Type_Id
                  FROM
                        @Tvp_Budget_Usage tbu
                        LEFT JOIN #Account_3year_Usage AS au
                              ON tbu.account_id = au.Account_Id
                                 AND month(tbu.month_identifier) = month(au.Month_Identifier)
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.BUDGET_USAGE AS bu
                                     WHERE
                                          bu.Account_Id = tbu.Account_Id
                                          AND bu.Month_Identifier = tbu.Month_Identifier
                                          AND bu.COMMODITY_TYPE_ID = @Commodity_Id )

      MERGE dbo.BUDGET_USAGE tgt
            USING @Final_Result AS src
            ON tgt.COMMODITY_TYPE_ID = @Commodity_Id
                  AND tgt.ACCOUNT_ID = src.Account_Id
                  AND tgt.MONTH_IDENTIFIER = src.Month_Identifier
            WHEN MATCHED 
                  THEN
				UPDATE
                    SET 
                        tgt.VOLUME = src.Volume
                       ,tgt.BUDGET_USAGE_TYPE_ID = src.Budget_Usage_Type_Id
                       ,tgt.VOLUME_UNIT_TYPE_ID = @Volume_Unit_Type_Id
            WHEN NOT MATCHED 
                  THEN
						INSERT
                        ( 
                         ACCOUNT_ID
                        ,COMMODITY_TYPE_ID
                        ,BUDGET_USAGE_TYPE_ID
                        ,MONTH_IDENTIFIER
                        ,VOLUME
                        ,VOLUME_UNIT_TYPE_ID )
                    VALUES
                        ( 
                         src.Account_Id
                        ,@Commodity_Id
                        ,src.Budget_Usage_Type_Id
                        ,src.Month_Identifier
                        ,src.Volume
                        ,@Volume_Unit_Type_Id );	

END;


;
GO

GRANT EXECUTE ON  [dbo].[Budget_Usage_Ins_Upd] TO [CBMSApplication]
GO
