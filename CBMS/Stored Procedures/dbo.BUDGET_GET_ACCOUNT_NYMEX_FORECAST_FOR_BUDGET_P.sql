SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--exec BUDGET_GET_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P '','',59,'06/01/2008', '31/05/2009'
CREATE     PROCEDURE dbo.BUDGET_GET_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int,
	@from_month datetime, 
	@to_month datetime
	AS
	begin
	set nocount on

	select	budget_nymex_forecast.month_identifier as monthIdentifier,
		budget_nymex_forecast.price as forecastPrice,
		budget_account.budget_account_id as budgetAccountId
 
	from 	budget join budget_account on budget_account.budget_id = budget.budget_id
		and budget_account.budget_id = @budget_id		 
		join budget_nymex_forecast on budget_nymex_forecast.account_id = budget_account.account_id		
		and budget_nymex_forecast.commodity_type_id = budget.commodity_type_id
		and budget_nymex_forecast.month_identifier between @from_month and @to_month
	end














GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P] TO [CBMSApplication]
GO
