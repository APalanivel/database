SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_AC_VALUE_P
	@charge_master_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT charge_value 
	FROM dbo.additional_charge 
	WHERE charge_master_id = @charge_master_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_AC_VALUE_P] TO [CBMSApplication]
GO
