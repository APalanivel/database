SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Utility_Vendor_Sel_By_State_Commodity_List

DESCRIPTION:
			To get utility vendors providing all the given input utilities in a particular state

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
-- Zero results means no single vendor providing all the given services in the given state

	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 38,'67,1585,291'
	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 38,'67,1585,58'
	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 128,'67,1585'
	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 128,'67,1585,290,291,100012,100029,58'
	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 128,'67,1585,290,291,100012,100029,58,1559'
	EXEC dbo.Utility_Vendor_Sel_By_State_Commodity_List 128,'290,291'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2013-10-15	MAINT-2127 Created

******/
CREATE PROCEDURE dbo.Utility_Vendor_Sel_By_State_Commodity_List
      ( 
       @State_Id INT
      ,@Commodity_Id_List VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Commodity_Id_Tbl TABLE ( Commodity_Id INT )
      DECLARE @Commodity_Count INT
      
      INSERT      INTO @Commodity_Id_Tbl
                  ( 
                   Commodity_Id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Commodity_Id_List, ',')
                        
      SELECT
            @Commodity_Count = count(Commodity_Id)
      FROM
            @Commodity_Id_Tbl;
      WITH  Cte_Vendors
              AS ( SELECT
                        ven.VENDOR_ID
                       ,ven.VENDOR_NAME
                       ,cmap.COMMODITY_TYPE_ID
                   FROM
                        dbo.VENDOR ven
                        INNER JOIN dbo.ENTITY typ
                              ON ven.VENDOR_TYPE_ID = typ.ENTITY_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP map
                              ON map.VENDOR_ID = ven.VENDOR_ID
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP cmap
                              ON cmap.VENDOR_ID = map.VENDOR_ID
                        INNER JOIN @Commodity_Id_Tbl com
                              ON cmap.COMMODITY_TYPE_ID = com.Commodity_Id
                   WHERE
                        ven.IS_HISTORY = 0
                        AND typ.ENTITY_NAME = 'Utility'
                        AND typ.ENTITY_DESCRIPTION = 'Vendor'
                        AND map.STATE_ID = @State_Id)
            SELECT
                  cv.VENDOR_ID
                 ,cv.VENDOR_NAME
            FROM
                  Cte_Vendors cv
            GROUP BY
                  cv.VENDOR_ID
                 ,cv.VENDOR_NAME
            HAVING
                  count(cv.COMMODITY_TYPE_ID) >= @Commodity_Count
            ORDER BY
                  cv.VENDOR_NAME

END
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Vendor_Sel_By_State_Commodity_List] TO [CBMSApplication]
GO
