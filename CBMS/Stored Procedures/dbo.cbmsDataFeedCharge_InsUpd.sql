SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******                                                          
  
 NAME:  dbo.[cbmsDataFeedCharge_InsUpd]                                              
                                                          
 DESCRIPTION:                                                          
   to mapping a new/existing ubm bucket code  to cbm bucket code
                                                         
 AUTHOR INITIALS:                                          
                                         
 Initials              Name                                          
---------------------------------------------------------------------------------------------------------------                                                        
 PK	                    Prasan kumar       
                                                           
 MODIFICATIONS:                                        
                                            
 Initials   Date     Modification                                        
------------  ----------    -----------------------------------------------------------------------------------------                                        
    
                                   
******/                
CREATE PROCEDURE [dbo].[cbmsDataFeedCharge_InsUpd]           
 (             
 @umb_id int   ,         
 @commodity_id int ,    
 @ubm_bucket_code varchar(255),    
 @cbm_bucket_master_id int,  
 @user_id int     
 )              
AS              
BEGIN              
	
	DECLARE @ubm_bucket_charge_map TABLE ( ubm_bucket_charge_map_id int )
	DECLARE @feedvalues TABLE 
	( 
      umb_id int   ,         
		commodity_id int ,    
		ubm_bucket_code varchar(255),    
		cbm_bucket_master_id int
	)

	 BEGIN TRY  
       BEGIN TRAN

			INSERT INTO @feedvalues values (@umb_id, @commodity_id, @ubm_bucket_code,@cbm_bucket_master_id )



			MERGE [dbo].UBM_BUCKET_CHARGE_MAP AS tar
			USING @feedvalues AS src
			ON tar.UBM_ID = src.umb_id
				and tar.COMMODITY_TYPE_ID = src.commodity_id
				and tar.ubm_bucket_code = src.UBM_BUCKET_CODE
			WHEN MATCHED THEN
			   UPDATE SET
			      tar.Bucket_Master_Id = src.cbm_bucket_master_id,
				  tar.Updated_User_Id = 	@user_id ,  
				  tar.Last_Change_Ts =    getdate()
			WHEN NOT MATCHED THEN
			   INSERT
			   (
					UBM_ID  
					,UBM_BUCKET_CODE  
					,COMMODITY_TYPE_ID  
					,Bucket_Master_Id  
					,Created_User_Id  
					,Created_Ts  
					,Updated_User_Id  
					,Last_Change_Ts  
				)  
			   VALUES
			   (
					src.umb_id, 
					src.UBM_BUCKET_CODE, 
					src.commodity_id, 
					src.cbm_bucket_master_id,
					@user_id,  
					getdate(),  
					@user_id,  
					getdate() 
			   )
			   OUTPUT inserted.UBM_BUCKET_CHARGE_MAP_ID INTO @ubm_bucket_charge_map;


			   DECLARE @map_id INT

			   SELECT @map_id = ubm_bucket_charge_map_id FROM @ubm_bucket_charge_map
			   EXEC [Ubm_Sub_Bucket_Map_Re_Load_For_Bucket_Change] @map_id,@user_id

		COMMIT TRAN
        END TRY  
        BEGIN CATCH  
            IF @@TRANCOUNT > 0  
                BEGIN  
                    ROLLBACK TRAN;  
                END;  

			 
			 INSERT INTO [dbo].[StoredProc_Error_Log]          
                 (          
                     [StoredProc_Name]          
                     , [Error_Line]          
                     , [Error_message]          
                     , [Input_Params]          
                 )          
            VALUES         
                ('[cbmsDataFeedCharge_InsUpd] -->'  +  ERROR_PROCEDURE()  
                 ,ERROR_NUMBER()        
                 ,ERROR_MESSAGE()           
                 ,'ubm id: ' + @umb_id +' UBM_BUCKET_CODE '+@UBM_BUCKET_CODE +' commodity_id '
					+@commodity_id+' cbm_bucket_master_id ' + @cbm_bucket_master_id);          
  
            EXEC dbo.usp_RethrowError;  
        END CATCH; 
END     

GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedCharge_InsUpd] TO [CBMSApplication]
GO
