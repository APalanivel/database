SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_UPDATE_VENDOR_COMMENTS_P    

DESCRIPTION:  This procedure used to update the records in Utility_detail table table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@comments varchar(1000),
@vendor_id int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    

Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
DR    8/4/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
******/
CREATE PROCEDURE [dbo].[CBMS_UPDATE_VENDOR_COMMENTS_P] 

@comments varchar(1000),
@vendor_id int

AS


UPDATE 
	UTILITY_DETAIL 
SET 
	UTILITY_COMMENTS = @comments 
WHERE 
	VENDOR_ID = @vendor_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_VENDOR_COMMENTS_P] TO [CBMSApplication]
GO
