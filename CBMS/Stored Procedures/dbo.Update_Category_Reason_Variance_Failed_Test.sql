SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/******        
NAME:      
 dbo.Update_Category_Reason_Variance_Failed_Test

DESCRIPTION:
		Used to update variance closure category and reason for the failed test
		
 INPUT PARAMETERS:      
	Name			DataType  Default Description      
------------------------------------------------------------      
	@Account_Id	INT
	@Old_Closure_Category_reason	varchar		NULL
	@Service_Month	DATE
	@New_reason_Cd		INT
	
 OUTPUT PARAMETERS:     
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------
	EXEC dbo.Update_Category_Reason_Variance_Failed_Test
	      @Account_Id = 741609                     -- int
	    , @Service_Month ='2018-08-01'      -- date
	    , @Old_Closure_Category_reason = 'Data Entry Only/Data Entry Only'   -- varchar(200)
	    , @New_reason_Cd = 39                  -- int

	
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 AP	Arunkumar Palanivel
 
 MODIFICATIONS
 Initials	     Date		     Modification
------------------------------------------------------------
AP				May 21,2020		New procedure is created to update variance closure category and reason for the closed failed test case						
******/

CREATE PROCEDURE [dbo].[Update_Category_Reason_Variance_Failed_Test]
      (
      @Account_Id                  INT
    , @Service_Month               DATE
    , @Old_Closure_Category_reason VARCHAR(200)
    , @New_reason_Cd               INT
	, @Closed_date DATE )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Old_Closure_Category           VARCHAR(100)
                , @Old_Closure_reason             VARCHAR(100)
                , @old_closure_category_reason_cd INT;


            SET @Old_Closure_Category = left(@Old_Closure_Category_reason, charindex('|', @Old_Closure_Category_reason) - 1);
            SET @Old_Closure_reason = right(@Old_Closure_Category_reason, len(@Old_Closure_Category_reason) - charindex('|', @Old_Closure_Category_reason));


            SELECT
                  @old_closure_category_reason_cd = vrc.Closed_Reason_Id
            FROM  dbo.Variance_Closed_Reason_Category vrc
                  JOIN
                  dbo.Code c
                        ON c.Code_Id = vrc.Closed_Reason_Cd
                  JOIN
                  dbo.Code c1
                        ON c1.Code_Id = vrc.Closure_Category_Cd
            WHERE c1.Code_Dsc = @Old_Closure_Category
                  AND   c.Code_Dsc = @Old_Closure_reason;


				  SELECT   @Account_Id, @Service_Month , @old_closure_category_reason_cd, @Closed_date;



            UPDATE
                  vrc
            SET
                  vrc.Reason_Cd = @New_reason_Cd
            FROM  dbo.Variance_Closed_Reason vrc
            WHERE vrc.Account_Id = @Account_Id
                  AND   Service_Month = @Service_Month
                  AND   Reason_Cd = @old_closure_category_reason_cd
				  AND Closed_DT = @Closed_date;



      END;
      ;
GO
GRANT EXECUTE ON  [dbo].[Update_Category_Reason_Variance_Failed_Test] TO [CBMSApplication]
GO
