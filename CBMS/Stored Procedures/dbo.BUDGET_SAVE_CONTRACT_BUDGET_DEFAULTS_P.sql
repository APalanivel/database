SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE    PROCEDURE dbo.BUDGET_SAVE_CONTRACT_BUDGET_DEFAULTS_P
	@budget_contract_id int,
	@volume decimal(32,16),
	@market_id int,
	@fuel decimal(32,16),
	@multiplier decimal(32,16),
	@adder decimal(32,16),
	@volume_unit_type_id int,
	@tax decimal(32,16),
	@currency_unit_id int,
	@isNymex bit
	AS

	insert into budget_contract_defaults (budget_contract_budget_id, volume, market_id, fuel, adder, tax, multiplier, volume_unit_type_id, currency_unit_id, is_nymex_forecast)
	values(@budget_contract_id, @volume, @market_id, @fuel, @adder, @tax, @multiplier, @volume_unit_type_id, @currency_unit_id, @isNymex)










GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_CONTRACT_BUDGET_DEFAULTS_P] TO [CBMSApplication]
GO
