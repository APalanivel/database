SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******          

NAME: dbo.Sso_Project_Sel_By_Owner_Id_Type
     
DESCRIPTION: 
	To Get Sso Project name and Project Category for given Owner Id and Owner Type.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
    @Owner_Id		INT
    @Owner_Type		VARCHAR(30)	  			Client/DIvision/Site
    @StartIndex		INT			1				
    @EndIndex		INT			2147483647
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	EXEC dbo.Sso_Project_Sel_By_Owner_Id_Type 2943, 'Site',1,25
	EXEC dbo.Sso_Project_Sel_By_Owner_Id_Type 10065,'Client',1,25
	EXEC dbo.Sso_Project_Sel_By_Owner_Id_Type 2222,'Division',1,25
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
	CPE			Chaitanya Panduga Eshwar
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JUN-10	CREATED
	PNR			24-AUG-10	Added Pagination Logic and also added two parameters  @StartIndex,@EndIndex   
	CPE			03/28/2011	Modified the SP to capture Client Hier Id from the Owner Id and Owner Type
  
*/

CREATE PROCEDURE dbo.Sso_Project_Sel_By_Owner_Id_Type
    (
        @Owner_Id		INT
       ,@Owner_Type		VARCHAR(30)
       ,@StartIndex		INT	= 1
	   ,@EndIndex		INT	= 2147483647
    )
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @Client_Hier_Id int

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier CH
      WHERE
            ( @OWNER_ID = ch.Client_Id
              AND @Owner_Type = 'Client'
              AND ch.Site_Id = 0
              AND ch.Sitegroup_Id = 0 )
            OR ( @OWNER_ID = ch.Sitegroup_Id
                 AND @Owner_Type = 'Division'
                 AND ch.Site_Id = 0 )
            OR ( @OWNER_ID = ch.Site_Id
                 AND @Owner_Type = 'Site'
                 AND ch.Site_Id > 0 )

	;WITH Cte_Sso_Project
	AS
	(
  		SELECT
			 SP.PROJECT_TITLE
			,CAT.ENTITY_NAME Project_category
			,Row_Num = ROW_NUMBER() OVER ( ORDER BY SP.PROJECT_TITLE )
			,Total_Rows = COUNT(1) OVER ()
		FROM
			dbo.SSO_PROJECT SP
			JOIN dbo.SSO_PROJECT_OWNER_MAP SPOM
				 ON SPOM.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
			JOIN dbo.ENTITY CAT
				 ON SP.PROJECT_CATEGORY_TYPE_ID = CAT.ENTITY_ID
		WHERE
			 SPOM.Client_Hier_Id = @Client_Hier_Id
		GROUP BY
			 SP.PROJECT_TITLE
			 ,CAT.ENTITY_NAME
	)
		SELECT
			 PROJECT_TITLE
			,Project_category
			,Total_Rows
		FROM
			Cte_Sso_Project
		WHERE
			Row_Num BETWEEN @StartIndex AND @EndIndex
END
GO
GRANT EXECUTE ON  [dbo].[Sso_Project_Sel_By_Owner_Id_Type] TO [CBMSApplication]
GO
