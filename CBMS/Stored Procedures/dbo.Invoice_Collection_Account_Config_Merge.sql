SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Invoice_Collection_Account_Config_Merge                
                            
 DESCRIPTION:                            
   To Update and insert data for Invoice_Collection_Account_Config table                      
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
@Account_Id        INT    
@Is_Chase_Activated      BIT    
@Invoice_Collection_Service_Start_Dt DATE    
@Invoice_Collection_Service_End_Dt  DATE    
@Invoice_Collection_Officer_User_Id  INT    
@Chase_Priority_Cd      INT    
@Invoice_Pattern_Cd      INT    
@Additional_Invoices_Per_Period   TINYINT    
@User_Info_Id       INT    
                           
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
 --INSERT    
 BEGIN TRAN    
 SELECT * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=109320 
 select * from Invoice_Collection_Account_Config_GroupId_Mapping where Invoice_Collection_Account_Config_Id=64419
 EXEC [dbo].[Invoice_Collection_Account_Config_Merge]     
      @Account_Id = 109320    
      ,@Is_Chase_Activated = 1    
      ,@Invoice_Collection_Service_Start_Dt ='2016-01-01'    
      ,@Invoice_Collection_Service_End_Dt ='2020-12-01'    
      ,@Invoice_Collection_Officer_User_Id =49    
      ,@Chase_Priority_Cd =NULL    
      ,@Invoice_Pattern_Cd =NULL    
      ,@Additional_Invoices_Per_Period =1    
      ,@User_Info_Id =49    
      ,@Invoice_Collection_Account_Config_Id_Out=NULL    
	  ,@Account_Config_Group_Name='tes'
      SELECT * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=109320    
 ROLLBACK             
    
--UPDATE    
 BEGIN TRAN    
 SELECT * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=109320    
 EXEC [dbo].[Invoice_Collection_Account_Config_Merge]     
      @Account_Id = 109320    
      ,@Is_Chase_Activated = 1    
      ,@Invoice_Collection_Service_Start_Dt ='2016-01-01'    
      ,@Invoice_Collection_Service_End_Dt ='2016-12-01'    
      ,@Invoice_Collection_Officer_User_Id =49    
      ,@Chase_Priority_Cd =NULL    
      ,@Invoice_Pattern_Cd =NULL    
      ,@Additional_Invoices_Per_Period =1          
      ,@User_Info_Id =49    
      ,@Invoice_Collection_Account_Config_Id_Out=NULL    
    
 EXEC [dbo].[Invoice_Collection_Account_Config_Merge]     
      @Account_Id = 109320    
      ,@Is_Chase_Activated = 1    
      ,@Invoice_Collection_Service_Start_Dt ='2016-02-01'    
      ,@Invoice_Collection_Service_End_Dt ='2016-12-01'    
      ,@Invoice_Collection_Officer_User_Id =49    
      ,@Chase_Priority_Cd =NULL    
      ,@Invoice_Pattern_Cd =NULL    
      ,@Additional_Invoices_Per_Period =1                 
      ,@User_Info_Id =49       
      ,@Invoice_Collection_Account_Config_Id_Out=NULL       
      SELECT * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=109320    
 ROLLBACK      
     
  EXEC dbo.CODE_SEL_BY_CodeSet_Name     
       @CodeSet_Name = 'InvoicePattern'     
           
   EXEC dbo.CODE_SEL_BY_CodeSet_Name     
       @CodeSet_Name = 'ChasePriority'       
    
EXEC dbo.CODE_SEL_BY_CodeSet_Name     
       @CodeSet_Name = 'InvoiceFrequency'     
    
           
    EXEC dbo.CODE_SEL_BY_CodeSet_Name     
       @CodeSet_Name = 'FrequencyPattern'                   
                                 
 AUTHOR INITIALS:            
           
 Initials              Name  
---------------------------------------------------------------------------------------------------------------                          
 SP            Sandeep Pigilam 
 SLP		   Sri Lakshmi Pallikonda                               
 MODIFICATIONS:          
              
 Initials        Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                2016-11-17     Created for Invoice Tracking Phase I.
 RKV               2017-11-27     Added New Column Invoice_Collection_Owner_User_Id 
 RKV               2019-05-24     Added new column Service_Type_Cd   
 SLP			   2019-10-14     Added the paramter @Account_Config_Group_Name
 SLP		       2019-11-13	  Included @user_info_id while executing the [Invoice_Collection_Account_Config_Group_Ins]
                           
******/


CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Merge]
(
    @Account_Id INT = NULL,
    @Is_Chase_Activated BIT = NULL,
    @Invoice_Collection_Alternative_Account_Number NVARCHAR(200) = NULL,
    @Invoice_Collection_Service_Start_Dt DATE = NULL,
    @Invoice_Collection_Service_End_Dt DATE = NULL,
    @Invoice_Collection_Officer_User_Id INT = NULL,
    @Chase_Priority_Cd INT = NULL,
    @Invoice_Pattern_Cd INT = NULL,
    @Additional_Invoices_Per_Period TINYINT = NULL,
    @User_Info_Id INT = NULL,
    @Invoice_Collection_Account_Config_Id INT = NULL,
    @Preserve_Existing_Value_When_Null BIT = 0,
    @Invoice_Collection_Account_Config_Id_Out INT OUTPUT,
    @Invoice_Collection_Owner_User_Id INT = NULL,
    @Service_Type_Cd INT = NULL,
    @Account_Config_Group_Name NVARCHAR(236) = NULL,
    @Is_Account_Config_Group_Name_Bulk_Update BIT = 0
)
AS
BEGIN
    SET NOCOUNT ON;
    DECLARE @Invoice_Collection_Account_Config_Group_Id INT = NULL,
            @Invoice_Collection_Account_Config_Group_Id_Out INT;

    IF @Account_Config_Group_Name IS NOT NULL
        EXEC [Invoice_Collection_Account_Config_Group_Ins] @Account_Config_Group_Name,
                                                           @User_Info_Id,
                                                           @Invoice_Collection_Account_Config_Group_Id_Out = @Invoice_Collection_Account_Config_Group_Id OUTPUT;

    --   select @Invoice_Collection_Account_Config_Group_Id

    CREATE TABLE #AuditingInfo
    (
        Existing_Config_Group_Id INT,
        New_Config_Group_Id INT
    );

    SELECT @Is_Chase_Activated = ISNULL(@Is_Chase_Activated, ica.Is_Chase_Activated),
           @Invoice_Collection_Alternative_Account_Number
               = ISNULL(
                           @Invoice_Collection_Alternative_Account_Number,
                           ica.Invoice_Collection_Alternative_Account_Number
                       ),
           @Invoice_Collection_Service_Start_Dt
               = ISNULL(@Invoice_Collection_Service_Start_Dt, ica.Invoice_Collection_Service_Start_Dt),
           @Invoice_Collection_Service_End_Dt
               = ISNULL(@Invoice_Collection_Service_End_Dt, ica.Invoice_Collection_Service_End_Dt),
           @Invoice_Collection_Officer_User_Id
               = ISNULL(@Invoice_Collection_Officer_User_Id, ica.Invoice_Collection_Officer_User_Id),
           @Chase_Priority_Cd = ISNULL(@Chase_Priority_Cd, ica.Chase_Priority_Cd),
           @Invoice_Pattern_Cd = ISNULL(@Invoice_Pattern_Cd, ica.Invoice_Pattern_Cd),
           @Additional_Invoices_Per_Period
               = ISNULL(@Additional_Invoices_Per_Period, ica.Additional_Invoices_Per_Period),
           @Invoice_Collection_Owner_User_Id
               = ISNULL(@Invoice_Collection_Owner_User_Id, ica.Invoice_Collection_Owner_User_Id),
           @Service_Type_Cd = ISNULL(@Service_Type_Cd, ica.Service_Type_Cd)
    FROM dbo.Invoice_Collection_Account_Config ica
    WHERE ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
          AND @Preserve_Existing_Value_When_Null = 1;


    SELECT @Invoice_Collection_Account_Config_Group_Id = icacgm.Invoice_Collection_Account_Config_Group_Id
    FROM dbo.Invoice_Collection_Account_Config_Group_Map icacgm
    WHERE icacgm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
          AND @Is_Account_Config_Group_Name_Bulk_Update = 0
          AND @Preserve_Existing_Value_When_Null = 1;

    MERGE INTO dbo.Invoice_Collection_Account_Config AS tgt
    USING
    (
        SELECT @Account_Id AS Account_Id,
               @Is_Chase_Activated AS Is_Chase_Activated,
               @Invoice_Collection_Alternative_Account_Number AS Invoice_Collection_Alternative_Account_Number,
               @Invoice_Collection_Service_Start_Dt AS Invoice_Collection_Service_Start_Dt,
               @Invoice_Collection_Service_End_Dt AS Invoice_Collection_Service_End_Dt,
               @Invoice_Collection_Officer_User_Id AS Invoice_Collection_Officer_User_Id,
               @Chase_Priority_Cd AS Chase_Priority_Cd,
               @Invoice_Pattern_Cd AS Invoice_Pattern_Cd,
               @Additional_Invoices_Per_Period AS Additional_Invoices_Per_Period,
               @User_Info_Id AS User_Info_Id,
               @Invoice_Collection_Owner_User_Id AS Invoice_Collection_Owner_User_Id,
               @Service_Type_Cd AS Service_Type_Cd,
               GETDATE() AS Ts
    --,@Invoice_Collection_Account_Config_Group_Id  as Invoice_Collection_Account_Config_Group_Id
    ) AS src
    ON tgt.Invoice_Collection_Account_Config_Id = ISNULL(@Invoice_Collection_Account_Config_Id, 0)
    WHEN MATCHED THEN
        UPDATE SET Invoice_Collection_Service_Start_Dt = src.Invoice_Collection_Service_Start_Dt,
                   Invoice_Collection_Service_End_Dt = src.Invoice_Collection_Service_End_Dt,
                   Invoice_Collection_Officer_User_Id = src.Invoice_Collection_Officer_User_Id,
                   Chase_Priority_Cd = src.Chase_Priority_Cd,
                   Invoice_Pattern_Cd = src.Invoice_Pattern_Cd,
                   Additional_Invoices_Per_Period = src.Additional_Invoices_Per_Period,
                   Updated_User_Id = src.User_Info_Id,
                   Last_Change_Ts = src.Ts,
                   Invoice_Collection_Alternative_Account_Number = src.Invoice_Collection_Alternative_Account_Number,
                   Invoice_Collection_Owner_User_Id = src.Invoice_Collection_Owner_User_Id,
                   Service_Type_Cd = src.Service_Type_Cd
    WHEN NOT MATCHED THEN
        INSERT
        (
            Account_Id,
            Is_Chase_Activated,
            Invoice_Collection_Service_Start_Dt,
            Invoice_Collection_Service_End_Dt,
            Invoice_Collection_Officer_User_Id,
            Chase_Priority_Cd,
            Invoice_Pattern_Cd,
            Additional_Invoices_Per_Period,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id,
            Last_Change_Ts,
            Invoice_Collection_Alternative_Account_Number,
            Chase_Status_Updated_User_Id,
            Chase_Status_Lat_Updated_Ts,
            Invoice_Collection_Owner_User_Id,
            Service_Type_Cd
        )
        VALUES
        (src.Account_Id, src.Is_Chase_Activated, src.Invoice_Collection_Service_Start_Dt,
         src.Invoice_Collection_Service_End_Dt, src.Invoice_Collection_Officer_User_Id, src.Chase_Priority_Cd,
         src.Invoice_Pattern_Cd, src.Additional_Invoices_Per_Period, src.User_Info_Id, src.Ts, src.User_Info_Id,
         src.Ts, src.Invoice_Collection_Alternative_Account_Number, src.User_Info_Id, src.Ts,
         src.Invoice_Collection_Owner_User_Id, src.Service_Type_Cd);

    UPDATE Invoice_Collection_Account_Config
    SET Is_Chase_Activated = @Is_Chase_Activated,
        Chase_Status_Updated_User_Id = @User_Info_Id,
        Chase_Status_Lat_Updated_Ts = GETDATE()
    WHERE Is_Chase_Activated <> ISNULL(@Is_Chase_Activated, 0)
          AND Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

    --Map the Acct config with the Group Id                     
    SELECT @Invoice_Collection_Account_Config_Id_Out = SCOPE_IDENTITY();

    MERGE INTO dbo.Invoice_Collection_Account_Config_Group_Map AS tgt
    USING
    (
        SELECT @User_Info_Id AS User_Info_Id,
               GETDATE() AS Ts,
               @Invoice_Collection_Account_Config_Group_Id AS Invoice_Collection_Account_Config_Group_Id
    ) AS src
    ON tgt.Invoice_Collection_Account_Config_Id = ISNULL(
                                                            @Invoice_Collection_Account_Config_Id,
                                                            @Invoice_Collection_Account_Config_Id_Out
                                                        )
    WHEN MATCHED THEN
        UPDATE SET Invoice_Collection_Account_Config_Group_Id = src.Invoice_Collection_Account_Config_Group_Id,
                   Updated_User_Id = src.User_Info_Id,
                   Last_Change_Ts = src.Ts
    WHEN NOT MATCHED THEN
        INSERT
        (
            Invoice_Collection_Account_Config_Id,
            Invoice_Collection_Account_Config_Group_Id,
            Created_User_Id,
            Created_Ts,
            Updated_User_Id,
            Last_Change_Ts
        )
        VALUES
        (ISNULL(@Invoice_Collection_Account_Config_Id, @Invoice_Collection_Account_Config_Id_Out),
         @Invoice_Collection_Account_Config_Group_Id, src.User_Info_Id, src.Ts, src.User_Info_Id, src.Ts)
    OUTPUT deleted.Invoice_Collection_Account_Config_Group_Id,
           inserted.Invoice_Collection_Account_Config_Group_Id
    INTO #AuditingInfo;

    -- audit trailing   
    INSERT INTO Invoice_Collection_Account_Config_Group_Audit
    (
        Invoice_Collection_Account_Config_Group_ID,
        Invoice_Collection_Account_Config_Id,
        Created_User_Id
    )
    SELECT @Invoice_Collection_Account_Config_Group_Id,
           ISNULL(@Invoice_Collection_Account_Config_Id, @Invoice_Collection_Account_Config_Id_Out),
           @User_Info_Id
    FROM #AuditingInfo
    WHERE ISNULL(Existing_Config_Group_Id, '') <> ISNULL(New_Config_Group_Id, '');

END;

;







GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Merge] TO [CBMSApplication]
GO
