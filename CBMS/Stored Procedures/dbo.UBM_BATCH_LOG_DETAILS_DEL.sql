SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******  
NAME:  
 
 dbo.UBM_BATCH_LOG_DETAILS_DEL
 
 DESCRIPTION:   
 
 Deletes the UBM_BATCH_LOG_DETAILS table for the passed in UBM_BATCH_MASTER_LOG_ID.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@Ubm_Batch_Master_Log_Id Integer

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.UBM_BATCH_LOG_DETAILS_DEL 3256

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page    10/08/2009

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    
******/


CREATE PROCEDURE [dbo].[UBM_BATCH_LOG_DETAILS_DEL]
(
	@Ubm_Batch_Master_Log_Id Integer
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DELETE UBM_BATCH_LOG_DETAILS
	FROM UBM_BATCH_LOG_DETAILS ubld
	INNER JOIN UBM_BATCH_LOG ubl ON ubl.UBM_BATCH_LOG_ID = ubld.UBM_BATCH_LOG_ID
	WHERE ubl.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id	
	
END
GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_LOG_DETAILS_DEL] TO [CBMSApplication]
GO
