SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE PROCEDURE dbo.cbmsCostUsage_GetGFENotProcessed

 AS
begin
set nocount on

	   select cu.eventtype
		, rtrim(cu.doc_id) doc_id
		, cu.cbms_image_id
		, cu.site_code
		, cu.account_id
		, cu.site_id
		, convert(varchar, cu.service_month, 101)
		, cu.currency_unit_id
		, convert(varchar, cu.event_date, 101)
	     from cost_usage_gfe_change cu
	    where cu.is_processed = 0
--	      and cu.event_date < '6/20/2007'
	 order by cu.event_date 

end
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsage_GetGFENotProcessed] TO [CBMSApplication]
GO
