SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_UBM_DATAD_P
	@clientID int
	AS
	begin
		set nocount on

		delete from ubm_client_map 
		where client_id = @clientID 
	end


GO
GRANT EXECUTE ON  [dbo].[DELETE_UBM_DATAD_P] TO [CBMSApplication]
GO
