SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Utility_Account_Sel_By_Sup_Account_Metert_Id                     
                          
 DESCRIPTION:      
		Get the                     
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @Sup_Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
               
	EXEC dbo.Utility_Account_Sel_By_Sup_Account_Metert_Id  1740999

	EXEC dbo.Utility_Account_Sel_By_Sup_Account_Metert_Id  1740997
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/


CREATE PROCEDURE [dbo].[Utility_Account_Sel_By_Sup_Account_Metert_Id]
    (
        @Sup_Account_Id INT
        , @Meter_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;



        SELECT
            Uti_cha.Account_Id
            , Sup_Cha.Account_Id AS Supplier_Account_Id
            , ch.Client_Id
            , ch.Client_Name
            , ch.Site_name
            , Sup_Cha.Account_Vendor_Name
            , c.Commodity_Name
            , Sup_Cha.Last_Change_TS
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account Uti_cha
                ON Uti_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Sup_Cha
                ON Uti_cha.Meter_Id = Sup_Cha.Meter_Id
            INNER JOIN dbo.Commodity c
                ON Sup_Cha.Commodity_Id = c.Commodity_Id
        WHERE
            Uti_cha.Account_Type = 'Utility'
            AND Sup_Cha.Account_Type = 'Supplier'
            AND Sup_Cha.Account_Id = @Sup_Account_Id
            AND (   @Meter_Id IS NULL
                    OR  Sup_Cha.Meter_Id = @Meter_Id)
        GROUP BY
            Uti_cha.Account_Id
            , Sup_Cha.Account_Id
            , ch.Client_Name
            , ch.Site_name
            , Sup_Cha.Account_Vendor_Name
            , c.Commodity_Name
            , Sup_Cha.Last_Change_TS
            , ch.Client_Id;

    END;

GO
GRANT EXECUTE ON  [dbo].[Utility_Account_Sel_By_Sup_Account_Metert_Id] TO [CBMSApplication]
GO
