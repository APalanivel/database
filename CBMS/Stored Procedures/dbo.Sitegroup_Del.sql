SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sitegroup_Del]  

DESCRIPTION: It Deletes Site Group for Given Site Group Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Sitegroup_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  BEGIN TRAN
	   EXEC dbo.Sitegroup_Del 2526
  ROLLBACK TRAN
  
  SELECT * FROM dbo.Sitegroup WHERE Sitegroup_id = 2526

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			21-JULY-10	CREATED

*/

CREATE PROCEDURE dbo.Sitegroup_Del
    (
		 @Sitegroup_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Sitegroup
	WHERE
		Sitegroup_Id = @Sitegroup_Id
END
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_Del] TO [CBMSApplication]
GO
