
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******                
NAME:   dbo.CU_Invoice_Image_Sel_All_For_Data_Transfer             
      
          
DESCRIPTION:       
 Gets all image data from CU-Invoice where there is a valid service month      
       
 used specifically for ETL processes               
       
            
INPUT PARAMETERS:                
Name   DataType Default  Description                
------------------------------------------------------------                
      
                      
OUTPUT PARAMETERS:                
Name   DataType Default  Description                
------------------------------------------------------------       
      
               
USAGE EXAMPLES:                
------------------------------------------------------------       
 EXEC dbo.CU_Invoice_Image_Sel_All_For_Data_Transfer      
             
           
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKR      Ashok Kumar Raju
DMR	     Deana Ritter     
           
           
MODIFICATIONS                 
Initials Date Modification                
------------------------------------------------------------                
AKR   12/21/2011 Created   
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.   
*****/       
CREATE PROCEDURE [dbo].[CU_Invoice_Image_Sel_All_For_Data_Transfer]
AS 
BEGIN      
                
      SELECT
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.ACCOUNT_ID
           ,ci.Cu_Invoice_id
           ,convert(DATE, ism.Service_Month) AS Service_Month
           ,ci.CBMS_IMAGE_ID
      FROM
            CU_Invoice ci
            INNER JOIN cu_Invoice_Service_Month ism
                  ON ci.CU_invoice_Id = ism.CU_invoice_Id
            INNER JOIN Core.Client_Hier_Account cha
                  ON ism.Account_id = cha.Account_id
      WHERE
            ci.IS_REPORTED = 1
            AND ci.CBMS_Image_Id IS NOT NULL
            AND ism.SERVICE_MONTH IS NOT NULL
      GROUP BY
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.ACCOUNT_ID
           ,ci.Cu_Invoice_id
           ,convert(DATE, ism.Service_Month)
           ,ci.CBMS_IMAGE_ID      
END 



;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Image_Sel_All_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[CU_Invoice_Image_Sel_All_For_Data_Transfer] TO [ETL_Execute]
GO
