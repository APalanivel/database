SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:              
      [dbo].[EC_Contract_Attribute_History_Sel_By_EC_Contract_Attribute_Id]            
              
Description:              
            To get the details of active tracking data  data for a given contract attribute id.             
              
 Input Parameters:              
    Name						DataType			Default				Description                
----------------------------------------------------------------------------------------------            
	@EC_Contract_Attribute_Id   INT            
               
 Output Parameters:                    
    Name						DataType			Default				Description                
----------------------------------------------------------------------------------------------            
              
 Usage Examples:                  
----------------------------------------------------------------------------------------------       
     
 EXEC dbo.EC_Contract_Attribute_History_Sel_By_EC_Contract_Attribute_Id 1 
 
 EXEC dbo.EC_Contract_Attribute_History_Sel_By_EC_Contract_Attribute_Id  2
 
Author Initials:              
      Initials         Name              
----------------------------------------------------------------------------------------------            
      NR              Narayana Reddy       
 Modifications:              
      Initials		Date			Modification              
----------------------------------------------------------------------------------------------            
       NR			2015-05-08       Created for AS400.         
             
******/             
          
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_History_Sel_By_EC_Contract_Attribute_Id]
      ( 
       @EC_Contract_Attribute_Id INT )
AS 
BEGIN          
      SET NOCOUNT ON;   
      
      DECLARE @EC_Contract_Attribute_Id_Is_History_Exists BIT  
       
      SET @EC_Contract_Attribute_Id_Is_History_Exists = 0     
              
      SELECT
            @EC_Contract_Attribute_Id_Is_History_Exists = 1
      FROM
            dbo.EC_Contract_Attribute_Tracking ecat
      WHERE
            ecat.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id 
      SELECT
            @EC_Contract_Attribute_Id_Is_History_Exists AS EC_Contract_Attribute_Exists
                     
END  
        

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_History_Sel_By_EC_Contract_Attribute_Id] TO [CBMSApplication]
GO
