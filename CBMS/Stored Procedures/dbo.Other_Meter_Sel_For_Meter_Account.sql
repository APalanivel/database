SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Other_Meter_Sel_For_Meter_Account]  
     
DESCRIPTION: 
	
	Used to select all other meters belongs to the commodity of the given meter and associated with the utility account of the given meter.

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT
                
OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Other_Meter_Sel_For_Meter_Account 21086
	EXEC Other_Meter_Sel_For_Meter_Account 62461

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			8/30/2010	Created

*/

CREATE PROCEDURE dbo.Other_Meter_Sel_For_Meter_Account
(
	 @Meter_Id		INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		OthMtr.METER_ID
		,com.Commodity_Id
		,com.Commodity_Name
	FROM
		dbo.METER m
		JOIN dbo.Meter OthMtr
			ON OthMtr.ACCOUNT_ID = m.ACCOUNT_ID
		JOIN dbo.RATE othMtrRate
			ON othMtrRate.RATE_ID = OthMtr.RATE_ID
		JOIN dbo.Commodity com
			ON com.Commodity_Id = othMtrRate.COMMODITY_TYPE_ID
	WHERE
		m.METER_ID = @Meter_Id
		AND OthMtr.METER_ID != @Meter_Id

END
GO
GRANT EXECUTE ON  [dbo].[Other_Meter_Sel_For_Meter_Account] TO [CBMSApplication]
GO
