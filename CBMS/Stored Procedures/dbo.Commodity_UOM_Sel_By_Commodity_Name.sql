
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    

NAME:    
 dbo.Commodity_UOM_Sel_By_CommodityName  
  
DESCRIPTION:    

	Used to get all UOM from code table for a commodity  
  
INPUT PARAMETERS:    
Name				DataType	Default  Description    
------------------------------------------------------------    
      
@Commodity_Name		VARCHAR(50)    
@IS_ACTIVE			BIT			1
@Is_Associated		BIT			NULL

OUTPUT PARAMETERS:
Name				DataType	Default  Description
------------------------------------------------------------    

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Commodity_UOM_Sel_By_Commodity_Name ''

	EXEC dbo.Commodity_UOM_Sel_By_Commodity_Name 'Natural Gas',1,1
	EXEC dbo.Commodity_UOM_Sel_By_Commodity_Name 'Natural Gas',1,0
	EXEC dbo.Commodity_UOM_Sel_By_Commodity_Name 'Acetylene',1,0

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
GB			Geetansu Behera    
CMH			Chad Hattabaugh     
HG			Hari  
SKA			Shobhit Kr Agrawal  

MODIFICATIONS
Initials	Date		Modification    
------------------------------------------------------------
GB						Created
SKA			02-JUL-09	Modified
SS			03-JUL-09	Refered to CodeSet_Name from Codeset for filtering in place hardcoded Codeset_Id filter
							Converted correlated Subquery to explicit join
							Subselect removed using condition x.UnitOfMeasure > 0
							Added input variable @Is_Associated to filter different cases
SKA			25-AUG-09	Used @Commodity_Name in join and removed @Commodity_ID
DMR			09/10/2010	Modified for Quoted_Identifier
HG			2013-05-10	Manage service changes
							- Sorting the records by alphabet order of uom
							- Logic used to get the associated uom simplified.
******/
CREATE PROCEDURE [dbo].[Commodity_UOM_Sel_By_Commodity_Name]
      @Commodity_Name AS VARCHAR(50)
     ,@IS_ACTIVE BIT = 1
     ,@Is_Associated BIT = NULL
AS 
BEGIN  

      SET NOCOUNT ON;

      SELECT
            Is_Associated = case WHEN cuc.Base_Commodity_Id IS NULL THEN 0
                                 ELSE 1
                            END
           ,UOM_CD = CD.CODE_ID
           ,UOM_VALUE = CD.CODE_VALUE
           ,CD.DISPLAY_SEQ
      FROM
            dbo.CODE CD
            INNER JOIN dbo.CODESET cs
                  ON cs.codeset_Id = cd.codeset_id
            LEFT OUTER JOIN ( dbo.Commodity com
                              INNER JOIN dbo.Commodity_UOM_Conversion cuc
                                    ON cuc.Base_Commodity_Id = com.Commodity_Id
                                       AND cuc.Converted_Commodity_Id = com.Commodity_Id
                                       AND cuc.Base_UOM_Cd = cuc.Converted_UOM_Cd 
                                       AND cuc.Is_Active = 1)
                              ON com.Commodity_Name = @Commodity_Name
                                 AND cuc.Base_UOM_Cd = cd.Code_Id
                                 
      WHERE
            cs.std_column_name = 'UOM_CD'
            AND CD.IS_ACTIVE = @IS_ACTIVE
            AND ( @Is_Associated IS NULL
                  OR ( @Is_Associated = 1
                       AND cuc.Base_Commodity_Id IS NOT NULL )
                  OR ( @Is_Associated = 0
                       AND cuc.Base_Commodity_Id IS NULL ) )
      ORDER BY
            CD.Code_Value
                 
END
;
GO

GRANT EXECUTE ON  [dbo].[Commodity_UOM_Sel_By_Commodity_Name] TO [CBMSApplication]
GO
