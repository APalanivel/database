SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SR_RFP_GET_ALL_GROUP_LP_DOCUMENTS_P]
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int
	AS

	set nocount on
	begin

		select	approval.site_id,
			c.cbms_image_id,
			c.cbms_doc_id--,
			--c.cbms_image
				
		from 	cbms_image c(nolock),
			sr_rfp_lp_client_approval approval (nolock)
			 
		where 	approval.sr_rfp_id = @rfp_id	
			and approval.is_group_lp = 1
			and c.cbms_image_id = approval.cbms_image_id
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_GROUP_LP_DOCUMENTS_P] TO [CBMSApplication]
GO
