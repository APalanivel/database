SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******  

NAME:  
 dbo.cbmsSSOSavings_GetTotalByCatForClient  
  
DESCRIPTION:
  
 INPUT PARAMETERS:  
 Name				DataType	Default Description  
------------------------------------------------------------  
 @MyAccountId		INT
 @client_id			INT			NULL
 @division_id		INT			NULL
 @site_id			INT			NULL                    
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
 ------------------------------------------------------------  
  
	EXEC dbo.cbmsSSOSavings_GetTotalByCatForClient 49,10069,NULL,NULL
    
 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
  
 MODIFICATIONS :
 Initials Date		 Modification  
------------------------------------------------------------  
 PNR	  04/05/2011 Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
 
******/  

CREATE PROCEDURE dbo.cbmsSSOSavings_GetTotalByCatForClient
( 
 @MyAccountId INT
,@client_id INT = NULL
,@division_id INT = NULL
,@site_id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON ;

      EXEC dbo.cbmsSecurity_GetClientAccess @MyAccountId, @client_id OUTPUT, @division_id OUTPUT, @site_id OUTPUT  


      SELECT
            s.savings_category_type_id
           ,SUM(DISTINCT s.total_estimated_savings) AS total_estimated_savings
           ,e.entity_name AS savings_category_type
      FROM
            dbo.sso_savings s
            JOIN dbo.entity e
                  ON s.savings_category_type_id = e.entity_id
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.SSO_SAVINGS_OWNER_MAP ssom
                        JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = ssom.Client_Hier_Id
                     WHERE
                        ( @client_id IS NULL
                          OR ch.Client_Id = @client_id )
                        AND ( @division_id IS NULL
                              OR ch.Sitegroup_Id = @division_id )
                        AND ( @site_id IS NULL
                              OR ch.Site_Id = @site_id )
                        AND ssom.sso_savings_id = s.sso_savings_id )
      GROUP BY
            s.savings_category_type_id
           ,e.entity_name
END


GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetTotalByCatForClient] TO [CBMSApplication]
GO
