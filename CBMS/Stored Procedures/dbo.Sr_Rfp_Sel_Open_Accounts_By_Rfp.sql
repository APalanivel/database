SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.Sr_Rfp_Sel_Open_Accounts_By_Rfp                        
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                                    
 OUTPUT PARAMETERS:                   
                              
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                              
 USAGE EXAMPLES:              
-----------------------------------------------------------------------------------  
  
  EXEC dbo.Sr_Rfp_Sel_Open_Accounts_By_Rfp 13300
  
    EXEC dbo.Sr_Rfp_Sel_Open_Accounts_By_Rfp 13260
            
  
  
 AUTHOR INITIALS:              
             
 Initials			Name              
-----------------------------------------------------------------------------------  
 NR					Narayana Reddy                                    
                               
 MODIFICATIONS:            
             
 Initials               Date             Modification            
-----------------------------------------------------------------------------------  
 NR                     2016-03-31      Global Sourcing Phase-3 - Created RFP cloning
                             
******/ 

CREATE PROCEDURE [dbo].[Sr_Rfp_Sel_Open_Accounts_By_Rfp] ( @Sr_Rfp_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
      
      
      SELECT
            sra.ACCOUNT_ID
           ,sra.SR_RFP_ACCOUNT_ID
      FROM
            dbo.SR_RFP_ACCOUNT sra
            INNER JOIN dbo.ENTITY e
                  ON sra.BID_STATUS_TYPE_ID = e.ENTITY_ID
      WHERE
            sra.SR_RFP_ID = @Sr_Rfp_Id
            AND e.ENTITY_NAME IN ( 'Open', 'New', 'Bid Placed', 'Expired', 'Refresh', 'Processing' )
            AND e.ENTITY_DESCRIPTION = 'BID_STATUS'
            AND sra.IS_DELETED = 0
            
   
END  
  


;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Sel_Open_Accounts_By_Rfp] TO [CBMSApplication]
GO
