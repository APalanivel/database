SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Client_Hier_DMO_Config_Ins_Upd
           
DESCRIPTION:             
			To insert DMO configurations
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_Id				INT
    @Commodity_Id				INT
    @DMO_Start_Dt				DATE
    @DMO_End_Dt					DATE
    @Client_Hier_DMO_Config_Id	INT			NULL
    @User_Info_Id				INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	BEGIN TRANSACTION
		SELECT * FROM  dbo.Client_Hier_DMO_Config WHERE Client_Hier_Id = 56 AND Commodity_Id = 290
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2018-1-1','2018-12-31',NULL,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config WHERE Client_Hier_Id = 56 AND Commodity_Id = 290
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		DECLARE @Client_Hier_DMO_Config_Id INT = NULL
		
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id = 56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2017-1-1','2017-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id = 56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		
		SELECT @Client_Hier_DMO_Config_Id = Client_Hier_DMO_Config_Id FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id = 56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		SELECT @Client_Hier_DMO_Config_Id
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2018-1-1','2018-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id = 56 AND Commodity_Id = 290 AND DMO_Start_Dt= '2018-1-1' AND DMO_End_Dt='2018-12-31'
	ROLLBACK TRANSACTION
	
	BEGIN TRANSACTION
		DECLARE @Client_Hier_DMO_Config_Id INT = NULL
		
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 56,290,'2017-1-1','2017-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		
		SELECT @Client_Hier_DMO_Config_Id = Client_Hier_DMO_Config_Id FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		SELECT @Client_Hier_DMO_Config_Id
		EXEC dbo.Client_Hier_DMO_Config_Ins_Upd 5429,290,'2017-1-1','2017-12-31',@Client_Hier_DMO_Config_Id,16
		SELECT * FROM  dbo.Client_Hier_DMO_Config 
			WHERE Client_Hier_Id IN(56,5429) AND Commodity_Id = 290 AND DMO_Start_Dt= '2017-1-1' AND DMO_End_Dt='2017-12-31'
		SELECT * FROM dbo.Client_Hier_Not_Applicable_DMO_Config WHERE Client_Hier_Id IN(56,5429)
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-19	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[Client_Hier_DMO_Config_Ins_Upd]
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@DMO_Start_Dt DATE
      ,@DMO_End_Dt DATE
      ,@Client_Hier_DMO_Config_Id INT = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      DECLARE
            @Entity_Hier_Level_Cd INT
           ,@Config_Hier_Level_Cd INT
           ,@Client_Hier_DMO_Config_Id_New INT
      
      SELECT
            @Entity_Hier_Level_Cd = Hier_level_Cd
      FROM
            Core.Client_Hier
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            
      SELECT
            @Config_Hier_Level_Cd = ch.Hier_level_Cd
           ,@Commodity_Id = chdc.Commodity_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Client_Hier_DMO_Config chdc
                  ON ch.Client_Hier_Id = chdc.Client_Hier_Id
      WHERE
            chdc.Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
      
          
      INSERT      INTO dbo.Client_Hier_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Client_Hier_Id
                       ,@Commodity_Id
                       ,@DMO_Start_Dt
                       ,@DMO_End_Dt
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  WHERE
                        ( @Client_Hier_DMO_Config_Id IS NULL
                          OR @Entity_Hier_Level_Cd <> @Config_Hier_Level_Cd )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_DMO_Config
                                         WHERE
                                          Client_Hier_Id = @Client_Hier_Id
                                          AND Commodity_Id = @Commodity_Id
                                          AND DMO_Start_Dt = @DMO_Start_Dt
                                          AND DMO_End_Dt = @DMO_End_Dt )
                                          
      
      SELECT
            @Client_Hier_DMO_Config_Id_New = SCOPE_IDENTITY() 
            
      UPDATE
            dbo.Client_Hier_DMO_Config
      SET   
            DMO_Start_Dt = @DMO_Start_Dt
           ,DMO_End_Dt = @DMO_End_Dt
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            @Client_Hier_DMO_Config_Id IS NOT NULL
            AND Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
            AND @Entity_Hier_Level_Cd = @Config_Hier_Level_Cd
                                          
      
            
      EXEC dbo.DMO_Config_Make_Not_Applicable 
            @Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id_New
           ,@User_Info_Id = @User_Info_Id
           
      EXEC dbo.DMO_Config_Make_Not_Applicable 
            @Client_Hier_DMO_Config_Id = @Client_Hier_DMO_Config_Id
           ,@User_Info_Id = @User_Info_Id
      
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_DMO_Config_Ins_Upd] TO [CBMSApplication]
GO
