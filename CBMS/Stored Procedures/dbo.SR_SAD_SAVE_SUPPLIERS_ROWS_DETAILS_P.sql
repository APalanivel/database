SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select * from SR_DEAL_TICKET 

CREATE    PROCEDURE dbo.SR_SAD_SAVE_SUPPLIERS_ROWS_DETAILS_P
@srDealTicketId int,
@selectedSupplierId varchar(200),
@price varchar(200),
@isApproved bit,
@comments varchar(200)

AS
	set nocount on
	insert into SR_DT_SUPPLY_INFO
		(SR_DEAL_TICKET_ID, VENDOR_ID, PRICE, IS_APPROVED, COMMENTS)

	values
	(@srDealTicketId, @selectedSupplierId, @price, @isApproved, @comments)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_SUPPLIERS_ROWS_DETAILS_P] TO [CBMSApplication]
GO
