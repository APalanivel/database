SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
            
Name:   
	dbo.Account_Comment_Del       
              
Description:              
			To delete data from dbo.Account_Comment
              
 Input Parameters:              
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
	@Account_Id				INT
    
    
Output Parameters:                    
    Name					DataType		Default		Description                
---------------------------------------------------------------------------------
              
Usage Examples:                  
---------------------------------------------------------------------------------

	BEGIN TRAN   	
		SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007132
		EXEC dbo.Account_Comment_Del 1007132
		SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007132
	ROLLBACK TRAN
	
	BEGIN TRAN   	
		SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007166
		EXEC dbo.Account_Comment_Del 1007166
		SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007166
	ROLLBACK TRAN
   
	
Author Initials:              
    Initials	Name              
---------------------------------------------------------------------------------
	RR			Raghu Reddy               
 
Modifications:              
	Initials    Date		Modification              
---------------------------------------------------------------------------------
    RR			2015-06-19	Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[Account_Comment_Del] ( @Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @Total_Row_Count INT
           ,@Row_Counter INT
           ,@Comment_Id INT
           
      DECLARE @Comment_List TABLE
            ( 
             Comment_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )
            
      INSERT      INTO @Comment_List
                  ( 
                   Comment_Id )
                  SELECT
                        Comment_Id
                  FROM
                        dbo.Account_Comment
                  WHERE
                        Account_Id = @Account_Id
                        
      SELECT
            @Total_Row_Count = max(Row_Num)
      FROM
            @Comment_List
            
      SET @Row_Counter = 1 
      WHILE ( @Row_Counter <= @Total_Row_Count ) 
            BEGIN

                  SELECT
                        @Comment_Id = Comment_Id
                  FROM
                        @Comment_List
                  WHERE
                        Row_Num = @Row_Counter

                  DELETE FROM
                        dbo.Account_Comment
                  WHERE
                        Comment_ID = @Comment_Id
                        AND Account_Id = @Account_Id
                        
                  DELETE FROM
                        dbo.Comment
                  WHERE
                        Comment_ID = @Comment_Id
                        
                  

                  SET @Row_Counter = @Row_Counter + 1

            END
                 
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Comment_Del] TO [CBMSApplication]
GO
