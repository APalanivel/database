SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
                                   
 NAME: dbo.Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id                               
                                    
 DESCRIPTION:                
  Get the Suppler account associated invoices count based on Supplier Account.                             
                                    
 INPUT PARAMETERS:                
                                   
 Name                               DataType          Default       Description                
-------------------------------------------------------------------------------------            
 @Sup_Account_Id   INT                  
                                    
 OUTPUT PARAMETERS:                
                                         
 Name                               DataType          Default       Description                
-------------------------------------------------------------------------------------                                      
 USAGE EXAMPLES:                                        
-------------------------------------------------------------------------------------                           
          
         
 EXEC dbo.Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id  1740998            
          
 EXEC dbo.Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id 1728901            
                             
                                   
 AUTHOR INITIALS:              
                 
 Initials                   Name                
-------------------------------------------------------------------------------------            
 NR                     Narayana Reddy                                      
                                     
 MODIFICATIONS:              
                                     
 Initials               Date            Modification              
-------------------------------------------------------------------------------------            
 NR                     2019-06-13      Created for ADD Contract. 
 NR						2020-01-29		MAINT-9782 - Added IS_PROCESSED flag in output list.                       
                                 
                                   
******/

CREATE PROCEDURE [dbo].[Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id]
    (
        @Account_Id INT
        , @Contract_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            cism.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , Sup_Cha.Account_Id
            , ci.IS_PROCESSED
        FROM
            Core.Client_Hier_Account Sup_Cha
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Sup_Cha.Account_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            Sup_Cha.Account_Type = 'Supplier'
            AND Sup_Cha.Account_Id = @Account_Id
            AND (   @Contract_Id IS NULL
                    OR  Sup_Cha.Supplier_Contract_ID = @Contract_Id)
            AND cism.SERVICE_MONTH BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                   AND     ISNULL(Sup_Cha.Supplier_Account_End_Dt, '2099-01-31')
        GROUP BY
            cism.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , Sup_Cha.Account_Id
            , ci.IS_PROCESSED;

    END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id] TO [CBMSApplication]
GO
