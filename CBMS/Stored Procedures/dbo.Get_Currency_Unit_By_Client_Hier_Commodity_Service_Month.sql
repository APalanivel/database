SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 
NAME: 
	dbo.Get_Currency_Unit_By_Client_Hier_Commodity_Service_Month	
DESCRIPTION:

	To get first currency unit id based on given site, commodity and server month.

INPUT PARAMETERS: 
Name					DataType	Default		Description 
---------------------------------------------------------------------- 
@Site_Client_Hier_Id	INT 
@Commodity_Id			INT 
@Service_Month			DATE

OUTPUT PARAMETERS: 
Name	DataType	Default		Description 
---------------------------------------------------------------- 
USAGE EXAMPLES: 
-------------------------------------------------------------- 

EXEC DBO.Get_Currency_Unit_By_Client_Hier_Commodity_Service_Month	 1234,290,'2010-05-01'

AUTHOR INITIALS: 
Initials	Name 
------------------------------------------------------------ 
HG			Harihara Suthan Ganesan

MODIFICATIONS 
Initials	Date		Modification 
------------------------------------------------------------ 
HG			2012-06-08	Created 
******/

CREATE PROCEDURE DBO.Get_Currency_Unit_By_Client_Hier_Commodity_Service_Month
      (
      @Site_Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Service_Month DATE )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            cusd.CURRENCY_UNIT_ID
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
            JOIN dbo.Bucket_Master bm
                  ON cusd.Bucket_Master_Id = bm.Bucket_Master_Id
            JOIN dbo.Code cd
                  ON cd.code_id = bm.Bucket_Type_Cd
      WHERE
            cusd.Client_hier_id = @Site_Client_Hier_Id
            AND bm.Commodity_Id = @Commodity_Id
            AND cusd.Service_Month = @Service_Month
            AND cd.Code_Value = 'Charge'
      GROUP BY
            cusd.CURRENCY_UNIT_ID

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Currency_Unit_By_Client_Hier_Commodity_Service_Month] TO [CBMSApplication]
GO
