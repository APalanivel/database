SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_SAD_UPLOAD_RC_DOCUMENT_P]
	@userId int,
	@sessionId varchar(100),
	--@cbmsImageDocId varchar(200),
	--@cbmsImage image ,
	--@contentType varchar(200),
	@rfpId int,
	@cbmsImageId int,  -- added by Jaya
	@accountId int

AS
BEGIN
  
	SET NOCOUNT ON
	
	DECLARE @entityId INT
	--declare @cbmsImageId int  
		, @rcDocId INT
		, @rcContractDocId INT
		, @rcContract INT
		, @cnt INT
		, @DateNow DATETIME
		
	SELECT @cnt = COUNT(1) FROM dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP WHERE account_id = @accountId AND sr_rfp_id = @rfpId

	SELECT @entityId = ENTITY_ID FROM dbo.ENTITY (NOLOCK) WHERE Entity_Name = 'RC for Commercial ' AND Entity_Type = 100

	SELECT @rcDocId = rcDoc.sr_rc_contract_document_id
	FROM dbo.cbms_image cbmsImage
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.rc_image_id = cbmsImage.cbms_image_id
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId
		AND cbmsImage.cbms_image_type_id = @entityId
	  
	SELECT @rcContract =  ISNULL(rcDoc.sr_rc_contract_document_id, 0)
	FROM dbo.cbms_image cbmsImage
		INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.contract_image_id = cbmsImage.cbms_image_id
		INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id
	WHERE accMap.account_id = @accountId
		AND accMap.sr_rfp_id = @rfpId
	
	SET @DateNow = GETDATE()
	  
	IF ( @rcDocId > 0 AND @cnt > 0 AND @rcContract = 0 )
	 BEGIN
	  
		 /*INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)   
		 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())  
		  
		   
		 select @cbmsImageId = (select @@Identity)  */  
		   
		--added by Jaya for updating entityid  
	   
		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	  
		UPDATE dbo.SR_RC_CONTRACT_DOCUMENT
			SET RC_IMAGE_ID = @cbmsImageId,
				UPLOADED_BY = @userId,
				UPLOADED_DATE = @DateNow
		WHERE SR_RC_CONTRACT_DOCUMENT_ID = @rcDocId

	 END
	ELSE IF ( @cnt = 0 )
	 BEGIN
	 
		/* INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)
		 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())

		   
		select @cbmsImageId = (select @@Identity)  */
	   
		--added by Jaya for updating entityid
	   
		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	  
		INSERT INTO dbo.SR_RC_CONTRACT_DOCUMENT (RC_IMAGE_ID
			, UPLOADED_BY,UPLOADED_DATE)
		VALUES( @cbmsImageId
			, @userId
			, @DateNow)
	  
		SELECT @rcContractDocId = SCOPE_IDENTITY() -- @@identity
	  
		INSERT INTO dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP(SR_RC_CONTRACT_DOCUMENT_ID
			, ACCOUNT_ID,SR_RFP_ID)
		VALUES(@rcContractDocId
			, @accountId
			, @rfpId )

	 END
	ELSE IF ( @cnt > 0 AND @rcContract > 0 )
	 BEGIN
	 
		/* INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)   
		 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())  
		  
		   
		 select @cbmsImageId = (select @@Identity)  */  
		   
		 --added by Jaya for updating entityid  
	   
		UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	  
		UPDATE dbo.SR_RC_CONTRACT_DOCUMENT
			SET RC_IMAGE_ID = @cbmsImageId,
				UPLOADED_BY = @userId,  
				UPLOADED_DATE = @DateNow
		WHERE SR_RC_CONTRACT_DOCUMENT_ID = @rcContract

	 END
	  
	UPDATE rfpCheckList
		SET rfpCheckList.is_rc_completed = 1
	FROM dbo.SR_RFP_CHECKLIST rfpCheckList
		INNER JOIN dbo.sr_rfp_account rfpAcct ON rfpAcct.sr_rfp_account_id = rfpCheckList.sr_rfp_account_id
	WHERE rfpAcct.account_id = @accountId
		AND rfpAcct.sr_rfp_id = @rfpId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_UPLOAD_RC_DOCUMENT_P] TO [CBMSApplication]
GO
