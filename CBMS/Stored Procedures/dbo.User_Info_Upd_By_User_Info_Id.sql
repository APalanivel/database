SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******      
Name:      
    dbo.User_Info_Upd_By_User_Info_Id     
     
Description:      
    Used for updating the User info for SSO Details    
     
Input Parameters:      
    Name   DataType Default Description      
------------------------------------------------------------------------      
    @first_name  varchar(40)  
    ,@last_name  varchar(40)  
    ,@email_address VARCHAR(150)   
    ,@user_info_id int     
     
Output Parameters:      
 Name  Datatype  Default Description      
------------------------------------------------------------      
     
Usage Examples:    
------------------------------------------------------------      
   Execute   User_Info_Upd_By_User_Info_Id 'Angela1','Bandy','Jessica.Kipper@ems.schneider-electric.com',1  
     
     
Author Initials:      
 Initials   Name    
------------------------------------------------------------    
 AS			Arun Skaria    
     
 Modifications :      
 Initials   Date			Modification      
------------------------------------------------------------      
  AS		13-Mar-2014		Created
******/    
CREATE  PROCEDURE dbo.User_Info_Upd_By_User_Info_Id
      ( 
       @first_name VARCHAR(40)
      ,@last_name VARCHAR(40)
      ,@email_address VARCHAR(150) = NULL
      ,@user_info_id INT )
AS 
BEGIN      
      SET NOCOUNT ON     
         
      UPDATE
            USER_INFO
      SET   
            FIRST_NAME = @first_name
           ,LAST_NAME = @last_name
           ,EMAIL_ADDRESS = @email_address
      WHERE
            USER_INFO_ID = @user_info_id  
        
      
END 
;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Upd_By_User_Info_Id] TO [CBMSApplication]
GO
