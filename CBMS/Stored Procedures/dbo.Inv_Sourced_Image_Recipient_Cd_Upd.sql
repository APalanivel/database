SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
             
NAME: Inv_Sourced_Image_Recipient_Cd_Upd

DESCRIPTION:

To update the recipient cd (Prokarma/CTE) in the Inv_Sourced_Image table

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
@Cbms_File_Name					VARCHAR(200)			
@DE_Recipient_Cd				INT							Prokarma/CTE

OUTPUT PARAMETERS:
NAME     DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
USAGE EXAMPLES:
-----------------------------------------------------------------------

EXEC dbo.Code_Sel_By_CodeSet_Name
	@CodeSet_Name = 'DataEntryRecipient'

BEGIN TRAN

	EXEC dbo.Inv_Sourced_Image_Recipient_Cd_Upd
		  @Cbms_File_Name = '2340496_CLJRZLUZMAY2014.pdf'
		  ,@DE_Recipient_Cd = 100977

	SELECT
		  *
	FROM
		  dbo.Inv_Sourced_Image
	WHERE
		  Inv_Sourced_Image_Id = 2340496

ROLLBACK TRAN

BEGIN TRAN

	EXEC dbo.Inv_Sourced_Image_Recipient_Cd_Upd
		  @Cbms_File_Name = '2340495CLJRZLUZMAR2014.pdf'
		 ,@DE_Recipient_Cd = 100977

	SELECT
		  *
	FROM
		  dbo.Inv_Sourced_Image
	WHERE
		  Inv_Sourced_Image_Id = 2340495

ROLLBACK TRAN

SELECT * FROM INV_SOURCE WHERE Archive_Folder_Path = '\Ubm\BerryPlastics\Archive'

SELECT TOP 1 * FROM Inv_Sourced_Image ORDER BY Inv_Sourced_Image_Id DESC


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2015-03-25	Created for Invoice scraping recipient enh

*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Image_Recipient_Cd_Upd]
      (
       @Cbms_File_Name VARCHAR(200)
      ,@DE_Recipient_Cd INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Inv_Sourced_Image_Id INT

      BEGIN TRY

		-- Extract the Inv_Sourced_Image_Id from the file name
            SELECT
                  @Inv_Sourced_Image_Id = CASE WHEN ISNUMERIC(CASE WHEN CHARINDEX('_', @Cbms_File_Name, 1) > 1 THEN SUBSTRING(@Cbms_File_Name, 1, CHARINDEX('_', @Cbms_File_Name, 1) - 1)
                                                                   ELSE '0'
                                                              END) = 1 THEN CAST(CASE WHEN CHARINDEX('_', @Cbms_File_Name, 1) > 1 THEN SUBSTRING(@Cbms_File_Name, 1, CHARINDEX('_', @Cbms_File_Name, 1) - 1)
                                                                                      ELSE '0'
                                                                                 END AS DECIMAL)
                                               ELSE 0
                                          END

            SET @INV_SOURCED_IMAGE_ID = ISNULL(@INV_SOURCED_IMAGE_ID, 0)

            UPDATE
                  dbo.Inv_Sourced_Image
            SET
                  DE_Recipient_Cd = @DE_Recipient_Cd
                 ,Recipient_Transfer_Ts = GETDATE()
            WHERE
                  Inv_Sourced_Image_Id = @Inv_Sourced_Image_Id

      END TRY
      BEGIN CATCH
            PRINT 'Invalid file name : ' + @Cbms_File_Name
      END CATCH

END
;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Image_Recipient_Cd_Upd] TO [CBMSApplication]
GO
