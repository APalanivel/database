SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Site_Sel_For_Client_Data_Transfer]   

DESCRIPTION: This procedure used select divisions for   

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------

	DECLARE @tvp_CH_Filter AS [tvp_Client_Data_Transfer_CH_Filter]

	INSERT INTO @tvp_CH_Filter 
	VALUES	(1,1,32877,100015,0)
			,(1,1,194072,100017,0)

	EXEC [Site_Sel_For_Client_Data_Transfer] 10005,@tvp_CH_Filter

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			8 Jul 2019		Created 	
 MSV			10 Sep 2019		ICDT-47 - Sitegroup filter fix.
 
*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Site_Sel_For_Client_Data_Transfer]
	(
		@From_Client_Id INT,
		@tvp_CH_Filter [tvp_Client_Data_Transfer_CH_Filter] READONLY
	)
AS
BEGIN
	 
    SET NOCOUNT ON 

	DECLARE @tvp_Count INT

	CREATE TABLE #Site_Filter
	(
		Client_Hier_Id INT
	)

	SELECT @tvp_Count = COUNT(1)
	FROM @tvp_CH_Filter

	INSERT INTO #Site_Filter
	(
		Client_Hier_Id
	)
	SELECT chsite.Client_Hier_Id
	FROM Core.Client_Hier chsite
	INNER JOIN dbo.Sitegroup_Site ss
		ON chsite.Site_Id = ss.Site_id
	INNER JOIN Core.Client_Hier chsg
		ON chsg.Sitegroup_Id = ss.Sitegroup_id
			AND chsg.Site_Id = 0
	INNER JOIN @tvp_CH_Filter chf
            ON chf.Client_Hier_Id = CASE
                                        WHEN chf.Hier_Level_Cd = 100016 THEN --Site Filter
                                            chsite.Client_Hier_Id
                                        WHEN chf.Hier_Level_Cd IN ( 100015, 100017 ) --Division, Sitegroup Filter
                                             AND chf.Is_Copy_All_Site = 1 THEN
                                            chsg.Client_Hier_Id
                                    END
	WHERE chsite.Client_Id = @From_Client_Id
		AND chsite.Site_Id > 0 
	GROUP BY chsite.Client_Hier_Id


	SELECT ch.Client_Hier_Id,
           ch.Hier_level_Cd,
           ch2.Client_Hier_Id AS Division_Client_Hier_Id
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier ch2
            ON ch2.Site_Id = 0
               AND ch2.Sitegroup_Id = ch.Sitegroup_Id
		LEFT JOIN #Site_Filter sf
			ON sf.Client_Hier_Id = ch.Client_Hier_Id
    WHERE ch.Client_Id = @From_Client_Id
          AND ch.Site_Id > 0 --Sites
          AND
          (
              @tvp_Count = 0
              OR sf.Client_Hier_Id IS NOT NULL
          )
    GROUP BY ch.Client_Hier_Id,
             ch.Hier_level_Cd,
             ch2.Client_Hier_Id;
			
	DROP TABLE #Site_Filter;

END

GO
GRANT EXECUTE ON  [dbo].[Site_Sel_For_Client_Data_Transfer] TO [CBMSApplication]
GO
