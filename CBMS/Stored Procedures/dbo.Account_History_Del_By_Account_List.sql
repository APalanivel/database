SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Account_History_Del_By_Account_List]
     
DESCRIPTION:

	To Delete all the history associated with the given account

INPUT PARAMETERS:          
NAME			DATATYPE		DEFAULT		DESCRIPTION          
---------------------------------------------------------------------------------------------------------------------------------       
@Account_Id_List VARCHAR(max)

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
----------------------------------------------------------------------------------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------------------------------------------------------------------------------

	BEGIN TRAN
		EXEC dbo.Account_History_Del_By_Account_List '4714,5588,5724,6367'
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
----------------------------------------------------------------------------------------------------------------------------------
RR			Raghu Reddy
RKV         Ravi Kumar Vegesna
NR			Narayana Reddy

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------------------------------------------------------------------------------
RR			07/09/2011	MAINT-712, Created to remove all the supplier account history when the last meter of the supplier account 
						moved to the other accounts and reset the invoice_participation at site level.


RKV          2017-03-09 Added Account Inovice Collection History  and Account_Exception Del as Part of Invoice Collection 
RKV			2018-05-30  MAINT-7217 Added Cu_Invoice_Account_Commodity_Comment_Del
RKV         2018-08-09 MAINT-7624	Added Account_Comment_Del
RKV         2019-06-25 As part of Calculation Tester added functionality to delete the account details in table budget.Calculation_Tester_Audit 
NR			2019-11-11	Add Contract - Removed Account_Commodity_Invoice_Recalc_Type data when acount deleted.
NR			2019-12-18	Add Contract - Removed Account_Outside_Contract_Term_Config data when account deleted.

*/

CREATE PROCEDURE [dbo].[Account_History_Del_By_Account_List]
    (
        @Account_Id_List VARCHAR(MAX)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Account_Id INT
            , @Row_Counter INT
            , @Total_Row_Count INT
            , @Account_Exception_Id INT
            , @Calculation_Tester_Audit_Id INT;

        DECLARE @Account_Id_Tbl TABLE
              (
                  Account_Id INT
              );
        DECLARE @Account_Exception TABLE
              (
                  Account_Exception_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );
        DECLARE @Calculation_Tester_Audit TABLE
              (
                  Calculation_Tester_Audit_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        INSERT INTO @Account_Id_Tbl
             (
                 Account_Id
             )
        SELECT  Segments FROM   dbo.ufn_split(@Account_Id_List, ',');

        BEGIN TRY
            BEGIN TRAN;

            WHILE EXISTS (SELECT    Account_Id FROM @Account_Id_Tbl)
                BEGIN

                    SELECT  TOP 1   @Account_Id = Account_Id FROM   @Account_Id_Tbl;

                    EXEC dbo.Invoice_Participation_History_Del_For_Account @Account_Id;

                    EXEC dbo.Account_Other_History_Del_By_Account_Id @Account_Id;

                    EXEC dbo.Account_Inovice_Collection_History_Del_By_Account_Id
                        @Account_Id = @Account_Id;



                    INSERT INTO @Account_Exception
                         (
                             Account_Exception_Id
                         )
                    SELECT
                        ae.Account_Exception_Id
                    FROM
                        dbo.Account_Exception ae
                    WHERE
                        ae.Account_Id = @Account_Id;

                    SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Account_Exception;

                    SET @Row_Counter = 1;
                    WHILE (@Row_Counter <= @Total_Row_Count)
                        BEGIN
                            SELECT
                                @Account_Exception_Id = Account_Exception_Id
                            FROM
                                @Account_Exception
                            WHERE
                                Row_Num = @Row_Counter;

                            DELETE
                            dbo.Account_Exception
                            WHERE
                                Account_Exception_Id = @Account_Exception_Id;

                            SET @Row_Counter = @Row_Counter + 1;
                        END;

                    DELETE  FROM @Account_Exception;


                    INSERT INTO @Calculation_Tester_Audit
                         (
                             Calculation_Tester_Audit_Id
                         )
                    SELECT
                        Calculation_Tester_Audit_Id
                    FROM
                        Budget.Calculation_Tester_Audit
                    WHERE
                        Account_Id = @Account_Id;

                    SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Calculation_Tester_Audit;

                    SET @Row_Counter = 1;
                    WHILE (@Row_Counter <= @Total_Row_Count)
                        BEGIN
                            SELECT
                                @Calculation_Tester_Audit_Id = Calculation_Tester_Audit_Id
                            FROM
                                @Calculation_Tester_Audit
                            WHERE
                                Row_Num = @Row_Counter;

                            DELETE
                            Budget.Calculation_Tester_Audit
                            WHERE
                                Calculation_Tester_Audit_Id = @Calculation_Tester_Audit_Id;

                            SET @Row_Counter = @Row_Counter + 1;
                        END;

                    DELETE  FROM @Calculation_Tester_Audit;


                    DELETE
                    acirt
                    FROM
                        dbo.Account_Commodity_Invoice_Recalc_Type acirt
                    WHERE
                        acirt.Account_Id = @Account_Id;


                    DELETE
                    aoctc
                    FROM
                        dbo.Account_Outside_Contract_Term_Config aoctc
                    WHERE
                        aoctc.Account_Id = @Account_Id;


                    EXEC dbo.Cu_Invoice_Account_Commodity_Comment_Del @Account_Id = @Account_Id;

                    EXEC dbo.Account_Comment_Del @Account_Id;




                    EXEC dbo.Account_Del @Account_Id;


                    DELETE @Account_Id_Tbl WHERE Account_Id = @Account_Id;

                END;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;










GO
GRANT EXECUTE ON  [dbo].[Account_History_Del_By_Account_List] TO [CBMSApplication]
GO
