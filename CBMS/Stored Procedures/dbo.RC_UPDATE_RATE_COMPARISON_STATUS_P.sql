SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_UPDATE_RATE_COMPARISON_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@sessionId     	varchar(20)	          	
	@statusId      	int       	          	
	@rateComparisonId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE DBO.RC_UPDATE_RATE_COMPARISON_STATUS_P 

@userId int,
@sessionId varchar(20),
@statusId int,
@rateComparisonId int

AS
set nocount on
	update RC_RATE_COMPARISON
	
	set STATUS_TYPE_ID=@statusId,REVIEWED_BY=@userId
	
	where RC_RATE_COMPARISON_ID=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[RC_UPDATE_RATE_COMPARISON_STATUS_P] TO [CBMSApplication]
GO
