SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_DELETE_NOT_TO_DO_BUSINESS_MAP_P]


DESCRIPTION: Deeltes data from the PREFER_NOT_TODO_BUSINESS table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@divisionId             int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec CBMS_DELETE_NOT_TO_DO_BUSINESS_MAP_P @divisionId = 2968



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE dbo.CBMS_DELETE_NOT_TO_DO_BUSINESS_MAP_P
@divisionId int

AS


DELETE
	PREFER_NOT_TODO_BUSINESS 
WHERE 
	division_id=@divisionId
GO
GRANT EXECUTE ON  [dbo].[CBMS_DELETE_NOT_TO_DO_BUSINESS_MAP_P] TO [CBMSApplication]
GO
