SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
Su.Client_Links_Del    

DESCRIPTION:    
Used to Delete client links Information in CLIENT_LINKS table  

INPUT PARAMETERS:    
Name			DataType	Default Description    
------------------------------------------------------------    
@CLIENT_ID		INT,   
@LINK_NAME		VARCHAR(100)  

          
OUTPUT PARAMETERS:    
Name			DataType	Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC Su.Client_Links_Del 11231,'hotmail'

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
GB						Created  
SKA		14/07/2009		Modified as per coding standard

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [Su].[Client_Links_Del]    
   @CLIENT_ID INT,   
   @LINK_NAME VARCHAR(100) 
AS    
    

BEGIN    
   
SET NOCOUNT ON;    
     
 BEGIN TRY
		BEGIN TRANSACTION     
		  DELETE dbo.CLIENT_LINKS   
		  WHERE CLIENT_ID = @CLIENT_ID  
			   AND LINK_NAME = @LINK_NAME  
		COMMIT TRANSACTION
		END TRY 
		BEGIN CATCH
			ROLLBACK TRANSACTION
				EXEC dbo.usp_RethrowError                    -- Rethrows the error for the application. 
		END CATCH
END
GO
GRANT EXECUTE ON  [su].[Client_Links_Del] TO [CBMSApplication]
GO
