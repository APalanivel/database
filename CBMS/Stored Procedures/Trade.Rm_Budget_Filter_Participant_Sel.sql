SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Filter_Participant_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Filter_Participant_Sel  584,'2019-01-01','2019-12-01'  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Filter_Participant_Sel]
    (
        @Rm_Budget_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id INT
                 , Filter_Participant_Type VARCHAR(25)
                 , Filter_Participant_Id INT
                 , Filter_Participant_Name VARCHAR(1000)
             );

        INSERT INTO #Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Type
                 , Filter_Participant_Id
                 , Filter_Participant_Name
             )
        SELECT
            rbfp.Rm_Budget_Id
            , c.Code_Value
            , ch.Site_Id
            , ch.City + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
        FROM
            Trade.Rm_Budget_Filter_Participant rbfp
            INNER JOIN dbo.Code c
                ON c.Code_Id = rbfp.RM_Budget_Filter_Participant_Type_Cd
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rbfp.Filter_Participant_Id
        WHERE
            rbfp.Rm_Budget_Id = @Rm_Budget_Id
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Site';

        INSERT INTO #Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Type
                 , Filter_Participant_Id
                 , Filter_Participant_Name
             )
        SELECT
            rbfp.Rm_Budget_Id
            , c.Code_Value
            , ch.Sitegroup_Id
            , ch.Sitegroup_Name
        FROM
            Trade.Rm_Budget_Filter_Participant rbfp
            INNER JOIN dbo.Code c
                ON c.Code_Id = rbfp.RM_Budget_Filter_Participant_Type_Cd
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Sitegroup_Id = rbfp.Filter_Participant_Id
        WHERE
            rbfp.Rm_Budget_Id = @Rm_Budget_Id
            AND ch.Site_Id = 0
            AND ch.Sitegroup_Id > 0
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Division';

        INSERT INTO #Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Type
                 , Filter_Participant_Id
                 , Filter_Participant_Name
             )
        SELECT
            rbfp.Rm_Budget_Id
            , c.Code_Value
            , cs.Cbms_Sitegroup_Id
            , cs.Group_Name
        FROM
            Trade.Rm_Budget_Filter_Participant rbfp
            INNER JOIN dbo.Code c
                ON c.Code_Id = rbfp.RM_Budget_Filter_Participant_Type_Cd
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
            INNER JOIN dbo.Cbms_Sitegroup cs
                ON cs.Cbms_Sitegroup_Id = rbfp.Filter_Participant_Id
        WHERE
            rbfp.Rm_Budget_Id = @Rm_Budget_Id
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'RM Group';

        INSERT INTO #Rm_Budget_Filter_Participant
             (
                 Rm_Budget_Id
                 , Filter_Participant_Type
                 , Filter_Participant_Id
                 , Filter_Participant_Name
             )
        SELECT
            rbfp.Rm_Budget_Id
            , c.Code_Value
            , c3.COUNTRY_ID
            , c3.COUNTRY_NAME
        FROM
            Trade.Rm_Budget_Filter_Participant rbfp
            INNER JOIN dbo.Code c
                ON c.Code_Id = rbfp.RM_Budget_Filter_Participant_Type_Cd
            INNER JOIN dbo.Codeset c2
                ON c2.Codeset_Id = c.Codeset_Id
            INNER JOIN dbo.COUNTRY c3
                ON c3.COUNTRY_ID = rbfp.Filter_Participant_Id
        WHERE
            rbfp.Rm_Budget_Id = @Rm_Budget_Id
            AND c2.Codeset_Name = 'RMBudgetFilterParticipant'
            AND c.Code_Value = 'Country';

        SELECT
            Rm_Budget_Id
            , Filter_Participant_Type
            , Filter_Participant_Id
            , Filter_Participant_Name
        FROM
            #Rm_Budget_Filter_Participant rbfp;

        DROP TABLE #Rm_Budget_Filter_Participant;

    END;
GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Filter_Participant_Sel] TO [CBMSApplication]
GO
