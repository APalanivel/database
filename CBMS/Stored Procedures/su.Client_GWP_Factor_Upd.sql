SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME:        
 Su.Client_GWP_Factor_Upd      
    
DESCRIPTION:        
 Used to update CLIENT_GWP_FACTOR table      
    
INPUT PARAMETERS:        
Name			DataType  Default  Description        
------------------------------------------------------------        
@CLIENT_ID		INT      
@EMISSION_CD	INT      
@CO2E_FACTOR	DECIMAL(16,8)      
              
OUTPUT PARAMETERS:        
Name   DataType  Default  Description        
------------------------------------------------------------        
    
USAGE EXAMPLES:        
------------------------------------------------------------      
 Exec [Su].[Client_GWP_Factor_Upd] 11231,2,'1.5123'    
 
 
AUTHOR INITIALS:        

Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA  Shobhit Kr Agrawal      
    
MODIFICATIONS         
Initials Date  Modification        
------------------------------------------------------------        
GB    Created      
SKA  17-JUL-09 Modified as per coding standard    
SS   27-JUL-09 SP split from  [Su].[Client_GWP_Factor_Ins_Upd]  
SS	 01-AUG-09 Added a new variable @Is_updated 
    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [Su].[Client_GWP_Factor_Upd]    
  @CLIENT_ID INT,      
  @EMISSION_CD INT,      
  @CO2E_FACTOR DECIMAL(16,8) 
        
AS      

BEGIN      
     
SET NOCOUNT ON;      
 BEGIN TRY    
    
  BEGIN TRANSACTION    
         
    UPDATE [Su].[CLIENT_GWP_FACTOR]       
    SET CO2E_FACTOR = @CO2E_FACTOR      
    WHERE CLIENT_ID = @CLIENT_ID       
     AND EMISSION_CD = @EMISSION_CD      
  
  SELECT @@ROWCOUNT AS 'Updated_Rows'
  
  COMMIT TRANSACTION    
 END TRY    
  
 BEGIN CATCH    
     
  ROLLBACK TRAN     
  EXEC dbo.usp_RethrowError    
     
 END CATCH    
     
END
GO
GRANT EXECUTE ON  [su].[Client_GWP_Factor_Upd] TO [CBMSApplication]
GO
