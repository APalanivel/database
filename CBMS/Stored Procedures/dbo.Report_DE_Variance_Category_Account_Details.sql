
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
             
/******            
              
 NAME: dbo.Report_DE_Variance_Category_Account_Details            
              
 DESCRIPTION:              
     variance category details for a given client id.            
              
 INPUT PARAMETERS:              
             
 Name                               DataType        Default     Description              
---------------------------------------------------------------------------------------------------------------            
 @Client_Id_List     VARCHAR(MAX)                                          
 @Start_Date                        DATETIME            
 @End_Date                          DATETIME            
               
 OUTPUT PARAMETERS:             
                    
 Name                               DataType        Default     Description              
---------------------------------------------------------------------------------------------------------------            
              
              
 USAGE EXAMPLES:                  
---------------------------------------------------------------------------------------------------------------            
            
 EXEC  dbo.Report_DE_Variance_Category_Account_Details  11682,'2013-10-15','2013-10-18'            
             
 AUTHOR INITIALS:            
              
 Initials               Name              
---------------------------------------------------------------------------------------------------------------            
  NR                    Narayana Reddy                
               
 MODIFICATIONS:             
             
 Initials               Date            Modification            
---------------------------------------------------------------------------------------------------------------            
  NR                    2013-12-04      Created             
             
******/                           
                                 
              
CREATE PROCEDURE dbo.Report_DE_Variance_Category_Account_Details
      ( 
       @Client_Id_List VARCHAR(MAX)
      ,@Start_Date DATE
      ,@End_Date DATE )
AS 
BEGIN                                
      SET NOCOUNT ON;               
            
            
      DECLARE @ClientIds TABLE
            ( 
             Client_Id INT PRIMARY KEY )              
              
      INSERT      @ClientIds
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Client_Id_List, ',')              
                                            
            
--Failed Recalc Ids            
      CREATE TABLE #Cu_Invoice_Category
            ( 
             Cu_Invoice_Id INT
            ,Category_Name VARCHAR(50)
            ,Client_Id INT
            ,Account_Id INT NULL
            ,CU_INVOICE_EVENT_ID INT )              
            
      INSERT      INTO #Cu_Invoice_Category
                  ( 
                   Category_Name
                  ,Cu_Invoice_Id
                  ,Client_Id
                  ,Account_Id
                  ,CU_INVOICE_EVENT_ID )                        
      --  failedrecalc_cte            
                  SELECT
                        'FailedRecalc' [Category]
                       ,ce.CU_INVOICE_ID
                       ,c.Client_Id
                       ,cued.Account_ID
                       ,cie.CU_INVOICE_EVENT_ID
                  FROM
                        cu_exception ce
                        JOIN dbo.CU_EXCEPTIOn_detail cued
                              ON cued.CU_EXCEPTION_ID = ce.cu_exception_id
                        JOIN dbo.CU_EXCEPTION_TYPE cuet
                              ON cuet.cu_EXCEPTION_TYPE_ID = cued.EXCEPTION_TYPE_ID
                        JOIN cu_invoice cui
                              ON cui.CU_INVOICE_ID = ce.CU_INVOICE_ID
                        JOIN dbo.cu_invoice_event cie
                              ON cui.CU_INVOICE_ID = cie.CU_INVOICE_ID
                        JOIN @ClientIds C
                              ON cui.CLIENT_ID = C.Client_Id
                  WHERE
                        cuet.EXCEPTION_TYPE = 'failed recalc'
                        AND cui.IS_REPORTED = 1
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_DUPLICATE != 1
                        AND cui.is_dnt != 1
                        AND cie.EVENT_DESCRIPTION LIKE 'Invoice Inserted%'
                        AND cie.event_date BETWEEN @Start_Date AND @End_Date
                  GROUP BY
                        ce.CU_INVOICE_ID
                       ,c.Client_Id
                       ,cued.Account_ID
                       ,cie.CU_INVOICE_EVENT_ID       
    
      INSERT      INTO #Cu_Invoice_Category
                  ( 
                   Category_Name
                  ,Cu_Invoice_Id
                  ,Client_Id
                  ,Account_Id
                  ,CU_INVOICE_EVENT_ID )
                  SELECT
                        [Category] = CASE WHEN vl.category_name = 'Usage' THEN 'Volume'
                                          ELSE vl.Category_Name
                                     END
                       ,cui.CU_INVOICE_ID
                       ,c.Client_Id
                       ,vl.Account_ID
                       ,cie.CU_INVOICE_EVENT_ID
                  FROM
                        variance_log vl
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                              ON cuism.Account_ID = vl.Account_ID
                                 AND cuism.SERVICE_MONTH = vl.Service_Month
                        JOIN cu_invoice cui
                              ON cui.CU_INVOICE_ID = cuism.CU_INVOICE_ID
                        JOIN dbo.cu_invoice_event cie
                              ON cui.CU_INVOICE_ID = cie.CU_INVOICE_ID
                        JOIN @ClientIds C
                              ON cui.CLIENT_ID = C.Client_Id
                  WHERE
                        vl.is_failure = 1
                        AND cui.IS_REPORTED = 1
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_DUPLICATE != 1
                        AND cui.is_dnt != 1
                        AND cie.EVENT_DESCRIPTION LIKE 'Invoice Inserted%'
                        AND cie.event_date BETWEEN @Start_Date AND @End_Date
                  GROUP BY
                        cui.CU_INVOICE_ID
                       ,vl.category_name
                       ,c.Client_Id
                       ,vl.Account_ID
                       ,cie.CU_INVOICE_EVENT_ID         
            
      CREATE CLUSTERED INDEX IX_INV_Invoice_Category ON #Cu_Invoice_Category ( Category_Name,CU_Invoice_ID,Client_Id,Account_Id );            
                                   
      SELECT
            invcat.Category_Name
           ,cue.cu_invoice_id
           ,cha.Account_Id
           ,cha.Account_Number
           ,ch.Site_name
           ,cue.EVENT_DESCRIPTION
           ,cue.EVENT_DATE
           ,ch.State_Name
           ,ch.Country_Name
      FROM
            #Cu_Invoice_Category invcat
            INNER JOIN dbo.cu_invoice_event cue
                  ON invcat.CU_INVOICE_EVENT_ID = cue.CU_INVOICE_EVENT_ID
            JOIN cu_invoice cui
                  ON cui.CU_INVOICE_ID = cue.CU_INVOICE_ID
            JOIN Core.Client_Hier_Account CHA
                  ON invcat.Account_ID = CHA.Account_Id
            JOIN Core.Client_Hier CH
                  ON CH.Client_Hier_Id = CHA.Client_Hier_Id
      GROUP BY
            invcat.Category_Name
           ,cue.cu_invoice_id
           ,cha.Account_Id
           ,cha.Account_Number
           ,ch.Site_name
           ,cue.EVENT_DESCRIPTION
           ,cue.EVENT_DATE
           ,ch.State_Name
           ,ch.Country_Name
      ORDER BY
            invcat.Category_Name
           ,cue.cu_invoice_id
           ,cue.EVENT_DATE              
END     
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Variance_Category_Account_Details] TO [CBMS_SSRS_Reports]
GO
