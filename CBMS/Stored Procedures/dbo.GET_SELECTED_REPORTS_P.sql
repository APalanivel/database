SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	GET_SELECTED_REPORTS_P	

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId		INT
	@@StartIndex	INT				= 1,
	@endIndex		INT				= 2147483647
      	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	[dbo].[GET_SELECTED_REPORTS_P]  218,1,100
	[dbo].[GET_SELECTED_REPORTS_P]  10069,1,100

    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	SKA			06/28/2010	Added pagination logic        	

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[GET_SELECTED_REPORTS_P]
      @clientId INT
     ,@StartIndex INT = 1
     ,@endIndex INT = 2147483647
AS 

BEGIN

      SET NOCOUNT ON ;
      DECLARE @RM_BATCH_CONFIGURATION_MASTER_ID INT
 
      SELECT
            @RM_BATCH_CONFIGURATION_MASTER_ID = MAX(batch.RM_BATCH_CONFIGURATION_MASTER_ID)
      FROM
            dbo.RM_BATCH_CONFIGURATION_MASTER confmaster
            JOIN dbo.RM_BATCH_CONFIGURATION batch ON batch.RM_BATCH_CONFIGURATION_MASTER_ID = confmaster.RM_BATCH_CONFIGURATION_MASTER_ID
      WHERE
            batch.client_id = @clientId ;
 
      WITH  Cte_GET_SELECTED_REPORTS_P
              AS ( SELECT
                        list.report_name REPORT_NAME
                       ,details.site_id SITE_ID
                       ,details.division_id DIVISION_ID
                       ,Total = COUNT(1) OVER ( )
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY REPORT_NAME )
                   FROM
                        dbo.rm_batch_configuration_details details
                        JOIN dbo.rm_batch_configuration batch ON details.rm_batch_configuration_id = batch.rm_batch_configuration_id
                        JOIN dbo.report_list list ON list.report_list_id = batch.report_list_id
                   WHERE
                        batch.RM_BATCH_CONFIGURATION_MASTER_ID = @RM_BATCH_CONFIGURATION_MASTER_ID)
            SELECT
                  REPORT_NAME
                 ,SITE_ID
                 ,DIVISION_ID
                 ,Total
            FROM
                  Cte_GET_SELECTED_REPORTS_P
            WHERE
                  Row_Num BETWEEN @StartIndex AND @endIndex

END
GO
GRANT EXECUTE ON  [dbo].[GET_SELECTED_REPORTS_P] TO [CBMSApplication]
GO
