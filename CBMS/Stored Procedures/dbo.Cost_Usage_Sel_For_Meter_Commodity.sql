
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cost_Usage_Sel_For_Meter_Commodity]  
     
DESCRIPTION:

	To get cost usage Information for selected Meter and Commodity Ids with pagination.
	Cost_Usage values will be selected only of any of the bucket value is not equal to 0.

INPUT PARAMETERS:
NAME			DATATYPE DEFAULT		DESCRIPTION
------------------------------------------------------------
@Meter_Id		INT
@Commodity_Id	INT					Commodity of the meter
@Start_Index	INT	    1
@End_Index	INT	    2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Cost_Usage_Sel_For_Meter_Commodity  62461,290,1,10
	EXEC Cost_Usage_Sel_For_Meter_Commodity  559,291,1,10	
	EXEC Cost_Usage_Sel_For_Meter_Commodity  357237,291,1,10
	
     
AUTHOR INITIALS:
INITIALS	  NAME
------------------------------------------------------------
PNR		  PANDARINATH
RR		  Raghu Reddy
AP		  Athmaram Pabbathi
          
MODIFICATIONS           
INITIALS	  DATE	    MODIFICATION          
------------------------------------------------------------          
PNR		  07-JUNE-10	CREATED   
RR		  2012-02-01	Altered condition for natural gas as any one usage or cost or tax should not be zero from cost or usage not be zero 
						along with tax not eqaul to zero (Delete tool enhancement - USERDATAENTRY-519) 
AP		  2012-04-13	Addnl Data Changes
						--Removed reference of Cost_Usage Table & CTE Used for it
						--Replaced Meter table with CHA
RR		  2012-05-25	ADLDT-272 Commdity filter not applied on dbo.Cost_Usage_Account_Dtl table, if the account has the data for some commodity,
						the same data is returning for other commodity meter(s) also under that account. Removed table variable.
						Sub query is removed and directly joined with Core.Client_Hier_Account table.
*/

CREATE PROCEDURE [DBO].Cost_Usage_Sel_For_Meter_Commodity
      ( 
       @Meter_Id INT
      ,@Commodity_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON ;
	
      WITH  Cte_service_month
              AS ( SELECT
                        acdtl.Service_Month
                       ,Row_Num = row_number() OVER ( ORDER BY acdtl.Service_Month )
                       ,Total_Rows = count(1) OVER ( )
                   FROM
                        dbo.Cost_Usage_Account_Dtl acdtl
                        INNER JOIN dbo.Bucket_Master bm
                              ON acdtl.Bucket_Master_Id = bm.Bucket_Master_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = acdtl.Client_Hier_Id
                                 AND cha.Account_Id = acdtl.Account_Id
                                 AND bm.Commodity_Id = cha.Commodity_Id
                   WHERE
                        cha.Meter_Id = @Meter_Id
                        AND bm.Commodity_Id = @Commodity_Id
                   GROUP BY
                        acdtl.Service_Month)
            SELECT
                  Service_Month = datename(MONTH, cte_sm.Service_Month) + space(1) + datename(YYYY, cte_sm.Service_Month)
                 ,cte_sm.Total_Rows
            FROM
                  Cte_service_month cte_sm
            WHERE
                  cte_sm.Row_Num BETWEEN @Start_Index
                                 AND     @End_Index

END
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Sel_For_Meter_Commodity] TO [CBMSApplication]
GO
