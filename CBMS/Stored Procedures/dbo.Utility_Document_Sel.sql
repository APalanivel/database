SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Document_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Utility_Document_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Utility_Document_Sel 9963
		
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
    
CREATE PROC dbo.Utility_Document_Sel ( @Vendor_Id INT )
AS 
BEGIN    
     
      SET nocount ON ;    
    
      SELECT
            ud.Utility_Document_Id
           ,ud.VENDOR_ID
           ,ud.CBMS_IMAGE_ID
           ,ci.CBMS_DOC_ID
           ,v.VENDOR_NAME
           ,ud.Utility_Document_Type_ID
           ,c.Code_Value AS Utility_Document_Type
      FROM
            dbo.Utility_Document ud
            INNER JOIN dbo.VENDOR v
                  ON v.VENDOR_ID = ud.VENDOR_ID
            INNER JOIN dbo.cbms_image ci
                  ON ci.CBMS_IMAGE_ID = ud.CBMS_IMAGE_ID
            INNER JOIN dbo.Code c
                  ON c.Code_Id = ud.Utility_Document_Type_ID
      WHERE
            ud.VENDOR_ID = @Vendor_Id  
END 

GO
GRANT EXECUTE ON  [dbo].[Utility_Document_Sel] TO [CBMSApplication]
GO
