SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.Invoice_Data_Quality_Test_Comment_Ins  
    
DESCRIPTION:    
    To save the invoice data quality test comments.
    
INPUT PARAMETERS:    
  Name						DataType				Default		Description    
-------------------------------------------------------------------------------------    
  @Cu_Invoice_Id			INT
  @User_Info_Id				INT
  @Comment_Text				VARCHAR(MAX)			NULL


OUTPUT PARAMETERS:    
  Name						DataType				Default		Description    
-------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------  
BEGIN TRAN

Select * from dbo.Invoice_Data_Quality_Test_Comment


EXEC dbo.Invoice_Data_Quality_Test_Comment_Ins
    @Cu_Invoice_Id = 10
    , @Comment_Text = 'Welcome Data quality'
    , @User_Info_Id = 49

Select * from dbo.Invoice_Data_Quality_Test_Comment

ROLLBACK TRAN

  

    
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------------------------    
 NR			Narayana Reddy   
     
MODIFICATIONS    
    
 Initials	Date		Modification    
-------------------------------------------------------------------------------------    
 NR			2019-04-24  Data2.0 -  Created for Data Quality.   

******/
CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Test_Comment_Ins]
     (
         @Cu_Invoice_Id INT
         , @Comment_Text VARCHAR(MAX) = NULL
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Comment_Id INT = NULL
            , @Comment_Dt DATETIME = GETDATE()
            , @Comment_Type_Cd INT;

        SELECT
            @Comment_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'CommentType'
            AND c.Code_Value = 'Data Quality Test Comment';


        IF @Comment_Text IS NOT NULL
            BEGIN

                EXEC dbo.Comment_Ins
                    @Comment_Text = @Comment_Text
                    , @Comment_User_Info_Id = @User_Info_Id
                    , @Comment_Type_CD = @Comment_Type_Cd
                    , @Comment_Dt = @Comment_Dt
                    , @Comment_Id = @Comment_Id OUTPUT;

            END;



        IF @Comment_Id IS NOT NULL
            BEGIN

                INSERT INTO dbo.Invoice_Data_Quality_Test_Comment
                     (
                         Cu_Invoice_Id
                         , Comment_Id
                         , Created_User_Id
                         , Created_Ts
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                VALUES
                    (@Cu_Invoice_Id
                     , @Comment_Id
                     , @User_Info_Id
                     , GETDATE()
                     , @User_Info_Id
                     , GETDATE());


            END;



    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Test_Comment_Ins] TO [CBMSApplication]
GO
