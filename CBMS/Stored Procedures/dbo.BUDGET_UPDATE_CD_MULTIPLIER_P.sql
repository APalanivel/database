SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--select * from country where state_name like '%us%'
--select * from state where state_name like '%us%'

--select * from budget_details where budget_account_id=11396

--select * from budget_account where budget_id=485
CREATE           PROCEDURE dbo.BUDGET_UPDATE_CD_MULTIPLIER_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budgetId int,
	@stateId int,
	@conversionFactor decimal(32,16)
	
	AS
	begin
	set nocount on

		if @stateId > 0
		begin

		update budget_account set cd_create_multiplier = @conversionFactor
		where budget_account_id in(select bacc.budget_account_id from
					account a, vwSiteName site, budget_account bacc,
					budget_account_type_vw accountTypeVw,
					budget budget
					where bacc.budget_id = @budgetId
					and budget.budget_id = bacc.budget_id	
					and bacc.account_id= a.account_id										
					and accountTypeVw.budget_account_id = bacc.budget_account_id
					and accountTypeVw.budget_id = budget.budget_id
					and accountTypeVw.budget_account_type in('C&D','C&D Created')
					and a.site_id= site.site_id
					and site.state_id = @stateId )
		end 
else
		begin
			update budget_account set cd_create_multiplier = @conversionFactor
			where budget_account_id in(select bacc.budget_account_id from
						account a, vwSiteName site, budget_account bacc, 
						state stat,country country,
						budget_account_type_vw accountTypeVw,
						budget budget
						where bacc.budget_id = @budgetId
                                                and budget.budget_id = bacc.budget_id	
					        and bacc.account_id= a.account_id			
						and accountTypeVw.budget_account_id = bacc.budget_account_id
						and accountTypeVw.budget_id = budget.budget_id
						and accountTypeVw.budget_account_type in('C&D','C&D Created')
						and a.site_id= site.site_id
						and site.state_id = stat.state_id
                                                and stat.country_id=country.country_id
						and country.country_name not in('USA', 'Canada') )

		end
		
	end




















GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_CD_MULTIPLIER_P] TO [CBMSApplication]
GO
