SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_RATE_SCHEDULE_ID_P
	@rateId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT TOP 1 rate_schedule_id
	FROM dbo.rate_schedule 
	WHERE rate_id= @rateId
	ORDER BY rate_schedule_id DESC

END
GO
GRANT EXECUTE ON  [dbo].[GET_RATE_SCHEDULE_ID_P] TO [CBMSApplication]
GO
