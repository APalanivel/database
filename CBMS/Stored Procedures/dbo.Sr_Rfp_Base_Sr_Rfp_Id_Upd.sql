SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Base_Sr_Rfp_Id_Upd]
           
DESCRIPTION:             
			To upadte base rfi id of the cloned/new RFP with base RFP

INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@New_Sr_Rfp_Id		INT
    @Base_Sr_Rfp_Id		INT
    

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  


USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRANSACTION
		SELECT * FROM dbo.Sr_Rfp WHERE SR_RFP_ID = 9999
		EXEC dbo.Sr_Rfp_Base_Sr_Rfp_Id_Upd  9999,999
		SELECT * FROM dbo.Sr_Rfp WHERE SR_RFP_ID = 9999
	ROLLBACK TRANSACTION
		
AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-05	Global Sourcing - Phase3 - GCS-521 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Base_Sr_Rfp_Id_Upd]
      ( 
       @New_Sr_Rfp_Id INT
      ,@Base_Sr_Rfp_Id INT )
AS 
BEGIN

      UPDATE
            rfp
      SET   
            rfp.Base_Sr_Rfp_Id = @Base_Sr_Rfp_Id
      FROM
            dbo.SR_RFP rfp
      WHERE
            rfp.SR_RFP_ID = @New_Sr_Rfp_Id
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Base_Sr_Rfp_Id_Upd] TO [CBMSApplication]
GO
