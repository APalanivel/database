SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
	Su.Client_GWP_Factor_Ins
  
DESCRIPTION:      
	Used to Insert CLIENT_GWP_FACTOR table    
  
INPUT PARAMETERS:      
Name			DataType  Default  Description      
------------------------------------------------------------      
@CLIENT_ID		INT    
@EMISSION_CD	INT    
@CO2E_FACTOR	Decimal(16,8)    
            
OUTPUT PARAMETERS:      
Name			DataType  Default  Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:      
------------------------------------------------------------    
  
	Exec [Su].[Client_GWP_Factor_Ins] 11231,2,'1.2123'  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
GB   Geetansu Behera      
CMH  Chad Hattabaugh       
HG   Hari     
SKA  Shobhit Kr Agrawal    
  
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
GB				Created    
SKA				Newly Created this SP, as it was not existing  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [Su].[Client_GWP_Factor_Ins]  
	 @CLIENT_ID INT,    
	 @EMISSION_CD INT,    
	 @CO2E_FACTOR DECIMAL(16,8)
AS    

BEGIN    
   
SET NOCOUNT ON;    
 BEGIN TRY  
  
  BEGIN TRANSACTION  
       
    INSERT INTO  [Su].[CLIENT_GWP_FACTOR] (CLIENT_ID, Emission_Cd, CO2E_FACTOR)
    SELECT @CLIENT_ID, @EMISSION_CD, @CO2E_FACTOR    
     
  COMMIT TRANSACTION  
 END TRY  

 BEGIN CATCH  
   
  ROLLBACK TRAN   
  EXEC dbo.usp_RethrowError  
   
 END CATCH  
   
END
GO
GRANT EXECUTE ON  [su].[Client_GWP_Factor_Ins] TO [CBMSApplication]
GO
