SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_VENDORS_UNDER_CONTRACT_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@contractTypeid	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_RFP_GET_VENDORS_UNDER_CONTRACT_TYPE_P 
@userId varchar,
@sessionId varchar,
@contractTypeid int

as
set nocount on
DECLARE @contractType varchar(10)
SELECT @contractType = (select entity_name from entity where entity_id=@contractTypeid )

Select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v, ENTITY e
	

where 	v.VENDOR_TYPE_ID = e.ENTITY_ID AND
	E.ENTITY_NAME = @contractType
	

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VENDORS_UNDER_CONTRACT_TYPE_P] TO [CBMSApplication]
GO
