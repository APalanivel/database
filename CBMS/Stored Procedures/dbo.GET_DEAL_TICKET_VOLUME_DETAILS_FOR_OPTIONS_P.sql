SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec GET_DEAL_TICKET_VOLUME_DETAILS_FOR_OPTIONS_P 1,1,331,100175,1,264,638,593,0,2244,0


CREATE     PROCEDURE DBO.GET_DEAL_TICKET_VOLUME_DETAILS_FOR_OPTIONS_P

@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@dealTicketId integer ,
@currencyUnit integer,
@consumptionUnit integer,
@hedgeTypeId integer,
@hedgeLevelTypeId integer,
@divisionId integer,
@siteId integer,

--fix for BZ4933
--@contractId integer,
@cId integer,

@startDate datetime,
@endDate datetime,

--added for Group
@groupId integer



AS


begin
	set nocount on
		DECLARE @CORPORATE varchar(10)
		DECLARE @DIVISION varchar(10)
		DECLARE @PHYSICAL varchar(10)
		DECLARE @FINANCIAL varchar(10)
		DECLARE @hedgeTypeId1 varchar(10)
		DECLARE @whereClause varchar(10) --Where Clause For Physical Deal 
		DECLARE @fromClause varchar(10)  --From Clause For Physical Deal 
		DECLARE @GROUP varchar(10) --for groups
		--fix for BZ4933
		DECLARE @contractId integer
		SELECT @contractId = 0		

		SELECT @CORPORATE='CORPORATE'
		SELECT @DIVISION='DIVISION'
		SELECT @PHYSICAL='PHYSICAL'
		SELECT @FINANCIAL='FINANCIAL'
		SELECT @GROUP = 'GROUP'

if @hedgeTypeId=0
	
	SELECT @hedgeTypeId=entity_id from entity where entity_type=273 and entity_name=@FINANCIAL
	SELECT @hedgeTypeId1=entity_id from entity where entity_type=273 and entity_name=@PHYSICAL

	

if @hedgeLevelTypeId=0

	if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId=0
		SELECT @hedgeLevelTypeId=entity_id from entity where entity_type=262 and entity_name=@CORPORATE
		
	else if @clientId>0 and @divisionId>0 and @siteId=0 and @groupId=0
		SELECT @hedgeLevelTypeId=Entity_id from entity where entity_type=262 and entity_name=@DIVISION
	--added for groups
	else if @clientId>0 and @divisionId>0 and @siteId=0 and @groupId=0
		SELECT @hedgeLevelTypeId=Entity_id from entity where entity_type=262 and entity_name=@GROUP
	--added for groups uptohere

	--PRINT @hedgeLevelTypeId
	--PRINT @hedgeTypeId

if @currencyUnit>0 and @consumptionUnit>0

BEGIN

	if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)=@currencyUnit OR (select count(*) from RM_CURRENCY_UNIT_CONVERSION where client_id=@clientId)=0)

	BEGIN

		if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId=0
			BEGIN

					SELECT 
							(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
							STRIKE_PRICE,
							PREMIUM_PRICE,
							1.00 conversion_factor,
							ent.ENTITY_NAME,
							DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
							DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
							rmdtvd.site_id,
							rmdtd.RM_DEAL_TICKET_DETAILS_ID,
							rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
						
					FROM
							RM_DEAL_TICKET_DETAILS rmdtd,
							RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
							RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
							ENTITY ent,
							CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard
						
					WHERE
							rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdtd.month_identifier between @startDate and @endDate and
							rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
							consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
							consumption.CONVERTED_UNIT_ID=@consumptionUnit and
							onboard.site_id=rmdtvd.site_id and
							rmdtvd.site_id in 
							(
								
								select	
									DISTINCT RM_ONBOARD_HEDGE_SETUP.SITE_ID
								from
									RM_ONBOARD_CLIENT,
									RM_ONBOARD_HEDGE_SETUP
								where
									RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
									RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID AND
									RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
									RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=@hedgeLevelTypeId
									
							)
					ORDER BY 
							rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
							rmdtvd.site_id,
							ent.DISPLAY_ORDER
			END

		else if @clientId>0 and @divisionId>0 and @siteId=0 and @groupId=0
		
			BEGIN

						SELECT 
						(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
						STRIKE_PRICE,
						PREMIUM_PRICE,
						1.00 conversion_factor,
						ent.ENTITY_NAME,

						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtd.RM_DEAL_TICKET_DETAILS_ID,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
					
					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
						ENTITY ent,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard
					
					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.month_identifier between @startDate and @endDate and
						rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						onboard.site_id=rmdtvd.site_id and
						rmdtvd.site_id in 
						(
						    SELECT 
								RM_ONBOARD_HEDGE_SETUP.SITE_ID 
						    FROM 
								RM_ONBOARD_CLIENT,
								RM_ONBOARD_HEDGE_SETUP 
						    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.DIVISION_ID=@divisionId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
								( (select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId )
						)
					ORDER BY 
						rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
						rmdtvd.site_id,
						ent.DISPLAY_ORDER
			END
			
			--added for group
			else if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId>0
		
			BEGIN

						SELECT 
						(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
						STRIKE_PRICE,
						PREMIUM_PRICE,
						1.00 conversion_factor,
						ent.ENTITY_NAME,

						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtd.RM_DEAL_TICKET_DETAILS_ID,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
					
					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
						ENTITY ent,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard
					
					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.month_identifier between @startDate and @endDate and
						rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						onboard.site_id=rmdtvd.site_id and
						rmdtvd.site_id in 
						(
						    SELECT 
								RM_ONBOARD_HEDGE_SETUP.SITE_ID 
						    FROM 
								RM_ONBOARD_CLIENT,
								RM_ONBOARD_HEDGE_SETUP 
						    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
								( (select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId )
						)
					ORDER BY 
						rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
						rmdtvd.site_id,
						ent.DISPLAY_ORDER
			END

			--added up to here for Group

			else if @clientId>0 and @divisionId=0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId>0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId=0 and @siteId>0 and @groupId>0
			BEGIN
					if @contractId>0
					BEGIN

							SELECT 
							(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
							STRIKE_PRICE,
							PREMIUM_PRICE,
							1.00 conversion_factor,
							ent.ENTITY_NAME,
							DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
							DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
							rmdtvd.site_id,
							rmdtd.RM_DEAL_TICKET_DETAILS_ID,
							rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
						
						FROM
							RM_DEAL_TICKET rmdt,
							RM_DEAL_TICKET_DETAILS rmdtd,
							RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
							RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
							ENTITY ent,
							CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard
							
						
						WHERE
							rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdtd.month_identifier between @startDate and @endDate and
							rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdt.contract_id=@contractId and
							rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
							consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
							consumption.CONVERTED_UNIT_ID=@consumptionUnit and
							rmdtvd.site_id=onboard.site_id and
							onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
							rmdtvd.site_id =@siteId 
							

						ORDER BY 
							rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
							rmdtvd.site_id,
							ent.DISPLAY_ORDER
					END

					else
					BEGIN
						SELECT 
							(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
							STRIKE_PRICE,
							PREMIUM_PRICE,
							1.00 conversion_factor,
							ent.ENTITY_NAME,
							DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
							DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
							rmdtvd.site_id,
							rmdtd.RM_DEAL_TICKET_DETAILS_ID,
							rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID

						
						FROM
							RM_DEAL_TICKET_DETAILS rmdtd,
							RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
							RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
							ENTITY ent,
							CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard
							
						
						WHERE
							rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdtd.month_identifier between @startDate and @endDate and
							rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
							rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
							consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
							consumption.CONVERTED_UNIT_ID=@consumptionUnit and
							rmdtvd.site_id=onboard.site_id and
							onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
							rmdtvd.site_id =@siteId

						ORDER BY 
							rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
							rmdtvd.site_id,
							ent.DISPLAY_ORDER
					END
			END
		END
	else if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)!=@currencyUnit)

		BEGIN--6TH

		if((select CURRENCY_UNIT_NAME from CURRENCY_UNIT where CURRENCY_UNIT_ID=@currencyUnit)!='CAN')
		BEGIN--7TH

		if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId=0
			BEGIN

				SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					(1/currency.CONVERSION_FACTOR),
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
						select	
							DISTINCT RM_ONBOARD_HEDGE_SETUP.SITE_ID
						from
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP
						where
							RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
							RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID AND
							RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
							RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=@hedgeLevelTypeId
							
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END

		else if @clientId>0 and @divisionId>0 and @siteId=0 and @groupId=0

			BEGIN

					SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					(1/currency.CONVERSION_FACTOR),
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
					    SELECT 
							RM_ONBOARD_HEDGE_SETUP.SITE_ID 
					    FROM 
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP 
					    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND

								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.DIVISION_ID=@divisionId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
							( ( select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId)
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END

			--added for groups
			else if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId>0

			BEGIN

					SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					(1/currency.CONVERSION_FACTOR),
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
					    SELECT 
							RM_ONBOARD_HEDGE_SETUP.SITE_ID 
					    FROM 
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP 
					    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
							( ( select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId)
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END
			--added for groups upto here
		else if @clientId>0 and @divisionId=0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId>0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId=0 and @siteId>0 and @groupId>0
			BEGIN

			if @contractId>0
			  begin
				
				SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					(1/currency.CONVERSION_FACTOR),
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET rmdt,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdt.CONTRACT_ID=@contractId AND
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					rmdtvd.site_id=onboard.site_id AND
					onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
					rmdtvd.site_id =@siteId
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER


			  end
			else
			  BEGIN
				SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					(1/currency.CONVERSION_FACTOR),
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					rmdtvd.site_id=onboard.site_id AND
					onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
					rmdtvd.site_id =@siteId
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
				END
			END
		
		
		END--7TH CLOSED

		ELSE 
			--PRINT 'can-TO USD '
				BEGIN--11TH

				if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId=0
			BEGIN

				SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					currency.CONVERSION_FACTOR,
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
						select	
							DISTINCT RM_ONBOARD_HEDGE_SETUP.SITE_ID
						from
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP
						where
							RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
							RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID AND
							RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
							RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=@hedgeLevelTypeId
							
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END

		else if @clientId>0 and @divisionId>0 and @siteId=0 and @groupId=0

			BEGIN

					SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					currency.CONVERSION_FACTOR,
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,

					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,

					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
					    SELECT 
							RM_ONBOARD_HEDGE_SETUP.SITE_ID 
					    FROM 
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP 
					    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.DIVISION_ID=@divisionId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
							( ( select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId)
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END
			--added for Groups
			else if @clientId>0 and @divisionId=0 and @siteId=0 and @groupId>0

			BEGIN

					SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					currency.CONVERSION_FACTOR,
					ent.ENTITY_NAME,
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,

					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,

					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,
					ENTITY ent,
					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					onboard.site_id=rmdtvd.site_id and
					rmdtvd.site_id in 
					(
					    SELECT 
							RM_ONBOARD_HEDGE_SETUP.SITE_ID 
					    FROM 
							RM_ONBOARD_CLIENT,
							RM_ONBOARD_HEDGE_SETUP 
					    WHERE
								RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
								RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
								RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId AND
								RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
							( ( select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelTypeId)
					)
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
			END
			--added for groups
		else if @clientId>0 and @divisionId=0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId>0 and @siteId>0 and @groupId=0 or @clientId>0 and @divisionId=0 and @siteId>0 and @groupId>0
			BEGIN
				if @contractId>0
				BEGIN

					SELECT 
						(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
						STRIKE_PRICE,
						PREMIUM_PRICE,
						currency.CONVERSION_FACTOR,
						ent.ENTITY_NAME,
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtd.RM_DEAL_TICKET_DETAILS_ID,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
					
					FROM
						
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET rmdt,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
						RM_CURRENCY_UNIT_CONVERSION currency,
						ENTITY ent,
						RM_ONBOARD_HEDGE_SETUP onboard,
						CONSUMPTION_UNIT_CONVERSION consumption 
						
						
					
					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.month_identifier between @startDate and @endDate and
						rmdt.RM_DEAL_TICKET_ID=@dealTicketId and 
						rmdt.contract_id=@contractId and
						rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
						rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=100141)and
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						rmdtvd.site_id=onboard.site_id and
						onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
						rmdtvd.site_id =@siteId
			
					ORDER BY 
						rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
						rmdtvd.site_id,
						ent.DISPLAY_ORDER
				END

				ELSE
				
				BEGIN

					SELECT 
					(rmdtvd.HEDGE_VOLUME*consumption.CONVERSION_FACTOR) HEDGE_VOLUME,
					STRIKE_PRICE,
					PREMIUM_PRICE,
					currency.CONVERSION_FACTOR,
					ent.ENTITY_NAME,

					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtd.RM_DEAL_TICKET_DETAILS_ID,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID
				
				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
					RM_CURRENCY_UNIT_CONVERSION currency,

					ENTITY ent,

					RM_ONBOARD_HEDGE_SETUP onboard,
					CONSUMPTION_UNIT_CONVERSION consumption 
				
				WHERE
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.month_identifier between @startDate and @endDate and
					rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
					rmdtod.OPTION_TYPE_ID=ent.ENTITY_id AND
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =
					(
						select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)
					)AND
					rmdtvd.site_id=onboard.site_id AND
					onboard.HEDGE_TYPE_ID IN (@hedgeTypeId,@hedgeTypeId1) and
					rmdtvd.site_id =@siteId
				ORDER BY 
					rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
					rmdtvd.site_id,
					ent.DISPLAY_ORDER
				END
			end

				
				END--11TH
		END--7TH
END


else if @currencyUnit=0 and @consumptionUnit=0

	BEGIN
				SELECT 
				rmdtvd.HEDGE_VOLUME,
				STRIKE_PRICE,
				PREMIUM_PRICE,
				ent.ENTITY_NAME,
				DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
				DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
				rmdtvd.site_id,
				rmdtd.RM_DEAL_TICKET_DETAILS_ID
			
			FROM
				RM_DEAL_TICKET_DETAILS rmdtd,
				RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
				RM_DEAL_TICKET_OPTION_DETAILS rmdtod,
				ENTITY ent,
				RM_ONBOARD_HEDGE_SETUP onboard
			
			WHERE
				rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
				rmdtd.month_identifier between @startDate and @endDate and
				rmdtod.RM_DEAL_TICKET_DETAILS_ID=rmdtd.RM_DEAL_TICKET_DETAILS_ID AND
				rmdtvd.RM_DEAL_TICKET_DETAILS_ID=rmdtod.RM_DEAL_TICKET_DETAILS_ID AND
				rmdtod.OPTION_TYPE_ID=ent.ENTITY_id and
				rmdtvd.site_id=onboard.site_id and
				onboard.hedge_type_id=@hedgeTypeId
				
			
			ORDER BY 
				rmdtd.RM_DEAL_TICKET_DETAILS_ID, 
				rmdtvd.site_id,
				ent.DISPLAY_ORDER
	END
END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_VOLUME_DETAILS_FOR_OPTIONS_P] TO [CBMSApplication]
GO
