SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Client_Service_Category_Upd


DESCRIPTION: 

	Update data in the Client_Service_Category table.

INPUT PARAMETERS:    
	  Name					DataType          Default     Description    
------------------------------------------------------------------    
	  @Client_Service_Category_Id INT
	  @Service_Category_Name	NVARCHAR(200)
	  @Client_Id				INT
	  @Uom_Id					INT
	  @Service_Level_Cd			INT     
    
OUTPUT PARAMETERS:    
	  Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
begin tran
Exec Client_Service_Category_Upd  29,'el',10071,25,10012
select * from Client_Service_Category where client_service_category_id = 29
rollback tran



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	BCH			2011-10-19	Created

******/
CREATE PROCEDURE dbo.Client_Service_Category_Upd
      ( 
       @Client_Service_Category_Id INT
      ,@Service_Category_Name NVARCHAR(200)
      ,@Client_Id INT
      ,@Uom_Id INT
      ,@Service_Level_Cd INT )
AS 
BEGIN

      SET NOCOUNT ON
      UPDATE
            csc
      SET
           csc.Service_Category_Name = @Service_Category_Name
           ,csc.Client_Id = @Client_Id
           ,csc.Uom_Id = @Uom_Id
           ,csc.Service_Level_Cd = @Service_Level_Cd
      FROM
            dbo.Client_Service_Category csc
      WHERE
            csc.Client_Service_Category_Id = @Client_Service_Category_Id

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Upd] TO [CBMSApplication]
GO
