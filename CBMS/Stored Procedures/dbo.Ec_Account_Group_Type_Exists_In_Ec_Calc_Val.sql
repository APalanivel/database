SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Exists_In_Ec_Calc_Val        
                
Description:                
		This sproc to return the flag if Group_Accout_Type Exists in calc val .        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
   
   Exec dbo.Ec_Account_Group_Type_Exists_In_Ec_Calc_Val     2  
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	RKV				Ravi kumar Vegesna
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    RKV				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Exists_In_Ec_Calc_Val]
      ( 
       @Ec_Account_Group_Type_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      
      DECLARE @Is_Group_Type_Exists INT = 0
       
      SELECT
            @Is_Group_Type_Exists = 1
      FROM
            dbo.EC_Calc_Val ecv
      WHERE
            Ec_Account_Group_Type_Id = @Ec_Account_Group_Type_Id         
                
     
     
      SELECT
            @Is_Group_Type_Exists AS Is_Group_Type_Exists           
END
      


;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Exists_In_Ec_Calc_Val] TO [CBMSApplication]
GO
