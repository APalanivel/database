SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_FIXED_TRIGGER_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE            PROCEDURE dbo.GET_FIXED_TRIGGER_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P

@userId varchar,
@sessionId varchar,
@dealTicketId int

AS
set nocount on
SELECT 
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID, 
	rmdtd.MONTH_IDENTIFIER,
	rmdtd.TOTAL_VOLUME, 
	rmdtd.TRIGGER_PRICE,
	e1.ENTITY_NAME AS TRIGGER_STATUS, 
	rmdtd.HEDGE_PRICE, 
	e2.ENTITY_NAME AS UNIT_TYPE, 
	rmdtd.IS_TRIGGER_CONFIRMED

FROM 
	RM_DEAL_TICKET rmdt, 
	RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
	RM_DEAL_TICKET_DETAILS rmdtd LEFT JOIN ENTITY e1 ON (rmdtd.TRIGGER_STATUS_TYPE_ID = e1.ENTITY_ID), 
	Entity e2 
WHERE
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.RM_DEAL_TICKET_ID = rmdtd.RM_DEAL_TICKET_ID AND 
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID = rmdtd.RM_DEAL_TICKET_DETAILS_ID AND 
	rmdt.UNIT_TYPE_ID = e2.ENTITY_ID
GROUP BY
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID, 
	rmdtd.MONTH_IDENTIFIER, 
	rmdtd.TOTAL_VOLUME, 
	rmdtd.TRIGGER_PRICE, 
	e1.ENTITY_NAME, 
	rmdtd.HEDGE_PRICE, 
	e2.ENTITY_NAME, 
	rmdtd.IS_TRIGGER_CONFIRMED
GO
GRANT EXECUTE ON  [dbo].[GET_FIXED_TRIGGER_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P] TO [CBMSApplication]
GO
