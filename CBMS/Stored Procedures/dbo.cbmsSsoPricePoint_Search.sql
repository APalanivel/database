SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsSsoPricePoint_Search] 
	( @MyAccountId int
	, @report_year int
	, @price_point_id int = null
	, @market_index_id int = null
	)
AS
BEGIN

	set nocount on

	  declare @year varchar(4)
	set @year = cast(@report_year AS varchar(4))


	   select pr.index_id 		market_index_id
		, mi.entity_name 	market_index
		, pr.pricing_point 	price_point
		, pr.price_index_id     
		, 'Jan' = max(case when pv.index_month = '1/1/' + @year then pv.index_value else 0 end)
		, 'Feb' = max(case when pv.index_month = '2/1/' + @year then pv.index_value else 0 end)
		, 'Mar' = max(case when pv.index_month = '3/1/' + @year then pv.index_value else 0 end)
		, 'Apr' = max(case when pv.index_month = '4/1/' + @year then pv.index_value else 0 end)
		, 'May' = max(case when pv.index_month = '5/1/' + @year then pv.index_value else 0 end)
		, 'Jun' = max(case when pv.index_month = '6/1/' + @year then pv.index_value else 0 end)
		, 'Jul' = max(case when pv.index_month = '7/1/' + @year then pv.index_value else 0 end)
		, 'Aug' = max(case when pv.index_month = '8/1/' + @year then pv.index_value else 0 end)
		, 'Sep' = max(case when pv.index_month = '9/1/' + @year then pv.index_value else 0 end)
		, 'Oct' = max(case when pv.index_month = '10/1/' + @year then pv.index_value else 0 end)
		, 'Nov' = max(case when pv.index_month = '11/1/' + @year then pv.index_value else 0 end)
		, 'Dec' = max(case when pv.index_month = '12/1/' + @year then pv.index_value else 0 end)
             from price_index pr
	     join price_index_value pv on pv.price_index_id = pr.price_index_id
	     join entity mi on mi.entity_id = pr.index_id
	    where year(pv.index_month) = @report_year
   	      and mi.entity_id = isNull(@market_index_id, mi.entity_id)
   	      and pr.price_index_id = isNull(@price_point_id, pr.price_index_id)
	 group by pr.index_id 		
		, mi.entity_name 	
		, pr.pricing_point 	
		, pr.index_description 	
		, pr.price_index_id
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPricePoint_Search] TO [CBMSApplication]
GO
