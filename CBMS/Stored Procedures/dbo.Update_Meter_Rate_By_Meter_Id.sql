SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Update_Meter_Rate_By_Meter_Id

DESCRIPTION:
			To update meter rate

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Meter_Id      	INT 
	@Rate_ID      	INT          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT TOP 10 Meter_Id,Commodity_Id,Rate_Id,Rate_Name FROM core.Client_Hier_Account 
							WHERE Commodity_Id=290 GROUP BY Meter_Id,Commodity_Id,Rate_Id,Rate_Name
	BEGIN TRANSACTION
		SELECT Meter_Id,Rate_Id FROM dbo.METER WHERE METER_ID = 324610
		--EXEC dbo.Update_Meter_Rate_By_Meter_Id 422,7669
		--EXEC dbo.Update_Meter_Rate_By_Meter_Id 5819,2018
		EXEC dbo.Update_Meter_Rate_By_Meter_Id 324610,982
		SELECT Meter_Id,Rate_Id FROM dbo.METER WHERE METER_ID = 324610
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2013-10-17	MAINT-2127 Created

******/
CREATE PROCEDURE dbo.Update_Meter_Rate_By_Meter_Id
      ( 
       @Meter_Id INT
      ,@Rate_ID INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.METER
      SET   
            RATE_ID = @Rate_ID
      WHERE
            METER_ID = @Meter_Id 

END
;
GO
GRANT EXECUTE ON  [dbo].[Update_Meter_Rate_By_Meter_Id] TO [CBMSApplication]
GO
