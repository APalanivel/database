SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE [dbo].[cbmsSsoContracts_LoadImagesbyContract]

	( @MyAccountId int
	, @contract_id int = null
	)
AS
BEGIN



	   select cimg.cbms_image_id
		, cimg.description
		, cimg.document_type_id
		, cimg.contract_id
		, doc.entity_name document_type
		, img.cbms_doc_id
	     from contract_cbms_image_map cimg
	     join entity doc on doc.entity_id = cimg.document_type_id
	     join cbms_image img on cimg.cbms_image_id = img.cbms_image_id
	    where cimg.contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoContracts_LoadImagesbyContract] TO [CBMSApplication]
GO
