
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******                
NAME:   dbo.Site_Commodity_Image_Get_All_For_Data_Transfer          
      
          
DESCRIPTION:        
 Gets all records from Site_Commodity_Image table       
       
 used specifically for ETL prcoesses               
       
            
INPUT PARAMETERS:                
Name   DataType Default  Description                
------------------------------------------------------------                
      
                      
OUTPUT PARAMETERS:                
Name   DataType Default  Description                
------------------------------------------------------------       
      
               
USAGE EXAMPLES:                
------------------------------------------------------------       
EXEC Site_Commodity_Image_Sel_All_For_Data_Transfer
      
             
           
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
AKR     Ashok Kumar Raju       
DMR		Deana Ritter           
           
MODIFICATIONS                 
Initials Date Modification                
------------------------------------------------------------                
AKR   12/21/2011 Created 
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.     
*****/       
  
CREATE PROCEDURE [dbo].[Site_Commodity_Image_Sel_All_For_Data_Transfer]
AS 
BEGIN      
        
      SELECT
            ch.Client_Hier_Id
           ,NULL AS Account_id
           ,NULL AS CU_Invoice_Id
           ,sci.Commodity_id
           ,sci.Service_Month
           ,sci.CBMS_IMAGE_ID
      FROM
            Core.Client_Hier ch
            INNER JOIN Site_Commodity_Image sci
                  ON sci.SITE_id = ch.Site_id
      GROUP BY
            ch.Client_Hier_Id
           ,sci.Commodity_id
           ,sci.Service_Month
           ,sci.CBMS_IMAGE_ID      
END 


;
GO


GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Sel_All_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Sel_All_For_Data_Transfer] TO [ETL_Execute]
GO
