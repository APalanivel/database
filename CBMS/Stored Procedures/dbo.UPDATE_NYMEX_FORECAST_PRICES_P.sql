SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_NYMEX_FORECAST_PRICES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@forecastAsOfDate	datetime  	          	
	@monthIdentifier	datetime  	          	
	@aggressivePrice	decimal(32,16)	          	
	@conservativePrice	decimal(32,16)	          	
	@moderatePrice 	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2019-11-29	RM-Budgets Enahancement - Added input parameter @Rm_Forecast_Id

******/

CREATE PROCEDURE [dbo].[UPDATE_NYMEX_FORECAST_PRICES_P]
    (
        @userId VARCHAR(10)
        , @sessionId VARCHAR(20)
        , @forecastAsOfDate DATETIME
        , @monthIdentifier DATETIME
        , @aggressivePrice DECIMAL(32, 16)
        , @conservativePrice DECIMAL(32, 16)
        , @moderatePrice DECIMAL(32, 16)
        , @Rm_Forecast_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @forecastId INT
            , @recCount INT;

        SELECT
            @forecastId = rf.RM_FORECAST_ID
        FROM
            dbo.RM_FORECAST rf
        WHERE
            rf.FORECAST_AS_OF_DATE = @forecastAsOfDate
            AND (   @Rm_Forecast_Id IS NULL
                    OR  rf.RM_FORECAST_ID = @Rm_Forecast_Id);

        SELECT
            @recCount = COUNT(*)
        FROM
            dbo.RM_FORECAST_DETAILS rfd
        WHERE
            rfd.RM_FORECAST_ID = @forecastId
            AND rfd.MONTH_IDENTIFIER = @monthIdentifier;

        IF @recCount = 0
            INSERT INTO dbo.RM_FORECAST_DETAILS
                 (
                     RM_FORECAST_ID
                     , MONTH_IDENTIFIER
                     , AGGRESSIVE_PRICE
                     , CONSERVATIVE_PRICE
                     , MODERATE_PRICE
                 )
            VALUES
                (@forecastId
                 , @monthIdentifier
                 , @aggressivePrice
                 , @conservativePrice
                 , @moderatePrice);


        ELSE
            UPDATE
                dbo.RM_FORECAST_DETAILS
            SET
                AGGRESSIVE_PRICE = @aggressivePrice
                , CONSERVATIVE_PRICE = @conservativePrice
                , MODERATE_PRICE = @moderatePrice
            WHERE
                RM_FORECAST_ID = @forecastId
                AND MONTH_IDENTIFIER = @monthIdentifier;

    END;
GO
GRANT EXECUTE ON  [dbo].[UPDATE_NYMEX_FORECAST_PRICES_P] TO [CBMSApplication]
GO
