SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts]

DESCRIPTION:
	To Get Invoices Details for Selected utility Account  and the dependant supplier accounts

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION 
------------------------------------------------------------
@Account_Id		INT						Utility Account
@StartIndex		INT			1
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts  101527, 1,15
	EXEC CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts  116867

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
CPE			Chaitanya Panduga Eswara

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			2014-02-01	Created
*/

CREATE PROCEDURE dbo.CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts
	  (
	   @Account_Id INT
	  ,@StartIndex INT = 1
	  ,@EndIndex INT = 2147483647
	  )
AS
BEGIN

	  SET NOCOUNT ON;
	
	  WITH	Cte_Invoice_List
			  AS ( SELECT
						A.Account_Id
					   ,A.CU_INVOICE_ID
					   ,A.SERVICE_MONTH
					   ,A.Site_Id
					   ,A.Client_Hier_Id
					   ,A.Account_Number
					   ,A.Account_Type
					   ,ROW_NUMBER() OVER ( ORDER BY A.Order_Col, CASE WHEN A.Service_Month IS NULL THEN 1
																	   ELSE 0
																  END , A.Service_Month ) Row_Num
					   ,COUNT(1) OVER ( ) Total_Rows
				   FROM
						( SELECT
							  cha.Account_Id
							 ,cuim.CU_INVOICE_ID
							 ,cuim.SERVICE_MONTH
							 ,ch.Site_Id
							 ,ch.Client_Hier_Id
							 ,cha.Account_Number
							 ,1 AS Order_Col
							 ,'Account To Delete' AS Account_Type
						  FROM
							  dbo.CU_INVOICE_SERVICE_MONTH cuim
							  INNER JOIN core.Client_Hier_Account AS cha
									ON cha.Account_Id = cuim.Account_ID
							  INNER JOIN core.Client_Hier AS ch
									ON ch.Client_Hier_Id = cha.Client_Hier_Id
						  WHERE
							  cuim.ACCOUNT_ID = @Account_Id
						  GROUP BY
							  cha.Account_Id
							 ,cuim.CU_INVOICE_ID
							 ,cuim.SERVICE_MONTH
							 ,ch.Site_Id
							 ,ch.Client_Hier_Id
							 ,cha.Account_Number
						  UNION ALL
						  SELECT
							  cha.Account_Id
							 ,cuim.CU_INVOICE_ID
							 ,cuim.SERVICE_MONTH
							 ,ch.Site_Id
							 ,ch.Client_Hier_Id
							 ,cha.Account_Number
							 ,2 AS Order_Col
							 ,'Associated Supplier Accounts' AS Account_Type
						  FROM
							  dbo.CU_INVOICE_SERVICE_MONTH cuim
							  INNER JOIN core.Client_Hier_Account AS cha
									ON cha.Account_Id = cuim.Account_ID
							  INNER JOIN dbo.METER AS m
									ON m.METER_ID = cha.Meter_Id
							  INNER JOIN core.Client_Hier AS ch
									ON ch.Client_Hier_Id = cha.Client_Hier_Id
						  WHERE
							  m.ACCOUNT_ID = @Account_Id
							  AND cha.Account_Type = 'Supplier'
						  GROUP BY
							  cha.Account_Id
							 ,cuim.CU_INVOICE_ID
							 ,cuim.SERVICE_MONTH
							 ,ch.Site_Id
							 ,ch.Client_Hier_Id
							 ,cha.Account_Number ) A)
			SELECT
				  Account_Id
				 ,CU_INVOICE_ID
				 ,SERVICE_MONTH
				 ,Site_Id
				 ,Client_Hier_Id
				 ,Account_Number
				 ,Account_Type
				 ,Row_Num
				 ,Total_Rows
			FROM
				  Cte_Invoice_List
			WHERE
				  Row_Num BETWEEN @StartIndex AND @EndIndex

END
;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts] TO [CBMSApplication]
GO
