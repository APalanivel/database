SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[ADD_CONTRACT_METER_VOLUMES_P]
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	

		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2020-05-09	Added header
							GRMUER-81 - Added new optional input @Is_Edited
	
******/
CREATE PROCEDURE [dbo].[ADD_CONTRACT_METER_VOLUMES_P]
    (
        @meter_id INT
        , @contract_id INT
        , @volume DECIMAL(32, 16)
        , @month_identifier DATETIME
        , @unit_type_id INT
        , @frequency_type_id INT
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        INSERT INTO dbo.CONTRACT_METER_VOLUME
             (
                 METER_ID
                 , CONTRACT_ID
                 , VOLUME
                 , MONTH_IDENTIFIER
                 , UNIT_TYPE_ID
                 , FREQUENCY_TYPE_ID
                 , Is_Edited
             )
        VALUES
            (@meter_id
             , @contract_id
             , @volume
             , @month_identifier
             , @unit_type_id
             , @frequency_type_id
             , ISNULL(@Is_Edited, 0));

    END;






GO
GRANT EXECUTE ON  [dbo].[ADD_CONTRACT_METER_VOLUMES_P] TO [CBMSApplication]
GO
