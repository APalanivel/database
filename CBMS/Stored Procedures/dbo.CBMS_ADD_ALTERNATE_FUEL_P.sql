SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     PROCEDURE [dbo].[CBMS_ADD_ALTERNATE_FUEL_P] 

@siteID int,
@supplierID int,
@entityID int,
@storageCapacity varchar(100),
@conversion decimal,
@isReport bit,
@alternateID int out
AS

--declare @alternateID int


insert into ALTERNATE_FUEL (SITE_ID,VENDOR_ID,ALTERNATE_FUEL_TYPE_ID,STORAGE_CAPACITY,CONVERSION,IS_REPORTED) 
values 
(@siteID,@supplierID,@entityID,@storageCapacity,@conversion,@isReport) 

select @alternateID = scope_identity()
return
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_ALTERNATE_FUEL_P] TO [CBMSApplication]
GO
