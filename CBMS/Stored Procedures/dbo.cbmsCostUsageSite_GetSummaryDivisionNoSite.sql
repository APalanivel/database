
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Get_NG_Cost_Usage_Site_Summary]  
     
DESCRIPTION:

	To get usage and cost of all divisions of given client and year

INPUT PARAMETERS:          
NAME					   DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Currency_Unit_Id		   INT
@EL_Unit_of_Measure_Type_Id INT
@NG_Unit_of_Measure_Type_Id INT
@Begin_Date			   DATETIME
@End_Date				   DATETIME
@Client_Id			   INT
@Country_Id			   INT		NULL
@Not_Managed			   BIT		NULL

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXECUTE dbo.cbmsCostUsageSite_GetSummaryDivisionNoSite 
      @Currency_Unit_Id = 3
     ,@EL_Unit_of_Measure_Type_Id = 12
     ,@NG_Unit_of_Measure_Type_Id = 25
     ,@Begin_Date = '01/01/2011'
     ,@End_Date = '01/01/2011'
     ,@Client_Id = 235
     ,@Country_Id = NULL
     ,@Not_Managed = NULL


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
AP		Athmaram Pabbathi

MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------
AP	     04/03/2012    Addnl Data Changes
					   -- Removed @MyAccountId unused parameter
					   -- Removed base tables Account, Site, vwSite with CH & CHA
					   -- Replaced dbo.Cost_Usage_Site with Cost_Usage_Site_Dtl
					   -- Replaced dbo.Invoice_Participation_Site table and used dbo.Invoice_Participation
*/

CREATE  PROCEDURE [dbo].[cbmsCostUsageSite_GetSummaryDivisionNoSite]
      ( 
       @Currency_Unit_Id INT
      ,@EL_Unit_of_Measure_Type_Id INT
      ,@NG_Unit_of_Measure_Type_Id INT
      ,@Begin_Date DATETIME
      ,@End_Date DATETIME
      ,@Client_Id INT
      ,@Country_Id INT = NULL
      ,@Not_Managed BIT = NULL )
AS 
BEGIN

      SET NOCOUNT ON
      
      DECLARE
            @EL_Commodity_Id INT
           ,@NG_Commodity_Id INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )
            
      CREATE TABLE #Client_Hier
            ( 
             Client_Hier_Id INT
            ,Site_Id INT
            ,Sitegroup_Id INT
            ,Sitegroup_Name VARCHAR(200)
            ,Client_Currency_Group_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) )
            
      CREATE TABLE #Invoice_Particiption
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,EL_Is_Complete BIT
            ,EL_Is_Published BIT
            ,EL_Under_Review BIT
            ,NG_Is_Complete BIT
            ,NG_Is_Published BIT
            ,NG_Under_Review BIT
            ,Is_Published BIT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Service_Month ) )

      SELECT
            @EL_Commodity_Id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                   END)
           ,@NG_Commodity_Id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                   END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Natural Gas', 'Electric Power' )

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_Id 

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @NG_Commodity_Id 
                        
      INSERT      INTO #Client_Hier
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Client_Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,ch.Site_Id
                       ,ch.Sitegroup_Id
                       ,ch.Sitegroup_Name
                       ,ch.Client_Currency_Group_Id
                  FROM
                        Core.Client_Hier ch
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )
                        AND ( @Not_Managed IS NULL
                              OR ch.Site_Not_Managed = @Not_Managed )
                        AND ch.Site_Id > 0


      INSERT      INTO #Invoice_Particiption
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,NG_Is_Complete
                  ,NG_Is_Published
                  ,NG_Under_Review
                  ,EL_Is_Complete
                  ,EL_Is_Published
                  ,EL_Under_Review
                  ,Is_Published )
                  SELECT
                        cha.Client_Hier_Id
                       ,ip.Service_Month
                       ,( case WHEN ( sum(case WHEN CHA.Commodity_ID = @NG_Commodity_ID
                                                    AND IP.Is_Expected = 1 THEN 1
                                               ELSE 0
                                          END) = sum(case WHEN CHA.Commodity_ID = @NG_Commodity_ID
                                                               AND IP.Is_Received = 1 THEN 1
                                                          ELSE 0
                                                     END) ) THEN 1
                               ELSE 0
                          END ) AS NG_Is_Complete
                       ,( case WHEN sum(case WHEN CHA.Commodity_ID = @NG_Commodity_ID
                                                  AND IP.Is_Received = 1 THEN 1
                                             ELSE 0
                                        END) > 0 THEN 1
                               ELSE 0
                          END ) AS NG_Is_Published
                       ,isnull(max(case WHEN CHA.Commodity_ID = @NG_Commodity_ID
                                             AND IP.Recalc_Under_Review = 1 THEN 1
                                        WHEN CHA.Commodity_ID = @NG_Commodity_ID
                                             AND IP.Variance_Under_Review = 1 THEN 1
                                   END), 0) NG_Under_Review
                       ,( case WHEN ( sum(case WHEN CHA.Commodity_ID = @EL_Commodity_ID
                                                    AND IP.Is_Expected = 1 THEN 1
                                               ELSE 0
                                          END) = sum(case WHEN CHA.Commodity_ID = @EL_Commodity_ID
                                                               AND IP.Is_Received = 1 THEN 1
                                                          ELSE 0
                                                     END) ) THEN 1
                               ELSE 0
                          END ) AS EL_Is_Complete
                       ,( case WHEN sum(case WHEN CHA.Commodity_ID = @EL_Commodity_ID
                                                  AND IP.Is_Received = 1 THEN 1
                                             ELSE 0
                                        END) > 0 THEN 1
                               ELSE 0
                          END ) AS EL_Is_Published
                       ,isnull(max(case WHEN CHA.Commodity_ID = @EL_Commodity_ID
                                             AND IP.Recalc_Under_Review = 1 THEN 1
                                        WHEN CHA.Commodity_ID = @EL_Commodity_ID
                                             AND IP.Variance_Under_Review = 1 THEN 1
                                   END), 0) EL_Under_Review
                       ,( case WHEN sum(cast(IP.Is_Received AS INT)) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                  FROM
                        ( SELECT
                              ch.Client_Hier_Id
                             ,ch.Site_Id
                             ,cha.Account_Id
                             ,cha.Commodity_Id
                          FROM
                              #Client_Hier ch
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                          WHERE
                              cha.Commodity_Id IN ( @EL_Commodity_Id, @NG_Commodity_Id )
                          GROUP BY
                              ch.Client_Hier_Id
                             ,ch.Site_Id
                             ,cha.Account_Id
                             ,cha.Commodity_Id ) cha
                        INNER JOIN dbo.Invoice_Participation ip
                              ON ip.Account_Id = cha.Account_Id
                                 AND ip.Site_Id = cha.Site_Id
                  WHERE
                        ip.Service_Month BETWEEN @Begin_Date
                                         AND     @End_Date
                  GROUP BY
                        cha.Client_Hier_Id
                       ,ip.Service_Month

      SELECT
            ch.Sitegroup_Id AS Division_Id
           ,ch.Sitegroup_Name AS Division_Name
           ,convert(BIT, min(convert(INT, ip.NG_Is_Complete))) AS NG_Is_Complete
           ,convert(BIT, max(convert(INT, ip.NG_Is_Published))) AS NG_Is_Published
           ,convert(BIT, max(convert(INT, ip.NG_Under_Review))) AS NG_Under_Review
           ,convert(BIT, min(convert(INT, ip.EL_Is_Complete))) AS EL_Is_Complete
           ,convert(BIT, max(convert(INT, ip.EL_Is_Published))) AS EL_Is_Published
           ,convert(BIT, max(convert(INT, ip.EL_Under_Review))) AS EL_Under_Review
           ,isnull(sum(cusd.EL_Cost), 0) AS EL_Cost
           ,isnull(sum(cusd.EL_Usage), 0) AS EL_Usage
           ,isnull(sum(cusd.NG_Cost), 0) AS NG_Cost
           ,isnull(sum(cusd.NG_Usage), 0) AS NG_Usage
           ,convert(DECIMAL(32, 16), 0) AS EL_Unit_Cost
           ,convert(DECIMAL(32, 16), 0) AS NG_Unit_Cost
      FROM
            #Client_Hier ch
            LEFT OUTER JOIN ( SELECT
                                    ch.Client_Hier_Id
                                   ,cusd.Service_Month
                                   ,( case WHEN bm.Commodity_Id = @EL_Commodity_Id
                                                AND bkt.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * el_uc.Conversion_Factor
                                      END ) EL_Usage
                                   ,( case WHEN bm.Commodity_Id = @EL_Commodity_Id
                                                AND bkt.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cur_conv.Conversion_Factor
                                      END ) EL_Cost
                                   ,( case WHEN bm.Commodity_Id = @NG_Commodity_Id
                                                AND bkt.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * ng_uc.Conversion_Factor
                                      END ) NG_Usage
                                   ,( case WHEN bm.Commodity_Id = @NG_Commodity_Id
                                                AND bkt.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cur_conv.Conversion_Factor
                                      END ) NG_Cost
                              FROM
                                    #Client_Hier ch
                                    INNER JOIN dbo.Cost_Usage_Site_Dtl cusd
                                          ON cusd.Client_Hier_Id = ch.Client_Hier_Id
                                    INNER JOIN dbo.Bucket_Master bm
                                          ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
                                    INNER JOIN @Cost_Usage_Bucket_Id bkt
                                          ON bkt.Bucket_Master_Id = bm.Bucket_Master_Id
                                    LEFT OUTER JOIN dbo.Currency_Unit_Conversion cur_conv
                                          ON cur_conv.Base_Unit_Id = cusd.Currency_Unit_Id
                                             AND cur_conv.Converted_Unit_Id = @Currency_Unit_Id
                                             AND cur_conv.Currency_Group_Id = ch.Client_Currency_Group_Id
                                             AND cur_conv.Conversion_Date = cusd.Service_Month
                                    LEFT OUTER JOIN dbo.Consumption_Unit_Conversion el_uc
                                          ON el_uc.Base_Unit_Id = cusd.UOM_Type_Id
                                             AND el_uc.Converted_Unit_Id = @EL_Unit_of_Measure_Type_Id
                                             AND bm.Commodity_Id = @EL_Commodity_Id
                                    LEFT OUTER JOIN dbo.Consumption_Unit_Conversion ng_uc
                                          ON ng_uc.Base_Unit_Id = cusd.UOM_Type_Id
                                             AND ng_uc.Converted_Unit_Id = @NG_Unit_of_Measure_Type_Id
                                             AND bm.Commodity_Id = @NG_Commodity_Id
                              WHERE
                                    cusd.Service_Month BETWEEN @Begin_Date
                                                       AND     @End_Date ) cusd
                  ON cusd.Client_Hier_Id = ch.Client_Hier_Id
            LEFT OUTER JOIN #Invoice_Particiption ip
                  ON ip.Client_Hier_Id = ch.Client_Hier_Id
                     AND ip.Service_Month = cusd.Service_Month
      WHERE
            ( ip.Is_Published = 1
              OR ip.Is_Published IS NULL )
      GROUP BY
            ch.Sitegroup_Id
           ,ch.Sitegroup_Name
      ORDER BY
            Division_Name


END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetSummaryDivisionNoSite] TO [CBMSApplication]
GO
