SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_ARCHIVE_ACCOUNT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@RFP_ID int,
@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test the archive
--exec [SR_RFP_CREATE_ARCHIVE_ACCOUNT_P]
--@RFP_ID = 5 ,
--@sr_rfp_account_id = 164

---- Test the archived data at SV
--select * from sv..SR_RFP_ARCHIVE_ACCOUNT where sr_rfp_account_id = 164


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [DBO].[SR_RFP_CREATE_ARCHIVE_ACCOUNT_P]

@RFP_ID int,
@sr_rfp_account_id int

AS
	
SET NOCOUNT ON

	declare @cnt int


declare @sr_rfp_archive_account_id int, @rfp_account_id int , @account_number varchar(50), @site_name varchar(200), 
	@client_name varchar(200), @utility_name varchar(200), @service_level_name varchar(100), @lp_size decimal (32,16),
	@contracting_entity varchar(200), --@contracting_entity_details varchar(4000),
	@tariff_transport varchar(50), @duns_number varchar(50), @tax_number varchar(50)--, @eligible_date datetime


DECLARE C_ARC_ACC CURSOR FAST_FORWARD	
FOR 

SELECT 
	rfp_account.SR_RFP_ACCOUNT_ID,
	a.ACCOUNT_NUMBER,
	s.SITE_NAME,
	c.CLIENT_NAME,
	v.VENDOR_NAME, 					-- UTILITY_NAME
	e.ENTITY_NAME,					-- SERVICE_LEVEL_NAME
	dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE(rfp_account.SR_RFP_ACCOUNT_ID,0) AS
LP_SIZE,
	s.DOING_BUSINESS_AS,  				-- CONTRACTING_ENTITY
	rateType.ENTITY_NAME,				-- TARIFF_TRANSPORT
	s.DUNS_NUMBER,
	s.TAX_NUMBER 

from
	sr_rfp_account rfp_account,
	sr_rfp_closure closure,
	client c,
	division d,
	site s,
	account a,
	vendor v ,
	entity e,
	entity rateType,
	meter m

where
	rfp_account.sr_rfp_id = @RFP_ID
	and rfp_account.sr_rfp_account_id = @sr_rfp_account_id
	and rfp_account.is_deleted = 0
	and closure.sr_account_group_id = rfp_account.sr_rfp_account_id
	and rfp_account.SR_RFP_BID_GROUP_ID is null
	and a.account_id = rfp_account.account_id
	and v.vendor_id = a.vendor_id
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id
	and a.service_level_type_id = e.entity_id
	and m.purchase_method_type_id = rateType.entity_id
	and m.account_id = rfp_account.account_id
	

union

select
	rfp_account.SR_RFP_ACCOUNT_ID,
	a.ACCOUNT_NUMBER,
	s.SITE_NAME,
	c.CLIENT_NAME,
	v.VENDOR_NAME,
	e.ENTITY_NAME,
	dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE(rfp_account.SR_RFP_BID_GROUP_ID,1) AS LP_SIZE,
	s.DOING_BUSINESS_AS,
	rateType.ENTITY_NAME,
	s.DUNS_NUMBER,
	s.TAX_NUMBER --,
from
	sr_rfp_account rfp_account,
	sr_rfp_closure closure,
	client c,
	division d,
	site s,
	account a,
	vendor v ,
	entity e,
	entity rateType,
	meter m,
	SR_RFP_BID_GROUP

where
	rfp_account.sr_rfp_id = @RFP_ID
	and rfp_account.sr_rfp_account_id = @sr_rfp_account_id
	and rfp_account.is_deleted = 0
	and closure.sr_account_group_id = rfp_account.sr_rfp_account_id
	and rfp_account.SR_RFP_BID_GROUP_ID is not null
	and rfp_account.SR_RFP_BID_GROUP_ID = SR_RFP_BID_GROUP.SR_RFP_BID_GROUP_ID
	and a.account_id = rfp_account.account_id
	and v.vendor_id = a.vendor_id
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id
	and a.service_level_type_id = e.entity_id
	and m.purchase_method_type_id = rateType.entity_id
        and m.account_id = rfp_account.account_id


OPEN C_ARC_ACC

FETCH NEXT FROM C_ARC_ACC INTO @rfp_account_id, @account_number, @site_name,
				@client_name, @utility_name, @service_level_name, @lp_size,
				@contracting_entity, --@contracting_entity_details,
				@tariff_transport, @duns_number, @tax_number--, @eligible_date	
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
		select @cnt = count(1) from SR_RFP_ARCHIVE_ACCOUNT where SR_RFP_ACCOUNT_ID = @sr_rfp_account_id
		
		if (@cnt > 0)
		begin
		
		
			update SR_RFP_ARCHIVE_ACCOUNT 
			set	ACCOUNT_NUMBER = @account_number, 
				SITE_NAME = @site_name,
				CLIENT_NAME = @client_name , 
				UTILITY_NAME = @utility_name,
				SERVICE_LEVEL_NAME = @service_level_name,
				LP_SIZE = @lp_size,
				CONTRACTING_ENTITY = @contracting_entity, 
				TARIFF_TRANSPORT = @tariff_transport, 
				DUNS_NUMBER = @duns_number, 
				TAX_NUMBER = @tax_number
						 	    	
			where SR_RFP_ACCOUNT_ID = @rfp_account_id
			
		end
		else
		begin
				INSERT INTO	SR_RFP_ARCHIVE_ACCOUNT ( 
														SR_RFP_ACCOUNT_ID, 
														ACCOUNT_NUMBER, 
														SITE_NAME,
					    								CLIENT_NAME, 
					    								UTILITY_NAME ,
					    								SERVICE_LEVEL_NAME,
					    								LP_SIZE,
														CONTRACTING_ENTITY, --CONTRACTING_ENTITY_DETAILS,
					    								TARIFF_TRANSPORT, 
					    								DUNS_NUMBER, 
					    								TAX_NUMBER--, ELIGIBLE_DATE
						 							)		
											VALUES ( 	
														@rfp_account_id, 
														@account_number, 
														@site_name,
														@client_name, 
														@utility_name, 
														@service_level_name, 
														@lp_size,
														@contracting_entity, --@contracting_entity_details,
														@tariff_transport, 
														@duns_number, 
														@tax_number--, @eligible_date			
													)
		
		end
      SELECT @sr_rfp_archive_account_id = SCOPE_IDENTITY()
			
	
	END -- IF
		FETCH NEXT FROM C_ARC_ACC INTO @rfp_account_id, @account_number, @site_name,
						@client_name, @utility_name, @service_level_name, @lp_size,
						@contracting_entity, --@contracting_entity_details,
						@tariff_transport, @duns_number, @tax_number--, @eligible_date	
END -- WHILE
CLOSE C_ARC_ACC
DEALLOCATE C_ARC_ACC
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_ARCHIVE_ACCOUNT_P] TO [CBMSApplication]
GO
