SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[Invoice_Recalc_Get_All_Processing]   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
---------------------------------------------------------------------------- 



EXEc dbo.Invoice_Recalc_Get_All_Processing
     @User_Info_Id = 49



AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2019-09-03		Add Contract -  Created.
******/

CREATE PROCEDURE [dbo].[Invoice_Recalc_Get_All_Processing]
    (
        @Invoice_Recalc_Process_Batch_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            irp.Invoice_Recalc_Process_Batch_Dtl_Id
            , irp.Invoice_Recalc_Process_Batch_Id
            , irp.Cu_Invoice_Id
        FROM
            dbo.Invoice_Recalc_Process_Batch_Dtl irp
            INNER JOIN dbo.Invoice_Recalc_Process_Batch irpb
                ON irpb.Invoice_Recalc_Process_Batch_Id = irp.Invoice_Recalc_Process_Batch_Id
        WHERE
            irp.Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id;


    END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Recalc_Get_All_Processing] TO [CBMSApplication]
GO
