SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Account_Config_Bulk_Upload_Sel            
                        
 DESCRIPTION:                        
			To get the details of Invoice_Collection_Account_Config             
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
    
--Example 1 Grid




 DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns        
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
             ( 'Client_Id', '14525' ),('Exclude_Account_Groups','0')
			 
            

declare @p2 dbo.tvp_Invoice_Collection_Source
insert into @p2 values(N'Data Feed ',N'Invoice_Source_Type_Cd',102800)
      
 EXEC [dbo].Invoice_Collection_Account_Config_Bulk_Upload_Sel
      @Tvp_Filter_Columns = @Tvp_Filter_Columns
	  ,@Tvp_Invoice_Collection_Sources=@p2
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093  

--Example 2


 DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns        
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
            ( 'Client_Id', '14525' )
--,           ( 'SiteGroup_Id', '12275211' )
--,           ( 'Site_Id', '549361' )
--,           ( 'Country_Id', '31' )
--,           ( 'State_Id', '128' )
--,           ( 'Account_Id', '1342210' )
--,           ( 'Invoice_Collection_Office_Id', '71904' )

                                                       
           
 EXEC [dbo].Invoice_Collection_Account_Config_Bulk_Upload_Sel 
      @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Sort_Order = NULL
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093  
	 ,@Is_Excel_Download=1
	 ,@Total_Row_Count=1

--Example 3



 DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns        
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
            ( 'Client_Id', '14525' )
--,           ( 'SiteGroup_Id', '12275211' )
--,           ( 'Site_Id', '549361' )
--,           ( 'Country_Id', '31' )
--,           ( 'State_Id', '128' )
--,           ( 'Account_Id', '1342210' )
--,           ( 'Invoice_Collection_Office_Id', '71904' )


           
 EXEC [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel] 
      @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Sort_Order = NULL
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093 
     ,@StartIndex = 1
     ,@EndIndex = 3 
     
 DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns        
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
            ( 'Account_Ids', '1342211,1342213,1342228' )

 EXEC [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel] 
	  @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093
     ,@Is_Excel_Download=1 
     
       
 DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns        
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
 ( 'Invoice_Collection_Account_Config_Ids', '174,175,176' )
            

 EXEC [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel] 
	  @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093
     ,@Is_Excel_Download=1 
     
  DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns
  INSERT    INTO @Tvp_Filter_Columns
            ( 
             Column_Name
            ,Column_Value )
  VALUES
            ( 
             'Invoice_Collection_Account_Config_Ids'
            ,'592,591' )  
  EXEC [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel] 
      @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 15558
     ,@Is_Excel_Download = 1

	 
Example5
DECLARE @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns     

declare @Tvp_Invoice_Collection_Sources dbo.tvp_Invoice_Collection_Source
          
 INSERT     INTO @Tvp_Filter_Columns
            ( Column_Name, Column_Value )
 VALUES
             ( 'Client_Id', '14525' ),('Exclude_Account_Groups','0'),('Account_Number','541449206007107046'),('Commodity_Id','291'),
			 ('Group_Id','1'),(N'IC_Start_Date',N'2018-12-31')
--INSERT     INTO @Tvp_Invoice_Collection_Sources    
--            ( Column_Type, Column_Name, Column_Value )    
-- VALUES    
--            ( 'UBM', 'Invoice_Source_Type_Cd', '102292' )                
--    ,    ( 'Vendor', 'Invoice_Source_Type_Cd', '102293' )      
--    ,    ( 'Vendor', 'Invoice_Source_Method_of_Contact_Cd', '102302' )        
--    ,    ( 'Vendor', 'Invoice_Source_Type_Cd', '102297' )          
--    ,    ( 'Client', 'Invoice_Source_Type_Cd', '102291' )          
--    ,    ( 'Client', 'Invoice_Source_Method_of_Contact_Cd', '102299' )    
            
      
 EXEC [dbo].Invoice_Collection_Account_Config_Bulk_Upload_Sel
      @Tvp_Filter_Columns = @Tvp_Filter_Columns
     ,@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id = 9093  
   
       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam   
 NR					   Narayana Reddy 
 RKV                   Ravi Kumar vegesna      
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-30       Created for Invoice Tracking               
 NR					   2017-06-20       MAINT-5313 Modified to hadle when we have consolidated billing end date is open.
 RKV                   2017-11-27       Added new column Owner to the result set.   
 RKV                   2018-11-21       MAINT- 7991 modified to match the hard coded  primary invoice collection source parameter value with the value which is sending by app                     
 RKV                   2019-07-03       IC-Project Added new ETL Columns
 SLP				   2019-10-16		IC Project , Included logic for @Tvp_Filter_Columns, and added
										@Tvp_Invoice_Collection_Sources
 SLP				   2019-11-18		Included condition is_primary =1 for the Account_Invoice_collection_source
										& Invoice_Source Data Feed -ETL, Data Feed -Partner as part of MAINT-9529
 RKV                   2020-02-17       D20-356, Added two new columns Custom_days,Custom_Invoice_Received_day to the result when is_Excel_Download = 1
 NR					   2020-06-12		SE2017- Bot process - Added Enroll_In_Bot column from new table.
 NR					   2020-07-02		SE2017-1011 - added in filter column IC_Bots_Process_Id input and Added IC_Bots_Process_Name as output.


******/



CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel]
    (
        @Tvp_Filter_Columns tvp_Invoice_Collection_Bulk_Upload_Columns READONLY
        , @Sort_Order VARCHAR(MAX) = ''
        , @Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
        , @Total_Row_Count INT = 0
        , @Is_Excel_Download BIT = 0
        , @Vendor_Date DATE = NULL
        , @Display_All_Timebands BIT = 1
        , @Tvp_Invoice_Collection_Sources tvp_Invoice_Collection_Source READONLY
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Filter_Fields NVARCHAR(MAX) = N'';
        DECLARE @Cnt_Of_Invoice_Collection_Source BIT = 0;

        SET @Vendor_Date = ISNULL(@Vendor_Date, GETDATE());

        DECLARE
            @IC_Start_Date AS DATE = NULL
            , @IC_End_Date AS DATE = NULL;
        SELECT
            @IC_Start_Date = CAST(CONVERT(DATE, Column_Value, 105) AS VARCHAR(10))
        FROM
            @Tvp_Filter_Columns
        WHERE
            Column_Name = 'IC_Start_Date';
        SELECT
            @IC_End_Date = CAST(CONVERT(DATE, Column_Value, 105) AS VARCHAR(10))
        FROM
            @Tvp_Filter_Columns
        WHERE
            Column_Name = 'IC_End_Date';


        CREATE TABLE #Contact_Source_Type
             (
                 Contact_Level_Vendor_Cd INT
             );

        SELECT
            *
        INTO
            #Tvp_Invoice_Collection_Sources
        FROM
            @Tvp_Invoice_Collection_Sources;

        INSERT INTO #Contact_Source_Type
             (
                 Contact_Level_Vendor_Cd
             )
        SELECT
            c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel'
            AND c.Code_Value = 'Vendor';

        SELECT
            @Sort_Order = ' 
            Client_Name 
           ,Site_name
           ,Account_Number
           ,Invoice_Collection_Service_End_Dt DESC '
        WHERE
            @Sort_Order = '';


        SELECT
            @Filter_Fields = @Filter_Fields
                             + CASE WHEN Column_Name = 'Client_Id' THEN 'ch.Client_Id in ('
                                   WHEN Column_Name = 'Exclude_Account_Groups' THEN '(1='
                                   WHEN Column_Name = 'SiteGroup_Id' THEN 'ch.SiteGroup_Id='
                                   WHEN Column_Name = 'Site_Id' THEN 'ch.Client_Hier_Id='
                                   WHEN Column_Name = 'Country_Id' THEN 'ch.Country_Id='
                                   WHEN Column_Name = 'State_Id' THEN 'ch.State_Id='
                                   WHEN Column_Name = 'Vendor_Type' THEN
                                       'ISNULL(vd.Account_Vendor_Type,cha.Account_Type)='
                                   WHEN Column_Name = 'Commodity_Id' THEN 'cha.Commodity_Id='
                                   WHEN Column_Name = 'Vendor_Id' THEN
                                       'ISNULL(vd.Account_Vendor_Id,cha.Account_Vendor_Id) in ('
                                   WHEN Column_Name = 'Account_Id' THEN 'cha.Account_Id='
                                   WHEN Column_Name = 'Account_Ids' THEN 'cha.Account_Id IN ('
                                   WHEN Column_Name = 'Invoice_Collection_Account_Config_Ids' THEN
                                       'ica.Invoice_Collection_Account_Config_Id IN ('
                                   WHEN Column_Name = 'Invoice_Collection_Office_Id' THEN
                                       'ica.Invoice_Collection_Officer_User_Id='
                                   WHEN Column_Name = 'Invoice_Collection_Owner_Id' THEN
                                       'ica.Invoice_Collection_Owner_User_Id='
                                   WHEN Column_Name = 'Invoice_Collection_Setup' THEN 'ISNULL(chaa.Attribute_Value,0)='
                                   WHEN Column_Name = 'Is_Active' THEN 'ica.Is_Chase_Activated='
                                   WHEN Column_Name = 'Vendor_Name' THEN
                                       'ISNULL(vd.Account_Vendor_Name,cha.Account_Vendor_Name) LIKE '
                                   WHEN Column_Name = 'Chase_Priority' THEN 'ica.Chase_Priority_Cd='
                                   WHEN Column_Name = 'Invoice_Pattern' THEN 'ica.Invoice_Pattern_Cd='
                                   WHEN Column_Name = 'Primary_Invoice_Frequency' THEN
                                       'icl.Seq_No=0 AND ivf.Code_Value='
                                   WHEN Column_Name = 'Primary_Invoice_Collection_Source' THEN
                                       'ics.Is_Primary = 1 AND 1='
                                   WHEN Column_Name = 'ETL_GenericKey' THEN
                                       ' icsource.Code_Value=''Data Feed'' AND dfe.Generic_Key LIKE '
                                   WHEN Column_Name = 'ETL_URL' THEN
                                       ' icsource.Code_Value=''Data Feed'' AND (dfe.URL LIKE '
                                   WHEN Column_Name = 'ETL_Login' THEN
                                       ' icsource.Code_Value=''Data Feed'' AND (dfe.Login_Name LIKE '
                                   WHEN Column_Name = 'Partner' THEN 'ub.UBM_Id='
                                   WHEN Column_Name = 'Partner_URL' THEN
                                       ' icsource.Code_Value=''Data Feed'' AND dt.URL LIKE '
                                   WHEN Column_Name = 'Partner_Login' THEN
                                       ' icsource.Code_Value=''Data Feed'' AND dt.Login_Name LIKE '
                                   WHEN Column_Name = 'Vendor_Online_URL' THEN
                                       ' icsource.Code_Value=''Vendor'' AND dt.URL LIKE '
                                   WHEN Column_Name = 'Vendor_Online_User_Name' THEN
                                       ' icsource.Code_Value=''Vendor'' AND dt.Login_Name LIKE '
                                   WHEN Column_Name = 'Client_Online_URL' THEN
                                       ' icsource.Code_Value=''Client'' AND dt.URL LIKE '
                                   WHEN Column_Name = 'Client_Online_User_Name' THEN
                                       ' icsource.Code_Value=''Client'' AND dt.Login_Name LIKE '
                                   WHEN Column_Name = 'Vendor_primary_Contact_Name' THEN
                                       ' cl.Code_Value=''Vendor'' AND (cif.Last_Name LIKE '
                                   WHEN Column_Name = 'Client_primary_Contact_Name' THEN
                                       ' cl.Code_Value=''Client'' AND (cif.Last_Name LIKE '
                                   WHEN Column_Name = 'Account_primary_Contact_Name' THEN
                                       ' cl.Code_Value=''Account'' AND (cif.Last_Name LIKE '
                                   WHEN Column_Name = 'Account_Number' THEN 'cha.Display_Account_Number='
                                   WHEN Column_Name = 'Group_Id' THEN
                                       'icacg.Invoice_Collection_Account_Config_Group_Id='
                                   WHEN Column_Name = 'IC_Start_Date' THEN
                                       '''' + CAST(@IC_Start_Date AS VARCHAR(20)) + ''''
                                       + ' between ica.Invoice_Collection_Service_Start_Dt 
									  and ISNULL(ica.Invoice_Collection_Service_End_Dt, ''9999-12-31'') '
                                   WHEN Column_Name = 'IC_End_Date' THEN
                                       '''' + CAST(@IC_End_Date AS VARCHAR(20)) + ''''
                                       + ' between ica.Invoice_Collection_Service_Start_Dt 
									  and ISNULL(ica.Invoice_Collection_Service_End_Dt, ''9999-12-31'') '
                                   WHEN Column_Name = 'IC_Bots_Process_Id' THEN 'ibpicacm.IC_Bots_Process_Id='
                               END
                             + (CASE WHEN Column_Value = 'Client - Online' THEN
                                         '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Online'' '
                                    WHEN Column_Value = 'Client - Client Primary Contact - Email' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Client Primary Contact'' AND icsmc.Code_Value=''Email'' '
                                    WHEN Column_Value = 'Client - Client Primary Contact - Telephone' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Client Primary Contact'' AND icsmc.Code_Value=''Telephone'' '
                                    WHEN Column_Value = 'Client - Client Primary Contact - Others' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Client Primary Contact'' AND icsmc.Code_Value=''Others'' '
                                    WHEN Column_Value = 'Client - Account Primary Contact - Email' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Account Primary Contact'' AND icsmc.Code_Value=''Email'' '
                                    WHEN Column_Value = 'Client - Account Primary Contact - Telephone' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Account Primary Contact'' AND icsmc.Code_Value=''Telephone'' '
                                    WHEN Column_Value = 'Client - Account Primary Contact - Others' THEN
                                        '1 AND icsource.Code_Value=''Client'' AND icst.Code_Value=''Account Primary Contact'' AND icsmc.Code_Value=''Others'' '
                                    WHEN Column_Value = 'Vendor - Online' THEN
                                        '1 AND icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Online'' '
                                    WHEN Column_Value = 'Vendor - Mail Redirect' THEN
                                        '1 AND icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Mail Redirect'' '
                                    WHEN Column_Value = 'Vendor - Vendor Primary Contact - Email' THEN
                                        '1 AND icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Vendor Primary Contact'' AND icsmc.Code_Value=''Email'' '
                                    WHEN Column_Value = 'Vendor - Vendor Primary Contact - Telephone' THEN
                                        '1 AND icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Vendor Primary Contact'' AND icsmc.Code_Value=''Telephone'' '
                                    WHEN Column_Value = 'Vendor - Vendor Primary Contact - Others' THEN
                                        '1 AND icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Vendor Primary Contact'' AND icsmc.Code_Value=''Others'' '
                                    WHEN Column_Value = 'Partner' THEN
                                        '1 AND icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Online'''
                                    WHEN Column_Name = 'Exclude_Account_Groups'
                                         AND   Column_Value = '1' THEN '1 AND cha.Is_Invoice_Posting_Blocked = 0) '
                                    WHEN Column_Name = 'Exclude_Account_Groups'
                                         AND   Column_Value = '0' THEN '1 )'
                                    WHEN Column_Name = 'Vendor_Name' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'ETL_GenericKey' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'ETL_URL' THEN
                                        '''' + '%' + Column_Value + '%' + '''' + ' OR dfei.URL LIKE ' + '''' + '%'
                                        + Column_Value + '%' + '''' + ')'
                                    WHEN Column_Name = 'ETL_Login' THEN
                                        '''' + '%' + Column_Value + '%' + '''' + ' OR dfei.Login_Name LIKE ' + ''''
                                        + '%' + Column_Value + '%' + '''' + ')'
                                    WHEN Column_Name = 'Partner_URL' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Partner_Login' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Vendor_Online_URL' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Vendor_Online_User_Name' THEN
                                        '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Client_Online_URL' THEN '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Client_Online_User_Name' THEN
                                        '''' + '%' + Column_Value + '%' + ''''
                                    WHEN Column_Name = 'Vendor_primary_Contact_Name' THEN
                                        '''' + '%' + Column_Value + '%' + '''' + ' OR cif.First_Name LIKE ' + ''''
                                        + '%' + Column_Value + '%' + '''' + ')'
                                    WHEN Column_Name = 'Client_primary_Contact_Name' THEN
                                        '''' + '%' + Column_Value + '%' + '''' + ' OR cif.First_Name LIKE ' + ''''
                                        + '%' + Column_Value + '%' + '''' + ')'
                                    WHEN Column_Name = 'Account_primary_Contact_Name' THEN
                                        '''' + '%' + Column_Value + '%' + '''' + ' OR cif.First_Name LIKE ' + ''''
                                        + '%' + Column_Value + '%' + '''' + ')'
                                    WHEN Column_Name = 'Account_Ids' THEN Column_Value + ')'
                                    WHEN Column_Name = 'Invoice_Collection_Account_Config_Ids' THEN Column_Value + ')'
                                    WHEN Column_Name = 'IC_Start_Date' THEN ''
                                    WHEN Column_Name = 'IC_End_Date' THEN ''
                                    WHEN Column_Name = 'Client_Id' THEN Column_Value + ')'
                                    WHEN Column_Name = 'Vendor_Id' THEN Column_Value + ')'
                                    WHEN Column_Name = 'IC_Bots_Process_Id' THEN Column_Value
                                    ELSE '''' + Column_Value + ''''
                                END) + N' AND '
        FROM
            @Tvp_Filter_Columns;


        SELECT  @Filter_Fields = LEFT(@Filter_Fields, LEN(@Filter_Fields) - 4);
        SELECT
            @Cnt_Of_Invoice_Collection_Source = 1
        FROM
            #Tvp_Invoice_Collection_Sources tvp;
        DECLARE
            @Dynamic_Query1 NVARCHAR(MAX) = N''
            , @Dynamic_Query2 NVARCHAR(MAX) = N''
            , @Dynamic_Query3 NVARCHAR(MAX) = N''
            , @Dynamic_Query4 NVARCHAR(MAX) = N''
            , @Dynamic_Query5 NVARCHAR(MAX) = N''
            , @Dynamic_Query6 NVARCHAR(MAX) = N''
            , @Dynamic_Query7 NVARCHAR(MAX) = N''
            , @Dynamic_Query8 NVARCHAR(MAX) = N'';

        SELECT
            @Dynamic_Query1 = N'  DECLARE @Total_Row_Count INT = 0,   @Cnt_Of_Invoice_Collection_Source bit=0; 
				  
			;			WITH  CTE_Bulk_Get
              AS ( SELECT
					     ch.Client_Name 
						,CASE WHEN LEN(sites.Site_name) > 0 THEN LEFT(sites.Site_name, LEN(sites.Site_name) - 1)
                             ELSE sites.Site_name
                        END AS Site_name
						,cha.Display_Account_Number AS Account_Number
						,cha.Account_Id
						,ica.Invoice_Collection_Account_Config_Id
						,ica.Invoice_Collection_Alternative_Account_Number
						,c.Commodity_Name as  Commodity_Name
						,ica.Is_Chase_Activated
						,ch.Country_Name
						,ch.State_Name
						,ch.Sitegroup_Name
						,ISNULL(vd.Account_Vendor_Type,cha.Account_Type) AS Vendor_Type
						,ISNULL(vd.Account_Vendor_Name,cha.Account_Vendor_Name) Account_Vendor_Name
						,ISNULL(vd.Account_Vendor_Id,cha.Account_Vendor_Id) Account_Vendor_Id
						,ch.Client_Id
						,cha.Supplier_Contract_ID
						,ica.Additional_Invoices_Per_Period
						,ica.Invoice_Collection_Service_Start_Dt
						,ica.Invoice_Collection_Service_End_Dt
						,ipc.Code_value Invoice_Pattern
						,cp.Code_Value AS Chase_Priority
						,CASE WHEN Attribute_Value = 1 then ''True'' When Attribute_Value = 0 then ''False''  END Is_Invoice_Collection_Setup_Activated
						,ui.First_Name+SPACE(1)+ui.Last_Name as Officer
						,o_ui.First_Name+SPACE(1)+o_ui.Last_Name as Owner
						,case when vd.consolidated_Billing IS NULL THEN 0 ELSE 1 END Is_Consolidated_Billing
						,Max(icacg.Invoice_Collection_Account_Config_Group_name) Group_Name
						,MAX(icsource.Code_Value) as IC_Source
						
						,max(icsource.Code_Value) as icsource_Code_Value
						,max(icst.Code_Value) as icst_Code_Value
						,max(icsmc.Code_Value) as icsmc_Code_Value
						';

        SELECT
            @Dynamic_Query2 = N',MAX( CASE WHEN icl.Seq_No=0 THEN ivf.Code_Value END) AS Primary_Invoice_Frequency
					  ,MAX( CASE WHEN icl.Seq_No=1 THEN ivf.Code_Value END) AS Additional_1_Invoice_Frequency
					  ,MAX( CASE WHEN icl.Seq_No=2 THEN ivf.Code_Value END) AS Additional_2_Invoice_Frequency
					  ,MAX( CASE WHEN icl.Seq_No=3 THEN ivf.Code_Value END) AS Additional_3_Invoice_Frequency
					   ,MAX( CASE WHEN icl.Seq_No=4 THEN ivf.Code_Value END) AS Additional_4_Invoice_Frequency
                       ,MAX( CASE WHEN icl.Seq_No=0 THEN ivfp.Code_Value END)  AS Primary_Frequency_Pattern
                       ,MAX( CASE WHEN icl.Seq_No=1 THEN ivfp.Code_Value END) AS Additional_1_Frequency_Pattern
                       ,MAX( CASE WHEN icl.Seq_No=2 THEN ivfp.Code_Value END) AS Additional_2_Frequency_Pattern
                       ,MAX( CASE WHEN icl.Seq_No=3 THEN ivfp.Code_Value END) AS Additional_3_Frequency_Pattern
                       ,MAX( CASE WHEN icl.Seq_No=4 THEN ivfp.Code_Value END) AS Additional_4_Frequency_Pattern
                       ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Raised_Day END)  AS Primary_Expected_Invoice_Raised_Day
                 ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Raised_Day END) AS Additional_1_Expected_Invoice_Raised_Day
                       ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Raised_Day END) AS Additional_2_Expected_Invoice_Raised_Day
                       ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Raised_Day END) AS Additional_3_Expected_Invoice_Raised_Day
                       ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Raised_Day END) AS Additional_4_Expected_Invoice_Raised_Day
                       ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Raised_Month END)  AS Primary_Expected_Invoice_Raised_Month
					,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Raised_Month END) AS Additional_1_Expected_Invoice_Raised_Month
					   ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Raised_Month END) AS Additional_2_Expected_Invoice_Raised_Month
					   ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Raised_Month END) AS Additional_3_Expected_Invoice_Raised_Month
					   ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Raised_Month END) AS Additional_4_Expected_Invoice_Raised_Month
                       ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Raised_Day_Lag END)  AS Primary_Expected_Invoice_Raised_Day_Lag
                       ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Raised_Day_Lag END) AS Additional_1_Expected_Invoice_Raised_Day_Lag
                       ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Raised_Day_Lag END) AS Additional_2_Expected_Invoice_Raised_Day_Lag
                       ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Raised_Day_Lag END) AS Additional_3_Expected_Invoice_Raised_Day_Lag
          ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Raised_Day_Lag END) AS Additional_4_Expected_Invoice_Raised_Day_Lag
                       ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Received_Day END)  AS Primary_Expected_Invoice_Received_Day
                       ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Received_Day END) AS Additional_1_Expected_Invoice_Received_Day
					  ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Received_Day END) AS Additional_2_Expected_Invoice_Received_Day
					  ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Received_Day END) AS Additional_3_Expected_Invoice_Received_Day
					  ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Received_Day END) AS Additional_4_Expected_Invoice_Received_Day
                      ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Received_Month END)  AS Primary_Expected_Invoice_Received_Month
                      ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Received_Month END) AS Additional_1_Expected_Invoice_Received_Month
                      ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Received_Month END) AS Additional_2_Expected_Invoice_Received_Month
                      ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Received_Month END) AS Additional_3_Expected_Invoice_Received_Month
                      ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Received_Month END) AS Additional_4_Expected_Invoice_Received_Month
                      ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Expected_Invoice_Received_Day_Lag END)  AS Primary_Expected_Invoice_Received_Day_Lag
                      ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Expected_Invoice_Received_Day_Lag END) AS Additional_1_Expected_Invoice_Received_Day_Lag
                      ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Expected_Invoice_Received_Day_Lag END) AS Additional_2_Expected_Invoice_Received_Day_Lag
                      ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Expected_Invoice_Received_Day_Lag END) AS Additional_3_Expected_Invoice_Received_Day_Lag
                      ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Expected_Invoice_Received_Day_Lag END) AS Additional_4_Expected_Invoice_Received_Day_Lag 

					  ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Custom_Days END)  AS Primary_Custom_Days
                      ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Custom_Days END) AS Additional_1_Custom_Days
                      ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Custom_Days END) AS Additional_2_Custom_Days
                      ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Custom_Days END) AS Additional_3_Custom_Days
                      ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Custom_Days END) AS Additional_4_Custom_Days 

					  ,MAX( CASE WHEN icl.Seq_No=0 THEN icl.Custom_Invoice_Received_Day END)  AS Primary_Custom_Invoice_Received_Day
                      ,MAX( CASE WHEN icl.Seq_No=1 THEN icl.Custom_Invoice_Received_Day END) AS Additional_1_Custom_Invoice_Received_Day
                      ,MAX( CASE WHEN icl.Seq_No=2 THEN icl.Custom_Invoice_Received_Day END) AS Additional_2_Custom_Invoice_Received_Day
                      ,MAX( CASE WHEN icl.Seq_No=3 THEN icl.Custom_Invoice_Received_Day END) AS Additional_3_Custom_Invoice_Received_Day
                      ,MAX( CASE WHEN icl.Seq_No=4 THEN icl.Custom_Invoice_Received_Day END) AS Additional_4_Custom_Invoice_Received_Day 


                      ,MAX(CASE WHEN ics.Is_Primary=1 THEN icsource.Code_Value+SPACE(1)+icst.Code_Value END ) AS Primary_Source
				      		 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Mail Redirect'' THEN ics.Collection_Instruction END) AS Vendor_Mail_Redirect_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN ics.Collection_Instruction END ) AS DataFeed_ETL_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN ics.Collection_Instruction END ) AS DataFeed_Partner_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Vendor Primary Contact'' THEN ics.Collection_Instruction END ) AS Vendor_Primary_Contact_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Online'' THEN ics.Collection_Instruction END ) AS Vendor_Onlie_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' AND icst.Code_Value=''Client Primary Contact'' THEN ics.Collection_Instruction END ) AS Client_Primary_Contact_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' AND icst.Code_Value=''Account Primary Contact'' THEN ics.Collection_Instruction END ) AS Account_Primary_Contact_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' AND icst.Code_Value=''Online'' THEN ics.Collection_Instruction END ) AS Client_Onlie_CI
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Mail Redirect'' THEN ''Yes'' END) AS Mail_Redirect
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN ubm.UBM_NAME END ) AS Partner
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN ubmt.Code_Value END ) AS Partner_Type
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN dt.URL END ) AS Partner_URL
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN dt.Login_Name END ) AS Partner_Login_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN dt.Passcode END ) AS Partner_Passcode
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''Partner'' THEN dt.Instruction END) AS Partner_Instruction
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfedftc.Code_Value END ) AS Etl_Data_Feed_Type
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfefs.Code_Value END ) AS Etl_File_Source
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfev.Vendor_Name END ) AS Etl_File_Vendor_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfe.URL END ) AS Etl_File_URL
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfe.Login_Name END ) AS Etl_File_Login_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfe.Passcode END ) AS Etl_File_Passcode
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfe.Generic_Key END) AS Etl_Generic_Key
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfe.Website_Instructions END) AS Etl_Website_Instructions
							  ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfiisc.Code_Value END ) AS Etl_Image_Source
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfiv.Vendor_Name END ) AS Etl_Image_Vendor_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfei.URL END ) AS Etl_Image_URL
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfei.Login_Name END ) AS Etl_Image_Login_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfei.Passcode END ) AS Etl_Image_Passcode
							 ,MAX(CASE WHEN icsource.Code_Value=''Data Feed'' AND icst.Code_Value=''ETL'' THEN dfei.Image_Load_Location END) AS Etl_Image_Load_Location
							 ,MAX( CASE WHEN ibpicacm.Enroll_In_Bot = 1 THEN 1 ELSE 0 END ) AS Etl_Image_Enroll_In_Bot
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' THEN dt.URL END) AS Vendor_URL
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' THEN dt.Login_Name END) AS Vendor_Login_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' THEN dt.Passcode END) AS Vendor_Passcode
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' THEN dt.Instruction END) AS Vendor_Instruction
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' THEN dt.URL END) AS Client_URL
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' THEN dt.Login_Name END) AS Client_Login_Name
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' THEN dt.Passcode END) AS Client_Passcode
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' THEN dt.Instruction END ) AS Client_Instruction
							 ,MAX(CASE WHEN icsource.Code_Value=''Vendor'' AND icst.Code_Value=''Vendor Primary Contact'' THEN icsmc.Code_Value END ) AS Vendor_Primary_Contact
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' AND icst.Code_Value=''Client Primary Contact'' THEN icsmc.Code_Value END ) AS Client_Primary_Contact
							 ,MAX(CASE WHEN icsource.Code_Value=''Client'' AND icst.Code_Value=''Account Primary Contact'' THEN icsmc.Code_Value END ) AS Account_Primary_Contact
							 ,MAX(CASE WHEN cl.Code_Value=''Vendor'' AND icac.Is_Primary=1 THEN cif.First_Name+SPACE(1)+cif.Last_Name END ) AS Primary_Vendor_Contact
							 ,MAX(CASE WHEN cl.Code_Value=''Client'' AND icac.Is_Primary=1 THEN cif.First_Name+SPACE(1)+cif.Last_Name END ) AS Primary_Client_Contact
							,MAX(CASE WHEN cl.Code_Value=''Account'' AND icac.Is_Primary=1 THEN cif.First_Name+SPACE(1)+cif.Last_Name END ) AS Primary_Account_Contact
							,icastc.code_value Service_Type
							,ibp.IC_Bots_Process_Name
							,ibp.IC_Bots_Process_Id '
        WHERE
            @Is_Excel_Download = 1;



        SELECT
            @Dynamic_Query3 = N',DENSE_RANK() OVER (ORDER BY cha.Account_Id  ) Row_Num
                  
                   FROM
                        core.Client_Hier_Account cha
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
						LEFT JOIN Commodity c
							on cha.commodity_id=c.commodity_id
						LEFT JOIN dbo.Client_Hier_Account_Attribute_Tracking chaa
                              ON cha.Account_Id = chaa.Account_Id
                                 AND chaa.Client_Attribute_Id = '
                              + CAST(@Set_up_Invoice_Collection_for_account_Hub_Client_Attribute_Id AS VARCHAR)
                              + +N' LEFT JOIN dbo.Invoice_Collection_Account_Config ica
                              ON ica.Account_Id = cha.Account_Id
						
                        LEFT OUTER JOIN Code icastc
								on icastc.code_Id = ica.Service_Type_Cd
						LEFT JOIN dbo.Account_Invoice_Collection_Source ics
                              ON ica.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
							  and ics.is_primary=1
						LEFT JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl dt
							  ON ics.Account_Invoice_Collection_Source_Id = dt.Account_Invoice_Collection_Source_Id
						LEFT JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ub
							  ON ics.Account_Invoice_Collection_Source_Id = ub.Account_Invoice_Collection_Source_Id
						LEFT OUTER JOIN Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl dfe
							   ON ics.Account_Invoice_Collection_Source_Id = dfe.Account_Invoice_Collection_Source_Id
						LEFT OUTER JOIN Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl dfei
							   ON ics.Account_Invoice_Collection_Source_Id = dfei.Account_Invoice_Collection_Source_Id
						LEFT OUTER JOIN Code dfefs
								ON dfefs.Code_Id = dfe.File_Source_Cd
						LEFT OUTER JOIN Code dfedftc
								ON dfedftc.Code_Id = dfe.Data_Feed_Type_Cd
						LEFT OUTER JOIN Vendor dfev
						ON dfev.Vendor_Id = dfe.Vendor_Id
						LEFT OUTER JOIN Code dfiisc
								ON dfiisc.Code_Id = dfei.Image_Source_Cd
						LEFT OUTER JOIN Vendor dfiv
								ON dfiv.Vendor_Id = dfei.Vendor_Id
						LEFT JOIN dbo.cbms_image ci
								  ON dt.Instruction_Document_Image_Id = ci.CBMS_IMAGE_ID
			            LEFT JOIN dbo.Invoice_Collection_Account_Contact icac
							  ON icac.Invoice_Collection_Account_Config_Id=ica.Invoice_Collection_Account_Config_Id
						LEFT JOIN dbo.Contact_Info cif
							  ON icac.Contact_Info_Id = cif.Contact_Info_Id
						LEFT JOIN dbo.Code cl
							  ON cif.Contact_Level_Cd = cl.Code_Id							  
                        LEFT JOIN dbo.Account_Invoice_Collection_Frequency icl
                              ON ica.Invoice_Collection_Account_Config_Id = icl.Invoice_Collection_Account_Config_Id
                        LEFT JOIN dbo.Code cp
                              ON ica.Chase_Priority_Cd = cp.Code_Id
                        LEFT JOIN dbo.Code ipc
                              ON ica.Invoice_Pattern_Cd = ipc.Code_Id
                        LEFT JOIN dbo.Code ivf
							  ON icl.Invoice_Frequency_Cd=ivf.Code_Id
						LEFT JOIN dbo.Code ivfp
							  ON icl.Invoice_Frequency_Pattern_Cd=ivfp.Code_Id
						LEFT JOIN dbo.USER_INFO ui
						ON ica.Invoice_Collection_Officer_User_Id=ui.USER_INFO_ID
						LEFT JOIN dbo.USER_INFO o_ui
							ON ica.Invoice_Collection_Owner_User_Id=o_ui.USER_INFO_ID
						LEFT JOIN dbo.Code icsource
                              ON ics.Invoice_Collection_Source_Cd = icsource.Code_Id
                        LEFT JOIN dbo.Code icst
                              ON ics.Invoice_Source_Type_Cd = icst.Code_Id
                        LEFT JOIN dbo.Code icsmc
                              ON ics.Invoice_Source_Method_of_Contact_Cd = icsmc.Code_Id
                        LEFT JOIN dbo.Code ubmt
							ON ub.UBM_Type_Cd = ubmt.Code_Id
						LEFT JOIN dbo.UBM ubm
							ON ub.UBM_Id=ubm.UBM_ID 
						LEFT OUTER JOIN ( SELECT
                                                
      Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                                                AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                                                           WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME                                                                           
                                                                           
                                                                WHEN asbv1.Account_Id IS NOT NULL THEN ''Unknown Vendor''
                                            ELSE ucha.Account_Vendor_Name
                                                                      END
    ,Account_Vendor_Id = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                                                AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
																		 WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_Id
																		 WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
																			ELSE ucha.Account_Vendor_Id
                                                                      END
                                               ,Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                          AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                                           WHEN v.VENDOR_NAME IS NOT NULL THEN e.ENTITY_NAME
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN ve.ENTITY_NAME
																			 WHEN asbv1.Account_Id IS NOT NULL THEN ''Supplier''
                                                                           ELSE ucha.Account_Type
                                                                      END
                                               
                                               ,Contact_Info_Id =CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                                                AND asbv1.Account_Id IS NOT NULL THEN Null
                                                                           WHEN e.ENTITY_NAME IS NOT NULL THEN NULL
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN ci.Contact_Info_Id
                                                                           ELSE NULL
                                                                      END
                                               ,ucha.Account_Id
                                               ,asbv1.Account_Id consolidated_Billing
                                               
                                          FROM
                                                core.Client_Hier_Account ucha
                                                INNER JOIN core.Client_Hier ch
                                                      ON ucha.Client_Hier_Id = ch.Client_Hier_Id
                                                LEFT OUTER JOIN dbo.Invoice_Collection_Account_Config icac
                                                      ON icac.Account_Id = ucha.Account_Id';


        SELECT
            @Dynamic_Query4 = N'LEFT OUTER JOIN core.Client_Hier_Account scha
                                                      ON ucha.Meter_Id = scha.Meter_Id
                                                 AND scha.Account_Type = ''Supplier''
                                                         AND ''' + CAST(@Vendor_Date AS VARCHAR(20))
                              + N''' BETWEEN scha.Supplier_Account_begin_Dt
                                                                                                  AND     scha.Supplier_Account_End_Dt
                                                        
                                               
                                                LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv
                                                                  INNER JOIN dbo.ENTITY e
                                                  ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                                AND e.ENTITY_NAME = ''Supplier''
                                                                  LEFT OUTER JOIN dbo.VENDOR v
        ON v.VENDOR_ID = asbv.Supplier_Vendor_Id )
                                                                  ON asbv.Account_Id = ucha.Account_Id
                                                                  AND ''' + CAST(@Vendor_Date AS VARCHAR(20))
                              + N''' BETWEEN asbv.Billing_Start_Dt
                                                                                                             AND     ISNULL(asbv.Billing_End_Dt, ''9999-12-31'')';



        SELECT
            @Dynamic_Query5 = N'LEFT OUTER JOIN ( dbo.Account_Consolidated_Billing_Vendor asbv1
                                                                  INNER JOIN dbo.ENTITY e1
                                                                        ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                                                           AND e1.ENTITY_NAME = ''Supplier''
                                                                  LEFT OUTER JOIN dbo.VENDOR v1
                                                                        ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id )
                                                                  ON asbv1.Account_Id = ucha.Account_Id
                 		 LEFT OUTER JOIN( dbo.Invoice_Collection_Account_Contact icc
																	   INNER JOIN Contact_Info ci
																			ON ci.Contact_Info_Id = icc.Contact_Info_Id
																			 AND icc.Is_Primary = 1
																	  INNER JOIN #Contact_Source_Type cst
																	  ON cst.Contact_Level_Vendor_Cd = ci.Contact_Level_Cd	    
																	  INNER JOIN dbo.Vendor_Contact_Map vcm
																			ON ci.Contact_Info_Id = vcm.Contact_Info_Id
																	  INNER JOIN dbo.VENDOR icav
																			ON icav.VENDOR_ID = vcm.Vendor_Id																			
																	  INNER JOIN dbo.ENTITY ve
																			ON icav.Vendor_Type_Id = ve.Entity_Id 
																	  INNER JOIN dbo.Invoice_Collection_Account_Config icac1
																	         ON icc.Invoice_Collection_Account_Config_Id = icac1.Invoice_Collection_Account_Config_Id
																	         AND icac1.Is_Chase_Activated = 1	
																			)
													ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id	 WHERE
                                                ucha.Account_Type = ''Utility''
												GROUP BY';
        SELECT
            @Dynamic_Query6 = N' CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                                                AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                                                           WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME                                                                           
                                                                           WHEN asbv1.Account_Id IS NOT NULL THEN ''Unknown Vendor''
                                                                           ELSE ucha.Account_Vendor_Name
                                                                      END
                                                ,CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                         AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                                                 WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_Id
																			WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                                                                           ELSE ucha.Account_Vendor_Id
              END
   ,CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                          AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                                           WHEN v.VENDOR_NAME IS NOT NULL THEN e.ENTITY_NAME
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN ve.ENTITY_NAME
																			 WHEN asbv1.Account_Id IS NOT NULL THEN ''Supplier''
                                                                           ELSE ucha.Account_Type
                                                                      END
                                               ,ucha.Account_Id,asbv1.Account_Id,CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                                                                AND asbv1.Account_Id IS NOT NULL THEN Null
                                                                           WHEN e.ENTITY_NAME IS NOT NULL THEN NULL
                                                                           WHEN icav.VENDOR_NAME IS NOT NULL
																			AND asbv1.Account_Id IS NOT NULL THEN ci.Contact_Info_Id
                                  ELSE NULL
                                                                      END ) vd
                              ON( vd.Account_Id = cha.account_id)
                              CROSS APPLY ( SELECT  
                                          cch.Site_Name + '',''  
                                      FROM  
                                          core.Client_Hier_Account cucha
                                                INNER JOIN core.Client_Hier cch
                ON cucha.Client_Hier_Id = cch.Client_Hier_Id
                                      WHERE  
                                          cucha.Account_Id   = cha.Account_Id
                                          AND cucha.Account_Not_Managed = 0
                                         GROUP BY cch.Site_Name
                        FOR  
                                      XML PATH('''') ) sites ( Site_Name )	';


        SELECT
            @Dynamic_Query7 = N' LEFT OUTER JOIN Invoice_Collection_Account_Config_Group_Map icacm
							on icacm.Invoice_Collection_Account_Config_Id=ica.Invoice_Collection_Account_Config_Id
						LEFT JOIN Invoice_Collection_Account_Config_Group icacg
							  on icacm.Invoice_Collection_Account_Config_Group_Id=icacg.Invoice_Collection_Account_Config_Group_Id							  
						LEFT JOIN dbo.IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
						ON ibpicacm.Invoice_Collection_Account_Config_Id = ica.Invoice_Collection_Account_Config_Id
							LEFT JOIN dbo.IC_Bots_Process ibp
								ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id 
							   ';


        SELECT
            @Dynamic_Query8 = N'WHERE                       
                        ' + @Filter_Fields + N' AND cha.Account_Not_Managed = 0 '
                              + N'AND ( cif.Contact_Info_Id IS NULL OR vd.Contact_Info_Id is NULL OR vd.Contact_Info_Id = cif.Contact_Info_Id ) AND ('
                              + CAST(@Display_All_Timebands AS VARCHAR(10)) + N' = 1  
							  OR  ''' + CAST(@Vendor_Date AS VARCHAR)
                              + N''' BETWEEN ica.Invoice_Collection_Service_Start_Dt AND ISNULL(ica.Invoice_Collection_Service_End_Dt, ''9999-12-31''))'
                              + N' AND (  ' + CAST(@Cnt_Of_Invoice_Collection_Source AS VARCHAR(10))
                              + N' = 0
                    OR  ( ics.Is_Primary=1 AND  EXISTS (   SELECT
                                            1
                                       FROM
                                            #Tvp_Invoice_Collection_Sources tvp
                                       WHERE
                                            tvp.Column_Name = ''Invoice_Source_Type_Cd''
                                            AND ics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
											AND tvp.Column_Type = icsource.Code_Value
											AND icst.Code_Value NOT IN ( ''Online'', ''Mail Redirect''))
                    AND EXISTS (   SELECT
                    1
                                   FROM
                                                #Tvp_Invoice_Collection_Sources tvp
                                           WHERE
                                                tvp.Column_Name = ''Invoice_Source_Method_of_Contact_Cd''
                                                AND ics.Invoice_Source_Method_of_Contact_Cd = CAST(tvp.Column_Value AS INT)
                                                AND tvp.Column_Type = icsource.Code_Value
                                                AND icst.Code_Value NOT IN ( ''Online'', ''Mail Redirect'' )))
							OR  (ics.IS_Primary = 1 AND EXISTS (   SELECT
												1
											FROM
												#Tvp_Invoice_Collection_Sources tvp
											WHERE
												tvp.Column_Name = ''Invoice_Source_Type_Cd''
												AND ics.Invoice_Source_Type_Cd = CAST(tvp.Column_Value AS INT)
												AND tvp.Column_Type = icsource.Code_Value
												AND icst.Code_Value IN ( ''Online'', ''Mail Redirect'',''ETL'',''Partner'' )))
												
												 )'
                              + N' GROUP BY
                        cha.Account_Id                  
						,ica.Invoice_Collection_Account_Config_Id
						,ica.Invoice_Collection_Alternative_Account_Number
						,c.Commodity_Name 
						,ica.Is_Chase_Activated
						,icastc.code_value
						,ch.Country_Name
						,sites.Site_name
						,ch.State_Name
						,ch.Client_Name
						,ch.Sitegroup_Name
						,cha.Display_Account_Number
						,ISNULL(vd.Account_Vendor_Type,cha.Account_Type)
						,ISNULL(vd.Account_Vendor_Name,cha.Account_Vendor_Name)
						,ISNULL(vd.Account_Vendor_Id,cha.Account_Vendor_Id) 
						,ch.Client_Id
						,cha.Supplier_Contract_ID
						,ica.Additional_Invoices_Per_Period
						,ica.Invoice_Collection_Service_Start_Dt
						,ica.Invoice_Collection_Service_End_Dt
						,case when vd.consolidated_Billing IS NULL THEN 0 ELSE 1 END ,cp.Code_Value
						,ipc.Code_value
						,Attribute_Value
						,ui.First_Name+SPACE(1)+ui.Last_Name,o_ui.First_Name+SPACE(1)+o_ui.Last_Name
						,ibp.IC_Bots_Process_Name
						,ibp.IC_Bots_Process_Id
						 ),
                       total_row AS
                       ( SELECT COUNT(DISTINCT Row_Num) AS Total_rows  FROM CTE_Bulk_Get C )
            SELECT
				  c.*,CASE WHEN @Total_Row_Count=0 THEN C2.Total_rows ELSE @Total_Row_Count END AS Total_Count
            FROM
                  CTE_Bulk_Get c
                  CROSS JOIN total_row C2
			WHERE
                  c.Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR) + N' AND ' + CAST(@EndIndex AS VARCHAR)
                              + N'
            ORDER BY ' + @Sort_Order;


        EXEC (@Dynamic_Query1 + ' ' + @Dynamic_Query2 + ' ' + @Dynamic_Query3 + ' ' + @Dynamic_Query4 + ' ' + @Dynamic_Query5 + ' ' + @Dynamic_Query6 + ' ' + @Dynamic_Query7 + ' ' + @Dynamic_Query8);






        DROP TABLE
            #Contact_Source_Type
            , #Tvp_Invoice_Collection_Sources;
    END;







    ;



















GO



GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Bulk_Upload_Sel] TO [CBMSApplication]
GO
