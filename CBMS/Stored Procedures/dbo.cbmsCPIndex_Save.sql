SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE       PROCEDURE [dbo].[cbmsCPIndex_Save]
	( @clearport_index_id int = null
	, @clearport_index varchar(200)
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @clearport_index_id 


	if @this_id is null
	begin

		insert into clearport_index
			( clearport_index
			)
		 values
			( @clearport_index
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as clearport_index_id

	end
	else
	begin

		   update clearport_index with (rowlock)
		      set clearport_index = @clearport_index
		    where clearport_index_id = @this_id

	end

	set nocount off



END












SET QUOTED_IDENTIFIER ON 











GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndex_Save] TO [CBMSApplication]
GO
