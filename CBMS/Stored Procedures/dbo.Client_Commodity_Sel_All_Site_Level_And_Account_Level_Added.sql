
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType	  Default		Description
------------------------------------------------------------------------------------
    @Client_Id		INT
    @Division_Id	INT		  NULL
    @Client_Hier_Id INT		  NULL
     
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added 11239
	EXEC Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added 10069
	
	EXEC Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added 10069,561

	SELECT * FROM Core.Client_Commodity WHERE Client_id = 10069

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Harihara Suthan G
	AP		Athmaram Pabbathi

MODIFICATIONS
    Initials	Date		   Modification
------------------------------------------------------------
    HG        	4/20/2010	   Created
    AP		2012-03-19   Addnl Data Changes
						  -- Replaced @Site_Id parameter with @Client_Hier_Id
						  -- Removed Site, Division, Meter, Rate base tables and used CH & CHA
******/

CREATE PROCEDURE dbo.Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added
      ( 
       @Client_Id INT
      ,@Division_Id INT = NULL
      ,@Client_Hier_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            cc.Commodity_Id
           ,com.Commodity_Name
           ,cc.Hier_Level_Cd
           ,heir_level.Code_Value Hier_level_Cd_Value
           ,com.Default_UOM_Entity_Type_Id
           ,bkt_agg_rule.Code_Value Bucket_Aggregation_Rule
           ,com.UOM_Entity_Type
      FROM
            Core.Client_Commodity cc
            JOIN dbo.Commodity com
                  ON com.Commodity_id = cc.Commodity_id
            JOIN dbo.CODE heir_level
                  ON cc.hier_level_cd = heir_level.code_id
            JOIN dbo.CODE service_type
                  ON cc.commodity_service_cd = service_type.code_id
            JOIN dbo.Code lvl
                  ON lvl.Code_Id = cc.Hier_Level_Cd
            JOIN dbo.Code bkt_agg_rule
                  ON bkt_agg_rule.Code_Id = com.Bucket_Aggregation_Cd
      WHERE
            cc.Client_Id = @Client_id
            AND service_type.Code_Value = 'Invoice'
            AND ( ( lvl.Code_Value = 'Site' )
                  OR ( lvl.Code_Value = 'Account'
                       AND EXISTS ( SELECT
                                          1
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN Core.Client_Hier_Account cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                    WHERE
                                          ch.Client_ID = cc.Client_id
                                          AND cha.Commodity_id = cc.Commodity_Id
                                          AND ( @Division_Id IS NULL
                                                OR ch.Sitegroup_Id = @Division_Id )
                                          AND ( @Client_Hier_Id IS NULL
                                                OR ch.Client_Hier_Id = @Client_Hier_Id ) ) ) )
      ORDER BY
            com.Commodity_Name

END
;
GO

GRANT EXECUTE ON  [dbo].[Client_Commodity_Sel_All_Site_Level_And_Account_Level_Added] TO [CBMSApplication]
GO
