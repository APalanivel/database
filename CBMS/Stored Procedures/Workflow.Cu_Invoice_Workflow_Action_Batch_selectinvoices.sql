SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
/******                
NAME:    [Workflow].[Cu_Invoice_Workflow_Action_Batch_selectinvoices]            
DESCRIPTION:  it'll Return the Left Navigation Codunt details for each Modules.            
------------------------------------------------------------               
 INPUT PARAMETERS:                
 Name   DataType  Default Description                
 @userID   INT              
------------------------------------------------------------                
 OUTPUT PARAMETERS:                
 Name   DataType  Default Description                
------------------------------------------------------------                
 USAGE EXAMPLES:                
 EXEC workflow.Cu_Invoice_Workflow_Action_Batch_selectinvoices @userID   = 49 ,  @Module_ID=1     
------------------------------------------------------------                
AUTHOR INITIALS:                
Initials Name                
------------------------------------------------------------                
TRK   Ramakrishna Thummala Summit Energy    
AP ARUNKUMAR PALANIVEL           
             
 MODIFICATIONS                 
 Initials Date   Modification                
------------------------------------------------------------      
              
 TRK    03-Oct-2019  Created     
 AP  OCT 15,2019 MODIFIED TO GET BELOW INFORMATION  
   
 Action Name: Name of the action  
Action Started Date/Time: Date and time when the batch were requested  
Action Total Count: Total of invoices included in the batch action.  
Action Total Completed: How many out of Action Total Count are completed  
Action Total Erros: How many out of Action Total Completed failed  
         
            
******/        
CREATE PROC [Workflow].[Cu_Invoice_Workflow_Action_Batch_selectinvoices]          
(          
@userid INT ,    
@Module_ID INT               
)          
AS          
BEGIN          
       
;WITH CTE AS (  
SELECT C.Code_Value AS Action_name,  
ciwab.Requested_Ts   Action_start_time,  
c2.Code_dsc,  
ciwab.Cu_Invoice_Workflow_Action_Batch_Id       
FROM [Workflow].[Cu_Invoice_Workflow_Action_Batch] AS  CIWAB        
INNER JOIN Workflow.Workflow_Queue_Action AS WQA ON CIWAB.Workflow_Queue_Action_Id=WQA.Workflow_Queue_Action_Id        
INNER JOIN DBO.Code C ON C.Code_Id=WQA.Workflow_Queue_Action_Cd        
INNER JOIN [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl] CIWABD ON CIWAB.Cu_Invoice_Workflow_Action_Batch_Id=CIWABD.Cu_Invoice_Workflow_Action_Batch_Id        
INNER JOIN DBO.Code C2 ON C2.Code_Id=CIWABD.Status_Cd        
WHERE CIWAB.Requested_User_Id=@userid   AND   
CAST(CIWAB.Requested_Ts AS DATE) = CAST(GETDATE() AS DATE) AND WQA.Workflow_Queue_Id =@Module_ID  )  
  
  
   
   
SELECT *   
INTO #Batch_Temp  
FROM (  
    SELECT   
         c.Cu_Invoice_Workflow_Action_Batch_Id,  
   c.Action_start_time,  
   c.Code_dsc  ,  
   c.Action_name   
    FROM cte C  
) AS s  
PIVOT  
(  
    COUNT (code_dsc)   
    FOR Code_dsc IN (Completed, [Failed or Error] )  
     
)AS pvt   
  
SELECT   
t.Cu_Invoice_Workflow_Action_Batch_Id, 
t.Action_name as Action_Name , 
t.Action_start_time as  Action_Started_Date_Time,   
t.Completed AS Action_Total_Completed , 
t.[Failed or Error] AS  Action_Total_Erros  ,    
(SELECT COUNT (1) FROM workflow.Cu_Invoice_Workflow_Action_Batch_Dtl cud  WHERE cud.Cu_Invoice_Workflow_Action_Batch_Id = t.Cu_Invoice_Workflow_Action_Batch_Id)  as  Action_Total_Count  

FROM #Batch_Temp  t   
GROUP BY t.Action_name , t.Action_start_time, t.Completed, t.Cu_Invoice_Workflow_Action_Batch_Id, t.[Failed or Error]  
    
DROP TABLE #Batch_Temp  
          
END     
           
        
GO
GRANT EXECUTE ON  [Workflow].[Cu_Invoice_Workflow_Action_Batch_selectinvoices] TO [CBMSApplication]
GO
