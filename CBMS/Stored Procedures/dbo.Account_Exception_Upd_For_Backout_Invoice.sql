SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Exception_Upd_For_Backout_Invoice

DESCRIPTION:

INPUT PARAMETERS:
	Name								DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Account_Exception_Id				INT
    @User_Info_Id						INT
    @Exception_Status_Cd				INT

OUTPUT PARAMETERS:
	Name								DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
  
  BEGIN TRAN

EXEC Account_Exception_Upd_For_Backout_Invoice
    @Cu_Invoice_Id = 1  
    , @User_Info_Id = 1

	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------------------
	NR       	2019-07-19		Created for Add Contract.

******/

CREATE PROCEDURE [dbo].[Account_Exception_Upd_For_Backout_Invoice]
    (
        @Cu_Invoice_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Exception_Type_Cd INT
            , @Account_Exception_Id INT
            , @Remaining_Invoice_cnt INT
            , @New_Status_Cd INT
            , @Inprogress_Status_Cd INT
            , @Closed_Status_Cd INT
            , @Account_Id INT
            , @Counter INT = 1
            , @Max_Account_Cnt INT;



        CREATE TABLE #Account_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );


        INSERT INTO #Account_List
             (
                 Account_Id
             )
        SELECT
            cism.Account_ID
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cism.Account_ID;









        SELECT
            @New_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                 END)
            , @Inprogress_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                          END)
            , @Closed_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Closed' THEN c.Code_Id
                                      END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value IN ( 'New', 'In Progress', 'Closed' );

        SELECT  @Max_Account_Cnt = MAX(Id)FROM  #Account_List;


        WHILE (@Counter <= @Max_Account_Cnt)
            BEGIN

                SELECT
                    @Account_Id = al.Account_Id
                FROM
                    #Account_List al
                WHERE
                    al.Id = @Counter;



                SELECT
                    @Account_Exception_Id = ae.Account_Exception_Id
                FROM
                    dbo.Account_Exception_Cu_Invoice_Map aecim
                    INNER JOIN dbo.Account_Exception ae
                        ON ae.Account_Exception_Id = aecim.Account_Exception_Id
                WHERE
                    aecim.Cu_Invoice_Id = @Cu_Invoice_Id
                    AND ae.Account_Id = @Account_Id
                    AND ae.Exception_Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd );



                SELECT
                    @Remaining_Invoice_cnt = COUNT(DISTINCT aecim.Cu_Invoice_Id)
                FROM
                    dbo.Account_Exception_Cu_Invoice_Map aecim
                    INNER JOIN dbo.Account_Exception ae
                        ON ae.Account_Exception_Id = aecim.Account_Exception_Id
                WHERE
                    ae.Account_Exception_Id = @Account_Exception_Id
                    AND ae.Exception_Status_Cd IN ( @New_Status_Cd, @Inprogress_Status_Cd )
                    AND aecim.Cu_Invoice_Id <> @Cu_Invoice_Id;





                UPDATE
                    aecim
                SET
                    aecim.Last_Change_Ts = GETDATE()
                    , aecim.Updated_User_Id = @User_Info_Id
                    , aecim.Status_Cd = @Closed_Status_Cd
                FROM
                    dbo.Account_Exception_Cu_Invoice_Map aecim
                WHERE
                    aecim.Account_Exception_Id = @Account_Exception_Id
                    AND aecim.Cu_Invoice_Id = @Cu_Invoice_Id;






                UPDATE
                    ae
                SET
                    ae.Last_Change_Ts = GETDATE()
                    , ae.Updated_User_Id = @User_Info_Id
                    , ae.Exception_Status_Cd = @Closed_Status_Cd
                FROM
                    dbo.Account_Exception ae
                WHERE
                    ae.Account_Exception_Id = @Account_Exception_Id
                    AND ISNULL(@Remaining_Invoice_cnt, 0) = 0;



                SET @Counter = @Counter + 1;

            END;

    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Upd_For_Backout_Invoice] TO [CBMSApplication]
GO
