SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RPT_GET_ALL_SOURCING_ANALYSTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- GETS ALL THE SOURCING ANALYSTS IN THE SYSTEM

CREATE          PROCEDURE dbo.SR_SAD_RPT_GET_ALL_SOURCING_ANALYSTS_P  AS
/*
select 	userInfo.user_info_id USER_INFO_ID,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME ANALYST_NAME

from 	user_info userInfo, utility_analyst_map map 

where 	( map.gas_analyst_id  = userInfo.user_info_id or  map.power_analyst_id  = userInfo.user_info_id )
	and userInfo.is_history = 0

order by userInfo.FIRST_NAME

*/
	set nocount on
select 	userInfo.user_info_id USER_INFO_ID,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME ANALYST_NAME

from 	user_info userInfo, user_info_group_info_map map, group_info groupInfo

where 	map.group_info_id = groupInfo.group_info_id
	and map.user_info_id = userInfo.user_info_id
	and groupInfo.group_name='supply'

order by userInfo.FIRST_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_ALL_SOURCING_ANALYSTS_P] TO [CBMSApplication]
GO
