SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.RC_RATE_COMPARISON_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rcId          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.RC_RATE_COMPARISON_DETAILS_P 1042
	EXEC dbo.RC_RATE_COMPARISON_DETAILS_P 11911
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SSR			03/30/2010	Replaced Entity(Entity_id) with Commodity(Commodity_id)	        	
							Converted Non ansi-joins to ansi-joins		        	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE  PROCEDURE [dbo].[RC_RATE_COMPARISON_DETAILS_P]
	@rcId INTEGER 
AS

BEGIN

	SET NOCOUNT ON

	SELECT
		rateComparison.RC_RATE_COMPARISON_ID,
		rateComparison.CLIENT_ID,
		vendor.VENDOR_NAME UTILITY_NAME,
		userInfo.FIRST_NAME+SPACE(1)+userInfo.LAST_NAME CREATED_BY,
		ISNULL(rateComparison.RATE_COMPARISON_NAME,'' ) RATE_COMPARISON_NAME,
		ISNULL(client.CLIENT_NAME,'' ) CLIENT_NAME,
		s.SITE_ID SITE_ID,
		s.SITE_NAME SITE_NAME,
		ISNULL(region.REGION_DESCRIPTION,'' ) REGION_NAME,
		ISNULL(st.STATE_NAME,'' ) STATE_NAME,
		ISNULL(rateComparison.CITY,'' ) CITY,
		ISNULL(rate.RATE_NAME,'' ) RATE_NAME,
		ISNULL(CONVERT (VARCHAR(12), rateComparison.CREATION_DATE, 101),'')  CREATION_DATE,
		ISNULL(account.ACCOUNT_NUMBER,'' ) ACCOUNT_NUMBER,
		ISNULL(meter.METER_NUMBER,'' ) METER_NUMBER,
		entity.ENTITY_NAME STATUS_NAME,
		entity1.ENTITY_NAME RESULT_NAME,
		ISNULL(userInfo1.FIRST_NAME+SPACE(1)+userInfo1.LAST_NAME,'') REVIEWED_BY,
		ISNULL(rateComparison.RATE_COMPARISON_DOCUMENT_XML,'-1' ) RATE_COMPARISON_DOCUMENT_XML,
		com.Commodity_Name AS COMMODITY_NAME,
		rateComparison.REGION_ID,
		rateComparison.created_by AS CREATED_BY_ID,
		ISNULL(rateComparison.CBMS_IMAGE_ID,0)  CBMS_IMAGE_ID
	FROM
		dbo.RC_RATE_COMPARISON rateComparison
		INNER JOIN dbo.VENDOR vendor
			ON rateComparison.UTILITY_ID=vendor.VENDOR_ID
		INNER JOIN dbo.USER_INFO userInfo
			ON rateComparison.CREATED_BY=userInfo.USER_INFO_ID
		INNER JOIN dbo.ENTITY entity
			ON rateComparison.STATUS_TYPE_ID=entity.ENTITY_ID
		INNER JOIN dbo.ENTITY entity1
			ON rateComparison.RESULT_TYPE_ID=entity1.ENTITY_ID
		LEFT JOIN dbo.CLIENT
			ON rateComparison.CLIENT_ID = CLIENT.CLIENT_ID
		LEFT JOIN dbo.SITE s
			ON rateComparison.SITE_ID = s.SITE_ID
		LEFT JOIN dbo.REGION region
			ON rateComparison.REGION_ID = region.REGION_ID
		LEFT JOIN dbo.STATE st
			ON rateComparison.STATE_ID = st.STATE_ID
		LEFT JOIN dbo.ACCOUNT account
			ON rateComparison.ACCOUNT_ID = account.ACCOUNT_ID
		LEFT JOIN dbo.RATE rate
			ON rateComparison.RATE_ID = rate.RATE_ID
		LEFT JOIN dbo.Commodity com
			ON rate.commodity_type_id = com.Commodity_Id
		LEFT JOIN dbo.METER meter
			ON rateComparison.METER_ID = METER.METER_ID
		LEFT JOIN dbo.USER_INFO userInfo1
			ON rateComparison.REVIEWED_BY = userInfo1.USER_INFO_ID
	WHERE
	 	rateComparison.RC_RATE_COMPARISON_ID = @rcId

END
GO
GRANT EXECUTE ON  [dbo].[RC_RATE_COMPARISON_DETAILS_P] TO [CBMSApplication]
GO
