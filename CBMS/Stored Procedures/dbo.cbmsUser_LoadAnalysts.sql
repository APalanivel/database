
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.cbmsUser_LoadAnalysts       
              
Description:              
			This sproc to load the cbms Analysts users .   
                           
 Input Parameters:              
    Name							DataType				Default			Description                
---------------------------------------------------------------------------------------------
	@MyAccountId					INT
  
 Output Parameters:                    
    Name							DataType				Default			Description                
---------------------------------------------------------------------------------------------
              
 Usage Examples:                  
---------------------------------------------------------------------------------------------

	EXEC  dbo.cbmsUser_LoadAnalysts  49
	
	EXEC  dbo.cbmsUser_LoadAnalysts  49

Author Initials:              
    Initials		Name              
---------------------------------------------------------------------------------------------
	NR				Narayana Reddy 
	              
 Modifications:              
    Initials        Date			Modification              
---------------------------------------------------------------------------------------------
    NR				2015-07-20		MAINT-3061 Added Header and Removed the view vwCbms Analyst.    
             
******/   
CREATE PROCEDURE [dbo].[cbmsUser_LoadAnalysts] ( @MyAccountId INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      SELECT
            Analyst_Id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Analyst_Name
      FROM
            dbo.Utility_Detail_Analyst_Map UDAM
            INNER JOIN dbo.GROUP_INFO GI
                  ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = UDAM.Analyst_ID
            INNER JOIN dbo.Code c
                  ON c.Code_Id = gi.Group_Legacy_Name_Cd
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value IN ( 'Regulated_Market', 'Quality_Assurance' )
            AND cs.Codeset_Name = 'Group_Legacy_Name'
      GROUP BY
            Analyst_Id
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
      ORDER BY
            ui.FIRST_NAME + ' ' + ui.LAST_NAME 
            
END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsUser_LoadAnalysts] TO [CBMSApplication]
GO
