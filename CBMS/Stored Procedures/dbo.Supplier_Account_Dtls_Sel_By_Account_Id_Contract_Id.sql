SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Supplier_Account_Dtls_Sel_By_Account_Id_Contract_Id                     
                          
 DESCRIPTION:      
		Get the Suppler account associated invoices count based on Supplier Account.                   
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Sup_Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 

	EXEC dbo.Supplier_Account_Dtls_Sel_By_Account_Id_Contract_Id  1741006     
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/



CREATE PROCEDURE [dbo].[Supplier_Account_Dtls_Sel_By_Account_Id_Contract_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id AS Utility_Account_Id
            , Uti_cha.Account_Number AS Utility_Account_Number
            , Sup_Cha.Account_Id AS Supplier_Account_Id
            , Sup_Cha.Account_Number AS Supplier_Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , Sup_Cha.Supplier_Account_End_Dt
            , CASE WHEN LEN(cnt.Meter_Number) > 1 THEN LEFT(cnt.Meter_Number, LEN(cnt.Meter_Number) - 1)
                  ELSE cnt.Meter_Number
              END Meter_Number
            , COUNT(DISTINCT cism.CU_INVOICE_ID) AS Invoices
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account Uti_cha
                ON Uti_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Sup_Cha
                ON Uti_cha.Meter_Id = Sup_Cha.Meter_Id
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Sup_Cha.Account_Id
                   AND  cism.SERVICE_MONTH BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                           AND     ISNULL(Sup_Cha.Supplier_Account_End_Dt, '2099-01-31')
            CROSS APPLY (   SELECT
                                chh.Meter_Number + ','
                            FROM
                                Core.Client_Hier_Account chh
                            WHERE
                                chh.Account_Id = Sup_Cha.Account_Id
                                AND Sup_Cha.Account_Id = @Account_Id
                            GROUP BY
                                chh.Meter_Number
                            FOR XML PATH('')) cnt(Meter_Number)
        WHERE
            Uti_cha.Account_Type = 'Utility'
            AND Sup_Cha.Account_Type = 'Supplier'
            AND Sup_Cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id
            , Uti_cha.Account_Number
            , Sup_Cha.Account_Id
            , Sup_Cha.Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , Sup_Cha.Supplier_Account_End_Dt
            , CASE WHEN LEN(cnt.Meter_Number) > 1 THEN LEFT(cnt.Meter_Number, LEN(cnt.Meter_Number) - 1)
                  ELSE cnt.Meter_Number
              END
        ORDER BY
            Sup_Cha.Supplier_Account_begin_Dt DESC;

    END;




GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtls_Sel_By_Account_Id_Contract_Id] TO [CBMSApplication]
GO
