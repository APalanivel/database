
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       

NAME:
     dbo.cbmsDivision_GetForSite
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------------------
 @MyAccountId				 INT
 @site_id					 INT
          
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------------------     
          
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------------------     
 
      EXEC cbmsDivision_GetForSite 
            NULL
           ,448
	 
      EXEC cbmsDivision_GetForSite 
            NULL
           ,130
      EXEC cbmsDivision_GetForSite 
            NULL
           ,1501
     
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------------------     
 NR					   Narayana Reddy        

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
--------------------------------------------------------------------------------------     
 NR                   2014-11-12       Addedheader.
									   Added the Site_Reference_Number column in select list.
*********/

CREATE   PROCEDURE [dbo].[cbmsDivision_GetForSite]
      ( 
       @MyAccountId INT
      ,@site_id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            ch.Sitegroup_Id AS division_id
           ,ch.Sitegroup_Name AS division_name
           ,ch.Client_Id AS client_id
           ,ch.Client_Name AS client_name
           ,ch.Site_Reference_Number
      FROM
            core.Client_Hier ch
      WHERE
            ch.site_id = @site_id
      ORDER BY
            division_name

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsDivision_GetForSite] TO [CBMSApplication]
GO
