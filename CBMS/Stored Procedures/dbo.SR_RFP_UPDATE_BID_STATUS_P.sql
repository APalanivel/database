SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_BID_STATUS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                       DataType          Default     Description    
---------------------------------------------------------------------------------    
@accountGroupId                  int,
@isBidGroup                      int,
@supplierContactVendorMapId      int,
@due_date                        datetime
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- Test an update
--EXEC DBO.SR_RFP_UPDATE_BID_STATUS_P
--@accountGroupId int,
--@isBidGroup int,
--@supplierContactVendorMapId int,
--@due_date datetime


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.SR_RFP_UPDATE_BID_STATUS_P

@accountGroupId int,
@isBidGroup int,
@supplierContactVendorMapId int,
@due_date datetime


AS
	
SET NOCOUNT ON

declare @bid_group_status_id int
select @bid_group_status_id = entity_id from entity where entity_type = 1029 and entity_name = 'Refresh'

if(@isBidGroup = 0)
begin
	update sr_rfp_account set bid_status_type_id = @bid_group_status_id
	where sr_rfp_account_id = @accountGroupId
end
else if(@isBidGroup =1)
begin
	update sr_rfp_account set bid_status_type_id = @bid_group_status_id
	where sr_rfp_bid_group_id = @accountGroupId
end

update	sr_rfp_send_supplier set bid_status_type_id = @bid_group_status_id  
where	sr_account_group_id = @accountGroupId
	and is_bid_group = @isBidGroup
	and  sr_rfp_supplier_contact_vendor_map_id = @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_BID_STATUS_P] TO [CBMSApplication]
GO
