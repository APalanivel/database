SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Route_To_User]
           
DESCRIPTION:             
			To assign/route RFP form one user queue to the other user
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@SR_RFP_ID INT
	@Route_To_User_Id	INT
	@User_Info_Id		INT						User who is routing the RFP


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	SELECT TOP 10 * FROM dbo.SR_RFP
	EXEC SR_RFP_GET_ANALYST_NAMES_P 0,0

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_RFP WHERE SR_RFP_ID = 5
		EXEC dbo.Sr_Rfp_Route_To_User 5,53259,49
		SELECT * FROM dbo.SR_RFP WHERE SR_RFP_ID = 5
	ROLLBACK TRANSACTION


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-14		Global Sourcing - Created
								
******/

CREATE PROCEDURE [dbo].[Sr_Rfp_Route_To_User]
      ( 
       @SR_RFP_ID INT
      ,@Route_To_User_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Queue_Id INT
      
      SELECT
            @Queue_Id = QUEUE_ID
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @Route_To_User_Id
      
      UPDATE
            dbo.SR_RFP
      SET   
            Queue_Id = @Queue_Id
           ,Routed_By_User_Id = @User_Info_Id
           ,Routed_Ts = getdate()
      WHERE
            SR_RFP_ID = @SR_RFP_ID
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Route_To_User] TO [CBMSApplication]
GO
