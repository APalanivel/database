
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[SR_RFP_GET_PRODUCT_TERMS_P]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name							DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Selected_Pricing_Product_Id	INT
    @SR_RFP_ACCOUNT_TERM_ID			INT
    @User_Info_Id					INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
EXEC dbo.SR_RFP_GET_PRODUCT_TERMS_P  1,1,12102393,0
EXEC dbo.SR_RFP_GET_PRODUCT_TERMS_P  1,1,10011755,1
EXEC dbo.SR_RFP_GET_PRODUCT_TERMS_P  1,1,10011813,1
EXEC dbo.SR_RFP_GET_PRODUCT_TERMS_P  1,1,12105071,0
		
AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

MODIFICATIONS:
	Initials	Date		Modification
-----------------------------------------------------------------------
	RR			2016-04-01	Global Sourcing - Phase3 - GCS-478 Added Is_Term_Date_Specific column
******/

CREATE   PROCEDURE [dbo].[SR_RFP_GET_PRODUCT_TERMS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@rfpAccountId INT
      ,@bidGroupId INT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            srat.SR_RFP_ACCOUNT_TERM_ID
           ,srpp.SR_RFP_PRICING_PRODUCT_ID PRODUCT_ID
           ,srpp.PRODUCT_NAME
           ,srpp.PRODUCT_DESCRIPTION
           ,srsp.PRODUCT_DETAILS
           ,0 IS_SYSTEM_PRODUCT
           ,srsp.SR_RFP_SELECTED_PRODUCTS_ID
           ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN convert(VARCHAR(12), datepart(DD, srat.FROM_MONTH)) + '-' + substring(datename(month, srat.FROM_MONTH), 1, 3) + '-' + substring(datename(year, srat.FROM_MONTH), 3, 4)
                 WHEN rfp.Is_Term_Date_Specific = 0 THEN substring(datename(month, srat.FROM_MONTH), 1, 3) + '-' + substring(datename(year, srat.FROM_MONTH), 3, 4)
            END AS FROM_MONTH
           ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN convert(VARCHAR(12), datepart(DD, srat.TO_MONTH)) + '-' + substring(datename(month, srat.TO_MONTH), 1, 3) + '-' + substring(datename(year, srat.TO_MONTH), 3, 4)
                 WHEN rfp.Is_Term_Date_Specific = 0 THEN substring(datename(month, srat.TO_MONTH), 1, 3) + '-' + substring(datename(year, srat.TO_MONTH), 3, 4)
            END AS TO_MONTH
           ,rfp.Is_Term_Date_Specific
      FROM
            dbo.SR_RFP_ACCOUNT_TERM srat
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP srtpm
                  ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON srtpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                  ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
            INNER JOIN dbo.SR_RFP rfp
                  ON srpp.SR_RFP_ID = rfp.SR_RFP_ID
      WHERE
            srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
            AND srat.IS_BID_GROUP = @bidGroupId
            AND srsp.IS_SYSTEM_PRODUCT IS NULL
            AND srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NULL
      UNION
      SELECT
            srat.SR_RFP_ACCOUNT_TERM_ID
           ,spp.SR_PRICING_PRODUCT_ID PRODUCT_ID
           ,spp.PRODUCT_NAME
           ,spp.PRODUCT_DESCRIPTION
           ,srsp.PRODUCT_DETAILS
           ,srsp.IS_SYSTEM_PRODUCT
           ,srsp.SR_RFP_SELECTED_PRODUCTS_ID
           ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN convert(VARCHAR(12), datepart(DD, srat.FROM_MONTH)) + '-' + substring(datename(month, srat.FROM_MONTH), 1, 3) + '-' + substring(datename(year, srat.FROM_MONTH), 3, 4)
                 WHEN rfp.Is_Term_Date_Specific = 0 THEN substring(datename(month, srat.FROM_MONTH), 1, 3) + '-' + substring(datename(year, srat.FROM_MONTH), 3, 4)
            END AS FROM_MONTH
           ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN convert(VARCHAR(12), datepart(DD, srat.TO_MONTH)) + '-' + substring(datename(month, srat.TO_MONTH), 1, 3) + '-' + substring(datename(year, srat.TO_MONTH), 3, 4)
                 WHEN rfp.Is_Term_Date_Specific = 0 THEN substring(datename(month, srat.TO_MONTH), 1, 3) + '-' + substring(datename(year, srat.TO_MONTH), 3, 4)
            END AS TO_MONTH
           ,rfp.Is_Term_Date_Specific
      FROM
            dbo.SR_RFP_ACCOUNT_TERM srat
            INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP srtpm
                  ON srat.SR_RFP_ACCOUNT_TERM_ID = srtpm.SR_RFP_ACCOUNT_TERM_ID
            INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                  ON srtpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
            INNER JOIN dbo.SR_RFP rfp
                  ON srsp.SR_RFP_ID = rfp.SR_RFP_ID
            INNER JOIN dbo.SR_PRICING_PRODUCT spp
                  ON srsp.PRICING_PRODUCT_ID = spp.SR_PRICING_PRODUCT_ID
      WHERE
            srat.SR_ACCOUNT_GROUP_ID = @rfpAccountId
            AND srat.IS_BID_GROUP = @bidGroupId
            AND srsp.IS_SYSTEM_PRODUCT = 1
            AND srtpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NULL
      ORDER BY
            srsp.SR_RFP_SELECTED_PRODUCTS_ID

END;

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_PRODUCT_TERMS_P] TO [CBMSApplication]
GO
