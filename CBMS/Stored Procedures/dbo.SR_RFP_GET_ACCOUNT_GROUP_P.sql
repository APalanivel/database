SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ACCOUNT_GROUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	
	@siteId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec SR_RFP_GET_ACCOUNT_GROUP_P 1,10062
CREATE     PROCEDURE dbo.SR_RFP_GET_ACCOUNT_GROUP_P
@rfpId int,
@siteId int

AS
set nocount on
--DECLARE @accountGroupId int
--SELECT @accountGroupId = (
select rfp_account.sr_rfp_account_id	

	from 
		sr_rfp_account rfp_account,	
		site s, 
		account a,
		sr_rfp_lp_account_summary lpAccSummary
		
	where 
		rfp_account.sr_rfp_id = @rfpId	
		and a.account_id = rfp_account.account_id
		and rfp_account.sr_rfp_account_id = lpAccSummary.sr_account_group_id
		and rfp_account.is_deleted  = 0
		and s.site_id = a.site_id
		and s.site_id = @siteId 
--)

--return @accountGroupId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_GROUP_P] TO [CBMSApplication]
GO
