
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Cost_Usage_Site_Dtl_Sel_By_Cost_Usage_Site_Dtl_Id

DESCRIPTION:
	Used to Select the data from Cost_Usage_Site_Dtl table for the given Cost_Usage_Site_Dtl_Id
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Cost_Usage_Site_Dtl_Id	int


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Site_Dtl_Sel_By_Cost_Usage_Site_Dtl_Id 3


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	AP		Athmaram Pabbathi
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	HG        1/27/2010	Created
	AP		3/17/2012 Replaced Site_Id with Client_Hier_Id in the result set
******/

CREATE PROCEDURE [dbo].[Cost_Usage_Site_Dtl_Sel_By_Cost_Usage_Site_Dtl_Id]
      @Cost_Usage_Site_Dtl_Id INT
AS 
BEGIN
      SET NOCOUNT ON

      SELECT
            cusd.Bucket_Master_Id
           ,cusd.Bucket_Value
           ,cusd.UOM_Type_Id
           ,cusd.CURRENCY_UNIT_ID
           ,cusd.Created_By_Id
           ,cusd.Created_Ts
           ,cusd.Updated_Ts
           ,cusd.Client_Hier_Id
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
      WHERE
            cusd.Cost_Usage_Site_Dtl_Id = @Cost_Usage_Site_Dtl_Id
	
END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Sel_By_Cost_Usage_Site_Dtl_Id] TO [CBMSApplication]
GO
