SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	Cu_Invoice_ServiceMonth_Sel_By_Account_ID
DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
   @cu_invoice_id	INT    
   @account_id		INT   
   
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC Cu_Invoice_ServiceMonth_Sel_By_Account_ID 125396,6946
	EXEC Cu_Invoice_ServiceMonth_Sel_By_Account_ID 121441,12004
	EXEC Cu_Invoice_ServiceMonth_Sel_By_Account_ID 2927530,35152

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
MODIFICATIONS
   Initials		Date		Modification
------------------------------------------------------------
	        	02/17/2010  Created
	KS			03/08/2010	Removed the Cu_invoice and Account Map relationship after adding the account column in service Month       
	SSR			03/12/2010	Added table Owner Name
******/

CREATE  PROCEDURE  dbo.Cu_Invoice_ServiceMonth_Sel_By_Account_ID  
 ( @cu_invoice_id INT      
  ,@account_id INT     
 )      
AS      
BEGIN      

	SET NOCOUNT ON;
	
	SELECT   
		cs.cu_invoice_service_month_id      
		, cs.cu_invoice_id      
		, cs.service_month      
		, cs.account_id    
	FROM   
		dbo.cu_invoice_service_month cs
	WHERE  
		cs.cu_invoice_id = @cu_invoice_id      
		AND cs.account_id = @account_id   
		AND cs.SERVICE_MONTH IS NOT NULL   
	ORDER BY   
		cs.service_month   
    
END  
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_ServiceMonth_Sel_By_Account_ID] TO [CBMSApplication]
GO
