SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_ALL_CEM_P
@groupName varchar(200)
AS
begin
SELECT  a.user_info_id,
	a.username,
        a.first_name,
        a.last_name 
FROM    user_info a(nolock) 
	inner join user_info_group_info_map c(nolock) on  c.user_info_id = a.user_info_id
        inner join group_info b(nolock) on b.group_info_id = c.group_info_id
WHERE   b.group_name = @groupName and a.is_history = 0 
ORDER BY a.last_name
end


GO
GRANT EXECUTE ON  [dbo].[GET_ALL_CEM_P] TO [CBMSApplication]
GO
