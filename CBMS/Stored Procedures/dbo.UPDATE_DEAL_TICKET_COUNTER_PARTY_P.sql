SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_DEAL_TICKET_COUNTER_PARTY_P
	@is_manage_bid BIT,
	@rm_deal_ticket_id INT,
	@rm_counterparty_id INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.rm_deal_ticket_counter_party
	SET is_manage_bid = @is_manage_bid
	WHERE rm_deal_ticket_id = @rm_deal_ticket_id
		AND rm_counterparty_id = @rm_counterparty_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_COUNTER_PARTY_P] TO [CBMSApplication]
GO
