SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   
 dbo.RM_Group_Site_Status_Code_Sel
   
 DESCRIPTION:     
   
 This procedure gets all the Supplier name associated to the user.  
   
 INPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
 @Client_Id	INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Group_Site_Status_Code_Sel 
	   
	
  
AUTHOR INITIALS:    
 Initials		Name    
------------------------------------------------------------    
NR				Narayana Reddy
    
 MODIFICATIONS     
 Initials			Date			Modification    
------------------------------------------------------------    
   NR				25-07-2018		Created For Risk managemnet.		


******/

CREATE PROCEDURE [dbo].[RM_Group_Site_Status_Code_Sel] (@Client_ID INT = NULL)
AS
BEGIN

    SET NOCOUNT ON;
    SELECT 'Site Status' AS Filter_Name,
           CASE
               WHEN cd.Code_Value = 'Active' THEN
                   0
               ELSE
                   1
           END AS Filter_Value,
           cd.Code_Value AS Display_Filter_Name
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'Site Status'
          AND cd.Is_Active = 1
    ORDER BY cd.Display_Seq;
END;

GO
GRANT EXECUTE ON  [dbo].[RM_Group_Site_Status_Code_Sel] TO [CBMSApplication]
GO
