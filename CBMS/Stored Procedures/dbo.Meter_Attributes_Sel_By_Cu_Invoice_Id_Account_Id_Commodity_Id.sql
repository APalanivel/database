SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********        
NAME: dbo.Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id          
      
DESCRIPTION:           
      
 To get the meter_attributes based on the cu_invoice_id, account_id,Commodity_Id.      
       
INPUT PARAMETERS:          
Name     DataType  Default Description          
------------------------------------------------------------          
@Cu_Invoice_Id     INT           
@Account_Id     INT            
@Commodity_Id    INT           
      
OUTPUT PARAMETERS:          
Name   DataType  Default Description          
------------------------------------------------------------          
      
USAGE EXAMPLES:          
------------------------------------------------------------          
EXEC Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 75251930,1655579,290,'1227069,1227070,1227071','Billing Days Adjustment'      
EXEC Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 35239980,54239,291,'37193'      
      
  EXEC Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 30948218,685305,291,'473303,473304',"Billing Days Adjustment"     
      
      
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------          
RKV   Ravi Kumar Vegesna     
SP    Srinivas Patchava     
SC	  Sreenivasulu Cheerala       
MODIFICATIONS           
Initials Date    Modification          
------------------------------------------------------------          
 RKV     2015-09-29      Created      
 RKV     2016-11-02      Maint-4317 Added New Parameter @Attribute_Use         
 RKV     2019-05-15      SE2017-742 Added two new fields to the result set     
 SP      2019-09-03      MAINT-9116 Added meter_id Column and order by clause        
 NM   2020-02-11   MAINT-9879 changed left outer join to inner join.        
 SC		 2020-05-15   Added  @Attribute_Type VARCHAR(20) = 'Actual' to get Actual Meter Attributes only.        
******/
CREATE PROCEDURE [dbo].[Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id]
    @Cu_Invoice_Id INT
    , @Account_Id INT
    , @Commodity_Id INT
    , @Meter_Id_List VARCHAR(MAX)
    , @Attribute_Use VARCHAR(25) = 'For Sys'
    , @Attribute_Type VARCHAR(20) = 'Actual'
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Attribute_Use_Cd INT
            , @Attribute_Type_Actual INT;

        SELECT
            @Attribute_Use_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = @Attribute_Use
            AND cs.Codeset_Name = 'Attribute Use';

        SELECT
            @Attribute_Type_Actual = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            c.Code_Value = @Attribute_Type
            AND cs.Codeset_Name = 'MeterAttributeType';
        SELECT
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , MIN(ISNULL(emat.EC_Meter_Attribute_Value, emav.EC_Meter_Attribute_Value)) EC_Meter_Attribute_Value
            , emat.Start_Dt
            , emat.End_Dt
            , cha.Meter_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cha.Account_Id = cism.Account_ID
            INNER JOIN dbo.EC_Meter_Attribute ema
                ON cha.Commodity_Id = ema.Commodity_Id
                   AND  ema.State_Id = cha.Meter_State_Id
            INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON ema.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                   AND  cha.Meter_Id = emat.Meter_Id
                   AND  (   emat.Start_Dt BETWEEN cism.Begin_Dt
                                          AND     cism.End_Dt
                            OR  ISNULL(emat.End_Dt, '2099-01-01') BETWEEN cism.Begin_Dt
                                                                  AND     cism.End_Dt
                            OR  cism.Begin_Dt BETWEEN emat.Start_Dt
                                              AND     ISNULL(emat.End_Dt, '2099-01-01')
                            OR  cism.End_Dt BETWEEN emat.Start_Dt
                                            AND     ISNULL(emat.End_Dt, '2099-01-01'))
            LEFT OUTER JOIN dbo.EC_Meter_Attribute_Value emav
                ON ema.EC_Meter_Attribute_Id = emav.EC_Meter_Attribute_Id
                   AND  Is_Default = 1
        WHERE
            cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Id = @Account_Id
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ema.Attribute_Use_Cd = @Attribute_Use_Cd
            AND emat.Meter_Attribute_Type_Cd = @Attribute_Type_Actual
            AND (cha.Meter_Id IN ( SELECT   Segments FROM   dbo.ufn_split(@Meter_Id_List, ',') ))
        GROUP BY
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , emat.Start_Dt
            , emat.End_Dt
            , cha.Meter_Id
        ORDER BY
            cha.Meter_Id;


    END;

GO

GRANT EXECUTE ON  [dbo].[Meter_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id] TO [CBMSApplication]
GO
