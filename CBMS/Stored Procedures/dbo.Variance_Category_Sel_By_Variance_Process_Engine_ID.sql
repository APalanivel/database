SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
  
/*********        
NAME:  dbo.Variance_Category_Sel_By_Commodity  
         
DESCRIPTION:         
        
INPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
 @Commodity_Id          INT  
 @Variance_Process_Engine_ID INT           
OUTPUT PARAMETERS:            
      Name              DataType          Default     Description            
------------------------------------------------------------            
            
USAGE EXAMPLES:           
        
 EXEC dbo.Variance_Category_Sel_By_Commodity 291 
 EXEC dbo.Variance_Category_Sel_By_Commodity 67,2 
        
------------------------------------------------------------          
AUTHOR INITIALS:          
Initials Name          
------------------------------------------------------------          
AKR      Ashok Kumar Raju    
AP  Arunkumar Palanivel
NM  Nagaraju Muppa  
        
        
Initials Date Modification          
------------------------------------------------------------          
 AKR     2013-05-24 Created      
 AP  Jan 10 ,2020 Modified for Advanced Variance Testing,
 NM  Jan-13-2020 Modified for uncomment @Variance_Process_Engine_ID parameter  
******/        
        
CREATE PROCEDURE [dbo].[Variance_Category_Sel_By_Variance_Process_Engine_ID] 
(	
	@Variance_Process_Engine_ID INT   
)  
AS   
BEGIN           
      SET NOCOUNT ON ;  
       
      SELECT  
            vp.Variance_Category_Cd  
           ,c.Code_Value Variance_Category_Name  
      FROM  
            dbo.Variance_Parameter_Commodity_Map vpcm  
            INNER JOIN dbo.Variance_Parameter vp  
                  ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id  
            INNER JOIN dbo.Code c  
                  ON c.Code_Id = vp.Variance_Category_Cd  
				  WHERE c.Code_Value not IN ('Billed Demand', 'BTU Factor (CV)', 'Demand', 'Hauls', 'Off Peak Ratio', 'Power Factor')
      --WHERE  
      --     vpcm.variance_process_Engine_id = @Variance_Process_Engine_ID  
      GROUP BY  
            vp.Variance_Category_Cd  
           ,c.Code_Value  
      ORDER BY  
            c.Code_Value   
END;  
GO
GRANT EXECUTE ON  [dbo].[Variance_Category_Sel_By_Variance_Process_Engine_ID] TO [CBMSApplication]
GO
