SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select * from budget_account where budget_id=336
--select * from budget_details where budget_account_id=2712

-- exec BUDGET_CREATE_REVISED_BUDGET_DETAILS_P -1,-1,2711,2712

CREATE        PROCEDURE dbo.BUDGET_CREATE_REVISED_BUDGET_DETAILS_P
@user_id varchar(10),
@session_id varchar(20),
@originalBudgetId int,
@createdBudgetId int


AS
begin
set nocount on

	insert into budget_details
            (BUDGET_ACCOUNT_ID,
            MONTH_IDENTIFIER,
            VARIABLE,
            TRANSPORTATION,
            TRANSMISSION,
            DISTRIBUTION,
            OTHER_BUNDLED,
            OTHER_FIXED_COSTS,
            SOURCING_TAX,
            RATES_TAX,
            BUDGET_USAGE,
            BUDGET_USAGE_TYPE_ID,
            NYMEX_FORECAST,
	    VARIABLE_VALUE,
	    TRANSPORTATION_VALUE,
	    TRANSMISSION_VALUE,
	    DISTRIBUTION_VALUE,
	    OTHER_BUNDLED_VALUE,
	    OTHER_FIXED_COSTS_VALUE,
	    SOURCING_TAX_VALUE,
	    RATES_TAX_VALUE,
	    IS_MANUAL_GENERATION,
	    IS_MANUAL_SOURCING_TAX	
	   )
	
    select 
	   (select budget_account_id from budget_account where budget_id = @createdBudgetId and account_id = b_account.account_id) as budget_account_id,
           MONTH_IDENTIFIER,
           VARIABLE,
           TRANSPORTATION,
           TRANSMISSION,
           DISTRIBUTION,
           OTHER_BUNDLED,
           OTHER_FIXED_COSTS,
           SOURCING_TAX,
           RATES_TAX,
           BUDGET_USAGE,
           BUDGET_USAGE_TYPE_ID,
           NYMEX_FORECAST,
	   VARIABLE_VALUE,
    	   TRANSPORTATION_VALUE,
           TRANSMISSION_VALUE,
    	   DISTRIBUTION_VALUE,
    	   OTHER_BUNDLED_VALUE,
   	   OTHER_FIXED_COSTS_VALUE,
           SOURCING_TAX_VALUE,
   	   RATES_TAX_VALUE,
	   IS_MANUAL_GENERATION,
	   IS_MANUAL_SOURCING_TAX	

   from    budget_details details,
	   budget_account b_account
   where   b_account.budget_id = @originalBudgetId
	   and details.budget_account_id = b_account.budget_account_id
	   and b_account.is_deleted = 0

end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_REVISED_BUDGET_DETAILS_P] TO [CBMSApplication]
GO
