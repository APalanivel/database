SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Contract_Defaults_Del]  

DESCRIPTION: It Deletes Budget Contract Defaults for Selected Budget_Contract_Defaults_Id.     
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Budget_Contract_Defaults_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
		EXEC Budget_Contract_Defaults_Del 5271
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Contract_Defaults_Del
    (
      @Budget_Contract_Defaults_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.BUDGET_CONTRACT_DEFAULTS
	WHERE
		Budget_Contract_Defaults_Id = @Budget_Contract_Defaults_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Contract_Defaults_Del] TO [CBMSApplication]
GO
