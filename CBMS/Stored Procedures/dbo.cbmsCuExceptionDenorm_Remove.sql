
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[cbmsCuExceptionDenorm_Remove]  
     
DESCRIPTION: 


INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@cu_invoice_id		INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	EXEC cbmsCuExceptionDenorm_Remove 1545300
	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
SKA		Shobhit Kumar Agrawal

MODIFICATION
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
SKA			10/21/2010	Created
RKV			2015-10-30 added two optional parameters account_id and commodity_Id as part of AS400-PII

*/


CREATE PROCEDURE [dbo].[cbmsCuExceptionDenorm_Remove]
      ( 
       @cu_invoice_id INT
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT = NULL )
AS 
BEGIN  
  
      SET nocount ON  
  
      DELETE
            cu_exception_denorm
      WHERE
            cu_invoice_id = @cu_invoice_id
            AND ( @Account_Id IS NULL
                  OR Account_ID = @Account_Id )
            AND ( @Commodity_Id IS NULL
                  OR Commodity_Id = @Commodity_Id ) 
    
      EXEC cbmsCuExceptionDenorm_Get 
            @cu_invoice_id  
  
END  
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDenorm_Remove] TO [CBMSApplication]
GO
