SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec GET_PRICING_POINTS_P 320

CREATE   PROCEDURE dbo.GET_PRICING_POINTS_P
	@indexID int
	AS
	begin
	set nocount on
		select 	price_index_id, 
			index_id, 
			entity_name, 
			pricing_point,
			index_description 
		from	price_index 
			join entity on entity_id = index_id
			and index_id = @indexID

	end
GO
GRANT EXECUTE ON  [dbo].[GET_PRICING_POINTS_P] TO [CBMSApplication]
GO
