SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.GET_CURRENCY_ID_FOR_INDEX_PRICE_P
@priceIndexId int

as
	set nocount on
	select	priceindex.CURRENCY_UNIT_ID,currencyunit.CURRENCY_UNIT_NAME, priceindex.VOLUME_UNIT_ID 
	FROM 
	
	PRICE_INDEX priceindex , CURRENCY_UNIT currencyunit
	
	where currencyunit.CURRENCY_UNIT_ID= priceindex.CURRENCY_UNIT_ID AND
	priceindex.PRICE_INDEX_ID=@priceIndexId
GO
GRANT EXECUTE ON  [dbo].[GET_CURRENCY_ID_FOR_INDEX_PRICE_P] TO [CBMSApplication]
GO
