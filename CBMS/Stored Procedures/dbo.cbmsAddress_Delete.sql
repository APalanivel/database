SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:	dbo.[cbmsAddress_Delete]

DESCRIPTION: 
	
	Used to delete the address of site, in the following scenarios application will call this procedure
	
		- If this address is not the only address of the site
		- If this address is not the primary address of the site
		- If this address is not associated with any of the meter.

INPUT PARAMETERS:
    Name                 DataType          Default     Description
------------------------------------------------------------------
	@addressId           INT
	@User_Info_Id		 INT

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
	   EXEC CbmsAddress_Delete 9358,49
	   SELECT * FROM Application_audit_log where table_name = 'Address'

	ROLLBACK TRAN

	SELECT * FROM dbo.address a 
	where not exists(select 1 from meter m where m.address_id = a.address_id)
		and not exists(select 1 from site s where s.primary_Address_id = a.address_id)
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	HG			Harihara Suthan G
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR		    08/04/2009	Removed Linked Server Updates
	HG			08/20/2010	Unused @MyAccountId, @is_sv_rpl parameters removed.
	PNR			09/01/2010	Modified Script as per delete tool standards , added script to log the deleted record in to Application_Audit_Log table.
							Removed the call to procedure cbmsAddress_Get at the end of the procedure as this result was not used by the application.

******/

CREATE PROCEDURE dbo.cbmsAddress_Delete
	(
		 @addressId		INT
		,@User_Info_Id	INT
	)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @Application_Name		VARCHAR(30) = 'Delete Address'
			,@Audit_Function		SMALLINT = -1
			,@User_Name				VARCHAR(81)
			,@Client_Name			VARCHAR(200)
			,@Current_Ts			DATETIME
			,@Table_Name			VARCHAR(10) = 'ADDRESS'
			,@Lookup_Value			XML
			,@siteid				INT

	SET @Lookup_Value =	(	SELECT
								 ADDRESS.ADDRESS_ID
								,ADDRESS.ADDRESS_LINE1
								,STATE.STATE_NAME
								,ADDRESS.ZIPCODE
								,Address.CITY
								,Site.SITE_ID
								,Site.SITE_NAME
							FROM
								dbo.Address
								JOIN dbo.Site
									 ON SITE.SITE_ID = ADDRESS.ADDRESS_PARENT_ID
								JOIN dbo.STATE
									 ON STATE.STATE_ID = ADDRESS.STATE_ID
								WHERE
									ADDRESS.ADDRESS_ID = @addressId
							FOR XML RAW
						)
	SELECT
		@Client_Name = Client_Name
	FROM
		dbo.Address ad
		JOIN dbo.Site s
			 ON s.SITE_ID = Ad.Address_Parent_Id
		JOIN dbo.Client cl
			 ON cl.CLIENT_ID = s.Client_ID
	WHERE
		ad.ADDRESS_ID = @addressId

	SELECT
		@User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
	FROM
		dbo.USER_INFO
	WHERE
		USER_INFO_ID = @User_Info_Id

	EXEC dbo.Address_Del @addressId

	SET @Current_Ts = GETDATE()

	EXEC dbo.Application_Audit_Log_Ins	 @Client_Name
										,@Application_Name
										,@Audit_Function
										,@Table_Name
										,@Lookup_Value
										,@User_Name
										,@Current_Ts

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAddress_Delete] TO [CBMSApplication]
GO
