SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsPricePointValue_Remove]
	( @MyAccountId int
	, @price_point_value_id int
	)
AS
BEGIN

	set nocount on

	   delete price_index_value
	    where price_index_value_id = @price_point_value_id

--	set nocount off

	exec cbmsPricePointValue_Get @MyAccountId, @price_point_value_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPricePointValue_Remove] TO [CBMSApplication]
GO
