SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_UPDATE_RC_DOCUMENT_XML_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@rateComparisonId	int       	          	
	@rcDocumentXml 	text      	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.RC_UPDATE_RC_DOCUMENT_XML_P 

@userId varchar(20),
@sessionId varchar(20),
@rateComparisonId int,
@rcDocumentXml text


AS
set nocount on
	update RC_RATE_COMPARISON
	
	set RATE_COMPARISON_DOCUMENT_XML=@rcDocumentXml
	
	where RC_RATE_COMPARISON_ID=@rateComparisonId
GO
GRANT EXECUTE ON  [dbo].[RC_UPDATE_RC_DOCUMENT_XML_P] TO [CBMSApplication]
GO
