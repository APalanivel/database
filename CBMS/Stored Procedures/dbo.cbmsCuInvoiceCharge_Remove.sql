SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.cbmsCuInvoiceCharge_Remove

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@cu_invoice_charge_id	int       	          	
	@account_id				INT 
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
[USE CBMS_TK3]

	EXEC cbmsCuInvoiceCharge_Remove 4676

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NK			Nageswara Rao Kosuri
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	 HG			03/29/2010	Removed unused parameter @myaccountid from this procedure
							and removed the calling procedure cbmsCuInvoiceDeterminant_Get as it wont return any data.	        	
	 SSR		04/23/2010	Modified to incorporate deleting rows from cu_invoice_charge_map table	
	 SSR		04/28/2010	Modified Merge Statement Matched Condition
	 SSR		05/25/2010	Removed Merge and used simple delete statement with same conditions
	 SKA		05/26/2010	Removed the Select Count condition and used joins for that.
	 SKA		06/09/2010	Modified the where condition cu_invoice_charge_Account
******/
CREATE  PROCEDURE dbo.cbmsCuInvoiceCharge_Remove
      (
       @cu_invoice_charge_id INT
      ,@account_id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON ;

      BEGIN TRY
           BEGIN TRAN			
			  DELETE
					cica
			  FROM
					dbo.cu_invoice_charge_Account cica
			  WHERE
					cica.CU_INVOICE_CHARGE_ID = @cu_invoice_charge_id
					AND ( ( @account_id IS NULL )
						  OR ( @account_id > 0
							   AND cica.ACCOUNT_ID = @account_id ) )


			  DELETE
					cic
			  FROM
					cu_invoice_charge cic
					LEFT OUTER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica ON cica.cu_invoice_charge_id = cic.cu_invoice_charge_id
			  WHERE
					cic.cu_invoice_charge_id = @cu_invoice_charge_id
					AND cica.CU_INVOICE_CHARGE_ACCOUNT_ID IS NULL
		COMMIT TRAN  
      END TRY
	
      BEGIN CATCH
            ROLLBACK TRAN
            EXEC usp_RethrowError	
      END CATCH		   
END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceCharge_Remove] TO [CBMSApplication]
GO
