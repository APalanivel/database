SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME: [Workflow].[Workflow_Queue_Search_Commodity]       
DESCRIPTION:  Used to get EP, NG and other services commodities for client service map.        
        
INPUT PARAMETERS:          
Name      DataType  Default  Description  
@Is_Active INT = 1    
@keyWord_Search VARCHAR(MAX) = NULL 
@Start_Index INT = 1    
@End_Index INT = 2147483647    
     
------------------------------------------------------------          
               
OUTPUT PARAMETERS:          
Name      DataType  Default  Description          
------------------------------------------------------------          
        
USAGE EXAMPLES:          
------------------------------------------------------------        
        
EXEC [Workflow].[Workflow_Queue_Search_Commodity]       
    @Is_Active = 1      
 ,@keyWord_Search ='101'      
    , @Start_Index = 1      
    , @End_Index = 10000000      
      
 EXEC [Workflow].[Workflow_Queue_Search_Commodity]       
    @Is_Active = 1      
    , @Start_Index = 1      
    , @End_Index = 25      
      
       
 EXEC [Workflow].[Workflow_Queue_Search_Commodity]       
    @Is_Active = 1      
    , @Start_Index = 25      
    , @End_Index = 50      
      
  EXEC [Workflow].[Workflow_Queue_Search_Commodity]       
    @Is_Active = 1      
    , @Start_Index = 1      
    , @End_Index = 50      
        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
HG   Hari        
SP   Sandeep Pigilam        
        
MODIFICATIONS        
Initials Date  Modification        
------------------------------------------------------------        
HG   03/25/2010 Created        
SP   2015-08-07  Data Source Management Phase II,Added Is_Client_Specific in order by clause after Is_Alternate_Fuel column.        
        
******/    
 
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_Commodity]    
    (      
          @Is_Active INT = 1      
        , @keyWord_Search VARCHAR(MAX) = NULL      
        , @Start_Index INT = 1      
        , @End_Index INT = 2147483647      
    )      
AS      
    BEGIN      
      
        SET NOCOUNT ON;      
      
        ;WITH cte_Comm      
        AS (      
               SELECT      
                    Commodity_Id      
                    , Commodity_Name      
                    , COUNT(1) AS Total_Rows      
                    , ROW_NUMBER() OVER (ORDER BY      
                                             CASE WHEN c.Commodity_Name = 'Electric Power' THEN 1      
                                                 WHEN c.Commodity_Name = 'Natural Gas' THEN 2      
                                                 ELSE 3      
                                             END) AS Row_Num      
               FROM      
                    dbo.Commodity c      
               WHERE      
                    Is_Active = @Is_Active      
                    AND (   @keyWord_Search IS NULL      
                            OR  c.Commodity_Name LIKE '%' + @keyWord_Search + '%')      
               GROUP BY      
                   Commodity_Id      
                   , Commodity_Name      
           )      
        SELECT      
            cc.Commodity_Id      
            , cc.Commodity_Name      
            , cc.Total_Rows      
            , cc.Row_Num      
        FROM      
            cte_Comm cc      
        WHERE      
            cc.Row_Num BETWEEN @Start_Index      
                       AND     @End_Index      
        ORDER BY      
            cc.Row_Num;      
      
      
    END;     
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_Commodity] TO [CBMSApplication]
GO
