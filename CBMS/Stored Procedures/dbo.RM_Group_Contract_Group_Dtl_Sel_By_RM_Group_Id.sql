SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.RM_Group_Contract_Group_Dtl_Sel_By_RM_Group_Id                       
                            
 DESCRIPTION:        
    To get the manage Risk Management Groups.  
  
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------  
 @Client_Id       INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------  
                            
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------  
            
    EXEC  dbo.RM_Group_Contract_Group_Dtl_Sel_By_RM_Group_Id  485  
  
  
    EXEC  dbo.RM_Group_Contract_Group_Dtl_Sel_By_RM_Group_Id  280  
  
    EXEC dbo.RM_Group_Sel_By_Client_Id 235  
  
  
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------  
 NR                     2018-07-19      Created for GRM-51.                          
                           
******/


CREATE PROCEDURE [dbo].[RM_Group_Contract_Group_Dtl_Sel_By_RM_Group_Id]
    (
        @Cbms_Sitegroup_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        IF OBJECT_ID('tempdb.[dbo].[#Temp_Contract_Dtl]') IS NOT NULL
            DROP TABLE [#Temp_Contract_Dtl];


        CREATE TABLE dbo.[#Temp_Contract_Dtl]
             (
                 [CONTRACT_ID] INT
                 , [ED_CONTRACT_NUMBER] VARCHAR(150)
                 , [CONTRACT_START_DATE] DATETIME
                 , [CONTRACT_END_DATE] DATETIME
                 , [Account_Number] NVARCHAR(500)
                 , [Alternate_Account_Number] NVARCHAR(200)
                 , [Meter_Number] VARCHAR(500)
                 , [Site_name] VARCHAR(200)
                 , [City] VARCHAR(200)
                 , [Account_Vendor_Name] VARCHAR(200)
                 , [State_Id] INT
                 , [State_Name] VARCHAR(200)
                 , [Anaylist] VARCHAR(200)
                 , [Supplier] VARCHAR(200)
                 , [Include_New_Contract_Extensions] BIT
                 , [Site_Id] INT
                 , [Sitegroup_Id] INT
                 , [Client_Id] INT
                 , [Account_Id] INT
             );


        IF OBJECT_ID('tempdb.[dbo].[#Final_Temp_Contract_Dtl]') IS NOT NULL
            DROP TABLE [#Final_Temp_Contract_Dtl];

        CREATE TABLE dbo.[#Final_Temp_Contract_Dtl]
             (
                 [CONTRACT_ID] INT
                 , [ED_CONTRACT_NUMBER] VARCHAR(150)
                 , [CONTRACT_START_DATE] DATETIME
                 , [CONTRACT_END_DATE] DATETIME
                 , [Site_name] VARCHAR(MAX)
                 , [Account_Number] NVARCHAR(MAX)
                 , [Alternate_Account_Number] NVARCHAR(MAX)
                 , [Meter_Number] VARCHAR(MAX)
                 , [Cities] VARCHAR(MAX)
                 , [Account_Vendor_Name] VARCHAR(MAX)
                 , Distributor VARCHAR(MAX)
                 , [State_Id] VARCHAR(MAX)
                 , [State] VARCHAR(MAX)
                 , [Anaylist] VARCHAR(200)
                 , [Supplier] VARCHAR(200)
                 , [Include_New_Contract_Extensions] BIT
                 , Row_No INT
                 , [Site_Id] INT
                 , [Sitegroup_Id] INT
                 , [Client_Id] INT
                 , [Account_Id] INT
             );





        DECLARE
            @Default_Analyst INT
            , @Custom_Analyst INT
            , @Total INT;


        DECLARE @Contract_Dtl_Tbl AS TABLE
              (
                  CONTRACT_ID INT
                  , Site_name VARCHAR(200)
                  , Account_Number VARCHAR(50)
                  , Alternate_Account_Number NVARCHAR(200)
                  , Meter_Number VARCHAR(50)
                  , City VARCHAR(200)
                  , State_Name VARCHAR(200)
                  , Utility_Account_Vendor_Name VARCHAR(200)
                  , Anaylist VARCHAR(100)
                  , Supplier_Account_Vendor_Name VARCHAR(200)
              );

        SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
            , @Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                    END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Analyst Type';

        INSERT INTO [#Temp_Contract_Dtl]
             (
                 [CONTRACT_ID]
                 , [ED_CONTRACT_NUMBER]
                 , [CONTRACT_START_DATE]
                 , [CONTRACT_END_DATE]
                 , [Account_Number]
                 , [Alternate_Account_Number]
                 , [Meter_Number]
                 , [Site_name]
                 , [City]
                 , [Account_Vendor_Name]
                 , [State_Id]
                 , [State_Name]
                 , [Anaylist]
                 , [Supplier]
                 , [Include_New_Contract_Extensions]
                 , [Site_Id]
                 , [Sitegroup_Id]
                 , [Client_Id]
                 , [Account_Id]
             )
        SELECT
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , chautil.Account_Number
            , chautil.Alternate_Account_Number
            , chasupp.Meter_Number
            , ch.Site_name
            , ch.City
            , chautil.Account_Vendor_Name
            , ch.State_Id
            , ch.State_Name
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Anaylist
            , chasupp.Account_Vendor_Name AS Supplier
            , rgd.Include_New_Contract_Extensions
            , ch.Site_Id
            , ch.Sitegroup_Id
            , ch.Client_Id
            , chautil.Account_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
            INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                ON vcm.VENDOR_ID = chautil.Account_Vendor_Id
                   AND  vcm.COMMODITY_TYPE_ID = c.COMMODITY_TYPE_ID
            LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
            INNER JOIN Core.Client_Commodity ccc
                ON ccc.Client_Id = ch.Client_Id
                   AND  ccc.Commodity_Id = chautil.Commodity_Id
            LEFT JOIN dbo.Account_Commodity_Analyst aca
                ON aca.Account_Id = chautil.Account_Id
                   AND  aca.Commodity_Id = chautil.Commodity_Id
            LEFT JOIN dbo.Site_Commodity_Analyst sca
                ON sca.Site_Id = ch.Site_Id
                   AND  sca.Commodity_Id = chautil.Commodity_Id
            LEFT JOIN dbo.Client_Commodity_Analyst cca
                ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = CASE WHEN COALESCE(
                                                   chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd
                                                   , ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN
                                              COALESCE(
                                                  aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id
                                                  , cca.Analyst_User_Info_Id)
                                         ELSE vcam.Analyst_Id
                                     END
        WHERE
            rgd.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
            AND cd.Code_Value = 'Contract'
            AND chasupp.Account_Type = 'Supplier'
            AND chautil.Account_Type = 'Utility'
        GROUP BY
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , ch.State_Id
            , ch.State_Name
            , chautil.Account_Number
            , chautil.Alternate_Account_Number
            , chasupp.Meter_Number
            , ch.Site_name
            , ch.City
            , chautil.Account_Vendor_Name
            , chasupp.Account_Vendor_Name
            , rgd.Include_New_Contract_Extensions
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
            , ch.Site_Id
            , ch.Sitegroup_Id
            , ch.Client_Id
            , chautil.Account_Id;

        INSERT INTO [#Final_Temp_Contract_Dtl]
             (
                 [CONTRACT_ID]
                 , [ED_CONTRACT_NUMBER]
                 , [CONTRACT_START_DATE]
                 , [CONTRACT_END_DATE]
                 , [Site_name]
                 , [Account_Number]
                 , [Alternate_Account_Number]
                 , [Meter_Number]
                 , Cities
                 , [State_Id]
                 , [State]
                 , [Account_Vendor_Name]
                 , Distributor
                 , [Anaylist]
                 , [Supplier]
                 , [Include_New_Contract_Extensions]
                 , Row_No
                 , Site_Id
                 , Sitegroup_Id
                 , Client_Id
                 , Account_Id
             )
        SELECT  DISTINCT
                T1.CONTRACT_ID
                , T1.ED_CONTRACT_NUMBER
                , T1.CONTRACT_START_DATE
                , T1.CONTRACT_END_DATE
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Site_name
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Site_name
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Account_Number
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Account_Number
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Alternate_Account_Number
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Alternate_Account_Number
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Meter_Number
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Meter_Number
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.City
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Cities
                , STUFF((   SELECT  DISTINCT
                                    '| ' + CAST(T2.State_Id AS VARCHAR(10))
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS State_Id
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.State_Name
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS State
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Account_Vendor_Name
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Account_Vendor_Name
                , STUFF((   SELECT  DISTINCT
                                    '| ' + T2.Account_Vendor_Name
                            FROM
                                [#Temp_Contract_Dtl] T2
                            WHERE
                                T1.CONTRACT_ID = T2.CONTRACT_ID
                            FOR XML PATH('')), 1, 1, '') AS Distributor
                , T1.Anaylist
                , T1.Supplier
                , T1.Include_New_Contract_Extensions
                , DENSE_RANK() OVER (ORDER BY
                                         T1.CONTRACT_ID) AS Row_No
                , T1.Site_Id
                , T1.Sitegroup_Id
                , T1.Client_Id
                , T1.Account_Id
        FROM
            [#Temp_Contract_Dtl] T1
        GROUP BY
            T1.CONTRACT_ID
            , T1.ED_CONTRACT_NUMBER
            , T1.CONTRACT_START_DATE
            , T1.CONTRACT_END_DATE
            , T1.Account_Vendor_Name
            , T1.Supplier
            , T1.Include_New_Contract_Extensions
            , T1.Anaylist
            , T1.Site_Id
            , T1.Sitegroup_Id
            , T1.Client_Id
            , T1.Account_Id;

        SELECT  @Total = MAX(Row_No)FROM    [#Final_Temp_Contract_Dtl];

        SELECT
            cte.CONTRACT_ID
            , cte.ED_CONTRACT_NUMBER
            , cte.CONTRACT_START_DATE
            , cte.CONTRACT_END_DATE
            , cte.Site_name
            , cte.Account_Number
            , cte.Alternate_Account_Number
            , cte.Meter_Number
            , cte.Cities
            , cte.State_Id
            , cte.State
            , cte.Account_Vendor_Name
            , cte.Distributor
            , cte.Anaylist
            , cte.Supplier
            , cte.Include_New_Contract_Extensions
            , cte.Row_No
            , @Total AS Total
            , cte.Site_Id
            , cte.Sitegroup_Id
            , cte.Client_Id
            , cte.Account_Id
        FROM
            [#Final_Temp_Contract_Dtl] cte
        GROUP BY
            cte.CONTRACT_ID
            , cte.ED_CONTRACT_NUMBER
            , cte.CONTRACT_START_DATE
            , cte.CONTRACT_END_DATE
            , cte.Site_name
            , cte.Account_Number
            , cte.Alternate_Account_Number
            , cte.Meter_Number
            , cte.Cities
            , cte.State_Id
            , cte.State
            , cte.Account_Vendor_Name
            , cte.Distributor
            , cte.Anaylist
            , cte.Supplier
            , cte.Include_New_Contract_Extensions
            , cte.Row_No
            , cte.Site_Id
            , cte.Sitegroup_Id
            , cte.Client_Id
            , cte.Account_Id;


    END;









GO
GRANT EXECUTE ON  [dbo].[RM_Group_Contract_Group_Dtl_Sel_By_RM_Group_Id] TO [CBMSApplication]
GO
