SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_SUPPLIER_CONTACT_MAP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	
	@contactId     	int       	          	
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_CHECK_SUPPLIER_CONTACT_MAP_P
@rfpId int,
@contactId int,
@vendorId int

as
set nocount on
DECLARE @cnt int
SELECT @cnt = (
SELECT	COUNT(*) 
FROM		SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
WHERE	SR_RFP_ID = @rfpId
		AND SR_SUPPLIER_CONTACT_INFO_ID = @contactId
		AND VENDOR_ID = @vendorId
)

RETURN  @cnt
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_SUPPLIER_CONTACT_MAP_P] TO [CBMSApplication]
GO
