SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.SELECT_RATE_SCHEDULES_P
	@rateId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT rate_schedule_id,
		rs_start_date,
		rs_end_date
	FROM dbo.rate_schedule
	WHERE rate_id = @rateId
	ORDER BY rs_start_date

END
GO
GRANT EXECUTE ON  [dbo].[SELECT_RATE_SCHEDULES_P] TO [CBMSApplication]
GO
