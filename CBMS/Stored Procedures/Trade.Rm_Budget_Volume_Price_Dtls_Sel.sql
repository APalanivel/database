SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Volume_Price_Dtls_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Volume_Price_Dtls_Sel  1005
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-02-24	GRM-1725 Applied number conversion on hedged and un-hedged to remove decimal points	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Volume_Price_Dtls_Sel]
    (
        @Rm_Budget_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            rb.Rm_Budget_Id
            , rb.Budget_NAME
            , ch.Client_Hier_Id
            , ch.Client_Name AS Client_Name
            , ch.Sitegroup_Name AS Division_RM_Group_Name
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
            , dd.DATE_D AS Budget_Month
            , CAST(rbvpd.Forecast_Volume AS DECIMAL(22, 0)) AS Forecasted_Volume
            , CAST(rbvpd.Total_Volume_Hedged AS DECIMAL(22, 0)) AS Total_Volume_Hedged
            , rbvpd.WACOG_Hedge_Price AS WACOG_Of_Hedges
            , CAST((rbvpd.Forecast_Volume - ISNULL(rbvpd.Total_Volume_Hedged, 0)) AS DECIMAL(22, 0)) AS Total_Volume_Unhedged
            , CAST(rbvpd.Budget_Target_Price AS DECIMAL(22, 3)) AS Budget_Target_Price
        FROM
            Trade.Rm_Budget rb
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN Trade.Rm_Budget_Volume_Price_Dtl rbvpd
                ON rbvpd.Rm_Budget_Id = rb.Rm_Budget_Id
                   AND  dd.DATE_D = rbvpd.Service_Month
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = rbvpd.Client_Hier_Id
                   AND  ch.Client_Id = rb.Client_Id
        WHERE
            rb.Rm_Budget_Id = @Rm_Budget_Id
            AND dd.DATE_D BETWEEN rb.Budget_Start_Dt
                          AND     rb.Budget_End_Dt;
    END;





GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Volume_Price_Dtls_Sel] TO [CBMSApplication]
GO
