SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME: dbo.Group_Info_Category_Sel
  
 DESCRIPTION: It will display the group_info_categories for selected App_Menu_Profile_Id...
  
 INPUT PARAMETERS:
 Name					DataType			Default     Description      
------------------------------------------------------------------      
 @App_Menu_Profile_Id	INT

 OUTPUT PARAMETERS:      
 Name	DataType          Default     Description      
 -----------------------------------------------------------      
  
 USAGE EXAMPLES:
 ------------------------------------------------------------  

	EXEC dbo.Group_Info_Category_Sel 4
	EXEC dbo.Group_Info_Category_Sel 2
  
AUTHOR INITIALS:
Initials Name
------------------------------------------------------------  
PNR		 Pandarinath
  
MODIFICATIONS  
Initials	Date		  Modification  
------------------------------------------------------------  
PNR			03/31/2011	  Created as part of dv2 groups Revamp..
  
******/  

CREATE PROCEDURE dbo.Group_Info_Category_Sel
( 
	@App_Menu_Profile_Id INT
)
AS
BEGIN
    
    SET NOCOUNT ON;

    SELECT
		  Group_Info_Category_Id
         ,Category_Name
         ,Sort_Order
         ,App_Menu_Profile_Id
	FROM
		dbo.GROUP_INFO_CATEGORY
	WHERE
		App_Menu_Profile_Id = @App_Menu_Profile_Id
	ORDER BY
		Sort_Order ASC
END
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Category_Sel] TO [CBMSApplication]
GO
