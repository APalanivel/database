
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******          

NAME: [DBO].[cbmsInvoiceParticipationSite_Save3]  
     
DESCRIPTION:

	To merge the data in to Invoice_Participation_Site table

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@MyAccountId	INT
@site_id		INT
@service_month	DATETIME

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	SELECT * FROM INVOICE_PARTICIPATION_SITE WHERE SIte_id = 98121 AND SERVICE_MONTH = '2010-01-01'

		EXEC dbo.cbmsInvoiceParticipationSite_Save31 -1,98181,'2010-01-01'

	SELECT * FROM INVOICE_PARTICIPATION_SITE WHERE SIte_id = 98121 AND SERVICE_MONTH = '2010-01-01'
	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2012-03-27	Added comments header
						MAINT-1194, Fixed the issue of * _Expected_Count / *_Received_Count columns multiplied with the number of meters if it mapped with the same commodity.
							 - Changed the insert/update in to MERGE statement
							 - Removed the EXISTS check on Invoice_Participation table.
							 - Hardcoded commodity id replaced by prepopulating the values in a scalar variable.
 HG			2013-01-28	CAST the @Service_Month to date as it is createing duplicate data whenever app pass the value with time stamp
***/

CREATE PROCEDURE [dbo].[cbmsInvoiceParticipationSite_Save3]
      (
       @MyAccountId INT
      ,@site_id INT
      ,@service_month DATETIME )
AS
BEGIN
	
      SET NOCOUNT ON

      DECLARE
            @EL_Commodity_Id INT
           ,@NG_COmmodity_Id INT

      SELECT
            @EL_Commodity_Id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                   END)
           ,@NG_COmmodity_Id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                   END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' );

		SET @Service_Month = CAST(@Service_Month AS DATE);
		
      WITH  Cte_Ip_Site_Aggregation
              AS ( SELECT
                        expected_count = isnull(max(y.ng_expected_count) + max(y.el_expected_count), 0)
                       ,received_count = isnull(max(y.ng_received_count) + max(y.el_received_count), 0)
                       ,is_complete = isnull(case WHEN max(y.ng_is_complete) = 1
                                                       AND max(y.el_is_complete) = 1 THEN 1
                                                  ELSE 0
                                             END, 0)
                       ,is_published = isnull(case WHEN max(y.ng_is_published) = 1
                                                        OR max(y.el_is_published) = 1 THEN 1
                                                   ELSE 0
                                              END, 0)
                       ,under_review = isnull(case WHEN max(y.ng_under_review) = 1
                                                        OR max(y.el_under_review) = 1 THEN 1
                                                   ELSE 0
                                              END, 0)
                       ,ng_expected_count = isnull(max(y.ng_expected_count), 0)
                       ,ng_received_count = isnull(max(y.ng_received_count), 0)
                       ,ng_is_complete = isnull(max(y.ng_is_complete), 0)
                       ,ng_is_published = isnull(max(y.ng_is_published), 0)
                       ,ng_under_review = isnull(max(y.ng_under_review), 0)
                       ,el_expected_count = isnull(max(y.el_expected_count), 0)
                       ,el_received_count = isnull(max(y.el_received_count), 0)
 ,el_is_complete = isnull(max(y.el_is_complete), 0)
                       ,el_is_published = isnull(max(y.el_is_published), 0)
                       ,el_under_review = isnull(max(y.el_under_review), 0)
                   FROM
                        ( SELECT
                              x.site_id
                             ,x.service_month
                             ,0 expected_count
                             ,0 received_count
                             ,0 is_complete
                             ,0 is_published
                             ,0 under_review
                             ,sum(case WHEN x.is_expected = 1 THEN 1
                                       ELSE 0
                                  END) ng_expected_count
                             ,sum(case WHEN x.is_expected = 1
                                            AND x.is_received = 1 THEN 1
                                       ELSE 0
                                  END) ng_received_count
                             ,sum(case WHEN x.is_received = 1 THEN 1
                                       ELSE 0
                                  END) ng_actual_received_count
                             ,case WHEN sum(case WHEN x.is_expected = 1 THEN 1
                                                 ELSE 0
                                            END) = sum(case WHEN x.is_expected = 1
                                                                 AND x.is_received = 1 THEN 1
                                                            ELSE 0
                                                       END) THEN 1
                                   ELSE 0
                              END ng_is_complete
                             ,case WHEN sum(case WHEN x.is_received = 1 THEN 1
                                                 ELSE 0
                                            END) > 0 THEN 1
                                   ELSE 0
                              END ng_is_published
                             ,isnull(max(case WHEN x.recalc_under_review = 1 THEN 1
                                              ELSE x.variance_under_review
                                         END), 0) ng_under_review
                             ,0 el_expected_count
                             ,0 el_received_count
                             ,0 el_actual_received_count
                             ,0 el_is_complete
                             ,0 el_is_published
                             ,0 el_under_review
                          FROM
                              dbo.Invoice_participation x
                          WHERE
                              x.site_id = @site_id
                              AND x.service_month = @service_month
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                core.Client_Hier ch
                                                JOIN core.Client_Hier_Account cha
                                                      ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                           WHERE
                                                ch.Site_Id = x.Site_Id
                                                AND cha.Account_Id = x.Account_Id
                                                AND cha.Commodity_Id = @NG_Commodity_Id )
                          GROUP BY
                              x.site_id
                             ,x.service_month
                          UNION ALL
                          SELECT
                              x.site_id
                             ,x.service_month
                             ,0 expected_count
                             ,0 received_count
                             ,0 is_complete
                             ,0 is_published
                             ,0 under_review
                             ,0 ng_expected_count
                             ,0 ng_received_count
                             ,0 ng_actual_received_count
                             ,0 ng_is_complete
                             ,0 ng_is_published
                             ,0 ng_under_review
                             ,sum(case WHEN x.is_expected = 1 THEN 1
                                       ELSE 0
                                  END) el_expected_count
                             ,sum(case WHEN x.is_expected = 1
                                            AND x.is_received = 1 THEN 1
                                       ELSE 0
                                  END) el_received_count
                             ,sum(case WHEN x.is_received = 1 THEN 1
                                       ELSE 0
                                  END) el_actual_received_count
                             ,case WHEN sum(case WHEN x.is_expected = 1 THEN 1
                                                 ELSE 0
                                            END) = sum(case WHEN x.is_expected = 1
                                                                 AND x.is_received = 1 THEN 1
                                                            ELSE 0
                                                       END) THEN 1
                                   ELSE 0
                              END el_is_complete
                             ,case WHEN sum(case WHEN x.is_received = 1 THEN 1
                                                 ELSE 0
                                            END) > 0 THEN 1
                                   ELSE 0
                              END el_is_published
                             ,isnull(max(case WHEN x.recalc_under_review = 1 THEN 1
                                              ELSE x.variance_under_review
                                         END), 0) el_under_review
                          FROM
                              dbo.INVOICE_PARTICIPATION x
                          WHERE
                              x.site_id = @site_id
                              AND x.service_month = @service_month
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                core.Client_Hier ch
                                                JOIN core.Client_Hier_Account cha
                                                      ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                           WHERE
                                                ch.Site_Id = x.Site_Id
                                                AND cha.Account_Id = x.Account_Id
                                                AND cha.Commodity_Id = @EL_Commodity_Id )
                          GROUP BY
                              x.site_id
                             ,x.service_month ) y)
            MERGE INTO dbo.INVOICE_PARTICIPATION_SITE tgt
                  USING 
                        ( SELECT
                             expected_count
                             ,received_count
                             ,is_complete
                             ,is_published
                             ,under_review
                             ,ng_expected_count
                             ,ng_received_count
                             ,ng_is_complete
                             ,ng_is_published
                             ,ng_under_review
                             ,el_expected_count
                             ,el_received_count
                             ,el_is_complete
                             ,el_is_published
                             ,el_under_review
                             ,@Site_Id AS Site_id
                             ,@Service_Month AS Service_Month
         FROM
                              Cte_IP_Site_Aggregation ) src
                  ON src.Site_Id = tgt.SITE_ID
                        AND src.Service_Month = tgt.Service_Month
                  WHEN NOT MATCHED 
                        THEN
                  INSERT
                              ( 
                               site_id
                              ,service_month
                              ,expected_count
                              ,received_count
                              ,is_complete
                              ,is_published
                              ,under_review
                              ,ng_expected_count
                              ,ng_received_count
                              ,ng_is_complete
                              ,ng_is_published
                              ,ng_under_review
                              ,el_expected_count
                              ,el_received_count
                              ,el_is_complete
                              ,el_is_published
                              ,el_under_review )
                         VALUES
                              ( 
                               src.site_id
                              ,src.service_month
                              ,src.expected_count
                              ,src.received_count
                              ,src.is_complete
                              ,src.is_published
                              ,src.under_review
                              ,src.ng_expected_count
                              ,src.ng_received_count
                              ,src.ng_is_complete
                              ,src.ng_is_published
                              ,src.ng_under_review
                              ,src.el_expected_count
                              ,src.el_received_count
                              ,src.el_is_complete
                              ,src.el_is_published
                              ,src.el_under_review )
                  WHEN MATCHED 
                        THEN
                  UPDATE SET  
                              expected_count = src.expected_count
                             ,received_count = src.received_count
                             ,is_complete = src.is_complete
                             ,is_published = src.is_published
                             ,under_review = src.under_review
                             ,ng_expected_count = src.ng_expected_count
                             ,ng_received_count = src.ng_received_count
                             ,ng_is_complete = src.ng_is_complete
                             ,ng_is_published = src.ng_is_published
                             ,ng_under_review = src.ng_under_review
                             ,el_expected_count = src.el_expected_count
                             ,el_received_count = src.el_received_count
                             ,el_is_complete = src.el_is_complete
                             ,el_is_published = src.el_is_published
                             ,el_under_review = src.el_under_review ;
END

;
GO


GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipationSite_Save3] TO [CBMSApplication]
GO
