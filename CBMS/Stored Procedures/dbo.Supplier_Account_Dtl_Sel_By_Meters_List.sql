SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Supplier_Account_Dtl_Sel_By_Meters_List

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
USE CBMS

EXEC Supplier_Account_Dtl_Sel_By_Meters_List
    @Meter_Id = '1227203'

EXEC Supplier_Account_Dtl_Sel_By_Meters_List
    @Meter_Id = '1227204'

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR				2019-06-26		Created For Add contract.   

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtl_Sel_By_Meters_List]
    (
        @Meter_Id VARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            cha.Account_Type
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
			,cha.Account_Vendor_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND EXISTS (   SELECT
                                1
                           FROM
                                Core.Client_Hier_Account cha2
                                INNER JOIN dbo.ufn_split(@Meter_Id, ',') us
                                    ON us.Segments = cha2.Meter_Id
                                       AND  cha2.Account_Id = cha.Account_Id
                                       AND  cha2.Client_Hier_Id = cha.Client_Hier_Id
                                       AND  cha2.Account_Type = 'Supplier')
        GROUP BY
 cha.Account_Type
            , cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
			,cha.Account_Vendor_Id
        ORDER BY
            cha.Account_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtl_Sel_By_Meters_List] TO [CBMSApplication]
GO
