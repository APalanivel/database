SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************         
NAME: dbo.Client_CEM_Map_Sel_Email_Id_By_ClientId
    
DESCRIPTION:    
    
     This procedure will be used to get the Email Id's of CEM's and CSA's for the ClientId provided.
     
    
INPUT PARAMETERS:    
      NAME					DATATYPE				DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	
    @ClientId		         INT	
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	EXEC dbo.Client_CEM_Map_Sel_Email_Id_By_ClientId 109
       
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
      RMG         Rani Mary George
              
MODIFICATIONS:    
      INITIALS    DATE           MODIFICATION              
------------------------------------------------------------              
      RMG 		  2013-03-21	 CREATED
******************************************************************************************************/  


CREATE PROCEDURE dbo.Client_CEM_Map_Sel_Email_Id_By_ClientId 
( 
@ClientId INT 
)
AS 
BEGIN
 
      SET NOCOUNT ON ;
      
      
      SELECT
            ui.EMAIL_ADDRESS AS CEMEmailAddress
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.CLIENT_CEM_MAP cem
                  ON ui.USER_INFO_ID = cem.USER_INFO_ID
      WHERE
            cem.CLIENT_ID = @ClientId
            AND ui.IS_HISTORY = 0
           
					
      SELECT
            ui.EMAIL_ADDRESS AS CSAEmailAddress
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.CLIENT_CSA_MAP csa
                  ON ui.USER_INFO_ID = csa.USER_INFO_ID
      WHERE
            csa.CLIENT_ID = @ClientId
            AND ui.IS_HISTORY = 0
			
        
  
					
					
END



;
GO
GRANT EXECUTE ON  [dbo].[Client_CEM_Map_Sel_Email_Id_By_ClientId] TO [CBMSApplication]
GO
