SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********   
NAME:  dbo.Master_Variance_Test_SEL_Parameter_By_Category  
 
DESCRIPTION:  Used to select Parameter value from variance_parameter table using the category Id  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Category_Cd			Int   
        
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	Master_Variance_Test_SEL_Parameter_By_Category 100177
	Master_Variance_Test_SEL_Parameter_By_Category 100121
	

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/12/2009  Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Master_Variance_Test_SEL_Parameter_By_Category]
    @Category_Cd INT
    , @variance_process_Engine_id INT = NULL
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            vp.Variance_Parameter_Id
            , vp.Parameter
        FROM
            dbo.Variance_Parameter vp
            INNER JOIN dbo.Variance_Parameter_Commodity_Map vpcm
                ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
        WHERE
            Variance_Category_Cd = @Category_Cd
            --AND (   @variance_process_Engine_id IS NULL
            --        OR  vpcm.variance_process_Engine_id = @variance_process_Engine_id)
        GROUP BY
            vp.Variance_Parameter_Id
            , vp.Parameter;
    END;
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_SEL_Parameter_By_Category] TO [CBMSApplication]
GO
