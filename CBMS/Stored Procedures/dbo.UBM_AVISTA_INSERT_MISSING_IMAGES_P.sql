SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE       PROCEDURE [dbo].[UBM_AVISTA_INSERT_MISSING_IMAGES_P]
AS 
   set nocount on
   DECLARE @ubm_batch_master_log_id int
    , @cbms_image_id int
    , @invoice_identifier varchar(50)
    , @ubm_invoice_id int

-- exec master.dbo.xp_smtp_sendmail @FROM = N'summitheartdbas@summit.com',
--     @FROM_NAME   = N'It got called',
--     @TO          = N'mmccallon@summitenergy.com', 
--     @subject     = N'ubm_avista_insert_missing_images_p', @message ='',@type = N'text/plain',
--     @server     = N'evs.ses.int'
-- 
-- exec master..xp_sendmail @recipients='mmccallon@summitenergy.com',@message='',
-- @subject='avista insert missing images was called'


   DECLARE C_MISSING_IMAGE_INV CURSOR FAST_FORWARD
      FOR select  ubm_batch_master_log_id
          from    ubm_batch_master_log
          where   ubm_id = ( select ubm_id
                             from   ubm
                             where  ubm_name = 'Avista'
                           )
                  and status_type_id = ( select entity_id
                                         from   entity
                                         where  entity_type = 656
                                                and entity_name = 'Missing Images'
                                       )

   OPEN C_MISSING_IMAGE_INV
	
   FETCH NEXT FROM C_MISSING_IMAGE_INV INTO @ubm_batch_master_log_id
   WHILE ( @@fetch_status <> -1 )
      BEGIN
         IF ( @@fetch_status <> -2 ) 
            BEGIN

               DECLARE C_MISSING_IMAGE_INV_DETAILS CURSOR FAST_FORWARD
                  FOR select  ubm_invoice_id
                            , invoice_identifier
                      from    ubm_invoice
                      where   ubm_batch_master_log_id = @ubm_batch_master_log_id
                              and cbms_image_id is null	
	
               OPEN C_MISSING_IMAGE_INV_DETAILS
	
               FETCH NEXT FROM C_MISSING_IMAGE_INV_DETAILS
                  INTO @ubm_invoice_id, @invoice_identifier 
		 

               WHILE ( @@fetch_status <> -1 )
                  BEGIN
                     IF ( @@fetch_status <> -2 ) 
                        BEGIN
					--print '@bar_code =  '+@invoice_identifier

                           select   @cbms_image_id = MIN(cbms_image_id)
                           from     cbms_image
                           where    cbms_doc_id in (
                                    @invoice_identifier
                                    + '.tif',
                                    @invoice_identifier
                                    + '.pdf',
                                    @invoice_identifier
                                    + '.html',
                                    @invoice_identifier
                                    + '.htm',
                                    @invoice_identifier
                                    + '.jpg' )
					--print '@cbms_image_id =  '+str(@cbms_image_id)

                           update   ubm_invoice
                           set      cbms_image_id = @cbms_image_id
                           where    ubm_invoice_id = @ubm_invoice_id 
                        END
                     FETCH NEXT FROM
                        C_MISSING_IMAGE_INV_DETAILS INTO @ubm_invoice_id,
                        @invoice_identifier 
                  END
               CLOSE C_MISSING_IMAGE_INV_DETAILS
               DEALLOCATE C_MISSING_IMAGE_INV_DETAILS
		
               IF ( select count(ubm_invoice_id)
                    from   ubm_invoice
                    where  ubm_batch_master_log_id = @ubm_batch_master_log_id
                           and cbms_image_id is null
                  ) = 0 
                  BEGIN
			--print 'Status is successful '	
                     update   ubm_batch_master_log
                     set      status_type_id = ( select  entity_id
                                                 from    entity
                                                 where   entity_type = 656
                                                         and entity_name = 'Success'
                                               )
                     where    ubm_batch_master_log_id = @ubm_batch_master_log_id
                  END	

            END
         FETCH NEXT FROM C_MISSING_IMAGE_INV INTO @ubm_batch_master_log_id
      END
   CLOSE C_MISSING_IMAGE_INV
   DEALLOCATE C_MISSING_IMAGE_INV



GO
GRANT EXECUTE ON  [dbo].[UBM_AVISTA_INSERT_MISSING_IMAGES_P] TO [CBMSApplication]
GO
