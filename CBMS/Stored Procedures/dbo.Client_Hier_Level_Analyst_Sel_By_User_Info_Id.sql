SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:            
      dbo.Client_Hier_Level_Analyst_Sel_By_User_Info_Id          
             
 DESCRIPTION:             
      Gets the Analyst for a given Client,Division,Site,Account.          
                
 INPUT PARAMETERS:            
 Name   DataType Default Description            
----------------------------------------------------------            
 @Client_Id      INT               
 @Sitegroup_Id   INT   NULL          
 @Site_Id   INT   NULL                          
 @Commodity_Id   INT          
           
 OUTPUT PARAMETERS:          
 Name   DataType  Default Description          
----------------------------------------------------------          
 USAGE EXAMPLES:            
----------------------------------------------------------          
         
 EXEC dbo.Client_Hier_Level_Analyst_Sel_By_User_Info_Id 26957,NULL,1,100          
 EXEC dbo.Client_Hier_Level_Analyst_Sel_By_User_Info_Id 26957,NULL,100000,100100          
          
AUTHOR INITIALS:          
Initials Name          
----------------------------------------------------------           
 AKR  Ashok Kumar Raju          
           
          
 MODIFICATIONS             
 Initials Date  Modification          
----------------------------------------------------------          
 AKR        2012-09-26  Created for POCO          
          
******/          
          
CREATE  PROCEDURE dbo.Client_Hier_Level_Analyst_Sel_By_User_Info_Id
      ( 
       @Analyst_User_Info_Id INT
      ,@Level VARCHAR(10) = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN          
      SET NOCOUNT ON ;          
                       
 ;          
      WITH  cte_test
              AS ( SELECT
                        LEVEL
                       ,CLIENT_NAME
                       ,Site
                       ,Account
                       ,row_number() OVER ( ORDER BY order_clm, CLIENT_NAME, [Site], Account ) AS rownum
                       ,count(1) OVER ( ) AS Total_Row_Count
                   FROM
                        ( SELECT
                              'Client' [Level]
                             ,c.CLIENT_NAME
                             ,NULL [Site]
                             ,NULL Account
                             ,1 order_clm
                          FROM
                              dbo.Client_Commodity_Analyst cca
                              INNER JOIN core.Client_Commodity cc
                                    ON cca.Client_Commodity_Id = cc.Client_Commodity_Id
                              INNER JOIN dbo.CLIENT c
                                    ON cc.Client_Id = c.CLIENT_ID
                          WHERE
                              cca.Analyst_User_Info_Id = @Analyst_User_Info_Id
                              AND ( @Level IS NULL
                                    OR @Level = 'Client' )
                          GROUP BY
                              c.CLIENT_NAME
                          UNION ALL
                          SELECT
                              'Site' [Level]
                             ,ch.CLIENT_NAME
                             ,ch.Site_name [Site]
                             ,NULL Account
                             ,2 order_clm
                          FROM
                              core.Client_Hier ch
                              INNER JOIN core.Client_Commodity cc
                                    ON ch.Client_Id = cc.Client_Id
                              LEFT JOIN dbo.Client_Commodity_Analyst cca
                                    ON cc.Client_Commodity_Id = cca.Client_Commodity_Id
                              LEFT JOIN dbo.Site_Commodity_Analyst sca
                                    ON sca.Site_Id = ch.Site_Id
                                       AND sca.Commodity_Id = cc.Commodity_Id
                          WHERE
                              ch.site_Id > 0
                              AND ( sca.Analyst_User_Info_Id = @Analyst_User_Info_Id
                                    OR ( sca.Analyst_User_Info_Id IS NULL
                                         AND cca.Analyst_User_Info_Id = @Analyst_User_Info_Id ) )
                              AND ( @Level IS NULL
                                    OR @Level = 'Site' )
                          GROUP BY
                              ch.CLIENT_NAME
                             ,ch.Site_name
                          UNION ALL
                          SELECT
                              'Account' [Level]
                             ,ch.CLIENT_NAME
                             ,ch.Site_name [Site]
                             ,cha.Account_Number Account
                             ,3 order_clm
                          FROM
                              core.Client_Hier ch
                              INNER JOIN core.Client_Commodity cc
                                    ON ch.Client_Id = cc.Client_Id
                              INNER JOIN core.Client_Hier_Account cha
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                                       AND cha.Commodity_Id = cc.Commodity_Id
                              LEFT JOIN dbo.Client_Commodity_Analyst cca
                                    ON cc.Client_Commodity_Id = cca.Client_Commodity_Id
                              LEFT JOIN dbo.Site_Commodity_Analyst sca
                                    ON sca.Site_Id = ch.Site_Id
                                       AND sca.Commodity_Id = cc.Commodity_Id
                              LEFT JOIN dbo.Account_Commodity_Analyst aca
                                    ON aca.Account_Id = cha.Account_Id
                                       AND aca.Commodity_Id = cha.Commodity_Id
                          WHERE
                              ch.site_Id > 0
                              AND ( aca.Analyst_User_Info_Id = @Analyst_User_Info_Id
                                    OR ( aca.Analyst_User_Info_Id IS NULL
                                         AND sca.Analyst_User_Info_Id = @Analyst_User_Info_Id )
                                    OR ( aca.Analyst_User_Info_Id IS NULL
                                         AND sca.Analyst_User_Info_Id IS NULL
                                         AND cca.Analyst_User_Info_Id = @Analyst_User_Info_Id ) )
                              AND ( @Level IS NULL
                                    OR @Level = 'Account' )
                              AND cha.Account_Type = 'Utility'
                          GROUP BY
                              ch.CLIENT_NAME
                             ,ch.Site_name
                             ,cha.Account_Number ) k)
            SELECT
                  ct.LEVEL
                 ,ct.CLIENT_NAME
                 ,ct.Site
                 ,ct.Account
                 ,ct.Total_Row_Count
            FROM
                  cte_test ct
            WHERE
                  ct.rownum BETWEEN @Start_Index AND @End_Index           
                            
       
END     
;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_Level_Analyst_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
