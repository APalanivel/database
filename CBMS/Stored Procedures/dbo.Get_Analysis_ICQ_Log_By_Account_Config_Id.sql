SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   dbo.[Get_Analysis_ICQ_Log_By_Account_Config_Id]  
             
DESCRIPTION:               
     
     
INPUT PARAMETERS:              
 Name      DataType Default  Description    
---------------------------------------------------------------------------------    
  
OUTPUT PARAMETERS:  
 Name        DataType  Default  Description    
---------------------------------------------------------------------------------    
  
 USAGE EXAMPLES:  
---------------------------------------------------------------------------------    
  
  EXEC [Get_Analysis_ICQ_Log_By_Account_Config_Id]  
   @Invoice_Collection_Account_Config_id = 553  
  
 AUTHOR INITIALS:              
 Initials Name              
-------------------------------------------------------------              
 RKV        Ravi Kumar Vegesna  
  
 MODIFICATIONS:  
 Initials Date  Modification  
------------------------------------------------------------  
 RKV   2020-01-14 IC   
******/
CREATE PROCEDURE [dbo].[Get_Analysis_ICQ_Log_By_Account_Config_Id]
     (
         @Invoice_Collection_Account_Config_id INT
         , @Invoice_Collection_Queue_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS UserName
            , ic.Invoice_Collection_Queue_Id
            , ic.ICQ_Log_message
            , ic.Created_Ts AS Logged_Date
        FROM
            LOGDB.dbo.ICQ_log ic
            JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ic.USER_INFO_ID
        WHERE
            ic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_id
            AND (   @Invoice_Collection_Queue_Id IS NULL
                    OR  ic.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id)
        ORDER BY
            ic.Created_Ts;

    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Analysis_ICQ_Log_By_Account_Config_Id] TO [CBMSApplication]
GO
