SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
NAME:   dbo.cbmsBudgets_GetAllYearsForClientAndCommodity      
                 
DESCRIPTION:                   
 Return the budget data.      
                 
INPUT PARAMETERS:                  
Name					DataType		Default		 Description        
-----------------------------------------------------------------        
myaccountid				INT				
client_id				INT
commodity_type_id       INT
      
 OUTPUT PARAMETERS:                  
Name					DataType		Default		 Description        
-----------------------------------------------------------------        
        
               
 USAGE EXAMPLES:                  
-----------------------------------------------------------------        



EXEC cbmsBudgets_GetAllYearsForClientAndCommodity @client_id = 10036  , @commodity_type_id=291

      
 AUTHOR INITIALS:                  
 Initials	Name                  
-------------------------------------------------------------                  
 RKV		Ravi kumar Vegesna 
 
       
 MODIFICATIONS:                   
 Initials	Date		Modification                  
------------------------------------------------------------                  
  RKV		2018-09-20  Maint-7746,Added filter  IS_POSTED_TO_DV = 1     
 
******/

CREATE PROCEDURE [dbo].[cbmsBudgets_GetAllYearsForClientAndCommodity]
    (
        @myaccountid INT = NULL
        , @client_id INT = NULL
        , @commodity_type_id INT = NULL
    )
AS
    BEGIN

        SELECT
            BUDGET_ID
            , CLIENT_ID
            , BUDGET_NAME budget_title
            , DATE_INITIATED date_created
            , BUDGET_START_YEAR
            , BUDGET_START_MONTH
            , POSTED_BY user_info_id
            , COMMODITY_TYPE_ID
            , CBMS_IMAGE_ID
            , (CONVERT(VARCHAR(10), BUDGET_DATE, 101)) AS report_date
            , BUDGET_DATE AS full_report_date
        FROM
            dbo.BUDGET
        WHERE
            CLIENT_ID = @client_id
            AND COMMODITY_TYPE_ID = @commodity_type_id
            AND IS_POSTED_TO_DV = 1
        ORDER BY
            budget_title DESC;

    END;
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_GetAllYearsForClientAndCommodity] TO [CBMSApplication]
GO
