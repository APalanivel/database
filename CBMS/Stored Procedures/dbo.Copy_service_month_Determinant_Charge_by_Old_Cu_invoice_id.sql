SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Copy_service_month_Determinant_Charge_by_Old_Cu_invoice_id

DESCRIPTION:
	This SP is to Copy All Records from below tables of a given cu_invoice_id with New Cu_invoice_id
Tables
	Cu_invoice_service_month,cu_invoice_determinant,cu_invoice_determinant_account,cu_invoice_Charge,cu_invoice_charge_account,CU_INVOICE_COMMENT
INPUT PARAMETERS:
	Name			   DataType		Default	Description
------------------------------------------------------------	     	          	
	@Old_cu_invoice_id 	INT       	          	
    @New_cu_invoice_id 	INT  
    @Is_Account			BIT     	          	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN
	EXEC dbo.Copy_service_month_Determinant_Charge_by_Old_Cu_invoice_id 2928422,2928461,0
ROLLBACK TRAN	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SSR        	04/12/2010	Created
	SKA			05/04/2010	Removed the cursor from the script
	SSR			05/21/2010	Added Order By clause for Insertion 
	SKA		    05/27/2010	Use Error Handling because of multipile insert statement

******/

CREATE PROC [dbo].[Copy_service_month_Determinant_Charge_by_Old_Cu_invoice_id]
      (
       @Old_cu_invoice_id INT
      ,@New_cu_invoice_id INT
      ,@Is_Account BIT )
AS 
BEGIN

      SET NOCOUNT ON ;
      BEGIN TRY
            BEGIN TRAN
--Deleteing and then inserting the record based on Old & New cu_invoice_id
            DELETE
                  CU_INVOICE_SERVICE_MONTH
            WHERE
                  CU_INVOICE_ID = @New_cu_invoice_id
		
            INSERT INTO
                  CU_INVOICE_SERVICE_MONTH
                  (
                   CU_INVOICE_ID
                  ,Account_ID )
                  SELECT
                        @New_cu_invoice_id
                       ,Account_id
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
                        AND @Is_Account = 1
                  GROUP BY
                        Account_id 

--Inserting a record into CU_INVOICE_DETERMINANT based on old cu_invoice_id		
            INSERT INTO
                  CU_INVOICE_DETERMINANT
                  (
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,UBM_METER_NUMBER
                  ,UBM_SERVICE_TYPE_ID
                  ,UBM_SERVICE_CODE
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,UNIT_OF_MEASURE_TYPE_ID
                  ,UBM_UNIT_OF_MEASURE_CODE
                  ,DETERMINANT_NAME
                  ,DETERMINANT_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_NO
                  ,CU_DETERMINANT_CODE )
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_METER_NUMBER
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,UNIT_OF_MEASURE_TYPE_ID
                       ,UBM_UNIT_OF_MEASURE_CODE
                       ,DETERMINANT_NAME
                       ,DETERMINANT_VALUE
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_NO
                       ,CU_DETERMINANT_CODE
                  FROM
                        dbo.CU_INVOICE_DETERMINANT
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
                  GROUP BY
                        cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_METER_NUMBER
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,UNIT_OF_MEASURE_TYPE_ID
                       ,UBM_UNIT_OF_MEASURE_CODE
                       ,DETERMINANT_NAME
                       ,DETERMINANT_VALUE
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_NO
                       ,CU_DETERMINANT_CODE
                  ORDER BY
                        CU_DETERMINANT_NO	
			
	

--Inserting a record into CU_INVOICE_DETERMINANT_ACCOUNT based on old cu_invoice_id
            INSERT INTO
                  CU_INVOICE_DETERMINANT_ACCOUNT
                  (
                   CU_INVOICE_DETERMINANT_ID
                  ,ACCOUNT_ID
                  ,Determinant_Expression
                  ,Determinant_Value )
                  SELECT
                        new_inv.CU_INVOICE_DETERMINANT_ID
                       ,CASE WHEN @Is_Account = 0 THEN NULL
                             ELSE old_det.ACCOUNT_ID
                        END
                       ,old_det.Determinant_Expression
                       ,old_det.Determinant_Value
                  FROM
                        dbo.CU_INVOICE_DETERMINANT_ACCOUNT old_det
                        JOIN dbo.CU_INVOICE_DETERMINANT old_inv ON old_det.CU_INVOICE_DETERMINANT_ID = old_inv.CU_INVOICE_DETERMINANT_ID
                                                                   AND old_inv.CU_INVOICE_ID = @Old_cu_invoice_id
                        JOIN dbo.CU_INVOICE_DETERMINANT new_inv ON new_inv.CU_DETERMINANT_CODE = old_inv.CU_DETERMINANT_CODE
                                                                   AND new_inv.CU_INVOICE_ID = @new_cu_invoice_id
                  Order BY
                        old_inv.CU_INVOICE_DETERMINANT_ID

--Inserting a record into CU_INVOICE_CHARGE based on old cu_invoice_id
            INSERT INTO
                  CU_INVOICE_CHARGE
                  (
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,UBM_SERVICE_TYPE_ID
                  ,UBM_SERVICE_CODE
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,CHARGE_NAME
                  ,CHARGE_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_CODE )
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_SERVICE_TYPE_ID
                       ,UBM_SERVICE_CODE
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,CHARGE_NAME
                       ,CHARGE_VALUE
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_CODE
                  FROM
                        dbo.CU_INVOICE_CHARGE
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
                  ORDER BY
                        CU_INVOICE_CHARGE_ID

--Inserting a record into CU_INVOICE_CHARGE_ACCOUNT based on old cu_invoice_id
            INSERT INTO
                  CU_INVOICE_CHARGE_ACCOUNT
                  (
                   CU_INVOICE_CHARGE_ID
                  ,ACCOUNT_ID
                  ,Charge_Expression
                  ,Charge_Value )
                  SELECT
                        new_inv.CU_INVOICE_CHARGE_ID
                       ,CASE WHEN @Is_Account = 0 THEN NULL
                             ELSE old_chg.ACCOUNT_ID
                        END
                       ,old_chg.Charge_Expression
                       ,old_chg.Charge_Value
                  FROM
                        dbo.CU_INVOICE_CHARGE_ACCOUNT old_chg
                        JOIN dbo.CU_INVOICE_CHARGE old_inv ON old_chg.CU_INVOICE_CHARGE_ID = old_inv.CU_INVOICE_CHARGE_ID
                                                              AND old_inv.CU_INVOICE_ID = @Old_cu_invoice_id
                        JOIN dbo.CU_INVOICE_CHARGE new_inv ON new_inv.CU_DETERMINANT_CODE = old_inv.CU_DETERMINANT_CODE
                                                              AND new_inv.CU_INVOICE_ID = @new_cu_invoice_id
                  ORDER BY
                        old_inv.CU_INVOICE_CHARGE_ID
		
            INSERT INTO
                  CU_INVOICE_COMMENT
                  (
                   CU_INVOICE_ID
                  ,COMMENT_BY_ID
                  ,COMMENT_DATE
                  ,IMAGE_COMMENT )
                  SELECT
                        @New_cu_invoice_id
                       ,COMMENT_BY_ID
                       ,COMMENT_DATE
                       ,IMAGE_COMMENT
                  FROM
                        dbo.CU_INVOICE_COMMENT
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id
            COMMIT TRAN  
      END TRY
	
      BEGIN CATCH
            ROLLBACK TRAN
            EXEC usp_RethrowError	
      END CATCH		                  
	
END

GO
GRANT EXECUTE ON  [dbo].[Copy_service_month_Determinant_Charge_by_Old_Cu_invoice_id] TO [CBMSApplication]
GO
