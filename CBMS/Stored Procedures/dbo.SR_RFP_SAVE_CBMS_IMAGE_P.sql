SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_SAVE_CBMS_IMAGE_P]
	@User_Id VARCHAR(10),
	@Session_Id VARCHAR(20),
	@Image_Type VARCHAR(200),
	@CbmsImageId INT  --added by Jaya
	--@cbms_doc_id varchar(200),
	--@cbms_image image,
	--@content_type varchar(200)
AS
BEGIN

	DECLARE @Image_Type_Id INT
	
	SELECT @Image_Type_Id = Entity_id 
	FROM dbo.Entity (NOLOCK) 
	WHERE Entity_name = @Image_Type 
		AND Entity_type = 100
		
	/* commented by Jaya 
	insert into cbms_image (cbms_image_type_id, cbms_doc_id, cbms_image, date_imaged, content_type)
			values (@Image_Type_Id, @cbms_doc_id, @cbms_image, getdate(), @content_type)	 */
			
	--added by Jaya for updating entityid
     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @Image_Type_Id WHERE Cbms_Image_Id = @cbmsImageId
  	
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_CBMS_IMAGE_P] TO [CBMSApplication]
GO
