SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  [dbo].[cbmsQueueItem_GetCount]  
 
DESCRIPTION:  

Private Queue  (User queue count)
 
	Incoming Private Count                -  Count  of DISTINCT INVOICE_ID when Is_Manual = 1 in Cu_Invoice
 
	Exception Private Count               -  Count of DISTINCT INVOICE ID for exception type <>  "Failed Recalc" 
														and Is_Manual = 0 in Cu_Invoice 
                                             Count of DISTINCT INVOICE_ID, Account_id for exception type = "Failed Recalc" 
														and Is_Manual = 0 in Cu_Invoice 

Public Queue ( User group queue count)
 
	Incoming_public_count                -  Count of DISTINCT INVOICE_ID when Is_Manual = 1 in Cu_Invoice
 
	Exception Public Count               -  Count of DISTINCT INVOICE_ID when Is_Manual = 0 in Cu_Invoice

Here we are assuming that by the time a ?Failed Recalc? could arise in an Invoice it is typically in the private queue 
(of some user), and not in the public queue. Typically, public queue cases would not reach to the point of recalc test being executed;
	

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	 @MyAccountId		Int
	 
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
USAGE EXAMPLES:  

	EXEC cbmsQueueItem_GetCount 8522
	EXEC cbmsQueueItem_GetCount 122 
	EXEC cbmsQueueItem_GetCount 49
	EXEC cbmsQueueItem_GetCount 49
	EXEC dbo.cbmsQueueItem_GetCount 18653
	EXEC dbo.cbmsQueueItem_GetCount 78
	
	Exec cbmsQueueItem_GetCount 71929

	exec Account_Exception_Sel 71929
	
------------------------------------------------------------  
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
SKA			Shobhit Kumar Agrawal
HG			Harihara Suthan G
SP			Sandeep Pigilam
NR			Narayana Reddy

MODIFICATION
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
SKA			10/21/2010	Created
SKA			12/7/2010	modified the logic for exception_private_count based on Reclac (Bug#21882)
SKA			12/14/2010	Modified the condition for @exception_public_count
HG			12/14/2010	Removed the Binary check sum function which used to get DISTINCT count of invoices based on Invoice_id, Accountid for Failed Recalc exception type.
						Logic changed to save the cu_exception denorm values in a table variable then select the data from table variable for each condition for private count.
						Updated the comments header with the logic to get the counts
SP			2014-05-22	Data Operations Enhancement 4.2.2 ,Data Operations Enhancement Phase II ,Seperated Failed Recalc Count 
						from Exception_private_count and incoming_private_Count and showed it seperatly as Recalc_Count	
NR			2015-05-08	AS400 added count of Standing Data Queue .
HG			2016-05-12	Recalc queue count logic changed to consider the commodity as recalc exceptions are per commodity now.
NR			2016-12-22	MAINT-4618 - In variance count added commodity_Id in group clause because variance page the data is 
						filtering by Account ID, Service Month & Commodity to find total records (count) in the application.
						and also join core.Client_Hier ch table in variance Count.	
RKV			2017-02-02	Added Invoice_Collection Queue,@Invoice_Collection_Exception_Queue_Cnt Count As part of Invoice Collection.
						added @Is_Missing_IC_Included as optional param.		
******/
CREATE PROCEDURE [dbo].[cbmsQueueItem_GetCount]
     (
         @MyAccountId INT
         , @Is_Missing_IC_Included INT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @queue_id INT
            , @Incoming_Private_Count INT
            , @Recalc_Exception_Private_Count INT
            , @Other_Exception_Private_Count INT
            , @Incoming_Public_Count INT
            , @Exception_Public_Count INT
            , @Variance_Count INT
            , @Standing_Data_Queue_Count INT
            , @Invoice_Collection_Queue_Cnt INT
            , @Invoice_Collection_Exception_Queue_Cnt INT
            , @Exception_Type_Cd INT
            , @Account_Standing_Data_Queue_Count INT
            , @Contract_Standing_Data_Queue_Count INT
            , @Cu_Invoice_Standing_Data_Exception_Count INT;




        DECLARE @Cu_Exception_Private_Queue TABLE
              (
                  Cu_Invoice_Id INT
                  , Client_Hier_Id INT
                  , Account_Id INT
                  , Commodity_Id INT
                  , Exception_Type VARCHAR(200)
                  , Is_Manual BIT
              );

        SELECT
            @queue_id = QUEUE_ID
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @MyAccountId;

        SELECT
            @Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND c.Code_Value = 'Missing IC Data';

        INSERT INTO @Cu_Exception_Private_Queue
             (
                 Cu_Invoice_Id
                 , Client_Hier_Id
                 , Account_Id
                 , Commodity_Id
                 , Exception_Type
                 , Is_Manual
             )
        SELECT
            CU_INVOICE_ID
            , Client_Hier_ID
            , Account_ID
            , Commodity_Id
            , EXCEPTION_TYPE
            , IS_MANUAL
        FROM
            dbo.CU_EXCEPTION_DENORM
        WHERE
            QUEUE_ID = @queue_id;

        SELECT
            @Incoming_Private_Count = COUNT(DISTINCT Cu_Invoice_Id)
        FROM
            @Cu_Exception_Private_Queue
        WHERE
            Is_Manual = 1
            AND Exception_Type != 'Failed Recalc';

        SELECT
            @Recalc_Exception_Private_Count = COUNT(1)
        FROM    (   SELECT
                        Cu_Invoice_Id
                        , Client_Hier_Id
                        , Account_Id
                        , Commodity_Id
                    FROM
                        @Cu_Exception_Private_Queue
                    WHERE
                        Exception_Type = 'Failed Recalc'
                    GROUP BY
                        Cu_Invoice_Id
                        , Account_Id
                        , Commodity_Id
                        , Client_Hier_Id) Prv_cnt;

        SELECT
            @Other_Exception_Private_Count = COUNT(DISTINCT Cu_Invoice_Id)
        FROM
            @Cu_Exception_Private_Queue
        WHERE
            Is_Manual = 0
            AND Exception_Type != 'Failed Recalc';

        SELECT
            @Incoming_Public_Count = ISNULL(SUM(pub_Cnt.Incoming_Public_Count), 0)
            , @Exception_Public_Count = ISNULL(SUM(pub_Cnt.Exception_Public_Count), 0)
        FROM    (   SELECT
                        CASE WHEN IS_MANUAL = 1 THEN COUNT(DISTINCT t.CU_INVOICE_ID)
                        END AS Incoming_Public_Count
                        , CASE WHEN IS_MANUAL = 0 THEN COUNT(DISTINCT t.CU_INVOICE_ID)
                          END AS Exception_Public_Count
                    FROM
                        dbo.USER_INFO_GROUP_INFO_MAP ugm
                        JOIN dbo.GROUP_INFO gi
                            ON gi.GROUP_INFO_ID = ugm.GROUP_INFO_ID
                        JOIN dbo.CU_EXCEPTION_DENORM t
                            ON t.QUEUE_ID = gi.QUEUE_ID
                    WHERE
                        ugm.USER_INFO_ID = @MyAccountId
                    GROUP BY
                        IS_MANUAL) pub_Cnt;

        /**Variance Count**/

        SELECT
            @Variance_Count = ISNULL(SUM(Variance_Count), 0)
        FROM    (   SELECT
                        1 AS Variance_Count
                    FROM
                        dbo.Variance_Log l
                        INNER JOIN Core.Client_Hier ch
                            ON l.Site_ID = ch.Site_Id
                    WHERE
                        Queue_Id = @queue_id
                        AND Closed_Dt IS NULL
                        AND Is_Failure = 1
                    GROUP BY
                        l.Account_ID
                        , l.Service_Month) Var_Cnt;


        /**Standing Data Queue**/

        SELECT
            @Account_Standing_Data_Queue_Count = COUNT(1) OVER ()
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Code TypeCd
                ON TypeCd.Code_Id = ae.Exception_Type_Cd
            INNER JOIN dbo.Code StatusCd
                ON StatusCd.Code_Id = ae.Exception_Status_Cd
        WHERE
            ae.Queue_Id = @queue_id
            AND StatusCd.Code_Value IN ( 'New', 'In Progress' )
            AND (   @Is_Missing_IC_Included IS NULL
                    OR  (   @Is_Missing_IC_Included = 1
                            AND ae.Exception_Type_Cd = @Exception_Type_Cd)
                    OR  (   @Is_Missing_IC_Included = 0
                            AND ae.Exception_Type_Cd <> @Exception_Type_Cd))
        GROUP BY
            ae.Account_Exception_Id;



        SELECT
            @Contract_Standing_Data_Queue_Count = COUNT(1) OVER ()
        FROM
            dbo.Contract_Exception ae
            INNER JOIN dbo.Code TypeCd
                ON TypeCd.Code_Id = ae.Exception_Type_Cd
            INNER JOIN dbo.Code StatusCd
                ON StatusCd.Code_Id = ae.Exception_Status_Cd
        WHERE
            ae.Queue_Id = @queue_id
            AND StatusCd.Code_Value IN ( 'New', 'In Progress' )
        GROUP BY
            ae.Contract_Exception_Id;


        SELECT
            @Standing_Data_Queue_Count = ISNULL(@Account_Standing_Data_Queue_Count, 0)
                                         + ISNULL(@Contract_Standing_Data_Queue_Count, 0);


        /**Invoice Collection Queue**/
        SELECT
            @Invoice_Collection_Queue_Cnt = COUNT(1) OVER ()
        FROM
            Invoice_Collection_Queue icqe
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icqe.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON icac.Account_Id = cha.Account_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icqe.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code scc
                ON scc.Code_Id = icqe.Status_Cd
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icqe.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm
                       LEFT OUTER JOIN Account_Invoice_Collection_Frequency aicfe
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                       LEFT OUTER JOIN Code ifc
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                       LEFT OUTER JOIN dbo.Code icgc
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
                            INNER JOIN Code Sc
                                ON Sc.Code_Id = iccc.Status_Cd
                                   AND  Sc.Code_Value = 'Close')
                ON icccm.Invoice_Collection_Queue_Id = icqe.Invoice_Collection_Queue_Id
        WHERE
            icac.Invoice_Collection_Officer_User_Id = @MyAccountId
            AND icqtc.Code_Value = 'ICR'
            AND scc.Code_Value = 'Open'
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , CASE WHEN icccm.Invoice_Collection_Queue_Id IS NOT NULL THEN 'Y'
                  ELSE 'N'
              END
            , CAST(icqe.Created_Ts AS DATE)
            , ISNULL(ifc.Code_Value, icgc.Code_Value);





        /**Invoice Collection Exception Queue**/
        SELECT
            @Invoice_Collection_Exception_Queue_Cnt = COUNT(1) OVER ()
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Code icqtc
                ON icqtc.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icq.Status_Cd
        WHERE
            icac.Invoice_Collection_Officer_User_Id = @MyAccountId
            AND icqtc.Code_Value = 'ICE'
            AND sc.Code_Value = 'Open';

        /*Cu_Invoice_Standing_Data_Exception_Cnt*/

        
		SELECT
            @Cu_Invoice_Standing_Data_Exception_Count = COUNT(1) OVER ()
        FROM
            dbo.Cu_Invoice_Standing_Data_Exception sde
            INNER JOIN dbo.Code StatusCd
                ON StatusCd.Code_Id = sde.Exception_Status_Cd
        WHERE
            sde.Queue_Id = @queue_id
            AND StatusCd.Code_Value IN ( 'New', 'In Progress', 'Archived' );

        SELECT
            @Standing_Data_Queue_Count = @Standing_Data_Queue_Count + ISNULL(@Cu_Invoice_Standing_Data_Exception_Count,0);
        
		SELECT
            @Incoming_Public_Count AS Incoming_Public_Count
            , @Incoming_Private_Count AS Incoming_Private_Count
            , @Exception_Public_Count AS Exception_Public_Count
            , @Other_Exception_Private_Count AS Exception_private_count
            , @Variance_Count AS variance_count
            , @Recalc_Exception_Private_Count AS Recalc_Count
            , @Standing_Data_Queue_Count AS Standing_Data_Queue_Count
            , @Invoice_Collection_Queue_Cnt AS Invoice_Collection_Queue_Cnt
            , @Invoice_Collection_Exception_Queue_Cnt AS Invoice_Collection_Exception_Queue_Cnt;

    END;
    ;


    ;




    ;


GO





GRANT EXECUTE ON  [dbo].[cbmsQueueItem_GetCount] TO [CBMSApplication]
GO
