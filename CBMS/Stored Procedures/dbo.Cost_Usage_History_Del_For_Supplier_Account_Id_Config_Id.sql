SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
      
NAME: dbo.Cost_Usage_History_Del_For_Supplier_Account_Id_Config_Id      
           
DESCRIPTION:       
      
 To Delete Cost/Usage history associated with the given Account Id & Time bancg.      
            
INPUT PARAMETERS:                
NAME								DATATYPE			DEFAULT				DESCRIPTION                
----------------------------------------------------------------------------------------                
@Account_Id							INT   
@Supplier_Account_Config_Id			INT         
                      
OUTPUT PARAMETERS:      
NAME								DATATYPE			DEFAULT				DESCRIPTION                
----------------------------------------------------------------------------------------                

USAGE EXAMPLES:      
----------------------------------------------------------------------------------------                
      
BEGIN TRAN

EXEC dbo.Cost_Usage_History_Del_For_Supplier_Account_Id_Config_Id
    @Account_Id = 1
    , @Supplier_Account_Config_Id = 1

ROLLBACK TRAN

      
AUTHOR INITIALS:  
INITIALS			 NAME  
----------------------------------------------------------------------------------------       
NR					Narayana Reddy         

  
MODIFICATIONS  
INITIALS		DATE		MODIFICATION  
----------------------------------------------------------------------------------------     
NR				2020-01-16	MAINT-9734 - Created.
           
  
*/
CREATE PROCEDURE [dbo].[Cost_Usage_History_Del_For_Supplier_Account_Id_Config_Id]
    (
        @Account_Id INT
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Cost_Usage_Account_Dtl_List
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Bucket_Master_Id INT
                 , Service_Month DATE
                 , Row_Num INT IDENTITY(1, 1)
             );

        CREATE TABLE #Bucket_Account_Interval_Dtl_List
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Bucket_Master_Id INT
                 , Service_Start_Dt DATE
                 , Service_End_Dt DATE
                 , Data_Source_Cd INT
                 , Row_Num INT IDENTITY(1, 1)
             );

        DECLARE
            @Client_Hier_Id INT
            , @Service_Month DATE
            , @Service_Start_Dt DATE
            , @Service_End_Dt DATE
            , @Data_Source_Cd INT
            , @Bucket_Master_Id INT
            , @Total_Row_Count INT
            , @Row_Counter INT;

        DECLARE @Ch_Acc TABLE
              (
                  Client_Hier_Id INT
                  , Account_Id INT
                  , Supplier_Account_begin_Dt DATE
                  , Supplier_Account_End_Dt DATE
              );

        INSERT INTO @Ch_Acc
             (
                 Client_Hier_Id
                 , Account_Id
                 , Supplier_Account_begin_Dt
                 , Supplier_Account_End_Dt
             )
        SELECT
            cha.Client_Hier_Id
            , cha.Account_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Supplier_Account_Config_Id = @Supplier_Account_Config_Id
        GROUP BY
            cha.Client_Hier_Id
            , cha.Account_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt;

        INSERT INTO #Cost_Usage_Account_Dtl_List
             (
                 Client_Hier_Id
                 , Account_Id
                 , Bucket_Master_Id
                 , Service_Month
             )
        SELECT
            cuad.Client_Hier_ID
            , cuad.ACCOUNT_ID
            , cuad.Bucket_Master_Id
            , cuad.Service_Month
        FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN @Ch_Acc ca
                ON ca.Account_Id = cuad.ACCOUNT_ID
        WHERE
            cuad.ACCOUNT_ID = @Account_Id
            AND cuad.Service_Month BETWEEN ca.Supplier_Account_begin_Dt
                                   AND     ca.Supplier_Account_End_Dt;

        INSERT INTO #Bucket_Account_Interval_Dtl_List
             (
                 Client_Hier_Id
                 , Account_Id
                 , Bucket_Master_Id
                 , Service_Start_Dt
                 , Service_End_Dt
                 , Data_Source_Cd
             )
        SELECT
            baid.Client_Hier_Id
            , baid.Account_Id
            , baid.Bucket_Master_Id
            , baid.Service_Start_Dt
            , baid.Service_End_Dt
            , baid.Data_Source_Cd
        FROM
            dbo.Bucket_Account_Interval_Dtl baid
            INNER JOIN @Ch_Acc cha
                ON baid.Account_Id = cha.Account_Id
                   AND  baid.Client_Hier_Id = cha.Client_Hier_Id
                   AND  baid.Service_Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                              AND     cha.Supplier_Account_End_Dt
                   AND  baid.Service_End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                            AND     cha.Supplier_Account_End_Dt;

        BEGIN TRY
            --Loop for deleting records from Cost_Usage_Account_Dtl table                         
            SELECT  @Total_Row_Count = MAX(Row_Num)FROM #Cost_Usage_Account_Dtl_List;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Client_Hier_Id = cuad.Client_Hier_Id
                        , @Bucket_Master_Id = cuad.Bucket_Master_Id
                        , @Service_Month = cuad.Service_Month
                    FROM
                        #Cost_Usage_Account_Dtl_List cuad
                    WHERE
                        cuad.Row_Num = @Row_Counter;

                    EXECUTE dbo.Cost_Usage_Account_Dtl_DEL_By_Client_Hier_Account_Bucket_Service_Month
                        @Client_Hier_Id = @Client_Hier_Id
                        , @Account_Id = @Account_Id
                        , @Bucket_Master_Id = @Bucket_Master_Id
                        , @Service_Month = @Service_Month;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

            --Loop for deleting records from Bucket_Account_Interval_Dtl table      
            SELECT
                @Total_Row_Count = MAX(Row_Num)
            FROM
                #Bucket_Account_Interval_Dtl_List;
            SET @Row_Counter = 1;

            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Client_Hier_Id = baid.Client_Hier_Id
                        , @Account_Id = baid.Account_Id
                        , @Bucket_Master_Id = baid.Bucket_Master_Id
                        , @Service_Start_Dt = baid.Service_Start_Dt
                        , @Service_End_Dt = baid.Service_End_Dt
                        , @Data_Source_Cd = baid.Data_Source_Cd
                    FROM
                        #Bucket_Account_Interval_Dtl_List baid
                    WHERE
                        Row_Num = @Row_Counter;

                    EXECUTE dbo.Bucket_Account_Interval_Dtl_Del
                        @Client_Hier_Id = @Client_Hier_Id
                        , @Account_Id = @Account_Id
                        , @Bucket_Master_Id = @Bucket_Master_Id
                        , @Service_Start_Dt = @Service_Start_Dt
                        , @Service_End_Dt = @Service_End_Dt
                        , @Data_Source_Cd = @Data_Source_Cd;

                    SET @Row_Counter = @Row_Counter + 1;
                END;

        END TRY
        BEGIN CATCH
            EXEC dbo.usp_RethrowError;
        END CATCH;

        DROP TABLE #Cost_Usage_Account_Dtl_List;
        DROP TABLE #Bucket_Account_Interval_Dtl_List;

    END;

GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_History_Del_For_Supplier_Account_Id_Config_Id] TO [CBMSApplication]
GO
