
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsCuInvoiceServiceMonth_GetForInvoice

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------         	
	@cu_invoice_id 	int       	          	
	@account_id		INT				NULL
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 107
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 121441,12004
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 125396,6946
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 124674
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 3920503
	EXEC cbmsCuInvoiceServiceMonth_GetForInvoice 3975763
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	HG			Harihara Suthan G
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SSR			03/11/2010	Added @Account_ID input parameter
	SSR			03/18/2010	Removed @MyAccountId input parameter
	HG			08/10/2010	Added Account_ID column in select Clause.
	NR			26-10-2017	MAINT-6014 - Added Begin_Dt,End_Dt,Billing_Days columns.
******/

CREATE PROCEDURE [dbo].[cbmsCuInvoiceServiceMonth_GetForInvoice]
      ( 
       @cu_invoice_id INT
      ,@account_id INT = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON

      SELECT
            cs.cu_invoice_service_month_id
           ,cs.cu_invoice_id
           ,cs.service_month
           ,cs.Account_ID
           ,cs.Begin_Dt
           ,cs.End_Dt
           ,cs.Billing_Days
      FROM
            dbo.cu_invoice_service_month cs
      WHERE
            ( cs.cu_invoice_id = @cu_invoice_id )
            AND ( @account_id IS NULL
                  OR cs.account_id = @account_id )
            AND ( cs.service_month IS NOT NULL )
      ORDER BY
            cs.service_month

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceServiceMonth_GetForInvoice] TO [CBMSApplication]
GO
