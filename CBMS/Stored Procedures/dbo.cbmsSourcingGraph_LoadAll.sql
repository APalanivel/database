SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSourcingGraph_LoadAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsSourcingGraph_LoadAll]
(
	@MyAccountId int
)
as
select sourcing_graph_id
	,source_type_id
	,pricing_point_id
	,strip_type_id
	,from_date
	,to_date
	,single_date
	,user_info_id
	,position
from sourcing_graph
where user_info_id = @MyAccountId
GO
GRANT EXECUTE ON  [dbo].[cbmsSourcingGraph_LoadAll] TO [CBMSApplication]
GO
