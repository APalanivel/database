SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_ACCOUNT_EXISTENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select entity_id  from entity where entity_type =1029 and entity_name ='Closed'
--select * from entity where entity_id=1060
--select * from sr_rfp_account where account_id=18617
--select * from entity where entity_type=10041029
--exec SR_RFP_CHECK_ACCOUNT_EXISTENCY_P 18617--15286
CREATE       PROCEDURE DBO.SR_RFP_CHECK_ACCOUNT_EXISTENCY_P

@accountId varchar(20)

AS
set nocount on
declare @entityID as int
select @entityID =  entity_id  from entity where entity_type =1029 and entity_name = 'Closed'
/*select	count(1)

from	sr_rfp_account 

where	 bid_status_type_id <> @entityID
	and account_id= @accountId*/


select	rfp.commodity_type_id

from	sr_rfp_account rfpAcc,
	sr_rfp rfp 

where	rfpAcc.sr_rfp_id = rfp.sr_rfp_id 	
	and rfpAcc.bid_status_type_id <> @entityID
	and rfpAcc.account_id=  @accountId
	and rfpAcc.is_deleted = 0
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_ACCOUNT_EXISTENCY_P] TO [CBMSApplication]
GO
