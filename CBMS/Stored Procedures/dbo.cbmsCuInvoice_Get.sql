
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.cbmsCuInvoice_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@cu_invoice_Id 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.cbmsCuInvoice_Get 1034533
	EXEC cbmsCuInvoice_Get 2120626
	
	EXEC cbmsCuInvoice_Get 1557590

	EXEC cbmsCuInvoice_Get 16046665	

	EXEC cbmsCuInvoice_Get 	16043656

	EXEC cbmsCuInvoice_Get 	16029974

	EXEC cbmsCuInvoice_Get 	14357511
	
	EXEC dbo.cbmsCuInvoice_Get 1023746

    SELECT TOP 1000
      sm.*
    FROM
      Cu_Invoice_Service_Month sm
      JOIN Account a
            ON a.Account_Id = sm.Account_Id
               AND a.Account_Type_Id = 37
               AND sm.Service_Month IS NOT NULL
    ORDER BY
      CU_INVOICE_Id DESC
	
	Account_Id DESC, Service_Month DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash
	HG			Hari
	SP			Sandeep Pigilam
	RKV			Ravi Kumar Vegesna
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SS			9/29/2009	Refers to supplier_account_meter_map's Account_Id in place of contract's Account_Id     		        	
	HG			10/23/2009	Meter Dissassociation date should be greater than the contract start date.
							Service month mapping with left outer subselect mapped with Supplier_Account_begin_dt and End_Dt as the invoice related information should mapped with supplier account.
	SSR			03/25/2010	Removed Cu_invoice(Account_Id) with cu_invoice_service_month(Account_Id)
	SKA			04/12/2010	Removed View vwCbmsDistinctAccountSite
							Removed nolock hints
	SKA			06/10/2010	Removed the CTE logic and used existing logic
	SKA			06/25/2010	Add the entity table to get only Utility Accounts
	SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(samm.meter_disassociation_date IS NULL)
							- After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
    DMR		    08/24/2011  Added Set Ansi_Warnings statement within this stored procedure to eliminate then noise
						    to help identify actual SSIS execution issues.  The Min(Service_Month) from cu_invoice_service_month 
						    is the statement that throws this error.							
	HG			2013-07-09	MAINT-1972, Replaced the view with the base tables to improve the performance
	HG			2013-09-03	Modified the script to accept the NULL value in table variable @Account_Analyst_Map as all the accounts may not have analysts mapped
							DISTINCT replaced by GROUP BY
	SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names 
	RKV         2015-08-27  Added Meter_Site_Id to the result 
							

***************************/
CREATE PROCEDURE [dbo].[cbmsCuInvoice_Get] ( @Cu_Invoice_Id INT )
AS 
BEGIN

      SET NOCOUNT ON
      SET ANSI_WARNINGS OFF

      DECLARE @Cu_Invoice_Service_Month TABLE
            ( 
             Account_Id INT
            ,Service_Month DATE NULL
            ,Cu_Invoice_Id INT )
	
      DECLARE @Client_Hier_Account TABLE
            ( 
             Account_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(2000)
            ,Account_Type VARCHAR(200)
            ,Account_Number VARCHAR(200)
            ,Account_Group_Id INT
            ,Account_Not_Managed BIT
            ,Utility_Account_Vendor_Id INT
            ,Meter_State_Id INT )

      DECLARE @Account_Analyst_Map TABLE
            ( 
             Account_Id INT
            ,Regulated_Analyst_Id INT NULL
            ,Deregulated_Analyst_Id INT NULL )

      DECLARE @Min_Service_Month DATE
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Quality_Assurance'  
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market'  
        

      INSERT      INTO @Cu_Invoice_Service_Month
                  ( 
                   Account_Id
                  ,Service_Month
                  ,Cu_Invoice_Id )
                  SELECT
                        Account_Id
                       ,Service_Month
                       ,@Cu_Invoice_Id
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH
                  WHERE
                        CU_INVOICE_Id = @Cu_Invoice_Id
                  GROUP BY
                        Account_Id
                       ,Service_Month

      INSERT      INTO @Client_Hier_Account
                  ( 
                   Account_Id
                  ,Site_Id
                  ,Site_Name
                  ,Account_Type
                  ,Account_Number
                  ,Account_Group_Id
                  ,Account_Not_Managed
                  ,Utility_Account_Vendor_Id
                  ,Meter_State_Id )
                  SELECT
                        cha.Account_Id
                       ,ch.Site_Id
                       ,RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,cha.Account_Type
                       ,cha.Account_Number
                       ,cha.Account_Group_Id
                       ,cha.Account_Not_Managed
                       ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Id
                        END
                       ,cha.Meter_State_Id
                  FROM
                        core.Client_Hier ch
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Cu_Invoice_Service_Month sm
                              ON sm.Account_Id = cha.Account_Id
                  GROUP BY
                        cha.Account_Id
                       ,ch.Site_Id
                       ,RTRIM(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')'
                       ,cha.Account_Type
                       ,cha.Account_Number
                       ,cha.Account_Group_Id
                       ,cha.Account_Not_Managed
                       ,CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Id
                        END
                       ,cha.Meter_State_Id

      INSERT      INTO @Account_Analyst_Map
                  ( 
                   Account_Id
                  ,Regulated_Analyst_Id
                  ,Deregulated_Analyst_Id )
                  SELECT
                        cha.Account_Id
                       ,MAX(CASE WHEN gil.Legacy_Group_Name = 'Regulated_Market' THEN udam.Analyst_Id
                            END)
                       ,MAX(CASE WHEN gil.Legacy_Group_Name = 'Quality_Assurance' THEN udam.Analyst_Id
                            END)
                  FROM
                        @Client_Hier_Account cha
                        INNER JOIN dbo.UTILITY_DETAIL ud
                              ON ud.VENDOR_Id = cha.Utility_Account_Vendor_Id
                        INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                              ON udam.Utility_Detail_Id = ud.UTILITY_DETAIL_Id
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.GROUP_INFO_Id = udam.Group_Info_Id
                        INNER JOIN @Group_Legacy_Name gil
                              ON gi.GROUP_INFO_Id = gil.GROUP_INFO_Id
                  WHERE
                        cha.Account_Type = 'Utility'
                  GROUP BY
                        cha.Account_Id						


      INSERT      INTO @Account_Analyst_Map
                  ( 
                   Account_Id
                  ,Regulated_Analyst_Id
                  ,Deregulated_Analyst_Id )
                  SELECT
                        Account_Id
                       ,Regulated_Analyst_Id
                       ,Deregulated_Analyst_Id
                  FROM
                        ( SELECT
                              cha.Account_Id
                             ,MAX(CASE WHEN gil.Legacy_Group_Name = 'Regulated_Market' THEN udam.Analyst_Id
                                  END) AS Regulated_Analyst_Id
                             ,MAX(CASE WHEN gil.Legacy_Group_Name = 'Quality_Assurance' THEN udam.Analyst_Id
                                  END) AS Deregulated_Analyst_Id
                          FROM
                              @Client_Hier_Account cha
                              INNER JOIN core.Client_Hier_Account scha
                                    ON scha.Account_Id = cha.Account_Id
                              INNER JOIN core.Client_Hier_Account ucha
                                    ON ucha.Meter_Id = scha.Meter_Id
                                       AND ucha.Client_Hier_Id = scha.Client_Hier_Id
                                       AND ucha.Account_Type = 'Utility'
                              INNER JOIN dbo.UTILITY_DETAIL ud
                                    ON ud.VENDOR_Id = ucha.Account_Vendor_Id
                              INNER JOIN dbo.Utility_Detail_Analyst_Map udam
                                    ON udam.Utility_Detail_Id = ud.UTILITY_DETAIL_Id
                              INNER JOIN dbo.GROUP_INFO gi
                                    ON gi.GROUP_INFO_Id = udam.Group_Info_Id
                              INNER JOIN @Group_Legacy_Name gil
                                    ON gi.GROUP_INFO_Id = gil.GROUP_INFO_Id
                          WHERE
                              cha.Account_Type = 'Supplier'
                          GROUP BY
                              cha.Account_Id
                             ,ucha.Account_Id ) an
                  GROUP BY
                        Account_Id
                       ,Regulated_Analyst_Id
                       ,Deregulated_Analyst_Id

      SELECT
            @Min_Service_Month = MIN(Service_Month)
      FROM
            @Cu_Invoice_Service_Month

      SELECT
            ci.Cu_Invoice_Id
           ,ci.Cbms_Image_Id
           ,img.Cbms_Doc_Id
           ,ci.Ubm_Id
           ,ubm.Ubm_Name
           ,ml.Ubm_Batch_Master_Log_Id
           ,ci.Ubm_Invoice_Id
           ,ci.Ubm_Account_Code
           ,ci.Ubm_Client_Code
           ,ci.Ubm_City
           ,ci.Ubm_State_Code
           ,ci.Ubm_Vendor_Code
           ,ci.Ubm_Account_Number
           ,ci.Account_Group_Id
           ,cism.Account_Id
           ,cha.Account_Number
           ,'Group_Billing_Number' = CASE WHEN cha.Account_Id IS NULL THEN 'Unknown'
                                          ELSE ISNULL(ag.Group_Billing_Number, 'Not part of Group Bill')
                                     END
           ,ci.Client_Id
           ,cl.Client_Name
           ,cha.Site_Name
           ,ci.Vendor_Id
           ,ci.Begin_Date
           ,ci.End_Date
           ,ci.Billing_Days
           ,ci.Ubm_Currency_Code
           ,ci.Currency_Unit_Id
           ,ci.Current_Charges
           ,ci.Is_Default
           ,ci.Is_Processed
           ,ci.Is_Reported
           ,ci.Is_Dnt
           ,ci.Do_Not_Track_Id
           ,ci.Updated_By_Id
           ,ci.Updated_Date
           ,'Managed_As' = CASE WHEN cha.Account_Type IS NULL THEN 'Unknown'
                                WHEN cha.Account_Type = 'Supplier' THEN 'De-regulated'
                                ELSE CASE WHEN x.Contract_Id IS NOT NULL THEN 'De-regulated'
                                          ELSE 'Regulated'
                                     END
                           END
           ,'Managed_By_Id' = CASE WHEN cha.Account_Type IS NULL THEN NULL
                                   WHEN cha.Account_Type = 'Supplier' THEN aa.Deregulated_Analyst_Id
                                   ELSE CASE WHEN x.Contract_Id IS NOT NULL THEN aa.Deregulated_Analyst_Id
                                             ELSE aa.Regulated_Analyst_Id
                                        END
                              END
           ,'Managed_By' = ui.First_Name + SPACE(1) + ui.Last_Name
           ,ui.Queue_Id AS Managed_By_Queue_Id
           ,ci.Ubm_Invoice_Identifier
           ,ci.Ubm_Feed_File_Name
           ,ci.Is_Manual
           ,ci.Is_Duplicate
           ,cha.Meter_State_Id
      FROM
            dbo.Cu_Invoice ci
            INNER JOIN dbo.Cbms_Image img
                  ON img.cbms_image_Id = ci.cbms_image_Id
            LEFT OUTER JOIN @CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_Id = ci.CU_INVOICE_Id
            LEFT OUTER JOIN @Client_Hier_Account cha
                  ON cha.Account_Id = cism.Account_Id
            LEFT OUTER JOIN dbo.Client cl
                  ON cl.client_Id = ci.client_Id
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP ag
                  ON ag.account_group_Id = cha.Account_group_Id
            LEFT OUTER JOIN ( SELECT
                                    cha.Account_Id
                                   ,scha.Supplier_Contract_Id AS Contract_Id
                                   ,ISNULL(scha.Supplier_Meter_Association_Date, scha.Supplier_Account_Begin_Dt) SuppAcct_begin_dt
                                   ,ISNULL(scha.Supplier_Meter_Disassociation_Date, scha.Supplier_Account_End_Dt) SuppAcct_End_dt
                              FROM
                                    @Client_Hier_Account cha
                                    INNER JOIN core.Client_Hier_Account ucha
                                          ON ucha.Account_Id = cha.Account_Id
                                    INNER JOIN core.Client_Hier_Account scha
                                          ON scha.Meter_Id = ucha.Meter_Id
                                             AND scha.Client_Hier_Id = ucha.Client_Hier_Id
                                             AND scha.Account_Type = 'Supplier'
                              WHERE
                                    cha.Account_Not_Managed = 0
                                    AND cha.Account_Type = 'Utility'
                                    AND scha.Supplier_Contract_Id IS NOT NULL ) x
                  ON x.Account_Id = cism.Account_Id
                     AND @Min_Service_Month BETWEEN x.SuppAcct_begin_dt
                                            AND     x.SuppAcct_End_dt
            LEFT OUTER JOIN @Account_Analyst_Map aa
                  ON aa.Account_Id = cism.Account_Id
            LEFT OUTER JOIN dbo.user_info ui
                  ON ui.user_info_Id = CASE WHEN cha.Account_Type IS NULL THEN NULL
                                            WHEN cha.Account_Type = 'Supplier' THEN aa.deregulated_analyst_Id
                                            ELSE CASE WHEN x.Contract_Id IS NOT NULL THEN aa.deregulated_analyst_Id
                                                      ELSE aa.Regulated_Analyst_Id
                                                 END
                                       END
            LEFT OUTER JOIN dbo.Ubm_invoice i
                  ON i.Ubm_invoice_Id = ci.Ubm_invoice_Id
            LEFT OUTER JOIN dbo.Ubm_batch_master_log ml
                  ON ml.Ubm_batch_master_log_Id = i.Ubm_batch_master_log_Id
            LEFT OUTER JOIN dbo.ubm
                  ON ubm.Ubm_Id = ml.Ubm_Id
      WHERE
            ci.cu_invoice_Id = @cu_invoice_Id
      GROUP BY
            ci.Cu_Invoice_Id
           ,ci.Cbms_Image_Id
           ,img.Cbms_Doc_Id
           ,ci.Ubm_Id
           ,ubm.Ubm_Name
           ,ml.Ubm_Batch_Master_Log_Id
           ,ci.Ubm_Invoice_Id
           ,ci.Ubm_Account_Code
           ,ci.Ubm_Client_Code
           ,ci.Ubm_City
           ,ci.Ubm_State_Code
           ,ci.Ubm_Vendor_Code
           ,ci.Ubm_Account_Number
           ,ci.Account_Group_Id
           ,cism.Account_Id
           ,cha.Account_Number
           ,CASE WHEN cha.Account_Id IS NULL THEN 'Unknown'
                 ELSE ISNULL(ag.Group_Billing_Number, 'Not part of Group Bill')
            END
           ,ci.Client_Id
           ,cl.Client_Name
           ,cha.Site_Name
           ,ci.Vendor_Id
           ,ci.Begin_Date
           ,ci.End_Date
           ,ci.Billing_Days
           ,ci.Ubm_Currency_Code
           ,ci.Currency_Unit_Id
           ,ci.Current_Charges
           ,ci.Is_Default
           ,ci.Is_Processed
           ,ci.Is_Reported
           ,ci.Is_Dnt
           ,ci.Do_Not_Track_Id
           ,ci.Updated_By_Id
           ,ci.Updated_Date
           ,CASE WHEN cha.Account_Type IS NULL THEN 'Unknown'
                 WHEN cha.Account_Type = 'Supplier' THEN 'De-regulated'
                 ELSE CASE WHEN x.Contract_Id IS NOT NULL THEN 'De-regulated'
                           ELSE 'Regulated'
                      END
            END
           ,CASE WHEN cha.Account_Type IS NULL THEN NULL
                 WHEN cha.Account_Type = 'Supplier' THEN aa.Deregulated_Analyst_Id
                 ELSE CASE WHEN x.Contract_Id IS NOT NULL THEN aa.Deregulated_Analyst_Id
                           ELSE aa.Regulated_Analyst_Id
                      END
            END
           ,ui.First_Name + SPACE(1) + ui.Last_Name
           ,ui.Queue_Id
           ,ci.Ubm_Invoice_Identifier
           ,ci.Ubm_Feed_File_Name
           ,ci.Is_Manual
           ,ci.Is_Duplicate
           ,cha.Meter_State_Id
		
END;
;

;
GO





GRANT EXECUTE ON  [dbo].[cbmsCuInvoice_Get] TO [CBMSApplication]
GO
