SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seContent_GetAllLatest
As
BEGIN
	set nocount on
	
		declare @StatusId int
		
		set @StatusId = dbo.dmlLookup_GetId ('Content Status', 'Approved')
	
	 select c.ContentId
				, cp.ContentPageId
				, cp.PageLabel
				, c.ContentPlacementId
				, cpl.PlacementLabel
				, c.ContentLocaleId
				, cl.LookupName ContentLocale
				, c.Version
				, c.CurrentStatusId
				, c.ContentText
		 from seContent c
		 join dmlLookup cl on cl.LookupId = c.ContentLocaleId
		 join seContentPlacement cpl on cpl.ContentPlacementId = c.ContentPlacementId
		 join seContentPage cp on cp.ContentPageId = cpl.ContentPageId
		 join (select ContentLocaleId
								, ContentPlacementId
								, max(Version) Version
						 from seContent
						where CurrentStatusId = @StatusId
				 group by ContentLocaleId
								, ContentPlacementId
					) x on (c.ContentPlacementId = x.ContentPlacementId and c.ContentLocaleId = x.ContentLocaleId and c.Version = x.Version)
 order by cp.PageLabel asc
				, cpl.PlacementLabel


END
GO
GRANT EXECUTE ON  [dbo].[seContent_GetAllLatest] TO [CBMSApplication]
GO
