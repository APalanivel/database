SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Account_Exception_History_Sel_By_Account_Id    
    
DESCRIPTION:    
    
INPUT PARAMETERS:    
 Name          DataType  Default  Description    
-------------------------------------------------------------------------------------------    
 @Account_Id         INT    
       
    
OUTPUT PARAMETERS:    
 Name          DataType  Default  Description    
-------------------------------------------------------------------------------------------    
     
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------------    
      
EXEC dbo.Account_Exception_History_Sel_By_Account_Id 227691    
    
EXEC Account_Exception_History_Sel_By_Account_Id @Account_Id = 1741006    
    
EXEC dbo.Account_Exception_History_Sel_By_Account_Id 1741098  

EXEC Account_Exception_History_Sel_By_Account_Id 1741168       
 
 
 exec Account_Exception_History_Sel_By_Account_Id 1655652   
    
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------------------------------    
 NR   Narayana Reddy    
     
MODIFICATIONS    
 Initials Date   Modification    
-------------------------------------------------------------------------------------------    
 NR        2019-07-19  Created for Add Contract.    
    
******/
CREATE PROCEDURE [dbo].[Account_Exception_History_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ae.Exception_Type_Cd
            , c.Code_Value AS Exception_Type
            , ae.Exception_Status_Cd
            , cc.Code_Value AS Exception_Status
            , ae.Exception_Created_Ts
            , ae.Exception_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Exception_By_User
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , ae.Service_Desk_Ticket_XId
            , ae.Exception_Begin_Dt
            , ae.Exception_End_Dt
            , ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Code c
                ON c.Code_Id = ae.Exception_Type_Cd
            INNER JOIN Code cc
                ON cc.Code_Id = ae.Exception_Status_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ae.Exception_By_User_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = ae.Account_Id
        WHERE
            ae.Account_Id = @Account_Id
            AND c.Code_Value = 'Missing Contract'
            AND cha.Supplier_Contract_ID = -1
            AND cc.Code_Value IN ( 'New', 'In Progress' )
        GROUP BY
            ae.Exception_Type_Cd
            , c.Code_Value
            , ae.Exception_Status_Cd
            , cc.Code_Value
            , ae.Exception_Created_Ts
            , ae.Exception_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , ae.Service_Desk_Ticket_XId
            , ae.Exception_Begin_Dt
            , ae.Exception_End_Dt
            , ae.Account_Exception_Id
        ORDER BY
            ae.Exception_Created_Ts;
    END;
    ;




GO
GRANT EXECUTE ON  [dbo].[Account_Exception_History_Sel_By_Account_Id] TO [CBMSApplication]
GO
