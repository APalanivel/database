SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: Dbo.Sr_Load_Profile_Additional_Row_Del  
     
DESCRIPTION: 
	It Deletes	Sr Load Profile Additional Row for Selected Sr Load Profile Additional Row Id. 
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------------------         
	@Sr_Load_Profile_Additional_Row_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Sr_Load_Profile_Additional_Row_Del  41
		--EXEC dbo.Sr_Load_Profile_Additional_Row_Del  44

	ROLLBACK TRAN

	SELECT  * FROM dbo.Sr_Load_Profile_Additional_Row
				   WHERE Sr_Load_Profile_Additional_Row_Id = 41
	
	SELECT  * FROM dbo.Sr_Load_Profile_Additional_Row 
				   WHERE Sr_Load_Profile_Additional_Row_Id = 44



AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			24-August-10	CREATED

*/
CREATE PROCEDURE dbo.Sr_Load_Profile_Additional_Row_Del
    (
      @Sr_Load_Profile_Additional_Row_Id	INT
     )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Sr_load_profile_additional_row
	WHERE
		Sr_Load_Profile_Additional_Row_Id = @Sr_Load_Profile_Additional_Row_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Load_Profile_Additional_Row_Del] TO [CBMSApplication]
GO
