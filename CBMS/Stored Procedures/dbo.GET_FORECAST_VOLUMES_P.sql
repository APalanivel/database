SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_FORECAST_VOLUMES_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------
@userId			varchar(10)
@sessionId		varchar(20)
@clientId		int
@forecastYear	int
@forecastAsOfDate datetime
@viewId			int

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------


EXEC dbo.GET_FORECAST_VOLUMES_P 1,1,10069,2009,NULL,1
EXEC dbo.GET_FORECAST_VOLUMES_P 1,1,10069,2009,NULL,2
EXEC dbo.GET_FORECAST_VOLUMES_P 1,1,10069,2009,NULL,3


AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------ 
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						vwSiteName view replaced by Client_hier table in the site level query.

*/
CREATE PROCEDURE dbo.GET_FORECAST_VOLUMES_P
	@userId				VARCHAR(10)
	,@sessionId			VARCHAR(20)
	,@clientId			INT
	,@forecastYear		INT
	,@forecastAsOfDate	DATETIME
	,@viewId			INT
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Rm_Forecast_Volume_Id	INT
		,@MMBtu_Unit_Id				INT

	SELECT
		@Rm_Forecast_Volume_Id = rm_forecast_volume_id
	FROM
		dbo.rm_forecast_volume
	WHERE
		forecast_year = @forecastYear
		AND client_id = @clientId

	SELECT
		@MMBtu_Unit_Id = entity_id
	FROM
		dbo.Entity
	WHERE
		ENTITY_DESCRIPTION = 'Unit for Gas'
		AND entity_name = 'MMBtu'

	IF (@viewId = 3) -- // Site level
	BEGIN

		SELECT
			RTRIM(ch.city) + ', ' + ch.State_name + ' (' + ch.site_name + ')'	AS	site_name
			,CONVERT(VARCHAR(12), details.month_identifier, 101)				AS	month_identifier
			,details.volume
			,volumeType.entity_name
			,siteUnit.entity_name
		FROM
			dbo.RM_FORECAST_VOLUME_DETAILS details
			INNER JOIN dbo.rm_onboard_hedge_setup setup
				ON details.site_id = setup.site_id
			INNER JOIN dbo.entity volumeType
				ON volumeType.entity_id = details.volume_type_id
			INNER JOIN Core.Client_Hier ch
				ON ch.Site_Id = setup.Site_Id
			INNER JOIN dbo.entity siteUnit
				ON siteUnit.entity_id = setup.volume_units_type_id
		WHERE
			details.rm_forecast_volume_id = @Rm_Forecast_Volume_Id
		ORDER BY
			Site_name

	END
	ELSE IF (@viewId = 2) -- // Division level
	BEGIN
	
		SELECT
			sg.Sitegroup_Name										AS	division_name
			,CONVERT(VARCHAR(12), details.month_identifier, 101)	AS	month_identifier
			,SUM(details.volume * conversion.CONVERSION_FACTOR)		AS	volume
			,volumeType.entity_name
		FROM
			dbo.rm_forecast_volume_details details
			INNER JOIN dbo.Sitegroup sg
				ON details.division_id = sg.Sitegroup_Id
			INNER JOIN dbo.rm_onboard_hedge_setup setup
				ON details.site_id = setup.site_id
		
			INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION conversion
				ON conversion.base_unit_id = setup.VOLUME_UNITS_TYPE_ID
			INNER JOIN dbo.entity volumeType
				ON volumeType.entity_id = details.volume_type_id
		WHERE
			details.rm_forecast_volume_id = @Rm_Forecast_Volume_Id
			AND conversion.converted_unit_id = @MMBtu_Unit_Id

		GROUP BY
			month_identifier
			,sg.Sitegroup_Name
			,volumeType.entity_name

	END
	ELSE IF (@viewId = 1) -- // Corporate level
	BEGIN

		SELECT
			CONVERT(VARCHAR(12), details.month_identifier, 101)		AS month_identifier
			,SUM(details.volume * conversion.CONVERSION_FACTOR)		AS volume
			,volumeType.entity_name
		FROM
			dbo.rm_forecast_volume_details details
			INNER JOIN dbo.rm_onboard_hedge_setup setup
				ON details.site_id = setup.site_id
			INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION conversion
				ON conversion.base_unit_id = setup.VOLUME_UNITS_TYPE_ID
			INNER JOIN dbo.entity volumeType
				ON volumeType.entity_id = details.volume_type_id
		WHERE
			details.rm_forecast_volume_id = @Rm_Forecast_Volume_Id
			AND conversion.converted_unit_id = @MMBtu_Unit_Id
		GROUP BY
			month_identifier
			,volumeType.entity_name

	END

END
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUMES_P] TO [CBMSApplication]
GO
