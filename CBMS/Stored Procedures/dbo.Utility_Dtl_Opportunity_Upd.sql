SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Opportunity_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	 @Utility_Dtl_Opportunity_Id	INT
     @Program_Dsc					VARCHAR(500) = NULL
     @Program_Req					VARCHAR(500) = NULL
     @Savings_Potential_Cd			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
	EXEC Utility_Dtl_Opportunity_Upd 1,'Program_Dsc TEST','Program_Req TEST',100309
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC dbo.Utility_Dtl_Opportunity_Upd
      (
       @Utility_Dtl_Opportunity_Id INT
      ,@Program_Dsc VARCHAR(500) = NULL
      ,@Program_Req VARCHAR(500) = NULL
      ,@Savings_Potential_Cd INT = NULL )
AS 
BEGIN
 
      SET nocount ON ;

      UPDATE
            Utility_Dtl_Opportunity
      SET   
            Program_Dsc = @Program_Dsc
           ,Program_Req = @Program_Req
           ,Savings_Potential_Cd = @Savings_Potential_Cd
      WHERE
            Utility_Dtl_Opportunity_Id = @Utility_Dtl_Opportunity_Id
END                        
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Opportunity_Upd] TO [CBMSApplication]
GO
