SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: [dbo].[Vendor_Commodity_Sel_By_Country_State]              
                          
 DESCRIPTION:                          
   To get list of Vendor Commodities based on State OR Country                 
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------   
@Country_Id						INT     
@State_Id						INT  
@Vendory_Type				VARCHAR(200)	'Utility'
@Show_History_Vendor			BIT				0                 
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
   EXEC [dbo].[Vendor_Commodity_Sel_By_Country_State]   
      @State_Id=5  
  
         
                         
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 SP                    Sandeep Pigilam            
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 SP                    2014-02-24       Created                  
                         
******/   
CREATE PROCEDURE [dbo].[Vendor_Commodity_Sel_By_Country_State]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Vendory_Type VARCHAR(200) = 'Utility'
      ,@Show_History_Vendor BIT = 0 )
AS 
BEGIN    
      SET NOCOUNT ON;    
   
      WITH  Cte_Services
              AS ( SELECT
                        cmap.COMMODITY_TYPE_ID
                       ,com.Commodity_Name
                       ,CASE WHEN com.Commodity_Name = 'Electric Power' THEN 1
                             WHEN com.Commodity_Name = 'Natural Gas' THEN 2
                             ELSE 3
                        END Sort_Order
                   FROM
                        dbo.VENDOR ven
                        INNER JOIN dbo.ENTITY typ
                              ON ven.VENDOR_TYPE_ID = typ.ENTITY_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP map
                              ON map.VENDOR_ID = ven.VENDOR_ID
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP cmap
                              ON cmap.VENDOR_ID = map.VENDOR_ID
                        INNER JOIN dbo.Commodity com
                              ON cmap.COMMODITY_TYPE_ID = com.Commodity_Id
                        INNER JOIN dbo.STATE s
                              ON s.STATE_ID = map.STATE_ID
                   WHERE
                        ven.IS_HISTORY = @Show_History_Vendor
                        AND typ.ENTITY_NAME = @Vendory_Type
                        AND typ.ENTITY_DESCRIPTION = 'Vendor'
                        AND ( @State_Id IS NULL
                              OR map.STATE_ID = @State_Id )
                        AND ( @Country_Id IS NULL
                              OR s.COUNTRY_ID = @Country_Id )
                   GROUP BY
                        cmap.COMMODITY_TYPE_ID
                       ,com.Commodity_Name)
            SELECT
                  cs.COMMODITY_TYPE_ID
                 ,cs.Commodity_Name
            FROM
                  Cte_Services cs
            ORDER BY
                  cs.Sort_Order
                 ,cs.Commodity_Name
  
     
     
END  
;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Commodity_Sel_By_Country_State] TO [CBMSApplication]
GO
