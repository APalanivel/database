SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rfp_Lp_Send_Client_Del]  
     
DESCRIPTION: 
	It Deletes Sr_Rfp_Lp_Send_Client for given Sr_Rfp_Lp_Send_Client_Id

INPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------
@Sr_Rfp_Lp_Send_Client_Id		INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran

		EXEC Sr_Rfp_Lp_Send_Client_Del  2541

	Rollback Tran
    
	SELECT
		* FROM Sr_Rfp_Lp_Send_Client WHERE Sr_Rfp_Site_Lp_Xml_Id = 1700
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
HG		    07/26/2010	CREATED

*/

CREATE PROCEDURE dbo.Sr_Rfp_Lp_Send_Client_Del
   (
    @Sr_Rfp_Lp_Send_Client_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.Sr_Rfp_Lp_Send_Client
	WHERE
		Sr_Rfp_Lp_Send_Client_Id = @Sr_Rfp_Lp_Send_Client_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Lp_Send_Client_Del] TO [CBMSApplication]
GO
