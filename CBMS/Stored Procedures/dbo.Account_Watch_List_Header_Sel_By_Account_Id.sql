SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Watch_List_Header_Sel_By_Account_Id                    
                  
Description:                  
			Showing Watch List - Header.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Id								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------

-- Supplier 

Exec Account_Watch_List_Header_Sel_By_Account_Id  @Account_Id=1655251

-- Utility

Exec Account_Watch_List_Header_Sel_By_Account_Id  @Account_Id=53655

                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/

CREATE PROCEDURE [dbo].[Account_Watch_List_Header_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE @Group_Legacy_Name TABLE
              (
                  GROUP_INFO_ID INT
                  , GROUP_NAME VARCHAR(200)
                  , Legacy_Group_Name VARCHAR(200)
              );

        INSERT INTO @Group_Legacy_Name
             (
                 GROUP_INFO_ID
                 , GROUP_NAME
                 , Legacy_Group_Name
             )
        EXEC dbo.Group_Info_Sel_By_Group_Legacy
            @Group_Legacy_Name_Cd_Value = 'Regulated_Market';


        SELECT
            ch.Client_Id
            , ch.Client_Name
            , LEFT(Sites.Site_Name, LEN(Sites.Site_Name) - 2) AS Site_Name
            , cha.Account_Id
            , ISNULL(cha.Account_Number, 'Not Yet Assigned') AS Account_Number
            , cha.Account_Type
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Contract_ID
            , cha.Account_Invoice_Source_Cd
            , c.Code_Value AS Account_Invoice_Source
            , ISNULL(us.UBM_SERVICE_NAME, 'None') AS UBM_SERVICE_NAME
            , CASE WHEN cha.Account_Group_ID IS NULL THEN 'Not part of Group Bill'
                  ELSE ISNULL(ag.GROUP_BILLING_NUMBER, 'Unspecified Group Number')
              END AS GROUP_BILLING_NUMBER
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Analyst
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN dbo.Code c
                ON c.Code_Id = cha.Account_Invoice_Source_Cd
            LEFT OUTER JOIN dbo.UBM_SERVICE AS us
                ON us.UBM_SERVICE_ID = ch.Ubm_Service_Id
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP AS ag
                ON ag.ACCOUNT_GROUP_ID = cha.Account_Group_ID
            LEFT JOIN(dbo.UTILITY_DETAIL ud
                      JOIN Utility_Detail_Analyst_Map udmap
                          ON udmap.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                      JOIN GROUP_INFO gi
                          ON gi.GROUP_INFO_ID = udmap.Group_Info_ID
                      INNER JOIN @Group_Legacy_Name gil
                          ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID)
                ON ud.VENDOR_ID = cha.Account_Vendor_Id
            LEFT OUTER JOIN USER_INFO ui
                ON ui.USER_INFO_ID = udmap.Analyst_ID
            CROSS APPLY (SELECT (   SELECT
                                        CAST(ch_Site.Site_name AS VARCHAR(MAX)) + '|'
                                        + CAST(ch_Site.Site_Id AS VARCHAR(MAX)) + '@$'
                                    FROM
                                        Core.Client_Hier ch_Site
                                        INNER JOIN Core.Client_Hier_Account cha_Acc
                                            ON cha_Acc.Client_Hier_Id = ch_Site.Client_Hier_Id
                                    WHERE
                                        cha_Acc.Account_Id = cha.Account_Id
                                    GROUP BY
                                        ch_Site.Site_name
                                        , ch_Site.Site_Id
                                    FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') Site_Name) Sites
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Id
            , ch.Client_Name
            , LEFT(Sites.Site_Name, LEN(Sites.Site_Name) - 2)
            , cha.Account_Id
            , cha.Account_Number
            , cha.Account_Type
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Supplier_Contract_ID
            , cha.Account_Invoice_Source_Cd
            , c.Code_Value
            , us.UBM_SERVICE_NAME
            , CASE WHEN cha.Account_Group_ID IS NULL THEN 'Not part of Group Bill'
                  ELSE ISNULL(ag.GROUP_BILLING_NUMBER, 'Unspecified Group Number')
              END
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME;



    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Watch_List_Header_Sel_By_Account_Id] TO [CBMSApplication]
GO
