SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_NYMEX_CLIENT_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec dbo.BUDGET_GET_NYMEX_CLIENT_LIST_P '' ,''



CREATE	PROCEDURE dbo.BUDGET_GET_NYMEX_CLIENT_LIST_P
	@userId varchar(10),
	@sessionId varchar(20)

	AS
begin
		set nocount on
		
		select	cli.client_name ,
			cli.client_id,
 		case	entity_name 
		when 	'January' then 1
		when 	'February'then 2
		when 	'March'	  then 3
		when 	'April'	  then 4
		when 	'May' 	  then 5
		when 	'June' 	  then 6
		when 	'July' 	  then 7
		when 	'August'   then 8
		when 	'September'then 9
		when 	'October'  then 10
		when 	'November' then 11
		when 	'December' then 12
		end fiscal_month

		from	client cli , 
			entity e
		where 	--cli.NOT_MANAGED = 0  
			cli.FISCALYEAR_STARTMONTH_TYPE_ID = e.entity_id
			and e.entity_type = 158
		ORDER BY CLIENT_NAME

		end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_NYMEX_CLIENT_LIST_P] TO [CBMSApplication]
GO
