SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Ubm_Batch_Master_Log_Sel_For_Dashboard_View

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------	     	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
--EXEC dbo.Ubm_Batch_Master_Log_Sel_For_Dashboard_View       


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------	        	
	NKOSURI		19-Aug-2009	created
								        	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.UBM_BATCH_MASTER_LOG_SEL_FOR_DASHBOARD_VIEW
AS

BEGIN

	SET NOCOUNT ON;

	WITH Cte_Ubm_Batch_Log AS
		(
		SELECT
			masterlog.UBM_BATCH_MASTER_LOG_ID,   
			u.UBM_NAME,       
			EXPECTED_IMAGE_RECORDS,       
			ACTUAL_IMAGE_RECORDS,       
			e.ENTITY_NAME,  
			CASE WHEN is_processed = 1 THEN 1 END AS EXPECTED_DATA_RECORDS,  
			CASE WHEN is_processed = 0 THEN 1 END AS ACTUAL_DATA_RECORDS  
		FROM
			dbo.ubm u 
			JOIN dbo.ubm_batch_master_log masterlog 
				ON u.ubm_id = masterlog.ubm_id        
			JOIN dbo.entity e 
				ON e.entity_id=masterlog.status_type_id    
			LEFT JOIN dbo.ubm_invoice inv 
				ON inv.ubm_batch_master_log_id = masterlog.ubm_batch_master_log_id  
		WHERE masterlog.End_date < GETDATE()
			AND masterlog.Start_date > GETDATE()- 1

		)
		SELECT
			UBM_BATCH_MASTER_LOG_ID BATCH_ID,
			UBM_NAME,
			ENTITY_NAME STATUS,
			COUNT(EXPECTED_DATA_RECORDS) AS EXPECTED_DATA_RECORDS,
			COUNT(ACTUAL_DATA_RECORDS) AS ACTUAL_DATA_RECORDS,
			EXPECTED_IMAGE_RECORDS,
			ACTUAL_IMAGE_RECORDS
		FROM
			Cte_Ubm_Batch_Log
		GROUP BY
			UBM_BATCH_MASTER_LOG_ID,
			UBM_NAME,
			EXPECTED_IMAGE_RECORDS,
			ACTUAL_IMAGE_RECORDS,     
			ENTITY_NAME

END
GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_MASTER_LOG_SEL_FOR_DASHBOARD_VIEW] TO [CBMSApplication]
GO
