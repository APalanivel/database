SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_BATCH_REPORT_IDENTITY_P
@userId varchar,
@sessionId varchar

as

select MAX(RM_REPORT_BATCH_ID) RM_REPORT_BATCH_ID

from	RM_REPORT_BATCH
GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_REPORT_IDENTITY_P] TO [CBMSApplication]
GO
