SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_RISK_DOCUMENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@riskDocumentId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE dbo.DELETE_RISK_DOCUMENT_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@riskDocumentId INT
AS
BEGIN

	SET NOCOUNT ON
	--declare @cbmsImageId int
	--select @cbmsImageId = cbms_image_id from RM_RISK_PLAN_PROFILE 
	--	where RM_RISK_PLAN_PROFILE_ID = @riskDocumentId

	DELETE dbo.RM_RISK_PLAN_PROFILE WHERE RM_RISK_PLAN_PROFILE_ID = @riskDocumentId

	--delete CBMS_IMAGE where cbms_image_id = @cbmsImageId
END
GO
GRANT EXECUTE ON  [dbo].[DELETE_RISK_DOCUMENT_P] TO [CBMSApplication]
GO
