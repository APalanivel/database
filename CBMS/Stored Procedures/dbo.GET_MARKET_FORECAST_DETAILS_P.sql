SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MARKET_FORECAST_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@asOfDate      	datetime  	          	
	@year          	int       	          	
	@marketId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.GET_MARKET_FORECAST_DETAILS_P -1,-1,'2007-10-23',2007,1



CREATE PROCEDURE dbo.GET_MARKET_FORECAST_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@asOfDate Datetime,
@year int,
@marketId int


as
--  DECLARE @forecastId int
--    PRINT @asOfDate
	set nocount on
IF @asOfDate IS NOT NULL 
BEGIN


	select b.AsofDate,
	b.hi_range_price, b.low_range_price, 
	b.forecast_year,b.forecast_quarter,
	e.entity_name
	from RM_MARKET a, RM_MARKET_FORECAST b,entity e
	where  a.RM_MARKET_ID=@marketId
              and a.RM_MARKET_ID=b.RM_MARKET_ID 
               and  b.AsofDate=@asOfDate
               and b.forecast_year=@year
               and e.entity_id= b.risk_tolerance_type_id
               and e.entity_type=1063

	      order by b.AsofDate,b.forecast_quarter
END
ELSE
	select b.AsofDate, 
	b.hi_range_price, b.low_range_price, b.forecast_year,b.forecast_quarter,e.entity_name
	from RM_MARKET a, RM_MARKET_FORECAST b,entity e
	where  a.RM_MARKET_ID=@marketId
                and a.RM_MARKET_ID=b.RM_MARKET_ID 
                and b.AsofDate=
		(
			SELECT MAX(AsofDate) FROM 
			RM_MARKET_FORECAST WHERE b.forecast_year=@year
		)
	
               and e.entity_id= b.risk_tolerance_type_id
               and e.entity_type=1063
GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_FORECAST_DETAILS_P] TO [CBMSApplication]
GO
