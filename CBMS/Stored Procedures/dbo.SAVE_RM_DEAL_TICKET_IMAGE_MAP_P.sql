SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SAVE_RM_DEAL_TICKET_IMAGE_MAP_P]
	@emailName VARCHAR(200),     
	@emailType INT,  
	@dealTicketId INT,  
	@getPkeyFlag BIT, 
	@cbmsImageId INT,
	@emailTypeId INT OUTPUT,  
	@pkey INT OUTPUT,
	@newcbmsImageId INT OUTPUT  
AS  
BEGIN  
    
	SET NOCOUNT ON  
  
	--DECLARE @emailTypeId INT  
  
	SELECT @emailTypeId = ENTITY_ID FROM dbo.ENTITY (NOLOCK) WHERE ENTITY_TYPE = @emailType  
		AND ENTITY_NAME = @emailName  
    
	SELECT @pkey = RM_DEAL_TICKET_IMAGE_MAP_ID  
		, @newcbmsImageId = CBMS_IMAGE_ID  
	FROM dbo.RM_DEAL_TICKET_IMAGE_MAP  
	WHERE RM_DEAL_TICKET_ID = @dealTicketId  
  
	IF @getPkeyFlag = 1  
	 BEGIN  
    
		INSERT INTO dbo.RM_DEAL_TICKET_IMAGE_MAP(RM_DEAL_TICKET_ID  
			, CBMS_IMAGE_ID  
			, TRANSACTION_DATE)  
		VALUES(@dealTicketId  
			, @cbmsImageId  
			, GETDATE())  
  
	 END  
    
	--SELECT @emailTypeId emailTypeId, @pkey pkey, @cbmsImageId cbmsImageId  
    
	RETURN  
END
GO
GRANT EXECUTE ON  [dbo].[SAVE_RM_DEAL_TICKET_IMAGE_MAP_P] TO [CBMSApplication]
GO
