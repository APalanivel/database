SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CEM_RMA_USER_INFO_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	RR			04-07-2019	GRM-Modified to refer permission in place of group
	HG			2019-07-26	Added NOCOUNT ON as Java was throwing error without that.
******/

CREATE PROCEDURE [dbo].[GET_CEM_RMA_USER_INFO_LIST_P]
      (
      @userId      VARCHAR      = NULL
    , @sessionId   VARCHAR      = NULL
    , @Key_Word    VARCHAR(200) = NULL
    , @Start_Index INT          = 1
    , @End_Index   INT          = 2147483647 )
AS
      BEGIN
            SET NOCOUNT ON;

            DECLARE @Tbl_Vendors AS TABLE
                  ( USER_INFO_ID  INT
                  , QUEUE_ID      INT
                  , USERNAME      VARCHAR(30)
                  , FIRST_NAME    VARCHAR(40)
                  , MIDDLE_NAME   VARCHAR(40)
                  , LAST_NAME     VARCHAR(40)
                  , EMAIL_ADDRESS VARCHAR(150)
                  , Row_Num       INT );
            INSERT INTO @Tbl_Vendors (
                                           USER_INFO_ID
                                         , QUEUE_ID
                                         , USERNAME
                                         , FIRST_NAME
                                         , MIDDLE_NAME
                                         , LAST_NAME
                                         , EMAIL_ADDRESS
                                         , Row_Num
                                     )
                        SELECT
                                    ui.USER_INFO_ID
                                  , ui.QUEUE_ID
                                  , ui.USERNAME
                                  , ui.FIRST_NAME
                                  , ui.MIDDLE_NAME
                                  , ui.LAST_NAME
                                  , ui.EMAIL_ADDRESS
                                  , row_number() OVER  ( ORDER BY ui.FIRST_NAME )
                        FROM        USER_INFO ui
                                    INNER JOIN
                                    USER_INFO_GROUP_INFO_MAP uigim
                                          ON uigim.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN
                                    dbo.GROUP_INFO_PERMISSION_INFO_MAP gipim
                                          ON gipim.GROUP_INFO_ID = uigim.GROUP_INFO_ID
                                    INNER JOIN
                                    dbo.PERMISSION_INFO pinfo
                                          ON pinfo.PERMISSION_INFO_ID = gipim.PERMISSION_INFO_ID
                        WHERE       (     @Key_Word IS NULL
                                          OR
                                                (     ui.USERNAME LIKE '%' + @Key_Word + '%'
                                                      OR    ui.FIRST_NAME LIKE '%' + @Key_Word + '%'
                                                      OR    ui.MIDDLE_NAME LIKE '%' + @Key_Word + '%'
                                                      OR    ui.LAST_NAME LIKE '%' + @Key_Word + '%'
                                                      OR    ui.EMAIL_ADDRESS LIKE '%' + @Key_Word + '%' ))
                                    AND   pinfo.PERMISSION_NAME IN (
                                                'menu.riskMgmt_new'
                                              , 'menu.riskMgmt')
                        GROUP BY    ui.USER_INFO_ID
                                  , ui.QUEUE_ID
                                  , ui.USERNAME
                                  , ui.FIRST_NAME
                                  , ui.MIDDLE_NAME
                                  , ui.LAST_NAME
                                  , ui.EMAIL_ADDRESS;

            SELECT
                        ui.USER_INFO_ID
                      , ui.QUEUE_ID
                      , ui.USERNAME
                      , ui.FIRST_NAME
                      , ui.MIDDLE_NAME
                      , ui.LAST_NAME
                      , ui.EMAIL_ADDRESS
            FROM        @Tbl_Vendors ui
            WHERE       Row_Num
            BETWEEN     @Start_Index AND @End_Index
            ORDER BY    Row_Num;

      END;

GO
GRANT EXECUTE ON  [dbo].[GET_CEM_RMA_USER_INFO_LIST_P] TO [CBMSApplication]
GO
