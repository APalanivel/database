SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Queue_Type_Upd         
                
Description:                
   This sproc to update Invoice_Collection_Queue  
                             
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
@Invoice_Collection_Queue_Id   varchar  
@User_Info_Id       INT  
@Invoice_Collection_Queue_Type_Cd        INT     
                   
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------   
  
  
BEGIN TRAN    
  
      
EXEC dbo.Invoice_Collection_Queue_Type_Upd   
      @Invoice_Collection_Queue_Id = 465  
     ,@User_Info_Id = 49  
     ,@Invoice_Collection_Queue_Type_Cd = 102342  
   
  
select * from Invoice_Collection_Queue where Invoice_Collection_Queue_id = 619  
  
ROLLBACK  
   
   
    
        
        
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    RKV    2017-01-31  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Type_Upd]  
      (   
       @Invoice_Collection_Queue_Id VARCHAR(MAX)  
      ,@User_Info_Id INT  
      ,@Invoice_Collection_Queue_Type_Cd INT )  
AS   
BEGIN  
      SET NOCOUNT ON   
         
        
      UPDATE  
            icq  
      SET     
            icq.Invoice_Collection_Queue_Type_Cd = @Invoice_Collection_Queue_Type_Cd  
      FROM  
            dbo.Invoice_Collection_Queue icq  
            INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') us  
                  ON us.Segments = icq.Invoice_Collection_Queue_Id 
END;  
  
  
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Type_Upd] TO [CBMSApplication]
GO
