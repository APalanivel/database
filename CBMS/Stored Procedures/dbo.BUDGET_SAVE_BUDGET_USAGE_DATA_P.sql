
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******
NAME:
	CBMS.dbo.BUDGET_SAVE_BUDGET_USAGE_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_Usage_Map	XML
	@Commodity_Id		INT
	@Unit_Type_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
DECLARE @Account_Usage_Map XML
SELECT @Account_Usage_Map = '
<root>
	<Acct val = "100">
		<Dtl Mnth="2012-01-01" Vol="26" Type="Last Year"/>
		<Dtl Mnth="2012-02-01" Vol="27" Type="2 Years Ago"/>
		<Dtl Mnth="2012-03-01" Vol="28" Type="Estimated"/>
	</Acct>
	<Acct val = "200">
		<Dtl Mnth="2012-01-01" Vol="26" Type="Last Year"/>
		<Dtl Mnth="2012-02-01" Vol="27" Type="2 Years Ago"/>
		<Dtl Mnth="2012-03-01" Vol="28" Type="Estimated"/>
	</Acct>
</root>
'
BEGIN TRAN
	EXEC [dbo].[BUDGET_SAVE_BUDGET_USAGE_DATA_P] @Account_Usage_Map, 290, 12
ROLLBACK	


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	 CPE		2013-03-19	Modified the SP for commercial Budget.
	 
******/

CREATE   PROCEDURE [dbo].[BUDGET_SAVE_BUDGET_USAGE_DATA_P]
      ( 
       @Account_Usage_Map XML
      ,@Commodity_Id INT
      ,@Unit_Type_Id INT )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE @Conv_Unit_Id INT

      SELECT
            @Conv_Unit_Id = Default_UOM_Entity_Type_Id
      FROM
            dbo.Commodity AS c
      WHERE
            Commodity_Id = @Commodity_Id
            
      MERGE INTO dbo.BUDGET_USAGE AS tgt
            USING 
                  ( SELECT
                        T.c.value('@val', 'INT') AS Account_Id
                       ,c.c.value('@Mnth', 'DATE') AS Month_Identifier
                       ,c.c.value('@Vol', 'DECIMAL(32,16)') * cuc.CONVERSION_FACTOR AS Volume
                       ,e.ENTITY_ID AS Budget_Usage_Type_Id
                    FROM
                        @Account_Usage_Map.nodes('/root/Acct') T ( c )
                        CROSS APPLY T.c.nodes('./Dtl') AS c ( c )
                        JOIN dbo.ENTITY e
                              ON e.ENTITY_NAME = c.c.value('@Type', 'VARCHAR(20)')
                                 AND e.ENTITY_DESCRIPTION = 'BUDGET_USAGE_TYPE'
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = @Unit_Type_Id
                                 AND cuc.CONVERTED_UNIT_ID = @Conv_Unit_Id ) AS src
            ON tgt.ACCOUNT_ID = src.Account_Id
                  AND tgt.MONTH_IDENTIFIER = src.Month_Identifier
                  AND tgt.COMMODITY_TYPE_ID = @Commodity_Id
            WHEN MATCHED 
                  THEN
UPDATE              SET 
                        BUDGET_USAGE_TYPE_ID = src.Budget_Usage_Type_Id
                       ,VOLUME = src.Volume
                       ,VOLUME_UNIT_TYPE_ID = @Conv_Unit_Id
            WHEN NOT MATCHED 
                  THEN
INSERT
                        ( 
                         ACCOUNT_ID
                        ,COMMODITY_TYPE_ID
                        ,BUDGET_USAGE_TYPE_ID
                        ,MONTH_IDENTIFIER
                        ,VOLUME
                        ,VOLUME_UNIT_TYPE_ID )
                    VALUES
                        ( 
                         src.Account_Id
                        ,@Commodity_Id
                        ,src.Budget_Usage_Type_Id
                        ,src.Month_Identifier
                        ,src.Volume
                        ,@Conv_Unit_Id );
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_BUDGET_USAGE_DATA_P] TO [CBMSApplication]
GO
