SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsSsoMisGlossary_RandomWord]
	( @MyAccountId int
	)
AS
BEGIN



	set nocount on

	declare @RecordCount int
	declare @Min int
	declare @Max int
	declare @RandID int

	  select @RecordCount = count(*)
	    from sso_mis_glossary

	set @Min = 1
	set @Max = @RecordCount

	select @RandID = cast((@Min + (SELECT RAND() as RandNumber) * (@Max-@Min)) as int)


	if @RandID > @Min and @RandID < @Max
	begin

		   select sso_mis_glossary_id
			, term
			, definition
		     from sso_mis_glossary
		    where sso_mis_glossary_id = @RandID
	end


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoMisGlossary_RandomWord] TO [CBMSApplication]
GO
