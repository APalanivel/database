SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[cbmsCuExceptionDenorm_Insert]  
     
DESCRIPTION: 


INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@cu_invoice_id		INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
SKA		Shobhit Kumar Agrawal
RKV     Ravi Kumar Vegesna
AP		ARUNKUMAR PALANIVEL

MODIFICATION
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
SKA			10/21/2010	Created
RKV			2015-08-20	Added New Columns Commodity_Id,Recalc_Failed_Reason_Type_cd as Part Of AS400-II
RKV			2016-01-12	Added New Parameter Recalc_Response_Error_Dsc
AP			AUG 30, 2019	ADDED NEW COLUMN IN CU EXCEPTION DENORM TABLE. THE SAME HAS BEEN ADDED IN THIS PROCEDURE AS NEW PARAMETER FOR INSERT
AP			DEC 13, 2019	MAINT-9668 , For the manually inserted images Cu_Invoice_Event table gets entry after the exception created thus taking the date from Cu_Exception if it doesn't exist in Cu_Invoice_Event.
*/
CREATE PROCEDURE [dbo].[cbmsCuExceptionDenorm_Insert]
      (
      @cbms_image_id                INT
    , @cbms_doc_id                  VARCHAR(200)
    , @cu_invoice_id                INT
    , @queue_id                     INT
    , @exception_type               VARCHAR(200)
    , @exception_status_type        VARCHAR(200)
    , @UBM_Account_Number           VARCHAR(200)  = NULL
    , @service_month                VARCHAR(50)   = NULL
    , @UBM_Client_Name              VARCHAR(200)  = NULL
    , @UBM_Site_Name                VARCHAR(200)  = NULL
    , @UBM_State_Name               VARCHAR(200)  = NULL
    , @UBM_City                     VARCHAR(200)  = NULL
    , @is_manual                    BIT
    , @date_in_queue                DATETIME
    , @Client_Hier_ID               INT
    , @Account_ID                   INT
    , @sort_order                   INT           = 1
    , @Commodity_Id                 INT           = NULL
    , @Recalc_Failed_Reason_Type_cd INT           = NULL
    , @Recalc_Type_Cd               INT           = NULL
    , @Recalc_Response_Error_Dsc    NVARCHAR(MAX) = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE @Date_In_CBMS DATETIME;

            SET @Date_In_CBMS = isnull((     SELECT
                                                      min(EVENT_DATE)
                                             FROM     CU_INVOICE_EVENT
                                             WHERE    CU_INVOICE_ID = @cu_invoice_id )
                                     , (     SELECT   TOP ( 1 )
                                                      OPENED_DATE
                                             FROM     dbo.CU_EXCEPTION
                                             WHERE    CU_INVOICE_ID = @cu_invoice_id ));

            INSERT INTO CU_EXCEPTION_DENORM (
                                                  CBMS_IMAGE_ID   --1
                                                , CBMS_DOC_ID
                                                , CU_INVOICE_ID
                                                , QUEUE_ID
                                                , EXCEPTION_TYPE
                                                , EXCEPTION_STATUS_TYPE
                                                , UBM_Account_Number
                                                , SERVICE_MONTH
                                                , UBM_Client_Name
                                                , UBM_Site_Name   --10
                                                , UBM_State_Name
                                                , UBM_City
                                                , IS_MANUAL
                                                , DATE_IN_QUEUE
                                                , SORT_ORDER
                                                , Client_Hier_ID
                                                , Account_ID      --17
                                                , Commodity_Id
                                                , Recalc_Failed_Reason_Type_cd
                                                , Recalc_Type_cd
                                                , Recalc_Response_Error_Dsc
                                                , Date_In_CBMS
                                            )
            VALUES
                 ( @cbms_image_id                                                                                                                                             --1
                 , @cbms_doc_id, @cu_invoice_id, @queue_id, @exception_type, @exception_status_type, @UBM_Account_Number, @service_month, @UBM_Client_Name, @UBM_Site_Name    --10
                 , @UBM_State_Name, @UBM_City, @is_manual, @date_in_queue, @sort_order, @Client_Hier_ID, @Account_ID                                                          --17
                 , @Commodity_Id, @Recalc_Failed_Reason_Type_cd, @Recalc_Type_Cd, @Recalc_Response_Error_Dsc, @Date_In_CBMS );

            EXEC cbmsCuExceptionDenorm_Get
                  @cu_invoice_id;

      END;
      ;
      ;


GO

GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDenorm_Insert] TO [CBMSApplication]
GO
