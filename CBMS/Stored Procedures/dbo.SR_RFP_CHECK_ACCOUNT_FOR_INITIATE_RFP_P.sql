SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec SR_RFP_CHECK_ACCOUNT_FOR_INITIATE_RFP_P 94273, 291
CREATE PROCEDURE [dbo].[SR_RFP_CHECK_ACCOUNT_FOR_INITIATE_RFP_P]
	@accountId INT,
	@commodityId INT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @entityID INT
	
	SELECT @entityID = entity_id FROM dbo.entity WHERE entity_name = 'Closed' AND entity_type = 1029
  
	SELECT rfp.commodity_type_id
	FROM dbo.sr_rfp_account rfpAcc
		JOIN dbo.sr_rfp rfp ON rfp.sr_rfp_id = rfpAcc.sr_rfp_id
	WHERE rfpAcc.account_id=  @accountId
		AND rfpAcc.bid_status_type_id <> @entityID
		AND rfpAcc.is_deleted = 0
		AND rfp.commodity_type_id = @commodityId

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_ACCOUNT_FOR_INITIATE_RFP_P] TO [CBMSApplication]
GO
