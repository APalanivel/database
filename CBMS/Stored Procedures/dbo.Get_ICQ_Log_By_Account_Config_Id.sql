SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Get_ICQ_Log_by_Account_config_Id
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  

OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

		EXEC [Get_ICQ_Log_by_Account_config_Id] 
			@Invoice_Collection_Account_Config_id = 553

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	HG			Hariharasuthan Ganesan

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	HG			2017-03-31	IC 
******/                       
CREATE PROCEDURE [dbo].[Get_ICQ_Log_By_Account_Config_Id]
      (
       @Invoice_Collection_Account_Config_id INT )
AS
BEGIN                    
      SET NOCOUNT ON                     
       
      SELECT
            ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS UserName
           ,ic.Log_message
           ,ic.created_ts AS Logged_Date
      FROM
            dbo.ICQ_log ic
            JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = ic.User_info_id
      WHERE
            ic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_id
      ORDER BY
            ic.created_ts 

END                    
;
GO
GRANT EXECUTE ON  [dbo].[Get_ICQ_Log_By_Account_Config_Id] TO [CBMSApplication]
GO
