SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.UPDATE_TIER_DETAIL_P 
	@lowerBound DECIMAL(32,16),
	@upperBound DECIMAL(32,16),
	@tierNumber INT,
	@chargeSeasonMapId INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.tier_detail
	SET lower_bound = @lowerBound , 
		upper_bound = @upperBound, 
		tier_number = @tierNumber 
	WHERE charge_season_map_id = @chargeSeasonMapId

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_TIER_DETAIL_P] TO [CBMSApplication]
GO
