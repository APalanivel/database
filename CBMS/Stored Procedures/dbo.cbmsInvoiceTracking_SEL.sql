SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**********              
NAME:              
 dbo.cbmsInvoiceTracking_SEL               
              
DESCRIPTION:              
   This is a Merged Report for both Scraping and Invoice_By_Exception.              
              
INPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------                                
  @DocId  INT =  NULL,                                  
  @FileName  VARCHAR(100) =  NULL,                      
  @BatchId  INT =  NULL,                                  
  @CuInvoiceId  INT =  NULL,                                  
  @CountryId INT =  NULL,                                  
  @StateId  INT =  NULL,                                  
  @ClientId  INT =  NULL,                                  
  @DivisionId INT =  NULL,                                  
  @SiteId  INT =  NULL,                                  
  @AccountId INT =  NULL,                                  
  @VendorTypeId INT =  NULL,                                  
  @VendorId  INT =  NULL,                                  
  @ExceptionTypeId INT =    NULL,                                  
  @AnalystId INT =  NULL,                                  
  @BeginDate DATETIME =  NULL,                                  
  @EndDate  DATETIME =  NULL,                                  
  @MonthPosted  INT =  NULL,                                  
  @YearPosted INT =  NULL,                                  
  @IsProcessed  INT =  NULL,                                  
  @inv_source_keyword VARCHAR(1000) = NULL,                                  
  @SourceCodeId INT =  NULL,                                              
  @statustypeid INT =  NULL                  
              
OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
                   
USAGE EXAMPLES:              
------------------------------------------------------------      
[USE CBMS_TK3]    
    
    
exec dbo.cbmsInvoiceTracking_SEL      
      @DocId = NULL      
     ,@FileName = NULL      
     ,@BatchId = NULL      
     ,@CuInvoiceId = 81287315      
     ,@ClientId = null      
     ,@AccountId = NULL      
     ,@ExceptionTypeId = NULL      
     ,@BeginDate = NULL      
     ,@EndDate = NULL      
     ,@MonthPosted = NULL      
     ,@YearPosted = NULL      
     ,@IsProcessed = null      
     ,@inv_source_keyword = NULL      
     ,@SourceCodeId = null     
     ,@statustypeid = null      
     ,@inv_source_label = NULL      
     ,@User_info_ID = 49                
    
    
    
    
exec dbo.cbmsInvoiceTracking_SEL    
      @DocId = NULL    
     ,@FileName = NULL    
     ,@BatchId = NULL    
     ,@CuInvoiceId = NULL    
     ,@ClientId = 190    
     ,@AccountId = NULL    
     ,@ExceptionTypeId = NULL    
     ,@BeginDate = NULL    
     ,@EndDate = NULL    
     ,@MonthPosted = NULL    
     ,@YearPosted = NULL    
     ,@IsProcessed = 1    
     ,@inv_source_keyword = NULL    
     ,@SourceCodeId = 6    
     ,@statustypeid = 1250    
     ,@inv_source_label = NULL    
     ,@User_info_ID = 1                
    
    
exec dbo.cbmsInvoiceTracking_SEL1    
      @DocId = NULL    
     ,@FileName = NULL    
     ,@BatchId = NULL    
     ,@CuInvoiceId = NULL    
     ,@ClientId = 190    
     ,@AccountId = NULL    
     ,@ExceptionTypeId = NULL    
     ,@BeginDate = NULL    
     ,@EndDate = NULL    
     ,@MonthPosted = NULL    
     ,@YearPosted = NULL    
     ,@IsProcessed = 1    
     ,@inv_source_keyword = NULL    
     ,@SourceCodeId = 6    
     ,@statustypeid = 1250    
     ,@inv_source_label = NULL    
     ,@User_info_ID = 1     
    
exec cbmsInvoiceTracking_SEL @ClientId=11806,@BeginDate=NULL,@EndDate=NULL,@User_info_ID=41550    
exec cbmsInvoiceTracking_SEL @ClientId=11806,@BeginDate=NULL,@EndDate=NULL,@IsProcessed=0,@User_info_ID=41550    
exec cbmsInvoiceTracking_SEL @ClientId=13087,@BeginDate=NULL,@EndDate=NULL,@User_info_ID=39377    
exec cbmsInvoiceTracking_SEL @ClientId=12530,@BeginDate='2014-04-01 00:00:00',@EndDate='2014-05-31 00:00:00',@User_info_ID=43081    
    
exec cbmsInvoiceTracking_SEL @BatchId=17024    
exec cbmsInvoiceTracking_SEL    
      @BatchId = 16943    
     ,@ClientId = 11370    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = '424d0232'    
    ,@BatchId  = 17057    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = '424d0232'    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = '424d0232'    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = '2335446_SKM454e14052209500KM454e14072814440_0729201406071027_4.pdf'    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = '1e52b897-b64d-ddb5-a951-22000b94093e'    
    
EXEC [cbmsInvoiceTracking_SEL]    
 @FileName = 'abc'    
    
 use cbms    
    
exec dbo.cbmsInvoiceTracking_SEL    
@DocId = NULL    
,@FileName = 'text'    
,@BatchId = NULL    
,@CuInvoiceId = NULL    
,@ClientId = null    
,@AccountId = NULL    
,@ExceptionTypeId = NULL    
,@BeginDate = NULL    
,@EndDate =NULL    
,@MonthPosted = NULL    
,@YearPosted = NULL    
,@IsProcessed = null    
,@inv_source_keyword = NULL    
,@SourceCodeId = null    
,@statustypeid = null    
,@inv_source_label = NULL    
,@User_info_ID = 49    
    
    
AUTHOR INITIALS:              
 Initials Name    
------------------------------------------------------------    
 KC   Kailash Chowdhary              
 SS   Subhash Subramanyam              
 KS   Khushwath Shah              
 NK   Nageswara Rao Kosuri              
    
MODIFICATIONS    
 Initials Date  Modification    
------------------------------------------------------------    
 KC & SS    7/20/2009 Created the SP to merge scraping and exception related SProc    
 SS   8/20/2009 Modified to acheive expected results  (Added Full join to ubm_batch_master_log_id and account_number along with cbms_image_id)    
 KC & SS 9/01/2009 Fixed bug  - , Eliminated redundant rows, Prokarma Records uncommon to Invoice_Exception now displaying address details,        
         Time Constraints    
 SS   9/07/2009 Added missing filter @inv_source_label as input parameter, eliminated distinct from the second query,    
      Added @SourceCodeId, @statustypeid, @inv_source_label as Global Filter, Removed unused filters    
 SS   9/08/2009 Fixed Issues - Sent_Date / Received Date, Batch_ID 5879, Batch 5806    
 SS   9/09/2009 Fixed Issues -  11517 , 11518. ( 11528 was not a bug as it had different track_id for the records)    
 HG   9/09/2009 Removed Unused parameters, Added MAX of INV_SOURCED_IMAGE_TRACK_ID to elminated redundancy (fix for 11528)    
 SS   9/10/2009 Cu_Invoice_Id filter added    
 SS   9/14/2009 Resolved the reopened issue about redundant rows. Added COALESCE for joins on account_number and ubm_batch_master_log_id    
 SS   9/15/2009 Query Tuning: 1. Filters which are part of outermost query added to respective innermost queries removing Outermost Where condition    
         2. Filters exceptional to individual queries added    
      Changes: Like operator added to Invoice_Source_Keyword filter    
    
 NK & SS 9/16/2009 @MyAccountId removed, @isProcessed type changed to bit, Derived queries use Temp Table / Table variable    
      All outermost query filters of Exception_Report are moved to innermost query              
      Eliminated use of meter and country              
 GP   9/16/2009 Changed logic to be dynamic and only use the appropriate tables.        
 NK   9/25/2009 Changed Sent date and Received logic and also replaced global temp table with local temp table         
 NK   9/29/2009 Modified to get city state for suplier accounts    
 SS   10/9/2009 Using contract_id in place of account_id to join Supplier_account_meter_map and Contract table    
 HG   12/23/2009 Added the condition map.Is_History = 0 to select only the accounts which doesn't have any history.    
 SKA  04/22/2010 Moved the where condition for @IsProcessed to the final select statement as it is causing the issue while using in dynamic statement due to conversioning     SKA  04/23/2010  Removed CU_invoice(Account_id) with Cu_invoice_service_month(a
ccount_id)    
 SSR  05/21/2010  Added Account table to get Account level Vendor Names.    
 DMR  09/10/2010 Modified for Quoted_Identifier    
 RR   2015-01-20 MAINT-3328 Added new input parameter @User_info_ID for production support    
 DMR  02/24/2015 MAINT-2782 & Maint-3386  Performance tuning by implementation of FullTextIndexing.    
 HG   2015-05-28 MAINT-2782, replaced the base tables with Client_Hier_Account, Client table Join is not replaced with CHA and CH as we can have invoice with out accout maping.    
 HG   2016-12-16 MAINT-4767, Since the Full text search is not finding the image id if the file_name is too long changed the logic to ignore the file_name(set to null) and use Invoice id to return the details.    
 HG   2017-02-21 MAINT-4711, Modified the sproc to use the full text search only if the number of splits returned by FT parser is less than or equal to 4 otherwise LIKE search will be used to find the image names starts with the given string.    
 NR   2017-05-26 MAINT-5412 Modified LIKE search will be used to find the image names partial search with part of the given string.    
*********/    
CREATE PROCEDURE [dbo].[cbmsInvoiceTracking_SEL]    
(    
    @DocId INT = NULL,    
    @FileName VARCHAR(100) = NULL,    
    @BatchId INT = NULL,    
    @CuInvoiceId INT = NULL,    
    @ClientId INT = NULL,    
    @AccountId INT = NULL,    
    @ExceptionTypeId INT = NULL,    
    @BeginDate DATETIME = NULL,    
    @EndDate DATETIME = NULL,    
    @MonthPosted INT = NULL,    
    @YearPosted INT = NULL,    
    @IsProcessed BIT = NULL,    
    @inv_source_keyword VARCHAR(1000) = NULL,    
    @SourceCodeId INT = NULL,    
    @statustypeid INT = NULL,    
    @inv_source_label VARCHAR(200) = NULL,    
    @User_info_ID INT = NULL    
)    
WITH RECOMPILE    
AS    
BEGIN    
    
    SET NOCOUNT ON;    
    
    -- When the user searching based on the File name this table would be populated and joined with the main query, this is done seperately as FT search not working fine with all the table joins    
    CREATE TABLE #Cbms_Image    
    (    
        Cbms_Image_Id INT PRIMARY KEY CLUSTERED,    
        Cbms_Doc_Id VARCHAR(200)    
    );    
    
    /**********************************************************************************************     
   NOTE:  2/14/2015   DMR    
   Some parameters other than IsProcessed must be selected, otherwise throw error. @User_Info_ID    
   is not evaluated as it is intentionally an unused parameter.    
       
    There is a bug in the code where it is either allowing or sending no parameters resulting     
    in 8 min runtimes.  This RaisError statement will check before executing and hopefullly help     
    us identify where in the code it allows no parameters.            
   **********************************************************************************************/    
    IF COALESCE(    
                   CAST(@DocId AS VARCHAR(255)),    
                   @FileName,    
                   CAST(@BatchId AS VARCHAR(255)),    
                   CAST(@CuInvoiceId AS VARCHAR(255)),    
                   CAST(@ClientId AS VARCHAR(255)),    
                   CAST(@AccountId AS VARCHAR(255)),    
                   CAST(@ExceptionTypeId AS VARCHAR(255)),    
                   CAST(@BeginDate AS VARCHAR(255)),    
                   CAST(@EndDate AS VARCHAR(255)),    
                   CAST(@MonthPosted AS VARCHAR(255)),    
                   CAST(@YearPosted AS VARCHAR(255)),    
                   @inv_source_keyword,    
                   CAST(@SourceCodeId AS VARCHAR(255)),    
                   CAST(@statustypeid AS VARCHAR(255)),    
                   @inv_source_label    
               ) IS NULL    
    BEGIN    
        RAISERROR('You must select additional parameters', 16, 0) WITH NOWAIT;    
        RETURN 99;    
    END;    
    
    SET @FileName = CASE    
                        WHEN @CuInvoiceId IS NOT NULL THEN    
                      NULL    
                        ELSE    
                            @FileName    
                    END;    
    
    DECLARE @SQL_INSERT VARCHAR(MAX),    
            @SQL_SELECT VARCHAR(MAX),    
            @SQL_FROM VARCHAR(MAX),    
            @SQL_WHERE VARCHAR(MAX),    
            @SQL_GROUP_BY VARCHAR(MAX),    
            @SQL NVARCHAR(MAX),    
            @CBMS_File_Name_Search VARCHAR(200) = '"*' + dbo.udf_StripNonAlphaNumerics(@FileName) + '*"',    
            @SQL_Cbms_Image VARCHAR(MAX),    
            @FT_Parser_String_Split_Cnt INT = 0,    
            @FT_Split_Threshold_Cnt INT;    
    
    CREATE TABLE #TEMP_INVOICE_TRACKING    
    (    
        CBMS_IMAGE_ID INT,    
        CBMS_DOC_ID VARCHAR(200),    
        CU_INVOICE_ID INT,    
        INV_SOURCED_IMAGE_TRACK_ID INT,    
        INV_SOURCE_LABEL VARCHAR(200),    
        SENT_DATE DATETIME,    
        RECEIVED_DATE DATETIME,    
        STATUS_TYPE VARCHAR(200),    
        ACCOUNT_ID INT,    
        ACCOUNT_NUMBER VARCHAR(200),    
        SERVICE_MONTH DATETIME,    
        CLIENT_NAME VARCHAR(200),    
        CITY_STATE VARCHAR(200),    
        VENDOR_NAME VARCHAR(200),    
        QUEUE VARCHAR(200),    
        STATUS VARCHAR(100),    
        UBM_BATCH_MASTER_LOG_ID INT,    
        EXCEPTION_COMMENTS VARCHAR(MAX),    
        PROKARMA_STATUS VARCHAR(200),    
        IS_PROCESSED BIT,    
        Contract_Id INT NULL    
    );    
    DECLARE @FTS_String TABLE    
    (    
        Parsed_String NVARCHAR(MAX)    
    );    
    
    SELECT @FT_Split_Threshold_Cnt = App_Config_Value    
    FROM dbo.App_Config    
    WHERE App_Config_Cd = 'FT_Split_Threshold_Cnt';    
    
    IF @FileName IS NOT NULL    
    BEGIN    
        INSERT INTO @FTS_String    
        (    
            Parsed_String    
        )    
        EXEC dbo.FTS_String_Parser @Object_Name = 'Cbms_Image',    
                                   @Keyword = @CBMS_File_Name_Search;    
    
        SELECT @FT_Parser_String_Split_Cnt = COUNT(1)    
        FROM @FTS_String;    
    END;    
    
    SELECT @BeginDate = CASE    
                            WHEN @BeginDate IS NULL THEN    
                                '1/1/2005'    
                            ELSE    
                                @BeginDate    
                        END;    
    
    SELECT @EndDate = CASE    
                          WHEN @EndDate IS NOT NULL THEN    
                              DATEADD(SS, -1, DATEADD(D, 1, CAST(CONVERT(VARCHAR(12), @EndDate, 101) AS DATETIME)))    
                          ELSE    
                              '1/1/' + CONVERT(VARCHAR, YEAR(GETDATE()) + 1)    
                      END;    
    
    SET @SQL_Cbms_Image    
        = '    
   INSERT INTO #Cbms_Image    
    ( Cbms_Image_Id, Cbms_Doc_Id)    
   SELECT    
    ci.Cbms_Image_Id    
    ,ci.Cbms_Doc_Id    
   FROM    
    dbo.Cbms_Image ci    
   WHERE    
    1 = 1 ';    
    
    IF @DocId IS NOT NULL    
        SET @SQL_Cbms_Image = @SQL_Cbms_Image + ' AND (ci.CBMS_IMAGE_ID = ' + CONVERT(VARCHAR, @DocId) + ') ';    
    
    IF @FileName IS NOT NULL    
    BEGIN    
    
        IF @FT_Parser_String_Split_Cnt <= @FT_Split_Threshold_Cnt    
        BEGIN    
    
    --        SET @SQL_Cbms_Image    
    --            = @SQL_Cbms_Image + ' AND CONTAINS((ci.CBMS_DOC_ID_FTSearch,ci.CBMS_DOC_ID),'''    
    --              + @CBMS_File_Name_Search + ''')';    
    --    END;    
    --    ELSE    
    --    BEGIN    
    --        SET @SQL_Cbms_Image = @SQL_Cbms_Image + ' AND ci.CBMS_DOC_ID LIKE ' + '''%' + @FileName + '%''';    
    --    END;    
    --END;    
    
	    IF LEN(@CBMS_File_Name_Search)<=64  
       BEGIN       
    
                  
      SET @SQL_Cbms_Image = @SQL_Cbms_Image        
                                              + ' AND CONTAINS((ci.CBMS_DOC_ID_FTSearch,ci.CBMS_DOC_ID),'''        
                                              + @CBMS_File_Name_Search + ''')'      
              
      END        
      ELSE       
      BEGIN       
   
 
      SET @SQL_Cbms_Image = @SQL_Cbms_Image + ' AND ci.CBMS_DOC_ID LIKE ' + '''%' + @FileName + '%''';       
      END           
                     
                    END;        
                ELSE        
                    BEGIN        
       
                        SET @SQL_Cbms_Image = @SQL_Cbms_Image + ' AND ci.CBMS_DOC_ID LIKE ' + '''%' + @FileName + '%''';        
                    END;        
            END;    


    SET @SQL_Cbms_Image = CASE    
                              WHEN @DocId IS NULL    
                                   AND @FileName IS NULL THEN    
                                  NULL    
                              ELSE    
                                  @SQL_Cbms_Image    
                          END;    
    EXEC (@SQL_Cbms_Image);    
    
    
    SET @SQL_INSERT = 'INSERT INTO #TEMP_INVOICE_TRACKING ';    
    SET @SQL_SELECT    
        = 'SELECT    
        ci.CBMS_IMAGE_ID    
        ,ci.CBMS_DOC_ID    
        ,cu.CU_INVOICE_ID    
        ,MAX(COALESCE(isir.INV_SOURCED_IMAGE_TRACK_ID,0)) AS INV_SOURCED_IMAGE_TRACK_ID    
        ,isir.INV_SOURCE_LABEL    
        ,CONVERT(VARCHAR(12),isir.SENT_DATE,101) AS SENT_DATE    
        ,CONVERT(VARCHAR(12),isir.RECEIVED_DATE,101) AS RECEIVED_DATE    
        ,isir.STATUS_TYPE    
        ,cism.ACCOUNT_ID    
        ,cha.Account_Number     
        ,cism.SERVICE_MONTH    
        ,c.CLIENT_NAME    
        ,cha.Meter_City + '','' + cha.Meter_State_Name AS CITY_STATE    
        ,cha.Account_Vendor_Name AS Vendor_Name    
        ,q.queue_name as QUEUE    
        ,isir.STATUS_TYPE AS STATUS    
        ,ui.UBM_BATCH_MASTER_LOG_ID    
        ,isir.EXCEPTION_COMMENTS    
        ,COALESCE(isir.PROKARMA_FILENAME,''No'') AS PROKARMA_STATUS    
        ,cu.IS_PROCESSED    
        ,cha.Supplier_Contract_ID    
        ';    
    
    SET @SQL_FROM    
        = ' FROM     
       dbo.CU_INVOICE cu     
       LEFT OUTER JOIN dbo.UBM_INVOICE ui     
        ON ui.UBM_INVOICE_ID = cu.UBM_INVOICE_ID    
       ';    
    SET @SQL_FROM    
        = @SQL_FROM + CASE    
                          WHEN @DocId IS NULL    
                               AND @FileName IS NULL THEN    
                              ' LEFT OUTER JOIN dbo.Cbms_Image ci '    
                          ELSE    
                              ' INNER JOIN #Cbms_Image ci '    
                      END + ' ON ci.CBMS_IMAGE_ID = cu.CBMS_IMAGE_ID '    
          + 'LEFT OUTER JOIN dbo.INV_SOURCED_IMAGE_REPORT isir    
        ON isir.CBMS_IMAGE_ID = cu.CBMS_IMAGE_ID    
         AND ' + '((isir.SENT_DATE BETWEEN ''' + CONVERT(VARCHAR, @BeginDate) + ''' AND '''    
          + CONVERT(VARCHAR, @EndDate) + ''')    
           OR (isir.RECEIVED_DATE BETWEEN ''' + CONVERT(VARCHAR, @BeginDate)    
          + ''' AND ''' + CONVERT(VARCHAR, @EndDate)    
          + '''))    
       LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism    
        ON cism.CU_INVOICE_ID = cu.CU_INVOICE_ID    
    
       LEFT OUTER JOIN CU_EXCEPTION ce    
        ON ce.CU_INVOICE_ID = cu.CU_INVOICE_ID    
       LEFT OUTER JOIN dbo.CU_EXCEPTION_DETAIL ced    
        ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID     
         AND ' + '(ced.OPENED_DATE >= ''' + CONVERT(VARCHAR, @BeginDate)    
          + ''')     
         AND (ced.OPENED_DATE <= ''' + CONVERT(VARCHAR, @EndDate)    
          + ''')     
       LEFT OUTER JOIN dbo.vwCbmsQueueName q    
        ON q.queue_id = ce.QUEUE_ID    
     
       LEFT OUTER JOIN dbo.CLIENT c    
        ON c.CLIENT_ID = cu.CLIENT_ID    
           
       LEFT OUTER JOIN core.Client_Hier_Account cha    
        ON cha.Account_Id = cism.Account_Id    
        ';    
    
    SET @SQL_WHERE = ' WHERE 1 = 1 ';    
    
    
    IF @BatchId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (ui.UBM_BATCH_MASTER_LOG_ID = ' + CONVERT(VARCHAR, @BatchId) + ') ';    
    IF @CuInvoiceId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (cu.CU_INVOICE_ID = ' + CONVERT(VARCHAR, @CuInvoiceId) + ') ';    
    IF @ClientId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (c.CLIENT_ID = ' + CONVERT(VARCHAR, @ClientId) + ') ';    
    IF @AccountId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (cha.ACCOUNT_ID = ' + CONVERT(VARCHAR, @AccountId) + ') ';    
    IF @ExceptionTypeId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (ced.EXCEPTION_TYPE_ID = ' + CONVERT(VARCHAR, @ExceptionTypeId) + ') ';    
    IF @MonthPosted IS NOT NULL    
        SET @SQL_WHERE    
            = @SQL_WHERE + ' AND ((DATEPART(MM, cism.SERVICE_MONTH) = ' + CONVERT(VARCHAR, @MonthPosted) + ') ';    
    IF @YearPosted IS NOT NULL    
        SET @SQL_WHERE    
            = @SQL_WHERE + ' AND ((DATEPART(YYYY, cism.SERVICE_MONTH) = ' + CONVERT(VARCHAR, @YearPosted) + ') ';    
    IF @inv_source_keyword IS NOT NULL    
        SET @SQL_WHERE    
            = @SQL_WHERE + ' AND (isir.INV_SOURCE_KEYWORD LIKE ''%' + CONVERT(VARCHAR, @inv_source_keyword) + '%'')';    
    IF @SourceCodeId IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.INV_SOURCE_ID = ' + CONVERT(VARCHAR, @SourceCodeId) + ') ';    
    IF @statustypeid IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.STATUS_TYPE_ID = ' + CONVERT(VARCHAR, @statustypeid) + ') ';    
    IF @inv_source_label IS NOT NULL    
        SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.INV_SOURCE_LABEL = ' + CONVERT(VARCHAR, @inv_source_label) + ') ';    
    
    SET @SQL_GROUP_BY    
        = ' GROUP BY     
        ci.CBMS_IMAGE_ID    
        ,ci.CBMS_DOC_ID    
        ,cu.CU_INVOICE_ID    
        ,isir.INV_SOURCE_LABEL    
        ,isir.SENT_DATE    
        ,isir.RECEIVED_DATE    
        ,isir.STATUS_TYPE    
        ,isir.Account_Number    
        ,cism.ACCOUNT_ID    
        ,cha.Account_Number    
        ,cism.SERVICE_MONTH    
        ,c.CLIENT_NAME    
        ,cha.Meter_City + '','' + cha.Meter_State_Name    
        ,cha.Account_Vendor_Name    
        ,q.queue_name    
        ,isir.STATUS_TYPE    
        ,ui.UBM_BATCH_MASTER_LOG_ID    
        ,isir.EXCEPTION_COMMENTS    
        ,COALESCE(isir.PROKARMA_FILENAME, ''No'')    
        ,cu.IS_PROCESSED    
        ,cha.Supplier_Contract_ID';    
    
    SELECT @SQL = @SQL_INSERT + @SQL_SELECT + @SQL_FROM + @SQL_WHERE + @SQL_GROUP_BY;    
    
    EXEC sp_executesql @SQL;    
    -----------------------------------------------------------------------------------------------------------------------              
    
    IF @CuInvoiceId IS NULL    
    BEGIN    
    
        -- Secondly, dynamically create a SQL statement using the INV_SOURCED_IMAGE_REPORT table as the primary table.  Only insert              
        -- rows that do not already exist in the #TEMP_INVOICE_TRACKING table.    
    
        SET @SQL_SELECT    
            = 'SELECT     
          isir.CBMS_IMAGE_ID    
          ,COALESCE(isir.ORIGINAL_FILENAME, isir.PROKARMA_FILENAME)    
          ,cu.CU_INVOICE_ID    
          ,MAX(COALESCE(isir.INV_SOURCED_IMAGE_TRACK_ID, 0)) AS INV_SOURCED_IMAGE_TRACK_ID    
          ,isir.INV_SOURCE_LABEL    
          ,CONVERT(VARCHAR(12),isir.SENT_DATE,101) AS SENT_DATE    
          ,CONVERT(VARCHAR(12),isir.RECEIVED_DATE,101) AS RECEIVED_DATE    
          ,isir.STATUS_TYPE    
          ,cism.ACCOUNT_ID    
          ,cha.Account_Number    
          ,cism.SERVICE_MONTH    
          ,c.CLIENT_NAME    
          ,cha.Meter_City + '','' + cha.Meter_State_Name AS CITY_STATE    
          ,cha.Account_Vendor_Name AS Vendor_Name    
          ,q.Queue_Name as QUEUE    
          ,isir.STATUS_TYPE AS STATUS    
          ,isir.UBM_BATCH_MASTER_LOG_ID    
          ,isir.EXCEPTION_COMMENTS    
          ,COALESCE(isir.PROKARMA_FILENAME, ''No'') AS PROKARMA_STATUS    
          ,cu.IS_PROCESSED     
          ,cha.Supplier_Contract_Id';    
        SET @SQL_FROM    
            = ' FROM    
          dbo.INV_SOURCED_IMAGE_REPORT isir    
          LEFT OUTER JOIN dbo.CU_INVOICE cu    
           ON cu.CBMS_IMAGE_ID = isir.CBMS_IMAGE_ID    
          LEFT OUTER JOIN dbo.UBM_INVOICE ui    
           ON ui.UBM_INVOICE_ID = cu.UBM_INVOICE_ID    
          LEFT OUTER JOIN dbo.Cbms_Image ci    
           ON ci.CBMS_IMAGE_ID = cu.CBMS_IMAGE_ID    
          LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism    
           ON cism.CU_INVOICE_ID = cu.CU_INVOICE_ID    
    
          LEFT OUTER JOIN dbo.CU_EXCEPTION ce     
           ON ce.CU_INVOICE_ID = cu.CU_INVOICE_ID    
          LEFT OUTER JOIN dbo.CU_EXCEPTION_DETAIL ced    
           ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID    
          LEFT OUTER JOIN dbo.vwCbmsQueueName q    
           ON q.queue_id = ce.QUEUE_ID    
    
          LEFT OUTER JOIN dbo.CLIENT c    
           ON c.CLIENT_ID = cu.CLIENT_ID    
    
          LEFT OUTER JOIN core.Client_Hier_Account cha    
           ON cha.Account_Id = cism.Account_Id    
           ';    
        SET @SQL_WHERE    
            = 'WHERE    
NOT EXISTS(SELECT    
              1    
             FROM    
              #TEMP_INVOICE_TRACKING tit    
             WHERE    
              tit.CBMS_IMAGE_ID = isir.CBMS_IMAGE_ID ) ';    
    
        IF @DocId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.CBMS_IMAGE_ID = ' + CONVERT(VARCHAR, @DocId) + ') ';    
        IF @FileName IS NOT NULL    
        BEGIN    
            IF @FT_Parser_String_Split_Cnt <= @FT_Split_Threshold_Cnt    
            BEGIN    
            --    SET @SQL_WHERE    
            --        = @SQL_WHERE + ' AND (CONTAINS((isir.ORIGINAL_FILENAME_FTSearch,isir.ORIGINAL_FILENAME),'''    
            --          + @CBMS_File_Name_Search + ''')'    
            --          + ' OR CONTAINS((isir.PROKARMA_FILENAME_FTSearch,isir.PROKARMA_FILENAME),'''    
            --          + @CBMS_File_Name_Search + '''))';    
            --END;    
            --ELSE    
            --BEGIN    
            --    SET @SQL_WHERE    
            --        = @SQL_WHERE + 'AND ( isir.ORIGINAL_FILENAME LIKE ' + '''' + @FileName + '%'''    
            --          + ' OR isir.PROKARMA_FILENAME LIKE ' + '''' + @FileName + '%''' + ')';    
    
            --END;  
      
                                --SET @SQL_WHERE = @SQL_WHERE        
                                --                 + ' AND (CONTAINS((isir.ORIGINAL_FILENAME_FTSearch,isir.ORIGINAL_FILENAME),'''        
                                --                 + @CBMS_File_Name_Search + ''')'        
                                --                 + ' OR CONTAINS((isir.PROKARMA_FILENAME_FTSearch,isir.PROKARMA_FILENAME),'''        
                                --                 + @CBMS_File_Name_Search + '''))';        
      
       IF LEN (@CBMS_File_Name_Search)<=64      
       BEGIN      
     
                               SET @SQL_WHERE = @SQL_WHERE        
                                                 + ' AND (CONTAINS((isir.ORIGINAL_FILENAME_FTSearch,isir.ORIGINAL_FILENAME),'''        
                                                 + @CBMS_File_Name_Search + ''')'        
                                                 + ' OR CONTAINS((isir.PROKARMA_FILENAME_FTSearch,isir.PROKARMA_FILENAME),'''        
                                                 + @CBMS_File_Name_Search + '''))';     
           
       END       
       ELSE       
       
             SET @SQL_WHERE = @SQL_WHERE + 'AND ( isir.ORIGINAL_FILENAME LIKE ' + '''' + @FileName        
                                                 + '%''' + ' OR isir.PROKARMA_FILENAME LIKE ' + '''' + @FileName        
                                                 + '%''' + ')';        
                            END;        
                        ELSE        
                            BEGIN        
            
                                SET @SQL_WHERE = @SQL_WHERE + 'AND ( isir.ORIGINAL_FILENAME LIKE ' + '''' + @FileName        
                                                 + '%''' + ' OR isir.PROKARMA_FILENAME LIKE ' + '''' + @FileName        
                                                 + '%''' + ')';        
        
                            END;
			  
        END;      
        IF @BatchId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.UBM_BATCH_MASTER_LOG_ID = ' + CONVERT(VARCHAR, @BatchId) + ') ';    
        IF @CuInvoiceId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (cu.CU_INVOICE_ID = ' + CONVERT(VARCHAR, @CuInvoiceId) + ') ';    
        IF @ClientId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (c.CLIENT_ID = ' + CONVERT(VARCHAR, @ClientId) + ') ';    
        IF @AccountId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (cha.Account_Id = ' + CONVERT(VARCHAR, @AccountId) + ') ';    
        IF @ExceptionTypeId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (ced.EXCEPTION_TYPE_ID = ' + CONVERT(VARCHAR, @ExceptionTypeId) + ') ';    
        IF @BeginDate IS NOT NULL    
           AND @EndDate IS NOT NULL    
            SET @SQL_WHERE    
                = @SQL_WHERE + ' AND ((isir.SENT_DATE BETWEEN ''' + CONVERT(VARCHAR, @BeginDate) + ''' AND '''    
                  + CONVERT(VARCHAR, @EndDate) + ''') OR (isir.RECEIVED_DATE BETWEEN ''' + CONVERT(VARCHAR, @BeginDate)    
                  + ''' AND ''' + CONVERT(VARCHAR, @EndDate) + '''))';    
        IF @MonthPosted IS NOT NULL    
            SET @SQL_WHERE    
                = @SQL_WHERE + ' AND ((DATEPART(MM, cism.SERVICE_MONTH) = ' + CONVERT(VARCHAR, @MonthPosted) + ') ';    
        IF @YearPosted IS NOT NULL    
            SET @SQL_WHERE    
                = @SQL_WHERE + ' AND ((DATEPART(YYYY, cism.SERVICE_MONTH) = ' + CONVERT(VARCHAR, @YearPosted) + ') ';    
        IF @inv_source_keyword IS NOT NULL    
            SET @SQL_WHERE    
                = @SQL_WHERE + ' AND (isir.INV_SOURCE_KEYWORD LIKE ''%' + CONVERT(VARCHAR, @inv_source_keyword)    
                  + '%'')';    
        IF @SourceCodeId IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.INV_SOURCE_ID = ' + CONVERT(VARCHAR, @SourceCodeId) + ') ';    
        IF @statustypeid IS NOT NULL    
            SET @SQL_WHERE = @SQL_WHERE + ' AND (isir.STATUS_TYPE_ID = ' + CONVERT(VARCHAR, @statustypeid) + ') ';    
        IF @inv_source_label IS NOT NULL    
            SET @SQL_WHERE    
                = @SQL_WHERE + ' AND (isir.INV_SOURCE_LABEL = ' + CONVERT(VARCHAR, @inv_source_label) + ') ';    
    
        SET @SQL_GROUP_BY    
            = ' GROUP BY     
           isir.CBMS_IMAGE_ID    
           ,isir.PROKARMA_FILENAME    
           ,isir.ORIGINAL_FILENAME    
           ,cu.CU_INVOICE_ID    
           ,isir.INV_SOURCE_LABEL    
           ,isir.SENT_DATE    
           ,isir.RECEIVED_DATE    
           ,isir.STATUS_TYPE    
           ,isir.Account_Number    
           ,cism.ACCOUNT_ID    
           ,cha.Account_Number    
           ,cism.SERVICE_MONTH    
           ,c.CLIENT_NAME    
           ,cha.Meter_City + '','' + cha.Meter_State_Name    
           ,cha.Account_Vendor_Name    
           ,q.Queue_Name    
           ,isir.STATUS_TYPE    
           ,isir.UBM_BATCH_MASTER_LOG_ID    
           ,isir.EXCEPTION_COMMENTS    
           ,COALESCE(isir.PROKARMA_FILENAME, ''No'')    
           ,cu.IS_PROCESSED    
           ,cha.Supplier_Contract_ID';    
    
        SELECT @SQL = @SQL_INSERT + @SQL_SELECT + @SQL_FROM + @SQL_WHERE + @SQL_GROUP_BY;    
    
        EXEC sp_executesql @SQL;    
    
    END;    
    
    
    
    --UPDATE    
    --    tinv    
    --SET    
    --    ACCOUNT_NUMBER = tinv.ACCOUNT_NUMBER + ' (' + CONVERT(VARCHAR(12), con.CONTRACT_START_DATE, 101) + ' - '    
    --                     + CONVERT(VARCHAR(12), con.CONTRACT_END_DATE, 101) + ')'    
    --FROM    
    --    #TEMP_INVOICE_TRACKING tinv    
    --    INNER JOIN dbo.CONTRACT con    
    --        ON con.CONTRACT_ID = tinv.Contract_Id;    
    
    
    SELECT CBMS_IMAGE_ID,    
           CBMS_DOC_ID,    
           CU_INVOICE_ID,    
           INV_SOURCED_IMAGE_TRACK_ID,    
           INV_SOURCE_LABEL,    
           SENT_DATE,    
           RECEIVED_DATE,    
           STATUS_TYPE,    
           ACCOUNT_NUMBER,    
           SERVICE_MONTH,    
           CLIENT_NAME,    
           CITY_STATE,    
           VENDOR_NAME,    
           QUEUE,    
           STATUS,    
           UBM_BATCH_MASTER_LOG_ID,    
           EXCEPTION_COMMENTS,    
           PROKARMA_STATUS,    
           IS_PROCESSED    
    FROM #TEMP_INVOICE_TRACKING    
    WHERE (    
              (@IsProcessed IS NULL)    
              OR (IS_PROCESSED = @IsProcessed)    
          )    
    GROUP BY CBMS_IMAGE_ID,    
             CBMS_DOC_ID,    
             CU_INVOICE_ID,    
             INV_SOURCED_IMAGE_TRACK_ID,    
             INV_SOURCE_LABEL,    
             SENT_DATE,    
             RECEIVED_DATE,    
             STATUS_TYPE,    
             ACCOUNT_NUMBER,    
             SERVICE_MONTH,    
             CLIENT_NAME,    
             CITY_STATE,    
             VENDOR_NAME,    
             QUEUE,    
             STATUS,    
             UBM_BATCH_MASTER_LOG_ID,    
             EXCEPTION_COMMENTS,    
             PROKARMA_STATUS,    
             IS_PROCESSED    
    ORDER BY CBMS_IMAGE_ID;    
    
    DROP TABLE #TEMP_INVOICE_TRACKING;    
    DROP TABLE #Cbms_Image;    
    
END;    
    
;    
    
    

GO







GRANT EXECUTE ON  [dbo].[cbmsInvoiceTracking_SEL] TO [CBMSApplication]
GO
