SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.SR_RFP_INSERT_RFP_SELECT_SUPPLIERS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_Id		INT     	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2018-07-05	Global Risk Management - Added filter to select "RFP"  contacts only
							

******/

CREATE   PROCEDURE [dbo].[SR_RFP_INSERT_RFP_SELECT_SUPPLIERS_P] ( @rfpId INT )
AS 
SET NOCOUNT ON;

DECLARE @TextLength INT
DECLARE @Pos INT
DECLARE @Delimiter VARCHAR(10)
DECLARE @End INT 
DECLARE @DelimLength INT 
DECLARE @account_id VARCHAR(20)
DECLARE @commodity INT
DECLARE @vendorId INT
DECLARE @contactIds VARCHAR(200)
DECLARE @TextLength1 INT
DECLARE @Pos1 INT
DECLARE @Delimiter1 VARCHAR(10)
DECLARE @End1 INT 
DECLARE @DelimLength1 INT 
DECLARE @contactId VARCHAR(20)
DECLARE @accountIds VARCHAR(4000)
		

SET @accountIds = ''

		--delete SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId 
		
SELECT
      @accountIds = ( @accountIds + LTRIM(STR(ACCOUNT_ID)) + ',' )
FROM
      SR_RFP_ACCOUNT
WHERE
      SR_RFP_ID = @rfpId
      AND is_deleted = 0
		
IF @accountIds <> NULL
      OR @accountIds <> '' 
      BEGIN
            SELECT
                  @accountIds = SUBSTRING(@accountIds, 1, LEN(@accountIds) - 1)
      END
		
		

SET @Pos = 1
SET @TextLength = DATALENGTH(@accountIds)
SET @commodity = 0
		--select * from sr_rfp	 
SELECT
      @commodity = commodity_type_id
FROM
      sr_rfp
WHERE
      sr_rfp_id = @rfpId
		 
SET @Delimiter = ','
SET @DelimLength = DATALENGTH(@Delimiter)
		
		      
		      -- Tack on delimiter to 'see' the last token
SET @accountIds = @accountIds + @Delimiter
		      -- Find the end character of the first token
SET @End = CHARINDEX(@Delimiter, @accountIds)
WHILE @End > 0 
      BEGIN
			
			
            SELECT
                  @account_id = SUBSTRING(@accountIds, @Pos, @End - @Pos)
			 
            SET @vendorId = 0
            SET @contactIds = ''

            SELECT
                  @vendorId = dbo.SR_RFP_FN_GET_VENDOR_IDS_FOR_SELECT_SUPPLIERS(@account_id)
			-- select dbo.SR_RFP_FN_GET_VENDOR_IDS_FOR_SELECT_SUPPLIERS(1715,getdate(),291)
							
            IF @vendorId > 0 
                  BEGIN
				
                        SELECT
                              @contactIds = ( @contactIds + LTRIM(STR(scInfo.sr_supplier_contact_info_id)) + ',' )
                        FROM
                              dbo.SR_SUPPLIER_CONTACT_INFO scInfo
                              INNER JOIN dbo.USER_INFO uinfo
                                    ON uinfo.user_info_id = scInfo.user_info_id
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP map
                                    ON scInfo.SR_SUPPLIER_CONTACT_INFO_ID = map.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.Sr_Supplier_Contact_Type_Map ssctyp
                                    ON scInfo.SR_SUPPLIER_CONTACT_INFO_ID = ssctyp.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.Code cd
                                    ON ssctyp.Contact_Type_Cd = cd.Code_Id
                              INNER JOIN dbo.Codeset cs
                                    ON cd.Codeset_Id = cs.Codeset_Id
                        WHERE
                              uinfo.IS_HISTORY <> 1
                              AND map.VENDOR_ID = @vendorId
                              AND map.IS_PRIMARY = 0
                              AND map.COMMODITY_TYPE_ID = @commodity
                              AND cs.Codeset_Name = 'ContactType'
                              AND cd.Code_Value IN ( 'RFP' )

                        IF @contactIds <> NULL
                              OR @contactIds <> '' 
                              BEGIN
                                    SELECT
                                          @contactIds = SUBSTRING(@contactIds, 1, LEN(@contactIds) - 1)
			 									
					-------------------------------------------------------------------
                                    SET @Pos1 = 1
                                    SET @TextLength1 = DATALENGTH(@contactIds)
					 
                                    SET @Delimiter1 = ','
                                    SET @DelimLength1 = DATALENGTH(@Delimiter1)
                                    SET @contactIds = @contactIds + @Delimiter1
                                    SET @End1 = CHARINDEX(@Delimiter1, @contactIds)
                                    WHILE @End1 > 0 
                                          BEGIN
                                                SELECT
                                                      @contactId = SUBSTRING(@contactIds, @Pos1, @End1 - @Pos1)
                                                IF ( SELECT
                                                      COUNT(SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID)
                                                     FROM
                                                      SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
                                                     WHERE
                                                      SR_RFP_ID = @rfpId
                                                      AND SR_SUPPLIER_CONTACT_INFO_ID = @contactId ) = 0 
                                                      BEGIN
                                                            SELECT
                                                                  @vendorId = vendor_id
                                                            FROM
                                                                  SR_SUPPLIER_CONTACT_VENDOR_MAP
                                                            WHERE
                                                                  SR_SUPPLIER_CONTACT_INFO_ID = @contactId
                                                                  AND IS_PRIMARY = 1

                                                            EXEC dbo.SR_RFP_SAVE_SUPPLIER_CONTACT_MAP_P 
                                                                  @rfpId
                                                                 ,@contactId
                                                                 ,@vendorId
							--PRINT 'IN ADDED CONTACT ID'+ @contactId
                                                      END
                                                SET @Pos1 = @End1 + @DelimLength1
                                                SET @End1 = CHARINDEX(@Delimiter1, @contactIds, @Pos1)
						 
						
                                          END
					-------------------------------------------------------------------
                              END

                  END 
	
            SET @Pos = @End + @DelimLength
	
            SET @End = CHARINDEX(@Delimiter, @accountIds, @Pos)
      END


GO
GRANT EXECUTE ON  [dbo].[SR_RFP_INSERT_RFP_SELECT_SUPPLIERS_P] TO [CBMSApplication]
GO
