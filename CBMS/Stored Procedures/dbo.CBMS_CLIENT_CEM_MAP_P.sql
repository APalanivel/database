SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_CLIENT_CEM_MAP_P]


DESCRIPTION: Inserts data into the CLIENT_CEM_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@clientId                 int
@userInfoId               int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
   exec CBMS_CLIENT_CEM_MAP_P @clientId = 11471, @userInfoId = 17589


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE dbo.CBMS_CLIENT_CEM_MAP_P
@clientId int,
@userInfoId int

AS


INSERT INTO CLIENT_CEM_MAP

(
	CLIENT_ID,
	USER_INFO_ID
) 
VALUES
(
	@clientId,
	@userInfoId
)
GO
GRANT EXECUTE ON  [dbo].[CBMS_CLIENT_CEM_MAP_P] TO [CBMSApplication]
GO
