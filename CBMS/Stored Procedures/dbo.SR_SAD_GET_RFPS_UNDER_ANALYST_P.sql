SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_RFPS_UNDER_ANALYST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@analystId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_RFPS_UNDER_ANALYST_P 
@analystId int

as
set nocount on
select 	sr_rfp_id 

from 	sr_rfp 
	
where 	initiated_by = 	@analystId

order by sr_rfp_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RFPS_UNDER_ANALYST_P] TO [CBMSApplication]
GO
