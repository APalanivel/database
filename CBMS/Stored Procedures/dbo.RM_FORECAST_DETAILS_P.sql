SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RM_FORECAST_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@monthIdentifier	datetime  	          	
	@forecastId    	int       	          	
	@aggressivePrice	decimal(32,16)	          	
	@conservativePrice	decimal(32,16)	          	
	@moderatePrice 	decimal(32,16)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.RM_FORECAST_DETAILS_P

@userId varchar(10),
@sessionId varchar(20),

@monthIdentifier Datetime, 
@forecastId int,
@aggressivePrice decimal(32,16),
@conservativePrice decimal(32,16),
@moderatePrice decimal(32,16)

AS
set nocount on
	insert into RM_FORECAST_DETAILS ( month_identifier,rm_forecast_id,
	 aggressive_price, conservative_price,moderate_price ) 
	values (@monthIdentifier, @forecastId, @aggressivePrice,
	 @conservativePrice, @moderatePrice)
GO
GRANT EXECUTE ON  [dbo].[RM_FORECAST_DETAILS_P] TO [CBMSApplication]
GO
