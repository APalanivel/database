SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      

/******
NAME:
	 dbo.Report_DE_Active_Supplier_Contracts
		
DESCRIPTION:
	Query to Get the Active Supplier Contractsfor Non North Region
	query built to accomodate request from jeff sullivan,to look for active supplier contracts and have the query run every monday   

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Report_DE_Active_Supplier_Contracts



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AKR        Ashok Kumar Raju    

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	AKR        2012-05-23  cloned from the View [lec_ActiveSupplierContracts]
	
******/

CREATE PROCEDURE dbo.Report_DE_Active_Supplier_Contracts
AS 
BEGIN

      SET NOCOUNT ON 

      DECLARE @Contract_Type INT


      SELECT
            @Contract_Type = Entity_Id
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'Supplier'
            AND en.ENTITY_DESCRIPTION = 'Contract Type'


      SELECT
            ch.client_name Client
           ,e.ENTITY_NAME SiteType
           ,utility.Meter_state_name [State]
           ,c.ed_contract_number [Contract Number]
           ,suppl.Account_Vendor_Name Supplier
           ,utility.Account_Vendor_Name Utility
           ,com.Commodity_Name Commodity
           ,suppl.Account_Number [SupplierAccount#]
           ,suppl.supplier_Account_Recalc_Type_Dsc AS [SupplierAccountRecalcType]
           ,[SupplierAccountExpected] = case WHEN suppl.Account_Not_Expected = 1 THEN 'No'
                                             ELSE 'Yes'
                                        END
      FROM
            core.Client_Hier_Account suppl
            INNER JOIN dbo.CONTRACT c
                  ON suppl.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = c.COMMODITY_TYPE_ID
            INNER JOIN core.Client_Hier_Account utility
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = utility.Client_Hier_Id
                  ON suppl.Meter_Id = utility.Meter_Id
            INNER JOIN dbo.SITE s
                  ON s.site_id = ch.Site_Id
            INNER JOIN dbo.entity e
                  ON s.SITE_TYPE_ID = e.ENTITY_ID
      WHERE
            suppl.account_Type = 'Supplier'
            AND utility.account_Type = 'Utility'
            AND getdate() BETWEEN contract_start_date
                          AND     contract_end_date
            AND utility.Meter_country_Name IN ( 'USA', 'Canada' )
            AND ch.Site_not_managed = 0
            AND c.contract_Type_Id = @Contract_Type
      GROUP BY
            ch.client_name
           ,e.ENTITY_NAME
           ,utility.Meter_state_name
           ,c.ed_contract_number
           ,suppl.Account_Vendor_Name
           ,utility.Account_Vendor_Name
           ,com.Commodity_Name
           ,suppl.Account_Number
           ,suppl.supplier_Account_Recalc_Type_Dsc
           ,suppl.Account_Not_Expected 
           
           
END

 

;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Active_Supplier_Contracts] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Active_Supplier_Contracts] TO [CBMSApplication]
GO
