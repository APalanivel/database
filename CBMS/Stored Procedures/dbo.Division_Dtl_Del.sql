
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Division_Dtl_Del  

DESCRIPTION: 

	Used to delete the Sitegroup entry from both Division_Dtl and Sitegroup table.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------
	@SiteGroup_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

  Begin Tran
	EXEC Division_Dtl_Del  30
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	BCH			Balaraju

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			20-JULY-10	CREATED
	BCH			2011-12-30  Deleted TRANSACTIONS (delete tool enhancement - transactions maintained by Application)

*/

CREATE PROCEDURE dbo.Division_Dtl_Del
    (
      @SiteGroup_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	BEGIN TRY

		
		
			DELETE 
			FROM
				dbo.Division_Dtl 
			WHERE
				SiteGroup_Id = @SiteGroup_Id

			EXEC dbo.Sitegroup_Del @Sitegroup_Id
	
		

	END TRY
	BEGIN CATCH
		
		EXEC usp_RethrowError

	END CATCH
		
END
GO

GRANT EXECUTE ON  [dbo].[Division_Dtl_Del] TO [CBMSApplication]
GO
