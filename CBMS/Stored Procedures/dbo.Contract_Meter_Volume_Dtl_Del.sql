SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Contract_Meter_Volume_Dtl_Del 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
	Exec dbo.Contract_Meter_Volume_Dtl_Del NULL,1005,290,'2020-01-01','2020-02-01','2818,2815',NULL
  
   
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-01	GRM - Contract volumes enhancement - Created
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Dtl_Del]
    (
        @Contract_Id INT
        , @Meter_Id INT = NULL
        , @Month_Identifier DATETIME = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @CONTRACT_METER_VOLUME_ID INT;

        DELETE
        cmvd
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
            INNER JOIN dbo.Contract_Meter_Volume_Dtl cmvd
                ON cmvd.CONTRACT_METER_VOLUME_ID = cmv.CONTRACT_METER_VOLUME_ID
        WHERE
            cmv.METER_ID = @Meter_Id
            AND cmv.CONTRACT_ID = @Contract_Id
            AND (   @Month_Identifier IS NULL
                    OR  cmv.MONTH_IDENTIFIER = @Month_Identifier);



    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Dtl_Del] TO [CBMSApplication]
GO
