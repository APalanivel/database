SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_ALL_CEAS_FOR_CLIENT_P
	@client_id int
	AS
	begin
		set nocount on

		select	USER_INFO_ID 
		from	CLIENT_CEA_MAP 
		where	CLIENT_ID = @client_id

	end


GO
GRANT EXECUTE ON  [dbo].[GET_ALL_CEAS_FOR_CLIENT_P] TO [CBMSApplication]
GO
