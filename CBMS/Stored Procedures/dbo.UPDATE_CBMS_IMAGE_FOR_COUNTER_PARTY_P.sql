SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE        PROCEDURE dbo.UPDATE_CBMS_IMAGE_FOR_COUNTER_PARTY_P

@userId varchar,
@sessionId varchar,
@cbmsImageId int,
@counterPartyId int,
@dealTicketId int 


AS
	set nocount on
update 
	RM_DEAL_TICKET_COUNTER_PARTY set CBMS_IMAGE_ID = @cbmsImageId 


WHERE 
	RM_COUNTERPARTY_ID = @counterPartyId and 
	RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_CBMS_IMAGE_FOR_COUNTER_PARTY_P] TO [CBMSApplication]
GO
