SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME:  
  dbo.GET_DOCUMENT_IMAGE_FOR_CLIENTS_OF_CEM_P
  
DESCRIPTION:
  
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------
@Client_Id			int			
  
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
	
	EXEC dbo.GET_DOCUMENT_IMAGE_FOR_CLIENTS_OF_CEM_P


AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
 CPE		Chaitanya Panduga Eshwar
  
MODIFICATIONS  
 Initials Date		 Modification  
------------------------------------------------------------  
 CPE	  03/28/2011 Replaced vwCbmsSsoDocumentOwnerFlat view with SSO_DOCUMENT_OWNER_MAP tables.
 
******/

CREATE  PROCEDURE dbo.GET_DOCUMENT_IMAGE_FOR_CLIENTS_OF_CEM_P @client_id INT
AS 
BEGIN

      SET NOCOUNT ON

      SELECT DISTINCT
            ci.CBMS_IMAGE_ID
           ,ci.CONTENT_TYPE
           ,SD.DOCUMENT_TITLE
           ,ci.DATE_IMAGED
           ,SD.sso_document_id
      FROM
            dbo.SSO_DOCUMENT_OWNER_MAP SDOM
            JOIN dbo.SSO_DOCUMENT SD
                  ON SD.SSO_DOCUMENT_ID = SDOM.SSO_DOCUMENT_ID
            JOIN Core.Client_Hier CH
                  ON SDOM.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.CBMS_IMAGE ci
                  ON ci.CBMS_IMAGE_ID = SD.CBMS_IMAGE_ID
      WHERE
            CH.client_id = @client_id
            AND SD.CEM_HOMEPAGE = 1 
END
GO
GRANT EXECUTE ON  [dbo].[GET_DOCUMENT_IMAGE_FOR_CLIENTS_OF_CEM_P] TO [CBMSApplication]
GO
