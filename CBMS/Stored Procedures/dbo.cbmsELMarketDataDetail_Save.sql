SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsELMarketDataDetail_Save]
	( @price_point_detail_id int
	, @price_point_value decimal (18,4)
	, @price_point_date datetime
	, @price_point_id int = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @price_point_detail_id


	if @this_id is null
	begin

		insert into el_market_data_detail
			( price_point_value
			, price_point_date
			, price_point_id
			)
		 values
			( @price_point_value
			, @price_point_date
			, @price_point_id
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as price_point_detail_id

	end
	else
	begin

		   update el_market_data_detail with (rowlock)
		      set price_point_value = @price_point_value
			, price_point_date = @price_point_date
			, price_point_id = @price_point_id
		    where price_point_detail_id = @this_id

	end

--	set nocount off

	exec cbmsElMarketDataDetail_Get @this_id


END


SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE ON  [dbo].[cbmsELMarketDataDetail_Save] TO [CBMSApplication]
GO
