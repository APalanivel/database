SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DM_GET_NO_OF_UNKNOWN_ACCOUNT_INVOICES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@fromDate      	varchar(20)	          	
	@toDate        	varchar(20)	          	
	@ubmId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DM_GET_NO_OF_UNKNOWN_ACCOUNT_INVOICES_P '1','1','01/1/2010','03/01/2010',1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	
******/

CREATE PROCEDURE dbo.DM_GET_NO_OF_UNKNOWN_ACCOUNT_INVOICES_P
    @userId		VARCHAR(20),
    @sessionId	VARCHAR(20),
    @fromDate	VARCHAR(20),
    @toDate		VARCHAR(20),
    @ubmId		INT
AS 
BEGIN

	SET NOCOUNT ON ;
	
    DECLARE  @selectClause	VARCHAR(8000)
			,@fromClause	VARCHAR(8000)
			,@whereClause	VARCHAR(8000)
			,@groupByClause VARCHAR(8000)
			,@SQLStatement	VARCHAR(8000)

    SELECT  @selectClause = 'ubm.UBM_NAME UBM_NAME,
				--SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3) MONTH_IDENTIFIER,
				CONVERT(Varchar(12), masterLog.END_DATE,101) MONTH_IDENTIFIER,
				COUNT(distinct invoice.UBM_INVOICE_ID)	NO_OF_INVOICES,
				COUNT(distinct masterLog.UBM_BATCH_MASTER_LOG_ID)	NO_OF_FEEDS
			       '	

    SELECT  @fromClause = '	UBM_INVOICE invoice,
				UBM_BATCH_MASTER_LOG masterLog,
				--CU_EXCEPTION exception,
				--CU_EXCEPTION_DETAIL exception_detail,
				UBM ubm				
			     ' 

    SELECT  @groupByClause = ' --SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3),
				  CONVERT(Varchar(12), masterLog.END_DATE,101),
				  ubm.UBM_NAME
			        ' 


	
    SELECT  @whereClause = ' --exception_detail.EXCEPTION_TYPE_ID = (select entity_id from entity where entity_type = 136 and entity_name = ''Unknown Account'') AND
				--exception.CU_EXCEPTION_ID = exception_detail.CU_EXCEPTION_ID AND
				--invoice.CBMS_IMAGE_ID = exception.CBMS_IMAGE_ID AND
				invoice.IS_QUARTERLY = 0 AND
				masterLog.UBM_BATCH_MASTER_LOG_ID = invoice.UBM_BATCH_MASTER_LOG_ID AND
		                masterLog.UBM_ID = ubm.UBM_ID 
			      '

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN
            IF ( @fromDate = @toDate ) 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND CONVERT(Varchar(12), masterLog.END_DATE,101) = '
                             +''''+ CONVERT(VARCHAR(12), @fromDate, 101) +''''
                END
            ELSE 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND masterLog.END_DATE BETWEEN ' + '''' +
                            +  CONVERT(VARCHAR(12), @fromDate, 101) + ''''
                            + ' AND ' +''''+ CONVERT(VARCHAR(12), @toDate, 101) +''''

                END
        END 

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NULL
              OR @toDate = ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE >= '
                    +''''+ CONVERT(VARCHAR(12), @fromDate, 101) +''''
        END 

    IF ( @fromDate IS NULL
         OR @fromDate = ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE <= '
                    +''''+CONVERT(VARCHAR(12), @toDate, 101) +''''
        END 
        
    IF @ubmId > 0 
        BEGIN
            SELECT  @whereClause = @whereClause + ' AND ubm.UBM_ID = '
                    + STR(@ubmId) 
        END 

    SELECT  @SQLStatement = 'SELECT ' + @selectClause + ' FROM ' + @fromClause
            + ' WHERE ' + @whereClause + ' GROUP BY ' + @groupByClause

    EXEC ( @SQLStatement )
    
END
GO
GRANT EXECUTE ON  [dbo].[DM_GET_NO_OF_UNKNOWN_ACCOUNT_INVOICES_P] TO [CBMSApplication]
GO
