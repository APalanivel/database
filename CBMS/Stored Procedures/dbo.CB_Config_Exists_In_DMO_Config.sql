SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.CB_Config_Exists_In_DMO_Config       
              
Description:              
			To validated whether all the commodities in the given account have DMO config for the given date range
                           
 Input Parameters:              
    Name				DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@Account_Id			INT
	@Billing_Start_Dt	DATE
    @Billing_End_Dt		DATE
                 
 
 Output Parameters:                    
    Name					DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
	SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 95408
	EXEC dbo.Account_DMO_Config_Sel @Account_Id = 95408
	EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 95408
	EXEC dbo.CB_Config_Exists_In_DMO_Config 95408,'01/01/2014','12/01/2014'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 95408,'01/01/2014','12/01/2015'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 95408,'01/01/2016','12/01/2016'
	
	SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 1269921
	EXEC dbo.Account_DMO_Config_Sel @Account_Id = 1269921
	EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 1269921
	EXEC dbo.CB_Config_Exists_In_DMO_Config 1269921,'01/01/2017','01/01/2017'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 1269921,'02/01/2017','02/01/2017'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 1269921,'01/01/2017','02/01/2017'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 1269921,'02/01/2017','12/01/2017'
	EXEC dbo.CB_Config_Exists_In_DMO_Config 1269921,'02/01/2018','12/01/2018'

      
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-02-22		Contract Placeholder CP-50 Created
             
******/

CREATE PROCEDURE [dbo].[CB_Config_Exists_In_DMO_Config]
      ( 
       @Account_Id INT
      ,@Billing_Start_Dt DATE
      ,@Billing_End_Dt DATE = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE
            @CB_Config_Matches_DMO_Config_For_All_Services INT = NULL
           ,@CB_Config_Overlapping_DMO_Config INT = NULL
           
      
      DECLARE @CB_Config_Matches_DMO_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Commodity_Id INT
            ,DMO_Start_Dt DATE
            ,DMO_End_Dt DATE )
      
      DECLARE @CB_Config_Matches_DMO_Config_Services AS TABLE
            ( 
             Commodity_Id INT
            ,DMO_Start_Dt DATE
            ,DMO_End_Dt DATE )
      
      DECLARE @CB_Config_Overlaps_DMO_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Commodity_Id INT
            ,DMO_Start_Dt DATE
            ,DMO_End_Dt DATE )
            
      DECLARE @CB_Config_Overlaps_DMO_Config_Services AS TABLE
            ( 
             Commodity_Id INT
            ,DMO_Start_Dt DATE
            ,DMO_End_Dt DATE )
            
      DECLARE @Commodities AS TABLE ( Commodity_Id INT )
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Site_Id INT
           ,@Client_Hier_Id INT
           ,@Account_Hier_level_Cd INT
           ,@Account_Services_Cnt INT
      
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
           ,@Client_Hier_Id = cha.Client_Hier_Id
           ,@Account_Services_Cnt = COUNT(DISTINCT cha.Commodity_Id)
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            cha.Account_Id = @Account_Id
      GROUP BY
            NULLIF(ch.Client_Id, 0)
           ,NULLIF(ch.Sitegroup_Id, 0)
           ,NULLIF(ch.Site_Id, 0)
           ,cha.Client_Hier_Id
           
      INSERT      INTO @Commodities
                  ( 
                   Commodity_Id )
                  SELECT
                        Commodity_Id
                  FROM
                        Core.Client_Hier_Account
                  WHERE
                        Account_Id = @Account_Id
                  GROUP BY
                        Commodity_Id
            
      SELECT
            @Account_Hier_level_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND cd.Code_Value = 'Account'
      
------------------> Cosolidated billing configuration matches DMO configauration
      INSERT      INTO @CB_Config_Matches_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        ch.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt >= chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                         AND     chdc.DMO_End_Dt
                                   AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt ) )
      
      INSERT      INTO @CB_Config_Matches_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt >= chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                         AND     chdc.DMO_End_Dt
                                   AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt ) )
      
      INSERT      INTO @CB_Config_Matches_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND @Site_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt >= chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                         AND     chdc.DMO_End_Dt
                                   AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                       AND     chdc.DMO_End_Dt ) )
                                                       
      INSERT      INTO @CB_Config_Matches_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        @Client_Hier_Id AS Client_Hier_Id
                       ,adc.Commodity_Id
                       ,NULL
                       ,adc.DMO_Start_Dt
                       ,adc.DMO_End_Dt
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN @Commodities comm
                              ON adc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        adc.Account_Id = @Account_Id
                        AND ( ( @Billing_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt >= adc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt >= adc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= adc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND @Billing_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                         AND     adc.DMO_End_Dt
                                   AND @Billing_End_Dt BETWEEN adc.DMO_Start_Dt
                                                       AND     adc.DMO_End_Dt ) )
                     
      
      INSERT      INTO @CB_Config_Matches_DMO_Config_Services
                  ( 
                   Commodity_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        adcc.Commodity_Id
                       ,adcc.DMO_Start_Dt
                       ,adcc.DMO_End_Dt
                  FROM
                        @CB_Config_Matches_DMO_Config adcc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                     WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = 0
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND @Site_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = @Site_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Account_Id = @Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id )
                  GROUP BY
                        adcc.Commodity_Id
                       ,adcc.DMO_Start_Dt
                       ,adcc.DMO_End_Dt
      
      
     
      SELECT
            @CB_Config_Matches_DMO_Config_For_All_Services = 1
      WHERE
            @Account_Services_Cnt = ( SELECT
                                          COUNT(DISTINCT Commodity_Id)
                                      FROM
                                          @CB_Config_Matches_DMO_Config_Services )
                              
                              
------------------> Cosolidated billing configuration overlaps DMO configauration
      INSERT      INTO @CB_Config_Overlaps_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        ch.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt < chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt < chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( ( @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                             AND     chdc.DMO_End_Dt
                                           AND @Billing_End_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                               AND         chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                                AND         chdc.DMO_End_Dt
                                              AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                                  AND     chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt < chdc.DMO_Start_Dt
                                              AND @Billing_End_Dt > chdc.DMO_End_Dt ) ) ) )
      
      INSERT      INTO @CB_Config_Overlaps_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt < chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt < chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( ( @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                             AND     chdc.DMO_End_Dt
                                           AND @Billing_End_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                               AND         chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                                AND         chdc.DMO_End_Dt
                                              AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                                  AND     chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt < chdc.DMO_Start_Dt
                                              AND @Billing_End_Dt > chdc.DMO_End_Dt ) ) ) )
      
      INSERT      INTO @CB_Config_Overlaps_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                       ,chdc.DMO_Start_Dt
                       ,chdc.DMO_End_Dt
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN @Commodities comm
                              ON chdc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND @Site_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        AND ( ( @Billing_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt < chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt < chdc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( ( @Billing_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                             AND     chdc.DMO_End_Dt
                                           AND @Billing_End_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                               AND         chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt NOT BETWEEN chdc.DMO_Start_Dt
                                                                AND         chdc.DMO_End_Dt
                                              AND @Billing_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                                  AND     chdc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt < chdc.DMO_Start_Dt
                                              AND @Billing_End_Dt > chdc.DMO_End_Dt ) ) ) )
                                                       
      INSERT      INTO @CB_Config_Overlaps_DMO_Config
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        @Client_Hier_Id AS Client_Hier_Id
                       ,adc.Commodity_Id
                       ,NULL
                       ,adc.DMO_Start_Dt
                       ,adc.DMO_End_Dt
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN @Commodities comm
                              ON adc.Commodity_Id = comm.Commodity_Id
                  WHERE
                        adc.Account_Id = @Account_Id
                        AND ( ( @Billing_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL
                                AND @Billing_Start_Dt < adc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND @Billing_Start_Dt < adc.DMO_Start_Dt
                                   AND @Billing_End_Dt >= adc.DMO_Start_Dt )
                              OR ( @Billing_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( ( @Billing_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                             AND     adc.DMO_End_Dt
                                           AND @Billing_End_Dt NOT BETWEEN adc.DMO_Start_Dt
                                                               AND         adc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt NOT BETWEEN adc.DMO_Start_Dt
                                                                AND         adc.DMO_End_Dt
                                              AND @Billing_End_Dt BETWEEN adc.DMO_Start_Dt
                                                                  AND     adc.DMO_End_Dt )
                                         OR ( @Billing_Start_Dt < adc.DMO_Start_Dt
                                              AND @Billing_End_Dt > adc.DMO_End_Dt ) ) ) )
                     
      
      INSERT      INTO @CB_Config_Overlaps_DMO_Config_Services
                  ( 
                   Commodity_Id
                  ,DMO_Start_Dt
                  ,DMO_End_Dt )
                  SELECT
                        adcc.Commodity_Id
                       ,adcc.DMO_Start_Dt
                       ,adcc.DMO_End_Dt
                  FROM
                        @CB_Config_Overlaps_DMO_Config adcc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                     WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = 0
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND @Site_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = @Site_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Account_Id = @Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id )
                  GROUP BY
                        adcc.Commodity_Id
                       ,adcc.DMO_Start_Dt
                       ,adcc.DMO_End_Dt
      
      SELECT
            @CB_Config_Overlapping_DMO_Config = -1
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @CB_Config_Overlaps_DMO_Config_Services dcs )    
                        
      /* Status defnition
		1 -> Consolidated billing date range within DMO configuration, supplier is mandatory
		0 -> Consolidated billing date range outside DMO configuration, supplier drop down hides and no supplier needed
		-1 -> Consolidated billing date range overlapping with DMO configuration, dates should adjusted to match any one condition above
      */
      SELECT
            COALESCE(@CB_Config_Matches_DMO_Config_For_All_Services, @CB_Config_Overlapping_DMO_Config, 0) AS CB_Config_Status
            
END;
;

;
GO
GRANT EXECUTE ON  [dbo].[CB_Config_Exists_In_DMO_Config] TO [CBMSApplication]
GO
