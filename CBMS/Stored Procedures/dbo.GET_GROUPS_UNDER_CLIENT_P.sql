SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_GROUPS_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
	set nocount on
select	group1.RM_GROUP_ID,group1.GROUP_NAME from RM_GROUP group1
where 	group1.CLIENT_ID = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_GROUPS_UNDER_CLIENT_P] TO [CBMSApplication]
GO
