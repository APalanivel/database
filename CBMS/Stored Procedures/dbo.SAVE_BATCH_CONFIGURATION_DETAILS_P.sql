SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.SAVE_BATCH_CONFIGURATION_DETAILS_P


@userId varchar(10),
@sessionId varchar(20),
@batchConfigId int,
@siteId int,
@divisionId int

as
	set nocount on

	insert into RM_BATCH_CONFIGURATION_DETAILS 
	(
		RM_BATCH_CONFIGURATION_ID,
		SITE_ID,
		DIVISION_ID
	)
	values
	(
		@batchConfigId,
		@siteId,
		@divisionId
	)
GO
GRANT EXECUTE ON  [dbo].[SAVE_BATCH_CONFIGURATION_DETAILS_P] TO [CBMSApplication]
GO
