
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******            
NAME:   etl.Site_Commodity_Image_Get_All         
  
      
DESCRIPTION:    
 Gets all records from Site_Commodity-Image table   
   
 used specifically for ETL prcoesses           
   
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
  
         
       
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
CMH   Chad Hattabaugh    
AKR   Ashok Kumar Raju
DMR	  Deana Ritter
       
       
MODIFICATIONS             
Initials Date Modification            
------------------------------------------------------------            
CMH   12/29/2010 Created  
AKR   2012-08-29 Modified to include Is_Reported Flag
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Causing transaction logs to fill up.

*****/   
CREATE PROCEDURE [ETL].[Site_Commodity_Image_Get_All]
AS 
BEGIN  
   
      SELECT
            ch.Client_Hier_Id
           ,0 AS Account_id
           ,0 AS CU_Invoice_Id
           ,sci.Commodity_id
           ,sci.Service_Month
           ,sci.CBMS_IMAGE_ID
           ,CONVERT(BIT, NULL) AS Is_Reported
           ,'i' AS Sys_CHANGE_OPERATION
           ,'Site_Commodity_Image_Get_All' AS DATA_Source
      FROM
            Core.Client_Hier ch
            INNER JOIN Site_Commodity_Image sci
                  ON sci.SITE_id = ch.Site_id
      GROUP BY
            ch.Client_Hier_Id
           ,sci.Commodity_id
           ,sci.Service_Month
           ,sci.CBMS_IMAGE_ID  
END  

;
;
GO


GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_Get_All] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_Get_All] TO [ETL_Execute]
GO
