SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.SmartGroup_Rule_INS
 
 DESCRIPTION:   
 
	This procedure used to insert the Non metric(Division/State/Country/Supplier/Vendor..) rules of a smart group
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Rule_Filter_Id INT	
	, @Filter_Condition_Id INT
	, @Filter_Value VARCHAR(50)
	, @Is_Inclusive BIT
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  
BEGIN TRAN	
	
DECLARE @tvp_Cbms_Smartgroup_Rule tvp_Cbms_Smartgroup_Rule
INSERT INTO @tvp_Cbms_Smartgroup_Rule VALUES (10, 63, '1000')
Exec dbo.Cbms_Sitegroup_Rule_Ins
    @Cbms_SiteGroup_Id = 1
    , @tvp_Cbms_Smartgroup_Rule = @tvp_Cbms_Smartgroup_Rule
    , @Is_Inclusive = 1 -- (1 - ALL 0 for ANY )
	, @User_Info_Id  =49

ROLLBACK TRAN

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2018-09-27	Created for GRM.
  
******/

CREATE PROCEDURE [dbo].[Cbms_Sitegroup_Rule_Ins]
      ( 
       @Cbms_SiteGroup_Id INT
      ,@tvp_Cbms_Smartgroup_Rule dbo.tvp_Cbms_Smartgroup_Rule READONLY
      ,@Is_Inclusive BIT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT OFF;
      
      DECLARE
            @Site INT
           ,@Min_Rule_Id INT
           ,@Max_Rule_Id INT
           ,@CBMS_Sitegroup_Rule_Id INT
           
      DECLARE @Display_Seq INT;
      
      DECLARE @Sites AS TABLE
            ( 
             Site_Id INT
            ,Site_name VARCHAR(200) )
            
      DECLARE @tbl_Cbms_Smartgroup_Rule AS TABLE
            ( 
             Rule_Id INT IDENTITY(1, 1)
            ,Cbms_Rule_Filter_Id INT
            ,Filter_Condition_Id NVARCHAR(2000)
            ,Filter_Value NVARCHAR(2000) )

      DELETE
            csrpv
      FROM
            dbo.CBMS_Sitegroup_Rule csr
            INNER JOIN dbo.Cbms_Sitegroup_Rule_Param_Value csrpv
                  ON csrpv.CBMS_Sitegroup_Rule_Id = csr.CBMS_Sitegroup_Rule_Id
      WHERE
            csr.Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id;

      DELETE
            csr
      FROM
            dbo.CBMS_Sitegroup_Rule csr
      WHERE
            csr.Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id;

     

      SELECT
            @Display_Seq = COALESCE(MAX(Display_Seq), 0) + 1
      FROM
            dbo.CBMS_Sitegroup_Rule
      WHERE
            Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id;

      
      INSERT      INTO @tbl_Cbms_Smartgroup_Rule
                  ( 
                   Cbms_Rule_Filter_Id
                  ,Filter_Condition_Id
                  ,Filter_Value )
                  SELECT
                        Cbms_Rule_Filter_Id
                       ,Filter_Condition_Id
                       ,Filter_Value
                  FROM
                        @tvp_Cbms_Smartgroup_Rule
                        
      SELECT
            @Min_Rule_Id = MIN(Rule_Id)
           ,@Max_Rule_Id = MAX(Rule_Id)
      FROM
            @tbl_Cbms_Smartgroup_Rule   
            
            
      WHILE ( @Min_Rule_Id <= @Max_Rule_Id ) 
            BEGIN
            
                  INSERT      dbo.CBMS_Sitegroup_Rule
                              ( 
                               Cbms_Sitegroup_Id
                              ,Cbms_Rule_Filter_Id
                              ,Is_Inclusive
                              ,Display_Seq
                              ,Created_User_Id
                              ,Created_Ts
                              ,Updated_User_Id
                              ,Last_Change_Ts )
                              SELECT
                                    @Cbms_SiteGroup_Id
                                   ,rf.Cbms_Rule_Filter_Id
                                   ,@Is_Inclusive
                                   ,@Display_Seq
                                   ,@User_Info_Id
                                   ,GETDATE()
                                   ,@User_Info_Id
                                   ,GETDATE()
                              FROM
                                    @tbl_Cbms_Smartgroup_Rule rf
                              WHERE
                                    rf.Rule_Id = @Min_Rule_Id
                                    
                  SELECT
                        @CBMS_Sitegroup_Rule_Id = SCOPE_IDENTITY()
                  --WHERE
                  --      EXISTS ( SELECT
                  --                  1
                  --               FROM
                  --                  @tvp_Cbms_Smartgroup_Rule tvp
                  --               WHERE
                  --                  tvp.Cbms_Rule_Filter_Id = rf.Cbms_Rule_Filter_Id );


                  INSERT      dbo.Cbms_Sitegroup_Rule_Param_Value
                              ( 
                               CBMS_Sitegroup_Rule_Id
                              ,Cbms_Filter_Param_Id
                              ,Param_Value )
                              SELECT
                                    sgr.CBMS_Sitegroup_Rule_Id
                                   ,fp.Cbms_Filter_Param_Id
                                   ,tvp.Filter_Condition_Id
                              FROM
                                    dbo.Cbms_Sitegroup sg
                                    INNER JOIN dbo.CBMS_Sitegroup_Rule sgr
                                          ON sg.Cbms_Sitegroup_Id = sgr.Cbms_Sitegroup_Id
                                    INNER JOIN dbo.Cbms_Rule_Filter rf
                                          ON sgr.Cbms_Rule_Filter_Id = rf.Cbms_Rule_Filter_Id
                                    INNER JOIN dbo.Cbms_Filter_Param fp
                                          ON rf.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                                    INNER JOIN @tbl_Cbms_Smartgroup_Rule tvp
                                          ON tvp.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                              WHERE
                                    fp.Default_Value IS NULL
                                    AND sg.Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id
                                    AND fp.Param_Name = 'Condition'
                                    AND tvp.Rule_Id = @Min_Rule_Id
                                    AND sgr.CBMS_Sitegroup_Rule_Id = @CBMS_Sitegroup_Rule_Id
                              GROUP BY
                                    sgr.CBMS_Sitegroup_Rule_Id
                                   ,fp.Cbms_Filter_Param_Id
                                   ,tvp.Filter_Condition_Id;

                  INSERT      dbo.Cbms_Sitegroup_Rule_Param_Value
                              ( 
                               CBMS_Sitegroup_Rule_Id
                              ,Cbms_Filter_Param_Id
                              ,Param_Value )
                              SELECT
                                    sgr.CBMS_Sitegroup_Rule_Id
                                   ,fp.Cbms_Filter_Param_Id
                                   ,tvp.Filter_Value
                              FROM
                                    dbo.Cbms_Sitegroup sg
                                    INNER JOIN dbo.CBMS_Sitegroup_Rule sgr
                                          ON sg.Cbms_Sitegroup_Id = sgr.Cbms_Sitegroup_Id
                                    INNER JOIN dbo.Cbms_Rule_Filter rf
                                          ON sgr.Cbms_Rule_Filter_Id = rf.Cbms_Rule_Filter_Id
                                    INNER JOIN dbo.Cbms_Filter_Param fp
                                          ON rf.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                                    INNER JOIN @tbl_Cbms_Smartgroup_Rule tvp
                                          ON tvp.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                              WHERE
                                    fp.Default_Value IS NULL
                                    AND sg.Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id
                                    AND fp.Param_Name = 'TargetTest'
                                    AND tvp.Rule_Id = @Min_Rule_Id
                                    AND sgr.CBMS_Sitegroup_Rule_Id = @CBMS_Sitegroup_Rule_Id
                              GROUP BY
                                    sgr.CBMS_Sitegroup_Rule_Id
                                   ,fp.Cbms_Filter_Param_Id
                                   ,tvp.Filter_Value;
           
                  SELECT
                        @Min_Rule_Id = @Min_Rule_Id + 1
            END 
      
      INSERT      INTO @Sites
                  ( 
                   Site_Id
                  ,Site_name )
                  EXEC dbo.Cbms_SmartGroup_Site_Sel 
                        @Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id
            
            
            
      SELECT
            @Site = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'RM Group Participants'
            AND cd.Code_Value = 'Site'


      DELETE FROM
            dbo.Cbms_Sitegroup_Participant
      WHERE
            Cbms_Sitegroup_Id = @Cbms_SiteGroup_Id
            
      INSERT      INTO dbo.Cbms_Sitegroup_Participant
                  ( 
                   Cbms_Sitegroup_Id
                  ,Group_Participant_Id
                  ,Group_Participant_Type_Cd
                  ,Include_New_Contract_Extensions
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Cbms_SiteGroup_Id
                       ,Site_Id
                       ,@Site
                       ,0
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        @Sites
                  GROUP BY
                        Site_Id

END;




GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Rule_Ins] TO [CBMSApplication]
GO
