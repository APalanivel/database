SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsAccount_MakeNotManaged

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@account_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script			
	HG	        11/05/2009	Removed unused param @myaccountid from the calling procedure cbmsAccount_Get
							Division view join removed as it is not necessary here.	
******/

CREATE   PROCEDURE dbo.cbmsAccount_MakeNotManaged
	( @MyAccountId int
	, @account_id int
	)
AS
BEGIN

	SET NOCOUNT ON

	IF NOT EXISTS(SELECT 1 FROM do_not_track WHERE account_id = @account_id)
	BEGIN

		INSERT INTO dbo.do_not_track( user_info_id
			, account_id
			, reason_type_id
			, account_number
			, client_name
			, client_city
			, state_id
			, vendor_name
			, date_added)
		SELECT
			DISTINCT 16 user_info_id
			, a.account_id
			, 314
			, isNull(a.account_number, 'Unspecified Account Number')
			, cl.client_name
			, ad.city
			, ad.state_id
			, v.vendor_name
			, GETDATE()
		FROM 
			dbo.Account a
			JOIN vwAccountMeter vam on vam.account_id = a.account_id
			JOIN site s on s.site_id = vam.site_id
			JOIN client cl on cl.CLIENT_ID = s.client_id
			JOIN address ad on ad.address_id = s.primary_address_id
			JOIN vendor v on v.vendor_id = a.vendor_id
		WHERE
			a.account_id = @account_id

	END

	EXEC cbmsAccount_Get @account_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_MakeNotManaged] TO [CBMSApplication]
GO
