SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
             
NAME: Inv_Sourced_Duplicate_Image_Keyword_Upd

DESCRIPTION:

 To Update the keyword column in Inv_Sourced_Duplicate_Image table.

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT  DESCRIPTION
------------------------------------------------------------
@Inv_Sourced_Image_Batch_Id		INT

OUTPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------

BEGIN TRAN

	dbo.Inv_Sourced_Duplicate_Image_Keyword_Upd
       @Inv_Sourced_Image_Batch_Id = 3433

ROLLBACK TRAN

SELECT TOP 100 * FROM INV_SOURCED_DUPLICATE_IMAGE WHERE Inv_Sourced_Image_Batch_ID IS NOT NULL

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2014-08-14	Created for Invoice scraping image hash enh

*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Duplicate_Image_Keyword_Upd]
      ( 
       @Inv_Sourced_Image_Batch_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Email_Inv_Source_Id INT
	
      SELECT
            @Email_Inv_Source_Id = Inv_Source_Id
      FROM
            dbo.Inv_Source
      WHERE
            INV_SOURCE_LABEL = 'E-mail Folder'

	 -- Images received through email
      UPDATE
            img
      SET   
            img.Keyword = l.From_Address
      FROM
            dbo.Inv_Sourced_Duplicate_Image img
            INNER JOIN dbo.Inv_Source_Email_Attachment ea
                  ON ea.Final_Filename = img.Image_File_Name
            INNER JOIN dbo.Inv_Source_Email_Log l
                  ON l.Inv_Source_Email_Log_Id = ea.Inv_Source_Email_Log_Id
                     AND l.Inv_Sourced_Image_Batch_Id = img.Inv_Sourced_Image_Batch_Id
      WHERE
            img.Duplicate_Image_Inv_Source_Id = @Email_Inv_Source_Id
            AND img.Keyword IS NULL
            AND img.Inv_Sourced_Image_Batch_Id = @Inv_Sourced_Image_Batch_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Duplicate_Image_Keyword_Upd] TO [CBMSApplication]
GO
