SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	Change_Control_Check_All_Thresholds

DESCRIPTION:
	Initiates a conversation to Change Control Service to
	check the trhesholds for all services. 
	
	This is executed buy tidal every 5 minutes to ensure 
	the change control process is kept alive  



INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Change_Control_Check_Thresholds

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			10/24/2011	Created
******/
Create PROCEDURE [dbo].[Change_Control_Check_All_Thresholds]
AS 
BEGIN
	SET NOCOUNT ON; 
	
	DECLARE @c_Handle UNIQUEIDENTIFIER;
	DECLARE @Message XML = '<Check_Threshold><Table_Name></Table_Name></Check_Threshold>'
	
	;BEGIN DIALOG CONVERSATION @c_Handle
	FROM SERVICE [//Change_Control/Service/CBMS/Change_Control]
	TO SERVICE '//Change_Control/Service/CBMS/Change_Control'
	ON CONTRACT [//Change_Control/Contract/Table_Change]
	
	;SEND ON CONVERSATION @c_Handle MESSAGE TYPE [//Change_Control/Message/Check_Threshold] (@Message)
END

GO
GRANT EXECUTE ON  [dbo].[Change_Control_Check_All_Thresholds] TO [CBMSApplication]
GO
