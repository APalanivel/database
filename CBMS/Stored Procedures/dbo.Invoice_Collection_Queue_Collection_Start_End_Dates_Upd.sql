SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Queue_Collection_Start_End_Dates_Upd       
              
Description:              
			This sproc to get the attribute details for a Given id's.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	 @Invoice_Collection_Queue_Id		 INT
     @Collection_Start_Date				 DATE				 NULL
     @Collection_End_Date				 DATE				 NULL
     @User_Info_Id						 INT                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

  declare @tvp_Invoice_Collection_Queue_Collection_Start_End_Date tvp_Invoice_Collection_Queue_Collection_Start_End_Date
   insert into @tvp_Invoice_Collection_Queue_Collection_Start_End_Date values (72,'2016-05-29',NULL),(72,NULL,'2016-07-29')
   Exec dbo.Invoice_Collection_Queue_Collection_Start_End_Dates_Upd @tvp_Invoice_Collection_Queue_Collection_Start_End_Date,74779 
   
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection.         
             
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Collection_Start_End_Dates_Upd]
      (
       @tvp_Invoice_Collection_Queue_Collection_Start_End_Date tvp_Invoice_Collection_Queue_Collection_Start_End_Date READONLY
      ,@User_Info_Id INT )
AS
BEGIN
      SET NOCOUNT ON; 

      CREATE TABLE #Invoice_Collection_Queue_Collection_Start_End_Date
            (
             Invoice_Collection_Queue_Id INT
            ,Collection_Start_Date DATE
            ,Collection_End_Date DATE );
      
      INSERT      INTO #Invoice_Collection_Queue_Collection_Start_End_Date
                  ( Invoice_Collection_Queue_Id
                  ,Collection_Start_Date
                  ,Collection_End_Date )
                  SELECT
                        Invoice_Collection_Queue_Id
                       ,MAX(Collection_Start_Date)
                       ,MAX(Collection_End_Date)
                  FROM
                        @tvp_Invoice_Collection_Queue_Collection_Start_End_Date
                  GROUP BY
                        Invoice_Collection_Queue_Id;
     
      UPDATE
            icq
      SET
            Collection_Start_Dt = ISNULL(tvpicd.Collection_Start_Date, Collection_Start_Dt)
           ,Collection_End_Dt = ISNULL(tvpicd.Collection_End_Date, Collection_End_Dt)
           ,Is_Invoice_Collection_Start_Dt_Changed = CASE WHEN tvpicd.Collection_Start_Date IS NULL THEN Is_Invoice_Collection_Start_Dt_Changed
                                                          ELSE 1
                                                     END
           ,Is_Invoice_Collection_End_Dt_Changed = CASE WHEN tvpicd.Collection_End_Date IS NULL THEN Is_Invoice_Collection_End_Dt_Changed
                                                        ELSE 1
                                                   END
           ,Is_Locked = CASE WHEN tvpicd.Collection_Start_Date IS NULL
                                  AND tvpicd.Collection_End_Date IS NULL THEN Is_Locked
                             ELSE 1
                        END
      FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Queue_Collection_Start_End_Date tvpicd
                  ON icq.Invoice_Collection_Queue_Id = tvpicd.Invoice_Collection_Queue_Id;
             
      
            
           
			     
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Collection_Start_End_Dates_Upd] TO [CBMSApplication]
GO
