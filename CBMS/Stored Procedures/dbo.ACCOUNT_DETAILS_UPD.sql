SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [DBO].[ACCOUNT_DETAILS_UPD]    
    
DESCRIPTION:    
 UPDATES ACCOUNT DETAILS IN ACCOUNT TABLE WHEN EDITING ACCOUNT DETAILS    
    
    
    
INPUT PARAMETERS:    
 NAME      DATATYPE DEFAULT  DESCRIPTION    
-----------------------------------------------------------------    
 @VENDOR_ID     INT    
 @ACCOUNT_NUMBER    VARCHAR(50)    
 @ACCOUNT_TYPE_ID   INT    
 @NOT_EXPECTED    BIT    
 @NOT_MANAGED    BIT    
 @NOT_EXPECTED_DATE   DATETIME    
 @RA_WATCH_LIST    BIT      
 @DM_WATCH_LIST    BIT    
 @PROCESSING_INSTRUCTIONS VARCHAR(6000)    
 @SUPPLIER_ACCOUNT_BEGIN_DT DATETIME    
 @SUPPLIER_ACCOUNT_END_DT DATETIME    
 @SERVICE_LEVEL_TYPE_ID  INT    
 @DEFAULT_SETTING   INT    
 @CONTRACT_ID    INT    
 @INVOICE_SOURCE_TYPE_ID  INT    
 @RECALC_TYPE_ID    INT    
 @ACCOUNT_ID     INT   
 @Is_Invoice_Posting_Blocked BIT   
    
OUTPUT PARAMETERS:    
 NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
  BEGIN TRAN  
   SELECT * FROM dbo.ACCOUNT WHERE ACCOUNT_ID = 99  
   EXEC dbo.ACCOUNT_DETAILS_UPD  269,'2655061002-2655061002',38,1,1,'01-01-2011','1081377',NULL,NULL,861,NULL,NULL,NULL,NULL,99,NULL,NULL,1,'Alternate_Account_Number'  
   SELECT * FROM dbo.ACCOUNT WHERE ACCOUNT_ID = 99  
  ROLLBACK TRAN  
    
AUTHOR INITIALS:    
 INITIALS NAME    
------------------------------------------------------------    
 MGB   BHASKARAN GOPALAKRISHNAN    
 HG   Hari    
 NK   Nageswara Rao Kosuri    
 SP   Sandeep Pigilam  
 RR   Raghu Reddy  
 NR   Narayana Reddy  
 TRK  Ramakrishna Thummala 
  
MODIFICATIONS    
 INITIALS DATE  MODIFICATION    
------------------------------------------------------------    
 MGB   21-AUG-09 CREATED    
 SS   05-OCT-09 Replaced IN clause with EXISTS, IF statement converted into a filter    
 MGB   03-Nov-09 included @RA_WATCH_LIST as a parameter      
 HG   11/05/2009 Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity    
       Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd     
       Parameter @RECALC_TYPE_ID renamed to @RECALC_TYPE_CD   
 NK   11/11/2009 Icorporated watchlist group and IS Data Entry Only     
 MGB   11/17/2009 Modified @UBM_ID to @UBM_ACCOUNT_CODE     
 NK   12/12/2009  Incorporated Calling sproc to Update Varaince_log account_number    
 NK   01/21/2010 Removed commented code and added Execute statement Account_Variance_Data_Entry_Only_Rules_Merge   
 NK   02/02/2010 Removed exec Account_Variance_Data_Entry_Only_Rules_INS_UPD   
 SP   2014-08-05  Data Operations Enhancement Phase III,Added Is_Invoice_Posting_Blocked as input parameter.   
 RR   2015-07-23  For Global CBMS Sourcing - Added @Alternate_Account_Number as input parameter   
 NR   2016-10-19 MAINT-4358 Added is null Function for Is_Data_Entry_Only column.   
 SP   2016-12-14 Invoice tracking,Added  Invoice_Collection_Is_Chased,Invoice_Collection_Is_Chased_Changed AS INPUT PARAM.     
 NR   2018-02-22 Recalc Data Interval - Add  @Supplier_Account_Determinant_Source_Cd To this Parameter.    
 NR   2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.  
 NR   2019-08-09 Add Contract - Added Supplier_Account_Config table to save the Min and Max Supp dates.  
 RKV         2020-01-21  Removed UBM_Account_Code From the parameter  
 NR   2020-02-11 MAINT-9923 - Added @Supplier_Account_Config_Id Parameter.  
 TRK  2020-02-03		Added Audit Table for Blocking info for Invoice Blocking Process.  
 RKV  2020-02-24      Modified the case statment while getting the data in to the variable @Is_Audit   
 NR		2020-04-27	MAINT-10253 - Saved Service Level type as Blank entity for Suppler accounts.		

***************/

CREATE PROCEDURE [dbo].[ACCOUNT_DETAILS_UPD]
    (
        @VENDOR_ID INT
        , @ACCOUNT_NUMBER VARCHAR(50)
        , @ACCOUNT_TYPE_ID INT
        , @NOT_EXPECTED BIT
        , @NOT_MANAGED BIT
        , @NOT_EXPECTED_DATE DATETIME
        , @SUPPLIER_ACCOUNT_BEGIN_DT DATETIME
        , @SUPPLIER_ACCOUNT_END_DT DATETIME
        , @SERVICE_LEVEL_TYPE_ID INT
        , @DEFAULT_SETTING INT
        , @CONTRACT_ID INT
        , @INVOICE_SOURCE_TYPE_ID INT
        , @RECALC_TYPE_CD INT
        , @ACCOUNT_ID INT
        , @DATA_ENTRY_ONLY INT
        , @Is_Invoice_Posting_Blocked BIT
        , @Alternate_Account_Number NVARCHAR(200)
        , @Invoice_Collection_Is_Chased VARCHAR = NULL
        , @Invoice_Collection_Is_Chased_Changed BIT = NULL
        , @User_Info_Id INT = NULL
        , @Supplier_Account_Determinant_Source_Cd INT = NULL
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Min_Supplier_Account_Begin_Dt DATE
            , @Max_Supplier_Account_End_Dt DATE
            , @Is_Audit BIT;

        DECLARE
            @Blank_Entity_Id INT
            , @Supplier_Account_Type_Id INT;

        SELECT
            @Blank_Entity_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = ' '
            AND e.ENTITY_TYPE = 708;

        SELECT
            @Supplier_Account_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_DESCRIPTION = 'Account Type';


        BEGIN TRY
            BEGIN TRAN;

            DECLARE @Invoice_Collection_Is_Chased_Bit BIT;
            UPDATE
                AC
            SET
                AC.Supplier_Account_Is_Default_Setting = 0
            FROM
                dbo.ACCOUNT AC
                JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                    ON samm.ACCOUNT_ID = AC.ACCOUNT_ID
            WHERE
                samm.Contract_ID = @CONTRACT_ID
                AND @DEFAULT_SETTING = 1;



            UPDATE
                sac
            SET
                Supplier_Account_Begin_Dt = @SUPPLIER_ACCOUNT_BEGIN_DT
                , Supplier_Account_End_Dt = @SUPPLIER_ACCOUNT_END_DT
                , sac.Last_Change_Ts = GETDATE()
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Account_Id = @ACCOUNT_ID
                AND sac.Contract_Id = @CONTRACT_ID
                AND sac.Supplier_Account_Config_Id = @Supplier_Account_Config_Id;



            SELECT
                @Min_Supplier_Account_Begin_Dt = MIN(sac.Supplier_Account_Begin_Dt)
                , @Max_Supplier_Account_End_Dt = MAX(sac.Supplier_Account_End_Dt)
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Account_Id = @ACCOUNT_ID;

            SELECT
                @Is_Audit = CASE WHEN A.Is_Invoice_Posting_Blocked <> @Is_Invoice_Posting_Blocked THEN 1
                                ELSE 0
                            END
            FROM
                dbo.ACCOUNT AS A
            WHERE
                A.ACCOUNT_ID = @ACCOUNT_ID;

            UPDATE
                dbo.ACCOUNT
            SET
                VENDOR_ID = @VENDOR_ID
                , ACCOUNT_NUMBER = @ACCOUNT_NUMBER
                , ACCOUNT_TYPE_ID = @ACCOUNT_TYPE_ID
                , NOT_EXPECTED = @NOT_EXPECTED
                , NOT_MANAGED = @NOT_MANAGED
                , NOT_EXPECTED_DATE = @NOT_EXPECTED_DATE
                , Supplier_Account_Begin_Dt = @Min_Supplier_Account_Begin_Dt
                , Supplier_Account_End_Dt = @Max_Supplier_Account_End_Dt
                , SERVICE_LEVEL_TYPE_ID = CASE WHEN @Supplier_Account_Type_Id = @ACCOUNT_TYPE_ID
                                                    AND @SERVICE_LEVEL_TYPE_ID IS NULL THEN @Blank_Entity_Id
                                              ELSE @SERVICE_LEVEL_TYPE_ID
                                          END
                , Supplier_Account_Is_Default_Setting = @DEFAULT_SETTING
                , INVOICE_SOURCE_TYPE_ID = @INVOICE_SOURCE_TYPE_ID
                , Supplier_Account_Recalc_Type_Cd = @RECALC_TYPE_CD
                , Is_Data_Entry_Only = ISNULL(@DATA_ENTRY_ONLY, 0)
                , Is_Invoice_Posting_Blocked = @Is_Invoice_Posting_Blocked
                , Alternate_Account_Number = @Alternate_Account_Number
                , Supplier_Account_Determinant_Source_Cd = @Supplier_Account_Determinant_Source_Cd
            WHERE
                ACCOUNT_ID = @ACCOUNT_ID;

            EXEC Variance_Log_Account_Number_UPD @ACCOUNT_ID, @ACCOUNT_NUMBER;





            IF @Is_Audit = 1
                BEGIN
                    IF NOT EXISTS (   SELECT
                                            1
                                      FROM
                                            dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                                      WHERE
                                            ACCOUNT_ID = @ACCOUNT_ID)
                        BEGIN
                            INSERT INTO dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                                 (
                                     ACCOUNT_ID
                                     , Created_User_Id
                                     , Created_Ts
                                     , Updated_User_Id
                                     , Last_Change_Ts
                                 )
                            VALUES
                                (@ACCOUNT_ID        -- ACCOUNT_ID - int
                                 , @User_Info_Id    -- Created_User_Id - int
                                 , GETDATE()        -- Created_Ts - datetime
                                 , @User_Info_Id    -- Updated_User_Id - int
                                 , GETDATE()        -- Last_Change_Ts - datetime
                                );
                        END;
                END;

            IF @Is_Audit = 1
                BEGIN
                    IF EXISTS (   SELECT
                                        1
                                  FROM
                                        dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                                  WHERE
                                        ACCOUNT_ID = @ACCOUNT_ID)
                        BEGIN
                            UPDATE
                                dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                            SET
                                Updated_User_Id = @User_Info_Id
                                , Last_Change_Ts = GETDATE()
                            WHERE
                                ACCOUNT_ID = @ACCOUNT_ID;
                        END;
                END;

            SELECT
                @Invoice_Collection_Is_Chased_Bit = CAST(ISNULL(@Invoice_Collection_Is_Chased, 0) AS BIT);


            EXEC dbo.Account_Exception_Ins_By_Missing_Invoice_Collection_data
                @Account_Id = @ACCOUNT_ID
                , @User_Info_Id = @User_Info_Id
                , @Invoice_Collection_Is_Chased = @Invoice_Collection_Is_Chased_Bit
                , @Invoice_Collection_Is_Chased_Changed = @Invoice_Collection_Is_Chased_Changed;

            COMMIT TRAN;

        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError 'Error In Update';

        END CATCH;
    END;








GO


GRANT EXECUTE ON  [dbo].[ACCOUNT_DETAILS_UPD] TO [CBMSApplication]
GO
