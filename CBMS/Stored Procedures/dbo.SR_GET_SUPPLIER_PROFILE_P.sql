
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_GET_SUPPLIER_PROFILE_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name            DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId			VARCHAR
    @sessionId		VARCHAR
    @vendorId		INT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	
		EXEC dbo.SR_GET_SUPPLIER_PROFILE_P 1,1,689
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy 

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-07-10	Added header
							Global Sourcing - Added Genaral_Comment to select list


******/
CREATE   PROCEDURE [dbo].[SR_GET_SUPPLIER_PROFILE_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@vendorId INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            VENDOR_ID
           ,CREDIT_BIO_COMMENTS
           ,TARGET_MARKET_COMMENTS
           ,COMPETITIVE_ADVANTAGE_COMMENTS
           ,IS_SUMMIT_APPROVED
           ,SUMMIT_APPROVED_COMMENTS
           ,IS_MINORITY_OWNED
           ,MINORITY_OWNED_COMMENTS
           ,IS_GREEN_ENERGY
           ,GREEN_ENERGY_COMMENTS
           ,IS_GOVT_CONTRACTS
           ,GOVT_CONTRACTS_COMMENTS
           ,Genaral_Comment
      FROM
            dbo.SR_SUPPLIER_PROFILE
      WHERE
            VENDOR_ID = @vendorId
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_GET_SUPPLIER_PROFILE_P] TO [CBMSApplication]
GO
