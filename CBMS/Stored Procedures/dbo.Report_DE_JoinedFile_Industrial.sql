SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                        
NAME:                        
  dbo.Report_DE_JoinedFile_Industrial              
                  
DESCRIPTION:                   
 Client Info, Accounts, Meters, Contract, Utility , Rate data                
                  
                  
INPUT PARAMETERS:                      
 Name					DataType			Default			Description
------------------------------------------------------------------------
 @region_name			VARCHAR(10)			NULL			Name of the region wanted to be filtered out, when NULL all the regions are returned
 @Excluded_Client_Type	VARCHAR(max)		Commercial|Demo	Client types which needs to be excluded from the output, separated by pipe |

OUTPUT PARAMETERS:
      Name              DataType          Default     Description         
------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------

Stress /Prod
      
 exec Report_DE_JoinedFile_Industrialtest 'ne'             
 exec Report_DE_JoinedFile_Industrial 'cp'                

EXEC dbo.Report_DE_JoinedFile_Industrial
      @Region_Name = 'Aus'
     ,@Excluded_Client_Type = 'Demo'
     


EXEC dbo.Report_DE_JoinedFile_Industrialtest
      @Region_Name = 'Aus'
     ,@Excluded_Client_Type = 'Commercial|Demo'

             
AUTHOR INITIALS:                
      
Initials	Name                
------------------------------------------------------------                
LC			Lynn Cox                
AKR			Ashok Kumar Raju                
HG			Harihara Suthan G  
PS			Patchava Srinivasarao     
      
Initials	Date		Modification                 
------------------------------------------------------------                    
AKR			2012-06-07	Cloned from LEC Objects (Report_JoinedFile_Industrial). Replaced base tables with client hier tables                      
AJ			12/17/2012	Added two columns Utility Account ID, Supplier Account ID to the select list          
AKR			2013-01-28	Added the Analyst Name and Broker Fee columns      
AKR			2013-02-01	Added indexes to Temp tables.          
lc			2013-02-01	Removed hardcode NE region issue      
HG			2013-05-02	Added INNER JOIN with code table to filter only the Cost & usages services mapped to the client in Core.Client_Commodity      
AKR			2013-09-04	Added the Sourcing Analyst Mappings for Client, Account and Site    
AKR			2014-01-05	Modified the sproc to exclude Demo clients insetad of Gfe Clients
HG			2015-11-24	REPTMGR-41 , added new parameter to pass the excluded client type from the calling app as for Australian feed only the Demo client types should be excluded other feeds will excluded the commercial client types also.
LEC			2016-08-08  Added columns for Load Profile, contracting entity, legal structure, fein and duns to the end of the file
LEC			2016-09-01  Added Reg Markets analyst column, inserted before LP information that was added 8/8
HG			2017-03-10	Added new columns.
						SupplierAccountStartDate
						SupplierAccountEndDate when null will be shown as Unspecified and date is casted to varchar as to show Unspecified when null.
						SupplierAccountCreatedBy - show username similar to the existing column "ContractCreatedBy" in the Joined File today.
						SupplierAccountCreatedDate  - show date and timestamp similar to the existing column "AccountCreated" in the Joined File today.
						Added optional parameter @Excluded_Client to exclude the ATT for all other joined file exception Aus region.
LEC			2017-04-20	UBM Account Name added
LEC			2018-02-14  Added new column ContractProductType
PS		    2018-08-22  Added new Meter_Id column
lec			2020-01-27	Modified to left outer join for service level for supplier accts.  Supplier accts no longer require service level after 11/19 enhancement.  Supplier accts were not showing up in JF		
HG			2020-01-29	Ubm Account Name returned as comma separated values
lec		    2020-04-06 Added Rate_ID , s.CONTRACTING_ENTITY [SiteContractingEntity],( regmarketsanalyst already in this file)
lec			2020-04-07 Modified Site Contracting Entity to pull with replace to accomodate in line carriage return
lec			2020-06-15 Modified to add alternate account number, rolling meter for contracts, original contract end date
*/
CREATE PROCEDURE [dbo].[Report_DE_JoinedFile_Industrial]
    (
        @region_name          VARCHAR (10)  = NULL
       ,@Excluded_Client_Type VARCHAR (MAX) = 'Commercial|Demo'
       ,@Excluded_Client      VARCHAR (MAX) = '11231'
    )
AS
BEGIN

    SET NOCOUNT ON;

    CREATE TABLE #UtilityAccts_CTE
        (
            Account_Number              VARCHAR (50)
           ,Account_Id                  INT
           ,UtilityAccountServiceLevel  VARCHAR (200)
           ,AccountCreated              DATETIME
           ,AccountInvoiceNotExpected   VARCHAR (10)
           ,AccountNotManaged           VARCHAR (10)
           ,UtilityAccountInvoiceSource VARCHAR (200)
           ,Utility                     VARCHAR (200)
           ,CommodityType               VARCHAR (50)
           ,Rate                        VARCHAR (200)
		   ,Rate_ID						INT 
           ,PurchaseMethod              VARCHAR (50)
           ,Site_Id                     INT
           ,Client_Hier_Id              INT
           ,ADDRESS_ID                  INT
           ,MeterNumber                 VARCHAR (50)
           ,Meter_Id                    INT
           ,AccountCreatedBy            VARCHAR (30)
           ,Consumption_Level_Desc      VARCHAR (100)
           ,IsDataEntryOnly             VARCHAR (10)
           ,Is_Broker_Account           VARCHAR (5)
           ,Broker_Fee                  DECIMAL (28, 12)
           ,BrokerFee_Currency_Unit     VARCHAR (10)
           ,BrokerFee_Uom_Unit          VARCHAR (25)
           ,Analyst_Id                  INT
           ,Account_Analyst_Mapping_Cd  INT
           ,Site_Analyst_Mapping_Cd     INT
           ,Client_Analyst_Mapping_Cd   INT
           ,[Reg Markets Analyst]       VARCHAR (100)
           ,Account_UBM_Name            VARCHAR (100)
		  ,AlternateAccountNumber		varchar(200)) 
        ;

    CREATE TABLE #Contracts_CTE
        (
            ContractId                   INT
           ,Account_Id                   INT
           ,BaseContractId               INT
           ,ContractNumber               VARCHAR (150)
           ,ContractPricingStatus        VARCHAR (10)
           ,ContractType                 VARCHAR (200)
           ,ContractedVendor             VARCHAR (200)
           ,ContractStartDate            DATETIME
           ,ContractEndDate              DATETIME
           ,NotificationDays             DECIMAL (12, 2)
           ,NotificationDate             DATETIME
           ,ContractTriggerRights        VARCHAR (10)
           ,FullRequirements             VARCHAR (10)
           ,CONTRACT_PRICING_SUMMARY     VARCHAR (1000)
           ,ContractComments             VARCHAR (4000)
           ,Currency                     VARCHAR (200)
           ,RenewalType                  VARCHAR (200)
           ,SupplierAccountNumber        VARCHAR (50)
           ,SupplierAccountServiceLevel  VARCHAR (200)
           ,SupplierAccountInvoiceSource VARCHAR (200)
           ,ContractCreatedBy            VARCHAR (30)
           ,ContractCreatedDate          DATETIME
           ,Meter_Id                     INT
           ,SupplierAccountStartDate     DATETIME
           ,SupplierAccountEndDate       DATETIME
           ,SupplierAccountCreatedBy     VARCHAR (30)
           ,SupplierAccountCreatedDate   DATETIME
           ,account_ubm_name             VARCHAR (100)
           ,contractproducttype          VARCHAR (200)
		   	,SupplierAltAccount			 VARCHAR(200)
			,is_Rolling_meter			 bit
			,Original_contract_end_date  datetime
        );

    DECLARE @Excluded_Client_Type_Tbl TABLE
        (
            Client_Type_Id INT
        );
    DECLARE @Excluded_Client_Id TABLE
        (
            Client_Id INT
        );
    DECLARE
        @Supplier_Contract_Type_Id INT
       ,@Utility_Contract_Type_Id  INT
       ,@Contract_End_Date         DATE = '2010-12-31'
       ,@Account_Table             INT
       ,@Contract_Table            INT
       ,@Site_Table                INT
       ,@Client_Type_Id_commercial INT
       ,@Client_Type_Id_Demo       INT;

    DECLARE
        @Default_Analyst INT
       ,@Custom_Analyst  INT;
    SELECT
        @Default_Analyst = MAX(   CASE
                                      WHEN c.Code_Value = 'Default' THEN
                                          c.Code_Id
                                  END
                              )
       ,@Custom_Analyst = MAX(   CASE
                                     WHEN c.Code_Value = 'Custom' THEN
                                         c.Code_Id
                                 END
                             )
    FROM
        dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE
        cs.Codeset_Name = 'Analyst Type';

    --Meta Data for SSIS                   
    IF 1 = 2
    BEGIN
        SELECT
            CAST(NULL AS VARCHAR (200)) AS ClientName
           ,CAST(NULL AS INT) AS ClientID
           ,CAST(NULL AS VARCHAR (3)) AS ClientNotManaged
           ,CAST(NULL AS VARCHAR (200)) AS DivisionName
           ,CAST(NULL AS INT) AS DivisionID
           ,CAST(NULL AS VARCHAR (3)) AS DivisionNotManaged
           ,CAST(NULL AS VARCHAR (200)) AS SiteName
           ,CAST(NULL AS INT) AS SiteID
           ,CAST(NULL AS DATETIME) AS SiteCreated
           ,CAST(NULL AS VARCHAR (30)) AS [SiteRef.]
           ,CAST(NULL AS VARCHAR (100)) AS SiteRMGroup
           ,CAST(NULL AS VARCHAR (3)) AS SiteNotManaged
           ,CAST(NULL AS VARCHAR (3)) AS SiteClosed
           ,CAST(NULL AS VARCHAR (200)) AS SiteType
           ,CAST(NULL AS VARCHAR (200)) AS [AddressLine1]
           ,CAST(NULL AS VARCHAR (200)) AS [AddressLine2]
           ,CAST(NULL AS VARCHAR (200)) AS City
           ,CAST(NULL AS VARCHAR (200)) AS [State_Province]
           ,CAST(NULL AS VARCHAR (30)) AS [ZipCode]
           ,CAST(NULL AS VARCHAR (200)) AS [Country]
           ,CAST(NULL AS VARCHAR (200)) AS [Region]
           ,CAST(NULL AS VARCHAR (50)) AS [AccountNumber]
           ,CAST(NULL AS VARCHAR (200)) AS UtilityAccountServiceLevel
           ,CAST(NULL AS DATETIME) AS AccountCreated
           ,CAST(NULL AS VARCHAR (10)) AS AccountInvoiceNotExpected
           ,CAST(NULL AS VARCHAR (10)) AS AccountNotManaged
           ,CAST(NULL AS VARCHAR (200)) AS UtilityAccountInvoiceSource
           ,CAST(NULL AS VARCHAR (200)) AS Utility
           ,CAST(NULL AS VARCHAR (50)) AS CommodityType
           ,CAST(NULL AS VARCHAR (200)) AS Rate
		   ,cast(null as INT)as Rate_Id
           ,CAST(NULL AS VARCHAR (50)) AS MeterNumber
           ,CAST(NULL AS VARCHAR (50)) AS PurchaseMethod
           ,CAST(NULL AS INT) AS ContractID
           ,CAST(NULL AS INT) AS BaseContractId
           ,CAST(NULL AS VARCHAR (150)) AS ContractNumber
           ,CAST(NULL AS VARCHAR (8)) AS ContractPricingStatus
           ,CAST(NULL AS VARCHAR (200)) AS ContractType
           ,CAST(NULL AS VARCHAR (200)) AS ContractedVendor
           ,CAST(NULL AS DATETIME) AS ContractStartDate
           ,CAST(NULL AS DATETIME) AS ContractEndDate
           ,CAST(NULL AS DECIMAL (12, 2)) AS NotificationDays
           ,CAST(NULL AS DATETIME) AS NotificationDate
           ,CAST(NULL AS VARCHAR (3)) AS ContractTriggerRights
           ,CAST(NULL AS VARCHAR (3)) AS FullRequirements
           ,CAST(NULL AS VARCHAR (1000)) AS Contract_Pricing_Summary
           ,CAST(NULL AS VARCHAR (4000)) AS ContractComments
           ,CAST(NULL AS VARCHAR (200)) AS Currency
           ,CAST(NULL AS VARCHAR (200)) AS RenewalType
           ,CAST(NULL AS VARCHAR (50)) AS SupplierAccountNumber
           ,CAST(NULL AS VARCHAR (200)) AS SupplierAccountServiceLevel
           ,CAST(NULL AS VARCHAR (200)) AS SupplierAccountInvoiceSource
           ,CAST(NULL AS VARCHAR (81)) AS ContractCreatedBy
           ,CAST(NULL AS DATETIME) AS ContractCreatedDate
           ,CAST(NULL AS VARCHAR (30)) AS AccountCreatedBy
           ,CAST(NULL AS VARCHAR (100)) AS Consumption_Level_Desc
           ,CAST(NULL AS VARCHAR (10)) AS IsDataEntryOnly
           ,CAST(NULL AS VARCHAR (100)) AS Account_UBM_Name
           ,CAST(NULL AS VARCHAR (200)) AS ContractProductType
		   ,CAST(NULL AS VARCHAR (200)) AS SupplierAltAccount		
		   ,cast(null as bit) as is_Rolling_meter			
		   ,CAST(NULL AS DATETIME) AS Original_contract_end_date  
    END;

    SET @Excluded_Client = NULLIF(@Excluded_Client, '');

    INSERT INTO @Excluded_Client_Id
    (
        Client_Id
    )
    SELECT Segments
    FROM
        dbo.ufn_split(@Excluded_Client, ',')
    GROUP BY Segments;

    SELECT
        @Supplier_Contract_Type_Id = MAX(   CASE
                                                WHEN E.ENTITY_NAME = 'Supplier' THEN
                                                    E.ENTITY_ID
                                            END
                                        )
       ,@Utility_Contract_Type_Id = MAX(   CASE
                                               WHEN E.ENTITY_NAME = 'Utility' THEN
                                                   E.ENTITY_ID
                                           END
                                       )
       ,@Account_Table = MAX(   CASE
                                    WHEN E.ENTITY_NAME = 'Account_Table' THEN
                                        E.ENTITY_ID
                                END
                            )
       ,@Contract_Table = MAX(   CASE
                                     WHEN E.ENTITY_NAME = 'CONTRACT_TABLE' THEN
                                         E.ENTITY_ID
                                 END
                             )
       ,@Site_Table = MAX(   CASE
                                 WHEN E.ENTITY_NAME = 'Site_Table' THEN
                                     E.ENTITY_ID
                             END
                         )
       ,@Client_Type_Id_commercial = MAX(   CASE
                                                WHEN E.ENTITY_NAME = 'Commercial'
                                                     AND E.ENTITY_DESCRIPTION = 'Client Type' THEN
                                                    E.ENTITY_ID
                                            END
                                        )
       ,@Client_Type_Id_Demo = MAX(   CASE
                                          WHEN E.ENTITY_NAME = 'Demo'
                                               AND E.ENTITY_DESCRIPTION = 'Client Type' THEN
                                              E.ENTITY_ID
                                      END
                                  )
    FROM
        dbo.ENTITY E
    WHERE
        E.ENTITY_DESCRIPTION IN ( 'Table_Type', 'Table Type', 'Contract Type', 'Client Type' );

    INSERT INTO @Excluded_Client_Type_Tbl
    (
        Client_Type_Id
    )
    SELECT et.ENTITY_ID
    FROM
        dbo.ufn_split(@Excluded_Client_Type, '|') ect
        JOIN dbo.ENTITY et
            ON et.ENTITY_NAME = ect.Segments
    WHERE
        et.ENTITY_DESCRIPTION = 'Client Type'
    GROUP BY et.ENTITY_ID

    --Utility Accounts              
    ;
    WITH Cte_Accounts_List
    AS (
       SELECT
           cha.Account_Number AS Account_Number
          ,cha.Account_Id AS Account_Id
          ,svl.ENTITY_NAME AS UtilityAccountServiceLevel
          ,ea.MODIFIED_DATE AS AccountCreated
          ,AccountInvoiceNotExpected = CASE cha.Account_Not_Expected
                                           WHEN 1 THEN
                                               'Yes'
                                           ELSE
                                               'No'
                                       END
          ,AccountNotManaged = CASE cha.Account_Not_Managed
                                   WHEN 1 THEN
                                       'Yes'
                                   ELSE
                                       'No'
                               END
          ,istyp.ENTITY_NAME AS UtilityAccountInvoiceSource
          ,cha.Account_Vendor_Name AS Utility
          ,comm.Commodity_Name AS CommodityType
          ,cha.Rate_Name AS Rate
		  ,cha.Rate_Id Rate_ID
          ,'NotApplicable' AS PurchaseMethod
          ,ch.Site_Id
          ,cha.Client_Hier_Id
          ,cha.Meter_Address_ID AS ADDRESS_ID
          ,cha.Meter_Number AS MeterNumber
          ,cha.Meter_Id
          ,ui.USERNAME AS AccountCreatedBy
          ,vcl.Consumption_Level_Desc
          ,IsDataEntryOnly = CASE cha.Account_Is_Data_Entry_Only
                                 WHEN 1 THEN
                                     'Yes'
                                 ELSE
                                     'No'
                             END
          ,Is_Broker_Account = CASE cha.Account_Is_Broker
                                   WHEN 1 THEN
                                       'Yes'
                                   ELSE
                                       'No'
                               END
          ,acbf.Broker_Fee
          ,ecur.CURRENCY_UNIT_NAME AS BrokerFee_Currency_Unit
          ,euom.ENTITY_NAME AS BrokerFee_UomName
          ,ch.Client_Id
          ,cha.Commodity_Id
          ,cha.Account_Vendor_Id
          ,COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
          ,COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd
          ,ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
          ,RegMAnalyst.[Reg Markets Analyst]
          ,LEFT(accubm.Ubm_Name, LEN(accubm.Ubm_Name) - 1) AS Account_UBM_Name
		  ,cha.Alternate_Account_Number
       FROM
           Core.Client_Hier_Account cha
           INNER JOIN dbo.ENTITY svl
               ON svl.ENTITY_ID = cha.Account_Service_level_Cd
           INNER JOIN Core.Client_Hier ch
               ON cha.Client_Hier_Id = ch.Client_Hier_Id
           INNER JOIN dbo.STATE st
               ON st.STATE_ID = ch.State_Id
           INNER JOIN dbo.REGION reg
               ON reg.REGION_ID = st.REGION_ID
           INNER JOIN dbo.ENTITY_AUDIT ea
               ON ea.ENTITY_IDENTIFIER = cha.Account_Id
                  AND ea.ENTITY_ID = @Account_Table
                  AND ea.AUDIT_TYPE = 1
           LEFT OUTER JOIN dbo.ENTITY istyp
               ON istyp.ENTITY_ID = cha.Account_Invoice_Source_Cd
           INNER JOIN dbo.Commodity comm
               ON comm.Commodity_Id = cha.Commodity_Id
           INNER JOIN dbo.USER_INFO ui
               ON ui.USER_INFO_ID = ea.USER_INFO_ID
           INNER JOIN dbo.Account_Variance_Consumption_Level av
               ON av.ACCOUNT_ID = cha.Account_Id
           INNER JOIN dbo.Variance_Consumption_Level vcl
               ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                  AND vcl.Commodity_Id = cha.Commodity_Id
           ---            
           LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
               ON cha.Account_Id = acbf.Account_Id
                  AND cha.Commodity_Id = acbf.Commodity_Id
           LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
               ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
           LEFT OUTER JOIN dbo.ENTITY euom
               ON euom.ENTITY_ID = acbf.UOM_Entity_Id
           LEFT OUTER JOIN (
                               SELECT DISTINCT
                                      ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME [Reg Markets Analyst]
                                     ,ud.VENDOR_ID [RMVID]
                               --,m.Group_Info_ID
                               FROM
                                   dbo.Utility_Detail_Analyst_Map m
                                   JOIN dbo.GROUP_INFO gi
                                       ON gi.GROUP_INFO_ID = m.Group_Info_ID
                                   JOIN dbo.USER_INFO ui
                                       ON ui.USER_INFO_ID = m.Analyst_ID
                                   JOIN dbo.UTILITY_DETAIL ud
                                       ON ud.UTILITY_DETAIL_ID = m.Utility_Detail_ID
                               WHERE
                                   m.Group_Info_ID = 133
                           ) RegMAnalyst
               ON RegMAnalyst.RMVID = cha.Account_Vendor_Id
           OUTER APPLY (
                           SELECT u.UBM_NAME + ','
                           FROM
                               dbo.Account_Ubm_Account_Code_Map uam
                               INNER JOIN dbo.UBM u
                                   ON u.UBM_ID = uam.Ubm_Id
                           WHERE
                               uam.Account_Id = cha.Account_Id
                           GROUP BY u.UBM_NAME
                           FOR XML PATH('')
                       ) accubm(Ubm_Name)
       WHERE
           cha.Account_Type = 'Utility'
           AND (
                   reg.REGION_NAME = @region_name
                   OR @region_name IS NULL
               )
           AND (
                   @Excluded_Client IS NULL
                   OR NOT EXISTS (
                                     SELECT 1
                                     FROM
                                         @Excluded_Client_Id ec
                                     WHERE
                                         ec.Client_Id = ch.Client_Id
                                 )
               )
           AND NOT EXISTS (
                              SELECT 1
                              FROM
                                  @Excluded_Client_Type_Tbl ect
                              WHERE
                                  ect.Client_Type_Id = ch.Client_Type_Id
                          )
       )
    INSERT INTO #UtilityAccts_CTE
    (
        Account_Number
       ,Account_Id
       ,UtilityAccountServiceLevel
       ,AccountCreated
       ,AccountInvoiceNotExpected
       ,AccountNotManaged
       ,UtilityAccountInvoiceSource
       ,Utility
       ,CommodityType
       ,Rate
	   ,Rate_ID
       ,PurchaseMethod
       ,Site_Id
       ,Client_Hier_Id
       ,ADDRESS_ID
       ,MeterNumber
       ,Meter_Id
       ,AccountCreatedBy
       ,Consumption_Level_Desc
       ,IsDataEntryOnly
       ,Is_Broker_Account
       ,Broker_Fee
       ,BrokerFee_Currency_Unit
       ,BrokerFee_Uom_Unit
       ,Analyst_Id
       ,Account_Analyst_Mapping_Cd
       ,Site_Analyst_Mapping_Cd
       ,Client_Analyst_Mapping_Cd
       ,[Reg Markets Analyst]
       ,Account_UBM_Name
	  	,AlternateAccountNumber 
    )
    SELECT
        acc.Account_Number
       ,acc.Account_Id
       ,acc.UtilityAccountServiceLevel
       ,acc.AccountCreated
       ,acc.AccountInvoiceNotExpected
       ,acc.AccountNotManaged
       ,acc.UtilityAccountInvoiceSource
       ,acc.Utility
       ,acc.CommodityType
       ,acc.Rate
	   ,acc.rate_ID
       ,acc.PurchaseMethod
       ,acc.Site_Id
       ,acc.Client_Hier_Id
       ,acc.ADDRESS_ID
       ,acc.MeterNumber
       ,acc.Meter_Id
       ,acc.AccountCreatedBy
       ,acc.Consumption_Level_Desc
       ,acc.IsDataEntryOnly
       ,acc.Is_Broker_Account
       ,acc.Broker_Fee
       ,acc.BrokerFee_Currency_Unit
       ,acc.BrokerFee_UomName
       ,CASE
            WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN
                vcam.Analyst_Id
            WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN
                COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
        END AS Analyst_Id
       ,acc.Analyst_Mapping_Cd
       ,acc.Site_Analyst_Mapping_Cd
       ,acc.Client_Analyst_Mapping_Cd
       ,acc.[Reg Markets Analyst]
       ,acc.Account_UBM_Name
	  	  	,Alternate_Account_Number
    FROM
        Cte_Accounts_List acc
        LEFT OUTER JOIN dbo.VENDOR_COMMODITY_MAP vcm
            ON acc.Account_Vendor_Id = vcm.VENDOR_ID
               AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
        LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
            ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
        LEFT OUTER JOIN(Core.Client_Commodity ccc
        INNER JOIN dbo.Code cd
            ON cd.Code_Id = ccc.Commodity_Service_Cd
               AND cd.Code_Value = 'Invoice')
            ON ccc.Client_Id = acc.Client_Id
               AND ccc.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca
            ON aca.Account_Id = acc.Account_Id
               AND aca.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Site_Commodity_Analyst sca
            ON sca.Site_Id = acc.Site_Id
               AND sca.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca
            ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id;

    CREATE CLUSTERED INDEX [ix_#UtilityAccts_CTE__Site_Id__Address_ID]
    ON [#UtilityAccts_CTE] (
                               [Site_Id]
                              ,[ADDRESS_ID]
                           );

    --Contracts
    INSERT INTO #Contracts_CTE
    (
        ContractId
       ,Account_Id
       ,BaseContractId
       ,ContractNumber
       ,ContractPricingStatus
       ,ContractType
       ,ContractedVendor
       ,ContractStartDate
       ,ContractEndDate
       ,NotificationDays
       ,NotificationDate
       ,ContractTriggerRights
       ,FullRequirements
       ,CONTRACT_PRICING_SUMMARY
       ,ContractComments
       ,Currency
       ,RenewalType
       ,SupplierAccountNumber
       ,SupplierAccountServiceLevel
       ,SupplierAccountInvoiceSource
       ,ContractCreatedBy
       ,ContractCreatedDate
       ,Meter_Id
       ,SupplierAccountStartDate
       ,SupplierAccountEndDate
       ,SupplierAccountCreatedBy
       ,SupplierAccountCreatedDate
       ,account_ubm_name
       ,contractproducttype
	   	,SupplierAltAccount 
		,is_Rolling_meter 
		,Original_contract_end_date     
    )
    SELECT
        c.CONTRACT_ID ContractId
       ,cha.Account_Id Account_Id
       ,c.BASE_CONTRACT_ID BaseContractId
       ,c.ED_CONTRACT_NUMBER ContractNumber
       ,ContractPricingStatus = CASE
                                    WHEN lps.CONTRACT_ID IS NULL THEN
                                        'NotBuilt'
                                    ELSE
                                        'Built'
                                END
       ,e.ENTITY_NAME ContractType
       ,cha.Account_Vendor_Name ContractedVendor
       ,c.CONTRACT_START_DATE ContractStartDate
       ,c.CONTRACT_END_DATE ContractEndDate
       ,c.NOTIFICATION_DAYS NotificationDays
       ,NotificationDate = CONTRACT_END_DATE - (NOTIFICATION_DAYS + 15)
       ,ContractTriggerRights = CASE c.IS_CONTRACT_TRIGGER_RIGHTS
                                    WHEN 1 THEN
                                        'Yes'
                                    ELSE
                                        'No'
                                END
       ,FullRequirements = CASE c.IS_CONTRACT_FULL_REQUIREMENT
                               WHEN 1 THEN
                                   'Yes'
                               ELSE
                                   'No'
                           END
       ,c.CONTRACT_PRICING_SUMMARY
       ,c.CONTRACT_COMMENTS ContractComments
       ,cu.CURRENCY_UNIT_NAME Currency
       ,e1.ENTITY_NAME RenewalType
       ,cha.Account_Number SupplierAccountNumber
       ,e2.ENTITY_NAME SupplierAccountServiceLevel
       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
       ,ui.USERNAME ContractCreatedBy
       ,ea.MODIFIED_DATE ContractCreatedDate
       ,cha.Meter_Id
       ,cha.Supplier_Account_begin_Dt AS SupplierAccountStartDate
       ,cha.Supplier_Account_End_Dt AS SupplierAccountEndDate
       ,ui1.USERNAME SupplierAccountCreatedBy
       ,ea1.MODIFIED_DATE SupplierAccountCreatedDate
       ,accubm.Ubm_Name AS Account_UBM_Name
       ,prodtype.Code_Value ContractProductType
	   ,cha.Alternate_Account_Number [SupplierAltAccount]
		,samm.Is_Rolling_Meter
		,c.Original_contract_end_date
    FROM
        dbo.CONTRACT c
        INNER JOIN Core.Client_Hier_Account cha
            ON c.CONTRACT_ID = cha.Supplier_Contract_ID
			join SUPPLIER_ACCOUNT_METER_MAP samm on samm.ACCOUNT_ID = cha.account_id
			and samm.METER_ID = cha.Meter_Id
        INNER JOIN Core.Client_Hier ch
            ON cha.Client_Hier_Id = ch.Client_Hier_Id
        INNER JOIN dbo.ENTITY e
            ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
        INNER JOIN dbo.CURRENCY_UNIT cu
            ON cu.CURRENCY_UNIT_ID = c.CURRENCY_UNIT_ID
        INNER JOIN dbo.ENTITY e1
            ON e1.ENTITY_ID = c.RENEWAL_TYPE_ID
        LEFT OUTER JOIN dbo.ENTITY e2
            ON e2.ENTITY_ID = cha.Account_Service_level_Cd
        LEFT OUTER JOIN dbo.ENTITY e3
            ON e3.ENTITY_ID = cha.Account_Invoice_Source_Cd
        INNER JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
               AND ea.ENTITY_ID = @Contract_Table --494              
               AND ea.AUDIT_TYPE = 1
        INNER JOIN dbo.USER_INFO ui
            ON ui.USER_INFO_ID = ea.USER_INFO_ID
        LEFT OUTER JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
            ON lps.CONTRACT_ID = c.CONTRACT_ID
        LEFT JOIN dbo.ENTITY_AUDIT ea1
            ON ea1.ENTITY_IDENTIFIER = cha.Account_Id
               AND ea1.ENTITY_ID = @Account_Table --494                      
               AND ea1.AUDIT_TYPE = 1
        LEFT JOIN dbo.USER_INFO ui1
            ON ui1.USER_INFO_ID = ea1.USER_INFO_ID
        LEFT JOIN Code prodtype
            ON prodtype.Code_Id = c.Contract_Product_Type_Cd
        OUTER APPLY (
                        SELECT u.UBM_NAME + ','
                        FROM
                            dbo.Account_Ubm_Account_Code_Map uam
                            INNER JOIN dbo.UBM u
                                ON u.UBM_ID = uam.Ubm_Id
                        WHERE
                            uam.Account_Id = cha.Account_Id
                        GROUP BY u.UBM_NAME
                        FOR XML PATH('')
                    ) accubm(Ubm_Name)
    WHERE
        (
            c.CONTRACT_TYPE_ID = @Utility_Contract_Type_Id
            OR (
                   c.CONTRACT_TYPE_ID = @Supplier_Contract_Type_Id
                   AND c.CONTRACT_END_DATE > @Contract_End_Date
               )
        )
        AND cha.Account_Type = 'Supplier'
        AND (
                @Excluded_Client IS NULL
                OR NOT EXISTS (
                                  SELECT 1
                                  FROM
                                      @Excluded_Client_Id ec
                                  WHERE
                                      ec.Client_Id = ch.Client_Id
                              )
            )
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               @Excluded_Client_Type_Tbl ect
                           WHERE
                               ect.Client_Type_Id = ch.Client_Type_Id
                       );

    INSERT INTO #Contracts_CTE
    (
        ContractId
       ,Account_Id
       ,BaseContractId
       ,ContractNumber
       ,ContractPricingStatus
       ,ContractType
       ,ContractedVendor
       ,ContractStartDate
       ,ContractEndDate
       ,NotificationDays
       ,NotificationDate
       ,ContractTriggerRights
       ,FullRequirements
       ,CONTRACT_PRICING_SUMMARY
       ,ContractComments
       ,Currency
       ,RenewalType
       ,SupplierAccountNumber
       ,SupplierAccountServiceLevel
       ,SupplierAccountInvoiceSource
       ,ContractCreatedBy
       ,ContractCreatedDate
       ,Meter_Id
       ,SupplierAccountStartDate
       ,SupplierAccountEndDate
       ,SupplierAccountCreatedBy
       ,SupplierAccountCreatedDate
       ,account_ubm_name
       ,contractproducttype
	   	,SupplierAltAccount
		,Is_Rolling_Meter
		,Original_contract_end_date
    )
    SELECT
        -1 ContractId
       ,cha.Account_Id Account_Id
       ,NULL BaseContractId
       ,NULL ContractNumber
       ,NULL ContractPricingStatus
       ,NULL ContractType
       ,NULL ContractedVendor
       ,NULL ContractStartDate
       ,NULL ContractEndDate
       ,NULL NotificationDays
       ,NULL NotificationDate
       ,NULL ContractTriggerRights
       ,NULL FullRequirements
       ,NULL CONTRACT_PRICING_SUMMARY
       ,NULL ContractComments
       ,NULL Currency
       ,NULL RenewalType
       ,suppacc.ACCOUNT_NUMBER SupplierAccountNumber
       ,e2.ENTITY_NAME SupplierAccountServiceLevel
       ,e3.ENTITY_NAME SupplierAccountInvoiceSource
       ,NULL ContractCreatedBy
       ,NULL ContractCreatedDate
       ,cha.Meter_Id
       ,suppacc.Supplier_Account_Begin_Dt AS SupplierAccountStartDate
       ,suppacc.Supplier_Account_End_Dt AS SupplierAccountEndDate
       ,ui.USERNAME SupplierAccountCreatedBy
       ,ea.MODIFIED_DATE SupplierAccountCreatedDate
       ,LEFT(accubm.Ubm_Name, LEN(accubm.Ubm_Name) - 1) AS Account_UBM_Name
       ,NULL AS ContractProductType
	   ,null SupplierAltAccount
		, null as is_Rolling_Meter
		,null as Original_contract_end_date
    FROM
        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        INNER JOIN dbo.ACCOUNT suppacc
            ON samm.ACCOUNT_ID = suppacc.ACCOUNT_ID
        LEFT JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = suppacc.ACCOUNT_ID
               AND ea.ENTITY_ID = @Account_Table --494                      
               AND ea.AUDIT_TYPE = 1
        LEFT JOIN dbo.USER_INFO ui
            ON ui.USER_INFO_ID = ea.USER_INFO_ID
        LEFT OUTER JOIN dbo.ENTITY e2
            ON e2.ENTITY_ID = suppacc.SERVICE_LEVEL_TYPE_ID

        LEFT OUTER JOIN dbo.ENTITY e3
            ON e3.ENTITY_ID = suppacc.INVOICE_SOURCE_TYPE_ID
        INNER JOIN Core.Client_Hier_Account cha
            ON samm.ACCOUNT_ID = cha.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON cha.Client_Hier_Id = ch.Client_Hier_Id
			  OUTER APPLY (
                        SELECT u.UBM_NAME + ','
                        FROM
                            dbo.Account_Ubm_Account_Code_Map uam
                            INNER JOIN dbo.UBM u
                                ON u.UBM_ID = uam.Ubm_Id
                        WHERE
                            uam.Account_Id = suppacc.Account_Id
                        GROUP BY u.UBM_NAME
                        FOR XML PATH('')
                    ) accubm(Ubm_Name)
    WHERE
        samm.Contract_ID = -1
        AND (
                @Excluded_Client IS NULL
                OR NOT EXISTS (
                                  SELECT 1
                                  FROM
                                      @Excluded_Client_Id ec
                                  WHERE
                                      ec.Client_Id = ch.Client_Id
                              )
            )
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               @Excluded_Client_Type_Tbl ect
                           WHERE
                               ect.Client_Type_Id = ch.Client_Type_Id
                       );

    CREATE CLUSTERED INDEX [ix_#Contracts_CTE__Meter_Id]
    ON [#Contracts_CTE] ([Meter_Id]);
    --SiteList              

    SELECT
        ch.Client_Name [ClientName]
       ,ch.Client_Id [ClientID]
       ,ClientNotManaged = CASE ch.Client_Not_Managed
                               WHEN 1 THEN
                                   'Yes'
                               ELSE
                                   'No'
                           END
       ,ch.Sitegroup_Name [DivisionName]
       ,ch.Sitegroup_Id [DivisionID]
       ,DivisionNotManaged = CASE ch.Site_Not_Managed
                                 WHEN 1 THEN
                                     'Yes'
                                 ELSE
                                     'No'
                             END
       ,ch.Site_name [SiteName]
       ,ch.Site_Id [SiteID]
       ,ea.MODIFIED_DATE SiteCreated
       ,s.SITE_REFERENCE_NUMBER [SiteRef.]
       ,rg.GROUP_NAME SiteRMGroup
       ,SiteNotManaged = CASE ch.Site_Not_Managed
                             WHEN 1 THEN
                                 'Yes'
                             ELSE
                                 'No'
                         END
       ,SiteClosed = CASE ch.Site_Closed
                         WHEN 1 THEN
                             'Yes'
                         ELSE
                             'No'
                     END
       ,ch.Site_Type_Name SiteType
       ,addr.ADDRESS_LINE1 [AddressLine1]
       ,addr.ADDRESS_LINE2 [AddressLine2]
       ,addr.CITY City
       ,st.STATE_NAME [State_Province]
       ,addr.ZIPCODE [ZipCode]
       ,Ctry.COUNTRY_NAME [Country]
       ,reg.REGION_NAME [Region]
       ,uacte.Account_Number [AccountNumber]
       ,uacte.UtilityAccountServiceLevel
       ,uacte.AccountCreated
       ,uacte.AccountInvoiceNotExpected
       ,uacte.AccountNotManaged
       ,uacte.UtilityAccountInvoiceSource
       ,uacte.Utility
       ,uacte.CommodityType
       ,uacte.Rate
       ,uacte.MeterNumber
       ,uacte.PurchaseMethod
       ,cc.ContractId
       ,BaseContractId
       ,cc.ContractNumber
       ,cc.ContractPricingStatus
       ,cc.ContractType
       ,cc.ContractedVendor
       ,cc.ContractStartDate
       ,cc.ContractEndDate
       ,cc.NotificationDays
       ,cc.NotificationDate
       ,cc.ContractTriggerRights
       ,cc.FullRequirements
       ,cc.CONTRACT_PRICING_SUMMARY
       ,cc.ContractComments
       ,cc.Currency
       ,cc.RenewalType
       ,cc.SupplierAccountNumber
       ,cc.SupplierAccountServiceLevel
       ,cc.SupplierAccountInvoiceSource
       ,cc.ContractCreatedBy
       ,cc.ContractCreatedDate
       ,uacte.AccountCreatedBy
       ,uacte.Consumption_Level_Desc
       ,uacte.IsDataEntryOnly
       ,uacte.Account_Id [Utility Account ID]
       ,cc.Account_Id [Supplier Account ID]
       ,uacte.Is_Broker_Account [Broker Account]
       ,CAST(uacte.Broker_Fee AS VARCHAR) + ' ' + uacte.BrokerFee_Currency_Unit + '/' + uacte.BrokerFee_Uom_Unit AS [Broker Fee]
       ,uia.FIRST_NAME + SPACE(1) + uia.LAST_NAME [Sourcing Analyst]
       ,cacc.Code_Value [Client Sourcing Mapping]
       ,sacc.Code_Value [Site Sourcing Mapping]
       ,aacc.Code_Value [Account Sourcing Mapping]
       ,uacte.[Reg Markets Analyst]
       ,s.LP_CONTACT_FIRST_NAME [LP Contact First Name]
       ,s.LP_CONTACT_LAST_NAME [LP Contact Last Name]
       ,s.LP_CONTACT_EMAIL_ADDRESS [LP Contact Email]
       ,s.CONTRACTING_ENTITY [Contracting Entity]
       ,s.CLIENT_LEGAL_STRUCTURE [Legal Structure Statement]
       ,s.TAX_NUMBER [FEIN]
       ,s.DUNS_NUMBER [DUNS Number]
       ,cc.SupplierAccountStartDate
       ,CASE
            WHEN cc.ContractId = -1
                 AND cc.SupplierAccountEndDate IS NULL THEN
                'Unspecified'
            ELSE
                CONVERT(VARCHAR (10), cc.SupplierAccountEndDate, 101)
        END AS SupplierAccountEndDate
       ,cc.SupplierAccountCreatedBy
       ,cc.SupplierAccountCreatedDate
       ,uacte.Account_UBM_Name
       ,cc.contractproducttype
       ,uacte.Meter_Id AS Meter_Id
	   ,uacte.rate_id as rate_id
	  ,REPLACE(REPLACE(REPLACE(s.CONTRACTING_ENTITY, CHAR(9), ''), CHAR(13), ''), CHAR(10), '') [SiteContractingEntity]
	     ,AlternateAccountNumber [Utility Alternate Acct#]
		   ,SupplierAltAccount [Supplier Alternate Acct#]
		   ,[Is_Rolling_Meter] = case when is_Rolling_meter = 1 then 'Yes' when is_rolling_meter = 0 then  'No' else null  end 
		   ,Original_contract_end_date
    FROM
        Core.Client_Hier ch
        JOIN dbo.SITE s
            ON ch.Site_Id = s.SITE_ID
        JOIN dbo.ADDRESS addr
            ON addr.ADDRESS_PARENT_ID = ch.Site_Id
        JOIN dbo.STATE st
            ON st.STATE_ID = addr.STATE_ID
        JOIN dbo.COUNTRY Ctry
            ON Ctry.COUNTRY_ID = st.COUNTRY_ID
        JOIN dbo.REGION reg
            ON reg.REGION_ID = st.REGION_ID
        JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = ch.Site_Id
               AND ea.ENTITY_ID = @Site_Table -- 506              
               AND ea.AUDIT_TYPE = 1
        LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgm
            ON rgm.SITE_ID = ch.Site_Id
        LEFT OUTER JOIN dbo.RM_GROUP rg
            ON rg.RM_GROUP_ID = rgm.RM_GROUP_ID
        LEFT OUTER JOIN #UtilityAccts_CTE uacte
            ON uacte.Site_Id = ch.Site_Id
               AND uacte.ADDRESS_ID = addr.ADDRESS_ID
        LEFT JOIN Code cacc
            ON cacc.Code_Id = uacte.Client_Analyst_Mapping_Cd
        LEFT JOIN Code sacc
            ON sacc.Code_Id = uacte.Site_Analyst_Mapping_Cd
        LEFT JOIN Code aacc
            ON aacc.Code_Id = uacte.Account_Analyst_Mapping_Cd
        LEFT OUTER JOIN #Contracts_CTE cc
            ON cc.Meter_Id = uacte.Meter_Id
        LEFT OUTER JOIN dbo.USER_INFO uia
            ON uacte.Analyst_Id = uia.USER_INFO_ID
    WHERE
        (
            reg.REGION_NAME = @region_name
            OR @region_name IS NULL
        )
        AND (
                @Excluded_Client IS NULL
                OR NOT EXISTS (
                                  SELECT 1
                                  FROM
                                      @Excluded_Client_Id ec
                                  WHERE
                                      ec.Client_Id = ch.Client_Id
                              )
            )
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               @Excluded_Client_Type_Tbl ect
                           WHERE
                               ect.Client_Type_Id = ch.Client_Type_Id
                       )
        AND ch.Site_Not_Managed != 1
    GROUP BY ch.Client_Name
            ,ch.Client_Id
            ,ch.Client_Not_Managed
            ,ch.Sitegroup_Name
            ,ch.Sitegroup_Id
            ,ch.Division_Not_Managed
            ,ch.Site_name
            ,ch.Site_Id
            ,ea.MODIFIED_DATE
            ,s.SITE_REFERENCE_NUMBER
            ,rg.GROUP_NAME
            ,ch.Site_Not_Managed
            ,ch.Site_Closed
            ,ch.Site_Type_Name
            ,addr.ADDRESS_LINE1
            ,addr.ADDRESS_LINE2
            ,addr.CITY
            ,st.STATE_NAME
            ,addr.ZIPCODE
            ,Ctry.COUNTRY_NAME
            ,reg.REGION_NAME
            ,uacte.Account_Number
            ,uacte.UtilityAccountServiceLevel
            ,uacte.AccountCreated
            ,uacte.AccountInvoiceNotExpected
            ,uacte.AccountNotManaged
            ,uacte.UtilityAccountInvoiceSource
            ,uacte.Utility
            ,uacte.CommodityType
            ,uacte.Rate
            ,uacte.MeterNumber
            ,uacte.PurchaseMethod
            ,cc.ContractId
            ,BaseContractId
            ,cc.ContractNumber
            ,cc.ContractPricingStatus
            ,cc.ContractType
            ,cc.ContractedVendor
            ,cc.ContractStartDate
            ,cc.ContractEndDate
            ,cc.NotificationDays
            ,cc.NotificationDate
            ,cc.ContractTriggerRights
            ,cc.FullRequirements
            ,cc.CONTRACT_PRICING_SUMMARY
            ,cc.ContractComments
            ,cc.Currency
            ,cc.RenewalType
            ,cc.SupplierAccountNumber
            ,cc.SupplierAccountServiceLevel
            ,cc.SupplierAccountInvoiceSource
            ,cc.ContractCreatedBy
            ,cc.ContractCreatedDate
            ,addr.ADDRESS_ID
            ,uacte.AccountCreatedBy
            ,uacte.Consumption_Level_Desc
            ,uacte.IsDataEntryOnly
            ,uacte.Account_Id
            ,cc.Account_Id
            ,uacte.Is_Broker_Account
            ,uacte.Broker_Fee
            ,uacte.BrokerFee_Currency_Unit
            ,uacte.BrokerFee_Uom_Unit
            ,uia.FIRST_NAME
            ,uia.LAST_NAME
            ,cacc.Code_Value
            ,sacc.Code_Value
            ,aacc.Code_Value
            ,s.LP_CONTACT_FIRST_NAME
            ,s.LP_CONTACT_LAST_NAME
            ,s.LP_CONTACT_EMAIL_ADDRESS
            ,s.CONTRACTING_ENTITY
            ,s.CLIENT_LEGAL_STRUCTURE
            ,s.TAX_NUMBER
            ,s.DUNS_NUMBER
            ,uacte.[Reg Markets Analyst]
            ,cc.SupplierAccountStartDate
            ,CASE
                 WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN
                     'Unspecified'
                 ELSE
                     CONVERT(VARCHAR (10), cc.SupplierAccountEndDate, 101)
             END
            ,cc.SupplierAccountCreatedBy
            ,cc.SupplierAccountCreatedDate
            ,uacte.Account_UBM_Name
            ,cc.contractproducttype
            ,uacte.Meter_Id
			,uacte.rate_id
			 ,s.CONTRACTING_ENTITY 
			 ,AlternateAccountNumber
		   ,SupplierAltAccount 
		  ,Original_contract_end_date
		  ,is_Rolling_meter
    ORDER BY ch.Client_Name
            ,ch.Site_name
            ,uacte.Account_Number
	
		   ;


    DROP TABLE #Contracts_CTE;
    DROP TABLE #UtilityAccts_CTE;

END;
GO









GRANT EXECUTE ON  [dbo].[Report_DE_JoinedFile_Industrial] TO [CBMS_SSRS_Reports]
GO
