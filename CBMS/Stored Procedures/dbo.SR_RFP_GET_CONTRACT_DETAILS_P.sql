SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTRACT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contractNumber	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE DBO.SR_RFP_GET_CONTRACT_DETAILS_P

@contractNumber varchar(20)

AS
set nocount on
select 	contract_id,
	contract_end_date,				
	contract_end_date - notification_days contract_notice_date

from	contract

where	ed_contract_number = @contractNumber
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTRACT_DETAILS_P] TO [CBMSApplication]
GO
