SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
  
NAME:                           
     [dbo].[Bulk_Upd_Sites_To_New_Divisions]  
  
DESCRIPTION:   
 To bulk move the Sites to new division                                         
  
 INPUT PARAMETERS:                                          
  
 Name                        DataType         Default       Description                                        
---------------------------------------------------------------------------------------------------------------                                      
  
  
 OUTPUT PARAMETERS:                                          
  
 Name                        DataType         Default       Description                                        
---------------------------------------------------------------------------------------------------------------                                      
  
 USAGE EXAMPLES:                                                            
---------------------------------------------------------------------------------------------------------------                                                            
  
 BEGIN TRAN                                
 EXEC [dbo].Bulk_Upd_Sites_To_New_Divisions                                 
        
 ROLLBACK TRAN                                   
  
  
 AUTHOR INITIALS:                                        
  
 Initials   Name                                        
---------------------------------------------------------------------------------------------------------------                                                      
 ABK        Aditya Bharadwaj Kolipaka  
 NM			Nagaraju Muppa  
                                                                    
  
 MODIFICATIONS:                                      
  
 Initials Date        Modification                                      
---------------------------------------------------------------------------------------------------------------                                      
 ABK   03-10-2018  Created for DDCR - 34.  
 NM   2020-02-21 MAINT-9855 - If the division name also exists as a sitegroup for   
								the same client, the tool is updating randomly, assigning sites to whatever division it likes.   
								Added to the process to look for sitegroups of division type code.code_id = 100001 only.                                
   
*********/  
  
CREATE PROCEDURE [dbo].[Bulk_Upd_Sites_To_New_Divisions]  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        CREATE TABLE #temp  
             (  
                 ID INT NOT NULL IDENTITY(1, 1)  
                 , Division_Name VARCHAR(200) NULL  
                 , Site_Name VARCHAR(200) NULL  
                 , Site_ID INT NULL  
                 , New_Division VARCHAR(200) NULL  
                 , Email_Address VARCHAR(500) NULL  
                 , Error_Msg VARCHAR(MAX)  
                 , Is_Validation_Passed BIT DEFAULT (1)  
             );  
  
        DECLARE  
            @id INT  
            , @site_id INT  
            , @Old_Division_ID INT  
            , @New_Division_ID INT  
            , @entity_id INT  
            , @user_info_id INT  
            , @audit_type INT  
            , @Division_Sitegroup_type_cd INT;
			  
  ----MAINT-9855 Get division type code.code_id = 100001 only start
        SELECT  
            @Division_Sitegroup_type_cd = c.Code_Id  
        FROM  
            dbo.Code c  
            INNER JOIN dbo.Codeset c2  
                ON c2.Codeset_Id = c.Codeset_Id  
        WHERE  
            c2.Codeset_Name = 'SiteGroup_Type'  
            AND c.Code_Value = 'Division'  
            AND c2.Std_Column_Name = 'Sitegroup_type_cd';  
  ----MAINT-9855 Get division type code.code_id = 100001 only END
  
  
        SET @id = 1;  
        SET @audit_type = 2;  
  
        SELECT  
            @user_info_id = ui.USER_INFO_ID  
        FROM  
            dbo.USER_INFO ui  
        WHERE  
            ui.USERNAME = 'conversion'  
            AND ui.FIRST_NAME = 'System';  
  
        BEGIN  
  
            INSERT INTO #temp  
                 (  
                     Division_Name  
                     , Site_Name  
                     , Site_ID  
                     , New_Division  
                    , Email_Address  
                     , Error_Msg  
                     , Is_Validation_Passed  
                 )  
            (SELECT  
                    Division_Name  
                    , Site_Name  
                    , Site_ID  
                    , New_Division  
                    , Email_Address  
                    , Error_Msg  
                    , Is_Validation_Passed  
             FROM  
                    #Update_Sites_To_New_Division_Stage  
             WHERE  
                 Is_Validation_Passed = 1);  
  
            WHILE (@id <= (SELECT   COUNT(*)FROM    #temp))  
                BEGIN  
                    SET @site_id = (SELECT  Site_ID FROM    #temp WHERE ID = @id);  
  
                    SET @Old_Division_ID = (   SELECT  
                                                    c.Sitegroup_Id  
                                               FROM  
                                                    #temp t  
                                                    JOIN Core.Client_Hier c  
                                                        ON t.Site_ID = c.Site_Id  
                                                           AND  t.Division_Name = c.Sitegroup_Name  
                                               WHERE  
                                                    t.ID = @id);  
  
                    SET @New_Division_ID = (   SELECT  
                                                    sg.Sitegroup_Id  
                                               FROM  
                                                    #temp t  
                                                    JOIN Core.Client_Hier c  
                                                        ON t.Site_ID = c.Site_Id  
                                                    JOIN Core.Client_Hier sg  
                                                        ON sg.Client_Id = c.Client_Id  
                                                           AND  t.New_Division = sg.Sitegroup_Name  
                                                           AND  sg.Site_Id = 0
							----MAINT-9855 Added division type Filter with code.code_id = 100001  get the Only division
                                                           AND  sg.Sitegroup_Type_cd = @Division_Sitegroup_type_cd  -- To get the Only division  
                                               WHERE  
                                                    t.ID = @id);  
  
                    -- updating Site table  
                    BEGIN TRY  
                        BEGIN TRANSACTION;  
  
                        UPDATE  
                            s  
                        SET  
                            s.DIVISION_ID = @New_Division_ID  
                        FROM  
                            dbo.SITE s  
                        WHERE  
                            s.SITE_ID = @site_id  
                            AND s.DIVISION_ID = @Old_Division_ID;  
  
  
                        -- Calling sproc to update roles  
  
                        EXEC dbo.Update_Roles_On_Site_Move  
                            @Site_Id = @site_id                     -- int  
                            , @Old_Division_Id = @Old_Division_ID   -- int  
                            , @New_Division_Id = @New_Division_ID;  -- int  
  
  
  
                        -- Updating entity audit table  
  
  
                        EXEC dbo.cbmsEntityAudit_Log  
                            @MyAccountId = @user_info_id    -- int  
                            , @table_name = 'Site_Table'    -- varchar(50)  
                            , @entity_identifier = @site_id -- int  
                            , @audit_type = 'Edit';         -- varchar(10)  
  
                        COMMIT;  
                    END TRY  
                    BEGIN CATCH  
                        IF @@TRANCOUNT > 0  
                            BEGIN  
                                ROLLBACK;  
                            END;  
  
                    END CATCH;  
  
                    SET @id = @id + 1;  
  
                END;  
        END;  
    END;  
  
GO
GRANT EXECUTE ON  [dbo].[Bulk_Upd_Sites_To_New_Divisions] TO [CBMSApplication]
GO
