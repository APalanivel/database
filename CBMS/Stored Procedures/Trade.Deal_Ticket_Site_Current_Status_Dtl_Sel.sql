SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name				DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  195
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  198
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  212
	
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  288
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  288,105
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  288,108
	
	Exec Trade.Deal_Ticket_Current_Status_Sel_By_Deal_Ticket_Id  291
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  131289
	Exec Trade.Deal_Ticket_Site_Current_Status_Dtl_Sel  132270
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava    
    
MODIFICATIONS     
	Initials    Date        Modification      
-------------------------------------------------------------------------------------------------------------------       
	SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Site_Current_Status_Dtl_Sel]
    (
        @Deal_Ticket_Id INT
        , @Workflow_Status_Map_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Tbl_Deal_Ticket_Client_Hier AS TABLE
              (
                  Client_Hier_Id INT
              );
        DECLARE
            @Workflow_Name NVARCHAR(255)
            , @Deal_Ticket_Frequency VARCHAR(25);

        SELECT
            @Workflow_Name = wf.Workflow_Name
            , @Deal_Ticket_Frequency = frq.Code_Value
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Workflow wf
                ON dt.Workflow_Id = wf.Workflow_Id
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt.Deal_Ticket_Frequency_Cd
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;



        INSERT INTO @Tbl_Deal_Ticket_Client_Hier
             (
                 Client_Hier_Id
             )
        SELECT
            dtvol.Client_Hier_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtvol
                ON dt.Deal_Ticket_Id = dtvol.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON dtvol.Client_Hier_Id = cha.Client_Hier_Id
                   AND  dtvol.Account_Id = cha.Account_Id
                   AND  cha.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  cmv.MONTH_IDENTIFIER = dtvol.Deal_Month
                   AND  cmv.CONTRACT_ID = dtvol.Contract_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cmcuc
                ON cmcuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cmcuc.CONVERTED_UNIT_ID = dt.Uom_Type_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION chcuc
                ON chcuc.BASE_UNIT_ID = dtvol.Uom_Type_Id
                   AND  chcuc.CONVERTED_UNIT_ID = dt.Uom_Type_Id
            INNER JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = cmv.MONTH_IDENTIFIER
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND @Workflow_Name IN ( 'Workflow 1', 'Workflow 2' )
        GROUP BY
            dtvol.Client_Hier_Id
        HAVING
            SUM((CASE WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                          dtvol.Total_Volume * chcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM
                     ELSE dtvol.Total_Volume * chcuc.CONVERSION_FACTOR
                 END)) > SUM((CASE WHEN frq.ENTITY_NAME = 'Daily' THEN
                                       cmv.VOLUME * cmcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM
                                  ELSE cmv.VOLUME * cmcuc.CONVERSION_FACTOR
                              END));

        SELECT
            dtch.Deal_Ticket_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , ws.Workflow_Status_Name
            , CASE WHEN ws.Workflow_Status_Name = 'Pending Internal Approval'
                        AND tblch.Client_Hier_Id IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_Contract_Vol_Above_Contract_Vol
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN @Tbl_Deal_Ticket_Client_Hier tblch
                ON ch.Client_Hier_Id = tblch.Client_Hier_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
            AND (   @Workflow_Status_Map_Id IS NULL
                    OR  chws.Workflow_Status_Map_Id = @Workflow_Status_Map_Id)
        GROUP BY
            dtch.Deal_Ticket_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , ws.Workflow_Status_Name
            , CASE WHEN ws.Workflow_Status_Name = 'Pending Internal Approval'
                        AND tblch.Client_Hier_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , ch.Client_Id
            , ch.Sitegroup_Id
            , ch.Site_Id;


    END;



GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Site_Current_Status_Dtl_Sel] TO [CBMSApplication]
GO
