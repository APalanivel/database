SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
-----------------------------------------------------------------------------------    
/******    
NAME:    
	dbo.Sso_Project_Upd

DESCRIPTION:    
	Used to insert or update in to sso_project table  

INPUT PARAMETERS:    
Name						DataType		Default Description    
------------------------------------------------------------    
@MyAccountId				int  
@project_id					int  
@project_title				varchar(600)  
@project_description		varchar(4000)	NULL  
@commodity_type_id			int  
@projected_end_date			datetime  
@project_ownership_type_id	int  
@is_urgent					bit  
@project_status_type_id		int  
@project_category_type_id	int  
@sus_project_subcategory_cd int				-1  


OUTPUT PARAMETERS:    
Name						DataType		Default Description    

------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC dbo.INS_CbmsSsoProject_P -1,3620,'Rate Review','The power contract with Quest Energy expired 3/15/06.  
			This site will go back to tariff based on cost & usage analysis performed by Summit.  
			As per Vic Tuttle, Summit will give notice to Quest Energy regarding the switch back to tariff.   
			*Completed...Jeremy Warner contacted Quest Energy on 1/4/06.', 832,'2006-01-13 00:00:00.000',769,0,703,1488,'2006-01-04'  

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA	 Shobhit Kumar Agrawal

MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
GB					Created  
SKA		08/07/09	Modified as per coding standard
GB		8 Aug 2009	set the default value Null for @sus_project_subcategory_cd instead of -1

******/  

CREATE PROCEDURE [dbo].[Sso_Project_Upd](
	@MyAccountId INT,
	@project_id INT = NULL,
	@project_title VARCHAR(600),
	@project_description VARCHAR(4000) = NULL,
	@commodity_type_id INT,
	@projected_end_date DATETIME,
	@project_ownership_type_id INT,
	@is_urgent BIT,
	@project_status_type_id INT,
	@project_category_type_id INT,
	@sus_project_subcategory_cd INT = NULL,
	@Start_Date DATETIME 
	)  
AS  
BEGIN  

SET NOCOUNT ON  

	DECLARE @this_id INT  
	SET @this_id = @project_id  

	
	BEGIN TRANSACTION
		UPDATE dbo.sso_project  
		SET	 project_title = @project_title,
			project_description = @project_description,
			project_status_type_id = @project_status_type_id,
			commodity_type_id = @commodity_type_id,
			projected_end_date = @projected_end_date,
			project_ownership_type_id = @project_ownership_type_id,
			is_urgent = @is_urgent,
			updated_date = GETDATE(),
			project_category_type_id = @project_category_type_id,
			sus_proj_subcategory_cd = @sus_project_subcategory_cd,
			[START_DATE]=@Start_Date
		WHERE sso_project_id = @this_id  

	EXEC cbmsEntityAudit_Log @MyAccountId, 'sso_project', @This_Id, 'Edit'  

	IF @@ERROR <> 0 
	BEGIN
		ROLLBACK TRANSACTION
	END
	ELSE
	BEGIN
		COMMIT TRANSACTION
	END

END  


GO
GRANT EXECUTE ON  [dbo].[Sso_Project_Upd] TO [CBMSApplication]
GO
