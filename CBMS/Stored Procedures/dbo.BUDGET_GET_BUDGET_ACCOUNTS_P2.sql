SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--select * from budget_account






--select * from budget where budget_id = 954

-- exec BUDGET_GET_BUDGET_ACCOUNTS_P '','',954

create          PROCEDURE dbo.BUDGET_GET_BUDGET_ACCOUNTS_P2
	@userId varchar(10),
	@sessionId varchar(20),
	@budget_id int
	AS
	begin
	set nocount on 

	declare @site_table table(site_id int, site_name varchar(1000))
	
	insert into @site_table(site_id, site_name)
	select site_id, site_name from vwSiteName(nolock)

	select	div.division_name as divisionName,
		vsite.site_name as SiteName,
		service_level.entity_name as service_level_char,
		utilacc.account_id as accountId,
		utilacc.account_number accountNumber,
		utility.vendor_name as utility_name,
		accountTypeVw.Tariff_Transport as tariff_transport,
		user_info.FIRST_NAME+' '+user_info.LAST_NAME as raCompletedBy,
		budget_account.rates_completed_date as raCompletedDate,
		user_info1.FIRST_NAME+' '+user_info1.LAST_NAME as saCompletedBy,
		budget_account.sourcing_completed_date as saCompletedDate,
		user_info2.FIRST_NAME+' '+user_info2.LAST_NAME as cannotPerformedBy,
		budget_account.cannot_performed_date as cannotPerformedDate,
		budget_account.cd_create_multiplier as cdCreateMultiplier,
		budget_commodity.entity_name as commodity,
		budget.budget_start_year as startYear,
		budget.budget_start_month as startMonth,
		budget_account.budget_account_id as budgetAccountId,
		budget_account.is_reforecast,
		budget.original_budget_id,
		budget.posted_by as isPosted

		
	
	from 	budget_account budget_account(nolock)
		join budget budget(nolock)on budget.budget_id = budget_account.budget_id
		and budget_account.budget_id = @budget_id		
		join account utilacc(nolock)on utilacc.account_id = budget_account.account_id
		join budget_account_type_vw accountTypeVw on accountTypeVw.budget_account_id = budget_account.budget_account_id
		and accountTypeVw.commodity_type_id = budget.commodity_type_id
		and accountTypeVw.budget_id = budget.budget_id
		and accountTypeVw.budget_account_type in('A&B', 'C&D Created')
		join @site_table vsite on vsite.site_id = utilacc.site_id
		join site sit on sit.site_id = vsite.site_id
		join division div(nolock)on div.division_id = sit.division_id and div.client_id=budget.client_id
		join client cli(nolock)on cli.client_id = div.client_id
		join vendor utility(nolock)on utility.vendor_id = utilacc.vendor_id
		join entity service_level(nolock)on service_level.entity_id = utilacc.service_level_type_id
		join entity budget_commodity(nolock)on budget_commodity.entity_id = budget.commodity_type_id
		and budget_commodity.entity_type = 157		
		left join user_info user_info(nolock) on budget_account.rates_completed_by = user_info.user_info_id 
		left join user_info user_info1(nolock) on budget_account.sourcing_completed_by = user_info1.user_info_id
		left join user_info user_info2(nolock) on budget_account.cannot_performed_by = user_info2.user_info_id 
		
		

	end























GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_ACCOUNTS_P2] TO [CBMSApplication]
GO
