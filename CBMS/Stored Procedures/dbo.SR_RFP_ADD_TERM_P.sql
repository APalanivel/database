SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_ADD_TERM_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id int,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@term_id int out


USAGE EXAMPLES:
------------------------------------------------------------
---- TEST INSERT ON SR_RFP_TERM
--EXEC dbo.SR_RFP_ADD_TERM_P
--	@rfp_id = 2047 ,
--	@term_id = NULL
	
--NOTE SR_RFP_TERM_ID

--TEST AT SV IF SR_RFP_TERM_ID EXIST
--SELECT SR_RFP_TERM_ID, SR_RFP_ID FROM SR_RFP_TERM 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_ADD_TERM_P
	@rfp_id int,
	@term_id int out
	AS
	
set nocount on
	
	INSERT INTO sr_rfp_term (sr_rfp_id) 
	VALUES(@rfp_id)					

	select @term_id = SCOPE_IDENTITY()
	
RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_ADD_TERM_P] TO [CBMSApplication]
GO
