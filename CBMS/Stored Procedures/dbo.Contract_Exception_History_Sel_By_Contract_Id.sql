SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Exception_History_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Contract_Id				INT
    
OUTPUT PARAMETERS:
	Name										DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
  
      EXEC Contract_Exception_History_Sel_By_Contract_Id 
            @Contract_Id = 227691



AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------------------
	NR       	2019-07-19		Created for Add Contract.

******/

CREATE PROCEDURE [dbo].[Contract_Exception_History_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ae.Exception_Type_Cd
            , c.Code_Value AS Exception_Type
            , ae.Exception_Status_Cd
            , cc.Code_Value AS Exception_Status
            , ae.Created_Ts AS Exception_Created_Ts
            , ae.Created_User_Id AS Exception_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Exception_By_User
            , c2.CONTRACT_START_DATE
            , c2.CONTRACT_END_DATE
            , c2.ED_CONTRACT_NUMBER
            , ae.Service_Desk_Ticket_XId
        FROM
            dbo.Contract_Exception ae
            INNER JOIN dbo.Code c
                ON c.Code_Id = ae.Exception_Type_Cd
            INNER JOIN Code cc
                ON cc.Code_Id = ae.Exception_Status_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ae.Created_User_Id
            INNER JOIN dbo.CONTRACT c2
                ON c2.CONTRACT_ID = ae.Contract_Id
        WHERE
            ae.Contract_Id = @Contract_Id
        GROUP BY
            ae.Exception_Type_Cd
            , c.Code_Value
            , ae.Exception_Status_Cd
            , cc.Code_Value
            , ae.Created_Ts
            , ae.Created_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , c2.CONTRACT_START_DATE
            , c2.CONTRACT_END_DATE
            , c2.ED_CONTRACT_NUMBER
            , ae.Service_Desk_Ticket_XId;
    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_History_Sel_By_Contract_Id] TO [CBMSApplication]
GO
