SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipationQueue_Get]
	( @MyAccountId int
	, @invoice_participation_queue_id int
	)
AS
BEGIN

	   select invoice_participation_queue_id
		, event_type
		, client_id
		, division_id
		, site_id
		, account_id
		, service_month
		, event_by_id
		, event_date
		, invoice_participation_batch_id
	     from invoice_participation_queue WITH (NOLOCK) 
	    where invoice_participation_queue_id = @invoice_participation_queue_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipationQueue_Get] TO [CBMSApplication]
GO
