SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.UPDATE_CONTRACT_IMAGE_INFO_P
	@description VARCHAR(500),
	@document_type_id INT,
	@signature_type_id INT,
	@contract_id INT,
	@cbms_image_id INT
AS
BEGIN
	
	SET NOCOUNT ON

	UPDATE dbo.CONTRACT_CBMS_IMAGE_MAP
		SET DESCRIPTION = @description
			, DOCUMENT_TYPE_ID = @document_type_id
			, SIGNATURE_TYPE_ID =@signature_type_id
	WHERE CONTRACT_ID = @contract_id
		AND CBMS_IMAGE_ID = @cbms_image_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_CONTRACT_IMAGE_INFO_P] TO [CBMSApplication]
GO
