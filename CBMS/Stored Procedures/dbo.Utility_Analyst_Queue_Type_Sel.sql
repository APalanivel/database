SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

                 
/******                        
 NAME: [dbo].[Utility_Analyst_Queue_Type_Sel]            
                        
 DESCRIPTION:                        
				To get list of Queues based on QueueType.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@QueueType					  VARCHAR(25)                   
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
   EXEC dbo.Utility_Analyst_Queue_Type_Sel
			@QueueType='InvoiceProcessingTeam' 

       
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-02-24			Created                
                       
******/ 
   

CREATE PROCEDURE [dbo].[Utility_Analyst_Queue_Type_Sel] ( @QueueType VARCHAR(25) )
AS 
BEGIN  
      SET NOCOUNT ON;  
 
      SELECT
            gi.GROUP_INFO_ID
           ,gi.GROUP_NAME
           ,gi.GROUP_DESCRIPTION
           ,cs.Codeset_Name
      FROM
            dbo.GROUP_INFO gi
            JOIN dbo.QUEUE q
                  ON q.queue_id = gi.QUEUE_ID
            JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = q.QUEUE_TYPE_ID
      WHERE
            cs.Codeset_Name = @QueueType  
   
END  


;
GO
GRANT EXECUTE ON  [dbo].[Utility_Analyst_Queue_Type_Sel] TO [CBMSApplication]
GO
