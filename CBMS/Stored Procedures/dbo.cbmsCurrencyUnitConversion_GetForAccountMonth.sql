SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsCurrencyUnitConversion_GetForAccountMonth]
	( @MyAccountId integer
	, @account_id integer
	, @base_unit_id integer
	, @converted_unit_id integer
	, @service_month datetime
	)
AS
BEGIN

	   select a.account_id
		, cuc.base_unit_id
		, cuc.converted_unit_id
		, cuc.conversion_date
		, cuc.conversion_factor
	     from account a
	     join vwAccountMeter vam on vam.account_id = a.account_id
	     join site s on s.site_id = vam.site_id
	     join division d on d.division_id = s.division_id
	     join client cl on cl.client_id = d.client_id
	     join currency_unit_conversion cuc on cuc.currency_group_id = cl.currency_group_id
	    where a.account_id = @account_id
	      and cuc.base_unit_id = @base_unit_id
	      and cuc.converted_unit_id = @converted_unit_id
	      and cuc.conversion_date = @service_month

END



GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnitConversion_GetForAccountMonth] TO [CBMSApplication]
GO
