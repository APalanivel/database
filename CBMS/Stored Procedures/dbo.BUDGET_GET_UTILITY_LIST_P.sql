SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_GET_UTILITY_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@stateId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        PROCEDURE dbo.BUDGET_GET_UTILITY_LIST_P
@stateId int

AS
begin
set nocount on
	Select v.VENDOR_ID, v.VENDOR_NAME 
               from VENDOR v,
                    VENDOR_STATE_MAP vsm , 
                    entity venType 
               where vsm.STATE_ID = @stateId
                     and v.VENDOR_ID = vsm.VENDOR_ID 
                     and  v.vendor_type_id = venType.entity_id 
                     and venType.entity_name = 'Utility'
               order by v.vendor_name 

end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_UTILITY_LIST_P] TO [CBMSApplication]
GO
