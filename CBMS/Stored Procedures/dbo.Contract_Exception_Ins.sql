SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
                
Name:       
 dbo.Contract_Exception_Ins           
                  
Description:                  
   Contract Level exceptions raised.   
                  
 Input Parameters:                  
 Name							DataType				Default				 Description                    
---------------------------------------------------------------------------------------------    
 @Exception_Type_Cd				INT    
 @Contract_Id					INT    
 @Meter_Id						INT    
 @User_Info_Id					INT    
        
Output Parameters:                        
 Name							DataType				Default				 Description                    
---------------------------------------------------------------------------------------------    
                  
Usage Examples:                      
---------------------------------------------------------------------------------------------    
EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Exception Type'
    , @Code_Value = 'Missing Meter Attribute'   -->Exception Type    
    
   BEGIN TRANSACTION   
    
  SELECT * FROM dbo.Contract_Exception WHERE Contract_Id =166647   
  
EXEC dbo.Contract_Exception_Ins  
    @Contract_Id = 166647  
    , @Exception_Type_Cd = 102902     
    , @User_Info_Id = 49  
     
         
  SELECT * FROM dbo.Contract_Exception WHERE Contract_Id =166647   
  
 ROLLBACK TRANSACTION       
     
Author Initials:                  
 Initials			Name                  
---------------------------------------------------------------------------------------------    
 NR					 Narayana Reddy             
     
Modifications:                  
Initials	Date				Modification                  
---------------------------------------------------------------------------------------------    
 NR			2019-05-30			Add Contract Project - Save Contract exception details.           
******/

CREATE PROCEDURE [dbo].[Contract_Exception_Ins]
    (
        @Contract_Id INT
        , @Exception_Type_Cd INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE
            @Exception_Status_Cd INT
            , @Queue_User_Info_Id INT
            , @Client_Id INT;




        SELECT
            @Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';


        SELECT  TOP 1
                @Client_Id = ch.Client_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id;


        IF EXISTS (   SELECT
                            1
                      FROM
                            dbo.Code c
                            INNER JOIN dbo.Codeset cs
                                ON cs.Codeset_Id = c.Codeset_Id
                      WHERE
                            cs.Codeset_Name = 'Exception Type'
                            AND cs.Std_Column_Name = 'Exception_Type_Cd'
                            AND c.Code_Id = @Exception_Type_Cd
                            AND c.Code_Value = 'Missing Contract Image')
            BEGIN


                SELECT
                    @Queue_User_Info_Id = c.Contract_Admin_User_Info_Id
                FROM
                    dbo.CONTRACT c
                WHERE
                    c.CONTRACT_ID = @Contract_Id;



                SELECT
                    @Queue_User_Info_Id = vcam.Analyst_Id
                FROM
                    Core.Client_Hier_Account uti_Cha
                    INNER JOIN Core.Client_Hier_Account Supp_Cha
                        ON uti_Cha.Client_Hier_Id = Supp_Cha.Client_Hier_Id
                           AND  uti_Cha.Meter_Id = Supp_Cha.Meter_Id
                    INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                        ON uti_Cha.Account_Vendor_Id = vcm.VENDOR_ID
                           AND  uti_Cha.Commodity_Id = vcm.COMMODITY_TYPE_ID
                    INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                        ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                WHERE
                    Supp_Cha.Supplier_Contract_ID = @Contract_Id
                    AND uti_Cha.Account_Type = 'Utility'
                    AND Supp_Cha.Account_Type = 'Supplier'
                    AND @Queue_User_Info_Id IS NULL;

            END;


        IF EXISTS (   SELECT
                            1
                      FROM
                            dbo.Code c
                            INNER JOIN dbo.Codeset cs
                                ON cs.Codeset_Id = c.Codeset_Id
                      WHERE
                            cs.Codeset_Name = 'Exception Type'
                            AND cs.Std_Column_Name = 'Exception_Type_Cd'
                            AND c.Code_Id = @Exception_Type_Cd
                            AND c.Code_Value IN ( 'Missing Valcon Setup' ))
            BEGIN



                SELECT
                    @Queue_User_Info_Id = ISNULL(ui.USER_INFO_ID, rmm.USER_INFO_ID)
                FROM
                    Core.Client_Hier_Account utlity_cha
                    INNER JOIN Core.Client_Hier_Account Sup_cha
                        ON utlity_cha.Client_Hier_Id = Sup_cha.Client_Hier_Id
                           AND  utlity_cha.Meter_Id = Sup_cha.Meter_Id
                    INNER JOIN dbo.UTILITY_DETAIL ud
                        ON ud.VENDOR_ID = utlity_cha.Account_Vendor_Id
                    LEFT JOIN(dbo.Utility_Detail_Analyst_Map map
                              INNER JOIN dbo.GROUP_INFO gi
                                  ON gi.GROUP_INFO_ID = map.Group_Info_ID
                              INNER JOIN dbo.USER_INFO ui
                                  ON ui.USER_INFO_ID = map.Analyst_ID)
                        ON map.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                           AND  gi.GROUP_NAME = 'IAV Analyst Team'
                    LEFT OUTER JOIN dbo.STATE st
                        ON st.STATE_ID = utlity_cha.Meter_State_Id
                    LEFT OUTER JOIN dbo.REGION reg
                        ON reg.REGION_ID = st.REGION_ID
                    LEFT OUTER JOIN dbo.REGION_MANAGER_MAP rmm
                        ON rmm.REGION_ID = reg.REGION_ID
                WHERE
                    Sup_cha.Supplier_Contract_ID = @Contract_Id
                    AND Sup_cha.Account_Type = 'Supplier'
                    AND utlity_cha.Account_Type = 'Utility';


            END;







        SELECT
            @Queue_User_Info_Id = rmm.USER_INFO_ID
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.STATE st
                ON st.STATE_ID = cha.Meter_State_Id
            INNER JOIN dbo.REGION reg
                ON reg.REGION_ID = st.REGION_ID
            INNER JOIN dbo.REGION_MANAGER_MAP rmm
                ON rmm.REGION_ID = reg.REGION_ID
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND @Queue_User_Info_Id IS NULL;



        INSERT INTO dbo.Contract_Exception
             (
                 Contract_Id
                 , Exception_Type_Cd
                 , Queue_Id
                 , Exception_Status_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Contract_Id AS Contract_Id
            , @Exception_Type_Cd AS Exception_Type_Cd
            , ui.QUEUE_ID
            , @Exception_Status_Cd AS Exception_Status_Cd
            , @User_Info_Id AS Created_User_Id
            , GETDATE()
            , @User_Info_Id AS Updated_User_Id
            , GETDATE() AS Last_Change_Ts
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USER_INFO_ID = @Queue_User_Info_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Contract_Exception ae
                               WHERE
                                    ae.Contract_Id = @Contract_Id
                                    AND ae.Exception_Type_Cd = @Exception_Type_Cd
                                    AND ae.Exception_Status_Cd = @Exception_Status_Cd);

    END;










GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_Ins] TO [CBMSApplication]
GO
