SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  [dbo].[ADDCommodityMapping]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType  Default      Description    
---------------------------------------------------------------------------                  
  @ACTCommmodityID   VARCHAR(255)
  @ACTCommmodityName VARCHAR(250)
  @CBMSCommodityID   VARCHAR(MAX)
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
 



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		   Naga Jyothi
   
MODIFICATIONS    
    
 Initials	Date		Modification    
------------------------------------------------------------    
 NJ		   2019-07-06	Created
******/
CREATE PROCEDURE [dbo].[ADDCommodityMapping]
    (
        @ACT_Commmodity_ID VARCHAR(255)
        , @ACT_Commmodity_Name VARCHAR(250)
        , @CBMS_Commodity_ID VARCHAR(MAX)
        , @User_Info_Id INT
    )
AS
    BEGIN


        DECLARE @name NVARCHAR(255);
        DECLARE @pos INT;
        DECLARE @csv TABLE
              (
                  Id VARCHAR(10)
              );
        WHILE CHARINDEX(',', @CBMS_Commodity_ID) > 0
            BEGIN
                SELECT  @pos = CHARINDEX(',', @CBMS_Commodity_ID);
                SELECT  @name = SUBSTRING(@CBMS_Commodity_ID, 1, @pos - 1);

                INSERT INTO @csv SELECT @name;

                SELECT
                    @CBMS_Commodity_ID = SUBSTRING(@CBMS_Commodity_ID, @pos + 1, LEN(@CBMS_Commodity_ID) - @pos);
            END;

        INSERT INTO @csv SELECT @CBMS_Commodity_ID;

        IF @ACT_Commmodity_ID <> 0
            BEGIN -- update

                UPDATE
                    dbo.ACT_Commodity
                SET
                    ACT_Commodity_XName = @ACT_Commmodity_Name
                    , Created_User_Id = @User_Info_Id
                    , Updated_User_Id = @User_Info_Id
                    , Last_Change_Ts = GETDATE()
                WHERE
                    ACT_Commodity_Id = @ACT_Commmodity_ID;

                DELETE
                dbo.Commodity_ACT_Commodity_Map
                WHERE
                    ACT_Commodity_Id = @ACT_Commmodity_ID;

                DECLARE
                    @count AS INTEGER
                    , @Commodity_Id AS INTEGER;
                SET @count = 0;
                SELECT  @count = COUNT(*)FROM   @csv;

                WHILE @count > 0
                    BEGIN
                        SELECT  TOP (1) @Commodity_Id = Id FROM @csv;

                        IF NOT EXISTS (   SELECT
                                                *
                                          FROM
                                                dbo.Commodity_ACT_Commodity_Map cacm
                                          WHERE
                                                cacm.ACT_Commodity_Id = @ACT_Commmodity_ID
                                                AND cacm.Commodity_Id = @Commodity_Id)
                            BEGIN

                                INSERT INTO dbo.Commodity_ACT_Commodity_Map
                                     (
                                         ACT_Commodity_Id
                                         , Commodity_Id
                                         , Created_User_Id
                                         , Created_Ts
                                     )
                                VALUES
                                    (@ACT_Commmodity_ID
                                     , @Commodity_Id
                                     , @User_Info_Id
                                     , GETDATE());
                            END;
                        DELETE TOP (1)  FROM @csv;
                        SET @count = @count - 1;
                    END;



            END;
        ELSE --insert
            BEGIN
                INSERT INTO dbo.ACT_Commodity
                     (
                         ACT_Commodity_XName
                         , Created_User_Id
                         , Created_Ts
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                VALUES
                    (@ACT_Commmodity_Name
                     , @User_Info_Id
                     , GETDATE()
                     , @User_Info_Id
                     , GETDATE());




                DECLARE @count1 AS INTEGER;
                SET @count1 = 0;
                SELECT  @count1 = COUNT(*)FROM  @csv;

                WHILE @count1 > 0
                    BEGIN
                        SELECT  TOP (1) @Commodity_Id = Id FROM @csv;

                        DECLARE @ACT_Commmodity_ID_MAX AS INT;
                        SELECT
                            @ACT_Commmodity_ID_MAX = MAX(ac.ACT_Commodity_Id)
                        FROM
                            dbo.ACT_Commodity ac;
                        INSERT INTO dbo.Commodity_ACT_Commodity_Map
                             (
                                 ACT_Commodity_Id
                                 , Commodity_Id
                                 , Created_User_Id
                                 , Created_Ts
                             )
                        VALUES
                            (@ACT_Commmodity_ID_MAX
                             , @Commodity_Id
                             , @User_Info_Id
                             , GETDATE());

                        DELETE TOP (1)  FROM @csv;
                        SET @count1 = @count1 - 1;
                    END;




            END;


    END;

GO
GRANT EXECUTE ON  [dbo].[ADDCommodityMapping] TO [CBMSApplication]
GO
