SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
                  
 NAME: dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load                     

 DESCRIPTION:      
		  To load TXN trade data from stage table to CBMS tables

 INPUT PARAMETERS:      

	Name                   DataType          Default       Description      
-------------------------------------------------------------------------------------

 OUTPUT PARAMETERS:      

	Name                   DataType          Default       Description      
-------------------------------------------------------------------------------------

 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------

		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load 671

		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load 677

		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load 683

		  EXEC  dbo.RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load 686

 AUTHOR INITIALS:    

	Initials	Name      
-------------------------------------------------------------------------------------
	Raghu       Raghu Reddy                            
                           
 MODIFICATIONS:    
                           
	Initials    Date            Modification    
-------------------------------------------------------------------------------------
	RR          2013-03-15      Created for GRM
	RR			2019-09-03		GRM-1458 & GRM-1496 - Removed the code that deletes the canceled participant/site from deal ticket
	RR			2019-09-30		GRM-1543 Modified to update new trade price irrespective of trade status
	RR			2019-11-01		GRM-1574 Modified to pick and map the latest trade if multiple trades merged       
    RR			2019-11-01		GRM-1597 Modified to update comments even if no change in trade status                   
******/
CREATE PROCEDURE [dbo].[RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load]
     (
         @Deal_Ticket_Id INT = NULL
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Start_Time DATETIME
            , @End_Time DATETIME
            , @System_User_Id INT
            , @Start_LSN BINARY(10)
            , @End_LSN BINARY(10);

        SELECT
            @Start_Time = CAST(App_Config_Value AS DATETIME)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Stage_Deal_Tickets_End_Lsn';

        CREATE TABLE #Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id INT NOT NULL
                 , Workflow_Status_Map_Id INT NOT NULL
                 , Workflow_Status_Comment NVARCHAR(MAX)
                 , Is_Active BIT NOT NULL
                 , Last_Notification_Sent_Ts DATETIME NULL
                 , Created_Ts DATETIME NOT NULL
                 , Updated_User_Id INT NOT NULL
                 , Last_Change_Ts DATETIME NOT NULL
                 , CBMS_Image_Id INT NULL
                 , Trade_Month DATE NULL
                 , Workflow_Task_Id INT NULL
                 , Contract_Id INT NULL
                 , Rnk INT
                 , MAx_Id INT
             );

        CREATE INDEX ix_#Deal_Ticket_Client_Hier_Workflow_Status_Deal_Ticket_Client_Hier_Id
            ON #Deal_Ticket_Client_Hier_Workflow_Status
        (
            Deal_Ticket_Client_Hier_Id
        )   ;

        SET @End_Time = GETDATE();

        SELECT
            @System_User_Id = USER_INFO_ID
        FROM
            dbo.USER_INFO
        WHERE
            USERNAME = 'Conversion';

        SELECT
            @Start_LSN = sys.fn_cdc_map_time_to_lsn('smallest greater than', @Start_Time);
        SELECT
            @End_LSN = sys.fn_cdc_map_time_to_lsn('largest less than or equal', @End_Time);
        SELECT
            @End_LSN = sys.fn_cdc_map_time_to_lsn('largest less than or equal', @End_Time);


        SELECT
            *
        INTO
            #Changes
        FROM
            cdc.fn_cdc_get_net_changes_trade_deal_tickets(@Start_LSN, @End_LSN, N'all') sdt
        WHERE
            __$operation != 1;  -- Excluding deleted records

        CREATE INDEX ix_#Changes
            ON #Changes
        (
            cbms_deal_ticket_id
            , cbms_site_client_hier_id
        )   ;

        SELECT
            dt.cbms_site_client_hier_id
            , dt.cbms_deal_ticket_id
            , dt.period_from_date
            , dt.cbms_contract_id
            , dt.status
            , dt.cbms_trade_number
            , dt.id
            , DENSE_RANK() OVER (PARTITION BY
                                     dt.cbms_site_client_hier_id
                                     , dt.cbms_deal_ticket_id
                                     , dt.period_from_date
                                     , dt.cbms_contract_id
                                     , dt.status
                                 ORDER BY
                                     dt.cbms_trade_number DESC) AS Trade_Rnk
        INTO
            #Trades
        FROM
            #Changes dt;

        /* GRM-1574	If multiple trades merged but still recived all the trades old and new with latest status, then system should
		reatin latest/highset trade by number, so deleting duplicate trades then the system will pick the latest one */
        DELETE
        c
        FROM
            #Changes c
            INNER JOIN #Trades t
                ON t.id = c.id
        WHERE
            t.Trade_Rnk > 1;

        UPDATE
            tp
        SET
            tp.Trade_Price = chng.trade_price
            , tp.Last_Change_Ts = GETDATE()
        /* Updating time stamp only if trade price updated as it is used for sending alert, where as mid_price & market_price
			 are updated without time stamp update in next code block*/

        FROM
            #Changes chng
            JOIN
            (   SELECT
                    sdt.deal_tickets_price_id
                    , MAX(id) Max_Id
                FROM
                    #Changes sdt
                WHERE
                    deal_tickets_price_id IS NOT NULL
                GROUP BY
                    sdt.deal_tickets_price_id) x
                ON x.deal_tickets_price_id = chng.deal_tickets_price_id
                   AND  x.Max_Id = chng.id
            INNER JOIN Trade.Trade_Price tp
                ON tp.Deal_Tickets_Price_XId = chng.deal_tickets_price_id
                   AND  (chng.trade_price <> tp.Trade_Price);

        UPDATE
            tp
        SET
            tp.Mid_Price = chng.mid_price
            , tp.Market_Price = chng.market_price
        FROM
            #Changes chng
            JOIN
            (   SELECT
                    sdt.deal_tickets_price_id
                    , MAX(id) Max_Id
                FROM
                    #Changes sdt
                WHERE
                    deal_tickets_price_id IS NOT NULL
                GROUP BY
                    sdt.deal_tickets_price_id) x
                ON x.deal_tickets_price_id = chng.deal_tickets_price_id
                   AND  x.Max_Id = chng.id
            INNER JOIN Trade.Trade_Price tp
                ON tp.Deal_Tickets_Price_XId = chng.deal_tickets_price_id
                   AND  (   ISNULL(chng.mid_price, 0) <> ISNULL(tp.Mid_Price, 0)
                            OR  ISNULL(chng.market_price, 0) <> ISNULL(tp.Market_Price, 0));

        UPDATE -- GRM-1597 To update only comment of existing trade records
            chws
        SET
            chws.Workflow_Status_Comment = new.comment
            , chws.Last_Change_Ts = GETDATE()
        FROM
            #Changes new
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON new.cbms_deal_ticket_id = dtch.Deal_Ticket_Id
                   AND  new.cbms_site_client_hier_id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Name = new.status
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                   AND  wsm.Workflow_Id = dt.Workflow_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                   AND  new.cbms_contract_id = chws.Contract_Id
                   AND  new.period_from_date = chws.Trade_Month
                   AND  chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
        WHERE
            chws.Workflow_Status_Comment <> new.comment;


        DELETE
        new
        FROM
            #Changes new
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON new.cbms_deal_ticket_id = dtch.Deal_Ticket_Id
                   AND  new.cbms_site_client_hier_id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                   AND  wsm.Workflow_Id = dt.Workflow_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
        WHERE
            ws.Workflow_Status_Name = 'Completed'
            AND (   chws.Trade_Month IS NULL
                    OR  new.period_from_date = chws.Trade_Month)
            AND (   chws.Contract_Id IS NULL
                    OR  new.cbms_contract_id = chws.Contract_Id);



        INSERT INTO Trade.Trade_Price
             (
                 Deal_Tickets_Price_XId
                 , Trade_Price
                 , Mid_Price
                 , Market_Price
                 , Created_Ts
                 , Last_Change_Ts
             )
        SELECT
            chng.deal_tickets_price_id
            , chng.trade_price
            , chng.mid_price
            , chng.market_price
            , GETDATE()
            , GETDATE()
        FROM
            #Changes chng
            JOIN
            (   SELECT
                    sdt.deal_tickets_price_id
                    , MAX(id) Max_Id
                FROM
                    #Changes sdt
                WHERE
                    deal_tickets_price_id IS NOT NULL
                GROUP BY
                    sdt.deal_tickets_price_id) x
                ON x.deal_tickets_price_id = chng.deal_tickets_price_id
                   AND  x.Max_Id = chng.id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.Trade_Price tp
                           WHERE
                                tp.Deal_Tickets_Price_XId = chng.deal_tickets_price_id);

        INSERT INTO #Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , CBMS_Image_Id
                 , Trade_Month
                 , Workflow_Task_Id
                 , Contract_Id
                 , Rnk
                 , MAx_Id
             )
        SELECT
            x.Deal_Ticket_Client_Hier_Id
            , x.Workflow_Status_Map_Id
            , chng.comment
            , x.Is_Active
            , x.Last_Notification_Sent_Ts
            , x.Created_Ts
            , x.Updated_User_Id
            , x.Last_Change_Ts
            , x.CBMS_Image_Id
            , x.period_from_date
            , x.Workflow_Task_Id
            , x.cbms_contract_id
            , DENSE_RANK() OVER (PARTITION BY
                                     x.Deal_Ticket_Client_Hier_Id
                                     , x.cbms_contract_id
                                     , x.period_from_date
                                 ORDER BY
                                     x.Max_Id DESC)
            , x.Max_Id
        FROM
        (   SELECT
                dt.Deal_Ticket_Id
                , dtch.Deal_Ticket_Client_Hier_Id
                , dtch.Client_Hier_Id
                , wsm.Workflow_Status_Map_Id
                , 0 Is_Active
                , NULL Last_Notification_Sent_Ts
                , GETDATE() Created_Ts
                , 16 Updated_User_Id
                , GETDATE() Last_Change_Ts
                , NULL CBMS_Image_Id
                , sdt.period_from_date
                , NULL Workflow_Task_Id
                , sdt.cbms_contract_id
                , MAX(sdt.id) AS Max_Id
            FROM
                #Changes sdt
                INNER JOIN Trade.Deal_Ticket dt
                    ON sdt.cbms_deal_ticket_id = dt.Deal_Ticket_Id
                INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                    ON sdt.cbms_site_client_hier_id = dtch.Client_Hier_Id
                       AND  dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                INNER JOIN Trade.Workflow_Status ws
                    ON ws.Workflow_Status_Name = sdt.status
                INNER JOIN Trade.Workflow_Status_Map wsm
                    ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                       AND  wsm.Workflow_Id = dt.Workflow_Id
            WHERE
                (   @Deal_Ticket_Id IS NULL
                    OR  sdt.cbms_deal_ticket_id = @Deal_Ticket_Id)
            GROUP BY
                dtch.Deal_Ticket_Client_Hier_Id
                , wsm.Workflow_Status_Map_Id
                , sdt.cbms_contract_id
                , sdt.period_from_date
                , dt.Deal_Ticket_Id
                , dtch.Client_Hier_Id) x
        LEFT OUTER JOIN #Changes chng
            ON chng.id = x.Max_Id
               AND  chng.cbms_deal_ticket_id = x.Deal_Ticket_Id
               AND  chng.cbms_site_client_hier_id = x.Client_Hier_Id;


        UPDATE
            ws
        SET
            ws.Is_Active = 0
        FROM
            Trade.Deal_Ticket_Client_Hier_Workflow_Status ws
        WHERE
            EXISTS -- This sub query in exists clause returns only the DT,Site, month, contract which was not loaded yet in CBMS
        (   SELECT
                1
            FROM
                #Deal_Ticket_Client_Hier_Workflow_Status ns
            WHERE
                NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier_Workflow_Status ows
                               WHERE
                                    ows.Deal_Ticket_Client_Hier_Id = ns.Deal_Ticket_Client_Hier_Id
                                    AND (ows.Contract_Id = ns.Contract_Id)
                                    AND (ows.Trade_Month = ns.Trade_Month)
                                    AND ows.Workflow_Status_Map_Id = ns.Workflow_Status_Map_Id)
                AND ns.Deal_Ticket_Client_Hier_Id = ws.Deal_Ticket_Client_Hier_Id
                AND (   ws.Contract_Id IS NULL
                        OR  ISNULL(ns.Contract_Id, -1) = ISNULL(ws.Contract_Id, -1))
                AND (   ws.Trade_Month IS NULL
                        OR  ns.Trade_Month = ws.Trade_Month))
            AND ws.Is_Active = 1;


        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , CBMS_Image_Id
                 , Trade_Month
                 , Workflow_Task_Id
                 , Contract_Id
             )
        SELECT
            Deal_Ticket_Client_Hier_Id
            , Workflow_Status_Map_Id
            , Workflow_Status_Comment
            , CASE WHEN Rnk = 1 THEN 1
                  ELSE 0
              END AS Is_Active
            , Last_Notification_Sent_Ts
            , Created_Ts
            , Updated_User_Id
            , Last_Change_Ts
            , CBMS_Image_Id
            , Trade_Month
            , Workflow_Task_Id
            , Contract_Id
        FROM
            #Deal_Ticket_Client_Hier_Workflow_Status ns
        WHERE
            NOT EXISTS --- Added this NOT EXISTS to avoid adding duplicate records in to this table , this helps to avoid duplicate records load if TXN sends the same records again.
        (   SELECT
                1
            FROM
                Trade.Deal_Ticket_Client_Hier_Workflow_Status ows
            WHERE
                ows.Deal_Ticket_Client_Hier_Id = ns.Deal_Ticket_Client_Hier_Id
                AND ISNULL(ows.Contract_Id, -1) = ISNULL(ns.Contract_Id, -1)
                AND ISNULL(ows.Trade_Month, '1900-01-01') = ISNULL(ns.Trade_Month, '1900-01-01')
                AND ows.Workflow_Status_Map_Id = ns.Workflow_Status_Map_Id);

        UPDATE
            txnsts
        SET
            txnsts.Cbms_Trade_Number = sdt.cbms_trade_number
            , txnsts.Id = ws.MAx_Id
            , txnsts.Assignee = sdt.assignee
            , txnsts.Trade_Price_Id = ttp.Trade_Price_Id
            , txnsts.Cbms_Counterparty_Id = sdt.cbms_counterparty_id
            , txnsts.Date_Data_Amended = sdt.date_data_amended
            , txnsts.Last_Change_Ts = GETDATE()
        FROM
            #Deal_Ticket_Client_Hier_Workflow_Status ws
            INNER JOIN #Changes sdt
                ON sdt.id = ws.MAx_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = ws.Deal_Ticket_Client_Hier_Id
                   AND  dtchws.Trade_Month = ws.Trade_Month
                   AND  dtchws.Workflow_Status_Map_Id = ws.Workflow_Status_Map_Id
                   AND  ISNULL(dtchws.Contract_Id, -1) = ISNULL(ws.Contract_Id, -1)
            INNER JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status txnsts
                ON txnsts.Deal_Ticket_Client_Hier_Workflow_Status_Id = dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id
            LEFT JOIN Trade.Trade_Price ttp
                ON ttp.Deal_Tickets_Price_XId = sdt.deal_tickets_price_id;

        INSERT INTO Trade.Deal_Ticket_Client_Hier_TXN_Status
             (
                 Deal_Ticket_Client_Hier_Workflow_Status_Id
                 , Cbms_Trade_Number
                 , Date_Data_Amended
                 , Transaction_Status
                 , Trade_Price_Id
                 , Cbms_Counterparty_Id
                 , Id
                 , Assignee
                 , Created_Ts
                 , Last_Change_Ts
             )
        SELECT
            dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id
            , sdt.cbms_trade_number
            , sdt.date_data_amended
            , sdt.status
            , ttp.Trade_Price_Id
            , sdt.cbms_counterparty_id
            , sdt.id
            , sdt.assignee
            , GETDATE()
            , GETDATE()
        FROM
            #Deal_Ticket_Client_Hier_Workflow_Status ws
            INNER JOIN #Changes sdt
                ON sdt.id = ws.MAx_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
                ON dtchws.Deal_Ticket_Client_Hier_Id = ws.Deal_Ticket_Client_Hier_Id
                   AND  dtchws.Trade_Month = ws.Trade_Month
                   AND  dtchws.Workflow_Status_Map_Id = ws.Workflow_Status_Map_Id
                   AND  ISNULL(dtchws.Contract_Id, -1) = ISNULL(ws.Contract_Id, -1)
            LEFT JOIN Trade.Trade_Price ttp
                ON ttp.Deal_Tickets_Price_XId = sdt.deal_tickets_price_id
        WHERE
            (   @Deal_Ticket_Id IS NULL
                OR  sdt.cbms_deal_ticket_id = @Deal_Ticket_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier_TXN_Status txnsts
                               WHERE
                                    dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id = txnsts.Deal_Ticket_Client_Hier_Workflow_Status_Id)
        GROUP BY
            dtchws.Deal_Ticket_Client_Hier_Workflow_Status_Id
            , sdt.cbms_trade_number
            , sdt.date_data_amended
            , sdt.status
            , ttp.Trade_Price_Id
            , sdt.cbms_counterparty_id
            , sdt.id
            , sdt.assignee;

        UPDATE
            hv
        SET
            hv.Trade_Number = x.cbms_trade_number
            , hv.Trade_Price_Id = x.Trade_Price_Id
            , hv.Trade_Executed_Ts = x.trade_date
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl hv
            JOIN
            (   SELECT
                    chng.cbms_deal_ticket_id
                    , chng.cbms_site_client_hier_id
                    , chng.cbms_trade_number
                    , chng.period_from_date
                    , chng.cbms_contract_id
                    , chng.trade_date
                    , chng.id
                    , chng.trade_price
                    , tp.Trade_Price_Id
                    , ROW_NUMBER() OVER (PARTITION BY
                                             chng.cbms_deal_ticket_id
                                             , chng.cbms_site_client_hier_id
                                             , chng.period_from_date
                                             , chng.cbms_contract_id
                                         ORDER BY
                                             chng.id DESC) AS Latest_Record
                FROM
                    #Changes chng
                    LEFT OUTER JOIN Trade.Trade_Price tp
                        ON tp.Deal_Tickets_Price_XId = chng.deal_tickets_price_id
                WHERE
                    (   chng.cbms_trade_number IS NOT NULL
                        OR  (   chng.deal_tickets_price_id IS NOT NULL
                                AND chng.trade_price IS NOT NULL))  --- There are records with price id not null but the trade price is null
            ) x
                ON x.cbms_deal_ticket_id = hv.Deal_Ticket_Id
                   AND  x.cbms_site_client_hier_id = hv.Client_Hier_Id
                   AND  ISNULL(x.cbms_contract_id, -1) = hv.Contract_Id
                   AND  x.period_from_date = hv.Deal_Month
                   AND  x.Latest_Record = 1
                   AND  (   x.cbms_trade_number <> ISNULL(hv.Trade_Number, -1)
                            OR  ISNULL(x.Trade_Price_Id, -1) <> ISNULL(hv.Trade_Price_Id, -1)
                            OR  ISNULL(x.trade_date, '1900-01-01') <> ISNULL(hv.Trade_Executed_Ts, '1900-01-01'));

        UPDATE
            dtchvd
        SET
            dtchvd.Total_Volume = 0
        FROM
            #Deal_Ticket_Client_Hier_Workflow_Status dtchws
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Client_Hier_Id = dtchws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dtchvd.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                   AND  dtchvd.Contract_Id = dtchws.Contract_Id
                   AND  dtchvd.Deal_Month = dtchws.Trade_Month
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws2
                ON dtchws2.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                   AND  dtchws2.Contract_Id = dtchvd.Contract_Id
                   AND  dtchws2.Trade_Month = dtchvd.Deal_Month
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Map_Id = dtchws2.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            ws.Workflow_Status_Name = 'Canceled';


        SELECT  @End_Time = sys.fn_cdc_map_lsn_to_time(@End_LSN);

        UPDATE
            dbo.App_Config
        SET
            App_Config_Value = CONVERT(VARCHAR(200), @End_Time, 126)
        WHERE
            App_Config_Cd = 'RM_Stage_Deal_Tickets_End_Lsn';

        DROP TABLE #Changes;
        DROP TABLE #Trades;
        DROP TABLE #Deal_Ticket_Client_Hier_Workflow_Status;


    END;





GO
GRANT EXECUTE ON  [dbo].[RM_CBMS_TXN_Deal_Ticket_Trade_Data_Load] TO [CBMSApplication]
GO
