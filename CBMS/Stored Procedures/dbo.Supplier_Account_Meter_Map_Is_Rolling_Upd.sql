SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                            
 NAME: dbo.Supplier_Account_Meter_Map_Is_Rolling_Upd         
                            
 DESCRIPTION:                            
   Update the Is Rolling based on Meter and Contract.                        
                            
 INPUT PARAMETERS:              
                         
 Name       DataType         Default       Description            
------------------------------------------------------------------------------         
@User_Info_Id     INT    
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------         
  
BEGIN TRAN

EXEC dbo.Supplier_Account_Meter_Map_Is_Rolling_Upd
    @Contract_Id = 1
    , @Meter_Id = 1
    , @Is_Rolling_Meter = 1

ROLLBACK TRAN
   
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
------------------------------------------------------------------------------         
 NR						Narayana Reddy    
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
------------------------------------------------------------------------------         
 NR                    2019-05-29       Created for - Add Contract.    
                           
******/
CREATE PROCEDURE [dbo].[Supplier_Account_Meter_Map_Is_Rolling_Upd]
     (
         @Contract_Id INT
         , @Meter_Id INT
         , @Is_Rolling_Meter BIT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            samm
        SET
            samm.Is_Rolling_Meter = @Is_Rolling_Meter
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        WHERE
            samm.Contract_ID = @Contract_Id
            AND samm.METER_ID = @Meter_Id;

    END;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Meter_Map_Is_Rolling_Upd] TO [CBMSApplication]
GO
