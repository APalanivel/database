SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*SELECT 
	budget.client_id CLIENT_ID,cli.client_name CLIENT_NAME,
	budget.division_id DIVISION_ID, budget.site_id SITE_ID,
	MONTH_IDENTIFIER MIDENTIFIER,TOTAL_VOLUME_HEDGED VOLUME,
	budget.budget_start_month START_MONTH,budget.budget_end_month END_MONTH,
	budget.budget_creation_date CREATION_DATE
FROM 
	RM_TARGET_BUDGET target, rm_budget budget,
	client cli
WHERE 
	budget.RM_BUDGET_ID=143 and 
	budget.RM_BUDGET_ID=target.RM_BUDGET_ID and 
	budget.client_id=cli.client_id

*/

--exec dbo.GET_CLIENT_GENERATED_BUDGET_P 40,-1,175

CREATE    PROCEDURE dbo.GET_CLIENT_GENERATED_BUDGET_P
@userId varchar(10),
@sessionId varchar(20),
@budgetId int

AS
	SELECT 
		budget.client_id CLIENT_ID,budget.budget_name BUDGET_NAME,cli.client_name CLIENT_NAME,
		budget.division_id DIVISION_ID, budget.site_id SITE_ID,
		budget.budget_start_month START_MONTH,budget.budget_end_month END_MONTH,
		budget.budget_creation_date CREATION_DATE,
		MONTH_IDENTIFIER MIDENTIFIER,budget_target VOLUME

	FROM 
		RM_TARGET_BUDGET target, rm_budget budget,
		client cli
	WHERE 
		budget.RM_BUDGET_ID=@budgetId and 
		budget.RM_BUDGET_ID=target.RM_BUDGET_ID and 
		budget.client_id=cli.client_id 

	order by MONTH_IDENTIFIER asc
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_GENERATED_BUDGET_P] TO [CBMSApplication]
GO
