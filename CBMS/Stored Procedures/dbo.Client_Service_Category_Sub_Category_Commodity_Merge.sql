SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******          
NAME: dbo.Client_Service_Category_Sub_Category_Commodity_Merge          
          
          
DESCRIPTION:           
          
	Used to merge the commodities of the Level-2 category added to Level-3 Category

INPUT PARAMETERS:              
      Name     DataType          Default     Description              
------------------------------------------------------------------              
      @Client_Service_Category_id   INT          
      @Sub_Client_Service_Category_Id VARCHAR(MAX) )  list of sub client service categories with comma separator

OUTPUT PARAMETERS:              
      Name              DataType          Default     Description              
------------------------------------------------------------              
          
          
USAGE EXAMPLES:          
------------------------------------------------------------          
          
BEGIN TRAN

	DECLARE @SUB_CLIENT_SERVICE_CATEGORY_ID VARCHAR(MAX)  = '77,64'

	EXEC CLIENT_SERVICE_CATEGORY_SUB_CATEGORY_COMMODITY_MERGE  29 ,@SUB_CLIENT_SERVICE_CATEGORY_ID
	SELECT TOP 10 * FROM CLIENT_SERVICE_CATEGORY_COMMODITY WHERE CLIENT_SERVICE_CATEGORY_ID = 29

ROLLBACK TRAN

	select TOP 10 * from Client_Service_Category_Commodity WHERE CLient_Service_Category_id = 29
	SELECT TOP 10 * FROM CLIENT_SERVICE_CATEGORY_COMMODITY WHERE CLIENT_SERVICE_CATEGORY_ID in (22,64)

	select top 10 * from Client_Service_Category

AUTHOR INITIALS:          
 Initials Name          
------------------------------------------------------------          
 BCH   Balaraju          
          
MODIFICATIONS          

 Initials	Date		Modification
------------------------------------------------------------          
 BCH		2011-10-19  Created

******/
CREATE PROCEDURE dbo.Client_Service_Category_Sub_Category_Commodity_Merge
      ( 
       @Client_Service_Category_id INT
      ,@Sub_Client_Service_Category_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Sub_Client_Category_Id_List TABLE
            (
             Sub_Client_Service_Category_Id INT )

      INSERT      INTO @Sub_Client_Category_Id_List
                  (
                   Sub_Client_Service_Category_Id )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Sub_Client_Service_Category_Id, ',')

      MERGE INTO dbo.Client_Service_Category_Commodity tgt
            USING
                  ( SELECT
                        @Client_Service_Category_Id AS Client_Service_Category_Id
                       ,csc.Client_Service_Category_Id AS Sub_Client_Service_Category_Id
                       ,csc.commodity_id
                    FROM
                        @Sub_Client_Category_Id_List cl
                        JOIN dbo.Client_Service_Category_Commodity csc
                              ON csc.Client_Service_Category_Id = cl.Sub_Client_Service_Category_Id ) src
            ON tgt.client_service_category_id = src.Client_Service_Category_Id
                  AND tgt.Commodity_Id = src.Commodity_Id
                  AND tgt.Sub_Client_Service_Category_Id = src.Sub_Client_Service_Category_Id
            WHEN NOT MATCHED
                  THEN
   INSERT
                        (
                         Client_Service_Category_Id
                        ,Commodity_id
                        ,Sub_Client_Service_Category_Id )
                    VALUES
                        ( 
                         src.Client_Service_Category_Id
                        ,src.commodity_id
                        ,src.Sub_Client_Service_Category_Id )
            WHEN  NOT MATCHED BY SOURCE AND tgt.client_service_category_id = @Client_Service_Category_id
                  AND tgt.Sub_Client_Service_Category_Id IS NOT NULL
                  THEN
 DELETE ;

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Sub_Category_Commodity_Merge] TO [CBMSApplication]
GO
