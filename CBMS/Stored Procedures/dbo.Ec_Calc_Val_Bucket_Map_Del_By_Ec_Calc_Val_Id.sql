SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:   dbo.Ec_Calc_Val_Bucket_Map_Del_By_Ec_Calc_Val_Id           
                      
Description:                      
   This sproc to delete the Buckets & sub Buckets for given Calc val ID.              
                                   
 Input Parameters:                      
    Name            DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
@EC_Calc_Val_Id       INT      
                         
         
 Output Parameters:                            
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                      
 Usage Examples:                          
----------------------------------------------------------------------------------------           
  BEGIN TRAN      
  EXEC Ec_Calc_Val_Bucket_Map_Del_By_Ec_Calc_Val_Id 1  
  ROLLBACK      
        
        
             
           
Author Initials:                      
    Initials  Name                      
----------------------------------------------------------------------------------------                        
   
 Modifications:                      
    Initials        Date   Modification                      
----------------------------------------------------------------------------------------                        
   VRV  Venkata Reddy Vanga
        
                     
******/  
  
CREATE PROCEDURE [dbo].[Ec_Calc_Val_Bucket_Map_Del_By_Ec_Calc_Val_Id]  
     (  
         @EC_Calc_Val_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        BEGIN TRY  
            BEGIN TRAN;  
  
  
  
  
  
  
  
            DELETE  
            ecvbsbm  
            FROM  
                dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm  
                INNER JOIN dbo.Ec_Calc_Val_Bucket_Map ecvbm  
                    ON ecvbm.Ec_Calc_Val_Bucket_Map_Id = ecvbsbm.Ec_Calc_Val_Bucket_Map_Id  
            WHERE  
                ecvbm.Ec_Calc_Val_Id = @EC_Calc_Val_Id;  
  
  
  
  
  
            DELETE  FROM dbo.Ec_Calc_Val_Bucket_Map WHERE   Ec_Calc_Val_Id = @EC_Calc_Val_Id;  
  
  
  
            COMMIT TRAN;  
  
  
        END TRY  
        BEGIN CATCH  
  
            IF @@TRANCOUNT > 0  
                BEGIN  
                    ROLLBACK TRAN;  
                END;  
  
            EXEC dbo.usp_RethrowError;  
  
        END CATCH;  
  
  
  
    END;  
GO
GRANT EXECUTE ON  [dbo].[Ec_Calc_Val_Bucket_Map_Del_By_Ec_Calc_Val_Id] TO [CBMSApplication]
GO
