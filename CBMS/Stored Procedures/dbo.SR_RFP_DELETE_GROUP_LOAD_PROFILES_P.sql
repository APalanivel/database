SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_GROUP_LOAD_PROFILES_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId varchar,
@sessionId varchar,
@siteId int,
@cbmsImageId int,
@rfpId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
-- TEST 1
--EXEC SR_RFP_DELETE_GROUP_LOAD_PROFILES_P 1,1,2,1,130218


---- TEST 2
---- Delete an entry for Test
--EXEC [dbo].[SR_RFP_DELETE_GROUP_LOAD_PROFILES_P] 
--@userId =-1,
--@sessionId =-1,
--@siteId =12258,
--@cbmsImageId =251037,
--@rfpId =6

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_GROUP_LOAD_PROFILES_P] 

@userId varchar,
@sessionId varchar,
@siteId int,
@cbmsImageId int,
@rfpId int

AS
	
set nocount on

		DELETE FROM 
		SR_RFP_LP_CLIENT_APPROVAL 
		WHERE 
		SITE_ID= @siteId and SR_RFP_ID=@rfpId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_GROUP_LOAD_PROFILES_P] TO [CBMSApplication]
GO
