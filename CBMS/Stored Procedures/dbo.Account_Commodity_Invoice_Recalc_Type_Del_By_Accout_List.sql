SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Sel_Account_Contract

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT	
	@Contract_Id				INT				
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
    
BEGIN TRAN

EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Del_By_Accout_List
    @Contract_Id = 1
    , @Account_List = '1655697'
    , @Commodity_Id = 290

ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	NR       	2019-10-30		Created for Add Contract.
	 
******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Del_By_Accout_List]
    (
        @Contract_Id INT
        , @Account_List NVARCHAR(MAX)
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Source_cd INT;


        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';



        DELETE
        acirt
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.ufn_split(@Account_List, ',') us
                ON us.Segments = acirt.Account_Id
        WHERE
            acirt.Source_Cd = @Source_cd
            AND (   @Commodity_Id IS NULL
                    OR  acirt.Commodity_ID = @Commodity_Id)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.CONTRACT c
                           WHERE
                                c.CONTRACT_ID = @Contract_Id
                                AND (   acirt.Start_Dt BETWEEN c.CONTRACT_START_DATE
                                                       AND     c.CONTRACT_END_DATE
                                        OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                               AND     c.CONTRACT_END_DATE
                                        OR  c.CONTRACT_START_DATE BETWEEN acirt.Start_Dt
                                                                  AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  c.CONTRACT_END_DATE BETWEEN acirt.Start_Dt
                                                                AND     ISNULL(acirt.End_Dt, '9999-12-31')));




    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Del_By_Accout_List] TO [CBMSApplication]
GO
