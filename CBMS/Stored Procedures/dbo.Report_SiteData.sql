SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
   
 dbo.Report_SiteData  
   
DESCRIPTION:  
 This report is to get All the Site data for Client \Clients  
  
INPUT PARAMETERS:  
Name   DataType  Default Description  
-------------------------------------------------------------------------------------------  
@CLIENT_LIST VARCHAR(MAX)  
@Site_Active INT   
@Primary_Address INT   
   
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
  
Report_SiteData '10014',-1,1  
Report_SiteData '10014',-1,0  
Report_SiteData '10014',-1,-1  
   
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
SSR   Sharad srivastava  
AKR         Ashok Kumar Raju  
 MODIFICATIONS Initials Date  Modification  
------------------------------------------------------------  
 SSR    07/28/2010 Created  
 AKR       09/21/2011   Added ZipCode Column  
 AKR       2014-06-19   Added ad.GEO_LAT,ad.GEO_LONG columns in select list.  
 LEC	2019-02-21  added site reference number
*/  
CREATE PROCEDURE [dbo].[Report_SiteData]
      ( 
       @CLIENT_LIST VARCHAR(MAX)
      ,@Site_Active INT
      ,@Primary_Address INT )
AS 
BEGIN   
      SET NOCOUNT ON   
  
      DECLARE @Ent_aud INT   
   
      SELECT
            @Ent_aud = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_DESCRIPTION = 'Table_Type'
            AND ent.ENTITY_NAME = 'SITE_TABLE'  
      
      SELECT
            c.CLIENT_NAME [Client]
           ,ent_cl_typ.ENTITY_NAME [Client Type]
           ,st.Sitegroup_Name [Division Name]
           ,CASE WHEN dv_dtl.NOT_MANAGED = 0 THEN 'Yes'
                 ELSE 'No'
            END [Division Managed]
           ,s.SITE_NAME [Site Name]
           ,s.SITE_ID [Site ID]
           ,CASE WHEN s.NOT_MANAGED = 0 THEN 'Yes'
                 ELSE 'No'
            END [Site Managed]
           ,CASE WHEN s.CLOSED = 1 THEN 'Yes'
                 ELSE 'No'
            END [Site Closed]
           ,ad.ADDRESS_LINE1 [Address]
           ,ad.CITY [City]
           ,stt.STATE_NAME [State]
           ,ad.ZIPCODE [Zip Code]
           ,cntry.COUNTRY_NAME [Country]
           ,regn.REGION_DESCRIPTION [Region]
           ,CASE WHEN ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID THEN 'Yes'
                 ELSE 'No'
            END [Primary]
           ,ent_busi.ENTITY_NAME [Site Type ID]
           ,s.DOING_BUSINESS_AS [DOING BUSINESS AS]
           ,s.CONTRACTING_ENTITY [Contracting Entity]
           ,s.TAX_NUMBER [FEIN]
           ,s.DUNS_NUMBER [DUNS]
           ,s.LP_CONTACT_FIRST_NAME + SPACE(1) + s.LP_CONTACT_LAST_NAME [Load Profile Contact Name]
           ,s.LP_CONTACT_EMAIL_ADDRESS [Load Profile Email Address]
           ,s.LP_CONTACT_CC [Load Profile Contact CC]
           ,uinf.FIRST_NAME + SPACE(1) + uinf.LAST_NAME [Site Created By]
           ,ad.ADDRESS_ID
           ,s.PRIMARY_ADDRESS_ID
           ,ad.ADDRESS_PARENT_ID
           ,ad.GEO_LAT
           ,ad.GEO_LONG
		   ,s.site_reference_number
      FROM
            dbo.CLIENT c
            JOIN dbo.ufn_split(@CLIENT_LIST, ',') ufn_User_tmp
                  ON CAST(ufn_User_tmp.Segments AS INT) = c.CLIENT_ID
            JOIN dbo.SITE s
                  ON s.Client_ID = c.CLIENT_ID
            JOIN dbo.Sitegroup st
                  ON st.Sitegroup_Id = s.DIVISION_ID
                     AND st.Client_Id = c.CLIENT_ID
            JOIN dbo.division_dtl dv_dtl
                  ON dv_dtl.SiteGroup_Id = st.Sitegroup_Id
                     AND dv_dtl.CLIENT_ID = c.CLIENT_ID
            JOIN dbo.ENTITY_AUDIT ent_aud
                  ON ent_aud.ENTITY_IDENTIFIER = s.SITE_ID
            JOIN dbo.USER_INFO uinf
                  ON uinf.USER_INFO_ID = ent_aud.USER_INFO_ID
            JOIN dbo.ENTITY ent_cl_typ
                  ON ent_cl_typ.ENTITY_ID = c.CLIENT_TYPE_ID
            JOIN dbo.ENTITY ent_busi
                  ON ent_busi.ENTITY_ID = s.SITE_TYPE_ID
            LEFT JOIN dbo.ADDRESS ad
                  ON ad.ADDRESS_PARENT_ID = s.SITE_ID
            LEFT JOIN dbo.STATE stt
                  ON stt.STATE_ID = ad.STATE_ID
            LEFT JOIN COUNTRY cntry
                  ON cntry.COUNTRY_ID = stt.COUNTRY_ID
            LEFT JOIN REGION regn
                  ON regn.REGION_ID = stt.REGION_ID
      WHERE
            ent_aud.AUDIT_TYPE = 1
            AND ent_aud.ENTITY_ID = @Ent_aud
            AND ( @Site_Active = -1
                  OR s.NOT_MANAGED = @Site_Active )
            AND ( ( @Primary_Address = 1
                    AND ad.ADDRESS_ID = s.PRIMARY_ADDRESS_ID )
                  OR ( @Primary_Address = -1 )
                  OR ( @Primary_Address = 0
                       AND ad.ADDRESS_ID <> s.PRIMARY_ADDRESS_ID ) )  
END  
  
  
  
;
GO

GRANT EXECUTE ON  [dbo].[Report_SiteData] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_SiteData] TO [CBMSApplication]
GO
