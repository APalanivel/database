SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[User_Info_Upd_Division_Id_Null_By_User_Info_Id]  

DESCRIPTION:

	To Update Division column as NULL in user_info for the given user_info_id.	

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION
--------------------------------------------------------------------
	@User_Info_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
	   EXEC dbo.User_Info_Upd_Division_Id_Null_By_User_Info_Id 202
	ROLLBACK TRAN

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.User_Info_Upd_Division_Id_Null_By_User_Info_Id
    (
		@User_Info_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	UPDATE 
		dbo.User_Info
		SET DIVISION_ID = NULL
	WHERE
		User_Info_Id = @User_Info_Id
	
END
GO
GRANT EXECUTE ON  [dbo].[User_Info_Upd_Division_Id_Null_By_User_Info_Id] TO [CBMSApplication]
GO
