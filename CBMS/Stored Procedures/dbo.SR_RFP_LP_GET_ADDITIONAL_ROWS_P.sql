SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_RFP_LP_GET_ADDITIONAL_ROWS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_id int
	AS
	set nocount on
	select 	row.sr_rfp_lp_account_additional_row_id, 
		row.row_name,
		row.row_value
	from 	sr_rfp_lp_account_additional_row row(nolock), sr_rfp_account rfp_account(nolock)
	where	row.sr_rfp_account_id = @rfp_account_id
		and rfp_account.sr_rfp_account_id = row.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and row.row_type_id != (select entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'New')
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_ADDITIONAL_ROWS_P] TO [CBMSApplication]
GO
