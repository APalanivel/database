
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[SR_RFP_GET_RFP_PRICE_PRODUCTS_P]
           
DESCRIPTION:             
			To get service condition templates matching RFP commodity and accounts geography 
			
INPUT PARAMETERS:            
	Name		DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Rfp_Id	INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13258
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13245
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13263
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13299
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13268
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13254
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13298
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13273
	EXEC dbo.SR_RFP_GET_RFP_PRICE_PRODUCTS_P  1,1,13293
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-28	Global Sourcing - Phase3 - GCS-564 Selecting templates defined for the products

******/
CREATE   PROCEDURE [dbo].[SR_RFP_GET_RFP_PRICE_PRODUCTS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@rfpId INT ) -- for showing system level product for particular commodity with in RFP
AS 
BEGIN
      SET NOCOUNT ON;
       
      DECLARE @Commodity_Id INT
       
      DECLARE @Acc_Country_Commodity AS TABLE
            ( 
             Country_Id INT
            ,Commodity_Id INT
            ,Id INT IDENTITY(1, 1) )
            
      DECLARE @Products_Templates AS TABLE
            ( 
             SR_RFP_PRICING_PRODUCT_ID INT
            ,Template_Name NVARCHAR(255) )
            
      SELECT
            @Commodity_Id = COMMODITY_TYPE_ID
      FROM
            dbo.SR_RFP
      WHERE
            SR_RFP_ID = @rfpId
      
      INSERT      INTO @Acc_Country_Commodity
                  ( 
                   Country_Id
                  ,Commodity_Id )
                  SELECT
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfpacc.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = rfp.COMMODITY_TYPE_ID
                  WHERE
                        rfp.SR_RFP_ID = @rfpId
                  GROUP BY
                        cha.Meter_Country_Id
                       ,rfp.COMMODITY_TYPE_ID
                       
      INSERT      INTO @Products_Templates
                  ( 
                   SR_RFP_PRICING_PRODUCT_ID
                  ,Template_Name )
                  SELECT
                        rfpprod.SR_RFP_PRICING_PRODUCT_ID
                       ,ssct.Template_Name
                  FROM
                        dbo.SR_RFP_PRICING_PRODUCT rfpprod
                        INNER JOIN dbo.SR_PRICING_PRODUCT spp
                              ON rfpprod.PRODUCT_NAME = spp.PRODUCT_NAME
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON sppcm.SR_Pricing_Product_Id = spp.SR_PRICING_PRODUCT_ID
                        INNER JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctpm
                              ON ssctpm.SR_Pricing_Product_Country_Map_Id = sppcm.SR_Pricing_Product_Country_Map_Id
                        INNER JOIN dbo.Sr_Service_Condition_Template ssct
                              ON ssct.Sr_Service_Condition_Template_Id = ssctpm.Sr_Service_Condition_Template_Id
                        INNER JOIN @Acc_Country_Commodity acccom
                              ON acccom.Country_Id = sppcm.Country_Id
                                 AND spp.COMMODITY_TYPE_ID = acccom.Commodity_Id
                  WHERE
                        rfpprod.SR_RFP_ID = @rfpId
                        AND spp.COMMODITY_TYPE_ID = @Commodity_Id
                  GROUP BY
                        rfpprod.SR_RFP_PRICING_PRODUCT_ID
                       ,ssct.Template_Name
                       
      INSERT      INTO @Products_Templates
                  ( 
                   SR_RFP_PRICING_PRODUCT_ID
                  ,Template_Name )
                  SELECT
                        rfpprod.SR_RFP_PRICING_PRODUCT_ID
                       ,rfpssct.Template_Name
                  FROM
                        dbo.SR_RFP_PRICING_PRODUCT rfpprod
                        LEFT JOIN dbo.Sr_Service_Condition_Template rfpssct
                              ON rfpprod.Sr_Service_Condition_Template_Id = rfpssct.Sr_Service_Condition_Template_Id
                  WHERE
                        rfpprod.SR_RFP_ID = @rfpId
                  GROUP BY
                        rfpprod.SR_RFP_PRICING_PRODUCT_ID
                       ,rfpssct.Template_Name
                        
      SELECT
            SR_RFP_PRICING_PRODUCT_ID
           ,1 AS IS_RFP_PRODUCT
           ,PRODUCT_NAME
           ,PRODUCT_DESCRIPTION
           ,left(Template.Templates, len(Template.Templates) - 1) AS Template_Name
      FROM
            dbo.SR_RFP_PRICING_PRODUCT rfpprod
            CROSS APPLY ( SELECT
                              tmplts.Template_Name + ','
                          FROM
                              @Products_Templates tmplts
                          WHERE
                              tmplts.SR_RFP_PRICING_PRODUCT_ID = rfpprod.SR_RFP_PRICING_PRODUCT_ID
                              AND tmplts.Template_Name IS NOT NULL
                          GROUP BY
                              tmplts.Template_Name
            FOR
                          XML PATH('') ) Template ( Templates )
      WHERE
            rfpprod.SR_RFP_ID = @rfpId

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_RFP_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
