SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cu_Invoice_Recalc_Determinant_Ins           
              
Description:              
        To insert Data to Cu_Invoice_Account_Commodity_Recalc_Lock table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  @Recalc_Header_Id						INT
  @EC_Invoice_Sub_Bucket_Master_Id		INT
  @Sub_Bucket_Name						NVARCHAR(255)
  @Determinant_Value					DECIMAL(28, 10)
  @Uom_Name								NVARCHAR(255)
  @Uom_Type_Id							INT
  @Expected_Slots						INT
  @Actual_Slots							INT
  @Missing_Slots						INT
  @Edited_Slots							INT      
              
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                

   BEGIN TRAN
    
 DECLARE @tvp_Cu_Invoice_Recalc_Response_Sub_Buckets tvp_Cu_Invoice_Recalc_Response_Sub_Buckets 
   
 INSERT     INTO @tvp_Cu_Invoice_Recalc_Response_Sub_Buckets
            ( Sub_Bucket_Name, Determinant_Value, Uom_Name, Expected_Slots, Actual_Slots, Missing_Slots, Edited_Slots )
 VALUES
            ( 'Actual Demand', '1235', 'KWh', 300, NULL, 200, 100 ),
            ( 'Total Usage', '27941', 'KWh', 400, NULL, 0, 200 )
 SELECT
      *
 FROM
      dbo.Cu_Invoice_Recalc_Determinant  
    
 EXEC dbo.Cu_Invoice_Recalc_Determinant_Ins 
      @Recalc_Header_Id = 1457080
     ,@tvp_Cu_Invoice_Recalc_Response_Sub_Buckets = @tvp_Cu_Invoice_Recalc_Response_Sub_Buckets
     
     
     
 SELECT
      *
 FROM
      dbo.Cu_Invoice_Recalc_Determinant 
	
	
 ROLLBACK TRAN
       
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR			   Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2018-03-15		Recalc Interval data.         
             
******/ 

CREATE PROC [dbo].[Cu_Invoice_Recalc_Determinant_Ins]
      (
       @Recalc_Header_Id INT
      ,@tvp_Cu_Invoice_Recalc_Response_Sub_Buckets AS tvp_Cu_Invoice_Recalc_Response_Sub_Buckets READONLY )
AS
BEGIN 
      SET NOCOUNT ON       

      DECLARE
            @State_Id INT
           ,@Cu_Invoice_Id INT
           ,@Commodity_Id INT
           ,@Uom_Entity_Type INT

      SELECT
            @State_Id = cha.Meter_State_Id
           ,@Cu_Invoice_Id = rh.CU_INVOICE_ID
           ,@Commodity_Id = rh.Commodity_Id
      FROM
            dbo.RECALC_HEADER rh
            INNER JOIN Core.Client_Hier_Account cha
                  ON cha.Account_Id = rh.ACCOUNT_ID
                     AND cha.Commodity_Id = rh.Commodity_Id
      WHERE
            rh.RECALC_HEADER_ID = @Recalc_Header_Id

      SELECT
            @Uom_Entity_Type = UOM_Entity_Type
      FROM
            dbo.Commodity
      WHERE
            Commodity_Id = @Commodity_Id

      INSERT      INTO dbo.Cu_Invoice_Recalc_Determinant
                  (Recalc_Header_Id
                  ,EC_Invoice_Sub_Bucket_Master_Id
                  ,Sub_Bucket_Name
                  ,Determinant_Value
                  ,Uom_Name
                  ,Uom_Type_Id
                  ,Expected_Slots
                  ,Actual_Slots
                  ,Missing_Slots
                  ,Edited_Slots
                  ,Last_Change_Ts )
                  SELECT
                        @Recalc_Header_Id
                       ,eisbm.EC_Invoice_Sub_Bucket_Master_Id
                       ,tcirrsb.Sub_Bucket_Name
                       ,tcirrsb.Determinant_Value
                       ,tcirrsb.Uom_Name
                       ,uom.ENTITY_ID
                       ,tcirrsb.Expected_Slots
                       ,tcirrsb.Actual_Slots
                       ,tcirrsb.Missing_Slots
                       ,tcirrsb.Edited_Slots
                       ,GETDATE()
                  FROM
                        @tvp_Cu_Invoice_Recalc_Response_Sub_Buckets tcirrsb
                        LEFT OUTER  JOIN dbo.ENTITY uom
                              ON uom.ENTITY_NAME = tcirrsb.Uom_Name
                                 AND uom.ENTITY_TYPE = @Uom_Entity_Type
                        LEFT OUTER JOIN ( dbo.EC_Invoice_Sub_Bucket_Master eisbm
                                          INNER JOIN dbo.Bucket_Master bm
                                                ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
                                          INNER JOIN dbo.Code bt
                                                ON bt.Code_Id = bm.Bucket_Type_Cd
                                                   AND bt.Code_Value = 'Determinant' )
                              ON eisbm.Sub_Bucket_Name = tcirrsb.Sub_Bucket_Name
                                 AND bm.Commodity_Id = @Commodity_Id
                                 AND eisbm.State_Id = @State_Id
                                 

END
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Determinant_Ins] TO [CBMSApplication]
GO
