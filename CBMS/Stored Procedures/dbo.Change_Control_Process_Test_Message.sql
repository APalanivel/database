SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Change_Control_Process_Test_Message

DESCRIPTION:
	Activated by Change_Control_Queue, it processes messages
	of //Change_Control/Message/TestMessage
	
	Procedure Only Responds with a message of type of //Change_Control/Message/Broker_Ack


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	,@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed by Change_Control_Receive_Table_Changes
 when a message type is Recieved

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			11/04/2011	Created
******/
CREATE PROCEDURE [dbo].[Change_Control_Process_Test_Message]
(	@Message				XML						
	,@Conversation_Handle	UNIQUEIDENTIFIER	)
AS 
BEGIN
	SET NOCOUNT ON; 
	
	SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Broker_Ack] 
END

GO
GRANT EXECUTE ON  [dbo].[Change_Control_Process_Test_Message] TO [CBMSApplication]
GO
