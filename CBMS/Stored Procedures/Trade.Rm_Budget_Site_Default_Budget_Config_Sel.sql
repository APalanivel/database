SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Default_Budget_Config_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Site_Default_Budget_Config_Sel  @Client_Id = 1005,@Start_Index=1,@End_Index = 25
	EXEC Trade.Rm_Budget_Site_Default_Budget_Config_Sel  @Client_Id = 15118,@Start_Index=1,@End_Index = 25
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2020-02-17	GRM-1712 - Added new input filter @Participant_Sites
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Default_Budget_Config_Sel]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Country_Id NVARCHAR(MAX) = NULL
        , @State_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @Site_Not_Managed INT = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
        , @Currency_Unit_Id INT = NULL
        , @Uom_Id INT = NULL
        , @Index_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @RM_Group_Id INT = NULL
        , @Participant_Sites VARCHAR(20) = 'All Sites'
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , Site_name VARCHAR(1000)
             );

        CREATE TABLE #Sites
             (
                 Client_Hier_Id INT
                 , SITE_ID INT
                 , Site_Name VARCHAR(1000)
                 , Site_Rnk INT
             );

        DECLARE
            @Total_Cnt INT
            , @All_Sites BIT = 0
            , @Hedgable_Sites BIT = 0
            , @Reported_Sites BIT = 0;

        SELECT  @All_Sites = 1 WHERE @Participant_Sites = 'All Sites';
        SELECT  @Hedgable_Sites = 1 WHERE   @Participant_Sites = 'Hedgeable Sites';
        SELECT  @Reported_Sites = 1 WHERE   @Participant_Sites = 'Reported Sites';

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_name
             )
        EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id
            @Cbms_Sitegroup_Id = @RM_Group_Id;

        INSERT INTO #Sites
             (
                 Client_Hier_Id
                 , SITE_ID
                 , Site_Name
                 , Site_Rnk
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
            , DENSE_RANK() OVER (ORDER BY
                                     RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')') AS Site_Rnk
        FROM
            Core.Client_Hier ch
        WHERE
            (   @Client_Id IS NULL
                OR  ch.Client_Id = @Client_Id)
            AND ch.Site_Id > 0
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') us
                                   WHERE
                                        CAST(us.Segments AS INT) = ch.Country_Id))
            AND (   @State_Id IS NULL
                    OR  ch.State_Id = @State_Id)
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   NULLIF(@Site_Id, '') IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Id, ',') us
                                   WHERE
                                        CAST(us.Segments AS INT) = ch.Site_Id))
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND (   @All_Sites = 1
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard siteob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                   WHERE
                                        siteob.Client_Hier_Id = ch.Client_Hier_Id
                                        AND siteob.Commodity_Id = @Commodity_Id
                                        AND @All_Sites = 0
                                        AND (   @Hedgable_Sites = 0
                                                OR  et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
                                        AND (   @Reported_Sites = 0
                                                OR  chhc.Include_In_Reports = 1)
                                        AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                     AND     @End_Dt
                                                OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                       AND     @End_Dt
                                                OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                              AND     chhc.Config_End_Dt
                                                OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                            AND     chhc.Config_End_Dt))
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard clntob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                        INNER JOIN Core.Client_Hier clch
                                            ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                   WHERE
                                        clch.Sitegroup_Id = 0
                                        AND clch.Client_Id = ch.Client_Id
                                        AND clntob.Country_Id = ch.Country_Id
                                        AND clntob.Commodity_Id = @Commodity_Id
                                        AND @All_Sites = 0
                                        AND (   @Hedgable_Sites = 0
                                                OR  et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
                                        AND (   @Reported_Sites = 0
                                                OR  chhc.Include_In_Reports = 1)
                                        AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                     AND     @End_Dt
                                                OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                       AND     @End_Dt
                                                OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                              AND     chhc.Config_End_Dt
                                                OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                            AND     chhc.Config_End_Dt)
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Trade.RM_Client_Hier_Onboard siteob1
                                                           WHERE
                                                                siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                                AND siteob1.Commodity_Id = @Commodity_Id)));

        SELECT  @Total_Cnt = MAX(Site_Rnk)FROM  #Sites s;



        WITH Cte_Sites
        AS (
               SELECT
                    ch.Client_Hier_Id
                    , rb.Rm_Budget_Id
                    , rb.Budget_NAME
                    , rb.Budget_Start_Dt
                    , rb.Budget_End_Dt
                    , indx.ENTITY_NAME AS Index_Name
                    , uom.ENTITY_NAME AS UOM_Name
                    , cu.CURRENCY_UNIT_NAME
                    , REPLACE(CONVERT(VARCHAR(20), rbsdbc.Last_Change_Ts, 106), ' ', '-') AS Last_Updated
                    , rbsdbc.Start_Dt
                    , rbsdbc.End_Dt
                    , rbsdbc.Rm_Budget_Site_Default_Budget_Config_Id
                    , CASE rb.Is_Client_Generated WHEN 1 THEN 'Yes'
                          WHEN 0 THEN 'No'
                          ELSE NULL
                      END AS Is_Client_Generated
                    , ch.SITE_ID
                    , rbsdbc.Last_Change_Ts
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
               FROM
                    #Sites ch
                    LEFT JOIN Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                        ON rbsdbc.Client_Hier_Id = ch.Client_Hier_Id
                    LEFT JOIN Trade.Rm_Budget rb
                        ON rb.Rm_Budget_Id = rbsdbc.Rm_Budget_Id
                           AND  (   @Commodity_Id IS NULL
                                    OR  rb.Commodity_Id = @Commodity_Id)
                    LEFT JOIN dbo.ENTITY indx
                        ON indx.ENTITY_ID = rb.Index_Id
                    LEFT JOIN dbo.ENTITY uom
                        ON rb.Uom_Type_Id = uom.ENTITY_ID
                    LEFT JOIN dbo.CURRENCY_UNIT cu
                        ON rb.Currency_Unit_Id = cu.CURRENCY_UNIT_ID
                    LEFT JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = rbsdbc.Last_Updated_By
               WHERE
                    ch.Site_Rnk BETWEEN @Start_Index
                                AND     @End_Index
                    AND (   (   @Start_Dt IS NULL
                                AND @End_Dt IS NULL)
                            OR  (   @Start_Dt IS NOT NULL
                                    AND @End_Dt IS NULL
                                    AND @Start_Dt BETWEEN rb.Budget_Start_Dt
                                                  AND     rb.Budget_End_Dt)
                            OR  (   @Start_Dt IS NULL
                                    AND @End_Dt IS NOT NULL
                                    AND @End_Dt BETWEEN rb.Budget_Start_Dt
                                                AND     rb.Budget_End_Dt)
                            OR  (   @Start_Dt BETWEEN rb.Budget_Start_Dt
                                              AND     rb.Budget_End_Dt
                                    OR  @End_Dt BETWEEN rb.Budget_Start_Dt
                                                AND     rb.Budget_End_Dt
                                    OR  rb.Budget_Start_Dt BETWEEN @Start_Dt
                                                           AND     @End_Dt
                                    OR  rb.Budget_End_Dt BETWEEN @Start_Dt
                                                         AND     @End_Dt))
                    AND (   @Index_Id IS NULL
                            OR  rb.Index_Id = @Index_Id)
                    AND (   @Currency_Unit_Id IS NULL
                            OR  rb.Currency_Unit_Id = @Currency_Unit_Id)
                    AND (   @Uom_Id IS NULL
                            OR  rb.Uom_Type_Id = @Uom_Id)
           )
        SELECT
            s.Site_Name
            , s.Client_Hier_Id
            , cs.Rm_Budget_Id
            , cs.Budget_NAME
            , cs.Budget_Start_Dt
            , cs.Budget_End_Dt
            , cs.Index_Name
            , cs.UOM_Name
            , cs.CURRENCY_UNIT_NAME
            , cs.Last_Updated
            , s.Site_Rnk
            , cs.Start_Dt
            , cs.End_Dt
            , cs.Rm_Budget_Site_Default_Budget_Config_Id
            , cs.Is_Client_Generated
            , s.SITE_ID
            , cs.Last_Change_Ts
            , cs.Last_Updated_By
            , REPLACE(CONVERT(VARCHAR(20), cs.Last_Change_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), cs.Last_Change_Ts, 100), 7)) + ' EST' + ' by ' + cs.Last_Updated_By AS Last_Updated_Ts_User
            , @Total_Cnt AS Total_Cnt
        FROM
            #Sites s
            LEFT JOIN Cte_Sites cs
                ON cs.Client_Hier_Id = s.Client_Hier_Id
        WHERE
            s.Site_Rnk BETWEEN @Start_Index
                       AND     @End_Index;

    END;



GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Default_Budget_Config_Sel] TO [CBMSApplication]
GO
