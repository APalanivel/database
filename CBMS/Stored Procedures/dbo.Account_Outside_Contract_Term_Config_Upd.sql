SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Outside_Contract_Term_Config_Upd          
              
Description:              
        To insert Data into Oct account update table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Id						INT
     @Commodity_Id						INT
     @@Config_Start_Dt							DATE
     @@Config_End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Account_Outside_Contract_Term_Config 
    EXEC dbo.Account_Outside_Contract_Term_Config_Upd 
      @Account_Outside_Contract_Term_Config_Id =1148520
     ,@OCT_Parameter_Cd = 1
	 ,@OCT_Tolerance_Date_Cd = 1
	 ,@OCT_Dt_Range_Cd = 1
     ,@Config_Start_Dt = '2015-01-01'
     ,@Config_End_Dt= NULL   
     ,@User_Info_Id = 49
    
	SELECT * FROM Account_Outside_Contract_Term_Config 

	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/
CREATE PROCEDURE [dbo].[Account_Outside_Contract_Term_Config_Upd]
    (
        @Account_Outside_Contract_Term_Config_Id INT
        , @OCT_Parameter_Cd INT
        , @OCT_Tolerance_Date_Cd INT
        , @OCT_Dt_Range_Cd INT
        , @Config_Start_Dt DATE
        , @Config_End_Dt DATE = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        UPDATE
            cloct
        SET
            cloct.OCT_Parameter_Cd = @OCT_Parameter_Cd
            , cloct.OCT_Tolerance_Date_Cd = @OCT_Tolerance_Date_Cd
            , cloct.OCT_Dt_Range_Cd = @OCT_Dt_Range_Cd
            , cloct.Config_Start_Dt = @Config_Start_Dt
            , cloct.Config_End_Dt = @Config_End_Dt
            , cloct.Updated_User_Id = @User_Info_Id
            , cloct.Last_Change_Ts = GETDATE()
        FROM
            dbo.Account_Outside_Contract_Term_Config cloct
        WHERE
            cloct.Account_Outside_Contract_Term_Config_Id = @Account_Outside_Contract_Term_Config_Id;



    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Outside_Contract_Term_Config_Upd] TO [CBMSApplication]
GO
