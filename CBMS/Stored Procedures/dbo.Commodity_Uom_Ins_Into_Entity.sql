
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: [DBO].[Commodity_Uom_Ins_Into_Entity]

DESCRIPTION:

	This stored procedure used to save the uom mapped to a commodity in to Entity table and load the default conversion factors in Consumption_Unit_Conversion & Consumption_Unit_Conversion

INPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
	@Commodity_Id					INT
	@Associated_Uom_Cd				VARCHAR(max) NULL		Comma seperated UOM cd value selected by the user

OUTPUT PARAMETERS:
	NAME							DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
	@Uom_Entity_Type_Id				INT

USAGE EXAMPLES:
-----------------------------------------------------------------------

	BEGIN TRAN
	
		DECLARE @Uom_Entity_Type INT

		EXEC dbo.Commodity_Uom_Ins_Into_Entity
			@Commodity_Id = 57
			,@Associated_Uom_Cd = '100089,100095,100109,100086'
			,@Uom_Entity_Type_id = @Uom_Entity_Type

	ROLLBACK TRAN

    SELECT
      *
    FROM
      ENTITY e
      JOIN Code cd
            ON cd.Code_Value = e.Entity_Name
	WHERE
		ENTITY_TYPE = 717
		OR e.Entity_Description = 'Units for Coal'
	
	SELECT * FROM dbo.Commodity WHERE commodity_id = 57	

AUTHOR INITIALS:
	INITIALS	NAME
-----------------------------------------------------------------------
	HG			Harihara Suthan G
	NR          Narayana Reddy

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
-----------------------------------------------------------------------
	HG			2013-04-22	Created for manage services requirement
	HG			2013-09-04	MAINT-2166, changed the logic to get the entity_description as the EP, NG and other services are having different pattern for entity description
	NR          2014-09-10  MAINT-3036  To update the Display_order column in Entity table.

*/

CREATE PROCEDURE [dbo].[Commodity_Uom_Ins_Into_Entity]
      ( 
       @Commodity_Id INT
      ,@Associated_Uom_Cd VARCHAR(MAX)
      ,@Uom_Entity_Type_Id INT OUT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Associated_Uom_List TABLE
            ( 
             Uom_Cd INT PRIMARY KEY CLUSTERED
            ,Uom_Name VARCHAR(25) )

      DECLARE @Saved_Entity_List TABLE
            ( 
             Entity_Id INT PRIMARY KEY CLUSTERED )

      DECLARE
            @Uom_Entity_Type INT
           ,@Commodity_Name VARCHAR(50)
           ,@DISPLAY_ORDER INT

		-- Save the uom names in to Entity table
		-- Save the default conversion factor for the usage in to Consumption_Unit_Conversion_Factor table

      SELECT
            @Uom_Entity_Type = MAX(ENTITY_TYPE)
      FROM
            dbo.ENTITY

	-- Get the Uom_Entity_Type from commodity table if the commodity is already saved in to the system

      SET @Uom_Entity_Type = ISNULL(@Uom_Entity_Type, 0) + 1

      SELECT
            @Uom_Entity_Type = ISNULL(com.UOM_Entity_Type, @Uom_Entity_Type)
           ,@Commodity_Name = com.Commodity_Name
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Id = @Commodity_Id
            
      SELECT
            @DISPLAY_ORDER = MAX(ISNULL(e.DISPLAY_ORDER, 0))
      FROM
            dbo.ENTITY e
      WHERE
            ENTITY_TYPE = @Uom_Entity_Type

      BEGIN TRY
            BEGIN TRAN

            INSERT      INTO @Associated_Uom_List
                        ( 
                         Uom_Cd
                        ,Uom_Name )
                        SELECT
                              cd.Code_Id
                             ,cd.Code_Value
                        FROM
                              dbo.ufn_split(@Associated_Uom_Cd, ',') uomcd
                              INNER JOIN dbo.Code cd
                                    ON cd.Code_Id = uomcd.Segments

		-- Insert the new uom mappings
            INSERT      INTO dbo.ENTITY
                        ( 
                         ENTITY_NAME
                        ,ENTITY_TYPE
                        ,ENTITY_DESCRIPTION
                        ,DISPLAY_ORDER )
            OUTPUT      INSERTED.Entity_Id
                        INTO @Saved_Entity_List
                        SELECT
                              aul.Uom_Name
                             ,@Uom_Entity_Type
                             ,CASE @Commodity_Name
                                WHEN 'Electric Power' THEN 'Unit for electricity'
                                WHEN 'Natural Gas' THEN 'Unit for Gas'
                                WHEN 'Alternative Natural Gas' THEN 'Unit for alternate fuel'
                                ELSE 'Units for ' + @Commodity_Name
                              END
                             ,ROW_NUMBER() OVER ( ORDER BY aul.Uom_Name ) + @DISPLAY_ORDER AS DISPLAY_ORDER
                        FROM
                              @Associated_Uom_List aul
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Entity uom
                                           WHERE
                                                uom.ENTITY_TYPE = @Uom_Entity_Type
                                                AND uom.Entity_Name = aul.Uom_Name )

			-- Add default conversion factors for the new uoms

            INSERT      INTO dbo.CONSUMPTION_UNIT_CONVERSION
                        ( 
                         BASE_UNIT_ID
                        ,CONVERTED_UNIT_ID
                        ,CONVERSION_FACTOR )
                        SELECT
                              el.Entity_Id
                             ,el.Entity_Id
                             ,1
                        FROM
                              @Saved_Entity_List el

            INSERT      INTO dbo.Commodity_UOM_Conversion
                        ( 
                         Base_Commodity_Id
                        ,Base_UOM_Cd
                        ,Converted_Commodity_Id
                        ,Converted_UOM_Cd
                        ,Conversion_Factor
                        ,Is_Active )
                        SELECT
                              @Commodity_Id
                             ,uomcd.Uom_Cd
                             ,@Commodity_Id
                             ,uomcd.Uom_Cd
                             ,1
                             ,1
                        FROM
                              @Associated_Uom_List uomcd
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Commodity_UOM_Conversion cuc
                                           WHERE
                                                cuc.Base_Commodity_Id = @Commodity_Id
                                                AND cuc.Base_UOM_Cd = uomcd.Uom_Cd
                                                AND cuc.Converted_Commodity_Id = @Commodity_Id
                                                AND cuc.Converted_UOM_Cd = uomcd.Uom_Cd )

            SET @Uom_Entity_Type_Id = @Uom_Entity_Type

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK
                  END

            EXEC dbo.usp_RethrowError

      END CATCH

END;

GO


GRANT EXECUTE ON  [dbo].[Commodity_Uom_Ins_Into_Entity] TO [CBMSApplication]
GO
