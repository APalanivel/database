SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsPricePointValue_Save]
	( @MyAccountId int
	, @price_point_value_id int
	, @price_point_id int
	, @close_month datetime
	, @close_price decimal(32,16)
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @price_point_value_id

	if @this_id is null
	begin

	   select @this_id = price_index_value_id
	     from price_index_value
	    where price_index_id = @price_point_id
	      and index_month = @close_month

	end


	if @this_id is null
	begin

		insert into price_index_value
			( price_index_id, index_value, index_month )
		 values
			( @price_point_id, @close_price, @close_month )

		set @this_id = @@IDENTITY

	end
	else
	begin

		   update price_index_value
		      set index_value = @close_price
		    where price_index_value_id = @this_id

	end

--	set nocount off

	exec cbmsPricePointValue_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsPricePointValue_Save] TO [CBMSApplication]
GO
