SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cu_Invoice_Standing_Data_Exception_Route_To_User       
              
Description:              
			This sproc route the queue id updated in the Cu_Invoice_Standing_Data_Exception table.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Routed_To_User_Queue				INT					
    @Cu_Invoice_Standing_Data_Exception_Ids				INT	
    @User_Info_Id						INT				
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
    BEGIN TRAN
    EXEC dbo.Cu_Invoice_Standing_Data_Exception_Route_To_User 
      49
     ,'1,2'
     ,49
    ROLLBACK TRAN

    BEGIN TRAN
    EXEC dbo.Cu_Invoice_Standing_Data_Exception_Route_To_User 
      49
     ,'10,12,15,18'
     ,49
    ROLLBACK TRAN

   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna             
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2020-06-11		Created For AS400.         
             
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Standing_Data_Exception_Route_To_User]
     (
         @Routed_To_User_Queue INT
         , @Cu_Invoice_Standing_Data_Exception_Ids VARCHAR(MAX)
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Cu_Invoice_Standing_Data_Exception TABLE
              (
                  Cu_Invoice_Standing_Data_Exception_Id INT
              );




        INSERT  @Cu_Invoice_Standing_Data_Exception
             (
                 Cu_Invoice_Standing_Data_Exception_Id
             )
        SELECT
            us.Segments
        FROM
            dbo.ufn_split(@Cu_Invoice_Standing_Data_Exception_Ids, ',') AS us
        GROUP BY
            us.Segments;


        UPDATE
            cisde
        SET
            cisde.Queue_Id = @Routed_To_User_Queue
            , cisde.Updated_User_Id = @User_Info_Id
            , cisde.Last_Change_Ts = GETDATE()
        FROM
            dbo.Cu_Invoice_Standing_Data_Exception cisde
            INNER JOIN @Cu_Invoice_Standing_Data_Exception cisde2
                ON cisde2.Cu_Invoice_Standing_Data_Exception_Id = cisde.Cu_Invoice_Standing_Data_Exception_Id;

    END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Standing_Data_Exception_Route_To_User] TO [CBMSApplication]
GO
