SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsSsoSavingsOwner_RemoveForOwner]
	( @MyAccountId int
	, @savings_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_savings_owner_map
	   where sso_savings_id = @savings_id

--	exec cbmsPermissionInfo_GetMy @MyAccountId, @group_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavingsOwner_RemoveForOwner] TO [CBMSApplication]
GO
