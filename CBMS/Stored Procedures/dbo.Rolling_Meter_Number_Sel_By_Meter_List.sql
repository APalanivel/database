SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Rolling_Meter_Number_Sel_By_Meter_List    
    
DESCRIPTION:    
    
 Rolling_Meters.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    

select top 1 * from supplier_account_meter_map where meter_id = 340354

EXEC dbo.Rolling_Meter_Number_Sel_By_Meter_List @Meter_Id ='340354'

    
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
  
******/

CREATE PROCEDURE [dbo].[Rolling_Meter_Number_Sel_By_Meter_List]
    (
        @Meter_Id VARCHAR(MAX)
    )
AS
    SET NOCOUNT ON;

    BEGIN
        SELECT
            samm.METER_ID
            , m.METER_NUMBER
            , samm.Is_Rolling_Meter
            , samm.Contract_ID
            , samm.METER_DISASSOCIATION_DATE 
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
            INNER JOIN dbo.METER m
                ON m.METER_ID = samm.METER_ID
        WHERE
            samm.Is_Rolling_Meter = 1
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Meter_Id, ',') us
                           WHERE
                                us.Segments = m.METER_ID)
        GROUP BY
            samm.METER_ID
            , m.METER_NUMBER
            , samm.Is_Rolling_Meter
            , samm.Contract_ID
           , samm.METER_DISASSOCIATION_DATE 

    END;


GO
GRANT EXECUTE ON  [dbo].[Rolling_Meter_Number_Sel_By_Meter_List] TO [CBMSApplication]
GO
