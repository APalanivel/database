SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_PREVIOUS_NYMEX_PRICE_FOR_BATCH_REPORTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE       PROCEDURE dbo.GET_PREVIOUS_NYMEX_PRICE_FOR_BATCH_REPORTS_P 
@userId varchar(10),
@sessionId varchar(20)

as
set nocount on

select	DESCRIPTION,
	CAST(CLOSE_PRICE as decimal(7,3)) CLOSE_PRICE

from	RM_NYMEX_DATA

where BATCH_EXECUTION_DATE=(select MAX(BATCH_EXECUTION_DATE) from RM_NYMEX_DATA)
GO
GRANT EXECUTE ON  [dbo].[GET_PREVIOUS_NYMEX_PRICE_FOR_BATCH_REPORTS_P] TO [CBMSApplication]
GO
