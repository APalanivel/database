
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME: [DBO].[Bucket_Category_Rule_Sel_By_Account_Aggregation_Level]    
       
DESCRIPTION:  
  
 To get the list of bucket rules based on the given Account and aggregation level  
  
INPUT PARAMETERS:  
NAME     DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
@Account_id    INT  
@CU_Aggregation_Level VARCHAR(25)  
                  
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION     
         
------------------------------------------------------------            
USAGE EXAMPLES:            
------------------------------------------------------------    
 EXEC Bucket_Category_Rule_Sel_By_Account_Aggregation_Level  30008, 'MultiMonth'  
 EXEC Bucket_Category_Rule_Sel_By_Account_Aggregation_Level  34970, 'Site'  
  
 SELECT DISTINCT COmmodity_Id FROM Core.Client_Hier_Account WHERE Account_id = 30008  
  
AUTHOR INITIALS:  
INITIALS	NAME  
------------------------------------------------------------  
HG			Harihara Suthan G  
RKV         Ravi Kumar Vegesna
  
MODIFICATIONS  
INITIALS	DATE		MODIFICATION  
------------------------------------------------------------            
HG			08/04/2011  Created for Additional Data enhancement requirement
HG			2012-01-18	Modified the script to filter the records based on the aggregation level
RKV         2016-10-04  Added two Parameters Country_Id and State_Id,also added new join 
					    Country_State_Category_Bucket_Aggregation_Rule as part of PF Enhancement
*/

CREATE PROCEDURE [dbo].[Bucket_Category_Rule_Sel_By_Account_Aggregation_Level]
      ( 
       @Account_Id INT
      ,@CU_Aggregation_Level VARCHAR(25)
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            bm.Commodity_Id
           ,bcr.Category_Bucket_Master_Id
           ,bm.Bucket_Name AS Category_Bucket_Name
           ,BktTyp.Code_Value AS Bucket_Type
           ,bm.Default_Uom_Type_Id
           ,bcr.Child_Bucket_Master_Id
           ,ChldBM.Bucket_Name AS Child_Bucket_Name
           ,CASE WHEN cscbar.Category_Bucket_Master_Id IS NULL THEN AggTyp.Code_Value
                 ELSE 'CountryStateAggregationRule'
            END AS Aggregation_Type
           ,bcr.Is_Aggregate_Category_Bucket
           ,bcr.Priority_Order
           ,MAX(ISNULL(cscbar.Priority_Order, bcr.Priority_Order)) OVER ( PARTITION BY ISNULL(cscbar.Category_Bucket_Master_Id, bcr.Category_Bucket_Master_Id), ISNULL(cscbar.CU_Aggregation_Level_Cd, bcr.CU_Aggregation_Level_Cd) ) Max_Priority
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Bucket_Category_Rule bcr
                  ON bcr.Category_Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master ChldBM
                  ON ChldBM.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code AggLvl
                  ON Agglvl.Code_Id = bcr.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Code AggTyp
                  ON aggtyp.Code_Id = bcr.Aggregation_Type_CD
            INNER JOIN dbo.Code BktTyp
                  ON BktTyp.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN ( dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
                              INNER JOIN code csalvcd
                                    ON csalvcd.Code_Id = cscbar.CU_Aggregation_Level_Cd )
                              ON bcr.Category_Bucket_Master_Id = cscbar.Category_Bucket_Master_Id
                                 AND ( @Country_Id IS NULL
                                       OR cscbar.Country_Id = @Country_Id )
                                 AND ( @State_Id IS NULL
                                       OR cscbar.State_Id = @Country_Id )
                                 AND ( ( csalvcd.Code_Value = 'Site'
                                         AND bm.Is_Shown_on_Site = 1 )
                                       OR ( csalvcd.Code_Value = 'Account'
                                            AND bm.Is_Shown_On_Account = 1 )
                                       OR ( csalvcd.Code_Value NOT IN ( 'Site', 'Account' )
                                            AND bm.Is_Shown_On_Account = 0
                                            AND bm.Is_Shown_on_Site = 0 ) )
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        core.Client_Hier_Account cha
                     WHERE
                        cha.Account_Id = @Account_Id
                        AND cha.Commodity_Id = bm.Commodity_Id ) -- Used Exists Clause to filter the commodity as One account can have more than one meter associated with the same Commodity    
            AND AggLvl.Code_Value = @CU_Aggregation_Level
            AND bm.Is_Active = 1
            AND ( ( AggLvl.Code_Value = 'Site'
                    AND bm.Is_Shown_on_Site = 1 )
                  OR ( AggLvl.Code_Value = 'Account'
                       AND bm.Is_Shown_On_Account = 1 )
                  OR AggLvl.Code_Value NOT IN ( 'Site', 'Account' ) )
      ORDER BY
            bm.Sort_Order    
  
END;

;
;
GO


GRANT EXECUTE ON  [dbo].[Bucket_Category_Rule_Sel_By_Account_Aggregation_Level] TO [CBMSApplication]
GO
