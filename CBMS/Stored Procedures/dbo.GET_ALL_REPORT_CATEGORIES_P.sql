SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_ALL_REPORT_CATEGORIES_P
AS
BEGIN

	SET NOCOUNT ON

	SELECT report_category_id,
		report_category_name,
		report_category_description
	FROM dbo.report_category
	ORDER BY report_category_name

END
GO
GRANT EXECUTE ON  [dbo].[GET_ALL_REPORT_CATEGORIES_P] TO [CBMSApplication]
GO
