SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Batch_Dtls_sel_Invoice_Collection_Batch_Id         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
 @Invoice_Collection_Batch_Id INT
 @Status_Cd_Value VARCHAR(25)                        
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  EXEC dbo.Invoice_Collection_Batch_Dtls_sel_Invoice_Collection_Batch_Id
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2017-02-03  Created For Invoice_Collection.           
               
******/   
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtls_Sel_Invoice_Collection_Batch_Id]
      ( 
       @Invoice_Collection_Batch_Id INT
      ,@Status_Cd_Value VARCHAR(25) )
AS 
BEGIN  
      SET NOCOUNT ON;  
        
     
      SELECT
            Invoice_Collection_Batch_Dtl_Id
           ,Invoice_Collection_Account_Config_Id
           ,Invoice_Collection_Batch_Id
           ,Collection_Start_Dt
           ,Collection_End_Dt
           ,sc.Code_Value Record_Status
           ,Error_Dsc
      FROM
            dbo.Invoice_Collection_Batch_Dtl icbd
            INNER JOIN dbo.Code sc
                  ON sc.Code_Id = icbd.Status_Cd
      WHERE
            icbd.Invoice_Collection_Batch_Id = @Invoice_Collection_Batch_Id
            AND sc.Code_Value = @Status_Cd_Value
            
            
  
     

END  
  




;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtls_Sel_Invoice_Collection_Batch_Id] TO [CBMSApplication]
GO
