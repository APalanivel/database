SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost     
       
DESCRIPTION:  This will fetch account information for the cu_invoice_id  
      
INPUT PARAMETERS:          
 Name              DataType          Default					Description          
----------------------------------------------------------------------------------------------          
 @Account_Id		INT				 NULL
 @Cu_Invoice_Id		INT
 @Commodity_Id		INT				 NULL
 @Client_Hier_Id	INT				 NULL
 @Client_Id			INT				 NULL
 @Total_Cost		DECIMAL(28, 6)  NULL       
          
OUTPUT PARAMETERS:          
 Name              DataType          Default					Description          
----------------------------------------------------------------------------------------------          
USAGE EXAMPLES: 

	EXEC Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost 1394663,69668881,290,1,2,NULL
	EXEC Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost 1394663,69668881,290,1,2,'851.56'
	EXEC Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost 1394663,69668881,290,1,2,'851.58'
	EXEC Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost 1394663,69668881,290,1,2,'8515.73'

	
	
EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 1247036       -- int
    , @Cu_Invoice_Id = 55972925 -- int
    , @Commodity_Id = 290       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '50'        -- decimal(28, 6) 


	EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 1247036       -- int
    , @Cu_Invoice_Id = 55972925 -- int
    , @Commodity_Id = 290       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '1000'        -- decimal(28, 6) 


	EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 1247036       -- int
    , @Cu_Invoice_Id = 55972925 -- int
    , @Commodity_Id = 290       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '10000'        -- decimal(28, 6) 


	EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 1247036       -- int
    , @Cu_Invoice_Id = 55972925 -- int
    , @Commodity_Id = 290       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '10000000'        -- decimal(28, 6) 



	EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 488157        -- int
    , @Cu_Invoice_Id = 74882425 -- int
    , @Commodity_Id = 291       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '846.53'     -- decimal(28, 6) 

EXEC dbo.Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost
    @Account_Id = 488157        -- int
    , @Cu_Invoice_Id = 74882425 -- int
    , @Commodity_Id = 291       -- int
    , @Client_Hier_Id = NUll    -- int
    , @Client_Id = null         -- int
    , @Total_Cost = '62'      -- decimal(28, 6) 


AUTHOR INITIALS:        
Initials	Name        
----------------------------------------------------------------------------------------------          
NR			Narayana Reddy
      
Initials	Date		Modification        
----------------------------------------------------------------------------------------------          
NR			2018-13-11	Created Data2.0.    
******/
CREATE PROC [dbo].[Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost]
    (
        @Account_Id INT = NULL
        , @Cu_Invoice_Id INT
        , @Commodity_Id INT = NULL
        , @Client_Hier_Id INT = NULL
        , @Client_Id INT = NULL
        , @Total_Cost DECIMAL(28, 6) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Currency_Unit_Id INT
            , @Client_Currency_Group_Id INT
            , @Service_Month DATE
            , @Absolute_Code_Id INT;




        SELECT
            @Client_Currency_Group_Id = MAX(ch.Client_Currency_Group_Id)
            , @Service_Month = MAX(cism.SERVICE_MONTH)
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id;




        SELECT
            @Currency_Unit_Id = ci.CURRENCY_UNIT_ID
        FROM
            dbo.CU_INVOICE ci
        WHERE
            ci.CU_INVOICE_ID = @Cu_Invoice_Id;

        SELECT
            @Absolute_Code_Id = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Recalc Toleran Value Type'
            AND c.Code_Value = 'Absolute';




        CREATE TABLE #Recalc_Tolerance_Value
             (
                 Total_Cost_Lower_Range DECIMAL(28, 2)
                 , Total_Cost_Upper_Range DECIMAL(28, 2)
                 , Tolerance_Value DECIMAL(28, 2)
                 , Value_Type_Cd INT
                 , Base_Currency_Id INT
                 , Comparison_Operation VARCHAR(255)
                 , Converted_Currency_Name VARCHAR(100)
             );

        --------------------------To fetch the Account Specfic configuration  Tolerance values---------------------------

        INSERT INTO #Recalc_Tolerance_Value
             (
                 Total_Cost_Lower_Range
                 , Total_Cost_Upper_Range
                 , Tolerance_Value
                 , Value_Type_Cd
                 , Base_Currency_Id
                 , Comparison_Operation
                 , Converted_Currency_Name
             )
        SELECT
            CAST(rtvac.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Lower_Range
            , CAST(rtvac.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Upper_Range
            , CASE WHEN rtvac.Value_Type_Cd = @Absolute_Code_Id THEN
                       CAST(rtvac.Tolerance_Value * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2))
                  ELSE rtvac.Tolerance_Value
              END AS Tolerance_Value
            , rtvac.Value_Type_Cd
            , rtvac.Base_Currency_Id
            , rtvac.Comparison_Operation
            , cue.CURRENCY_UNIT_NAME
        FROM
            dbo.Recalc_Tolerance_Value_Account_Config rtvac
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = rtvac.Account_Id
                   AND  cha.Commodity_Id = rtvac.Commodity_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = rtvac.Account_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cuc.BASE_UNIT_ID = rtvac.Base_Currency_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvac.Base_Currency_Id)
                   AND  cuc.CONVERSION_DATE = @Service_Month
            LEFT JOIN dbo.CURRENCY_UNIT cue
                ON cue.CURRENCY_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvac.Base_Currency_Id)
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
            AND (   @Account_Id IS NULL
                    OR  rtvac.Account_Id = @Account_Id)
            AND (   @Commodity_Id IS NULL
                    OR  rtvac.Commodity_Id = @Commodity_Id)
            AND (   @Client_Hier_Id IS NULL
                    OR  ch.Client_Hier_Id = @Client_Hier_Id)
            AND (   @Client_Id IS NULL
                    OR  ch.Client_Id = @Client_Id)
            AND (   @Total_Cost IS NULL
                    OR  (   (@Total_Cost >= CASE WHEN rtvac.Total_Cost_Upper_Range IS NULL THEN
                                                     ROUND(rtvac.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                            END)
                            OR  (@Total_Cost BETWEEN ROUND(rtvac.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                             AND     ROUND(rtvac.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR, 2, 1))))
        GROUP BY
            rtvac.Total_Cost_Lower_Range
            , rtvac.Total_Cost_Upper_Range
            , rtvac.Tolerance_Value
            , rtvac.Value_Type_Cd
            , rtvac.Base_Currency_Id
            , rtvac.Comparison_Operation
            , rtvac.Base_Currency_Id
            , cue.CURRENCY_UNIT_NAME
            , cuc.CONVERSION_FACTOR;

        --------------------------If not exist the Account specific values then go to the client specfic values from Default config table ---------------------------


        INSERT INTO #Recalc_Tolerance_Value
             (
                 Total_Cost_Lower_Range
                 , Total_Cost_Upper_Range
                 , Tolerance_Value
                 , Value_Type_Cd
                 , Base_Currency_Id
                 , Comparison_Operation
                 , Converted_Currency_Name
             )
        SELECT
            CAST(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Lower_Range
            , CAST(rtvdc.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Upper_Range
            , CASE WHEN rtvdc.Value_Type_Cd = @Absolute_Code_Id THEN
                       CAST(rtvdc.Tolerance_Value * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2))
                  ELSE rtvdc.Tolerance_Value
              END AS Tolerance_Value
            , rtvdc.Value_Type_Cd
            , rtvdc.Base_Currency_Id
            , rtvdc.Comparison_Operation
            , cue.CURRENCY_UNIT_NAME
        FROM
            dbo.Recalc_Tolerance_Value_Default_Config rtvdc
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cuc.BASE_UNIT_ID = rtvdc.Base_Currency_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvdc.Base_Currency_Id)
                   AND  cuc.CONVERSION_DATE = @Service_Month
            LEFT JOIN dbo.CURRENCY_UNIT cue
                ON cue.CURRENCY_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvdc.Base_Currency_Id)
        WHERE
            (   @Client_Id IS NOT NULL
                AND rtvdc.Client_Id = @Client_Id)
            AND (   @Total_Cost IS NULL
                    OR  (   (@Total_Cost >= CASE WHEN rtvdc.Total_Cost_Upper_Range IS NULL THEN
                                                     ROUND(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                            END)
                            OR  (@Total_Cost BETWEEN ROUND(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                             AND     ROUND(rtvdc.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR, 2, 1))))
            AND NOT EXISTS (SELECT  1 FROM  #Recalc_Tolerance_Value rtv);


        --------------------------If not exist the client specific values then go to the -1  values from Default config table ---------------------------



        INSERT INTO #Recalc_Tolerance_Value
             (
                 Total_Cost_Lower_Range
                 , Total_Cost_Upper_Range
                 , Tolerance_Value
                 , Value_Type_Cd
                 , Base_Currency_Id
                 , Comparison_Operation
                 , Converted_Currency_Name
             )
        SELECT
            CAST(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Lower_Range
            , CAST(rtvdc.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2)) AS Total_Cost_Upper_Range
            , CASE WHEN rtvdc.Value_Type_Cd = @Absolute_Code_Id THEN
                       CAST(rtvdc.Tolerance_Value * cuc.CONVERSION_FACTOR AS DECIMAL(10, 2))
                  ELSE rtvdc.Tolerance_Value
              END AS Tolerance_Value
            , rtvdc.Value_Type_Cd
            , rtvdc.Base_Currency_Id
            , rtvdc.Comparison_Operation
            , cue.CURRENCY_UNIT_NAME
        FROM
            dbo.Recalc_Tolerance_Value_Default_Config rtvdc
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cuc.BASE_UNIT_ID = rtvdc.Base_Currency_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvdc.Base_Currency_Id)
                   AND  cuc.CONVERSION_DATE = @Service_Month
            LEFT JOIN dbo.CURRENCY_UNIT cue
                ON cue.CURRENCY_UNIT_ID = ISNULL(@Currency_Unit_Id, rtvdc.Base_Currency_Id)
        WHERE
            rtvdc.Client_Id = -1
            AND (   @Total_Cost IS NULL
                    OR  (   (@Total_Cost >= CASE WHEN rtvdc.Total_Cost_Upper_Range IS NULL THEN
                                                     ROUND(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                            END)
                            OR  (@Total_Cost BETWEEN ROUND(rtvdc.Total_Cost_Lower_Range * cuc.CONVERSION_FACTOR, 2, 1)
                                             AND     ROUND(rtvdc.Total_Cost_Upper_Range * cuc.CONVERSION_FACTOR, 2, 1))))
            AND NOT EXISTS (SELECT  1 FROM  #Recalc_Tolerance_Value rtv);





        SELECT
            rtv.Total_Cost_Lower_Range
            , rtv.Total_Cost_Upper_Range
            , rtv.Tolerance_Value
            , CASE WHEN c.Code_Value = 'Absolute' THEN rtv.Converted_Currency_Name
                  ELSE c.Code_Value
              END AS Value_Type
            , rtv.Base_Currency_Id
            , cu.CURRENCY_UNIT_NAME
            , rtv.Converted_Currency_Name
            , rtv.Comparison_Operation
            , CASE WHEN c.Code_Value = '%' THEN 'Percentage'
                  ELSE c.Code_Value
              END AS Value_Differance
            , DENSE_RANK() OVER (ORDER BY
                                     rtv.Total_Cost_Lower_Range
                                     , rtv.Total_Cost_Upper_Range) AS Cnt_For_Logical_Operator
        FROM
            #Recalc_Tolerance_Value rtv
            INNER JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = rtv.Base_Currency_Id
            LEFT JOIN dbo.Code c
                ON c.Code_Id = rtv.Value_Type_Cd;



    END;

GO
GRANT EXECUTE ON  [dbo].[Recalc_Tolerance_Value_Sel_By_Account_Id_Service_Month_Invoice_Total_Cost] TO [CBMSApplication]
GO
