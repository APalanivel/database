
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Site_Commodity_Image_Del]  

DESCRIPTION: It Deletes Site Commodity Image for Given Site Id,Commodity Id,Service Month.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
    @Client_Hier_Id		INT
    @Commodity_Id		INT
    @Service_Month		DATE
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
SELECT TOP 100 * FROM  dbo.Site_Commodity_Image WHERE Site_Id = 281 AND Commodity_Id = 65 AND Service_Month = '2008-01-01'

  BEGIN TRAN
	   EXEC dbo.Site_Commodity_Image_Del 5351,65,'2008-01-01'
  ROLLBACK TRAN

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR		PANDARINATH
	AP		Athmaram Pabbathi

MODIFICATIONS:
	INITIALS	DATE		   MODIFICATION
------------------------------------------------------------
	PNR		28-JUN-10	   CREATED
	AP		2012-03-19   Replaced @Site_Id parameter with @Client_Hier_Id (Addl Data Changes)
*/

CREATE PROCEDURE dbo.Site_Commodity_Image_Del
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Service_Month DATE )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Site_Id INT
      
      SELECT
            @Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      DELETE FROM
            dbo.Site_Commodity_Image
      WHERE
            Site_Id = @Site_Id
            AND Commodity_Id = @Commodity_Id
            AND Service_Month = @Service_Month

END
;
GO

GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Del] TO [CBMSApplication]
GO
