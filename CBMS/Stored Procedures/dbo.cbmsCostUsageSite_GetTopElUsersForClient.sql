
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsCostUsageSite_GetTopElUsersForClient

DESCRIPTION:
		
INPUT PARAMETERS:
	Name			    DataType	    Default	Description
------------------------------------------------------------
      @Client_ID	    INT
      @Division_ID	    INT		    NULL
      @Begin_Month	    DATETIME
      @End_Month	    DATETIME 
      @Top_Count	    INT			5

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXECUTE dbo.cbmsCostUsageSite_GetTopElUsersForClient 10069, NULL, '01/01/2011', '12/01/2011'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
     AP		Athmaram Pabbathi
    
MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	AP		08/11/2011   Removed unused parameter @MyAccountId and Removed Cost_Usage_Site, Invoice_Participation_Site, vsSiteName tables and 
					   used Cost_Usage_Account_Dtl, Bucket_Master, Account, Invoice_Participation, commodity, Address, State tables 
     AP		08/12/2011   Removed Cost_Usage_Account_Dtl table and used Cost_Usage_Site_Dtl
     AP		08/22/2011   Added qualifiers to all objects with the owner name.
     AP		08/24/2011   Removed base tables from CTE_Client_Dtl and used Client_Hier table and removed NOLOCK statements
     AP		03/29/2012   Modified code to use Client_Hier_Id instead Site_Id for join on CUSD
						  Removed CTE's and used temp tables
						  Added @Top_Count parameter to get top list
******/

CREATE       PROCEDURE [dbo].[cbmsCostUsageSite_GetTopElUsersForClient]
      ( 
       @Client_ID INT
      ,@Division_ID INT = NULL
      ,@Begin_Month DATETIME
      ,@End_Month DATETIME
      ,@Top_Count INT = 5 )
AS 
BEGIN
      SET NOCOUNT ON ;
    
      DECLARE
            @EL_Commodity_ID INT
           ,@UOM_Type_ID INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      CREATE TABLE #Client_Dtl
            ( 
             Client_Hier_Id INT PRIMARY KEY CLUSTERED
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Currency_Group_Id INT )

      CREATE TABLE #Cost_Usage_Site_Dtl
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,EL_Usage DECIMAL(28, 10) )

      CREATE INDEX IDX_Cost_Usage_Site_Dtl ON #Cost_Usage_Site_Dtl(Client_Hier_Id, Service_Month)

      CREATE TABLE #Invoice_Participation_Site
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Is_Published SMALLINT )

      CREATE INDEX IDX_Invoice_Participation_Site ON #Invoice_Participation_Site(Client_Hier_Id, Service_Month)
      
      SELECT
            @UOM_Type_ID = uom.Entity_Id
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'kWh'
            AND uom.Entity_Description = 'Unit for electricity'


      SELECT
            @EL_Commodity_ID = com.Commodity_ID
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power' 
            
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_ID 

      INSERT      INTO #Client_Dtl
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Site_Name
                  ,Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' Site_Name
                       ,CH.Client_Currency_Group_ID AS Currency_Group_ID
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        ch.Client_Id = @Client_ID
                        AND ( @Division_ID IS NULL
                              OR ch.Sitegroup_Id = @Division_ID )
                        AND CH.Site_Closed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_ID > 0
                  GROUP BY
                        ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')'
                       ,CH.Client_Currency_Group_ID ;
                                                   
      INSERT      INTO #Cost_Usage_Site_Dtl
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,EL_Usage )
                  SELECT
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month
                       ,isnull(sum(CUSD.Bucket_Value * uc.Conversion_Factor), 0)
                  FROM
                        dbo.Cost_Usage_Site_Dtl CUSD
                        INNER JOIN #Client_Dtl CD
                              ON CD.Client_Hier_Id = CUSD.Client_Hier_Id
                        INNER JOIN @Cost_Usage_Bucket_Id CUB
                              ON CUB.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        INNER JOIN dbo.Consumption_Unit_Conversion UC
                              ON UC.Base_Unit_ID = CUSD.UOM_Type_Id
                  WHERE
                        CUB.Bucket_Type = 'Determinant'
                        AND UC.Converted_Unit_ID = @UOM_Type_ID
                        AND CUSD.Service_Month BETWEEN @Begin_Month
                                               AND     @End_Month
                  GROUP BY
                        CUSD.Client_Hier_Id
                       ,CUSD.Service_Month
                       

      INSERT      INTO #Invoice_Participation_Site
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Is_Published )
                  SELECT
                        cd.Client_Hier_Id
                       ,IP.Service_Month
                       ,( case WHEN sum(cast(IP.Is_Received AS INT)) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                  FROM
                        dbo.Invoice_Participation IP
                        INNER JOIN #Client_Dtl CD
                              ON CD.Site_ID = IP.Site_ID
                        INNER JOIN ( SELECT
                                          cha.Account_Id
                                         ,cha.Commodity_Id
                                     FROM
                                          Core.Client_Hier_Account cha
                                     GROUP BY
                                          cha.Account_Id
                                         ,cha.Commodity_Id ) CHA
                              ON CHA.Account_ID = IP.Account_ID
                  WHERE
                        IP.Service_Month BETWEEN @Begin_Month
                                         AND     @End_Month
                        AND CHA.Commodity_Id = @EL_Commodity_ID
                  GROUP BY
                        cd.Client_Hier_Id
                       ,IP.Service_Month


      SELECT TOP ( @Top_Count )
            CD.Site_ID
           ,CD.Site_Name
           ,sum(CUSD.EL_Usage) AS EL_Usage
      FROM
            #Client_Dtl CD
            LEFT OUTER JOIN #Cost_Usage_Site_Dtl CUSD
                  ON CUSD.Client_Hier_Id = CD.Client_Hier_Id
            LEFT OUTER JOIN #Invoice_Participation_Site IPS
                  ON IPS.Client_Hier_Id = CD.Client_Hier_Id
                     AND IPS.Service_Month = CUSD.Service_Month
      WHERE
            ( IPS.Is_Published = 1
              OR IPS.Is_Published IS NULL )
      GROUP BY
            CD.Site_ID
           ,CD.Site_Name
      ORDER BY
            EL_Usage DESC
                  
END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetTopElUsersForClient] TO [CBMSApplication]
GO
