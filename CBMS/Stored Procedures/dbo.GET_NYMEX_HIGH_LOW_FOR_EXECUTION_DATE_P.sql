SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_NYMEX_HIGH_LOW_FOR_EXECUTION_DATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@lastTradeDate 		varchar(12)	          	
	@symbol        		varchar(200)	          	
	@isIndividualMonth	bit       	          	
	@batchExecutionDate	varchar(12)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_NYMEX_HIGH_LOW_FOR_EXECUTION_DATE_P '2004-08-10','NGF8',0,'2004-09-12'
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	        	
******/

CREATE PROCEDURE dbo.GET_NYMEX_HIGH_LOW_FOR_EXECUTION_DATE_P
@lastTradeDate		VARCHAR(12),
@symbol				VARCHAR(200), 
@isIndividualMonth	BIT,
@batchExecutionDate VARCHAR(12)
AS
BEGIN

	SET NOCOUNT ON ;
	
	--fetch the NYMEX High and Low only for a particular month
	IF @isIndividualMonth > 0
		BEGIN
			SELECT ISNULL(HIGH_PRICE,CLOSE_PRICE) AS NYMEX_HIGH, 
			       ISNULL(LOW_PRICE,CLOSE_PRICE) AS NYMEX_LOW
			FROM 	dbo.RM_NYMEX_DATA 
			WHERE 	LAST_TRADE_DATE = CONVERT(VARCHAR(12),@lastTradeDate, 101) AND 
				SYMBOL = @symbol AND
				BATCH_EXECUTION_DATE = CONVERT(VARCHAR(12),@batchExecutionDate, 101)
		END
	--fetch the average of NYMEX High and Low for all months in DT
	ELSE 
		BEGIN
			DECLARE @SQLStatement VARCHAR(1000)
			DECLARE @whereClause VARCHAR(1000)
			DECLARE @baseQuery VARCHAR(1000)

			SELECT @baseQuery = 	' SELECT AVG(isnull(HIGH_PRICE,CLOSE_PRICE)) AS NYMEX_HIGH, ' + 
						' AVG(isnull(LOW_PRICE,CLOSE_PRICE)) AS NYMEX_LOW ' + 
						' FROM 	RM_NYMEX_DATA WHERE '
			
			SELECT @whereClause = 	' LAST_TRADE_DATE = ' +''''+ CONVERT(VARCHAR(12),@lastTradeDate, 101)  +''''+' AND ' + 
						' SYMBOL IN (' +''''+ @symbol +''''+ ')' + ' AND ' +
						' BATCH_EXECUTION_DATE = ' +''''+ CONVERT(VARCHAR(12),@batchExecutionDate, 101) +''''

			SELECT @SQLStatement = @baseQuery + @whereClause
			EXEC (@SQLStatement)
		END
END
GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_HIGH_LOW_FOR_EXECUTION_DATE_P] TO [CBMSApplication]
GO
