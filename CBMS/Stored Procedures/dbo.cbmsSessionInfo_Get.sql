SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSessionInfo_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	app_server_id			INT
    session_info_id			INT
    ip_address				VARCHAR(50)
    page_url    			INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
RKV				Ravi Kumar vegesna
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
RKV	        	2018-12-13	Added comments,Removed ip_address filter when selecting the SESSION_INFO_ID
 


******/


CREATE PROCEDURE [dbo].[cbmsSessionInfo_Get]
    (
        @app_server_id INT
        , @session_info_id INT
        , @ip_address VARCHAR(50)
        , @page_url VARCHAR(4000)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @this_id INT
            , @now DATETIME;

        SET @now = GETDATE();

        SELECT
            @this_id = SESSION_INFO_ID
        FROM
            SESSION_INFO
        WHERE
            SESSION_INFO_ID = @session_info_id
            AND LAST_UPDATED IS NOT NULL
            AND DATEDIFF("mi", LAST_UPDATED, @now) < 321;


        IF @this_id IS NULL
            BEGIN

                INSERT INTO SESSION_INFO
                     (
                         APP_SERVER_ID
                         , IP_ADDRESS
                         , LAST_UPDATED
                     )
                VALUES
                    (@app_server_id
                     , @ip_address
                     , @now);

                SET @this_id = @@IDENTITY;

            END;
        ELSE
            BEGIN

                UPDATE
                    SESSION_INFO
                SET
                    LAST_UPDATED = GETDATE()
                WHERE
                    SESSION_INFO_ID = @this_id;
            END;

        EXEC cbmsSessionActivity_Save @this_id, @page_url;

        SELECT
            si.SESSION_INFO_ID
            , si.USER_INFO_ID
            , NULL queue_id
            , si.SESSION_XML
        FROM
            SESSION_INFO si
        WHERE
            si.SESSION_INFO_ID = @this_id;


    END;

GO
GRANT EXECUTE ON  [dbo].[cbmsSessionInfo_Get] TO [CBMSApplication]
GO
