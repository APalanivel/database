SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






































/******                              
 NAME: dbo.[[Get_Variance_Process_Engine_By_Category]]                  
                              
 DESCRIPTION:    
          
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
 
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	 [dbo].[Get_Batch_Rerun_Variance_Records] 

 
GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					April 29,2020	Procedure is created
******/
CREATE PROCEDURE [dbo].[Get_Batch_Rerun_Variance_Records]
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE @start_date_performance DATETIME2 = getdate();
            DECLARE
                  @Pending_Status_Cd INT
                , @In_Process_Cd  INT;

             SELECT @Pending_Status_Cd = c.Code_Id FROM COde C
				  JOIN dbo.Codeset cs
				  ON cs.Codeset_Id = C.Codeset_Id 
				  WHERE cs.Codeset_Name ='Data Upload Status' 
				  AND c.Code_Dsc='Pending'

 SELECT @In_Process_Cd = c.Code_Id FROM COde C
				  JOIN dbo.Codeset cs
				  ON cs.Codeset_Id = C.Codeset_Id 
				  WHERE cs.Codeset_Name ='Data Upload Status' 
				  AND c.Code_Dsc='InProcess'

            DECLARE @temp TABLE
                  ( Variance_Batch_Rerun_Log_id INT );

            --SELECT vb.Account_Id, vb.Commodity_Id, vb.Service_Month

            UPDATE
                  VB
            SET
                  VB.Status_Cd = @In_Process_Cd
                , VB.Changes_Ts = getdate()
            OUTPUT
                  deleted.Variance_Batch_Rerun_Log_id
            INTO @temp
            FROM  dbo.Variance_Batch_Rerun_Logs VB
            WHERE VB.Status_Cd = @Pending_Status_Cd;


            SELECT
                  vb.*,
				  ch.Client_id
            FROM  dbo.Variance_Batch_Rerun_Logs vb
			JOIN core.Client_Hier ch
			ON ch.Client_Hier_Id = vb.Client_Hier_Id
                  JOIN
                  @temp t
                        ON vb.Variance_Batch_Rerun_Log_id = t.Variance_Batch_Rerun_Log_id;

      END;
GO
GRANT EXECUTE ON  [dbo].[Get_Batch_Rerun_Variance_Records] TO [CBMSApplication]
GO
