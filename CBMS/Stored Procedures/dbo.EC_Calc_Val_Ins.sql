SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
                
Name:       
 dbo.EC_Calc_Val_Ins           
                  
Description:                  
   To insert Calc Val configuration    
                  
 Input Parameters:                  
    Name        DataType  Default  Description                    
---------------------------------------------------------------------------------    
 @State_Id       INT    
 @Commodity_Id      INT    
 @Calc_Value_Name     NVARCHAR(255)    
 @Bucket_Master_Id     INT    
 @EC_Invoice_Sub_Bucket_Master_Id INT    
 @Starting_Period_Cd     INT    
 @Starting_Period_Operator_Cd  INT    
 @Starting_Period_Operand   SMALLINT    
 @End_Period_Cd      INT    
 @End_Period_Operator_Cd    INT    
 @End_Period_Operand     SMALLINT    
 @Aggregation_Cd      INT    
 @User_Info_Id      INT    
        
Output Parameters:                        
    Name        DataType  Default  Description                    
---------------------------------------------------------------------------------    
                  
Usage Examples:                      
---------------------------------------------------------------------------------    
    
 SELECT TOP 10 a.EC_Invoice_Sub_Bucket_Master_Id,a.Bucket_Master_Id,a.State_Id,b.Commodity_Id    
 FROM dbo.EC_Invoice_Sub_Bucket_Master a JOIN dbo.Bucket_Master b ON a.Bucket_Master_Id = b.Bucket_Master_Id    
 DECLARE  @EC_Calc_Val_Id_Out INT     
 BEGIN TRANSACTION    
  SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'    
  EXEC dbo.EC_Calc_Val_Ins  73,290,'Test_Calc_Val_Ins',329,15,101031,101017,5,101030,101018,5,100957,16,@EC_Calc_Val_Id_Out OUTPUT,8    
  SELECT * FROM dbo.EC_Calc_Val WHERE Calc_Value_Name ='Test_Calc_Val_Ins'    
 ROLLBACK TRANSACTION    
       
     
Author Initials:                  
    Initials Name                  
---------------------------------------------------------------------------------    
 RR    Raghu Reddy     
 RKV   Ravi Kumar Vegesna  
 VRV   Venkata Reddy Vanga                  
     
Modifications:                  
 Initials    Date  Modification                  
---------------------------------------------------------------------------------    
    RR          2015-05-13 Created For AS400.             
    RKV         2015-10-01  Removed  EC_Invoice_Sub_Bucket_Master_Id and Bucket_Master_Id as part of AS400-II    
    RKV         2016-11-17  Added New Parameter @Ec_Account_Group_Type_Id as part of maint-4563       
    NR          2018-02-23 Recalc Data Interval - Saved the IDM_Commodity_Measurement_Group_Id.    
    VRV         2019-10-31 added new parameters for aggregation logic,Monthly settings, Type,Uom added  as part of SE2017-875     
******/     
    
CREATE PROCEDURE [dbo].[EC_Calc_Val_Ins]    
      (     
        @State_Id INT    
      , @Commodity_Id INT    
      , @Calc_Value_Name NVARCHAR(255)    
      , @EC_Invoice_Sub_Bucket_Master_Id INT = NULL    
      , @Starting_Period_Cd INT    
      , @Starting_Period_Operator_Cd INT    
      , @Starting_Period_Operand SMALLINT    
      , @End_Period_Cd INT    
      , @End_Period_Operator_Cd INT    
      , @End_Period_Operand SMALLINT    
      , @Aggregation_Cd INT    
      , @User_Info_Id INT    
      , @Ec_Account_Group_Type_Id INT    
      , @EC_Calc_Val_Id INT OUTPUT    
      , @IDM_Commodity_Measurement_Group_Id INT = NULL  
      , @Multiple_Option_Value_Selection_Cd int  
      , @Multiple_Option_No_Of_Month int  
      , @Multiple_Option_Total_Aggregation_Cd int  
      , @Monthly_settings_Cd int  
      , @Monthly_Setting_Start_Month_Num int
      , @Monthly_Setting_End_Month_Num int  
      , @Supplier_Account_Source_Type_Cd int 
      , @Calc_Val_Type_Cd int  
      , @Uom_Cd  int)  
  
         AS     
BEGIN    
      SET NOCOUNT ON     
                
      INSERT      INTO dbo.EC_Calc_Val    
                  (     
                    State_Id    
                  , Commodity_Id    
                  , Calc_Value_Name    
                  , Starting_Period_Cd    
                  , Starting_Period_Operator_Cd    
                  , Starting_Period_Operand    
                  , End_Period_Cd    
                  , End_Period_Operator_Cd    
                  , End_Period_Operand    
                  , Aggregation_Cd    
                  , Created_User_Id    
                  , Created_Ts    
                  , Updated_User_Id    
                  , Last_Change_Ts    
                  , Ec_Account_Group_Type_Id    
                  , IDM_Commodity_Measurement_Group_Id  
                  , Multiple_Option_Value_Selection_Cd   
                  , Multiple_Option_No_Of_Month   
                  , Multiple_Option_Total_Aggregation_Cd   
                  , Monthly_settings_Cd   
                  , Monthly_Setting_Start_Month_Num   
                  , Monthly_Setting_End_Month_Num   
                  , Supplier_Account_Source_Type_Cd   
                  , Calc_Val_Type_Cd  
                  , Uom_Cd    )    
      VALUES    
                  (     
                    @State_Id    
                  , @Commodity_Id    
                  , @Calc_Value_Name    
                  , @Starting_Period_Cd    
                  , @Starting_Period_Operator_Cd    
                  , @Starting_Period_Operand    
                  , @End_Period_Cd    
                  , @End_Period_Operator_Cd    
                  , @End_Period_Operand    
                  , @Aggregation_Cd    
                  , @User_Info_Id    
                  , GETDATE()    
                  , @User_Info_Id    
                  , GETDATE()    
                  , @Ec_Account_Group_Type_Id    
                  , @IDM_Commodity_Measurement_Group_Id  
                  , @Multiple_Option_Value_Selection_Cd   
                  , @Multiple_Option_No_Of_Month   
                  , @Multiple_Option_Total_Aggregation_Cd   
                  , @Monthly_settings_Cd   
                  , @Monthly_Setting_Start_Month_Num   
                  , @Monthly_Setting_End_Month_Num   
                  , @Supplier_Account_Source_Type_Cd  
                  , @Calc_Val_Type_Cd   
                  , @Uom_Cd )    
                                 
      SET @EC_Calc_Val_Id = SCOPE_IDENTITY()    
          
END;    
    
;    
    
;    
    
;    
    
    
  
  
  
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Ins] TO [CBMSApplication]
GO
