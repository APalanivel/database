SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Term_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name

DESCRIPTION:  

	Displays the Schedule and Schedule term details of the Vendor/Commodity/Schedule Name combination

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	  @Vendor_Id						INT
      @Commodity_Id						INT
      @Schedule_Name					NVARCHAR(200)
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.TOU_Schedule_Term_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name 530,-1,null

AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		Balaraju


	Initials Date		Modification
------------------------------------------------------------
	BCH		2012-07-10  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Term_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name
      ( 
       @Vendor_Id INT
      ,@Commodity_Id INT = -1
      ,@Schedule_Name NVARCHAR(200) = NULL )
AS 
BEGIN
    
      SET NOCOUNT ON;
      SELECT
            TOUS.Time_Of_Use_Schedule_Id
           ,TOUS.Schedule_Name
           ,COM.Commodity_Name
           ,TOUST.Time_Of_Use_Schedule_Term_Id
           ,TOUST.Start_Dt
           ,case WHEN TOUST.End_Dt = '2099-12-31' THEN NULL
                 ELSE TOUST.End_Dt
            END AS End_Dt
      FROM
            dbo.Time_Of_Use_Schedule TOUS
            JOIN dbo.Commodity COM
                  ON TOUS.Commodity_Id = COM.Commodity_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term TOUST
                  ON TOUS.Time_Of_Use_Schedule_Id = TOUST.Time_Of_Use_Schedule_Id
      WHERE
            Tous.VENDOR_ID = @Vendor_Id
            AND ( @Commodity_Id = -1
                  OR TOUS.Commodity_Id = @Commodity_Id )
            AND ( @Schedule_Name IS NULL
                  OR Tous.Schedule_Name LIKE '%' + @Schedule_Name + '%' ) 
END
;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name] TO [CBMSApplication]
GO
