SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_COMMUNICATIONS_P]
	@rfpId int,
	@cbmsImageId int,
	--@cbmsDocId varchar(200),
	--@cbmsImage image,
	@comments VARCHAR(2000)
AS
BEGIN

	DECLARE @entityId INT

	SELECT @entityId = (SELECT ENTITY_ID FROM dbo.ENTITY WHERE entity_name = 'Communications' AND entity_type = 100)

	/*UPDATE CBMS_IMAGE 

	SET 	CBMS_DOC_ID = @cbmsDocId,
		CBMS_IMAGE = @cbmsImage,
		DATE_IMAGED= getDate()

	WHERE	CBMS_IMAGE_ID = @cbmsImageId 
	*/

	--added by Jaya for updating entityid    
	     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId

	UPDATE dbo.SR_RFP_COMMUNICATIONS
	SET COMMENTS = @comments
		, UPLOADED_DATE = GETDATE()
	WHERE SR_RFP_ID = @rfpId 
		AND CBMS_IMAGE_ID = @cbmsImageId

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_COMMUNICATIONS_P] TO [CBMSApplication]
GO
