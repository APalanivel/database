SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UtilityAccounts_utility_list
	
DESCRIPTION:
	This Procedure is to get the Utility List 
	
INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UtilityAccounts_utility_list  
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   09/05/2010	Created
*/
CREATE PROC [dbo].[Report_UtilityAccounts_utility_list]
AS 
    BEGIN 
        SET NOCOUNT ON

        DECLARE @Vendor_type_ID INT 

        SELECT
            @Vendor_type_ID = a.ENTITY_ID
        FROM
            ENTITY a
        WHERE
            a.ENTITY_DESCRIPTION = 'Vendor'
            AND a.ENTITY_NAME = 'Utility' ;

        WITH    CTE_VendorList
                  AS ( SELECT
                        0 AS VENDOR_ID
                      , ' None' VENDOR_NAME
                       UNION
                       SELECT
                        v.VENDOR_ID
                      , CONVERT(VARCHAR(100), v.VENDOR_NAME) VENDOR_NAME
                       FROM
                        VENDOR v
                       WHERE
                        v.VENDOR_TYPE_ID = @Vendor_type_ID
                        AND v.IS_HISTORY = 0
                       GROUP BY
                        v.VENDOR_ID
                      , v.VENDOR_NAME
                     )
            SELECT
                cte_v.VENDOR_ID
              , cte_v.VENDOR_NAME
            FROM
                CTE_VendorList cte_v
            ORDER BY
                cte_v.VENDOR_NAME

    END


GO
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_utility_list] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UtilityAccounts_utility_list] TO [CBMSApplication]
GO
