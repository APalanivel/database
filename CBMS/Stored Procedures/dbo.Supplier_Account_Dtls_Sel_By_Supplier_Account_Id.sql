SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Supplier_Account_Dtls_Sel_By_Supplier_Account_Id                       
                            
 DESCRIPTION:        
  Get the Suppler account associated invoices count based on Supplier Account.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Sup_Account_Id   INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  
 EXEC dbo.Supplier_Account_Dtls_Sel_By_Supplier_Account_Id  1740998    
  
 EXEC Supplier_Account_Dtls_Sel_By_Supplier_Account_Id 1728901    
 EXEC Supplier_Account_Dtls_Sel_By_Supplier_Account_Id 1741006                    
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2019-06-13      Created for ADD Contract.                          
                           
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtls_Sel_By_Supplier_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id AS Utility_Account_Id
            , Uti_cha.Account_Number AS Utility_Account_Number
            , Sup_Cha.Account_Id AS Supplier_Account_Id
            , Sup_Cha.Account_Number AS Supplier_Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31') AS Supplier_Account_End_Dt
            , Sup_Cha.Meter_Number
            , Sup_Cha.Supplier_Contract_ID
            , CASE WHEN Sup_Cha.Supplier_Contract_ID IS NULL
                        OR  Sup_Cha.Supplier_Contract_ID = -1 THEN 'Missing Contract'
                  ELSE c.ED_CONTRACT_NUMBER
              END AS Contract_Label
            , Sup_Recalc.Code_Value AS Recalc_Type
            , COUNT(DISTINCT cism.CU_INVOICE_ID) AS Invoices
            , Sup_Cha.Supplier_Account_Config_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account Uti_cha
                ON Uti_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Sup_Cha
                ON Uti_cha.Meter_Id = Sup_Cha.Meter_Id
            LEFT JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = Sup_Cha.Supplier_Contract_ID
            LEFT JOIN dbo.Account_Commodity_Invoice_Recalc_Type clsrt
                ON clsrt.Account_Id = Sup_Cha.Account_Id
                   AND  (   Sup_Cha.Supplier_Account_begin_Dt BETWEEN clsrt.Start_Dt
                                                              AND     ISNULL(clsrt.End_Dt, '2099-12-31')
                            OR  Sup_Cha.Supplier_Account_End_Dt BETWEEN clsrt.Start_Dt
                                                                AND     ISNULL(clsrt.End_Dt, '2099-12-31')
                            OR  clsrt.Start_Dt BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                               AND     Sup_Cha.Supplier_Account_End_Dt
                            OR  ISNULL(clsrt.End_Dt, '2099-12-31') BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                                                   AND     Sup_Cha.Supplier_Account_End_Dt)
            LEFT JOIN dbo.Code Sup_Recalc
                ON Sup_Recalc.Code_Id = clsrt.Invoice_Recalc_Type_Cd
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Sup_Cha.Account_Id
                   AND  cism.SERVICE_MONTH BETWEEN Sup_Cha.Supplier_Account_begin_Dt
                                           AND     Sup_Cha.Supplier_Account_End_Dt
        WHERE
            Uti_cha.Account_Type = 'Utility'
            AND Sup_Cha.Account_Type = 'Supplier'
            AND Sup_Cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id
            , Uti_cha.Account_Number
            , Sup_Cha.Account_Id
            , Sup_Cha.Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31')
            , Sup_Cha.Meter_Number
            , Sup_Cha.Supplier_Contract_ID
            , CASE WHEN Sup_Cha.Supplier_Contract_ID IS NULL
                        OR  Sup_Cha.Supplier_Contract_ID = -1 THEN 'Missing Contract'
                  ELSE c.ED_CONTRACT_NUMBER
              END
            , Sup_Recalc.Code_Value
            , Sup_Cha.Supplier_Account_Config_Id
        ORDER BY
            Sup_Cha.Supplier_Account_begin_Dt DESC;

    END;



GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtls_Sel_By_Supplier_Account_Id] TO [CBMSApplication]
GO
