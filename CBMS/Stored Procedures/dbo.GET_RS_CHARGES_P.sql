SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_RS_CHARGES_P
AS
BEGIN

	SET NOCOUNT ON

	SELECT c.charge_id parent_chargeId,
		c.charge_master_id parent_masterId,
		cm.charge_display_id parent_displayId,
		cm.charge_parent_id parent_rate_id,
		c.charge_parent_id parent_rs_id
	FROM dbo.charge c INNER JOIN dbo.charge_master cm ON cm.charge_master_id = c.charge_master_id
		INNER JOIN dbo.entity ent1 ON ent1.entity_id = cm.charge_parent_type_id
		INNER JOIN dbo.entity ent2 ON ent2.entity_id = cm.charge_parent_type_id
	WHERE ent1.entity_name = 'Rate'
		AND ent1.entity_type = 120
		AND ent2.entity_name ='Rate Schedule'
		AND ent2.entity_type = 120
END
GO
GRANT EXECUTE ON  [dbo].[GET_RS_CHARGES_P] TO [CBMSApplication]
GO
