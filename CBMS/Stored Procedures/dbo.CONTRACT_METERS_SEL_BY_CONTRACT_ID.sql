SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME:        
 cbms_prod.dbo.CONTRACT_METERS_SEL_BY_CONTRACT_ID         
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
 @Client_id		INT    
 @State_Id		INT      
 @Commodity_Id  INT    
 @City			VARCHAR(200)	= NULL    
 @Account_Number VARCHAR(50)	= NULL    
 @Meter_Number  VARCHAR(50)		= NULL    
 @Contract_Id	INT    
 @utilityId		INT				= NULL      
 @divisionId	INT				= NULL      
 @siteName		VARCHAR(50)		= NULL     
 @siteId		INT				= NULL    
 @sortIndex		VARCHAR(20)		= 'Asc'   Asc/Desc    
 @sortColumn	VARCHAR(20)		= 'Site_name' All displayed columns can be used for sorting    
 @StartIndex	INT				= 1      
 @EndIndex		INT				= 2147483647     
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
     
 EXEC CONTRACT_METERS_SEL_BY_CONTRACT_ID  1005,28,291,null,null,null,20270,NULL,null,NULL,NULL,'Asc','Site_name',1,50    
   
 EXEC CONTRACT_METERS_SEL_BY_CONTRACT_ID  117,16,291,null,null,null,47347,NULL,NULL,NULL,NULL,'Asc','Site_name',1,50    
     
         
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 MGB   Bhaskaran Gopalakrishnan        
MODIFICATIONS        
        
 Initials  Date   Modification        
------------------------------------------------------------        
		10/28/2009  Created        
 SSR	04/28/2010  Added Input parameters for pagination and as per requirement,    
					Converted Static to dynamic as user has ability to choose Sorting on Columns    
					Used CTE for pagination concept, and the same we used again avoid ambiguous column error     
					Removed the reference of Client & SiteGroup, as necessary info can be fetch from Site table    
					@City,@Account_Number,@Meter_Number defined as null, as it is not mandatory    
SKA		06/04/2010	Added 4 more column in select clause					
******/        
CREATE PROCEDURE dbo.CONTRACT_METERS_SEL_BY_CONTRACT_ID
      (
       @Client_id INT
      ,@State_Id INT
      ,@Commodity_Id INT
      ,@City VARCHAR(200) = NULL
      ,@Account_Number VARCHAR(50) = NULL
      ,@Meter_Number VARCHAR(50) = NULL
      ,@Contract_Id INT
      ,@utilityId INT = NULL
      ,@divisionId INT = NULL
      ,@siteName VARCHAR(50) = NULL
      ,@siteId INT = NULL
      ,@sortIndex VARCHAR(20) = 'Asc'
      ,@sortColumn VARCHAR(20) = 'Site_name'
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN    
        
      SET NOCOUNT ON;      
      DECLARE @Sql VARCHAR(8000)    
      
	SET @Sql = ';WITH Cte_Contract_sel    
	AS    
	(    
		SELECT         
			sit.site_id    
			, sit.site_name        
			, acc.account_id        
			, acc.account_number        
			, met.meter_id        
			, met.meter_number        
			, rat.rate_id        
			, rat.rate_name        
			, ven.vendor_id        
			, ven.vendor_name        
			, addr.city        
			, sta.state_id        
			, sta.state_name        
			, Supp_Acct.ACCOUNT_NUMBER Supplier_Account_Number  
			, ct.CONTRACT_START_DATE   
			, ct.CONTRACT_END_DATE    
			, map.METER_ASSOCIATION_DATE    
			, map.METER_DISASSOCIATION_DATE  
		FROM         
			dbo.site sit    
			INNER JOIN dbo.account acc      
				ON acc.site_id = sit.site_id        
			INNER JOIN dbo.meter met      
				ON met.account_id = acc.account_id        
			INNER JOIN dbo.rate rat      
				ON  rat.rate_id = met.rate_id        
			INNER JOIN dbo.vendor ven      
				ON  ven.vendor_id = rat.vendor_id        
			INNER JOIN dbo.address addr      
				ON addr.address_id = sit.primary_address_id        
			INNER JOIN dbo.state sta      
				ON sta.state_id = addr.state_id      
			RIGHT JOIN (dbo.supplier_account_meter_map map       
							INNER JOIN dbo.ACCOUNT Supp_Acct     
								ON Supp_Acct.ACCOUNT_ID = map.ACCOUNT_ID      
						) ON map.METER_ID = met.METER_ID    
			JOIN dbo.CONTRACT ct  
				ON map.Contract_Id=ct.Contract_Id     
					AND map.Contract_Id = ' + CAST(@Contract_Id AS VARCHAR) + '     
					AND map.METER_DISASSOCIATION_DATE IS NULL      
		WHERE    
			sit.client_id = ' + CAST(@Client_id AS VARCHAR) + '    
			AND sta.state_id = ' + CAST(@State_Id AS VARCHAR) + '    
			AND rat.commodity_type_id = ' + CAST(@Commodity_Id AS VARCHAR)     
			IF @city IS NOT NULL 
				SET @Sql = @Sql + ' AND (addr.city LIKE ''%' + @city + '%'')'    
			IF @Account_Number IS NOT NULL 
				SET @Sql = @Sql + ' AND (acc.account_number LIKE ''' + @Account_Number + '%'')'    
			IF @Meter_Number IS NOT NULL 
				SET @Sql = @Sql + ' AND (met.meter_number LIKE ''' + @Meter_Number + '%'')'    
			IF @siteName IS NOT NULL 
				SET @Sql = @Sql + ' AND (sit.site_name LIKE ''%' + @siteName + '%'')'    
			IF @siteId IS NOT NULL 
				SET @Sql = @Sql + ' AND (sit.site_id = ' + CAST(@siteId AS VARCHAR) + ')'    
			IF @divisionId IS NOT NULL 
				SET @Sql = @Sql + ' AND (sit.Division_id = ' + CAST(@divisionId AS VARCHAR) + ')'    
			IF @utilityId IS NOT NULL 
				SET @Sql = @Sql + ' AND (ven.vendor_id = ' + CAST(@utilityId AS VARCHAR) + ')'    

		SET @Sql = @Sql + '    
			GROUP BY    
			sit.site_id    
			, sit.site_name        
			, acc.account_id        
			, acc.account_number        
			, met.meter_id        
			, met.meter_number        
			, rat.rate_id        
			, rat.rate_name        
			, ven.vendor_id        
			, ven.vendor_name        
			, addr.city        
			, sta.state_id        
			, sta.state_name        
			, Supp_Acct.ACCOUNT_NUMBER    
			, ct.CONTRACT_START_DATE   
			, ct.CONTRACT_END_DATE    
			, map.METER_ASSOCIATION_DATE    
			, map.METER_DISASSOCIATION_DATE  
		) ,    
	CTE_Contract_final    
		AS    
		(    
		SELECT     
			site_id    
			, site_name        
			, account_id        
			, account_number        
			, meter_id        
			, meter_number        
			, rate_id        
			, rate_name        
			, vendor_id        
			, vendor_name        
			, city        
			, state_id        
			, state_name        
			, Supplier_Account_Number  
			, CONTRACT_START_DATE   
			, CONTRACT_END_DATE    
			, METER_ASSOCIATION_DATE    
			, METER_DISASSOCIATION_DATE      
			, Row_Num = ROW_NUMBER() OVER(ORDER BY ' + @SortColumn + SPACE(1) + @Sortindex + ')      
			, Total = COUNT(1) OVER()        
		FROM    
			Cte_Contract_sel    
		)'    

	SET @Sql = @Sql + '    
		SELECT    
			site_id    
			, site_name        
			, account_id        
			, account_number        
			, meter_id        
			, meter_number        
			, rate_id        
			, rate_name        
			, vendor_id        
			, vendor_name        
			, city        
			, state_id        
			, state_name        
			, Supplier_Account_Number  
			, Contract_Start_Date   
			, Contract_End_Date    
			, Meter_Association_Date    
			, Meter_Disassociation_Date         
			, Total    
		FROM     
			CTE_Contract_final    
		WHERE     
			Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR) + ' AND ' + CAST(@EndIndex AS VARCHAR)      
	
		EXEC ( @SQL )    
END   
GO
GRANT EXECUTE ON  [dbo].[CONTRACT_METERS_SEL_BY_CONTRACT_ID] TO [CBMSApplication]
GO
