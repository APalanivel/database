
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 cbmsCuExceptionDetail_GetForException    
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @cu_invoice_id  int                       
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
    
AUTHOR INITIALS:    
 Initials  Name    
------------------------------------------------------------    
 RKV   Ravi Kumar Vegesna
 
    
MODIFICATIONS    
    
Initials Date  Modification    
------------------------------------------------------------    
RKV   2015-10-30 added two optional parameters as part of AS400-PII
******/  

CREATE    PROCEDURE [dbo].[cbmsCuExceptionDetail_GetForException]
      ( 
       @MyAccountId INT
      ,@cu_exception_id INT
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT = NULL )
AS 
BEGIN

      SELECT
            ced.cu_exception_detail_id
           ,ced.cu_exception_id
           ,ced.exception_type_id
           ,et.exception_type
           ,ced.exception_status_type_id
           ,ced.opened_date
           ,ced.is_closed
           ,ced.closed_reason_type_id
           ,ced.closed_by_id
           ,ced.closed_date
           ,ced.Account_ID
           ,ced.Commodity_Id
      FROM
            cu_exception_detail ced WITH ( NOLOCK )
            JOIN cu_exception_type et WITH ( NOLOCK )
                                           ON et.cu_exception_type_id = ced.exception_type_id
      WHERE
            ced.cu_exception_id = @cu_exception_id
            AND ( @Account_Id IS NULL
                  OR ced.Account_ID = @Account_Id )
            AND ( @Commodity_Id IS NULL
                  OR ced.Commodity_Id = @Commodity_Id )

END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_GetForException] TO [CBMSApplication]
GO
