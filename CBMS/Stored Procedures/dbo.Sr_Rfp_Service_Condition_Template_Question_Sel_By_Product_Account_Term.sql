SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term]
           
DESCRIPTION:             
			To get service condition template details 
			
INPUT PARAMETERS:            
	Name								DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Language_CD						INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211452,109701,240598,NULL
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term  1211452,109702,240598,NULL
			
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-02	Global Sourcing - Phase3 - GCS-478 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term]
      ( 
       @SR_RFP_SELECTED_PRODUCTS_ID INT
      ,@SR_RFP_ACCOUNT_TERM_ID INT
      ,@SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Bid_Id INT
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage'
      
      SELECT
            @Bid_Id = tpm.SR_RFP_BID_ID
      FROM
            dbo.SR_RFP_TERM_PRODUCT_MAP tpm
            INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                  ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
      WHERE
            tpm.SR_RFP_ACCOUNT_TERM_ID = @SR_RFP_ACCOUNT_TERM_ID
            AND tpm.SR_RFP_SELECTED_PRODUCTS_ID = @SR_RFP_SELECTED_PRODUCTS_ID
            AND cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID;
            
            
      WITH  Cte_Qtns
              AS ( SELECT
                        rfpsctq.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                       ,sscc.Sr_Service_Condition_Category_Id
                       ,isnull(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name
                       ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label
                       ,sscq.Is_Comment_Shown
                       ,isnull(sscqlv.Comment_Label_Locale_Value, sscq.Comment_Label) AS Comment_Box_Label
                       ,cd.Code_Value AS Response_Type
                       ,rfpsctq.Category_Display_Seq
                       ,rfpsctq.Question_Display_Seq
                       ,rfpsctq.Sr_Service_Condition_Question_Id
                       ,sscq.Is_Response_Required
                   FROM
                        dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map rfpsctq
                        INNER JOIN dbo.Sr_Service_Condition_Question sscq
                              ON rfpsctq.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = sscq.Response_Type_Cd
                        LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                              ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                                 AND sscqlv.Language_Cd = @Language_CD
                        INNER JOIN dbo.Sr_Service_Condition_Category sscc
                              ON rfpsctq.Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
                        LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                              ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                                 AND sscclv.Language_Cd = @Language_CD
                   WHERE
                        rfpsctq.Sr_Rfp_Bid_Id = @Bid_Id)
            SELECT
                  qtns.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                 ,qtns.Sr_Service_Condition_Category_Id
                 ,qtns.Category_Name
                 ,NULL Sr_Service_Condition_Template_Question_Map_Id
                 ,qtns.Question_Label
                 ,qtns.Is_Comment_Shown
                 ,qtns.Comment_Box_Label
                 ,qtns.Response_Type
                 ,qtns.Category_Display_Seq
                 ,qtns.Question_Display_Seq
                 ,qtns.Sr_Service_Condition_Question_Id
                 ,isnull(Response_Controls_Cnt, 1) AS Response_Controls_Cnt
                 ,left(resp.Options, len(resp.Options) - 1) AS Response_Options
                 ,'Response_Option_Id^Option_Value^Is_Comment_Required^Display_Seq' AS Response_Options_Field_Dtls
                 ,bidresponse.Response_Text_Value
                 ,bidresponse.Comment AS Comment
                 ,cast(qtns.Is_Response_Required AS INT) AS Is_Response_Required
            FROM
                  Cte_Qtns qtns
                  LEFT JOIN ( SELECT
                                    count(sscro.Sr_Service_Condition_Response_Option_Id) AS Response_Controls_Cnt
                                   ,sscro.Sr_Service_Condition_Question_Id
                              FROM
                                    dbo.Sr_Service_Condition_Response_Option sscro
                              GROUP BY
                                    sscro.Sr_Service_Condition_Question_Id ) Cnt
                        ON qtns.Sr_Service_Condition_Question_Id = Cnt.Sr_Service_Condition_Question_Id
                  CROSS APPLY ( SELECT
                                    cast(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + isnull(sscrlv.Option_Locale_Value, sscro.Option_Value) + '^' + cast(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + cast(sscro.Display_Seq AS VARCHAR(10)) + ','
                                FROM
                                    dbo.Sr_Service_Condition_Response_Option sscro
                                    LEFT JOIN dbo.Sr_Service_Condition_Response_Option_Locale_Value sscrlv
                                          ON sscro.Sr_Service_Condition_Response_Option_Id = sscrlv.Sr_Service_Condition_Response_Option_Id
                                             AND sscrlv.Language_Cd = @Language_CD
                                WHERE
                                    sscro.Sr_Service_Condition_Question_Id = qtns.Sr_Service_Condition_Question_Id
                                GROUP BY
                                    cast(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + isnull(sscrlv.Option_Locale_Value, sscro.Option_Value) + '^' + cast(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + cast(sscro.Display_Seq AS VARCHAR(10))
                                   ,sscro.Display_Seq
                                ORDER BY
                                    sscro.Display_Seq
                  FOR
                                XML PATH('') ) resp ( Options )
                  LEFT JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response bidresponse
                        ON qtns.Sr_Rfp_Service_Condition_Template_Question_Map_Id = bidresponse.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                           AND ( @Bid_Id IS NULL
                                 OR bidresponse.SR_RFP_BID_ID = @Bid_Id )
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Service_Condition_Template_Question_Sel_By_Product_Account_Term] TO [CBMSApplication]
GO
