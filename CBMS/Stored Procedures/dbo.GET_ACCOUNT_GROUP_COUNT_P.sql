SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_ACCOUNT_GROUP_COUNT_P
	@site_id int
	AS
	begin
		select count(*) from account_group 
		where account_group_id in( select account_group_id from account where site_id = @site_id)
	end


GO
GRANT EXECUTE ON  [dbo].[GET_ACCOUNT_GROUP_COUNT_P] TO [CBMSApplication]
GO
