SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[Valcon_Invoice_Recalc_Process_Batch_Ins]   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
---------------------------------------------------------------------------- 

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Pending'

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Error'


EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Completed'
 
BEGIN TRAN

SELECT  * FROM  dbo.Invoice_Recalc_Process_Batch_Dtl where   Invoice_Recalc_Process_Batch_Id = 223


EXEC dbo.Valcon_Invoice_Recalc_Process_Batch_Ins
    @Contract_Id = 110147
    , @Account_List = '806356'
    , @Is_Consolidated_Account = 0
    , @Is_Associated_Invoice = 0
    , @Is_Dis_Associated_Invoice = 1
    , @User_Info_Id = 49

SELECT  * FROM  dbo.Invoice_Recalc_Process_Batch_Dtl where   Invoice_Recalc_Process_Batch_Id = 223

ROLLBACK


AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2019-09-03		Add Contract -  Created.
NR			2020-01-29		MAINT-9782 - Added IS_PROCESSED flag in Temp table.                       

******/

CREATE PROCEDURE [dbo].[Valcon_Invoice_Recalc_Process_Batch_Ins]
    (
        @Contract_Id INT
        , @Account_List NVARCHAR(MAX)
        , @Is_Consolidated_Account BIT
        , @Is_Associated_Invoice BIT
        , @Is_Dis_Associated_Invoice BIT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE
            @Max_Account_Cnt INT
            , @Account_Counter INT = 1
            , @Account_Id INT
            , @Max_invoice_Cnt INT
            , @Counter INT = 1
            , @Invoice_Recalc_Process_Batch_Id INT
            , @Pending_Status_Cd INT
            , @Error_Status_Cd INT;
        CREATE TABLE #Account_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );
        CREATE TABLE #Invoice_List
             (
                 Id INT IDENTITY(1, 1)
                 , Cu_Invoice_Id INT
                 , Account_Id INT
                 , CBMS_IMAGE_ID INT
                 , Begin_Dt DATE
                 , End_Dt DATE
                 , IS_PROCESSED BIT
             );



        SELECT
            @Pending_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Pending' THEN c.Code_Id
                                         ELSE 0
                                     END)
            , @Error_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Error' THEN c.Code_Id
                                         ELSE 0
                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value IN ( 'Pending', 'Error' );



        INSERT INTO #Account_List
             (
                 Account_Id
             )
        SELECT
            us.Segments
        FROM
            dbo.ufn_split(@Account_List, ',') us
        GROUP BY
            us.Segments;

        SELECT  @Max_Account_Cnt = MAX(Id)FROM  #Account_List;

        WHILE (@Account_Counter <= @Max_Account_Cnt)
            BEGIN

                SELECT
                    @Account_Id = al.Account_Id
                FROM
                    #Account_List al
                WHERE
                    al.Id = @Account_Counter;

                IF (@Is_Consolidated_Account = 0)
                    BEGIN
                        INSERT INTO #Invoice_List
                             (
                                 Cu_Invoice_Id
                                 , Account_Id
                                 , CBMS_IMAGE_ID
                                 , Begin_Dt
                                 , End_Dt
                                 , IS_PROCESSED
                             )
                        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
                            @Contract_Id = @Contract_Id
                            , @Account_Id = @Account_Id
                            , @Is_Associated_Invoice = @Is_Associated_Invoice
                            , @Is_Dis_Associated_Invoice = @Is_Dis_Associated_Invoice;

                    END;


                IF (@Is_Consolidated_Account = 1)
                    BEGIN
                        INSERT INTO #Invoice_List
                             (
                                 Cu_Invoice_Id
                                 , Account_Id
                                 , CBMS_IMAGE_ID
                                 , Begin_Dt
                                 , End_Dt
                                 , IS_PROCESSED
                             )
                        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated
                            @Contract_Id = @Contract_Id
                            , @Account_Id = @Account_Id
                            , @Is_Associated_Invoice = @Is_Associated_Invoice
                            , @Is_Dis_Associated_Invoice = @Is_Dis_Associated_Invoice;

                    END;

                SET @Account_Counter = @Account_Counter + 1;

            END;


        BEGIN TRY
            BEGIN TRAN;

            -- If atleast one invoice to be moved to associated bucket then load the data in to Batch table.  
            IF EXISTS (SELECT   TOP 1   ua.Cu_Invoice_Id FROM   #Invoice_List ua)
                BEGIN

                    INSERT INTO dbo.Invoice_Recalc_Process_Batch
                         (
                             Status_Cd
                             , Created_User_Id
                             , Created_Ts
                             , Updated_User_Id
                             , Last_Change_Ts
                         )
                    VALUES
                        (@Pending_Status_Cd
                         , @User_Info_Id
                         , GETDATE()
                         , @User_Info_Id
                         , GETDATE());

                    SET @Invoice_Recalc_Process_Batch_Id = SCOPE_IDENTITY();
                END;





            SELECT  @Max_invoice_Cnt = MAX(Id)FROM  #Invoice_List;


            WHILE (@Counter <= @Max_invoice_Cnt)
                BEGIN

                    INSERT INTO dbo.Invoice_Recalc_Process_Batch_Dtl
                         (
                             Invoice_Recalc_Process_Batch_Id
                             , Cu_Invoice_Id
                             , Status_Cd
                             , Created_User_Id
                             , Created_Ts
                             , Updated_User_Id
                             , Last_Change_Ts
                         )
                    SELECT
                        @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id
                        , il.Cu_Invoice_Id
                        , @Pending_Status_Cd
                        , @User_Info_Id
                        , GETDATE()
                        , @User_Info_Id
                        , GETDATE()
                    FROM
                        #Invoice_List il
                    WHERE
                        il.Id BETWEEN @Counter
                              AND     @Counter + 499
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Recalc_Process_Batch_Dtl ir
                                           WHERE
                                                ir.Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id
                                                AND ir.Cu_Invoice_Id = il.Cu_Invoice_Id)
                        AND EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.CU_INVOICE ci
                                       WHERE
                                            ci.CU_INVOICE_ID = il.Cu_Invoice_Id
                                            AND ci.IS_REPORTED = 1
                                            AND ci.IS_PROCESSED = 1)
                    GROUP BY
                        il.Cu_Invoice_Id;



                    SET @Counter = @Counter + 500;

                END;

            COMMIT TRAN;

            SELECT  @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id;

        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;
            EXEC dbo.usp_RethrowError @CustomMessage = '';  -- varchar(2500)  

        END CATCH;
    END;





GO
GRANT EXECUTE ON  [dbo].[Valcon_Invoice_Recalc_Process_Batch_Ins] TO [CBMSApplication]
GO
