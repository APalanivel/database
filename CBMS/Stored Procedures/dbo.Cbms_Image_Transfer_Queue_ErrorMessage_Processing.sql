SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****************************************************************************************    
 Name:  dbo.Cbms_Image_Transfer_Queue_ErrorMessage_Processing
     
 Description:    
  This Procedure is used to Re process the Cbms_Image_Transfer_Queue messages .
  Note: Only DBA has to execute this

 Input Parameters:    
 Name     DataType  Default Description    
--------------------------------------------------------------------------------------    
  
 Output Parameters:    
 Name     Datatype  Default Description    
--------------------------------------------------------------------------------------   
   
 Usage Examples:  
--------------------------------------------------------------------------------------   
 EXEC dbo.Cbms_Image_Transfer_Queue_ErrorMessage_Processing
   
 Author Initials:    
 Initials		Name  
--------------------------------------------------------------------------------------  
 DSC			Kaushik
   
 Modifications :    
 Initials		Date			Modification    
--------------------------------------------------------------------------------------   
 DSC			10/10/2014		Created
***********************************************************************************************/
CREATE PROCEDURE [dbo].[Cbms_Image_Transfer_Queue_ErrorMessage_Processing]
AS
BEGIN

      SET NOCOUNT ON 

      DECLARE
            @id1 INT = 1
           ,@id2 INT = 1
           ,@Conversation_Group_Id UNIQUEIDENTIFIER
           ,@Conversation_Handle UNIQUEIDENTIFIER
           ,@Old_Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Body XML
           ,@Message_Type VARCHAR(255)
           ,@Status VARCHAR(25)
           ,@Completed_Status_Cd VARCHAR(25) = 'Completed'
           ,@Error_Message_Ts DATETIME
	 
	  
      CREATE TABLE #Cbms_Image_Queue_ErrorMessage
            (
             Message_Id INT IDENTITY(1, 1)
            ,Conversation_Group_Id UNIQUEIDENTIFIER
            ,[Conversation_Handle] UNIQUEIDENTIFIER
            ,MESSAGE_Body XML
            ,Message_Sequence_Number INT
            ,Message_Type VARCHAR(255)
            ,Message_Received_Ts DATETIME
            ,Queue_Name VARCHAR(255)
            ,CBMS_IMAGE_ID INT
            ,Op_Code CHAR(1) )
      INSERT      INTO #Cbms_Image_Queue_ErrorMessage
                  (Conversation_Group_Id
                  ,Conversation_Handle
                  ,MESSAGE_Body
                  ,Message_Sequence_Number
                  ,Message_Type
                  ,Message_Received_Ts
                  ,Queue_Name )
                  SELECT
                        [Conversation_Group_Id]
                       ,[Conversation_Handle]
                       ,convert(XML, MESSAGE_Body)
                       ,Message_Sequence_Number
                       ,Message_Type
                       ,Message_Received_Ts
                       ,Queue_Name
                  FROM
                        dbo.Service_Broker_Poison_Message
                  WHERE
                        Queue_Name = 'Cbms_Image_Transfer_Queue'
                        AND MESSAGE_Body IS NOT NULL
                  ORDER BY
                        Message_Received_Ts
                       ,Conversation_Group_Id
                       ,Conversation_Handle
                       ,Message_Sequence_Number
           
      SELECT
            @id2 = @@ROWCOUNT  


      UPDATE
            #Cbms_Image_Queue_ErrorMessage
      SET
            CBMS_IMAGE_ID = MESSAGE_Body.value('(/Cbms_Image_Changes/Cbms_Image_Change/CBMS_IMAGE_ID/node())[1]', 'INT')
           ,Op_Code = MESSAGE_Body.value('(/Cbms_Image_Changes/Cbms_Image_Change/Op_Code/node())[1]', 'varchar') 





      SELECT
            @Error_Message_Ts = min(Message_Received_Ts)
      FROM
            #Cbms_Image_Queue_ErrorMessage

      CREATE TABLE #Service_Broker_Message_Monitor
            (
             Message_Body XML
            ,Queue_name VARCHAR(255)
            ,Message_Received_Ts DATETIME
            ,Message_Status VARCHAR(10)
            ,CBMS_IMAGE_ID INT
            ,Op_Code CHAR(1) )
      INSERT      INTO #Service_Broker_Message_Monitor
                  (Message_Body
                  ,Queue_name
                  ,Message_Received_Ts
                  ,Message_Status )
                  SELECT
                        convert(XML, Message_Body) AS message_body
                       ,Queue_Name
                       ,Message_Received_Ts
                       ,Message_Status
                  FROM
                        dbo.Service_Broker_Message_Monitor
                  WHERE
                        Queue_Name = 'Cbms_Image_Transfer_Queue'
                        AND Message_Body IS NOT NULL
                        AND Message_Status = @Completed_Status_Cd
                        AND Message_Received_Ts >= @Error_Message_Ts
                   

      UPDATE
            #Service_Broker_Message_Monitor
      SET
            CBMS_IMAGE_ID = Message_Body.value('(/Cbms_Image_Changes/Cbms_Image_Change/CBMS_IMAGE_ID/node())[1]', 'INT')
           ,Op_Code = Message_Body.value('(/Cbms_Image_Changes/Cbms_Image_Change/Op_Code/node())[1]', 'varchar') 


        
      WHILE ( @id2 >= @id1 )
            BEGIN
	
                  BEGIN TRY 
			
                        BEGIN TRANSACTION
				
                        SELECT
                              @Conversation_Group_Id = Conversation_Group_Id
                             ,@Old_Conversation_Handle = Conversation_Handle
                             ,@Message_Type = Message_Type
                             ,@Message_Body = MESSAGE_Body
                        FROM
                              #Cbms_Image_Queue_ErrorMessage
                        WHERE
                              Message_Id = @id1	
                    
                        IF ( @Message_Type = '//Change_Control/Message/Cbms_Image_Transfer' )
                              BEGIN

					-- IF NO NEW MSG WITH SAME Cbms_Image_Id in monitor table Then processes the msg in poison table
                                    IF NOT EXISTS ( SELECT
                                                      1
                                                    FROM
                                                      #Cbms_Image_Queue_ErrorMessage queue_em
                                                      INNER JOIN #Service_Broker_Message_Monitor msg_monitor
                                                            ON msg_monitor.Queue_Name = queue_em.Queue_Name
                                                    WHERE
                                                      Message_Id = @id1
                                                      AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts
                                                      AND queue_em.CBMS_IMAGE_ID = msg_monitor.CBMS_IMAGE_ID )
                                          BEGIN 
                                                SET @Conversation_Handle = NULL;
							;
                                                BEGIN DIALOG CONVERSATION @Conversation_Handle --begin new conversation
													FROM SERVICE [//Change_Control/Service/CBMS/Cbms_Image_Transfer]
													TO SERVICE '//Change_Control/Service/CBMS/Cbms_Image_Transfer'
													ON CONTRACT [//Change_Control/Contract/Cbms_Image_Transfer];
						
								;
                                                SEND ON CONVERSATION @Conversation_Handle 
													MESSAGE TYPE [//Change_Control/Message/Cbms_Image_Transfer] (@Message_Body);
								;
                                                END CONVERSATION @Conversation_Handle	
                                          END	
					
					-- IF msg is present in monitor table (it will be either 'U' , if 'U' Check if cbms_image_id exists , then update @Msg_Body with values in msg monitor , if record doesnt exists then update @msg_body with values in msg_monitor and keep op_Code as 'I' )
                                    ELSE
                                          IF EXISTS ( SELECT
                                                            1
                                                      FROM
                                                            #Cbms_Image_Queue_ErrorMessage queue_em
                                                            INNER JOIN #Service_Broker_Message_Monitor msg_monitor
                                                                  ON msg_monitor.Queue_Name = queue_em.Queue_Name
                                                      WHERE
                                                            Message_Id = @id1
                                                            AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts
                                                            AND queue_em.Cbms_Image_ID = msg_monitor.Cbms_Image_ID
                                                            AND ( msg_monitor.Op_code = 'U'
                                                                  AND NOT EXISTS ( SELECT
                                                                                    1
                                                                                   FROM
                                                                                    #Service_Broker_Message_Monitor sbmm
                                                                                          WHERE  sbmm.cbms_image_id = msg_monitor.cbms_image_id
                                                                                             AND sbmm.Message_Received_Ts > msg_monitor.Message_Received_Ts
                                                                                             AND op_Code != 'D' ) )
                                                            AND queue_em.Op_Code = 'I' )
                                                BEGIN 
							 

                                                      UPDATE
                                                            #Service_Broker_Message_Monitor
                                                      SET
                                                            Message_Body.modify('replace value of (/Cbms_Image_Changes/Cbms_Image_Change/Op_Code/text())[1] with ("I")')
                                                      FROM
                                                            #Cbms_Image_Queue_ErrorMessage queue_em
                                                            INNER JOIN #Service_Broker_Message_Monitor msg_monitor
                                                                  ON msg_monitor.Queue_Name = queue_em.Queue_Name
                                                      WHERE
                                                            Message_Id = @id1
                                                            AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts
                                                            AND queue_em.Cbms_Image_ID = msg_monitor.Cbms_Image_ID
                                                            AND msg_monitor.Op_code = 'U'
                                                            AND queue_em.Op_Code = 'I'

                                                      SELECT
                                                            @Message_Body = msg_monitor.Message_Body
                                                      FROM
                                                            #Cbms_Image_Queue_ErrorMessage queue_em
                                                            INNER JOIN #Service_Broker_Message_Monitor msg_monitor
                                                                  ON msg_monitor.Queue_Name = queue_em.Queue_Name
                                                      WHERE
                                                            Message_Id = @id1
                                                            AND msg_monitor.Message_Received_Ts > queue_em.Message_Received_Ts
                                                            AND queue_em.Cbms_Image_ID = msg_monitor.Cbms_Image_ID
                                                            AND msg_monitor.Op_code = 'U'
                                                            AND queue_em.Op_Code = 'I'




                                                      SET @Conversation_Handle = NULL; -- No need to send 
							;
                                                      BEGIN DIALOG CONVERSATION @Conversation_Handle --begin new conversation
															FROM SERVICE [//Change_Control/Service/CBMS/Cbms_Image_Transfer]
															TO SERVICE '//Change_Control/Service/CBMS/Cbms_Image_Transfer'
															ON CONTRACT [//Change_Control/Contract/Cbms_Image_Transfer];
						
								;
                                                      SEND ON CONVERSATION @Conversation_Handle 
															MESSAGE TYPE [//Change_Control/Message/Cbms_Image_Transfer] (@Message_Body);
								;
                                                      END CONVERSATION @Conversation_Handle	
                                                END	


                              END				
				
				
                        SET @id1 = @id1 + 1
				
				-- Delete error message from poison table
                        DELETE FROM
                              dbo.Service_Broker_Poison_Message
                        WHERE
                              Queue_Name = 'Cbms_Image_Transfer_Queue'
                              AND Conversation_Group_Id = @Conversation_Group_Id
                              AND Conversation_Handle = @Old_Conversation_Handle
						
                        COMMIT TRANSACTION
			
                  END TRY
                  BEGIN CATCH
                        DECLARE @errormessage NVARCHAR(4000) = error_message()
                        IF @@TRANCOUNT > 1
                              ROLLBACK TRANSACTION
                        RAISERROR(60001, 16, 1, 'SP:Cbms_Image_Transfer_Queue_ErrorMessage_Processing', @errormessage)
                  END CATCH	
            END
END;







;
GO
