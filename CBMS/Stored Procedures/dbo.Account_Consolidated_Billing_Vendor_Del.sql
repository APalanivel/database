SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_Consolidated_Billing_Vendor_Del
           
DESCRIPTION:             
			To delete consolidated billing configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id			INT


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_Consolidated_Billing_Vendor
        
    BEGIN TRANSACTION
		SELECT * FROM dbo.Account_Consolidated_Billing_Vendor WHERE Account_Consolidated_Billing_Vendor_Id = 1
		EXEC dbo.Account_Consolidated_Billing_Vendor_Del 1
		SELECT * FROM dbo.Account_Consolidated_Billing_Vendor WHERE Account_Consolidated_Billing_Vendor_Id = 1
	ROLLBACK TRANSACTION    
		

	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-20	Contract placeholder - CP-50 Created
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Del]
      ( 
       @Account_Consolidated_Billing_Vendor_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DELETE FROM
            dbo.Account_Consolidated_Billing_Vendor
      WHERE
            Account_Consolidated_Billing_Vendor_Id = @Account_Consolidated_Billing_Vendor_Id
      
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Del] TO [CBMSApplication]
GO
