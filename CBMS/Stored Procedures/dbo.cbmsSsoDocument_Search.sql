SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******


NAME:  

	dbo.cbmsSsoDocument_Search


DESCRIPTION:

INPUT PARAMETERS:  

Name				DataType	Default Description  
------------------------------------------------------------

@start_date			datetime		null
@end_date			datetime		null
@category_id		int				null
@client_id			int				null
@division_id		int				null
@site_id			int				null
@is_active			bit				null
@document_title     VARCHAR(200)	NULL  


OUTPUT PARAMETERS:  

Name				DataType	Default Description  

------------------------------------------------------------



USAGE EXAMPLES:  

------------------------------------------------------------  

    DECLARE @categories tvpCategory
    EXEC dbo.cbmsSsoDocument_Search 
      NULL
     ,NULL
     ,@categories
     ,235
    ,NULL
     ,NULL
     ,NULL
     ,'Kraft Foods Group, Inc._Energy Market Report_December 2013.xlsx'

	

    DECLARE @Tvp AS tvpCategory
    EXEC dbo.cbmsSsoDocument_Search 
      NULL
     ,NULL
     ,@Tvp
     ,235
     ,263
     ,NULL
     ,NULL
	

    DECLARE @Tvp AS tvpCategory

    EXEC dbo.cbmsSsoDocument_Search 
      NULL
     ,NULL
     ,@Tvp
     ,235
     ,262
     ,NULL
     ,NULL
     ,NULL

		



AUTHOR INITIALS:  

 Initials	Name  

------------------------------------------------------------

 CPE		Chaitanya Panduga Eshwar
 DRG		Dhilu
 RR			Raghu Reddy
 RMG        Rani Mary George

 

MODIFICATIONS  

 Initials	Date			Modification  

------------------------------------------------------------  
 CPE		03/28/2011		Replaced vwCbmsSsoDocumentOwnerFlat view with SSO_DOCUMENT_OWNER_MAP tables.
 DRG		02/SEP/2014		MOdified to take category from new table Client_document_category
 RMG        24/NOV/2014     DME-49 Issue fix changes(Search using document title instead of file name)

 
************************************************************************************************/

CREATE PROCEDURE [dbo].[cbmsSsoDocument_Search]
      ( 
       @start_date DATETIME = NULL
      ,@end_date DATETIME = NULL
      ,@categories tvpCategory READONLY
      ,@client_id INT = NULL
      ,@division_id INT = NULL
      ,@site_id INT = NULL
      ,@is_active BIT = NULL
      ,@document_title VARCHAR(200) = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            sd.SSO_Document_ID
           ,sd.DOCUMENT_TITLE
           ,sd.DOCUMENT_DESCRIPTION
           ,sd.DOCUMENT_REFERENCE_DATE
           ,ci.CBMS_Image_Size
           ,ci.CBMS_Doc_Id
           ,Cd.Display_Seq AS Owner_Sort
           ,left(cat.category, len(cat.category) - 1) AS category_type
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS Owner_Id
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS Owner_Name
      FROM
            dbo.SSO_Document sd
            INNER JOIN cbms_image ci
                  ON ci.CBMS_IMAGE_ID = sd.CBMS_IMAGE_ID
            INNER JOIN dbo.Client_Document_Category_map cdcm
                  ON sd.SSO_Document_ID = cdcm.SSO_Document_ID
            CROSS APPLY ( SELECT
                              cdc.Category_Name + ','
                          FROM
                              dbo.Client_Document_Category cdc
                              INNER JOIN dbo.Client_Document_Category_map cdcm
                                    ON cdcm.Client_Document_Category_Id = cdc.Client_Document_Category_Id
                          WHERE
                              sd.SSO_Document_ID = cdcm.SSO_Document_ID
            FOR
                          XML PATH('') ) cat ( category )
            INNER JOIN dbo.SSO_DOCUMENT_OWNER_MAP own
                  ON own.SSO_DOCUMENT_ID = sd.SSO_DOCUMENT_ID
            INNER JOIN core.Client_Hier CH
                  ON own.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Code cd
                  ON ch.Hier_level_Cd = cd.Code_Id
      WHERE
            ( @end_date IS NULL
              OR SD.DOCUMENT_REFERENCE_DATE <= @end_date )
            AND ( @start_date IS NULL
                  OR SD.DOCUMENT_REFERENCE_DATE >= @start_date )
            AND ( @document_title IS NULL
                  OR sd.DOCUMENT_TITLE LIKE '%' + @document_title + '%' )
            AND ( @is_active IS NULL
                  OR case CD.Code_Value
                       WHEN 'Corporate' THEN CH.Client_Not_Managed
                       WHEN 'Division' THEN CH.Division_Not_Managed
                       WHEN 'Site' THEN CH.Site_Not_Managed
                     END = @is_active )
            AND ( @Client_Id IS NULL
                  OR CH.client_id = @client_id )
            AND ( @division_id IS NULL
                  OR CH.Sitegroup_Id = @division_id )
            AND ( @site_id IS NULL
                  OR CH.Site_Id = @site_id )
            AND ( NOT EXISTS ( SELECT
                                    1
                               FROM
                                    @categories )
                  OR cdcm.Client_Document_Category_Id IN ( SELECT
                                                            category_id
                                                           FROM
                                                            @categories ) )
      GROUP BY
            sd.SSO_Document_ID
           ,sd.DOCUMENT_TITLE
           ,sd.DOCUMENT_DESCRIPTION
           ,sd.DOCUMENT_REFERENCE_DATE
           ,ci.CBMS_Image_Size
           ,ci.CBMS_Doc_Id
           ,Cd.Display_Seq
           ,left(cat.category, len(cat.category) - 1)
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END
           ,case CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END
      ORDER BY
            SD.document_title
           ,SD.sso_document_id
           ,owner_sort
           ,owner_name



END;












;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_Search] TO [CBMSApplication]
GO
