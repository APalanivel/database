SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Ins]
           
DESCRIPTION:             
			To insert service condition template details
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Template_Name						NVARCHAR(255)
    @Is_Active							BIT				1
    @User_Info_Id						INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRAN
		SELECT * FROM dbo.Sr_Service_Condition_Template where Template_Name = 'Test Service Condition Template'
		DECLARE @Template_Id Int
			
        EXEC dbo.Sr_Service_Condition_Template_Ins 
			@Template_Name = 'Test Service Condition Template'
			,@Is_Active = 1
			,@User_Info_Id = 16
			,@Sr_Service_Condition_Template_Id = @Template_Id OUT
			,@Commodity_Id =290
			,@Country_Id = '4,6,9'
			,@SR_PRICING_PRODUCT_ID ='155,156'
		SELECT @Template_Id	
		SELECT * FROM dbo.Sr_Service_Condition_Template a 
			JOIN dbo.Sr_Service_Condition_Template_Product_Map b ON a.Sr_Service_Condition_Template_Id = b.Sr_Service_Condition_Template_Id
			JOIN dbo.SR_Pricing_Product_Country_Map c ON b.SR_Pricing_Product_Country_Map_Id = c.SR_Pricing_Product_Country_Map_Id
			where Template_Name = 'Test Service Condition Template'
	ROLLBACK
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-22	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Ins]
      ( 
       @Template_Name NVARCHAR(255)
      ,@Is_Active BIT = 1
      ,@User_Info_Id INT
      ,@Sr_Service_Condition_Template_Id INT OUT
      ,@Commodity_Id INT
      ,@Country_Id VARCHAR(MAX)
      ,@SR_PRICING_PRODUCT_ID VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      INSERT      INTO dbo.Sr_Service_Condition_Template
                  ( 
                   Template_Name
                  ,Is_Active
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
      VALUES
                  ( 
                   @Template_Name
                  ,@Is_Active
                  ,@User_Info_Id
                  ,getdate()
                  ,@User_Info_Id
                  ,getdate() )
                  
      SET @Sr_Service_Condition_Template_Id = scope_identity()
      
      INSERT      INTO dbo.Sr_Service_Condition_Template_Product_Map
                  ( 
                   Sr_Service_Condition_Template_Id
                  ,SR_Pricing_Product_Country_Map_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Sr_Service_Condition_Template_Id
                       ,sppcm.SR_Pricing_Product_Country_Map_Id
                       ,@User_Info_Id
                       ,getdate()
                       ,@User_Info_Id
                       ,getdate()
                  FROM
                        dbo.SR_PRICING_PRODUCT srpr
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON srpr.SR_PRICING_PRODUCT_ID = sppcm.SR_PRICING_PRODUCT_ID
                  WHERE
                        srpr.COMMODITY_TYPE_ID = @Commodity_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.ufn_split(@Country_Id, ',')
                                     WHERE
                                          sppcm.Country_ID = cast(Segments AS INT) )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.ufn_split(@SR_PRICING_PRODUCT_ID, ',')
                                     WHERE
                                          srpr.SR_PRICING_PRODUCT_ID = cast(Segments AS INT) )
      
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Ins] TO [CBMSApplication]
GO
