SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
  
NAME: dbo.Add_Site_To_Roles  
  
DESCRIPTION: To add client_hier_id to all the roles that have the site�s division assigned.
  
INPUT PARAMETERS:      
 Name			DataType          Default     Description      
------------------------------------------------------------------      
 @Site_Id	    INT  
      
OUTPUT PARAMETERS:
 Name          DataType          Default     Description      
------------------------------------------------------------      

  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Add_Site_To_Roles 622
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 PNR	  Pandarinath  
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 PNR		11/23/2010	created as part proj : Security Roles Administration.
 
******/  

CREATE PROCEDURE dbo.Add_Site_To_Roles
    (
		@Site_Id	INT
    )
AS 
BEGIN  
	
	SET NOCOUNT ON ;
	
	DECLARE
		  @Client_Id				INT
		 ,@Division_Id				INT
		 ,@Site_Client_Hier_Id		INT
		 ,@Division_Client_Hier_Id	INT

	SELECT
		 @Client_Id = CLIENT_ID
		,@Division_Id = DIVISION_ID
	FROM 
		dbo.Site
	WHERE
		SITE_ID = @Site_Id

	SELECT
		 @Site_Client_Hier_Id = ch.Client_Hier_Id
	FROM
		Core.Client_Hier ch
		JOIN dbo.Code c 
			 ON ch.Hier_level_Cd = c.Code_Id
		JOIN dbo.Codeset cs 
			 ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Site_Id = @Site_Id
		AND cs.Codeset_Name = 'HierLevel'
		AND c.Code_Value = 'Site'
		
	SELECT
		 @Division_Client_Hier_Id = ch.Client_Hier_Id
	FROM
		Core.Client_Hier ch
		JOIN dbo.Code c 
			 ON ch.Hier_level_Cd = c.Code_Id
		JOIN dbo.Codeset cs 
			 ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Sitegroup_Id = @Division_Id
		AND cs.Codeset_Name = 'HierLevel'
		AND c.Code_Value = 'Division'

	INSERT INTO dbo.Security_Role_Client_Hier
	(
		 Security_Role_Id
		,Client_Hier_Id
	)
	SELECT
		 sch1.security_role_id
		,@Site_Client_Hier_Id
	FROM
		dbo.Security_Role_Client_Hier sch1
		JOIN dbo.Security_Role sr 
				   ON sr.Security_Role_Id = sch1.Security_Role_Id
		JOIN dbo.Security_Role_Client_Hier sch2 
				   ON sch1.Security_Role_Id = sch2.Security_Role_Id
	WHERE
		sr.Client_Id = @Client_Id
		AND sch2.Client_Hier_Id = @Division_Client_Hier_Id
	GROUP BY sch1.security_role_id

END
GO
GRANT EXECUTE ON  [dbo].[Add_Site_To_Roles] TO [CBMSApplication]
GO
