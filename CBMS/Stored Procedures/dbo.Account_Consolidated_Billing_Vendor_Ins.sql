SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_Consolidated_Billing_Vendor_Ins
           
DESCRIPTION:             
			To insert consolidated billing configurations
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id				INT
    @Billing_Start_Dt		DATE
    @Billing_End_Dt			DATE
    @Invoice_Vendor_Type_Id INT
    @Supplier_Vendor_Id		INT			NULL
    @User_Info_Id			INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_Consolidated_Billing_Vendor
            
	BEGIN TRANSACTION
		SELECT * FROM  dbo.Account_Consolidated_Billing_Vendor WHERE Account_Id = 2 
		EXEC dbo.Account_Consolidated_Billing_Vendor_Ins 2,'2018-1-1','2018-12-31',288,1,16
		SELECT * FROM  dbo.Account_Consolidated_Billing_Vendor WHERE Account_Id = 2
	ROLLBACK TRANSACTION
	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-19	Contract placeholder - CP-50 Created
******/

CREATE PROCEDURE [dbo].[Account_Consolidated_Billing_Vendor_Ins]
      ( 
       @Account_Id INT
      ,@Billing_Start_Dt DATE
      ,@Billing_End_Dt DATE
      ,@Invoice_Vendor_Type_Id INT
      ,@Supplier_Vendor_Id INT = NULL
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      INSERT      INTO dbo.Account_Consolidated_Billing_Vendor
                  ( 
                   Account_Id
                  ,Billing_Start_Dt
                  ,Billing_End_Dt
                  ,Invoice_Vendor_Type_Id
                  ,Supplier_Vendor_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @Account_Id
                       ,@Billing_Start_Dt
                       ,@Billing_End_Dt
                       ,@Invoice_Vendor_Type_Id
                       ,@Supplier_Vendor_Id
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  
                                          
      
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Consolidated_Billing_Vendor_Ins] TO [CBMSApplication]
GO
