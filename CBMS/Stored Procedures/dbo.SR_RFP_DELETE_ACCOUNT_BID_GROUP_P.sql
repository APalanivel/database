SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_ACCOUNT_BID_GROUP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId int,
@groupId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_DELETE_ACCOUNT_BID_GROUP_P NULL, 3856

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_DELETE_ACCOUNT_BID_GROUP_P
@rfpId int,
@groupId int

AS
	
SET NOCOUNT ON

UPDATE SR_RFP_ACCOUNT SET SR_RFP_BID_GROUP_ID = NULL
WHERE 
	SR_RFP_BID_GROUP_ID = @groupId
AND 	SR_RFP_ID = @rfpId

DELETE SR_RFP_BID_GROUP WHERE SR_RFP_BID_GROUP_ID = @groupId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_ACCOUNT_BID_GROUP_P] TO [CBMSApplication]
GO
