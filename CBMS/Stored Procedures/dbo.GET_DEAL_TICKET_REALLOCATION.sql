SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_DEAL_TICKET_REALLOCATION

@userId varchar,
@sessionId varchar,
@clientId integer,
@dealTicketId integer,
@startDate datetime,
@endDate datetime,
@currencyUnit integer,
@consumptionUnit integer
AS
	set nocount on
	select 
	rmdtvd.site_id,
	DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
	DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
	sum(rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR) hedgeVolume
	
from
	RM_DEAL_TICKET_DETAILS rmdtd,
	RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
	CURRENCY_UNIT_CONVERSION currency,
	CONSUMPTION_UNIT_CONVERSION consumption,
	CLIENT_CURRENCY_GROUP_MAP CCGM
where
	rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
	rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID and
	rmdtd.month_identifier between @startDate and @endDate and
	
	consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)and
	consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
	CCGM.CLIENT_ID=@clientId AND
	CCGM.CURRENCY_GROUP_ID=currency.CURRENCY_GROUP_ID AND
	currency.BASE_UNIT_ID=(select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId) AND
	currency.CONVERTED_UNIT_ID=@currencyUnit AND
	CONVERSION_DATE=
	(
	SELECT MAX(CONVERSION_DATE) 
	FROM CURRENCY_UNIT_CONVERSION cuc
	WHERE cuc.BASE_UNIT_ID=(select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)
	AND cuc.CONVERTED_UNIT_ID=@currencyUnit
	)
group by 
	DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER),DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER),rmdtvd.site_id

order by rmdtvd.site_id
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_REALLOCATION] TO [CBMSApplication]
GO
