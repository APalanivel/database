SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [dbo].[CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id]

DESCRIPTION:

	To Get invoice existence status for the given account_id except the given Invoice_id.

	Used by Stage-3 Application.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT						Utility Account

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id  2029962, 2181
	
	EXEC dbo.CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id  1759076, 60012

	EXEC dbo.CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id  996224, 60013
	
grantexecute
	
	SELECT TOP 10
		Account_Id, COUNT(1) CNT
	FROM
		Cu_Invoice
	GROUP BY
		Account_id
	ORDER BY
		CNT DESC		
	
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
VV			Venkat Vangala

MODIFICATIONS
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
VV			10-JUN-10	CREATED
HG			21-JUN-10	Modified to refer Account_Id from Cu_Invoice_Service_Month table as this column moved from Cu_Invoice as a part of Group bill.
NR			2019-04-10	MAINT-8602 - Unknown Month logic - Added Service_Month Filter.
*/

CREATE PROCEDURE [dbo].[CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id]
    (
        @Cu_Invoice_Id INT
        , @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Invoice_Exist BIT = 0;

        IF EXISTS (   SELECT
                            1
                      FROM
                            dbo.CU_INVOICE cui
                            JOIN dbo.CU_INVOICE_SERVICE_MONTH cusm
                                ON cui.CU_INVOICE_ID = cusm.CU_INVOICE_ID
                      WHERE
                            cusm.Account_ID = @Account_Id
                            AND cui.CU_INVOICE_ID != @Cu_Invoice_Id
                            AND cusm.SERVICE_MONTH IS NOT NULL
							AND cui.IS_REPORTED = 1
							AND cui.IS_PROCESSED = 1
							AND cui.IS_DNT = 0
							AND cui.IS_DUPLICATE = 0
							)
            BEGIN

                SET @Is_Invoice_Exist = 1;

            END;

        SELECT  @Is_Invoice_Exist Is_Invoice_Exist;

    END;


GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Existence_Status_Sel_By_Account_Invoice_Id] TO [CBMSApplication]
GO
