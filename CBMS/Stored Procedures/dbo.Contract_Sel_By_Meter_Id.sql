SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Sel_By_Meter_Id]  
     
DESCRIPTION: 
	To Get Contracts Information for Selected Meter Id with pagination.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT						
@Start_Index	INT			1
@End_Index		INT			2147483647			
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  

	EXEC Contract_Sel_By_Meter_Id  62461,1,1
	
	EXEC Contract_Sel_By_Meter_Id  559,1,5
	
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR		PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		03-JUNE-10		CREATED  
*/

CREATE PROCEDURE dbo.Contract_Sel_By_Meter_Id
(
	 @Meter_Id		INT						
	,@Start_Index	INT = 1
	,@End_Index		INT = 2147483647
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	WITH Cte_Contracts
	AS
	(
		SELECT 
			  c.ED_CONTRACT_NUMBER
			  ,Row_Num = ROW_NUMBER() OVER (ORDER BY c.ED_CONTRACT_NUMBER)
			  ,Total = COUNT(1) OVER ()
		FROM 
			dbo.SUPPLIER_ACCOUNT_METER_MAP map
			JOIN dbo.CONTRACT c
				 ON map.Contract_ID=C.CONTRACT_ID
		WHERE 
			map.METER_ID = @Meter_Id
	)
	SELECT
		  contr.ED_CONTRACT_NUMBER
		  ,contr.Total
	FROM
		Cte_Contracts contr
	WHERE 
	    contr.Row_Num BETWEEN @Start_Index AND @End_Index

END
GO
GRANT EXECUTE ON  [dbo].[Contract_Sel_By_Meter_Id] TO [CBMSApplication]
GO
