SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                
Name:   dbo.Invoice_Collection_Issue_Upd_By_Invoice_Collection_Queue     
                
Description:                
   This sproc update Invoice collection issue for given queue ids
                             
 Input Parameters:                
    Name												DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
@Invoice_Collection_Queue_Ids				VARCHAR(MAX)
@User_Info_Id								INT           
@Event_Status_Desc							VARCHAR(50) Can be 'Excluded' or 'Received' OR 'Reset from Excluded' default Excluded.  
@Issue_Status_Cd							INT			NULL default as Closed but when reset from excluded is selected it will be Opened     
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
  
  BEGIN TRAN
  SELECT TOP 1 * FROM dbo.Invoice_Collection_Issue_Event ORDER BY Invoice_Collection_Issue_Event_Id DESC

  EXEC Invoice_Collection_Issue_Upd_By_Invoice_Collection_Queue 
      @Invoice_Collection_Queue_Ids = '141,142'
     ,@User_Info_Id = 49
     ,@Event_Status_Desc = 'Excluded'
     SELECT TOP 1 * FROM dbo.Invoice_Collection_Issue_Event ORDER BY Invoice_Collection_Issue_Event_Id DESC

  ROLLBACK
  
    
  BEGIN TRAN
       SELECT TOP 1 * FROM dbo.Invoice_Collection_Issue_Event ORDER BY Invoice_Collection_Issue_Event_Id DESC

  EXEC Invoice_Collection_Issue_Upd_By_Invoice_Collection_Queue 
      @Invoice_Collection_Queue_Ids = '141,142'
     ,@User_Info_Id = 49
     ,@Event_Status_Desc = 'Received'
          SELECT TOP 1 * FROM dbo.Invoice_Collection_Issue_Event ORDER BY Invoice_Collection_Issue_Event_Id DESC

  ROLLBACK
  
  
       
     
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
SP			Sandeep Pigilam 
 
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    SP			2016-01-19		created for Invoice collection.       
	SLP			2019-11-14		Blocker flag has to be removed when issue is closed
								(Updated as part of the Story: DO20-343)   
            
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Upd_By_Invoice_Collection_Queue]
(
    @Invoice_Collection_Queue_Ids VARCHAR(MAX),
    @User_Info_Id INT,
    @Event_Status_Desc VARCHAR(50) = 'Excluded',
    @Issue_Status_Cd INT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Issue_Status_Change_Event_Type_Cd INT,
            @Ts DATETIME = GETDATE(),
            @User_Name VARCHAR(80),
            @User_Ts VARCHAR(50);

    SELECT @User_Name = ui.FIRST_NAME + ' ' + ui.LAST_NAME,
           @User_Ts = REPLACE(CONVERT(VARCHAR, @Ts, 106), ' ', '-')
    FROM dbo.USER_INFO ui
    WHERE ui.USER_INFO_ID = @User_Info_Id;


    SELECT @Issue_Status_Change_Event_Type_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
          AND c.Code_Value = 'Issue Status Change';


    SELECT @Issue_Status_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Codeset_Name = 'IC Chase Status'
          AND c.Code_Value = 'Close'
          AND @Issue_Status_Cd IS NULL;

    SELECT @Issue_Status_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Codeset_Name = 'IC Chase Status'
          AND c.Code_Value = 'Close'
          AND @Issue_Status_Cd IS NULL;


    BEGIN TRY
        BEGIN TRAN;




        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Issue_Status_Change_Event_Type_Cd,
               @User_Name + ' ' + @User_Ts + ' Invoice marked as ' + @Event_Status_Desc + '. Issue '
               + CASE
                     WHEN @Event_Status_Desc = 'Reset from Excluded' THEN
                         'Opened'
                     ELSE
                         'Closed'
                 END + ' Automatically',
               @User_Info_Id,
               @Ts
        FROM Invoice_Collection_Issue_Log ici
            INNER JOIN dbo.Code aic
                ON ici.Issue_Status_Cd = aic.Code_Id
            INNER JOIN dbo.Code gic
                ON gic.Code_Id = @Issue_Status_Cd
        WHERE ici.Issue_Status_Cd <> @Issue_Status_Cd
              AND EXISTS
        (
            SELECT 1
            FROM dbo.ufn_split(@Invoice_Collection_Queue_Ids, ',') ai
            WHERE ai.Segments = ici.Invoice_Collection_Queue_Id
        );



        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Created_Ts
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Issue_Status_Change_Event_Type_Cd,
               'Issue Status change from ' + aic.Code_Value + ' to ' + gic.Code_Value,
               @User_Info_Id,
               @Ts
        FROM Invoice_Collection_Issue_Log ici
            LEFT JOIN dbo.Code aic
                ON ici.Issue_Status_Cd = aic.Code_Id
            LEFT JOIN dbo.Code gic
                ON gic.Code_Id = @Issue_Status_Cd
        WHERE ici.Issue_Status_Cd <> @Issue_Status_Cd
              AND EXISTS
        (
            SELECT 1
            FROM dbo.ufn_split(@Invoice_Collection_Queue_Ids, ',') ai
            WHERE ai.Segments = ici.Invoice_Collection_Queue_Id
        )
              AND @Issue_Status_Cd IS NOT NULL;



        UPDATE ici
        SET Issue_Status_Cd = @Issue_Status_Cd,
            Updated_User_Id = @User_Info_Id,
            Last_Change_Ts = @Ts
        FROM dbo.Invoice_Collection_Issue_Log ici
        WHERE EXISTS
        (
            SELECT 1
            FROM dbo.ufn_split(@Invoice_Collection_Queue_Ids, ',') ai
            WHERE ai.Segments = ici.Invoice_Collection_Queue_Id
        );


        UPDATE ici
        SET ici.Is_Blocker = 0,
            Blocker_Action_Date = NULL
        FROM dbo.Invoice_Collection_Issue_Log ici
            JOIN Code c
                ON c.Code_Id = ici.Issue_Status_Cd
                   AND c.Code_Id = @Issue_Status_Cd
        WHERE ici.Is_Blocker = 1
              AND c.Code_Value = 'Close'
              AND EXISTS
        (
            SELECT 1
            FROM dbo.ufn_split(@Invoice_Collection_Queue_Ids, ',') ai
            WHERE ai.Segments = ici.Invoice_Collection_Queue_Id
        );

        COMMIT TRAN;
    END TRY
    BEGIN CATCH

        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
        END;

        EXEC dbo.usp_RethrowError;

    END CATCH;



END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Upd_By_Invoice_Collection_Queue] TO [CBMSApplication]
GO
