SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE  PROCEDURE [dbo].[dmlLookup_GetAll]
AS
BEGIN
   select t.LookupTypeId
	, t.LookupTypeName
	, l.LookupId
	, l.LookupName
	, l.SortOrder
     from dmlLookupType t
     join dmlLookup l on l.LookupTypeId = t.LookupTypeId
 order by t.LookupTypeName
	, l.SortOrder
	, l.LookupName
END



GO
GRANT EXECUTE ON  [dbo].[dmlLookup_GetAll] TO [CBMSApplication]
GO
