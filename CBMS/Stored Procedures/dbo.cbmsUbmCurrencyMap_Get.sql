SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUbmCurrencyMap_Get]
	( @MyAccountId int
	, @ubm_currency_map_id int
	)
AS
BEGIN

	   select ubm_currency_map_id
		, ubm_id
		, ubm_currency_code
		, currency_unit_id
	     from ubm_currency_map
	    where ubm_currency_map_id = @ubm_currency_map_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmCurrencyMap_Get] TO [CBMSApplication]
GO
