SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Site_Count_By_Division_Client]  
     
DESCRIPTION: 

	To get the count of sites in Division which the site is associated and the total no of sites for the client the site is associated with.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Division_Id				INT						Division Id of the selected Site
@Client_Id					INT						Client_id of the selected site

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC dbo.Site_Count_By_Division_Client  10069, 569
	
	EXEC dbo.Site_Count_By_Division_Client  11231,12208672
	EXEC dbo.Site_Count_By_Division_Client  11231,1914

	SELECT 
		DISTINCT DIVISION_ID
		,COUNT(1)
	FROM 
		SITE 
	WHERE
		CLient_ID = 11231 
	GROUP BY 
		DIVISION_ID

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			01/06/2011	Created for Site bulk update enhancement

*/

CREATE PROCEDURE dbo.Site_Count_By_Division_Client
      @Client_Id	INT
     ,@Division_Id	INT
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            SUM(CASE WHEN Division_Id = @Division_Id THEN 1
                     ELSE 0
                END)									AS Site_Cnt_In_Division
           ,COUNT(Client_Id)							AS Site_Cnt_In_Client
      FROM
            dbo.Site
      WHERE
			Client_id = @Client_Id

END
GO
GRANT EXECUTE ON  [dbo].[Site_Count_By_Division_Client] TO [CBMSApplication]
GO
