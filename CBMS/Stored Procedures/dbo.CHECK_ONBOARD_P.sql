SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CHECK_ONBOARD_P]
    @meter_id INT
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @RM_ONBOARD_HEDGE_SETUP_ID AS INT;

        SELECT
            @RM_ONBOARD_HEDGE_SETUP_ID = chhc.RM_Client_Hier_Hedge_Config_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Core.Client_Hier_Account cha
                       WHERE
                            cha.Meter_Id = @meter_id
                            AND chob.Commodity_Id = cha.Commodity_Id
                            AND cha.Client_Hier_Id = ch.Client_Hier_Id)
            AND ht.ENTITY_NAME <> 'Does Not Hedge';

        SELECT
            @RM_ONBOARD_HEDGE_SETUP_ID = chhc.RM_Client_Hier_Hedge_Config_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chcl
                ON ch.Client_Id = chcl.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chcl.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Core.Client_Hier_Account cha
                            INNER JOIN Core.Client_Hier ch1
                                ON ch1.Client_Hier_Id = cha.Client_Hier_Id
                       WHERE
                            cha.Meter_Id = @meter_id
                            AND chob.Commodity_Id = cha.Commodity_Id
                            AND ch.Client_Id = ch1.Client_Id)
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
            AND chcl.Sitegroup_Id = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    siteonb.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id);

        SELECT  @RM_ONBOARD_HEDGE_SETUP_ID AS RM_ONBOARD_HEDGE_SETUP_ID;

    END;

GO
GRANT EXECUTE ON  [dbo].[CHECK_ONBOARD_P] TO [CBMSApplication]
GO
