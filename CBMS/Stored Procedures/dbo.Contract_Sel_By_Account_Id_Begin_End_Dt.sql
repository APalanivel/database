SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Contract_Sel_By_Account_Id_Begin_End_Dt            
                        
 DESCRIPTION:                        
			To get Supplier Account Details                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
@Account_Id						INT
@Start_Dt						Date
@End_Dt							Date			                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 USAGE EXAMPLES:                            
------------------------------------------------------------------------------     
 
EXEC dbo.Contract_Sel_By_Account_Id_Begin_End_Dt
    @Account_Id = 123
    , @Start_Dt = '2019-01-01'
    , @End_Dt ='2019-01-01'

    

  
                     
 AUTHOR INITIALS:        
       
 Initials              Name        
------------------------------------------------------------------------------     
 NR						Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
------------------------------------------------------------------------------     
 NR                    2019-05-29       Created for - Add Contract.
                       
******/
CREATE PROCEDURE [dbo].[Contract_Sel_By_Account_Id_Begin_End_Dt]
     (
         @Account_Id INT
         , @Start_Dt DATE
         , @End_Dt DATE
     )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cha.Supplier_Contract_ID
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND (   @Start_Dt BETWEEN Supplier_Account_begin_Dt
                              AND     Supplier_Account_End_Dt
                    OR  @End_Dt BETWEEN Supplier_Account_begin_Dt
                                AND     Supplier_Account_End_Dt
                    OR  Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                  AND     @End_Dt
                    OR  Supplier_Account_End_Dt BETWEEN @Start_Dt
                                                AND     @End_Dt)
            AND cha.Supplier_Contract_ID <> -1;




    END;

GO
GRANT EXECUTE ON  [dbo].[Contract_Sel_By_Account_Id_Begin_End_Dt] TO [CBMSApplication]
GO
