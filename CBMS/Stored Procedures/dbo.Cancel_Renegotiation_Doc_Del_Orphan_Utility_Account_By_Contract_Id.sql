SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 dbo.Cancel_Renegotiation_Doc_Del_Orphan_Utility_Account_By_Contract_Id
 DESCRIPTION:   To delete the orphan utility account from SR_CANCEL_RENEGOTIATION_DOCUMENT for the given contract_id  
 
 INPUT PARAMETERS:  
 Name     DataType  Default Description  
----------------------------------------------------------------  
 @contract_id      INT  
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------  

BEGIN TRAN    
SELECT * FROM SR_CANCEL_RENEGOTIATION_DOCUMENT WHERE Contract_id = 24216 AND ACcount_Id = 13264   
EXEC dbo.Cancel_Renegotiation_Doc_Del_Orphan_Utility_Account_By_Contract_Id  24216   
SELECT * FROM dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT WHERE Contract_id = 24216  AND ACcount_Id = 13264  
ROLLBACK TRAN   
 
BEGIN TRAN    
SELECT * FROM SR_CANCEL_RENEGOTIATION_DOCUMENT WHERE Contract_id = 53173 AND ACcount_Id = 95505   
EXEC dbo.Cancel_Renegotiation_Doc_Del_Orphan_Utility_Account_By_Contract_Id  53173   
SELECT * FROM SR_CANCEL_RENEGOTIATION_DOCUMENT WHERE Contract_id = 53173  AND ACcount_Id = 95505  
ROLLBACK TRAN   



 Initials Name  
------------------------------------------------------------  
 AKR		Ashok Kumar Raju    
 HG			Harihara Suthan G

 Initials	Date		Modification  
------------------------------------------------------------  
 AKR        06/13/2011	MAINT-657 Created to delete the orphan utility account from SR_CANCEL_RENEGOTIATION_DOCUMENT
 HG			06/21/2011	Table variable created to save the Utility accounts of contract removed and the using the base tables in the subselect query which used to fetch orphan utility account from SR_CANCEL_RENEGOTIATION_DOCUMENT
						
******/
  
CREATE PROCEDURE dbo.Cancel_Renegotiation_Doc_Del_Orphan_Utility_Account_By_Contract_Id ( @contract_id INT )
AS 
BEGIN
   
      SET NOCOUNT ON  
  
  
      DECLARE @Sr_Cancel_Renegotiation_Document_List TABLE
		(
		 Sr_Cancel_Renegotiation_Document_Id INT PRIMARY KEY CLUSTERED
		,Cbms_Image_ID INT )

      DECLARE @Cbms_Image_id TABLE ( Cbms_Image_ID INT )

      DECLARE @Sr_Cancel_Renegotiation_Document_Id INT

      BEGIN TRY
            BEGIN TRAN

			-- Save the orphan utilty account of Contract
            INSERT INTO
                  @Sr_Cancel_Renegotiation_Document_List
                  ( 
                   Sr_Cancel_Renegotiation_Document_Id
                  ,Cbms_Image_ID )
                  SELECT
                        crd.Sr_Cancel_Renegotiation_Document_Id
                       ,crd.CBMS_IMAGE_ID
                  FROM
                        dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT crd
                  WHERE
                        crd.CONTRACT_ID = @Contract_Id
                        AND NOT EXISTS ( SELECT
                                          m.Account_Id
                                         FROM
                                          dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                          INNER JOIN dbo.Meter m
                                                ON samm.Meter_Id = m.Meter_Id
                                         WHERE
                                          samm.Contract_Id = @Contract_Id
                                          AND m.Account_Id = crd.ACCOUNT_ID ) 

            INSERT INTO
                  @Cbms_Image_id ( Cbms_Image_ID )
                  SELECT
                        Cbms_Image_ID
                  FROM
                        @Sr_Cancel_Renegotiation_Document_List
                  GROUP BY
                        Cbms_Image_ID
  
     -- Delete the records based on the PK of SR_CANCEL_RENEGOTIATION_DOCUMENT  
            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Sr_Cancel_Renegotiation_Document_List )  
                  BEGIN  
  
                        SET @Sr_Cancel_Renegotiation_Document_Id = ( SELECT TOP 1
                                                                        SR_CANCEL_RENEGOTIATION_DOCUMENT_ID
                                                                     FROM
                                                                        @Sr_Cancel_Renegotiation_Document_List )                    

                        EXEC dbo.SR_Cancel_Renegotiation_Document_Del @Sr_Cancel_Renegotiation_Document_Id

                        DELETE
                              crd
                        FROM
                              @Sr_Cancel_Renegotiation_Document_List crd
                        WHERE
                              Sr_Cancel_Renegotiation_Document_Id = @Sr_Cancel_Renegotiation_Document_Id  
  
                  END

            COMMIT TRAN

            SELECT
                  Cbms_Image_Id
            FROM
                  @Cbms_Image_id

      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0
                  ROLLBACK TRAN

            EXEC dbo.usp_RethrowError

      END CATCH

END
GO

GRANT EXECUTE ON  [dbo].[Cancel_Renegotiation_Doc_Del_Orphan_Utility_Account_By_Contract_Id] TO [CBMSApplication]