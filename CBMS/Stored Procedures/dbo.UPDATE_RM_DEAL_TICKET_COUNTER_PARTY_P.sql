SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_RM_DEAL_TICKET_COUNTER_PARTY_P
	@is_bid BIT,
	@rm_counterparty_id INT,
	@rm_deal_ticket_id INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.rm_deal_ticket 
	SET is_bid=@is_bid,rm_counterparty_id = @rm_counterparty_id 
	WHERE rm_deal_ticket_id = @rm_deal_ticket_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_DEAL_TICKET_COUNTER_PARTY_P] TO [CBMSApplication]
GO
