SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:

	dbo.Move_Account_To_New_Site

DESCRIPTION:
 Updates the Account details in all related table to map to new Site

INPUT PARAMeterS:
 Name					DataType	Default		Description
-----------------------------------------------------------------
 @Account_Id			INT
 @Old_Client_Hier_Id	INT
 @New_Client_Hier_Id	INT
 @User_Info_Id			INT						User who is moving the Account

OUTPUT PARAMeterS:
 Name					DataType	Default		Description
-----------------------------------------------------------------
BEGIN TRAN     
 EXEC Move_Account_To_New_Site 750897, 374752, 2279    
 SELECT * FROM dbo.Application_Audit_Log WHERE Application_Name='Move Account'    
ROLLBACK    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------    
    
AUTHOR INITIALS:    
	Initials		Name    
-----------------------------------------------------------------    
	CPE				Chaitanya Panduga Eswara    
    RR				Raghu Reddy    
    HG				Harihara Suthan G
	RKV				Ravi Kumar Raju

MODIFICATIONS
	Initials		Date		Modification    
-----------------------------------------------------------------    
	CPE				2014-02-01	Created
	RR				2014-04-08	Modified script to update dbo.Invoice_Participation table Client_Hier_Id column added as a part of
									IP client hier sum enhancement
	HG				2014-07-14	Production issue, Script which used to find @Account_Id_For_Old_Site is modified to look for the other Utility Accounts. ( If it got the supplier Account of the same Account being moved then those Accounts also will be moved to the new Site by default)
	AK				2014-08-06	Modified the query to move the Accounts that have shared supplier Accounts with no Cost Usage data.
	HG				2016-01-07	MAINT-3731, Modified the sproc to update the Old Site's other accounts data source code to CBMS so that after the nightly aggregation data source gets updated correctly at site level and the data goes to RA.
	HG				2016-01-12	MAINT-3613, modified the script to create IP if the new site is not at all associated with the supplier accounts tied up with meter of the utility account.
	RKV             2017-11-22  MAINT - 6331,Modified the script to update the Client_Hier_Id column in CU_Exception_Denorm Table
	RKV				2018-06-07	MAINT-7562 - Taking Commodities for the given account and applied the delete statement.	
******/


CREATE PROCEDURE [dbo].[Move_Account_To_New_Site]
    (
        @Account_Id INT
        , @Old_Client_Hier_Id INT
        , @New_Client_Hier_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #CU_Service_Months
             (
                 Service_Month DATE
             );

        DECLARE @Supplier_Account TABLE
              (
                  Account_Id INT
                  , Is_Shared_Supplier BIT DEFAULT (0)
                  , Has_Other_Utility_Account_Of_Old_Site BIT DEFAULT (0)
                  , Is_Supplier_Associated_With_New_Site BIT DEFAULT (0)
              );

        DECLARE
            @Old_Site_Id INT
            , @New_Site_Id INT
            --, @Account_Id_For_Old_Site INT
            , @New_Address_Id INT
            , @CBMS_Data_Source_Id INT
            , @DE_Data_Source_Id INT
            , @Commodity_Id INT
            , @Client_Name VARCHAR(200)
            , @Application_Name VARCHAR(30) = 'Move Account'
            , @Audit_Function SMALLINT = 1
            , @Current_Ts DATETIME = GETDATE()
            , @Lookup_Value XML
            , @User_Name VARCHAR(81)
            , @Min_Id INT = 1
            , @Max_Id INT
            , @Account_Loop_Id INT;

        DECLARE @Sites_Ipq TABLE
              (
                  ID INT IDENTITY(1, 1) PRIMARY KEY
                  , Account_Id INT
              );

        SELECT
            @Old_Site_Id = MAX(CASE WHEN ch.Client_Hier_Id = @Old_Client_Hier_Id THEN ch.Site_Id
                               END)
            , @New_Site_Id = MAX(CASE WHEN ch.Client_Hier_Id = @New_Client_Hier_Id THEN ch.Site_Id
                                 END)
            , @Client_Name = MAX(ch.Client_Name)
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Client_Hier_Id IN ( @Old_Client_Hier_Id, @New_Client_Hier_Id );

        SELECT
            @New_Address_Id = s.PRIMARY_ADDRESS_ID
        FROM
            dbo.SITE s
        WHERE
            s.SITE_ID = @New_Site_Id;

        SELECT
            @User_Name = ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USER_INFO_ID = @User_Info_Id;

        SELECT
            @CBMS_Data_Source_Id = MAX(CASE WHEN c.Code_Value = 'CBMS' THEN c.Code_Id
                                       END)
            , @DE_Data_Source_Id = MAX(CASE WHEN c.Code_Value = 'DE' THEN c.Code_Id
                                       END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'DataSource'
            AND c.Code_Value IN ( 'CBMS', 'DE' );


        DECLARE @Commodities_old_site_Other_Than_Moving_Account TABLE
              (
                  Commodity_Id INT
              );
        DECLARE @Commodities_Moving_Account TABLE
              (
                  Commodity_Id INT
              );

        DECLARE @Account_Id_For_Old_Site TABLE
              (
                  Account_Id INT
              );

        INSERT INTO @Commodities_old_site_Other_Than_Moving_Account
             (
                 Commodity_Id
             )
        SELECT
            cha.Commodity_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Client_Hier_Id = @Old_Client_Hier_Id
            AND cha.Account_Id <> @Account_Id
        GROUP BY
            cha.Commodity_Id;


        INSERT INTO @Commodities_Moving_Account
             (
                 Commodity_Id
             )
        SELECT
            cha.Commodity_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            cha.Commodity_Id;


        --To Get the Supplier Account for the Utility Accounts       
        INSERT INTO @Supplier_Account
             (
                 Account_Id
                 , Is_Shared_Supplier
                 , Has_Other_Utility_Account_Of_Old_Site
                 , Is_Supplier_Associated_With_New_Site
             )
        SELECT
            supp_acc.Account_Id
            , CASE WHEN COUNT(DISTINCT supp_acc.Client_Hier_Id) > 1 THEN 1
                  ELSE 0
              END AS Is_Shared_Supplier
            , MAX(CASE WHEN util_acc.Account_Id IS NOT NULL THEN 1
                      ELSE 0
                  END)
            , MAX(CASE WHEN supp_acc.Client_Hier_Id = @New_Client_Hier_Id THEN 1
                      ELSE 0
                  END)
        FROM
            dbo.METER m
            INNER JOIN Core.Client_Hier_Account scha
                ON m.METER_ID = scha.Meter_Id
            INNER JOIN Core.Client_Hier_Account supp_acc
                ON scha.Account_Id = supp_acc.Account_Id
            LEFT OUTER JOIN Core.Client_Hier_Account util_acc
                ON util_acc.Meter_Id = supp_acc.Meter_Id
                   AND  util_acc.Account_Type = 'Utility'
                   AND  util_acc.Account_Id <> @Account_Id
                   AND  util_acc.Client_Hier_Id = @Old_Client_Hier_Id
        WHERE
            scha.Account_Type = 'Supplier'
            AND m.ACCOUNT_ID = @Account_Id
        GROUP BY
            supp_acc.Account_Id;

        -- Get an Account under the old Site with the same service    
        INSERT INTO @Account_Id_For_Old_Site
             (
                 Account_Id
             )
        SELECT
            MAX(cha.Account_Id)
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN @Commodities_Moving_Account cma
                ON cma.Commodity_Id = cha.Commodity_Id
        WHERE
            cha.Client_Hier_Id = @Old_Client_Hier_Id
            AND cha.Account_Type = 'Utility'
            AND cha.Account_Id <> @Account_Id
        GROUP BY
            cha.Commodity_Id;


        SET @Lookup_Value = (   SELECT
                                    @Old_Site_Id Old_Site_Id
                                    , @New_Site_Id New_Site_Id
                                    , @Account_Id Account_Id
                                FOR XML RAW);

        -- Get the service months for which we have cost and usage data for the Account    
        -- These are the service months for which the Site aggregation should be run    
        INSERT  #CU_Service_Months
             (
                 Service_Month
             )
        SELECT
            cuad.Service_Month
        FROM
            dbo.Cost_Usage_Account_Dtl cuad
        WHERE
            cuad.ACCOUNT_ID = @Account_Id
        GROUP BY
            cuad.Service_Month;

        BEGIN

            BEGIN TRY
                BEGIN TRAN;

                --Utility Account cu    
                UPDATE
                    dbo.Cost_Usage_Account_Dtl
                SET
                    Client_Hier_ID = @New_Client_Hier_Id
                    , Data_Source_Cd = CASE WHEN Data_Source_Cd = @DE_Data_Source_Id THEN @CBMS_Data_Source_Id
                                           ELSE Data_Source_Cd
                                       END
                WHERE
                    ACCOUNT_ID = @Account_Id
                    AND Client_Hier_ID = @Old_Client_Hier_Id;

                --Calendarization    
                UPDATE
                    dbo.Bucket_Account_Interval_Dtl
                SET
                    Client_Hier_Id = @New_Client_Hier_Id
                    , Data_Source_Cd = CASE WHEN Data_Source_Cd = @DE_Data_Source_Id THEN @CBMS_Data_Source_Id
                                           ELSE Data_Source_Cd
                                       END
                WHERE
                    Account_Id = @Account_Id
                    AND Client_Hier_Id = @Old_Client_Hier_Id;

                --Supplier Account cu    
                UPDATE
                    cu
                SET
                    cu.Client_Hier_ID = @New_Client_Hier_Id
                    , cu.Data_Source_Cd = CASE WHEN cu.Data_Source_Cd = @DE_Data_Source_Id THEN @CBMS_Data_Source_Id
                                              ELSE cu.Data_Source_Cd
                                          END
                FROM
                    dbo.Cost_Usage_Account_Dtl cu
                    INNER JOIN Core.Client_Hier_Account ca
                        ON ca.Account_Id = cu.ACCOUNT_ID
                    INNER JOIN dbo.METER m
                        ON m.METER_ID = ca.Meter_Id
                WHERE
                    m.ACCOUNT_ID = @Account_Id
                    AND cu.Client_Hier_ID = @Old_Client_Hier_Id
                    AND ca.Account_Type = 'Supplier';

                -- Update the Old site's Other accounts data source code to CBMS so that the site levels will Datasource code will be updated as CBMS and the data transfer will pickup them up and put it in DVDEHub

                UPDATE
                    cuad
                SET
                    cuad.Data_Source_Cd = @CBMS_Data_Source_Id
                FROM
                    dbo.Cost_Usage_Account_Dtl cuad
                WHERE
                    cuad.Client_Hier_ID = @Old_Client_Hier_Id
                    AND cuad.Data_Source_Cd = @DE_Data_Source_Id;

                UPDATE
                    cuad
                SET
                    cuad.Data_Source_Cd = @CBMS_Data_Source_Id
                FROM
                    dbo.Bucket_Account_Interval_Dtl cuad
                WHERE
                    cuad.Client_Hier_Id = @Old_Client_Hier_Id
                    AND cuad.Data_Source_Cd = @DE_Data_Source_Id;

                --Supplier Account calendarization    
                UPDATE
                    baid
                SET
                    baid.Client_Hier_Id = @New_Client_Hier_Id
                    , baid.Data_Source_Cd = CASE WHEN baid.Data_Source_Cd = @DE_Data_Source_Id THEN
                                                     @CBMS_Data_Source_Id
                                                ELSE baid.Data_Source_Cd
                                            END
                FROM
                    dbo.Bucket_Account_Interval_Dtl baid
                    INNER JOIN Core.Client_Hier_Account ca
                        ON ca.Account_Id = baid.Account_Id
                    INNER JOIN dbo.METER m
                        ON m.METER_ID = ca.Meter_Id
                WHERE
                    m.ACCOUNT_ID = @Account_Id
                    AND baid.Client_Hier_Id = @Old_Client_Hier_Id
                    AND ca.Account_Type = 'Supplier';

                --Invoice Participation Utility    
                UPDATE
                    dbo.INVOICE_PARTICIPATION
                SET
                    SITE_ID = @New_Site_Id
                    , Client_Hier_Id = @New_Client_Hier_Id
                WHERE
                    ACCOUNT_ID = @Account_Id
                    AND SITE_ID = @Old_Site_Id
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.INVOICE_PARTICIPATION ip
                                       WHERE
                                            ip.SITE_ID = @New_Site_Id
                                            AND ip.ACCOUNT_ID = @Account_Id);


                --Cu_Exception_Denorm Utility
                UPDATE
                    dbo.CU_EXCEPTION_DENORM
                SET
                    Client_Hier_ID = @New_Client_Hier_Id
                WHERE
                    Account_ID = @Account_Id
                    AND Client_Hier_ID = @Old_Client_Hier_Id;

                -- Invoice Participation Queue Utility    
                UPDATE
                    dbo.INVOICE_PARTICIPATION_QUEUE
                SET
                    SITE_ID = @New_Site_Id
                WHERE
                    ACCOUNT_ID = @Account_Id
                    AND INVOICE_PARTICIPATION_BATCH_ID IS NULL
                    AND SITE_ID = @Old_Site_Id;


                -- Supplier Account changes for Invoice Participation and Invoice Participation Queue  


                /*  
   Use Case 1 Description:  
   Utility Account(Acc1) to be moved from SiteA to SiteB, and Acc1 has shared supplier Accounts(supp1), that spans across SiteA and SiteB.  
   If Supplier Accounts are not associated to any other Utility Accounts other than Acc1 for SiteA,   
   then Delete the IP records for supp1 and SiteA Combination.  
*/
                DELETE
                ip
                FROM
                    @Supplier_Account supp_acc
                    INNER JOIN dbo.INVOICE_PARTICIPATION ip
                        ON supp_acc.Account_Id = ip.ACCOUNT_ID
                WHERE
                    ip.Client_Hier_Id = @Old_Client_Hier_Id
                    AND supp_acc.Is_Shared_Supplier = 1 -- ( Supplier Account associated with other Site)
                    AND supp_acc.Has_Other_Utility_Account_Of_Old_Site = 0; -- No Other Utility Account of old Site is associated with the supplier Account , thus it can be deleted.


                DELETE
                ced
                FROM
                    @Supplier_Account supp_acc
                    INNER JOIN dbo.CU_EXCEPTION_DENORM ced
                        ON supp_acc.Account_Id = ced.Account_ID
                WHERE
                    ced.Client_Hier_ID = @Old_Client_Hier_Id
                    AND supp_acc.Is_Shared_Supplier = 1
                    AND supp_acc.Has_Other_Utility_Account_Of_Old_Site = 0;



                /*  

   Use Case 2 Description:  
   Utility Account(Acc1) to be moved from SiteA to SiteB, and Acc1 has shared supplier Accounts(supp1), that does not belong to SiteB.  
   If Supplier Accounts are not associated to any other Utility Accounts other than Acc1 for SiteA,   
   then Update the IP records for supp1, SiteA combination to supp1, SiteB combination  

*/

                DECLARE @Supplier_Accounts_Updated_To_New_Site TABLE
                      (
                          Account_Id INT
                      );

                UPDATE
                    ip
                SET
                    ip.Client_Hier_Id = @New_Client_Hier_Id
                    , ip.SITE_ID = @New_Site_Id
                OUTPUT
                    (INSERTED.ACCOUNT_ID)
                INTO @Supplier_Accounts_Updated_To_New_Site (Account_Id)
                FROM
                    @Supplier_Account supp_acc
                    INNER JOIN dbo.INVOICE_PARTICIPATION ip
                        ON supp_acc.Account_Id = ip.ACCOUNT_ID
                WHERE
                    ip.Client_Hier_Id = @Old_Client_Hier_Id
                    AND supp_acc.Is_Shared_Supplier = 0 -- ( Supplier Account belongs only the old Site )
                    AND supp_acc.Has_Other_Utility_Account_Of_Old_Site = 0; -- ( And no other utility Account of the Site associated with that supplier Account)

                UPDATE
                    ipq
                SET
                    ipq.SITE_ID = @New_Site_Id
                OUTPUT
                    (INSERTED.ACCOUNT_ID)
                INTO @Supplier_Accounts_Updated_To_New_Site (Account_Id)
                FROM
                    @Supplier_Account supp_acc
                    INNER JOIN dbo.INVOICE_PARTICIPATION_QUEUE ipq
                        ON supp_acc.Account_Id = ipq.ACCOUNT_ID
                WHERE
                    ipq.INVOICE_PARTICIPATION_BATCH_ID IS NULL
                    AND ipq.SITE_ID = @Old_Site_Id
                    AND supp_acc.Is_Shared_Supplier = 0 -- ( Supplier Account belongs only the old Site )
                    AND supp_acc.Has_Other_Utility_Account_Of_Old_Site = 0; -- ( And no other utility Account of the Site associated with that supplier Account)



                UPDATE
                    ced
                SET
                    ced.Client_Hier_ID = @New_Client_Hier_Id
                FROM
                    @Supplier_Account supp_acc
                    INNER JOIN dbo.CU_EXCEPTION_DENORM ced
                        ON supp_acc.Account_Id = ced.Account_ID
                WHERE
                    ced.Client_Hier_ID = @Old_Client_Hier_Id
                    AND supp_acc.Is_Shared_Supplier = 0 -- ( Supplier Account belongs only the old Site )
                    AND supp_acc.Has_Other_Utility_Account_Of_Old_Site = 0; -- ( And no other utility Account of the Site associated with that supplier Account)




                /*  
   Use Case 3 Description:  
   Utility Account(Acc1) to be moved from SiteA to SiteB, and Acc1 has shared supplier Accounts(supp1), that does not belong to SiteB.  
   If Supplier Accounts are associated to any other Utility Accounts other than Acc1 for SiteA,   
   then Insert the IPQ records for supp1 to SiteB  
*/

                INSERT INTO @Sites_Ipq
                     (
                         Account_Id
                     )
                SELECT
                    supp_acc.Account_Id
                FROM
                    @Supplier_Account supp_acc
                WHERE
                    supp_acc.Is_Supplier_Associated_With_New_Site = 0
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            @Supplier_Accounts_Updated_To_New_Site upd_sup_acc
                                       WHERE
                                            upd_sup_acc.Account_Id = supp_acc.Account_Id)
                GROUP BY
                    supp_acc.Account_Id;

                SELECT  @Max_Id = MAX(ID)FROM   @Sites_Ipq;
                -- Move_Account_To_New_Site

                WHILE @Min_Id <= @Max_Id
                    BEGIN
                        SELECT  @Account_Loop_Id = Account_Id FROM  @Sites_Ipq WHERE ID = @Min_Id;

                        EXEC dbo.cbmsInvoiceParticipationQueue_Save
                            @MyAccountId = 0
                            , @event_type = '2'
                            , @client_id = NULL
                            , @division_id = NULL
                            , @site_id = @New_Site_Id
                            , @account_id = @Account_Loop_Id
                            , @service_month = NULL;

                        SET @Min_Id = @Min_Id + 1;
                    END;

                --Rate Comparisons    
                UPDATE
                    dbo.RC_RATE_COMPARISON
                SET
                    SITE_ID = @New_Site_Id
                WHERE
                    ACCOUNT_ID = @Account_Id
                    AND SITE_ID = @Old_Site_Id;

                --Meter    
                UPDATE
                    dbo.METER
                SET
                    ADDRESS_ID = @New_Address_Id
                WHERE
                    ACCOUNT_ID = @Account_Id;

                --Account    
                UPDATE
                    dbo.ACCOUNT
                SET
                    SITE_ID = @New_Site_Id
                WHERE
                    ACCOUNT_ID = @Account_Id
                    AND SITE_ID = @Old_Site_Id;



                -- IP Aggregation    
                EXEC dbo.Invoice_Participation_Site_ReAggregate
                    @Account_Id
                    , @Old_Site_Id
                    , @New_Site_Id;

                -- Cost_Usage_Transfer_Queue
                INSERT  Stage.Cost_Usage_Transfer_Queue
                     (
                         Client_Hier_Id
                         , Account_Id
                         , Service_Month
                         , Is_Aggregated_To_Site
                         , Is_Variance_Tested
                     )
                SELECT
                    @New_Client_Hier_Id
                    , @Account_Id
                    , sm.Service_Month
                    , 0
                    , 1
                FROM
                    #CU_Service_Months sm;

                IF EXISTS (SELECT   1 FROM  @Account_Id_For_Old_Site)
                    BEGIN
                        INSERT  Stage.Cost_Usage_Transfer_Queue
                             (
                                 Client_Hier_Id
                                 , Account_Id
                                 , Service_Month
                                 , Is_Aggregated_To_Site
                                 , Is_Variance_Tested
                             )
                        SELECT
                            @Old_Client_Hier_Id
                            , aif.Account_Id
                            , sm.Service_Month
                            , 0
                            , 1
                        FROM
                            @Account_Id_For_Old_Site aif
                            CROSS JOIN #CU_Service_Months sm
                        GROUP BY
                            aif.Account_Id
                            , sm.Service_Month;


                        DELETE
                        bsid
                        FROM
                            dbo.Bucket_Site_Interval_Dtl bsid
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = bsid.Bucket_Master_Id
                            INNER JOIN @Commodities_Moving_Account cma
                                ON bm.Commodity_Id = cma.Commodity_Id
                        WHERE
                            bsid.Client_Hier_Id = @Old_Client_Hier_Id
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    @Commodities_old_site_Other_Than_Moving_Account coso
                                               WHERE
                                                    cma.Commodity_Id = coso.Commodity_Id);



                        DELETE
                        cusd
                        FROM
                            dbo.Cost_Usage_Site_Dtl cusd
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
                            INNER JOIN @Commodities_Moving_Account cma
                                ON bm.Commodity_Id = cma.Commodity_Id
                        WHERE
                            cusd.Client_Hier_Id = @Old_Client_Hier_Id
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    @Commodities_old_site_Other_Than_Moving_Account coso
                                               WHERE
                                                    cma.Commodity_Id = coso.Commodity_Id);

                    END;

                -- If there is no Account of the same service under the old Site, delete the CU data for the commodity from the Site dtl table    
                ELSE
                    BEGIN
                        DELETE
                        cusd
                        FROM
                            dbo.Cost_Usage_Site_Dtl cusd
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
                            INNER JOIN @Commodities_Moving_Account cma
                                ON cma.Commodity_Id = bm.Commodity_Id
                        WHERE
                            cusd.Client_Hier_Id = @Old_Client_Hier_Id;

                        DELETE
                        bsid
                        FROM
                            dbo.Bucket_Site_Interval_Dtl bsid
                            INNER JOIN dbo.Bucket_Master bm
                                ON bm.Bucket_Master_Id = bsid.Bucket_Master_Id
                            INNER JOIN @Commodities_Moving_Account cma
                                ON cma.Commodity_Id = bm.Commodity_Id
                        WHERE
                            bsid.Client_Hier_Id = @Old_Client_Hier_Id;
                    END;

                EXEC dbo.Application_Audit_Log_Ins
                    @Client_Name
                    , @Application_Name
                    , @Audit_Function
                    , 'Account'
                    , @Lookup_Value
                    , @User_Name
                    , @Current_Ts;

                COMMIT;
            END TRY
            BEGIN CATCH
                IF @@TRANCOUNT > 0
                    ROLLBACK;
                EXEC dbo.usp_RethrowError;
            END CATCH;
        END;
        DROP TABLE #CU_Service_Months;

    END;
    ;



GO





GRANT EXECUTE ON  [dbo].[Move_Account_To_New_Site] TO [CBMSApplication]
GO
