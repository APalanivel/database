SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME:  
	dbo.Group_Info_Category_Upd
 
 DESCRIPTION:  It will update data into dbo.Group_Info_Category table for selected group_info_category_id.
 
 INPUT PARAMETERS:  
 Name						DataType	  Default Description  
------------------------------------------------------------  
 @Group_Info_Category_Id	INT
 @Category_Name				VARCHAR(50)
 @Sort_Order				INT
 @App_Menu_Profile_Id		INT
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  BEGIN TRAN
  
	EXEC dbo.Group_Info_Category_Upd 1, 'Dashboard',1,4
  
  ROLLBACK TRAN
  
	SELECT * FROM GROUP_INFO_CATEGORY
	
  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 PNR		Pandarinath
 
 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 PNR	  03/31/2011	Created as part of Proj : DV groups Revamp.
            
******/

CREATE PROCEDURE dbo.Group_Info_Category_Upd
( 
	  @Group_Info_Category_Id	INT
	 ,@Category_Name			VARCHAR(50)
	 ,@Sort_Order				INT
	 ,@App_Menu_Profile_Id		INT
)
AS 
BEGIN

      SET NOCOUNT ON ;

      UPDATE
		dbo.Group_Info_Category
	  SET
		 Category_Name			= @Category_Name
		,Sort_Order				= @Sort_Order
		,APP_MENU_PROFILE_ID	= @App_Menu_Profile_Id
	  WHERE
		 Group_Info_Category_Id = @Group_Info_Category_Id

END
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Category_Upd] TO [CBMSApplication]
GO
