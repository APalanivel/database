SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndex_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_id	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsCPIndex_Get]
	( @clearport_index_id int = null
	)
AS
BEGIN

	   select clearport_index_id
		, clearport_index
	     from clearport_index with (nolock)
	    where clearport_index_id = @clearport_index_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndex_Get] TO [CBMSApplication]
GO
