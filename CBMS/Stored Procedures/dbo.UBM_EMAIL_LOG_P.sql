SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--insert into entity values('UBM_alert_for_Corrupted_Zip_File',666,'UBM_ALERT_CORRUPTED_ZIP',null)  
--insert into entity values('UBM_alert_for_Duplicate_Data_File',666,'UBM_ALERT_DUPLICATE_DATA_FILE',null)  
--insert into entity values('UBM_alert_for_empty_dump',666,'UBM_ALERT_EMPTY_DUMP',null)  
  
CREATE PROCEDURE dbo.UBM_EMAIL_LOG_P
	@ubmName VARCHAR(10),
	@alertName VARCHAR(200),  
	@fromEmailAddress VARCHAR(100),  
	@toEmailAddress VARCHAR(100),  
	@ccEmailAddress VARCHAR(1000),  
	@subject TEXT,
	@content TEXT,
	@generationDate DATETIME
AS

BEGIN

	SET NOCOUNT ON
	
	DECLARE @emailAlertId INT					
	
	SELECT @emailAlertId = entity_id FROM dbo.entity (NOLOCK)
	WHERE entity_name = @alertName
		AND entity_type = 666

	INSERT INTO dbo.UBM_EMAIL_LOG(UBM_ID,
		EMAIL_TYPE_ID,
		FROM_EMAIL_ADDRESS,
		TO_EMAIL_ADDRESS,
		CC_EMAIL_ADDRESS,
		SUBJECT,
		CONTENT,
		GENERATION_DATE)
	SELECT Ubm_ID,
		@emailAlertId,
		@fromEmailAddress,
		@toEmailAddress,
		@ccEmailAddress,
		@subject,
		@content,
		@generationDate
	FROM dbo.Ubm(NOLOCK)
	WHERE Ubm_Name = @ubmname
	
END
GO
GRANT EXECUTE ON  [dbo].[UBM_EMAIL_LOG_P] TO [CBMSApplication]
GO
