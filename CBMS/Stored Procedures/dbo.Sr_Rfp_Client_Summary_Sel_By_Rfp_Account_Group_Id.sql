SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id                  
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default			Description              
---------------------------------------------------------------------
 @SR_RFP_ID				INT
 @SR_RFP_ACCOUNT_ID		INT
 @SR_RFP_BID_GROUP_ID	INT
                                    
 OUTPUT PARAMETERS:                   
                              
 Name				DataType			Default			Description              
---------------------------------------------------------------------
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------
  
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id  1429
  
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 350
   
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 3246
   
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13430
   
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13524
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13137,NULL,10011899
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13137,NULL,10011900
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13137,12103934
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13570
	
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13559
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13560
	
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13556
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13580
	
	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 13751,0,0
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 13751,null,NULL

	EXEC dbo.Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id 12768,0,0
	EXEC dbo.Sr_Rfp_Load_Profile_Sel_By_Rfp_Id_Rfp_Account_Id 12768,null,NULL



  
AUTHOR INITIALS:              
	Initials	Name              
---------------------------------------------------------------------
	RKV			Ravi Kumar Raju
	RR			Raghu Reddy                        
                               
MODIFICATIONS:            
	Initials	Date        Modification            
---------------------------------------------------------------------
	RKV			2016-06-09  Created GCS Phase-5 GCS-1028
	RR			2016-07-21	GCS-1135 Modified to return load profile size in country default UOM for natural gas only
	RR			2018-01-04	SE2017-384 Revertted the above change GCS-1135, total usage actual UOM should be used and returned always and 
							also string "Multiple" should be displayed if there are multiple UOMs
                             
******/ 
CREATE  PROCEDURE [dbo].[Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id]
      ( 
       @SR_RFP_ID INT
      ,@SR_RFP_ACCOUNT_ID INT = NULL
      ,@SR_RFP_BID_GROUP_ID INT = NULL )
AS 
BEGIN 
 
      SET NOCOUNT ON;
 
      DECLARE @Client_Dtls TABLE
            ( 
             Client_Id INT
            ,client_Name VARCHAR(200)
            ,SR_RFP_ACCOUNT_ID INT
            ,SR_RFP_BID_GROUP_ID INT
            ,Account_ID INT
            ,Meter_cnt INT
            ,Is_Bid_Group INT )
 
      DECLARE @Duns_number TABLE
            ( 
             Client_Id INT
            ,SR_RFP_ACCOUNT_ID INT
            ,Credit_number VARCHAR(30) )
     
      DECLARE @Contract_Volume TABLE
            ( 
             Client_Id INT
            ,SR_RFP_ACCOUNT_ID INT
            ,Annual_Contract_Volume DECIMAL(32, 16)
            ,Volume_Uom VARCHAR(200) )
 
      CREATE TABLE #SR_RFP_Account_Id
            ( 
             SR_RFP_Account_Id INT )
      
      DECLARE @Client_Contract_Details TABLE
            ( 
             Client_Id INT
            ,SR_RFP_ACCOUNT_ID INT
            ,Con_Start_Date VARCHAR(25)
            ,Con_End_Date VARCHAR(25) )
      
 
      DECLARE @Client_Contract_Details_Final TABLE
            ( 
             Client_Id INT
            ,SR_RFP_ACCOUNT_ID INT
            ,Con_Start_Date VARCHAR(25)
            ,Con_End_Date VARCHAR(25) )
            
      DECLARE @Account_UOM TABLE
            ( 
             SR_RFP_ACCOUNT_ID INT
            ,Convertion_Id INT )
      
 
      DECLARE
            @Convertion_Id INT
           ,@Convertion_UOM_Cnt INT
           
 
      INSERT      INTO #SR_RFP_Account_Id
                  ( 
                   SR_RFP_Account_Id )
                  SELECT
                        sra.SR_RFP_ACCOUNT_ID
                  FROM
                        dbo.SR_RFP_ACCOUNT sra
                  WHERE
                        sra.SR_RFP_ID = @SR_RFP_ID
                        AND sra.IS_DELETED = 0


-- Getting Client Details
      INSERT      INTO @Client_Dtls
                  ( 
                   Client_Id
                  ,client_Name
                  ,SR_RFP_ACCOUNT_ID
                  ,SR_RFP_BID_GROUP_ID
                  ,Account_ID
                  ,Meter_cnt
                  ,Is_Bid_Group )
                  SELECT
                        ch.Client_Id
                       ,ch.client_Name
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       ,rfpacc.ACCOUNT_ID
                       ,COUNT(sramm.METER_ID)
                       ,CASE WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END
                  FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                        INNER JOIN #SR_RFP_Account_Id srai
                              ON rfpacc.SR_RFP_ACCOUNT_ID = srai.SR_RFP_Account_Id
                        INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                              ON srai.SR_RFP_Account_Id = sramm.SR_RFP_ACCOUNT_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND sramm.METER_ID = cha.Meter_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                  GROUP BY
                        ch.Client_Id
                       ,ch.client_Name
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,rfpacc.ACCOUNT_ID
                       ,rfpacc.SR_RFP_BID_GROUP_ID
                       
                       
                       
-- RFP_ACCOUNT_ID duns numbers
      INSERT      INTO @Duns_number
                  ( 
                   Client_Id
                  ,SR_RFP_ACCOUNT_ID
                  ,Credit_number )
                  SELECT
                        ch.Client_Id
                       ,rfpacc.SR_RFP_ACCOUNT_ID
                       ,CASE WHEN COUNT(DISTINCT s.DUNS_NUMBER) > 1 THEN 'Multiple'
                             ELSE MAX(s.DUNS_NUMBER)
                        END Credit_number
                  FROM
                        @Client_Dtls rfpacc
                        INNER JOIN dbo.SR_RFP_ACCOUNT sra
                              ON sra.SR_RFP_ACCOUNT_ID = rfpacc.SR_RFP_ACCOUNT_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON sra.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON rfpacc.ACCOUNT_ID = cha.Account_Id
                                 AND rfp.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.METER mtr
                              ON cha.Meter_Id = mtr.METER_ID
                        INNER JOIN dbo.SITE s
                              ON ch.Site_Id = s.SITE_ID
                  GROUP BY
                        ch.Client_Id
                       ,rfpacc.SR_RFP_ACCOUNT_ID


 
 

 
 -- Getting Contract Details
 
      INSERT      INTO @Client_Contract_Details
                  ( 
                   Client_Id
                  ,SR_RFP_ACCOUNT_ID
                  ,Con_Start_Date
                  ,Con_End_Date )
                  SELECT
                        cd.Client_Id
                       ,cd.SR_RFP_ACCOUNT_ID
                       ,CAST(srat.FROM_MONTH AS DATE)
                       ,CAST(srat.TO_MONTH AS DATE)
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM srat
                        INNER JOIN @Client_Dtls cd
                              ON srat.SR_ACCOUNT_GROUP_ID = ISNULL(cd.SR_RFP_BID_GROUP_ID, cd.SR_RFP_ACCOUNT_ID)
                                 AND srat.IS_BID_GROUP = cd.Is_Bid_Group
                  GROUP BY
                        cd.Client_Id
                       ,cd.SR_RFP_ACCOUNT_ID
                       ,srat.FROM_MONTH
                       ,srat.TO_MONTH
  
            
      
     
      INSERT      INTO @Client_Contract_Details_Final
                  ( 
                   Client_Id
                  ,SR_RFP_ACCOUNT_ID
                  ,Con_Start_Date
                  ,Con_End_Date )
                  SELECT
                        Client_Id
                       ,SR_RFP_ACCOUNT_ID
                       ,Contract_Start_Date
                       ,Contract_End_Date
                  FROM
                        ( SELECT
                              ccd.Client_Id
                             ,ccd.SR_RFP_ACCOUNT_ID
                             ,CASE WHEN MAX(row_cnt.Start_Date_cnt) = 1 THEN ccd.Con_Start_Date
                                   ELSE 'Multiple'
                              END Contract_Start_Date
                             ,CASE WHEN MAX(row_cnt.End_Date_cnt) = 1 THEN ccd.Con_End_Date
                                   ELSE 'Multiple'
                              END Contract_End_Date
                          FROM
                              @Client_Contract_Details ccd
                              INNER JOIN ( SELECT
                                                ccd1.Client_Id
                                               ,ccd1.SR_RFP_ACCOUNT_ID
                                               ,COUNT(DISTINCT ccd1.Con_Start_Date) Start_Date_cnt
                                               ,COUNT(DISTINCT ccd1.Con_End_Date) End_Date_cnt
                                           FROM
                                                @Client_Contract_Details ccd1
                                           GROUP BY
                                                ccd1.Client_Id
                                               ,ccd1.SR_RFP_ACCOUNT_ID ) row_cnt
                                    ON ccd.Client_Id = row_cnt.Client_Id
                                       AND ccd.SR_RFP_ACCOUNT_ID = row_cnt.SR_RFP_ACCOUNT_ID
                          GROUP BY
                              ccd.Client_Id
                             ,ccd.SR_RFP_ACCOUNT_ID
                             ,ccd.Con_End_Date
                             ,ccd.Con_Start_Date ) x
                  GROUP BY
                        Client_Id
                       ,SR_RFP_ACCOUNT_ID
                       ,Contract_Start_Date
                       ,Contract_End_Date
                       
      INSERT      INTO @Account_UOM
                  ( 
                   SR_RFP_ACCOUNT_ID
                  ,Convertion_Id )
                  SELECT
                        cd.SR_RFP_ACCOUNT_ID
                       ,MIN(srlpd.DETERMINANT_UNIT_TYPE_ID)
					   -- @Convertion_Id = MIN(srlpd.DETERMINANT_UNIT_TYPE_ID)
					   --,@Convertion_UOM_Cnt = COUNT(DISTINCT srlpd.DETERMINANT_UNIT_TYPE_ID)
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN @Client_Dtls cd
                              ON srlps.SR_RFP_ACCOUNT_ID = cd.SR_RFP_ACCOUNT_ID
                  WHERE
                        srlpd.DETERMINANT_NAME = 'Total Usage'
                  GROUP BY
                        cd.SR_RFP_ACCOUNT_ID
            
          

    
 --Getting Contract Volume Details
      INSERT      INTO @Contract_Volume
                  SELECT
                        cd.Client_Id
                       ,cd.SR_RFP_ACCOUNT_ID
                       ,SUM(srldv.LP_VALUE * ccon.CONVERSION_FACTOR)
                       --,CASE WHEN @Convertion_UOM_Cnt > 1 THEN 'Multiple'
                       --      ELSE e.ENTITY_NAME
                       -- END
                       ,e.ENTITY_NAME
                  FROM
                        dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                              ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
                        INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES srldv
                              ON srlpd.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = srldv.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                        INNER JOIN dbo.ENTITY en
                              ON srldv.READING_TYPE_ID = en.ENTITY_ID
                        INNER JOIN @Client_Dtls cd
                              ON srlps.SR_RFP_ACCOUNT_ID = cd.SR_RFP_ACCOUNT_ID
                        LEFT JOIN @Account_UOM au
                              ON srlps.SR_RFP_ACCOUNT_ID = au.SR_RFP_ACCOUNT_ID
                        LEFT OUTER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = au.Convertion_Id
                        LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION ccon
                              ON ccon.BASE_UNIT_ID = srlpd.DETERMINANT_UNIT_TYPE_ID
                                 AND ccon.CONVERTED_UNIT_ID = au.Convertion_Id
                  WHERE
                        srlpd.DETERMINANT_NAME = 'Total Usage'
                        AND en.ENTITY_TYPE = 1031
                        AND ( en.ENTITY_NAME = 'Actual LP Value'
                              OR en.ENTITY_NAME = 'Actual Avg LP Value' )
                        AND srldv.LP_VALUE IS NOT NULL
                  GROUP BY
                        cd.Client_Id
                       ,cd.SR_RFP_ACCOUNT_ID
                       ,e.ENTITY_NAME
                       
    
 -- Final Result
      SELECT
            cd.Client_Name
           ,dn.Credit_number
           ,ccd.Con_Start_Date
           ,ccd.Con_End_Date
           ,NULL Response_Due_Date
           ,cd.Meter_Cnt Number_Of_Supply_Points
           ,CAST(ROUND(cv.Annual_Contract_Volume, 0) AS INT) Annual_Contract_Volume
           ,cv.Volume_Uom
           ,CASE WHEN cd1.Is_Bid_Group = 1 THEN cd1.SR_RFP_BID_GROUP_ID
                 ELSE cd1.SR_RFP_ACCOUNT_ID
            END SR_Account_Group_Id
           ,cd1.Is_Bid_Group
           ,cd1.SR_RFP_ACCOUNT_ID
      FROM
            @Client_Dtls cd1
            LEFT OUTER JOIN @Client_Contract_Details_Final ccd
                  ON ccd.Client_Id = cd1.Client_Id
                     AND ccd.SR_RFP_ACCOUNT_ID = cd1.SR_RFP_ACCOUNT_ID
            LEFT OUTER JOIN @Duns_number dn
                  ON dn.Client_Id = cd1.Client_Id
                     AND dn.SR_RFP_ACCOUNT_ID = cd1.SR_RFP_ACCOUNT_ID
            INNER JOIN ( SELECT
                              Client_Id
                             ,Client_Name
                             ,SR_RFP_ACCOUNT_ID
                             ,SUM(Meter_Cnt) Meter_Cnt
                         FROM
                              @Client_Dtls
                         GROUP BY
                              Client_Id
                             ,Client_Name
                             ,SR_RFP_ACCOUNT_ID ) cd
                  ON cd.Client_Id = cd1.Client_Id
                     AND cd.SR_RFP_ACCOUNT_ID = cd1.SR_RFP_ACCOUNT_ID
            LEFT JOIN @Contract_Volume cv
                  ON cv.Client_Id = cd1.Client_Id
                     AND cv.SR_RFP_ACCOUNT_ID = cd1.SR_RFP_ACCOUNT_ID
      GROUP BY
            cd.Client_Name
           ,ccd.Con_Start_Date
           ,ccd.Con_End_Date
           ,cd.Meter_Cnt
           ,cv.Annual_Contract_Volume
           ,cv.Volume_Uom
           ,dn.Credit_number
           ,cd1.SR_RFP_BID_GROUP_ID
           ,cd1.Is_Bid_Group
           ,cd1.SR_RFP_ACCOUNT_ID


      DROP TABLE #SR_RFP_Account_Id


END






;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Client_Summary_Sel_By_Rfp_Account_Group_Id] TO [CBMSApplication]
GO
