SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                
/******                        
 NAME: dbo.Cu_Invoice_Upd_Is_Duplicate_By_InvoiceId            
                        
 DESCRIPTION:                        
			To Update  Is_Duplicate                     
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Updated_By_Id                INT              
 @CU_Invoice_Id                INT     
 @Is_Duplicate				   INT				0                    
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

BEGIN TRAN
Select IS_DUPLICATE from dbo.CU_INVOICE where CU_Invoice_Id = 2754
 EXEC [dbo].Cu_Invoice_Upd_Is_Duplicate_By_InvoiceId 
      @Updated_By_Id = 49
     ,@CU_Invoice_Id = 2754
Select IS_DUPLICATE from dbo.CU_INVOICE where CU_Invoice_Id = 2754     
ROLLBACK     
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-04-28       Created,For Project Operations Enhancement (4.1.3.1	Backing Out / Routing of Invoices).             
                       
******/ 
CREATE PROCEDURE dbo.Cu_Invoice_Upd_Is_Duplicate_By_InvoiceId
      ( 
       @Updated_By_Id INT
      ,@CU_Invoice_Id INT
      ,@Is_Duplicate BIT = 0 )
AS 
BEGIN  
  
      SET NOCOUNT ON  
      BEGIN  
  
            UPDATE
                  dbo.CU_INVOICE
            SET   
                  IS_DUPLICATE = @Is_Duplicate
                 ,UPDATED_BY_ID = @Updated_By_Id
                 ,UPDATED_DATE = GETDATE()
            WHERE
                  CU_INVOICE_ID = @CU_Invoice_Id

      END
END
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Upd_Is_Duplicate_By_InvoiceId] TO [CBMSApplication]
GO
