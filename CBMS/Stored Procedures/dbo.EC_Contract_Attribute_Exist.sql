SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Exist       
              
Description:              
			This sproc checks if the EC_Contract_Attribute_Name is already present for the Given stae and commodity.      
            If already exists then return 1 else 0.                
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@EC_Contract_Attribute_Name			NVARCHAR(255)
    @EC_Contract_Attribute_Id			INT					 NULL
    @Commodity_Id						INT
    @State_Id							INT        
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Contract_Attribute_Exist 'Test_AS400_Inserted',Null,1,2  
   
   Exec dbo.EC_Contract_Attribute_Exist 'Test_AS400_Inserted',Null,100,200         

   Exec dbo.EC_Contract_Attribute_Exist 'Test_AS400_Inserted',10,1,2    
   
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 

CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Exist]
      ( 
       @EC_Contract_Attribute_Name NVARCHAR(255)
      ,@EC_Contract_Attribute_Id INT = NULL
      ,@State_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
            
      SELECT
            eca.EC_Contract_Attribute_Id
      FROM
            dbo.EC_Contract_Attribute eca
      WHERE
            eca.State_Id = @State_Id
            AND eca.Commodity_Id = @Commodity_Id
            AND ( ( @EC_Contract_Attribute_Id IS NULL
                    AND eca.EC_Contract_Attribute_Name = @EC_Contract_Attribute_Name )
                  OR ( eca.EC_Contract_Attribute_Id != @EC_Contract_Attribute_Id
                       AND eca.EC_Contract_Attribute_Name = @EC_Contract_Attribute_Name ) )
                 
END

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Exist] TO [CBMSApplication]
GO
