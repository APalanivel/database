SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******      
  
  
NAME:      
dbo.Report_DE_PAM_CBMS_Vendor_Data    
  
DESCRIPTION:      
Runs against the reptmgr database. Users load PAM vendor list to REPTMGR database using 
a tool that picks up data from a file on the F drive.  Users pick vendor names from a vendor list provided
by a dropped excel file.  data is stored in the PAM_Vendor_Status table 
then a query is run tying that data to the data in cbms to show account/meter information for those vendors


INPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
 
  
            
OUTPUT PARAMETERS:      
Name   DataType Default Description      
------------------------------------------------------------      
USAGE EXAMPLES:      
------------------------------------------------------------    
  
EXEC dbo.Report_DE_PAM_CBMS_Vendor_Data  
  
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
LEC Lynn Cox      
  
MODIFICATIONS       
Initials	Date		Modification      
------------------------------------------------------------      
LEC		 2016-06-09     Created
  
******/
CREATE PROCEDURE [dbo].[Report_DE_PAM_CBMS_Vendor_Data]
AS
BEGIN    

      SET NOCOUNT ON;     

      SELECT DISTINCT
            ch.Client_Name [Client Name]
           ,ch.Site_name [Site Name]
           ,ch.City
           ,ca.Meter_State_Name [State]
           ,ch.Country_Name [Country]
           ,ca.Account_Vendor_Name [Vendor]
           ,ca.Account_Type [Vendor Type]
           ,ca.Display_Account_Number [Account]
           ,com.Commodity_Name [Commodity]
           ,ca.Meter_Number [Meter]
           ,ca.Rate_Name [Rate]
           ,[Vendor Status] = CASE WHEN vs.Is_PAM_Vendor = 1 THEN 'PAM'
                                   ELSE 'Verified Non-PAM'
                              END
      FROM
            Core.Client_Hier ch
            JOIN Core.Client_Hier_Account ca
                  ON ca.Client_Hier_Id = ch.Client_Hier_Id
            JOIN ENTITY e
                  ON e.ENTITY_ID = ch.Client_Type_Id
            JOIN Commodity com
                  ON com.Commodity_Id = ca.Commodity_Id
            JOIN REPTMGR.dbo.PAM_Vendor_Status vs
                  ON vs.[Vendor_Id] = ca.Account_Vendor_Id
                     AND vs.[Vendor_State_Name] = ca.Meter_State_Name
                     AND vs.[Vendor_Type] = ca.Account_Type
      WHERE
            ch.Client_Not_Managed = 0
            AND ch.Site_Id != 0
            AND ch.Site_Not_Managed = 0
            AND ca.Account_Not_Managed = 0
            AND e.ENTITY_NAME != 'demo'-- All Demo type client should be excluded from this report
            AND ch.Client_Name != 'blackstone'; -- Blackstone is an aggregate client and should be excluded to avoid duplicated site data
  
  
END
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_PAM_CBMS_Vendor_Data] TO [CBMSApplication]
GO
