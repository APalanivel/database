SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.cbmsRecalcHeader_Save              
                          
 DESCRIPTION:                          
   To Insert into recalc_header table.                          
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType			Default       Description          
---------------------------------------------------------------------------------------------------------------        
 @MyAccountId				INT  
,@cu_invoice_id				INT  
,@account_id				INT  
,@contract_id				INT  
,@grand_total				DECIMAL(32, 16)  
,@post_data					VARCHAR(400)		NULL  
,@recalc_xml				VARCHAR(7000)		NULL  
,@Updated_User_Id			INT					NULL                       
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
       BEGIN TRAN  
         
       EXEC dbo.cbmsRecalcHeader_Save @MyAccountId=49,@cu_invoice_id=124486,@account_id=5800,@contract_id=14050,@grand_total=178879.6800000000000000,  
       @post_data='ServiceMonth=8/1/2005&AccountId=5800&A1=7/31/2005&A2=8/31/2005&A3=32&A4=28519.500000000000000000000000&',  
       @recalc_xml='<?xml version="1.0" encoding="UTF-8"?><recalc_results contract_id="14050" account_id="5800" service_month="8/1/2005"><contract_determinants><determinant type="Normal" display_id="A1" name="From Date" value="07/31/2005" bucket_type_id="" unit_of_measure_type_id="" /><determinant type="Normal" display_id="A2" name="To Date" value="08/31/2005" bucket_type_id="" unit_of_measure_type_id="" /><determinant type="Normal" display_id="A3" name="Billing Days" value="32" bucket_type_id="" unit_of_measure_type_id="" /><determinant type="Normal" display_id="A4" name="Total Volume" value="28519.5" bucket_type_id="74" unit_of_measure_type_id="25" /></contract_determinants><contract_charges><charge type="Baseload" display_id="B1" name="Base Load" unit_of_measure_type_id="25" expected_volume="23940.0" actual_volume="23940.0" price="7.647" calculation_type_id="0" actual_cost="183,069.18" /><charge type="BaseloadAdditionalCharge" display_id="E10" name="Basis1" unit_of_measure_type_id="25" expected_volume="23940.0" actual_volume="23940.0" price="-0.175" calculation_type_id="99" actual_cost="-4,189.5" /><charge type="Swing" display_id="B2" name="Swing Load" unit_of_measure_type_id="20" expected_volume="" actual_volume="4579.5" price="0" calculation_type_id="0" actual_cost="0" /><charge type="SwingAdditionalCharge" display_id="E7" name="Decremental Adder" unit_of_measure_type_id="20" expected_volume="" actual_volume="4579.5" price="0" calculation_type_id="101" actual_cost="0" /><charge type="Cashout" display_id="B3" name="Cash Out" unit_of_measure_type_id="20" expected_volume="" actual_volume="0.0" price="0" calculation_type_id="0" actual_cost="0" /><charge type="CashoutAdditionalCharge" display_id="E9" name="Decremental Adder" unit_of_measure_type_id="20" e





xpected_volume="" actual_volume="0.0" price="0" calculation_type_id="101" actual_cost="0" /></contract_charges><other_charges></other_charges><physical_hedges></physical_hedges><spot_purchases></spot_purchases><total_expected_cost>178,879.68</total_expect






ed_cost></recalc_results>'  
       ,@Updated_User_Id=49  
         
       ROLLBACK     
                         
 AUTHOR INITIALS:          
         
 Initials				Name          
---------------------------------------------------------------------------------------------------------------                        
 SP						Sandeep Pigilam 
 RKV					Ravi Kumar Vegesna     
 NR						Narayana Reddy      
       
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 SP                    2014-07-31      Comments Header Added.  
										Data Operations Enhancement Phase III,Added @Updated_User_Id as Optional Input Parameter.  
										AND removed internal call of cbmsRecalcHeader_Get by replacing with select statement.Used SCOPE_IDENTITY() instead of @@IDENTITY     
 RKV				   2015-10-30		Added two parameters Commodity_id and Recalc_Type_code as part of AS400-PII						
											Adjusted the data type of Recalc_Xml column to max 
 RKV                   2016-10-17       MAINT-4394,Change the Parameter @grand_total to optional parameter.
 HG					   2017-10-27		SE2017-263 - Modified to merge the records as the reclacl header will not be deleted when reponse charges are locked.
 NR					   2018-11-26		Data2.0 -  Added @Recalc_Failure_Desc parameter. 
******/
CREATE PROCEDURE [dbo].[cbmsRecalcHeader_Save]
    (
        @MyAccountId INT
        , @cu_invoice_id INT
        , @account_id INT
        , @contract_id INT
        , @grand_total DECIMAL(32, 16) = NULL
        , @post_data VARCHAR(400) = NULL
        , @recalc_xml VARCHAR(MAX) = NULL
        , @Updated_User_Id INT = NULL
        , @Commodity_Id INT = -1
        , @Recalc_Type_Cd INT = NULL
        , @Recalc_Failure_Desc NVARCHAR(MAX) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @this_id INT
            , @date_performed DATETIME = GETDATE();

        MERGE INTO dbo.RECALC_HEADER tgt
        USING (   SELECT
                        @cu_invoice_id AS Cu_invoice_Id
                        , @account_id AS Account_Id
                        , @contract_id AS Contract_Id
                        , @grand_total AS Grand_Total
                        , @date_performed AS Date_Performed
                        , @post_data AS Post_Data
                        , @recalc_xml AS Recalc_xml
                        , @Updated_User_Id AS Updated_User_Id
                        , @Commodity_Id AS Commodity_Id
                        , @Recalc_Failure_Desc AS Recalc_Failure_Desc) src
        ON src.Cu_invoice_Id = tgt.CU_INVOICE_ID
           AND  src.Account_Id = tgt.ACCOUNT_ID
           AND  src.Commodity_Id = tgt.Commodity_Id
        WHEN MATCHED THEN UPDATE SET
                              tgt.POST_DATA = src.Post_Data
                              , tgt.RECALC_XML = src.Recalc_xml
                              , tgt.GRAND_TOTAL = src.Grand_Total
                              , tgt.DATE_PERFORMED = src.Date_Performed
                              , tgt.Last_Change_Ts = GETDATE()
                              , tgt.Recalc_Failure_Desc = src.Recalc_Failure_Desc
        WHEN NOT MATCHED THEN INSERT (CU_INVOICE_ID
                                      , ACCOUNT_ID
                                      , CONTRACT_ID
                                      , GRAND_TOTAL
                                      , DATE_PERFORMED
                                      , POST_DATA
                                      , RECALC_XML
                                      , Updated_User_Id
                                      , Commodity_Id
                                      , Recalc_Failure_Desc)
                              VALUES
                                  (src.Cu_invoice_Id
                                   , src.Account_Id
                                   , src.Contract_Id
                                   , src.Grand_Total
                                   , src.Date_Performed
                                   , src.Post_Data
                                   , src.Recalc_xml
                                   , src.Updated_User_Id
                                   , src.Commodity_Id
                                   , src.Recalc_Failure_Desc);

        SELECT
            rh.RECALC_HEADER_ID AS recalc_header_id
            , @cu_invoice_id AS cu_invoice_id
            , @account_id AS account_id
            , @contract_id AS contract_id
            , @date_performed AS date_performed
            , @grand_total AS grand_total
        FROM
            dbo.RECALC_HEADER rh
        WHERE
            rh.CU_INVOICE_ID = @cu_invoice_id
            AND rh.ACCOUNT_ID = @account_id
            AND rh.Commodity_Id = @Commodity_Id;

    END;

    ;


    ;

GO




GRANT EXECUTE ON  [dbo].[cbmsRecalcHeader_Save] TO [CBMSApplication]
GO
