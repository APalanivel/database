SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
                    
NAME: Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id      
      
DESCRIPTION:      
      
 To get the distinct invoice billing start and end date for the given invoice and account      
      
INPUT PARAMETERS:      
NAME			DATATYPE	DEFAULT  DESCRIPTION
------------------------------------------------------------      
@Cu_Invoice_Id  INT
@Account_Id		INT

OUTPUT PARAMETERS:
NAME     DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

    EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id = 6618163
     ,@Account_Id = 441747

    EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id = 6618166
     ,@Account_Id = 178680

    EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id = 16038748
     ,@Account_Id = 655824

    EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id = 12272957
     ,@Account_Id = 54823

    EXEC dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id = 9558391
     ,@Account_Id = 489631

    SELECT TOP 10 * FROM CU_INVOICE_SERVICE_MONTH WHERE YEAR(SERVICE_MONTH) = 2012

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PKY			Pavan K Yadalam

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------   
PKY			2013-03-14	Created for Calendarization requirement

*/      
 
CREATE PROCEDURE dbo.Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id
      @Cu_Invoice_Id INT
     ,@Account_Id INT = NULL
AS 
BEGIN
 
      SET NOCOUNT ON      

      DECLARE
            @Billing_Begin_Dt AS DATE
           ,@Billing_End_Dt AS DATE      

      SELECT
            @Billing_Begin_Dt = MIN(sm.Begin_Dt)
           ,@Billing_End_Dt = MAX(sm.End_Dt)
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH sm
      WHERE
            sm.Account_ID = @Account_Id
            AND sm.CU_INVOICE_ID = @Cu_Invoice_Id

      SET @Billing_Begin_Dt = CONVERT(VARCHAR(10), DATEADD(dd, -( DAY(@Billing_Begin_Dt) - 1 ), @Billing_Begin_Dt), 120)
      SET @Billing_End_Dt = DATEADD(dd, -1, DATEADD(mm, 1, CONVERT(VARCHAR(10), DATEADD(dd, -( DAY(@Billing_End_Dt) - 1 ), @Billing_End_Dt), 120)))

      SELECT
            sm.Begin_Dt
           ,sm.End_Dt
           ,sm.CU_INVOICE_ID
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH sm
            INNER JOIN dbo.CU_INVOICE ci
                  ON sm.CU_INVOICE_ID = ci.CU_INVOICE_ID
      WHERE
            ( sm.Account_ID = @Account_Id )
            AND ( ( sm.Begin_Dt BETWEEN @Billing_Begin_Dt
                                AND     @Billing_End_Dt )
                  OR ( sm.End_Dt BETWEEN @Billing_Begin_Dt
                                 AND     @Billing_End_Dt ) )
            AND ( ci.IS_REPORTED = 1
                  OR ci.CU_INVOICE_ID = @Cu_Invoice_Id )
      GROUP BY
            sm.Begin_Dt
           ,sm.End_Dt
           ,sm.CU_INVOICE_ID

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Cu_Invoice_Begin_Dt_End_Dt_By_Invoice_Id_Account_Id] TO [CBMSApplication]
GO
