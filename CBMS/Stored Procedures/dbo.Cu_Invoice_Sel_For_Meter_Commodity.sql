
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: DBO.Cu_Invoice_Sel_For_Meter_Commodity  
     
DESCRIPTION: 

	To Get Invoice Id which associated with the given Meter Id and the commodity of the meter.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Meter_Id		INT
	@StartIndex		INT			1			
	@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC Cu_Invoice_Sel_For_Meter_Commodity  62461,1,10
	EXEC Cu_Invoice_Sel_For_Meter_Commodity 26245

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	RR			Raghu Reddy

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			04-June-10		CREATED
	PNR			17-AUG-2010		Cu Invoice Id selected from Union of CU_INVOICE_DETERMINANT and CU_INVOICE_CHARGE tables
	RR			2013-10-15		MAINT-2260 Modified the script to return only invoices that are having charges or determinants 
								with the meter's utility account
	
*/

CREATE PROCEDURE dbo.Cu_Invoice_Sel_For_Meter_Commodity
      (
       @Meter_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS
BEGIN

      SET NOCOUNT ON;
	
      DECLARE @Invoice TABLE
            (
             Cu_Invoice_id INT
            ,Service_Month DATETIME
            ,Begin_Date DATETIME
            ,End_Date DATETIME
            ,Is_Processed BIT
            ,Is_Duplicate BIT
            ,Commodity_Id INT
            ,ACCOUNT_ID INT )

      INSERT      INTO @Invoice
                  SELECT
                        ci.CU_INVOICE_ID
                       ,cuism.Service_Month
                       ,cuism.Begin_Dt
                       ,cuism.End_Dt
                       ,ci.IS_PROCESSED
                       ,ci.IS_DUPLICATE
                       ,r.COMMODITY_TYPE_ID
                       ,m.ACCOUNT_ID
                  FROM
                        dbo.METER m
                        JOIN dbo.RATE r
                              ON m.RATE_ID = r.RATE_ID
                        JOIN dbo.CU_INVOICE_SERVICE_MONTH cuism
                              ON cuism.ACCOUNT_ID = m.ACCOUNT_ID
                        JOIN dbo.CU_INVOICE ci
                              ON cuism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                  WHERE
                        m.METER_ID = @Meter_Id;

      WITH  Cte_Determinant_Charge_Invoice
              AS ( SELECT
                        inv.Cu_Invoice_id
                   FROM
                        dbo.CU_INVOICE_DETERMINANT det
                        JOIN @Invoice inv
                              ON det.CU_INVOICE_ID = inv.Cu_Invoice_id
                                 AND inv.Commodity_Id = det.COMMODITY_TYPE_ID
                        JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT invdet
                              ON det.CU_INVOICE_DETERMINANT_ID = invdet.CU_INVOICE_DETERMINANT_ID
                                 AND inv.ACCOUNT_ID = invdet.ACCOUNT_ID
                   UNION
                   SELECT
                        inv.Cu_Invoice_id
                   FROM
                        dbo.CU_INVOICE_CHARGE chg
                        JOIN @Invoice inv
                              ON chg.CU_INVOICE_ID = inv.Cu_Invoice_id
                                 AND inv.Commodity_Id = chg.COMMODITY_TYPE_ID
                        JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT invchg
                              ON chg.CU_INVOICE_CHARGE_ID = invchg.CU_INVOICE_CHARGE_ID
                                 AND inv.ACCOUNT_ID = invchg.ACCOUNT_ID ),
            Cte_Determinant_Charge_Invoice_final
              AS ( SELECT
                        cui.CU_INVOICE_ID
                       ,Service_Month
                       ,BEGIN_DATE
                       ,END_DATE
                       ,IS_PROCESSED
                       ,IS_DUPLICATE
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY cui.Service_Month )
                       ,Total_Rows = COUNT(1) OVER ( )
                   FROM
                        Cte_Determinant_Charge_Invoice dci
                        JOIN @Invoice cui
                              ON cui.Cu_Invoice_id = dci.Cu_Invoice_id
                   GROUP BY
                        cui.CU_INVOICE_ID
                       ,Service_Month
                       ,BEGIN_DATE
                       ,END_DATE
                       ,IS_PROCESSED
                       ,IS_DUPLICATE)
            SELECT
                  CU_INVOICE_ID
                 ,Service_Month = DATENAME(MONTH, SERVICE_MONTH) + SPACE(1) + DATENAME(YYYY, SERVICE_MONTH)
                 ,BEGIN_DATE
                 ,END_DATE
                 ,IS_PROCESSED
                 ,IS_DUPLICATE
                 ,Total_Rows
            FROM
                  Cte_Determinant_Charge_Invoice_final
            WHERE
                  Row_Num BETWEEN @StartIndex AND @EndIndex

END

;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_For_Meter_Commodity] TO [CBMSApplication]
GO
