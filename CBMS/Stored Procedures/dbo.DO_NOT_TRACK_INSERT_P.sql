
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DO_NOT_TRACK_INSERT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@USER_INFO_ID	INT
	@ACCOUNT_NUMBER	VARCHAR(200)
	@CLIENT_NAME	VARCHAR(50)
	@CLIENT_CITY	VARCHAR(200)
	@STATE_ID		INT
	@VENDOR_NAME	VARCHAR(200)
	@REASON_TYPE_ID INT
	@UBM_ID			INT
	@UBM_ACCOUNT_CODE VARCHAR(200)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN

    EXEC dbo.DO_NOT_TRACK_INSERT_P
      @USER_INFO_ID = 16
     ,@ACCOUNT_NUMBER = '18608 58000 02 (FKA 18608 58000 01)'
     ,@CLIENT_NAME = 'AZZ incorporated AZZ incorporated AZZ incorporated AZZ incorporated'
     ,@CLIENT_CITY = 'Westborough'
     ,@STATE_ID = 20
     ,@VENDOR_NAME = 'National Grid of MA'
     ,@REASON_TYPE_ID = 314
     ,@UBM_ID = NULL
     ,@UBM_ACCOUNT_CODE = NULL	

ROLLBACK TRAN

SELECT TOP 10 * FROM DO_NOT_TRACK WHERE Account_ID IS NOT NULL 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2013-12-09	Comments header added
							MAINT-2419, @Client_Name param width changed to 200 as Client_Name column width on DO_NOT_TRACK increased to VARCHAR(200) to match with Client.Client_Name
******/

CREATE PROCEDURE dbo.DO_NOT_TRACK_INSERT_P
      @USER_INFO_ID INT
     ,@ACCOUNT_NUMBER VARCHAR(200)
     ,@CLIENT_NAME VARCHAR(200)
     ,@CLIENT_CITY VARCHAR(200)
     ,@STATE_ID INT
     ,@VENDOR_NAME VARCHAR(200)
     ,@REASON_TYPE_ID INT
     ,@UBM_ID INT
     ,@UBM_ACCOUNT_CODE VARCHAR(200)
AS
BEGIN

      SET NOCOUNT ON

      INSERT      INTO dbo.DO_NOT_TRACK
                  ( USER_INFO_ID
                  ,ACCOUNT_NUMBER
                  ,CLIENT_NAME
                  ,CLIENT_CITY
                  ,STATE_ID
                  ,VENDOR_NAME
                  ,REASON_TYPE_ID
                  ,DATE_ADDED
                  ,UBM_ID
                  ,UBM_ACCOUNT_CODE )
      VALUES
                  ( @USER_INFO_ID
                  ,@ACCOUNT_NUMBER
                  ,@CLIENT_NAME
                  ,@CLIENT_CITY
                  ,@STATE_ID
                  ,@VENDOR_NAME
                  ,@REASON_TYPE_ID
                  ,GETDATE()
                  ,@UBM_ID
                  ,@UBM_ACCOUNT_CODE )

END

;
GO

GRANT EXECUTE ON  [dbo].[DO_NOT_TRACK_INSERT_P] TO [CBMSApplication]
GO
