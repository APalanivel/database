SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier                     
                          
 DESCRIPTION:      
		Get the Contract & Account Level OCT rules.                   
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Contract_Id						INT    
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------    
                        
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 

	EXEC dbo.Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier
    @Contract_Id = 159815


EXEC Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier
    @Contract_Id = 167450

EXEC Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier
    166647

EXEC Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier
    159815


	EXEC Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier
    166628

	exec Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier 166704


	EXEC Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier 159815



	  use cbms            
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.      
 NR						2020-01-29		MAINT-9782 - Removed Is_processed filter.   
 NR						2020-04-29		MAINT-10157 - Removed contract filter on map table for un associated list to if invoice is mapped any other contract.
 NR						2020-06-01		MAINT-  - Contract Extension - To show the recalcs in account level if that recalcs not related passed contract.                           
              
                         
******/

CREATE PROCEDURE [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Valcon_Account_Config_Type_Cd INT
            , @Contract_Associated_Invoice_Cnt INT
            , @Contract_Dis_Associated_Invoice_Cnt INT;;


        SELECT
            @Valcon_Account_Config_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Config Type'
            AND c.Code_Value = 'Supplier Account';


        CREATE TABLE #Supplier_Account_List
             (
                 Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Meter_Id INT
                 , Supplier_Contract_ID INT
                 , Client_Hier_Id INT
                 , Supplier_Account_begin_Dt DATE
                 , Supplier_Account_End_Dt DATE
                 , Commodity_Id INT
             );

        CREATE TABLE #Associated_Invoice_List
             (
                 Account_Id INT
                 , Supplier_Contract_ID INT
                 , Invoice_Cnt INT
             );

        CREATE TABLE #Un_Associated_Invoice_List
             (
                 Account_Id INT
                 , Supplier_Contract_ID INT
                 , Invoice_Cnt INT
             );


        CREATE TABLE #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id INT
                 , Account_Id INT
                 , Commodity_ID INT
                 , Invoice_Recalc_Type_Cd INT
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Source_Cd INT
                 , Contract_Id INT
             );

        INSERT INTO #Supplier_Account_List
             (
                 Account_Id
                 , Account_Number
                 , Meter_Id
                 , Supplier_Contract_ID
                 , Client_Hier_Id
                 , Supplier_Account_begin_Dt
                 , Supplier_Account_End_Dt
                 , Commodity_Id
             )
        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , @Contract_Id
            , cha.Client_Hier_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Commodity_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Number <> 'Not Yet Assigned'
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Client_Hier_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Commodity_Id;


        INSERT INTO #Un_Associated_Invoice_List
             (
                 Account_Id
                 , Supplier_Contract_ID
                 , Invoice_Cnt
             )
        SELECT
            cism.Account_ID
            , sal.Supplier_Contract_ID
            , COUNT(DISTINCT cism.CU_INVOICE_ID)
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN #Supplier_Account_List sal
                ON sal.Account_Id = cism.Account_ID
        WHERE
            EXISTS (SELECT  1 FROM  dbo.CU_INVOICE ci WHERE cism.CU_INVOICE_ID = ci.CU_INVOICE_ID)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Contract_Cu_Invoice_Map cmpa
                               WHERE
                                    cmpa.Account_Id = sal.Account_Id
                                    AND cmpa.Cu_Invoice_Id = cism.CU_INVOICE_ID)
            AND (   cism.Begin_Dt BETWEEN sal.Supplier_Account_begin_Dt
                                  AND     sal.Supplier_Account_End_Dt
                    OR  cism.End_Dt BETWEEN sal.Supplier_Account_begin_Dt
                                    AND     sal.Supplier_Account_End_Dt
                    OR  sal.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                      AND     cism.End_Dt
                    OR  sal.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                    AND     cism.End_Dt)
        GROUP BY
            cism.Account_ID
            , sal.Supplier_Contract_ID;



        INSERT INTO #Associated_Invoice_List
             (
                 Account_Id
                 , Supplier_Contract_ID
                 , Invoice_Cnt
             )
        SELECT
            ccim.Account_Id
            , ccim.Contract_Id
            , COUNT(DISTINCT ccim.Cu_Invoice_Id)
        FROM
            dbo.Contract_Cu_Invoice_Map ccim
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = ccim.Account_Id
                   AND  cism.CU_INVOICE_ID = ccim.Cu_Invoice_Id
            INNER JOIN #Supplier_Account_List sal
                ON sal.Account_Id = ccim.Account_Id
                   AND  sal.Supplier_Contract_ID = ccim.Contract_Id
        WHERE
            (   cism.Begin_Dt BETWEEN sal.Supplier_Account_begin_Dt
                              AND     sal.Supplier_Account_End_Dt
                OR  cism.End_Dt BETWEEN sal.Supplier_Account_begin_Dt
                                AND     sal.Supplier_Account_End_Dt
                OR  sal.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                  AND     cism.End_Dt
                OR  sal.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                AND     cism.End_Dt)
        GROUP BY
            ccim.Account_Id
            , ccim.Contract_Id;


        SELECT
            @Contract_Associated_Invoice_Cnt = SUM(ail.Invoice_Cnt)
        FROM
            #Associated_Invoice_List ail
        WHERE
            ail.Supplier_Contract_ID = @Contract_Id;
        SELECT
            @Contract_Dis_Associated_Invoice_Cnt = SUM(uail.Invoice_Cnt)
        FROM
            #Un_Associated_Invoice_List uail
        WHERE
            uail.Supplier_Contract_ID = @Contract_Id;


        -------------------------------------------------- Getting all recalcs in temp table and apply ---------------------------------
        DECLARE @Source_cd INT;

        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';

        INSERT INTO #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
                 , Account_Id
                 , Commodity_ID
                 , Invoice_Recalc_Type_Cd
                 , Start_Dt
                 , End_Dt
                 , Source_Cd
                 , Contract_Id
             )
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , NULL AS Source_Cd
            , NULL AS Contract_Id
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Supplier_Account_List sal
                       WHERE
                            sal.Account_Id = acirt.Account_Id
                            AND sal.Commodity_Id = acirt.Commodity_ID)
        GROUP BY
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt;
        -------------------------------Supplier Account Contract Level ---------------------------------------------   

        INSERT INTO #Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
                 , Account_Id
                 , Commodity_ID
                 , Invoice_Recalc_Type_Cd
                 , Start_Dt
                 , End_Dt
                 , Source_Cd
                 , Contract_Id
             )
        SELECT
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , @Source_cd AS Source_Cd
            , crc.Contract_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Contract_Recalc_Config crc
                ON crc.Contract_Id = cha.Supplier_Contract_ID
            INNER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON acirt.Account_Id = cha.Account_Id
                   AND  crc.Start_Dt = acirt.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(acirt.End_Dt, '9999-12-31')
            INNER JOIN dbo.Code c
                ON crc.Supplier_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Source_Cd = @Source_cd
            AND crc.Is_Consolidated_Contract_Config = 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Supplier_Account_List sal
                           WHERE
                                sal.Account_Id = acirt.Account_Id
                                AND sal.Commodity_Id = acirt.Commodity_ID)
        GROUP BY
            acirt.Account_Commodity_Invoice_Recalc_Type_Id
            , acirt.Account_Id
            , acirt.Commodity_ID
            , c.Code_Id
            , acirt.Start_Dt
            , acirt.End_Dt
            , crc.Contract_Id;



        SELECT
            'Contract Level' AS Configuration_Level
            , COUNT(DISTINCT cha.Meter_Id) AS Meter
            , ISNULL(@Contract_Associated_Invoice_Cnt, 0) AS Associated_Invoices
            , ISNULL(@Contract_Dis_Associated_Invoice_Cnt, 0) AS Un_Associated_Invoices
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END AS Recalc_Type
            , aloct.OCT_Parameter_Cd
            , Parameter_Cd.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , Dates_Cd.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , Range_Cd.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Contract_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END OCT_Rule_Exist
            , vcc.Comment_Id
            , cnt.Comment_Text
            , ISNULL(vcc.Is_Config_Complete, 0) AS Is_Config_Complete
            , aloct.Contract_Outside_Contract_Term_Config_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            dbo.CONTRACT c
            LEFT JOIN #Supplier_Account_List cha
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
            CROSS APPLY (   SELECT
                                CAST(clsrt_Valu.Supplier_Recalc_Type_Cd AS VARCHAR(100)) + '|'
                                + Sup_Recalc_valu.Code_Value + '(' + CONVERT(VARCHAR, clsrt_Valu.Start_Dt, 106) + ' - '
                                + ISNULL(CONVERT(VARCHAR, clsrt_Valu.End_Dt, 106), 'Open') + ')' + ','
                            FROM
                                dbo.Contract_Recalc_Config clsrt_Valu
                                INNER JOIN dbo.Code Sup_Recalc_valu
                                    ON Sup_Recalc_valu.Code_Id = clsrt_Valu.Supplier_Recalc_Type_Cd
                            WHERE
                                clsrt_Valu.Contract_Id = c.CONTRACT_ID
                                AND clsrt_Valu.Is_Consolidated_Contract_Config = 0
                            ORDER BY
                                clsrt_Valu.Start_Dt
                            FOR XML PATH('')) uerc(Invoice_Recalc_Type)
            LEFT JOIN dbo.Valcon_Contract_Config vcc
                ON vcc.Contract_Id = c.CONTRACT_ID
                   AND  vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd
            LEFT JOIN(dbo.Contract_Outside_Contract_Term_Config aloct
                      INNER JOIN dbo.Valcon_Contract_Config vc
                          ON vc.Valcon_Contract_Config_Id = aloct.Valcon_Contract_Config_Id
                             AND vc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_Cd)
                ON vc.Contract_Id = c.CONTRACT_ID
            LEFT JOIN dbo.Code Parameter_Cd
                ON Parameter_Cd.Code_Id = aloct.OCT_Parameter_Cd
            LEFT JOIN dbo.Code Dates_Cd
                ON Dates_Cd.Code_Id = aloct.OCT_Tolerance_Date_Cd
            LEFT JOIN dbo.Code Range_Cd
                ON Range_Cd.Code_Id = aloct.OCT_Dt_Range_Cd
            LEFT JOIN(dbo.Comment cnt
                      INNER JOIN dbo.Code cde
                          ON cde.Code_Id = cnt.Comment_Type_CD
                             AND cde.Code_Value = 'Valcal Contract Comment')
                ON vcc.Comment_Id = cnt.Comment_ID
        WHERE
            c.CONTRACT_ID = @Contract_Id
        GROUP BY
            Parameter_Cd.Code_Value
            , Dates_Cd.Code_Value
            , Range_Cd.Code_Value
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Contract_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , aloct.OCT_Tolerance_Date_Cd
            , aloct.OCT_Dt_Range_Cd
            , aloct.OCT_Parameter_Cd
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END
            , cnt.Comment_Text
            , vcc.Comment_Id
            , aloct.Contract_Outside_Contract_Term_Config_Id
            , ISNULL(vcc.Is_Config_Complete, 0)
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;


        SELECT
            'Account Level' AS Configuration_Level
            , cha.Account_Number
            , cha.Account_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , COUNT(DISTINCT cha.Meter_Id) AS Meter
            , ISNULL(cism.Invoice_Cnt, 0) AS Associated_Invoices
            , ISNULL(uail.Invoice_Cnt, 0) AS Un_Associated_Invoices
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END AS Recalc_Type
            , aloct.OCT_Parameter_Cd
            , Parameter_Cd.Code_Value AS OCT_Parameter
            , aloct.OCT_Tolerance_Date_Cd
            , Dates_Cd.Code_Value AS OCT_Tolerance_Date
            , aloct.OCT_Dt_Range_Cd
            , Range_Cd.Code_Value AS OCT_Dt_Range
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END OCT_Rule_Exist
            , cm.Comment_Text
            , vac.Comment_Id
            , ISNULL(vac.Is_Config_Complete, 0) AS Is_Config_Complete
            , aloct.Account_Outside_Contract_Term_Config_Id
            , ch.Client_Id
            , MIN(ch.Site_Id) AS Site_Id
            , src.Code_Value AS Source_Value
            , MIN(CASE WHEN src_acirt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                            AND src_acirt.Contract_Id = @Contract_Id
                            AND src_acirt.Source_Cd IS NOT NULL THEN 1
                      ELSE 0
                  END) Is_Contract_Level_Recalc
        FROM
            #Supplier_Account_List cha
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN #Associated_Invoice_List cism
                ON cha.Supplier_Contract_ID = cism.Supplier_Contract_ID
                   AND  cha.Account_Id = cism.Account_Id
            LEFT JOIN #Un_Associated_Invoice_List uail
                ON uail.Account_Id = cha.Account_Id
                   AND  uail.Supplier_Contract_ID = cha.Supplier_Contract_ID
            CROSS APPLY (   SELECT
                                CAST(clsrt_Valu.Invoice_Recalc_Type_Cd AS VARCHAR(100)) + '|'
                                + Sup_Recalc_valu.Code_Value + '(' + CONVERT(VARCHAR, clsrt_Valu.Start_Dt, 106) + ' - '
                                + ISNULL(CONVERT(VARCHAR, clsrt_Valu.End_Dt, 106), 'Open') + ')' + ','
                            FROM
                                #Account_Commodity_Invoice_Recalc_Type clsrt_Valu
                                INNER JOIN dbo.Code Sup_Recalc_valu
                                    ON Sup_Recalc_valu.Code_Id = clsrt_Valu.Invoice_Recalc_Type_Cd
                            WHERE
                                clsrt_Valu.Account_Id = cha.Account_Id
                                AND clsrt_Valu.Commodity_ID = cha.Commodity_Id
                                AND (   clsrt_Valu.Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                            AND     cha.Supplier_Account_End_Dt
                                        OR  ISNULL(clsrt_Valu.End_Dt, '9999-12-31') BETWEEN cha.Supplier_Account_begin_Dt
                                                                                    AND     cha.Supplier_Account_End_Dt
                                        OR  cha.Supplier_Account_begin_Dt BETWEEN clsrt_Valu.Start_Dt
                                                                          AND     ISNULL(
                                                                                      clsrt_Valu.End_Dt, '9999-12-31')
                                        OR  cha.Supplier_Account_End_Dt BETWEEN clsrt_Valu.Start_Dt
                                                                        AND     ISNULL(clsrt_Valu.End_Dt, '9999-12-31'))
                            ORDER BY
                                clsrt_Valu.Start_Dt
                            FOR XML PATH('')) uerc(Invoice_Recalc_Type)
            LEFT JOIN dbo.Valcon_Account_Config vac
                ON vac.Account_Id = cha.Account_Id
                   AND  vac.Contract_Id = @Contract_Id
            LEFT JOIN dbo.Code src
                ON src.Code_Id = vac.Source_Cd
            LEFT JOIN dbo.Account_Outside_Contract_Term_Config aloct
                ON aloct.Account_Id = cha.Account_Id
                   AND  aloct.Contract_Id = @Contract_Id
            LEFT JOIN dbo.Code Parameter_Cd
                ON Parameter_Cd.Code_Id = aloct.OCT_Parameter_Cd
            LEFT JOIN dbo.Code Dates_Cd
                ON Dates_Cd.Code_Id = aloct.OCT_Tolerance_Date_Cd
            LEFT JOIN dbo.Code Range_Cd
                ON Range_Cd.Code_Id = aloct.OCT_Dt_Range_Cd
            LEFT JOIN(dbo.Comment cm
                      INNER JOIN dbo.Code cde
                          ON cde.Code_Id = cm.Comment_Type_CD
                             AND cde.Code_Value = 'Valcal Account Comment')
                ON cm.Comment_ID = vac.Comment_Id
            LEFT JOIN #Account_Commodity_Invoice_Recalc_Type src_acirt
                ON src_acirt.Account_Id = cha.Account_Id
                   AND  src_acirt.Commodity_ID = cha.Commodity_Id
                   AND  (   src_acirt.Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                               AND     cha.Supplier_Account_End_Dt
                            OR  ISNULL(src_acirt.End_Dt, '9999-12-31') BETWEEN cha.Supplier_Account_begin_Dt
                                                                       AND     cha.Supplier_Account_End_Dt
                            OR  cha.Supplier_Account_begin_Dt BETWEEN src_acirt.Start_Dt
                                                              AND     ISNULL(src_acirt.End_Dt, '9999-12-31')
                            OR  cha.Supplier_Account_End_Dt BETWEEN src_acirt.Start_Dt
                                                            AND     ISNULL(src_acirt.End_Dt, '9999-12-31'))
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Number <> 'Not Yet Assigned'
        GROUP BY
            cha.Account_Number
            , cha.Account_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , Parameter_Cd.Code_Value
            , Dates_Cd.Code_Value
            , Range_Cd.Code_Value
            , aloct.Config_Start_Dt
            , aloct.Config_End_Dt
            , CASE WHEN aloct.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END
            , aloct.OCT_Tolerance_Date_Cd
            , aloct.OCT_Dt_Range_Cd
            , aloct.OCT_Parameter_Cd
            , cm.Comment_Text
            , vac.Comment_Id
            , CASE WHEN LEN(uerc.Invoice_Recalc_Type) > 1 THEN
                       LEFT(uerc.Invoice_Recalc_Type, LEN(uerc.Invoice_Recalc_Type) - 1)
                  ELSE uerc.Invoice_Recalc_Type
              END
            , aloct.Account_Outside_Contract_Term_Config_Id
            , ch.Client_Id
            , ISNULL(vac.Is_Config_Complete, 0)
            , src.Code_Value
            , ISNULL(cism.Invoice_Cnt, 0)
            , ISNULL(uail.Invoice_Cnt, 0);


        DROP TABLE #Supplier_Account_List;
        DROP TABLE #Un_Associated_Invoice_List;
        DROP TABLE #Associated_Invoice_List;
    END;

























GO
GRANT EXECUTE ON  [dbo].[Valcon_Setup_Contract_And_Account_Level_Sel_By_Contract_Id_For_Supplier] TO [CBMSApplication]
GO
