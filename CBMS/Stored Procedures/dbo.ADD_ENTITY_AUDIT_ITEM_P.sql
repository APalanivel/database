SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.ADD_ENTITY_AUDIT_ITEM_P
	@entity_identifier INT,
	@user_info_id INT,
	@audit_type INT,
	@entity_name VARCHAR(200),
	@entity_type INT

AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.ENTITY_AUDIT(Entity_ID
		, entity_identifier
		, user_info_id
		, audit_type
		, modified_date)
	SELECT ENTITY_ID
		, @entity_identifier
		, @user_info_id
		, @audit_type
		, GETDATE() 
	FROM dbo.ENTITY
	WHERE ENTITY_NAME=@entity_name 
		AND ENTITY_TYPE=@entity_type

END
GO
GRANT EXECUTE ON  [dbo].[ADD_ENTITY_AUDIT_ITEM_P] TO [CBMSApplication]
GO
