SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:    [Workflow].[Is_group_Exist_Filter_User]    
DESCRIPTION: THIS STORED PROCEDURE IS CREATED TO INSERT OR UPDATE THE GROUPING    
    
If output column id is empty, then code will delete the user column preferences.    
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description       
   @userID INT,  
    @Workflow_Queue_Saved_Filter_Query_id INT,  
    @Group_Name VARCHAR(MAX), --- new group name    
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description    
 @group_exist int   
------------------------------------------------------------        
 USAGE EXAMPLES:    
   
 USE [CBMS]  
GO  
  
DECLARE    
  @group_exist int  
  
EXEC   [Workflow].[Is_group_Exist_Filter_User]  
  @userID = 49,  
  @Workflow_Queue_Saved_Filter_Query_id = 178,  
  @Group_Name = N'JRGoup',  
  @group_exist = @group_exist OUTPUT  
  
SELECT @group_exist as N'@group_exist'   
  
GO  
      
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKP   ARUNKUMAR PALANIVEL Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 AKP    SEP 23,2019 Created    
    
******/  
CREATE PROCEDURE [Workflow].[Is_group_Exist_Filter_User]  
    -- Add the parameters for the stored procedure here       
    @userID INT,  
    @Workflow_Queue_Saved_Filter_Query_id INT,  
    @Group_Name VARCHAR(MAX), --- new group name   
    @group_exist INT OUTPUT   --- 1 EXIST  0 NOT EXIST  
AS  
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from       
    -- interfering with SELECT statements.       
    DECLARE @Proc_name VARCHAR(100) = 'Is_group_Exist_FIlter_User',  
            @Input_Params VARCHAR(1000),  
            @Error_Line INT,  
            @Error_Message VARCHAR(3000);   
  
    SELECT @Input_Params  
        = '@User ID:' + CAST(@userID AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'  
          + CAST(@Workflow_Queue_Saved_Filter_Query_id AS VARCHAR);  
    SET NOCOUNT ON;  
  
 SET @group_exist =0  
  
  
    BEGIN TRY  
  
        /* DELETE THE INVOICES MAPPED IN EXISTING GROUP*/  
  
  
  
        IF EXISTS  
        (  
            SELECT 1  
            FROM Workflow.Workflow_Queue_Search_Filter_Group  
            WHERE Workflow_Queue_Saved_Filter_Query_Id = @Workflow_Queue_Saved_Filter_Query_id  
                  AND Filter_Group_Name = @Group_Name  
                  AND Owner_User_Info_Id = @userID  
        )  
        BEGIN  
  
            SET @group_exist = 1; -- group exist for the same user under same filter  
  
  
        END;  
  
    --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR      
  
    END TRY  
    BEGIN CATCH  
  
        PRINT 'error';  
  
  
        -- Entry made to the logging SP to capture the errors.      
        SELECT @Error_Line = ERROR_LINE(),  
               @Error_Message = ERROR_MESSAGE();  
  
        INSERT INTO dbo.StoredProc_Error_Log  
        (  
            StoredProc_Name,  
            Error_Line,  
            Error_message,  
            Input_Params  
        )  
        VALUES  
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);  
    END CATCH;  
END; 

GO
GRANT EXECUTE ON  [Workflow].[Is_group_Exist_Filter_User] TO [CBMSApplication]
GO
