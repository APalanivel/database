SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                   
/******    
                  
NAME: [dbo].[Sitegroup_Id_Sel_By_Client_Hier_Id]
                    
DESCRIPTION:                    
			  To get SiteGroups Based on Client_Hier_Ids.                    
                    
INPUT PARAMETERS:    
                   
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
@Client_Hier_Id                   VARCHAR(MAX)          
                    
OUTPUT PARAMETERS:      
                      
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
                    
USAGE EXAMPLES:                        
---------------------------------------------------------------------------------------------------------------                          
            
EXEC [dbo].[Sitegroup_Id_Sel_By_Client_Hier_Id] 
      @Client_Hier_Id = '32401,32402,32403' 
  
                   
AUTHOR INITIALS:    
                  
Initials                Name    
---------------------------------------------------------------------------------------------------------------  
SP                      Sandeep Pigilam      
                     
MODIFICATIONS:   
                    
Initials                Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 SP                     2014-01-10      Created for RA Admin user management                  
                   
******/ 
CREATE PROCEDURE [dbo].[Sitegroup_Id_Sel_By_Client_Hier_Id]
      ( 
       @Client_Hier_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON


      DECLARE @Client_Hier_Ids TABLE ( Client_Hier_Id INT )                
                
                
      INSERT      INTO @Client_Hier_Ids
                  ( 
                   Client_Hier_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Client_Hier_Id, ',') ufn             
         
         

      SELECT
            ch.Sitegroup_Id
      FROM
            CORE.Client_Hier ch
            INNER JOIN @Client_Hier_Ids tempids
                  ON ch.Client_Hier_Id = tempids.Client_Hier_Id
      WHERE
            Sitegroup_Id > 0
            AND Site_Id = 0                  
                  




END



;
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_Id_Sel_By_Client_Hier_Id] TO [CBMSApplication]
GO
