SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Sites_Send_Confirmation_To_Client_Dtls_Sel_By_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Sites_Send_Confirmation_To_Client_Dtls_Sel_By_Deal_Ticket  291
	EXEC Trade.Sites_Send_Confirmation_To_Client_Dtls_Sel_By_Deal_Ticket  131852
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          19-02-2019  Created GRM
	RR			25-10-2019	GRM-1556 - Modified to pull respective executed trade of a site if a site have multiple trades  
                       
******/
CREATE PROCEDURE [Trade].[Sites_Send_Confirmation_To_Client_Dtls_Sel_By_Deal_Ticket]
     (
         @Deal_Ticket_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Configs
             (
                 Client_Id INT
                 , Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Site_Not_Managed BIT
                 , Last_Updated_Ts DATETIME
                 , Email_Address NVARCHAR(150)
             );

        ---------Already mapped
        INSERT INTO #Configs
             (
                 Client_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Site_Not_Managed
                 , Last_Updated_Ts
                 , Email_Address
             )
        SELECT
            ch.Client_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , dtcc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , ch.Site_Not_Managed
            , dtcc.Last_Change_Ts
            , ci.Email_Address
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Contact dtcc
                ON dt.Deal_Ticket_Id = dtcc.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Contact_Client_Hier dtcch
                ON dtcc.Deal_Ticket_Client_Contact_Id = dtcch.Deal_Ticket_Client_Contact_Id
            INNER JOIN Core.Client_Hier ch
                ON dtcch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Contact_Info ci
                ON dtcc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;


        ----------------------To get site own config  
        INSERT INTO #Configs
             (
                 Client_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Site_Not_Managed
                 , Last_Updated_Ts
                 , Email_Address
             )
        SELECT
            ch.Client_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , ch.Site_Not_Managed
            , chhc.Last_Updated_Ts
            , ci.Email_Address
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
                   AND  dt.Commodity_Id = chob.Commodity_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            LEFT JOIN(dbo.Contact_Info ci
                      INNER JOIN dbo.Code cd
                          ON ci.Contact_Type_Cd = cd.Code_Id
                      INNER JOIN dbo.Codeset cs
                          ON cd.Codeset_Id = cs.Codeset_Id
                             AND cs.Codeset_Name = 'ContactType'
                             AND cd.Code_Value = 'RM Client Contact')
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND NOT EXISTS (SELECT  1 FROM  #Configs chcc WHERE ch.Client_Hier_Id = chcc.Client_Hier_Id);

        ----------------------To get default config                         
        INSERT INTO #Configs
             (
                 Client_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Site_Not_Managed
                 , Last_Updated_Ts
                 , Email_Address
             )
        SELECT
            ch.Client_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , ch.Site_Not_Managed
            , chhc.Last_Updated_Ts
            , ci.Email_Address
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  dt.Commodity_Id = chob.Commodity_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            LEFT JOIN(dbo.Contact_Info ci
                      INNER JOIN dbo.Code cd
                          ON ci.Contact_Type_Cd = cd.Code_Id
                      INNER JOIN dbo.Codeset cs
                          ON cd.Codeset_Id = cs.Codeset_Id
                             AND cs.Codeset_Name = 'ContactType'
                             AND cd.Code_Value = 'RM Client Contact')
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chclient.Sitegroup_Id = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                               WHERE
                                    siteob.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob.Commodity_Id = chob.Commodity_Id)
            AND NOT EXISTS (SELECT  1 FROM  #Configs chcc WHERE ch.Client_Hier_Id = chcc.Client_Hier_Id);

        SELECT
            c1.Client_Hier_Id
            , c1.Site_name
            , c1.Client_Contact_Info_Id
            , c1.Client_Contact
            , ws.Workflow_Status_Name
            , Email_Address
            , CASE WHEN ws.Workflow_Status_Name = 'Order Executed' THEN 1
                  WHEN ws.Workflow_Status_Name IN ( 'Closed', 'Completed' ) THEN 0
              END AS Send_Confirmation_To_Client
            , chws.CBMS_Image_Id
            , vol.Trade_Number AS Trade_Id
            , CASE WHEN ws.Workflow_Status_Name = 'Order Executed' THEN NULL
                  WHEN ws.Workflow_Status_Name IN ( 'Closed', 'Completed' ) THEN chws.Last_Change_Ts
              END AS Task_Status_Ts
            , dtchnl.Notification_Msg_Dtl_Id AS Notification_Msg_Dtl_Id
        FROM
            #Configs c1
            INNER JOIN
            (   SELECT
                    c2.Site_Id
                    , MAX(c2.Last_Updated_Ts) AS Last_Updated_Ts
                FROM
                    #Configs c2
                GROUP BY
                    c2.Site_Id) c3
                ON c1.Site_Id = c3.Site_Id
                   AND  c1.Last_Updated_Ts = c3.Last_Updated_Ts
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON c1.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON vol.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = vol.Client_Hier_Id
                   AND  vol.Deal_Month = chws.Trade_Month
                   AND  vol.Contract_Id = chws.Contract_Id
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = vol.Trade_Price_Id
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Notification_Log dtchnl
                ON chws.Deal_Ticket_Client_Hier_Workflow_Status_Id = dtchnl.Deal_Ticket_Client_Hier_Workflow_Status_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
            AND ws.Workflow_Status_Name IN ( 'Order Executed', 'Closed', 'Completed' )
            AND tp.Trade_Price IS NOT NULL
        GROUP BY
            c1.Client_Hier_Id
            , c1.Site_name
            , c1.Client_Contact_Info_Id
            , c1.Client_Contact
            , ws.Workflow_Status_Name
            , Email_Address
            , CASE WHEN ws.Workflow_Status_Name = 'Order Executed' THEN 1
                  WHEN ws.Workflow_Status_Name IN ( 'Closed', 'Completed' ) THEN 0
              END
            , chws.CBMS_Image_Id
            , CASE WHEN ws.Workflow_Status_Name = 'Order Executed' THEN NULL
                  WHEN ws.Workflow_Status_Name IN ( 'Closed', 'Completed' ) THEN chws.Last_Change_Ts
              END
            , dtchnl.Notification_Msg_Dtl_Id
            , vol.Trade_Number;

        DROP TABLE #Configs;
    END;





GO
GRANT EXECUTE ON  [Trade].[Sites_Send_Confirmation_To_Client_Dtls_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
