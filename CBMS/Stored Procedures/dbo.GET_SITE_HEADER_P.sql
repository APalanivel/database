SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
Name:      
 dbo.GET_SITE_HEADER_P   
Description:  
  
 Used site details  
     
 Input Parameters:      
    Name      DataType Default   Description      
-------------------------------------------------------------------------------------      
 @siteID   INT  
   
 Output Parameters:    
 Name   Datatype  Default Description      
------------------------------------------------------------      
     
 Usage Examples:    
------------------------------------------------------------      
   
 Exec dbo.GET_SITE_HEADER_P 27  
 Exec dbo.GET_SITE_HEADER_P 262595  
     
     
Author Initials:    
 Initials	 Name    
------------------------------------------------------------    
 RR			 Raghu Reddy  
 AKR         Ashok Kumar Raju  
 SP          Srinivas Patchava
      
 Modifications :  
 Initials  Date  Modification  
------------------------------------------------------------  
 RR    2012-01-31  Added client hier id column in select clause as part of delete tool enhancement  
 AKR   2012-09-25  Added Analyst_Mapping_Cd as a part of POCO.  
 AKR   2012-10-09  Modified to Standards
 SP    2019-06-18  Added Client_Not_Managed Column  in select clause
******/
CREATE PROCEDURE [dbo].[GET_SITE_HEADER_P]
    (
        @siteID INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            c.ENTITY_NAME
            , a.SITE_TYPE_ID
            , a.SITE_NAME
            , a.DIVISION_ID
            , b.Sitegroup_Name
            , a.UBMSITE_ID
            , a.PRIMARY_ADDRESS_ID
            , a.SITE_REFERENCE_NUMBER
            , a.SITE_PRODUCT_SERVICE
            , a.PRODUCTION_SCHEDULE
            , a.SHUTDOWN_SCHEDULE
            , a.DOING_BUSINESS_AS
            , a.NAICS_CODE
            , a.TAX_NUMBER
            , a.DUNS_NUMBER
            , b.Client_Name
            , a.CLOSED
            , a.CLOSED_DATE
            , a.NOT_MANAGED
            , a.CONTRACTING_ENTITY
            , a.CLIENT_LEGAL_STRUCTURE
            , a.LEGAL_STRUCTURE_CBMS_IMAGE_ID
            , a.IS_PREFERENCE_BY_EMAIL
            , a.IS_PREFERENCE_BY_DV
            , a.LP_CONTACT_EMAIL_ADDRESS
            , a.LP_CONTACT_FIRST_NAME
            , a.LP_CONTACT_LAST_NAME
            , a.LP_CONTACT_CC
            , b.Client_Hier_Id
            , ISNULL(b.Site_Analyst_Mapping_Cd, b.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
            , b.Client_Not_Managed
        FROM
            dbo.SITE a
            INNER JOIN Core.Client_Hier b
                ON a.SITE_ID = b.Site_Id
                   AND  b.Site_Id = @siteID
            INNER JOIN dbo.ENTITY c
                ON c.ENTITY_ID = a.SITE_TYPE_ID
        GROUP BY
            c.ENTITY_NAME
            , a.SITE_TYPE_ID
            , a.SITE_NAME
            , a.DIVISION_ID
            , b.Sitegroup_Name
            , a.UBMSITE_ID
            , a.PRIMARY_ADDRESS_ID
            , a.SITE_REFERENCE_NUMBER
            , a.SITE_PRODUCT_SERVICE
            , a.PRODUCTION_SCHEDULE
            , a.SHUTDOWN_SCHEDULE
            , a.DOING_BUSINESS_AS
            , a.NAICS_CODE
            , a.TAX_NUMBER
            , a.DUNS_NUMBER
            , b.Client_Name
            , a.CLOSED
            , a.CLOSED_DATE
            , a.NOT_MANAGED
            , a.CONTRACTING_ENTITY
            , a.CLIENT_LEGAL_STRUCTURE
            , a.LEGAL_STRUCTURE_CBMS_IMAGE_ID
            , a.IS_PREFERENCE_BY_EMAIL
            , a.IS_PREFERENCE_BY_DV
            , a.LP_CONTACT_EMAIL_ADDRESS
            , a.LP_CONTACT_FIRST_NAME
            , a.LP_CONTACT_LAST_NAME
            , a.LP_CONTACT_CC
            , b.Client_Hier_Id
            , ISNULL(b.Site_Analyst_Mapping_Cd, b.Client_Analyst_Mapping_Cd)
            , b.Client_Not_Managed;

    END;

GO


GRANT EXECUTE ON  [dbo].[GET_SITE_HEADER_P] TO [CBMSApplication]
GO
