SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************                        
NAME: dbo.Invoice_Submission_Log_Sel_For_Report
    
DESCRIPTION:    
    
      Select the values for SSRS report
    
INPUT PARAMETERS:    
      NAME                                      DATATYPE    DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    

      
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION 
-----------------------------------------------------------      



------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            

	Exec Invoice_Submission_Log_Sel_For_Report @Start_Dt ='2018-10-01',@End_Dt ='2018-10-31'
         
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------              
		TP			Anoop
		      
MODIFICATIONS:    
      INITIALS    DATE			MODIFICATION              
------------------------------------------------------------              
      TP		23 Oct 2018		Created
	  KVK		12/17/2018		modified comment table join to Left Join
	  KVK		03/19/2019		modified to show all completed status files
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Invoice_Submission_Log_Sel_For_Report]
(
    @File_Name NVARCHAR(510) = NULL,
    @Site_Name VARCHAR(200) = NULL,
    @Start_Dt DATETIME = NULL,
    @End_Dt DATETIME = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT ui.USERNAME,
           ui.EMAIL_ADDRESS,
           ch.Site_name,
           isl.External_File_Name,
           isi.CBMS_FILENAME System_File_Name,
           c.Comment_Text,
           isl.Created_Ts AS Date_Submitted
    FROM dbo.Invoice_Submission_Log isl
        INNER JOIN dbo.Code cd_status
            ON cd_status.Code_Id = isl.Status_Cd
        LEFT JOIN dbo.INV_SOURCED_IMAGE isi
            ON isi.ORIGINAL_FILENAME = isl.System_File_Name
        LEFT JOIN dbo.USER_INFO ui
            ON ui.USER_INFO_ID = isl.User_Info_Id
        LEFT JOIN Core.Client_Hier ch
            ON isl.Client_Hier_Id = ch.Client_Hier_Id
        LEFT JOIN dbo.Comment c
            ON c.Comment_ID = isl.Comment_Id
    WHERE cd_status.Code_Value = 'Completed'
          AND
          (
              @File_Name IS NULL
              OR isl.External_File_Name LIKE '%' + @File_Name + '%'
          )
          AND
          (
              @Site_Name IS NULL
              OR ch.Site_name LIKE '%' + @Site_Name + '%'
          )
          AND
          (
              (
                  @Start_Dt IS NULL
                  AND @End_Dt IS NULL
              )
              OR (CAST(isl.Created_Ts AS DATE) BETWEEN @Start_Dt AND @End_Dt)
          )
    ORDER BY isl.Created_Ts DESC;

END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Submission_Log_Sel_For_Report] TO [CBMSApplication]
GO
