SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
     dbo.[Report_DE_AEG_Monthly_IC]  
           
DESCRIPTION:              
       
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------     
@budget_start_year			INT    
              
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
 EXEC [dbo].[Report_DE_AEG_Monthly_IC]  
     
      
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
LEC					Lynn Cox        
    
      
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 LEC                   2019-09-26      Created.    This is based on a query that the Invoice team would run for the CM each month on the 5th
*********/    
create PROCEDURE [dbo].[Report_DE_AEG_Monthly_IC]  
as begin
select distinct

i.client_Name

,i.Site_Name

,i.UBM

,i.Invoice_ID

,i.CBMS_Post_Date

,CU.BEGIN_DATE as 'Invoice Start Date'

,cu.END_DATE as 'Invoice End Date'

from Analysis.dbo.posted_invoice_data_JMM i

join CBMS.dbo.CU_INVOICE CU on CU.CU_INVOICE_ID=i.Invoice_ID

where i.Client_id = 11554 --AEG

and i.CBMS_Post_Date >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 1, 0) and i.CBMS_Post_Date < DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 0, 0)

and UBM <> 'Cass'

and CU.End_Date >= DATEADD(yy, DATEDIFF(yy, 0, DATEADD(mm, DATEDIFF(mm, 0, getdate()) - 1, 0)), 0)
end

GO
GRANT EXECUTE ON  [dbo].[Report_DE_AEG_Monthly_IC] TO [CBMS_SSRS_Reports]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_AEG_Monthly_IC] TO [CBMSApplication]
GO
