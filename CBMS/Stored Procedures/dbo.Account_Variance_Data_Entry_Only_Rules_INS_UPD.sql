SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Variance_Data_Entry_Only_Rules_INS_UPD  
 
DESCRIPTION:  Used to delete and insert Is_Data_Entry Rules for account

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_id						int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	Account_Variance_Data_Entry_Only_Rules_INS_UPD 12345
	
	

------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	01/21/2010  Created


******/  


CREATE PROCEDURE dbo.Account_Variance_Data_Entry_Only_Rules_INS_UPD
@Account_id	int
		
AS
BEGIN
		
	DELETE 
		dbo.Variance_Rule_Dtl_Account_Override
	WHERE
		ACCOUNT_ID = @Account_id
	
	INSERT INTO dbo.Variance_Rule_Dtl_Account_Override
					(Variance_Rule_Dtl_Id
					,ACCOUNT_ID
					)
	SELECT
		vrdtl.variance_rule_dtl_id,@Account_id
	FROM
		dbo.variance_rule_dtl vrdtl
		JOIN dbo.Account acc
			ON ISNULL(acc.Is_Data_Entry_Only,0) = vrdtl.Is_Data_Entry_Only
	WHERE
		acc.ACCOUNT_ID = @Account_id
		AND vrdtl.IS_Active = 1
END
GO
GRANT EXECUTE ON  [dbo].[Account_Variance_Data_Entry_Only_Rules_INS_UPD] TO [CBMSApplication]
GO
