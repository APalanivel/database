SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec GET_ACTUAL_SITES_UNDER_CLIENT_P '-1','-1',1009



-- THIS PROCEDURE WILL HAVE ACTUAL SITE NAME NOT CITY STATE




CREATE PROCEDURE dbo.GET_ACTUAL_SITES_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
select	site.SITE_ID,
	site.SITE_NAME

from	SITE site,

	DIVISION division 


where 	division.CLIENT_ID=@clientId AND
	site.DIVISION_ID=division.DIVISION_ID 

	

order by site.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_ACTUAL_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
