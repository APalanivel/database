SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE  PROCEDURE dbo.BUDGET_GET_REVISED_ACCOUNTS_P
	@budgetId int
	AS
	begin
		set nocount on
		select 	bacc.budget_account_id 
		from 	budget_account bacc,
			BUDGET_ACCOUNT_TYPE_VW type_vw
		where 	bacc.budget_id = @budgetId
			and type_vw.budget_id = bacc.budget_id
			and type_vw.budget_account_id = bacc.budget_account_id
			--and type_vw.budget_account_type in('A&B', 'C&D Created')
			and bacc.is_reforecast = 0
	end







GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_REVISED_ACCOUNTS_P] TO [CBMSApplication]
GO
