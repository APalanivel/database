SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.Code_Ins_By_Codeset_Name
 
DESCRIPTION:  
	Insert new question type - external reporting
  
INPUT PARAMETERS:  
Name				DataType		Default Description  
------------------------------------------------------------  
@code_value			VARCHAR(25)
@code_dsc			VARCHAR(255)
@is_active			BIT				1
@is_default			BIT				0
@display_seq		INT				100
@std_column_name	VARCHAR(255)  


OUTPUT PARAMETERS:  
Name				DataType		Default Description  
------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.Code_Ins_By_Codeset_Name 'Test2','Test2',1,1,1,'Question_Type_Cd','Question Types'

AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
SS			Sani

 
MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
SS			2018/05/02     	created

******/

CREATE PROCEDURE [dbo].[Code_Ins_By_Codeset_Name]
      ( 
       @code_value VARCHAR(25)
      ,@code_dsc VARCHAR(255)
      ,@is_active BIT = 1
      ,@is_default BIT = 1
      ,@display_seq INT = 1
      ,@std_column_name VARCHAR(255)
      ,@Codeset_Name VARCHAR(25) )
AS 
BEGIN  

      SET NOCOUNT ON

      INSERT      INTO dbo.CODE
                  ( 
                   CODE_VALUE
                  ,CODESET_ID
                  ,CODE_DSC
                  ,IS_ACTIVE
                  ,IS_DEFAULT
                  ,DISPLAY_SEQ )
                  SELECT
                        @Code_Value
                       ,CodeSet_Id
                       ,@Code_dsc
                       ,@is_Active
                       ,@is_Default
                       ,@display_seq
                  FROM
                        dbo.CodeSet cs
                  WHERE
                        STD_COLUMN_NAME = @std_column_name
                        AND Codeset_Name = @Codeset_Name
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.CODE c
                                         WHERE
                                          Code_Value = @code_value
                                          AND c.Codeset_Id = cs.Codeset_Id )

END
;
GO
GRANT EXECUTE ON  [dbo].[Code_Ins_By_Codeset_Name] TO [CBMSApplication]
GO
