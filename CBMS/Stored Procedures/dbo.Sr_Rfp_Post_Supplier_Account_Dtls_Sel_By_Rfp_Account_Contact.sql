SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default		Description
-------------------------------------------------------------------
	@Rfp_Id INT
    @Sr_Rfp_Account_Id	VARCHAR(MAX)	NULL   	          	

OUTPUT PARAMETERS:
	Name				DataType		Default		Description
-------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------
 
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 1876
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 1069
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 3374
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 13258,'12105018'
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 13258,'12105019'
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 13258,'12105018,12105019','239644,239643,239645'
	EXEC dbo.Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact 13258,'12105019','239644,239643'

	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-19	Global Sourcing - Phase3 - GCS-531 Created

******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact]
      ( 
       @Rfp_Id INT
      ,@Sr_Rfp_Account_Id VARCHAR(MAX) = NULL
      ,@Supplier_Contact_Vendor VARCHAR(MAX) = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
	
      WITH  Cte_Accs
              AS ( SELECT
                        isnull(rfpacc.SR_RFP_BID_GROUP_ID, rfpacc.SR_RFP_ACCOUNT_ID) AS SR_ACCOUNT_GROUP_ID
                       ,case WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END AS Is_Bid_Group
                       ,rfpacc.ACCOUNT_ID
                       ,rfpacc.SR_RFP_ID
                   FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                   WHERE
                        rfpacc.SR_RFP_ID = @Rfp_Id
                        AND ( @Sr_Rfp_Account_Id IS NULL
                              OR EXISTS ( SELECT
                                                cast(Segments AS INT)
                                          FROM
                                                dbo.ufn_split(@Sr_Rfp_Account_Id, ',')
                                          WHERE
                                                cast(Segments AS INT) = rfpacc.SR_RFP_ACCOUNT_ID ) ))
            SELECT
                  vndr.VENDOR_NAME
                 ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Contact_Name
                 ,convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,ch.Client_Name
                 ,ch.Site_Id
                 ,ch.Site_name
                 ,cha.Display_Account_Number
                 ,NULL AS group_name
                 ,convndr.SR_RFP_ID
                 ,ui.EMAIL_ADDRESS
            FROM
                  dbo.SR_RFP_SEND_SUPPLIER srss
                  INNER JOIN dbo.ENTITY en
                        ON en.ENTITY_ID = srss.STATUS_TYPE_ID
                  INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP convndr
                        ON convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                  INNER JOIN dbo.VENDOR vndr
                        ON convndr.VENDOR_ID = vndr.VENDOR_ID
                  INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                        ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = convndr.SR_SUPPLIER_CONTACT_INFO_ID
                  INNER JOIN dbo.USER_INFO ui
                        ON ssci.USER_INFO_ID = ui.USER_INFO_ID
                  INNER JOIN Cte_Accs accs
                        ON accs.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                           AND srss.IS_BID_GROUP = accs.Is_Bid_Group
                  INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Account_Id = accs.ACCOUNT_ID
                  INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
            WHERE
                  en.ENTITY_NAME = 'Post'
                  AND ENTITY_TYPE = 1013
                  AND ( @Supplier_Contact_Vendor IS NULL
                        OR EXISTS ( SELECT
                                          cast(Segments AS INT)
                                    FROM
                                          dbo.ufn_split(@Supplier_Contact_Vendor, ',')
                                    WHERE
                                          cast(Segments AS INT) = srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID ) )
            GROUP BY
                  vndr.VENDOR_NAME
                 ,ui.FIRST_NAME + ' ' + ui.LAST_NAME
                 ,convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                 ,ch.Client_Name
                 ,ch.Site_Id
                 ,ch.Site_name
                 ,cha.Display_Account_Number
                 ,convndr.SR_RFP_ID
                 ,ui.EMAIL_ADDRESS
            
				
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Post_Supplier_Account_Dtls_Sel_By_Rfp_Account_Contact] TO [CBMSApplication]
GO
