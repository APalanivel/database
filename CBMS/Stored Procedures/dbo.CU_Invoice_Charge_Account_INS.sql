SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       
NAME:  dbo.CU_Invoice_Charge_Account_INS 
     
DESCRIPTION:  
	This object is used to insert the record during stage-3 process for CU_INVOICE_CHARGE_ACCOUNT
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	@CU_Invoice_Id	int     
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
    
 EXEC CU_Invoice_Charge_Account_INS 31662
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
SKA		Shobhit Kumar Agrawal            
    
    
Initials Date  Modification      
------------------------------------------------------------      
SKA		 06/07/2010  Created    
		
******/      

CREATE PROCEDURE dbo.CU_Invoice_Charge_Account_INS
      (
       @CU_Invoice_Id AS INT )
AS 
BEGIN
      INSERT INTO
            CU_Invoice_Charge_Account
            (
            CU_INVOICE_CHARGE_ID )
        SELECT
             CU_INVOICE_CHARGE_ID
        FROM
              dbo.CU_INVOICE_CHARGE
        WHERE
              CU_INVOICE_ID = @CU_Invoice_Id
				  
END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Account_INS] TO [CBMSApplication]
GO
