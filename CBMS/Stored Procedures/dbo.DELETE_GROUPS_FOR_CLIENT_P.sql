SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_GROUPS_FOR_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@groupId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[DELETE_GROUPS_FOR_CLIENT_P] @userId varchar,
@sessionId varchar,
@groupId int


as
DECLARE @statusId int
DECLARE @statusCheck int  

	SELECT @statusId = RM_DEAL_TICKET_ID FROM RM_DEAL_TICKET 
	WHERE RM_GROUP_ID = @groupId

	select @statusCheck = RM_GROUP_SITE_MAP.RM_GROUP_ID from  RM_GROUP_SITE_MAP
	where RM_GROUP_ID = @groupId

	IF @statusId IS  NULL and @statusCheck is NULL

	BEGIN
		delete RM_GROUP where RM_GROUP_ID = @groupId
	END
GO
GRANT EXECUTE ON  [dbo].[DELETE_GROUPS_FOR_CLIENT_P] TO [CBMSApplication]
GO
