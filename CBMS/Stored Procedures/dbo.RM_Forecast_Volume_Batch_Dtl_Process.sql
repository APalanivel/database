SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Forecast_Volume_Batch_Dtl_Process                      
                        
Description:                        
        To load forecast volumes for on-boarded sites  
                        
Input Parameters:                        
    Name        DataType    Default     Description                          
--------------------------------------------------------------------------------  
 @RM_Forecast_Volume_Batch_Dtl_Id INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
  exec [dbo].[RM_Forecast_Volume_Batch_Dtl_Process]  148
                         
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-09-27     Global Risk Management - Created   
                       
******/
CREATE PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Dtl_Process]
    (
        @RM_Forecast_Volume_Batch_Dtl_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @RM_Sites_Forecast AS TABLE
              (
                  Client_Hier_Id INT
                  , Commodity_Id INT
                  , Service_Month DATE
                  , RM_Forecast_UOM_Type_Id INT
              );

        DECLARE @Tbl_RM_Accs AS TABLE
              (
                  Year_Id INT
                  , Client_Hier_Id INT
                  , Account_Id INT
                  , Commodity_Id INT
                  , Service_Month DATE
                  , Days_Month INT
                  , Uom_Id INT
              );

        DECLARE @Tbl_RM_Volumes AS TABLE
              (
                  Year_Id INT
                  , Client_Hier_Id INT
                  , Account_Id INT
                  , Commodity_Id INT
                  , Service_Month DATE
                  , Volume DECIMAL(28, 12)
                  , Uom_Id INT
                  , Volume_Source_Cd INT
                  , Days_Month INT
              );


        DECLARE
            @Data_Change_Level_Cd INT
            , @ActualContract_Volume_Source_Cd INT
            , @HistoricalContractSystem_Volume_Source_Cd INT
            , @Min_Year_Id INT
            , @Max_Year_Id INT
            , @Contract_Min_Start_Dt DATE
            , @Service_Month DATE
            , @Min_Dt DATE
            , @User_Info_Id INT;

        SELECT
            @Data_Change_Level_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Data Change Level'
            AND cd.Code_Value = 'Account';


        SELECT
            @ActualContract_Volume_Source_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'RM Forecast Volume Source'
            AND cd.Code_Value = 'ActualContract';

        SELECT
            @HistoricalContractSystem_Volume_Source_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'RM Forecast Volume Source'
            AND cd.Code_Value = 'HistoricalContractSystem';


        INSERT INTO @RM_Sites_Forecast
             (
                 Client_Hier_Id
                 , Commodity_Id
                 , Service_Month
                 , RM_Forecast_UOM_Type_Id
             )
        SELECT
            dtl.Client_Hier_Id
            , chob.Commodity_Id
            , dtl.Forecast_Service_Month
            , chob.RM_Forecast_UOM_Type_Id
        FROM
            Trade.RM_Forecast_Volume_Batch_Dtl dtl
            INNER JOIN Trade.RM_Forecast_Volume_Batch bacth
                ON dtl.RM_Forecast_Volume_Batch_Id = bacth.RM_Forecast_Volume_Batch_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON bacth.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config config
                ON chob.RM_Client_Hier_Onboard_Id = config.RM_Client_Hier_Onboard_Id
        WHERE
            dtl.RM_Forecast_Volume_Batch_Dtl_Id = @RM_Forecast_Volume_Batch_Dtl_Id
        GROUP BY
            dtl.Client_Hier_Id
            , chob.Commodity_Id
            , dtl.Forecast_Service_Month
            , chob.RM_Forecast_UOM_Type_Id;


        SELECT
            @User_Info_Id = config.Last_Updated_By
        FROM
            Trade.RM_Forecast_Volume_Batch_Dtl dtl
            INNER JOIN Trade.RM_Forecast_Volume_Batch bacth
                ON dtl.RM_Forecast_Volume_Batch_Id = bacth.RM_Forecast_Volume_Batch_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON bacth.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config config
                ON chob.RM_Client_Hier_Onboard_Id = config.RM_Client_Hier_Onboard_Id
        WHERE
            dtl.RM_Forecast_Volume_Batch_Dtl_Id = @RM_Forecast_Volume_Batch_Dtl_Id;

        SELECT  @Service_Month = Service_Month FROM @RM_Sites_Forecast;

        SELECT
            @Contract_Min_Start_Dt = MIN(CAST(cmv.MONTH_IDENTIFIER AS DATE))
        FROM
            @RM_Sites_Forecast trs
            INNER JOIN Core.Client_Hier_Account chutil
                ON trs.Client_Hier_Id = chutil.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account chsupp
                ON chutil.Meter_Id = chsupp.Meter_Id
            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON chsupp.Meter_Id = cmv.METER_ID
            INNER JOIN dbo.CONTRACT con
                ON cmv.CONTRACT_ID = con.CONTRACT_ID
                   AND  trs.Commodity_Id = con.COMMODITY_TYPE_ID
        WHERE
            chutil.Account_Type = 'Utility'
            AND chsupp.Account_Type = 'Supplier';

        SELECT
            @Min_Dt = CASE WHEN @Contract_Min_Start_Dt < @Service_Month THEN @Contract_Min_Start_Dt
                          ELSE @Service_Month
                      END;

        INSERT INTO @Tbl_RM_Accs
             (
                 Year_Id
                 , Client_Hier_Id
                 , Account_Id
                 , Commodity_Id
                 , Service_Month
                 , Days_Month
                 , Uom_Id
             )
        SELECT
            DENSE_RANK() OVER (ORDER BY
                                   dd.YEAR_NUM)
            , chautil.Client_Hier_Id
            , chautil.Account_Id
            , rsf.Commodity_Id
            , dd.DATE_D
            , dd.DAYS_IN_MONTH_NUM
            , rsf.RM_Forecast_UOM_Type_Id
        FROM
            meta.DATE_DIM dd
            CROSS JOIN @RM_Sites_Forecast rsf
            INNER JOIN Core.Client_Hier_Account chautil
                ON rsf.Client_Hier_Id = chautil.Client_Hier_Id
                   AND  rsf.Commodity_Id = chautil.Commodity_Id
        WHERE
            chautil.Account_Type = 'Utility'
            AND dd.DATE_D <= rsf.Service_Month
            AND dd.DATE_D >= @Min_Dt
            AND DATEPART(mm, dd.DATE_D) = DATEPART(mm, rsf.Service_Month)
        GROUP BY
            dd.YEAR_NUM
            , chautil.Client_Hier_Id
            , chautil.Account_Id
            , rsf.Commodity_Id
            , dd.DATE_D
            , dd.DAYS_IN_MONTH_NUM
            , rsf.RM_Forecast_UOM_Type_Id;

        SELECT
            @Min_Year_Id = MIN(Year_Id)
            , @Max_Year_Id = MAX(Year_Id)
        FROM
            @Tbl_RM_Accs;




        INSERT INTO @Tbl_RM_Volumes
             (
                 Year_Id
                 , Client_Hier_Id
                 , Account_Id
                 , Commodity_Id
                 , Service_Month
                 , Volume
                 , Uom_Id
                 , Volume_Source_Cd
                 , Days_Month
             )
        SELECT
            trs.Year_Id
            , trs.Client_Hier_Id
            , trs.Account_Id
            , trs.Commodity_Id
            , trs.Service_Month
            , SUM(CASE freq.ENTITY_NAME WHEN 'Daily' THEN cmv.VOLUME * trs.Days_Month
                      ELSE cmv.VOLUME
                  END)
            , MAX(cmv.UNIT_TYPE_ID)
            , @ActualContract_Volume_Source_Cd
            , trs.Days_Month
        FROM
            @Tbl_RM_Accs trs
            INNER JOIN Core.Client_Hier_Account chutil
                ON trs.Account_Id = chutil.Account_Id
                   AND  trs.Client_Hier_Id = chutil.Client_Hier_Id
                   AND  chutil.Commodity_Id = trs.Commodity_Id
            INNER JOIN Core.Client_Hier_Account chsupp
                ON chutil.Meter_Id = chsupp.Meter_Id
                   AND  chsupp.Commodity_Id = chutil.Commodity_Id
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON chsupp.Meter_Id = cmv.METER_ID
                   AND  trs.Service_Month = CAST(cmv.MONTH_IDENTIFIER AS DATE)
                   AND  chsupp.Supplier_Contract_ID = cmv.CONTRACT_ID
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  cuc.CONVERTED_UNIT_ID = trs.Uom_Id
            LEFT JOIN dbo.CONTRACT con
                ON cmv.CONTRACT_ID = con.CONTRACT_ID
                   AND  trs.Commodity_Id = con.COMMODITY_TYPE_ID
            LEFT JOIN dbo.ENTITY freq
                ON cmv.FREQUENCY_TYPE_ID = freq.ENTITY_ID
        WHERE
            chsupp.Account_Type = 'Supplier'
        GROUP BY
            trs.Year_Id
            , trs.Client_Hier_Id
            , trs.Account_Id
            , trs.Commodity_Id
            , trs.Service_Month
            , trs.Days_Month;



        WHILE (@Min_Year_Id < @Max_Year_Id)
            BEGIN
                UPDATE
                    t2
                SET
                    t2.Volume = CASE WHEN t2.Volume IS NULL
                                          AND   t1.Volume IS NOT NULL THEN t1.Volume
                                    ELSE t2.Volume
                                END
                    , t2.Uom_Id = CASE WHEN t2.Volume IS NULL
                                            AND t1.Volume IS NOT NULL THEN t1.Uom_Id
                                      ELSE t2.Uom_Id
                                  END
                    , t2.Volume_Source_Cd = CASE WHEN t2.Volume IS NULL
                                                      AND   t1.Volume IS NOT NULL THEN
                                                     @HistoricalContractSystem_Volume_Source_Cd
                                                ELSE t2.Volume_Source_Cd
                                            END
                FROM
                    @Tbl_RM_Volumes t1
                    INNER JOIN @Tbl_RM_Volumes t2
                        ON t1.Client_Hier_Id = t2.Client_Hier_Id
                           AND  t1.Account_Id = t2.Account_Id
                           AND  DATEPART(mm, t1.Service_Month) = DATEPART(mm, t2.Service_Month)
                WHERE
                    t1.Year_Id = @Min_Year_Id
                    AND t2.Year_Id = @Min_Year_Id + 1;

                SELECT  @Min_Year_Id = @Min_Year_Id + 1;
            END;

        --SELECT  
        --      *  
        --FROM  
        --      @RM_Sites_Forecast  
        --SELECT  
        --      @Contract_Min_Start_Dt  
        --SELECT  
        --      *  
        --FROM  
        --      @Tbl_RM_Volumes  
        --SELECT  
        --      @Min_Year_Id  
        --     ,@Max_Year_Id  
        --SELECT  
        --      *  
        --FROM  
        --      @Tbl_RM_Volumes  

        --DELETE
        --      accfc
        --FROM
        --      Trade.RM_Account_Forecast_Volume accfc
        --      INNER JOIN @Tbl_RM_Volumes trs
        --            ON trs.Client_Hier_Id = accfc.Client_Hier_Id
        --               AND trs.Account_Id = accfc.Account_Id
        --               AND trs.Commodity_Id = accfc.Commodity_Id
        --               AND trs.Service_Month = accfc.Service_Month
        --      INNER JOIN @RM_Sites_Forecast rsf
        --            ON trs.Client_Hier_Id = rsf.Client_Hier_Id
        --               AND trs.Service_Month = rsf.Service_Month
        --      INNER JOIN dbo.Code cd
        --            ON accfc.Volume_Source_Cd = cd.Code_Id
        --      INNER JOIN dbo.Codeset cs
        --            ON cd.Codeset_Id = cs.Codeset_Id
        --WHERE
        --      cs.Codeset_Name = 'RM Forecast Volume Source'
        --      AND cd.Code_Value IN ( 'ActualContract', 'HistoricalContractSystem' );



        --DELETE
        --      chfc
        --FROM
        --      Trade.RM_Client_Hier_Forecast_Volume chfc
        --      INNER JOIN @Tbl_RM_Volumes trs
        --            ON trs.Client_Hier_Id = chfc.Client_Hier_Id
        --               AND trs.Commodity_Id = chfc.Commodity_Id
        --               AND trs.Service_Month = chfc.Service_Month
        --      INNER JOIN @RM_Sites_Forecast rsf
        --            ON trs.Client_Hier_Id = rsf.Client_Hier_Id
        --               AND trs.Service_Month = rsf.Service_Month
        --      INNER JOIN dbo.Code cd
        --            ON chfc.Volume_Source_Cd = cd.Code_Id
        --      INNER JOIN dbo.Codeset cs
        --            ON cd.Codeset_Id = cs.Codeset_Id
        --WHERE
        --      cs.Codeset_Name = 'RM Forecast Volume Source'
        --      AND cd.Code_Value IN ( 'ActualContract', 'HistoricalContractSystem' );


        INSERT INTO Trade.RM_Account_Forecast_Volume
             (
                 Client_Hier_Id
                 , Account_Id
                 , Commodity_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Volume_Source_Cd
                 , Data_Change_Level_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            trs.Client_Hier_Id
            , trs.Account_Id
            , trs.Commodity_Id
            , trs.Service_Month
            , ISNULL(trs.Volume, 0)
            , ISNULL(trs.Uom_Id, 0)
            , ISNULL(trs.Volume_Source_Cd, 0)
            , @Data_Change_Level_Cd
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        FROM
            @Tbl_RM_Volumes trs
            INNER JOIN @RM_Sites_Forecast rsf
                ON trs.Client_Hier_Id = rsf.Client_Hier_Id
                   AND  trs.Service_Month = rsf.Service_Month
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Account_Forecast_Volume afv
                           WHERE
                                trs.Client_Hier_Id = afv.Client_Hier_Id
                                AND trs.Account_Id = afv.Account_Id
                                AND trs.Commodity_Id = afv.Commodity_Id
                                AND trs.Service_Month = afv.Service_Month);

        INSERT INTO Trade.RM_Client_Hier_Forecast_Volume
             (
                 Client_Hier_Id
                 , Commodity_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Volume_Source_Cd
                 , Data_Change_Level_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            trs.Client_Hier_Id
            , trs.Commodity_Id
            , trs.Service_Month
            , SUM(ISNULL(trs.Volume, 0))
            , MAX(ISNULL(trs.Uom_Id, 0))
            , MAX(ISNULL(trs.Volume_Source_Cd, 0))
            , @Data_Change_Level_Cd
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        FROM
            @Tbl_RM_Volumes trs
            INNER JOIN @RM_Sites_Forecast rsf
                ON trs.Client_Hier_Id = rsf.Client_Hier_Id
                   AND  trs.Service_Month = rsf.Service_Month
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Client_Hier_Forecast_Volume chv
                           WHERE
                                trs.Client_Hier_Id = chv.Client_Hier_Id
                                AND trs.Commodity_Id = chv.Commodity_Id
                                AND trs.Service_Month = chv.Service_Month)
        GROUP BY
            trs.Client_Hier_Id
            , trs.Commodity_Id
            , trs.Service_Month;



    END;



GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Dtl_Process] TO [CBMSApplication]
GO
