SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO


CREATE   PROCEDURE dbo.SR_SAD_SAVE_CEM_INFO_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@isCemParticipation bit,
@participationComments varchar(100),
@supplier varchar(100),
@isAuthorityGranted bit,
@authorityLimit varchar(100)
AS
	set nocount on
IF (select count(1) from SR_DT_CEM_INFO where SR_DEAL_TICKET_ID = @srDealTicketId) = 0
BEGIN	

	insert into SR_DT_CEM_INFO
			(SR_DEAL_TICKET_ID, IS_CEM_PARTICIPATION, PARTICIPATION_COMMENTS, 
			 VENDOR_ID, IS_AUTHORITY_GRANTED, AUTHORITY_LIMIT)
	
		values
		(@srDealTicketId, @isCemParticipation, @participationComments, 
		 @supplier, @isAuthorityGranted, @authorityLimit)

END

ELSE
BEGIN

	update SR_DT_CEM_INFO set IS_CEM_PARTICIPATION=@isCemParticipation, 
		PARTICIPATION_COMMENTS=@participationComments, VENDOR_ID=@supplier,
		IS_AUTHORITY_GRANTED=@isAuthorityGranted, AUTHORITY_LIMIT=@authorityLimit

	where
		SR_DEAL_TICKET_ID=@srDealTicketId
	


END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_CEM_INFO_P] TO [CBMSApplication]
GO
