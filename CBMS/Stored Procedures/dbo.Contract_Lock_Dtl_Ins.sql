SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.Contract_Lock_Dtl_Ins       
       
DESCRIPTION:   
  
Used to Insert the data into dbo.Contract_Lock_Dtl_Ins table.  
      
INPUT PARAMETERS:          
      Name     DataType          Default     Description          
------------------------------------------------------------          
   @Contract_Id   INT  
   @Start_Dt    DATE   
   @Expiration_Dt      DATE   
   @Nymex_Price   DECIMAL (28, 10)   
    @Percentage_Locked     DECIMAL (5, 2)   
          
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:     
  
BEGIN TRAN  
select * from dbo.Contract_Lock_Dtl where Contract_Id = 10871  
EXEC DBO.Contract_Lock_Dtl_Ins 10871,'2012-01-01','2012-12-01',100,200,300,50,15  
select * from dbo.Contract_Lock_Dtl where Contract_Id = 10871  
ROLLBACK TRAN  
  
  
------------------------------------------------------------    
      
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKR   Ashok Kumar Raju  
      
      
Initials Date  Modification        
------------------------------------------------------------        
AKR  2012-09-20  Created new sp (for POCO Project)  
******/  
  
CREATE PROCEDURE dbo.Contract_Lock_Dtl_Ins
      ( 
       @Contract_Id INT
      ,@Start_Dt DATE
      ,@Expiration_Dt DATE
      ,@Nymex_Price DECIMAL(28, 10)
      ,@Percentage_Locked DECIMAL(5, 2) )
AS 
BEGIN  
      SET NOCOUNT ON ;  
        
      INSERT      INTO dbo.Contract_Lock_Dtl
                  ( 
                   Contract_Id
                  ,Start_Dt
                  ,Expiration_Dt
                  ,Nymex_Price
                  ,Percentage_Locked )
      VALUES
                  ( 
                   @Contract_Id
                  ,@Start_Dt
                  ,@Expiration_Dt
                  ,@Nymex_Price
                  ,@Percentage_Locked ) ;  
                    
END  
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Lock_Dtl_Ins] TO [CBMSApplication]
GO
