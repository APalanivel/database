SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Forecast_Volumes_Sel_For_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
DECLARE  @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites 
INSERT INTO @Trade_tvp_Hedge_Contract_Sites
		(Contract_Id, Site_Id) VALUES (157798, 14079),(149817,3054)
EXEC dbo.RM_Forecast_Volumes_Sel_For_Deal_Ticket  @Client_Id = 1043,@Commodity_Id = 291  
		,@Start_Dt='2019-11-01',@End_Dt='2020-03-01',@Hedge_Type=586,@Uom_Id=25
		,@Deal_Ticket_Frequency_Cd = 102744,@Trade_tvp_Hedge_Contract_Sites = @Trade_tvp_Hedge_Contract_Sites

DECLARE  @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites 
INSERT INTO @Trade_tvp_Hedge_Contract_Sites
		(Contract_Id, Site_Id) VALUES (162766, 10449), (162766, 498222), (162787, 10449), (183567, 10449), (183568, 10449)
		, (183568, 498222)
EXEC dbo.RM_Forecast_Volumes_Sel_For_Deal_Ticket  @Client_Id = 10017,@Commodity_Id = 291  
		,@Start_Dt='2020-06-01',@End_Dt='2020-09-01',@Hedge_Type=586,@Uom_Id=25
		,@Deal_Ticket_Frequency_Cd = 102744,@Trade_tvp_Hedge_Contract_Sites = @Trade_tvp_Hedge_Contract_Sites
  
Author Initials:    
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	PR			02-11-2018  Created GRM
	RR			2109-09-25	GRM-1530 - Added Site_Forecast_Volume to select list
	RR			2019-10-31	GRM-1550 Multiplying Site_Forecast_Volume if multiple UOMs there in a single month, 
							moved site level aggregation to subquery
	RR			2020-03-26	GRM-1819 - Site level forecasted volume doubling volumes as accounts have mulitple UOMs, to fix
							it modified to use site level UOM in conversion
******/
CREATE PROCEDURE [dbo].[RM_Forecast_Volumes_Sel_For_Deal_Ticket]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Hedge_Type INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Contract_Id VARCHAR(MAX) = NULL
        , @Uom_Id INT = NULL
        , @Site_Str VARCHAR(MAX) = NULL
        , @Division_Id VARCHAR(MAX) = NULL
        , @RM_Group_Id VARCHAR(MAX) = NULL
        , @Deal_Ticket_Frequency_Cd INT = NULL
        , @Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Hedge_Type_Input VARCHAR(200)
            , @Deal_Ticket_Frequency VARCHAR(25)
            , @Hedge_Contract_Sites_Cnt INT;

        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        SELECT
            @Deal_Ticket_Frequency = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Deal_Ticket_Frequency_Cd;

        SELECT
            @Hedge_Contract_Sites_Cnt = COUNT(1)
        FROM
            @Trade_tvp_Hedge_Contract_Sites;

        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 0)
                 , Uom_Id INT
                 , Max_Hedge_Pct DECIMAL(5, 2)
                 , Max_Hedge_Volume DECIMAL(28, 0)
                 , Site_Forecast_Volume DECIMAL(28, 0)
                 , Site_Uom_Id INT
             );

        DECLARE @Common_Uom_Id INT;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Group_Id, ',') con
                           WHERE
                                rgd.Cbms_Sitegroup_Id = CAST(Segments AS INT))
                AND grp.Code_Value = 'Site');


        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT  CAST(Segments AS INT)FROM   dbo.ufn_split(@Site_Str, ',') con;

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Site_Id > 0
            AND (EXISTS (   SELECT
                                1
                            FROM
                                dbo.ufn_split(@Division_Id, ',') con
                            WHERE
                                ch.Sitegroup_Id = CAST(Segments AS INT)));



        ----------------------To get site own config  
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Max_Hedge_Pct
                 , Max_Hedge_Volume
                 , Site_Forecast_Volume
                 , Site_Uom_Id
             )
        SELECT
            afv.Client_Hier_Id
            , afv.Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume AS Forecast_Volume
            , afv.Uom_Id AS Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100
            , MAX(rchfv.Forecast_Volume)
            , MAX(rchfv.Uom_Id)
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            LEFT JOIN Trade.RM_Account_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
            LEFT JOIN Core.Client_Hier_Account cha
                ON afv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  afv.Account_Id = cha.Account_Id
            LEFT JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
                   AND  suppacc.Account_Type = 'Supplier'
            LEFT JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON rchfv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = rchfv.Commodity_Id
                   AND  rchfv.Service_Month = afv.Service_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND (   @Hedge_Contract_Sites_Cnt > 0
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        @Trade_tvp_Hedge_Contract_Sites hcs
                                   WHERE
                                        suppacc.Supplier_Contract_ID = hcs.Contract_Id
                                        AND ch.Site_Id = hcs.Site_Id)
                    AND afv.Service_Month BETWEEN con.CONTRACT_START_DATE
                                          AND     con.CONTRACT_END_DATE)
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                  AND     chhc.Config_End_Dt
        GROUP BY
            afv.Client_Hier_Id
            , afv.Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume
            , afv.Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100;



        ----------------------To get default config                         
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Max_Hedge_Pct
                 , Max_Hedge_Volume
                 , Site_Forecast_Volume
                 , Site_Uom_Id
             )
        SELECT
            afv.Client_Hier_Id
            , afv.Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume AS Forecast_Volume
            , afv.Uom_Id AS Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100
            , MAX(rchfv.Forecast_Volume)
            , MAX(rchfv.Uom_Id)
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.RM_Account_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
            LEFT JOIN Core.Client_Hier_Account cha
                ON afv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  afv.Account_Id = cha.Account_Id
            LEFT JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
                   AND  suppacc.Account_Type = 'Supplier'
            LEFT JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = suppacc.Supplier_Contract_ID
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON rchfv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = rchfv.Commodity_Id
                   AND  rchfv.Service_Month = afv.Service_Month
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND (   @Hedge_Contract_Sites_Cnt > 0
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        @Trade_tvp_Hedge_Contract_Sites hcs
                                   WHERE
                                        suppacc.Supplier_Contract_ID = hcs.Contract_Id
                                        AND ch.Site_Id = hcs.Site_Id)
                    AND afv.Service_Month BETWEEN con.CONTRACT_START_DATE
                                          AND     con.CONTRACT_END_DATE)
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                  AND     chhc.Config_End_Dt
        GROUP BY
            afv.Client_Hier_Id
            , afv.Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume
            , afv.Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100;


        ----------------------To get site own config  
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Max_Hedge_Pct
                 , Max_Hedge_Volume
                 , Site_Forecast_Volume
                 , Site_Uom_Id
             )
        SELECT
            afv.Client_Hier_Id
            , -1 AS Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume AS Forecast_Volume
            , afv.Uom_Id AS Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100
            , afv.Forecast_Volume
            , afv.Uom_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND (EXISTS (SELECT 1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                  AND     chhc.Config_End_Dt
            AND NOT EXISTS (SELECT  1 FROM  #Volumes accch WHERE afv.Client_Hier_Id = accch.Client_Hier_Id)
        GROUP BY
            afv.Client_Hier_Id
            , afv.Service_Month
            , afv.Forecast_Volume
            , afv.Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100;



        ----------------------To get default config                         
        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Service_Month
                 , Forecast_Volume
                 , Uom_Id
                 , Max_Hedge_Pct
                 , Max_Hedge_Volume
                 , Site_Forecast_Volume
                 , Site_Uom_Id
             )
        SELECT
            afv.Client_Hier_Id
            , -1 AS Account_Id
            , afv.Service_Month
            , afv.Forecast_Volume AS Forecast_Volume
            , afv.Uom_Id AS Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100
            , afv.Forecast_Volume
            , afv.Uom_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chob.Commodity_Id = afv.Commodity_Id
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND (   (   @Hedge_Type_Input = 'Physical'
                        AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ))
                    OR  (   @Hedge_Type_Input = 'Financial'
                            AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND (EXISTS (SELECT 1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
            AND afv.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND afv.Service_Month BETWEEN chhc.Config_Start_Dt
                                  AND     chhc.Config_End_Dt
            AND NOT EXISTS (SELECT  1 FROM  #Volumes accch WHERE afv.Client_Hier_Id = accch.Client_Hier_Id)
        GROUP BY
            afv.Client_Hier_Id
            , afv.Service_Month
            , afv.Forecast_Volume
            , afv.Uom_Id
            , chhc.Max_Hedge_Pct
            , (chhc.Max_Hedge_Pct * afv.Forecast_Volume) / 100;



        SELECT  @Common_Uom_Id = MAX(Uom_Id)FROM    #Volumes;

        SELECT
            Client_Hier_Id
            , Service_Month
            , MAX(Site_Uom_Id) AS Uom_Id
            , MAX(Site_Forecast_Volume) AS Site_Forecast_Volume
        INTO
            #sitevols
        FROM
            #Volumes
        GROUP BY
            Client_Hier_Id
            , Service_Month;

        SELECT
            dd.DATE_D AS Service_Month
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL(SUM(vol.Forecast_Volume * cuc.CONVERSION_FACTOR), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(SUM(vol.Forecast_Volume * cuc.CONVERSION_FACTOR) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Forecast_Volume
            , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                       CAST(ISNULL(SUM(Max_Hedge_Volume * cuc.CONVERSION_FACTOR), 0) AS DECIMAL(28, 0))
                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                      CAST(ISNULL(SUM(Max_Hedge_Volume * cuc.CONVERSION_FACTOR) / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
              END AS Max_Hedge_Volume
            , MAX(vol.Max_Hedge_Pct) AS Max_Hedge_Pct
            , MAX(sitvol.Site_Forecast_Volume) AS Site_Forecast_Volume
        FROM
            meta.DATE_DIM dd
            LEFT JOIN #Volumes vol
                ON dd.DATE_D = vol.Service_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = vol.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, @Common_Uom_Id)
            LEFT JOIN
            (   SELECT
                    sitvol.Service_Month
                    , CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN
                               CAST(ISNULL(SUM(sitvol.Site_Forecast_Volume * sitcuc.CONVERSION_FACTOR), 0) AS DECIMAL(28, 0))
                          WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                              CAST(ISNULL(
                                       SUM(sitvol.Site_Forecast_Volume * sitcuc.CONVERSION_FACTOR)
                                       / dd.DAYS_IN_MONTH_NUM, 0) AS DECIMAL(28, 0))
                      END AS Site_Forecast_Volume
                FROM
                    #sitevols sitvol
                    LEFT JOIN meta.DATE_DIM dd
                        ON dd.DATE_D = sitvol.Service_Month
                    LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION sitcuc
                        ON sitcuc.BASE_UNIT_ID = sitvol.Uom_Id
                           AND  sitcuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, @Common_Uom_Id)
                GROUP BY
                    sitvol.Service_Month
                    , dd.DAYS_IN_MONTH_NUM) sitvol
                ON dd.DATE_D = sitvol.Service_Month
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt
        GROUP BY
            dd.DATE_D
            , dd.DAYS_IN_MONTH_NUM
        ORDER BY
            dd.DATE_D;

        DROP TABLE #RM_Group_Sites;
        DROP TABLE #Volumes;
        DROP TABLE #sitevols;
    END;












GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volumes_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
