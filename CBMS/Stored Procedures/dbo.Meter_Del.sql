SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Meter_Del]  
     
DESCRIPTION:

	It Deletes Meter for given Meter Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Meter_Id		INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Meter_Del 14977
	ROLLBACK TRAN

	SELECT
		*
	FROM
		METER m
	WHERE
		NOT EXISTS(SELECT 1 FROM METER_CBMS_IMAGE_MAP cim WHERE cim.meter_id = m.meter_id)
		AND NOT EXISTS(SELECT 1 FROM SR_RFP_ACCOUNT_METER_MAP cim WHERE cim.meter_id = m.meter_id)

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Meter_Del
    (
      @Meter_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
		dbo.METER
	WHERE
		METER_ID = @Meter_Id

END
GO
GRANT EXECUTE ON  [dbo].[Meter_Del] TO [CBMSApplication]
GO
