SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.CHECK_END_DATE_NULL_P
	@rateId INT,
	@rsStartDate DATETIME
AS
BEGIN

	SET NOCOUNT ON

	SELECT rs_start_date
	FROM dbo.rate_schedule
	WHERE rate_id = @rateId
		AND rs_start_date < @rsStartDate 
		AND rs_end_date IS NULL

END

GO
GRANT EXECUTE ON  [dbo].[CHECK_END_DATE_NULL_P] TO [CBMSApplication]
GO
