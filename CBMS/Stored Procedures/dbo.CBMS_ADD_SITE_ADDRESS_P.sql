
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********       
NAME:      
 dbo.CBMS_ADD_SITE_ADDRESS_P      
  
DESCRIPTION:  This procedure used to insert the records in address table   
  
IINPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@addressTypeId   int,    
@stateId    int,    
@addressLine1   varchar(200),    
@addressLine2   varchar(200),    
@city     varchar(80),    
@zipcode    varchar(20),    
@addressParentId  int,    
@addressParentTypeId int,    
@latitude    decimal(32,16),    
@longitude    decimal(32,16),   
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    @addressId   INT  
      
USAGE EXAMPLES:      
------------------------------------------------------------      
  BEGIN TRAN
  DECLARE  @addressId int
  SELECT * FROM ADDRESS  where Address_Type_Id = 54 and Address_Line1 = '13100 E. Michigan Ave.' and STATE_ID = 35   

  EXEC dbo.CBMS_ADD_SITE_ADDRESS_P 
      @addressTypeId = 54
      ,@stateId = 35
     ,@addressLine1 = '13100 E. Michigan Ave.'
     ,@addressLine2 = '13100 E. Michigan Ave.'
     ,@city = 'Galesburg'
     ,@zipcode = '49053'
     ,@addressParentId = 638
     ,@addressParentTypeId = 235
     ,@latitude = NULL
     ,@longitude = NULL
     ,@Is_System_Generated_Geocode = 1
     ,@AddressId = @addressID OUTPUT
     
  SELECT * FROM ADDRESS  where Address_Type_Id = 54 and Address_Line1 = '13100 E. Michigan Ave.' and STATE_ID = 35   
      
  RollBack TRAN
Initials Name      
------------------------------------------------------------      
SS   Subhash Subramanyam           
HG   Harihara Suthan Ganeshan       
MA   Mohammed Aman  
SKA  Shobhit Kr Agrawal
RKV  Ravi Kumar Vegesna  
  
Initials Date  Modification      
------------------------------------------------------------      
  
SKA 15/07/2009  Removed the column @squarefootage which was using as input paramater as per Bug #9936  
SKA 16/07/2009  Modified comment section as per changes  
    Added comment section  
DR    8/4/2009    Removed Linked Server Updates  
DMR    09/10/2010 Modified for Quoted_Identifier
RKV 2014-01-10 MAINT-2444 Added New Column @Is_System_Generated_Geocode			      
  
            
******/  
CREATE PROCEDURE [dbo].[CBMS_ADD_SITE_ADDRESS_P]
      @addressTypeId INT
     ,@stateId INT
     ,@addressLine1 VARCHAR(200)
     ,@addressLine2 VARCHAR(200)
     ,@city VARCHAR(80)
     ,@zipcode VARCHAR(20)
     ,@addressParentId INT
     ,@addressParentTypeId INT
     ,@latitude DECIMAL(32, 16)
     ,@longitude DECIMAL(32, 16)
     ,@addressId INT OUT
     ,@Is_System_Generated_Geocode BIT = 1
AS 
INSERT      INTO address
            ( 
             ADDRESS_TYPE_ID
            ,STATE_ID
            ,ADDRESS_LINE1
            ,ADDRESS_LINE2
            ,CITY
            ,ZIPCODE
            ,ADDRESS_PARENT_ID
            ,ADDRESS_PARENT_TYPE_ID
            ,GEO_LAT
            ,GEO_LONG
            ,Is_System_Generated_Geocode )
VALUES
            ( 
             @addressTypeId
            ,@stateId
            ,@addressLine1
            ,@addressLine2
            ,@city
            ,@zipcode
            ,@addressParentId
            ,@addressParentTypeId
            ,@latitude
            ,@longitude
            ,@Is_System_Generated_Geocode )    
    
SELECT
      @addressId = SCOPE_IDENTITY()  
      
      

       
   
RETURN 

;
GO

GRANT EXECUTE ON  [dbo].[CBMS_ADD_SITE_ADDRESS_P] TO [CBMSApplication]
GO
