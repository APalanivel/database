SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******            
NAME:    [Workflow].[Workflow_Dynamic_Priority_Cu_Invoice_Attribute]        
DESCRIPTION: This stored procedure is created to update the priority of the invoice        
        
the inputs will come with comma delimited if multiple invoices which need to maek/remove priority        
------------------------------------------------------------           
 INPUT PARAMETERS:            
 Name   DataType  Default Description            
  @CU_INVOICE_ID INT,        
  @User_Id INT         
------------------------------------------------------------            
 OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 USAGE EXAMPLES:            
 EXEC   [Workflow].[Workflow_Dynamic_Priority_Cu_Invoice_Attribute]    @CU_INVOICE_ID = 5508231      
------------------------------------------------------------            
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
TRK RAMAKRISHNA THUMMALA Summit Energy      
AP ARUNKUMAR PALANIVEL       
         
 MODIFICATIONS             
 Initials Date   Modification            
------------------------------------------------------------      
        
 TRK    03-OCT-2019 Created.     
 AP OCT 11 2019  MODIFIED JOIN CONDITION TO TAKE CLIENT ID 
 AP D20-1486 System Priority flag should not be removed until an invoice is either posted or closed      
        
******/
CREATE PROCEDURE [Workflow].[Workflow_Dynamic_Priority_Cu_Invoice_Attribute]
      (
      @CU_INVOICE_ID INT
    , @User_ID       INT )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Proc_name     VARCHAR(100) = 'Workflow_Dynamic_Priority_Cu_Invoice_Attribute'
                , @Input_Params  VARCHAR(1000)
                , @Error_Line    INT
                , @Error_Message VARCHAR(3000);

            SELECT
                  @Input_Params = '@CU_INVOICE_ID:' + cast(@CU_INVOICE_ID AS VARCHAR) + '|' + '@User_ID:' + cast(@User_ID AS VARCHAR);

            DECLARE
                  @Source_Cd          INT
                , @Userprioritymanual INT
                , @Client_Id          INT;
            SELECT
                  @Source_Cd = Code_Id
            FROM  dbo.Code
            WHERE Code_Value = 'Systempriorityclient';
            SELECT
                  @Userprioritymanual = Code_Id
            FROM  dbo.Code
            WHERE Code_Value = 'Userprioritymanual';

			
                  SELECT
                        @Client_Id = CI.CLIENT_ID
                  FROM  dbo.CU_INVOICE CI WITH ( NOLOCK )
                  WHERE CI.CU_INVOICE_ID = @CU_INVOICE_ID;


            --/*System Priority flag should not be removed until an invoice is either posted or closed*/
            --IF EXISTS
            --      (     SELECT
            --                  1
            --            FROM  Workflow.Cu_Invoice_Attribute
            --            WHERE CU_INVOICE_ID = @CU_INVOICE_ID
            --                  AND   Source_Cd = @Source_Cd )
            --      RETURN 0;

            BEGIN TRY



                  IF @User_ID IS NULL
                        BEGIN
                              SET @User_ID = 16;
                        END;

                


                  IF ( @Client_Id IS NULL )
                        BEGIN
                              IF EXISTS
                                    (     SELECT
                                                1
                                          FROM  Workflow.Cu_Invoice_Attribute
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID
                                                AND   Is_Priority = 1
                                                AND   Source_Cd = @Source_Cd )
                                    BEGIN
                                          UPDATE
                                                Workflow.Cu_Invoice_Attribute
                                          SET
                                                Is_Priority = 0
                                              , Updated_User_Id = @User_ID
                                              , Last_Change_Ts = getdate()
                                              , Source_Cd = @Userprioritymanual
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID;
                                    END;

                              RETURN;
                        END;


                  IF NOT EXISTS
                        (     SELECT
                                    1
                              FROM  Client_Attribute
                              WHERE Client_Id = @Client_Id )
                        BEGIN

                              IF EXISTS
                                    (     SELECT
                                                1
                                          FROM  Workflow.Cu_Invoice_Attribute
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID
                                                AND   Source_Cd = @Source_Cd )
                                    BEGIN
                                          UPDATE
                                                Workflow.Cu_Invoice_Attribute
                                          SET
                                                Is_Priority = 0
                                              , Updated_User_Id = @User_ID
                                              , Last_Change_Ts = getdate()
                                              , Source_Cd = @Userprioritymanual
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID;
                                    END;

                              RETURN;

                        END;
                  --  PRINT 'client attribute mapped'      
                  --  IF ( 1 = (SELECT Mark_Invoice_Exception_As_Priority FROM Client_Attribute WHERE Client_Id = @client_id) )      
                  --  BEGIN      

                  --IF EXISTS (SELECT 1 FROM Workflow.Cu_Invoice_Attribute WHERE CU_INVOICE_ID=@CU_INVOICE_ID       
                  --AND ISNULL(Source_Cd,@Userprioritymanual) = @Userprioritymanual)      
                  --RETURN;   

                  --  PRINT 'manual priority not linked'    
                  IF EXISTS
                        (     SELECT
                                    1
                              FROM  dbo.Client_Attribute
                              WHERE Client_Id = @Client_Id
                                    AND   Mark_Invoice_Exception_As_Priority = 1 )
                        BEGIN

                              IF NOT EXISTS
                                    (     SELECT
                                                1
                                          FROM  Workflow.Cu_Invoice_Attribute
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID )
                                    BEGIN
                                          INSERT INTO Workflow.Cu_Invoice_Attribute (
                                                                                          CU_INVOICE_ID
                                                                                        , Is_Priority
                                                                                        , Created_User_Id
                                                                                        , Created_Ts
                                                                                        , Updated_User_Id
                                                                                        , Last_Change_Ts
                                                                                        , Source_Cd
                                                                                    )
                                          VALUES
                                               ( @CU_INVOICE_ID, 1, @User_ID, getdate(), @User_ID, getdate(), @Source_Cd );
                                    END;
                              ELSE
                                    BEGIN
                                          UPDATE
                                                Workflow.Cu_Invoice_Attribute
                                          SET
                                                Is_Priority = 1
                                              , Source_Cd = @Source_Cd
                                              , Updated_User_Id = @User_ID
                                              , Last_Change_Ts = getdate()
                                          WHERE CU_INVOICE_ID = @CU_INVOICE_ID;
                                    END;
                        END;
            --ELSE  
            --BEGIN   

            -- IF EXISTS (SELECT 1 FROM Workflow.Cu_Invoice_Attribute WHERE CU_INVOICE_ID=@CU_INVOICE_ID       
            --  AND Source_Cd = @Source_Cd)  

            --  BEGIN  
            --   UPDATE Workflow.Cu_Invoice_Attribute SET Is_Priority = 0,Updated_User_Id = @User_ID , Last_Change_Ts = getdate() WHERE CU_INVOICE_ID=@CU_INVOICE_ID  
            --  END  

            --END  




            END TRY
            BEGIN CATCH
                  -- ENTRY MADE TO THE LOGGING SP TO CAPTURE THE ERRORS.               
                  SELECT
                        @Error_Line = error_line()
                      , @Error_Message = error_message();

                  INSERT INTO StoredProc_Error_Log (
                                                         StoredProc_Name
                                                       , Error_Line
                                                       , Error_message
                                                       , Input_Params
                                                   )
                  VALUES
                       ( @Proc_name, @Error_Line, @Error_Message, @Input_Params );

                  RETURN -99;
            END CATCH;
      END;
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Dynamic_Priority_Cu_Invoice_Attribute] TO [CBMSApplication]
GO
