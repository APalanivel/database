SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Comment_Ins                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Deal_Ticket_Id		INT
    @Comment_Id		INT
    @User_Info_Id	INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	DECLARE @Deal_Ticket_Id INT
	EXEC Trade.Deal_Ticket_Comment_Ins 
	SELECT * FROM Trade.Deal_Ticket		
	SELECT @Deal_Ticket_Id
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-14     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Comment_Ins]
      ( 
       @Deal_Ticket_Id INT
      ,@Comment_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      INSERT      INTO Trade.Deal_Ticket_Comment
                  ( 
                   Deal_Ticket_Id
                  ,Comment_Id
                  ,Created_User_Id
                  ,Created_Ts )
      VALUES
                  ( 
                   @Deal_Ticket_Id
                  ,@Comment_Id
                  ,@User_Info_Id
                  ,GETDATE() )
                       
                  
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Comment_Ins] TO [CBMSApplication]
GO
