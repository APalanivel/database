SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME: [DBO].[Utility_Account_History_Del_By_Account_Id]    
       
DESCRIPTION:  
  
 To Delete all the history associated with the utility account  
  
INPUT PARAMETERS:            
NAME   DATATYPE  DEFAULT  DESCRIPTION            
------------------------------------------------------------            
@Account_Id  INT  
@User_Info_Id INT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 BEGIN TRAN      
  EXEC dbo.Utility_Account_History_Del_By_Account_Id 109664,49  
 ROLLBACK TRAN  
   
 BEGIN TRAN      
  
  EXEC dbo.Utility_Account_History_Del_By_Account_Id 109400,49  
  
 ROLLBACK TRAN  
   
 BEGIN TRAN      
  SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007132  
  EXEC dbo.Utility_Account_History_Del_By_Account_Id 1007132,49  
  SELECT * FROM dbo.Account_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1007132  
 ROLLBACK TRAN  
       
    BEGIN TRAN      
  SELECT * FROM dbo.Account_Exception where  Account_Id = 1148583  
  EXEC dbo.Utility_Account_History_Del_By_Account_Id 1148583,49  
  SELECT * FROM dbo.Account_Exception where Account_Id = 1148583  
 ROLLBACK TRAN   
       
       
     BEGIN TRAN      
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where  Account_Id = 208656  
  EXEC dbo.Utility_Account_History_Del_By_Account_Id 208656,49  
        SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where  Account_Id = 208656  
 ROLLBACK TRAN    
       
	   BEGIN TRAN      
	   SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where   Account_Id = 1338934 
		SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map where  Cu_Invoice_Account_Commodity_Comment_Id = 670188  
		EXEC dbo.Utility_Account_History_Del_By_Account_Id 1338934,16  
        SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment where  Account_Id = 1338934  
		SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map where  Cu_Invoice_Account_Commodity_Comment_Id = 670188 
 ROLLBACK TRAN    
       
AUTHOR INITIALS:  
	INITIALS	NAME  
------------------------------------------------------------  
	HG			Harihara Suthan G  
	BCH			Balaraju Chalumuri  
	AKR         Ashok Kumar Raju  
	RR			Raghu Reddy  
	RKV         Ravi Kumar Vegesna  
	NR			Narayana Reddy
	ABK			Aditya Bharadwaj  

	MODIFICATIONS	DATE		MODIFICATION  
------------------------------------------------------------  
	HG				06/07/2010	Created  
	BCH				2011-12-30  Deleted transactions (delete tool enhancement - Transactions are maintained in Application)  
	AKR				2012-10-01  Modified to Delete data from Account_Commodity_Analyst while deleting a Account as a part of POCO  
	BCH				2012-10-05  Modified for using the incrementing the rownum for deletion instead of deleting the data from table varibale.  
	BCH				2015-06-19  Modified script to call dbo.Account_Comment_Del, to delete from table dbo.Account_Comment created for AS400  
	RKV				2015-10-29  Modified script to delete the given account from the tables dbo.Account_Exception and Account_Commodity_Invoice_Recalc_Type  
								as part of AS400-PII  
	NR				2016-12-22	MAINT-4741 To delete the Cu_Invoice_Account_Commodity_Comment table data when utility account is deleted.   
	RKV				2017-03-09	Added Account Inovice Collection History as Part of Contract PlaceHolder  
	ABK				2019-06-14	SE2017-757:   Added code to delete data from Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map table
	RR				2019-08-29	MAINT-9168 @Lookup_Value for does not accept null/empty values, if there is no meter script is failing as the
								@Lookup_Value string value is null, to continue delete account meter table join changed to left
	NR				2020-06-05	MAINT-10360 - Deleted OCT rules when account is deleted.							 
*/
CREATE PROCEDURE [dbo].[Utility_Account_History_Del_By_Account_Id]
    (
        @Account_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Application_Name VARCHAR(30) = 'Delete Utility Account'
            , @Audit_Function SMALLINT = -1
            , @User_Name VARCHAR(81)
            , @Client_Name VARCHAR(200)
            , @Current_Ts DATETIME
            , @Table_Name VARCHAR(10) = 'ACCOUNT'
            , @Lookup_Value XML
            , @Total_Row_Count INT
            , @Row_Counter INT
            , @Account_Exception_Id INT;

        DECLARE
            @Meter_Id INT
            , @Account_Entity_Id INT
            , @Commodity_Id INT
            , @Acco_Broker_Commodity_Id INT
            , @Account_Commodity_Invoice_Recalc_Type_Id INT;

        DECLARE @Meter_List TABLE
              (
                  Meter_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Account_Commodity_Analyst_List TABLE
              (
                  Commodity_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Account_Commodity_Broker_Fee_List TABLE
              (
                  Commodity_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        DECLARE @Account_Exception TABLE
              (
                  Account_Exception_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );


        DECLARE @Account_Commodity_Invoice_Recalc_Type TABLE
              (
                  Account_Commodity_Invoice_Recalc_Type_Id INT PRIMARY KEY CLUSTERED
                  , Row_Num INT IDENTITY(1, 1)
              );

        SET @Lookup_Value = (   SELECT
                                    SITE.SITE_NAME
                                    , ACCOUNT.ACCOUNT_NUMBER
                                    , ACCOUNT.ACCOUNT_ID
                                    , ISNULL(METER.METER_NUMBER, 'No Meter Found') AS METER_NUMBER
                                FROM
                                    SITE
                                    INNER JOIN ACCOUNT
                                        ON SITE.SITE_ID = ACCOUNT.SITE_ID
                                    LEFT JOIN METER
                                        ON METER.ACCOUNT_ID = ACCOUNT.ACCOUNT_ID
                                WHERE
                                    ACCOUNT.ACCOUNT_ID = @Account_Id
                                FOR XML AUTO);


        SELECT
            @Account_Entity_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_NAME = 'ACCOUNT_TABLE'
            AND ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @Client_Name = CLIENT_NAME
        FROM
            dbo.ACCOUNT acc
            JOIN dbo.SITE s
                ON s.SITE_ID = acc.SITE_ID
            JOIN dbo.CLIENT cl
                ON cl.CLIENT_ID = s.Client_ID
        WHERE
            acc.ACCOUNT_ID = @Account_Id;

        SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
        FROM
            dbo.USER_INFO
        WHERE
            USER_INFO_ID = @User_Info_Id;

        INSERT INTO @Meter_List
             (
                 Meter_Id
             )
        SELECT  METER_ID FROM   dbo.METER WHERE ACCOUNT_ID = @Account_Id;


        INSERT INTO @Account_Commodity_Analyst_List
             (
                 Commodity_Id
             )
        SELECT
            aca.Commodity_Id
        FROM
            dbo.Account_Commodity_Analyst aca
        WHERE
            aca.Account_Id = @Account_Id;

        INSERT INTO @Account_Commodity_Broker_Fee_List
             (
                 Commodity_Id
             )
        SELECT
            Commodity_Id
        FROM
            dbo.Account_Commodity_Broker_Fee
        WHERE
            Account_Id = @Account_Id;

        INSERT INTO @Account_Exception
             (
                 Account_Exception_Id
             )
        SELECT
            ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Account_Id = @Account_Id;

        INSERT INTO @Account_Commodity_Invoice_Recalc_Type
             (
                 Account_Commodity_Invoice_Recalc_Type_Id
             )
        SELECT
            Account_Commodity_Invoice_Recalc_Type_Id
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            Account_Id = @Account_Id;



        BEGIN TRY


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Meter_List;
            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT  @Meter_Id = Meter_Id FROM   @Meter_List WHERE   Row_Num = @Row_Counter;

                    EXEC dbo.Meter_History_Del_By_Meter_Id @Meter_Id, @User_Info_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;


            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Account_Exception;

            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN
                    SELECT
                        @Account_Exception_Id = Account_Exception_Id
                    FROM
                        @Account_Exception
                    WHERE
                        Row_Num = @Row_Counter;

                    DELETE
                    dbo.Account_Exception
                    WHERE
                        Account_Exception_Id = @Account_Exception_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;


            SELECT
                @Total_Row_Count = MAX(Row_Num)
            FROM
                @Account_Commodity_Invoice_Recalc_Type;
            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Account_Commodity_Invoice_Recalc_Type_Id = Account_Commodity_Invoice_Recalc_Type_Id
                    FROM
                        @Account_Commodity_Invoice_Recalc_Type
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Del
                        @Account_Commodity_Invoice_Recalc_Type_Id
                        , @User_Info_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;


            -- Budget History delete for account  
            EXEC dbo.Budget_History_Del_For_Account @Account_Id;

            -- Cost/Usage History delete for account  
            EXEC dbo.Cost_Usage_History_Del_For_Account @Account_Id;

            EXEC dbo.Sr_Rfp_History_Del_For_Account @Account_Id;

            EXEC dbo.Invoice_Participation_History_Del_For_Account @Account_Id;

            EXEC dbo.Account_Other_History_Del_By_Account_Id @Account_Id;

            EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
                @Account_Entity_Id
                , @Account_Id;

            SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Account_Commodity_Analyst_List;
            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Commodity_Id = al.Commodity_Id
                    FROM
                        @Account_Commodity_Analyst_List al
                    WHERE
                        Row_Num = @Row_Counter;

                    EXEC dbo.Account_Commodity_Analyst_Del @Account_Id, @Commodity_Id;

                    SET @Row_Counter = @Row_Counter + 1;
                END;


            SELECT
                @Total_Row_Count = MAX(Row_Num)
            FROM
                @Account_Commodity_Broker_Fee_List;
            SET @Row_Counter = 1;
            WHILE (@Row_Counter <= @Total_Row_Count)
                BEGIN

                    SELECT
                        @Acco_Broker_Commodity_Id = al.Commodity_Id
                    FROM
                        @Account_Commodity_Broker_Fee_List al
                    WHERE
                        Row_Num = @Row_Counter;


                    EXEC dbo.Account_Commodity_Broker_Fee_Del
                        @Account_Id
                        , @Acco_Broker_Commodity_Id;

                    SET @Row_Counter = @Row_Counter + 1;

                END;

            EXEC dbo.Account_Comment_Del @Account_Id;

            DELETE
            ciaccrstm
            FROM
                dbo.Cu_Invoice_Account_Commodity_Comment ciacc
                INNER JOIN dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map ciaccrstm
                    ON ciaccrstm.Cu_Invoice_Account_Commodity_Comment_Id = ciacc.Cu_Invoice_Account_Commodity_Comment_Id
            WHERE
                Account_Id = @Account_Id;

            DELETE
            ciacc
            FROM
                dbo.Cu_Invoice_Account_Commodity_Comment ciacc
            WHERE
                Account_Id = @Account_Id;

            EXEC dbo.Account_Inovice_Collection_History_Del_By_Account_Id
                @Account_Id = @Account_Id;

            DELETE
            aoctc
            FROM
                dbo.Account_Outside_Contract_Term_Config aoctc
            WHERE
                aoctc.Account_Id = @Account_Id;



            EXEC dbo.Account_Del @Account_Id;

            SET @Current_Ts = GETDATE();

            EXEC dbo.Application_Audit_Log_Ins
                @Client_Name
                , @Application_Name
                , @Audit_Function
                , @Table_Name
                , @Lookup_Value
                , @User_Name
                , @Current_Ts;


        END TRY
        BEGIN CATCH


            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;




GO






GRANT EXECUTE ON  [dbo].[Utility_Account_History_Del_By_Account_Id] TO [CBMSApplication]
GO
