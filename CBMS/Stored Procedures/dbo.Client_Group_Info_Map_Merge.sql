
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                       
/******                        
 NAME: dbo.Client_Group_Info_Map_Merge            
                        
 DESCRIPTION:                        
			To update the Client_Group_Info_Map table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id                  INT              
 @Group_Info_Id              VARCHAR(MAX)       
 @Assigned_User_Id           INT                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 ----Example 1------      
	 BEGIN TRAN          
	        
	 SELECT          
		  *          
	 FROM          
		  dbo.Client_Group_Info_Map          
	 WHERE          
		  Client_Id = 235
		  AND Group_Info_Id =211


		SELECT
			  ui.client_id
			 ,ui.Client_App_Access_Role_Id
			 ,ui.App_Access_Role_Name
			 ,uigi.Group_Info_Id
		FROM
			  dbo.Client_App_Access_Role_Group_Info_Map uigi
			  INNER JOIN Client_App_Access_Role ui
					ON uigi.Client_App_Access_Role_Id = ui.Client_App_Access_Role_Id
		WHERE
			  ui.client_id = 235
			  AND uigi.Group_Info_Id =211


	   SELECT  
		COUNT(uigi.User_Info_Id) AS Users,uigi.Group_Info_Id  
	   FROM  
		dbo.User_Info_Group_Info_Map uigi  
		INNER JOIN User_Info ui  
		   ON uigi.User_info_id = ui.user_info_id  
	   WHERE  
		ui.client_id = 235  
		AND access_level = 1 
		AND uigi.Group_Info_Id =211 	
		GROUP BY uigi.Group_Info_Id  	  
		  
	                
	 EXEC dbo.Client_Group_Info_Map_Merge           
		  @Client_Id = 235          
		 ,@Group_Info_Id = '212,213,214,215,217,219,220,226,227,229,230,231,233,234,235,237,238,244,246,247,250,252,253,256,258,259,260,261,262,263,264,266,267,268,271,278,286,298,301,304,305,310,336,349,354,361,366,371,372'          
		 ,@Assigned_User_Id=182 
        
	               
	        
	 SELECT          
		  *          
	 FROM          
		  dbo.Client_Group_Info_Map          
	 WHERE          
		  Client_Id = 235
		  AND Group_Info_Id =211


		SELECT
			  ui.client_id
			 ,ui.Client_App_Access_Role_Id
			 ,ui.App_Access_Role_Name
			 ,uigi.Group_Info_Id
		FROM
			  dbo.Client_App_Access_Role_Group_Info_Map uigi
			  INNER JOIN Client_App_Access_Role ui
					ON uigi.Client_App_Access_Role_Id = ui.Client_App_Access_Role_Id
		WHERE
			  ui.client_id = 235
			  AND uigi.Group_Info_Id =211


	   SELECT  
		COUNT(uigi.User_Info_Id) AS Users,uigi.Group_Info_Id  
	   FROM  
		dbo.User_Info_Group_Info_Map uigi  
		INNER JOIN User_Info ui  
		   ON uigi.User_info_id = ui.user_info_id  
	   WHERE  
		ui.client_id = 235  
		AND access_level = 1 
		AND uigi.Group_Info_Id =211 	
		GROUP BY uigi.Group_Info_Id  	  
		   	     		         
		  
	 ROLLBACK TRAN       



BEGIN TRAN

EXEC Client_Group_Info_Map_Merge
      @Client_Id = '218'
     ,@Group_Info_Id = '252,263,212,274,335,354,355,302,301,213,326,218,215,214,356,336,216,217,337,374,365,219,281,278,283,379,327,277,328,366,304,298,305,292,293,280,290,291,279,235,226,227,318,315,237,311,314,316,253,310,313,317,312,229,230,231,233,234,324,285,288,289,284,276,371,372,256,246,247,250,244,258,294,295,282,259,260,308,261,286,262,345,341,266,268,373,271,350,220,331,334,333,323'
     ,@Assigned_User_Id = '49'


ROLLBACK
 
                       
 AUTHOR INITIALS:        
       
 Initials           Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                 Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials           Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                 2013-11-25     Created for RA Admin user management 
 SP					2014-03-19		MAINT-2677 Called Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group
										inside the sp by taking the deleted Groups from Merge.                   
 HG					2015-03-11		MAINT-3444, When a group is added and removed from the client  @Deleted_Group_Info_Ids variables becomes NULL as the deleted.Group_Info_id for the new record will be NULL. Fixed the code to get only the Group_Info_Id which are not NULL from the table variable.        
******/                        

CREATE PROCEDURE [dbo].[Client_Group_Info_Map_Merge]
      (
       @Client_Id INT
      ,@Group_Info_Id VARCHAR(MAX)
      ,@Assigned_User_Id INT )
AS
BEGIN
                
      SET NOCOUNT ON;  

      DECLARE @Deleted_Group_Info_Ids VARCHAR(MAX) = ''        

      DECLARE @Client_Group_Info_Map_List TABLE
            (
             Client_Id INT
            ,Group_Info_Id INT )   

      DECLARE @Deleted_Group_Info_Id_Tbl TABLE ( Group_Info_Id INT )                              
                    
      INSERT      INTO @Client_Group_Info_Map_List
                  ( Client_Id
                  ,Group_Info_Id )
                  SELECT
                        @Client_Id
                       ,ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id, ',') ufn                 

      BEGIN TRY                          
            BEGIN TRANSACTION                
                   
            MERGE INTO dbo.Client_Group_Info_Map AS tgt
            USING
                  ( SELECT
                        urr.Client_Id
                       ,urr.Group_Info_Id
                    FROM
                        @Client_Group_Info_Map_List urr ) AS src
            ON tgt.Client_Id = src.Client_Id
                  AND tgt.Group_Info_Id = src.Group_Info_Id
            WHEN NOT MATCHED BY SOURCE AND  tgt.Client_Id = @Client_Id THEN
                  DELETE
            WHEN NOT MATCHED BY TARGET THEN
                  INSERT
                         ( Client_Id
                         ,Group_Info_Id
                         ,Assigned_User_Id
                         ,Assigned_Ts )
                  VALUES ( src.Client_Id
                         ,src.Group_Info_Id
                         ,@Assigned_User_Id
                         ,GETDATE() )
            OUTPUT
                  DELETED.Group_Info_Id
                  INTO @Deleted_Group_Info_Id_Tbl;  
		
            SELECT
                  @Deleted_Group_Info_Ids = CAST(Group_Info_Id AS VARCHAR(MAX)) + ',' + @Deleted_Group_Info_Ids
            FROM
                  @Deleted_Group_Info_Id_Tbl
            WHERE
                  Group_Info_Id IS NOT NULL	-- This is to exclude the DELETED.Group_Info_Id captured as NULL for the new groups assigned to the user
		   
            SELECT
                  @Deleted_Group_Info_Ids = LEFT(@Deleted_Group_Info_Ids, LEN(@Deleted_Group_Info_Ids) - 1)
            WHERE
                  LEN(@Deleted_Group_Info_Ids) > 1     
                  
            EXEC dbo.Create_Admin_FullAccess_Roles
                  @Client_Id = @Client_Id
                 ,@Group_Info_Id = @Group_Info_Id
                 ,@Assigned_User_Id = @Assigned_User_Id
                 
	                                              
            EXEC dbo.User_Info_Group_Info_Map_Del_By_Client_Group
                  @Client_Id = @Client_Id
                 ,@Group_Info_Id_List = @Deleted_Group_Info_Ids

            EXEC dbo.Client_App_Access_Role_Group_Info_Map_Del_By_Client_Group
                  @Client_Id = @Client_Id
                 ,@Group_Info_Id_List = @Deleted_Group_Info_Ids
                                   
            COMMIT TRANSACTION       

      END TRY                      
      BEGIN CATCH                      
            IF @@TRANCOUNT > 0
                  BEGIN 
                        ROLLBACK TRANSACTION      
                  END            
            EXEC dbo.usp_RethrowError                      
      END CATCH                                    

END;
;
GO


GRANT EXECUTE ON  [dbo].[Client_Group_Info_Map_Merge] TO [CBMSApplication]
GO
