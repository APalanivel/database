SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_VIEW_BIDS_TERMS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@termsId       	varchar(200)	          	
	@mode          	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC SR_RFP_GET_VIEW_BIDS_TERMS_P 1,1,'28729,39239,40802','View_SOP'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/
CREATE PROCEDURE dbo.SR_RFP_GET_VIEW_BIDS_TERMS_P
    @userId VARCHAR
  , @sessionId VARCHAR
  , @termsId VARCHAR(200)
  , @mode VARCHAR(200)
AS 
    BEGIN 
    
        SET nocount ON

        DECLARE @selectQuery VARCHAR(500)
        DECLARE @fromQuery VARCHAR(500)
        DECLARE @whereQuery VARCHAR(500)
        DECLARE @finalQuery VARCHAR(1500)



        SET @selectQuery = ' SELECT SR_RFP_ACCOUNT_TERM_ID,'
            + ' SR_RFP_ACCOUNT_TERM.FROM_MONTH,'
            + ' SR_RFP_ACCOUNT_TERM.TO_MONTH '

        SET @fromQuery = ' FROM SR_RFP_ACCOUNT_TERM '

        SET @whereQuery = +' where SR_RFP_ACCOUNT_TERM_ID IN (' + @termsId
            + ')  '

        IF @mode = 'View_SOP' 
            BEGIN
                SET @whereQuery = @whereQuery + ' AND IS_SOP=1 '
            END


        SET @finalQuery = @selectQuery + @fromQuery + @whereQuery
            + 'order by SR_RFP_ACCOUNT_TERM.NO_OF_MONTHS,SR_RFP_ACCOUNT_TERM.FROM_MONTH, SR_RFP_ACCOUNT_TERM.TO_MONTH  '
       
       
        EXEC ( @finalQuery )
    END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VIEW_BIDS_TERMS_P] TO [CBMSApplication]
GO
