SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
CREATE PROCEDURE [dbo].[cbmsChargeBucketList]    
 ( @commodity_Id INT    
 )    
AS    
BEGIN    
    
  SELECT bm.bucket_master_Id,bm.Bucket_Name, c.code_Value , bm.* FROM dbo.Bucket_Master bm     
  INNER JOIN code c    
  ON c.Code_Id = bm.Bucket_Type_Cd    
  WHERE (@commodity_Id = 0 or bm.Commodity_Id =  @commodity_Id) 
  AND bm.is_Active=1    
  AND c.code_Value ='Charge'
    
END 

GO
GRANT EXECUTE ON  [dbo].[cbmsChargeBucketList] TO [CBMSApplication]
GO
