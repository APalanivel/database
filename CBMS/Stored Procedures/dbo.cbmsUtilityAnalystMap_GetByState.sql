SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.cbmsUtilityAnalystMap_GetByState

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@state_id      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SKA			6 Aug 2009	Removed the unneeded alias from order by clause
	 DMR		  09/10/2010 Modified for Quoted_Identifier 	        	
******/
CREATE    PROCEDURE [dbo].[cbmsUtilityAnalystMap_GetByState]
( 
	@MyAccountId INT,
	@state_id INT
	)
AS

BEGIN
SET NOCOUNT ON
		SELECT v.vendor_id,
				v.vendor_name,
				map.Analyst_ID user_info_id,
				ui.first_name + ' ' + ui.last_name username,
				map.GROUP_INFO_ID,
				gi.GROUP_NAME	
				
				FROM vendor v
				JOIN vendor_state_map stmap 
					ON stmap.vendor_id = v.vendor_id
				JOIN state st 
					ON st.state_id = stmap.state_id
				JOIN utility_detail ud 
					ON ud.vendor_id = v.vendor_id
				LEFT OUTER JOIN utility_detail_analyst_map map 
					ON map.utility_detail_id = ud.utility_detail_id
				left outer JOIN GROUP_INFO gi on gi.GROUP_INFO_ID = map.Group_Info_ID
				LEFT OUTER JOIN user_info ui 
					ON ui.user_info_id = map.Analyst_ID
				WHERE st.state_id = @state_id
				ORDER BY vendor_name				

		--SELECT v.vendor_id,
		--   v.vendor_name,
		--   map.user_info_id,
		--   ui.first_name + ' ' + ui.last_name username,
		--   ui2.first_name + ' ' + ui2.last_name regulated_username,
		--   ui3.first_name + ' ' + ui3.last_name deregulated_username,
		--   ui4.first_name + ' ' + ui4.last_name sourcing_ops_username,
		--   map.regulated_analyst_id,
		--   map.deregulated_analyst_id,
		--   map.sourcing_ops_analyst_id,
		--   map.GROUP_INFO_ID
		--FROM vendor v
		--JOIN vendor_state_map stmap 
		--	ON stmap.vendor_id = v.vendor_id
		--JOIN state st 
		--	ON st.state_id = stmap.state_id
		--JOIN utility_detail ud 
		--	ON ud.vendor_id = v.vendor_id
		--LEFT OUTER JOIN utility_analyst_map map 
		--	ON map.utility_detail_id = ud.utility_detail_id
		--LEFT OUTER JOIN user_info ui 
		--	ON ui.user_info_id = map.user_info_id
		--LEFT OUTER JOIN user_info ui2 
		--	ON ui2.user_info_id = map.regulated_analyst_id
		--LEFT OUTER JOIN user_info ui3 
		--	ON ui3.user_info_id = map.deregulated_analyst_id
		--LEFT OUTER JOIN user_info ui4 
		--	ON ui4.user_info_id = map.sourcing_ops_analyst_id
		--WHERE st.state_id = @state_id
		--ORDER BY vendor_name
END
GO
GRANT EXECUTE ON  [dbo].[cbmsUtilityAnalystMap_GetByState] TO [CBMSApplication]
GO
