SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsCuInvoiceEvent_Save]
	( @MyAccountId int
	, @cu_invoice_id int
	, @event_description varchar(4000)
	)
AS
BEGIN

	set nocount on

	declare @this_id int
	
	insert into cu_invoice_event
		( cu_invoice_id
		, event_date
		, event_by_id
		, event_description
		)
	values	( @cu_invoice_id
		, getdate()
		, @MyAccountId
		, @event_description
		)

	set @this_id = @@IDENTITY

--	set nocount off

	exec cbmsCuInvoiceEvent_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceEvent_Save] TO [CBMSApplication]
GO
