SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                      
 NAME: dbo.[Invoice_Collection_Queue_Month_Map_Sel_Invoice_Collection_Queue_Id]                          
                                      
 DESCRIPTION:                                      
   To get Invoice_Collection_Queue_Month map details based on Invoice_Collection_Queue_Id 
                                      
 INPUT PARAMETERS:                        
                                   
 Name                        DataType         Default       Description                      
---------------------------------------------------------------------------------------------------------------                    
@Invoice_Collection_Queue_Id INT  NULL               
                                            
 OUTPUT PARAMETERS:                        
                                         
 Name                        DataType         Default       Description                      
---------------------------------------------------------------------------------------------------------------                    
                                      
 USAGE EXAMPLES:                                          
---------------------------------------------------------------------------------------------------------------                                          
                  
              
  EXEC dbo.[Invoice_Collection_Queue_Month_Map_Sel_Invoice_Collection_Queue_Id]               
      @Invoice_Collection_Queue_Id = 342              
                    
              
                                     
 AUTHOR INITIALS:                      
                     
 Initials				Name                      
---------------------------------------------------------------------------------------------------------------                                    
 
                                       
 MODIFICATIONS:                    
                        
 Initials				Date             Modification                    
---------------------------------------------------------------------------------------------------------------                    
    PR					2020-04-29		Created For Invoice_Collection.                       
                                     
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Month_Map_Sel_Invoice_Collection_Queue_Id]
    (@Invoice_Collection_Queue_Id INT)
AS
BEGIN
    SET NOCOUNT ON;


    SELECT
        Invoice_Collection_Queue_Id
       ,Account_Invoice_Collection_Month_Id
    FROM
        dbo.Invoice_Collection_Queue_Month_Map icqmm
    WHERE
        icqmm.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id;


END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Month_Map_Sel_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
