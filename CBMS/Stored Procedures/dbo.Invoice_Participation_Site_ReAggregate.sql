SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Invoice_Participation_Site_ReAggregate]  
     
DESCRIPTION:

	When an account is moved from one site to other, this SP is called to reaggregate the IP_Site data for both the sites
	It calls cbmsInvoiceParticipationSite_Save3 SP for every site/month combination for all the months that the account has an entry in IP table.

INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT
@Old_Site_Id	INT
@New_Site_Id	INT

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
BEGIN TRAN  
	SELECT * FROM INVOICE_PARTICIPATION_SITE WHERE SIte_id = 98121 AND SERVICE_MONTH = '2010-01-01'

		EXEC dbo.Invoice_Participation_Site_ReAggregate 9,

	SELECT * FROM INVOICE_PARTICIPATION_SITE WHERE SIte_id = 98121 AND SERVICE_MONTH = '2010-01-01'
ROLLBACK	
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------

***/

CREATE PROCEDURE [dbo].[Invoice_Participation_Site_ReAggregate]
	  (
	   @Account_Id INT
	  ,@Old_Site_Id INT
	  ,@New_Site_Id INT
	  )
AS
BEGIN
	
	  SET NOCOUNT ON

	  DECLARE
			@Loop INT = 1
		   ,@Max_Row_Id INT
		   ,@Service_Month DATE
		   ,@Site_Id INT

	  CREATE TABLE #IP_Service_Months
			(
			 Row_Id INT IDENTITY(1, 1)
			,Site_Id INT
			,Service_Month DATE
			)

	  INSERT	  #IP_Service_Months
				  ( Site_Id
				  ,Service_Month )
				  SELECT
						A.Site_Id
					   ,Service_Month
				  FROM
						dbo.INVOICE_PARTICIPATION AS ip
						CROSS JOIN ( SELECT
										  @Old_Site_Id AS Site_Id
									 UNION ALL
									 SELECT
										  @New_Site_Id ) A
				  WHERE
						ACCOUNT_ID = @Account_Id
				  GROUP BY
						A.Site_Id
					   ,Service_Month
		

	  SELECT
			@Max_Row_Id = MAX(Row_Id)
	  FROM
			#IP_Service_Months AS ism
		
	  WHILE ( @Loop <= @Max_Row_Id )
			BEGIN
		
				  SELECT
						@Site_Id = Site_Id
					   ,@Service_Month = Service_Month
				  FROM
						#IP_Service_Months AS ism
				  WHERE
						Row_Id = @Loop

				  EXEC dbo.cbmsInvoiceParticipationSite_Save3
						@MyAccountId = 0
					   , -- int
						@site_id = @Site_Id
					   , -- int
						@service_month = @Service_Month
				  

				  SELECT
						@Loop = @Loop + 1
		
			END
	  

END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Site_ReAggregate] TO [CBMSApplication]
GO
