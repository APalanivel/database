SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TOTAL_EXPECTED_USAGE_GROUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@groupId       	int       	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelId  	int       	          	
	@conversionId  	int       	          	
	@fromDate      	datetime  	          	
	@toDate        	datetime  	          	
	@fromyear      	int       	          	
	@toYear        	int       	          	
	@clientId      	int       	          	
	@dealticketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_TOTAL_EXPECTED_USAGE_GROUP_P -1,-1,41,587,842,25,'06/01/2006','05/01/2007',2006,2007,217

CREATE   PROCEDURE [dbo].[GET_TOTAL_EXPECTED_USAGE_GROUP_P]
@userId varchar(10),
@sessionId varchar(20),
@groupId integer,
@hedgeTypeId integer,--571
@hedgeLevelId integer,--533
@conversionId integer,--25
@fromDate datetime,
@toDate datetime,
@fromyear integer,
@toYear integer,
@clientId integer,
@dealticketId integer
--@monthIdentifier datetime,
--@clientId integer


AS
set nocount on
if(@dealticketId = 0 or @dealticketId is null )

begin
SELECT 
A.HEDGE_VOLUME HEDGE_VOLUME,
A.dealTicketMonth,
A.dealTicketYear,
onBoardClient.max_hedge_percent,
A.site_id site_id
FROM
(
SELECT 
	RM_FORECAST_VOLUME_DETAILS.VOLUME*CONVERSION_FACTOR HEDGE_VOLUME,
	DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) dealTicketMonth,
	DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) dealTicketYear,
	RM_FORECAST_VOLUME_DETAILS.SITE_ID SITE_ID
	

	
FROM
	RM_ONBOARD_CLIENT,
	RM_ONBOARD_HEDGE_SETUP RIGHT JOIN RM_FORECAST_VOLUME_DETAILS ON
	(RM_ONBOARD_HEDGE_SETUP.SITE_ID=RM_FORECAST_VOLUME_DETAILS.SITE_ID),
	RM_FORECAST_VOLUME,
	CONSUMPTION_UNIT_CONVERSION

WHERE
	RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
	RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId AND
	RM_ONBOARD_HEDGE_SETUP.VOLUME_UNITS_TYPE_ID=CONSUMPTION_UNIT_CONVERSION.BASE_UNIT_ID AND
	CONSUMPTION_UNIT_CONVERSION.CONVERTED_UNIT_ID=@conversionId AND
	RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
	RM_FORECAST_VOLUME_DETAILS.RM_FORECAST_VOLUME_ID=RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID AND
	RM_FORECAST_VOLUME_DETAILS.SITE_ID IN
	(
	select RM_ONBOARD_HEDGE_SETUP.SITE_ID 

	from 
		RM_ONBOARD_CLIENT,
		RM_ONBOARD_HEDGE_SETUP

	where
		RM_ONBOARD_CLIENT.client_id=@clientId and 
		RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
		RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId and
		RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
		(RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=@hedgeLevelId OR RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=
		(select entity_id from entity where entity_type=262 and entity_name='Corporate'))
			
	) 
AND
	RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID IN
	(
		SELECT 
			RM_FORECAST_VOLUME_ID from RM_FORECAST_VOLUME
		WHERE
			FORECAST_AS_OF_DATE IN
			(
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@fromYear AND CLIENT_ID=@clientId
				UNION 	
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@toYear AND CLIENT_ID=@clientId
			)

		--FORECAST_YEAR=(SELECT DATEPART(YEAR, @monthIdentifier))AND 
		--FORECAST_AS_OF_DATE=(SELECT MAX(FORECAST_AS_OF_DATE)FROM RM_FORECAST_VOLUME where CLIENT_ID=@clientId)
	) 
	AND
		RM_FORECAST_VOLUME.CLIENT_ID=@clientId AND
		--RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER=@monthIdentifier
		RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER BETWEEN @fromDate AND @toDate

--GROUP BY 
	--DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER),
	--DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER)
) AS A,RM_ONBOARD_CLIENT onBoardClient where onBoardClient.client_id=@clientId order by dealTicketYear,dealTicketMonth asc

--GROUP BY 
	--dealTicketMonth,dealTicketYear
end

else 
begin
SELECT 
A.HEDGE_VOLUME HEDGE_VOLUME,
A.dealTicketMonth,
A.dealTicketYear,
onBoardClient.max_hedge_percent,
A.site_id site_id
FROM
(
SELECT 
	RM_FORECAST_VOLUME_DETAILS.VOLUME*CONVERSION_FACTOR HEDGE_VOLUME,
	DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) dealTicketMonth,
	DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) dealTicketYear,
	RM_FORECAST_VOLUME_DETAILS.SITE_ID SITE_ID
	

	
FROM
	RM_ONBOARD_CLIENT,
	RM_ONBOARD_HEDGE_SETUP RIGHT JOIN RM_FORECAST_VOLUME_DETAILS ON
	(RM_ONBOARD_HEDGE_SETUP.SITE_ID=RM_FORECAST_VOLUME_DETAILS.SITE_ID),
	RM_FORECAST_VOLUME,
	CONSUMPTION_UNIT_CONVERSION

WHERE
	RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
	RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId AND
	RM_ONBOARD_HEDGE_SETUP.VOLUME_UNITS_TYPE_ID=CONSUMPTION_UNIT_CONVERSION.BASE_UNIT_ID AND
	CONSUMPTION_UNIT_CONVERSION.CONVERTED_UNIT_ID=@conversionId AND
	RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
	RM_FORECAST_VOLUME_DETAILS.RM_FORECAST_VOLUME_ID=RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID AND
	RM_FORECAST_VOLUME_DETAILS.SITE_ID IN
	(
	select RM_ONBOARD_HEDGE_SETUP.SITE_ID 

	from 
		RM_ONBOARD_CLIENT,
		RM_ONBOARD_HEDGE_SETUP

	where
		RM_ONBOARD_CLIENT.client_id=@clientId and 
		RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
		RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId and
		RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
		(RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=@hedgeLevelId OR RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID=
		(select entity_id from entity where entity_type=262 and entity_name='Corporate'))
	UNION
		
		select
			distinct rm_deal_ticket_volume_details.site_id 
		from 	
			rm_deal_ticket_volume_details,rm_deal_ticket_details,rm_deal_ticket,RM_ONBOARD_CLIENT,
			RM_ONBOARD_HEDGE_SETUP
		where  
			rm_deal_ticket_volume_details.rm_deal_ticket_details_id = rm_deal_ticket_details.rm_deal_ticket_details_id
			and rm_deal_ticket_details.rm_deal_ticket_id= rm_deal_ticket.rm_deal_ticket_id 
			and rm_deal_ticket.rm_deal_ticket_id = @dealticketId
			and rm_deal_ticket.client_id = RM_ONBOARD_CLIENT.CLIENT_ID and 
			RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
			rm_deal_ticket_volume_details.site_id = RM_ONBOARD_HEDGE_SETUP.SITE_ID AND
			RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID AND
			--RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and 
			RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID=@groupId 
		
	) 
AND
	RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID IN
	(
		SELECT 
			RM_FORECAST_VOLUME_ID from RM_FORECAST_VOLUME
		WHERE
			FORECAST_AS_OF_DATE IN
			(
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@fromYear AND CLIENT_ID=@clientId
				UNION 	
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@toYear AND CLIENT_ID=@clientId
			)

		--FORECAST_YEAR=(SELECT DATEPART(YEAR, @monthIdentifier))AND 
		--FORECAST_AS_OF_DATE=(SELECT MAX(FORECAST_AS_OF_DATE)FROM RM_FORECAST_VOLUME where CLIENT_ID=@clientId)
	) 
	AND
		RM_FORECAST_VOLUME.CLIENT_ID=@clientId AND
		--RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER=@monthIdentifier
		RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER BETWEEN @fromDate AND @toDate

--GROUP BY 
	--DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER),
	--DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER)
) AS A,RM_ONBOARD_CLIENT onBoardClient where onBoardClient.client_id=@clientId order by dealTicketYear,dealTicketMonth asc
end
GO
GRANT EXECUTE ON  [dbo].[GET_TOTAL_EXPECTED_USAGE_GROUP_P] TO [CBMSApplication]
GO
