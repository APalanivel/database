SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Queue_Status_Upd       
              
Description:              
			This sproc to update Invoice_Collection_Queue
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@tvp_Invoice_Collection_Queue			tvp_Invoice_Collection_Queue READONLY
@User_Info_Id							INT
@Status_Cd								INT   
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 


BEGIN TRAN  
DECLARE @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue        
          
INSERT      INTO @tvp_Invoice_Collection_Queue
            ( Invoice_Collection_Queue_Id, Invoice_File_Name )
VALUES
            ( 1, 'name.xp' )            
	   ,    ( 2, 'name2.xp' )  
	   
EXEC dbo.Invoice_Collection_Queue_Status_Upd 
      @tvp_Invoice_Collection_Queue = @tvp_Invoice_Collection_Queue
     ,@User_Info_Id = 49
     ,@Status_Cd = 102345
	
ROLLBACK
	
	
   SELECT
      *
FROM
      dbo.Code c
      INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
WHERE
      cs.Codeset_Name = 'ICR Status'
      AND c.Code_Value = 'Received'
      
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	SP				Sandeep Pigilam
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    SP				2017-01-18		Created For Invoice_Collection.
    RKV             2017-09-13      MAINT 5798,Update is_Locked to true for the Excluded status 
	RKV             2019-07-17      Added new column Invoice_Date_Received in the update statement.       
             
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Status_Upd]
     (
         @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue READONLY
         , @User_Info_Id INT
         , @Status_Cd INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Ts DATETIME = GETDATE()
            , @Is_Received_Status BIT = 0
            , @Is_Processed_Status BIT = 0
            , @Is_Excluded_Status BIT = 0;

        SELECT
            @Is_Received_Status = 1
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICR Status'
            AND c.Code_Value = 'Received'
            AND c.Code_Id = @Status_Cd;

        SELECT
            @Is_Processed_Status = 1
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICE Status'
            AND c.Code_Value = 'Processed'
            AND c.Code_Id = @Status_Cd;


        SELECT
            @Is_Excluded_Status = 1
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name IN ( 'ICR Status', 'ICE Status' )
            AND c.Code_Value = 'Excluded'
            AND c.Code_Id = @Status_Cd;



        UPDATE
            icq
        SET
            icq.Invoice_File_Name = CASE WHEN @Is_Received_Status = 1 THEN tvp.Invoice_File_Name
                                        ELSE NULL
                                    END
            , icq.Status_Cd = @Status_Cd
            , Received_Status_Updated_Dt = CASE WHEN @Is_Received_Status = 1 THEN
                                                    ISNULL(tvp.Invoice_Date_Received, @Ts)
                                               ELSE Received_Status_Updated_Dt
                                           END
            , Is_Locked = CASE WHEN (   @Is_Received_Status = 1
                                        OR  @Is_Processed_Status = 1
                                        OR  @Is_Excluded_Status = 1) THEN 1
                              ELSE Is_Locked
                          END
            , Updated_User_Id = @User_Info_Id
            , Last_Change_Ts = @Ts
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN @tvp_Invoice_Collection_Queue tvp
                ON icq.Invoice_Collection_Queue_Id = tvp.Invoice_Collection_Queue_Id;

    END;


GO

GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Status_Upd] TO [CBMSApplication]
GO
