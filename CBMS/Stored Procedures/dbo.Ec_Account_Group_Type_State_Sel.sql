SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_State_Sel        
                
Description:                
		This sproc to get the Group types based country and state and commodity.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Country_Id					INT  
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
   
   Exec dbo.Ec_Account_Group_Type_State_Sel   26   
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_State_Sel] ( @Country_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            s.STATE_ID
           ,s.STATE_NAME
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN dbo.STATE s
                  ON eagt.State_Id = s.STATE_ID
            INNER JOIN dbo.COUNTRY c
                  ON s.COUNTRY_ID = c.COUNTRY_ID
      WHERE
            c.COUNTRY_ID = @Country_Id
      GROUP BY
            s.STATE_ID
           ,s.STATE_NAME
      ORDER BY
            s.STATE_NAME
                  
                
                  
END
      

;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_State_Sel] TO [CBMSApplication]
GO
