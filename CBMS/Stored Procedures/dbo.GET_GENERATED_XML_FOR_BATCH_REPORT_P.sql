SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.GET_GENERATED_XML_FOR_BATCH_REPORT_P
@userId varchar,
@sessionId varchar,
@reportDate varchar(12),
@clientId int,
@divisionId int,
@siteId int,
@volumeId int,
@currencyId int,
@reportName varchar(50),
@displayStatus int,
@fiscalYear int,
@groupId int

as
	set nocount on
if @displayStatus=1

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 628 AND
						INPUT_PARAMETER = @clientId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( select	RM_REPORT_ID 
														 from	RM_REPORT_FILTER_DATA filter
														 where	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 


								)
						
				      )	


else if @displayStatus=2

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 629 AND
						INPUT_PARAMETER = @divisionId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( select	RM_REPORT_ID 
														 from	RM_REPORT_FILTER_DATA filter
														 where	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 

								)
						
				      )	



else if @displayStatus=3

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 630 AND
						INPUT_PARAMETER = @siteId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( select	RM_REPORT_ID 
														 from	RM_REPORT_FILTER_DATA filter
														 where	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 

								)
						
				      )	







else if @displayStatus=4

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 628 AND
						INPUT_PARAMETER = @clientId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId 
											) 


								)
						
				      )	


else if @displayStatus=5

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 629 AND
						INPUT_PARAMETER = @divisionId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId 
											) 



								)
						
				      )	



else if @displayStatus=6

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 630 AND
						INPUT_PARAMETER = @siteId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId 
											) 

								)
						
				      )	




else if @displayStatus=7

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 841 AND
						INPUT_PARAMETER = @groupId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER = @currencyId 
											) 

								)
						
				      )	



else if @displayStatus=8

	select report.REPORT_XML
	
	from	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	where	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( select	RM_REPORT_ID 
					from	RM_REPORT_FILTER_DATA filter
					where	INPUT_PARAMETER_TYPE_ID = 841 AND
						INPUT_PARAMETER = @groupId AND
						
						RM_REPORT_ID IN( select	RM_REPORT_ID 
								 from	RM_REPORT_FILTER_DATA filter
								 where	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER = @volumeId AND 

									RM_REPORT_ID IN( select	RM_REPORT_ID 
											 from	RM_REPORT_FILTER_DATA filter
											 where	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( select	RM_REPORT_ID 
														 from	RM_REPORT_FILTER_DATA filter
														 where	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 

								)
						
				      )
GO
GRANT EXECUTE ON  [dbo].[GET_GENERATED_XML_FOR_BATCH_REPORT_P] TO [CBMSApplication]
GO
