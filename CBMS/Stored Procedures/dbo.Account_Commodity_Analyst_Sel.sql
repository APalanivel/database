
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
      dbo.Account_Commodity_Analyst_Sel        
           
 DESCRIPTION:           
      Gets the Analyst for a given Client,Division,Site,Account.        
              
 INPUT PARAMETERS:          
 Name   DataType Default Description          
------------------------------------------------------------          
 @Client_Id  INT        
 @Sitegroup_Id  INT   NULL        
 @Site_Id  INT   NULL        
 @Account_Id INT   NULL        
 @Commodity_Id INT        
 @Start_Index INT   1        
 @End_Index  INT   2147483647        
         
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:          
------------------------------------------------------------        
        
	EXEC dbo.Account_Commodity_Analyst_Sel 235,NULL,NULL,NULL,290
	EXEC dbo.Account_Commodity_Analyst_Sel 235,NULL,NULL,NULL,290,1,1000,1
	EXEC dbo.Account_Commodity_Analyst_Sel 235,NULL,NULL,NULL,290,1,1000,4
	EXEC dbo.Account_Commodity_Analyst_Sel 235,NULL,NULL,NULL,290,1,1000,4,44

	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,66,1,100
	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,291,1,100
	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,291,1,100,21
	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,291,1,100,38
	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,291,1,100,4
	EXEC dbo.Account_Commodity_Analyst_Sel 11231,NULL,NULL,NULL,291,1,100,4,44     
        
AUTHOR INITIALS:        
	Initials	Name        
------------------------------------------------------------         
	AKR			Ashok Kumar Raju
	RR			Raghu Reddy        
         
        
 MODIFICATIONS           
	Initials	Date		Modification        
------------------------------------------------------------        
	AKR			2012-09-26  Created for POCO
	RR			2015-07-10	Global Sourcing - Added new input optional parameters @Country_Id, @State_Id and existing
							parameter @Client_Id made as optional, application will pass any one of these three parameters        
        
******/        
CREATE  PROCEDURE [dbo].[Account_Commodity_Analyst_Sel]
      ( 
       @Client_Id INT = NULL
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN        
      SET nocount ON        
              
              
              
      DECLARE @Cl_Analyst_User_Info_Id INT        
              
      SELECT
            @Cl_Analyst_User_Info_Id = CCA.Analyst_User_Info_Id
      FROM
            Core.Client_Commodity CC
            JOIN dbo.Code Cd
                  ON CC.Commodity_Service_Cd = Cd.Code_Id
            JOIN dbo.Client_Commodity_Analyst CCA
                  ON CC.Client_Commodity_Id = CCA.Client_Commodity_Id
            JOIN dbo.Codeset Cs
                  ON Cd.Codeset_Id = Cs.Codeset_Id
                     AND Cd.Code_Value = 'Invoice'
                     AND Cs.Codeset_Name = 'CommodityServiceSource'
      WHERE
            CC.Commodity_Id = @Commodity_Id
            AND CC.Client_Id = @Client_Id;        
      WITH  cte_Site_Account
              AS ( SELECT
                        ch.Site_Id
                       ,cha.Account_Id
                       ,row_number() OVER ( ORDER BY ch.Site_name, cha.Account_Number ) Row_Num
                       ,count(1) OVER ( ) AS Total_Row_Count
                       ,cha.Account_Number
                       ,ch.Site_name
                       ,ch.Sitegroup_Name
                       ,ch.State_Name
                       ,ch.Country_Name
                   FROM
                        Core.Client_Hier ch
                        JOIN core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                   WHERE
                        ( @Client_Id IS NULL
                          OR ch.CLIENT_ID = @client_id )
                        AND cha.Commodity_Id = @Commodity_Id
                        AND ( @Sitegroup_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Sitegroup_Site SGS
                                          WHERE
                                                SGS.Sitegroup_Id = @Sitegroup_Id
                                                AND SGS.Site_id = CH.Site_Id ) )
                        AND ( ( @Site_Id IS NULL
                                AND CH.Site_Id > 0 )
                              OR CH.Site_Id = @Site_Id )
                        AND ( @Account_Id IS NULL
                              OR cha.Account_Id = @Account_Id )
                        AND cha.Account_Type = 'Utility'
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )
                        AND ( @State_Id IS NULL
                              OR ch.State_Id = @State_Id )
                   GROUP BY
                        ch.Site_Id
                       ,cha.Account_Id
                       ,ch.Site_name
                       ,cha.Account_Number
                       ,ch.Sitegroup_Name
                       ,ch.State_Name
                       ,ch.Country_Name)
            SELECT
                  csa.Sitegroup_Name
                 ,csa.Site_name
                 ,csa.Site_Id
                 ,csa.Account_Id
                 ,csa.Account_Number
                 ,coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, @Cl_Analyst_User_Info_Id) Analyst_User_Info_Id
                 ,csa.Row_Num
                 ,csa.Total_Row_Count
                 ,csa.State_Name
                 ,csa.Country_Name
            FROM
                  cte_Site_Account csa
                  LEFT JOIN dbo.Account_Commodity_Analyst aca
                        ON csa.Account_Id = aca.Account_Id
                           AND aca.Commodity_Id = @Commodity_Id
                  LEFT JOIN dbo.Site_Commodity_Analyst sca
                        ON csa.Site_id = sca.Site_Id
                           AND sca.Commodity_Id = @Commodity_Id
            WHERE
                  csa.Row_Num BETWEEN @Start_Index AND @End_Index        
           
END;



;
GO

GRANT EXECUTE ON  [dbo].[Account_Commodity_Analyst_Sel] TO [CBMSApplication]
GO
