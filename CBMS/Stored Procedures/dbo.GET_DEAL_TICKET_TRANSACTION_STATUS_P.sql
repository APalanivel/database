SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_TRANSACTION_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@latestStatus  	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

	USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.GET_DEAL_TICKET_TRANSACTION_STATUS_P -1,-1,103352,1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.
	
******/

CREATE PROCEDURE dbo.GET_DEAL_TICKET_TRANSACTION_STATUS_P
    @userId			VARCHAR(10),
    @sessionId		VARCHAR(20),
    @dealTicketId	INT,
    @latestStatus	BIT
AS 
BEGIN

    SET NOCOUNT ON;
    
    DECLARE @selectClause VARCHAR(1000)
    DECLARE @fromClause VARCHAR(1000)
    DECLARE @whereClause VARCHAR(1000)
    DECLARE @SQLStatement VARCHAR(8000)
    DECLARE @statusCheck INT
    DECLARE @cancelCheck INT
    DECLARE @PFCheckIM INT
    DECLARE @potentialCheck INT
	
    SELECT  @statusCheck = COUNT(1)
    FROM    RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
            ENTITY e
    WHERE   rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
            AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
            AND e.ENTITY_NAME IN ( 'CLIENT CONFIRMATION',
                                   'COUNTERPARTY CONFIRMATION' )
		

    SELECT  @cancelCheck = COUNT(1)
    FROM    RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
            ENTITY e
    WHERE   rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
            AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
            AND e.ENTITY_NAME IN ( 'CANCELLED' )

    SELECT  @PFCheckIM = COUNT(1)
    FROM    RM_DEAL_TICKET_DETAILS rmdtd,
            ENTITY e
    WHERE   rmdtd.RM_DEAL_TICKET_ID = @dealTicketId
            AND rmdtd.TRIGGER_STATUS_TYPE_ID = e.ENTITY_ID
            AND e.ENTITY_NAME LIKE ( 'Potentially Fixed' )

	
    SELECT  @potentialCheck = ( SELECT  COUNT(1)
                                FROM    RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts,
                                        ENTITY e
                                WHERE   rmdtts.RM_DEAL_TICKET_ID = @dealTicketId
                                        AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = e.ENTITY_ID
                                        AND e.ENTITY_NAME LIKE ( 'Potentially Fixed' )
                                        AND rmdtts.DEAL_TRANSACTION_DATE IN (
                                        SELECT  MAX(DEAL_TRANSACTION_DATE)
                                        FROM    RM_DEAL_TICKET_TRANSACTION_STATUS
                                        WHERE   RM_DEAL_TICKET_ID = @dealTicketId )
                              ) 


    SELECT  @selectClause = ' rmdtts.DEAL_TRANSACTION_DATE, '
            + ' rmdtts.CBMS_IMAGE_ID '

    SELECT  @fromClause = ' RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts '
    SELECT  @whereClause = ' rmdtts.RM_DEAL_TICKET_ID = ' + STR(@dealTicketId) 

    IF @statusCheck = 1 
        IF @cancelCheck = 0 
            BEGIN
                SELECT  @selectClause = ' ''Order Executed'' AS ENTITY_NAME, '
                        + @selectClause
            END
        ELSE 
            BEGIN
                SELECT  @selectClause = ' ''Cancelled'' AS ENTITY_NAME, '
                        + @selectClause
            END
    ELSE 
        IF @statusCheck = 2 
            IF @cancelCheck = 0 
                BEGIN
                    SELECT  @selectClause = ' ''Closed'' AS ENTITY_NAME, '
                            + @selectClause
                END
            ELSE 
                BEGIN
                    SELECT  @selectClause = ' ''Cancelled'' AS ENTITY_NAME, '
                            + @selectClause
                END
        ELSE 
            IF @statusCheck = 0
                AND @PFCheckIM > 0 
                BEGIN
                    SELECT  @selectClause = ' e.ENTITY_NAME, ' + @selectClause
                    SELECT  @fromClause = @fromClause + ' , ENTITY e ' 
                    SELECT  @whereClause = @whereClause
                            + ' AND e.ENTITY_ID = rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID '
                END
	
            ELSE 
                IF @statusCheck = 0
                    AND @PFCheckIM = 0
                    AND @potentialCheck = 1 
                    BEGIN
                        SELECT  @selectClause = ' ''Order Executed'' AS ENTITY_NAME, '
                                + @selectClause
			
                    END

	
                ELSE 
                    IF @statusCheck = 0
                        AND @PFCheckIM = 0
                        AND @potentialCheck = 0 
                        BEGIN
                            SELECT  @selectClause = ' e.ENTITY_NAME, '
                                    + @selectClause
                            SELECT  @fromClause = @fromClause + ' , ENTITY e ' 
                            SELECT  @whereClause = @whereClause
                                    + ' AND e.ENTITY_ID = rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID '
                        END

    IF @latestStatus > 0 
        BEGIN
            SELECT  @SQLStatement = ' SELECT TOP 1 ' + @selectClause
                    + ' FROM ' + @fromClause + ' WHERE ' + @whereClause
                    + ' ORDER BY  rmdtts.DEAL_TRANSACTION_DATE DESC '
        END
	
    ELSE 
        BEGIN
            SELECT  @SQLStatement = ' SELECT  ' + @selectClause + ' FROM '
                    + @fromClause + ' WHERE ' + @whereClause
                    + ' ORDER BY  rmdtts.DEAL_TRANSACTION_DATE DESC '
        END


    EXEC ( @SQLStatement )

END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_TRANSACTION_STATUS_P] TO [CBMSApplication]
GO
