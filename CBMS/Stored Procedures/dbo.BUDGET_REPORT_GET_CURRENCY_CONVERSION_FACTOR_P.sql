SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_REPORT_GET_CURRENCY_CONVERSION_FACTOR_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@budget_id     	int       	          	
	@currency_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from currency_unit
--select * from budget_currency_map where budget_id = 1427
--exec BUDGET_REPORT_GET_CURRENCY_CONVERSION_FACTOR_P '','',1427,1

CREATE       PROCEDURE dbo.BUDGET_REPORT_GET_CURRENCY_CONVERSION_FACTOR_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int,
	@currency_id int
	AS
begin
	set nocount on

	select	case when isnull(budget_currency_map.conversion_factor,0) > 0
		then budget_currency_map.conversion_factor
		else curr_conv.conversion_factor
		end conversion_factor

	from  	currency_unit_conversion curr_conv 
		left join budget_currency_map on curr_conv.base_unit_id = budget_currency_map.currency_unit_id
		and budget_currency_map.budget_id = @budget_id 

	where curr_conv.base_unit_id = @currency_id
              and curr_conv.converted_unit_id = 3 --// USD 
              and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = @currency_id and converted_unit_id = 3 and currency_group_id = 3)
              and curr_conv.currency_group_id = 3

	
	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_REPORT_GET_CURRENCY_CONVERSION_FACTOR_P] TO [CBMSApplication]
GO
