SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE    PROCEDURE dbo.BUDGET_SAVE_CONTRACT_BUDGET_DETAIL_P
	@contract_budget_month_id int,
	@volume decimal(32,16),
	@market_id int,
	@fuel decimal(32,16),
	@multiplier decimal(32,16),
	@adder decimal(32,16),
	@volume_unit_type_id int,
	@tax decimal(32,16),
	@currency_unit_id int,
	@is_Nymex bit
	AS

	insert into budget_contract_budget_detail (budget_contract_budget_month_id, volume, market_id, fuel, multiplier, adder, volume_unit_type_id, tax, currency_unit_id, is_nymex_forecast)
	values(@contract_budget_month_id, @volume, @market_id, @fuel, @multiplier, @adder, @volume_unit_type_id, @tax, @currency_unit_id, @is_Nymex)











GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_CONTRACT_BUDGET_DETAIL_P] TO [CBMSApplication]
GO
