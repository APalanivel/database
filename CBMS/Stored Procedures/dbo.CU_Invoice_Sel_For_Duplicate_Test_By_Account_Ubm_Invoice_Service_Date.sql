
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date

DESCRIPTION:

	This procedure will fetch the duplicate invoices matches with the given account_id , ubm_invoice_id and the date range.
	
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_id			INT
	@ubm_invoice_id		int       	          	
	@Service_Begin_Dt   date
	@Service_End_Dt     date

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	SET STATISTICS IO ON

	EXEC dbo.CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date 24222, 350446,'2006-06-01','2006-06-30'
	EXEC dbo.CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date 225659, 15199894,'2013-04-01','2013-04-30' -- not returning any rows in TK3 as there are no duplicate invoices, this is as expected.

AUTHOR INITIALS:

	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G
	RR			Raghu Reddy
	DR

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			06/15/2010	Created based on cbmsUbmInvoice_GetProcessedForDuplicateTest
	HG			06/21/2010	As Account_id column moved to Cu_Invoice_Service_Month from Cu_Invoice added it in query to filter the account_id.
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			07/07/2011	MAINT-682 The column collection altered to collect service_start_date,service_end_date from CU_INVOICE_SERVICE_MONTH table,
							as it is previously collecting from UBM_INVOICE_DETAILS table, and the table UBM_INVOICE_DETAILS has been 
							deleted as there are no more references.
	RR			07/14/2011	Renamed the sp from "dbo.Ubm_Invoice_Sel_For_Duplicate_Test_By_Account_Id" to current name 
							"dbo.CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date" as a part of MAINT-682
	HG			08/13/2013	MAINT-2118, identified that complex AND / OR condition on Cu_Invoice_Service_Month table causing the peformance issue. Simplified the logic by saving the records in table variable and applying complex filters on the table variable.
	DR			10/17/2013  MAINT-2322 Irratic performance impacts all of invoice processing.  Adding w/recompile to stabalize executions.
	RR			2014-05-28	Data Operations Enhancements Ph-2 - Added filter condition IS_REPORTED = 1
	HG			2015-03-27	Modified to remove the Is_Reported flag added part of Data Ops Phase-2 enhancement as the duplicate test should look at all the invoices instead of reported one.
	HG			2015-07-21	Modified the temp table Ubm_Currency_Cd column width to match with the base table column as it is failing in Prod
********/
CREATE PROCEDURE [dbo].[CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date]
      ( 
       @Account_Id INT
      ,@Ubm_Invoice_Id INT
      ,@Service_Begin_Dt DATE
      ,@Service_End_Dt DATE )
      WITH RECOMPILE
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Invoice_Dtl TABLE
            ( 
             Ubm_Invoice_Id INT
            ,Ubm_Account_Id VARCHAR(50)
            ,Ubm_Currency_Cd VARCHAR(100)
            ,Cu_Invoice_Id INT
            ,Service_Start_Date DATE
            ,Service_End_Date DATE )

      DECLARE
            @Start_Dt_Begin_Range DATE
           ,@Start_Dt_End_Range DATE
           ,@End_Dt_Begin_Range DATE
           ,@End_Dt_End_Range DATE

      SELECT
            @Start_Dt_Begin_Range = DATEADD(D, -1, @Service_Begin_Dt)
           ,@Start_Dt_End_Range = DATEADD(D, 1, @Service_Begin_Dt)
           ,@End_Dt_Begin_Range = DATEADD(D, -1, @Service_End_Dt)
           ,@End_Dt_End_Range = DATEADD(D, 1, @Service_End_Dt)


      INSERT      INTO @Invoice_Dtl
                  ( 
                   Ubm_Invoice_Id
                  ,Ubm_Account_Id
                  ,Ubm_Currency_Cd
                  ,Cu_Invoice_Id
                  ,Service_Start_Date
                  ,Service_End_Date )
                  SELECT
                        i.ubm_invoice_id
                       ,i.ubm_account_id
                       ,i.currency
                       ,cu.cu_invoice_id
                       ,cism.Begin_Dt
                       ,cism.End_Dt
                  FROM
                        dbo.ubm_invoice i
                        INNER JOIN dbo.cu_invoice cu
                              ON cu.ubm_invoice_id = i.ubm_invoice_id
                        INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                              ON cism.CU_INVOICE_ID = cu.CU_INVOICE_ID
                  WHERE
                        cism.ACCOUNT_ID = @Account_Id
                        AND i.UBM_INVOICE_ID != @ubm_invoice_id
                        AND i.UBM_ACCOUNT_ID IS NOT NULL
                        AND i.IS_PROCESSED = 1
                        AND cu.IS_DUPLICATE = 0
                        AND cu.IS_DNT = 0
                        AND ( cism.Begin_Dt >= @Start_Dt_Begin_Range
                              AND cism.End_Dt <= @End_Dt_End_Range )
                  GROUP BY
                        i.ubm_invoice_id
                       ,i.ubm_account_id
                       ,i.currency
                       ,cu.cu_invoice_id
                       ,cism.Begin_Dt
                       ,cism.End_Dt

      SELECT
            Ubm_Invoice_Id
           ,Ubm_Account_Id
           ,Ubm_Currency_Cd
           ,Cu_Invoice_Id
           ,Service_Start_Date
           ,Service_End_Date
      FROM
            @Invoice_Dtl
      WHERE
            ( ( Service_Start_Date = @Service_Begin_Dt
                AND ( Service_End_Date BETWEEN @End_Dt_Begin_Range
                                       AND     @End_Dt_End_Range ) )
              OR ( Service_End_Date = @Service_End_Dt
                   AND ( Service_Start_Date BETWEEN @Start_Dt_Begin_Range
                                            AND     @Start_Dt_End_Range ) ) )

END
;
GO





GRANT EXECUTE ON  [dbo].[CU_Invoice_Sel_For_Duplicate_Test_By_Account_Ubm_Invoice_Service_Date] TO [CBMSApplication]
GO
