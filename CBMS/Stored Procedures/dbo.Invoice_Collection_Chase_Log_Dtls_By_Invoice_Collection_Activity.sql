SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Chase_Log_Dtls_By_Invoice_Collection_Activity           
                  
Description:                  
   This sproc to used get the Chased records    
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
@Invoice_Collection_Activity_Id   INT         
                               
                        
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec dbo.Invoice_Collection_Chase_Log_Dtls_By_Invoice_Collection_Activity 1840,102276    
   Exec dbo.Invoice_Collection_Chase_Log_Dtls_By_Invoice_Collection_Activity 10989  
     
       
       
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna                   
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    RKV    2016-12-27  Created For Invoice_Collection.             
    RKV             2018-01-17  MAINT-6364, Removed Invoice_Collection_Officer_User from group clause when the chase type is null.             
    RKV             2018-02-01      MAINT-6738, Added two new parameters @Start_Index and  @End_Index for pagination    
	SLP				2019-09-24		Added column Is_Manual  ,Is_Not_Default_Vendor,Manual_ICR_Vendor_Id
******/


CREATE PROCEDURE [dbo].[Invoice_Collection_Chase_Log_Dtls_By_Invoice_Collection_Activity]
(
    @Invoice_Collection_Activity_Id INT,
    @Contact_Level_Cd INT = NULL,
    @Start_Index INT = 1,
    @End_Index INT = 2147483647,
    @Sort_Col VARCHAR(50) = 'Default',
    @Sort_Order VARCHAR(15) = 'ASC'
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Source_Type_Client INT,
            @Source_Type_Account INT,
            @Source_Type_Vendor INT,
            @Contact_Level VARCHAR(25) = NULL,
            @SQL_Statement NVARCHAR(MAX),
            @SQL_Statement1 NVARCHAR(MAX);



    SELECT @Contact_Level = c.Code_Value
    FROM dbo.Code c
    WHERE c.Code_Id = @Contact_Level_Cd
          AND @Contact_Level_Cd IS NOT NULL;

    SELECT @Contact_Level = 'N/A'
    WHERE @Contact_Level IS NULL;


    SELECT @SQL_Statement
        = N';WITH  Cte_Invoice_Collection_Chase_Records      
              AS ( SELECT TOP ( ' + CAST(@End_Index AS VARCHAR(MAX))
          + N' )      
                              
      CASE WHEN icq.Is_Not_Default_Vendor  = 1 then ''Vendor''
	  WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
          + N' <> 0      
                                  AND icccm.contact_Name IS NOT NULL      
                                 AND moc.Code_Value IN ( ''Telephone'', ''Others'' ) THEN '''
          + ISNULL(@Contact_Level, '') + N'''       
                           WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
          + N'<>0      
                                  AND icccm.contact_Name IS NOT NULL      
                                  AND icccm.Email_Address IS NOT NULL      
                                  AND moc.Code_Value IN ( ''Email'' ) THEN ''' + ISNULL(@Contact_Level, '')
          + N'''      
                           WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
          + N' =0 THEN istc.Code_Value      
                             ELSE NULL      
                        END Chase_Type      
                       ,CASE WHEN icq.Is_Not_Default_Vendor  = 1 then ''Email''
							 WHEN moc.Code_Value IS NULL THEN ''Unknown''      
                             WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
          + N'  <>0      
                                  AND moc.Code_Value IN ( ''Email'' )      
                                  AND ( icccm.contact_Name IS NULL      
                                        OR icccm.Email_Address IS  NULL ) THEN ''Unknown''      
                             WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
          + N' <>0      
                                  AND moc.Code_Value IN ( ''Telephone'', ''Others'' )      
                                  AND icccm.contact_Name IS NULL THEN ''Unknown''      
                             ELSE moc.Code_Value      
                        END Chase_Method      
                       ,Case When icq.Is_Not_Default_Vendor  = 1 then icqv.Vendor_Name Else icccm.contact_Name END Person_To_Chase       
                       ,CASE WHEN icq.Is_Not_Default_Vendor  = 1 then '''' ELSE icccm.Email_Address end  Email_Address      
                       ,icccm.Phone_Number Phone_Number      
                       ,COUNT(DISTINCT icq.Invoice_Collection_Queue_Id) Number_Of_Accounts      
                       ,csc.Code_Value Chase_Action      
                       ,icccm.Invoice_Collection_Chase_Log_Id      
                       ,icccm.Fax_Number      
                       ,CASE WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) > 1 THEN -1      
                             WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) = 1 THEN MAX(ica.Invoice_Collection_Officer_User_Id)      
                             ELSE ''''      
                        END Invoice_Collection_Officer_User_Id      
                       ,CASE WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) > 1 THEN ''Multiple''      
                             WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) = 1 THEN MAX(ui.FIRST_NAME + '' '' + ui.LAST_NAME)      
                             ELSE ''''      
                        END AS Invoice_Collection_Officer      
                       ,ci.Language_Cd      
                       ,ROW_NUMBER() OVER ( ORDER BY '
          + CASE
                WHEN @Sort_Col = 'Default' THEN
                    'CASE WHEN moc.Code_Value IS NULL THEN 2      
                                                          WHEN moc.Code_Value = ''Exception'' THEN 2      
                                                          ELSE 1      
                                                     END, CASE WHEN '
                    + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                                                    AND icccm.contact_Name IS NOT NULL      
                                                                    AND moc.Code_Value IN ( ''Telephone'', ''Others'' ) THEN '''
                    + ISNULL(@Contact_Level, '')
                    + '''      
                                                               WHEN '
                    + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                                                    AND icccm.contact_Name IS NOT NULL      
                                                                    AND icccm.Email_Address IS NOT NULL      
                                                                    AND moc.Code_Value IN ( ''Email'' ) THEN '''
                    + ISNULL(@Contact_Level, '') + ''' WHEN '
                    + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' =0 THEN istc.Code_Value      
                                                               ELSE NULL      
                                                          END, icccm.Invoice_Collection_Chase_Log_Id '
                WHEN @Sort_Col = 'Chase_Type' THEN
                    ' CASE WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                  AND icccm.contact_Name IS NOT NULL      
                                  AND moc.Code_Value IN ( ''Telephone'', ''Others'' ) THEN '''
                    + ISNULL(@Contact_Level, '') + '''      
                             WHEN '
                    + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                  AND icccm.contact_Name IS NOT NULL      
                                  AND icccm.Email_Address IS NOT NULL      
                                  AND moc.Code_Value IN ( ''Email'' ) THEN ''' + ISNULL(@Contact_Level, '')
                    + '''      
                             WHEN '
                    + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' =0 THEN isnull(istc.Code_Value,''Exceptions'')      
                             ELSE ''Exceptions''      
                        END '
                WHEN @Sort_Col = 'Chase_Method' THEN
                    'CASE WHEN moc.Code_Value IS NULL THEN ''Unknown''      
                             WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                  AND moc.Code_Value IN ( ''Email'' )      
                                  AND ( icccm.contact_Name IS NULL      
                                        OR icccm.Email_Address IS  NULL ) THEN ''Unknown''      
                             WHEN ' + CAST(ISNULL(CAST(@Contact_Level_Cd AS VARCHAR(MAX)), 0) AS VARCHAR(MAX))
                    + ' <>0      
                                  AND moc.Code_Value IN ( ''Telephone'', ''Others'' )      
                                  AND icccm.contact_Name IS NULL THEN ''Unknown''      
                             ELSE moc.Code_Value      
                        END '
                WHEN @Sort_Col = 'Invoice_Collection_Officer' THEN
                    'CASE WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) > 1 THEN ''Multiple''      
                             WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) = 1 THEN MAX(ui.FIRST_NAME + '' '' + ui.LAST_NAME)      
                             ELSE ''''      
                        END '
                WHEN @Sort_Col = 'Invoice_Collection_Officer_User_Id' THEN
                    'CASE WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) > 1 THEN -1      
                             WHEN COUNT(DISTINCT ( ica.Invoice_Collection_Officer_User_Id )) = 1 THEN MAX(ica.Invoice_Collection_Officer_User_Id)      
                             ELSE ''''      
                        END '
                WHEN @Sort_Col = 'Person_To_Chase' THEN
                    'isnull(icccm.contact_Name,''Unknown'') '
                WHEN @Sort_Col = 'Email_Address' THEN
                    'isnull(icccm.Email_Address,''Unknown'') '
                WHEN @Sort_Col = 'Phone_Number' THEN
                    'CASE WHEN istc.Code_Value is null then ''Exceptions'' else icccm.Phone_Number end  '
                WHEN @Sort_Col = 'Number_Of_Accounts' THEN
                    'COUNT(DISTINCT icq.Invoice_Collection_Queue_Id) '
                WHEN @Sort_Col = 'Chase_Action' THEN
                    'csc.Code_Value '
                WHEN @Sort_Col = 'Fax_Number' THEN
               'CASE WHEN istc.Code_Value is null then ''Exceptions'' else icccm.Fax_Number end  '
            END + N' ' + @Sort_Order
          + N'       
                      
                      
                ) AS Row_Num      
                       ,COUNT(1) OVER ( ) Total_Count   
        
        
		,icq.Is_Not_Default_Vendor     
                   FROM      
                        dbo.Invoice_Collection_Chase_Log icccm      
                        INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icccmqm   
                              ON icccm.Invoice_Collection_Chase_Log_Id = icccmqm.Invoice_Collection_Chase_Log_Id      
                        INNER JOIN dbo.Invoice_Collection_Queue icq      
                              ON icccmqm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id 
						Left Outer join vendor icqv
						on icqv.Vendor_Id = icq.Manual_ICR_Vendor_Id     
                        INNER JOIN dbo.Invoice_Collection_Account_Config ica      
                              ON icq.Invoice_Collection_Account_Config_Id = ica.Invoice_Collection_Account_Config_Id      
                        LEFT OUTER JOIN Contact_Info ci      
        ON ci.Contact_Info_Id = icccm.Contact_Info_Id      
                        LEFT OUTER JOIN dbo.Code istc      
                              ON istc.Code_Id = ci.Contact_Level_Cd      
                        LEFT OUTER JOIN dbo.Code moc      
                              ON moc.Code_Id = icccm.Invoice_Collection_Method_Of_Contact_Cd      
                        LEFT OUTER JOIN dbo.Code csc      
                              ON csc.Code_Id = icccm.Status_Cd      
                        LEFT JOIN dbo.USER_INFO ui      
                              ON ica.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID      
                   WHERE      
                        icccm.Invoice_Collection_Activity_Id = '
          + CAST(@Invoice_Collection_Activity_Id AS VARCHAR(MAX))
          + N'      
                   GROUP BY      
                        istc.Code_Value      
                       ,moc.Code_Value      
                       ,Case When icq.Is_Not_Default_Vendor  = 1 then icqv.Vendor_Name Else icccm.contact_Name END      
                       ,icccm.Email_Address 
					   ,icccm.contact_Name      
                       ,icccm.Phone_Number      
                       ,csc.Code_Value      
                       ,icccm.Invoice_Collection_Chase_Log_Id      
                       ,icccm.Fax_Number      
                       ,ci.Language_Cd  
         
       ,icq.Is_Not_Default_Vendor   
        )      
            SELECT      
                  ISNULL(Chase_Type, ''Exceptions'') Chase_Type      
                 ,Chase_Method      
                 ,CASE WHEN Chase_Type IS NULL THEN ''Unknown''      
                       ELSE Person_To_Chase      
                  END Person_To_Chase      
                 ,CASE WHEN Chase_Type IS NULL THEN ''Unknown''      
                       ELSE Email_Address      
                  END Email_Address      
                 ,CASE WHEN Chase_Type IS NULL THEN ''Unknown''      
                       ELSE Phone_Number      
                  END Phone_Number      
                 ,Number_Of_Accounts      
                 ,Chase_Action      
                 ,Invoice_Collection_Chase_Log_Id      
                 ,CASE WHEN Chase_Type IS NULL THEN ''Unknown''      
                       ELSE Fax_Number      
                  END Fax_Number      
                 ,Invoice_Collection_Officer_User_Id      
                 ,Invoice_Collection_Officer      
                 ,Language_Cd      
                 ,Total_Count   
      
       ,Is_Not_Default_Vendor 
	   ,0 Is_Manual       
            FROM      
                  Cte_Invoice_Collection_Chase_Records ciccr ';

    SELECT @SQL_Statement1
        = N' WHERE      
                  ciccr.Row_Num BETWEEN ' + CAST(@Start_Index AS VARCHAR) + N' AND ' + CAST(@End_Index AS VARCHAR)
          + N' ORDER BY      
                  Row_Num ';



    EXEC (@SQL_Statement + ' ' + @SQL_Statement1);

END;







;




GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Chase_Log_Dtls_By_Invoice_Collection_Activity] TO [CBMSApplication]
GO
