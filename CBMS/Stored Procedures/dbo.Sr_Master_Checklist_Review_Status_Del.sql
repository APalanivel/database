SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******   

NAME: [DBO].[Sr_Master_Checklist_Review_Status_Del]  
     
DESCRIPTION: 
	It Deletes Sr Master Checklist Review Status for Selected Sr Master Checklist Review Status Id.
      
INPUT PARAMETERS:          
NAME									DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------          
@Sr_Master_Checklist_Review_Status_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	Begin Tran
	  EXEC Sr_Master_Checklist_Review_Status_Del 10
	Rollback Tran

AUTHOR INITIALS:          
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			09-JUNE-10	CREATED

*/

CREATE PROCEDURE dbo.Sr_Master_Checklist_Review_Status_Del
    (
      @Sr_Master_Checklist_Review_Status_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
	FROM
		dbo.SR_MASTER_CHECKLIST_REVIEW_STATUS
	WHERE
		SR_MASTER_CHECKLIST_REVIEW_STATUS_ID = @Sr_Master_Checklist_Review_Status_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Master_Checklist_Review_Status_Del] TO [CBMSApplication]
GO
