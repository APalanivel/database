SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Bid_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Bid for Selected 
						SR RFP Bid Id.
      
INPUT PARAMETERS:          
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@SR_RFP_Bid_Id			INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran
		EXEC SR_RFP_Bid_Del  28
	Rollback Tran

    SELECT * FROM SR_RFP_BID WHERE SR_RFP_BID_ID = 28

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    30-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Bid_Del
   (
    @SR_RFP_Bid_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_BID
	WHERE
		SR_RFP_BID_ID = @SR_RFP_Bid_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Bid_Del] TO [CBMSApplication]
GO
