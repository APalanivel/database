SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Procedure [dbo].[DMR_MISSING_COMMODITY_RESEARCH](@CUInvoice int)
as

Declare @ubmInvoice int,
        @BillHeader varchar(255)
        
Declare @ServiceID Table (Account_ID varchar(255), Vendor_ID varchar(255), Account_Service_ID varchar(255))

Select @ubmInvoice = ubm_invoice_id
From CU_invoice
Where CU_invoice_ID = @cuInvoice

Select @BillHeader = Invoice_identifier
From ubm_invoice U
Where ubm_invoice_id = @ubmInvoice

Insert @ServiceID (Account_ID, Vendor_ID, Account_Service_ID)
Select Account_ID, Vendor_ID, Account_Service_ID
From ubm_cass_bill_charges 
Where Util_Bill_Header_ID = @BillHeader
Group By Account_ID, Vendor_ID, Account_Service_ID

Select 'CU_Invoice_Determinant', *
From cu_Invoice_Determinant
Where cu_invoice_ID = @CUInvoice

Select 'CU_Invoice_Charge', *
From cu_invoice_Charge
Where cu_invoice_ID = @CUInvoice


Select 'UBM_Invoice', *
From  ubm_invoice U
Where ubm_invoice_id = @ubmInvoice


Select 'Bill', *
From ubm_cass_bill
Where Util_Bill_Header_ID = @BillHeader


Select 'Charges', *
From ubm_cass_bill_charges
Where Util_Bill_Header_ID = @BillHeader


Select 'Service', SS.Account_Service_ID, IsNull(S.Service_type_Code, 'Missing') Service_Type_Code 
from ubm_cass_service S
  Right Outer Join
     @ServiceID SS on S.Account_Service_ID = SS.Account_Service_ID
Group By SS.Account_Service_ID, IsNull(S.Service_type_Code, 'Missing')


Select 'Vendor', V.Vendor_ID, V.Vendor_Service_Desc, V.Vendor_Name, V.State_Code, V.Country, S.Account_ID, S.Account_Service_ID
From ubm_cass_vendor V
       inner join
     @ServiceID S on V.Vendor_ID = S.Vendor_ID
Group By  V.Vendor_ID, V.Vendor_Service_Desc, V.Vendor_Name, V.State_Code, V.Country, S.Account_ID, S.Account_Service_ID
GO
GRANT EXECUTE ON  [dbo].[DMR_MISSING_COMMODITY_RESEARCH] TO [CBMSApplication]
GO
