SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Client_Contact_Ins

    
DESCRIPTION:    

      To check duplicate client contact


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              

	BEGIN TRANSACTION
		EXEC dbo.RM_Client_Contact_Ins @Client_Id=10003,@First_Name='Test',@Last_Name='DB',@Email_Address='rvintha@ctepl.com'
		,@User_Info_Id = 16
		SELECT TOP 5 * FROM dbo.Client_Contact_Map a INNER JOIN dbo.Contact_Info b ON a.Contact_Info_Id = b.Contact_Info_Id
			WHERE a.CLIENT_ID  = 10003 ORDER BY b.Last_Change_Ts DESC
	ROLLBACK TRANSACTION

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/ 
CREATE PROCEDURE [dbo].[RM_Client_Contact_Ins]
      ( 
       @Client_Id INT = NULL
      ,@First_Name NVARCHAR(60)
      ,@Last_Name NVARCHAR(60)
      ,@Email_Address NVARCHAR(150)
      ,@Phone_Number NVARCHAR(60) = NULL
      ,@Mobile_Number NVARCHAR(60) = NULL
      ,@Fax_Number NVARCHAR(60) = NULL
      ,@User_Info_Id INT
      ,@Contact_Info_Id INT = NULL
      ,@Deal_Ticket_Id INT = NULL )
AS 
BEGIN
	
      SET NOCOUNT ON;
         
      DECLARE
            @Contact_Type_Cd INT
           ,@Contact_Info_Id_New INT
           ,@Contact_Level_Cd INT
           
      SELECT
            @Client_Id = Client_Id
      FROM
            Trade.Deal_Ticket
      WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id
            AND @Deal_Ticket_Id IS NOT NULL
      
      SELECT
            @Contact_Type_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Client Contact'
            
      SELECT
            @Contact_Level_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactLevel'
            AND cd.Code_Value = 'Client'
            
            
      INSERT      INTO dbo.Contact_Info
                  ( 
                   Contact_Type_Cd
                  ,Contact_Level_Cd
                  ,First_Name
                  ,Last_Name
                  ,Email_Address
                  ,Phone_Number
                  ,Fax_Number
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts
                  ,Mobile_Number
                  ,Language_Cd )
                  SELECT
                        @Contact_Type_Cd
                       ,@Contact_Level_Cd
                       ,@First_Name
                       ,@Last_Name
                       ,@Email_Address
                       ,@Phone_Number
                       ,@Fax_Number
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@Mobile_Number
                       ,-1
                       
      SELECT
            @Contact_Info_Id_New = SCOPE_IDENTITY()
                  
      INSERT      INTO dbo.Client_Contact_Map
                  ( 
                   CLIENT_ID
                  ,Contact_Info_Id
                  ,Created_Ts )
                  SELECT
                        @Client_Id
                       ,@Contact_Info_Id_New
                       ,GETDATE()
      SELECT
            @Contact_Info_Id_New AS Contact_Info_Id
				

END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Contact_Ins] TO [CBMSApplication]
GO
