SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       
NAME:  dbo.CU_Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id      
     
DESCRIPTION:  This will fetch all the accounts for the cu_invoice_id ,
to fill the accounts drop down in invoice details section 
    
INPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
@cu_invoice_id		int
       
OUTPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------     
   

exec CU_Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id 75252687 
 

	  

AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
NR			Narayana Reddy

Initials	 Date			Modification      
------------------------------------------------------------      
NR			2019-10-18		Created for Add Contract.   

******/

CREATE PROCEDURE [dbo].[CU_Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id AS INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cism.Account_ID
            , cha.Display_Account_Number AS ACCOUNT_NUMBER
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cism.Account_ID
            , cha.Display_Account_Number;


    END;

GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Accounts_Dtl_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
