SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:  Invoice_Collection_Batch_dtl_Error_Sel  
  
DESCRIPTION:    
IC tool revamp
    
INPUT PARAMETERS:    
 Name				DataType		 Default   Description    
------------------------------------------------------------------------------    
  
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 SET STATISTICS IO ON    
  EXEC [Invoice_Collection_Batch_dtl_Error_Sel] '2018-01-01'
   
  
AUTHOR INITIALS:  
  
 Initials Name    
------------------------------------------------------------    
   PR		PRadip rajput  
    
MODIFICATIONS:    
 Initials	Date		Modification    
------------------------------------------------------------    
 PR			27-04-2020	IC tool revamp.
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_dtl_Error_Sel]
    (
        @min_Batch_Dtl_Time DATETIME
    )
AS
    BEGIN
        SET NOCOUNT ON;




        SELECT
            bt.Invoice_Collection_Account_Config_Id
            , bt.Collection_Start_Dt
            , bt.Collection_End_Dt
            , bt.Error_Dsc
            , bt.Invoice_Collection_Batch_Dtl_Id
        FROM
            dbo.Invoice_Collection_Batch_Dtl bt
            INNER JOIN Code c
                ON bt.Status_Cd = c.Code_Id
        WHERE
            c.Code_Value = 'Error'
            AND bt.Created_Ts > @min_Batch_Dtl_Time;

    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_dtl_Error_Sel] TO [CBMSApplication]
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_dtl_Error_Sel] TO [Netstress]
GO
