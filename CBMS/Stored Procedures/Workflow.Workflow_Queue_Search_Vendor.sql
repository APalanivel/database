SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******      
NAME:    [Workflow].[Workflow_Queue_Search_Vendor]  
DESCRIPTION: It'll Return the Vendor Information   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @VENDOR_NAME VARCHAR(200) = '',      
 @Start_Index INT = 1,      
 @End_Index INT = 2147483647   
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna  Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK   Aug-2019  Created  
  TRK   Sep-2019  Migrated to Workflow Schema.  
******/      

CREATE PROC [Workflow].[Workflow_Queue_Search_Vendor]      
(        
 @VENDOR_NAME VARCHAR(200) = '',        
 @Start_Index INT = 1,        
    @End_Index INT = 2147483647        
)        
AS          
BEGIN          
         
 SELECT @VENDOR_NAME = REPLACE(@VENDOR_NAME,'''','''''');          
       
        
 WITH CTE AS        
 (        
  SELECT         
   VENDOR_ID,        
   VENDOR_NAME,        
   ROW_NUMBER() OVER (ORDER BY VENDOR_ID,VENDOR_NAME) AS Row_Num        
  FROM         
   VENDOR         
  WHERE         
   VENDOR_NAME LIKE '%'+@VENDOR_NAME+'%'         
          
 )        
 SELECT           
  VENDOR_ID,        
  VENDOR_NAME         
 FROM        
  CTE WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index        
        ORDER BY VENDOR_NAME        
                    
END;    
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_Vendor] TO [CBMSApplication]
GO
