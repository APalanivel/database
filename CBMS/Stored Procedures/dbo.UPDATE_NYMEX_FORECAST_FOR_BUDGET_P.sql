SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
CREATE PROCEDURE dbo.UPDATE_NYMEX_FORECAST_FOR_BUDGET_P
	@client_id INT,  
	@commodity_id INT,  
	@month_identifier DATETIME,  
	@nymex_price DECIMAL(10,3)  
AS
BEGIN

	SET NOCOUNT ON
  
	DECLARE @account_id INT
		, @nymex_type_id INT
		, @loopCntr INT
		, @totCnt INT
  
	DECLARE @nymex_forecast TABLE (nymxRowID INT IDENTITY(1,1),Account_ID INT)

	SELECT @nymex_type_id =  entity_id FROM entity where entity_name = 'Manual' AND entity_type = 1056  
    	
	INSERT INTO @nymex_forecast(Account_ID)
	SELECT utilacc.account_id
	FROM account utilacc
		INNER JOIN dbo.BUDGET_ACCOUNT_COMMODITY_VW acc_cmdty_vw ON acc_cmdty_vw.account_id = utilacc.account_id
		INNER JOIN dbo.vwSiteName site ON site.site_id = utilacc.site_id
		INNER JOIN dbo.client cli ON cli.client_id = site.client_id
		INNER JOIN dbo.vendor utility ON utility.vendor_id = utilacc.vendor_id
		LEFT OUTER JOIN dbo.budget_nymex_forecast forecast ON forecast.account_id = utilacc.account_id
			AND forecast.month_identifier = @month_identifier
			AND forecast.commodity_type_id = @commodity_id
		LEFT OUTER JOIN entity ON entity.entity_id = forecast.nymex_type_id
	WHERE cli.client_id = @client_id
		AND acc_cmdty_vw.commodity_type_id = @commodity_id
	
		UPDATE BF
		SET NYMEX_TYPE_ID=@nymex_type_id 
			, COMMODITY_TYPE_ID =@commodity_id
			, PRICE= @nymex_price
      FROM BUDGET_NYMEX_FORECAST BF
             INNER JOIN
           @NYMEX_FORECAST NF ON BF.ACCOUNT_ID = NF.ACCOUNT_ID
		WHERE  MONTH_IDENTIFIER = @month_identifier
			AND COMMODITY_TYPE_ID = @commodity_id
	  
		INSERT INTO dbo.BUDGET_NYMEX_FORECAST
		         (ACCOUNT_ID
				, NYMEX_TYPE_ID
				, COMMODITY_TYPE_ID
				, MONTH_IDENTIFIER
				, PRICE )
		SELECT ACCOUNT_ID
				, @nymex_type_id
				, @commodity_id
				, @month_identifier
				, @nymex_price
        FROM @NYMEX_FORECAST NF
       WHERE NOT EXISTS (SELECT * FROM BUDGET_NYMEX_FORECAST BF1 
                          WHERE NF.ACCOUNT_ID = BF1.ACCOUNT_ID 
                            AND BF1.MONTH_IDENTIFIER = @MONTH_IDENTIFIER
                            AND BF1.COMMODITY_TYPE_ID = @COMMODITY_ID)

		

   
END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_NYMEX_FORECAST_FOR_BUDGET_P] TO [CBMSApplication]
GO
