SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUMMIT_BOC_FOR_PLACE_BID_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@bidRequirementId int,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@selectedProductId int,
@accountTermId int,
@productName varchar(200),
@isSubProduct int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test insert an entry
--exec [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_PLACE_BID_P] 
--@bidRequirementId = 0,
--@delivery = 'BlueStar',
--@transServiceLevelId = 1007,
--@supplierNominationId = 1081,
--@supplierBalancingId = 1081,
--@summitComments = 'Test Data',
--@deliveryPowerId = NULL,
--@transServiceLevelPowerId = NULL,
--@selectedProductId = 3923,
--@accountTermId = 25554,
--@productName = 'Test Data',
--@isSubProduct = 0


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_PLACE_BID_P] 

@bidRequirementId int,
@delivery varchar(200),
@transServiceLevelId int,
@supplierNominationId int,
@supplierBalancingId int,
@summitComments varchar(4000),
@deliveryPowerId int,
@transServiceLevelPowerId int,
@selectedProductId int,
@accountTermId int,
@productName varchar(200),
@isSubProduct int

AS

SET NOCOUNT ON

declare @newIdentity int
declare @newIdentity1 int
DECLARE @prodTermMapId int


-- insert operation

if @bidRequirementId=0

BEGIN
 
	
		INSERT INTO SR_RFP_BID_REQUIREMENTS 
		(
			DELIVERY_POINT,
			TRANSPORTATION_LEVEL_TYPE_ID,
			NOMINATION_TYPE_ID,
			BALANCING_TYPE_ID,
			COMMENTS,
			DELIVERY_POINT_POWER_TYPE_ID,
			SERVICE_LEVEL_POWER_TYPE_ID
		)  
		
		VALUES
		
		(
			@delivery,
			@transServiceLevelId,
			@supplierNominationId,
			@supplierBalancingId,
			@summitComments,
			@deliveryPowerId ,
			@transServiceLevelPowerId 
			
		)
		
		SELECT @newIdentity=SCOPE_IDENTITY()
		
		INSERT INTO SR_RFP_BID 
		(
			PRODUCT_NAME,
			SR_RFP_BID_REQUIREMENTS_ID,
			SR_RFP_SUPPLIER_PRICE_COMMENTS_ID,
			SR_RFP_SUPPLIER_SERVICE_ID,
			IS_SUB_PRODUCT
		)  
		
		VALUES
		
		(
			@productName,
			@newIdentity,
			null,
			null,
			@isSubProduct
		)		
		select @newIdentity1=SCOPE_IDENTITY()

		INSERT INTO SR_RFP_TERM_PRODUCT_MAP 
		(
			SR_RFP_ACCOUNT_TERM_ID,
			SR_RFP_SELECTED_PRODUCTS_ID,
			SR_RFP_BID_ID
		)
		VALUES 
		(
			@accountTermId,
			@selectedProductId,
			@newIdentity1
		)

			select @prodTermMapId=SCOPE_IDENTITY()

		
END

--UPDATE operation

else if @bidRequirementId>0

begin
		
		UPDATE 
			SR_RFP_BID_REQUIREMENTS SET 
			DELIVERY_POINT=@delivery,
			TRANSPORTATION_LEVEL_TYPE_ID=@transServiceLevelId,
			NOMINATION_TYPE_ID=@supplierNominationId,
			BALANCING_TYPE_ID=@supplierBalancingId,
			COMMENTS=@summitComments,
			DELIVERY_POINT_POWER_TYPE_ID=@deliveryPowerId,
			SERVICE_LEVEL_POWER_TYPE_ID=@transServiceLevelPowerId

		WHERE
			SR_RFP_BID_REQUIREMENTS_ID=@bidRequirementId


		UPDATE  
			SR_RFP_BID 
		SET 
			PRODUCT_NAME=@productName 
		WHERE 
			SR_RFP_BID_REQUIREMENTS_ID=@bidRequirementId

		
		
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUMMIT_BOC_FOR_PLACE_BID_P] TO [CBMSApplication]
GO
