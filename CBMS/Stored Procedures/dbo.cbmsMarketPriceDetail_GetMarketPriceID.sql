SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsMarketPriceDetail_GetMarketPriceID

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Market_Price_Point	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create       Procedure dbo.cbmsMarketPriceDetail_GetMarketPriceID
	( @Market_Price_Point varchar(100)
	)
As
BEGIN

	/*

	This sproc returns the Market_Price_Point_ID.

	*/

	set nocount on
	
	select Market_Price_Point_ID from Market_Price_Point where Market_Price_Point=@Market_Price_Point



	
END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketPriceDetail_GetMarketPriceID] TO [CBMSApplication]
GO
