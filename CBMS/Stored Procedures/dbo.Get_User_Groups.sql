SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
 NAME: [dbo].[Get_User_Groups]      
      
 DESCRIPTION:       
       get all the groups of the user by function or roles mapped       
           
 INPUT PARAMETERS:        
       
 Name                               DataType            Default          Description        
---------------------------------------------------------------------------------------------------------------      
 @User_Info_Id                      INT      
        
 OUTPUT PARAMETERS:              
 Name                               DataType            Default          Description        
---------------------------------------------------------------------------------------------------------------      
        
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------       

 EXEC dbo.Get_User_Groups 235
 EXEC dbo.Get_User_Groups 41139
 
 SELECT * FROM dbo.User_Info_Client_App_Access_Role_Map
       
 AUTHOR INITIALS:       
       
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 HG                     Harihara Suthan Ganesan
         
 MODIFICATIONS:       
        
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------
 HG                     2014-03-03      Created for RA Admin user management

******/
CREATE PROCEDURE [dbo].[Get_User_Groups] ( @User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON        
      DECLARE @Groups TABLE
            ( 
             Group_Info_Id INT PRIMARY KEY CLUSTERED
            ,Group_Name VARCHAR(200)
            ,Is_Function_Based BIT )
	
      INSERT      INTO @Groups
                  ( 
                   Group_Info_Id
                  ,Group_Name
                  ,Is_Function_Based )
                  SELECT
                        gi.Group_Info_Id
                       ,gi.Group_Name
                       ,0 AS Is_Function_Based
                  FROM
                        dbo.Group_Info gi
                        INNER JOIN dbo.User_Info_Group_Info_Map map
                              ON map.group_info_id = gi.group_info_id
                  WHERE
                        map.User_Info_Id = @User_Info_Id
     
      INSERT      INTO @Groups
                  ( 
                   Group_Info_Id
                  ,Group_Name
                  ,Is_Function_Based )
                  SELECT
                        gi.Group_Info_Id
                       ,gi.Group_Name
                       ,1 AS Is_Function_Based
                  FROM
                        dbo.Group_Info gi
                        INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map map
                              ON map.group_info_id = gi.group_info_id
                        INNER JOIN dbo.User_Info_Client_App_Access_Role_Map urm
                              ON urm.Client_App_Access_Role_Id = map.Client_App_Access_Role_Id
                  WHERE
                        urm.User_Info_Id = @User_Info_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Groups g
                                         WHERE
                                          g.Group_Info_Id = gi.GROUP_INFO_ID )
                  GROUP BY
                        gi.Group_Info_Id
                       ,gi.Group_Name

      SELECT
            Group_Info_Id
           ,Group_Name
           ,Is_Function_Based
      FROM
            @Groups
      ORDER BY
            Group_Name
END;
;
GO
GRANT EXECUTE ON  [dbo].[Get_User_Groups] TO [CBMSApplication]
GO
