SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexDetail_GetForIndexMonthIDAndDate

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_month_id	int       	null      	
	@index_detail_date	datetime  	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsCPIndexDetail_GetForIndexMonthIDAndDate]
	( @clearport_index_month_id int = null
	, @index_detail_date datetime = null
	)
AS
BEGIN

	   select clearport_index_detail_id
		, index_detail_value
		, index_detail_date
		, clearport_index_month_id	
	     from clearport_index_detail with (nolock)
	    where clearport_index_month_id = @clearport_index_month_id
       		and day(index_detail_date) = day(@index_detail_date)
		and month(index_detail_date) = month(@index_detail_date)
		and year(index_detail_date) = year(@index_detail_date)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexDetail_GetForIndexMonthIDAndDate] TO [CBMSApplication]
GO
