SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO















-- EXEC dbo.BUDGET_GET_GENERATION_FOR_ALL_ACCOUNTS_P 1139, '04/01/2008', '08/01/2008'
-- exec dbo.BUDGET_GET_GENERATION_FOR_ALL_ACCOUNTS_P 1238,'01/01/2008', '12/01/2008'
-- This Stored Proc is to get generation for all transport accounts in a budget
CREATE                 PROCEDURE dbo.BUDGET_GET_GENERATION_FOR_ALL_ACCOUNTS_P
	@budget_id int,
	@from_date datetime,
	@to_date datetime
	AS
	begin
		set nocount on
--added for 16542 bug
        declare @temp table ([sno][int] IDENTITY(1,1)PRIMARY KEY,[budget_account_id] [int],[month_identifier][datetime],[volume]decimal(32,16),[index_detail_value]decimal(32,16),[adder]decimal(32,16),[fuel]decimal(32,16),[multiplier][int],[tax]decimal(32,16),[is_nymex][int],[budget_contract_budget_detail_id][int],[contract_start_date][datetime],[contract_end_date][datetime],[contract_id][int] )
	insert into @temp(budget_account_id,month_identifier,volume,index_detail_value,adder,fuel,multiplier,tax,is_nymex,budget_contract_budget_detail_id,contract_start_date,contract_end_date,contract_id) 


		select	budget_account.budget_account_id,
			months.month_identifier,
			isnull(detail.volume,100) as volume,
			index_detail.index_detail_value,
			case when conv.conversion_factor > 0
			     then ((isnull(detail.adder,0) * curr_conv.conversion_factor)/conv.conversion_factor)
			     else 0
			end adder,
			0 as fuel,
			detail.multiplier,
			detail.tax,
			detail.is_nymex_forecast as is_nymex,
			detail.budget_contract_budget_detail_id,
			budget_contract_vw.contract_start_date,
			budget_contract_vw.contract_end_date,
			budget_contract_vw.contract_id
		
		from	budget join budget_account on budget_account.budget_id = budget.budget_id 
			and budget.budget_id = @budget_id  
			/*and budget_account.budget_account_id  not in (

				select 	bacc.budget_account_id
				from 	budget b join budget_account bacc on bacc.budget_id = b.budget_id 
					and b.budget_id = budget.budget_id  
					join budget_contract_vw vw on vw.account_id = bacc.account_id 
					and vw.commodity_type_id = b.commodity_type_id
					join budget_contract_budget con_budget on con_budget.contract_id = vw.contract_id
				group by bacc.budget_account_id having   count( vw.contract_id) > 1
			)*/

			join budget_contract_vw on budget_contract_vw.account_id = budget_account.account_id 
			and budget_contract_vw.commodity_type_id = budget.commodity_type_id
			and budget_contract_vw.contract_start_date <= ( dateadd(month, 1, @to_date) - 1 )
			and budget_contract_vw.contract_end_date > getdate()
			join budget_contract_budget contract_budget on contract_budget.contract_id = budget_contract_vw.contract_id
			join budget_contract_budget_months months 
			on months.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			and months.month_identifier between @from_date and @to_date
			join budget_contract_budget_detail detail on detail.budget_contract_budget_month_id = months.budget_contract_budget_month_id
			join consumption_unit_conversion conv on conv.base_unit_id = detail.volume_unit_type_id
			and conv.converted_unit_id = 12 --// kWh
			join currency_unit_conversion curr_conv on curr_conv.base_unit_id = detail.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = detail.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3
			left join clearport_index_months index_months on index_months.clearport_index_id = detail.market_id
			and index_months.clearport_index_month = months.month_identifier
			left join clearport_index_detail index_detail 
			on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id)

			--left join clearport_index_detail index_detail on index_detail.clearport_index_id=detail.market_id
			--and index_detail.cp_index_month=months.month_identifier
			--and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_id = detail.market_id and cp_index_month = months.month_identifier)


		group by budget_account.budget_account_id, 
			 budget_contract_vw.contract_id,
			 months.month_identifier,
			 budget_contract_vw.contract_start_date, 
			 budget_contract_vw.contract_end_date,
			 detail.volume,
			 index_detail.index_detail_value,
			 conv.conversion_factor,
			 detail.adder,
			 curr_conv.conversion_factor,
			 detail.multiplier,
			 detail.tax,
			 detail.is_nymex_forecast,
			 detail.budget_contract_budget_detail_id
		order by
			 budget_account.budget_account_id, 
			 budget_contract_vw.contract_start_date 
--select * from @temp


--added for bug 16542 start
	declare @budget_account_id1 int,
		@budget_account_id2 int,
		@sid int,
		@cnt int,
		@contract_start_date1 datetime,
		@contract_end_date1 datetime,
		@contract_start_date2 datetime,
		@contract_end_date2 datetime,
		@contract_id1 int,
		@contract_id2 int
	
	select 
		@cnt=count(budget_account_id)from @temp;
	 	
	
		set 
		@sid=1

	while(@cnt>0)
	begin --//while
		select 
			@budget_account_id1=budget_account_id,
			@contract_start_date1=contract_start_date,
			@contract_end_date1=contract_end_date,
			@contract_id1=contract_id
		from 
			@temp 
		where 
			sno=@sid
	
		select  
			@budget_account_id2=budget_account_id,
			@contract_start_date2=contract_start_date,
			@contract_end_date2=contract_end_date,
			@contract_id2=contract_id 
		from 
			@temp  
		where 
			sno=(@sid+1)
	
		

		if(@budget_account_id1 =@budget_account_id2)
		begin--if1 begin
			
			if((@contract_id1 <> @contract_id2) AND (@contract_start_date2<=@contract_end_date1))
		      	begin --if2 begin 
		           delete @temp where budget_account_id=@budget_account_id1
			   	
		           set @sid=(select top 1 sno from @temp)

		      	end--if2 end
		      	else
	      		begin
				set @sid=(@sid+1)
		      	end	

   		end--if1 end
		else
		begin
		set @sid = (@sid+1)
		end
		set @cnt = @cnt-1
	end--while 

	select * from @temp

	--added for bug 16542 end


	end






















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_GENERATION_FOR_ALL_ACCOUNTS_P] TO [CBMSApplication]
GO
