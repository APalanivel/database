SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_TRIGGER_DEAL_TICKET_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	
	@hedgePrice    	decimal(32,16)	          	
	@monthIdentifier	datetime  	          	
	@triggerPrice  	decimal(32,16)	          	
	@triggerStatusType	int       	          	
	@triggerStatusName	varchar(200)	          	
	@totalVolume   	decimal(32,16)	          	
	@dealTransactionDate	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        PROCEDURE DBO.CREATE_TRIGGER_DEAL_TICKET_DETAILS_P 

@userId varchar,
@sessionId varchar,
@dealTicketId int, 
@hedgePrice decimal(32,16), 
@monthIdentifier datetime, 
@triggerPrice decimal(32,16), 
@triggerStatusType int, 
@triggerStatusName varchar(200), 
@totalVolume decimal(32,16), 
@dealTransactionDate datetime

AS
begin
	set nocount on
	DECLARE @entityId int
	
	select @entityId = ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@triggerStatusType AND 
	ENTITY_NAME=@triggerStatusName
	
	INSERT INTO RM_DEAL_TICKET_DETAILS 
	(RM_DEAL_TICKET_ID, HEDGE_PRICE, MONTH_IDENTIFIER, TRIGGER_PRICE, 
	TRIGGER_STATUS_TYPE_ID, TOTAL_VOLUME, DEAL_TRANSACTION_DATE)
	VALUES
	(@dealTicketId, @hedgePrice, @monthIdentifier, @triggerPrice, 
	@entityId, @totalVolume, @dealTransactionDate)
end
GO
GRANT EXECUTE ON  [dbo].[CREATE_TRIGGER_DEAL_TICKET_DETAILS_P] TO [CBMSApplication]
GO
