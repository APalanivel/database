SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                
NAME:                
  dbo.Report_DE_JoinedFile_ATT_2_0            
          
DESCRIPTION:           
 Client Info, Accounts, Meters, Contract, Utility , Rate data        
          
          
INPUT PARAMETERS:              
      Name              DataType          Default     Description              
-----------------------------------------------------------------              
 region_name        
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------         
        
USAGE EXAMPLES:        
------------------------------------------------------------        
Stress /Prod        
exec Report_DE_JoinedFile_ATT_2_0 'cp' --Central Plains        
exec Report_DE_JoinedFile_ATT_2_0 'ne' --Northeast Region        
exec Report_DE_JoinedFile_ATT_2_0   -- All Regions (almost 2 minutes to run 149,304 records)        
         
        
AUTHOR INITIALS:        
         
Initials	Name        
------------------------------------------------------------        
ABK			Aditya Bharadwaj Kolipaka

        
Initials	Date		Modification                
------------------------------------------------------------           
ABK			9/25/2018	https://summit.jira.com/browse/REPTMGR-92

*/

CREATE PROCEDURE [dbo].[Report_DE_JoinedFile_ATT_2_0]
(@region_name VARCHAR(10) = NULL)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Supplier_Contract_Type_Id INT,
            @Utility_Contract_Type_Id INT,
            @Contract_End_Date DATE = '2010-12-31',
            @Account_Table INT,
            @Contract_Table INT,
            @Site_Table INT,
            @Client_Id INT,
            @Default_Analyst INT,
            @Custom_Analyst INT;

    CREATE TABLE #UtilityAccts_CTE
    (
        Account_Number VARCHAR(50),
        Account_Id INT,
        UtilityAccountServiceLevel VARCHAR(200),
        AccountCreated DATETIME,
        AccountInvoiceNotExpected VARCHAR(10),
        AccountNotManaged VARCHAR(10),
        AccountNotExpectedDt DATE,
        Utility VARCHAR(200),
        CommodityType VARCHAR(50),
        Rate VARCHAR(200),
        Site_Id INT,
        Client_Hier_Id INT,
        ADDRESS_ID INT,
        MeterNumber VARCHAR(50),
        Meter_Id INT,
        AccountCreatedBy VARCHAR(30),
        Consumption_Level_Desc VARCHAR(100),
        IsDataEntryOnly VARCHAR(10),
        BrokerFee_Currency_Unit VARCHAR(10),
        BrokerFee_Uom_Unit VARCHAR(25),
        Analyst_Id INT,
        Account_Analyst_Mapping_Cd INT,
        Site_Analyst_Mapping_Cd INT,
        Client_Analyst_Mapping_Cd INT
    );


    CREATE TABLE #Contracts_CTE
    (
        ContractId INT,
        Account_Id INT,
        BaseContractId INT,
        ContractNumber VARCHAR(150),
        ContractType VARCHAR(200),
        ContractedVendor VARCHAR(200),
        ContractStartDate DATETIME,
        ContractEndDate DATETIME,
        CONTRACT_PRICING_SUMMARY VARCHAR(1000),
        SupplierAccountNumber VARCHAR(50),
        ContractCreatedDate DATETIME,
        Meter_Id INT,
        SupplierAccountStartDate DATETIME,
        SupplierAccountEndDate DATETIME,
        SupplierAccountCreatedDate DATETIME
    );


    SELECT @Supplier_Contract_Type_Id = MAX(   CASE
                                                   WHEN ENTITY_NAME = 'Supplier' THEN
                                                       ENTITY_ID
                                               END
                                           ),
           @Utility_Contract_Type_Id = MAX(   CASE
                                                  WHEN ENTITY_NAME = 'Utility' THEN
                                                      ENTITY_ID
                                              END
                                          ),
           @Account_Table = MAX(   CASE
                                       WHEN ENTITY_NAME = 'Account_Table' THEN
                                           ENTITY_ID
                                   END
                               ),
           @Contract_Table = MAX(   CASE
                                        WHEN ENTITY_NAME = 'CONTRACT_TABLE' THEN
                                            ENTITY_ID
                                    END
                                ),
           @Site_Table = MAX(   CASE
                                    WHEN ENTITY_NAME = 'Site_Table' THEN
                                        ENTITY_ID
                                END
                            )
    FROM dbo.ENTITY
    WHERE ENTITY_DESCRIPTION IN ( 'Table_Type', 'Table Type', 'Contract Type', 'Client Type' );

    SELECT @Client_Id = CLIENT_ID
    FROM dbo.CLIENT
    WHERE CLIENT_NAME = 'AT&T Services, Inc.';

    SELECT @Default_Analyst = MAX(   CASE
                                         WHEN c.Code_Value = 'Default' THEN
                                             c.Code_Id
                                     END
                                 ),
           @Custom_Analyst = MAX(   CASE
                                        WHEN c.Code_Value = 'Custom' THEN
                                            c.Code_Id
                                    END
                                )
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'Analyst Type'

    --Utility Accounts        
    ;
    WITH UtilityAccts_CTE
    AS (SELECT cha.Account_Number Account_Number,
               cha.Account_Id Account_Id,
               svl.ENTITY_NAME UtilityAccountServiceLevel,
               ea.MODIFIED_DATE AccountCreated,
               AccountInvoiceNotExpected = CASE cha.Account_Not_Expected
                                               WHEN 1 THEN
                                                   'Yes'
                                               ELSE
                                                   'No'
                                           END,
               AccountNotManaged = CASE cha.Account_Not_Managed
                                       WHEN 1 THEN
                                           'Yes'
                                       ELSE
                                           'No'
                                   END,
               AccountNotExpectedDt = cha.Account_Not_Expected_Dt,
               cha.Account_Vendor_Name Utility,
               com.Commodity_Name CommodityType,
               cha.Rate_Name Rate,
               ch.Site_Id SITE_ID,
               cha.Client_Hier_Id,
               cha.Meter_Address_ID ADDRESS_ID,
               cha.Meter_Id METER_ID,
               cha.Meter_Number MeterNumber,
               ui.USERNAME AccountCreatedBy,
               vcl.Consumption_Level_Desc,
               IsDataEntryOnly = CASE cha.Account_Is_Data_Entry_Only
                                     WHEN 1 THEN
                                         'Yes'
                                     ELSE
                                         'No'
                                 END,
               ecur.CURRENCY_UNIT_NAME BrokerFee_Currency_Unit,
               euom.ENTITY_NAME BrokerFee_Uom_Unit,
               ch.Client_Id,
               cha.Commodity_Id,
               cha.Account_Vendor_Id,
               COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd,
               COALESCE(ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Site_Analyst_Mapping_Cd,
               ch.Client_Analyst_Mapping_Cd Client_Analyst_Mapping_Cd
        FROM Core.Client_Hier_Account cha
            INNER JOIN dbo.ENTITY svl
                ON svl.ENTITY_ID = cha.Account_Service_level_Cd
            INNER JOIN Core.Client_Hier ch
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.ENTITY_AUDIT ea
                ON ea.ENTITY_IDENTIFIER = cha.Account_Id
                   AND ea.ENTITY_ID = @Account_Table
                   AND ea.AUDIT_TYPE = 1
            INNER JOIN dbo.Commodity com
                ON cha.Commodity_Id = com.Commodity_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ea.USER_INFO_ID
            INNER JOIN dbo.Account_Variance_Consumption_Level av
                ON av.ACCOUNT_ID = cha.Account_Id
            INNER JOIN dbo.Variance_Consumption_Level vcl
                ON vcl.Variance_Consumption_Level_Id = av.Variance_Consumption_Level_Id
                   AND vcl.Commodity_Id = cha.Commodity_Id
            LEFT OUTER JOIN dbo.Account_Commodity_Broker_Fee acbf
                ON cha.Account_Id = acbf.Account_Id
                   AND cha.Commodity_Id = acbf.Commodity_Id
            LEFT OUTER JOIN dbo.CURRENCY_UNIT ecur
                ON ecur.CURRENCY_UNIT_ID = acbf.Currency_Unit_Id
            LEFT OUTER JOIN dbo.ENTITY euom
                ON euom.ENTITY_ID = acbf.UOM_Entity_Id
        WHERE cha.Account_Type = 'Utility'
              AND ch.Client_Id = @Client_Id)
    INSERT INTO #UtilityAccts_CTE
    (
        Account_Number,
        Account_Id,
        UtilityAccountServiceLevel,
        AccountCreated,
        AccountInvoiceNotExpected,
        AccountNotManaged,
        AccountNotExpectedDt,
        Utility,
        CommodityType,
        Rate,
        Site_Id,
        Client_Hier_Id,
        ADDRESS_ID,
        MeterNumber,
        Meter_Id,
        AccountCreatedBy,
        Consumption_Level_Desc,
        IsDataEntryOnly,
        BrokerFee_Currency_Unit,
        BrokerFee_Uom_Unit,
        Analyst_Id,
        Account_Analyst_Mapping_Cd,
        Site_Analyst_Mapping_Cd,
        Client_Analyst_Mapping_Cd
    )
    SELECT acc.Account_Number,
           acc.Account_Id,
           acc.UtilityAccountServiceLevel,
           acc.AccountCreated,
           acc.AccountInvoiceNotExpected,
           acc.AccountNotManaged,
           acc.AccountNotExpectedDt,
           acc.Utility,
           acc.CommodityType,
           acc.Rate,
           acc.SITE_ID,
           acc.Client_Hier_Id,
           acc.ADDRESS_ID,
           acc.MeterNumber,
           acc.METER_ID,
           acc.AccountCreatedBy,
           acc.Consumption_Level_Desc,
           acc.IsDataEntryOnly,
           acc.BrokerFee_Currency_Unit,
           acc.BrokerFee_Uom_Unit,
           CASE
               WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN
                   vcam.Analyst_Id
               WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN
                   COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
           END AS Analyst_Id,
           acc.Analyst_Mapping_Cd,
           acc.Site_Analyst_Mapping_Cd,
           acc.Client_Analyst_Mapping_Cd
    FROM UtilityAccts_CTE acc
        LEFT OUTER JOIN dbo.VENDOR_COMMODITY_MAP vcm
            ON acc.Account_Vendor_Id = vcm.VENDOR_ID
               AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
        LEFT OUTER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
            ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
        LEFT OUTER JOIN(Core.Client_Commodity ccc
        INNER JOIN dbo.Code cd
            ON cd.Code_Id = ccc.Commodity_Service_Cd
               AND cd.Code_Value = 'Invoice')
            ON ccc.Client_Id = acc.Client_Id
               AND ccc.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca
            ON aca.Account_Id = acc.Account_Id
               AND aca.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Site_Commodity_Analyst sca
            ON sca.Site_Id = acc.SITE_ID
               AND sca.Commodity_Id = acc.Commodity_Id
        LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca
            ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id;


    CREATE CLUSTERED INDEX [ix_#UtilityAccts_CTE__Site_Id__Address_ID]
    ON [#UtilityAccts_CTE] (
                               [Site_Id],
                               [ADDRESS_ID]
                           );

    --Contracts          

    INSERT INTO #Contracts_CTE
    (
        ContractId,
        Account_Id,
        BaseContractId,
        ContractNumber,
        ContractType,
        ContractedVendor,
        ContractStartDate,
        ContractEndDate,
        CONTRACT_PRICING_SUMMARY,
        SupplierAccountNumber,
        ContractCreatedDate,
        Meter_Id,
        SupplierAccountStartDate,
        SupplierAccountEndDate,
        SupplierAccountCreatedDate
    )
    SELECT c.CONTRACT_ID ContractId,
           cha.Account_Id Account_Id,
           c.BASE_CONTRACT_ID BaseContractId,
           c.ED_CONTRACT_NUMBER ContractNumber,
           e.ENTITY_NAME ContractType,
           cha.Account_Vendor_Name ContractedVendor,
           c.CONTRACT_START_DATE ContractStartDate,
           c.CONTRACT_END_DATE ContractEndDate,
           c.CONTRACT_PRICING_SUMMARY,
           cha.Account_Number SupplierAccountNumber,
           ea.MODIFIED_DATE ContractCreatedDate,
           cha.Meter_Id,
           cha.Supplier_Account_begin_Dt AS SupplierAccountStartDate,
           cha.Supplier_Account_End_Dt AS SupplierAccountEndDate,
           ea1.MODIFIED_DATE SupplierAccountCreatedDate
    FROM dbo.CONTRACT c
        INNER JOIN Core.Client_Hier_Account cha
            ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        INNER JOIN Core.Client_Hier ch
            ON cha.Client_Hier_Id = ch.Client_Hier_Id
        INNER JOIN dbo.ENTITY e
            ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
        INNER JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = c.CONTRACT_ID
               AND ea.ENTITY_ID = @Contract_Table
               AND ea.AUDIT_TYPE = '1'
        LEFT JOIN dbo.ENTITY_AUDIT ea1
            ON ea1.ENTITY_IDENTIFIER = cha.Account_Id
               AND ea1.ENTITY_ID = @Account_Table --494                      
               AND ea1.AUDIT_TYPE = 1
    WHERE (
              c.CONTRACT_TYPE_ID = @Utility_Contract_Type_Id
              OR
              (
                  c.CONTRACT_TYPE_ID = @Supplier_Contract_Type_Id
                  AND c.CONTRACT_END_DATE > @Contract_End_Date
              )
          )
          AND cha.Account_Type = 'Supplier'
          AND ch.Client_Id = @Client_Id;

    INSERT INTO #Contracts_CTE
    (
        ContractId,
        Account_Id,
        BaseContractId,
        ContractNumber,
        ContractType,
        ContractedVendor,
        ContractStartDate,
        ContractEndDate,
        CONTRACT_PRICING_SUMMARY,
        SupplierAccountNumber,
        ContractCreatedDate,
        Meter_Id,
        SupplierAccountStartDate,
        SupplierAccountEndDate,
        SupplierAccountCreatedDate
    )
    SELECT -1 ContractId,
           cha.Account_Id Account_Id,
           NULL BaseContractId,
           NULL ContractNumber,
           NULL ContractType,
           NULL ContractedVendor,
           NULL ContractStartDate,
           NULL ContractEndDate,
           NULL CONTRACT_PRICING_SUMMARY,
           suppacc.ACCOUNT_NUMBER SupplierAccountNumber,
           NULL ContractCreatedDate,
           cha.Meter_Id,
           suppacc.Supplier_Account_Begin_Dt AS SupplierAccountStartDate,
           suppacc.Supplier_Account_End_Dt AS SupplierAccountEndDate,
           ea.MODIFIED_DATE SupplierAccountCreatedDate
    FROM dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        INNER JOIN dbo.ACCOUNT suppacc
            ON samm.ACCOUNT_ID = suppacc.ACCOUNT_ID
        LEFT JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = suppacc.ACCOUNT_ID
               AND ea.ENTITY_ID = @Account_Table --494                      
               AND ea.AUDIT_TYPE = 1
        INNER JOIN Core.Client_Hier_Account cha
            ON samm.ACCOUNT_ID = cha.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON cha.Client_Hier_Id = ch.Client_Hier_Id
    WHERE samm.Contract_ID = -1
          AND ch.Client_Id = @Client_Id;

    CREATE CLUSTERED INDEX [ix_#Contracts_CTE__Meter_Id]
    ON [#Contracts_CTE] ([Meter_Id]);

    --SiteList        
    SELECT ch.Client_Name [ClientName],
           ch.Client_Id [ClientID],
           ClientNotManaged = ISNULL(   CASE ch.Client_Not_Managed
                                            WHEN '1' THEN
                                                'Yes'
                                            ELSE
                                                'No'
                                        END,
                                        'Null'
                                    ),
           ch.Sitegroup_Name Division_Name,
           ch.Sitegroup_Id [DivisionID],
           DivisionNotManaged = ISNULL(   CASE ch.Site_Not_Managed
                                              WHEN '1' THEN
                                                  'Yes'
                                              ELSE
                                                  'No'
                                          END,
                                          'Null'
                                      ),
           ch.Site_name [SiteName],
           ch.Site_Id [SiteID],
           ea.MODIFIED_DATE SiteCreated,
           s.SITE_REFERENCE_NUMBER [SiteRef.],
           SiteNotManaged = ISNULL(   CASE ch.Site_Not_Managed
                                          WHEN '1' THEN
                                              'Yes'
                                          ELSE
                                              'No'
                                      END,
                                      'Null'
                                  ),
           SiteClosed = ISNULL(   CASE ch.Site_Closed
                                      WHEN '1' THEN
                                          'Yes'
                                      ELSE
                                          'No'
                                  END,
                                  'Null'
                              ),
           SiteClosedDt = CASE ch.Site_Closed
                              WHEN '0' THEN
                                  NULL
                              WHEN '1' THEN
                                  ch.Site_Closed_Dt
                          END,
           ch.Site_Type_Name SiteType,
           addr.ADDRESS_LINE1 [AddressLine1],
           addr.ADDRESS_LINE2 [AddressLine2],
           addr.CITY City,
           st.STATE_NAME [State_Province],
           addr.ZIPCODE [ZipCode],
           Ctry.COUNTRY_NAME [Country],
           reg.REGION_NAME [Region],
           uacte.Account_Number [AccountNumber],
           uacte.UtilityAccountServiceLevel,
           uacte.AccountCreated,
           uacte.AccountInvoiceNotExpected,
           uacte.AccountNotManaged,
           uacte.AccountNotExpectedDt,
           uacte.Utility,
           uacte.CommodityType,
           uacte.Rate,
           uacte.MeterNumber,
           cc.ContractId,
           cc.ContractNumber,
           cc.ContractType,
           cc.ContractedVendor,
           cc.ContractStartDate,
           cc.ContractEndDate,
           cc.CONTRACT_PRICING_SUMMARY,
           cc.SupplierAccountNumber,
           cc.ContractCreatedDate,
           uacte.AccountCreatedBy,
           uacte.Consumption_Level_Desc,
           uacte.IsDataEntryOnly,
           uacte.Account_Id [Utility Account ID],
           cc.Account_Id [Supplier Account ID],
           cc.SupplierAccountStartDate,
           CASE
               WHEN cc.ContractId = -1
                    AND cc.SupplierAccountEndDate IS NULL THEN
                   'Unspecified'
               ELSE
                   CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
           END AS SupplierAccountEndDate,
           cc.SupplierAccountCreatedDate,
           uacte.Meter_Id AS Meter_Id
    FROM Core.Client_Hier ch
        JOIN dbo.SITE s
            ON ch.Site_Id = s.SITE_ID
        JOIN dbo.ADDRESS addr
            ON addr.ADDRESS_PARENT_ID = ch.Site_Id
        JOIN dbo.STATE st
            ON st.STATE_ID = addr.STATE_ID
        JOIN dbo.COUNTRY Ctry
            ON Ctry.COUNTRY_ID = st.COUNTRY_ID
        JOIN dbo.REGION reg
            ON reg.REGION_ID = st.REGION_ID
        JOIN dbo.ENTITY_AUDIT ea
            ON ea.ENTITY_IDENTIFIER = ch.Site_Id
               AND ea.ENTITY_ID = @Site_Table
               AND ea.AUDIT_TYPE = 1
        LEFT OUTER JOIN dbo.RM_GROUP_SITE_MAP rgm
            ON rgm.SITE_ID = ch.Site_Id
        LEFT OUTER JOIN #UtilityAccts_CTE uacte
            ON uacte.Site_Id = ch.Site_Id
               AND uacte.ADDRESS_ID = addr.ADDRESS_ID
        LEFT OUTER JOIN #Contracts_CTE cc
            ON cc.Meter_Id = uacte.Meter_Id
    WHERE (
              reg.REGION_NAME = @region_name
              OR @region_name IS NULL
          )
          AND ch.Client_Id = @Client_Id
    GROUP BY ch.Client_Name,
             ch.Client_Id,
             ch.Client_Not_Managed,
             ch.Sitegroup_Name,
             ch.Sitegroup_Id,
             ch.Division_Not_Managed,
             ch.Site_name,
             ch.Site_Id,
             ea.MODIFIED_DATE,
             s.SITE_REFERENCE_NUMBER,
             ch.Site_Not_Managed,
             ch.Site_Closed,
             ch.Site_Closed_Dt,
             ch.Site_Type_Name,
             addr.ADDRESS_LINE1,
             addr.ADDRESS_LINE2,
             addr.CITY,
             st.STATE_NAME,
             addr.ZIPCODE,
             Ctry.COUNTRY_NAME,
             reg.REGION_NAME,
             uacte.Account_Number,
             uacte.UtilityAccountServiceLevel,
             uacte.AccountCreated,
             uacte.AccountInvoiceNotExpected,
             uacte.AccountNotManaged,
             uacte.AccountNotExpectedDt,
             uacte.Utility,
             uacte.CommodityType,
             uacte.Rate,
             uacte.MeterNumber,
             cc.ContractId,
             BaseContractId,
             cc.ContractNumber,
             cc.ContractType,
             cc.ContractedVendor,
             cc.ContractStartDate,
             cc.ContractEndDate,
             cc.CONTRACT_PRICING_SUMMARY,
             cc.SupplierAccountNumber,
             cc.ContractCreatedDate,
             addr.ADDRESS_ID,
             uacte.AccountCreatedBy,
             uacte.Consumption_Level_Desc,
             uacte.IsDataEntryOnly,
             uacte.Account_Id,
             cc.Account_Id,
             uacte.BrokerFee_Currency_Unit,
             uacte.BrokerFee_Uom_Unit,
             cc.SupplierAccountStartDate,
             CASE
                 WHEN cc.ContractId = -1
                      AND cc.SupplierAccountEndDate IS NULL THEN
                     'Unspecified'
                 ELSE
                     CONVERT(VARCHAR(10), cc.SupplierAccountEndDate, 101)
             END,
             cc.SupplierAccountCreatedDate,
             uacte.Meter_Id;

    DROP TABLE #UtilityAccts_CTE;
    DROP TABLE #Contracts_CTE;

END;


GO
GRANT EXECUTE ON  [dbo].[Report_DE_JoinedFile_ATT_2_0] TO [CBMS_SSRS_Reports]
GO
GRANT EXECUTE ON  [dbo].[Report_DE_JoinedFile_ATT_2_0] TO [CBMSApplication]
GO
