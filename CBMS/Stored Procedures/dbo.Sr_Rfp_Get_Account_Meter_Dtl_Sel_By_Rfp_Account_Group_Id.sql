SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 dbo.Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id

DESCRIPTION:
  

INPUT PARAMETERS:
Name					DataType		Default			Description
-------------------------------------------------------------------------
@Rfp_Account_Group_Id	INT
@Is_Bid_Group			BIT
	
       
OUTPUT PARAMETERS:
Name				DataType		Default			Description
-------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------

  EXEC  dbo.Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id 12105709,0
  
  EXEC  dbo.Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id 10012153,1
  
  EXEC  dbo.Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id 10012153,1

      
AUTHOR INITIALS:
Initials		Name
-------------------------------------------------------------------------
NR				Narayana Reddy
	
MODIFICATIONS

Initials	Date			Modification
-------------------------------------------------------------------------
NR			2016-06-13		Created for GCS Phase-5 GCS-992.	
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id]
      ( 
       @Rfp_Account_Group_Id INT
      ,@Is_Bid_Group BIT )
AS 
BEGIN
      SET NOCOUNT ON
      IF @is_bid_group = 1 
            BEGIN
                  SELECT
                        cha.Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,cha.Meter_Address_Line_1
                       ,cha.Meter_City + ', ' + cha.Meter_State_Name AS Meter_City_State
                       ,cha.Meter_Country_Name
                  FROM
                        SR_RFP_ACCOUNT AS rfp_account
                        INNER JOIN SR_RFP_BID_GROUP AS bid_group
                              ON rfp_account.SR_RFP_BID_GROUP_ID = bid_group.SR_RFP_BID_GROUP_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                              ON sramm.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfp_account.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Meter_Id = sramm.METER_ID
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        ( rfp_account.SR_RFP_BID_GROUP_ID = @Rfp_Account_Group_Id )
                        AND ( rfp_account.IS_DELETED = 0 )
                 
            END
      ELSE 
            BEGIN
    
                  SELECT
                        cha.Account_Number
                       ,cha.Alternate_Account_Number
                       ,cha.Meter_Number
                       ,cha.Meter_Address_Line_1
                       ,cha.Meter_City + ', ' + cha.Meter_State_Name AS Meter_City_State
                       ,cha.Meter_Country_Name
                  FROM
                        SR_RFP_ACCOUNT AS rfp_account
                        INNER JOIN dbo.SR_RFP_ACCOUNT_METER_MAP sramm
                              ON sramm.SR_RFP_ACCOUNT_ID = rfp_account.SR_RFP_ACCOUNT_ID
                        INNER JOIN core.Client_Hier_Account cha
                              ON rfp_account.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Meter_Id = sramm.METER_ID
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        ( rfp_account.SR_RFP_ACCOUNT_ID = @Rfp_Account_Group_Id )
                        AND ( rfp_account.SR_RFP_BID_GROUP_ID IS NULL )
                        AND ( rfp_account.IS_DELETED = 0 )
                  
            END
      
END


;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Get_Account_Meter_Dtl_Sel_By_Rfp_Account_Group_Id] TO [CBMSApplication]
GO
