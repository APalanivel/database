SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cost_Usage_Account_Bucket_Value_Sel_By_Bucket_Name]  
     
DESCRIPTION:

	To get the bucket values from Cost_Usage_Account_Dtl based on the given input

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Client_Hier_Id			INT
@Account_id				INT
@Bucket_Name			VARCHAR(200)
@Bucket_Type			VARCHAR(25)
@Service_Month			DATE

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Bucket_Name  101425,390500, 'Late Fees','Charge','2010-07-01'
	EXEC Cost_Usage_Account_Bucket_Value_Sel_By_Bucket_Name  284694,507226, 'Tax','Charge','2012-04-01'

	SELECT TOP 10 
		cuad.Bucket_Value
		,cuad.UOM_Type_Id
		,cuad.CURRENCY_UNIT_ID
		,com.Commodity_Name
		,com.Commodity_Id
		,account_id
		,service_month
		,bm.bucket_name
		,cuad.Client_Hier_Id
	FROM
		dbo.Cost_Usage_Account_Dtl cuad (NOLOCK)
		INNER JOIN dbo.Bucket_Master bm
			ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
		INNER JOIN dbo.Code bt
			ON bt.Code_Id = bm.Bucket_Type_Cd
		INNER JOIN dbo.Commodity com
			ON com.Commodity_Id	= bm.Commodity_Id			
	WHERE
		cuad.Service_Month <= getdate()
		AND bm.Bucket_Name = 'Tax'
		AND bucket_Value >0
	ORDER BY
		service_month desc
     
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
HG			08/29/2011	Created for Additional Data enhancement requirement

*/

CREATE PROCEDURE dbo.Cost_Usage_Account_Bucket_Value_Sel_By_Bucket_Name
      @Client_Hier_Id INT
     ,@Account_id INT
     ,@Bucket_Name VARCHAR(200)
     ,@Bucket_Type VARCHAR(25)
     ,@Service_Month DATE
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            cuad.Bucket_Value
           ,cuad.UOM_Type_Id
           ,un.Entity_Name AS Uom_Name
           ,cuad.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           ,com.Commodity_Name
           ,com.Commodity_Id
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
			left OUTER JOIN dbo.Entity un
				ON un.Entity_id = cuad.UOM_Type_Id
			left OUTER JOIN dbo.CURRENCY_UNIT cu
				ON cu.CURRENCY_UNIT_ID = cuad.CURRENCY_UNIT_ID
      WHERE
            cuad.ACCOUNT_ID = @Account_id
            AND cuad.Client_Hier_Id = @Client_Hier_Id
            AND cuad.Service_Month = @Service_Month
            AND bm.Bucket_Name = @Bucket_Name
            AND bt.Code_Value = @Bucket_Type

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Value_Sel_By_Bucket_Name] TO [CBMSApplication]
GO
