SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Detail_Snap_Shot_Del]  
     
DESCRIPTION: 
	It Deletes Budget Detail Snap Shot for Selected Budget Detail Snap Shot Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Budget_Detail_Snap_Shot_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN
		EXEC Budget_Detail_Snap_Shot_Del 80
	ROLLBACK TRAN

    
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Detail_Snap_Shot_Del
    (
      @Budget_Detail_Snap_Shot_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
	FROM
		dbo.BUDGET_DETAIL_SNAP_SHOT
	WHERE
		BUDGET_DETAIL_SNAP_SHOT_ID = @Budget_Detail_Snap_Shot_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Detail_Snap_Shot_Del] TO [CBMSApplication]
GO
