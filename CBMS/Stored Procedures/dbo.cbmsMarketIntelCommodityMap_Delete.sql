SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[cbmsMarketIntelCommodityMap_Delete]
	( @MyAccountId int
	, @MarketIntelId int
	)
AS
BEGIN

	set nocount on

	  delete market_intel_commodity_map
	   where market_intel_id = @MarketIntelId

--	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntelCommodityMap_Delete] TO [CBMSApplication]
GO
