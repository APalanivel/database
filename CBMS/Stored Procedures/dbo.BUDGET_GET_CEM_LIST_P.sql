SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_CEM_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	        	
******/

CREATE          PROCEDURE dbo.BUDGET_GET_CEM_LIST_P
@userId varchar(10),
@sessionId varchar(20)


AS
begin
set nocount on
select 	userInfo.user_info_id ,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from 	user_info userInfo, user_info_group_info_map map, group_info groupInfo

where 	map.group_info_id = groupInfo.group_info_id
	and map.user_info_id = userInfo.user_info_id
	and groupInfo.group_name='cem'

order by userInfo.FIRST_NAME
end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CEM_LIST_P] TO [CBMSApplication]
GO
