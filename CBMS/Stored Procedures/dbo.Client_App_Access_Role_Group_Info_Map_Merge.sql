SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                            
/******             
                         
 NAME: [dbo].[Client_App_Access_Role_Group_Info_Map_Merge]                          
                            
 DESCRIPTION:                            
			To update the Client_App_Access_Role_Group_Info_Map table.                            
                            
 INPUT PARAMETERS:            
                           
 Name                            DataType           Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @Client_App_Access_Role_Id      INT                  
 @Group_Info_Id                  VARCHAR(MAX)       NULL           
 @Assigned_User_Id               INT                          
                            
 OUTPUT PARAMETERS:                 
                            
 Name                            DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          
----Example 1------          
 BEGIN TRAN              
            
 SELECT              
      *              
 FROM              
      dbo.Client_App_Access_Role_Group_Info_Map              
 WHERE              
      Client_App_Access_Role_Id = 2               
                    
 EXEC dbo.Client_App_Access_Role_Group_Info_Map_Merge               
      @Client_App_Access_Role_Id = 2              
     ,@Group_Info_Id = '3,4,5,6,7'              
     ,@Assigned_User_Id=39116             
                   
 SELECT              
      *              
 FROM              
      dbo.Client_App_Access_Role_Group_Info_Map              
 WHERE              
      Client_App_Access_Role_Id = 2                  
 ROLLBACK TRAN           
          
          
----Example 2------          
 BEGIN TRAN              
            
 SELECT              
      *              
 FROM              
      dbo.Client_App_Access_Role_Group_Info_Map              
 WHERE              
      Client_App_Access_Role_Id = 2               
                    
 EXEC dbo.Client_App_Access_Role_Group_Info_Map_Merge               
      @Client_App_Access_Role_Id = 2              
     ,@Group_Info_Id = '1,2,3,4,5,6'              
     ,@Assigned_User_Id=39116                                  
           
                   
 SELECT              
      *              
 FROM              
      dbo.Client_App_Access_Role_Group_Info_Map              
 WHERE              
      Client_App_Access_Role_Id = 2                  
 ROLLBACK TRAN          
                           
 AUTHOR INITIALS:            
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SP                     Sandeep Pigilam              
                             
 MODIFICATIONS:          
           
 Initials               Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                     2013-11-25       Created for RA Admin user management                          
                           
******/                                
                            
CREATE PROCEDURE [dbo].[Client_App_Access_Role_Group_Info_Map_Merge]
      ( 
       @Client_App_Access_Role_Id INT
      ,@Group_Info_Id VARCHAR(MAX) = NULL
      ,@Assigned_User_Id INT )
AS 
BEGIN              
                
      SET NOCOUNT ON;                 
                            
      DECLARE @Client_App_Access_Role_Group_Info_Map_List TABLE
            ( 
             Client_App_Access_Role_Id INT
            ,Group_Info_Id INT )                          
             
      INSERT      INTO @Client_App_Access_Role_Group_Info_Map_List
                  ( 
                   Client_App_Access_Role_Id
                  ,Group_Info_Id )
                  SELECT
                        @Client_App_Access_Role_Id
                       ,ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id, ',') ufn                       
                   
      BEGIN TRY                                
            BEGIN TRANSACTION                                 
                         
            MERGE INTO dbo.Client_App_Access_Role_Group_Info_Map AS tgt
                  USING 
                        ( SELECT
                              urr.Client_App_Access_Role_Id
                             ,urr.Group_Info_Id
                          FROM
                              @Client_App_Access_Role_Group_Info_Map_List urr ) AS src
                  ON tgt.Client_App_Access_Role_Id = src.Client_App_Access_Role_Id
                        AND tgt.Group_Info_Id = src.Group_Info_Id
                  WHEN NOT MATCHED BY SOURCE AND  tgt.Client_App_Access_Role_Id = @Client_App_Access_Role_Id
                        THEN                        
    DELETE
                  WHEN NOT MATCHED BY TARGET 
                        THEN                        
    INSERT
                              ( 
                               Client_App_Access_Role_Id
                              ,Group_Info_Id
                              ,Assigned_User_Id
                              ,Assigned_Ts )
                         VALUES
                              ( 
                               src.Client_App_Access_Role_Id
                              ,src.Group_Info_Id
                              ,@Assigned_User_Id
                              ,GETDATE() );                        
                                             
            COMMIT TRANSACTION                                         
                                           
      END TRY                            
      BEGIN CATCH                            
            IF @@TRANCOUNT > 0 
                  BEGIN            
                        ROLLBACK TRANSACTION            
                  END                           
            EXEC usp_RethrowError                            
      END CATCH 
                                               
                                   
END 

;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Group_Info_Map_Merge] TO [CBMSApplication]
GO
