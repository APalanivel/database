SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******            
NAME:   dbo.Client_Commodity_Sel_By_Service_Level_Hier_Level
  
      
DESCRIPTION:     
 Gets a list of ClientName and CommodityName       
   
        
INPUT PARAMETERS:            
Name               DataType      Default   Description            
------------------------------------------------------------            
@Hier_Level         Varchar(25)	  Account			
@Service_Level      Varchar(25)	  Invoice 
@Client_Not_Managed INT			  0

OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   

	Exec [dbo].[Client_Commodity_Sel_By_Service_Level_Hier_Level]

	Exec [dbo].[Client_Commodity_Sel_By_Service_Level_Hier_Level] 'Site','Invoice'
         
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
AKR   Ashok Kumar Raju  
       
       
MODIFICATIONS             
Initials	Date		Modification            
------------------------------------------------------------
AKR			13/06/2011	Created
  
*****/   
CREATE PROCEDURE [dbo].[Client_Commodity_Sel_By_Service_Level_Hier_Level]
(
 @Hier_Level VARCHAR(25) = 'Account'
,@Service_Level VARCHAR(25) = 'Invoice'
,@Client_Not_Managed INT = 0 )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            cl.Client_Name
           ,c.Commodity_Name
      FROM
            core.Client_Commodity cc
            INNER JOIN dbo.Code hl
                  ON hl.Code_Id = cc.Hier_Level_Cd
            INNER JOIN dbo.Code cs
                  ON cs.Code_Id = cc.Commodity_Service_Cd
            INNER JOIN dbo.Client cl
                  ON cl.Client_Id = cc.Client_Id
            INNER JOIN dbo.Commodity c
                  ON c.Commodity_Id = cc.Commodity_Id
      WHERE
            hl.Code_Value = @Hier_Level
            AND cs.Code_Value = @Service_Level
            AND cl.NOT_MANAGED = @Client_Not_Managed
      ORDER BY
            cl.CLIENT_NAME
           ,c.Commodity_Name

END
GO

GRANT EXECUTE ON  [dbo].[Client_Commodity_Sel_By_Service_Level_Hier_Level] TO [CBMSApplication]
GO