SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE                   PROCEDURE 
[dbo].[cbmsCostUsageSite_GetDetailElForBudget]
--128,3,12,25,2006,1025
--128,3,12,25,2006,110,313,188






	( @MyAccountId int
	, @currency_unit_id int
	, @el_unit_of_measure_type_id int
	, @ng_unit_of_measure_type_id int
	, @report_year int
	, @client_id int
	, @division_id int = null
	, @site_id int = null
	)
AS
BEGIN

	declare @active_table varchar(50)

	select @active_table = active_table from RPT_VARIANCE_COST_USAGE_SITE_FLAG

	

if @active_table = 'RPT_VARIANCE_COST_USAGE_SITE_1'
begin


--CLIENT VIEW - show divisions rolled up
if @division_id is null and @site_id is null and @client_id is not null
  begin

	 select  division_id
		, division_name
		, field_type 
		, field 
		, sum(month1) month1
		, sum(month2) month2
		, sum(month3) month3
		, sum(month4) month4
		, sum(month5) month5
		, sum(month6)   month6
		, sum(month7)   month7
		, sum(month8)   month8
		, sum(month9)   month9
		, sum(month10)  month10
		, sum(month11) month11
		, sum(month12) month12
		, sum(total)  total
		, max(month1_complete) month1_complete
		, max(month2_complete) month2_complete
		, max(month3_complete) month3_complete
		, max(month4_complete) month4_complete
		, max(month5_complete) month5_complete
		, max(month6_complete) month6_complete
		, max(month7_complete) month7_complete
		, max(month8_complete) month8_complete
		, max(month9_complete) month9_complete
		, max(month10_complete)month10_complete
		, max(month11_complete)month11_complete
		, max(month12_complete)month12_complete
		, max(month1_published) month1_published
		, max(month2_published) month2_published
		, max(month3_published) month3_published
		, max(month4_published) month4_published
		, max(month5_published) month5_published
		, max(month6_published) month6_published
		, max(month7_published) month7_published
		, max(month8_published) month8_published
		, max(month9_published) month9_published
		, max(month10_published) month10_published
		, max(month11_published) month11_published
		, max(month12_published) month12_published
		, max(month1_under_review) month1_under_review
		, max(month2_under_review) month2_under_review
		, max(month3_under_review) month3_under_review
		, max(month4_under_review) month4_under_review
		, max(month5_under_review) month5_under_review
		, max(month6_under_review) month6_under_review
		, max(month7_under_review) month7_under_review
		, max(month8_under_review) month8_under_review
		, max(month9_under_review) month9_under_review
		, max(month10_under_review) month10_under_review
		, max(month11_under_review)  month11_under_review
		, max(month12_under_review) month12_under_review

from rpt_variance_cost_usage_site_1

	    where client_id = @client_id
		and report_year = @report_year
		and commodity_type = 'Electric Power'
		group by
		 division_id
		, division_name
		, field_type 
		, field 
		order by division_name
			, field_type desc



end


--if they have a division id but no site id
--DIVISION VIEW - show divisions rolled up
if  @division_id is not null

  begin
	  


 select  site_id
		, site_name
		, field_type 
		, field 
		, month1
		, month2
		, month3
		, month4
		, month5
		, month6  
		, month7  
		, month8  
		, month9  
		, month10 
		, month11 
		, month12 
		, total

		, month1_complete 
		, month2_complete 
		, month3_complete 
		, month4_complete 
		, month5_complete 
		, month6_complete 
		, month7_complete 
		, month8_complete 
		, month9_complete 
		, month10_complete
		, month11_complete
		, month12_complete

		, month1_published 
		, month2_published
		, month3_published
		, month4_published
		, month5_published
		, month6_published
		, month7_published
		, month8_published
		, month9_published
		, month10_published
		, month11_published
		, month12_published

		, month1_under_review
		, month2_under_review
		, month3_under_review
		, month4_under_review
		, month5_under_review
		, month6_under_review
		, month7_under_review
		, month8_under_review
		, month9_under_review
		, month10_under_review 
		, month11_under_review 
		, month12_under_review 

	     from rpt_variancE_cost_usage_site_1

	    where client_id = @client_id
		and division_id = @division_id
		and site_id = isNull(@site_id, site_id)
		and report_year = @report_year
		and commodity_type = 'Electric Power'

	 order by site_name
			, field_type desc




end 


end
else
begin





--CLIENT VIEW - show divisions rolled up
if @division_id is null and @site_id is null and @client_id is not null
  begin

	 select  division_id
		, division_name
		, field_type 
		, field 
		, sum(month1) month1
		, sum(month2) month2
		, sum(month3) month3
		, sum(month4) month4
		, sum(month5) month5
		, sum(month6)   month6
		, sum(month7)   month7
		, sum(month8)   month8
		, sum(month9)   month9
		, sum(month10)  month10
		, sum(month11) month11
		, sum(month12) month12
		, sum(total)  total
		, max(month1_complete) month1_complete
		, max(month2_complete) month2_complete
		, max(month3_complete) month3_complete
		, max(month4_complete) month4_complete
		, max(month5_complete) month5_complete
		, max(month6_complete) month6_complete
		, max(month7_complete) month7_complete
		, max(month8_complete) month8_complete
		, max(month9_complete) month9_complete
		, max(month10_complete)month10_complete
		, max(month11_complete)month11_complete
		, max(month12_complete)month12_complete
		, max(month1_published) month1_published
		, max(month2_published) month2_published
		, max(month3_published) month3_published
		, max(month4_published) month4_published
		, max(month5_published) month5_published
		, max(month6_published) month6_published
		, max(month7_published) month7_published
		, max(month8_published) month8_published
		, max(month9_published) month9_published
		, max(month10_published) month10_published
		, max(month11_published) month11_published
		, max(month12_published) month12_published
		, max(month1_under_review) month1_under_review
		, max(month2_under_review) month2_under_review
		, max(month3_under_review) month3_under_review
		, max(month4_under_review) month4_under_review
		, max(month5_under_review) month5_under_review
		, max(month6_under_review) month6_under_review
		, max(month7_under_review) month7_under_review
		, max(month8_under_review) month8_under_review
		, max(month9_under_review) month9_under_review
		, max(month10_under_review) month10_under_review
		, max(month11_under_review)  month11_under_review
		, max(month12_under_review) month12_under_review

from rpt_variance_cost_usage_site_2

	    where client_id = @client_id
		and report_year = @report_year
		and commodity_type = 'Electric Power'
		group by
		 division_id
		, division_name
		, field_type 
		, field 
		order by division_name
			, field_type desc



end


--if they have a division id but no site id
--DIVISION VIEW - show divisions rolled up
if  @division_id is not null

  begin
	  


 select  site_id
		, site_name
		, field_type 
		, field 
		, month1
		, month2
		, month3
		, month4
		, month5
		, month6  
		, month7  
		, month8  
		, month9  
		, month10 
		, month11 
		, month12 
		, total

		, month1_complete 
		, month2_complete 
		, month3_complete 
		, month4_complete 
		, month5_complete 
		, month6_complete 
		, month7_complete 
		, month8_complete 
		, month9_complete 
		, month10_complete
		, month11_complete
		, month12_complete

		, month1_published 
		, month2_published
		, month3_published
		, month4_published
		, month5_published
		, month6_published
		, month7_published
		, month8_published
		, month9_published
		, month10_published
		, month11_published
		, month12_published

		, month1_under_review
		, month2_under_review
		, month3_under_review
		, month4_under_review
		, month5_under_review
		, month6_under_review
		, month7_under_review
		, month8_under_review
		, month9_under_review
		, month10_under_review 
		, month11_under_review 
		, month12_under_review 

	     from rpt_variancE_cost_usage_site_2

	    where client_id = @client_id
		and division_id = @division_id
		and site_id = isNull(@site_id, site_id)
		and report_year = @report_year
		and commodity_type = 'Electric Power'

	 order by site_name
			, field_type desc




end 


end



END
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetDetailElForBudget] TO [CBMSApplication]
GO
