
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_BUDGET_NG_UNIT_COST_DETAILS_P
    
Description:
    What is the business description/function of the procedure.

Input Parameters:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@BudgetID				INT
	@From_Month			DATETIME
	@To_Month				DATETIME

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec dbo.BUDGET_GET_BUDGET_NG_UNIT_COST_DETAILS_P 283,'01/01/2010','12/31/2010'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		Athmaram Pabbathi

MODIFICATIONS:
Initials	Date		    Modification
--------	----------    ----------------------------------------------
AP		07/25/2011    Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
AP		07/26/2011    Removed unused parameters @User_ID VARCHAR(10) & @Session_ID VARCHAR(20)
AP		08/22/2011    Added qualifiers to all objects with the owner name.
AP		09/22/2011    Removed hardcoded bucket name and used dbo.Cost_usage_Bucket_Sel_By_Commodity instead.
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_BUDGET_NG_UNIT_COST_DETAILS_P]
      ( 
       @BudgetID INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE
            @Consumption_Conv_Unit_ID INT
           ,@Currency_Conv_Unit_ID INT
           ,@Currency_Group_ID INT
           ,@UOM_Entity_Type INT
           ,@Commodity_Id INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )
                 
      SELECT
            @Commodity_Id = com.Commodity_Id
           ,@UOM_Entity_Type = com.UOM_Entity_Type
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas'

      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id 

      SELECT
            @Consumption_Conv_Unit_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Type = @UOM_Entity_Type
            AND uom.Entity_Name = 'MMbtu'
            
      SELECT
            @Currency_Conv_Unit_ID = cu.Currency_Unit_Id
      FROM
            dbo.Currency_Unit cu
      WHERE
            cu.Currency_Unit_Name = 'USD'
 
      SELECT
            @Currency_Group_ID = cg.currency_group_id
      FROM
            dbo.Currency_Group cg
      WHERE
            cg.Currency_Group_Name = 'Standard' ;


      WITH  CTE_Max_Currency_Conv_Date
              AS ( SELECT
                        max(cuc.Conversion_Date) AS Max_Conversion_Date
                       ,cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id
                   FROM
                        dbo.Currency_Unit_Conversion cuc
                   WHERE
                        cuc.Converted_Unit_ID = @Currency_Conv_Unit_ID
                        AND cuc.Currency_Group_ID = @Currency_Group_ID
                   GROUP BY
                        cuc.Base_Unit_Id
                       ,cuc.Converted_Unit_Id
                       ,cuc.Currency_Group_Id),
            CTE_Cost_Usage_Account_Info
              AS ( SELECT
                        BAT.Budget_Account_ID AS BudgetAccountID
                       ,BA.CD_Create_Multiplier AS ConversionFactor
                       ,CUAD.Service_Month AS ServiceMonth
                       ,isnull(( case WHEN CUB.Bucket_Type = 'Charge' THEN CUAD.Bucket_Value * CurConv.Conversion_Factor
                                 END ), 0) AS Total_Cost
                       ,isnull(( case WHEN CUB.Bucket_Type = 'Determinant' THEN CUAD.Bucket_Value * UC.Conversion_Factor
                                 END ), 0) AS Total_Volume
                   FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN dbo.Budget_Account BA
                              ON BA.Account_ID = CUAD.Account_ID
                        INNER JOIN dbo.Budget B
                              ON B.Budget_ID = BA.Budget_ID
                        INNER JOIN dbo.Budget_Account_Type_VW BAT
                              ON BAT.Budget_Account_ID = BA.Budget_Account_ID
                                 AND BAT.Budget_ID = B.Budget_ID
                        INNER JOIN @Cost_Usage_Bucket_Id CUB
                              ON CUB.Bucket_Master_Id = CUAD.Bucket_Master_Id
		--Consumption Unit Converstion		
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UC
                              ON UC.Base_Unit_ID = CUAD.UOM_Type_ID
                                 AND UC.Converted_Unit_ID = @Consumption_Conv_Unit_ID
		--Currency Unit Converstion		
                        LEFT OUTER JOIN ( dbo.Currency_Unit_Conversion CurConv
                                          INNER JOIN CTE_Max_Currency_Conv_Date max_date
                                                ON CurConv.Base_Unit_Id = max_date.Base_Unit_Id
                                                   AND CurConv.Converted_Unit_ID = max_date.Converted_Unit_ID
                                                   AND CurConv.Currency_Group_ID = max_date.Currency_Group_ID
                                                   AND CurConv.Conversion_Date = max_date.Max_Conversion_Date )
                                          ON CurConv.Base_Unit_ID = CUAD.Currency_Unit_ID
                                             AND CurConv.Converted_Unit_ID = @Currency_Conv_Unit_ID
                                             AND CurConv.Currency_Group_ID = @Currency_Group_ID
                   WHERE
                        BA.Budget_ID = @BudgetID
                        AND BAT.Budget_Account_Type IN ( 'C&D', 'C&D Created' )
                        AND CUAD.Service_Month BETWEEN @From_Month
                                               AND     @To_Month)
            SELECT
                  CUA.BudgetAccountID
                 ,CUA.ConversionFactor
                 ,CUA.ServiceMonth
                 ,sum(CUA.Total_Cost) / nullif(sum(CUA.Total_Volume), 0) AS Unit_Cost
            FROM
                  CTE_Cost_Usage_Account_Info CUA
            GROUP BY
                  CUA.BudgetAccountID
                 ,CUA.ConversionFactor
                 ,CUA.ServiceMonth
			
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_NG_UNIT_COST_DETAILS_P] TO [CBMSApplication]
GO
