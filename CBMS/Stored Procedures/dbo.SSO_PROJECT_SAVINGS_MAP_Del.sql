SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.SSO_PROJECT_SAVINGS_MAP_Del

DESCRIPTION:

	Used to delete SSO_PROJECT_SAVINGS_MAP .

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@SSO_Project_Id		INT
	@SSO_Savings_Id		INT

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.SSO_PROJECT_SAVINGS_MAP_Del 1533, 7351

	ROLLBACK TRAN

	SELECT TOP 100 * FROM SSO_PROJECT_SAVINGS_MAP
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created

*/
CREATE PROCEDURE dbo.SSO_PROJECT_SAVINGS_MAP_Del
    (
       @SSO_Project_Id	INT
       ,@SSO_Savings_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.SSO_PROJECT_SAVINGS_MAP
	WHERE
		SSO_Project_ID = @SSO_Project_Id
		AND SSO_SAVINGS_ID = @Sso_Savings_Id

END
GO
GRANT EXECUTE ON  [dbo].[SSO_PROJECT_SAVINGS_MAP_Del] TO [CBMSApplication]
GO
