SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_CONTRACT_GET_BUDGETDETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@contract_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_account where account_id =36231,10844,11471,22160)
--select * from budget_account where account_id in (10844,10845)
--select * from vwsitename where client_id=10027
--select * from account where account_number='10443720009007083'
--select * from account where account_id in (10844,10845)
--select * from client where client_name like 'cad%'
--select * from contract where account_id in(10844)
--select * from budget_account where budget_id=655
--select * from contract where account_id =10844
--select * from contract where ed_contract_number='28532-0001'

--select * from contract where contract_id=30149 
--exec DELTOOL_CONTRACT_GET_BUDGETDETAILS_P 0,30149
--select * from SR_ALL_CONTRACT_VW
CREATE procedure dbo.DELTOOL_CONTRACT_GET_BUDGETDETAILS_P
( 
	@userId int
	,@contract_id int
)
AS
BEGIN

	select  b.budget_id
		,b.budget_name as 'BUDGET_NAME'
		,b.date_posted 
		,b.is_posted_to_dv
		,ISNULL(b.original_budget_id , 0)AS ORIGINAL_BUDGET_ID

	from 	sr_all_contract_vw scv
		join budget_account ba on ba.account_id = scv.account_id
		and scv.contract_id = @contract_id
		join budget b on b.budget_id = ba.budget_id 
        

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_CONTRACT_GET_BUDGETDETAILS_P] TO [CBMSApplication]
GO
