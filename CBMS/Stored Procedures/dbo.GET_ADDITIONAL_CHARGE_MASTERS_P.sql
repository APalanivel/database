SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.GET_ADDITIONAL_CHARGE_MASTERS_P
	 @chargeParentId INT,
	 @entityType INT,
	 @entityName VARCHAR(200),
	 @parentEntityType INT,
	 @parentEntityName VARCHAR(200)
AS

 BEGIN

    SET NOCOUNT ON

    SELECT CM.CHARGE_MASTER_ID,
		CM.CHARGE_TYPE_ID,
		TYP.ENTITY_NAME TYPE_ID,
		CM.CHARGE_DISPLAY_ID,  
		CM.CHARGE_PARENT_ID,    
		CM.CHARGE_PARENT_TYPE_ID,
		masterParentType.entity_name
    FROM dbo.charge_master cm INNER JOIN dbo.ENTITY TYP ON TYP.ENTITY_ID = CM.CHARGE_TYPE_ID
		INNER JOIN dbo.entity masterParentType ON masterParentType.ENTITY_ID = CM.CHARGE_PARENT_TYPE_ID
	WHERE typ.entity_name = @entityName
		AND typ.entity_type = @entityType
		AND cm.charge_parent_id = @chargeParentId
		AND masterParentType.entity_name = @parentEntityName
		AND masterParentType.entity_type = @parentEntityType	
	ORDER BY CAST(SUBSTRING(CHARGE_DISPLAY_ID,2,LEN(CHARGE_DISPLAY_ID)) AS INT)

END
GO
GRANT EXECUTE ON  [dbo].[GET_ADDITIONAL_CHARGE_MASTERS_P] TO [CBMSApplication]
GO
