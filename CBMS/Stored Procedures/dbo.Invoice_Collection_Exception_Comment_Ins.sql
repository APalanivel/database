SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Exception_Comment_Ins       
              
Description:              
			This sproc is used in batch process to fill Invoice Collection Queue
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	                 
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Exception_Comment_Ins 1,'ICR Created',49 
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-01-31		Created For Invoice_Collection.         
             
******/
      
CREATE PROCEDURE [dbo].[Invoice_Collection_Exception_Comment_Ins]
      ( 
       @Invoice_Collection_Queue_Id VARCHAR(MAX)
      ,@Comment_Desc NVARCHAR(4000)
      ,@User_Info_Id INT
      ,@Comment_Type_Cd INT )
AS 
BEGIN
      INSERT      INTO dbo.Invoice_Collection_Exception_Comment
                  ( 
                   Invoice_Collection_Queue_Id
                  ,Comment_Desc
                  ,Created_User_Id
                  ,Comment_Type_Cd )
                  SELECT
                        us.Segments
                       ,@Comment_Desc
                       ,@User_Info_Id
                       ,@Comment_Type_Cd
                  FROM
                        dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') us
      
END



;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Exception_Comment_Ins] TO [CBMSApplication]
GO
