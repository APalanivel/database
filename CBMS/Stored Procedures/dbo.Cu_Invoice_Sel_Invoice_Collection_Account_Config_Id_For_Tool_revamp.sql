SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
 NAME: dbo.Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id_For_Tool_revamp                      
                                  
 DESCRIPTION:                                  
   To get the details of invoice for the given account_config                   
                                  
 INPUT PARAMETERS:                    
                               
 Name                        DataType         Default       Description                  
---------------------------------------------------------------------------------------------------------------                
@Invoice_Collection_Account_Config_Id INT  NULL           
                                        
 OUTPUT PARAMETERS:                    
                                     
 Name                        DataType         Default       Description                  
---------------------------------------------------------------------------------------------------------------                
                                  
 USAGE EXAMPLES:                                      
---------------------------------------------------------------------------------------------------------------                                      
              
          
EXEC Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id_For_Tool_revamp  1371,'2018-10-23', '2018-11-19'   

EXEC Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id  1371,'2018-10-23', '2018-11-19'    

EXEC [Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id_For_Tool_revamp]  
@Invoice_Collection_Account_Config_Id   = 31731
, @Invoice_Start_dt   ='2017-09-01'
, @Invoice_End_dt ='2020-07-31' 
          
                                 
 AUTHOR INITIALS:                  
                 
 Initials              Name                  
---------------------------------------------------------------------------------------------------------------                                
 RKV                  Ravi Kumar Vegesna          
 PR					  Pradip Rajput                                  
 MODIFICATIONS:                
                    
 Initials              Date             Modification                
---------------------------------------------------------------------------------------------------------------                
    RKV    2020-03-31  Created For Invoice_Collection tool revamp.                   
    PR	   2020-06-26  Group Bill invoice once resolved to one config, its not resolving other accounts. Added condition to check 
						Invoice presence to specific account config.                                                
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id_For_Tool_revamp]
(
	@Invoice_Collection_Account_Config_Id INT
	, @Invoice_Start_dt					  DATE
	, @Invoice_End_dt					  DATE
)
AS
BEGIN
	SET NOCOUNT ON;


	SELECT
		cism.CU_INVOICE_ID
		, cism.SERVICE_MONTH
		, cism.Begin_Dt
		, cism.End_Dt
		, ci.IS_PROCESSED
	FROM
		dbo.Invoice_Collection_Account_Config icac
		INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
			ON icac.Account_Id = cism.Account_ID
		INNER JOIN dbo.CU_INVOICE ci
			ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
	WHERE
		icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
		AND ISNULL(cism.SERVICE_MONTH, '') <> ''
		AND (
				(cism.Begin_Dt BETWEEN @Invoice_Start_dt AND @Invoice_End_dt)
				OR (cism.End_Dt BETWEEN @Invoice_Start_dt AND @Invoice_End_dt)
			)
		AND NOT EXISTS (
						   SELECT
							   1
						   FROM
							   dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
							   INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmp
								   ON icqmp.Account_Invoice_Collection_Month_Id = aicmcim.Account_Invoice_Collection_Month_Id
							   INNER JOIN dbo.Account_Invoice_Collection_Month aicm
								   ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
						   WHERE
							   aicmcim.Cu_Invoice_Id = ci.CU_INVOICE_ID
							   AND aicm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
					   )
	ORDER BY
		cism.Begin_Dt
		, cism.SERVICE_MONTH;

END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id_For_Tool_revamp] TO [CBMSApplication]
GO
