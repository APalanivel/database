SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure dbo.GetIPQueueCount
	@iscurrent bit = 0
as
begin
declare @ipbatchid int

set nocount on

if @iscurrent =0
begin
	select count(*) from invoice_participation_queue where invoice_participation_batch_id is null
	and event_date>=getdate()-2
end
else
	select count(*) from invoice_participation_queue where invoice_participation_batch_id=
	(select max(invoice_participation_batch_id) from invoice_participation_batch)
	
end



GO
GRANT EXECUTE ON  [dbo].[GetIPQueueCount] TO [CBMSApplication]
GO
