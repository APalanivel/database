SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Get_All_Client_Details_KeyWord]
    (
        @Keyword VARCHAR(100) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN TRY
            --get client details of the given ClientID
            SELECT
                C.CLIENT_ID
                , C.CLIENT_NAME
            FROM
                CLIENT C
            WHERE
                (   @Keyword IS NULL
                    OR  C.CLIENT_NAME LIKE  + @Keyword + '%')
            ORDER BY
                C.CLIENT_NAME;
        END TRY
        BEGIN CATCH
            EXEC usp_RethrowError;
        END CATCH;
    END;



GO
GRANT EXECUTE ON  [dbo].[Get_All_Client_Details_KeyWord] TO [CBMSApplication]
GO
