SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******                                                            
    
 NAME:  dbo.[cbmsDataFeedCharge_del]                                                
                                                            
 DESCRIPTION:                                                            
   to mapping a new/existing ubm bucket code  to cbm bucket code  
                                                           
 AUTHOR INITIALS:                                            
                                           
 Initials              Name                                            
---------------------------------------------------------------------------------------------------------------                                                          
 PK                     Prasan kumar         
                                                             
 MODIFICATIONS:                                          
                                              
 Initials   Date     Modification                                          
------------  ----------    -----------------------------------------------------------------------------------------                                          
      
                                     
******/                  
CREATE PROCEDURE [dbo].[cbmsDataFeedCharge_del]             
 (               
 @umb_id INT   ,           
 @commodity_id INT ,      
 @ubm_bucket_code VARCHAR(255),    
 @user_id INT       
 )                
AS                
BEGIN                
   
 
  
  BEGIN TRY    
       BEGIN TRAN  
			
			DECLARE @Ubm_Bucket_Charge_Map_Id INT


			SELECT @Ubm_Bucket_Charge_Map_Id = UBM_BUCKET_CHARGE_MAP_ID 
			FROM UBM_BUCKET_CHARGE_MAP 
			WHERE UBM_ID = @umb_id  
					and COMMODITY_TYPE_ID = @commodity_id  
					and ubm_bucket_code = @ubm_bucket_code 
			
			DELETE  FROM  
            dbo.Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map  
            WHERE  
                Ubm_Bucket_Charge_Map_Id = @Ubm_Bucket_Charge_Map_Id; 

			DELETE  FROM  
            dbo.UBM_BUCKET_CHARGE_MAP  
            WHERE  
                UBM_BUCKET_CHARGE_MAP_ID = @Ubm_Bucket_Charge_Map_Id; 
  
		COMMIT TRAN  
        END TRY    
        BEGIN CATCH    
            ROLLBACK TRAN;   
  
      
			INSERT INTO [dbo].[StoredProc_Error_Log]            
                 (            
                     [StoredProc_Name]            
                     , [Error_Line]            
                     , [Error_message]            
                     , [Input_Params]            
                 )            
            VALUES           
                ('[cbmsDataFeedCharge_del]' 
                 ,ERROR_NUMBER()          
                 ,ERROR_MESSAGE()             
                 ,'ubm id: ' + @umb_id +' UBM_BUCKET_CODE '+@UBM_BUCKET_CODE +' commodity_id '  
					+@commodity_id+' userid ' + @user_id);            
    
            EXEC dbo.usp_RethrowError;    
        END CATCH;   
END 
GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedCharge_del] TO [CBMSApplication]
GO
