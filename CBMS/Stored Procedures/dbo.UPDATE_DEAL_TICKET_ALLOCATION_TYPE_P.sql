SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_ALLOCATION_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(16)	          	
	@allocationTypeId	int       	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.UPDATE_DEAL_TICKET_ALLOCATION_TYPE_P 

@userId varchar(10),
@sessionId varchar(16),
@allocationTypeId int, 
@dealTicketId int

AS
set nocount on
UPDATE RM_DEAL_TICKET SET ALLOCATION_TYPE_ID = @allocationTypeId
WHERE RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_ALLOCATION_TYPE_P] TO [CBMSApplication]
GO
