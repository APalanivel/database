
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_UPDATE_SEND_SUPPLIER_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                    DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id				VARCHAR(10)
    @session_id				VARCHAR(20)
    @account_group_id		INT
    @is_bid_group			BIT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.SR_RFP_GET_POSTED_SUPPLIER_CONTACT_P 1, 1, 12105078, 0
	EXEC dbo.SR_RFP_GET_POSTED_SUPPLIER_CONTACT_P 1, 1, 10012036, 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values
******/
CREATE  PROCEDURE [dbo].[SR_RFP_GET_POSTED_SUPPLIER_CONTACT_P]
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
     ,@account_group_id INT
     ,@is_bid_group BIT
AS 
BEGIN 

      SET nocount ON
      SELECT
            map.SR_SUPPLIER_CONTACT_INFO_ID
           ,map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
      FROM
            dbo.SR_RFP_SEND_SUPPLIER supp
            INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP map
                  ON supp.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
      WHERE
            supp.SR_ACCOUNT_GROUP_ID = @account_group_id
            AND supp.IS_BID_GROUP = @is_bid_group
            AND supp.STATUS_TYPE_ID = ( SELECT
                                          entity_id
                                        FROM
                                          entity
                                        WHERE
                                          entity_name = 'Post'
                                          AND entity_type = 1013 )
            
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_POSTED_SUPPLIER_CONTACT_P] TO [CBMSApplication]
GO
