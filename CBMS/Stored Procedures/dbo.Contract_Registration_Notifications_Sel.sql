
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Contract_Registration_Notifications_Sel 

DESCRIPTION: 
				To get the termination notifications sent

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Date						DATETIME
@Start_Index				INT
@End_Index					INT
@Total_Count				INT


OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   

EXEC dbo.Contract_Registration_Notifications_Sel NULL,'2016-06-10'
EXEC dbo.Contract_Registration_Notifications_Sel NULL,'2016-06-11'
EXEC dbo.Contract_Registration_Notifications_Sel NULL,'2016-06-12'
EXEC dbo.Contract_Registration_Notifications_Sel 21047,'2016-06-12'
EXEC dbo.Contract_Registration_Notifications_Sel 42670,'2016-06-12'
EXEC dbo.Contract_Registration_Notifications_Sel 49,'2016-06-12'
EXEC dbo.Contract_Registration_Notifications_Sel NULL,'2016-06-15'

AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	RR			Narayana Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	RR			2016-06-03  Created  GCS-988 GCS-Phase-5
	RR			2016-07-22	GCS-1179 - Modified to get supplier contact mapped to commodity, state and country  of the expiring contract 

	 	 		 
******/
CREATE PROCEDURE [dbo].[Contract_Registration_Notifications_Sel]
      ( 
       @Analyst_Id INT = NULL
      ,@Date DATETIME = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Count INT = 0 )
AS 
BEGIN 

      SET NOCOUNT ON;

      DECLARE @Tbl_All_Msg TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT
            ,Meter_Id INT );
            
      DECLARE @Tbl_Latest_Msg TABLE
            ( 
             Contract_Id INT
            ,Meter_Start_End_Date DATE
            ,Notification_Msg_Queue_Id INT );
            
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT;
           
      DECLARE @Tbl_Contracts TABLE
            ( 
             Contract_Id INT
            ,ED_CONTRACT_NUMBER VARCHAR(150)
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Supplier_Meter_Association_Date DATE
            ,Account_Vendor_Name VARCHAR(200)
            ,Account_Vendor_Id INT
            ,Notice_Date DATE
            ,Notice_Sent DATETIME
            ,Row_Num INT
            ,Notification_Msg_Queue_Id INT
            ,Meter_Start_End_Date DATE );
            
      DECLARE @Tbl_Sent_To_Supplier TABLE
            ( 
             Notification_Msg_Queue_Id INT );
             
             
        
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type';
            
      --SELECT
      --      @Date = isnull(@Date, convert(VARCHAR(10), getdate(), 101))
            
      INSERT      INTO @Tbl_All_Msg
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id
                  ,Meter_Id )
                  SELECT
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt AS Meter_Start_End_Date
                       ,fvl.Notification_Msg_Queue_Id
                       ,fvmd.Meter_Id
                  FROM
                        dbo.Contract_Notification_Log fvl
                        INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                              ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                        INNER JOIN dbo.Notification_Msg_Queue nmq
                              ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                        INNER JOIN dbo.Notification_Template nt
                              ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = nt.Notification_Type_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cd.Codeset_Id
                  WHERE
                        cd.Code_Value = 'Registration Notification'
                        AND cs.Codeset_Name = 'Notification Type'
                        AND ( ( @Date IS NOT NULL
                                AND CONVERT(DATE, fvl.Last_Change_Ts, 101) BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD, -90, @Date), 101))
                                                                           AND     @Date )
                              OR ( @Date IS NULL
                                   AND CONVERT(DATE, fvl.Last_Change_Ts, 101) < CONVERT(DATETIME, CONVERT(VARCHAR(10), DATEADD(DD, -90, GETDATE()), 101)) ) )
                  GROUP BY
                        fvl.Contract_Id
                       ,fvl.Meter_Term_Dt
                       ,fvl.Notification_Msg_Queue_Id
                       ,fvmd.Meter_Id;
                       
      INSERT      INTO @Tbl_Latest_Msg
                  ( 
                   Contract_Id
                  ,Meter_Start_End_Date
                  ,Notification_Msg_Queue_Id )
                  SELECT
                        tm.Contract_Id
                       ,tm.Meter_Start_End_Date
                       ,MAX(tm.Notification_Msg_Queue_Id)
                  FROM
                        @Tbl_All_Msg tm
                  GROUP BY
                        tm.Contract_Id
                       ,tm.Meter_Start_End_Date;
      
      INSERT      INTO @Tbl_Contracts
                  ( 
                   Contract_Id
                  ,ED_CONTRACT_NUMBER
                  ,Meter_Id
                  ,Meter_Number
                  ,Supplier_Meter_Association_Date
                  ,Account_Vendor_Name
                  ,Account_Vendor_Id
                  ,Notice_Date
                  ,Notice_Sent
                  ,Row_Num
                  ,Notification_Msg_Queue_Id
                  ,Meter_Start_End_Date )
                  SELECT
                        con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,chasupp.Supplier_Meter_Association_Date
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Account_Vendor_Id
                       ,DATEADD(DAY, -30, chasupp.Supplier_Meter_Association_Date) AS Notice_Date
                       ,fvl.Last_Change_Ts AS Notice_Sent
                       ,DENSE_RANK() OVER ( ORDER BY fvl.Last_Change_Ts DESC ) AS Row_Num
                       ,tt.Notification_Msg_Queue_Id
                       ,tt.Meter_Start_End_Date
                  FROM
                        @Tbl_Latest_Msg tt
                        INNER JOIN dbo.Contract_Notification_Log fvl
                              ON tt.Contract_Id = fvl.Contract_Id
                                 AND tt.Notification_Msg_Queue_Id = fvl.Notification_Msg_Queue_Id
                        INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                              ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON fvmd.Meter_Id = chasupp.Meter_Id
                                 AND fvl.Contract_Id = chasupp.Supplier_Contract_ID
                                 AND fvl.Meter_Term_Dt = chasupp.Supplier_Meter_Association_Date
                        INNER JOIN dbo.Notification_Msg_Queue nmq
                              ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                        INNER JOIN dbo.Notification_Template nt
                              ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = nt.Notification_Type_Cd
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cd.Codeset_Id
                        INNER JOIN dbo.CONTRACT con
                              ON fvl.Contract_Id = con.CONTRACT_ID
                        INNER JOIN Core.Client_Hier ch
                              ON chasupp.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                                 AND chautil.Meter_Id = chasupp.Meter_Id
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON chautil.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND chautil.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        INNER JOIN Core.Client_Commodity ccc
                              ON ccc.Client_Id = ch.Client_Id
                                 AND ccc.Commodity_Id = chasupp.Commodity_Id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = chautil.Account_Id
                                 AND aca.Commodity_Id = chautil.Commodity_Id
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = chasupp.Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                  WHERE
                        cd.Code_Value = 'Registration Notification'
                        AND cs.Codeset_Name = 'Notification Type'
                        AND chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        AND ( @Analyst_Id IS NULL
                              OR @Analyst_Id = CASE WHEN COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Default_Analyst THEN vcam.Analyst_Id
                                                    WHEN COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                               END )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_All_Msg mtrs
                                     WHERE
                                          mtrs.Meter_Id = chasupp.Meter_Id
                                          AND mtrs.Meter_Start_End_Date = chasupp.Supplier_Meter_Association_Date
                                          AND mtrs.Contract_Id = chasupp.Supplier_Contract_ID )
                                               
      SELECT
            @Total_Count = MAX(Row_Num)
      FROM
            @Tbl_Contracts;
            
            
      INSERT      INTO @Tbl_Sent_To_Supplier
                  ( 
                   Notification_Msg_Queue_Id )
                  SELECT
                        tc.Notification_Msg_Queue_Id
                  FROM
                        @Tbl_Contracts tc
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.Notification_Msg_Dtl nmd
                                 WHERE
                                    nmd.Notification_Msg_Queue_Id = tc.Notification_Msg_Queue_Id );
           
      
      SELECT
            cc.ED_CONTRACT_NUMBER
           ,cc.Contract_Id
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) AS Meter_Number
           ,cc.Supplier_Meter_Association_Date AS Contract_Start_Date
           ,cc.Account_Vendor_Name
           ,cc.Account_Vendor_Id
           ,LEFT(regmail.regmails, LEN(regmail.regmails) - 1) AS Registration_Contact_Email_Address
           ,cc.Notice_Date
           ,cc.Notice_Sent
           ,@Total_Count AS Total_Rows
           ,cc.Row_Num
           ,cc.Notification_Msg_Queue_Id
           ,cc.Meter_Start_End_Date
           ,CASE WHEN nssupp.Notification_Msg_Queue_Id IS NOT NULL THEN 0
                 WHEN nssupp.Notification_Msg_Queue_Id IS NULL THEN 1
            END AS Not_Sent_To_Supplier
      FROM
            @Tbl_Contracts cc
            CROSS APPLY ( SELECT
                              ct.Meter_Number + ','
                          FROM
                              @Tbl_Contracts ct
                          WHERE
                              cc.Contract_Id = ct.Contract_Id
                              AND cc.Notification_Msg_Queue_Id = ct.Notification_Msg_Queue_Id
                              AND ct.Meter_Number IS NOT NULL
                          GROUP BY
                              ct.Meter_Number
                             ,ct.Meter_Id
            FOR
                          XML PATH('') ) mtrnum ( mtrnums )
            CROSS APPLY ( SELECT
                              ssci.Registration_Contact_Email_Address + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_All_Msg mtr
                                    ON cha.Meter_Id = mtr.Meter_Id
                                       AND cha.Supplier_Contract_ID = mtr.Contract_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cc.Account_Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND metermap.IS_PRIMARY = 0
                              AND mtr.Contract_Id = cc.Contract_Id
                          GROUP BY
                              ssci.Registration_Contact_Email_Address
            FOR
                          XML PATH('') ) regmail ( regmails )
            LEFT JOIN @Tbl_Sent_To_Supplier nssupp
                  ON nssupp.Notification_Msg_Queue_Id = cc.Notification_Msg_Queue_Id
      WHERE
            cc.Row_Num BETWEEN @Start_Index AND @End_Index
      GROUP BY
            cc.ED_CONTRACT_NUMBER
           ,cc.Contract_Id
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1)
           ,cc.Supplier_Meter_Association_Date
           ,cc.Account_Vendor_Name
           ,cc.Account_Vendor_Id
           ,LEFT(regmail.regmails, LEN(regmail.regmails) - 1)
           ,cc.Notice_Date
           ,cc.Notice_Sent
           ,cc.Row_Num
           ,cc.Notification_Msg_Queue_Id
           ,cc.Meter_Start_End_Date
           ,CASE WHEN nssupp.Notification_Msg_Queue_Id IS NOT NULL THEN 0
                 WHEN nssupp.Notification_Msg_Queue_Id IS NULL THEN 1
            END
      ORDER BY
            cc.Row_Num;        
   
END;

;
GO

GRANT EXECUTE ON  [dbo].[Contract_Registration_Notifications_Sel] TO [CBMSApplication]
GO
