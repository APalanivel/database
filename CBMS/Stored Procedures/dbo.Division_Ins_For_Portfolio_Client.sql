SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************

NAME : dbo.Division_Ins_For_Portfolio_Client   

     

DESCRIPTION: This procedure used to aggregate  

     

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      

--------------------------------------------------------------------        

 @Message		XML

      

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      

--------------------------------------------------------------------      

  

  

  USAGE EXAMPLES:      

--------------------------------------------------------------------      

	

	SP Should be executed from Service Broker 

	

	DECLARE @Message XML =  '<Mapping_Client_To_Portfolio>

								<Client_Id>108</Client_Id>

								<Portfolio_Client_Id>11738</Portfolio_Client_Id >

								</Mapping_Client_To_Portfolio>

							'



	EXEC Division_Ins_For_Portfolio_Client @Message



	

    

AUTHOR INITIALS:      

 Initials		Name      

-------------------------------------------------------------------       

 DSC			Kaushik

    

 MODIFICATIONS  

 Initials		Date			Modification  

--------------------------------------------------------------------  
 DSC			06/23/2014		Created 	

*****************************************************************************************************/
CREATE PROCEDURE dbo.Division_Ins_For_Portfolio_Client
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN



      SET NOCOUNT ON 


	 
      DECLARE
            @Client_Id INT
           ,@Portfolio_Client_Id INT = NULL
           ,@Client_Name VARCHAR(255)
           ,@User_Info_Id INT
           ,@audit_type INT = 1 -- Add operation
           ,@entity_name VARCHAR(MAX) = 'DIVISION_TABLE'
           ,@entity_type INT = 500
           ,@Site_Message XML
           ,@Id INT = 1
           ,@Sitegroup_Id INT
           ,@Savings_Category_Type_Id INT
           ,@Portfolio_Client_Hier_Reference_Number INT



      SELECT
            @User_Info_Id = ui.User_Info_Id
      FROM
            dbo.User_Info AS ui
      WHERE
            ui.username = 'conversion'



      SELECT
            @Client_Id = cng.ch.value('Client_Id[1]', 'INT')
           ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
      FROM
            @Message.nodes('/Mapping_Client_To_Portfolio/Client') cng ( ch ) 



      SELECT
            @Portfolio_Client_Hier_Reference_Number = Client_Hier_Id
           ,@Client_Name = Client_Name
      FROM
            core.Client_Hier
      WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0





      DECLARE @Savings_Category_Type_Table TABLE
            (
             Id INT IDENTITY(1, 1)
            ,Savings_Category_Type_Id INT )  



      DECLARE @Sites_For_New_Divison TABLE
            (
             Id INT IDENTITY(1, 1)
            ,Site_Id INT )



      DECLARE @Portfolio_Client_Ids TABLE
            (
             Portfolio_Client_Id INT )



      INSERT      INTO @Portfolio_Client_Ids
                  (Portfolio_Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.App_Config AS ac
                        CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                  WHERE
                        ac.App_Config_Cd = 'Portfolio_ClientHier_Management'



-- Mapping Client to Portfolio



      IF EXISTS ( SELECT
                        1
                  FROM
                        @Portfolio_Client_Ids
                  WHERE
                        Portfolio_Client_Id = @Portfolio_Client_Id )
            BEGIN





                  IF NOT EXISTS ( SELECT
                                    1
                                  FROM
                                    dbo.Sitegroup AS s
                                  WHERE
                                    s.Client_Id = @Portfolio_Client_Id
                                    AND s.Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number )
                        BEGIN



                              DECLARE
                                    @sbaTypeId INT
                                   ,@priceIndexId INT
                                   ,@clientId INT
                                   ,@termPreferredTypeId INT
                                   ,@contractReviewerTypeid INT
                                   ,@decisionMakerTypeid INT
                                   ,@signatoryTypeId INT
                                   ,@divisionName VARCHAR(200)
                                   ,@isInterestMinoritySuppliers INT
                                   ,@isCorporateHedge INT
                                   ,@naicsCode VARCHAR(30)
                                   ,@taxNumber VARCHAR(30)
                                   ,@dunsNumber VARCHAR(30)
                                   ,@isCorporateDivision INT
                                   ,@triggerRights INT
                                   ,@miscComments VARCHAR(4000)
                                   ,@notManaged INT
                                   ,@contractingEntity VARCHAR(200)
                                   ,@clientLegalStructure VARCHAR(4000)
                                   ,@divisionId INT  



                              SELECT
                                    @sbaTypeId = sba_Type_Id
                                   ,@priceIndexId = price_Index_Id
                                   ,@clientId = @Portfolio_Client_Id
                                   ,@termPreferredTypeId = term_Preferred_Type_Id
                                   ,@contractReviewerTypeid = contract_Reviewer_Type_id
                                   ,@decisionMakerTypeid = decision_Maker_Type_id
                                   ,@signatoryTypeId = signatory_Type_Id
                                   ,@divisionName = @Client_Name
                                   ,@isInterestMinoritySuppliers = is_Interest_Minority_Suppliers
                                   ,@isCorporateHedge = is_Corporate_Hedge
                                   ,@naicsCode = naics_Code
                                   ,@taxNumber = tax_Number
                                   ,@dunsNumber = duns_Number
                                   ,@isCorporateDivision = is_Corporate_Division
                                   ,@triggerRights = trigger_Rights
                                   ,@miscComments = misc_Comments
                                   ,@notManaged = not_Managed
                                   ,@contractingEntity = contracting_Entity
                                   ,@clientLegalStructure = client_Legal_Structure
                                   ,@Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number
                              FROM
                                    dbo.Sitegroup AS s
                                    INNER JOIN dbo.Division_Dtl AS dd
                                          ON s.Sitegroup_Id = dd.SiteGroup_Id
                              WHERE
                                    s.client_id = @Portfolio_Client_Id
                                    AND dd.IS_CORPORATE_DIVISION = 1



                              EXEC CBMS_ADD_DIVISION_P
                                    @sbaTypeId
                                   ,@priceIndexId
                                   ,@clientId
                                   ,@termPreferredTypeId
                                   ,@contractReviewerTypeid
                                   ,@decisionMakerTypeid
                                   ,@signatoryTypeId
                                   ,@divisionName
                                   ,@isInterestMinoritySuppliers
                                   ,@isCorporateHedge
                                   ,@naicsCode
                                   ,@taxNumber
                                   ,@dunsNumber
                                   ,@isCorporateDivision
                                   ,@triggerRights
                                   ,@miscComments
                                   ,@notManaged
                                   ,@contractingEntity
                                   ,@clientLegalStructure
                                   ,@divisionId OUTPUT
                                   ,@Portfolio_Client_Hier_Reference_Number



                              INSERT      INTO @Savings_Category_Type_Table
                                          (Savings_Category_Type_Id )
                                          SELECT
                                                Savings_Category_Type_Id
                                          FROM
                                                SAVINGS_CATEGORY_DIVISION_MAP
                                          WHERE
                                                DIVISION_ID = @divisionId



                              WHILE EXISTS ( SELECT
                                                1
                                             FROM
                                                @Savings_Category_Type_Table )
                                    BEGIN 



                                          SELECT TOP 1
                                                @Savings_Category_Type_Id = Savings_Category_Type_Id
                                          FROM
                                                @Savings_Category_Type_Table

                                                

                                          EXEC ADD_SAVINGS_DIVISION_MAP_P
                                                @Savings_Category_Type_Id
                                               ,@divisionId

			

                                          DELETE
                                                @Savings_Category_Type_Table
                                          WHERE
                                                Savings_Category_Type_Id = @Savings_Category_Type_Id





                                    END



                              EXEC ADD_ENTITY_AUDIT_ITEM_P
                                    @divisionId
                                   ,@user_info_id
                                   ,@audit_type
                                   ,@entity_name
                                   ,@entity_type 





                              EXEC Add_Div_To_Corp_Role
                                    @divisionId





                              INSERT      INTO @Sites_For_New_Divison
                                          (Site_Id )
                                          SELECT
                                                Site_Id
                                          FROM
                                                core.Client_Hier ch
                                          WHERE
                                                Client_Id = @client_id
                                                AND site_id > 0



                              WHILE EXISTS ( SELECT
                                                1
                                             FROM
                                                @Sites_For_New_Divison )
                                    BEGIN

                              

                                          SET @Site_Message = ( SELECT
                                                                  Site_Id
                                                                 ,@Client_Id AS Client_Id
                                                                 ,'I' AS Op_Code
                                                                FROM
                                                                  @Sites_For_New_Divison
                                                                WHERE
                                                                  id = @id
                                                FOR
                                                                XML PATH('Site')
                                                                   ,ELEMENTS
                                                                   ,ROOT('Site_Info') )


                                          DECLARE @CH UNIQUEIDENTIFIER;    



                                          BEGIN DIALOG CONVERSATION @CH  

											FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]

											TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'

											ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    



                                          SEND ON CONVERSATION @CH

                                          MESSAGE TYPE [//Change_Control/Message/Mapping_Site_To_Portfolio] (@Site_Message) 

                                          

                                          DELETE
                                                @Sites_For_New_Divison
                                          WHERE
                                                id = @id											

											

                                          SET @id = @id + 1

                                    END





                        END







            END;

            



END























;
GO
GRANT EXECUTE ON  [dbo].[Division_Ins_For_Portfolio_Client] TO [CBMSApplication]
GO
