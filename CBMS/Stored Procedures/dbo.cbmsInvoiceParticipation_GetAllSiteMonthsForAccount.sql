SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForAccount]
	( @MyAccountId int
	, @account_id int
	)
AS
BEGIN


   select distinct ip.site_id
	, ip.service_month
     from invoice_participation ip
    where ip.account_id = @account_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForAccount] TO [CBMSApplication]
GO
