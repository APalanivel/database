SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******    
NAME:    
 dbo.Cu_Invoice_Standing_Data_Exception_Status_Upd  
    
DESCRIPTION:    
    
INPUT PARAMETERS:    
 Name            DataType   Default     Description    
------------         -----------   --------------   ------------------------------------------------------    
 @Cu_Invoice_Standing_Data_Exception_Id   INT    
 @User_Info_Id         INT   
 @Exception_Status_Cd          INT   
     
OUTPUT PARAMETERS:    
 Name          DataType  Default  Description    
-------------------------------------------------------------------------------------------    
     
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------------    
        
BEGIN TRAN  
EXEC Cu_Invoice_Standing_Data_Exception_Status_Upd   1,49,103015     
ROLLBACK TRAN   
   
    
    
AUTHOR INITIALS:    
 Initials    Name    
--------------   -----------------------------------------------------------------------------    
 HKT        Harish Kumar Tirumandyam  
   
     
MODIFICATIONS    
 Initials   Date    Modification    
-------------  ---------------  ---------------------------------------------------------------    
 HKT    2020-04-21           Created for Data Purple   
 HKT    2020-05-27            Added Detail Table Logic 
   
    
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Standing_Data_Exception_Status_Upd]
    (
        
        @Cu_Invoice_Id INT
        , @Exception_Type_Cd INT
        , @User_Info_Id INT
        , @Account_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Exception_Status_Cd INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Closed_Status_cd INT = 0;

        SELECT
            @Is_Closed_Status_cd = 1
        FROM
            dbo.Code cd
        WHERE
            cd.Code_Value = 'Closed'
            AND cd.Code_Id = @Exception_Status_Cd;


        BEGIN TRY
            BEGIN TRAN;

            UPDATE
                cisded
            SET
                cisded.Status_Cd = @Exception_Status_Cd
                , cisded.Updated_User_Id = @User_Info_Id
                , cisded.Last_Change_Ts = GETDATE()
            FROM
                dbo.Cu_Invoice_Standing_Data_Exception_Dtl cisded
				INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception cisde
				ON cisde.Cu_Invoice_Standing_Data_Exception_Id = cisded.Cu_Invoice_Standing_Data_Exception_Id
            WHERE
                cisde.Cu_Invoice_Id = @Cu_Invoice_Id
				AND cisde.Exception_Type_Cd = @Exception_Type_Cd
                AND (   @Account_Id IS NULL
                        OR  cisded.Account_Id = @Account_Id)
                AND (   @Commodity_Id IS NULL
                        OR  cisded.Commodity_Id = @Commodity_Id);


            UPDATE
                cisde
            SET
                cisde.Exception_Status_Cd = @Exception_Status_Cd
                , cisde.Updated_User_Id = @User_Info_Id
                , cisde.Closed_By_User_Id = CASE WHEN @Is_Closed_Status_cd = 1 THEN @User_Info_Id
                                                ELSE cisde.Closed_By_User_Id
                                            END
                , cisde.Closed_Ts = CASE WHEN @Is_Closed_Status_cd = 1 THEN GETDATE()
                                        ELSE cisde.Closed_Ts
                                    END
                , cisde.Last_Change_Ts = GETDATE()
            FROM
                dbo.Cu_Invoice_Standing_Data_Exception cisde
            WHERE
                cisde.Cu_Invoice_Id = @Cu_Invoice_Id
				AND cisde.Exception_Type_Cd = @Exception_Type_Cd
                AND NOT EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Cu_Invoice_Standing_Data_Exception_Dtl cisded
                                   WHERE
                                        cisded.Cu_Invoice_Standing_Data_Exception_Id = cisde.Cu_Invoice_Standing_Data_Exception_Id
                                        AND cisded.Status_Cd <> @Exception_Status_Cd);

            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;






    END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Standing_Data_Exception_Status_Upd] TO [CBMSApplication]
GO
