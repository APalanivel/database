
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******        
                           
 NAME:  [dbo].[cbmsRmMarketGroup_GetAll]                        
                            
 DESCRIPTION:        
				TO get the List of market group outlooks .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
----------------------------------------------------------------------------------    
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
----------------------------------------------------------------------------------   
                            
 USAGE EXAMPLES:                                
----------------------------------------------------------------------------------   
       
 Exec  dbo.cbmsRmMarketGroup_GetAll 49 
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
----------------------------------------------------------------------------------   
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
----------------------------------------------------------------------------------   
 NR                     2017-07-13      MAINT-5012 - Added Header and  RM_MARKET_GROUP column 
										added in Order By clause.                        
                           
******/  

CREATE PROCEDURE [dbo].[cbmsRmMarketGroup_GetAll] ( @AccountId INT )
AS 
BEGIN


      SELECT
            [RM_MARKET_GROUP_ID]
           ,[RM_MARKET_GROUP]
      FROM
            [dbo].[RM_MARKET_GROUP]
      ORDER BY
            RM_MARKET_GROUP

END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsRmMarketGroup_GetAll] TO [CBMSApplication]
GO
