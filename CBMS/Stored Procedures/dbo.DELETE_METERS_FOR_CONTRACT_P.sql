SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******        
NAME:        
 cbms_prod.dbo.DELETE_METERS_FOR_CONTRACT_P         
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name     DataType  Default Description        
------------------------------------------------------------        
@contract_id	INT
@meter_id		VARCHAR(MAX) 
        
OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
BEGIN TRAN
	exec DELETE_METERS_FOR_CONTRACT_P 12048,'183,321,953'
ROLLBACK TRAN
select * from SUPPLIER_ACCOUNT_METER_MAP where Contract_ID = 12048
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
SKA		Shobhit Kumar Agrawal

MODIFICATIONS                
 Initials  Date   Modification        
------------------------------------------------------------        
SKA		06/03/2009  Created    
SKA		06/08/2010	Added the usage example    
SKA	    06/09/2010	Used cast feature for Segments column of ufn_Split as comparing with integer value

 
******/     

CREATE PROCEDURE [dbo].[DELETE_METERS_FOR_CONTRACT_P]
      @contract_id INT
     ,@meter_id VARCHAR(MAX)
AS 
BEGIN
      SET NOCOUNT ON ;    
	
      DELETE
            samm
      FROM
            SUPPLIER_ACCOUNT_METER_MAP samm
            INNER JOIN ufn_Split(@meter_id, ',') segm ON CAST(segm.Segments AS INT) = samm.METER_ID
      WHERE
            samm.Contract_ID = @contract_id
            		
END


GO
GRANT EXECUTE ON  [dbo].[DELETE_METERS_FOR_CONTRACT_P] TO [CBMSApplication]
GO
