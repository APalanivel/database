SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsAccount_GetUtilityforSite

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsAccount_GetUtilityforSite]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN
	
  	 select a.account_id 
	    	, a.account_number
		, v.vendor_name
		, a.account_type_id
		from account a
		join vendor v on v.vendor_id = a.vendor_id
		where site_id = @site_id 
		and a.account_type_id = 38

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_GetUtilityforSite] TO [CBMSApplication]
GO
