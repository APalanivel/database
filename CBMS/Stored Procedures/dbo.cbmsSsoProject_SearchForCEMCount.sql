SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
	dbo.cbmsSsoProject_SearchForCEMCount

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@commodity_type_id	int       	null      	
	@project_status_type_id	int       	null      	
	@project_ownership_type_id	int       	null      	
	@client_id     	int       	null      	
	@division_id   	int       	null      	
	@site_id       	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.cbmsSsoProject_SearchForCEMCount 10
EXEC dbo.cbmsSsoProject_SearchForCEMCount @MyAccountId = 10, @Client_Id = 1024

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Eshwar Panduga
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
							Removed LEFT JOIN on vwSsoProject_LastActivity and COM Entity.
******/

CREATE PROCEDURE dbo.cbmsSsoProject_SearchForCEMCount
( 
 @MyAccountId INT
,@commodity_type_id INT = NULL
,@project_status_type_id INT = NULL
,@project_ownership_type_id INT = NULL
,@client_id INT = NULL
,@division_id INT = NULL
,@site_id INT = NULL )
AS 
BEGIN
      SET nocount ON

      EXEC cbmsSecurity_GetClientAccess @MyAccountId, @client_id OUTPUT, @division_id OUTPUT, @site_id OUTPUT

      SELECT
            @Project_Status_Type_Id = ISNULL(@Project_Status_Type_Id, ENTITY_ID)
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Project Status Type'
            AND ENTITY_NAME = 'Open'
            
      SELECT
            COUNT(1)
      FROM
            dbo.SSO_PROJECT SP
            JOIN dbo.ENTITY PST
                  ON PST.ENTITY_ID = SP.PROJECT_STATUS_TYPE_ID
            JOIN dbo.ENTITY POT
                  ON POT.ENTITY_ID = SP.PROJECT_OWNERSHIP_TYPE_ID
            JOIN ( SELECT DISTINCT
                        SSO_PROJECT_ID
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Id
                          WHEN 'Division' THEN CH.Sitegroup_Id
                          WHEN 'Site' THEN CH.Site_Id
                        END AS owner_id
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Name
                          WHEN 'Division' THEN CH.Sitegroup_Name
                          WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
                        END AS owner_name
                       ,CH.Client_Id
                   FROM
                        dbo.SSO_PROJECT_OWNER_MAP SPOM
                        JOIN Core.Client_Hier CH
                              ON SPOM.Client_Hier_Id = CH.Client_Hier_Id
                        JOIN dbo.Code Cd
                              ON CH.Hier_level_Cd = Cd.Code_Id
                   WHERE
                        ( @client_id IS NULL
                          OR @client_id = CH.Client_Id )
                        AND ( @division_id IS NULL
                              OR @division_id = CH.Sitegroup_Id )
                        AND ( @site_id IS NULL
                              OR @Site_Id = CH.Site_Id )
                        AND CH.Client_Not_Managed = 0 ) acc
                  ON acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN dbo.CLIENT_CEM_MAP CCM
                  ON ( CCM.CLIENT_ID = acc.Client_Id
                       AND CCM.USER_INFO_ID = @MyAccountId )
      WHERE
            ( @commodity_type_id IS NULL
              OR @commodity_type_id = SP.COMMODITY_TYPE_ID )
            AND SP.PROJECT_STATUS_TYPE_ID = @project_status_type_id
            AND ( @project_ownership_type_id IS NULL
                  OR @project_ownership_type_id = SP.PROJECT_OWNERSHIP_TYPE_ID )

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_SearchForCEMCount] TO [CBMSApplication]
GO
