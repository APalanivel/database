SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Delete_Contract_Recalc_Config_Copy_To_Account_Level

DESCRIPTION:

	To Delete Contract History Associated with given Contract Id.

INPUT PARAMETERS:
	NAME					DATATYPE			DEFAULT					DESCRIPTION     
-------------------------------------------------------------------------------------
	@Contract_Id			INT				
		
                
OUTPUT PARAMETERS:          
	NAME					DATATYPE			DEFAULT					DESCRIPTION     
-------------------------------------------------------------------------------------

USAGE EXAMPLES:          
------------------------------------------------------------

----> Supplier Account 

	BEGIN TRAN

	SELECT * FROM dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =156755 
    SELECT * FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt WHERE acirt.Account_Id = 1476602 
	SELECT COUNT(1) FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt

	EXEC dbo.Delete_Contract_Recalc_Config_Copy_To_Account_Level
     
         @Contract_Id = 156755
         , @User_Info_Id = 49
		
	SELECT * FROM dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =156755 
    SELECT * FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt WHERE acirt.Account_Id = 1476602 
	SELECT COUNT(1) FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt

	ROLLBACK TRAN
	

----> Consolidated  Account 

BEGIN TRAN

SELECT * FROM dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =155526 
SELECT * FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt WHERE acirt.Account_Id = 1460058 
SELECT COUNT(1) FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt

	EXEC dbo.Delete_Contract_Recalc_Config_Copy_To_Account_Level
     
         @Contract_Id = 155526
         , @User_Info_Id = 49
		
SELECT * FROM dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =155526 
SELECT * FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt WHERE acirt.Account_Id = 1460058 
SELECT COUNT(1) FROM dbo.Account_Commodity_Invoice_Recalc_Type acirt

ROLLBACK TRAN


	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	NR			Narayana Reddy
	

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	NR			2019-12-16	MAINT-9670 - Copy the account level When contract deleted .

*/

CREATE PROC [dbo].[Delete_Contract_Recalc_Config_Copy_To_Account_Level]
    (
        @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Counter INT = 1
            , @Total_Count INT
            , @Account_Id INT;



        CREATE TABLE #Supplier_Account
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );
        CREATE TABLE #Consolidated_Account
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Id INT
             );


        DECLARE
            @Source_cd INT
            , @Commodity_Id INT
            , @Contract_Start_Dt DATE
            , @Contract_End_Dt DATE;

        SELECT
            @Commodity_Id = c.COMMODITY_TYPE_ID
            , @Contract_Start_Dt = c.CONTRACT_START_DATE
            , @Contract_End_Dt = c.CONTRACT_END_DATE
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;

        SELECT
            @Source_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Valcon Source Type'
            AND c.Code_Value = 'Contract';



        INSERT INTO #Supplier_Account
             (
                 Account_Id
             )
        SELECT
            cha.Account_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Number <> 'Not Yet Assigned'
            AND cha.Commodity_Id = @Commodity_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Commodity_Invoice_Recalc_Type acirt
                           WHERE
                                acirt.Account_Id = cha.Account_Id
                                AND acirt.Commodity_ID = @Commodity_Id
                                AND acirt.Source_Cd = @Source_cd
                                AND (   @Contract_Start_Dt BETWEEN acirt.Start_Dt
                                                           AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  @Contract_End_Dt BETWEEN acirt.Start_Dt
                                                             AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  acirt.Start_Dt BETWEEN @Contract_Start_Dt
                                                           AND     @Contract_End_Dt
                                        OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Contract_Start_Dt
                                                                               AND     @Contract_End_Dt))
        GROUP BY
            cha.Account_Id;



        INSERT INTO #Consolidated_Account
             (
                 Account_Id
             )
        SELECT
            utility_cha.Account_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
                   AND  Supplier_Cha.Client_Hier_Id = utility_cha.Client_Hier_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Commodity_Id = @Commodity_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                          AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                            AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                              AND     c.CONTRACT_END_DATE
                    OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                  AND     c.CONTRACT_END_DATE)
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Commodity_Invoice_Recalc_Type acirt
                           WHERE
                                acirt.Account_Id = utility_cha.Account_Id
                                AND acirt.Commodity_ID = @Commodity_Id
                                AND acirt.Source_Cd = @Source_cd
                                AND (   c.CONTRACT_START_DATE BETWEEN acirt.Start_Dt
                                                              AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  c.CONTRACT_END_DATE BETWEEN acirt.Start_Dt
                                                                AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                        OR  acirt.Start_Dt BETWEEN c.CONTRACT_START_DATE
                                                           AND     c.CONTRACT_END_DATE
                                        OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                               AND     c.CONTRACT_END_DATE))
        GROUP BY
            utility_cha.Account_Id;




        SELECT  @Total_Count = MAX(Id)FROM  #Supplier_Account;


        WHILE (@Counter <= @Total_Count)
            BEGIN


                SELECT
                    @Account_Id = sa.Account_Id
                FROM
                    #Supplier_Account sa
                WHERE
                    sa.Id = @Counter;


                DELETE
                acirt
                FROM
                    dbo.Account_Commodity_Invoice_Recalc_Type acirt
                WHERE
                    acirt.Account_Id = @Account_Id
                    AND acirt.Commodity_ID = @Commodity_Id
                    AND acirt.Source_Cd = @Source_cd
                    AND (   @Contract_Start_Dt BETWEEN acirt.Start_Dt
                                               AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  @Contract_End_Dt BETWEEN acirt.Start_Dt
                                                 AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  acirt.Start_Dt BETWEEN @Contract_Start_Dt
                                               AND     @Contract_End_Dt
                            OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Contract_Start_Dt
                                                                   AND     @Contract_End_Dt);

                INSERT INTO dbo.Account_Commodity_Invoice_Recalc_Type
                     (
                         Account_Id
                         , Commodity_ID
                         , Start_Dt
                         , End_Dt
                         , Invoice_Recalc_Type_Cd
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                         , Determinant_Source_Cd
                         , Source_Cd
                     )
                SELECT
                    @Account_Id
                    , crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Supplier_Recalc_Type_Cd
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                    , crc.Determinant_Source_Cd
                    , NULL
                FROM
                    dbo.Contract_Recalc_Config crc
                WHERE
                    crc.Is_Consolidated_Contract_Config = 0
                    AND crc.Contract_Id = @Contract_Id
                    AND crc.Commodity_Id = @Commodity_Id
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acr
                                       WHERE
                                            acr.Account_Id = @Account_Id
                                            AND acr.Commodity_ID = crc.Commodity_Id
                                            AND acr.Start_Dt = crc.Start_Dt
                                            AND ISNULL(acr.End_Dt, '9999-12-31') = ISNULL(crc.End_Dt, '9999-12-31'))
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acirt
                                       WHERE
                                            acirt.Account_Id = @Account_Id
                                            AND acirt.Commodity_ID = crc.Commodity_Id
                                            AND (   crc.Start_Dt BETWEEN acirt.Start_Dt
                                                                 AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                                    OR  ISNULL(crc.End_Dt, '9999-12-31') BETWEEN acirt.Start_Dt
                                                                                         AND     ISNULL(
                                                                                                     acirt.End_Dt
                                                                                                     , '9999-12-31')
                                                    OR  acirt.Start_Dt BETWEEN crc.Start_Dt
                                                                       AND     ISNULL(crc.End_Dt, '9999-12-31')
                                                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN crc.Start_Dt
                                                                                           AND     ISNULL(
                                                                                                       crc.End_Dt
                                                                                                       , '9999-12-31')))
                GROUP BY
                    crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Determinant_Source_Cd
                    , crc.Supplier_Recalc_Type_Cd;


                SET @Counter = @Counter + 1;


            END;


        SELECT  @Total_Count = MAX(Id)FROM  #Consolidated_Account;
        SET @Counter = 1;

        WHILE (@Counter <= @Total_Count)
            BEGIN


                SELECT
                    @Account_Id = sa.Account_Id
                FROM
                    #Consolidated_Account sa
                WHERE
                    sa.Id = @Counter;


                DELETE
                acirt
                FROM
                    dbo.Account_Commodity_Invoice_Recalc_Type acirt
                WHERE
                    acirt.Account_Id = @Account_Id
                    AND acirt.Commodity_ID = @Commodity_Id
                    AND acirt.Source_Cd = @Source_cd
                    AND (   @Contract_Start_Dt BETWEEN acirt.Start_Dt
                                               AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  @Contract_End_Dt BETWEEN acirt.Start_Dt
                                                 AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  acirt.Start_Dt BETWEEN @Contract_Start_Dt
                                               AND     @Contract_End_Dt
                            OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Contract_Start_Dt
                                                                   AND     @Contract_End_Dt);

                INSERT INTO dbo.Account_Commodity_Invoice_Recalc_Type
                     (
                         Account_Id
                         , Commodity_ID
                         , Start_Dt
                         , End_Dt
                         , Invoice_Recalc_Type_Cd
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                         , Determinant_Source_Cd
                         , Source_Cd
                     )
                SELECT
                    @Account_Id
                    , crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Supplier_Recalc_Type_Cd
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                    , crc.Determinant_Source_Cd
                    , NULL
                FROM
                    dbo.Contract_Recalc_Config crc
                WHERE
                    crc.Is_Consolidated_Contract_Config = 1
                    AND crc.Contract_Id = @Contract_Id
                    AND crc.Commodity_Id = @Commodity_Id
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acr
                                       WHERE
                                            acr.Account_Id = @Account_Id
                                            AND acr.Commodity_ID = crc.Commodity_Id
                                            AND acr.Start_Dt = crc.Start_Dt
                                            AND ISNULL(acr.End_Dt, '9999-12-31') = ISNULL(crc.End_Dt, '9999-12-31'))
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acirt
                                       WHERE
                                            acirt.Account_Id = @Account_Id
                                            AND acirt.Commodity_ID = crc.Commodity_Id
                                            AND (   crc.Start_Dt BETWEEN acirt.Start_Dt
                                                                 AND     ISNULL(acirt.End_Dt, '9999-12-31')
                                                    OR  ISNULL(crc.End_Dt, '9999-12-31') BETWEEN acirt.Start_Dt
                                                                                         AND     ISNULL(
                                                                                                     acirt.End_Dt
                                                                                                     , '9999-12-31')
                                                    OR  acirt.Start_Dt BETWEEN crc.Start_Dt
                                                                       AND     ISNULL(crc.End_Dt, '9999-12-31')
                                                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN crc.Start_Dt
                                                                                           AND     ISNULL(
                                                                                                       crc.End_Dt
                                                                                                       , '9999-12-31')))
                GROUP BY
                    crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Determinant_Source_Cd
                    , crc.Supplier_Recalc_Type_Cd;


                SET @Counter = @Counter + 1;
            END;




    END;


GO
GRANT EXECUTE ON  [dbo].[Delete_Contract_Recalc_Config_Copy_To_Account_Level] TO [CBMSApplication]
GO
