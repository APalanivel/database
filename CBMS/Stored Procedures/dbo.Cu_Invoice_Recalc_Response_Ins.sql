
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

--> Create  the same  sproc  with new changes.

/******    
NAME:    
  dbo.Cu_Invoice_Recalc_Response_Ins    
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name								DataType		Default			Description    
-------------------------------------------------------------------------------------    
 @Created_Updated_User_Id			INT
 @tvp_Cu_Invoice_Recalc_Response	tvp_Cu_Invoice_Recalc_Response READONLY
 @Recalc_Response_Source			VARCHAR(25)
 @Cu_Invoice_Id						INT				 NULL
 @Account_Id						INT				 NULL
 @Commodity_Id						INT				 NULL
    
  
OUTPUT PARAMETERS:    
 Name								DataType		Default			Description    
-------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------    

BEGIN TRAN

DECLARE @tvp_Cu_Invoice_Recalc_Response tvp_Cu_Invoice_Recalc_Response
INSERT      INTO @tvp_Cu_Invoice_Recalc_Response
            ( 
             Charge_Name
            ,Bucket_Name
            ,Determinant_Unit
            ,Determinant_Value
            ,Rate_Unit
            ,Rate_Currency
            ,Rate_Amount
            ,Net_Amount
            ,Calculator_Working
            ,Net_Amount_Currency
            ,Is_Hidden_On_RA
            ,Is_Valid_Charge
            ,Charge_GUID
            ,Is_Locked
            ,Is_Send_To_Sys
            ,Comment_Text )
VALUES ( 'SE2017-263','Commodity','Days','100','Ccf','AED','100','1000',NULL,'CAN',0,1,'xxx',0,0,'Welcome 263 Save')

DECLARE @RECALC_HEADER_ID INT
SELECT * FROM dbo.RECALC_HEADER rh WHERE CU_INVOICE_ID = 7162976 AND ACCOUNT_ID = 20727 AND Commodity_Id = 291
SELECT    @RECALC_HEADER_ID=RECALC_HEADER_ID  FROM dbo.RECALC_HEADER rh WHERE CU_INVOICE_ID = 7162976 AND ACCOUNT_ID = 20727 AND Commodity_Id = 291 
SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr WHERE Recalc_Header_Id =@RECALC_HEADER_ID  
                       

EXEC [dbo].[Cu_Invoice_Recalc_Response_Ins]
@Created_Updated_User_Id = 49
,@tvp_Cu_Invoice_Recalc_Response = @tvp_Cu_Invoice_Recalc_Response
,@Recalc_Response_Source = 'User'
,@Cu_Invoice_Id =7162976 
,@Account_Id = 20727
,@Commodity_Id = 291

SELECT * FROM dbo.RECALC_HEADER rh WHERE CU_INVOICE_ID = 7162976 AND ACCOUNT_ID = 20727 AND Commodity_Id = 291
SELECT    @RECALC_HEADER_ID=RECALC_HEADER_ID  FROM dbo.RECALC_HEADER rh WHERE CU_INVOICE_ID = 7162976 AND ACCOUNT_ID = 20727 AND Commodity_Id = 291 
SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr WHERE Recalc_Header_Id =@RECALC_HEADER_ID  


ROLLBACK TRAN
                                            
                      
    
    
AUTHOR INITIALS:    
 Initials		Name    
-------------------------------------------------------------------------------------    
 RKV			Ravi Kumar Vegesna    
 NR				Narayana Reddy.
     
MODIFICATIONS    
    
 Initials	Date		Modification    
-------------------------------------------------------------------------------------    
 RKV		2015-10-14  Created for AS400-PII   
 NR			2017-03-15	MAINT-4845(5058) To Save the  Is_Valid_Charge Column.
 NR			2017-10-17	SE2017-263 - Save the Charge_GUID,Is_Locked,Is_Send_To_Sys,Charge_Comment_Id new columns.        

******/    
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Ins]
      (
       @Created_Updated_User_Id INT
      ,@tvp_Cu_Invoice_Recalc_Response AS tvp_Cu_Invoice_Recalc_Response READONLY
      ,@Recalc_Response_Source VARCHAR(25)
      ,@Cu_Invoice_Id INT = NULL
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT = NULL )
AS
BEGIN    
      SET NOCOUNT ON;
      
      DECLARE
            @Recalc_Response_Source_Cd INT
           ,@EC_Recalc_Response_Source_Cd INT
           ,@System_User INT
           ,@Comment_Id INT
           ,@Comment_Text VARCHAR(MAX)
           ,@MinId INT
           ,@MaxId INT
           ,@Recalc_Header_Id INT = NULL;
      
      DECLARE @tvp_Cu_Invoice_Recalc_Response_Dtl TABLE
 (
             Id INT IDENTITY(1, 1)
            ,Charge_Name NVARCHAR(200) NOT NULL
            ,Bucket_Name VARCHAR(255) NOT NULL
            ,Determinant_Unit NVARCHAR(200) NULL
            ,Determinant_Value DECIMAL(28, 10) NULL
            ,Rate_Unit NVARCHAR(200) NULL
            ,Rate_Currency NVARCHAR(200) NULL
            ,Rate_Amount DECIMAL(28, 10) NULL
            ,Net_Amount DECIMAL(28, 10) NULL
            ,Calculator_Working NVARCHAR(MAX) NULL
            ,Net_Amount_Currency NVARCHAR(200) NULL
            ,Is_Hidden_On_RA BIT NOT NULL
            ,Is_Valid_Charge BIT NOT NULL
            ,Charge_GUID NVARCHAR(60) NULL
            ,Is_Locked BIT NOT NULL
            ,Is_Send_To_Sys BIT NOT NULL
            ,Comment_Text NVARCHAR(MAX) NULL
            ,Comment_Id INT NULL )
     
      INSERT      INTO @tvp_Cu_Invoice_Recalc_Response_Dtl
                  (Charge_Name
                  ,Bucket_Name
                  ,Determinant_Unit
                  ,Determinant_Value
                  ,Rate_Unit
                  ,Rate_Currency
                  ,Rate_Amount
                  ,Net_Amount
                  ,Calculator_Working
                  ,Net_Amount_Currency
                  ,Is_Hidden_On_RA
                  ,Is_Valid_Charge
                  ,Charge_GUID
                  ,Is_Locked
                  ,Is_Send_To_Sys
                  ,Comment_Text
                  ,Comment_Id )
                  SELECT
                        Charge_Name
                       ,Bucket_Name
                       ,Determinant_Unit
                       ,Determinant_Value
                       ,Rate_Unit
                       ,Rate_Currency
                       ,Rate_Amount
                       ,Net_Amount
                       ,Calculator_Working
                       ,Net_Amount_Currency
                       ,Is_Hidden_On_RA
                       ,Is_Valid_Charge
                       ,Charge_GUID
                       ,Is_Locked
                       ,Is_Send_To_Sys
                       ,Comment_Text
                       ,NULL AS Comment_Id
                  FROM
                        @tvp_Cu_Invoice_Recalc_Response
  
      SELECT
            @MinId = MIN(Id)
           ,@MaxId = MAX(Id)
      FROM
            @tvp_Cu_Invoice_Recalc_Response_Dtl;
         
      WHILE @MinId <= @MaxId
            BEGIN   
                  SELECT
                        @Comment_Text = Comment_Text
                  FROM
                        @tvp_Cu_Invoice_Recalc_Response_Dtl
                  WHERE
                        Id = @MinId
                        AND Comment_Text IS NOT NULL
                        AND Comment_Id IS NULL;

                  IF @Comment_Text IS NOT NULL
                        BEGIN	
                              INSERT      INTO dbo.Comment
                                          (Comment_User_Info_Id
                                          ,Comment_Text )
                              VALUES
                                          (@Created_Updated_User_Id
                                          ,@Comment_Text )
								   
                              SELECT
                                    @Comment_Id = SCOPE_IDENTITY();	
					
                              UPDATE
                                    tvp
                              SET
                                    tvp.Comment_Id = @Comment_Id
                              FROM
                                    @tvp_Cu_Invoice_Recalc_Response_Dtl tvp
                              WHERE
                                    Id = @MinId;
		  
                        END;			   

                  SET @MinId = @MinId + 1;
				   
            END;       
            

      SELECT
            @Recalc_Header_Id = rh.RECALC_HEADER_ID
      FROM
            dbo.RECALC_HEADER rh
      WHERE
            CU_INVOICE_ID = @Cu_Invoice_Id
            AND ACCOUNT_ID = @Account_Id
            AND Commodity_Id = @Commodity_Id
  
            
        
      IF @Recalc_Header_Id IS NULL
            AND @Cu_Invoice_Id IS NOT NULL
            BEGIN  
                  INSERT      INTO dbo.RECALC_HEADER
                              (CU_INVOICE_ID
                              ,ACCOUNT_ID
                              ,CONTRACT_ID
                              ,GRAND_TOTAL
                              ,DATE_PERFORMED
                              ,Updated_User_Id
                              ,Commodity_Id )
                  VALUES
                              (@Cu_Invoice_Id
                              ,@Account_Id
                              ,-1
                              ,0
                              ,GETDATE()
                              ,@Created_Updated_User_Id
                              ,@Commodity_Id );    
       
                  SET @Recalc_Header_Id = SCOPE_IDENTITY();    
            END;

      SELECT
            @EC_Recalc_Response_Source_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'External calc'
            AND cs.Codeset_Name = 'Recalc Response Source';

      SELECT
            @Created_Updated_User_Id = ISNULL(@Created_Updated_User_Id, USER_INFO_ID)
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = 'Conversion';

      SELECT
            @Recalc_Response_Source_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = @Recalc_Response_Source
            AND cs.Codeset_Name = 'Recalc Response Source';

      INSERT      INTO dbo.Cu_Invoice_Recalc_Response
                  (Recalc_Header_Id
                  ,Charge_Name
                  ,Bucket_Master_Id
                  ,Determinant_Unit
                  ,Determinant_Value
                  ,Rate_Unit
                  ,Rate_Currency
                  ,Rate_Amount
                  ,Net_Amount
                  ,Calculator_Working
                  ,Rate_Uom_Type_Id
                  ,Rate_Currency_Unit_Id
                  ,Net_Amt_Currency_Unit_Id
                  ,Created_User_Id
                  ,Updated_User_Id
                  ,Is_Hidden_On_RA
                  ,Recalc_Response_Source_Cd
                  ,Is_Valid_Charge
                  ,Charge_GUID
                  ,Is_Locked
                  ,Is_Send_To_Sys
                  ,Charge_Comment_Id )
                  SELECT
                        @Recalc_Header_Id
                       ,tcirr.Charge_Name
                       ,bm.Bucket_Master_Id
                       ,tcirr.Determinant_Unit
                       ,tcirr.Determinant_Value
                       ,tcirr.Rate_Unit
                       ,tcirr.Rate_Currency
                       ,tcirr.Rate_Amount
                       ,tcirr.Net_Amount
                       ,tcirr.Calculator_Working
                       ,NULL
                       ,rcu.CURRENCY_UNIT_ID
                       ,ncu.CURRENCY_UNIT_ID
                       ,@Created_Updated_User_Id
                       ,@Created_Updated_User_Id
                       ,tcirr.Is_Hidden_On_RA
                       ,ISNULL(@Recalc_Response_Source_Cd, @EC_Recalc_Response_Source_Cd)
                       ,tcirr.Is_Valid_Charge
                       ,tcirr.Charge_GUID
                       ,tcirr.Is_Locked
                       ,tcirr.Is_Send_To_Sys
                       ,tcirr.Comment_Id
                  FROM
                        @tvp_Cu_Invoice_Recalc_Response_Dtl tcirr
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Name = tcirr.Bucket_Name
                        INNER JOIN dbo.Code c
                              ON c.Code_Id = bm.Bucket_Type_Cd
                        INNER JOIN dbo.RECALC_HEADER rh
                              ON bm.Commodity_Id = rh.Commodity_Id
                        INNER JOIN dbo.CURRENCY_UNIT ncu
                              ON ncu.CURRENCY_UNIT_NAME = tcirr.Net_Amount_Currency
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT rcu
                              ON rcu.CURRENCY_UNIT_NAME = tcirr.Rate_Currency
                  WHERE
                        rh.RECALC_HEADER_ID = @Recalc_Header_Id
                        AND c.Code_Value = 'Charge'
                        -- Excludes the response charges if the GUID matches with the existing record
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Cu_Invoice_Recalc_Response rr
                                         WHERE
                                          rr.Recalc_Header_Id = @Recalc_Header_Id
                                          AND rr.Charge_GUID = tcirr.Charge_GUID
                                          AND rr.Is_Locked = 1 )

END;

GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Ins] TO [CBMSApplication]
GO
