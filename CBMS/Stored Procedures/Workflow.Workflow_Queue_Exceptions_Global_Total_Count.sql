SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******      
NAME:    [Workflow].[Workflow_Queue_Exceptions_Global_Total_Count]  
DESCRIPTION:  This Procedure is created to get list of all global items count
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 EXEC [Workflow].[Workflow_Queue_Exceptions_Global_Total_Count]  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy 
AP	Arunkumar Palanivel  
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AP		Created new procedure to get global items count on left navigation
  
******/

CREATE PROCEDURE [Workflow].[Workflow_Queue_Exceptions_Global_Total_Count]
AS
      BEGIN




            DECLARE @Table_Count TABLE
                  ( Workflow_Sub_Queue_Name VARCHAR(100)
                  , total_Count             INT );
            WITH unassigned
            AS ( SELECT DISTINCT
                        cuex.CU_INVOICE_ID
                 FROM   dbo.CU_EXCEPTION_DENORM cuex
                        JOIN
                        dbo.QUEUE qt
                              ON cuex.QUEUE_ID = qt.QUEUE_ID
                        JOIN
                        dbo.ENTITY et
                              ON et.ENTITY_ID = qt.QUEUE_TYPE_ID
                 WHERE  et.ENTITY_NAME = 'public' 
				 AND CUEX.EXCEPTION_TYPE <> 'FAILED RECALC')
            INSERT      @Table_Count
                        SELECT
                              'Unassigned'
                            , count(CU_INVOICE_ID) AS Invoice_Count
                        FROM  unassigned;
            WITH assigned
            AS ( SELECT DISTINCT
                        cuex.CU_INVOICE_ID
                 FROM   dbo.CU_EXCEPTION_DENORM cuex
                        JOIN
                        dbo.QUEUE qt
                              ON cuex.QUEUE_ID = qt.QUEUE_ID
                        JOIN
                        dbo.ENTITY et
                              ON et.ENTITY_ID = qt.QUEUE_TYPE_ID
                 WHERE  et.ENTITY_NAME = 'Private'
				 AND CUEX.EXCEPTION_TYPE <> 'FAILED RECALC' )
            INSERT      @Table_Count
                        SELECT
                              'Assigned'
                            , count(CU_INVOICE_ID) AS Invoice_Count
                        FROM  assigned;

            INSERT      @Table_Count
                        SELECT
                              'All Open Exceptions'
                            , sum(total_Count)
                        FROM  @Table_Count;


            SELECT
                  *
            FROM  @Table_Count;

      END;
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Exceptions_Global_Total_Count] TO [CBMSApplication]
GO
