
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity

DESCRIPTION:

	Used to Select the data from Cost_Usage_Account_Dtl table for the given Account_id , period and commodity
	
INPUT PARAMETERS:
	Name				   DataType		Default	Description
------------------------------------------------------------
	@Account_Id			   INT
	@Client_Hier_Id		   INT
	@Commodity_id		   INT
	@Begin_Dt			   DATE
	@End_Dt				   DATE

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity 54752,15205,290,'1/1/2009','12/1/2009'
	EXEC dbo.Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity 538722,193597,291,'1/1/2012','12/1/2012'
	EXEC dbo.Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity 526708,289850,290,'1/1/2011','12/1/2011'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	AP			Athmaram Pabbathi
	Bch			Balaraju Chalumuri
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
	HG			3/06/2010	   Created
    DMR			09/10/2010   Modified for Quoted_Identifier
    AP			03/26/2012   Addnl Data Changes
						  -- Added @Client_Hier_Id parameter and used in filtering data
						  -- Added Client_Hier_Id in the return result
	BCH			2013-06-06	ENHANCEMENT-68, Added Last_Updated_Date and Last_Updated_By columns in output
	RR			2013-11-18	MAINT-2325 Added  data source code and uom name in select list
******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity
      ( 
       @Account_ID INT
      ,@Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN

      SET NOCOUNT ON
	
      SELECT
            cuad.ACCOUNT_ID
           ,cuad.Client_Hier_Id
           ,cuad.Service_Month
           ,cuad.UOM_Type_Id
           ,cuad.CURRENCY_UNIT_ID
           ,bkt_cd.Code_Value Bucket_Type
           ,bm.Bucket_Name
           ,cuad.Bucket_Value
           ,cuad.Cost_Usage_Account_Dtl_Id
           ,cuad.Bucket_Master_Id
           ,bm.Commodity_Id
           ,cuad.Updated_Ts AS Last_Updated_Date
           ,case WHEN cuad.Updated_By_Id IS NULL THEN crusr.FIRST_NAME + space(1) + crusr.LAST_NAME
                 ELSE modusr.FIRST_NAME + space(1) + modusr.LAST_NAME
            END AS Last_Updated_By
           ,dscd.Code_Value Data_Source_Code
           ,uom.ENTITY_NAME UOM
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.User_Info crusr
                  ON crusr.USER_INFO_ID = cuad.Created_By_Id
            LEFT OUTER JOIN dbo.USER_INFO modusr
                  ON cuad.Updated_By_Id = modusr.USER_INFO_ID
            LEFT OUTER JOIN dbo.Code dscd
                  ON cuad.Data_Source_Cd = dscd.Code_Id
            LEFT OUTER JOIN dbo.ENTITY uom
                  ON cuad.UOM_Type_Id = uom.ENTITY_ID
      WHERE
            cuad.ACCOUNT_ID = @Account_ID
            AND cuad.Client_Hier_Id = @Client_Hier_Id
            AND ( @Commodity_id IS NULL
                  OR bm.Commodity_Id = @Commodity_id )
            AND cuad.Service_Month BETWEEN @Begin_Dt AND @End_Dt
      ORDER BY
            cuad.Service_Month
           ,bkt_cd.Code_Value DESC
           ,bm.Bucket_Name

END;
;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Sel_Default_By_Account_Commodity] TO [CBMSApplication]
GO
