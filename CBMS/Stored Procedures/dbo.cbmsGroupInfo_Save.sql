SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.cbmsGroupInfo_Save
  
DESCRIPTION:   
  
INPUT PARAMETERS:      
 Name						DataType			Default     Description      
------------------------------------------------------------------      

 @group_info_id				INT					NULL  
 @group_name				VARCHAR(200)  
 @group_description			VARCHAR(500)		NULL  
 @queue_type				VARCHAR(200)
 @Group_Info_Category_Id	INT  
 @Is_Fee_Module				BIT					0
 @Is_Required_For_RA_Login		BIT					0
 @App_Access_Role_Type_Cd	INT					NULL 
 @Is_Shown_On_Watch_List	BIT									 

OUTPUT PARAMETERS:      
Name              DataType          Default     Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	BEGIN TRAN
		EXEC dbo.cbmsGroupInfo_Save  
			@group_info_id =1
			,@group_name = 'admin'
			,@group_description  = 'admin' 
			,@Group_info_Category_id = 10
			,@Is_Fee_Module = 1
			,@App_Access_Role_Type_Cd = NULL
			,@Is_Shown_On_Watch_List=0
	ROLLBACK TRAN

AUTHOR INITIALS:
Initials Name
------------------------------------------------------------
DR		 Deana Ritter
PNR		 Pandarinath
RR		 Raghu Reddy
HG		 Harihara Suthan G
SP		 Sandeep Pigilam
 
MODIFICATIONS

 Initials	Date		  Modification
------------------------------------------------------------
 DR			08/04/2009    Removed Linked Server Updates
 KH			10/14/2009    Added @queue_type as param
 DMR		09/10/2010	  Modified for Quoted_Identifier
 PNR		03/31/2011	  Added Group_Info_Category_Id column to the update statement as well added @Group_Info_Category_Id parameter to param list.
								Removed unused parameter @MyAccountid
 HG			04/27/2011	  Declaration statements Combined as per the review comments
 RR			2013-06-06	  ENHANCE-60 Its an enhancement as the modules will be Fee or non Fee, added new parameter @Is_Fee_Module to save in 
							Is_Fee_Module of dbo.group_info table
 HG			2014-01-30	  for RA Admin user management added @Is_Required_For_RA_Login,@App_Access_Role_Type_Cd as Input params 
						  and called Group_Info_App_Access_Role_Type_Cd_Merge Internally.	
 SP			2014-08-25    For Data Transition, Added Input parameter @Is_Shown_On_Watch_List.
******/

CREATE PROCEDURE [dbo].[cbmsGroupInfo_Save]
      ( 
       @queue_type VARCHAR(200) = NULL
      ,@group_info_id INT = NULL
      ,@group_name VARCHAR(200)
      ,@group_description VARCHAR(500) = NULL
      ,@Group_Info_Category_Id INT
      ,@Is_Fee_Module BIT = 0
      ,@Is_Required_For_RA_Login BIT = 0
      ,@App_Access_Role_Type_Cd INT = NULL
      ,@Is_Shown_On_Watch_List BIT = 0 )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Group_Info_Id_Tbl TABLE ( Group_Info_Id INT )

      DECLARE
            @new_queue_id INT
           ,@queue_type_id INT

      BEGIN TRY
            BEGIN TRAN
	
            IF @group_info_id IS NULL 
                  BEGIN 

                        IF @queue_type IS NULL 
                              BEGIN   
                                    SELECT
                                          @queue_type_id = entity_id
                                    FROM
                                          dbo.ENTITY
                                    WHERE
                                          ENTITY_NAME = 'Public'
                                          AND ENTITY_DESCRIPTION = 'Queue'
     
                                    INSERT      INTO dbo.queue
                                                ( queue_type_id )
                                    VALUES
                                                ( @queue_type_id )  
    
                                    SET @new_queue_id = SCOPE_IDENTITY()  
                              END  
                        ELSE 
                              BEGIN  
                                    SELECT
                                          @queue_type_id = Codeset_Id
                                    FROM
                                          dbo.Codeset
                                    WHERE
                                          codeset_name = @queue_type  
  
                                    INSERT      INTO dbo.queue
                                                ( queue_type_id )
                                    VALUES
                                                ( @queue_type_id )  
     
                                    SET @new_queue_id = SCOPE_IDENTITY()   
                              END  
                  END

            MERGE INTO dbo.Group_Info tgt
                  USING 
                        ( SELECT
                              @new_queue_id AS Queue_id
                             ,@group_info_id AS Group_Info_Id
                             ,@group_name AS Group_Name
                             ,@group_description AS Group_Description
                             ,@Group_Info_Category_Id AS Group_Info_Category_Id
                             ,@Is_Fee_Module AS Is_Fee_Module
                             ,@Is_Required_For_RA_Login AS Is_Required_For_RA_Login
                             ,@Is_Shown_On_Watch_List AS Is_Shown_On_Watch_List ) src
                  ON src.Group_Info_Id = tgt.Group_Info_Id
                  WHEN NOT MATCHED 
                        THEN
                  INSERT
                              ( 
                               queue_id
                              ,group_name
                              ,group_description
                              ,Group_Info_Category_Id
                              ,Is_Fee_Module
                              ,Is_Required_For_RA_Login
                              ,Is_Shown_On_Watch_List )
                         VALUES
                              ( 
                               src.Queue_Id
                              ,src.Group_Name
                              ,src.Group_Description
                              ,src.Group_Info_Category_Id
                              ,src.Is_Fee_Module
                              ,src.Is_Required_For_RA_Login
                              ,src.Is_Shown_On_Watch_List )
                  WHEN MATCHED 
                        THEN
                  UPDATE SET  
                              group_name = src.group_name
                             ,group_description = src.group_description
                             ,Group_Info_Category_Id = src.Group_Info_Category_Id
                             ,Is_Fee_Module = src.Is_Fee_Module
                             ,Is_Required_For_RA_Login = src.Is_Required_For_RA_Login
                             ,Is_Shown_On_Watch_List = src.Is_Shown_On_Watch_List
                  OUTPUT
                        INSERTED.Group_Info_Id
                        INTO @Group_Info_Id_Tbl
                              ( Group_Info_Id );

            SELECT
                  @Group_info_id = Group_info_id
            FROM
                  @Group_Info_Id_Tbl

            EXEC dbo.Group_Info_App_Access_Role_Type_Cd_Merge 
                  @Group_Info_Id = @Group_Info_Id
                 ,@App_Access_Role_Type_Cd = @App_Access_Role_Type_Cd

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END
	
            EXEC dbo.usp_RethrowError 
                  @CustomMessage = ''

      END CATCH

      SELECT
            @Group_info_id AS Group_info_id

END;
;

;
GO





GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_Save] TO [CBMSApplication]
GO
GO