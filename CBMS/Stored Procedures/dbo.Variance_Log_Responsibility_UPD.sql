
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/*********   
NAME:  
    dbo.Variance_Log_Responsibility_UPD  
 
DESCRIPTION:  
    Used to update queue_id, queue_name, queu_dt of variance_log table from Review Variance Page  
    when user selects resposibility dropdown

INPUT PARAMETERS:    
    Name              DataType          Default     Description    
------------------------------------------------------------    
    @Account_ID	  INT
    @Commodity_ID	  INT
    @Service_Month    DATE
    @Queue_Id		  INT		
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
------------------------------------------------------------  
BEGIN TRAN	
    EXECUTE dbo.Variance_Log_Responsibility_UPD 125493, 290, '2010-06-01', 145
ROLLBACK

	   select vlog.*
	      FROM
            dbo.Variance_Log vlog
            JOIN dbo.User_Info ui
                  ON ui.queue_id = 145
      WHERE
            vlog.Account_ID = 125493
            AND vlog.Commodity_ID = 290
            AND vlog.Service_Month = '2010-06-01'
            AND vlog.is_failure = 1
            AND vlog.Closed_Dt IS NULL

AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK	    Nageswara Rao Kosuri         
AP	    Athmaram Pabbathi

Initials	  Date	    Modification  
------------------------------------------------------------  
NK		  10/13/2009  Created
HG		  12/4/2009   Subselect used in the update statement replaced.
AP		  08/19/2011  Removed @Cost_Usage_Id Parameter and added following new parameters @Account_ID, @Commodity_ID, @Service_Month 
						and modified the script to use these parameters.
******/  


CREATE PROCEDURE dbo.Variance_Log_Responsibility_UPD
      ( 
       @Account_ID INT
      ,@Commodity_ID INT
      ,@Service_Month DATE
      ,@Queue_ID INT )
AS 
BEGIN
	
      SET NOCOUNT ON ;

      UPDATE
            vlog
      SET   
            vlog.Queue_Id = @Queue_Id
           ,vlog.Queue_Dt = getdate()
           ,vlog.Queue_Name = ui.First_Name + space(1) + ui.Last_Name
      FROM
            dbo.Variance_Log vlog
            JOIN dbo.User_Info ui
                  ON ui.queue_id = @Queue_Id
      WHERE
            vlog.Account_ID = @Account_ID
            AND vlog.Commodity_ID = @Commodity_ID
            AND vlog.Service_Month = @Service_Month
            AND vlog.is_failure = 1
            AND vlog.Closed_Dt IS NULL

			
END
;
GO

GRANT EXECUTE ON  [dbo].[Variance_Log_Responsibility_UPD] TO [CBMSApplication]
GO
