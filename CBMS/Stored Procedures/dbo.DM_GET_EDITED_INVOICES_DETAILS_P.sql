SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DM_GET_EDITED_INVOICES_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@fromDate      	VARCHAR(20)	          	
	@toDate        	VARCHAR(20)	          	
	@ubmId         	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DM_GET_EDITED_INVOICES_DETAILS_P '08/01/2008','08/31/2008',2

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/23/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.

******/

CREATE PROCEDURE dbo.DM_GET_EDITED_INVOICES_DETAILS_P
    @fromDate	VARCHAR(20),
    @toDate		VARCHAR(20),
    @ubmId		INT
AS
BEGIN

        DECLARE @selectClause	VARCHAR(8000),
				@fromClause		VARCHAR(8000),
				@whereClause	VARCHAR(8000),
				@groupByClause	VARCHAR(8000),
				@SQLStatement	VARCHAR(8000)

        SELECT  @selectClause = ' userInfo.FIRST_NAME + SPACE(1) + userInfo.LAST_NAME Corrected_By,
				cuInvoiceChangeLog.current_value Corrected_Value,
				cuInvoice.UPDATED_DATE Date_Edited,
				masterLog.Start_Date Date_of_Feed,
				cuInvoice.CBMS_IMAGE_ID DocId,
				cuInvoiceChangeLog.field_name Field,
				cuInvoiceChangeLog.previous_value Previous_Value,
				ubm.ubm_name Source,
				entity.entity_name as Field_Type
			        '

        SELECT  @fromClause = 'UBM_INVOICE invoice,
				UBM_BATCH_MASTER_LOG masterLog,
				UBM ubm,
				CU_INVOICE_CHANGE_LOG cuInvoiceChangeLog LEFT JOIN ENTITY entity ON (cuInvoiceChangeLog.Field_type_id = entity.Entity_id ),
				CU_INVOICE cuInvoice LEFT JOIN USER_INFO userinfo ON cuInvoice.UPDATED_BY_ID = userinfo.USER_INFO_ID '

        SELECT  @whereClause = 'invoice.UBM_BATCH_MASTER_LOG_ID = masterLog.UBM_BATCH_MASTER_LOG_ID AND
			      masterLog.UBM_ID = ubm.UBM_ID AND
			      invoice.IS_QUARTERLY = 0 AND
			      cuInvoice.UBM_INVOICE_ID = invoice.UBM_INVOICE_ID AND
			      cuInvoice.CBMS_IMAGE_ID = invoice.CBMS_IMAGE_ID AND
			      cuInvoiceChangeLog.CU_INVOICE_ID = cuInvoice.CU_INVOICE_ID '
	

        IF @ubmId > 0 
            BEGIN
                SELECT  @whereClause = @whereClause + ' AND ubm.UBM_ID = ' + STR(@ubmId) 
            END     

        IF ( @fromDate IS NOT NULL
             AND @fromDate <> ''
           )
            AND ( @toDate IS NOT NULL
                  AND @toDate <> ''
                ) 
            BEGIN

                IF ( @fromDate = @toDate ) 
                    BEGIN
                        SELECT  @whereClause = @whereClause
                                + ' AND CONVERT(Varchar(12), masterLog.END_DATE,101) = '
                                + CONVERT(VARCHAR(12), @fromDate, 101) 
                    END
                ELSE 
                    BEGIN
                        SELECT  @whereClause = @whereClause
                                + ' AND masterLog.END_DATE BETWEEN ' + '''' +
                                + CONVERT(VARCHAR(12), @fromDate, 101) + '''' +
                                + ' AND '+ '''' + CONVERT(VARCHAR(12), @toDate, 101) + ''''

                    END
            END 
		
        IF ( @fromDate IS NOT NULL
             AND @fromDate <> ''
           )
            AND ( @toDate IS NULL
                  OR @toDate = ''
                ) 
            BEGIN

                SELECT  @whereClause = @whereClause
                        + ' AND masterLog.END_DATE >= ' + CONVERT(VARCHAR(12), @fromDate, 101)
            END 


        IF ( @fromDate IS NULL
             OR @fromDate = ''
           )
            AND ( @toDate IS NOT NULL
                  AND @toDate <> ''
                ) 
            BEGIN

                SELECT  @whereClause = @whereClause
                        + ' AND masterLog.END_DATE <= '
                        + CONVERT(VARCHAR(12), @toDate, 101) 
            END 


        SELECT  @SQLStatement = 'SELECT ' + @selectClause + ' FROM '
                + @fromClause + ' WHERE ' + @whereClause 

        EXEC ( @SQLStatement )
        PRINT @SqlStatement
        

END
GO
GRANT EXECUTE ON  [dbo].[DM_GET_EDITED_INVOICES_DETAILS_P] TO [CBMSApplication]
GO
