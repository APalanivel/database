
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************    
NAME :	dbo.cbmsUserSiteMap_GetAssignedForUser 
   
DESCRIPTION: 
			This procedure used to get assigned sites to the perticular user
NOTE: Sp is created my Summit team.

   
 INPUT PARAMETERS:    
 Name              DataType               Default        Description    
------------------------------------------------------------------------------------     
@MyAccountId       INT
@MyClientID		   INT

   
 OUTPUT PARAMETERS:    
 Name               DataType              Default        Description    
-----------------------------------------------------------------------------------    
site_id				 INT
site_name			 VARCHAR
user_info_id		 INT
client_id			 INT
division_name		 VARCHAR
user_site_map_id	 INT
Division_Id			 INT


  
  USAGE EXAMPLES:    
-----------------------------------------------------------------------------------    
	 EXEC dbo.cbmsUserSiteMap_GetAssignedForUser  138,235
  
AUTHOR INITIALS:    
 Initials          Name    
-----------------------------------------------------------------------------------    
 KVK               K VINAY KUMAR
 NR                NARAYANA REDDY.
   
 MODIFICATIONS     
 Initials          Date                       Modification    
-----------------------------------------------------------------------------------    
 KVK               08/20/2010(MM/DD/YYYY)     Added "DivisionID" in select list
 KVK               12/02/2010                 Division is a view across all database to get the Division name we can have join with Sitegroup table and get the name
  NR               2014-01-01                 For RA-Admin added for Country name,state and city  columns
											  and Replaced to use User_Security_Role, Security_Role_Client_Hier and Client_Hier tables
											  
******************************************************************************************************/

CREATE PROCEDURE dbo.cbmsUserSiteMap_GetAssignedForUser
      ( 
       @MyAccountId INT
      ,@MyClientID INT )
AS 
BEGIN  
      SET NOCOUNT ON; 
       
      DECLARE
            @DivLevelCode INT
           ,@Security_Role_Id INT
    
    
    
      SELECT
            @DivLevelCode = cd.code_id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND cd.Code_Value = 'Division'
            
      SELECT
            @Security_Role_Id = sr.Security_Role_Id
      FROM
            dbo.Security_Role sr
            INNER JOIN dbo.User_Security_Role usr
                  ON sr.Security_Role_Id = usr.Security_Role_Id
      WHERE
            usr.User_Info_Id = @MyAccountId
            AND sr.Client_Id = @MyClientID     
              
      SELECT
            ch.site_id
           ,ch.site_name
           ,@MyAccountId AS user_info_id
           ,ch.client_id
           ,ch.Sitegroup_Name AS division_name
           ,ch.Sitegroup_Id AS Division_Id
           ,ch.Country_Name
           ,ch.City
           ,ch.State_Name
      FROM
            dbo.Security_Role_Client_Hier srch
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = srch.Client_Hier_Id
      WHERE
            srch.Security_Role_Id = @Security_Role_Id
            AND ch.Site_Id > 0
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Security_Role_Client_Hier srch1
                              INNER JOIN core.Client_Hier ch1
                                    ON ch1.Client_Hier_Id = srch1.Client_Hier_Id
                             WHERE
                              srch1.Security_Role_Id = @Security_Role_Id
                              AND ch1.Hier_level_Cd = @DivLevelCode
                              AND ch1.Sitegroup_Id = ch.Sitegroup_Id )
      GROUP BY
            ch.site_id
           ,ch.site_name
           ,ch.client_id
           ,ch.Sitegroup_Name
           ,ch.Sitegroup_Id
           ,ch.Country_Name
           ,ch.City
           ,ch.State_Name
      ORDER BY
            ch.Site_name


               
                        
END;


            
;
GO

GRANT EXECUTE ON  [dbo].[cbmsUserSiteMap_GetAssignedForUser] TO [CBMSApplication]
GO
