SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Account_History_Del_For_Contract]  
     
DESCRIPTION: 

	To Delete Account History associated with the given Contract Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------------------------------------------
@Contract_Id	INT						
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
------------------------------------------------------------------------------------------------


USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------

	EXEC Account_History_Del_For_Contract  30008
	
	EXEC Account_History_Del_For_Contract  34970


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------------------------------------------
PNR			PANDARINATH
NR			Narayana Reddy

MODIFICATIONS
INITIALS	DATE			MODIFICATION
------------------------------------------------------------------------------------------------
PNR			06/21/2010		Created
NR			2019-03-01		Watch List - Replaced Account-Processing-instruction to Config table.

*/

CREATE PROCEDURE [dbo].[Account_History_Del_For_Contract]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Account_Id INT
            , @Do_Not_Track_Id INT
            , @Entity_Id INT
            , @Variance_Consumption_Level_Id INT
            , @Variance_Rule_Dtl_Id INT
            , @Account_Invoice_Processing_Config_Id INT
            , @Account_Audit_Id INT
            , @Tracking_Id INT;


        DECLARE @Do_Not_Track_List TABLE
              (
                  Do_Not_Track_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Entity_List TABLE
              (
                  Entity_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Variance_Consumption_Level_List TABLE
              (
                  Variance_Consumption_Level_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Variance_Rule_Dtl_Account_Override_List TABLE
              (
                  Variance_Rule_Dtl_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Invoice_Processing_Config_List TABLE
              (
                  Account_Invoice_Processing_Config_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Audit_List TABLE
              (
                  Account_Audit_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Audit_Tracking_List TABLE
              (
                  Tracking_Id INT PRIMARY KEY CLUSTERED
              );

        SELECT
            @Account_Id = ACCOUNT_ID
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
        WHERE
            Contract_ID = @Contract_Id;

        INSERT INTO @Do_Not_Track_List
             (
                 Do_Not_Track_Id
             )
        SELECT  DO_NOT_TRACK_ID FROM    dbo.DO_NOT_TRACK WHERE ACCOUNT_ID = @Account_Id;

        INSERT INTO @Entity_List
             (
                 Entity_Id
             )
        SELECT  ENTITY_ID FROM  dbo.ENTITY WHERE ENTITY_TYPE = 500;


        INSERT INTO @Account_Variance_Consumption_Level_List
             (
                 Variance_Consumption_Level_Id
             )
        SELECT
            Variance_Consumption_Level_Id
        FROM
            dbo.Account_Variance_Consumption_Level
        WHERE
            ACCOUNT_ID = @Account_Id;


        INSERT INTO @Variance_Rule_Dtl_Account_Override_List
             (
                 Variance_Rule_Dtl_Id
             )
        SELECT
            Variance_Rule_Dtl_Id
        FROM
            dbo.Variance_Rule_Dtl_Account_Override
        WHERE
            ACCOUNT_ID = @Account_Id;



        INSERT INTO @Account_Invoice_Processing_Config_List
             (
                 Account_Invoice_Processing_Config_Id
             )
        SELECT
            Account_Invoice_Processing_Config_Id
        FROM
            dbo.Account_Invoice_Processing_Config
        WHERE
            Account_Id = @Account_Id;



        INSERT INTO @Account_Audit_List
             (
                 Account_Audit_Id
             )
        SELECT  ACCOUNT_AUDIT_ID FROM   dbo.ACCOUNT_AUDIT WHERE ACCOUNT_ID = @Account_Id;



        INSERT INTO @Account_Audit_Tracking_List
             (
                 Tracking_Id
             )
        SELECT
            aat.TRACKING_ID
        FROM
            dbo.ACCOUNT_AUDIT_TRACKING aat
            JOIN dbo.ACCOUNT_AUDIT aa
                ON aat.ACCOUNT_AUDIT_ID = aa.ACCOUNT_AUDIT_ID
        WHERE
            aa.ACCOUNT_ID = @Account_Id;


        BEGIN TRY
            BEGIN TRAN;

            WHILE EXISTS (SELECT    1 FROM  @Account_Variance_Consumption_Level_List)
                BEGIN

                    SET @Variance_Consumption_Level_Id = (   SELECT TOP 1
                                                                    Variance_Consumption_Level_Id
                                                             FROM
                                                                    @Account_Variance_Consumption_Level_List);

                    EXEC dbo.Account_Variance_Consumption_Level_Del
                        @Variance_Consumption_Level_Id
                        , @Account_Id;

                    DELETE
                    @Account_Variance_Consumption_Level_List
                    WHERE
                        Variance_Consumption_Level_Id = @Variance_Consumption_Level_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Variance_Rule_Dtl_Account_Override_List)
                BEGIN

                    SET @Variance_Rule_Dtl_Id = (   SELECT  TOP 1
                                                            Variance_Rule_Dtl_Id
                                                    FROM
                                                        @Variance_Rule_Dtl_Account_Override_List);

                    EXEC dbo.Variance_Rule_Dtl_Account_Override_Del
                        @Variance_Rule_Dtl_Id
                        , @Account_Id;

                    DELETE
                    @Variance_Rule_Dtl_Account_Override_List
                    WHERE
                        Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;
                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Invoice_Processing_Config_List)
                BEGIN

                    SET @Account_Invoice_Processing_Config_Id = (   SELECT  TOP 1
                                                                            Account_Invoice_Processing_Config_Id
                                                                    FROM
                                                                        @Account_Invoice_Processing_Config_List);

                    EXEC dbo.Account_Invoice_Processing_Config_History_Del_By_Config_Id
                        @Account_Invoice_Processing_Config_Id;


                    DELETE
                    @Account_Invoice_Processing_Config_List
                    WHERE
                        Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Audit_Tracking_List)
                BEGIN

                    SET @Tracking_Id = (SELECT  TOP 1   Tracking_Id FROM    @Account_Audit_Tracking_List);

                    EXEC dbo.SR_RFP_Bid_Del @Tracking_Id;

                    DELETE @Account_Audit_Tracking_List WHERE   Tracking_Id = @Tracking_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Audit_List)
                BEGIN

                    SET @Account_Audit_Id = (SELECT TOP 1   Account_Audit_Id FROM   @Account_Audit_List);

                    EXEC dbo.Account_Audit_Del @Account_Audit_Id;

                    DELETE @Account_Audit_List WHERE Account_Audit_Id = @Account_Audit_Id;
                END;


            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;


GO
GRANT EXECUTE ON  [dbo].[Account_History_Del_For_Contract] TO [CBMSApplication]
GO
