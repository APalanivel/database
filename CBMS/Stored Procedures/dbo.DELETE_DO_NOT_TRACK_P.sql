SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_DO_NOT_TRACK_P
	@account_id int
	AS
	begin
		set nocount on

		delete from do_not_track where account_id = @account_id
	end		



GO
GRANT EXECUTE ON  [dbo].[DELETE_DO_NOT_TRACK_P] TO [CBMSApplication]
GO
