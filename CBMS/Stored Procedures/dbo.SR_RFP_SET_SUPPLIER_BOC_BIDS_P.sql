SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_BIDS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@summitBidId               int,
@deliveryId                int,
@balancingId               int,
@riskManagementId          int,
@rfpAlternateFuelId        int,
@pricingScopeid            int,
@miscId                    int,
@supplierPriceCommentsId   int,
@selectedProductId         int,
@accountTermId             int,
@productName               varchar(200),
@supplierContactId         int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- check before and after insert of rows at SV
--SELECT * FROM sv..SR_RFP_SUPPLIER_SERVICE WHERE SR_RFP_SUPPLIER_SERVICE_ID = (SELECT MAX(SR_RFP_SUPPLIER_SERVICE_ID) FROM SV..SR_RFP_SUPPLIER_SERVICE)
----197763
--SELECT * FROM SV..SR_RFP_BID WHERE SR_RFP_BID_ID = (SELECT MAX(SR_RFP_BID_ID) FROM SV..SR_RFP_BID)
----216619
--SELECT * FROM SV..SR_RFP_TERM_PRODUCT_MAP WHERE SR_RFP_TERM_PRODUCT_MAP_ID = (SELECT MAX(SR_RFP_TERM_PRODUCT_MAP_ID) FROM SV..SR_RFP_TERM_PRODUCT_MAP)
----125416

---- Test a new Insert
--EXEC [dbo].[SR_RFP_SET_SUPPLIER_BOC_BIDS_P] 
--@summitBidId = null,
--@deliveryId = 197208,
--@balancingId = 197763,
--@riskManagementId =194602,
--@rfpAlternateFuelId =197203,
--@pricingScopeid = 196472,
--@miscId = 196472,
--@supplierPriceCommentsId = NULL, -- Orginal value = NULL
--@selectedProductId = 35,
--@accountTermId = 24396,
--@productName = NULL,
--@supplierContactId =NULL


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_BIDS_P] 
@summitBidId int,
@deliveryId int,
@balancingId int,
@riskManagementId int,
@rfpAlternateFuelId int,
@pricingScopeid int,
@miscId int,
@supplierPriceCommentsId int,
@selectedProductId int,
@accountTermId int,
@productName varchar(200),
@supplierContactId int



AS

SET NOCOUNT ON

DECLARE @supplierBOCId int
DECLARE @isSubProduct int
DECLARE @prodTermMapId int
DECLARE @supplierServiceId int

if @productName is not null
	begin
		select @isSubProduct=1
	end
else
	begin
		select @isSubProduct=null
	end

		INSERT INTO SR_RFP_SUPPLIER_SERVICE 
		(
			SR_RFP_DELIVERY_FUEL_ID,
			SR_RFP_BALANCING_TOLERANCE_ID,
			SR_RFP_RISK_MANAGEMENT_ID,
			SR_RFP_ALTERNATE_FUEL_ID,
			SR_RFP_PRICING_SCOPE_ID,
			SR_RFP_MISCELLANEOUS_POWER_ID
		)  
		VALUES
		(
			@deliveryId,
			@balancingId,
			@riskManagementId,
			@rfpAlternateFuelId,
			@pricingScopeid,
			@miscId
		)

		select @supplierServiceId=SCOPE_IDENTITY()


		INSERT INTO SR_RFP_BID 
		(
			PRODUCT_NAME,
			SR_RFP_BID_REQUIREMENTS_ID,
			SR_RFP_SUPPLIER_PRICE_COMMENTS_ID,
			SR_RFP_SUPPLIER_SERVICE_ID,
			IS_SUB_PRODUCT
		)  
		
		VALUES
		
		(
			@productName,
			@summitBidId,
			@supplierPriceCommentsId,
			@supplierServiceId,
			@isSubProduct
		)		
		select @supplierBOCId=@@identity

		INSERT INTO SR_RFP_TERM_PRODUCT_MAP 
		(
			SR_RFP_ACCOUNT_TERM_ID,
			SR_RFP_SELECTED_PRODUCTS_ID,
			SR_RFP_BID_ID,
			SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,
			BID_STATUS
		)
		VALUES 
		(
			@accountTermId,
			@selectedProductId,
			@supplierBOCId,
			@supplierContactId,
			convert(varchar(10),getdate(),101)
		)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_BIDS_P] TO [CBMSApplication]
GO
