SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_SAD_CREATE_SUPPLIER_PROFILE_DOCUMENT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId INT,
	@sessionId VARCHAR(200),
	@vendorId INT,
	@entityId INT,
	@cbmsImageId INT, --ADDED BY JAYA
	@reviewDate DATETIME
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_SAD_CREATE_SUPPLIER_PROFILE_DOCUMENT_P]
	@userId INT,
	@sessionId VARCHAR(200),
	@vendorId INT,
	@entityId INT,
	@cbmsImageId INT, --ADDED BY JAYA
	@reviewDate DATETIME
AS

BEGIN

	SET NOCOUNT ON
    
	DECLARE @profileId  int -- ,@cbmsImageId int --@entityId int,
		, @sr_supplier_profile_documents_id INT

	--replication
    
    
	--added by Jaya for updating entityid    
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId
	
	IF EXISTS(SELECT 1 FROM dbo.SR_SUPPLIER_PROFILE WHERE VENDOR_ID = @vendorId)
	 BEGIN

		SELECT @profileId = SR_SUPPLIER_PROFILE_ID FROM dbo.SR_SUPPLIER_PROFILE WHERE VENDOR_ID = @vendorId

		INSERT INTO dbo.SR_SUPPLIER_PROFILE_DOCUMENTS(SR_SUPPLIER_PROFILE_ID,
			CBMS_IMAGE_ID,
			UPLOADED_BY,
			UPLOADED_DATE,
			REVIEW_DATE)
		VALUES(@profileId,
			@cbmsImageId,    
			@userId,    
			GETDATE(),
			@reviewDate)

		UPDATE rfpCheckList
			SET rfpCheckList.IS_SUPPLIER_DOCS = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList	
			INNER JOIN dbo.Vendor V ON v.Vendor_ID = rfpCheckList.NEW_SUPPLIER_ID
			INNER JOIN dbo.SR_RFP_CLOSURE rfpCl ON rfpCl.VENDOR_ID = V.VENDOR_ID
		WHERE v.VENDOR_ID = @vendorId   

		SELECT @sr_supplier_profile_documents_id = SCOPE_IDENTITY() -- @@identity

		UPDATE rfpCheckList
			SET rfpCheckList.IS_SUPPLIER_DOCS = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList	
			INNER JOIN dbo.Vendor V ON v.Vendor_ID = rfpCheckList.NEW_SUPPLIER_ID
			INNER JOIN dbo.SR_RFP_CLOSURE rfpCl ON rfpCl.VENDOR_ID = V.VENDOR_ID
		WHERE v.VENDOR_ID = @vendorId   

	 END
	ELSE
	 BEGIN

		INSERT INTO dbo.SR_SUPPLIER_PROFILE(VENDOR_ID) VALUES(@vendorId)
		SELECT @profileId = SCOPE_IDENTITY() -- @@IDENTITY

		INSERT INTO dbo.SR_SUPPLIER_PROFILE_DOCUMENTS(SR_SUPPLIER_PROFILE_ID
			, CBMS_IMAGE_ID
			, UPLOADED_BY
			, UPLOADED_DATE
			, REVIEW_DATE)     
		VALUES(@profileId
			, @cbmsImageId
			, @userId
			, GETDATE()
			, @reviewDate)    
    
		UPDATE rfpCheckList
			SET rfpCheckList.IS_SUPPLIER_DOCS = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.VENDOR v ON v.Vendor_ID = rfpCheckList.NEW_SUPPLIER_ID
			INNER JOIN dbo.SR_RFP_CLOSURE rfpCl ON rfpCl.VENDOR_ID = v.Vendor_ID
		WHERE v.Vendor_ID = @vendorId
		    
		SELECT @sr_supplier_profile_documents_id = SCOPE_IDENTITY() --@@identity 

		UPDATE rfpCheckList
			SET rfpCheckList.IS_SUPPLIER_DOCS = 1
		FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.VENDOR v ON v.Vendor_ID = rfpCheckList.NEW_SUPPLIER_ID
			INNER JOIN dbo.SR_RFP_CLOSURE rfpCl ON rfpCl.VENDOR_ID = v.Vendor_ID
		WHERE v.Vendor_ID = @vendorId
	 END
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_CREATE_SUPPLIER_PROFILE_DOCUMENT_P] TO [CBMSApplication]
GO
