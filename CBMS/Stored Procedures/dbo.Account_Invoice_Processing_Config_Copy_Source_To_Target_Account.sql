SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
      dbo.Account_Invoice_Processing_Config_Copy_Source_To_Target_Account               
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                       DataType        Default        Description                    
-------------------------------------------------------------------------------------------------                   
 @Source_Account_Id							INT
 @Target_Account_Id							INT
 @User_Info_Id								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------




BEGIN TRAN


SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config] where Account_Id = 325423 
SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config] where Account_Id = 1655222 



SELECT
    *
FROM
    [dbo].[Account_Invoice_Processing_Config_Log] a
    INNER JOIN [dbo].[Account_Invoice_Processing_Config] b
        On a.Account_Invoice_Processing_Config_Id = b.Account_Invoice_Processing_Config_Id
where
    b.account_id = 1655222
	

EXEC dbo.Account_Invoice_Processing_Config_Copy_Source_To_Target_Account
         @Source_Account_Id =325423
         , @Target_Account_Id =1655222
         , @User_Info_Id = 49

SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config] where Account_Id = 325423 
SELECT  * FROM  [dbo].[Account_Invoice_Processing_Config] where Account_Id = 1655222 


SELECT    * FROM
    [dbo].[Account_Invoice_Processing_Config_Log] a
    INNER JOIN [dbo].[Account_Invoice_Processing_Config] b
        On a.Account_Invoice_Processing_Config_Id = b.Account_Invoice_Processing_Config_Id
where
    b.account_id = 1655222



ROLLBACK TRAN


                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Copy_Source_To_Target_Account]
    (
        @Source_Account_Id INT
        , @Target_Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE
            @Target_Processing_Instruction_Category_Cd INT
            , @Target_Config_Start_Dt DATE
            , @Target_Config_End_Dt DATE
            , @Target_Processing_Instruction NVARCHAR(MAX)
            , @Target_User_Info_Id INT
            , @Counter INT = 1;

        CREATE TABLE #Account_Invoice_Processing_Config
             (
                 Id INT IDENTITY(1, 1)
                 , Processing_Instruction_Category_Cd INT
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Processing_Instruction NVARCHAR(MAX)
                 , User_Info_Id INT
             );


        INSERT INTO #Account_Invoice_Processing_Config
             (
                 Processing_Instruction_Category_Cd
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Processing_Instruction
                 , User_Info_Id
             )
        SELECT
            aipc.Processing_Instruction_Category_Cd
            , aipc.Config_Start_Dt
            , aipc.Config_End_Dt
            , aipc.Processing_Instruction
            , aipc.Updated_User_Id
        FROM
            dbo.Account_Invoice_Processing_Config aipc
        WHERE
            aipc.Account_Id = @Source_Account_Id
            AND aipc.Is_Active = 1;



        BEGIN TRY
            BEGIN TRAN;

            WHILE (@Counter <= (SELECT  COUNT(1)FROM    #Account_Invoice_Processing_Config))
                BEGIN


                    SELECT
                        @Target_Processing_Instruction_Category_Cd = aipc.Processing_Instruction_Category_Cd
                        , @Target_Config_Start_Dt = aipc.Config_Start_Dt
                        , @Target_Config_End_Dt = aipc.Config_End_Dt
                        , @Target_Processing_Instruction = aipc.Processing_Instruction
                        , @Target_User_Info_Id = aipc.User_Info_Id
                    FROM
                        #Account_Invoice_Processing_Config aipc
                    WHERE
                        aipc.Id = @Counter;


                    EXEC dbo.Account_Invoice_Processing_Config_Ins
                        @Account_Id = @Target_Account_Id
                        , @Processing_Instruction_Category_Cd = @Target_Processing_Instruction_Category_Cd
                        , @Config_Start_Dt = @Target_Config_Start_Dt
                        , @Config_End_Dt = @Target_Config_End_Dt
                        , @Processing_Instruction = @Target_Processing_Instruction
                        , @User_Info_Id = @Target_User_Info_Id;



                    SET @Counter = @Counter + 1;


                END;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;


    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Copy_Source_To_Target_Account] TO [CBMSApplication]
GO
