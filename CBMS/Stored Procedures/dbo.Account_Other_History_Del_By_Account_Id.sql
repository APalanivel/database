SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME: [DBO].[Account_Other_History_Del_By_Account_Id]    
       
DESCRIPTION:   
  
 To Delete Account(both supplier and utility) other history associated with the given Account Id.  
 Following are the list of tables gets deleted in this procedure,  
  
  - Do_Not_Track  
  - Account_Variance_Consumption_Level  
  - Variance_Rule_Dtl_Account_Override  
  - Account_Invoice_Processing_Config  
  - Account_Audit_Tracking  
  - Account_Audit  
  
INPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION            
------------------------------------------------------------            
@Account_Id  INT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
  
------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 BEGIN TRAN  
  EXEC Account_Other_History_Del_By_Account_Id 3103  
 ROLLBACK TRAN  
  
 BEGIN TRAN  
  EXEC dbo.Account_Other_History_Del_By_Account_Id 3105  
 ROLLBACK TRAN  
  
AUTHOR INITIALS:  
 INITIALS NAME  
------------------------------------------------------------  
 HG   Harihara Suthan  
 RR   Raghu Reddy  
 SC  Sreenivasulu Cheerala 
MODIFICATIONS  
 INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
 HG   07/10/2010 Created  
 RR   2017-10-06 MAINT-5940 - Throwing FK reference error on delelting GROUP_BILLING_NUMBER(dbo.ACCOUNT_GROUP) as ACCOUNT_GROUP_ID   
       referenced by dbo.ACCOUNT table. Moved the script block to dbo.Account_Del script  
 NR   2019-03-01  Watch List - Replaced Account-Processing-instruction to Config table.  
 SC  2020-04-14  Added logic to delete primary meter flaged and Budget calc type account         
*/

CREATE PROCEDURE [dbo].[Account_Other_History_Del_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Do_Not_Track_Id INT
            , @Variance_Consumption_Level_Id INT
            , @Variance_Rule_Dtl_Id INT
            , @Account_Invoice_Processing_Config_Id INT
            , @Account_Audit_Id INT
            , @Tracking_Id INT;

        DECLARE @Account_Variance_Consumption_Level_List TABLE
              (
                  Variance_Consumption_Level_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Variance_Rule_Dtl_Account_Override_List TABLE
              (
                  Variance_Rule_Dtl_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Invoice_Processing_Config_List TABLE
              (
                  Account_Invoice_Processing_Config_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Audit_List TABLE
              (
                  Account_Audit_Id INT PRIMARY KEY CLUSTERED
              );
        DECLARE @Account_Audit_Tracking_List TABLE
              (
                  Tracking_Id INT PRIMARY KEY CLUSTERED
              );

        INSERT INTO @Account_Variance_Consumption_Level_List
             (
                 Variance_Consumption_Level_Id
             )
        SELECT
            Variance_Consumption_Level_Id
        FROM
            dbo.Account_Variance_Consumption_Level
        WHERE
            ACCOUNT_ID = @Account_Id;

        INSERT INTO @Variance_Rule_Dtl_Account_Override_List
             (
                 Variance_Rule_Dtl_Id
             )
        SELECT
            Variance_Rule_Dtl_Id
        FROM
            dbo.Variance_Rule_Dtl_Account_Override
        WHERE
            ACCOUNT_ID = @Account_Id;


        INSERT INTO @Account_Invoice_Processing_Config_List
             (
                 Account_Invoice_Processing_Config_Id
             )
        SELECT
            Account_Invoice_Processing_Config_Id
        FROM
            dbo.Account_Invoice_Processing_Config
        WHERE
            Account_Id = @Account_Id;

        INSERT INTO @Account_Audit_List
             (
                 Account_Audit_Id
             )
        SELECT  ACCOUNT_AUDIT_ID FROM   dbo.ACCOUNT_AUDIT WHERE ACCOUNT_ID = @Account_Id;

        INSERT INTO @Account_Audit_Tracking_List
             (
                 Tracking_Id
             )
        SELECT
            aat.TRACKING_ID
        FROM
            dbo.ACCOUNT_AUDIT_TRACKING aat
            JOIN dbo.ACCOUNT_AUDIT aa
                ON aat.ACCOUNT_AUDIT_ID = aa.ACCOUNT_AUDIT_ID
        WHERE
            aa.ACCOUNT_ID = @Account_Id;

        BEGIN TRY
            BEGIN TRAN;

            EXEC dbo.Do_Not_Track_Del @Account_Id;

            WHILE EXISTS (SELECT    1 FROM  @Account_Variance_Consumption_Level_List)
                BEGIN

                    SET @Variance_Consumption_Level_Id = (   SELECT TOP 1
                                                                    Variance_Consumption_Level_Id
                                                             FROM
                                                                    @Account_Variance_Consumption_Level_List);

                    EXEC dbo.Account_Variance_Consumption_Level_Del
                        @Variance_Consumption_Level_Id
                        , @Account_Id;

                    DELETE
                    @Account_Variance_Consumption_Level_List
                    WHERE
                        Variance_Consumption_Level_Id = @Variance_Consumption_Level_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Variance_Rule_Dtl_Account_Override_List)
                BEGIN

                    SET @Variance_Rule_Dtl_Id = (   SELECT  TOP 1
                                                            Variance_Rule_Dtl_Id
                                                    FROM
                                                        @Variance_Rule_Dtl_Account_Override_List);

                    EXEC dbo.Variance_Rule_Dtl_Account_Override_Del
                        @Variance_Rule_Dtl_Id
                        , @Account_Id;

                    DELETE
                    @Variance_Rule_Dtl_Account_Override_List
                    WHERE
                        Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Invoice_Processing_Config_List)
                BEGIN
                    SET @Account_Invoice_Processing_Config_Id = (   SELECT  TOP 1
                                                                            Account_Invoice_Processing_Config_Id
                                                                    FROM
                                                                        @Account_Invoice_Processing_Config_List);

                    EXEC dbo.Account_Invoice_Processing_Config_History_Del_By_Config_Id
                        @Account_Invoice_Processing_Config_Id;


                    DELETE
                    @Account_Invoice_Processing_Config_List
                    WHERE
                        Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;


                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Audit_Tracking_List)
                BEGIN

                    SET @Tracking_Id = (SELECT  TOP 1   Tracking_Id FROM    @Account_Audit_Tracking_List);

                    EXEC dbo.Account_Audit_Tracking_Del @Tracking_Id;

                    DELETE @Account_Audit_Tracking_List WHERE   Tracking_Id = @Tracking_Id;

                END;

            WHILE EXISTS (SELECT    1 FROM  @Account_Audit_List)
                BEGIN

                    SET @Account_Audit_Id = (SELECT TOP 1   Account_Audit_Id FROM   @Account_Audit_List);

                    EXEC dbo.Account_Audit_Del @Account_Audit_Id;

                    DELETE @Account_Audit_List WHERE Account_Audit_Id = @Account_Audit_Id;
                END;

            --Delete From Primary Meter Table  
            IF EXISTS (   SELECT
                                1
                          FROM
                                Budget.Account_Commodity_Primary_Meter
                          WHERE
                                Account_Id = @Account_Id)
                BEGIN
                    DELETE
                    acpm
                    FROM
                        Budget.Account_Commodity_Primary_Meter acpm
                    WHERE
                        acpm.Account_Id = @Account_Id;
                END;
            --Remove From Budget Calc Level Table.  
            IF EXISTS (   SELECT
                                1
                          FROM
                                Budget.Account_Commodity_Budget_Calc_Level
                          WHERE
                                Account_Id = @Account_Id)
                BEGIN
                    DELETE
                    acbcl
                    FROM
                        Budget.Account_Commodity_Budget_Calc_Level acbcl
                    WHERE
                        acbcl.Account_Id = @Account_Id;
                END;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;
    END;


GO


GRANT EXECUTE ON  [dbo].[Account_Other_History_Del_By_Account_Id] TO [CBMSApplication]
GO
