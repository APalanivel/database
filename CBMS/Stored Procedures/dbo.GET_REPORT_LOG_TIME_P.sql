SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_REPORT_LOG_TIME_P

@userId varchar(10),
@sessionId varchar(20), 
@batchId int,
@clientId int,
@reportListId int,
@divisionId int,
@siteId int,
@reportName varchar(200),
@reportCategoryName varchar(200)

AS
	set nocount on
IF @divisionId = 0 and @siteId = 0
BEGIN

	select max(batchLog.log_time)as logTime
	from rm_report_batch_log batchLog 
	left join division div on(div.division_id = batchLog.division_id) 
	left join site sit on(sit.site_id = batchLog.site_id), 
	rm_report_batch batch,	rm_report report, report_list list, 
	client cli, report_category category, report_list_report_category_map map 
	
	where batch.rm_report_batch_id = @batchId
	and batchLog.rm_report_batch_id = batch.rm_report_batch_id
	and report.rm_report_id = batchLog.rm_report_id
	and list.report_list_id = report.report_list_id
	and cli.client_id = batchLog.client_id
	and map.report_list_id = list.report_list_id
	and category.report_category_id = map.report_category_id
	and list.report_list_id = @reportListId
	and batchLog.client_id = @clientId
	and category.report_category_name = @reportCategoryName
	and list.report_name = @reportName
END
ELSE IF @divisionId > 0 	
BEGIN
	select max(batchLog.log_time)as logTime
	from rm_report_batch_log batchLog 
	left join division div on(div.division_id = batchLog.division_id) 
	left join site sit on(sit.site_id = batchLog.site_id), 
	rm_report_batch batch,	rm_report report, report_list list, 
	client cli, report_category category, report_list_report_category_map map 
	
	where batch.rm_report_batch_id = @batchId
	and batchLog.rm_report_batch_id = batch.rm_report_batch_id
	and report.rm_report_id = batchLog.rm_report_id
	and list.report_list_id = report.report_list_id
	and cli.client_id = batchLog.client_id
	and map.report_list_id = list.report_list_id
	and category.report_category_id = map.report_category_id
	and list.report_list_id = @reportListId
	and batchLog.client_id = @clientId
	and category.report_category_name = @reportCategoryName
	and list.report_name = @reportName
	and batchLog.division_id = @divisionId
END
ELSE IF @siteId > 0 	
BEGIN
	select max(batchLog.log_time)as logTime
	from rm_report_batch_log batchLog 
	left join division div on(div.division_id = batchLog.division_id) 
	left join site sit on(sit.site_id = batchLog.site_id), 
	rm_report_batch batch,	rm_report report, report_list list, 
	client cli, report_category category, report_list_report_category_map map 
	
	where batch.rm_report_batch_id = @batchId
	and batchLog.rm_report_batch_id = batch.rm_report_batch_id
	and report.rm_report_id = batchLog.rm_report_id
	and list.report_list_id = report.report_list_id
	and cli.client_id = batchLog.client_id
	and map.report_list_id = list.report_list_id
	and category.report_category_id = map.report_category_id
	and list.report_list_id = @reportListId
	and batchLog.client_id = @clientId
	and category.report_category_name = @reportCategoryName
	and list.report_name = @reportName
	and batchLog.site_id = @siteId
END
GO
GRANT EXECUTE ON  [dbo].[GET_REPORT_LOG_TIME_P] TO [CBMSApplication]
GO
