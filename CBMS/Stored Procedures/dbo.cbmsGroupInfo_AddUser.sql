
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsGroupInfo_AddUser]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId               int
	@user_info_id              int
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Insert new info mappings
begin tran
exec cbmsGroupInfo_AddUser
	 @MyAccountId = 1
	, @user_info_id = 17756
	, @group_info_id = 116
rollback



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter
		SP		Sandeep Pigilam

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
	  DMR    09/10/2010    Modified for Quoted_Identifier
	  SP     2014-01-08	   for RA Admin user management added Assigned_User_Id in Insert statement .
	  

******/
CREATE PROCEDURE [dbo].[cbmsGroupInfo_AddUser]
      ( 
       @MyAccountId INT
      ,@user_info_id INT
      ,@group_info_id INT )
AS 
BEGIN

      SET NOCOUNT ON

      INSERT      INTO user_info_group_info_map
                  ( 
                   group_info_id
                  ,user_info_id
                  ,Assigned_User_Id )
                  SELECT
                        @group_info_id
                       ,@user_info_id
                       ,@MyAccountId
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          user_info_group_info_map
                                     WHERE
                                          user_info_id = @user_info_id
                                          AND group_info_id = @group_info_id )       


      SELECT
            @group_info_id AS group_info_id
           ,@user_info_id AS user_info_id
 

END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_AddUser] TO [CBMSApplication]
GO
