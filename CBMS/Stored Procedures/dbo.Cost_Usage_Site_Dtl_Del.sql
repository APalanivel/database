SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cost_Usage_Site_Dtl_Del]  

DESCRIPTION: It Deletes Cost Usage Site for Given Site Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Cost_Usage_Site_Id	INT
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	   EXEC Cost_Usage_Site_Dtl_Del 3133
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			28-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Del
    (
       @Cost_Usage_Site_Dtl_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Cost_Usage_Site_Dtl 
	WHERE
		Cost_Usage_Site_Dtl_Id = @Cost_Usage_Site_Dtl_Id

END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Del] TO [CBMSApplication]
GO
