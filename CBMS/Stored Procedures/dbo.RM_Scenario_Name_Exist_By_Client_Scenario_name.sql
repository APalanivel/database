SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Name_Exist_By_Client_Scenario_name            
                        
 DESCRIPTION:                        
			To check if scenario name exist for given client and scenario name
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id						INT 
                 
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[RM_Scenario_Name_Exist_By_Client_Scenario_name] 
      @Client_Id = 10069
      ,@Scenario_Name='test 1'

EXEC [dbo].[RM_Scenario_Name_Exist_By_Client_Scenario_name] 
  @Client_Id = 10069
  ,@Scenario_Name='adf'        
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-14       Created                
                       
******/   
       
                
CREATE PROCEDURE [dbo].[RM_Scenario_Name_Exist_By_Client_Scenario_name]
      ( 
       @Client_Id INT
      ,@Scenario_Name NVARCHAR(200) )
AS 
BEGIN                
      SET NOCOUNT ON;     
      DECLARE @Is_Scenario_Name_Exist BIT = 0
      
      SELECT
            @Is_Scenario_Name_Exist = 1
      FROM
            dbo.RM_Scenario_Report rsr
      WHERE
            rsr.Client_Id = @Client_Id
            AND rsr.Scenario_Name = @Scenario_Name      
      SELECT
            @Is_Scenario_Name_Exist AS Is_Scenario_Name_Exist           
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Name_Exist_By_Client_Scenario_name] TO [CBMSApplication]
GO
