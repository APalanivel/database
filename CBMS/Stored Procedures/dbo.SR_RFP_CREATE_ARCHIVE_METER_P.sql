SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_ARCHIVE_METER_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@RFP_ID INT,
@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- earlier record count = 3
---- Test increase in rows before and after archiving
--select max(sr_rfp_archive_meter_id) from sv..SR_RFP_ARCHIVE_METER
---- 24740 + 3 rows after achiving
----Test archive
--exec [DBO].[SR_RFP_CREATE_ARCHIVE_METER_P] 5, 164

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [DBO].[SR_RFP_CREATE_ARCHIVE_METER_P] 

@RFP_ID INT,
@sr_rfp_account_id int

AS 
	
SET NOCOUNT ON


DECLARE @sr_rfp_archive_meter_id int, @sr_rfp_account_meter_map_id int, @meter_number varchar(200), @meter_address varchar(200), @rate_name varchar(200), @is_tax_exempt bit
DECLARE C_ARC_MET CURSOR FAST_FORWARD

FOR SELECT 
	map.SR_RFP_ACCOUNT_METER_MAP_ID, 
       	m.METER_NUMBER,
        ad.address_line1 + ' ' + ad.address_line2 as METER_ADDRESS,
        r.RATE_NAME,
        CASE m.TAX_EXEMPT_STATUS
       	WHEN '1' THEN 1
       	ELSE 0
	END As IS_TAX_EXEMPT
from 
	sr_rfp_account                  	rfp_account,
	sr_rfp_closure                  	closure,
	sr_rfp_account_meter_map 		map, 
	meter					m, 				
	address					ad, 			
	rate					r
where 
	rfp_account.SR_RFP_ID 		= @RFP_ID	
	and closure.sr_account_group_id = rfp_account.sr_rfp_account_id
     and 
	rfp_account.sr_rfp_account_id =  @sr_rfp_account_id 	
	and rfp_account.is_deleted = 0
     and
	m.METER_ID 			= map.METER_ID
     and
	m.ADDRESS_ID 			= ad.ADDRESS_ID 
     and
	r.RATE_ID = m.RATE_ID
     AND 
	map.SR_RFP_ACCOUNT_ID = rfp_account.sr_rfp_account_id

OPEN C_ARC_MET

FETCH NEXT FROM C_ARC_MET INTO @sr_rfp_account_meter_map_id, @meter_number, @meter_address, @rate_name, @is_tax_exempt
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN

		DELETE FROM 
			SR_RFP_ARCHIVE_METER 
		WHERE 
			sr_rfp_account_meter_map_id = @sr_rfp_account_meter_map_id and 
			meter_number = @meter_number 

		INSERT INTO SR_RFP_ARCHIVE_METER (
											SR_RFP_ACCOUNT_METER_MAP_ID, 
											METER_NUMBER, 
											METER_ADDRESS, 
											RATE_NAME, 
											IS_TAX_EXEMPT
										 )
								VALUES  (
											@sr_rfp_account_meter_map_id, 
											@meter_number, 
											@meter_address, 
											@rate_name, 
											@is_tax_exempt
										)
	
		SELECT @sr_rfp_archive_meter_id = @@identity
	
	END -- IF
		FETCH NEXT FROM C_ARC_MET INTO @sr_rfp_account_meter_map_id, @meter_number, @meter_address, @rate_name, @is_tax_exempt
END 
CLOSE C_ARC_MET
DEALLOCATE C_ARC_MET
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_ARCHIVE_METER_P] TO [CBMSApplication]
GO
