
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.Report_UtilityAccount_details  
   
DESCRIPTION:  
 This Procedure is to get the details based on input parameters  
   
INPUT PARAMETERS:  
Name			DataType	  Default Description  
-------------------------------------------------------------------------------------------  
@Utility		INT
@CEM_LIST		VARCHAR(MAX)  NULL
@RMA_LIST		VARCHAR(MAX)  NULL
@Rate_Name_LIST VARCHAR(MAX)  NULL
@State_LIST		VARCHAR(MAX)  NULL
@Region_LIST	VARCHAR(MAX)  NULL
@Site_Active	INT
@Account_Active INT
   
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
Execute Report_UtilityAccount_details  4655,'0','0','0','0','0',-1,-1  
Execute Report_UtilityAccount_details  4655,'0','0','0','0','0',0,0  
Execute Report_UtilityAccount_details  239,'0','0','12439,12438,1058','0','0',0,0   
   
AUTHOR INITIALS:  
Initials	 Name  
------------------------------------------------------------  
SSR		Sharad srivastava  
AKR     Ashok Kumar Raju
SP	   Sandeep Pigilam           


MODIFICATIONS 
Initials	Date		Modification  
------------------------------------------------------------  
 SSR		09/05/2010  Created  
 SKA		11/30/2010	Vendor_Commodity is for other services only, for regulated market details we have to refere Utility_Detail_Analyst_Map  
 LEC        12/02/2010	Commented out the filter for ATT  
 SSR		01/10/2011	Replaced Site address Details with Meter Address Details
 AKR        08/29/2011  Modified the sp to accept the Rate ID's for @Rate_ID_LIST(previously @Rate_Name_LIST), instead of Rate_Name.
 AKR        09/02/2011  Added Schema references for all the tables
 SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  

*/  
CREATE PROC [dbo].[Report_UtilityAccount_details]
      @Utility INT
     ,@CEM_LIST VARCHAR(MAX) = NULL
     ,@RMA_LIST VARCHAR(MAX) = NULL
     ,@Rate_ID_LIST VARCHAR(MAX) = NULL
     ,@State_LIST VARCHAR(MAX) = NULL
     ,@Region_LIST VARCHAR(MAX) = NULL
     ,@Site_Active INT
     ,@Account_Active INT
AS 
BEGIN              
             
      SET NOCOUNT ON 
        
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Market';              
    
      WITH  ratesanalysts_cte
              AS ( SELECT
                        ud.vendor_id
                       ,ui.first_Name + SPACE(1) + ui.last_name AS RegulatedMarketsAnalyst
                   FROM
                        dbo.UTILITY_DETAIL ud
                        JOIN dbo.Utility_Detail_Analyst_Map udam
                              ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                        JOIN dbo.GROUP_INFO gi
                              ON gi.GROUP_INFO_ID = udam.Group_Info_ID
                        INNER JOIN @Group_Legacy_Name gil
                              ON gi.Group_Info_ID = gil.GROUP_INFO_ID
                        JOIN dbo.user_info ui
                              ON ui.USER_INFO_ID = udam.Analyst_ID
                   WHERE
                        ( ( @RMA_LIST = '0' )
                          OR ( ui.USER_INFO_ID IN ( SELECT
                                                      ufn_rma_temp.Segments
                                                    FROM
                                                      dbo.ufn_split(@RMA_LIST, ',') ufn_rma_temp
                                                    GROUP BY
                                                      ufn_rma_temp.Segments ) ) )
                   GROUP BY
                        ud.vendor_id
                       ,ui.first_Name
                       ,ui.last_name)
            SELECT
                  ch.Client_name [Client]
                 ,cty.ENTITY_NAME [Client Type]
                 ,ch.Sitegroup_Name [Division Name]
                 ,CASE WHEN ch.Division_Not_Managed = 1 THEN 'No'
                       ELSE 'Yes'
                  END [Division Managed]
                 ,ch.site_name [Site Name]
                 ,sty.entity_name [Site Type]
                 ,ch.Site_id [Site ID]
                 ,CASE WHEN Ch.Site_Not_Managed = 1 THEN 'No'
                       ELSE 'Yes'
                  END [Site Managed]
                 ,CASE WHEN Ch.Site_Closed = 1 THEN 'Yes'
                       ELSE 'No'
                  END [Site Closed]
                 ,ch.region_name [Region]
                 ,cha.Account_Vendor_Name [Utility]
                 ,cha.account_number [Account Number]
                 ,CASE WHEN cha.Account_Not_Expected = 1 THEN 'Yes'
                       ELSE 'No'
                  END [Account Invoice Not Expected]
                 ,CASE WHEN cha.Account_Not_Managed = 1 THEN 'No'
                       ELSE 'Yes'
                  END [Account Managed]
                 ,cha.Meter_Number [Meter Number]
                 ,cha.Meter_Address_Line_1 + SPACE(1) + cha.Meter_Address_Line_2 [Meter Address]
                 ,cha.Meter_City [Meter City]
                 ,cha.Meter_State_Name [Meter State]
                 ,cha.Meter_ZipCode [Meter ZipCode]
                 ,cha.Meter_Country_Name [Meter Country]
                 ,cha.Rate_Name [Rate]
                 ,Com.commodity_name [Commodity]
                 ,utsv.entity_name [Service Level]
                 ,vcl.Consumption_Level_Desc [Consumption Level]
                 ,racte.RegulatedMarketsAnalyst [Regulated Markets Analyst]
            FROM
                  Core.Client_hier Ch
                  JOIN dbo.Client cl
                        ON cl.CLIENT_ID = Ch.Client_Id
                  JOIN dbo.SITE s
                        ON s.SITE_ID = Ch.Site_Id
                  JOIN Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  JOIN dbo.Commodity com
                        ON com.Commodity_Id = cha.Commodity_Id
                  JOIN dbo.ENTITY cty
                        ON cty.ENTITY_ID = cl.CLIENT_TYPE_ID
                  JOIN dbo.ENTITY sty
                        ON sty.ENTITY_ID = s.SITE_TYPE_ID
                  JOIN dbo.ENTITY utsv
                        ON utsv.ENTITY_ID = cha.Account_Service_level_Cd
                  JOIN dbo.Account_Variance_Consumption_Level avcl
                        ON avcl.ACCOUNT_ID = cha.ACCOUNT_ID
                  JOIN dbo.Variance_Consumption_Level vcl
                        ON vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
                           AND vcl.Commodity_Id = cha.Commodity_Id
                  JOIN ratesanalysts_cte racte
                        ON racte.VENDOR_ID = cha.Account_Vendor_Id
            WHERE
                  ch.Site_Id <> 0
                  AND cha.Account_Type = 'Utility'
                  AND ( @Site_Active = -1
                        OR ch.Site_Not_Managed = @Site_Active )
                  AND ( @Account_Active = -1
                        OR cha.Account_Not_Managed = @Account_Active )
                  AND ( ( @Utility = 0 )
                        OR ( cha.Account_Vendor_Id = @Utility ) )
                  AND ( ( @State_LIST = '0' )
                        OR ( ch.STATE_ID IN ( SELECT
                                                ufn_State_temp.Segments
                                              FROM
                                                dbo.ufn_split(@State_LIST, ',') ufn_State_temp
                                              GROUP BY
                                                ufn_State_temp.Segments ) ) )
                  AND ( ( @Region_LIST = '0' )
                        OR ( ch.REGION_ID IN ( SELECT
                                                ufn_region_temp.Segments
                                               FROM
                                                dbo.ufn_split(@Region_LIST, ',') ufn_region_temp
                                               GROUP BY
                                                ufn_region_temp.Segments ) ) )
                  AND ( ( @Rate_ID_LIST = '0' )
                        OR ( cha.RATE_ID IN ( SELECT
                                                r.RATE_ID
                                              FROM
                                                dbo.ufn_split(@Rate_ID_LIST, ',') ufn_rate_id_temp
                                                JOIN dbo.RATE r
                                                      ON ufn_rate_id_temp.Segments = r.RATE_ID
                                              WHERE
                                                ( @Utility = 0 )
                                                OR ( r.VENDOR_ID = @Utility )
                                              GROUP BY
                                                r.RATE_ID ) ) )
                  AND ( ( @CEM_LIST = '0' )
                        OR ( cl.CLIENT_ID IN ( SELECT
                                                cem.CLIENT_ID
                                               FROM
                                                dbo.CLIENT_CEM_MAP cem
                                                JOIN dbo.ufn_split(@CEM_LIST, ',') ufn_CEM_temp
                                                      ON CAST(ufn_CEM_temp.Segments AS INT) = cem.USER_INFO_ID
                                               GROUP BY
                                                cem.CLIENT_ID ) ) )              
                                            
                
END 
;
GO

GRANT EXECUTE ON  [dbo].[Report_UtilityAccount_details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UtilityAccount_details] TO [CBMSApplication]
GO
