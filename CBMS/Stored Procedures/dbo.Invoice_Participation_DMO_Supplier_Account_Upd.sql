SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Invoice_Participation_DMO_Supplier_Account_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_info_id  	int       	          	
	@account_id    	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR	        2017-03-07	Created Contract placeholder CP-8
******/

CREATE PROCEDURE [dbo].[Invoice_Participation_DMO_Supplier_Account_Upd]
      ( 
       @User_Info_Id INT
      ,@Account_Id INT
       )
AS 
BEGIN

      SET NOCOUNT ON;

      
      EXEC dbo.cbmsInvoiceParticipationQueue_Save 
            @User_Info_Id
           ,21 -- see called sproc for definition
           ,NULL
           ,NULL
           ,NULL
           ,@Account_Id
           ,NULL
           ,1

END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_DMO_Supplier_Account_Upd] TO [CBMSApplication]
GO
