SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[SR_RFP_Archive_Checklist_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Archive Checklist for Selected Sr RFP Archive Checklist Id.
      
INPUT PARAMETERS:          
NAME						  DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Archive_Checklist_Id  INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Archive_Checklist_Del  180
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Archive_Checklist_Del
   (
    @SR_RFP_Archive_Checklist_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_ARCHIVE_CHECKLIST
	WHERE
		SR_RFP_ARCHIVE_CHECKLIST_ID = @SR_RFP_Archive_Checklist_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Archive_Checklist_Del] TO [CBMSApplication]
GO
