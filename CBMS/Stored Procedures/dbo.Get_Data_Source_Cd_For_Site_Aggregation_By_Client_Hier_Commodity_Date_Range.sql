SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
NAME:                  
 dbo.Get_Data_Source_Cd_For_Site_Aggregation_By_Client_Hier_Commodity_Date_Range        
                  
DESCRIPTION:                  

 This procedure is to get the Data Source code for site level aggregation
 
INPUT PARAMETERS:
   Name					DataType     Default     Description
------------------------------------------------------------------------------------------------
   @Service_Start_Dt	DATE
   @Service_End_Dt		DATE
   @Commodity_Id		INT
   @Client_Heir_Id		INT

OUTPUT PARAMETERS:
 Name          DataType     Default     Description
-------------------------------------------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------------------------------------------

	EXEC Get_Data_Source_Cd_For_Site_Aggregation_By_Client_Hier_Commodity_Date_Range   '2011-12-01', '2011-12-31',67,31263
	EXEC Get_Data_Source_Cd_For_Site_Aggregation_By_Client_Hier_Commodity_Date_Range   '2010-01-01', '2010-01-31',1585,31263

AUTHOR INITIALS:
 Initials  Name
------------------------------------------------------------------------------------------------
   RKV     Ravi Kumar Vegesna

MODIFICATIONS
 Initials  Date         Modification
------------------------------------------------------------------------------------------------
   RKV     2013-05-21   Created
******/

CREATE PROCEDURE dbo.Get_Data_Source_Cd_For_Site_Aggregation_By_Client_Hier_Commodity_Date_Range
      (
       @Service_Start_Dt DATE
      ,@Service_End_Dt DATE
      ,@Commodity_Id INT
      ,@Client_Heir_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Data_Source_Cd_CBMS INT
           ,@Data_Source_Cd_DE INT
           ,@Max_Data_Source_Cd INT
           ,@Has_Cbms_Data_Source_Cd BIT

      SELECT
            @Data_Source_Cd_CBMS = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.Code_Id
                                       END)
           ,@Data_Source_Cd_DE = MAX(CASE WHEN cd.Code_Value = 'DE' THEN cd.Code_Id
                                     END)
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = cd.Codeset_Id
      WHERE
            cd.Code_Value IN ( 'CBMS', 'DE' )
            AND cs.Codeset_Name = 'DataSource';

      WITH  Cte_Data_Source_Cd
              AS ( SELECT
                        MAX(CASE WHEN baid.Data_Source_Cd = @Data_Source_Cd_CBMS THEN 1
                                 ELSE 0
                            END) AS Has_Cbms_Data_Source
                       ,MAX(baid.Data_Source_Cd) AS Max_Data_Source_Cd
                   FROM
                        dbo.Bucket_Account_Interval_Dtl baid
                        INNER JOIN dbo.Bucket_Master bm
                              ON baid.Bucket_Master_Id = bm.Bucket_Master_Id
                   WHERE
                        ( baid.Service_Start_Dt BETWEEN @Service_Start_Dt
                                                AND     @Service_End_Dt
                          OR baid.Service_End_Dt BETWEEN @Service_Start_Dt
                                                 AND     @Service_End_Dt
                          OR @Service_Start_Dt BETWEEN baid.Service_Start_Dt
                                               AND     baid.Service_End_Dt
                          OR @Service_End_Dt BETWEEN baid.Service_Start_Dt
                                             AND     baid.Service_End_Dt )
                        AND baid.Client_Hier_Id = @Client_Heir_Id
                        AND bm.Commodity_Id = @Commodity_id
                        AND baid.Data_Source_Cd IN ( @Data_Source_Cd_CBMS, @Data_Source_Cd_DE ))
            SELECT
                  CASE WHEN ds.Has_Cbms_Data_Source = 1 THEN @Data_Source_Cd_CBMS
                       ELSE ds.Max_Data_Source_Cd
                  END AS Data_Source_Cd
            FROM
                  Cte_Data_Source_Cd ds
                                                                                       
END

;
GO
GRANT EXECUTE ON  [dbo].[Get_Data_Source_Cd_For_Site_Aggregation_By_Client_Hier_Commodity_Date_Range] TO [CBMSApplication]
GO
