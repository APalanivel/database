SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******             
                         
 NAME: [dbo].[Get_IC_Account_Config_Group_By_GroupName]                 
                            
 DESCRIPTION:                            
			To insert or update or delete data in [Invoice_Collection_Account_Config_Group]
                            
 INPUT PARAMETERS:            
                           
 Name                            DataType           Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @Keyword           
                            
 OUTPUT PARAMETERS:                 
                            
 Name                            DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          
--EXAMPLE 1

 exec Get_IC_Account_Config_Group_By_GroupName
 Get_IC_Account_Config_Group_By_GroupName 'test'

 
                           
 AUTHOR INITIALS:            
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SLP					Sri Lakshmi Pallikonda                
                             
 MODIFICATIONS:          
           
 Initials               Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SLP                    2019-10-14     Created for searching Acct Config Group Names   
******/

CREATE PROCEDURE [dbo].[Get_IC_Account_Config_Group_By_GroupName]
     (
         @Keyword VARCHAR(200) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT
            Invoice_Collection_Account_Config_Group_ID Group_Id
            , Invoice_Collection_Account_Config_Group_name AS Group_Name
        FROM
            Invoice_Collection_Account_Config_Group
        WHERE
            (   @Keyword IS NULL
                OR  Invoice_Collection_Account_Config_Group_name LIKE +'%' + @Keyword + '%');
    END;




GO
GRANT EXECUTE ON  [dbo].[Get_IC_Account_Config_Group_By_GroupName] TO [CBMSApplication]
GO
