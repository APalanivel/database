SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
           
/******            
 NAME: [dbo].[RA_User_Monthly_Login_Count_Get_By_Client_Id]          
          
 DESCRIPTION:            
  To get User Monthly Login Count based on Client Id            
            
 INPUT PARAMETERS:            
           
 Name                                DataType            Default        Description            
---------------------------------------------------------------------------------------------------------------          
 @Client_Id                          INT                    
 @User_Info_Id						 INT				  NULL
 @From_Dt							 DATE				  NULL
 @To_Dt								 DATE				  NULL         
            
 OUTPUT PARAMETERS:                  
 Name                                DataType            Default        Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------           
           
     EXEC dbo.RA_User_Monthly_Login_Count_Get_By_Client_Id 
      @Client_Id = 235
     ,@From_Dt = '2013-01-01'
     ,@To_Dt = '2013-12-01'   
      	   
     EXEC dbo.RA_User_Monthly_Login_Count_Get_By_Client_Id 
      @Client_Id = 235
     ,@User_Info_Id = 182
     ,@From_Dt = '2013-01-01'
     ,@To_Dt = '2013-12-01'  

  
 AUTHOR INITIALS:           
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SP                     Sandeep Pigilam              
             
 MODIFICATIONS:           
            
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                     2013-12-12      Created for RA Admin user management          
           
******/            
CREATE PROCEDURE [dbo].[RA_User_Monthly_Login_Count_Get_By_Client_Id]
      ( 
       @Client_Id INT
      ,@User_Info_Id INT = NULL
      ,@From_Dt DATE = NULL
      ,@To_Dt DATE = NULL )
AS 
BEGIN       
     
            
      SET NOCOUNT ON    
      

            
      SELECT
            Dat.DATE_D
           ,Monthly_Count.Month_Login_Count
           ,rumlc_user.Login_Cnt AS User_Monthly_Login_Count
      FROM
            meta.DATE_DIM Dat
            LEFT OUTER JOIN ( SELECT
                                    rumlc.Login_Month
                                   ,AVG(rumlc.Login_Cnt) AS Month_Login_Count
                              FROM
                                    dbo.RA_User_Monthly_Login_Count rumlc
                                    INNER JOIN dbo.USER_INFO ui
                                          ON rumlc.USER_INFO_ID = ui.USER_INFO_ID
                                             AND ui.Client_Id = @Client_Id
                                             AND ui.IS_HISTORY = 0
                                             AND ui.ACCESS_LEVEL = 1
                              WHERE
                                    rumlc.Login_Month >= @From_Dt
                                    AND rumlc.Login_Month <= @To_Dt
                              GROUP BY
                                    rumlc.Login_Month ) AS Monthly_Count
                  ON Dat.DATE_D = Monthly_Count.Login_Month
            LEFT OUTER JOIN dbo.RA_User_Monthly_Login_Count rumlc_user
                  ON Dat.DATE_D = rumlc_user.Login_Month
                     AND rumlc_user.USER_INFO_ID = @User_Info_Id
      WHERE
            Dat.DATE_D >= @From_Dt
            AND Dat.DATE_D <= @To_Dt
      ORDER BY
            Dat.DATE_D
            
END 



;
GO
GRANT EXECUTE ON  [dbo].[RA_User_Monthly_Login_Count_Get_By_Client_Id] TO [CBMSApplication]
GO
