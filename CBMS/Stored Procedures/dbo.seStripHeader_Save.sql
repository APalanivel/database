SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seStripHeader_Save
	(
		@StripHeaderId int = null
	, @AccountId int 
	, @StripName varchar(100) = null
	, @ExpirationDate datetime = null
	, @UniformVolume bit
	)
AS
BEGIN

	set nocount on
	
	declare @ThisId int
	
	set @ThisId = @StripHeaderId
	
	if @ThisId is null
	begin
	
		if @StripName is null
		begin

			select @ThisId = StripHeaderId
			  from seStripHeader
			 where AccountId = @AccountId
			   and StripName is null

		end
	
	end

	if @ThisId is null
	begin
	
		insert into seStripHeader
			( AccountId
			, StripName
			, ExpirationDate
			, UniformVolume
			)
			values
			( @AccountId
			, @StripName
			, @ExpirationDate
			, @UniformVolume
			)
	
		set @ThisId = @@IDENTITY

	end
	else
	begin
	
		update seStripHeader
		   set AccountId = @AccountId
		     , StripName = @StripName
		     , ExpirationDate = @ExpirationDate
		     , UniformVolume = @UniformVolume
		 where StripHeaderId = @ThisId
		
	end
	exec seStripHeader_Get @ThisId

END
GO
GRANT EXECUTE ON  [dbo].[seStripHeader_Save] TO [CBMSApplication]
GO
