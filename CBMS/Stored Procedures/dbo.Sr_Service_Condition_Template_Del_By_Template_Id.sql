
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Del_By_Template_Id]
           
DESCRIPTION:             
			To get mapped countries to SR pricing product
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Template_Name						INT				NULL
    @Commodity_Id						INT				NULL
    @Country_Id							VARCHAR(MAX)	NULL
    @SR_PRICING_PRODUCT_ID				INT				NULL


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	SELECT * FROM dbo.Sr_Service_Condition_Template

BEGIN TRANSACTION
	SELECT * FROM dbo.Sr_Service_Condition_Template ssct 
		left JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		ON ssct.Sr_Service_Condition_Template_Id = ssctcm.Sr_Service_Condition_Template_Id
		left JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		left JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctmp 
		ON ssct.Sr_Service_Condition_Template_Id = ssctmp.Sr_Service_Condition_Template_Id
		WHERE ssct.Sr_Service_Condition_Template_Id = 15
	EXEC dbo.Sr_Service_Condition_Template_Del_By_Template_Id 15
	SELECT * FROM dbo.Sr_Service_Condition_Template ssct 
		left JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm 
		ON ssct.Sr_Service_Condition_Template_Id = ssctcm.Sr_Service_Condition_Template_Id
		left JOIN dbo.Sr_Service_Condition_Template_Question_Map sctqm 
		ON ssctcm.Sr_Service_Condition_Template_Category_Map_Id = sctqm.Sr_Service_Condition_Template_Category_Map_Id
		left JOIN dbo.Sr_Service_Condition_Template_Product_Map ssctmp 
		ON ssct.Sr_Service_Condition_Template_Id = ssctmp.Sr_Service_Condition_Template_Id
		WHERE ssct.Sr_Service_Condition_Template_Id = 15
ROLLBACK TRANSACTION

		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-31	Global Sourcing - Phase3 - GCS-602 Created
				2016-05-16	Removing referneces from "SR_RFP_PRICING_PRODUCT" for the deleting template.
******/

CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Del_By_Template_Id]
      ( 
       @Sr_Service_Condition_Template_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      BEGIN TRY                                
            BEGIN TRANSACTION
      
            DELETE
                  ssctqm
            FROM
                  dbo.Sr_Service_Condition_Template_Question_Map ssctqm
                  INNER JOIN dbo.Sr_Service_Condition_Template_Category_Map ssctcm
                        ON ssctqm.Sr_Service_Condition_Template_Category_Map_Id = ssctcm.Sr_Service_Condition_Template_Category_Map_Id
            WHERE
                  ssctcm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            
            DELETE FROM
                  dbo.Sr_Service_Condition_Template_Category_Map
            WHERE
                  Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            
            DELETE FROM
                  dbo.Sr_Service_Condition_Template_Product_Map
            WHERE
                  Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                  
            UPDATE
                  dbo.SR_RFP_PRICING_PRODUCT
            SET   
                  Sr_Service_Condition_Template_Id = NULL
            WHERE
                  Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                  
            
            DELETE FROM
                  dbo.Sr_Service_Condition_Template
            WHERE
                  Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                  
                  
            
            COMMIT TRANSACTION                                         
                                           
      END TRY                            
      BEGIN CATCH                            
            IF @@TRANCOUNT > 0 
                  BEGIN            
                        ROLLBACK TRANSACTION            
                  END                           
            EXEC usp_RethrowError                            
      END CATCH 
                                        
END;
;

;
GO

GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Del_By_Template_Id] TO [CBMSApplication]
GO
