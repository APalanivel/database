SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[cbmsSourcingGraph_Delete]
(
	@MyAccountId int
	,@Position int
)
as

delete 
from sourcing_graph
where user_info_id = @MyAccountId
and position = @Position

GO
GRANT EXECUTE ON  [dbo].[cbmsSourcingGraph_Delete] TO [CBMSApplication]
GO
