SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Client_Service_Category_Del


DESCRIPTION: 
Delete data from the Client_Service_Category table.

INPUT PARAMETERS:    
      Name					DataType          Default     Description    
------------------------------------------------------------------    
@Client_Service_Category_Id   int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
Exec Client_Service_Category_Del  12



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	BCH     10/19/2011	   Created 
******/
CREATE PROCEDURE dbo.Client_Service_Category_Del
      (
       @Client_Service_Category_Id INT )
AS
BEGIN
      SET NOCOUNT ON;
      DELETE
            csc
      FROM
            dbo.Client_Service_Category csc
      WHERE
            Client_Service_Category_Id = @Client_Service_Category_Id
END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Del] TO [CBMSApplication]
GO
