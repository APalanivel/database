SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_CHARGE_DETAILS_P
	@charge_master_id INT,
	@charge_parent_id INT,
	@season_id INT
AS
BEGIN

	SET NOCOUNT ON

	IF (@season_id > 1)
	 BEGIN

		SELECT ch.charge_id,
			ch.currency_unit_id,
			ch.charge_calculation_type_id,
			calctype.entity_name,
			ch.charge_name,
			ch.is_prorate_monthly, 
			ch.charge_unit_type_id,
			ch.charge_frequency_type_id,
			ch.charge_expression,
			ch.tier_factor,
			csm.rate,
			td.lower_bound,
			td.upper_bound
		FROM ((dbo.charge ch LEFT JOIN dbo.charge_season_map csm ON ch.charge_id=csm.charge_id)
			LEFT JOIN dbo.tier_detail td ON CSM.CHARGE_SEASON_MAP_ID = TD.CHARGE_SEASON_MAP_ID) 
			LEFT JOIN dbo.entity calctype ON ch.charge_calculation_type_id = calctype.entity_id
		WHERE ch.charge_master_id=@charge_master_id
			AND ch.charge_parent_id=@charge_parent_id
			AND (csm.season_id = @season_id or csm.season_id = 1)

	 END
	ELSE
	 BEGIN

		SELECT ch.charge_id,  
			ch.currency_unit_id, 
			ch.charge_calculation_type_id, 
			calctype.entity_name, 
			ch.charge_name, 
			ch.is_prorate_monthly,  
			ch.charge_unit_type_id, 
			ch.charge_frequency_type_id, 
			ch.charge_expression,  
			ch.tier_factor, 
			csm.rate, 
			td.lower_bound, td.upper_bound 
		FROM ((dbo.charge ch LEFT JOIN dbo.charge_season_map csm ON ch.charge_id=csm.charge_id)
			LEFT JOIN dbo.tier_detail td ON CSM.CHARGE_SEASON_MAP_ID = TD.CHARGE_SEASON_MAP_ID)
			LEFT JOIN dbo.entity calctype ON ch.charge_calculation_type_id = calctype.entity_id
		WHERE ch.charge_master_id=@charge_master_id
			AND ch.charge_parent_id=@charge_parent_id

	 END
		
END
GO
GRANT EXECUTE ON  [dbo].[GET_CHARGE_DETAILS_P] TO [CBMSApplication]
GO
