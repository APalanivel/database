SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

 NAME:  
	dbo.Group_Info_Category_Ins
 
 DESCRIPTION:  
 
	It will insert data into dbo.Group_Info_Category table for given values.
 
 INPUT PARAMETERS:  
 Name					DataType	  Default Description  
------------------------------------------------------------  
 @Category_Name			VARCHAR(50)
 @App_Menu_Profile_Id	INT
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  

  BEGIN TRAN
	EXEC dbo.Group_Info_Category_Ins 'Markets Test',4
  ROLLBACK TRAN

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath

 MODIFICATIONS:
 Initials Date			Modification
------------------------------------------------------------
 PNR	  03/31/2011	Created as part of Proj : DV groups Revamp.

******/

CREATE PROCEDURE dbo.Group_Info_Category_Ins
( 
	 @Category_Name			VARCHAR(50)
	,@App_Menu_Profile_Id	INT
)
AS
BEGIN

      SET NOCOUNT ON ;

	  DECLARE @Sort_Order INT
	  
	  SELECT
			@Sort_Order = MAX(Sort_Order) + 1
	  FROM
		    dbo.Group_Info_Category
	  WHERE
			App_Menu_Profile_Id = @App_Menu_Profile_Id

	  SET @Sort_Order = ISNULL(@Sort_Order, 1)

      INSERT INTO dbo.Group_Info_Category
	  (
			 Category_Name
			,APP_MENU_PROFILE_ID
			,Sort_Order
			
	   )
      VALUES
      (
			 @Category_Name
			,@App_Menu_Profile_Id
			,@Sort_Order
      )
      
	  SELECT SCOPE_IDENTITY() AS Group_Info_Category_Id

END
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Category_Ins] TO [CBMSApplication]
GO
