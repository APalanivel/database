SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
 NAME: dbo.Contact_Info_Sel_By_Keyword                  
                              
 DESCRIPTION:                              
   To get the details of Contact info                      
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
 @Contact_Info_Id             INT      
,@Contact_Level_Cd            INT      
,@Contact_Type_Cd             INT      
,@Client_Id                   INT      
,@Vendor_Id                   INT      
,@Is_Active                   INT      
,@Keyword                  VARCHAR(100)    
                                    
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
 --Client Level      
       
  EXEC [dbo].[Contact_Info_Sel_By_Keyword]       
      @Client_Id = 14525      
     ,@Is_Active = 1      
     ,@Contact_Level_Cd =102255      
      ,@Contact_Type_Cd =102256      
   ,@Keyword='s'      
            
--Vendor level      
      
  EXEC [dbo].[Contact_Info_Sel_By_Keyword]       
    @Vendor_Id = 14525      
      ,@Is_Active = 1      
      ,@Contact_Level_Cd =102255      
      ,@Contact_Type_Cd =102257      
   ,@Keyword  = 's'      
--Account level      
      
  EXEC [dbo].[Contact_Info_Sel_By_Keyword]       
    @Client_Id = 14525      
      ,@Is_Active = 1      
      ,@Contact_Level_Cd =102254      
      ,@Contact_Type_Cd =102256      
             
  EXEC [dbo].[Contact_Info_Sel_By_Keyword]         
    @Client_Id = 11554      
      ,@Is_Active = 1        
      ,@Contact_Level_Cd =102255        
      ,@Contact_Type_Cd =102256      
      ,@StartIndex = 2      
      ,@EndIndex=3      
  
     
   EXEC [dbo].[Contact_Info_Sel_By_Keyword]       
      @Client_Id ='102'   
   ,@Vendor_Id = 10000      
   ,@Contact_Level_Cd =102299        
      ,@Contact_Type_Cd =102328     
    ,@Is_Active = 1 ,       
      @StartIndex  = 1,  
    @EndIndex  = 2147483647,  
    @Total_Row_Count  = 0    
                                
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 SLP				   Sri Lakshmi Pallikonda             
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 SLP       2019-09-27       Created                     
                           
******/

CREATE PROCEDURE [dbo].[Contact_Info_Sel_By_Vendor_Multiple_Clients_Keyword]
     (
         @Contact_Info_Id INT = NULL
         , @Contact_Level_Cd INT
         , @Contact_Type_Cd INT
         , @Client_Id VARCHAR(MAX) = NULL
         , @Vendor_Id VARCHAR(MAX)  = NULL
         , @Is_Active INT
         , @StartIndex INT = 1
         , @EndIndex INT = 2147483647
         , @Total_Row_Count INT = 0
         , @Keyword VARCHAR(100) = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Client_Name VARCHAR(200)
            , @Vendor_Name VARCHAR(200);

        --SELECT
        --    @Vendor_Name = VENDOR_NAME
        --FROM
        --    dbo.VENDOR
        --WHERE
        --    VENDOR_ID = @Vendor_Id;

		SELECT
            c.VENDOR_ID
            , c.VENDOR_NAME
        INTO
            #Vendor_Name
        FROM
            dbo.VENDOR c
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            dbo.ufn_split(@Vendor_Id, ',') us
                       WHERE
                            us.Segments = c.Vendor_Id );
		
        SELECT
            c.CLIENT_NAME
            , c.CLIENT_ID
        INTO
            #Client_Name
        FROM
            dbo.CLIENT c
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            dbo.ufn_split(@Client_Id, ',') us
                       WHERE
                            us.Segments = c.CLIENT_ID);



        IF @Total_Row_Count = 0
            BEGIN
                SELECT
                    @Total_Row_Count = COUNT(1)
                FROM
                    dbo.Contact_Info ci
                WHERE
                    (   @Contact_Info_Id IS NULL
                        OR  ci.Contact_Info_Id = @Contact_Info_Id)
                    AND ci.Contact_Level_Cd = @Contact_Level_Cd
                    AND ci.Contact_Type_Cd = @Contact_Type_Cd
                    AND ci.Is_Active = @Is_Active
                    AND (EXISTS (   SELECT
                                        1
                                    FROM
                                        dbo.Client_Contact_Map cc
                                        INNER JOIN #Client_Name cn
                                            ON cn.CLIENT_ID = cc.CLIENT_ID
                                    WHERE
                                        cc.Contact_Info_Id = ci.Contact_Info_Id))
                    --OR  (EXISTS (   SELECT
                    --                    1
                    --                FROM
                    --                    dbo.Vendor_Contact_Map vc
                    --                WHERE
                    --                    vc.Contact_Info_Id = ci.Contact_Info_Id
                    --                    AND vc.VENDOR_ID = @Vendor_Id));
					OR  (EXISTS (   SELECT
                    1
                FROM
                    dbo.Vendor_Contact_Map vc
					JOIN #Vendor_Name vn
					on vc.Vendor_Id=vn.Vendor_Id
                WHERE
                    vc.Contact_Info_Id = ci.Contact_Info_Id
                    ));

            END;
        WITH CTE_Contacts_List
        AS (
               SELECT
                    ci.Contact_Info_Id
                    , ci.Email_Address
                    , ci.Fax_Number
                    , ci.Phone_Number
                    , ci.First_Name
                    , ci.Last_Name
                    , ci.Job_Position
                    , ci.Location
                    , c.Comment_Text
                    , c.Comment_ID
                    , ci.Language_Cd
                    , lc.Code_Dsc AS Language_Cd_Value
                    --@Client_Name AS Client_Name,  
                    , cn.CLIENT_NAME AS Client_Name
                   -- , @Vendor_Name AS Vendor_Name
				   ,vn.Vendor_Name AS Vendor_Name
                    , ROW_NUMBER() OVER (ORDER BY
                                             ci.Last_Name
                                             , ci.First_Name) Row_Num
                    , MAX(CASE WHEN icaa.Contact_Info_Id IS NULL THEN 0
                              ELSE 1
                          END) Is_Having_IC_References
               FROM
                    dbo.Contact_Info ci
                    LEFT JOIN dbo.Comment c
                        ON ci.Comment_Id = c.Comment_ID
                    LEFT JOIN dbo.Code lc
                        ON ci.Language_Cd = lc.Code_Id
                    LEFT JOIN dbo.Invoice_Collection_Account_Contact icaa
                        ON icaa.Contact_Info_Id = ci.Contact_Info_Id
                    LEFT OUTER JOIN Client_Contact_Map cc
                        ON cc.Contact_Info_Id = ci.Contact_Info_Id
                    LEFT OUTER JOIN #Client_Name cn
                        ON cn.CLIENT_ID = cc.CLIENT_ID
					LEFT OUTER JOIN Vendor_Contact_Map vm 
						on vm.Contact_Info_Id = ci.Contact_Info_Id
					LEFT OUTER JOIN  #Vendor_Name vn
						on vn.Vendor_Id=vm.Vendor_Id
               WHERE
                    (   @Contact_Info_Id IS NULL
                        OR  ci.Contact_Info_Id = @Contact_Info_Id)
                    AND ci.Contact_Level_Cd = @Contact_Level_Cd
                    AND ci.Contact_Type_Cd = @Contact_Type_Cd
                    AND ci.Is_Active = @Is_Active
                    AND (   @Keyword IS NULL
                            OR  ci.First_Name LIKE '%' + @Keyword + '%'
                            OR  ci.Last_Name LIKE '%' + @Keyword + '%'
                            OR  ci.Email_Address LIKE '%' + @Keyword + '%')
                    AND (EXISTS (   SELECT
                                        1
                                    FROM
                                        dbo.Client_Contact_Map cc
                                        INNER JOIN #Client_Name cn1
                                            ON cn1.CLIENT_ID = cc.CLIENT_ID
                                    WHERE
                                        cc.Contact_Info_Id = ci.Contact_Info_Id))
                   
					OR  (EXISTS (   SELECT
									1
								FROM
									dbo.Vendor_Contact_Map vc
									JOIN #Vendor_Name vn
									on vc.Vendor_Id=vn.Vendor_Id
								WHERE
									vc.Contact_Info_Id = ci.Contact_Info_Id
									))
               GROUP BY
                   ci.Contact_Info_Id
                   , ci.Email_Address
                   , ci.Fax_Number
                   , ci.Phone_Number
                   , ci.First_Name
                   , ci.Last_Name
                   , ci.Job_Position
                   , ci.Location
                   , c.Comment_Text
                   , c.Comment_ID
                   , ci.Language_Cd
                   , lc.Code_Dsc
                   , cn.CLIENT_NAME
				   ,vn.Vendor_Name 
           )
        SELECT  TOP 300
                cl.Contact_Info_Id
                , cl.Email_Address
                , cl.Fax_Number
                , cl.Phone_Number
                , cl.First_Name
                , cl.Last_Name
                , cl.Job_Position
                , cl.Location
                , cl.Comment_Text
                , cl.Comment_ID
                , cl.Language_Cd
                , cl.Language_Cd_Value
                , cl.Client_Name
                , cl.Vendor_Name
                , @Total_Row_Count AS Total_Row_Count
                , cl.Is_Having_IC_References
        FROM
            CTE_Contacts_List cl
        WHERE
            cl.Row_Num BETWEEN @StartIndex
                       AND     @EndIndex
        ORDER BY
            cl.Last_Name
            , cl.First_Name;
		drop table #Vendor_Name, #Client_Name
    END;

GO
GRANT EXECUTE ON  [dbo].[Contact_Info_Sel_By_Vendor_Multiple_Clients_Keyword] TO [CBMSApplication]
GO
