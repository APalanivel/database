SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-18-07
--delta insert for dv2_ip
--
create procedure dbo.DV2GetIPDeltaInserts
as
begin
set nocount on

INSERT INTO [dbo].[DV2_IP]([DIVISION_ID], [SITE_ID], [SITE_NOT_MANAGED], [VENDOR_TYPE_ID], [VENDOR_ID], [ACCOUNT_ID], [COMMODITY_TYPE_ID], 
[COUNTRY_ID], [STATE_ID], [SERVICE_MONTH_ID], [IS_EXPECTED], [IS_RECEIVED], [INVOICE_COUNT], [CBMS_IMAGE_ID])
SELECT b.[DIVISION_ID], b.[SITE_ID], b.[SITE_NOT_MANAGED], b.[VENDOR_TYPE_ID], b.[VENDOR_ID], b.[ACCOUNT_ID], b.[COMMODITY_TYPE_ID], 
b.[COUNTRY_ID], b.[STATE_ID], b.[SERVICE_MONTH_ID], b.[IS_EXPECTED], b.[IS_RECEIVED], b.[INVOICE_COUNT], b.[CBMS_IMAGE_ID] 
FROM tempdb.[dbo].[DV2_IP_DELTA] b
left outer join dbo.dv2_ip a
on a.site_id=b.site_id and a.account_id=b.account_id and a.commodity_type_id=b.commodity_type_id and
a.service_month_id=b.service_month_id
where a.site_id is null

end
GO
GRANT EXECUTE ON  [dbo].[DV2GetIPDeltaInserts] TO [CBMSApplication]
GO
