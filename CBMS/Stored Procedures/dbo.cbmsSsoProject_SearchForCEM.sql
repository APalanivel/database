SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
NAME:
	dbo.cbmsSsoProject_SearchForCEM

DESCRIPTION:


INPUT PARAMETERS:
	Name						DataType	Default	Description
------------------------------------------------------------
	@MyAccountId   				int       	          	
	@commodity_type_id			int       	null      	
	@project_status_type_id		int			null      	
	@project_ownership_type_id	int       	null      	
	@client_id     				int       	null      	
	@division_id   				int       	null      	
	@site_id       				int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.cbmsSsoProject_SearchForCEM 10
EXEC dbo.cbmsSsoProject_SearchForCEM @MyAccountId = 10, @Client_Id = 1024, @division_id = 39754

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Eshwar Panduga
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	CPE			03/28/2011	Replaced vwCbmsSSOProjectOwnerFlat with SSO_PROJECT_OWNER_MAP table. 
							Replaced vwSsoProject_LastActivity with the base tables.
******/

CREATE PROCEDURE dbo.cbmsSsoProject_SearchForCEM
( 
 @MyAccountId int
,@commodity_type_id int = null
,@project_status_type_id int = null
,@project_ownership_type_id int = null
,@client_id int = null
,@division_id int = null
,@site_id int = null )
AS 
BEGIN

      SET NOCOUNT ON

      exec cbmsSecurity_GetClientAccess @MyAccountId, @client_id output, @division_id output, @site_id output

      SELECT DISTINCT
            SP.SSO_PROJECT_ID
           ,acc.owner_id
           ,acc.owner_name
           ,acc.owner_sort
           ,SP.PROJECT_TITLE
           ,SP.PROJECT_DESCRIPTION
           ,SP.PROJECT_STATUS_TYPE_ID
           ,PST.ENTITY_NAME Project_Status_Type
           ,SP.COMMODITY_TYPE_ID
           ,COM.ENTITY_NAME commodity_type
           ,SP.PROJECTED_END_DATE
           ,SP.PROJECT_OWNERSHIP_TYPE_ID
           ,POT.ENTITY_NAME project_ownership_type
           ,SP.IS_URGENT
           ,acc.Client_Name
           ,isNull(convert(varchar(14), PA.ACTIVITY_DATE, 101), '') + isNull(' - ' + PA.ACTIVITY_DESCRIPTION, SP.PROJECT_DESCRIPTION) last_activity
      from
            dbo.SSO_PROJECT SP
            JOIN dbo.ENTITY PST
                  on PST.ENTITY_ID = SP.PROJECT_STATUS_TYPE_ID
            JOIN ENTITY POT
                  on POT.ENTITY_ID = SP.PROJECT_OWNERSHIP_TYPE_ID
            JOIN ( SELECT DISTINCT
                        SSO_PROJECT_ID
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Id
                          WHEN 'Division' THEN CH.Sitegroup_Id
                          WHEN 'Site' THEN CH.Site_Id
                        END AS Owner_Id
                       ,CASE CD.Code_Value
                          WHEN 'Corporate' THEN CH.Client_Name
                          WHEN 'Division' THEN CH.Sitegroup_Name
                          WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
                        END AS Owner_Name
                       ,Cd.Display_Seq AS Owner_Sort
                       ,CH.Client_Id
                       ,CH.Client_Name
                   FROM
                        SSO_PROJECT_OWNER_MAP SPOM
                        JOIN Core.Client_Hier CH
                              ON SPOM.Client_Hier_Id = CH.Client_Hier_Id
                        JOIN dbo.Code Cd
                              ON CH.Hier_level_Cd = Cd.Code_Id
                   WHERE
                        ( @client_id IS NULL
                          OR @client_id = CH.Client_Id )
                        AND ( @division_id IS NULL
                              OR @division_id = CH.Sitegroup_Id )
                        AND ( @site_id IS NULL
                              OR @Site_Id = CH.Site_Id )
                        AND CH.Client_Not_Managed = 0 ) acc
                  on acc.SSO_PROJECT_ID = SP.SSO_PROJECT_ID
            JOIN dbo.CLIENT_CEM_MAP CCM
                  on ( CCM.CLIENT_ID = acc.Client_Id
                       and CCM.USER_INFO_ID = @MyAccountId )
            LEFT JOIN dbo.ENTITY COM
                  on COM.ENTITY_ID = SP.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              SSO_PROJECT_ID
                             ,MAX(SSO_PROJECT_ACTIVITY_ID) SSO_PROJECT_ACTIVITY_ID
                        FROM
                              dbo.SSO_PROJECT_ACTIVITY
                        GROUP BY
                              SSO_PROJECT_ID ) SPA
                  ON SP.SSO_PROJECT_ID = SPA.SSO_PROJECT_ID
            LEFT JOIN dbo.SSO_PROJECT_ACTIVITY PA
                  ON PA.SSO_PROJECT_ACTIVITY_ID = SPA.SSO_PROJECT_ACTIVITY_ID
      WHERE
            ( @commodity_type_id IS NULL
              OR @commodity_type_id = SP.COMMODITY_TYPE_ID )
            AND ( @project_status_type_id IS NULL
                  OR @project_status_type_id = SP.PROJECT_STATUS_TYPE_ID )
            AND ( @project_ownership_type_id IS NULL
                  OR @project_ownership_type_id = SP.PROJECT_OWNERSHIP_TYPE_ID )
      ORDER BY
            PST.ENTITY_NAME DESC
           ,SP.IS_URGENT DESC
           ,acc.owner_name


END


GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_SearchForCEM] TO [CBMSApplication]
GO
