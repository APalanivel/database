SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsSessionActivity_Save]
	( @session_info_id int
	, @page_url varchar(4000)
	)
AS
BEGIN

	set nocount on

	insert into session_activity 
		( session_info_id
		, access_date
		, page_url
		)
	values
		( @session_info_id
		, getdate()
		, @page_url
		)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSessionActivity_Save] TO [CBMSApplication]
GO
