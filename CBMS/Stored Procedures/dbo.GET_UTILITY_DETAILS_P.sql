
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.GET_UTILITY_DETAILS_P    

DESCRIPTION:  This procedure used to get the records in UTILITY_DETAILS table

INPUT PARAMETERS:    
      Name                        DataType          Default     Description    
---------------------------------------------------------------------------    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@vendorId			INT
    
USAGE EXAMPLES:    
------------------------------------------------------------    
	EXEC GET_UTILITY_DETAILS_P 7

Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
			8/4/2009	Removed Linked Server Updates
	SKA		01/21/2011	Modifed as we removed few columns from table
            
******/
  
CREATE PROCEDURE [dbo].[GET_UTILITY_DETAILS_P] @vendorId INT
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      SELECT
            utdet.utility_detail_id
           ,vnd.vendor_id
           ,utdet.website_url
           ,utdet.main_contact
           ,utdet.rate_contact
           ,utdet.billing_contact
           ,utdet.pipeline
           ,utdet.utility_comments
           ,utdet.Data_Contact
           ,vnd.vendor_name
      FROM
            dbo.VENDOR vnd
            JOIN dbo.utility_detail utdet
                  ON vnd.vendor_id = utdet.vendor_id
      WHERE
            vnd.vendor_id = @vendorId  
  
END  







  
GO

GRANT EXECUTE ON  [dbo].[GET_UTILITY_DETAILS_P] TO [CBMSApplication]
GO
