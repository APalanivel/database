SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_DELETE_CLIENT_CEA_MAP_P]


DESCRIPTION: Deletes data from the CLIENT_CEA_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@clientId                int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.CBMS_DELETE_CLIENT_CEA_MAP_P @clientId = 11471




AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE dbo.CBMS_DELETE_CLIENT_CEA_MAP_P
@clientId int

AS



DELETE 
	CLIENT_CEA_MAP 
WHERE 
	client_id=@clientId
GO
GRANT EXECUTE ON  [dbo].[CBMS_DELETE_CLIENT_CEA_MAP_P] TO [CBMSApplication]
GO
