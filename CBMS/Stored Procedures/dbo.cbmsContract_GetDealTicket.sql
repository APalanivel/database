SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsContract_GetDealTicket

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@contract_number	varchar(50)	          	
	@change_start_date	datetime  	          	
	@change_end_date	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsContract_GetDealTicket]
(
	@MyAccountId int
	,@contract_number varchar(50)
	,@change_start_date datetime
	,@change_end_date datetime
)
as
declare @current_start_date datetime
declare @current_end_date datetime

select @current_start_date = contract_start_date
	,@current_end_date = contract_end_date
from contract
where ed_contract_number = @contract_number-- contract_id = @contract_id

declare @date1 datetime
declare @date2 datetime
declare @is_removing_months bit

set @is_removing_months = 0

--establish the date range based on whether or not a start or end date is provided
--and based on the start and end date of the contract
if @change_start_date is null
	begin
	if @change_end_date < @current_end_date
		begin
		set @date1 = @change_end_date
		set @date2 = @current_end_date
	
		set @is_removing_months = 1
		end
	else --@change_end_date > @current_end_date
		begin
		set @date1 = @current_end_date
		set @date2 = @change_end_date
		end
	end
else --@change_end_date is null
	begin
	if @change_start_date < @current_start_date
		begin
		set @date1 = @change_start_date
		set @date2 = @current_start_date
		end
	else --@change_start_date > @current_start_date
		begin
		set @date1 = @current_start_date
		set @date2 = @change_start_date

		set @is_removing_months = 1
		end
	end

declare @contract_id int
select @contract_id = contract_id
from contract
where ed_contract_number = @contract_number

select dt.rm_deal_ticket_id
from rm_deal_ticket dt 
where dt.contract_id = @contract_id
and dt.hedge_start_month >= @date1
and dt.hedge_end_month <= @date2
GO
GRANT EXECUTE ON  [dbo].[cbmsContract_GetDealTicket] TO [CBMSApplication]
GO
