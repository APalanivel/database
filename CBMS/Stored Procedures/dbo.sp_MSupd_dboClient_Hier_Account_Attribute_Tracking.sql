SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[sp_MSupd_dboClient_Hier_Account_Attribute_Tracking]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 date = NULL,
		@c7 date = NULL,
		@c8 nvarchar(255) = NULL,
		@c9 int = NULL,
		@c10 datetime = NULL,
		@c11 int = NULL,
		@c12 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(2)
as
begin  
update [dbo].[Client_Hier_Account_Attribute_Tracking] set
		[Client_Hier_Id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Client_Hier_Id] end,
		[Account_Id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Account_Id] end,
		[Commodity_Id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Commodity_Id] end,
		[Client_Attribute_Id] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Client_Attribute_Id] end,
		[Start_Dt] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Start_Dt] end,
		[End_Dt] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [End_Dt] end,
		[Attribute_Value] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Attribute_Value] end,
		[Created_User_Id] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Created_User_Id] end,
		[Created_Ts] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Created_Ts] end,
		[Updated_User_Id] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Updated_User_Id] end,
		[Last_Change_Ts] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [Last_Change_Ts] end
where [Client_Hier_Account_Attribute_Tracking_Id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end 
GO
