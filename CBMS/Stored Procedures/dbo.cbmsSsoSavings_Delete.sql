SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoSavings_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_savings_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  procedure [dbo].[cbmsSsoSavings_Delete]
	( @MyAccountId int
	, @sso_savings_id int
	)
AS
BEGIN

	set nocount on

	delete sso_savings_owner_map where sso_savings_id = @sso_savings_id
	delete sso_savings_detail where sso_savings_id = @sso_savings_id
	delete sso_savings where sso_savings_id = @sso_savings_id

	exec cbmsEntityAudit_Log @MyAccountId, 'sso_savings', @sso_savings_id, 'Delete'

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavings_Delete] TO [CBMSApplication]
GO
