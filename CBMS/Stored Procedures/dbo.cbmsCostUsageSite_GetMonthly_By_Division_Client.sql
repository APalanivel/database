SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
 dbo.[cbmsCostUsageSite_GetMonthly_By_Division_Client]    
    
DESCRIPTION:    
    This SP is used by CU Monthly Report Page
    
INPUT PARAMETERS:    
    Name					  DataType    Default Description    
------------------------------------------------------------    
    @currency_unit_id		  int    
    @el_unit_of_measure_type_id int    
    @ng_unit_of_measure_type_id int    
    @report_year			  int    
    @client_id				  int	    null    
    @division_id			  int	    null    
    @country_id			  int	    null    
    @not_managed			  bit	    null                  
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------

     EXEC dbo.cbmsCostUsageSite_GetMonthly_By_Division_Client 
      @currency_unit_id = 3
     ,@el_unit_of_measure_type_id = 12
     ,@ng_unit_of_measure_type_id = 25
     ,@report_year = 2010
     ,@client_id = 10069

     EXEC cbmsCostUsageSite_GetMonthly_By_Division_Client 
      3
     ,25
     ,23
     ,2010
     ,NULL
     ,NULL
     ,NULL
     ,NULL

    
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
 AP			Athmaram Pabbathi   
MODIFICATIONS    
    
 Initials	Date		    Modification    
------------------------------------------------------------    
 AP		2012-01-30    Created to replace cbmsCostUsageSite_GetMonthly SP only for returning Division & Client level data.
******/
CREATE PROCEDURE dbo.cbmsCostUsageSite_GetMonthly_By_Division_Client
      ( 
       @currency_unit_id INT
      ,@el_unit_of_measure_type_id INT
      ,@ng_unit_of_measure_type_id INT
      ,@report_year INT
      ,@client_id INT = NULL
      ,@division_id INT = NULL
      ,@country_id INT = NULL
      ,@not_managed BIT = NULL )
AS 
BEGIN    
      SET NOCOUNT ON    
    
      DECLARE
            @Begin_Dt DATE
           ,@End_Dt DATE     
                
      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Month_Num SMALLINT )      
  
      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Num )
                  SELECT
                        dd.Date_D
                       ,row_number() OVER ( ORDER BY dd.Date_D )
                  FROM
                        ( SELECT
                              dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                          FROM
                              Core.Client_Hier ch
                          WHERE
                              ch.Client_ID = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0
                          UNION ALL
                          SELECT
                              cast('1/1/' + cast(@report_year AS VARCHAR(4)) AS VARCHAR(10))
                          WHERE
                              @client_id IS NULL ) X
                        CROSS JOIN meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN x.Start_month
                                  AND     dateadd(MONTH, -1, ( dateadd(YEAR, 1, X.Start_Month) ))

      SELECT
            @Begin_Dt = min(Service_Month)
           ,@End_dt = max(Service_Month)
      FROM
            @Service_Month
    
      SELECT
            m.service_month
           ,isnull(x.ng_is_complete, 0) ng_is_complete
           ,isnull(x.ng_is_published, 0) ng_is_published
           ,isnull(x.ng_under_review, 0) ng_under_review
           ,isnull(x.el_is_complete, 0) el_is_complete
           ,isnull(x.el_is_published, 0) el_is_published
           ,isnull(x.el_under_review, 0) el_under_review
           ,isnull(x.el_usage, 0) el_usage
           ,isnull(x.ng_usage, 0) ng_usage
           ,isnull(x.el_cost, 0) el_cost
           ,isnull(x.ng_cost, 0) ng_cost
           ,convert(DECIMAL(32, 16), 0) el_demand
           ,convert(DECIMAL(32, 16), 0) el_unit_cost
           ,convert(DECIMAL(32, 16), 0) ng_unit_cost
           ,convert(INT, 0) cbms_image_id
      FROM
            @Service_Month m
            LEFT OUTER JOIN ( SELECT
                                    cu.service_month
                                   ,ng_is_complete = min(convert(INT, ip.ng_is_complete))
                                   ,ng_is_published = max(convert(INT, ip.ng_is_published))
                                   ,ng_under_review = max(convert(INT, ip.ng_under_review))
                                   ,el_is_complete = min(convert(INT, ip.el_is_complete))
                                   ,el_is_published = max(convert(INT, ip.el_is_published))
                                   ,el_under_review = max(convert(INT, ip.el_under_review))
                                   ,el_usage = sum(convert(DECIMAL(32, 16), isnull(cu.el_usage, 0)) * cconel.conversion_factor)
                                   ,ng_usage = sum(convert(DECIMAL(32, 16), isnull(cu.ng_usage, 0)) * cconng.conversion_factor)
                                   ,el_cost = sum(convert(DECIMAL(32, 16), isnull(cu.el_cost, 0)) * cuc.conversion_factor)
                                   ,ng_cost = sum(convert(DECIMAL(32, 16), isnull(cu.ng_cost, 0)) * cuc.conversion_factor)
                                   ,el_demand = sum(convert(DECIMAL(32, 16), isnull(cu.el_demand, 0)))
                              FROM
                                    core.Client_Hier ch
                                    JOIN dbo.cost_usage_site cu
                                          ON ( cu.site_id = ch.site_id
                                               AND cu.is_default = 1
                                               AND cu.SYSTEM_ROW = 0 )
                                    LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                                          ON ( cuc.currency_group_id = ch.Client_Currency_Group_Id
                                               AND cuc.base_unit_id = cu.currency_unit_id
                                               AND cuc.converted_unit_id = @currency_unit_id
                                               AND cuc.conversion_date = cu.service_month )
                                    JOIN dbo.CONSUMPTION_UNIT_CONVERSION cconel
                                          ON cconel.base_unit_id = cu.el_unit_of_measure_type_id
                                             AND cconel.converted_unit_id = @el_unit_of_measure_type_id
                                    JOIN dbo.CONSUMPTION_UNIT_CONVERSION cconng
                                          ON cconng.base_unit_id = cu.ng_unit_of_measure_type_id
                                             AND cconng.converted_unit_id = @ng_unit_of_measure_type_id
                                    JOIN dbo.INVOICE_PARTICIPATION_SITE ip
                                          ON ip.site_id = ch.site_id
                                             AND ip.service_month = cu.service_month
                              WHERE
                                    ( ch.client_id = @client_id )
                                    AND ( @division_id IS NULL
                                          OR ch.Sitegroup_Id = @division_id )
                                    AND ( @Country_Id IS NULL
                                          OR ch.country_id = @country_id )
                                    AND ( cu.service_month BETWEEN @Begin_Dt AND @End_Dt )
                                    AND ( @not_managed IS NULL
                                          OR ch.Site_Not_Managed = @not_managed )
                              GROUP BY
                                    cu.service_month ) x
                  ON x.service_month = m.service_month
      ORDER BY
            m.service_month

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetMonthly_By_Division_Client] TO [CBMSApplication]
GO
