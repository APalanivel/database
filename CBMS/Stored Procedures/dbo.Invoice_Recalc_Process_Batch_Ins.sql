SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	[dbo].[Invoice_Recalc_Process_Batch_Ins]   

DESCRIPTION:     

	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@User_Info_Id					 	INT      

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
---------------------------------------------------------------------------- 

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Pending'

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Error'


EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'Request Status'
    , @Code_Value = 'Completed'
 
declare @tvp_Invoice_Recalc_Process tvp_Invoice_Recalc_Process

insert into @tvp_Invoice_Recalc_Process values (10, 100429)

EXEc dbo.Invoice_Recalc_Process_Batch_Ins
    @tvp_Invoice_Recalc_Process = @tvp_Invoice_Recalc_Process
    , @User_Info_Id = 49

Select * from dbo.Invoice_Recalc_Process_Batch
Select * from dbo.Invoice_Recalc_Process_Batch_Dtl


AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
-----------------------------------------------------------------------------    
NR			2019-09-03		Add Contract -  Created.
******/

CREATE PROCEDURE [dbo].[Invoice_Recalc_Process_Batch_Ins]
    (
        @tvp_Invoice_Recalc_Process tvp_Invoice_Recalc_Process READONLY
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Invoice_Recalc_Process_Batch_Id INT
            , @Pending_Status_Cd INT
            , @Error_Status_Cd INT;


        SELECT
            @Pending_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Pending' THEN c.Code_Id
                                         ELSE 0
                                     END)
            , @Error_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Error' THEN c.Code_Id
                                         ELSE 0
                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Request Status'
            AND c.Code_Value IN ( 'Pending', 'Error' );

        IF EXISTS (SELECT   1 FROM  @tvp_Invoice_Recalc_Process)
            BEGIN


                INSERT INTO dbo.Invoice_Recalc_Process_Batch
                     (
                         Status_Cd
                         , Created_User_Id
                         , Created_Ts
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                VALUES
                    (@Pending_Status_Cd
                     , @User_Info_Id
                     , GETDATE()
                     , @User_Info_Id
                     , GETDATE());

                SET @Invoice_Recalc_Process_Batch_Id = SCOPE_IDENTITY();


                UPDATE
                    irp
                SET
                    Invoice_Recalc_Process_Batch_Id = @Invoice_Recalc_Process_Batch_Id
                    , Status_Cd = @Pending_Status_Cd
                    , Error_Dsc = NULL
                FROM
                    Invoice_Recalc_Process_Batch_Dtl irp
                WHERE
                    irp.Status_Cd = @Error_Status_Cd
                    AND irp.Error_Dsc IS NOT NULL;




                INSERT INTO dbo.Invoice_Recalc_Process_Batch_Dtl
                     (
                         Invoice_Recalc_Process_Batch_Id
                         , Cu_Invoice_Id
                         , Status_Cd
                         , Created_User_Id
                         , Created_Ts
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                SELECT
                    @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id
                    , tvp.Cu_Invoice_Id
                    , tvp.Status_Cd
                    , @User_Info_Id
                    , GETDATE()
                    , @User_Info_Id
                    , GETDATE()
                FROM
                    @tvp_Invoice_Recalc_Process tvp;


            END;

        SELECT  @Invoice_Recalc_Process_Batch_Id AS Invoice_Recalc_Process_Batch_Id;

    END;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Recalc_Process_Batch_Ins] TO [CBMSApplication]
GO
