SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Service_Broker_Message_Monitor_Send

DESCRIPTION:
	procedure will send XML Message to Service_Broker_Message_Monitor_Queue


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	,@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			06/03/2015		Created
******/
CREATE PROCEDURE [dbo].[Service_Broker_Message_Monitor_Send]
      ( 
       @Queue_Name NVARCHAR(200)
      ,@Message_Type NVARCHAR(200)
      ,@Message_Received_Ts DATETIME
      ,@Message_Sequence_Number INT
      ,@Conversation_Group_Id UNIQUEIDENTIFIER
      ,@Conversation_Handle UNIQUEIDENTIFIER
      ,@Service_name NVARCHAR(200)
      ,@Contract_name NVARCHAR(200)
      ,@Error_Text VARCHAR(MAX)
      ,@Message_Body XML
      ,@Message_Status VARCHAR(200)
      ,@Message_Completed_Ts DATETIME
      ,@Message_Batch_Count INT
      ,@op_Code CHAR(1) )
AS 
BEGIN
      SET NOCOUNT ON; 

      DECLARE @Message XML
      DECLARE @ch UNIQUEIDENTIFIER

      SET @Message = ( SELECT
                        @Queue_Name Queue_Name
                       ,@Message_Type Message_Type
                       ,@Message_Received_Ts Message_Received_Ts
                       ,@Message_Sequence_Number Message_Sequence_Number
                       ,@Conversation_Group_Id Conversation_Group_Id
                       ,@Conversation_Handle Conversation_Handle
                       ,@Service_name Service_name
                       ,@Contract_name Contract_name
                       ,@Error_Text Error_Text
                       ,convert(VARCHAR(MAX), @Message_Body) Message_Body
                       ,@Message_Status Message_Status
                       ,@Message_Completed_Ts Message_Completed_Ts
                       ,@Message_Batch_Count Message_Batch_Count
            FOR
                       XML PATH('') );

       ;
      BEGIN DIALOG CONVERSATION @ch
		FROM SERVICE [//Change_Control/Service/CBMS/SB_Message_Monitor]    
		TO SERVICE '//Change_Control/Service/CBMS/SB_Message_Monitor'   
		ON CONTRACT [//Change_Control/Contract/SB_Message_Monitor];  

      IF @op_Code = 'I' 
            BEGIN            
/*
<Message_Monitor_Ins>
	<Queue_Name></Queue_Name>
	<Message_Type></Message_Type>
	<Message_Received_Ts></Message_Received_Ts>
	<Message_Sequence_Number></Message_Sequence_Number>
	<Conversation_Group_Id></Conversation_Group_Id>
	<Conversation_Handle></Conversation_Handle>
	<Service_name></Service_name>
	<Contract_name></ontract_name>
	<Message_Body></Message_Body>
	<Message_Status></Message_Status>
</Message_Monitor_Ins>
*/					
                  SET @Message = '<Message_Monitor_Ins>' + convert(VARCHAR(MAX), @Message) + '</Message_Monitor_Ins>';
                  SEND ON CONVERSATION @ch MESSAGE TYPE [//Change_Control/Message/SB_Message_Monitor_Ins] (@Message)
            END
      ELSE 
            IF @op_Code = 'U' 
                  BEGIN
/*
<Message_Monitor_Upd>
	<Queue_Name></Queue_Name>
	<Message_Type></Message_Type>
	<Conversation_Group_Id></Conversation_Group_Id>
	<Message_Status></Message_Status>
	<Message_Completed_Ts></Message_Completed_Ts>
	<Error_Text></Error_Text>
	<Message_Batch_Count></Message_Batch_Count>
</Message_Monitor_Upd>
*/
                        SET @Message = '<Message_Monitor_Upd>' + convert(VARCHAR(MAX), @Message) + '</Message_Monitor_Upd>';
                        SEND ON CONVERSATION @ch MESSAGE TYPE [//Change_Control/Message/SB_Message_Monitor_Upd] (@Message)
                  END
	
END
;
GO
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Send] TO [ETL_Execute]
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Send] TO [sb_Execute]
GO
