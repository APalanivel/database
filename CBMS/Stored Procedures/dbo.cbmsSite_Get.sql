
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsSite_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.CbmsSite_Get 16974

	SELECT TOP 10 * FROM Core.Client_Hier WHERE Client_Id = 10069 AND Site_id >0

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	HG			2012-04-10	Client_Hier_Id coulmn added in the select statement as per Additional data enhancement requirement	        	
							-- Division, Client and vwSitename replaced by Core.Client_Hier
							-- Unused @MyAccountId parameter removed
******/

CREATE PROCEDURE [dbo].[cbmsSite_Get] 
( @site_id INT )
AS 
BEGIN
	
	SET NOCOUNT ON
	
      SELECT
            s.site_id
           ,s.site_type_id
           ,st.entity_name AS site_type
           ,s.site_name
           ,s.division_id
           ,s.client_id
           ,ch.Sitegroup_Name AS division_name
           ,s.ubmsite_id
           ,s.primary_address_id
           ,s.site_reference_number
           ,s.site_product_service
           ,s.production_schedule
           ,s.shutdown_schedule
           ,s.is_alternate_power
           ,s.is_alternate_gas
           ,s.doing_business_as
           ,s.naics_code
           ,s.tax_number
           ,s.duns_number
           ,s.is_history
           ,s.history_reason_type_id
           ,hr.entity_name history_reason_type
           ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS site_label
           ,ch.client_name
           ,s.not_managed
           ,ch.Client_Hier_Id
      FROM
            dbo.Site s
            JOIN core.Client_Hier ch
                  ON ch.Site_Id = s.Site_Id
            LEFT OUTER JOIN dbo.entity st
                  ON st.entity_id = s.site_type_id
            LEFT OUTER JOIN dbo.entity hr
                  ON hr.entity_id = s.history_reason_type_id
      WHERE
            s.site_id = @site_id

END

;
GO

GRANT EXECUTE ON  [dbo].[cbmsSite_Get] TO [CBMSApplication]
GO
