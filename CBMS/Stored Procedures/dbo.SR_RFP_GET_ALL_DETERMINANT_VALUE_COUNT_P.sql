SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC dbo.SR_RFP_GET_ALL_DETERMINANT_VALUE_COUNT_P 225
CREATE  PROCEDURE dbo.SR_RFP_GET_ALL_DETERMINANT_VALUE_COUNT_P
	@rfp_id int
	AS
	set nocount on
		select  rfp_account.sr_rfp_account_id, 
			determinant.determinant_name,
			determinant.sr_rfp_load_profile_determinant_id as determinant_id,
			count(value.sr_rfp_lp_determinant_values_id) as actual_count

		from 	sr_rfp_account rfp_account (nolock)
			join sr_rfp_load_profile_setup setup(nolock) on setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
			and rfp_account.sr_rfp_id = @rfp_id and rfp_account.is_deleted = 0
			join sr_rfp_load_profile_determinant determinant(nolock) on determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
			and determinant.is_checked = 1
			left join sr_rfp_lp_determinant_values value (nolock)
			on value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
			and value.lp_value is not null
			and value.reading_type_id in(1160, 1161)

		group by rfp_account.sr_rfp_account_id,  
			determinant.determinant_name,
			determinant.sr_rfp_load_profile_determinant_id
		
		order by determinant.sr_rfp_load_profile_determinant_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_DETERMINANT_VALUE_COUNT_P] TO [CBMSApplication]
GO
