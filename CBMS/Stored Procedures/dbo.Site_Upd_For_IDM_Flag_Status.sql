SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [dbo].[Site_Upd_For_IDM_Flag_Status]
     
DESCRIPTION: 
	procedure to update IDM Node flag
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
 @SiteIDMInfo AS dbo.IDM_Site_Info
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

     declare @SiteIDMInfo AS dbo.IDM_Site_Info
	 insert into @SiteIDMInfo([Client_Hier_Id],[Is_IDM_Enabled])
	 values(3703,1)
	 exec [dbo].[Site_Upd_For_IDM_Flag_Status] 

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
	KVK		12/14/2014	created
*/
CREATE PROCEDURE [dbo].[Site_Upd_For_IDM_Flag_Status]
      ( 
       @tvp_Client_Hier_Id AS dbo.IDM_Site_Info READONLY )
AS 
BEGIN
  
 
      INSERT      INTO dbo.Site_IDM_Queue
                  ( 
                   Client_Hier_Id )
                  SELECT
                        tvp_s.Client_Hier_Id
                  FROM
                        @tvp_Client_Hier_Id tvp_s
                  WHERE
                        tvp_s.Is_IDM_Enabled = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Site_IDM_Queue q
                                         WHERE
                                          q.Client_Hier_Id = tvp_s.Client_Hier_Id )


      DELETE
            siq
      FROM
            dbo.Site_IDM_Queue siq
            JOIN @tvp_Client_Hier_Id tvp_s
                  ON siq.Client_Hier_Id = tvp_s.Client_Hier_Id
      WHERE
            tvp_s.Is_IDM_Enabled = 0

  
      UPDATE
            s
      SET   
            Is_IDM_Enabled = tvp_s.Is_IDM_Enabled
      FROM
            dbo.SITE s
            INNER JOIN core.Client_Hier ch
                  ON ch.Site_Id = s.SITE_ID
            INNER JOIN @tvp_Client_Hier_Id tvp_s
                  ON ch.Client_Hier_Id = tvp_s.Client_Hier_Id
      WHERE
            s.Is_IDM_Enabled <> tvp_s.Is_IDM_Enabled


END  
;
GO
GRANT EXECUTE ON  [dbo].[Site_Upd_For_IDM_Flag_Status] TO [CBMSApplication]
GO
