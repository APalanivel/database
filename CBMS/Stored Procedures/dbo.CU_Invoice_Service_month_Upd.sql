SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********         
NAME:  dbo.CU_Invoice_Service_month_Upd
       
DESCRIPTION:  This will update data when resolve month for account  
      
INPUT PARAMETERS:          
 Name              DataType          Default     Description          
------------------------------------------------------------          
 @Account_Id		int  
 @Cu_Invoice_Id		int  
 @Begin_Dt			date  
 @End_Dt			date  
 @Billing_days		int     
          
OUTPUT PARAMETERS:          
 Name              DataType          Default     Description          
------------------------------------------------------------          
         
USAGE EXAMPLES:   
USE CBMS_TK3
	EXEC CU_Invoice_Service_month_Upd 35415,2926819,'2010-01-02','2010-11-11',313
	  
-----------------------------------------------------------        

AUTHOR INITIALS:        
Initials	 Name
------------------------------------------------------------        
SSR			Sharad Srivastava

Initials	Date		Modification        
------------------------------------------------------------        
SSR			03/24/2010	Created
******/        

CREATE PROCEDURE dbo.CU_Invoice_Service_month_Upd
	   @Account_Id		AS INT  
	 , @Cu_Invoice_Id	AS INT  
	 , @Begin_Dt		AS DATE  
	 , @End_Dt			AS DATE  
	 , @Billing_days	AS INT  
AS   
BEGIN  

    SET  NOCOUNT ON;
      
    UPDATE dbo.CU_INVOICE_SERVICE_MONTH
    SET  Begin_Dt = @Begin_Dt  
        , End_Dt = @End_Dt  
        , Billing_Days = @Billing_days  
    WHERE ACCOUNT_ID = @Account_Id  
		AND CU_INVOICE_ID = @Cu_Invoice_Id  
                                         
END  
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_month_Upd] TO [CBMSApplication]
GO
