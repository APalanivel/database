SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.Sitegroup_User_Global_Sel_By_Client_User                    
                      
Description:                      
        To Get SiteGroups.                      
                      
 Input Parameters:                      
    Name            DataType        Default     Description                        
------------------------------------------------------------------------------------------------------                        
 @User_Info_Id      INT    
 @Client_Id         INT     
                      
 Output Parameters:                            
 Name               Datatype        Default		Description                            
------------------------------------------------------------------------------------------------------                        
                      
 Usage Examples:                          
------------------------------------------------------------------------------------------------------                        
 EXEC dbo.Sitegroup_User_Global_Sel_By_Client_User 49,235    
 EXEC dbo.Sitegroup_User_Global_Sel_By_Client_User 49,110        
 EXEC dbo.Sitegroup_User_Global_Sel_By_Client_User 49,11231
 EXEC dbo.Sitegroup_User_Global_Sel_By_Client_User 20092,11231
                     
 Author Initials:                      
      Initials       Name                      
------------------------------------------------------------------------------------------------------                        
       SP            Sandeep Pigilam        
                       
 Modifications:                      
      Initials       Date           Modification                      
------------------------------------------------------------------------------------------------------                        
       SP            2014-01-21     Created                    
                     
******/       
CREATE PROCEDURE [dbo].[Sitegroup_User_Global_Sel_By_Client_User]
      ( 
       @User_Info_Id INT
      ,@Client_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;    
      
      DECLARE @Tbl_Groups TABLE
            ( 
             Sitegroup_Name VARCHAR(200)
            ,Sitegroup_Id INT
            ,Sitegroup_Type VARCHAR(25) )

      INSERT      INTO @Tbl_Groups
                  ( 
                   Sitegroup_Name
                  ,Sitegroup_Id
                  ,Sitegroup_Type )
                  SELECT
                        ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,cd.Code_Value AS Sitegroup_Type
                  FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.Code cd
                              ON ch.Sitegroup_Type_cd = cd.Code_Id
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Site_Id = 0
                        AND ( ( ch.Sitegroup_Owner_User_id = @User_Info_Id
                                AND Sitegroup_Type_Name = 'User' )
                              OR Sitegroup_Type_Name IN ( 'Global' ) )
                  GROUP BY
                        ch.Sitegroup_Name
                       ,ch.Sitegroup_Id
                       ,cd.Code_Value
           
           
      INSERT      INTO @Tbl_Groups
                  ( 
                   Sitegroup_Name
                  ,Sitegroup_Id
                  ,Sitegroup_Type )
                  SELECT
                        NULL AS Sitegroup_Name
                       ,-1 AS Sitegroup_Id
                       ,'Global' AS Sitegroup_Type
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    @Tbl_Groups tg
                                 WHERE
                                    tg.Sitegroup_Type = 'Global' )
           
      INSERT      INTO @Tbl_Groups
                  ( 
                   Sitegroup_Name
                  ,Sitegroup_Id
                  ,Sitegroup_Type )
                  SELECT
                        NULL AS Sitegroup_Name
                       ,-2 AS Sitegroup_Id
                       ,'User' AS Sitegroup_Type
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    @Tbl_Groups tg
                                 WHERE
                                    tg.Sitegroup_Type = 'User' )
           
           
      SELECT
            case WHEN Sitegroup_Name IS NULL THEN '.....All ' + case Sitegroup_Type
                                                                  WHEN 'Global' THEN 'Global Groups'
                                                                  WHEN 'User' THEN 'User Groups'
                                                                END + '.....'
                 ELSE Sitegroup_Name
            END AS Sitegroup_Name
           ,Sitegroup_Id
      FROM
            @Tbl_Groups
      ORDER BY
            Sitegroup_Type
           ,Sitegroup_Id     
    
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_User_Global_Sel_By_Client_User] TO [CBMSApplication]
GO
