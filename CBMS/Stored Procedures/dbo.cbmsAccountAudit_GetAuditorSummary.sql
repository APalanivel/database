SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE                    procedure [dbo].[cbmsAccountAudit_GetAuditorSummary]
	( @AuditorId int = null
	, @start_date datetime = null
	, @end_date datetime = null
)
	
as 
begin


select a.auditor_id, ui.first_name + ' ' + ui.last_name [auditor], a.audit_count, b.sent_count, c.error_count [num_of_errors]
from

--Audit Count
	(select a.auditor_id
	, count(aat.tracking_id) as audit_count
	from account_audit_tracking aat with (nolock)
	join account_audit a with (nolock) on aat.account_audit_id = a.account_audit_id
	where (aat.audit_status_type_id = 1253 or (aat.audit_status_type_id = 1251 and aat.closed_reason_type_id = 1048))
	group by a.auditor_id
)a

--Sent Count
full outer join
	(select a.auditor_id
	, count(aat.tracking_id) sent_count
	from account_audit_tracking aat with (nolock)
	join account_audit a with (nolock) on aat.account_audit_id = a.account_audit_id
	where aat.audit_status_type_id  = 1253
	group by a.auditor_id
		

)b on b.auditor_id = a.auditor_id

--Error Count
full outer join
	(select a.auditor_id
	, count(aat.tracking_id) error_count
	from account_audit_tracking aat with (nolock)
	join account_audit a with (nolock) on aat.account_audit_id = a.account_audit_id
	where aat.audit_status_type_id = 1251	
		and (aat.closed_reason_type_id = 1045
		or aat.closed_reason_type_id = 1046
		or aat.closed_reason_type_id = 1049)
	group by a.auditor_id 

)c on c.auditor_id = a.auditor_id

join user_info ui with (nolock) on a.auditor_id = ui.user_info_id

group by  a.auditor_id
	, ui.first_name + ' ' + ui.last_name
	, a.audit_count
	, b.sent_count
	, c.error_count
	
end




GO
GRANT EXECUTE ON  [dbo].[cbmsAccountAudit_GetAuditorSummary] TO [CBMSApplication]
GO
