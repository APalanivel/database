SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.EDIT_GROUPS_FOR_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@groupId       	int       	          	
	@name          	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[EDIT_GROUPS_FOR_CLIENT_P] 

@userId varchar,
@sessionId varchar,
@groupId int,
@name varchar(100)
as
set nocount on

   update RM_GROUP set group_name= @name where RM_GROUP_ID = @groupId
GO
GRANT EXECUTE ON  [dbo].[EDIT_GROUPS_FOR_CLIENT_P] TO [CBMSApplication]
GO
