SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Counter_Party_Report_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Counter_Party_Report_Sel  @Start_Dt='2017-01-01',@End_Dt='2017-03-01',@Currency_Unit_Id = 3,@UOM_Id = 25
		,@Client_Id= 1005,@Hedge_Type_Cd = 586

	Exec Trade.Counter_Party_Report_Sel  @Start_Dt='2019-01-01',@End_Dt='2019-12-01',@Client_Id=10003
	Exec Trade.Counter_Party_Report_Sel  @Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Currency_Unit_Id = 3,@UOM_Id = 25
		,@Hedge_Type_Cd = 586,@Client_Id=1005
	Exec Trade.Counter_Party_Report_Sel  @Start_Dt='2018-01-01',@End_Dt='2018-12-01',@Currency_Unit_Id = 3,@UOM_Id = 25
		,@Hedge_Type_Cd = 586, @Start_Index = 5, @End_Index=10,@Index_Id=325
	Exec Trade.Counter_Party_Report_Sel  @Start_Dt='2018-01-01',@End_Dt='2018-04-01',@Currency_Unit_Id = 3,@UOM_Id = 25
		,@Client_Id = '1043',@Hedge_Type_Cd = 586	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-03-11	GRM - Created
	RR          2019-10-29	GRM-1472 - Performance tuning
	RR			2019-11-01	GRM-1529 Modified to return more columns Total Volume Hedged, Mark-to-Market on Hedges Unit Cost,
							Mark-to-Market on Hedges Total Cost,Total volume open, % Volume Open, Current Percentage Under Max Hedgeable,
							Current Index prices for the open volumes
	RR			2019-11-01	GRM-1471 Modified portfolio price formula
    RR			2019-11-01	GRM-1666 - Modified script to refer new risk management budget schema
    RR			2020-02-21	GRM-1743 Modified to get default configured budget price
							GRM-1745 - Updated budget price rollup
	RR			2020-03-27	GRM-1778 For all prices decimal places increased to 6 from 3
	RR			2020-05-14	GRMUER-17 Furture price should not be displayed for AECO
******/
CREATE PROC [Trade].[Counter_Party_Report_Sel]
    (
        @Hedge_Type_Cd INT
        , @Counter_Party_Id VARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Risk_Manager_User_Info_Id VARCHAR(MAX) = NULL
        , @Client_Manager_User_Info_Id INT = NULL
        , @Currency_Unit_Id INT
        , @UOM_Id INT
        , @Client_Id VARCHAR(MAX) = NULL
        , @Site_Client_Hier_Id VARCHAR(MAX) = NULL
        , @Index_Id INT = NULL
        , @Country_Id VARCHAR(MAX) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Hedge_Mode_Type_Id INT = NULL
    )
WITH RECOMPILE
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Tbl_CL_Forecast
             (
                 Client_Id INT
                 , Counterparty_Id INT
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 0)
                 , Index_Name VARCHAR(200)
             );

        CREATE TABLE #Tbl_CH_Forecast
             (
                 Client_Id INT
                 , Counterparty_Id INT
                 , Service_Month DATE
                 , Client_Hier_Id INT
                 , Forecast_Volume DECIMAL(28, 0)
                 , Index_Name VARCHAR(200)
             );

        CREATE TABLE #Tbl_CH_Hedge
             (
                 Deal_Ticket_Id INT
                 , Client_Id INT
                 , Client_Name VARCHAR(200)
                 , Counterparty_Name VARCHAR(200)
                 , Counterparty_Id INT
                 , Client_Hier_Id INT
                 , Index_Id INT
                 , Index_Name VARCHAR(200)
                 , Price_Index_Id INT
                 , Price_Point_Name VARCHAR(200)
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge DECIMAL(24, 6)
                 , Market_Price DECIMAL(28, 6)
                 , Hedge_Mode VARCHAR(200)
                 , Hedge_Mode_Type_Id INT
                 , Row_Num INT
             );

        CREATE TABLE #Tbl_CL_Hedge
             (
                 Client_Id INT
                 , Client_Name VARCHAR(200)
                 , Counterparty_Name VARCHAR(200)
                 , Counterparty_Id INT
                 , Index_Id INT
                 , Index_Name VARCHAR(200)
                 , Price_Index_Id INT
                 , Price_Point_Name VARCHAR(200)
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge_Price DECIMAL(28, 6)
                 , Market_Price DECIMAL(28, 6)
                 , Hedge_Mode VARCHAR(200)
                 , Hedge_Mode_Type_Id INT
                 , Row_Num INT
                 , WA_Hedge_Total DECIMAL(28, 6)
             );

        CREATE TABLE #Tbl_CL_PP
             (
                 Client_Id INT
                 , Counterparty_Id INT
                 , Price_Index_Id INT
                 , Service_Month DATE
                 , Open_Volume_Price DECIMAL(28, 6)
                 , Hedge_Mode VARCHAR(200)
                 , Hedge_Mode_Type_Id INT
                 , Market_Price DECIMAL(28, 6)
             );




        DECLARE @Market_Price AS TABLE
              (
                  Service_Month DATE
                  , Market_Price DECIMAL(28, 6)
              );

        DECLARE @Total INT;

        CREATE TABLE #RM_Budget_Price
             (
                 Index_Name VARCHAR(25)
                 , Service_Month DATE
                 , Budget_Target_Price DECIMAL(28, 6)
             );

        DECLARE @Sites AS TABLE
              (
                  Client_Hier_Id INT
              );

        DECLARE @Sites_Cnt INT;

        INSERT INTO @Sites
             (
                 Client_Hier_Id
             )
        SELECT
            CAST(us.Segments AS INT)
        FROM
            dbo.ufn_split(@Site_Client_Hier_Id, ',') us;

        SELECT  @Sites_Cnt = COUNT(DISTINCT s.Client_Hier_Id)FROM   @Sites s;


        INSERT INTO @Market_Price
             (
                 Service_Month
             )
        SELECT
            dd.DATE_D
        FROM
            meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt;

        WITH cte_Price
        AS (
               SELECT
                    CAST(nymexsettle.MONTH_IDENTIFIER AS DATE) AS Service_Month
                    , CAST(nymex.CLOSE_PRICE AS DECIMAL(28, 6)) Market_Price
               FROM
                    dbo.RM_NYMEX_SETTLEMENT AS nymexsettle
                    JOIN dbo.RM_NYMEX_DATA nymex
                        ON nymexsettle.SETTLEMENT_DATE = (nymex.BATCH_EXECUTION_DATE - 1)
                    JOIN dbo.RM_NYMEX_MONTH_SYMBOL nymex_symbol
                        ON nymex_symbol.MONTH_INDEX = MONTH(nymexsettle.MONTH_IDENTIFIER)
               WHERE
                    nymexsettle.MONTH_IDENTIFIER BETWEEN @Start_Dt
                                                 AND     @End_Dt
                    AND nymex.SYMBOL = 'NG' + nymex_symbol.MONTH_CODE
                                       + RIGHT(CONVERT(VARCHAR, YEAR(nymexsettle.MONTH_IDENTIFIER)), CASE WHEN LEN(RTRIM(
                                                                                                                       SUBSTRING(
                                                                                                                           nymex.SYMBOL
                                                                                                                           , PATINDEX(
                                                                                                                                 '%[0-9]%'
                                                                                                                                 , nymex.SYMBOL)
                                                                                                                           , 2))) = 1 THEN
                                                                                                              1
                                                                                                         WHEN LEN(RTRIM(
                                                                                                                      SUBSTRING(
                                                                                                                          nymex.SYMBOL
                                                                                                                          , PATINDEX(
                                                                                                                                '%[0-9]%'
                                                                                                                                , nymex.SYMBOL)
                                                                                                                          , 2))) = 2 THEN
                                                                                                             2
                                                                                                     END)
           )
        UPDATE
            mp
        SET
            mp.Market_Price = cp.Market_Price
        FROM
            cte_Price cp
            INNER JOIN @Market_Price mp
                ON mp.Service_Month = cp.Service_Month;


        ;WITH cte_futurptice
        AS (
               SELECT
                    mp.Service_Month
                    , CASE WHEN (OPEN_PRICE != 0) THEN CAST(LAST_PRICE AS DECIMAL(28, 6))
                          ELSE CAST(CLOSE_PRICE AS DECIMAL(28, 6))
                      END CLOSE_PRICE
               FROM
                    dbo.RM_NYMEX_DATA rnd
                    INNER JOIN @Market_Price mp
                        ON CAST(REPLACE(REPLACE(DESCRIPTION, 'NATURAL GAS ', ''), ' ', '-') AS VARCHAR(20)) = CAST(DATENAME(
                                                                                                                       mm
                                                                                                                       , mp.Service_Month) AS VARCHAR(20))
                                                                                                              + '-'
                                                                                                              + CAST(DATEPART(
                                                                                                                         yy
                                                                                                                         , mp.Service_Month) AS VARCHAR(20))
               WHERE
                    rnd.BATCH_EXECUTION_DATE = (SELECT  MAX(BATCH_EXECUTION_DATE)FROM   RM_NYMEX_DATA)
                    AND mp.Market_Price IS NULL
           )
        UPDATE
            mp
        SET
            mp.Market_Price = cp.CLOSE_PRICE
        FROM
            cte_futurptice cp
            INNER JOIN @Market_Price mp
                ON mp.Service_Month = cp.Service_Month;

        INSERT INTO #Tbl_CH_Hedge
             (
                 Deal_Ticket_Id
                 , Client_Id
                 , Client_Name
                 , Counterparty_Name
                 , Counterparty_Id
                 , Client_Hier_Id
                 , Index_Id
                 , Index_Name
                 , Price_Index_Id
                 , Price_Point_Name
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge
                 , Market_Price
                 , Hedge_Mode
                 , Hedge_Mode_Type_Id
                 , Row_Num
             )
        SELECT
            dt.Deal_Ticket_Id
            , dt.Client_Id
            , ch.Client_Name
            , MAX(CASE WHEN hdgtyp.ENTITY_NAME = 'Physical' THEN vndr.VENDOR_NAME
                      WHEN hdgtyp.ENTITY_NAME = 'Financial' THEN rmcp.COUNTERPARTY_NAME
                  END) AS Counterparty_Name
            , MAX(trd.Cbms_Counterparty_Id) AS Counterparty_Id
            , vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , CASE WHEN frq.Code_Value = 'Monthly' THEN
            (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                 WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
             END)
                  WHEN frq.Code_Value = 'Daily' THEN
            (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                 WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
             END) * dd.DAYS_IN_MONTH_NUM
              END
            , (CASE WHEN frq.Code_Value = 'Monthly' THEN
            (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                 WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
             END)
                   WHEN frq.Code_Value = 'Daily' THEN
            (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                 WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
             END) * dd.DAYS_IN_MONTH_NUM
               END) * tp.Trade_Price * cuc.CONVERSION_FACTOR AS WA_Hedge
            , MAX((piv.INDEX_VALUE * cucm.CONVERSION_FACTOR) / cuc3.CONVERSION_FACTOR) AS Market_Price
            , e.ENTITY_NAME AS Hedge_Mode
            , dt.Hedge_Mode_Type_Id
            , DENSE_RANK() OVER (ORDER BY
                                     ch.Client_Name)
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt.Deal_Ticket_Frequency_Cd
            INNER JOIN dbo.ENTITY hdgtyp
                ON dt.Hedge_Type_Cd = hdgtyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dt.Deal_Ticket_Id = vol.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = vol.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc2
                ON cuc2.BASE_UNIT_ID = vol.Uom_Type_Id
                   AND  cuc2.CONVERTED_UNIT_ID = @UOM_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Deal_Month
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = vol.Trade_Price_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON dt.Price_Index_Id = pridx.PRICE_INDEX_ID
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dt.Currency_Unit_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
                   AND  cuc.CONVERSION_DATE = vol.Deal_Month
                   AND  cuc.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_TXN_Status trd
                ON trd.Cbms_Trade_Number = vol.Trade_Number
            LEFT JOIN dbo.VENDOR vndr
                ON trd.Cbms_Counterparty_Id = vndr.VENDOR_ID
            LEFT JOIN dbo.RM_COUNTERPARTY rmcp
                ON trd.Cbms_Counterparty_Id = rmcp.RM_COUNTERPARTY_ID
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.ENTITY e
                ON dt.Hedge_Mode_Type_Id = e.ENTITY_ID
            LEFT JOIN dbo.PRICE_INDEX_VALUE piv
                ON piv.PRICE_INDEX_ID = pridx.PRICE_INDEX_ID
                   AND  piv.INDEX_MONTH = vol.Deal_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc3
                ON pridx.VOLUME_UNIT_ID = cuc3.BASE_UNIT_ID
                   AND  cuc3.CONVERTED_UNIT_ID = @UOM_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cucm
                ON cucm.BASE_UNIT_ID = pridx.CURRENCY_UNIT_ID
                   AND  cucm.CONVERTED_UNIT_ID = @Currency_Unit_Id
                   AND  cucm.CONVERSION_DATE = vol.Deal_Month
                   AND  cucm.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
        WHERE
            dt.Hedge_Type_Cd = @Hedge_Type_Cd
            AND (   @Client_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Client_Id))
            AND ch.Site_Id > 0
            AND tp.Trade_Price IS NOT NULL
            AND vol.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND (   @Hedge_Mode_Type_Id IS NULL
                    OR  dt.Hedge_Mode_Type_Id = @Hedge_Mode_Type_Id)
            AND (   @Counter_Party_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Counter_Party_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = trd.Cbms_Counterparty_Id))
            AND (   @Index_Id IS NULL
                    OR  pridx.INDEX_ID = @Index_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Country_Id))
            AND (   @Site_Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Client_Hier_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Client_Hier_Id))
            AND (   @Client_Manager_User_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.CLIENT_CEM_MAP ccm
                                   WHERE
                                        ccm.CLIENT_ID = ch.Client_Id
                                        AND ccm.USER_INFO_ID = @Client_Manager_User_Info_Id))
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                                    INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                        ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                               WHERE
                                    siteob.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob.Commodity_Id = dt.Commodity_Id
                                    AND chhc.Include_In_Reports = 1
                                    AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                 AND     @End_Dt
                                            OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                   AND     @End_Dt
                                            OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                          AND     chhc.Config_End_Dt
                                            OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                        AND     chhc.Config_End_Dt)
                                    AND (   @Risk_Manager_User_Info_Id IS NULL
                                            OR  EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                dbo.ufn_split(@Risk_Manager_User_Info_Id, ',')
                                                           WHERE
                                                                CAST(Segments AS INT) = chhc.Risk_Manager_User_Info_Id)))
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.RM_Client_Hier_Onboard clntob
                                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                            ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                        INNER JOIN dbo.ENTITY et
                                            ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                        INNER JOIN Core.Client_Hier clch
                                            ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                   WHERE
                                        clch.Sitegroup_Id = 0
                                        AND clch.Client_Id = ch.Client_Id
                                        AND clntob.Country_Id = ch.Country_Id
                                        AND clntob.Commodity_Id = dt.Commodity_Id
                                        AND chhc.Include_In_Reports = 1
                                        AND (   chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                                     AND     @End_Dt
                                                OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                                                       AND     @End_Dt
                                                OR  @Start_Dt BETWEEN chhc.Config_Start_Dt
                                                              AND     chhc.Config_End_Dt
                                                OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                                            AND     chhc.Config_End_Dt)
                                        AND (   @Risk_Manager_User_Info_Id IS NULL
                                                OR  EXISTS (   SELECT
                                                                    1
                                                               FROM
                                                                    dbo.ufn_split(@Risk_Manager_User_Info_Id, ',')
                                                               WHERE
                                                                    CAST(Segments AS INT) = chhc.Risk_Manager_User_Info_Id))
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Trade.RM_Client_Hier_Onboard siteob1
                                                           WHERE
                                                                siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                                AND siteob1.Commodity_Id = dt.Commodity_Id)))
        GROUP BY
            dt.Deal_Ticket_Id
            , dt.Client_Id
            , vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , vol.Total_Volume
            , tp.Trade_Price
            , tp.Market_Price
            , cuc.CONVERSION_FACTOR
            , trdact.Code_Value
            , ch.Client_Name
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , e.ENTITY_NAME
            , dt.Hedge_Mode_Type_Id
            , cuc2.CONVERSION_FACTOR;





        INSERT INTO #Tbl_CH_Forecast
             (
                 Client_Id
                 , Counterparty_Id
                 , Service_Month
                 , Client_Hier_Id
                 , Forecast_Volume
                 , Index_Name
             )
        SELECT
            ch.Client_Id
            , tch.Counterparty_Id
            , chf.Service_Month
            , ch.Client_Hier_Id
            , MAX(chf.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , 'NYMEX'
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chf
                ON chf.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chf.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @UOM_Id
            INNER JOIN #Tbl_CH_Hedge tch
                ON tch.Client_Id = ch.Client_Id
                   AND  tch.Client_Hier_Id = chf.Client_Hier_Id
                   AND  tch.Service_Month = chf.Service_Month
        WHERE
            chf.Service_Month BETWEEN @Start_Dt
                              AND     @End_Dt
            AND tch.Row_Num BETWEEN @Start_Index
                            AND     @End_Index
            AND (   ch.Country_Name = 'USA'
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        #Tbl_CH_Hedge tch
                                   WHERE
                                        tch.Client_Hier_Id = ch.Client_Hier_Id
                                        AND tch.Service_Month = chf.Service_Month
                                        AND tch.Index_Name = 'NYMEX'))
        GROUP BY
            ch.Client_Id
            , ch.Client_Name
            , chf.Service_Month
            , ch.Client_Hier_Id
            , tch.Counterparty_Id;

        INSERT INTO #Tbl_CH_Forecast
             (
                 Client_Id
                 , Counterparty_Id
                 , Service_Month
                 , Client_Hier_Id
                 , Forecast_Volume
                 , Index_Name
             )
        SELECT
            ch.Client_Id
            , tch.Counterparty_Id
            , chf.Service_Month
            , ch.Client_Hier_Id
            , MAX(chf.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , 'CGPR'
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chf
                ON chf.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chf.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @UOM_Id
            INNER JOIN #Tbl_CH_Hedge tch
                ON tch.Client_Id = ch.Client_Id
                   AND  tch.Client_Hier_Id = chf.Client_Hier_Id
                   AND  tch.Service_Month = chf.Service_Month
        WHERE
            chf.Service_Month BETWEEN @Start_Dt
                              AND     @End_Dt
            AND tch.Row_Num BETWEEN @Start_Index
                            AND     @End_Index
            AND ch.Country_Name = 'Canada'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Tbl_CH_Hedge tch
                               WHERE
                                    tch.Client_Hier_Id = ch.Client_Hier_Id
                                    AND tch.Service_Month = chf.Service_Month
                                    AND tch.Index_Name = 'NYMEX')
        GROUP BY
            ch.Client_Id
            , ch.Client_Name
            , chf.Service_Month
            , ch.Client_Hier_Id
            , tch.Counterparty_Id;

        INSERT INTO #Tbl_CL_Forecast
             (
                 Client_Id
                 , Counterparty_Id
                 , Service_Month
                 , Forecast_Volume
                 , Index_Name
             )
        SELECT
            Client_Id
            , Counterparty_Id
            , Service_Month
            , SUM(Forecast_Volume)
            , Index_Name
        FROM
            #Tbl_CH_Forecast
        GROUP BY
            Client_Id
            , Service_Month
            , Counterparty_Id
            , Index_Name;



        INSERT INTO #Tbl_CL_Hedge
             (
                 Client_Id
                 , Client_Name
                 , Counterparty_Name
                 , Counterparty_Id
                 , Index_Id
                 , Index_Name
                 , Price_Index_Id
                 , Price_Point_Name
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge_Price
                 , Market_Price
                 , Hedge_Mode
                 , Hedge_Mode_Type_Id
                 , Row_Num
                 , WA_Hedge_Total
             )
        SELECT
            Client_Id
            , Client_Name
            , Counterparty_Name
            , Counterparty_Id
            , Index_Id
            , Index_Name
            , Price_Index_Id
            , Price_Point_Name
            , Service_Month
            , SUM(Hedged_Volume) AS Hedged_Volume
            , SUM(WA_Hedge) / NULLIF(SUM(Hedged_Volume), 0) AS WA_Hedge_Price
            , Market_Price
            , Hedge_Mode
            , Hedge_Mode_Type_Id
            , MIN(Row_Num)
            , SUM(WA_Hedge)
        FROM
            #Tbl_CH_Hedge
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index
        GROUP BY
            Client_Id
            , Client_Name
            , Counterparty_Name
            , Counterparty_Id
            , Index_Id
            , Index_Name
            , Price_Index_Id
            , Price_Point_Name
            , Service_Month
            , Market_Price
            , Hedge_Mode
            , Hedge_Mode_Type_Id;


        INSERT INTO #Tbl_CL_PP
             (
                 Client_Id
                 , Price_Index_Id
                 , Counterparty_Id
                 , Service_Month
                 , Open_Volume_Price
                 , Hedge_Mode
                 , Hedge_Mode_Type_Id
                 , Market_Price
             )
        SELECT
            chh.Client_Id
            , chh.Price_Index_Id
            , chh.Counterparty_Id
            , chh.Service_Month
            , CASE WHEN chh.Hedge_Mode = 'Index' THEN
                       SUM(chf.Forecast_Volume * ISNULL(chh.Market_Price, mp.Market_Price))
                       - SUM(chh.Hedged_Volume * ISNULL(chh.Market_Price, mp.Market_Price))
                  WHEN chh.Hedge_Mode = 'Basis' THEN
                      SUM((chf.Forecast_Volume - chh.Hedged_Volume)
                          * (ISNULL(chh.Market_Price, mp.Market_Price) - mp.Market_Price))
              END
            , chh.Hedge_Mode
            , chh.Hedge_Mode_Type_Id
            , ISNULL(chh.Market_Price, mp.Market_Price) AS Market_Price
        FROM
            #Tbl_CL_Hedge chh
            INNER JOIN #Tbl_CL_Forecast chf
                ON chh.Client_Id = chf.Client_Id
                   AND  chh.Service_Month = chf.Service_Month
                   AND  chf.Counterparty_Id = chh.Counterparty_Id
            LEFT JOIN @Market_Price mp
                ON chh.Service_Month = mp.Service_Month
        GROUP BY
            chh.Client_Id
            , chh.Counterparty_Id
            , chh.Price_Index_Id
            , chh.Service_Month
            --, mp.Market_Price
            , chh.Hedge_Mode
            , ISNULL(chh.Market_Price, mp.Market_Price)
            , chh.Hedge_Mode_Type_Id;


        SELECT  @Total = MAX(Row_Num)FROM   #Tbl_CH_Hedge;

        WITH Cte_Budgets
        AS (
               SELECT
                    s.Client_Hier_Id
                    , CASE WHEN e.ENTITY_NAME = 'AECO' THEN 'CGPR'
                          ELSE e.ENTITY_NAME
                      END AS Index_Name
                    , s.Service_Month
                    , COALESCE(rbvpd.Budget_Target_Price, rbvpd1.Budget_Target_Price, rbpd.Rm_Budget_Price) AS Budget_Target_Price
                    , tcf.Forecast_Volume
               FROM
                    #Tbl_CH_Hedge s
                    INNER JOIN Trade.Rm_Budget_Participant rbp
                        ON rbp.Client_Hier_Id = s.Client_Hier_Id
                    INNER JOIN Trade.Rm_Budget rb
                        ON rbp.Rm_Budget_Id = rb.Rm_Budget_Id
                    INNER JOIN dbo.ENTITY e
                        ON e.ENTITY_ID = rb.Index_Id
                    INNER JOIN Trade.Rm_Budget_Price_Dtl rbpd
                        ON rbpd.Rm_Budget_Id = rb.Rm_Budget_Id
                           AND  s.Service_Month = rbpd.Service_Month
                    LEFT JOIN Trade.Rm_Budget_Volume_Price_Dtl rbvpd
                        ON rbvpd.Rm_Budget_Id = rbpd.Rm_Budget_Id
                           AND  rbvpd.Service_Month = rbpd.Service_Month
                           AND  rbvpd.Client_Hier_Id = rbp.Client_Hier_Id
                    LEFT JOIN(Trade.Rm_Budget_Volume_Price_Dtl rbvpd1
                              INNER JOIN Core.Client_Hier ch
                                  ON ch.Client_Hier_Id = rbvpd1.Client_Hier_Id
                                     AND ch.Site_Id = 0
                                     AND ch.Client_Id = @Client_Id)
                        ON rbvpd1.Rm_Budget_Id = rbpd.Rm_Budget_Id
                           AND  rbvpd1.Service_Month = rbpd.Service_Month
                           AND  rbvpd1.Client_Hier_Id <> rbp.Client_Hier_Id
                    INNER JOIN #Tbl_CH_Forecast tcf
                        ON tcf.Client_Hier_Id = s.Client_Hier_Id
                           AND  tcf.Service_Month = s.Service_Month
               WHERE
                    rb.Client_Id = @Client_Id
                    AND rbpd.Service_Month BETWEEN @Start_Dt
                                           AND     @End_Dt
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                                   WHERE
                                        rbp.Client_Hier_Id = rbsdbc.Client_Hier_Id
                                        AND rbp.Rm_Budget_Id = rbsdbc.Rm_Budget_Id
                                        AND rbpd.Service_Month BETWEEN rbsdbc.Start_Dt
                                                               AND     rbsdbc.End_Dt)
               GROUP BY
                   s.Client_Hier_Id
                   , CASE WHEN e.ENTITY_NAME = 'AECO' THEN 'CGPR'
                         ELSE e.ENTITY_NAME
                     END
                   , s.Service_Month
                   , COALESCE(rbvpd.Budget_Target_Price, rbvpd1.Budget_Target_Price, rbpd.Rm_Budget_Price)
                   , tcf.Forecast_Volume
           )
        INSERT INTO #RM_Budget_Price
             (
                 Index_Name
                 , Service_Month
                 , Budget_Target_Price
             )
        SELECT
            cb.Index_Name
            , cb.Service_Month
            , SUM(cb.Budget_Target_Price * cb.Forecast_Volume) / NULLIF(SUM(cb.Forecast_Volume), 0) AS Budget_Target_Price
        FROM
            Cte_Budgets cb
            INNER JOIN #Tbl_CH_Hedge tch
                ON tch.Service_Month = cb.Service_Month
                   AND  tch.Index_Name = cb.Index_Name
        GROUP BY
            cb.Index_Name
            , cb.Service_Month
        HAVING
            COUNT(DISTINCT cb.Client_Hier_Id) = COUNT(DISTINCT tch.Client_Hier_Id);

        SELECT
            hdg.Client_Id
            , hdg.Client_Name
            , hdg.Counterparty_Id
            , hdg.Counterparty_Name
            , hdg.Index_Id
            , hdg.Index_Name
            , hdg.Price_Index_Id
            , hdg.Price_Point_Name
            , hdg.Service_Month
            , CAST(((hdg.Hedged_Volume / NULLIF(fc.Forecast_Volume, 0)) * 100) AS DECIMAL(28, 0)) AS Percentage_Hedged
            , CAST(WA_Hedge_Price AS DECIMAL(28, 6)) AS WA_Hedge_Price
            , CAST((hdg.WA_Hedge_Total + pp.Open_Volume_Price) / NULLIF(fc.Forecast_Volume, 0) AS DECIMAL(28, 6)) AS Portfolio_Price
            , MAX(CAST((rbp.Budget_Target_Price) AS DECIMAL(28, 6))) AS Budget_Price
            , hdg.Row_Num
            , @Total AS Total
            , CAST(fc.Forecast_Volume AS DECIMAL(28, 0)) AS Forecast_Volume
            , CAST(hdg.Hedged_Volume AS DECIMAL(28, 0)) AS Hedged_Volume
            , hdg.Hedge_Mode
            , CAST(fc.Forecast_Volume AS DECIMAL(28, 0)) AS Total_RM_Forecasted_Volume
            , (CAST(pp.Market_Price - hdg.WA_Hedge_Price AS DECIMAL(28, 6))) AS Mark_To_Market_Hedges_Unit_Cost
            , (CAST((pp.Market_Price - hdg.WA_Hedge_Price) * hdg.Hedged_Volume AS DECIMAL(28, 0))) AS Mark_To_Market_Hedges_Total_Cost
            , CAST(fc.Forecast_Volume - hdg.Hedged_Volume AS DECIMAL(28, 3)) AS Total_Volume_Open
            , CAST((((fc.Forecast_Volume - hdg.Hedged_Volume) / NULLIF(fc.Forecast_Volume, 0)) * 100) AS DECIMAL(5, 0)) AS Percentage_Volume_Open
            , CASE WHEN hdg.Index_Name = 'NYMEX' THEN CAST(pp.Market_Price AS DECIMAL(28, 6))
                  ELSE NULL
              END AS Current_Index_Price
        FROM
            #Tbl_CL_Forecast fc
            INNER JOIN #Tbl_CL_Hedge hdg
                ON hdg.Client_Id = fc.Client_Id
                   AND  hdg.Service_Month = fc.Service_Month
                   AND  hdg.Counterparty_Id = fc.Counterparty_Id
                   AND  hdg.Index_Name = fc.Index_Name
            LEFT JOIN #Tbl_CL_PP pp
                ON hdg.Client_Id = pp.Client_Id
                   AND  hdg.Service_Month = pp.Service_Month
                   AND  hdg.Price_Index_Id = pp.Price_Index_Id
                   AND  hdg.Counterparty_Id = pp.Counterparty_Id
                   AND  pp.Hedge_Mode_Type_Id = hdg.Hedge_Mode_Type_Id
            LEFT JOIN #RM_Budget_Price rbp
                ON rbp.Index_Name = hdg.Index_Name
                   AND  rbp.Service_Month = hdg.Service_Month
        WHERE
            hdg.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        GROUP BY
            hdg.Client_Id
            , hdg.Client_Name
            , hdg.Counterparty_Id
            , hdg.Counterparty_Name
            , hdg.Index_Id
            , hdg.Index_Name
            , hdg.Price_Index_Id
            , hdg.Price_Point_Name
            , hdg.Service_Month
            , hdg.WA_Hedge_Price
            , hdg.Row_Num
            , fc.Forecast_Volume
            , hdg.Hedged_Volume
            , hdg.Hedge_Mode
            , hdg.WA_Hedge_Total
            , pp.Open_Volume_Price
            , pp.Market_Price;

        DROP TABLE
            #Tbl_CL_Forecast
            , #Tbl_CH_Forecast
            , #Tbl_CH_Hedge
            , #Tbl_CL_Hedge
            , #Tbl_CL_PP
            , #RM_Budget_Price;

    END;
GO
GRANT EXECUTE ON  [Trade].[Counter_Party_Report_Sel] TO [CBMSApplication]
GO
