SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  Procedure dbo.seStripDetail_GetForHeader
	(
		@StripHeaderId int
	)
AS
BEGIN
	set nocount on
	 select s.StripDetailId
				, s.StripHeaderId
				, s.Symbol
				, s.Volume
				, s.SortDate
				, sy.Label
		 from seStripDetail s
	         join seCommodity sy on sy.Symbol = s.Symbol
		where StripHeaderId = @StripHeaderId
 order by SortDate asc


END
GO
GRANT EXECUTE ON  [dbo].[seStripDetail_GetForHeader] TO [CBMSApplication]
GO
