SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_INSERT_ACCOUNTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
    @rfpAccountId             int,
    @contractExpDate          datetime,
    @contractNoticeDate       datetime,
    @rfpInitiatedDate         datetime,
    @incumbantVendorId        int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
 --   -- Test an entry
 --   exec dbo.SR_RFP_INSERT_ACCOUNTS_P
 --   @rfpAccountId =164,
 --   @contractExpDate = null,
 --   --@currentContractId int,
 --   @contractNoticeDate = null,
 --   @rfpInitiatedDate = '2006-02-28 00:00:00.000',
 --   @incumbantVendorId = 634
    
 --   -- check if the entry exist at SV
 --   select * from sr_rfp_checklist where sr_rfp_account_id = 164 and rfp_initiated_date = '2006-02-28 00:00:00.000' and incumbant_id = 634 
    
    
 --   -- delete the entry
	--delete from sr_rfp_checklist where sr_rfp_account_id = 164 and rfp_initiated_date = '2006-02-28 00:00:00.000' and incumbant_id = 634 
    

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_INSERT_ACCOUNTS_P
    @rfpAccountId int,
    @contractExpDate datetime,
    @contractNoticeDate datetime,
    @rfpInitiatedDate datetime,
    @incumbantVendorId int


    AS
	    
SET NOCOUNT ON
    

    INSERT INTO SR_RFP_CHECKLIST
    (
    SR_RFP_ACCOUNT_ID, 
    RFP_INITIATED_DATE,
    INCUMBANT_ID 
    )

    VALUES 
    (
    @rfpAccountId ,
    @rfpInitiatedDate,
    @incumbantVendorId 
    )
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_INSERT_ACCOUNTS_P] TO [CBMSApplication]
GO
