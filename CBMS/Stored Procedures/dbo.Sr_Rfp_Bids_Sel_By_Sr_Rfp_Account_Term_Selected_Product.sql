SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Bids_Sel_By_Sr_Rfp_Account_Term_Selected_Product]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Rfp_Bid_Id		INT
    @Language_CD		INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM 	dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	
	EXEC dbo.Sr_Rfp_Bids_Sel_By_Sr_Rfp_Account_Term_Selected_Product  12103458,0,NULL,13114
	EXEC dbo.Sr_Rfp_Bids_Sel_By_Sr_Rfp_Account_Term_Selected_Product  10011924,1,NULL,13154
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-06	Global Sourcing - Phase3 - GCS-530 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Bids_Sel_By_Sr_Rfp_Account_Term_Selected_Product]
      ( 
       @Sr_Account_Group_Id INT
      ,@Is_Bid_Group INT
      ,@Language_CD INT = NULL
      ,@Sr_Rfp_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Vendor_Header VARCHAR(MAX)
           ,@Sql_Str NVARCHAR(MAX)
           ,@Commodity_Name VARCHAR(50)
      
      SELECT
            @Language_CD = isnull(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage';
      
      SELECT
            @Commodity_Name = comm.Commodity_Name
      FROM
            dbo.SR_RFP rfp
            INNER JOIN dbo.Commodity comm
                  ON rfp.COMMODITY_TYPE_ID = comm.Commodity_Id
      WHERE
            rfp.SR_RFP_ID = @Sr_Rfp_Id
            
      CREATE TABLE #Bids
            ( 
             Category_Name_Locale_Value NVARCHAR(255)
            ,Category_Display_Seq INT
            ,Question_Label_Locale_Value NVARCHAR(MAX)
            ,Question_Display_Seq INT
            ,Response NVARCHAR(MAX)
            ,VENDOR_NAME VARCHAR(200)
            ,Vendor_Contact VARCHAR(100)
            ,Term VARCHAR(50)
            ,PRODUCT_NAME VARCHAR(200)
            ,FROM_MONTH DATETIME
            ,TO_MONTH DATETIME )
           
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        isnull(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name_Locale_Value
                       ,rfptqm.Category_Display_Seq
                       ,isnull(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
                       ,rfptqm.Question_Display_Seq
                       ,rfptqr.Response_Text_Value + isnull(( '    Comment: ' + rfptqr.Comment ), '') AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Map rfptqm
                              ON rfptqm.SR_RFP_SELECTED_PRODUCTS_ID = tpm.SR_RFP_SELECTED_PRODUCTS_ID
                                 AND rfptqm.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.Sr_Service_Condition_Question sscq
                              ON rfptqm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = sscq.Response_Type_Cd
                        LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                              ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                                 AND sscqlv.Language_Cd = @Language_CD
                        INNER JOIN dbo.Sr_Service_Condition_Category sscc
                              ON rfptqm.Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
                        LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                              ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                                 AND sscclv.Language_Cd = @Language_CD
                        LEFT JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response rfptqr
                              ON rfptqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id = rfptqr.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                                 AND tpm.SR_RFP_BID_ID = rfptqr.SR_RFP_BID_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                  
---------------Bid Requirements-----------------Start----------------
      IF @Commodity_Name = 'Electric Power' 
            BEGIN
                        
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Delivery Point' AS Question_Label_Locale_Value
                                   ,1 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srbr.DELIVERY_POINT_POWER_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Transportation Service Level' AS Question_Label_Locale_Value
                                   ,2 AS Question_Display_Seq
                                   ,ent.ENTITY_NAME AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    LEFT JOIN dbo.ENTITY ent
                                          ON srbr.SERVICE_LEVEL_POWER_TYPE_ID = ent.ENTITY_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                  INSERT      INTO #Bids
                              ( 
                               Category_Name_Locale_Value
                              ,Category_Display_Seq
                              ,Question_Label_Locale_Value
                              ,Question_Display_Seq
                              ,Response
                              ,VENDOR_NAME
                              ,Vendor_Contact
                              ,Term
                              ,PRODUCT_NAME
                              ,FROM_MONTH
                              ,TO_MONTH )
                              SELECT
                                    'Bid Requirements' AS Category_Name_Locale_Value
                                   ,-1 AS Category_Display_Seq
                                   ,'Comments' AS Question_Label_Locale_Value
                                   ,3 AS Question_Display_Seq
                                   ,srbr.COMMENTS AS Response
                                   ,vndr.VENDOR_NAME
                                   ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                   ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                         ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                    END AS Term
                                   ,srpp.PRODUCT_NAME
                                   ,term.FROM_MONTH
                                   ,term.TO_MONTH
                              FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                          ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                          ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO ui
                                          ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                    INNER JOIN dbo.VENDOR vndr
                                          ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                    INNER JOIN dbo.SR_RFP rfp
                                          ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                    INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                          ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                    INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                          ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                    INNER JOIN dbo.SR_RFP_BID bid
                                          ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                    LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                          ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                              WHERE
                                    term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                    AND term.IS_BID_GROUP = @Is_Bid_Group
            END
      ELSE 
            IF @Commodity_Name = 'Natural Gas' 
                  BEGIN
                        
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Delivery Point' AS Question_Label_Locale_Value
                                         ,1 AS Question_Display_Seq
                                         ,srbr.DELIVERY_POINT AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Transportation Service Level' AS Question_Label_Locale_Value
                                         ,2 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.TRANSPORTATION_LEVEL_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Supplier responsible for nominations' AS Question_Label_Locale_Value
                                         ,3 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.NOMINATION_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Supplier responsible for balancing' AS Question_Label_Locale_Value
                                         ,4 AS Question_Display_Seq
                                         ,ent.ENTITY_NAME AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                          LEFT JOIN dbo.ENTITY ent
                                                ON srbr.BALANCING_TYPE_ID = ent.ENTITY_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                              
                        INSERT      INTO #Bids
                                    ( 
                                     Category_Name_Locale_Value
                                    ,Category_Display_Seq
                                    ,Question_Label_Locale_Value
                                    ,Question_Display_Seq
                                    ,Response
                                    ,VENDOR_NAME
                                    ,Vendor_Contact
                                    ,Term
                                    ,PRODUCT_NAME
                                    ,FROM_MONTH
                                    ,TO_MONTH )
                                    SELECT
                                          'Bid Requirements' AS Category_Name_Locale_Value
                                         ,-1 AS Category_Display_Seq
                                         ,'Comments' AS Question_Label_Locale_Value
                                         ,5 AS Question_Display_Seq
                                         ,srbr.COMMENTS AS Response
                                         ,vndr.VENDOR_NAME
                                         ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                                         ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                               ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                                          END AS Term
                                         ,srpp.PRODUCT_NAME
                                         ,term.FROM_MONTH
                                         ,term.TO_MONTH
                                    FROM
                                          dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                          INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                                                ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                                          INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                                                ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                                          INNER JOIN dbo.USER_INFO ui
                                                ON ci.USER_INFO_ID = ui.USER_INFO_ID
                                          INNER JOIN dbo.VENDOR vndr
                                                ON cvm.VENDOR_ID = vndr.VENDOR_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                                ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                                          INNER JOIN dbo.SR_RFP rfp
                                                ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                                          INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                                                ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                                          INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                                                ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                                          INNER JOIN dbo.SR_RFP_BID bid
                                                ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                                          LEFT JOIN dbo.SR_RFP_BID_REQUIREMENTS srbr
                                                ON bid.SR_RFP_BID_REQUIREMENTS_ID = srbr.SR_RFP_BID_REQUIREMENTS_ID
                                    WHERE
                                          term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                                          AND term.IS_BID_GROUP = @Is_Bid_Group
                  END
---------------Bid Requirements-----------------End----------------
            
---------------Supplier Price and Comments-----------------Start----------------            
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Credit Approval' AS Question_Label_Locale_Value
                       ,1 AS Question_Display_Seq
                       ,case srspc.IS_CREDIT_APPROVAL
                          WHEN 1 THEN 'Yes'
                          WHEN 0 THEN 'No' + isnull(( '    Comment: ' + srspc.NO_CREDIT_COMMENTS ), '')
                        END AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Is the broker fee included?' AS Question_Label_Locale_Value
                       ,2 AS Question_Display_Seq
                       ,ent.ENTITY_NAME AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                        LEFT JOIN dbo.ENTITY ent
                              ON srspc.Broker_Included_Type_Id = ent.ENTITY_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Price' AS Question_Label_Locale_Value
                       ,3 AS Question_Display_Seq
                       ,srspc.PRICE_RESPONSE_COMMENTS AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
                              
      INSERT      INTO #Bids
                  ( 
                   Category_Name_Locale_Value
                  ,Category_Display_Seq
                  ,Question_Label_Locale_Value
                  ,Question_Display_Seq
                  ,Response
                  ,VENDOR_NAME
                  ,Vendor_Contact
                  ,Term
                  ,PRODUCT_NAME
                  ,FROM_MONTH
                  ,TO_MONTH )
                  SELECT
                        'Supplier Price and Comments' AS Category_Name_Locale_Value
                       ,0 AS Category_Display_Seq
                       ,'Pricing Comments' AS Question_Label_Locale_Value
                       ,4 AS Question_Display_Seq
                       ,srspc.Pricing_Comments AS Response
                       ,vndr.VENDOR_NAME
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Vendor_Contact
                       ,case WHEN rfp.Is_Term_Date_Specific = 1 THEN cast(datepart(dd, term.FROM_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datepart(dd, term.TO_MONTH) AS VARCHAR(5)) + '-' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + '-' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                             ELSE cast(datename(MM, term.FROM_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.FROM_MONTH) AS VARCHAR(5)) + ' - ' + cast(datename(MM, term.TO_MONTH) AS VARCHAR(3)) + ' ' + cast(datepart(yy, term.TO_MONTH) AS VARCHAR(5))
                        END AS Term
                       ,srpp.PRODUCT_NAME
                       ,term.FROM_MONTH
                       ,term.TO_MONTH
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP cvm
                              ON tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = cvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ci
                              ON cvm.SR_SUPPLIER_CONTACT_INFO_ID = ci.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO ui
                              ON ci.USER_INFO_ID = ui.USER_INFO_ID
                        INNER JOIN dbo.VENDOR vndr
                              ON cvm.VENDOR_ID = vndr.VENDOR_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                              ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.SR_RFP_ID = cvm.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_SELECTED_PRODUCTS srsp
                              ON tpm.SR_RFP_SELECTED_PRODUCTS_ID = srsp.SR_RFP_SELECTED_PRODUCTS_ID
                        INNER JOIN dbo.SR_RFP_PRICING_PRODUCT srpp
                              ON srsp.PRICING_PRODUCT_ID = srpp.SR_RFP_PRICING_PRODUCT_ID
                        INNER JOIN dbo.SR_RFP_BID bid
                              ON tpm.SR_RFP_BID_ID = bid.SR_RFP_BID_ID
                        LEFT JOIN dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS srspc
                              ON bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = srspc.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  WHERE
                        term.SR_ACCOUNT_GROUP_ID = @Sr_Account_Group_Id
                        AND term.IS_BID_GROUP = @Is_Bid_Group
 
  ---------------Supplier Price and Comments-----------------End----------------                      
  
      SELECT
            @Vendor_Header = left(Vendor.Vendors, len(Vendor.Vendors) - 1)
      FROM
            #Bids
            CROSS APPLY ( SELECT
                              '[' + vndr.VENDOR_NAME + '] ,'
                          FROM
                              #Bids vndr
                          GROUP BY
                              vndr.VENDOR_NAME
            FOR
                          XML PATH('') ) Vendor ( Vendors )

      
      SELECT
            @Sql_Str = 'SELECT PRODUCT_NAME,Category_Name_Locale_Value,Category_Display_Seq+2 AS Category_Display_Seq
						,Question_Label_Locale_Value,Question_Display_Seq,Term,' + @Vendor_Header + 'FROM (
						SELECT PRODUCT_NAME,Category_Name_Locale_Value,Category_Display_Seq
						,Question_Label_Locale_Value,Question_Display_Seq
						,Response ,VENDOR_NAME,Term,FROM_MONTH,TO_MONTH
						FROM #Bids) up
						PIVOT (max(Response) FOR VENDOR_NAME IN (' + @Vendor_Header + ')) AS pvt
						WHERE Question_Label_Locale_Value is not null
						ORDER BY PRODUCT_NAME,FROM_MONTH,TO_MONTH,Category_Display_Seq,Question_Display_Seq'
						
      EXECUTE (@Sql_Str)
      
      DROP TABLE #Bids

END;


;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Bids_Sel_By_Sr_Rfp_Account_Term_Selected_Product] TO [CBMSApplication]
GO
