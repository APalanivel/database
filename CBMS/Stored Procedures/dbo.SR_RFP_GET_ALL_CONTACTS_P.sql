
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ALL_CONTACTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.SR_RFP_GET_ALL_CONTACTS_P 1,1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-02-29	Global Sourcing Phase-3 GCS-482 Added group by to avoid duplicates as a single user can mapped
							as contact to multiple states/countries

******/


CREATE   PROCEDURE [dbo].[SR_RFP_GET_ALL_CONTACTS_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            scInfo.SR_SUPPLIER_CONTACT_INFO_ID
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME USER_INFO_NAME
      FROM
            dbo.USER_INFO userInfo
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO scInfo
                  ON userInfo.USER_INFO_ID = scInfo.USER_INFO_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP cvMap
                  ON scInfo.SR_SUPPLIER_CONTACT_INFO_ID = cvMap.SR_SUPPLIER_CONTACT_INFO_ID
      WHERE
            userInfo.is_History = 0
      GROUP BY
            scInfo.SR_SUPPLIER_CONTACT_INFO_ID
           ,userInfo.FIRST_NAME + ' ' + userInfo.LAST_NAME
      ORDER BY
            USER_INFO_NAME
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ALL_CONTACTS_P] TO [CBMSApplication]
GO
