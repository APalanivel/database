SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ACCOUNT_NUMBER_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@RFP_ID        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_RFP_GET_ACCOUNT_NUMBER_INFO_P
@RFP_ID INT

AS
set nocount on
select 
	a.account_id,
	a.account_number	
	
from 
	
	sr_rfp_account rfp_account,
	client c, 
	division d,
	site s, 
	account a
	 
where 
	rfp_account.sr_rfp_id = @RFP_ID	
	and a.account_id = rfp_account.account_id	
	and rfp_account.sr_rfp_bid_group_id is null
	and rfp_account.is_deleted = 0
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id

union

select 	distinct
	bid_group.sr_rfp_bid_group_id as group_id, 
	bid_group.group_name as group_name
	

from 
	--sr_rfp rfp,
	sr_rfp_account rfp_account,
	sr_rfp_bid_group bid_group,
	client c, 
	division d,
	site s, 
	account a

where 
	rfp_account.sr_rfp_id = @RFP_ID
	and rfp_account.sr_rfp_bid_group_id is not null
	and rfp_account.is_deleted = 0
	and bid_group.sr_rfp_bid_group_id = rfp_account.sr_rfp_bid_group_id
	and a.account_id = rfp_account.account_id
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_NUMBER_INFO_P] TO [CBMSApplication]
GO
