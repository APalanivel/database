SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_ONBOARD_CLIENT_NAMES_P 
@userId varchar(10),
@sessionId varchar(20)
as
set nocount on
select	DISTINCT client.CLIENT_ID,
	client.CLIENT_NAME

from	CLIENT client,
	RM_ONBOARD_CLIENT onboard

where client.CLIENT_ID=onboard.CLIENT_ID

order by client.CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_CLIENT_NAMES_P] TO [CBMSApplication]
GO
