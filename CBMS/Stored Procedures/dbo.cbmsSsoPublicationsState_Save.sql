SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   procedure [dbo].[cbmsSsoPublicationsState_Save]
	( @MyAccountId int
	, @sso_publication_id int
	, @state_id int
	)
AS
BEGIN

	set nocount on

	declare @RecordCount int

	  select @RecordCount = count(*)
	    from sso_publications_state_map
	   where sso_publication_id = @sso_publication_id
	     and state_id = @state_id

	if @RecordCount = 0
	begin

		insert into sso_publications_state_map
			( sso_publication_id
			, state_id
			)
		values
			(@sso_publication_id
			,@state_id
			)
	end

--	exec cbmsGroupInfoPermissionInfo_Get @MyAccountId, @group_info_id, @permission_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPublicationsState_Save] TO [CBMSApplication]
GO
