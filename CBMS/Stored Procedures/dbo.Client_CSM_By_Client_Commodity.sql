SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******            
NAME:            
 dbo.Client_CSM_By_Client_Commodity             
   
DESCRIPTION:            
 This stored procedure is application specific as we require empty columns to bind with the datagrid, the same datagrid is used for the site and division/sitegroup    
   
    
INPUT PARAMETERS:            
Name     DataType Default Description            
------------------------------------------------------------            
@Client_Commodity_Id INT,                
  
              
OUTPUT PARAMETERS:            
Name     DataType Default Description            
------------------------------------------------------------            
USAGE EXAMPLES:            
------------------------------------------------------------          
select distinct client_commodity_id from core.Client_Commodity_Detail        
  
EXEC dbo.Client_CSM_By_Client_Commodity 49        
EXEC dbo.Client_CSM_By_Client_Commodity 50        
EXEC dbo.Client_CSM_By_Client_Commodity 52        
  
    
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
GB  Geetansu Behera            
CMH Chad Hattabaugh        
HG  Hari           
SS  Subhash Subramanyam        
   
MODIFICATIONS             
Initials Date		Modification            
------------------------------------------------------------            
GB					Created          
SS					Removed	Subselect      
SKA  22-Jul-2009	Modified as per coding standard      
          
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Client_CSM_By_Client_Commodity
      (
       @Client_Commodity_Id AS INT
      )
AS 

BEGIN
      SET  NOCOUNT ON ;
    
      SELECT
            '' AS SITE_ID
           ,'' AS Site_Name
           ,'' AS DIVISION_ID
           ,'' AS DIVISION_NAME
           ,c.CLIENT_ID
           ,c.CLIENT_NAME
           ,ccd.Is_GHG_Reported
           ,ccd.UOM_Cd
           ,c.NOT_MANAGED
           ,Client_Commodity_Detail_Id
      FROM
            CLIENT AS c
            INNER JOIN Core.Client_Commodity_Detail AS ccd ON c.CLIENT_ID = ccd.CLIENT_ID
      WHERE
            ccd.Client_Commodity_Id = @Client_Commodity_Id
      ORDER BY
            c.CLIENT_ID
END
GO
GRANT EXECUTE ON  [dbo].[Client_CSM_By_Client_Commodity] TO [CBMSApplication]
GO
