SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_SAD_GET_SUPPLIER_CONTACT_PROFILE_P

DESCRIPTION: 


INPUT PARAMETERS:    
		Name            DataType          Default     Description    
---------------------------------------------------------------------------------    
		@userid			int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.SR_SAD_GET_SUPPLIER_CONTACT_PROFILE_P 149

	EXEC SR_SAD_GET_SUPPLIER_CONTACT_PROFILE_P 3808


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	GK			Gopi Konga
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
    DMR			09/10/2010  Modified for Quoted_Identifier
    GK			05/09/2011  MAINT-588 Fixes the code to remove the passcode column returned by this SP
							Passcode removed as it is not used by application
							Removed the unused variables
	RR			2015-07-08	Global sourcing - Added new columns to select list
	RR			2015-11-16	Global sourcing - Phase2 - Added logic to check the contacts for the selecting user are default or not
	RR			2018-06-25	Global Risk Management - Added Contact_Type_Cd(dbo.SR_SUPPLIER_CONTACT_INFO) field to select list
*******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_SUPPLIER_CONTACT_PROFILE_P]
    (
        @userid INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Termination_Contact_Email_Address NVARCHAR(MAX)
            , @Registration_Contact_Email_Address NVARCHAR(MAX)
            , @Vendor_Id INT;

        SELECT
            @Vendor_Id = VENDOR_ID
        FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP
        WHERE
            SR_SUPPLIER_CONTACT_INFO_ID = @userid;


        SELECT
            @Termination_Contact_Email_Address = ssci.Termination_Contact_Email_Address
        FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
        WHERE
            scvm.VENDOR_ID = @Vendor_Id
            AND ssci.Is_Default_Termination_Contact_Email_For_Vendor = 1;

        SELECT
            @Registration_Contact_Email_Address = ssci.Registration_Contact_Email_Address
        FROM
            dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
        WHERE
            scvm.VENDOR_ID = @Vendor_Id
            AND ssci.Is_Default_Registration_Contact_Email_For_Vendor = 1;

        SELECT
            ui.USERNAME AS USER_NAME
            , ui.FIRST_NAME AS FIRST_NAME
            , ui.MIDDLE_NAME AS MIDDLE_NAME
            , ui.LAST_NAME AS LAST_NAME
            , ssci.WORK_PHONE AS WORK_PHONE
            , ssci.CELL_PHONE AS CELL_PHONE
            , ssci.FAX AS FAX
            , ui.EMAIL_ADDRESS AS EMAIL_ADDRESS
            , ssci.PRIVILEGE_TYPE_ID
            , ssci.IS_RFP_PREFERENCE_WEBSITE
            , ssci.IS_RFP_PREFERENCE_EMAIL
            , ssci.VENDOR_TYPE_ID
            , v.VENDOR_ID
            , v.VENDOR_NAME
            , ssci.EMAIL_SENT_DATE
            , ui.country_id
            , ui.locale_code
            , ssci.Termination_Contact_Email_Address
            , CASE WHEN @Termination_Contact_Email_Address = ssci.Termination_Contact_Email_Address THEN 1
                  ELSE 0
              END AS Is_Default_Termination_Contact_Email_For_Vendor
            , ssci.Registration_Contact_Email_Address
            , CASE WHEN @Registration_Contact_Email_Address = ssci.Registration_Contact_Email_Address THEN 1
                  ELSE 0
              END AS Is_Default_Registration_Contact_Email_For_Vendor
            , comm.Comment_Text
            , LEFT(ct.Contact_Type_Cd, LEN(ct.Contact_Type_Cd) - 1) AS Contact_Type_Cd
        FROM
            dbo.SR_SUPPLIER_CONTACT_INFO ssci
            INNER JOIN dbo.USER_INFO ui
                ON ssci.USER_INFO_ID = ui.USER_INFO_ID
            LEFT JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP map
                ON map.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                   AND  map.IS_PRIMARY = 1
            LEFT JOIN dbo.VENDOR v
                ON map.VENDOR_ID = v.VENDOR_ID
            LEFT JOIN dbo.Comment comm
                ON ssci.Comment_Id = comm.Comment_ID
            CROSS APPLY (   SELECT
                                CAST(ssctm.Contact_Type_Cd AS VARCHAR(100)) + ', '
                            FROM
                                dbo.Sr_Supplier_Contact_Type_Map ssctm
                            WHERE
                                ssctm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                            GROUP BY
                                ssctm.Contact_Type_Cd
                            FOR XML PATH('')) ct(Contact_Type_Cd)
        WHERE
            (ssci.SR_SUPPLIER_CONTACT_INFO_ID = @userid);

    END;




GO


GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SUPPLIER_CONTACT_PROFILE_P] TO [CBMSApplication]
GO
