SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.User_Push_Notification_Bulk_Del            
                        
 DESCRIPTION:                        
			To Bulk delete  the User_Push_Notification table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Notification_Type_Cd_List  VARCHAR(MAX)               
 @User_Info_Id_List          VARCHAR(MAX)       
                   
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 BEGIN TRAN
 
 SELECT ui.USER_INFO_ID,Notification_Type_Cd,c.Code_Value FROM dbo.USER_INFO ui INNER JOIN dbo.User_Push_Notification upn 
		ON ui.USER_INFO_ID = upn.User_Info_Id INNER JOIN dbo.Code c ON c.Code_Id=upn.Notification_Type_Cd
		WHERE ui.USER_INFO_ID IN (31066,61157,61174)         
		      
 EXEC [dbo].[User_Push_Notification_Bulk_Del] 
      @User_Info_Id_List = '31066,61157,61174'
     ,@Notification_Type_Cd_List = '102128,102129'
     
     
 SELECT ui.USER_INFO_ID,Notification_Type_Cd,c.Code_Value FROM dbo.USER_INFO ui INNER JOIN dbo.User_Push_Notification upn 
		ON ui.USER_INFO_ID = upn.User_Info_Id INNER JOIN dbo.Code c ON c.Code_Id=upn.Notification_Type_Cd
		WHERE ui.USER_INFO_ID IN (31066,61157,61174)  

           
 ROLLBACK TRAN	      


                            
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 NR                    Narayana Reddy         
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                    2016-08-12       Created For MAINT-4014(4237).            
                       
******/    
 
CREATE PROCEDURE [dbo].[User_Push_Notification_Bulk_Del]
      ( 
       @User_Info_Id_List VARCHAR(MAX)
      ,@Notification_Type_Cd_List VARCHAR(MAX) = NULL )
AS 
BEGIN                
      SET NOCOUNT ON;   
                
                    
      DECLARE @Notification_Type_Cd_Tbl TABLE
            ( 
             Notification_Type_Cd INT PRIMARY KEY CLUSTERED ) 

      DECLARE @User_Info_Id_Tbl TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )                                 
                  
                  
      INSERT      INTO @Notification_Type_Cd_Tbl
                  ( 
                   Notification_Type_Cd )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Notification_Type_Cd_List, ',') ufn      


      INSERT      INTO @User_Info_Id_Tbl
                  ( 
                   User_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@User_Info_Id_List, ',') ufn                                     
  
      BEGIN TRY                        
            BEGIN TRANSACTION     
            
            
            DELETE
                  upn
            FROM
                  @Notification_Type_Cd_Tbl ntc
                  CROSS APPLY @User_Info_Id_Tbl ui
                  INNER JOIN dbo.User_Push_Notification upn
                        ON upn.Notification_Type_Cd = ntc.Notification_Type_Cd
                           AND upn.User_Info_Id = ui.User_Info_Id 
                        
                 
                       
            COMMIT TRANSACTION                                 
                                   
                                   
      END TRY                    
      BEGIN CATCH                    
            IF @@TRANCOUNT > 0 
                  BEGIN    
                        ROLLBACK TRANSACTION    
                  END                   
            EXEC dbo.usp_RethrowError                    
      END CATCH  
                       
END;

;
GO
GRANT EXECUTE ON  [dbo].[User_Push_Notification_Bulk_Del] TO [CBMSApplication]
GO
