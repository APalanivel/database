
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.GET_EXPECTED_VOLUME_FOR_SCENARIO_REPORT_P  
  
DESCRIPTION:
		
 
INPUT PARAMETERS:
	Name				DataType		Default			Description    
---------------------------------------------------------------------   
	@userId				VARCHAR
    @sessionId			VARCHAR
    @fromDate			VARCHAR(12)
    @toDate				VARCHAR(12)
    @fromYear			VARCHAR(12)
    @toYear				VARCHAR(12)
    @clientId			INTEGER
    @divisionId			INTEGER
    @siteId				INTEGER
    @conversionId		INTEGER
    @Country_Name		VARCHAR(200)	NULL

OUTPUT PARAMETERS:    
	Name				DataType		Default		Description    
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,10011,'8-1-2008','11-1-2008'
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,10011,'8-1-2008','11-1-2008','usa'
	
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,10011,'8-1-2007','11-1-2007'
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,10011,'8-1-2007','11-1-2007','usa'

	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,235,'1-1-2007','12-1-2007'
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,235,'1-1-2007','12-1-2007','usa'
	
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,235,'1-1-2013','12-1-2013'
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 1,1,235,'1-1-2013','12-1-2013','usa'
	
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P 0,0,11728,'07/01/2014','06/01/2015','usa'
	EXEC dbo.GET_DEALTICKET_DETAILS_FOR_SUMMIT_POSITION_REPORT_P 0,0,'07/01/2014','06/01/2015',11728,0,0,4,25,0,'usa'



AUTHOR INITIALS:
	Initials	Name 
---------------------------------------------------------------------
	RR			Raghu Reddy
  
MODIFICATIONS  
   
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2014-11-27  Added description header
							New optional input parameter @Country_Name added
	RR			2015-02-18	MAINT-3310 Script returning all deal type(fixed_price,options,trigger) deal tickets irrespective of 
							trigger status, but if the deal type is 'trigger' then the script should select deal tickets with 
							trigger status 'fixed' or 'closed' only
	
******/
CREATE PROCEDURE [dbo].[GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P]
      @userId VARCHAR(10)
     ,@sessionId VARCHAR(20)
     ,@clientId INTEGER
     ,@startDate DATETIME
     ,@endDate DATETIME
     ,@Country_Name VARCHAR(200) = NULL
AS 
SET NOCOUNT ON;

DECLARE
      @HEDGE_MODE_TYPE_NYMEX INT
     ,@Trigger INT

SELECT
      @HEDGE_MODE_TYPE_NYMEX = ENTITY_ID
FROM
      ENTITY
WHERE
      ENTITY_NAME = 'NYMEX'
      AND ENTITY_TYPE = 263
      
SELECT
      @Trigger = ENTITY_ID
FROM
      ENTITY
WHERE
      ENTITY_TYPE = 268
      AND ENTITY_NAME = 'trigger'     
                                          
SELECT
      rmdt.RM_DEAL_TICKET_ID
     ,ent.ENTITY_NAME
     ,ent1.ENTITY_NAME
     ,ent2.ENTITY_NAME
FROM
      RM_DEAL_TICKET rmdt
      INNER JOIN RM_DEAL_TICKET_DETAILS rmdt_dtl
            ON rmdt_dtl.RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID
      INNER JOIN ENTITY ent
            ON ent.ENTITY_ID = rmdt.PRICING_REQUEST_TYPE_ID
      INNER JOIN ENTITY ent1
            ON ent1.ENTITY_ID = rmdt.DEAL_TYPE_ID
      INNER JOIN ENTITY ent2
            ON ent2.ENTITY_ID = rmdt.ACTION_REQUIRED_TYPE_ID
      INNER JOIN RM_DEAL_TICKET_TRANSACTION_STATUS trans
            ON trans.RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID
      INNER JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
            ON rmdt_dtl.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
      INNER JOIN ENTITY ent5
            ON trans.DEAL_TRANSACTION_STATUS_TYPE_ID = ent5.ENTITY_ID
      INNER JOIN dbo.ENTITY Deal_Type
            ON rmdt.DEAL_TYPE_ID = Deal_Type.ENTITY_ID
WHERE
      rmdt.CLIENT_ID = @clientId
      AND rmdt.HEDGE_MODE_TYPE_ID = @HEDGE_MODE_TYPE_NYMEX
      AND rmdt_dtl.month_identifier BETWEEN @startDate
                                    AND     @endDate
      AND ent5.entity_name = 'Order Executed'
      AND EXISTS ( SELECT
                        1
                   FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                              ON ch.Site_Id = hedge.SITE_ID
                   WHERE
                        ch.CLIENT_ID = @clientId
                        AND ( @Country_Name IS NULL
                              OR ch.Country_Name = @Country_Name )
                        AND hedge.INCLUDE_IN_REPORTS = 1
                        AND dealvolume.SITE_ID = ch.SITE_ID )
      AND Deal_Type.ENTITY_TYPE = 268
      AND Deal_Type.ENTITY_NAME IN ( 'Fixed Price', 'Options' )
GROUP BY
      rmdt.RM_DEAL_TICKET_ID
     ,ent.ENTITY_NAME
     ,ent1.ENTITY_NAME
     ,ent2.ENTITY_NAME
UNION
SELECT
      rmdt.RM_DEAL_TICKET_ID
     ,ent.ENTITY_NAME
     ,ent1.ENTITY_NAME
     ,ent2.ENTITY_NAME
FROM
      dbo.RM_DEAL_TICKET rmdt
      INNER JOIN dbo.RM_DEAL_TICKET_DETAILS rmdt_dtl
            ON rmdt_dtl.RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID
      INNER JOIN dbo.ENTITY ent
            ON ent.ENTITY_ID = rmdt.PRICING_REQUEST_TYPE_ID
      INNER JOIN dbo.ENTITY ent1
            ON ent1.ENTITY_ID = rmdt.DEAL_TYPE_ID
      INNER JOIN dbo.ENTITY ent2
            ON ent2.ENTITY_ID = rmdt.ACTION_REQUIRED_TYPE_ID
      INNER JOIN dbo.RM_DEAL_TICKET_VOLUME_DETAILS dealvolume
            ON rmdt_dtl.RM_DEAL_TICKET_DETAILS_ID = dealvolume.RM_DEAL_TICKET_DETAILS_ID
      INNER JOIN dbo.ENTITY Trigger_Status
            ON rmdt_dtl.TRIGGER_STATUS_TYPE_ID = Trigger_Status.ENTITY_ID
WHERE
      rmdt.CLIENT_ID = @clientId
      AND rmdt.HEDGE_MODE_TYPE_ID = @HEDGE_MODE_TYPE_NYMEX
      AND rmdt_dtl.month_identifier BETWEEN @startDate
                                    AND     @endDate
      AND rmdt.DEAL_TYPE_ID = @Trigger
      AND Trigger_Status.ENTITY_TYPE = 287
      AND Trigger_Status.ENTITY_NAME IN ( 'fixed', 'closed' )
      AND EXISTS ( SELECT
                        1
                   FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.RM_ONBOARD_HEDGE_SETUP hedge
                              ON ch.Site_Id = hedge.SITE_ID
                   WHERE
                        ch.CLIENT_ID = @clientId
                        AND ( @Country_Name IS NULL
                              OR ch.Country_Name = @Country_Name )
                        AND hedge.INCLUDE_IN_REPORTS = 1
                        AND dealvolume.SITE_ID = ch.SITE_ID )
GROUP BY
      rmdt.RM_DEAL_TICKET_ID
     ,ent.ENTITY_NAME
     ,ent1.ENTITY_NAME
     ,ent2.ENTITY_NAME



;
;
GO


GRANT EXECUTE ON  [dbo].[GET_DEALTICKET_DETAILS_FOR_SCENARIO_REPORT_P] TO [CBMSApplication]
GO
