SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
                    
NAME: [dbo].[Site_Id_Sel_By_Client_Hier_Id]  
                      
DESCRIPTION:                      
    To get Sites Based on Client_Hier_Ids.                      
                      
INPUT PARAMETERS:      
                     
Name                              DataType            Default        Description      
---------------------------------------------------------------------------------------------------------------    
@Client_Hier_Id                   VARCHAR(MAX)            
                      
OUTPUT PARAMETERS:        
                        
Name                              DataType            Default        Description      
---------------------------------------------------------------------------------------------------------------    
                      
USAGE EXAMPLES:                          
---------------------------------------------------------------------------------------------------------------                            
              
EXEC dbo.Site_Id_Sel_By_Client_Hier_Id   
      @Client_Hier_Id = '1235,1236,1237'   
    
                     
AUTHOR INITIALS:      
                    
Initials                Name      
---------------------------------------------------------------------------------------------------------------    
SP                      Sandeep Pigilam        
                       
MODIFICATIONS:     
                      
Initials                Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 SP                     2014-01-10      Created for RA Admin user management                    
                     
******/

CREATE PROCEDURE [dbo].[Site_Id_Sel_By_Client_Hier_Id] (@Client_Hier_Id VARCHAR(MAX))
AS
BEGIN

    SET NOCOUNT ON;


    DECLARE @Client_Hier_Ids TABLE
    (
        Client_Hier_Id INT
    );


    INSERT INTO @Client_Hier_Ids
    (
        Client_Hier_Id
    )
    SELECT CAST(ufn.Segments AS INT) AS Client_Hier_Id
    FROM dbo.ufn_split(@Client_Hier_Id, ',') ufn
    GROUP BY ufn.Segments;



    SELECT ch.Site_Id,
           ch.Client_Hier_Id,
           RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name,
           ch.Client_Name
    FROM Core.Client_Hier ch
        INNER JOIN @Client_Hier_Ids tempids
            ON ch.Client_Hier_Id = tempids.Client_Hier_Id
    WHERE Site_Id > 0;




END;
GO
GRANT EXECUTE ON  [dbo].[Site_Id_Sel_By_Client_Hier_Id] TO [CBMSApplication]
GO
