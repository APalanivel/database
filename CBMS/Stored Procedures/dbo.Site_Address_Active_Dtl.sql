SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.Site_Address_Active_Dtl    
    
DESCRIPTION:    
  To get the list of active sites with adrress.  
    
INPUT PARAMETERS:    
 Name    DataType   Default     Description    
----------------------------------------------------------------------------    
  
OUTPUT PARAMETERS:    
 Name    DataType   Default     Description    
----------------------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
----------------------------------------------------------------------------    
  
EXEC dbo.Site_Address_Active_Dtl   
      @Start_Index = 1  
     ,@End_Index = 25  
  
    
AUTHOR INITIALS:    
 Initials Name    
----------------------------------------------------------------------------    
 NR   Narayana Reddy     
     
MODIFICATIONS    
    
 Initials  Date  Modification    
------------------------------------------------------------    
 NR      201-02-28 Created For Address Enhancement.  
           
******/    
  
CREATE  PROCEDURE [dbo].[Site_Address_Active_Dtl]
      (
       @Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Is_IDM_Enabled BIT = 0 )
AS
BEGIN  
      SET NOCOUNT ON;  
        
      DECLARE @Total_Row_Count INT;  
        
        
      
      SELECT
            @Total_Row_Count = COUNT(1)
      FROM
            dbo.SITE s
            INNER JOIN dbo.ADDRESS a
                  ON s.PRIMARY_ADDRESS_ID = a.ADDRESS_ID
                     AND a.GEO_LAT IS NOT NULL
                     AND a.GEO_LONG IS NOT NULL
      WHERE
            s.NOT_MANAGED = 0
            AND s.Time_Zone_Id IS NULL;  
              
              
      WITH  Cte_Site_List
              AS ( SELECT
                        s.SITE_ID
                       ,s.SITE_NAME
                       ,s.NOT_MANAGED
                       ,a.ADDRESS_ID
                       ,a.STATE_ID
                       ,a.ADDRESS_LINE1
                       ,a.ADDRESS_LINE2
                       ,a.CITY
                       ,a.ZIPCODE
                       ,a.PHONE_NUMBER
                       ,s.Weather_Station_Code
                       ,s.Is_System_Generated_Weather_Station
                       ,s.Time_Zone_Id
                       ,s.Is_System_Generated_Timezone
                       ,a.GEO_LAT
                       ,a.GEO_LONG
                       ,a.Is_System_Generated_Geocode
                       ,ROW_NUMBER() OVER ( ORDER BY SITE_NAME ) AS Row_Num
                   FROM
                        dbo.SITE s
                        INNER JOIN dbo.ADDRESS a
                              ON s.PRIMARY_ADDRESS_ID = a.ADDRESS_ID
                   WHERE
                        s.NOT_MANAGED = 0
                        AND s.Time_Zone_Id IS NULL
                        AND a.GEO_LAT IS NOT NULL
                        AND a.GEO_LONG IS NOT NULL
                        AND s.Is_IDM_Enabled = @Is_IDM_Enabled)
            SELECT
                  SITE_ID
                 ,SITE_NAME
                 ,NOT_MANAGED
                 ,ADDRESS_ID
                 ,STATE_ID
                 ,ADDRESS_LINE1
                 ,ADDRESS_LINE2
                 ,CITY
                 ,ZIPCODE
                 ,PHONE_NUMBER
                 ,Weather_Station_Code
                 ,Is_System_Generated_Weather_Station
                 ,Time_Zone_Id
                 ,Is_System_Generated_Timezone
                 ,GEO_LAT
                 ,GEO_LONG
                 ,Row_Num
                 ,Is_System_Generated_Geocode
                 ,@Total_Row_Count AS Total_Count
            FROM
                  Cte_Site_List c
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index;  
                    
                    
END;
;
GO
GRANT EXECUTE ON  [dbo].[Site_Address_Active_Dtl] TO [CBMSApplication]
GO
