SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Consumption_Level_Variance_Test_SEL_Commodity  
 
DESCRIPTION:  Used to select commodities based on VARIANCE_CONSUMPTION_LEVEL table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------     
        
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	EXEC dbo.Consumption_Level_Variance_Test_SEL_Commodity
	 
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials	Date		Modification  
------------------------------------------------------------  
NK			10/13/2009  Created
HG			02/25/2010	Order By clause added to sort it by EP and NG first and remaining by alphabet order
						Used Group By instead of Distinct Clause

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Consumption_Level_Variance_Test_SEL_Commodity
AS

BEGIN

	SET NOCOUNT ON;

	SELECT 
		com.COMMODITY_ID
		,com.COMMODITY_NAME
		,com.Is_Alternate_Fuel
	FROM
		dbo.VARIANCE_CONSUMPTION_LEVEL vcl
		JOIN dbo.COMMODITY com
			ON com.COMMODITY_ID = vcl.COMMODITY_ID
	GROUP BY
		com.COMMODITY_ID
		,com.COMMODITY_NAME
		,com.Is_Alternate_Fuel
	ORDER BY
		com.Is_Alternate_Fuel ASC
		,com.Commodity_Name
		
END
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Variance_Test_SEL_Commodity] TO [CBMSApplication]
GO
