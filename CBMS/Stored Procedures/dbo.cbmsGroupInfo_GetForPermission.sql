SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[cbmsGroupInfo_GetForPermission] 
	( @MyAccountId int
	, @permission_info_id int
	)
AS
BEGIN

		    select g.group_info_id
		  , g.group_name
		  , is_checked = case when map.group_info_id is null then 0 else 1 end
		      from group_info g
		  left outer join group_info_permission_info_map map on map.group_info_id = g.group_info_id and map.permission_info_id = @permission_info_id
      
		  order by g.group_name asc


END
GO
GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetForPermission] TO [CBMSApplication]
GO
