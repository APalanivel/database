
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:    [dbo].[GrantExecute] 

NOTE:  THIS STORED PROCEDURE SHOULD BE THE SAME ACROSS ALL DATABASES AND ALL ENVIRONMENTS.
    
DESCRIPTION:          
Grants Execute permission to all non-system level objects to the application role as appropriate.
Only the ETL schema has been removed from DV2/CBMS application role.  This has been moved into it's
own Schema.	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC GrantExecute
       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
DMR			Deana Ritter
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
DMR			A long time ago.	Created.
DMR			5/10/2010		Modified to adjust for ETL schemas and use Sys.Database_Principals to pull the
							role name, making this environment/DB generic.
DMR         04/15/2011     Modified to adust for variable role name of ETL.  This will map any ETL roles
                           to any ETL schema.				
			
*****/ 

CREATE PROCEDURE [dbo].[GrantExecute]
AS 
BEGIN
      SET nocount ON

      DECLARE @object NVARCHAR(300)
      DECLARE @Role NVARCHAR(55)
            
      SELECT TOP 1
            @Role = Name
      FROM
            Sys.Database_Principals
      WHERE
            Type = 'R'
            AND Name LIKE left(db_name(), 3) + '%'

      DECLARE cGrant CURSOR fast_forward
      FOR
      SELECT
            case Type
              WHEN 'TF' THEN 'Grant Select on ' + schema_name([SCHEMA_ID]) + '.' + Name + ' To ' + @Role + ';'
              ELSE 'Grant Execute on ' + schema_name([SCHEMA_ID]) + '.' + Name + ' To ' + @Role + ';'
            END AS Grant_Statement
      FROM
            Sys.Objects O
      WHERE
            type IN ( 'fn', 'tf', 'p' )
            AND NOT EXISTS ( SELECT
                              object_name(id)
                             ,user_name(uid)
                             ,*
                             FROM
                              sysprotects P
                             WHERE
                              user_name(uid) = @Role
                              AND O.object_id = p.id )
            AND Name NOT LIKE 'sp%'
            AND Name <> 'GrantExecute'
            AND schema_name([SCHEMA_ID]) <> 'ETL'
      OPEN cGrant
      FETCH NEXT FROM cGrant INTO @object
      WHILE @@fetch_status = 0 
            BEGIN
                  EXEC ( @object )
                  FETCH NEXT FROM cGrant INTO @object

            END
      CLOSE cGrant
      DEALLOCATE cGrant

      IF EXISTS ( SELECT
                        1
                  FROM
                        Sys.Database_Principals
                  WHERE
                        Name LIKE 'ETL%'
                        AND Type = 'R' ) 
            BEGIN
                  
                  SELECT TOP 1
                        @Role = Name
                  FROM
                        Sys.Database_Principals
                  WHERE
                        Type = 'R'
                        AND Name LIKE 'ETL%'

                  DECLARE cGrant CURSOR fast_forward
                  FOR
                  SELECT
                        case Type
                          WHEN 'TF' THEN 'Grant Select on ' + schema_name([SCHEMA_ID]) + '.' + Name + ' To ' + @Role + ';'
                          ELSE 'Grant Execute on ' + schema_name([SCHEMA_ID]) + '.' + Name + ' To ' + @Role + ';'
                        END AS Grant_Statement
                  FROM
                        Sys.Objects O
                  WHERE
                        type IN ( 'fn', 'tf', 'p' )
                        AND NOT EXISTS ( SELECT
                                          object_name(id)
                                         ,user_name(uid)
                                         ,*
                                         FROM
                                          sysprotects P
                                         WHERE
                                          user_name(uid) = 'ETL_Execute'
                                          AND O.object_id = p.id )
                        AND Name NOT LIKE 'sp%'
                        AND Name <> 'GrantExecute'
                        AND schema_name([SCHEMA_ID]) = 'ETL'             
                  OPEN cGrant
                  FETCH NEXT FROM cGrant INTO @object
                  WHILE @@fetch_status = 0 
                        BEGIN
                              EXEC ( @object )
                              FETCH NEXT FROM cGrant INTO @object

                        END
                  CLOSE cGrant
                  DEALLOCATE cGrant
            END


END



GO
