SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Contract_Budget_Detail_Del]  

DESCRIPTION: It Deletes Budget Contract Budget Detail for Selected Budget_Contract_Budget_Detail_Id.     
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Budget_Contract_Budget_Detail_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
		EXEC Budget_Contract_Budget_Detail_Del 17218
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Contract_Budget_Detail_Del
    (
      @Budget_Contract_Budget_Detail_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.BUDGET_CONTRACT_BUDGET_DETAIL
	WHERE
		Budget_Contract_Budget_Detail_Id = @Budget_Contract_Budget_Detail_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Contract_Budget_Detail_Del] TO [CBMSApplication]
GO
