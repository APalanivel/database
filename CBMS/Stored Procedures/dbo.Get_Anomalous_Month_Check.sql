SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*********     
NAME:    [dbo].[Get_Anomalous_Month_Check]  
   
DESCRIPTION:  Used to select code value from code table using the code name    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 
 EXEC [dbo].[Get_Anomalous_Month_Check]     
  
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
TRK Ramakrishna Thummala  
AP Arunkumar Palanivel  
  
Initials Date  Modification    
------------------------------------------------------------    

AP Feb 27,2020
  
******/
CREATE PROCEDURE [dbo].[Get_Anomalous_Month_Check]

(@Account_id INT,
@service_month DATE)
    
AS
    BEGIN

        SET NOCOUNT ON;

        IF EXISTS (SELECT 1 FROM dbo.Variance_Account_Month_Anomalous_Status WHERE Account_Id = @Account_id AND Service_Month = @service_month AND Is_Anomalous =1)
		BEGIN 
		SELECT 1 AS anomalous
		END
        ELSE 
		SELECT 0 AS anomalous
		END

GO
GRANT EXECUTE ON  [dbo].[Get_Anomalous_Month_Check] TO [CBMSApplication]
GO
