SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.SR_RFP_UPDATE_TERM_PRODUCT_FROM_SV_P (
	@curr_day datetime,
	@is_no_bid bit,
	@is_no_bid_comments varchar(4000),
	@account_term_id int,
	@product_id int,
	@vendormapid int,
	@bidid int,
	@saved_trem_prod_map_id int
)as
	set nocount on
	/*
	UPDATE SR_RFP_TERM_PRODUCT_MAP	
	set	BID_DATE = @curr_day ,
		IS_NO_BID = @is_no_bid ,
		NO_BID_COMMENTS = @is_no_bid_comments
	WHERE 	SR_RFP_ACCOUNT_TERM_ID = @account_term_id 
		and SR_RFP_SELECTED_PRODUCTS_ID = @product_id
		and SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @vendormapid
		and SR_RFP_BID_ID = @bidid 
		--AND BID_DATE IS NOT NULL
	*/
	if @saved_trem_prod_map_id > 0
			begin
				UPDATE SR_RFP_TERM_PRODUCT_MAP	
				set	BID_DATE = @curr_day ,
					IS_NO_BID = @is_no_bid ,
					NO_BID_COMMENTS = @is_no_bid_comments
				WHERE 	SR_RFP_ACCOUNT_TERM_ID = @account_term_id 
					and SR_RFP_SELECTED_PRODUCTS_ID = @product_id
					and SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @vendormapid
					and SR_RFP_BID_ID = @bidid 
					AND SR_RFP_TERM_PRODUCT_MAP_ID = @saved_trem_prod_map_id
			end
		ELSE
			BEGIN
				UPDATE SR_RFP_TERM_PRODUCT_MAP	
				set	BID_DATE = @curr_day ,
					IS_NO_BID = @is_no_bid ,
					NO_BID_COMMENTS = @is_no_bid_comments
				WHERE 	SR_RFP_ACCOUNT_TERM_ID = @account_term_id 
					and SR_RFP_SELECTED_PRODUCTS_ID = @product_id
					and SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @vendormapid
					and SR_RFP_BID_ID = @bidid 
			END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_TERM_PRODUCT_FROM_SV_P] TO [CBMSApplication]
GO
