SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_LP_STATUS_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srRfpAccountId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_LP_STATUS_TYPE_P

@srRfpAccountId int

as
set nocount on
DECLARE @rfpLPStatusTypeId int
SELECT @rfpLPStatusTypeId = (
	select	status_type_id 
	from	sr_rfp_lp_account_summary
	where	sr_account_group_id = @srRfpAccountId
)


RETURN  @rfpLPStatusTypeId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_LP_STATUS_TYPE_P] TO [CBMSApplication]
GO
