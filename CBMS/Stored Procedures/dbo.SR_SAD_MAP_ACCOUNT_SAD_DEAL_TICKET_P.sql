SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_MAP_ACCOUNT_SAD_DEAL_TICKET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@accountId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_SAD_MAP_ACCOUNT_SAD_DEAL_TICKET_P
@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int,
@accountId int

AS
set nocount on
insert into SR_DEAL_TICKET_ACCOUNT_MAP (SR_DEAL_TICKET_ID, ACCOUNT_ID)
values (@dealTicketId, @accountId)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_MAP_ACCOUNT_SAD_DEAL_TICKET_P] TO [CBMSApplication]
GO
