SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].Site_Dtl_Sel_By_Division_Id  
     
DESCRIPTION: 

	To get site detail for the given division id
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Division_Id	INT	
	@Start_Index	INT			1
    @End_Index		INT			2147483647		  		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	EXEC dbo.Site_Dtl_Sel_By_Division_Id 95,1,25
	
	EXEC dbo.Site_Dtl_Sel_By_Division_Id 298,1,15
	
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JULY-10	CREATED   
  
*/  

CREATE PROCEDURE dbo.Site_Dtl_Sel_By_Division_Id
       (
         @Division_Id INT
       , @Start_Index INT = 1
       , @End_Index INT = 2147483647
       )
AS 
       BEGIN

             SET NOCOUNT ON ;

             WITH   Cte_Site_List
                      AS ( SELECT
                            Site_Name
                          , s.PRIMARY_ADDRESS_ID
                          , Row_Num = ROW_NUMBER() OVER ( ORDER BY s.Site_Name )
                          , Total_Rows = COUNT(1) OVER ( )
                           FROM
                                                         dbo.Site s
                           WHERE
                                                         s.DIVISION_ID = @Division_Id
                         )
                  SELECT
                    s.Site_Name
                  , addr.CITY
                  , st.STATE_NAME
                  , s.Total_Rows
                  FROM
                    Cte_Site_List s
                    JOIN dbo.Address addr
                        ON addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
                    JOIN dbo.STATE st
                        ON st.STATE_ID = addr.STATE_ID
                  WHERE
                    s.Row_Num BETWEEN @Start_Index AND @End_Index

       END
GO
GRANT EXECUTE ON  [dbo].[Site_Dtl_Sel_By_Division_Id] TO [CBMSApplication]
GO
