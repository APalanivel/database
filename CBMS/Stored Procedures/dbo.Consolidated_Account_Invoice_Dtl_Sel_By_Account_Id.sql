SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
                                   
 NAME: dbo.Invoice_Dtl_Sel_By_Supplier_Account_Contract_Id                               
                                    
 DESCRIPTION:                
  Get the Suppler account associated invoices count based on Supplier Account.                             
                                    
 INPUT PARAMETERS:                
                                   
 Name                               DataType          Default       Description                
-------------------------------------------------------------------------------------            
 @Sup_Account_Id   INT                  
                                    
 OUTPUT PARAMETERS:                
                                         
 Name                               DataType          Default       Description                
-------------------------------------------------------------------------------------                                      
 USAGE EXAMPLES:                                        
-------------------------------------------------------------------------------------                           
          
         
 EXEC dbo.Consolidated_Account_Invoice_Dtl_Sel_By_Account_Id  16680    
 
  EXEC dbo.Consolidated_Account_Invoice_Dtl_Sel_By_Account_Id  1651029    
        
          
                             
                                   
 AUTHOR INITIALS:              
                 
 Initials                   Name                
-------------------------------------------------------------------------------------            
 NR                     Narayana Reddy                                      
                                     
 MODIFICATIONS:              
                                     
 Initials               Date            Modification              
-------------------------------------------------------------------------------------            
 NR                     2019-06-13      Created for ADD Contract.  
 NR						2019-12-26		MAINT-9659 - To get the account/contract unmapped invoice list.                   
                                
                                   
******/

CREATE PROCEDURE [dbo].[Consolidated_Account_Invoice_Dtl_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        CREATE TABLE #Invoice_List
             (
                 Account_Id INT
                 , CU_INVOICE_ID INT
             );


        INSERT INTO #Invoice_List
             (
                 Account_Id
                 , CU_INVOICE_ID
             )
        SELECT
            cism.Account_ID
            , cism.CU_INVOICE_ID
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
                   AND  Supplier_Cha.Client_Hier_Id = utility_cha.Client_Hier_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = utility_cha.Account_Id
                   AND  cism.SERVICE_MONTH BETWEEN c.CONTRACT_START_DATE
                                           AND     c.CONTRACT_END_DATE
        WHERE
            utility_cha.Account_Id = @Account_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                          AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                            AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                              AND     c.CONTRACT_END_DATE
                    OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                  AND     c.CONTRACT_END_DATE)
        GROUP BY
            cism.Account_ID
            , cism.CU_INVOICE_ID;



        SELECT
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , acbv.Account_Id
            , ci.IS_PROCESSED
        FROM
            dbo.Account_Consolidated_Billing_Vendor acbv
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = acbv.Account_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            acbv.Account_Id = @Account_Id
            AND cism.SERVICE_MONTH BETWEEN acbv.Billing_Start_Dt
                                   AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_List c
                               WHERE
                                    c.Account_Id = acbv.Account_Id
                                    AND c.CU_INVOICE_ID = cism.CU_INVOICE_ID)
        GROUP BY
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , acbv.Account_Id
            , ci.IS_PROCESSED;


        DROP TABLE #Invoice_List;
    END;



GO
GRANT EXECUTE ON  [dbo].[Consolidated_Account_Invoice_Dtl_Sel_By_Account_Id] TO [CBMSApplication]
GO
