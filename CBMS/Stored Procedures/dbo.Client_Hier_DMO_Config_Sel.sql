SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Client_Hier_DMO_Config_Sel
           
DESCRIPTION:             
			To select DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Client_Hier_DMO_Config
            
	EXEC dbo.Client_Hier_DMO_Config_Sel 56
	EXEC dbo.Client_Hier_DMO_Config_Sel 876
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-20	Contract placeholder - CP-4 Created
******/

CREATE PROCEDURE [dbo].[Client_Hier_DMO_Config_Sel] ( @Client_Hier_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Hier_level_Cd INT
            ,Code_Dsc VARCHAR(255)
            ,Commodity_Id INT
            ,Commodity_Name VARCHAR(50)
            ,DMO_Start_Dt VARCHAR(10)
            ,DMO_End_Dt VARCHAR(10)
            ,Updated_User VARCHAR(100)
            ,Last_Change_Ts DATETIME )
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Site_Id INT
      
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id
            
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,CONVERT(VARCHAR(10), chdc.DMO_Start_Dt, 101) AS DMO_Start_Dt
                       ,CONVERT(VARCHAR(10), chdc.DMO_End_Dt, 101) AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,CONVERT(VARCHAR(10), chdc.DMO_Start_Dt, 101) AS DMO_Start_Dt
                       ,CONVERT(VARCHAR(10), chdc.DMO_End_Dt, 101) AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,CONVERT(VARCHAR(10), chdc.DMO_Start_Dt, 101) AS DMO_Start_Dt
                       ,CONVERT(VARCHAR(10), chdc.DMO_End_Dt, 101) AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND @Site_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
      
      SELECT
            tc.Client_Hier_DMO_Config_Id
           ,tc.Client_Hier_Id
           ,tc.Hier_level_Cd
           ,tc.Code_Dsc
           ,tc.Commodity_Id
           ,tc.Commodity_Name
           ,tc.DMO_Start_Dt
           ,tc.DMO_End_Dt
           ,tc.Updated_User
           ,tc.Last_Change_Ts
      FROM
            @Tbl_Config tc
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                         WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND @Sitegroup_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND @Sitegroup_Id IS NOT NULL
                              AND @Site_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = @Site_Id )
      ORDER BY
            tc.Last_Change_Ts DESC
      
END;
;


;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_DMO_Config_Sel] TO [CBMSApplication]
GO
