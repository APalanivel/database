SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
              
  dbo.[Report_DE_Season_Holiday_Schedule_Sel]          
               
 DESCRIPTION:  To fetch holiday and schedule data
 INPUT PARAMETERS:              
Name    DataType  Default  Description              
------------------------------------------------------------              
  
  
OUTPUT PARAMETERS:              
Name    DataType  Default  Description              
------------------------------------------------------------    
            
 USAGE EXAMPLES:              
------------------------------------------------------------             
            
 EXEC dbo.[Report_DE_Season_Holiday_Schedule_Sel]
             
 AUTHOR INITIALS:              
Initials Name              
-----------------------------------------------------------              
AKR      Ashok Kumar Raju
          
MODIFICATIONS               
Initials Date  Modification              
------------------------------------------------------------              
AKR      2014-09-11   Created           

******/              
              
CREATE PROCEDURE [dbo].[Report_DE_Season_Holiday_Schedule_Sel]
AS 
BEGIN              
              
      SET NOCOUNT ON;            
      SELECT
            v.VENDOR_NAME [Vendor]
           ,ts.Schedule_Name [Schedule]
           ,com.Commodity_Name [Commodity]
           ,CONVERT(VARCHAR(2000), comm.Comment_Text) [Comment]
           ,CONVERT(NVARCHAR(2000), tst.Start_Dt, 1) [Start Date]
           ,[End Date] = CASE WHEN tst.end_Dt = '2099-12-31' THEN 'Unspecified'
                              ELSE CONVERT(NVARCHAR(2000), tst.End_Dt, 1)
                         END
           ,s.SEASON_NAME [Season]
           ,tstp.Peak_Name [Peak]
           ,tstpd.Start_Time [Start Time]
           ,tstpd.End_Time [End Time]
           ,cd.Code_Value [Day of Week]
           ,[Holidays] = CASE WHEN Is_Holiday_Active = 1 THEN 'Yes'
                              ELSE NULL
                         END
           ,cd.Display_Seq
      FROM
            dbo.vendor v
            INNER JOIN dbo.Time_Of_Use_Schedule ts
                  ON ts.VENDOR_ID = v.VENDOR_ID
            LEFT JOIN dbo.Comment comm
                  ON comm.Comment_ID = ts.Comment_ID
            LEFT JOIN dbo.Commodity com
                  ON com.Commodity_Id = ts.Commodity_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term tst
                  ON tst.Time_Of_Use_Schedule_Id = ts.Time_Of_Use_Schedule_Id
            LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Peak tstp
                  ON tstp.Time_Of_Use_Schedule_Term_Id = tst.Time_Of_Use_Schedule_Term_Id
            LEFT JOIN dbo.SEASON s
                  ON s.SEASON_ID = tstp.SEASON_ID
            LEFT JOIN dbo.Time_of_Use_Schedule_Term_Peak_Dtl tstpd
                  ON tstpd.Time_Of_Use_Schedule_Term_Peak_Id = tstp.Time_Of_Use_Schedule_Term_Peak_Id
            LEFT JOIN dbo.Code cd
                  ON cd.Code_Id = tstpd.Day_Of_Week_Cd
      GROUP BY
            v.VENDOR_NAME
           ,ts.Schedule_Name
           ,com.Commodity_Name
           ,comm.Comment_Text
           ,tst.Start_Dt
           ,tst.End_Dt
           ,s.SEASON_NAME
           ,tstp.Peak_Name
           ,tstpd.Start_Time
           ,tstpd.End_Time
           ,cd.Code_Value
           ,Is_Holiday_Active
           ,cd.Display_Seq
      ORDER BY
            v.VENDOR_NAME
           ,ts.Schedule_Name
           ,Season
           ,Peak_Name
           ,[Start Time]
           ,Display_Seq


             
END;  

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Season_Holiday_Schedule_Sel] TO [CBMS_SSRS_Reports]
GO
