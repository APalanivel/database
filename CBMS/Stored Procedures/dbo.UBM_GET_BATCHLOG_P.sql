
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_BATCHLOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default				Description
---------------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default				Description
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

EXEC dbo.UBM_GET_BATCHLOG_P '1','1',970

AUTHOR INITIALS:
	Initials		Name
---------------------------------------------------------------------
	NR				Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	NR			2017-08-17	Generic UBM - Payment File Processing - Added Group key column in Output List.

******/

CREATE   PROCEDURE [dbo].[UBM_GET_BATCHLOG_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@masterLogId INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      SELECT
            ubm.UBM_NAME UBM_NAME
           ,START_DATE
           ,END_DATE
           ,ENTITY_NAME STATUS
           ,BATCH_FAILURE_REASON
           ,EXPECTED_DATA_RECORDS
           ,ACTUAL_DATA_RECORDS
           ,EXPECTED_IMAGE_RECORDS
           ,ACTUAL_IMAGE_RECORDS
           ,EXPECTED_DOLLAR_AMOUNT
           ,ACTUAL_DOLLAR_AMOUNT
           ,img_control.TOTAL_BYTES TOTAL_BYTES
           , --//Consider for Cass otherwise ignore it
            ACTUAL_IMAGE_BYTES --//Consider for Cass otherwise ignore it
           ,usc.Group_Key
           ,usc.Plugin_Name
           ,usc.Image_Location
           ,ubb.Current_Step
           ,ubm.Is_Generic
           ,ubb.FTS_Input_File_Name
           ,ubb.File_UNC_Path
      FROM
            dbo.UBM_BATCH_MASTER_LOG master_log
            INNER JOIN dbo.UBM ubm
                  ON master_log.UBM_ID = ubm.UBM_ID
            INNER JOIN dbo.ENTITY entity
                  ON entity.entity_id = master_log.STATUS_TYPE_ID
            LEFT JOIN dbo.UBM_CASS_IMAGES_CONTROL img_control
                  ON master_log.UBM_BATCH_MASTER_LOG_ID = img_control.UBM_BATCH_MASTER_LOG_ID
            LEFT JOIN UBM_Generic.dbo.UBM_Bill_Batch ubb
                  ON master_log.UBM_BATCH_MASTER_LOG_ID = ubb.UBM_Batch_Master_Log_Id
            LEFT JOIN UBM_Generic.dbo.UBM_Source_Config usc
                  ON ubb.UBM_Source_Config_Id = usc.UBM_Source_Config_Id
      WHERE
            master_log.UBM_BATCH_MASTER_LOG_ID = @masterLogId

END



;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_BATCHLOG_P] TO [CBMSApplication]
GO
