SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsState_Get]
( 
	@MyAccountId int 
	, @state_id int
)
AS
BEGIN

	   select st.state_id
		, st.state_name
		, r.region_id
		, r.region_name
		, c.country_name
		, c.country_id
		,st.state_name as stateprovince_name
	     from state st
	     join region r on r.region_id = st.region_id
	     join country c on c.country_id = st.country_id
	    where st.state_id = @state_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsState_Get] TO [CBMSApplication]
GO
