SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_All_VENDOR_P
	@entityName VARCHAR(200),
	@entityType INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT a.vendor_ID
		,a.vendor_name 
	FROM dbo.VENDOR a INNER JOIN dbo.entity b ON b.entity_id = a.VENDOR_TYPE_ID
	WHERE b.entity_name = @entityName   
		AND entity_type = @entityType 
	ORDER BY VENDOR_NAME

END
GO
GRANT EXECUTE ON  [dbo].[GET_All_VENDOR_P] TO [CBMSApplication]
GO
