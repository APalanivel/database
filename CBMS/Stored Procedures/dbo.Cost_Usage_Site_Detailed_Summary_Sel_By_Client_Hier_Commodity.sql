
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
	dbo.Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity  
   
 Description:    
	Used to fetch the data for site level detailed bucket report

 Input Parameters:    
    Name						DataType			Default			Description    
---------------------------------------------------------------------------------    
	@Client_Hier_Id				INT
	@Commodity_Id				INT
	@Start_Dt					DATE
	@End_Dt						DATE
	@Currency_Unit_Id			INT
	@Tvp_Bucket_Uom				Tvp_Bucket			READONLY

 Output Parameters:    
    Name						DataType			Default			Description    
---------------------------------------------------------------------------------    
   
 Usage Examples:  
---------------------------------------------------------------------------------    
DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 101686, 11 )     
,           ( 101687, 11 )
,           ( 101685, 11 )  
,           ( 101688, 9 ) 
,           ( 100991, 12 )
,           ( 1577, NULL )

EXEC dbo.Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity 
      @Client_Hier_Id = 17654
     ,@Commodity_Id = 290
     ,@Start_Dt = '2011-01-01'
     ,@End_Dt = '2011-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp


DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 102517, 20 )     
,           ( 102518, 20 )
,           ( 102516, 20 )  
,           ( 100993, 25 ) 
,           ( 101701, NULL )
,           ( 110, NULL )

EXEC dbo.Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity 
      @Client_Hier_Id = 17654
     ,@Commodity_Id = 291
     ,@Start_Dt = '2011-01-01'
     ,@End_Dt = '2011-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp


DECLARE @Tvp Tvp_Bucket    
INSERT      INTO @Tvp
VALUES
            ( 100589, 1660 )     
,           ( 100598, NULL )
,           ( 100593, NULL )  
,           ( 100599, NULL ) 


EXEC dbo.Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity 
      @Client_Hier_Id = 17654
     ,@Commodity_Id = 100028
     ,@Start_Dt = '2011-01-01'
     ,@End_Dt = '2011-12-01'
     ,@Currency_Unit_Id = 3
     ,@Tvp_Bucket_Uom = @Tvp           

-- Group bill invoice acct
declare @p6 dbo.tvp_Bucket
insert into @p6 values(100991,12)
insert into @p6 values(100990,NULL)

exec dbo.Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity @Client_Hier_Id=2286,@Commodity_Id=290,@Start_Dt='2014-01-01',@End_Dt='2014-01-01',@currency_Unit_Id=1,@Tvp_Bucket_Uom=@p6

Author Initials:
 Initials	Name
---------------------------------------------------------------------------------    
 HG			Harihara Suthan G
 NR			Narayana Reddy

 Modifications :
 Initials		Date	    Modification
---------------------------------------------------------------------------------    
 HG				2014-07-23	Created
 NR				2015-07-30	MAINT-3534 Added a new column Reported_Invoice_Cnt  to  populated based on the number 
							of distinct reported and posted invoices for the given Site and service month. 
 HG				2015-09-20	MAINT-3646, Modified to count the each accounts in a group bill as separate.
 SP				2017-08-23	Data Estimations,Data_type is added in select list.
 ******/
CREATE PROCEDURE [dbo].[Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity]
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Start_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT
      ,@Tvp_Bucket_Uom Tvp_Bucket READONLY )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
      DECLARE @Currency_Group_Id INT;
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,uom_Type_Id INT
            ,Bucket_Type_cd INT ); 
             
      DECLARE @Invoice_Count TABLE
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Reported_Invoice_Cnt INT );       

      SELECT
            @Currency_Group_Id = ch.Client_currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id;

      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,uom_Type_Id
                  ,Bucket_Type_cd )
                  SELECT
                        bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,tvp.Uom_Id
                       ,bm.Bucket_Type_cd
                  FROM
                        dbo.Bucket_Master bm
                        INNER JOIN @Tvp_Bucket_Uom tvp
                              ON tvp.Bucket_Master_Id = bm.Bucket_Master_Id
                  WHERE
                        bm.Commodity_Id = @Commodity_Id
                        AND bm.Is_Shown_on_Site = 1
                        AND bm.Is_Active = 1;

      INSERT      INTO @Invoice_Count
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Reported_Invoice_Cnt )
                  SELECT
                        tbl.Client_Hier_Id
                       ,tbl.Service_Month
                       ,SUM(tbl.Cnt)
                  FROM
                        ( SELECT
                              chat.Client_Hier_Id
                             ,cism.SERVICE_MONTH
                             ,COUNT(DISTINCT cism.CU_INVOICE_ID) Cnt
                          FROM
                              core.Client_Hier_Account chat
                              INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                                    ON chat.Account_Id = cism.Account_ID
                              INNER JOIN dbo.CU_INVOICE ci
                                    ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                          WHERE
                              chat.Client_Hier_id = @Client_Hier_Id
                              AND cism.SERVICE_MONTH BETWEEN @Start_Dt AND @End_Dt
                              AND chat.Commodity_Id = @Commodity_Id
                              AND ci.IS_PROCESSED = 1
                              AND ci.IS_REPORTED = 1
                              AND ci.IS_DNT = 0
                              AND ci.IS_Duplicate = 0
                          GROUP BY
                              chat.Client_Hier_Id
                             ,cism.SERVICE_MONTH
                             ,cism.Account_ID ) tbl
                  GROUP BY
                        tbl.Client_Hier_Id
                       ,tbl.Service_Month;

      SELECT
            bl.Bucket_Name
           ,bt.Code_Value AS Bucket_Type
           ,sm.Date_D AS Service_Month
           ,sm.Month_Num
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cu.currency_Unit_Name
                   WHEN bt.Code_Value = 'Determinant' THEN uom.Entity_Name
              END ) AS uom_Name
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cusd.Bucket_Value * ISNULL(curconv.Conversion_Factor, 0)
                   WHEN bt.Code_Value = 'Determinant' THEN cusd.Bucket_Value * ISNULL(uomconv.Conversion_Factor, 0)
              END ) Bucket_Value
           ,cusd.Data_Source_Cd
           ,dsc.Code_Value AS Data_Source
           ,COUNT(DISTINCT ( CASE WHEN ip.IS_EXPECTED = 1 THEN ip.Account_Id
                             END )) AS IP_Expected_Cnt
           ,COUNT(DISTINCT ( CASE WHEN ip.IS_RECEIVED = 1 THEN ip.Account_Id
                             END )) AS IP_Received_Cnt
           ,ISNULL(ic.Reported_Invoice_Cnt, 0) AS Reported_Invoice_Cnt
           ,dtc.Code_Value AS Data_Type
      FROM
            @Bucket_List bl
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bl.Bucket_Type_cd
            CROSS JOIN meta.Date_Dim sm
            LEFT OUTER JOIN dbo.Cost_Usage_Site_Dtl cusd
                  ON cusd.Client_Hier_Id = @Client_Hier_Id
                     AND cusd.Service_Month = sm.Date_D
                     AND cusd.Bucket_Master_Id = bl.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Code dsc
                  ON dsc.Code_Id = cusd.Data_Source_Cd
            LEFT OUTER JOIN dbo.currency_Unit_Conversion curconv
                  ON curconv.Base_Unit_Id = cusd.Currency_Unit_Id
                     AND curconv.Conversion_Date = sm.DATE_D
                     AND curconv.currency_Group_Id = @Currency_Group_Id
                     AND curconv.Converted_Unit_Id = @Currency_Unit_Id
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion uomconv
                  ON uomconv.Base_Unit_Id = cusd.uom_Type_Id
                     AND uomconv.Converted_Unit_Id = bl.uom_Type_Id
            LEFT OUTER JOIN ( core.Client_Hier_Account cha
                              INNER JOIN dbo.INVOICE_PARTICIPATION ip
                                    ON ip.Client_Hier_Id = cha.Client_Hier_Id
                                       AND ip.ACCOUNT_ID = cha.Account_Id )
                              ON cha.Client_Hier_Id = @Client_Hier_Id
                                 AND cha.Commodity_Id = @Commodity_Id
                                 AND ip.Service_Month = sm.DATE_D
            LEFT OUTER JOIN dbo.currency_Unit cu
                  ON cu.currency_Unit_Id = @Currency_Unit_Id
            LEFT OUTER JOIN dbo.Entity uom
                  ON uom.Entity_Id = bl.uom_Type_Id
            LEFT OUTER JOIN @Invoice_Count ic
                  ON ic.Service_Month = sm.DATE_D
            LEFT JOIN dbo.Code dtc
                  ON dtc.Code_Id = cusd.Data_Type_Cd
      WHERE
            sm.DATE_D BETWEEN @Start_Dt AND @End_Dt
      GROUP BY
            bl.Bucket_Name
           ,bt.Code_Value
           ,sm.Date_D
           ,sm.Month_Num
           ,( CASE WHEN uomconv.Conversion_Factor = 0
                        AND bt.Code_Value = 'Determinant' THEN cusd.uom_Type_Id
                   WHEN uomconv.Conversion_Factor != 0
                        AND bt.Code_Value = 'Determinant' THEN bl.uom_Type_Id
              END )
           ,( CASE WHEN bt.Code_Value = 'Charge' THEN cusd.Bucket_Value * ISNULL(curconv.Conversion_Factor, 0)
                   WHEN bt.Code_Value = 'Determinant' THEN cusd.Bucket_Value * ISNULL(uomconv.Conversion_Factor, 0)
              END )
           ,cusd.Data_Source_Cd
           ,dsc.Code_Value
           ,cu.currency_Unit_Name
           ,uom.Entity_Name
           ,ISNULL(ic.Reported_Invoice_Cnt, 0)
           ,dtc.Code_Value
      ORDER BY
            bt.Code_Value DESC
           ,bl.Bucket_Name
           ,sm.Date_D;

END;
;
;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Detailed_Summary_Sel_By_Client_Hier_Commodity] TO [CBMSApplication]
GO
