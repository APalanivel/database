SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

NAME: dbo.Budget_Id_String_Sel_By_Account_Id  

DESCRIPTION: 

	It Returns Buget Ids in Comma Seperated format for given Account Id.
	Will be used in manage account page, when the account becomes not managed users should be alerted with the list budget the given account associated.
  
INPUT PARAMETERS:      
	Name	          DataType		Default     Description      
-----------------------------------------------------------
    @Account_Id       INT
                             
OUTPUT PARAMETERS:           
	Name              DataType         Default		Description      
------------------------------------------------------------      
    
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Budget_Id_String_Sel_By_Account_Id 22814
	
	EXEC dbo.Budget_Id_String_Sel_By_Account_Id 15738


AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
  PNR		PANDARINATH

MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
   PNR		06/30/2010  Created

******/

CREATE PROCEDURE dbo.Budget_Id_String_Sel_By_Account_Id
	 @Account_Id	INTEGER
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Budget_Id_String VARCHAR(1000)
		, @Delimiter CHAR = ','

	SELECT
		@Budget_Id_String = COALESCE(@Budget_Id_String + @Delimiter, '') + SPACE(1) + CONVERT(VARCHAR(30),BUDGET_ID)
	FROM
		dbo.BUDGET_ACCOUNT
	WHERE
		ACCOUNT_ID = @Account_Id
		AND IS_DELETED = 0

	SELECT @Budget_Id_String AS Budget_Id_String

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Id_String_Sel_By_Account_Id] TO [CBMSApplication]
GO
