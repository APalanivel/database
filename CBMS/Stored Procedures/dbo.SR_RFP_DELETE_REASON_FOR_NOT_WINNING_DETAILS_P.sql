SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_DELETE_REASON_FOR_NOT_WINNING_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@reasonNotWinningId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_DELETE_REASON_FOR_NOT_WINNING_DETAILS_P

@reasonNotWinningId int

AS
set nocount on
	delete 
	from SR_RFP_REASON_NOT_WINNING_MAP
	
	where SR_RFP_REASON_NOT_WINNING_ID = @reasonNotWinningId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_REASON_FOR_NOT_WINNING_DETAILS_P] TO [CBMSApplication]
GO
