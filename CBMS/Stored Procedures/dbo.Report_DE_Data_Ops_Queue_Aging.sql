
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Report_DE_Data_Ops_Queue_Aging            
                        
 DESCRIPTION:                        
			SSRS report for Data Ops Queue Aging                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
  EXEC  Report_DE_Data_Ops_Queue_Aging       
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2015-04-14       Created
 SP					   2015-05-06       Added distinct_invs_cte           
 HG						2017-12-14		REPTMGR-79, Modifed to exclude the not managed accounts from variance queue                      
******/                 
                
CREATE PROCEDURE [dbo].[Report_DE_Data_Ops_Queue_Aging]
AS
BEGIN                
      SET NOCOUNT ON;     

      WITH  Queue_Name_CTE
              AS ( SELECT DISTINCT
                        q.queue_id
                       ,ui.USER_INFO_ID
                       ,queue_name
                       ,dt.team_name
                       ,History = CASE WHEN ui.IS_HISTORY = 1 THEN 'History'
                                       ELSE 'Active User'
                                  END
                       ,[User Name] = ui.USERNAME
                       ,[CEM] = CASE WHEN uigim.USER_INFO_ID IS NOT NULL THEN 'Yes'
                                     ELSE 'No'
                                END
                       ,[Ops] = CASE WHEN uigim2.USER_INFO_ID IS NOT NULL THEN 'Yes'
                                     ELSE 'No'
                                END
                       ,[External User Alert] = CASE WHEN ui.ACCESS_LEVEL = 1 THEN 'ALERT'
                                                     ELSE NULL
                                                END
                   FROM
                        vwCbmsQueueName q
                        JOIN USER_INFO ui
                              ON ui.QUEUE_ID = q.queue_id
                        LEFT JOIN USER_INFO_GROUP_INFO_MAP uigim
                              ON uigim.USER_INFO_ID = ui.USER_INFO_ID
                                 AND uigim.GROUP_INFO_ID = 2
                        LEFT JOIN USER_INFO_GROUP_INFO_MAP uigim2
                              ON uigim2.USER_INFO_ID = ui.USER_INFO_ID
                                 AND uigim2.GROUP_INFO_ID = 7
                        LEFT JOIN Analysis.dbo.data_team_map_jmm dt
                              ON dt.USER_INFO_ID = ui.USER_INFO_ID
                   WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    CU_EXCEPTION_DENORM cued
                                 WHERE
                                    cued.QUEUE_ID = q.queue_id )
                        OR EXISTS ( SELECT
                                          1
                                    FROM
                                          dbo.Variance_Log vl
                                    WHERE
                                          vl.Partition_Key IS NULL
                                          AND vl.Is_Failure = 1
                                          AND vl.Queue_Id = q.queue_id )),
            distinct_invs_cte
              AS ( SELECT
                        QUEUE_ID
                       ,CU_INVOICE_ID
                       ,IS_MANUAL
                       ,DATE_IN_QUEUE
                       ,EXCEPTION_TYPE
                   FROM
                        CU_EXCEPTION_DENORM ced
                   GROUP BY
                        QUEUE_ID
                       ,CU_INVOICE_ID
                       ,IS_MANUAL
                       ,DATE_IN_QUEUE
                       ,EXCEPTION_TYPE),
            Cte_Counts
              AS ( SELECT
                        di.QUEUE_ID
                       ,COUNT(CASE WHEN IS_MANUAL = 1 THEN 1
                              END) [Incoming Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 2 ) THEN 1
                              END) [Incoming >2Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 5 ) THEN 1
                              END) [Incoming >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 15 ) THEN 1
                              END) [Incoming >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 30 ) THEN 1
                              END) [Incoming >30Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND EXCEPTION_TYPE != 'failed recalc' THEN 1
                              END) [Exception Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 2
                                              AND EXCEPTION_TYPE != 'failed recalc' ) THEN 1
                              END) [Exception >2Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 5
                                              AND EXCEPTION_TYPE != 'failed recalc' ) THEN 1
                              END) [Exception >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 15
                                              AND EXCEPTION_TYPE != 'failed recalc' ) THEN 1
                              END) [Exception >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 30
                                              AND EXCEPTION_TYPE != 'failed recalc' ) THEN 1
                              END) [Exception >30Days]
                              --Recalc
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND EXCEPTION_TYPE = 'failed recalc' THEN 1
                              END) [Recalc Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 2
                                              AND EXCEPTION_TYPE = 'failed recalc' ) THEN 1
                              END) [Recalc >2Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 5
                                              AND EXCEPTION_TYPE = 'failed recalc' ) THEN 1
                              END) [Recalc >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 15
                                              AND EXCEPTION_TYPE = 'failed recalc' ) THEN 1
                              END) [Recalc >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - di.DATE_IN_QUEUE >= 30
                                              AND EXCEPTION_TYPE = 'failed recalc' ) THEN 1
                              END) [Recalc >30Days]
                   FROM
                        distinct_invs_cte di
                   GROUP BY
                        di.QUEUE_ID),
            Variance_Count_CTE
              AS ( SELECT
                        COUNT(variance_count_blue) AS [Variance Total]
                       ,COUNT(CASE WHEN ( GETDATE() - vt.Execution_Dt >= 2 ) THEN 1
                              END) [Variance >2Days]
                       ,COUNT(CASE WHEN ( GETDATE() - vt.Execution_Dt >= 5 ) THEN 1
                              END) [Variance >5Days]
                       ,COUNT(CASE WHEN ( GETDATE() - vt.Execution_Dt >= 15 ) THEN 1
                              END) [Variance >15Days]
                       ,COUNT(CASE WHEN ( GETDATE() - vt.Execution_Dt >= 30 ) THEN 1
                              END) [Variance >30Days]
                       ,Queue_Id
                   FROM
                        ( SELECT
                              COUNT(1) AS variance_count_blue
                             ,l.Queue_Id
                             ,l.Execution_Dt
                          FROM
                              dbo.Variance_Log l
                          WHERE
                              l.Partition_Key IS NULL
                              AND l.Is_Failure = 1
                              AND EXISTS ( SELECT
                                                1
                                           FROM
                                                Core.Client_Hier_Account cha
                                           WHERE
                                                cha.Account_Id = l.Account_ID
                                                AND cha.Account_Not_Managed = 0 )
                          GROUP BY
                              l.Account_ID
                             ,l.Service_Month
                             ,l.Site_ID
                             ,l.Queue_Id
                             ,l.Execution_Dt ) vt
                   GROUP BY
                        vt.Queue_Id)
            SELECT
                  qc.queue_name [Queue Name]
                 ,qc.[User Name]
                 ,qc.team_name [Team]
                 ,qc.[External User Alert]
                 ,qc.History
                 ,qc.CEM
                 ,qc.Ops
                 ,ISNULL(cc.[Incoming Total], 0) [Incoming Total]
                 ,ISNULL(cc.[Incoming >2Days], 0) [Incoming >2Days]
                 ,ISNULL(cc.[Incoming >5Days], 0) [Incoming >5Days]
                 ,ISNULL(cc.[Incoming >15Days], 0) [Incoming >15Days]
                 ,ISNULL(cc.[Incoming >30Days], 0) [Incoming >30Days]
                 ,ISNULL(vc.[Variance Total], 0) [Variance Total]
                 ,ISNULL(vc.[Variance >2Days], 0) [Variance >2Days]
                 ,ISNULL(vc.[Variance >5Days], 0) [Variance >5Days]
                 ,ISNULL(vc.[Variance >15Days], 0) [Variance >15Days]
                 ,ISNULL(vc.[Variance >30Days], 0) [Variance >30Days]
                 ,ISNULL(cc.[Exception Total], 0) [Exception Total]
                 ,ISNULL(cc.[Exception >2Days], 0) [Exception >2Days]
                 ,ISNULL(cc.[Exception >5Days], 0) [Exception >5Days]
                 ,ISNULL(cc.[Exception >15Days], 0) [Exception >15Days]
                 ,ISNULL(cc.[Exception >30Days], 0) [Exception >30Days]
                 ,ISNULL(cc.[Recalc Total], 0) [Recalc Total]
                 ,ISNULL(cc.[Recalc >2Days], 0) [Recalc >2Days]
                 ,ISNULL(cc.[Recalc >5Days], 0) [Recalc >5Days]
                 ,ISNULL(cc.[Recalc >15Days], 0) [Recalc >15Days]
                 ,ISNULL(cc.[Recalc >30Days], 0) [Recalc >30Days]
            FROM
                  Queue_Name_CTE qc
                  LEFT JOIN Variance_Count_CTE vc
                        ON vc.Queue_Id = qc.queue_id
                  LEFT JOIN Cte_Counts cc
                        ON cc.QUEUE_ID = qc.queue_id
            ORDER BY
                  queue_name   
END;

;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Data_Ops_Queue_Aging] TO [CBMS_SSRS_Reports]
GO
