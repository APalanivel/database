SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.Commodity_Sel_By_All

DESCRIPTION:  
	Used to get all commodity

INPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  
       
OUTPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.Commodity_Sel_By_All
	EXEC dbo.Commodity_Sel_By_All 1,1
	
AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
GB			Geetansu Behera  
CMH			Chad Hattabaugh   
HG			Hari
SKA			Shobhit Kr Agrawal
SSR			Sharad Srivastava
MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
GB						created
SKA		08/07/09		Modified as per coding standard
HG		8/25/2009		Is_Sustainable column included in the select clause
						- Column alias removed as the alias and column names are same.
HG		8/28/2009		- Removed the Is_Sustainable column as this is not required here.
SKA		23-sep-2009		Added one more condition (Is_Sustainable) in where clause
SKA		01/28/2010		Added two more columns in the select clause along with input parameter
HG		03/11/2010		Commodity_id > 0 Filter condition added as we have added Not Applicable Commodity with the id of -1(Added to store non commodity specific buckets)
DMR		09/10/2010		Modified for Quoted_Identifier
SKA		01/04/2011		Added Service_Mapping_Instructions Column in the Select Clause.


******/
CREATE PROCEDURE [dbo].[Commodity_Sel_By_All]
	(@Is_Active INT = 1,
	@Is_Sustainable INT = NULL
	)
AS

BEGIN

	SET NOCOUNT ON

	SELECT 
		 COMMODITY_ID
		, COMMODITY_NAME
		, Default_UOM_Entity_Type_Id
		, Is_Alternate_Fuel
		, Service_Mapping_Instructions
	FROM
		dbo.COMMODITY
	WHERE
		Is_Active = @Is_Active 
		AND (@Is_Sustainable is null or Is_Sustainable = @Is_Sustainable)
		AND Commodity_Id > 0
	ORDER BY
		DISPLAY_SQ

END

GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_By_All] TO [CBMSApplication]
GO
