SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndex_GetAllIndexes

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsCPIndex_GetAllIndexes]

AS
BEGIN

	   select ci.clearport_index_id
		, ci.clearport_index
	     from clearport_index ci with (nolock)
	 order by ci.clearport_index


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndex_GetAllIndexes] TO [CBMSApplication]
GO
