SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Vendor_Name_Match_Invoice_Collection_Account_Config_For_Bulk_Update       
              
Description:              
			This sproc to get the Invoice Collection Records for the given filters     
                           
 Input Parameters:              
    Name									DataType								Default			Description                
-----------------------------------------------------------------------------------------------------------------                
	@Invoice_Collection_Account_Config_Id	INT
    @Contact_Info_Id						VARCHAR(MAX)
    
 Output Parameters:                    
    Name								DataType			Default			Description                
-----------------------------------------------------------------------------------------------------------------                
              
 Usage Examples:                  
-----------------------------------------------------------------------------------------------------------------                



  --SELECT * FROM dbo.Invoice_Collection_Account_Config icac--4,529 -- 503,525
EXEC Vendor_Name_Match_Invoice_Collection_Account_Config_For_Bulk_Update 503,'525'
        
              
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-03-14		Created For Invoice_Collection.         
             
******/


CREATE PROCEDURE [dbo].[Vendor_Name_Match_Invoice_Collection_Account_Config_For_Bulk_Update]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@Contact_Info_Id VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
 
      DECLARE
            @Is_Vendor_Contact_Exists BIT = 0
           ,@Is_Matched BIT = 1
 
 
      DECLARE
            @Source_Type_Vendor INT
           ,@Contact_Level_Vendor INT
           ,@Existing_Vendor_Name VARCHAR(200)
           ,@Existing_Vendor_Type VARCHAR(200) 
 
      SELECT
            @Source_Type_Vendor = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value = 'Vendor Primary Contact' 
            
   
      SELECT
            @Contact_Level_Vendor = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'ContactLevel'
            AND c.Code_Value = 'Vendor'
                  


      SELECT
            @Existing_Vendor_Name = v.VENDOR_NAME
           ,@Existing_Vendor_Type = e.ENTITY_NAME
           ,@Is_Vendor_Contact_Exists = 1
      FROM
            dbo.Invoice_Collection_Account_Contact icac
            INNER JOIN Contact_Info ci
                  ON ci.Contact_Info_Id = icac.Contact_Info_Id
                     AND icac.Is_Primary = 1
            INNER JOIN dbo.Vendor_Contact_Map vcm
                  ON icac.Contact_Info_Id = vcm.Contact_Info_Id
            INNER JOIN dbo.VENDOR v
                  ON vcm.VENDOR_ID = v.VENDOR_ID
            INNER JOIN dbo.ENTITY e
                  ON v.VENDOR_TYPE_ID = e.ENTITY_ID
      WHERE
            ci.Contact_Level_Cd = @Contact_Level_Vendor
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id         
      
 
                          
      
    
     
      SELECT
            @Is_Matched = 0
      FROM
            dbo.Contact_Info ci
            INNER JOIN dbo.Vendor_Contact_Map vcm
                  ON ci.Contact_Info_Id = vcm.Contact_Info_Id
            INNER JOIN dbo.VENDOR v
                  ON vcm.VENDOR_ID = v.VENDOR_ID
            INNER JOIN dbo.ENTITY e
                  ON v.VENDOR_TYPE_ID = e.ENTITY_ID
            INNER JOIN dbo.ufn_split(@Contact_Info_Id, ',') us
                  ON us.Segments = ci.Contact_Info_Id
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.VENDOR v
                              INNER JOIN dbo.ENTITY e
                                    ON v.VENDOR_TYPE_ID = e.ENTITY_ID
                         WHERE
                              vcm.VENDOR_ID = v.VENDOR_ID
                              AND v.VENDOR_NAME = @Existing_Vendor_Name
                              AND e.ENTITY_NAME = @Existing_Vendor_Type )
           

      SELECT
            @Is_Vendor_Contact_Exists Is_Vendor_Contact_Exists
           ,@Is_Matched Is_Matched


END

;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Name_Match_Invoice_Collection_Account_Config_For_Bulk_Update] TO [CBMSApplication]
GO
