SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.DELETE_LPS_P
	@loadProfileSpecificationID INT
AS
BEGIN

	SET NOCOUNT ON

	DELETE FROM dbo.LOAD_PROFILE_SPECIFICATION
	WHERE LOAD_PROFILE_SPECIFICATION_ID = @loadProfileSpecificationID

END




GO
GRANT EXECUTE ON  [dbo].[DELETE_LPS_P] TO [CBMSApplication]
GO
