SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
  
  
  
/******            
NAME:            
 dbo.SITEID_CITIES_SEL_BY_CONTRACT_ID            
     
 DESCRIPTION:             
     
 INPUT PARAMETERS:            
 Name   DataType Default Description            
------------------------------------------------------------            
 @contractId VARCHAR(150)          
          
OUTPUT PARAMETERS:            
Name DataType  Default Description            
------------------------------------------------------------            
    
 USAGE EXAMPLES:            
------------------------------------------------------------            
EXEC dbo.SITEID_CITIES_SEL_BY_CONTRACT_ID 26046    
    
 AUTHOR INITIALS:            
 Initials Name            
------------------------------------------------------------            
 MA   Mohammed Abdul Aman            
     
 MODIFICATIONS:            
 Initials Date   Modification            
------------------------------------------------------------            
  MA  04/21/2010  Created        
******/            
CREATE PROCEDURE [dbo].[SITEID_CITIES_SEL_BY_CONTRACT_ID]            
 @CONTRACTID   INT        
AS            
BEGIN            
            
    SET NOCOUNT ON ;    
        
    DECLARE @CITYSITEID TABLE (CITY VARCHAR(200),SITE_ID INT)           
            
    INSERT INTO @CITYSITEID    
 SELECT     
  ADDR.CITY AS CITY,          
  SIT.SITE_ID AS SITEID            
 FROM                 
  DBO.CONTRACT CON    
  INNER JOIN DBO.SUPPLIER_ACCOUNT_METER_MAP MAP    
   ON MAP.CONTRACT_ID = CON.CONTRACT_ID            
  INNER JOIN DBO.METER MET    
   ON MET.METER_ID = MAP.METER_ID            
  INNER JOIN DBO.ACCOUNT UTILACC    
   ON UTILACC.ACCOUNT_ID = MET.ACCOUNT_ID            
  INNER JOIN DBO.SITE SIT            
   ON SIT.SITE_ID = UTILACC.SITE_ID            
  INNER JOIN DBO.ADDRESS ADDR    
   ON ADDR.ADDRESS_ID = SIT.PRIMARY_ADDRESS_ID            
 WHERE            
  CON.CONTRACT_ID = @CONTRACTID        
     
 SELECT     
   CITY    
  ,LEFT(ST.ST_LIST,LEN(ST.ST_LIST) - 1) AS SITE_ID    
 FROM     
  @CITYSITEID CTY    
 CROSS APPLY (    
   SELECT     
    DISTINCT CONVERT(VARCHAR,ST.SITE_ID) + ', '      
   FROM       
    @CITYSITEID ST      
         WHERE      
   ST.CITY = CTY.CITY      
         FOR XML PATH('')    
         ) ST(ST_LIST)        
      GROUP BY    
   CITY    
  ,ST.ST_LIST    
     
END 

GO
GRANT EXECUTE ON  [dbo].[SITEID_CITIES_SEL_BY_CONTRACT_ID] TO [CBMSApplication]
GO
