SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_NYMEX_FORECAST_PRICES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@asOfDate      	datetime  	          	
	@year          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2019-11-29	RM-Budgets Enahancement - Added input parameter @Forecast_Type_Id, added RM_FORECAST_ID to result set

******/

CREATE PROCEDURE [dbo].[GET_NYMEX_FORECAST_PRICES_P]
    (
        @userId VARCHAR(10)
        , @sessionId VARCHAR(20)
        , @asOfDate DATETIME
        , @year INT
        , @Forecast_Type_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        IF @asOfDate IS NOT NULL
            BEGIN

                SELECT
                    a.FORECAST_AS_OF_DATE
                    , a.FORECAST_TO_DATE
                    , b.AGGRESSIVE_PRICE
                    , b.CONSERVATIVE_PRICE
                    , b.MODERATE_PRICE
                    , b.MONTH_IDENTIFIER
                    , a.RM_FORECAST_ID
                FROM
                    dbo.RM_FORECAST a
                    INNER JOIN dbo.RM_FORECAST_DETAILS b
                        ON a.RM_FORECAST_ID = b.RM_FORECAST_ID
                WHERE
                    (   @Forecast_Type_Id IS NULL
                        OR  a.FORECAST_TYPE_ID = @Forecast_Type_Id)
                    AND a.FORECAST_AS_OF_DATE = @asOfDate
                    AND DATEPART(YEAR, b.MONTH_IDENTIFIER) = @year
                ORDER BY
                    b.MONTH_IDENTIFIER;
            END;
        ELSE
            SELECT
                a.FORECAST_AS_OF_DATE
                , a.FORECAST_TO_DATE
                , b.AGGRESSIVE_PRICE
                , b.CONSERVATIVE_PRICE
                , b.MODERATE_PRICE
                , a.RM_FORECAST_ID
            FROM
                dbo.RM_FORECAST a
                INNER JOIN dbo.RM_FORECAST_DETAILS b
                    ON b.RM_FORECAST_ID = a.RM_FORECAST_ID
            WHERE
                (   @Forecast_Type_Id IS NULL
                    OR  a.FORECAST_TYPE_ID = @Forecast_Type_Id)
                AND a.FORECAST_AS_OF_DATE = (   SELECT
                                                    MAX(rf.FORECAST_AS_OF_DATE)
                                                FROM
                                                    dbo.RM_FORECAST rf
                                                WHERE
                                                    DATEPART(YEAR, rf.FORECAST_AS_OF_DATE) = @year
                                                    AND rf.FORECAST_TYPE_ID = a.FORECAST_TYPE_ID);

    END;

GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_FORECAST_PRICES_P] TO [CBMSApplication]
GO
