SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.RC_GET_CITIES_UNDER_CLIENT_P
@userId varchar(10),
@sessionId varchar(10),
@clientId int

as
set nocount on
select distinct substring(vw.SITE_NAME, 0, charindex(',' , vw.SITE_NAME) ) CITY	

from		VWSITENAME vw

where vw.SITE_ID in ( select SITE_ID 
		   from   SITE s,
			  DIVISION d, 
			  CLIENT cl
		   where   cl.CLIENT_ID = @clientId
			   AND d.CLIENT_ID = cl.CLIENT_ID
			   AND s.DIVISION_ID = d.DIVISION_ID
		 ) 
	
order by CITY
GO
GRANT EXECUTE ON  [dbo].[RC_GET_CITIES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
