
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*=============================================================
Name:
    CBMS.dbo.BUDGET_GET_BUDGET_REPORT_NG_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P
    
Description:
    What is the business description/function of the procedure.

Input Parameters:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@BudgetID				INT
	@From_Month			DATETIME
	@To_Month				DATETIME
	@ClientID				INT
	@DivisionID			INT
	@SiteID				INT
	@UnitID				INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec BUDGET_GET_BUDGET_REPORT_NG_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P 1193, '01/01/2010', '12/01/2010',null,null,null,25

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		Athmaram Pabbathi

MODIFICATIONS:
Initials	Date		    Modification
----------------------------------------------------------------
AP		07/26/2011    Removed @User_ID & @Session_ID parameters and 
				    Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Commodity tables
AP		08/23/2011    Removed @CommodityID (not required) parameter and replaced Account, Site, Division, Client tables with Client_Hier, Client_Hier_Account tables
				    and removed Budget_Account table & used the Budget_Account_ID reference from view.
AP		09/21/2011    Removed hardcoded buckets & Bucket_Master table and used dbo.Cost_usage_Bucket_Sel_By_Commodity SP
=============================================================*/

CREATE PROCEDURE [dbo].[BUDGET_GET_BUDGET_REPORT_NG_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P]
      ( 
       @BudgetId INT
      ,@From_Month DATETIME
      ,@To_Month DATETIME
      ,@ClientID INT
      ,@DivisionID INT
      ,@SiteID INT
      ,@UnitID INT )
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE @NG_Commodity_Id INT


      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

      SELECT
            @NG_Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas'
    
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @NG_Commodity_Id ;
                        
      SELECT
            CUAD.Service_Month
           ,( CUAD.Bucket_Value * UC.Conversion_Factor ) AS Usage_Value
           ,BAT.Budget_Account_ID
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN @Cost_Usage_Bucket_Id CUB
                  ON CUB.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Consumption_Unit_Conversion UC
                  ON UC.Base_Unit_ID = CUAD.UOM_Type_Id
            INNER JOIN Budget_Account_Type_VW BAT
                  ON BAT.Account_ID = CUAD.Account_ID
            INNER JOIN Budget B
                  ON B.Budget_ID = BAT.Budget_ID
            INNER JOIN ( SELECT
                              CHA.Account_Id
                             ,CH.Site_Id
                             ,CH.Sitegroup_Id
                             ,CH.Client_Id
                             ,ch.Client_Hier_Id
                         FROM
                              Core.Client_Hier_Account CHA
                              INNER JOIN Core.Client_Hier CH
                                    ON CH.Client_Hier_ID = CHA.Client_Hier_Id
                         GROUP BY
                              CHA.Account_Id
                             ,CH.Site_Id
                             ,CH.Sitegroup_Id
                             ,CH.Client_Id
                             ,ch.Client_Hier_Id ) CHA
                  ON CHA.Account_ID = CUAD.Account_ID
                     AND cha.Client_Hier_Id = cuad.Client_Hier_Id
      WHERE
            CUB.Bucket_Type = 'Determinant'
            AND B.Budget_ID = @BudgetID
            AND UC.Converted_Unit_ID = @UnitID
            AND CUAD.Service_Month BETWEEN @From_Month
                                   AND     @To_Month
            AND ( @SiteID IS NULL
                  OR CHA.Site_ID = @SiteID )
            AND ( @DivisionID IS NULL
                  OR CHA.Sitegroup_Id = @DivisionID )
            AND ( @ClientID IS NULL
                  OR CHA.Client_ID = @ClientID )
      GROUP BY
            CUAD.Service_Month
           ,( CUAD.Bucket_Value * UC.Conversion_Factor )
           ,BAT.Budget_Account_ID
      ORDER BY
            BAT.Budget_Account_ID

		
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_REPORT_NG_HISTORICAL_DATA_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
