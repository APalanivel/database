SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.EC_Invoice_Sub_Bucket_Exists_Sel_By_Cu_Invoice_Id  
  
DESCRIPTION:  
  Script to get available sub buckets for that account's state and commodity for which invoice is posted  
  
INPUT PARAMETERS:  
 Name     DataType  Default Description  
---------------------------------------------------------------  
 @Cu_Invoice_Id    INT  
   
  
OUTPUT PARAMETERS:  
 Name     DataType  Default Description  
------------------------------------------------------------  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.EC_Invoice_Sub_Bucket_Exists_Sel_By_Cu_Invoice_Id 16231936   
 EXEC EC_Invoice_Sub_Bucket_Exists_Sel_By_Cu_Invoice_Id 25534125

  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RR   Raghu Reddy  
 RKV  Ravi Kumar Vegesna
   
MODIFICATIONS  
 Initials Date  Modification  
------------------------------------------------------------  
 RR        2015-05-28 Created for AS400  
 RKV	   2016-02-11 Added new Parameter Bucket_Master_Id
 RKV       2018-02-06 SE2017-465,Added Is_EC_Invoice_Sub_Bucket_For_Charge_Exists to the result set 
  
******/  
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Exists_Sel_By_Cu_Invoice_Id]
      ( 
       @Cu_Invoice_Id INT
      ,@Bucket_Master_Id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
        
      DECLARE
            @Is_EC_Invoice_Sub_Bucket_Exists BIT = 0
           ,@Is_EC_Invoice_Sub_Bucket_For_Charge_Exists BIT = 0
        
      SELECT
            @Is_EC_Invoice_Sub_Bucket_For_Charge_Exists = CASE WHEN COUNT(CASE WHEN c.Code_Value = 'Charge' THEN 1
                                                                          END) > 0 THEN 1
                                                               ELSE 0
                                                          END
           ,@Is_EC_Invoice_Sub_Bucket_Exists = CASE WHEN COUNT(CASE WHEN c.Code_Value IN ( 'Determinant', 'Charge' ) THEN 1
                                                               END) > 0 THEN 1
                                                    ELSE 0
                                               END
      FROM
            dbo.CU_INVOICE cid
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cid.CU_INVOICE_ID = cism.CU_INVOICE_ID
            INNER JOIN core.Client_Hier_Account cha
                  ON cism.Account_ID = cha.Account_Id
            INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                  ON cha.Meter_State_Id = eisbm.State_Id
            INNER JOIN dbo.Bucket_Master bm
                  ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
                     AND cha.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.Code c
                  ON c.Code_Id = bm.Bucket_Type_Cd
      WHERE
            cid.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ( @Bucket_Master_Id IS NULL
                  OR bm.Bucket_Master_Id = @Bucket_Master_Id )
        
        
    
      SELECT
            @Is_EC_Invoice_Sub_Bucket_Exists AS Is_EC_Invoice_Sub_Bucket_Exists
           ,@Is_EC_Invoice_Sub_Bucket_For_Charge_Exists AS Is_EC_Invoice_Sub_Bucket_For_Charge_Exists 
                    
                    
END;  
;

;
;
GO

GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Exists_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
