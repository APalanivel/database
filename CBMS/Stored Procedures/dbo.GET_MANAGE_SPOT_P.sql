SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_MANAGE_SPOT_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT spot_id,
		e.entity_name,
		e1.entity_name,
		e2.entity_name,
		spot_name,
		spot_volume,
		spot_price,
		cu.currency_unit_name,
		spot_transaction_date,
		spot_comments 
	FROM dbo.spot s INNER JOIN dbo.currency_unit cu ON cu.currency_unit_id=s.currency_unit_id 
			AND s.contract_id=@contract_id
		INNER JOIN dbo.entity e ON e.entity_id=s.transaction_by_type_id
		INNER JOIN dbo.entity e1 ON e1.entity_id=s.spot_unit_type_id
		INNER JOIN dbo.entity e2 ON e2.entity_id=s.deal_type_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_MANAGE_SPOT_P] TO [CBMSApplication]
GO
