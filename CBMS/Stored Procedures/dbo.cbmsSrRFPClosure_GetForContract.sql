SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSrRFPClosure_GetForContract

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@contract_id   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[cbmsSrRFPClosure_GetForContract]
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select distinct r.sr_rfp_closure_id
		, a.sr_rfp_id
	     from sr_rfp_closure r
	     join sr_rfp_account a on a.sr_rfp_account_id = r.sr_account_group_id 
	    where r.contract_id = @contract_id 
	      and r.is_bid_group = 0

	union all

	   select distinct r.sr_rfp_closure_id
		, a.sr_rfp_id
	     from sr_rfp_closure r
	     join sr_rfp_account a on a.sr_rfp_bid_group_id = r.sr_account_group_id 
	    where r.contract_id = @contract_id 
	      and r.is_bid_group = 0


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSrRFPClosure_GetForContract] TO [CBMSApplication]
GO
