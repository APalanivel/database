SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
        
 dbo.Bucket_Site_Interval_Dtl_Del        
        
DESCRIPTION:        
        
 This procedure is used to Delete the records from dbo.Bucket_Site_Interval_Dtl table for given input.        
         
        
INPUT PARAMETERS:        
   Name                DataType  Default     Description         
-------------------------------------------------------------------------------------        
 @Client_Hier_Id        INT        
 @Bucket_Master_Id      INT        
 @Service_Start_Dt      DATE        
 @Service_End_Dt        DATE        
 @Data_Source_Cd        INT  
        
OUTPUT PARAMETERS:        
 Name       DataType     Default     Description                    
-------------------------------------------------------------------------------------        
        
USAGE EXAMPLES:        
-------------------------------------------------------------------------------------        
  
Example:-1 Starts Here  
    
BEGIN TRAN       
       
	DECLARE @tvp tvp_Bucket_Site_Interval_Dtl        
	INSERT INTO @tvp values (100001,110,'1/1/2013','1/5/2013',100350,150,12,null)        
        
	EXEC  dbo.Bucket_Site_Interval_Dtl_Ins @tvp,49,null        
       
	SELECT  * FROM Bucket_Site_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND  Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'      
                        AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350         
      
       
           
    EXEC dbo.Bucket_Site_Interval_Dtl_Del 100001,110, '1/1/2013', '1/5/2013',100350        
                              
	SELECT  * FROM Bucket_Site_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND  Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'      
                        AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350       
      
 ROLLBACK TRAN      
    
Example:-1 Ends Here           

AUTHOR INITIALS:        
 Initials	Name        
-------------------------------------------------------------------------------------        
   RKV		Ravi Kumar Vegesna        
                                      
MODIFICATIONS                    
 Initials  Date         Modification                    
-------------------------------------------------------------------------------------        
    RKV    2013-03-07   Created           
        
******/
CREATE PROCEDURE dbo.Bucket_Site_Interval_Dtl_Del
      (
       @Client_Hier_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Start_Dt DATE
      ,@Service_End_Dt DATE 
      ,@Data_Source_Cd INT )
AS
BEGIN

      SET NOCOUNT ON;

      DELETE FROM
            dbo.Bucket_Site_Interval_Dtl
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Start_Dt = @Service_Start_Dt
            AND Service_End_Dt = @Service_End_Dt
            AND Data_Source_Cd = @Data_Source_Cd

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Site_Interval_Dtl_Del] TO [CBMSApplication]
GO
