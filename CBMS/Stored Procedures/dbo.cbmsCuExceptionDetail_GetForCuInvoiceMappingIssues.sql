SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
    dbo.cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues      
      
DESCRIPTION:      
      
      
INPUT PARAMETERS:      
 Name    DataType   Default    Description      
-----------   ------------  -----------  --------------------------               
 @MyAccountId  INT       
 @cu_invoice_id  INT      
    
    
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
 exec cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues 3722703      
 exec cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues 3725217,15726      
 exec cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues 3721798,101590      
      
      
AUTHOR INITIALS:      
 Initials  Name      
-----------  -------------------------------------------------      
 HKT   Harish Kumar Tirumandyam    
       
MODIFICATIONS      
      
 Initials   Date    Modification      
-------------  ------------  -----------------------------------      
HKT     2020-04-22   Added Standard Data Exception  ( Union Part )     
HKT     2020-04-22   Added Header to SP .. there was no header     
           
******/    
    
CREATE PROCEDURE [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues]    
    (    
        @MyAccountId INT = NULL    
        , @cu_invoice_id INT    
    )    
AS    
    BEGIN    
    
        SET NOCOUNT ON;    
    
        DECLARE @mapping_type_id INT;    
    
        -- set nocount on        
    
        SELECT    
            @mapping_type_id = entity_id    
        FROM    
            entity WITH (NOLOCK)    
        WHERE    
            entity_type = 712    
            AND entity_name = 'Mapping Issues';    
    
        -- set nocount off        
    
    
        -- set nocount off        
    
        SELECT    
            ced.CU_EXCEPTION_DETAIL_ID
          , ced.CU_EXCEPTION_ID
          , ced.exception_group_type_id
          , ced.exception_group_type
          , ced.EXCEPTION_TYPE_ID
          , ced.EXCEPTION_TYPE
          , ced.EXCEPTION_STATUS_TYPE_ID
          , ced.exception_status_type
          , ced.OPENED_DATE
          , ced.IS_CLOSED
          , ced.CLOSED_REASON_TYPE_ID
          , ced.closed_reason_type
          , ced.CLOSED_BY_ID
          , ced.closed_by
          , ced.CLOSED_DATE   
        FROM    
        (   SELECT    
                ced.cu_exception_detail_id    
                , ced.cu_exception_id    
                , eg.entity_id exception_group_type_id    
                , eg.entity_name exception_group_type    
                , ced.exception_type_id    
                , et.exception_type    
                , ced.exception_status_type_id    
                , es.entity_name exception_status_type    
                , ced.opened_date    
                , ced.is_closed    
                , ced.closed_reason_type_id    
                , cr.entity_name closed_reason_type    
                , ced.closed_by_id    
                , ui.first_name + ' ' + ui.last_name closed_by    
                , ced.closed_date    
            FROM    
                cu_exception ce WITH (NOLOCK)    
                JOIN cu_exception_detail ced WITH (NOLOCK)    
                    ON ced.cu_exception_id = ce.cu_exception_id    
                LEFT OUTER JOIN cu_exception_type et WITH (NOLOCK)    
                    ON et.cu_exception_type_id = ced.exception_type_id    
                LEFT OUTER JOIN entity eg WITH (NOLOCK)    
                    ON eg.entity_id = et.exception_group_type_id    
                LEFT OUTER JOIN entity es WITH (NOLOCK)    
                    ON es.entity_id = ced.exception_status_type_id    
                LEFT OUTER JOIN entity cr WITH (NOLOCK)    
                    ON cr.entity_id = ced.closed_reason_type_id    
                LEFT OUTER JOIN user_info ui WITH (NOLOCK)    
                    ON ui.user_info_id = ced.closed_by_id    
            WHERE    
                ce.cu_invoice_id = @cu_invoice_id    
                AND et.exception_group_type_id = @mapping_type_id    
            UNION    
            SELECT    
                NULL    --ced.cu_exception_detail_id         
                , ced.Cu_Invoice_Standing_Data_Exception_Id    
                , NULL  --et.EXCEPTION_GROUP_TYPE_ID -- null --eg.entity_id exception_group_type_id        
                , NULL  -- eg.entity_name exception_group_type        
                , ced.Exception_Type_Cd AS exception_type_id    
                , cd.Code_Dsc AS exception_type    
                , ced.Exception_Status_Cd exception_status_type_id    
                , cdf.Code_Dsc AS exception_status_type    
                , ced.Created_Ts   as opened_date        
                , CASE WHEN cdf.Code_Dsc <> 'Closed' THEN 0    
                      ELSE 1    
                  END AS is_closed    
                , NULL  -- ced.closed_reason_type_id        
                , NULL  -- cr.entity_name closed_reason_type        
                , ced.Closed_By_User_Id    
                , ui.first_name + ' ' + ui.last_name closed_by    
                ,  ced.Closed_Ts        
            FROM    
                Cu_Invoice_Standing_Data_Exception ced WITH (NOLOCK)    
                LEFT OUTER JOIN user_info ui WITH (NOLOCK)    
                    ON ui.user_info_id = ced.Closed_By_User_Id    
                INNER JOIN Code cd WITH (NOLOCK)    
                    ON cd.code_id = ced.Exception_Type_Cd    
                INNER JOIN Code cdf WITH (NOLOCK)    
                    ON cdf.code_id = ced.Exception_Status_Cd    
            WHERE    
                ced.cu_invoice_id = @cu_invoice_id) ced    
        ORDER BY    
            ced.is_closed    
            , ced.opened_date DESC;    
    END;    
GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionDetail_GetForCuInvoiceMappingIssues] TO [CBMSApplication]
GO
