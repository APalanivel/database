SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
 Name     DataType  Default Description  
---------------------------------------------------------------  
 @Contract_Id   INT  
  
OUTPUT PARAMETERS:  
 Name     DataType  Default Description  
------------------------------------------------------------  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
   
  
 EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated   
      @Contract_Id =167450  
        , @Account_Id  = 1685738  
        , @Is_Associated_Invoice  = 0  
        , @Is_Dis_Associated_Invoice  = 1  
  
 EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated  
    @Account_Id = 153176      
    , @Contract_id = 166628  
    , @Is_Associated_Invoice = 0  
    , @Is_Dis_Associated_Invoice = 1  


	 EXEC Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated  
    @Account_Id = null      
    , @Contract_id = 166628  
    , @Is_Associated_Invoice = 0  
    , @Is_Dis_Associated_Invoice = 1  
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayaana Reddy  
   
MODIFICATIONS  
 Initials	Date			Modification  
------------------------------------------------------------  
 NR        2019-07-18		Created for Add - contract
 NR		   2020-01-29		MAINT-9782 -  Added   
 NR		   2020-04-29		MAINT-10157 - Removed contract filter on map table for un associated list to if invoice is mapped any other contract.
  
******/
CREATE PROCEDURE [dbo].[Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated]
    (
        @Contract_Id INT
        , @Account_Id INT = NULL
        , @Is_Associated_Invoice BIT = 0
        , @Is_Dis_Associated_Invoice BIT = 0
    )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE @Supplier_Vendor_Type_Id INT;




        SELECT
            @Supplier_Vendor_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_TYPE = 155
            AND e.ENTITY_DESCRIPTION = 'Vendor';


        CREATE TABLE #Client_Hier_Account
             (
                 Account_Id INT
                 , Contract_Id INT
                 , Contract_Start_Date DATE
                 , Contract_End_Date DATE
             );


        INSERT INTO #Client_Hier_Account
             (
                 Account_Id
                 , Contract_Id
                 , Contract_Start_Date
                 , Contract_End_Date
             )
        SELECT
            utility_cha.Account_Id
            , @Contract_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                          AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                            AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                              AND     c.CONTRACT_END_DATE
                    OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                  AND     c.CONTRACT_END_DATE)
        GROUP BY
            utility_cha.Account_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;




        SELECT
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED
        FROM
            #Client_Hier_Account Cha
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = Cha.Account_Id
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            (   @Account_Id IS NULL
                OR  Cha.Account_Id = @Account_Id)
            AND (   cism.Begin_Dt BETWEEN Cha.Contract_Start_Date
                                  AND     Cha.Contract_End_Date
                    OR  cism.End_Dt BETWEEN Cha.Contract_Start_Date
                                    AND     Cha.Contract_End_Date
                    OR  Cha.Contract_Start_Date BETWEEN cism.Begin_Dt
                                                AND     cism.End_Dt
                    OR  Cha.Contract_End_Date BETWEEN cism.Begin_Dt
                                              AND     cism.End_Dt)
            AND (   (   @Is_Associated_Invoice = 1
                        AND @Is_Dis_Associated_Invoice = 0
                        AND EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Contract_Cu_Invoice_Map ccim
                                       WHERE
                                            ccim.Cu_Invoice_Id = cism.CU_INVOICE_ID
                                            AND ccim.Contract_Id = Cha.Contract_Id
                                            AND (   @Account_Id IS NULL
                                                    OR  ccim.Account_Id = @Account_Id)))
                    OR  (   @Is_Associated_Invoice = 0
                            AND @Is_Dis_Associated_Invoice = 1
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Contract_Cu_Invoice_Map ccim
                                               WHERE
                                                    ccim.Cu_Invoice_Id = cism.CU_INVOICE_ID
                                                    AND ccim.Account_Id = cism.Account_ID
                                                    AND (   @Account_Id IS NULL
                                                            OR  ccim.Account_Id = @Account_Id))))
        GROUP BY
            cism.CU_INVOICE_ID
            , cism.Account_ID
            , ci.CBMS_IMAGE_ID
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.IS_PROCESSED;
    END;






GO
GRANT EXECUTE ON  [dbo].[Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated] TO [CBMSApplication]
GO
