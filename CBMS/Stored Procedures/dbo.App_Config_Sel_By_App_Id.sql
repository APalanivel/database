SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   App_Config_Sel_By_App_Id       

    
DESCRIPTION:          
	
      
INPUT PARAMETERS:          
Name				DataType		Default		Description          
------------------------------------------------------------          
@App_Id				INT
@App_Config_Cd		VARCHAR(255)	NULL
                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

EXEC App_Config_Sel_By_App_Id 1, 'Xid'

EXEC App_Config_Sel_By_App_Id 1

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			04/22/2010	Created
*****/ 
CREATE PROCEDURE [dbo].[App_Config_Sel_By_App_Id]
(	@App_Id				INT
	,@App_Config_Cd		VARCHAR(255)  = NULL		)
AS 
BEGIN
	SET NOCOUNT ON; 
	
	SELECT
		App_Config_Id
		,App_Id
		,App_Config_Cd
		,App_Config_Value
		,Is_Active
		,Start_Dt
		,End_Dt
	FROM
		App_Config
	WHERE 
		App_Id = @App_Id
		AND ( @App_Config_Cd IS NULL
				OR App_Config_Cd = @App_Config_Cd  ) 
END
GO
GRANT EXECUTE ON  [dbo].[App_Config_Sel_By_App_Id] TO [CBMSApplication]
GO
