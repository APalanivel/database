SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id       
              
Description:              
			This sproc to get the bucket details details for a Given id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@State_Id							INT
    @Bucket_Master_Id					INT	
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id 124,329
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id1 124,329

   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id 124,849
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id1 124,849

   
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id 1,70       
   Exec dbo.EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id1 1,70       
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
    RKV             2015-11-10      changed the table EC_Calc_Val to Ec_Calc_Val_Bucket_Sub_Bucket_Map 
									to get the info of sub_buckets as Part of AS400-PII 
	HG				2020-01-24		MAINT-9779,Optimized the proc for performance.
												Data Exists check on Invoice Charge/Determinant and Calc value moved to a separate query to improve the performance
******/
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id]
    (
        @State_Id         INT
       ,@Bucket_Master_Id INT
    )
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @is_used INT;

    CREATE TABLE #Sub_Bucket
        (
            EC_Invoice_Sub_Bucket_Master_Id INT
           ,Bucket_Master_Id                INT
           ,Sub_Bucket_Name                 VARCHAR (255)
           ,Commodity_id                    INT
           ,Country_Id                      INT
           ,State_Id                        INT
           ,State_Name                      VARCHAR (200)
           ,Is_Used                         INT
                DEFAULT (0)
           ,Bucket_Type                     VARCHAR (25)
        );

    INSERT INTO #Sub_Bucket
    (
        EC_Invoice_Sub_Bucket_Master_Id
       ,Bucket_Master_Id
       ,Sub_Bucket_Name
       ,Commodity_id
       ,Country_Id
       ,State_Id
       ,State_Name
       ,Is_Used
       ,Bucket_Type
    )
    SELECT
        eisbm.EC_Invoice_Sub_Bucket_Master_Id
       ,eisbm.Bucket_Master_Id
       ,eisbm.Sub_Bucket_Name
       ,bm.Commodity_Id
       ,s.COUNTRY_ID
       ,s.STATE_ID
       ,s.STATE_NAME
       ,0 AS Is_Used
       ,btc.Code_Value Bucket_type
    FROM
        dbo.EC_Invoice_Sub_Bucket_Master eisbm
        INNER JOIN dbo.Bucket_Master bm
            ON bm.Bucket_Master_Id = eisbm.Bucket_Master_Id
        INNER JOIN dbo.Code btc
            ON btc.Code_Id = bm.Bucket_Type_Cd
        INNER JOIN dbo.STATE s
            ON s.STATE_ID = eisbm.State_Id
    WHERE
        eisbm.State_Id = @State_Id
        AND eisbm.Bucket_Master_Id = @Bucket_Master_Id;

    UPDATE sb
    SET sb.Is_Used = 1
    FROM
        #Sub_Bucket sb
    WHERE
        sb.Bucket_Type = 'Determinant'
        AND sb.Is_Used = 0
        AND EXISTS (
                       SELECT 1
                       FROM
                           dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                       WHERE
                           ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id = sb.EC_Invoice_Sub_Bucket_Master_Id
                   );

    UPDATE sb
    SET sb.Is_Used = 1
    FROM
        #Sub_Bucket sb
    WHERE
        sb.Bucket_Type = 'Determinant'
        AND sb.Is_Used = 0
        AND EXISTS (
                       SELECT 1
                       FROM
                           dbo.CU_INVOICE_DETERMINANT cid
                       WHERE
                           cid.Bucket_Master_Id = @Bucket_Master_Id
                           AND cid.EC_Invoice_Sub_Bucket_Master_Id = sb.EC_Invoice_Sub_Bucket_Master_Id
                   );
    UPDATE sb
    SET sb.Is_Used = 1
    FROM
        #Sub_Bucket sb
    WHERE
        sb.Bucket_Type = 'Charge'
        AND sb.Is_Used = 0
        AND EXISTS (
                       SELECT 1
                       FROM
                           dbo.CU_INVOICE_CHARGE cic
                       WHERE
                           cic.Bucket_Master_Id = @Bucket_Master_Id
                           AND cic.EC_Invoice_Sub_Bucket_Master_Id = sb.EC_Invoice_Sub_Bucket_Master_Id
                   );

    SELECT
        EC_Invoice_Sub_Bucket_Master_Id
       ,Bucket_Master_Id
       ,Sub_Bucket_Name
       ,Commodity_id
       ,Country_Id
       ,State_Id
       ,State_Name
       ,Is_Used
       ,Bucket_Type
    FROM
        #Sub_Bucket
    ORDER BY Sub_Bucket_Name;

    DROP TABLE #Sub_Bucket;
END;
GO

GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Sel_By_State_Bucket_Master_Id] TO [CBMSApplication]
GO
