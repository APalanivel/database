SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- exec dbo.BUDGET_GET_DETAILS_FOR_ALL_ACCOUNTS_P 1313
CREATE     PROCEDURE dbo.BUDGET_GET_DETAILS_FOR_ALL_ACCOUNTS_P
	@budget_id int
	AS
	begin
		set nocount on

		select	budget_account.budget_account_id,
			budget_details.month_identifier,
			case when isnull(budget_account.has_details_saved, 0) > 0
			     then budget_details.variable
			     when budget.commodity_type_id = 290
			     then budget_details.variable
			     when vw.Tariff_Transport = 'Tariff' and utilacc.service_level_type_id = 861
			     then budget_details.variable
			     when vw.Tariff_Transport = 'Tariff' and utilacc.service_level_type_id = 1024
			     then budget_details.variable

			else 'A1'
			end variable,	     	
			budget_details.transportation,
			budget_details.transmission,
			budget_details.distribution,
			budget_details.other_bundled,
			budget_details.other_fixed_costs,
			budget_details.sourcing_tax,
			budget_details.rates_tax,
			budget_details.is_manual_generation,
			budget_details.is_manual_sourcing_tax
	 
		from 	budget 
			join budget_account on budget_account.budget_id = budget.budget_id and budget.budget_id = @budget_id 
			join budget_details on budget_details.budget_account_id = budget_account.budget_account_id
			join account utilacc on utilacc.account_id = budget_account.account_id
			join BUDGET_TARIFF_TRANSPORT_VW vw on vw.account_id = utilacc.account_id
			and vw.commodity_type_id = budget.commodity_type_id
			

	end









GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_DETAILS_FOR_ALL_ACCOUNTS_P] TO [CBMSApplication]
GO
