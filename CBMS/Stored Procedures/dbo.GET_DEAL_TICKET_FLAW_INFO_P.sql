SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_FLAW_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.GET_DEAL_TICKET_FLAW_INFO_P  

@userId varchar,
@sessionId varchar,
@dealTicketId int

AS
set nocount on
	SELECT 
		IS_MISTAKES_DETECTED,
		COST_TO_SUMMIT, 
		MONTH_TO_PAY, 
		COMMENTS
	FROM 
		RM_FLAWLESS_EXECUTION 
	WHERE	
		RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_FLAW_INFO_P] TO [CBMSApplication]
GO
