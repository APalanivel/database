SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SOP_SUMMARY_VOLUME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@rfpAccountGroupId	int       	          	
	@isBidGroup    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_SOP_SUMMARY_VOLUME_P 
@userId varchar,
@sessionId varchar,
@rfpAccountGroupId int,
@isBidGroup int


as
set nocount on
--DECLARE @volume numeric(32,16)
--DECLARE @volume numeric
--DECLARE @volume varchar
--SELECT @volume = (

select VOLUME 
			  from 	SR_RFP_SOP_SUMMARY
			  Where	SR_ACCOUNT_GROUP_ID = @rfpAccountGroupId
			  AND IS_BID_GROUP    =	 @isBidGroup
--)



--RETURN  @volume
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SOP_SUMMARY_VOLUME_P] TO [CBMSApplication]
GO
