
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_UPDATE_SEND_SUPPLIER_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                      DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id					varchar(10)
	@session_id					varchar(20)
	@rfp_id						int
	@account_group_id			int
	@is_bid_group				bit
	@supplier_id				int
	@contact_id					int
	@status						varchar(100)
	@sr_rfp_email_log_id		int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.SR_RFP_UPDATE_SEND_SUPPLIER_P '49', '-1', 13, 19, 0, 1091, 2, 'Post',null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values
	RR			2016-04-26	Removing the service condition data whenver a supplier posting is removed from posted list
******/

CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_SEND_SUPPLIER_P]
      ( 
       @user_id VARCHAR(10)
      ,@session_id VARCHAR(20)
      ,@rfp_id INT
      ,@account_group_id INT
      ,@is_bid_group BIT
      ,@supplier_id INT
      ,@contact_id INT
      ,@status VARCHAR(100)
      ,@sr_rfp_email_log_id INT )
AS 
BEGIN
      DECLARE
            @status_type_id INT
           ,@sr_rfp_send_supplier_id INT
      SELECT
            @status_type_id = entity_id
      FROM
            entity
      WHERE
            entity_type = 1013
            AND entity_name = @status
            
      DECLARE @supplierContactId INT
      DECLARE @bid_id INT

      SELECT
            @supplierContactId = sr_rfp_supplier_contact_vendor_map_id
      FROM
            SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
      WHERE
            sr_supplier_contact_info_id = @contact_id
            AND vendor_id = @supplier_id
            AND sr_rfp_id = @rfp_id 

      DECLARE @bid_status VARCHAR(10)
      IF @status = 'Post' 
            BEGIN
                  SELECT
                        @bid_status = '1151'
            END
	
      IF @is_bid_group = 1 
            BEGIN
                  SELECT
                        @sr_rfp_send_supplier_id = sr_rfp_send_supplier_id
                  FROM
                        sr_rfp_send_supplier
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 1
                        AND sr_rfp_supplier_contact_vendor_map_id = @supplierContactId

                  IF isnull(@sr_rfp_send_supplier_id, 0) > 0 
                        BEGIN
                              UPDATE
                                    sr_rfp_send_supplier
                              SET   
                                    status_type_id = @status_type_id
                                   ,sr_rfp_email_log_id = @sr_rfp_email_log_id
                                   ,bid_status_type_id = @bid_status
                              WHERE
                                    sr_account_group_id = @account_group_id
                                    AND is_bid_group = 1
                                    AND sr_rfp_supplier_contact_vendor_map_id = @supplierContactId
	
                              IF @status = 'Post' 
                                    BEGIN

                                          INSERT      INTO sr_rfp_send_supplier_log
                                                      ( 
                                                       sr_rfp_id
                                                      ,sr_rfp_send_supplier_id
                                                      ,generation_date
                                                      ,posted_by )
                                          VALUES
                                                      ( 
                                                       @rfp_id
                                                      ,@sr_rfp_send_supplier_id
                                                      ,getdate()
                                                      ,@user_id )			
				
                                          UPDATE
                                                sr_rfp_checklist
                                          SET   
                                                rfp_sent_date = getdate()
                                          WHERE
                                                sr_rfp_account_id IN ( SELECT
                                                                        sr_rfp_account_id
                                                                       FROM
                                                                        sr_rfp_account
                                                                       WHERE
                                                                        sr_rfp_bid_group_id = @account_group_id
                                                                        AND is_deleted = 0 )


                                          UPDATE
                                                sr_rfp_account
                                          SET   
                                                bid_status_type_id = 1151 --@bid_group_status_id
                                          WHERE
                                                sr_rfp_bid_group_id = @account_group_id
				
                                          UPDATE
                                                sr_rfp
                                          SET   
                                                rfp_status_type_id = 1059
                                          WHERE
                                                sr_rfp_id = @rfp_id
				
                                    END
                        END
                  ELSE 
                        BEGIN
                              INSERT      INTO sr_rfp_send_supplier
                                          ( 
                                           sr_account_group_id
                                          ,is_bid_group
                                          ,sr_rfp_supplier_contact_vendor_map_id
                                          ,status_type_id
                                          ,sr_rfp_email_log_id
                                          ,bid_status_type_id )
                                          SELECT
                                                @account_group_id
                                               ,1
                                               ,@supplierContactId
                                               ,@status_type_id
                                               ,@sr_rfp_email_log_id
                                               ,@bid_status

                              SELECT
                                    @sr_rfp_send_supplier_id = @@identity
			
                              IF @status = 'Post' 
                                    BEGIN
                                          INSERT      INTO sr_rfp_send_supplier_log
                                                      ( 
                                                       sr_rfp_id
                                                      ,sr_rfp_send_supplier_id
                                                      ,generation_date
                                                      ,posted_by )
                                          VALUES
                                                      ( 
                                                       @rfp_id
                                                      ,@sr_rfp_send_supplier_id
                                                      ,getdate()
                                                      ,@user_id )
				
                                          UPDATE
                                                sr_rfp_checklist
                                          SET   
                                                rfp_sent_date = getdate()
                                          WHERE
                                                sr_rfp_account_id IN ( SELECT
                                                                        sr_rfp_account_id
                                                                       FROM
                                                                        sr_rfp_account
                                                                       WHERE
                                                                        sr_rfp_bid_group_id = @account_group_id
                                                                        AND is_deleted = 0 )

                                          UPDATE
                                                sr_rfp_account
                                          SET   
                                                bid_status_type_id = 1151--@bid_group_status_id
                                          WHERE
                                                sr_rfp_bid_group_id = @account_group_id
				
                                          UPDATE
                                                sr_rfp
                                          SET   
                                                rfp_status_type_id = 1059
                                          WHERE
                                                sr_rfp_id = @rfp_id
				
                                    END
				
                        END
            END
      ELSE 
            BEGIN

                  SELECT
                        @sr_rfp_send_supplier_id = sr_rfp_send_supplier_id
                  FROM
                        sr_rfp_send_supplier
                  WHERE
                        sr_account_group_id = @account_group_id
                        AND is_bid_group = 0
                        AND sr_rfp_supplier_contact_vendor_map_id = @supplierContactId

                  IF isnull(@sr_rfp_send_supplier_id, 0) > 0 
                        BEGIN
                              UPDATE
                                    sr_rfp_send_supplier
                              SET   
                                    status_type_id = @status_type_id
                                   ,sr_rfp_email_log_id = @sr_rfp_email_log_id
                                   ,bid_status_type_id = @bid_status
                              WHERE
                                    sr_account_group_id = @account_group_id
                                    AND is_bid_group = 0
                                    AND sr_rfp_supplier_contact_vendor_map_id = @supplierContactId

                              IF @status = 'Post' 
                                    BEGIN

                                          INSERT      INTO sr_rfp_send_supplier_log
                                                      ( 
                                                       sr_rfp_id
                                                      ,sr_rfp_send_supplier_id
                                                      ,generation_date
                                                      ,posted_by )
                                          VALUES
                                                      ( 
                                                       @rfp_id
                                                      ,@sr_rfp_send_supplier_id
                                                      ,getdate()
                                                      ,@user_id )
				
                                          UPDATE
                                                sr_rfp_checklist
                                          SET   
                                                rfp_sent_date = getdate()
                                          WHERE
                                                sr_rfp_account_id = @account_group_id

                                          UPDATE
                                                sr_rfp_account
                                          SET   
                                                bid_status_type_id = 1151--@bid_group_status_id
                                          WHERE
                                                sr_rfp_account_id = @account_group_id
                                          UPDATE
                                                sr_rfp
                                          SET   
                                                rfp_status_type_id = 1059 --@rfp_status_id
                                          WHERE
                                                sr_rfp_id = @rfp_id
				
                                    END

                        END
                  ELSE 
                        BEGIN
                              INSERT      INTO sr_rfp_send_supplier
                                          ( 
                                           sr_account_group_id
                                          ,is_bid_group
                                          ,sr_rfp_supplier_contact_vendor_map_id
                                          ,status_type_id
                                          ,sr_rfp_email_log_id
                                          ,bid_status_type_id )
                                          SELECT
                                                @account_group_id
                                               ,0
                                               ,@supplierContactId
                                               ,@status_type_id
                                               ,@sr_rfp_email_log_id
                                               ,@bid_status

                              SELECT
                                    @sr_rfp_send_supplier_id = @@identity
			
                              IF @status = 'Post' 
                                    BEGIN
                                          INSERT      INTO sr_rfp_send_supplier_log
                                                      ( 
                                                       sr_rfp_id
                                                      ,sr_rfp_send_supplier_id
                                                      ,generation_date
                                                      ,posted_by )
                                          VALUES
                                                      ( 
                                                       @rfp_id
                                                      ,@sr_rfp_send_supplier_id
                                                      ,getdate()
                                                      ,@user_id )
				
                                          UPDATE
                                                sr_rfp_checklist
                                          SET   
                                                rfp_sent_date = getdate()
                                          WHERE
                                                sr_rfp_account_id = @account_group_id

				--select @bid_group_status_id = entity_id from entity where entity_type = 1029 and entity_name = 'Open'
				--select @rfp_status_id = entity_id from entity where entity_type = 1004 and entity_name = 'Open'
                                          UPDATE
                                                sr_rfp_account
                                          SET   
                                                bid_status_type_id = 1151 --@bid_group_status_id
                                          WHERE
                                                sr_rfp_account_id = @account_group_id
                                          UPDATE
                                                sr_rfp
                                          SET   
                                                rfp_status_type_id = 1059
                                          WHERE
                                                sr_rfp_id = @rfp_id
				
                                    END
                        END
            END

      IF @status = 'Remove' 
            BEGIN
                  DELETE
                        tqr
                  FROM
                        dbo.Sr_Rfp_Service_Condition_Template_Question_Response tqr
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = srat.SR_RFP_ACCOUNT_TERM_ID
                                 WHERE
                                    srat.SR_ACCOUNT_GROUP_ID = @account_group_id
                                    AND srat.IS_BID_GROUP = @is_bid_group
                                    AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                                    AND tqr.SR_RFP_BID_ID = tpm.SR_RFP_BID_ID )
                                    
                  DELETE
                        scqm
                  FROM
                        dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map scqm
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    INNER JOIN dbo.SR_RFP_ACCOUNT_TERM srat
                                          ON tpm.SR_RFP_ACCOUNT_TERM_ID = srat.SR_RFP_ACCOUNT_TERM_ID
                                 WHERE
                                    srat.SR_ACCOUNT_GROUP_ID = @account_group_id
                                    AND srat.IS_BID_GROUP = @is_bid_group
                                    AND tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                                    AND scqm.SR_RFP_BID_ID = tpm.SR_RFP_BID_ID )
                        
                        
                  DELETE
                        dbo.SR_RFP_TERM_PRODUCT_MAP
                  FROM
                        dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                  WHERE
                        tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.SR_RFP_ACCOUNT_TERM srat
                                     WHERE
                                          srat.SR_ACCOUNT_GROUP_ID = @account_group_id
                                          AND srat.IS_BID_GROUP = @is_bid_group
                                          AND tpm.SR_RFP_ACCOUNT_TERM_ID = srat.SR_RFP_ACCOUNT_TERM_ID )
            END

END;

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SEND_SUPPLIER_P] TO [CBMSApplication]
GO
