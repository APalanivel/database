SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Dates_Validation
           
DESCRIPTION:             
			
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT
    @DMO_Supp_Start_Dt	DATE
    @DMO_Supp_End_Dt	DATE		NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT a.*,b.Client_Id,b.Sitegroup_Id,b.Site_Id FROM dbo.Client_Hier_DMO_Config a 
	INNER JOIN Core.Client_Hier b ON a.Client_Hier_Id = b.Client_Hier_Id
	--WHERE b.Sitegroup_Id>0
	--AND b.Site_Id > 0
	ORDER BY b.Client_Id

            
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 218,100247,'2017-02-01','2017-06-01'
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 218,100247,'2017-02-01','2018-06-01'
	
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 1009,291,'2013-02-01','2013-12-01'
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 1009,291,'2013-01-01','2014-12-01'
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 1009,290,'2014-01-01','2014-12-01'
	
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 1009,291,'2016-02-01','2016-10-01'
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 1009,291,'2016-02-01','2016-03-01'
	
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 11278,290,'2017-09-01','2017-10-01'
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 11278,290,'2017-09-01',NULL
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 12032,327391,290,'2017-03-01','2017-12-31'	
	
	EXEC dbo.DMO_Supplier_Account_Dates_Validation 11987,314143,290,'2017-01-01','2017-07-31'
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 11987,290,'2017-01-01','2017-07-31',314143,null,null,null,null,null

	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-08	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Dates_Validation]
      ( 
       @Client_Id INT
      ,@Site_Id INT
      ,@Commodity_Id INT
      ,@DMO_Supp_Start_Dt DATE
      ,@DMO_Supp_End_Dt DATE = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Is_DMO_Config_Dates_Valid BIT = 1
      
      DECLARE
            @Sitegroup_Id INT
           ,@Account_Hier_level_Cd INT
           
      --SELECT
      --      @DMO_Supp_End_Dt = DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt)
      --WHERE
      --      @DMO_Supp_End_Dt IS NOT NULL
            
           
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Site_Id = @Site_Id
           
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Hier_level_Cd INT
            ,Account_Id INT
            ,Account_DMO_Config_Id INT
            ,Code_Dsc VARCHAR(255)
            ,Commodity_Id INT
            ,Commodity_Name VARCHAR(50)
            ,DMO_Start_Dt VARCHAR(10)
            ,DMO_End_Dt VARCHAR(10)
            ,Updated_User VARCHAR(100)
            ,Last_Change_Ts DATETIME )
      
      SELECT
            @Account_Hier_level_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND cd.Code_Value = 'Account'
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Account_Id
                  ,Account_DMO_Config_Id
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,NULL AS Account_Id
                       ,NULL AS Account_DMO_Config_Id
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,chdc.DMO_Start_Dt AS DMO_Start_Dt
                       ,chdc.DMO_End_Dt AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                      OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 ) )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND @DMO_Supp_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                            AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND ( @DMO_Supp_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt)), @DMO_Supp_End_Dt) <= 29 ) ) )
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Account_Id
                  ,Account_DMO_Config_Id
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,NULL AS Account_Id
                       ,NULL AS Account_DMO_Config_Id
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,chdc.DMO_Start_Dt AS DMO_Start_Dt
                       ,chdc.DMO_End_Dt AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                      OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 ) )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND @DMO_Supp_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                            AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND ( @DMO_Supp_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt)), @DMO_Supp_End_Dt) <= 29 ) ) )
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Account_Id
                  ,Account_DMO_Config_Id
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        chdc.Client_Hier_DMO_Config_Id
                       ,chdc.Client_Hier_Id
                       ,ch.Hier_level_Cd
                       ,NULL AS Account_Id
                       ,NULL AS Account_DMO_Config_Id
                       ,cd.Code_Dsc
                       ,chdc.Commodity_Id
                       ,com.Commodity_Name
                       ,chdc.DMO_Start_Dt AS DMO_Start_Dt
                       ,chdc.DMO_End_Dt AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,chdc.Last_Change_Ts
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON chdc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ch.Hier_level_Cd = cd.Code_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON chdc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                      OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 ) )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND @DMO_Supp_End_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                            AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, chdc.DMO_Start_Dt) <= 29 )
                                   AND ( @DMO_Supp_End_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt))
                                         OR DATEDIFF(dd, DATEADD(DD, -DATEPART(DD, chdc.DMO_End_Dt), DATEADD(mm, 1, chdc.DMO_End_Dt)), @DMO_Supp_End_Dt) <= 29 ) ) )
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_DMO_Config_Id
                  ,Client_Hier_Id
                  ,Hier_level_Cd
                  ,Account_Id
                  ,Account_DMO_Config_Id
                  ,Code_Dsc
                  ,Commodity_Id
                  ,Commodity_Name
                  ,DMO_Start_Dt
                  ,DMO_End_Dt
                  ,Updated_User
                  ,Last_Change_Ts )
                  SELECT
                        NULL AS Client_Hier_DMO_Config_Id
                       ,cha.Client_Hier_Id
                       ,@Account_Hier_level_Cd AS Hier_level_Cd
                       ,adc.Account_Id
                       ,adc.Account_DMO_Config_Id
                       ,'Account' AS Code_Dsc
                       ,adc.Commodity_Id
                       ,com.Commodity_Name
                       ,adc.DMO_Start_Dt AS DMO_Start_Dt
                       ,adc.DMO_End_Dt AS DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                       ,adc.Last_Change_Ts
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.Commodity com
                              ON adc.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON adc.Updated_User_Id = ui.USER_INFO_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Site_Id = @Site_Id
                        AND adc.Commodity_Id = @Commodity_Id
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL
                                AND ( @DMO_Supp_Start_Dt >= adc.DMO_Start_Dt
                                      OR DATEDIFF(dd, @DMO_Supp_Start_Dt, adc.DMO_Start_Dt) <= 29 ) )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND ( @DMO_Supp_Start_Dt >= adc.DMO_Start_Dt
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, adc.DMO_Start_Dt) <= 29 )
                                   AND @DMO_Supp_End_Dt >= adc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND ( @DMO_Supp_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                            AND     DATEADD(DD, -DATEPART(DD, adc.DMO_End_Dt), DATEADD(mm, 1, adc.DMO_End_Dt))
                                         OR DATEDIFF(dd, @DMO_Supp_Start_Dt, adc.DMO_Start_Dt) <= 29 )
                                   AND ( @DMO_Supp_End_Dt BETWEEN adc.DMO_Start_Dt
                                                          AND     DATEADD(DD, -DATEPART(DD, adc.DMO_End_Dt), DATEADD(mm, 1, adc.DMO_End_Dt))
                                         OR DATEDIFF(dd, DATEADD(DD, -DATEPART(DD, adc.DMO_End_Dt), DATEADD(mm, 1, adc.DMO_End_Dt)), @DMO_Supp_End_Dt) <= 29 ) ) )
                  GROUP BY
                        cha.Client_Hier_Id
                       ,adc.Account_Id
                       ,adc.Account_DMO_Config_Id
                       ,adc.Commodity_Id
                       ,com.Commodity_Name
                       ,adc.DMO_Start_Dt
                       ,adc.DMO_End_Dt
                       ,ui.FIRST_NAME + ' ' + ui.LAST_NAME
                       ,adc.Last_Change_Ts
      
      SELECT
            tc.Client_Hier_DMO_Config_Id
           ,tc.Client_Hier_Id
           ,tc.Hier_level_Cd
           ,tc.Account_Id
           ,tc.Account_DMO_Config_Id
           ,tc.Code_Dsc
           ,tc.Commodity_Id
           ,tc.Commodity_Name
           ,tc.DMO_Start_Dt
           ,CAST(DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) AS DATE) AS DMO_End_Dt
           ,tc.Updated_User
           ,tc.Last_Change_Ts
      FROM
            @Tbl_Config tc
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                         WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = 0 )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Hier_Not_Applicable_DMO_Config na
                              INNER JOIN Core.Client_Hier ch
                                    ON na.Client_Hier_Id = ch.Client_Hier_Id
                             WHERE
                              na.Client_Hier_DMO_Config_Id = tc.Client_Hier_DMO_Config_Id
                              AND @Client_Id IS NOT NULL
                              AND @Sitegroup_Id IS NOT NULL
                              AND @Site_Id IS NOT NULL
                              AND ch.Client_Id = @Client_Id
                              AND ch.Sitegroup_Id = @Sitegroup_Id
                              AND ch.Site_Id = @Site_Id )
      ORDER BY
            tc.Hier_level_Cd                                          
     
                                                             
END;
;




;
GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Dates_Validation] TO [CBMSApplication]
GO
