SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******    
NAME:    
   
 dbo.UBM_BATCH_LOG_DETAILS_UPD  
   
 DESCRIPTION:     
   
 Update Expected record count in the UBM_BATCH_LOG_DETAILS table.  
   
 INPUT PARAMETERS:    
 Name   DataType Default  Description    
------------------------------------------------------------      
 @Ubm_Batch_Master_Log_Id Integer  
  
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
  USAGE EXAMPLES:    
------------------------------------------------------------    
  
 EXEC dbo.[UBM_BATCH_LOG_DETAILS_UPD] 25611  
  
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 AC   Ajay Chejarla    8/22/2016  
  
 MODIFICATIONS     
 Initials Date  Modification    
------------------------------------------------------------           
******/  
  
  
CREATE PROCEDURE [dbo].[UBM_BATCH_LOG_DETAILS_UPD]  
(  
 @Ubm_Batch_Master_Log_Id Integer
)  
AS  
BEGIN  
  
 SET NOCOUNT ON    
     
 DECLARE @INSERTED_ROWS INTEGER
 
	SELECT 
		@INSERTED_ROWS = COUNT(*) 
	FROM UBM_INVOICE
	WHERE
		 UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id
		 
    UPDATE UBM_BATCH_LOG_DETAILS
    SET 
	    ACTUAL_IMAGE_RECORDS = @INSERTED_ROWS
    WHERE UBM_BATCH_LOG_ID IN (
							 SELECT 
								  UBM_BATCH_LOG_ID  
							 FROM UBM_BATCH_LOG ubl  
							 WHERE 
									ubl.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id  
                             )

	UPDATE UBM_BATCH_LOG_DETAILS
    SET 
	    ACTUAL_DATA_RECORDS = @INSERTED_ROWS
    WHERE UBM_BATCH_LOG_ID IN (
							 SELECT 
								  UBM_BATCH_LOG_ID  
							 FROM UBM_BATCH_LOG ubl  
							 WHERE 
									ubl.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id  
									AND ubl.CASS_TABLE_NAME = 'Bills' 
                             )


   UPDATE UBM_BATCH_MASTER_LOG SET
		ACTUAL_DATA_RECORDS = @INSERTED_ROWS,
	    ACTUAL_IMAGE_RECORDS = @INSERTED_ROWS
  WHERE UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id  
  
END  
  
;
GO
GRANT EXECUTE ON  [dbo].[UBM_BATCH_LOG_DETAILS_UPD] TO [CBMSApplication]
GO
