
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Master_Variance_Test_SEL_Commodity_By_Category      
     
DESCRIPTION:  Used to select commdoities from Variance_Category_Commodity_Map table using the category Id      
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
@Category_Cd   Int       
            
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:       
    
 EXEC dbo.Master_Variance_Test_SEL_Commodity_By_Category 100175    
 EXEC dbo.Master_Variance_Test_SEL_Commodity_By_Category 100176    
 EXEC dbo.Master_Variance_Test_SEL_Commodity_By_Category 100177    
     
    
select * from Variance_Category_Commodity_Map    
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
NK  Nageswara Rao Kosuri             
HG  Hari    
SKA  Shobhit    
    
Initials Date  Modification      
------------------------------------------------------------      
NK   10/19/2009  Created    
HG   02/08/2010 Commodity table added in the from clause to get the commodity name.    
SKA  02/26/2010 Added usage examples    
HG   04/14/2010 Order by clause added to show the EP/NG commodity first followed by other services.    
DMR  09/10/2010 Modified for Quoted_Identifier    
AKR  Modified as a part of Detailed Variance     
    
******/    
CREATE PROCEDURE dbo.Master_Variance_Test_SEL_Commodity_By_Category ( @Category_Cd INT )
AS 
BEGIN    
    
      SET NOCOUNT ON ;      
      
      SELECT
            com.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Variance_Parameter_Commodity_Map vccm
            INNER JOIN dbo.Variance_Parameter vp
                  ON vp.Variance_Parameter_Id = vccm.Variance_Parameter_Id
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = vccm.Commodity_Id
      WHERE
            vp.Variance_Category_Cd = @Category_Cd
      GROUP BY
            com.Commodity_Id
           ,com.Commodity_Name
           ,com.Is_Alternate_Fuel
      ORDER BY
            com.Is_Alternate_Fuel ASC
           ,Com.Commodity_Name      
    
END 


;
GO

GRANT EXECUTE ON  [dbo].[Master_Variance_Test_SEL_Commodity_By_Category] TO [CBMSApplication]
GO
