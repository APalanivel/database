
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: dbo.Copy_determinant_charge_Resolve_month_by_Old_Cu_invoice_Id
	

DESCRIPTION:
	Based on old cu_invoice_id it will create the copy for CU_INVOICE_DETERMINANT,CU_INVOICE_DETERMINANT_ACCOUNT & CU_INVOICE_CHARGE,CU_INVOICE_CHARGE_ACCOUNT
	This sproc are using for copying the old invoice template to the new invoice. 
	If the new invoice does not have all the accounts of the old invoice, then only those account ids we are updating as NULL

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Old_cu_invoice_id 	INT  
	@New_cu_invoice_id 	INT  

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.Copy_determinant_charge_Resolve_month_by_Old_Cu_invoice_Id

	GRANTEXECUTE

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	SKA			Shobhit Agrawal
	HG			Harihara Suthan G
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SSR        	04/16/2010	Created
	SKA			05/04/2010	Removed the cursor from the script
	SKA			06/28/2010	Removed the insertion of two columns (UBM_SERVICE_TYPE_ID,UBM_SERVICE_CODE) from sp
	HG			10/08/2010	Following changes made to fix #21226
							Logic to populate the Cu_Invoice_Determinant.Determinant_Name column changed as per the business requirement.
							For AS400 Ubm_Meter_Number should be populated as Determinant_Name for others actual Determinant_Name.
	RKV         2015-06-30  For AS400 Added column EC_Invoice_Sub_Bucket_Master_Id in the insert statement of both the tables.
******/
CREATE PROC [dbo].[Copy_determinant_charge_Resolve_month_by_Old_Cu_invoice_Id]
      ( 
       @Old_cu_invoice_id INT
      ,@New_cu_invoice_id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Ubm_Name VARCHAR(200)

      SELECT
            @Ubm_Name = ubm.UBM_NAME
      FROM
            dbo.CU_INVOICE cu
            INNER JOIN dbo.UBM
                  ON ubm.UBM_ID = cu.UBM_ID
      WHERE
            cu.CU_INVOICE_ID = @Old_cu_invoice_id

	--Inserting a record into CU_INVOICE_DETERMINANT based on old cu_invoice_id
      INSERT      INTO dbo.CU_INVOICE_DETERMINANT
                  ( 
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,UBM_METER_NUMBER
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,UNIT_OF_MEASURE_TYPE_ID
                  ,UBM_UNIT_OF_MEASURE_CODE
                  ,DETERMINANT_NAME
                  ,DETERMINANT_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_NO
                  ,CU_DETERMINANT_CODE
                  ,EC_Invoice_Sub_Bucket_Master_Id )
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,UBM_METER_NUMBER
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,UNIT_OF_MEASURE_TYPE_ID
                       ,UBM_UNIT_OF_MEASURE_CODE
                       ,CASE WHEN @Ubm_Name = 'AS400' THEN UBM_METER_NUMBER
                             ELSE DETERMINANT_NAME
                        END DETERMINANT_NAME
                       ,'0'
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_NO
                       ,CU_DETERMINANT_CODE
                       ,EC_Invoice_Sub_Bucket_Master_Id
                  FROM
                        dbo.CU_INVOICE_DETERMINANT
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id

	--Inserting a record into CU_INVOICE_DETERMINANT_ACCOUNT based on old cu_invoice_id
      INSERT      INTO CU_INVOICE_DETERMINANT_ACCOUNT
                  ( 
                   CU_INVOICE_DETERMINANT_ID
                  ,ACCOUNT_ID
                  ,Determinant_Expression
                  ,Determinant_Value )
                  SELECT
                        new_inv.CU_INVOICE_DETERMINANT_ID
                       ,old_det.ACCOUNT_ID
                       ,old_det.Determinant_Expression
                       ,0
                  FROM
                        dbo.CU_INVOICE_DETERMINANT_ACCOUNT old_det
                        JOIN dbo.CU_INVOICE_DETERMINANT old_inv
                              ON old_det.CU_INVOICE_DETERMINANT_ID = old_inv.CU_INVOICE_DETERMINANT_ID
                                 AND old_inv.CU_INVOICE_ID = @Old_cu_invoice_id
                        JOIN dbo.CU_INVOICE_DETERMINANT new_inv
                              ON new_inv.CU_DETERMINANT_CODE = old_inv.CU_DETERMINANT_CODE
                                 AND new_inv.CU_INVOICE_ID = @new_cu_invoice_id
						
      UPDATE
            cida
      SET   
            cida.ACCOUNT_ID = NULL
      FROM
            dbo.CU_INVOICE_DETERMINANT cid
            JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT cida
                  ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
            LEFT JOIN ( SELECT
                              Account_ID
                        FROM
                              dbo.CU_INVOICE_SERVICE_MONTH
                        WHERE
                              CU_INVOICE_ID = @New_cu_invoice_id
                        GROUP BY
                              Account_ID ) a
                  ON a.Account_ID = cida.ACCOUNT_ID
      WHERE
            cid.CU_INVOICE_ID = @New_cu_invoice_id
            AND a.Account_ID IS NULL

	----Inserting a record into CU_INVOICE_CHARGE based on old cu_invoice_id	
      INSERT      INTO CU_INVOICE_CHARGE
                  ( 
                   CU_INVOICE_ID
                  ,COMMODITY_TYPE_ID
                  ,Bucket_Master_Id
                  ,UBM_BUCKET_CODE
                  ,CHARGE_NAME
                  ,CHARGE_VALUE
                  ,UBM_INVOICE_DETAILS_ID
                  ,CU_DETERMINANT_CODE
                  ,EC_Invoice_Sub_Bucket_Master_Id )
                  SELECT
                        @New_cu_invoice_id
                       ,COMMODITY_TYPE_ID
                       ,Bucket_Master_Id
                       ,UBM_BUCKET_CODE
                       ,CHARGE_NAME
                       ,'0'
                       ,UBM_INVOICE_DETAILS_ID
                       ,CU_DETERMINANT_CODE
                       ,EC_Invoice_Sub_Bucket_Master_Id
                  FROM
                        dbo.CU_INVOICE_CHARGE
                  WHERE
                        CU_INVOICE_ID = @Old_cu_invoice_id

	
	----Inserting a record into CU_INVOICE_CHARGE_ACCOUNT based on old cu_invoice_id
      INSERT      INTO CU_INVOICE_CHARGE_ACCOUNT
                  ( 
                   ACCOUNT_ID
                  ,Charge_Expression
                  ,Charge_Value
                  ,CU_INVOICE_CHARGE_ID )
                  SELECT
                        old_chg.ACCOUNT_ID
                       ,old_chg.Charge_Expression
                       ,0
                       ,new_inv.CU_INVOICE_CHARGE_ID
                  FROM
                        dbo.CU_INVOICE_CHARGE_ACCOUNT old_chg
                        JOIN dbo.CU_INVOICE_CHARGE old_inv
                              ON old_chg.CU_INVOICE_CHARGE_ID = old_inv.CU_INVOICE_CHARGE_ID
                                 AND old_inv.CU_INVOICE_ID = @Old_cu_invoice_id
                        JOIN dbo.CU_INVOICE_CHARGE new_inv
                              ON new_inv.CU_DETERMINANT_CODE = old_inv.CU_DETERMINANT_CODE
                                 AND new_inv.CU_INVOICE_ID = @new_cu_invoice_id

      UPDATE
            cica
      SET   
            cica.ACCOUNT_ID = NULL
      FROM
            dbo.CU_INVOICE_CHARGE cic
            JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                  ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
            LEFT JOIN ( SELECT
                              Account_ID
                        FROM
                              dbo.CU_INVOICE_SERVICE_MONTH
                        WHERE
                              CU_INVOICE_ID = @New_cu_invoice_id
                        GROUP BY
                              Account_ID ) a
                  ON a.Account_ID = cica.ACCOUNT_ID
      WHERE
            cic.CU_INVOICE_ID = @New_cu_invoice_id
            AND a.Account_ID IS NULL
END

;
GO

GRANT EXECUTE ON  [dbo].[Copy_determinant_charge_Resolve_month_by_Old_Cu_invoice_Id] TO [CBMSApplication]
GO
