SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    dbo.Contract_Meter_Volume_Dtl_Ins 
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
	Exec dbo.Contract_Meter_Volume_Dtl_Ins NULL,1005,290,'2020-01-01','2020-02-01','2818,2815',NULL
  
   
AUTHOR INITIALS:       
	Initials    Name  
-----------------------------------------------------------------------------         
	RR			Raghu Reddy
      
MODIFICATIONS       
	Initials    Date		Modification        
-----------------------------------------------------------------------------         
	RR			2020-04-01	GRM - Contract volumes enhancement - Created
******/
CREATE PROC [dbo].[Contract_Meter_Volume_Dtl_Ins]
    (
        @Meter_Id INT
        , @Contract_Id INT
        , @Month_Identifier DATETIME
        , @Volume DECIMAL(32, 16)
        , @Bucket_Master_Id INT
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @CONTRACT_METER_VOLUME_ID INT;

        SELECT
            @CONTRACT_METER_VOLUME_ID = cmv.CONTRACT_METER_VOLUME_ID
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
        WHERE
            cmv.METER_ID = @Meter_Id
            AND cmv.CONTRACT_ID = @Contract_Id
            AND cmv.MONTH_IDENTIFIER = @Month_Identifier;

        INSERT INTO dbo.Contract_Meter_Volume_Dtl
             (
                 CONTRACT_METER_VOLUME_ID
                 , Bucket_Master_Id
                 , Volume
                 , Is_Edited
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        VALUES
            (@CONTRACT_METER_VOLUME_ID
             , @Bucket_Master_Id
             , @Volume
             , ISNULL(@Is_Edited, 0)
             , NULL
             , GETDATE()
             , NULL
             , GETDATE());


    END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Dtl_Ins] TO [CBMSApplication]
GO
