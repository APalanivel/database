
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.BUDGET_UPDATE_BUDGET_ACCOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@budget_account_id		INT
    @saUserId				INT
    @saCompletedDate		DATETIME
    @raUserId				INT
    @raCompletedDate		DATETIME
    @raReviewedUserId		INT
    @raReviewedDate			DATETIME
    @cannotPerformUserId	INT
    @cannotPerformDate		DATETIME
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	--SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 8552
	--SELECT * FROM dbo.Code WHERE Codeset_Id = 164
	--BEGIN TRAN
	--	SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
	--		ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
	--	UPDATE dbo.BUDGET SET Analyst_Queue_Display_Cd = 100579 WHERE BUDGET_ID = 8552
	--	EXEC BUDGET_UPDATE_BUDGET_ACCOUNT_P 8552
	--	SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
	--		ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
	--ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-05-26	Added header
							SE2017-26 - Modofied script to add/delete budget accounts to queue based cannot perform action
	
******/

CREATE  PROCEDURE [dbo].[BUDGET_CEM_UPDATE_BUDGET_ACCOUNT_P]
      ( 
       @budget_account_id INT
      ,@cannotPerformUserId INT
      ,@cannotPerformDate DATETIME )
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @OLD_CANNOT_PERFORMED_BY INT
           ,@Budget_Id INT
           
      SELECT
            @OLD_CANNOT_PERFORMED_BY = CANNOT_PERFORMED_BY
           ,@Budget_Id = BUDGET_ID
      FROM
            dbo.BUDGET_ACCOUNT
      WHERE
            BUDGET_ACCOUNT_ID = @budget_account_id
            
            
      UPDATE
            budget_account
      SET   
            cannot_performed_date = @cannotPerformDate
           ,cannot_performed_by = @cannotPerformUserId
           ,has_details_saved = 1
      WHERE
            budget_account_id = @budget_account_id
            
--->Budget - Cannot Perform Checked - Budget account no longer appears in any queue
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON baq.Budget_Account_Id = ba.BUDGET_ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                  ON ba.ACCOUNT_ID = cha.Account_Id
      WHERE
            ba.BUDGET_ACCOUNT_ID = @budget_account_id
            AND @cannotPerformUserId IS NOT NULL
            AND @cannotPerformDate IS NOT NULL

 --->Budget - Cannot Perform Unchecked - Budget accounts will goes to respective queue(s)
                      
      IF ( @OLD_CANNOT_PERFORMED_BY IS NOT NULL
           AND @cannotPerformUserId IS NULL
           AND @cannotPerformDate IS NULL ) 
            BEGIN 
                  EXEC dbo.Budget_Account_Queue_Ins_Upd 
                        @Budget_Id = @Budget_Id
                       ,@Budget_Account_Id = @Budget_Account_Id
            
            END 
            
END 






;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_CEM_UPDATE_BUDGET_ACCOUNT_P] TO [CBMSApplication]
GO
