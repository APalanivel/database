SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create  procedure
dbo.cbmsAccount_GetID_lec
@accountnumber varchar(200)
as begin
--declare @accountnumber varchar(200)
--set @accountnumber = '%1665%'

select account_id
	,account_number
from account
where account_number like @accountnumber 
end


GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_GetID_lec] TO [CBMSApplication]
GO
