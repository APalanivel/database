SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.GET_FORECAST_VOLUMES_FOR_CLIENT_AGGREGATION_P
@userId varchar(10),
@sessionId varchar(20),
@fromDate datetime,
@toDate datetime,
@fromYear int,
@toYear int,
@cemId int,
@cemGroupId int,
@unitId int,
@filter int
AS
	set nocount on
 if(@filter = 1) -- // Client Aggregation
begin

select CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
	CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) ,  
	 volume.client_id , cli.client_name  
from
	
	RM_FORECAST_VOLUME_DETAILS volumedetails,
	RM_FORECAST_VOLUME volume , CLIENT cli,
	CONSUMPTION_UNIT_CONVERSION consumption,
	RM_ONBOARD_HEDGE_SETUP onboard 


where   volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID 		
 	AND volume.FORECAST_AS_OF_DATE =(select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME  
					 where  CLIENT_ID= volume.client_id )

   	AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101)
	AND  CONVERT(Varchar(12),  @toDate, 101)
	and volume.client_id in (select distinct client_id from RM_ONBOARD_CLIENT)  
	and cli.client_id = volume.client_id
  
	and onboard.SITE_ID = volumedetails.SITE_ID
	AND consumption.BASE_UNIT_ID = onboard.VOLUME_UNITS_TYPE_ID
	AND consumption.CONVERTED_UNIT_ID=@unitId
GROUP BY     volumedetails.MONTH_IDENTIFIER  , volume.client_id , cli.client_name 
order by cli.client_name
end

else if(@filter = 2) --//CEM Client Aggregation
begin
	if(@cemId > 0)
  	begin	
	select CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
		CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) ,  
		 volume.client_id , cli.client_name  
	from
		
		RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume , CLIENT cli, CLIENT_CEM_MAP map,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard 
	
	
	where   volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID 		
	 	AND volume.FORECAST_AS_OF_DATE =(select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME  
						 where CLIENT_ID= volume.client_id )
	
	   	AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101)
		AND  CONVERT(Varchar(12),  @toDate, 101)
		and volume.client_id in (select distinct client_id from RM_ONBOARD_CLIENT)  
		and cli.client_id = volume.client_id  
		and map.client_id = volume.client_id  
		and  map.user_info_id = @cemId
		and onboard.SITE_ID = volumedetails.SITE_ID
		AND consumption.BASE_UNIT_ID = onboard.VOLUME_UNITS_TYPE_ID
		AND consumption.CONVERTED_UNIT_ID=@unitId
	
	
	GROUP BY     volumedetails.MONTH_IDENTIFIER  , volume.client_id , cli.client_name 
	order by cli.client_name
	end
	else if(@cemGroupId > 0)
	begin
		select  CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
			CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) ,  
			volume.client_id , cli.client_name ,
			map.user_info_id,
			(userInfo.first_name+' '+userInfo.last_name) as CEM
		from
			
			RM_FORECAST_VOLUME_DETAILS volumedetails,
			RM_FORECAST_VOLUME volume , CLIENT cli, CLIENT_CEM_MAP map,
			CONSUMPTION_UNIT_CONVERSION consumption,
			RM_ONBOARD_HEDGE_SETUP onboard ,USER_INFO userInfo
		
		
		where   volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID 		
		 	AND volume.FORECAST_AS_OF_DATE =(select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME  
							 where CLIENT_ID= volume.client_id )
		
		   	AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101)
			AND  CONVERT(Varchar(12),  @toDate, 101)
			and volume.client_id in (select distinct client_id from RM_ONBOARD_CLIENT)  
			and cli.client_id = volume.client_id  
			and map.client_id = volume.client_id  
			and map.user_info_id in (select cem_user_id from RM_CEM_TEAM where cem_team_type_id =@cemGroupId)
			and userInfo.user_info_id = map.user_info_id

			and onboard.SITE_ID = volumedetails.SITE_ID
			AND consumption.BASE_UNIT_ID = onboard.VOLUME_UNITS_TYPE_ID
			AND consumption.CONVERTED_UNIT_ID=@unitId
			
		GROUP BY     volumedetails.MONTH_IDENTIFIER  , volume.client_id , cli.client_name ,
			     map.user_info_id, userInfo.first_name, userInfo.last_name
	
		ORDER BY   userInfo.first_name, cli.client_name, volumedetails.MONTH_IDENTIFIER

/*
	select CAST( SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) as decimal(15,3)) TOTAL_VOLUME,
		CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) ,  
		 volume.client_id , cli.client_name  
	from
		
		RM_FORECAST_VOLUME_DETAILS volumedetails,
		RM_FORECAST_VOLUME volume , CLIENT cli, --CLIENT_CEM_MAP map,
		CONSUMPTION_UNIT_CONVERSION consumption,
		RM_ONBOARD_HEDGE_SETUP onboard 
	
	
	where   volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID 		
	 	AND volume.FORECAST_AS_OF_DATE =(select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME  
						 where CLIENT_ID= volume.client_id )
	
	   	AND volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101)
		AND  CONVERT(Varchar(12),  @toDate, 101)
		and volume.client_id in (select distinct client_id from RM_ONBOARD_CLIENT)  
		and cli.client_id = volume.client_id  
		--and map.client_id = volume.client_id  
		--and map.user_info_id in (select cem_user_id from RM_CEM_TEAM where cem_team_type_id =@cemGroupId)
		and volume.client_id in (select distinct client_id from CLIENT_CEM_MAP where  
				 	 user_info_id  in (select cem_user_id from RM_CEM_TEAM where cem_team_type_id =590)  
					)
		and onboard.SITE_ID = volumedetails.SITE_ID
		AND consumption.BASE_UNIT_ID = onboard.VOLUME_UNITS_TYPE_ID
		AND consumption.CONVERTED_UNIT_ID=@unitId
		
	GROUP BY     volumedetails.MONTH_IDENTIFIER  , volume.client_id , cli.client_name 
*/
	end
end
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUMES_FOR_CLIENT_AGGREGATION_P] TO [CBMSApplication]
GO
