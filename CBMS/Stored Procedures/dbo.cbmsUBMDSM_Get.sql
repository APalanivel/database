SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE           PROCEDURE [dbo].[cbmsUBMDSM_Get]
	( @user_info_id int
	)
AS
BEGIN  

         select ubm_id
		,ubm_username
		,ubm_password
		,dsm_name = 'Elutions'
		,dsm_username
		,dsm_password
	     from sso_other_logins   
		    
 	    where user_info_id = @user_info_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUBMDSM_Get] TO [CBMSApplication]
GO
