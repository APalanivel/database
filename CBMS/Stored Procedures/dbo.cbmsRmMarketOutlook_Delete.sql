SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[cbmsRmMarketOutlook_Delete]

( @AccountId int
, @OutlookId int
)

AS
BEGIN
	set nocount on

	delete from rm_market_outlook where rm_market_outlook_id = @OutlookId
END
GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlook_Delete] TO [CBMSApplication]
GO
