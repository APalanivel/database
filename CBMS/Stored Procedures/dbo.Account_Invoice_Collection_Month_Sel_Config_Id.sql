SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                              
 NAME: dbo.Account_Invoice_Collection_Month_Sel_Config_Id                  
                              
 DESCRIPTION:                              
   To get the details of invoice collection Service month                         
                              
 INPUT PARAMETERS:                
                           
 Name         DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Invoice_Collection_Account_Config_id INT      
@Seq_no            SMALLINT      
                                    
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
          
      
  EXEC Account_Invoice_Collection_Month_Sel_Config_Id 611,NULL ,0
            
                            
 AUTHOR INITIALS:              
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------                            
 RKV    Ravi Kumar Vegesna                
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------            
 RKV                  2017-01-27       Created   for Invoice tracking  
 RKV                  2020-04-21	   added new column Invoice_Frequency_Pattern_Cd to the result set.	                 
                             
******/

CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Sel_Config_Id]
     (
         @Invoice_Collection_Account_Config_id INT
         , @Seq_no SMALLINT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;



        SELECT
            aicm.Account_Invoice_Collection_Month_Id
            , aicm.Service_Month
            , aicm.Invoice_Collection_Global_Config_Value_Id
            , aicm.Account_Invoice_Collection_Frequency_Id
            , aicm.Seq_No
            , aicm.Invoice_Frequency_Cd
            , aicm.Invoice_Frequency_Pattern_Cd
        FROM
            Account_Invoice_Collection_Month aicm
        WHERE
            aicm.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_id
            AND (   (   @Seq_no IS NULL
                        AND Seq_No <> -1)
                    OR  Seq_No = @Seq_no)
        GROUP BY
            aicm.Account_Invoice_Collection_Month_Id
            , aicm.Service_Month
            , aicm.Invoice_Collection_Global_Config_Value_Id
            , aicm.Account_Invoice_Collection_Frequency_Id
            , aicm.Seq_No
            , aicm.Invoice_Frequency_Cd
            , aicm.Invoice_Frequency_Pattern_Cd;

    END;

    ;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Sel_Config_Id] TO [CBMSApplication]
GO
