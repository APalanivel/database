SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [Workflow].[Insert_Update_User_Preference_Workflow_Queue_Output_Columns]       
   @Userid int,      
   @ModuleId int,      
   @Output_Column_ID varchar(max)      
  -- Add the parameters for the stored procedure here       
        
AS       
  BEGIN -- SET NOCOUNT ON added to prevent extra result sets from       
      -- interfering with SELECT statements.       
      DECLARE @Proc_name     VARCHAR(100) =       
              'Insert_Update_User_Preference_Workflow_Queue_Output_Columns',       
              @Input_Params  VARCHAR (1000),       
              @Error_Line    INT,       
              @Error_Message VARCHAR(3000),      
     @display_seq_max int        
      
      SELECT @Input_Params = Cast ( @userID AS VARCHAR)  + Cast ( @ModuleID AS VARCHAR)  + Isnull (@Output_Column_ID,'Output Column id is empty')        
      SET nocount ON;       
          
           
      
      BEGIN TRY        
      
   update wqc       
   set is_ACTIVE  = 0,      
     Updated_User_Id=@userid,      
    Last_change_Ts =getdate()      
    from Workflow.Workflow_Queue_User_Preference_Column  wqc      
    join workflow.Workflow_Queue_Output_Column wqo      
    on wqc.Workflow_Queue_Output_Column_Id = wqo.Workflow_Queue_Output_Column_Id       
    where User_Info_Id =@userID      
    and wqo.Workflow_Queue_Id  = @ModuleID   
 AND wqo.Is_Visible_To_User <> 0     
      
         
             
   SELECT Segments as Output_Column_Id       
   into #Output_Columns      
   FROM dbo.ufn_split(@Output_Column_ID,',')       
      
   /*       
   IF THE USER ALREADY HAVE THAT COLUMN SELECTED WHICH THE USER WANT TO UNSELECT, THEN UPDATE IS_ACTIVE = 0      
   */      
      
        
      
   set @display_seq_max = isnull( (select max(wqc.display_seq)  from Workflow.Workflow_Queue_User_Preference_Column  wqc      
          join workflow.Workflow_Queue_Output_Column wqo       
          on wqc.Workflow_Queue_Output_Column_Id = wqo.Workflow_Queue_Output_Column_Id      
        where wqc.User_Info_Id = @userID      
        and wqo.Workflow_Queue_Id = @ModuleID   
  AND wqo.Is_Visible_To_User <> 0   ),0)      
      
      
     MERGE INTO Workflow.Workflow_Queue_User_Preference_Column as tgt      
      USING       
            ( select       
     wqo.Workflow_Queue_Output_Column_Id,      
    @display_seq_max as display_seq_max,      
     ROW_NUMBER() OVER( ORDER BY wqo.Workflow_Queue_Output_Column_Id ASC) as rn      
      from workflow.Workflow_Queue_Output_Column wqo      
    join #Output_Columns TempOutput      
    on tempoutput.Output_Column_Id = wqo.Workflow_Queue_Output_Column_Id        
    where wqo.Workflow_Queue_Id  = @ModuleID  
 AND wqo.Is_Visible_To_User <> 0   ) src      
      ON  src.Workflow_Queue_Output_Column_Id = tgt.Workflow_Queue_Output_Column_Id      
   and tgt.User_Info_Id =@userID      
         
                   
       WHEN NOT MATCHED THEN      
      INSERT      
                  (       
     User_Info_Id      
     ,Workflow_Queue_Output_Column_Id      
     ,Display_Seq      
     ,IS_ACTIVE      
     ,Created_User_Id      
     ,Created_Ts      
     ,Updated_User_Id      
     ,Last_Change_Ts )      
               VALUES      
                  (       
                   @userID      
                  ,src.Workflow_Queue_Output_Column_Id       
      , display_seq_max + rn      
      ,1      
                  ,@userID      
      ,getdate()      
      ,@userid      
      ,getdate() )       
        
  WHEN MATCHED THEN       
      
  UPDATE SET IS_ACTIVE = 1,      
    Updated_User_Id=@userid,      
    Last_change_Ts =getdate()  ;      
         
      
   --DELETE THE ENTRIES WHICH WAS DEFAULT EARLIER AND NOW USER DID NOT WANT TOR      
         
      END try       
      
      BEGIN catch       
      
   Print 'error'      
      
      
   -- Entry made to the logging SP to capture the errors.      
          SELECT @ERROR_LINE = Error_line(),       
                 @ERROR_MESSAGE = Error_message()       
      
          INSERT INTO storedproc_error_log       
                      (storedproc_name,       
                       error_line,       
                       error_message,       
                       input_params)       
          VALUES      ( @PROC_NAME,       
                        @ERROR_LINE,       
                        @ERROR_MESSAGE,       
                        @INPUT_PARAMS )           END catch       
  END       
    
GO
GRANT EXECUTE ON  [Workflow].[Insert_Update_User_Preference_Workflow_Queue_Output_Columns] TO [CBMSApplication]
GO
