SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*   
NAME: dbo.Cu_Exception_Is_Closed_Upd_By_Invoice_Account_Commodity                  
                              
 DESCRIPTION:        
 To update the IS_Closed Flag based on input.      
                              
 INPUT PARAMETERS:                
                           
 Name       DataType         Default       Description              
-------------------------------------------------------------------------------           
 @Cu_Invoice_Id                INT      
 @Account_Id       INT      
 @Commodity_Id       INT      
 @User_Info_Id       INT      
                     
                              
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
-------------------------------------------------------------------------------           
                              
 USAGE EXAMPLES:                                  
-------------------------------------------------------------------------------   
BEGIN TRANSACTION                
                 
SELECT    
    *    
FROM    
    dbo.Cu_Invoice_Account_Commodity_Comment a    
    INNER JOIN dbo.Comment b    
        ON a.Comment_Id = b.Comment_ID    
WHERE    
    a.Account_Id = 1               
                   
        EXEC dbo.[Cu_Invoice_Account_Commodity_External_Comment_Ins]                 
            @Cu_Invoice_Id = 1                
           ,@Account_Id = 1                
           ,@Commodity_Id = 291                
           ,@Comment_Type = 'Internal Recalc Comment'                
           ,@User_Info_Id = 16                
           ,@Comment_Text = NULL              
                 
                  
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID                
   WHERE a.Account_Id = 1                
                   
 ROLLBACK TRANSACTION     
   
               
                 
                             
 AUTHOR INITIALS:              
             
 Initials              Name              
-------------------------------------------------------------------------------           
 NR                    Narayana Reddy                
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
-------------------------------------------------------------------------------  
KAB     2019-06-24   SE2017-687     
      
*/


CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_External_Comment_Ins]
(
    @Cu_Invoice_Id INT,
    @Account_Id INT,
    @Commodity_Id INT,
    @Comment_Type VARCHAR(25) = 'External Recalc Comment',
    @User_Info_Id INT,
    @Comment_Text VARCHAR(MAX) = 'Invoice is correct/differential is acceptable'
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Comment_Id INT,
            @Comment_Type_CD INT,
            @Cu_Invoice_Account_Commodity_Comment_Id INT,
            @Recalc_Status_Type_Id INT;


    SET @Recalc_Status_Type_Id =
    (
        SELECT e.ENTITY_ID
        FROM dbo.ENTITY e
        WHERE e.ENTITY_NAME = 'Invoice is correct/differential is acceptable'
              AND e.ENTITY_TYPE = 102657
    );

    SELECT @Comment_Type_CD = cd.Code_Id
    FROM dbo.Code cd
        JOIN dbo.Codeset cs
            ON cd.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'CommentType'
          AND cd.Code_Value = @Comment_Type;

    EXEC dbo.Comment_Ins @Comment_Type_CD = @Comment_Type_CD,
                         @Comment_User_Info_Id = @User_Info_Id,
                         @Comment_Dt = NULL,
                         @Comment_Text = @Comment_Text,
                         @Comment_Id = @Comment_Id OUT;



    INSERT INTO dbo.Cu_Invoice_Account_Commodity_Comment
    (
        Cu_Invoice_Id,
        Account_Id,
        Commodity_Id,
        Comment_Id
    )
    VALUES
    (@Cu_Invoice_Id, @Account_Id, @Commodity_Id, @Comment_Id);


    SET @Cu_Invoice_Account_Commodity_Comment_Id = SCOPE_IDENTITY();


    INSERT INTO dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map
    (
        Cu_Invoice_Account_Commodity_Comment_Id,
        Recalc_Status_Type_Id
    )
    SELECT @Cu_Invoice_Account_Commodity_Comment_Id AS Cu_Invoice_Account_Commodity_Comment_Id,
           @Recalc_Status_Type_Id AS Recalc_Status_Type_Id
    WHERE @Recalc_Status_Type_Id IS NOT NULL;

END;
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_External_Comment_Ins] TO [CBMSApplication]
GO
