
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Send_Rfp_Email_Dtls_Sel_By_Email_Log_Id]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Id	INT
    @Contact_Id		INT
    @Sr_Rfp_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.SR_RFP_EMAIL_ATTACHMENT
            	
	EXEC dbo.Sr_Rfp_Send_Rfp_Email_Dtls_Sel_By_Email_Log_Id 1
	EXEC dbo.Sr_Rfp_Send_Rfp_Email_Dtls_Sel_By_Email_Log_Id 2
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-21	Global Sourcing - Phase3 - GCS-635 Created
	RR			2017-04-24	MAINT-5231 Replaced comma(,) with pipe(|) for string concatenation in CROSS APPLY
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Send_Rfp_Email_Dtls_Sel_By_Email_Log_Id]
      ( 
       @Sr_Rfp_Email_Log_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      WITH  Cte_Mail
              AS ( SELECT
                        mail.SR_RFP_EMAIL_LOG_ID
                       ,mail.FROM_EMAIL_ADDRESS
                       ,mail.TO_EMAIL_ADDRESS
                       ,mail.SUBJECT
                       ,mail.CONTENT
                       ,mail.GENERATION_DATE
                       ,ci.CBMS_DOC_ID
                       ,ci.CBMS_IMAGE_ID
                   FROM
                        dbo.SR_RFP_EMAIL_LOG mail
                        LEFT JOIN dbo.SR_RFP_EMAIL_ATTACHMENT atach
                              ON mail.SR_RFP_EMAIL_LOG_ID = atach.SR_RFP_EMAIL_LOG_ID
                        LEFT JOIN dbo.cbms_image ci
                              ON atach.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
                   WHERE
                        mail.SR_RFP_EMAIL_LOG_ID = @Sr_Rfp_Email_Log_Id)
            SELECT
                  cm.SR_RFP_EMAIL_LOG_ID
                 ,cm.FROM_EMAIL_ADDRESS
                 ,cm.TO_EMAIL_ADDRESS
                 ,cm.SUBJECT
                 ,cm.CONTENT
                 ,cm.GENERATION_DATE
                 ,REPLACE(LEFT(Doc.Docs, LEN(Doc.Docs) - 1), '&amp;', '&') AS Attachment
            FROM
                  Cte_Mail cm
                  CROSS APPLY ( SELECT
                                    doc.CBMS_DOC_ID + '^' + CAST(doc.CBMS_IMAGE_ID AS VARCHAR(50)) + '| '
                                FROM
                                    Cte_Mail doc
                                WHERE
                                    doc.CBMS_IMAGE_ID IS NOT NULL
                                    AND doc.SR_RFP_EMAIL_LOG_ID = cm.SR_RFP_EMAIL_LOG_ID
                                GROUP BY
                                    doc.CBMS_DOC_ID + '^' + CAST(doc.CBMS_IMAGE_ID AS VARCHAR(50))
                  FOR
                                XML PATH('') ) Doc ( Docs )
      
      
END;

;
GO

GRANT EXECUTE ON  [dbo].[Sr_Rfp_Send_Rfp_Email_Dtls_Sel_By_Email_Log_Id] TO [CBMSApplication]
GO
