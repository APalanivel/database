SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME:  dbo.Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id                       
                            
 DESCRIPTION:        
				TO get the roles information for clint app acess role table .                            
                            
 INPUT PARAMETERS:        
                           
 Name                       DataType        Default       Description        
--------------------------------------------------------------------------------- 
 @Cu_Invoice_Id             VARCHAR(MAX)          
                            
                         
 OUTPUT PARAMETERS:        
                                 
 Name                       DataType        Default       Description        
--------------------------------------------------------------------------------- 
                            
 USAGE EXAMPLES:                                
--------------------------------------------------------------------------------- 
       
 Exec dbo.Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id
    @Cu_Invoice_Id = '74409500'

Exec dbo.Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id
    @Cu_Invoice_Id = '7912776'  --This   one have 4invoice same acount s and same service month    

	
Exec dbo.Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id
    @Cu_Invoice_Id = '7912776,74409500' 
      
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
--------------------------------------------------------------------------------- 
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
--------------------------------------------------------------------------------- 
 NR                     2014-01-01      Created for RA Admin user management                          
                           
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id VARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Cu_Invoice_Account_Service_Month_Dtl TABLE
              (
                  Account_Id INT
                  , Service_Month DATE
              );


        INSERT INTO @Cu_Invoice_Account_Service_Month_Dtl
             (
                 Account_Id
                 , Service_Month
             )
        SELECT
            cism.Account_ID
            , cism.SERVICE_MONTH
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.SERVICE_MONTH IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Cu_Invoice_Id, ',') ufn
                           WHERE
                                ufn.Segments = cism.CU_INVOICE_ID)
        GROUP BY
            cism.Account_ID
            , cism.SERVICE_MONTH;




        SELECT
            ch.Site_Id
            , ch.Site_name
            , cha.Account_Id
            , cha.Account_Number
            , cism.SERVICE_MONTH
            , MAX(CASE WHEN ci.IS_REPORTED = 1 THEN 1
                      ELSE 0
                  END) AS Is_Reported
        FROM
            dbo.CU_INVOICE ci
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            @Cu_Invoice_Account_Service_Month_Dtl ciasmd
                       WHERE
                            ciasmd.Account_Id = cism.Account_ID
                            AND ciasmd.Service_Month = cism.SERVICE_MONTH)
        GROUP BY
            ch.Site_Id
            , ch.Site_name
            , cha.Account_Id
            , cha.Account_Number
            , cism.SERVICE_MONTH;
    END;



GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Is_Reported_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
