SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--EXEC dbo.BUDGET_GET_TRANS_DEFAULT_FOR_ALL_ACCOUNTS_P 1139, '04/01/2008', '08/01/2008'
-- Its to get default transportation for all transport accounts in a budget
CREATE           PROCEDURE dbo.BUDGET_GET_TRANS_DEFAULT_FOR_ALL_ACCOUNTS_P
	@budget_id int,
	@from_date datetime,
	@to_date datetime
	AS
	begin
		set nocount on
--added for 16542 bug
	declare @temp table ([budget_account_id] [int],[volume]decimal(32,16),[index_detail_value]decimal(32,16),[adder]decimal(32,16),[fuel]decimal(32,16),[multiplier][int],[tax]decimal(32,16),[is_nymex][int],[budget_contract_defaults_id][int],[contract_id][int],[type_of_contract][int] )
	declare @temp1 table ([budget_account_id] [int],[volume]decimal(32,16),[index_detail_value]decimal(32,16),[adder]decimal(32,16),[fuel]decimal(32,16),[multiplier][int],[tax]decimal(32,16),[is_nymex][int],[budget_contract_defaults_id][int],[contract_id][int],[type_of_contract][int] )
	

	insert into @temp(budget_account_id,volume,index_detail_value,adder,fuel,multiplier,tax,is_nymex,budget_contract_defaults_id,contract_id,type_of_contract) 


		select	budget_account.budget_account_id,
			isnull(contract_defaults.volume,100) as volume,
			avg(isnull(index_detail.index_detail_value, 0)) as index_detail_value,
			case when conv.conversion_factor > 0
			     then ((isnull(contract_defaults.adder,0) * curr_conv.conversion_factor)/conv.conversion_factor)
			     else 0
			end adder,
			contract_defaults.fuel,
			0 as multiplier,
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast as is_nymex,
			contract_defaults.budget_contract_defaults_id,
			budget_contract_vw.contract_id,
			1 as type_of_contract -- 1 represents current contract
	
		

		from	budget join budget_account on budget_account.budget_id = budget.budget_id 
			and budget.budget_id = @budget_id 

			
 
			join budget_contract_vw on budget_contract_vw.account_id = budget_account.account_id 
			and budget_contract_vw.commodity_type_id = budget.commodity_type_id
			and budget_contract_vw.contract_start_date <= ( dateadd(month, 1, @to_date) - 1 )
			and budget_contract_vw.contract_end_date > getdate()
			and budget_contract_vw.contract_end_date >= @from_date--added to get only current budget contracts
			join budget_contract_budget contract_budget on contract_budget.contract_id = budget_contract_vw.contract_id
			join budget_contract_defaults contract_defaults
			on contract_defaults.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			join consumption_unit_conversion conv on conv.base_unit_id = contract_defaults.volume_unit_type_id
			and conv.converted_unit_id = 25 --// MMBtu
			join currency_unit_conversion curr_conv on curr_conv.base_unit_id = contract_defaults.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = contract_defaults.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3
			join contract con on con.contract_id = contract_budget.contract_id
			left join clearport_index_months index_months on index_months.clearport_index_id = contract_defaults.market_id
			and index_months.clearport_index_month between dateadd(month, 1, convert(datetime,str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date))))and dateadd(month, 12, convert(datetime, str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date)))) 
			left join clearport_index_detail index_detail on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id)


			


		group by budget_account.budget_account_id, 
			 budget_contract_vw.contract_id, 
			contract_defaults.volume,			
			conv.conversion_factor,
			contract_defaults.adder,
			curr_conv.conversion_factor,
			contract_defaults.fuel,			
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast,
			contract_defaults.budget_contract_defaults_id

	
	insert into @temp1(budget_account_id,volume,index_detail_value,adder,fuel,multiplier,tax,is_nymex,budget_contract_defaults_id,contract_id,type_of_contract) 

	select	budget_account.budget_account_id,
			isnull(contract_defaults.volume,100) as volume,
			avg(isnull(index_detail.index_detail_value, 0)) as index_detail_value,
			case when conv.conversion_factor > 0
			     then ((isnull(contract_defaults.adder,0) * curr_conv.conversion_factor)/conv.conversion_factor)
			     else 0
			end adder,
			contract_defaults.fuel,
			0 as multiplier,
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast as is_nymex,
			contract_defaults.budget_contract_defaults_id,
			budget_contract_vw.contract_id,
			0 as type_of_contract -- 0 represents most recent contract type
	
		

		from	budget join budget_account on budget_account.budget_id = budget.budget_id 
			and budget.budget_id = @budget_id 

			
 
			join budget_contract_vw on budget_contract_vw.account_id = budget_account.account_id 
			and budget_contract_vw.commodity_type_id = budget.commodity_type_id
			and budget_contract_vw.contract_start_date <= ( dateadd(month, 1, @to_date) - 1 )
			and budget_contract_vw.contract_end_date > getdate()
			and budget_contract_vw.contract_end_date = (	select 	max(vw.contract_end_date)
						                    		from 	budget_contract_vw vw
										where 	vw.account_id = budget_account.account_id 
											and vw.commodity_type_id = budget.commodity_type_id
	 								    )--added to get only most recent contract defaults
			
			join budget_contract_budget contract_budget on contract_budget.contract_id = budget_contract_vw.contract_id
			join budget_contract_defaults contract_defaults
			on contract_defaults.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			join consumption_unit_conversion conv on conv.base_unit_id = contract_defaults.volume_unit_type_id
			and conv.converted_unit_id = 25 --// MMBtu
			join currency_unit_conversion curr_conv on curr_conv.base_unit_id = contract_defaults.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = contract_defaults.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3
			join contract con on con.contract_id = contract_budget.contract_id
			left join clearport_index_months index_months on index_months.clearport_index_id = contract_defaults.market_id
			and index_months.clearport_index_month between dateadd(month, 1, convert(datetime,str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date))))and dateadd(month, 12, convert(datetime, str(datepart(mm, con.contract_end_date))+'/01/'+str(datepart(yyyy, con.contract_end_date)))) 
			left join clearport_index_detail index_detail on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id)



		group by budget_account.budget_account_id, 
			 budget_contract_vw.contract_id, 
			contract_defaults.volume,			
			conv.conversion_factor,
			contract_defaults.adder,
			curr_conv.conversion_factor,
			contract_defaults.fuel,			
			contract_defaults.tax,
			contract_defaults.is_nymex_forecast,
			contract_defaults.budget_contract_defaults_id


	select * from @temp
	union 
	select * from @temp1 where budget_account_id not in (select budget_account_id from @temp) 

	end
















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_TRANS_DEFAULT_FOR_ALL_ACCOUNTS_P] TO [CBMSApplication]
GO
