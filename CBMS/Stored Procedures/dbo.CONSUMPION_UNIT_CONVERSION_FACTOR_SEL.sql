SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.CONSUMPION_UNIT_CONVERSION_FACTOR_SEL

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Base_Unit_Id		INT         	
	@Converted_unit_Id  INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC CONSUMPION_UNIT_CONVERSION_FACTOR_SEL 25,1369

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    SG          Suganya Gopinathan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SG			12/15/2009	Created this Stored Procedure to get Consumption Factor
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.CONSUMPION_UNIT_CONVERSION_FACTOR_SEL    
	@Base_Unit_Id INT
	,@Converted_unit_Id INT
AS

BEGIN

	SET NOCOUNT ON

	SELECT
		Conversion_Factor
	FROM
		dbo.Consumption_Unit_Conversion
	WHERE
		base_unit_id = @Base_Unit_Id
		AND converted_unit_id =  @Converted_unit_Id

END
GO
GRANT EXECUTE ON  [dbo].[CONSUMPION_UNIT_CONVERSION_FACTOR_SEL] TO [CBMSApplication]
GO
