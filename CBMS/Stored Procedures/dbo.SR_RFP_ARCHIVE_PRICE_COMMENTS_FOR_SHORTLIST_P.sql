SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_ARCHIVE_PRICE_COMMENTS_FOR_SHORTLIST_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@accountTermId int,
@selectedProductId int,
@supplierContactVendorMapId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Run the Procedure
--EXEC DBO.SR_RFP_ARCHIVE_PRICE_COMMENTS_FOR_SHORTLIST_P
--@accountTermId = 22053,
--@selectedProductId =3666,
--@supplierContactVendorMapId = 34122

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.SR_RFP_ARCHIVE_PRICE_COMMENTS_FOR_SHORTLIST_P

@accountTermId int,
@selectedProductId int,
@supplierContactVendorMapId int


AS
	
SET NOCOUNT ON

declare @supplierPriceCommentsId int
declare @priceComments varchar(4000)
declare @supplierPriceCommentsArchiveId int


SELECT  @supplierPriceCommentsId =dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID,
    	@priceComments = dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS.PRICE_RESPONSE_COMMENTS
	FROM       
	dbo.SR_RFP_BID 
	INNER JOIN 
	dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS ON dbo.SR_RFP_BID.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
	INNER JOIN
	dbo.SR_RFP_TERM_PRODUCT_MAP ON dbo.SR_RFP_BID.SR_RFP_BID_ID = dbo.SR_RFP_TERM_PRODUCT_MAP.SR_RFP_BID_ID 
	INNER JOIN
	dbo.SR_RFP_ACCOUNT_TERM ON dbo.SR_RFP_TERM_PRODUCT_MAP.SR_RFP_ACCOUNT_TERM_ID = dbo.SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID 
	INNER JOIN
	dbo.SR_RFP_SOP_SHORTLIST ON dbo.SR_RFP_ACCOUNT_TERM.SR_ACCOUNT_GROUP_ID = dbo.SR_RFP_SOP_SHORTLIST.SR_ACCOUNT_GROUP_ID
	WHERE     (dbo.SR_RFP_TERM_PRODUCT_MAP.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactVendorMapId) AND 
	(dbo.SR_RFP_TERM_PRODUCT_MAP.SR_RFP_SELECTED_PRODUCTS_ID = @selectedProductId) AND 
	(dbo.SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = @accountTermId)


INSERT INTO SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE (
													ARCHIVED_ON_DATE,
													PRICE_RESPONSE_COMMENTS, 
													SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
													)
											VALUES	(
													getDate(),
													@priceComments, 
													@supplierPriceCommentsId 
													)
SELECT @supplierPriceCommentsArchiveId = SCOPE_IDENTITY()

UPDATE	dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS 
SET		PRICE_RESPONSE_COMMENTS = '' 
WHERE
		SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierPriceCommentsId

RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_ARCHIVE_PRICE_COMMENTS_FOR_SHORTLIST_P] TO [CBMSApplication]
GO
