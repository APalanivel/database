SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Client_Hier_Hedge_Config_Upd                      
                        
Description:                        
        To update hedge configuration  
                        
Input Parameters:                        
    Name        DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @RM_Client_Hier_Hedge_Config_Id  INT  
    @Is_Default       BIT    1  
    @RM_Forecast_UOM_Type_Id   INT  
    @Config_Start_Dt     DATE  
    @Config_End_Dt      DATE  
    @Hedge_Type_Id      INT  
    @Max_Hedge_Pct      DECIMAL(28, 12)  
    @RM_Risk_Tolerance_Category_Id  INT  
    @Risk_Manager_User_Info_Id   INT  
    @Contact_Info_Id     INT  
    @Include_In_Reports     BIT  
    @User_Info_Id      INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
----------------------------------------------------------------------------  
  
                      
Usage Examples:                            
----------------------------------------------------------------------------  
    
                       
 Author Initials:                        
    Initials    Name                        
----------------------------------------------------------------------------  
    PR          Pramod Reddy          
                         
 Modifications:                        
    Initials Date           Modification                        
----------------------------------------------------------------------------  
    PR   2018-01-08     Global Risk Management - Created                      
                       
******/
CREATE PROCEDURE [dbo].[RM_Client_Hier_Hedge_Config_Upd]
(
    @RM_Client_Hier_Hedge_Config_Id INT,
    @RM_Forecast_UOM_Type_Id INT,
    @Config_Start_Dt DATE,
    @Config_End_Dt DATE,
    @Hedge_Type_Id INT,
    @Max_Hedge_Pct DECIMAL(5, 2),
    @RM_Risk_Tolerance_Category_Id INT,
    @Risk_Manager_User_Info_Id INT,
    @Contact_Info_Id INT,
    @Include_In_Reports BIT,
    @User_Info_Id INT
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @RM_Default_Config_Start_Dt DATE,
            @RM_Default_Config_End_Dt DATE;
    --,@RM_Client_Hier_Onboard_Id INT  

    SELECT @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
    FROM dbo.App_Config
    WHERE App_Config_Cd = 'RM_Default_Config_Start_Dt'
          AND App_Id = -1;

    SELECT @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
    FROM dbo.App_Config
    WHERE App_Config_Cd = 'RM_Default_Config_End_Dt'
          AND App_Id = -1;

    SELECT @Config_Start_Dt = DATEADD(dd, -DATEPART(dd, @Config_Start_Dt) + 1, @Config_Start_Dt);
    SELECT @Config_End_Dt = DATEADD(dd, -DATEPART(dd, @Config_End_Dt) + 1, @Config_End_Dt);

    UPDATE Trade.RM_Client_Hier_Hedge_Config
    SET --RM_Forecast_UOM_Type_Id = @RM_Forecast_UOM_Type_Id  
        --,  
        Config_Start_Dt = ISNULL(@Config_Start_Dt, @RM_Default_Config_Start_Dt),
        Config_End_Dt = ISNULL(@Config_End_Dt, @RM_Default_Config_End_Dt),
        Hedge_Type_Id = @Hedge_Type_Id,
        Max_Hedge_Pct = @Max_Hedge_Pct,
        RM_Risk_Tolerance_Category_Id = @RM_Risk_Tolerance_Category_Id,
        Risk_Manager_User_Info_Id = @Risk_Manager_User_Info_Id,
        Contact_Info_Id = @Contact_Info_Id,
        Include_In_Reports = @Include_In_Reports,
        Last_Updated_By = @User_Info_Id,
        Last_Updated_Ts = GETDATE()
    WHERE RM_Client_Hier_Hedge_Config_Id = @RM_Client_Hier_Hedge_Config_Id;

    UPDATE chob
    SET chob.RM_Forecast_UOM_Type_Id = @RM_Forecast_UOM_Type_Id
    FROM Trade.RM_Client_Hier_Hedge_Config chhc
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON chhc.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
    WHERE chhc.RM_Client_Hier_Hedge_Config_Id = @RM_Client_Hier_Hedge_Config_Id;

--SELECT  
--      @RM_Client_Hier_Onboard_Id = RM_Client_Hier_Onboard_Id  
--FROM  
--      dbo.RM_Client_Hier_Hedge_Config  
--WHERE  
--      RM_Client_Hier_Hedge_Config_Id = @RM_Client_Hier_Hedge_Config_Id  

--EXEC dbo.RM_Forecast_Volume_Load   
--      @RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id  



END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Hedge_Config_Upd] TO [CBMSApplication]
GO
