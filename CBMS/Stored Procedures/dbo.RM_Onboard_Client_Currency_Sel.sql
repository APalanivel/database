SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Onboard_Client_Currency_Sel                    
                      
Description:                      
        To get site's hedge configurations
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Client_Id			INT
    @Commodity_Id		INT
    @Hedge_Type		INT
    @Start_Dt	DATE
    @End_Dt		DATE
    @Contract_Id		VARCHAR(MAX)
    @Site_Id			INT				NULL
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------

	EXEC dbo.RM_Onboard_Client_Currency_Sel  
		

Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
	PR          2019-03-11	   Created GRM
                     
******/
CREATE  PROCEDURE [dbo].[RM_Onboard_Client_Currency_Sel]
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            cu.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
      FROM
            Trade.RM_Client_Hier_Onboard chob
            INNER JOIN dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
                  ON chob.Country_Id = cucm.COUNTRY_ID
            INNER JOIN dbo.CURRENCY_UNIT cu
                  ON cucm.CURRENCY_UNIT_ID = cu.CURRENCY_UNIT_ID
      GROUP BY
            cu.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           

END;
GO
GRANT EXECUTE ON  [dbo].[RM_Onboard_Client_Currency_Sel] TO [CBMSApplication]
GO
