SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_BATCH_REPORT_CLIENTS_AND_LIST_NAMES_P
@userId varchar(10),
@sessionId varchar(20)
AS
select 
	 list.report_name,list.report_list_id,	cli.client_name,cli.client_id
from 
	rm_batch_configuration config, 
	rm_report_batch batch,
	report_list list,
	client cli
where 
	config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
	and list.report_list_id = config.report_list_id
	and cli.client_id = config.client_id
	and batch.rm_report_batch_id = (select MAX(rm_report_batch_id) from rm_report_batch)

order by cli.client_name
GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_REPORT_CLIENTS_AND_LIST_NAMES_P] TO [CBMSApplication]
GO
