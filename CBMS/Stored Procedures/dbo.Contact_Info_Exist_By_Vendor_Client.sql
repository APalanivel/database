
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Contact_Info_Exist_By_Vendor_Client            
                        
 DESCRIPTION:                        
			To check if any contacts assigned to a vendor or a client when vendor delete this sproc can be used to validate if it can be deleted or not.              
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id						INT				 NULL
@Vendor_Id						INT				 NULL
                                  
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Is_Contact_Info_Exist			BIT			OUTPUT
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

 --Client Level
 
 DECLARE @Is_Contact_Info_Exist BIT= 0
 EXEC [dbo].[Contact_Info_Exist_By_Vendor_Client] 
      @Client_Id = 235
     ,@Is_Contact_Info_Exist = @Is_Contact_Info_Exist OUT
   select @Is_Contact_Info_Exist as Is_Contact_Info_Exist
      
--Vendor level

 DECLARE @Is_Contact_Info_Exist BIT= 0
 EXEC [dbo].[Contact_Info_Exist_By_Vendor_Client] 
      @Vendor_Id = 320
     ,@Is_Contact_Info_Exist = @Is_Contact_Info_Exist OUT
select @Is_Contact_Info_Exist as Is_Contact_Info_Exist
   


DECLARE @Is_Contact_Info_Exist BIT
EXEC [dbo].[Contact_Info_Exist_By_Vendor_Client] 
      @Vendor_Id = 29865
     ,@Is_Contact_Info_Exist = @Is_Contact_Info_Exist OUT
	select @Is_Contact_Info_Exist as Is_Contact_Info_Exist
EXEC [dbo].[Contact_Info_Exist_By_Vendor_Client] 
      @Vendor_Id = 28896
     ,@Is_Contact_Info_Exist = @Is_Contact_Info_Exist OUT
	select @Is_Contact_Info_Exist as Is_Contact_Info_Exist


                      
                       
AUTHOR INITIALS:        
	Initials	Name        
---------------------------------------------------------------------------------------------------------------                      
	SP          Sandeep Pigilam
         
                         
MODIFICATIONS:      
	Initials	Date		Modification      
---------------------------------------------------------------------------------------------------------------      
	SP          2016-11-21  Created for Invoice Tracking
	RR			2017-10-09	MAINT-6076 - Defaulted @Is_Contact_Info_Exist to zero, if no reocrd exists, NULL value causing issue in app
                       
******/    
            
CREATE PROCEDURE [dbo].[Contact_Info_Exist_By_Vendor_Client]
      ( 
       @Client_Id INT = NULL
      ,@Vendor_Id INT = NULL
      ,@Is_Contact_Info_Exist BIT OUTPUT )
AS 
BEGIN                
      SET NOCOUNT ON;  
      
      SELECT
            @Is_Contact_Info_Exist = 0

      SELECT
            @Is_Contact_Info_Exist = 1
      FROM
            dbo.Vendor_Contact_Map vcm
      WHERE
            vcm.VENDOR_ID = @Vendor_Id
     
      SELECT
            @Is_Contact_Info_Exist = 1
      FROM
            dbo.Client_Contact_Map vcm
      WHERE
            vcm.CLIENT_ID = @Client_Id
     
     
END;


;
GO

GRANT EXECUTE ON  [dbo].[Contact_Info_Exist_By_Vendor_Client] TO [CBMSApplication]
GO
