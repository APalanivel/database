SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_NYMEX_SETTLEMENT_DATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@monthIdentifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_NYMEX_SETTLEMENT_DATE_P

@userId varchar(10),
@sessionId varchar(20),
@monthIdentifier datetime

 AS
set nocount on
	select settlement_date from rm_nymex_settlement 
	where month_identifier = @monthIdentifier
GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_SETTLEMENT_DATE_P] TO [CBMSApplication]
GO
