SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
  
  
  
/******        
NAME:    [Workflow].[Workflow_Get_Navigation_Permission]    
DESCRIPTION:  it'll Return the CBMS Home Page Exceptions Counts details for each  Queue Modules    
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description        
 @userID   VARCHAR(MAX),         
 @moduleID INT        
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:        
 EXEC [Workflow].[Workflow_Get_Navigation_Permission] @User_ID   ='115680' --(Current User)  --115681,115680  
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
TRK   Ramakrishna Thummala Summit Energy   
AP Arunkumar Palanivel    
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 AP Nov 5,2019 This procedure is created to get the navigation permission details. this prpcedure will check if the user is part of DAta users then procedure will return 1  
 else 0 along with exdeption type of the invoice.  
    
******/    
    
CREATE PROCEDURE [Workflow].[Workflow_Get_Navigation_Permission]            
(            
  @User_ID   INT ,--(Current User)  ,  
  @Invoice_ID INT   
)            
AS            
BEGIN         
  
SET NOCOUNT ON   
  
DECLARE @exception_type VARCHAR(200) ,  
@User_permission CHAR(1)  
  
  
      
SELECT @exception_type =  EXCEPTION_TYPE FROM dbo.CU_EXCEPTION_DENORM   
WHERE CU_INVOICE_ID = @Invoice_ID  
  
SELECT @User_permission ='Y'  
FROM dbo.USER_INFO_GROUP_INFO_MAP UIG  
JOIN dbo.GROUP_INFO GI   
ON GI.GROUP_INFO_ID = UIG.GROUP_INFO_ID    
WHERE UIG.USER_INFO_ID = @User_ID   
AND GI.GROUP_NAME = 'Data Users Only'  
  
  
SELECT @exception_type AS Exception_Type, @User_permission AS User_Part_of_Data_Users_Group   
END                
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Get_Navigation_Permission] TO [CBMSApplication]
GO
