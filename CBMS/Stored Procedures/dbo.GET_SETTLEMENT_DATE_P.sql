SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_SETTLEMENT_DATE_P
@userId varchar(10),
@sessionId varchar(20),
@currentDate datetime
AS
	set nocount on
	SELECT
		COUNT(*)
		
	FROM
		RM_NYMEX_SETTLEMENT
		
	WHERE
		settlement_date=convert(varchar(12),@currentDate-1,101)
GO
GRANT EXECUTE ON  [dbo].[GET_SETTLEMENT_DATE_P] TO [CBMSApplication]
GO
