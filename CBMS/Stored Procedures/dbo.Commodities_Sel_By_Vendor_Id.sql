SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
        
/******        
NAME:        
 dbo.Commodities_Sel_By_Vendor_Id      
      
DESCRIPTION:        
 Used to get all commodity as per Client_ID & @Commodity_Service_Type      
      
INPUT PARAMETERS:        
Name      DataType  Default  Description        
------------------------------------------------------------        
  @vendor_id INT      
            
             
OUTPUT PARAMETERS:        
Name      DataType  Default  Description        
------------------------------------------------------------        
      
USAGE EXAMPLES:        
------------------------------------------------------------      
EXEC dbo.Commodities_Sel_By_Vendor_Id   2182
EXEC dbo.Commodities_Sel_By_Vendor_Id   10
       
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKR     Ashok Kumar Raju    
      
MODIFICATIONS         
Initials Date  Modification        
------------------------------------------------------------        
AKR      created, to show the commodities in specific Order.    
      
******/      
CREATE PROCEDURE dbo.Commodities_Sel_By_Vendor_Id ( @vendor_id INT )
AS 
BEGIN              
               
      SET NOCOUNT ON ;              
      
      WITH  cte_Commodity_list
              AS ( SELECT
                        vcm.commodity_type_id commodity_id
                       ,comm.Commodity_Name Commodity_Name
                       ,case WHEN comm.Commodity_Name = 'Electric Power' THEN 1
                             WHEN comm.Commodity_Name = 'Natural Gas' THEN 2
                             ELSE 3
                        END Sort_Order
                   FROM
                        dbo.vendor_commodity_map vcm
                        JOIN dbo.Commodity comm
                              ON comm.Commodity_Id = vcm.commodity_type_id
                   WHERE
                        vcm.vendor_id = @vendor_id)
            SELECT
                  ccl.commodity_id
                 ,ccl.Commodity_Name
            FROM
                  cte_Commodity_list ccl
            ORDER BY
                  ccl.Sort_Order
                 ,ccl.Commodity_Name        
                 
END      


;
GO
GRANT EXECUTE ON  [dbo].[Commodities_Sel_By_Vendor_Id] TO [CBMSApplication]
GO
