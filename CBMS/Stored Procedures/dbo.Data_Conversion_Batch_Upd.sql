SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
  
NAME:  
    dbo.Data_Conversion_Batch_Upd    
         
DESCRIPTION:            
     
        
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
 @Data_Conversion_Batch_Id   INT  
            
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        
            
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              
   
 BEGIN TRAN  
 EXEC dbo.Data_Conversion_Batch_Ins   
      'D:\Test.xls'  
 ROLLBACK TRAN     
    
        
 AUTHOR INITIALS:          
         
 Initials              Name          
---------------------------------------------------------------------------------------------------------------                        
 AKR                   Ashok Kumar Raju          
  
    
 MODIFICATIONS:        
            
 Initials              Date             Modification        
---------------------------------------------------------------------------------------------------------------        
 AKR                   2014-06-01      Created.  
*********/  
  
CREATE PROCEDURE dbo.Data_Conversion_Batch_Upd  
      (   
       @Data_Conversion_Batch_Id INT )  
AS   
BEGIN  
  
      UPDATE  
            etl.Data_Conversion_Batch  
      SET     
            Batch_End_Ts = GETDATE()  
      WHERE  
            Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                       
    
  
END  


;
GO
GRANT EXECUTE ON  [dbo].[Data_Conversion_Batch_Upd] TO [ETL_Execute]
GO
