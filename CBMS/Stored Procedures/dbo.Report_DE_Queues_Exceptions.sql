SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
/*****
        
NAME: dbo.Report_DE_Queues_Exceptions      
    
DESCRIPTION:        
	used to get the rates
       
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    
USAGE EXAMPLES:
------------------------------------------------------------
EXEC Report_DE_Queues_Exceptions      
    
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
AKR   Ashok Kumar Raju
     
MAINTENANCE LOG:        
Initials	Date		Modification 
--- ---------- ----------------------------------------------        
  AKR    2012-05-01  Cloned from View " dbo.lec_Queues_Exceptions"
******/

CREATE PROCEDURE dbo.Report_DE_Queues_Exceptions
AS 
BEGIN        
    
      SET NOCOUNT ON ;   
  
      SELECT
            q.queue_name [Analyst]
           ,d.CU_INVOICE_ID [Invoice ID]
           ,d.EXCEPTION_STATUS_TYPE [Status]
           ,c.IMAGE_COMMENT [Comment] --concatenate  
           ,c.COMMENTDATE [Comment Date]
           ,d.EXCEPTION_TYPE [Type]
           ,d.SERVICE_MONTH [Month]
           ,d.ubm_SITE_NAME [Site]
           ,d.ubm_CITY [City]
           ,d.ubm_STATE_NAME [State]
           ,d.ubm_ACCOUNT_NUMBER [Account Number]
           ,v.VENDOR_NAME [Vendor]
           ,d.DATE_IN_QUEUE [Date in Queue]
      FROM
            dbo.CU_EXCEPTION_DENORM d
            JOIN dbo.vwCbmsQueueName q
                  ON q.queue_id = d.QUEUE_ID
            LEFT JOIN dbo.account a
                  ON a.account_id = d.account_id
            LEFT JOIN dbo.vendor v
                  ON v.vendor_id = a.vendor_id
            LEFT JOIN dbo.user_info ui
                  ON ui.FIRST_NAME + ' ' + ui.last_name = q.queue_name
            JOIN dbo.USER_INFO_GROUP_INFO_MAP m
                  ON m.USER_INFO_ID = ui.USER_INFO_ID
                     AND m.GROUP_INFO_ID = 81
            LEFT JOIN ( SELECT
                              cuc.cu_invoice_id
                             ,image_comment
                             ,x.commentdate
                        FROM
                              dbo.cu_invoice_comment cuc
                              JOIN ( SELECT
                                          max(comment_date) commentdate
                                         ,cu_invoice_id
                                     FROM
                                          dbo.CU_INVOICE_COMMENT
                                     GROUP BY
                                          cu_invoice_id ) x
                                    ON x.CU_INVOICE_ID = cuc.cu_invoice_id
                                       AND x.commentdate = cuc.COMMENT_DATE
                        GROUP BY
                              cuc.CU_INVOICE_ID
                             ,image_comment
                             ,x.commentdate ) c
                  ON c.CU_INVOICE_ID = d.CU_INVOICE_ID
      GROUP BY
            queue_name
           ,d.CU_INVOICE_ID
           ,d.EXCEPTION_STATUS_TYPE
           ,c.IMAGE_COMMENT  --concatenate  
           ,c.COMMENTDATE
           ,d.EXCEPTION_TYPE
           ,d.SERVICE_MONTH
           ,d.ubm_SITE_NAME
           ,d.ubm_CITY
           ,d.ubm_STATE_NAME
           ,d.ubm_ACCOUNT_NUMBER
           ,v.VENDOR_NAME
           ,d.DATE_IN_QUEUE  
  
END

;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Queues_Exceptions] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Queues_Exceptions] TO [CBMSApplication]
GO
