SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsSsoMisGlossary_GetForStartLetter]
	( @MyAccountId int 
	, @start_letter char(1) = null
	)
AS
BEGIN

	   select term
		, definition
	     from sso_mis_glossary
	    where left(upper(term), 1) = isNull(@start_letter, left(upper(term), 1))
	 order by term

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoMisGlossary_GetForStartLetter] TO [CBMSApplication]
GO
