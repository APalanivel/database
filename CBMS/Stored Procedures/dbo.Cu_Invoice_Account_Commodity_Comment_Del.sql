SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
              
Name:     
 dbo.Cu_Invoice_Account_Commodity_Comment_Del          
                
Description:                
   To delete data from dbo.Cu_Invoice_Account_Commodity_Comment   
                
 Input Parameters:                
    Name     DataType  Default  Description                  
---------------------------------------------------------------------------------  
 @Account_Id    INT  
      
      
Output Parameters:                      
    Name     DataType  Default  Description                  
---------------------------------------------------------------------------------  
                
Usage Examples:                    
---------------------------------------------------------------------------------  
  
 BEGIN TRAN      
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 53755 and ac.Cu_Invoice_Id = 31062237 and ac.Commodity_Id = 291  
  EXEC dbo.Cu_Invoice_Account_Commodity_Comment_Del  31062237,53755,291  
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 53755 and ac.Cu_Invoice_Id = 31062237 and ac.Commodity_Id = 291  
 ROLLBACK TRAN  
   
 BEGIN TRAN      
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1150417 and ac.Cu_Invoice_Id = 35243504 and ac.Commodity_Id = 290  
  EXEC dbo.Cu_Invoice_Account_Commodity_Comment_Del  35243504,1150417,290  
  SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment ac JOIN dbo.Comment com ON ac.Comment_Id = com.Comment_ID WHERE ac.Account_Id = 1150417 and ac.Cu_Invoice_Id = 35243504 and ac.Commodity_Id = 290  
 ROLLBACK TRAN  
     
   
Author Initials:                
    Initials Name                
---------------------------------------------------------------------------------  
 RKV   Ravi kumar vegesna
 ABK	Aditya Bharadwaj Kolipaka                 
   
Modifications:                
 Initials    Date  Modification                
---------------------------------------------------------------------------------  
    RKV   2018-05-25 Created for Maint-7217.
	ABK	  2019-06-14 Added code to delete data from 	Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map table           
               
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Comment_Del]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Total_Row_Count INT
            , @Row_Counter INT
            , @Comment_Id INT;

        DECLARE @Comment_List TABLE
              (
                  Comment_Id INT
                  , Cu_Invoice_Id INT
                  , Commodity_Id INT
                  , Row_Num INT IDENTITY(1, 1)
              );

        INSERT INTO @Comment_List
             (
                 Comment_Id
                 , Cu_Invoice_Id
                 , Commodity_Id
             )
        SELECT
            Comment_Id
            , Cu_Invoice_Id
            , Commodity_Id
        FROM
            dbo.Cu_Invoice_Account_Commodity_Comment
        WHERE
            Account_Id = @Account_Id;

        SELECT  @Total_Row_Count = MAX(Row_Num)FROM @Comment_List;

        SET @Row_Counter = 1;
        WHILE (@Row_Counter <= @Total_Row_Count)
            BEGIN



                DELETE
                ciaccrstm
                FROM
                    dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map ciaccrstm
                    INNER JOIN dbo.Cu_Invoice_Account_Commodity_Comment ciac
                        ON ciac.Cu_Invoice_Account_Commodity_Comment_Id = ciaccrstm.Cu_Invoice_Account_Commodity_Comment_Id
                    INNER JOIN @Comment_List cl
                        ON cl.Comment_Id = ciac.Comment_Id
                           AND  cl.Commodity_Id = ciac.Commodity_Id
                           AND  cl.Cu_Invoice_Id = ciac.Cu_Invoice_Id
                WHERE
                    ciac.Account_Id = @Account_Id
                    AND cl.Row_Num = @Row_Counter;



                DELETE
                ciac
                FROM
                    dbo.Cu_Invoice_Account_Commodity_Comment ciac
                    INNER JOIN @Comment_List cl
                        ON cl.Comment_Id = ciac.Comment_Id
                           AND  cl.Commodity_Id = ciac.Commodity_Id
                           AND  cl.Cu_Invoice_Id = ciac.Cu_Invoice_Id
                WHERE
                    ciac.Account_Id = @Account_Id
                    AND cl.Row_Num = @Row_Counter;




                SELECT
                    @Comment_Id = Comment_Id
                FROM
                    @Comment_List
                WHERE
                    Row_Num = @Row_Counter;


                DELETE  FROM dbo.Comment WHERE  Comment_ID = @Comment_Id;



                SET @Row_Counter = @Row_Counter + 1;

            END;

        DELETE  FROM
        dbo.Cu_Invoice_Account_Commodity_Comment
        WHERE
            Account_Id = @Account_Id;
    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Comment_Del] TO [CBMSApplication]
GO
