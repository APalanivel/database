
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/***************************************************************************************************************
NAME: cbmsDocument_GetForProjectLinked

DESCRIPTION : This Procedure  is to search SSO_Documents for given Criteria 

INPUT PARAMETERS:
	Name					DataType		Default			Description
-----------------------------------------------------------------------------------
	@MyAccountId			int
	@sso_project_id			int

OUTPUT PARAMETERS:
	Name			DataType		Default				DESCription
------------------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------------------
	EXEC dbo.cbmsDocument_GetForProjectLinked 49 ,5519
	
AUTHOR INITIALS:

	Initials		Name
------------------------------------------------------------------------------------
	DRG				Dhilu Raichal George
		
MODIFICATIONS

	Initials		Date				Modification
------------------------------------------------------------------------------------
	DRG				10-SEP-2014			modified to get category from new table 
*******************************************************************************/

CREATE PROCEDURE [dbo].[cbmsDocument_GetForProjectLinked]
      ( 
       @MyAccountId INT
      ,@sso_project_id INT )
AS 
BEGIN

      SELECT
            map.sso_document_id
           ,left(cat.category, len(cat.category) - 1) category_type
           ,sd.document_title
           ,sd.document_reference_date
           ,sd.last_updated_date
           ,ci.cbms_image_size
      FROM
            dbo.SSO_PROJECT_DOCUMENT_MAP map
            INNER JOIN dbo.SSO_DOCUMENT sd
                  ON sd.sso_document_id = map.sso_document_id
            CROSS APPLY ( SELECT
                              cdc.Category_Name + ','
                          FROM
                              dbo.Client_Document_Category cdc
                              INNER  JOIN dbo.Client_Document_Category_Map cdcm
                                    ON cdcm.Client_Document_Category_Id = cdc.Client_Document_Category_Id
                          WHERE
                              sd.SSO_Document_ID = cdcm.SSO_Document_ID
            FOR
                          XML PATH('') ) cat ( category )
            INNER JOIN dbo.cbms_image ci
                  ON ci.cbms_image_id = sd.cbms_image_id
      WHERE
            map.sso_project_id = @sso_project_id

END
	 


;
GO

GRANT EXECUTE ON  [dbo].[cbmsDocument_GetForProjectLinked] TO [CBMSApplication]
GO
