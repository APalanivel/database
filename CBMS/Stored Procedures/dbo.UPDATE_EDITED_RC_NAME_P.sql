SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.UPDATE_EDITED_RC_NAME_P
	@RATE_COMPARISON_NAME VARCHAR(200),
	@RC_RATE_COMPARISON_ID INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.RC_RATE_COMPARISON
	SET RATE_COMPARISON_NAME = @RATE_COMPARISON_NAME
	WHERE RC_RATE_COMPARISON_ID = @RC_RATE_COMPARISON_ID

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_EDITED_RC_NAME_P] TO [CBMSApplication]
GO
