SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create PROCEDURE [dbo].[cbmsNymex_Get31ActivePrice]
	( @MyAccountId int 
	)
AS
BEGIN


select top 31 x.last_trade_date
		, x.ClosePrice
from
(select distinct(last_trade_date)
	, 'ClosePrice' = (select close_price from rm_nymex_data where rm_nymex_data_id = (select min(rm_nymex_data_id) from rm_nymex_data where last_trade_date = rm.last_trade_date))
from rm_nymex_data rm ) x
order by x.last_trade_date desc 

 

END

GO
GRANT EXECUTE ON  [dbo].[cbmsNymex_Get31ActivePrice] TO [CBMSApplication]
GO
