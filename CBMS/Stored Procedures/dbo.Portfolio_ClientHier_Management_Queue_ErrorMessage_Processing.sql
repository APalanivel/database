SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************      
NAME : [dbo].[Portfolio_ClientHier_Management_Queue_ErrorMessage_Processing]
     
DESCRIPTION: 
			This sp will reprocess msgs in poison table for Portfolio_ClientHier_Management_Queue.  
 
 INPUT PARAMETERS:      
 Name					DataType               Default        Description      
-------------------------------------------------------------------------------------------------------

 OUTPUT PARAMETERS:      
 Name			DataType				Default			Description      
-------------------------------------------------------------------------------------------------------  

 USAGE EXAMPLES:      
-------------------------------------------------------------------------------------------------------

Only DB will execute this sp 
EXEC [dbo].[Portfolio_ClientHier_Management_Queue_ErrorMessage_Processing]

AUTHOR INITIALS:      
 Initials				Name      
-------------------------------------------------------------------------------------------------------
	DSC					Kaushik

 MODIFICATIONS       
 Initials			Date				Modification      
-------------------------------------------------------------------------------------------------------
	DSC				2014-07-07			Created
******************************************************************************************************/  
CREATE PROCEDURE [dbo].[Portfolio_ClientHier_Management_Queue_ErrorMessage_Processing]
AS
BEGIN

      SET NOCOUNT ON;


      DECLARE
            @id1 INT = 1
           ,@id2 INT = 1
           ,@id INT = 1
           ,@Conversation_Group_Id UNIQUEIDENTIFIER
           ,@Conversation_Handle UNIQUEIDENTIFIER
           ,@Old_Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Body XML
           ,@Message_Type VARCHAR(255)
           ,@Client_Id INT
           ,@Portfolio_Client_Id INT
           ,@Site_Id INT
           ,@Opcode VARCHAR(1)
           ,@Division_Id INT
           ,@Portfolio_Client_Hier_Reference_Number INT
           ,@Site_Ch_Id INT
           ,@Site_Message XML
           ,@Sitegroup_Id INT


      DECLARE @Sites_For_New_Divison TABLE
            (
             Id INT IDENTITY(1, 1)
            ,Site_Id INT )

	  DECLARE @Site_Del_Error_Msgs TABLE (Client_Id INT)


		
      CREATE TABLE #Portfolio_ClientHier_Management_Queue_ErrorMessage
            (
             Message_Id INT IDENTITY(1, 1)
            ,Queue_Name VARCHAR(255)
            ,Conversation_Group_Id UNIQUEIDENTIFIER
            ,[Conversation_Handle] UNIQUEIDENTIFIER
            ,MESSAGE_Body XML
            ,Message_Sequence_Number INT
            ,Message_Type VARCHAR(255)
            ,Message_Received_Ts DATETIME )

      CREATE CLUSTERED INDEX ix_ErrorMessage ON #Portfolio_ClientHier_Management_Queue_ErrorMessage
      (Message_Id)


      INSERT      INTO #Portfolio_ClientHier_Management_Queue_ErrorMessage
                  ( Queue_Name
                  ,Conversation_Group_Id
                  ,Conversation_Handle
                  ,MESSAGE_Body
                  ,Message_Sequence_Number
                  ,Message_Type
                  ,Message_Received_Ts )
                  SELECT
                        Queue_Name
                       ,[Conversation_Group_Id]
                       ,[Conversation_Handle]
                       ,convert(XML, MESSAGE_Body)
                       ,Message_Sequence_Number
                       ,Message_Type
                       ,Message_Received_Ts
                  FROM
                        dbo.Service_Broker_Poison_Message WITH ( NOLOCK )
                  WHERE
                        Queue_Name = 'Portfolio_ClientHier_Management_Queue'
                        AND MESSAGE_Body IS NOT NULL
                  ORDER BY
                        Message_Received_Ts
                       ,Conversation_Group_Id
                       ,Conversation_Handle
                       ,Message_Sequence_Number
				
      SELECT
            @id2 = @@ROWCOUNT


      DECLARE @Portfolio_Client_Ids TABLE
            (
             Portfolio_Client_Id INT )



      INSERT      INTO @Portfolio_Client_Ids
                  ( Portfolio_Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.App_Config AS ac
                        CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                  WHERE
                        ac.App_Config_Cd = 'Portfolio_ClientHier_Management'



      WHILE ( @id2 >= @id1 )
            BEGIN
	
                  BEGIN TRY 
			
                        BEGIN TRANSACTION
				
                        SELECT
                              @Conversation_Group_Id = Conversation_Group_Id
                             ,@Old_Conversation_Handle = Conversation_Handle
                             ,@Message_Type = Message_Type
                             ,@Message_Body = Message_Body
                        FROM
                              #Portfolio_ClientHier_Management_Queue_ErrorMessage
                        WHERE
                              Message_Id = @id1
								   
                        IF EXISTS ( SELECT
                                          1
                                    FROM
                                          sys.conversation_endpoints
                                    WHERE
                                          conversation_handle = @Old_Conversation_Handle
                                          AND state != 'CD' )
					
							  --End old conversation
                              END CONVERSATION @Old_Conversation_Handle--Error Msg Conversation


                        SET @Conversation_Handle = NULL;

						
                        IF ( @Message_Type = '//Change_Control/Message/Delete_Portfolio_Site' )
                              BEGIN	
								  
                                    SET @Opcode = NULL
                                    SET @Site_Ch_Id = NULL
                                    SET @Client_Id = NULL
                                    SET @Portfolio_Client_Id = NULL
                                    SET @Division_Id = NULL

                                    SELECT
                                          @Opcode = cng.ch.value('Op_Code[1]', 'VARCHAR')
                                         ,@Site_Ch_Id = cng.ch.value('Client_Hier_Id[1]', 'INT')
                                         ,@Client_Id = cng.ch.value('Client_Id[1]', 'INT')
                                         ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
                                    FROM
                                          @Message_Body.nodes('/Site_Info/Site') cng ( ch ) 
									
                                    SELECT
                                          @Division_Id = Division_Id
                                    FROM
                                          dbo.Site
                                    WHERE
                                          Portfolio_Client_Hier_Reference_Number = @Site_Ch_Id


                                    SELECT
                                          @Client_Id = Client_Id
                                         ,@Portfolio_Client_Id = isnull(Portfolio_Client_Id,@Portfolio_Client_Id)
                                    FROM
                                          CORE.Client_Hier
                                    WHERE
                                          Client_Hier_Id = @Site_Ch_Id

									-- There are 2 cases to get this msg 
										-- original site deleted in site table we get client_Id
										-- original site is unmapped we get Portfolio_Client_Id
									


									IF EXISTS ( SELECT
														1
												FROM
														@Portfolio_Client_Ids
												WHERE
														Portfolio_Client_Id = @Portfolio_Client_Id )
									BEGIN 


												IF EXISTS ( SELECT
																  1
															FROM
																  Core.Client_Hier
															WHERE
																  Client_Hier_Id = @Site_Ch_Id
																  AND Portfolio_Client_Id IS NULL )
													  BEGIN 
										
												
															SET @Conversation_Handle = NULL;
												
															BEGIN DIALOG CONVERSATION @Conversation_Handle -- New Conversation
															FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
															TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
															ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    
															;
															SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Delete_Portfolio_Site] (@Message_Body)

															END CONVERSATION @Conversation_Handle

													  END


												-- Original site is deleted , duplicate site should be deleted.
														-- 1. @client_Id not null indicates original site is deleted.
														-- 2. Chk if original site is mapped or not.

												
												
												IF EXISTS ( SELECT
																  1
															FROM
																  Core.Client_Hier
															WHERE
																  Client_Hier_Id = @Site_Ch_Id
																  AND Portfolio_Client_Id IS NOT NULL )
													  BEGIN 

													  
													  IF NOT EXISTS (SELECT 1 FROM @Site_Del_Error_Msgs WHERE client_Id = @Client_Id)
													  BEGIN

															INSERT INTO @Site_Del_Error_Msgs (Client_Id) SELECT @Client_Id


															DECLARE @Del_Sites TABLE
																  (
																   Id INT IDENTITY(1, 1)
																  ,Site_CH_Id INT )

															INSERT      INTO @Del_Sites
																		( Site_Ch_Id )
																		SELECT
																			  ch.Client_Hier_Id
																		FROM
																			  core.Client_Hier ch
																		WHERE
																			  Client_Id = @Client_Id
																			  AND Site_Id > 0


															SET @Conversation_Handle = NULL
												
															SET @Site_Message = NULL

															SET @id = 1
                                                
															WHILE EXISTS ( SELECT
																			  1
																		   FROM
																			  @Del_Sites )
																  BEGIN

																		SET @Site_Message = ( SELECT
																								Site_Ch_Id AS Client_Hier_Id
																							   ,'D' AS Op_Code
																							   ,@Portfolio_Client_Id AS Portfolio_Client_Id
																							  FROM
																								@Del_Sites
																							  WHERE
																								id = @id
																			  FOR
																							  XML PATH('Site')
																								 ,ELEMENTS
																								 ,ROOT('Site_Info') )


																	  
																		IF @Site_Message IS NOT NULL
																			  BEGIN
																	  	
																	  
																					BEGIN DIALOG CONVERSATION @Conversation_Handle
																					FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																					TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'    
																					ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

																					SEND ON CONVERSATION @Conversation_Handle    
																					MESSAGE TYPE [//Change_Control/Message/Delete_Portfolio_Site] (@Site_Message)    


																			  END


																		DELETE
																			  @Del_Sites
																		WHERE
																			  id = @id

																		SET @Id = @Id + 1

                                               
																  END 



														-- Inserting Latest sites 

															DECLARE @Ins_Latest_Sites TABLE
																  (
																   Id INT IDENTITY(1, 1)
																  ,Site_Id INT )

															INSERT      INTO @Ins_Latest_Sites
																		(Site_Id )
																		SELECT
																			  Site_Id
																		FROM
																			  core.Client_Hier ch
																		WHERE
																			  Client_Id = @client_id
																			  AND site_id > 0



															SET @id = 1

															SET @Conversation_Handle = NULL

															SET @Site_Message = NULL

															WHILE EXISTS ( SELECT
																			  1
																		   FROM
																			  @Ins_Latest_Sites )
																  BEGIN

                              

																		SET @Site_Message = ( SELECT
																								Site_Id
																							   ,@Client_Id AS Client_Id
																							   ,'I' AS Op_Code
																							  FROM
																								@Ins_Latest_Sites
																							  WHERE
																								id = @id
																			  FOR
																							  XML PATH('Site')
																								 ,ELEMENTS
																								 ,ROOT('Site_Info') )


																			
																		IF @Site_Message IS NOT NULL
																			  BEGIN
																					BEGIN DIALOG CONVERSATION @Conversation_Handle  
																				FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]
																				TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
																				ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    
																					SEND ON CONVERSATION @Conversation_Handle
																				MESSAGE TYPE [//Change_Control/Message/Mapping_Site_To_Portfolio] (@Site_Message) 

																			  END
                                          

																		DELETE
																			  @Ins_Latest_Sites
																		WHERE
																			  id = @id											

											

																		SET @id = @id + 1

																  END			
													END
														
                                          END
									END
                                    

                              END
                        ELSE
                              IF ( @Message_Type = '//Change_Control/Message/Division_Del' )

									-- while reprocessing we need to check if the same client is not mapped again. 
									-- If client is mapped again since sitegroup is already present it will skip inserting sg and sites  
										-- (first delete that old sitegroup and insert sg with latest sites
										-- when unmapped we cant find portfolio_Client_id so didnt included app_config table chk
                                    BEGIN

                                          SET @Division_Id = NULL

                                          SET @Portfolio_Client_Hier_Reference_Number = NULL
										  
                                          SET @Portfolio_Client_Id = NULL
										  
                                          SET @Client_Id = NULL

                                          SELECT
                                                @Division_Id = cng.ch.value('Division_Id[1]', 'INT')
                                          FROM
                                                @Message_Body.nodes('/Delete_Division') cng ( ch ) 

                                          SELECT
                                                @Portfolio_Client_Hier_Reference_Number = Portfolio_Client_Hier_Reference_Number
                                          FROM
                                                dbo.Sitegroup
                                          WHERE
                                                Sitegroup_Id = @Division_Id

                                          SELECT
                                                @Client_Id = Client_Id
                                          FROM
                                                core.Client_Hier
                                          WHERE
                                                Client_Hier_Id = @Portfolio_Client_Hier_Reference_Number




                                          IF EXISTS ( SELECT
                                                            1
                                                      FROM
                                                            dbo.Client
                                                      WHERE
                                                            CLIENT_ID = @Client_Id
                                                            AND Portfolio_Client_Id IS NULL )
                                                AND EXISTS ( SELECT
                                                                  1
                                                             FROM
                                                                  dbo.Sitegroup
                                                             WHERE
                                                                  Sitegroup_Id = @Division_Id )
                                                BEGIN
                                                      BEGIN DIALOG CONVERSATION @Conversation_Handle -- New Conversation
																	FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																	TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
																	ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];  
					
												;
                                                      SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Division_Del] (@Message_Body)

                                                      END CONVERSATION @Conversation_Handle
                                                END
                                         
                                          IF EXISTS ( SELECT
                                                            1
                                                      FROM
                                                            Client
                                                      WHERE
                                                            CLIENT_ID = @Client_Id
                                                            AND Portfolio_Client_Id IS NOT NULL )
                                                AND NOT EXISTS ( SELECT
                                                                  1
                                                                 FROM
                                                                  dbo.SITE
                                                                 WHERE
                                                                  DIVISION_ID = @Division_Id )
                                                AND EXISTS ( SELECT
                                                                  1
                                                             FROM
                                                                  dbo.Sitegroup
                                                             WHERE
                                                                  Sitegroup_Id = @Division_Id )
                                                BEGIN 
		

                                                      SET @Message_Body = ( SELECT
                                                                              @Division_Id AS Division_Id
                                                            FOR
                                                                            XML PATH('Delete_Division') )

                                                      SET @Conversation_Handle = NULL

                                                      BEGIN DIALOG CONVERSATION @Conversation_Handle    
															FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
															TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
															ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];    
                                                      SEND ON CONVERSATION @Conversation_Handle 
															MESSAGE TYPE [//Change_Control/Message/Division_Del] (@Message_Body)

												

                                                      SET @Message_Body = ( SELECT
                                                                              @Client_Id AS Client_Id
                                                                             ,@Portfolio_Client_Id AS Portfolio_Client_Id
                                                            FOR
                                                                            XML PATH('Client')
                                                                               ,ELEMENTS
                                                                               ,ROOT('Mapping_Client_To_Portfolio') )

						                          
                                                      SET @Conversation_Handle = NULL

                                                      BEGIN DIALOG CONVERSATION @Conversation_Handle
															FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
															TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
															ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];  

															;
                                                      SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Mapping_Client_To_Portfolio] (@Message_Body)
													  
						
                                                      END CONVERSATION @Conversation_Handle


                                                END
											
                                                
                                    END
                              ELSE
                                    IF ( @Message_Type = '//Change_Control/Message/UnMapping_Client_From_Portfolio' )

                                          -- If we get this error, it means sitegroup_id is not deleted from sitegroup table. 
										  -- if it is not mapped again we chk if client is there as sg, if exists send this msg for reprocessing.
										  -- if same client is mapped, it had skipped insert sg and latest site msgs.
                                          BEGIN


                                                SET @Client_Id = NULL
                                                SET @Portfolio_Client_Id = NULL
                                                SET @Portfolio_Client_Hier_Reference_Number = NULL
                                                SET @Sitegroup_Id = NULL


                                                SELECT
                                                      @Client_Id = cng.ch.value('Client_Id[1]', 'INT')
                                                     ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
                                                FROM
                                                      @Message_Body.nodes('/UnMapping_Client_From_Portfolio/Client') cng ( ch ) 


                                                SELECT
                                                      @Portfolio_Client_Hier_Reference_Number = Client_Hier_Id
                                                FROM
                                                      Core.Client_Hier
                                                WHERE
                                                      Client_Id = @Client_Id
                                                      AND Sitegroup_Id = 0

                                                SELECT
                                                      @Sitegroup_Id = Sitegroup_Id
                                                FROM
                                                      dbo.Sitegroup
                                                WHERE
                                                      Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number

												
                                                IF EXISTS ( SELECT
                                                                  1
                                                            FROM
                                                                  @Portfolio_Client_Ids
                                                            WHERE
                                                                  Portfolio_Client_Id = @Portfolio_Client_Id )
                                                      BEGIN 
												
												-- 1. not mapped
												-- 2. client exists in sg table 

                                                            IF NOT EXISTS ( SELECT
                                                                              1
                                                                            FROM
                                                                              dbo.CLIENT
                                                                            WHERE
                                                                              CLIENT_ID = @Client_Id
                                                                              AND Portfolio_Client_Id = @Portfolio_Client_Id )
                                                                  AND @Sitegroup_Id IS NOT NULL
                                                                  BEGIN


                                                                        SET @Conversation_Handle = NULL

                                                                        BEGIN DIALOG CONVERSATION @Conversation_Handle 
																		FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																		TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
																		ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];  
																		;
                                                                        SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/UnMapping_Client_From_Portfolio] (@Message_Body)
                                          
                                                                        END CONVERSATION @Conversation_Handle

                                                                  END


											-- Same client is mapped again then send Site msg (both insert and delete) for all sites so that it will get refreshed.
													  
													  
                                                            IF EXISTS ( SELECT
                                                                              1
                                                                        FROM
                                                                              dbo.CLIENT
                                                                        WHERE
                                                                              CLIENT_ID = @Client_Id
                                                                              AND Portfolio_Client_Id = @Portfolio_Client_Id )
                                                                  BEGIN 


                                                                        IF EXISTS ( SELECT
                                                                                          1
                                                                                    FROM
                                                                                          dbo.Sitegroup
                                                                                    WHERE
                                                                                          Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number )
                                                                              BEGIN 
																--unmap all sites

                                                                                    DECLARE @Site_Ch TABLE
                                                                                          (
                                                                                           Id INT IDENTITY(1, 1)
                                                                                          ,Site_CH_Id INT )

                                                                                    INSERT      INTO @Site_Ch
                                                                                                ( Site_Ch_Id )
                                                                                                SELECT
                                                                                                      ch.Portfolio_Client_Hier_Reference_Number
                                                                                                FROM
                                                                                                      dbo.SITE s
                                                                                                      INNER JOIN core.Client_Hier ch
                                                                                                            ON ch.Site_Id = s.SITE_ID
                                                                                                WHERE
                                                                                                      s.Division_Id = @Sitegroup_Id


                                                                                    SET @Conversation_Handle = NULL

                                                                                    SET @id = 1
                                                                                    WHILE EXISTS ( SELECT
                                                                                                      1
                                                                                                   FROM
                                                                                                      @Site_Ch )
                                                                                          BEGIN

                                                                                                SET @Site_Message = ( SELECT
                                                                                                                        Site_Ch_Id AS Client_Hier_Id
                                                                                                                       ,'D' AS Op_Code
                                                                                                                       ,@Portfolio_Client_Id AS Portfolio_Client_Id
                                                                                                                      FROM
                                                                                                                        @Site_Ch
                                                                                                                      WHERE
                                                                                                                        id = @id
                                                                                                      FOR
                                                                                                                      XML PATH('Site')
                                                                                                                         ,ELEMENTS
                                                                                                                         ,ROOT('Site_Info') )


																	  
                                                                                                IF @Site_Message IS NOT NULL
                                                                                                      BEGIN
																	  	
																	  
                                                                                                            BEGIN DIALOG CONVERSATION @Conversation_Handle
																											FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																											TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'    
																											ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

                                                                                                            SEND ON CONVERSATION @Conversation_Handle    
																											MESSAGE TYPE [//Change_Control/Message/Delete_Portfolio_Site] (@Site_Message)    


                                                                                                      END


                                                                                                DELETE
                                                                                                      @Site_Ch
                                                                                                WHERE
                                                                                                      id = @id

                                                                                                SET @Id = @Id + 1

                                               
                                                                                          END 


											-- inserting sites 

                                                                                    INSERT      INTO @Sites_For_New_Divison
                                                                                                (Site_Id )
                                                                                                SELECT
                                                                                                      Site_Id
                                                                                                FROM
                                                                                                      core.Client_Hier ch
                                                                                                WHERE
                                                                                                      Client_Id = @client_id
                                                                                                      AND site_id > 0



                                                                                    SET @id = 1

                                                                                    SET @Conversation_Handle = NULL

                                                                                    SET @Site_Message = NULL

                                                                                    WHILE EXISTS ( SELECT
                                                                                                      1
                                                                                                   FROM
                                                                                                      @Sites_For_New_Divison )
                                                                                          BEGIN

                              

                                                                                                SET @Site_Message = ( SELECT
                                                                                                                        Site_Id
                                                                                                                       ,@Client_Id AS Client_Id
                                                                                                                       ,'I' AS Op_Code
                                                                                                                      FROM
                                                                                                                        @Sites_For_New_Divison
                                                                                                                      WHERE
                                                                                                                        id = @id
                                                                                                      FOR
                                                                                                                      XML PATH('Site')
                                                                                                                         ,ELEMENTS
                                                                                                                         ,ROOT('Site_Info') )


																			
                                                                                                IF @Site_Message IS NOT NULL
                                                                                                      BEGIN
                                                                                                            BEGIN DIALOG CONVERSATION @Conversation_Handle  
																											FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]
																											TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
																											ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    
                                                                                                            SEND ON CONVERSATION @Conversation_Handle
																											MESSAGE TYPE [//Change_Control/Message/Mapping_Site_To_Portfolio] (@Site_Message) 

                                                                                                      END
                                          

                                                                                                DELETE
                                                                                                      @Sites_For_New_Divison
                                                                                                WHERE
                                                                                                      id = @id											

											

                                                                                                SET @id = @id + 1

                                                                                          END			
														
													 
													 
                                                                              END
													 
                                                                  END 

                                                      END
                                          END;
                                    ELSE
                                          IF ( @Message_Type = '//Change_Control/Message/Mapping_Site_To_Portfolio' )
                                                BEGIN

                                                      SET @Site_Id = NULL
                                                      SET @Client_Id = NULL
                                                      SET @Opcode = NULL
                                                      SET @Portfolio_Client_Id = NULL
                                                      SET @Portfolio_Client_Hier_Reference_Number = NULL


                                                      SELECT
                                                            @Site_Id = cng.ch.value('Site_Id[1]', 'INT')
                                                           ,@Client_Id = cng.ch.value('Client_Id[1]', 'INT')
                                                           ,@Opcode = cng.ch.value('Op_Code[1]', 'VARCHAR')
                                                      FROM
                                                            @Message_Body.nodes('/Site_Info/Site') cng ( ch ) 


                                                      SELECT
                                                            @Portfolio_Client_Id = Portfolio_Client_Id
                                                      FROM
                                                            dbo.client
                                                      WHERE
                                                            Client_Id = @Client_Id 

													 
                                                      SELECT
                                                            @Portfolio_Client_Hier_Reference_Number = Client_Hier_Id
                                                      FROM
                                                            Core.Client_Hier
                                                      WHERE
                                                            Site_Id = @Site_Id


													  

													-- Msg is for both insert/update, If unmapped and mapped again this @site_id would be processed for update so no issues.
                                                      
													-- 1. Client should be mapped
													-- 2. Portfolio client should be in app_config table 
													-- 3. Chk if client is unmapped and site is deleted and client is mapped again. we dont need to send this site msg.

                                                      IF @Portfolio_Client_Id IS NOT NULL
                                                            AND EXISTS ( SELECT
                                                                              1
                                                                         FROM
                                                                              @Portfolio_Client_Ids
                                                                         WHERE
                                                                              Portfolio_Client_Id = @Portfolio_Client_Id )
                                                            AND EXISTS ( SELECT
                                                                              1
                                                                         FROM
                                                                              dbo.site
                                                                         WHERE
                                                                              Site_Id = @Site_Id )
                                                            BEGIN
													  
                                                                  BEGIN DIALOG CONVERSATION @Conversation_Handle -- New Conversation
																FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
																ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];  

																;
                                                                  SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE[//Change_Control/Message/Mapping_Site_To_Portfolio]  (@Message_Body)

                                                                  END CONVERSATION @Conversation_Handle

                                                            END
													  
                                                END


                                          ELSE
                                                IF ( @Message_Type = '//Change_Control/Message/Mapping_Client_To_Portfolio' )
                                                      BEGIN

                                                            SET @Client_Id = NULL
                                                            SET @Portfolio_Client_Id = NULL
                                                            SET @Portfolio_Client_Hier_Reference_Number = NULL

                                                            SELECT
                                                                  @Client_Id = cng.ch.value('Client_Id[1]', 'INT')
                                                                 ,@Portfolio_Client_Id = cng.ch.value('Portfolio_Client_Id[1]', 'INT')
                                                            FROM
                                                                  @Message_Body.nodes('/Mapping_Client_To_Portfolio/Client') cng ( ch ) 


                                                            SELECT
                                                                  @Portfolio_Client_Hier_Reference_Number = Client_Hier_Id
                                                            FROM
                                                                  Core.Client_Hier
                                                            WHERE
                                                                  Client_Id = @Client_Id
                                                                  AND Sitegroup_Id = 0


															-- 1.Client should be mapped 
															-- 2.portfolio client should be in app_config table 
															-- 3.client should not be in sitegroup table.

                                                            IF EXISTS ( SELECT
                                                                              1
                                                                        FROM
                                                                              dbo.Client
                                                                        WHERE
                                                                              Client_Id = @Client_Id
                                                                              AND Portfolio_Client_Id = @Portfolio_Client_Id )
                                                                  AND EXISTS ( SELECT
                                                                                    1
                                                                               FROM
                                                                                    @Portfolio_Client_Ids
                                                                               WHERE
                                                                                    Portfolio_Client_Id = @Portfolio_Client_Id )
                                                                  AND NOT EXISTS ( SELECT
                                                                                    1
                                                                                   FROM
                                                                                    dbo.Sitegroup sg
                                                                                   WHERE
                                                                                    Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number )
                                                                  BEGIN
													  
						                          
                                                                        BEGIN DIALOG CONVERSATION @Conversation_Handle -- New Conversation
																		FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
																		TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'   
																		ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];  

																		;
                                                                        SEND ON CONVERSATION @Conversation_Handle MESSAGE TYPE [//Change_Control/Message/Mapping_Client_To_Portfolio] (@Message_Body)
													  
						
                                                                        END CONVERSATION @Conversation_Handle

													  
                                                                  END


                                                      END;
                        
						
						

                        SET @id1 = @id1 + 1

				-- delete error message from poison table
                        DELETE FROM
                              dbo.Service_Broker_Poison_Message
                        WHERE
                              Queue_Name = 'Portfolio_ClientHier_Management_Queue'
                              AND Conversation_Group_Id = @Conversation_Group_Id
                              AND Conversation_Handle = @Old_Conversation_Handle

                        COMMIT TRANSACTION
			
                  END TRY
                  BEGIN CATCH

                        DECLARE @errormessage NVARCHAR(4000) = error_message()
	
                        IF @@TRANCOUNT > 1
                              ROLLBACK TRANSACTION
			
                        RAISERROR(60001, 16, 1, 'SP:Portfolio_ClientHier_Management_Queue_ErrorMessage_Processing', @errormessage)
		
                  END CATCH
	
            END


			




END;













;
GO
