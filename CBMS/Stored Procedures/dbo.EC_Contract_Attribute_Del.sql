SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name: dbo.EC_Contract_Attribute_Del             
              
Description:              
        To delete Data to EC_Contract_Attribute table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------        
    @EC_Contract_Attribute_Id			INT
    @User_Info_Id						INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @EC_Contract_Attribute_Id_Out INT 
	BEGIN TRAN  
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS400_Deleted'
    EXEC dbo.EC_Contract_Attribute_Ins 
      @State_Id = 1
     ,@Commodity_Id = 290
     ,@EC_Contract_Attribute_Name = 'Test_AS400_Deleted'
     ,@Attribute_Type_Cd = 100026
     ,@User_Info_Id = 49
     ,@EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id_Out OUTPUT
      
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS400_Deleted'
		
    EXEC dbo.EC_Contract_Attribute_Del 
      @EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id_Out
      ,@User_Info_Id=49
      
      SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS400_Deleted'	
      SELECT * FROM dbo.Application_Audit_Log aal WHERE Application_Name='Delete Contract Attribute'
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Del]
      ( 
       @EC_Contract_Attribute_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE
            @User_Name VARCHAR(30)
           ,@Current_Ts DATETIME= GETDATE()
           ,@Lookup_Value XML
      
      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id
            
            
      SET @Lookup_Value = ( SELECT
                              eca.EC_Contract_Attribute_Id
                             ,eca.EC_Contract_Attribute_Name
                             ,c.Commodity_Name
                             ,s.STATE_NAME
                             ,cd.Code_Value AS Attribute_Type
                            FROM
                              dbo.EC_Contract_Attribute eca
                              INNER JOIN dbo.Commodity c
                                    ON eca.Commodity_Id = c.Commodity_Id
                              INNER JOIN code cd
                                    ON cd.Code_Id = eca.Attribute_Type_Cd
                              INNER JOIN dbo.STATE s
                                    ON eca.State_Id = s.STATE_ID
                            WHERE
                              eca.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
            FOR
                            XML RAW )
                            
            
      BEGIN TRY
            BEGIN TRAN
            
            DELETE
                  ecav
            FROM
                  dbo.EC_Contract_Attribute_Value ecav
            WHERE
                  ecav.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
            
            DELETE
                  eca
            FROM
                  dbo.EC_Contract_Attribute eca
            WHERE
                  eca.EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
            
            EXEC dbo.Application_Audit_Log_Ins 
                  @Client_Name = NULL
                 ,@Application_Name = 'Delete Contract Attribute'
                 ,@Audit_Function = -1
                 ,@Table_Name = 'EC_Contract_Attribute'
                 ,@Lookup_Value = @Lookup_Value
                 ,@Event_By_User = @User_Name
                 ,@Execution_Ts = @Current_Ts
           
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK TRAN
                  END
		
            EXEC usp_RethrowError
      END CATCH	
            

END



;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Del] TO [CBMSApplication]
GO
