SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.GET_APPROVEING_SITES_P

@userId varchar(10),
@sessionId varchar(20),
@budgetId int 

as
	set nocount on
	SELECT SITE_ID FROM RM_budget_site_map
	where rm_budget_id=@budgetId
GO
GRANT EXECUTE ON  [dbo].[GET_APPROVEING_SITES_P] TO [CBMSApplication]
GO
