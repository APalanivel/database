SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SET_UPDATE_PROCESSING_INSTRUCTIONS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userid                       integer,
@account_id                   integer,
@proInstructions              varchar(4000)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test an update
--exec dbo.SET_UPDATE_PROCESSING_INSTRUCTIONS_P
--@userid = NULL,
----@verdor_id integer,
----@account_number varchar(50),
--@account_id = 84153 ,
--@proInstructions ='o invoices should be posted to this account; route to DM as wrong account and attack to utility.'
--				   -- Original Value = 'No invoices should be posted to this account; route to DM as wrong account and attack to utility.'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SET_UPDATE_PROCESSING_INSTRUCTIONS_P
@userid integer,
@account_id integer,
@proInstructions varchar(4000)

As



SET NOCOUNT ON
	
UPDATE 
	Account 
SET 	
	PROCESSING_INSTRUCTIONS= @proInstructions,
	PROCESSING_INSTRUCTIONS_UPDATED_USER_ID = @userid,
	PROCESSING_INSTRUCTIONS_UPDATED_DATE = getdate()
	
WHERE  ACCOUNT_ID=@account_id
GO
GRANT EXECUTE ON  [dbo].[SET_UPDATE_PROCESSING_INSTRUCTIONS_P] TO [CBMSApplication]
GO
