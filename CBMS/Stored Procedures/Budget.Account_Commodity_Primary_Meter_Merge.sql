SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                                  
NAME:  Budget.Account_Commodity_Primary_Meter_Merge                                                                         
                                                                                  
DESCRIPTION: To Update or Insert Primary Meter Flag Based on Account and Commodity Id.                                                                                  
             Used at:-Client_Info-->Search Clinets --> Manage Clients --> Manage Sites --> Manage Utility Accounts                                                                     
                                                                                  
INPUT PARAMETERS:                                                                                  
           Name         DataType  Default Description                                                                                  
------------------------------------------------------------                                                                                  
@Account_Id     INT          
@Commodity_Id    INT          
@Is_Primary_Meter   INT          
@User_Info_Id    INT                 
                                                            
OUTPUT PARAMETERS:                                                                                  
 Name   DataType  Default Description                                                                                  
------------------------------------------------------------                                                                                  
                                                                                  
USAGE EXAMPLES:                                                                                  
------------------------------------------------------------              
Begin Tran                                                                                  
EXEC Budget.Account_Commodity_Primary_Meter_Merge              
@Account_Id =1741058          
,@Commodity_Meter_Id ='291|1303738'         
,@User_Info_Id = 49                
              
Rollback Tran              
                                              
AUTHOR INITIALS:                                                                                  
 Initials Name                                                                                  
------------------------------------------------------------                                                                                  
SC   Sreenivasulu Cheerala                                                            
                                                                                  
MODIFICATIONS                                                                                  
 Initials Date  Modification                                                                                  
------------------------------------------------------------                                                                                  
 SC   2019-11-04   Initial Created 
                                                                                  
******/
CREATE PROCEDURE [Budget].[Account_Commodity_Primary_Meter_Merge]
    (
        @Account_Id INT
        , @Commodity_Meter_Id VARCHAR(MAX)
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @Commodity_Meter TABLE
              (
                  Commodity_ID INT
                  , Meter_Id INT
              );

        INSERT INTO @Commodity_Meter
             (
                 Commodity_ID
                 , Meter_Id
             )
        SELECT
            CAST(SUBSTRING(us.Segments, 1, CHARINDEX('|', us.Segments) - 1) AS INT)
            , SUBSTRING(us.Segments, CHARINDEX('|', us.Segments) + 1, LEN(us.Segments))
        FROM
            dbo.ufn_split(@Commodity_Meter_Id, ',') us;


        MERGE Budget.Account_Commodity_Primary_Meter AS Tgt
        USING (   SELECT
                        Commodity_ID
                        , Meter_Id
                  FROM
                        @Commodity_Meter) Src
        ON Tgt.[Account_Id] = @Account_Id
           AND  Tgt.[Commodity_Id] = Src.[Commodity_ID]
        WHEN MATCHED THEN UPDATE SET
                              Tgt.Meter_Id = Src.Meter_Id
                              , Tgt.Last_Change_Ts = GETDATE()
                              , Tgt.Updated_User_Id = @User_Info_Id
        WHEN NOT MATCHED THEN INSERT (Account_Id
                                      , Commodity_Id
                                      , Meter_Id
                                      , Created_User_Id
                                      , Created_Ts
                                      , Updated_User_Id
                                      , Last_Change_Ts)
                              VALUES
                                  (@Account_Id
                                   , Src.Commodity_ID
                                   , Src.Meter_Id
                                   , @User_Info_Id
                                   , GETDATE()
                                   , @User_Info_Id
                                   , GETDATE());
    END;
GO
GRANT EXECUTE ON  [Budget].[Account_Commodity_Primary_Meter_Merge] TO [CBMSApplication]
GO
