
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
 cbmsPricePoint_Save  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType   Default  Description  
---------------------------------------------------------------  
@MyAccountId   int    
@price_point_id   int    
@market_index_id  int    
@price_point   varchar(200)    
@price_point_desc  varchar(1000) null    
@currency_unit_id  int   
@Price_Point_Short_Name nvarchar(255)   null                  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
---------------------------------------------------------------  
  
USAGE EXAMPLES:  
---------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
---------------------------------------------------------------  
 RKV   Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials Date  Modification  
---------------------------------------------------------------  
 RKV         02/10/2016 added comments and added new parameter @Price_Point_Short_Name as part of AS400-PII  
******/    
CREATE   PROCEDURE [dbo].[cbmsPricePoint_Save]
      ( 
       @MyAccountId INT
      ,@price_point_id INT
      ,@market_index_id INT
      ,@price_point VARCHAR(200)
      ,@price_point_desc VARCHAR(1000) = NULL
      ,@currency_unit_id INT
      ,@Price_Point_Short_Name NVARCHAR(255) = NULL
      ,@Commodity_Id INT = NULL
      ,@Volume_Unit_Id INT = NULL )
AS 
BEGIN    
    
      SET nocount ON    
    
      DECLARE
            @this_id INT
           ,@default_Commodity_Id INT     
    
      SET @this_id = @price_point_id    
    
      SELECT
            @default_Commodity_Id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Natural Gas'  
              
        
        
      IF @this_id IS NULL 
            BEGIN    
    
                  SELECT
                        @this_id = price_index_id
                  FROM
                        price_index
                  WHERE
                        index_id = @market_index_id
                        AND pricing_point = @price_point    
    
            END    
    
    
      IF @this_id IS NULL 
            BEGIN    
    
                  INSERT      INTO price_index
                              ( 
                               index_id
                              ,pricing_point
                              ,index_description
                              ,currency_unit_id
                              ,Price_Point_Short_Name
                              ,Commodity_Id
                              ,Volume_Unit_Id )
                  VALUES
                              ( 
                               @market_index_id
                              ,@price_point
                              ,@price_point_desc
                              ,@currency_unit_id
                              ,@Price_Point_Short_Name
                              ,ISNULL(@Commodity_Id, @default_Commodity_Id)
                              ,@Volume_Unit_Id )    
    
                  SET @this_id = @@IDENTITY    
    
            END    
      ELSE 
            BEGIN    
    
                  UPDATE
                        price_index
                  SET   
                        index_id = @market_index_id
                       ,pricing_point = @price_point
                       ,index_description = @price_point_desc
                       ,currency_unit_id = @currency_unit_id
                       ,Price_Point_Short_Name = @Price_Point_Short_Name
                       ,Commodity_Id = ISNULL(@Commodity_Id, Commodity_Id)
                       ,Volume_Unit_Id = ISNULL(@Volume_Unit_Id, Volume_Unit_Id)
                  WHERE
                        price_index_id = @this_id    
    
            END    
    
--- set nocount off    
    
      EXEC cbmsPricePoint_Get 
            @MyAccountId
           ,@this_id    
    
END    
    
SET QUOTED_IDENTIFIER ON  
  
;
GO

GRANT EXECUTE ON  [dbo].[cbmsPricePoint_Save] TO [CBMSApplication]
GO
