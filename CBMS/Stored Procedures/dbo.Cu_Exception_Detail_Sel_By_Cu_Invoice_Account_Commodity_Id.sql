SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Cu_Exception_Detail_Sel_By_Cu_Invoice_Account_Commodity_Id                     
                          
 DESCRIPTION:      
		  To get exception status based invoice and account and coommdity.                          
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @cu_invoice_id				         INT  
 @Account_Id						 INT
 @Commodity_Id						 INT    
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
               
 EXEC [Cu_Exception_Detail_Sel_By_Cu_Invoice_Account_Commodity_Id] 
      @cu_invoice_id = 25609530
     ,@Account_Id = 689131
     ,@Commodity_Id = 291  
	 
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2013-12-30      Created for MAINT-5844.                  
                         
******/   
  
 CREATE  PROCEDURE [dbo].[Cu_Exception_Detail_Sel_By_Cu_Invoice_Account_Commodity_Id]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT )
 AS 
 BEGIN      
      
      SELECT
            ced.CU_EXCEPTION_DETAIL_ID
           ,ced.CU_EXCEPTION_ID
           ,ced.EXCEPTION_STATUS_TYPE_ID
           ,ced.IS_CLOSED
           ,e.ENTITY_NAME AS Exception_Status_Type
      FROM
            CU_EXCEPTION ce
            LEFT JOIN CU_EXCEPTION_DETAIL ced
                  ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
            LEFT JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = ced.EXCEPTION_STATUS_TYPE_ID
                     AND e.ENTITY_TYPE = 703
      WHERE
            ce.CU_INVOICE_ID = @cu_invoice_id
            AND ced.Account_ID = @Account_Id
            AND ced.Commodity_Id = @Commodity_Id
            AND ced.IS_CLOSED = 0  
     
      
 END; 
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Detail_Sel_By_Cu_Invoice_Account_Commodity_Id] TO [CBMSApplication]
GO
