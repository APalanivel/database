SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_APP_MENU_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec dbo.GET_APP_MENU_INFO_P


CREATE          PROCEDURE dbo.GET_APP_MENU_INFO_P

AS
--script for fetching menu info from APP_MENU table
	SELECT	APP_MENU_ID, DISPLAY_TEXT, MENU_DESCRIPTION, 
		TARGET_ACTION, MENU_LEVEL, DISPLAY_ORDER, 
		PARENT_MENU_ID, APP_MENU.PERMISSION_INFO_ID, 
		PERMISSION_NAME, TARGET_SERVER 
	FROM 	APP_MENU, PERMISSION_INFO 
	WHERE 	PERMISSION_INFO.PERMISSION_INFO_ID = APP_MENU.PERMISSION_INFO_ID 
		AND APP_MENU_PROFILE_ID = 2
	ORDER BY DISPLAY_ORDER

/*
	--script for fetching menu info from MENU table
	SELECT	MENU_ID, DISPLAY_TEXT, MENU_DESCRIPTION, 
		TARGET_ACTION, MENU_LEVEL, DISPLAY_ORDER, 
		PARENT_MENU_ID, MENU.PERMISSION_INFO_ID, 
		PERMISSION_NAME
	FROM 	MENU, PERMISSION_INFO 
	WHERE 	PERMISSION_INFO.PERMISSION_INFO_ID = MENU.PERMISSION_INFO_ID 
	ORDER BY DISPLAY_ORDER
*/
GO
GRANT EXECUTE ON  [dbo].[GET_APP_MENU_INFO_P] TO [CBMSApplication]
GO
