SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsDataFeedModule_Sel]          
 (             
 @app_name varchar(100),          
 @module_name varchar(100)          
 )              
AS              
BEGIN              
 --'DataFeedMapping'          
           
           
 select Filter_Display_Name,c_filter.code_value,asf.Is_Default,asf.Is_Hidden,asf.Is_Smart_Search_Filter,asf.Display_Seq           
    ,asf.Data_Source_Query             
 from App_Global_Config_Search_Filter asf            
   inner join App_Global_Config agc on asf.App_Global_Config_Id = agc.App_Global_Config_Id            
   left join code c_app on c_app.code_id = agc.App_Module_Cd            
   left join code c_filter on c_filter.code_id = asf.Filter_Type_Cd            
 where c_app.code_value = @app_name            
 and agc.Config_Name = @module_name          
 and asf.Is_Active = 1          
            
 select Column_Display_Name,Data_Source_Column_Name,asf.Display_Seq,Date_Format,Column_Header_CSS,asf.Is_Active,    
   Data_Source_Query ,Is_Visible_To_User, c_sel.code_value AS [Selection_Filter_Type], Is_Editable, c_link.code_value AS [Link_Type]     
 from App_Global_Config_Output_Column asf            
   inner join App_Global_Config agc on asf.App_Global_Config_Id = agc.App_Global_Config_Id            
   left join code c_app on c_app.code_id = agc.App_Module_Cd                
   left join code c_sel on c_sel.code_id = asf.Selection_Filter_Type_Cd    
   left join code c_link on c_link.code_id = asf.Link_Type_CD   
 where c_app.code_value = @app_name            
 and agc.Config_Name = @module_name          
 and asf.Is_Active = 1          
 order by asf.Display_Seq  
  
END 

GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedModule_Sel] TO [CBMSApplication]
GO
