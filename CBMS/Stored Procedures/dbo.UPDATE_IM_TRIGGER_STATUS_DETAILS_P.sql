SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
CREATE        PROCEDURE dbo.UPDATE_IM_TRIGGER_STATUS_DETAILS_P  

@userId varchar(10),
@sessionId varchar(20),
@dealTicketDetailsId int, 
@cbmsImageId int, 
@triggerStatusType int, 
@triggerStatusName varchar(200), 
@isTriggerConfirmed bit

AS
	set nocount on
	IF @triggerStatusType > 0 
		BEGIN
			DECLARE @triggerStatusTypeId int
			SELECT 	@triggerStatusTypeId = ENTITY_ID FROM ENTITY 
			WHERE 	ENTITY_NAME = @triggerStatusName AND 
				ENTITY_TYPE = @triggerStatusType

			UPDATE 	RM_DEAL_TICKET_DETAILS 
			SET 	TRIGGER_STATUS_TYPE_ID = @triggerStatusTypeId
			WHERE 	RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
		END 
	ELSE 
		BEGIN
			IF @isTriggerConfirmed > 0 
				BEGIN
					UPDATE 
						RM_DEAL_TICKET_DETAILS 
					SET 
						CLIENT_CONFIRM_CBMS_IMAGE_ID = @cbmsImageId, 
						IS_TRIGGER_CONFIRMED = @isTriggerConfirmed
					WHERE 
						RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
				END
			ELSE
				BEGIN
					UPDATE 
						RM_DEAL_TICKET_DETAILS 
					SET 
						CP_CONFIRM_CBMS_IMAGE_ID = @cbmsImageId, 
						IS_TRIGGER_CONFIRMED = @isTriggerConfirmed
					WHERE 
						RM_DEAL_TICKET_DETAILS_ID = @dealTicketDetailsId
				END
		END

GO
GRANT EXECUTE ON  [dbo].[UPDATE_IM_TRIGGER_STATUS_DETAILS_P] TO [CBMSApplication]
GO
