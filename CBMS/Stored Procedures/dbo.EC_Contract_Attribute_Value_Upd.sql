SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.EC_Contract_Attribute_Value_Upd               
                  
Description:                  
        To update Data to EC_Contract_Attribute_Value table.                  
                  
 Input Parameters:                  
    Name								DataType			Default			Description                    
----------------------------------------------------------------------------------------      
    @EC_Contract_Attribute_Value_Id		INT    
    @EC_Contract_Attribute_Id			INT    
    @EC_Contract_Attribute_Value		NVARCHAR(510)    
    @User_Info_Id						INT    
            
 Output Parameters:                        
    Name								DataType			Default			Description                    
----------------------------------------------------------------------------------------      
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------                    
  BEGIN TRAN      
    DECLARE  @tvp_EC_Contract_Attribute_Value_insert tvp_EC_Contract_Attribute_Value
			, @tvp_EC_Contract_Attribute_Value_update tvp_EC_Contract_Attribute_Value  
			,@EC_Contract_Attribute_Value_id1 int,@EC_Contract_Attribute_Value_id2 int
			
	 INSERT @tvp_EC_Contract_Attribute_Value_insert      
	 SELECT NULL,'EC_Contract_Attribute_Value_testing1'     
	 UNION ALL      
	 SELECT NULL,'EC_Contract_Attribute_Value_testing2' 
    
	EXEC dbo.EC_Contract_Attribute_Value_Ins @tvp_EC_Contract_Attribute_Value = @tvp_EC_Contract_Attribute_Value_insert ,@EC_Contract_Attribute_Id = 7      
	,@User_Info_Id = 100    

    SELECT * FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value in ('EC_Contract_Attribute_Value_testing1','EC_Contract_Attribute_Value_testing2')    
    SELECT @EC_Contract_Attribute_Value_id1 = EC_Contract_Attribute_Value_id  FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value = 'EC_Contract_Attribute_Value_testing1'
    SELECT @EC_Contract_Attribute_Value_id2 = EC_Contract_Attribute_Value_id  FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value = 'EC_Contract_Attribute_Value_testing2'

	INSERT @tvp_EC_Contract_Attribute_Value_update      
	SELECT @EC_Contract_Attribute_Value_id1,'EC_Contract_Attribute_Value_testing_update1'     
	UNION ALL      
	SELECT @EC_Contract_Attribute_Value_id2,'EC_Contract_Attribute_Value_testing2_update2'  

	EXEC EC_Contract_Attribute_Value_Upd
    @tvp_EC_Contract_Attribute_Value = @tvp_EC_Contract_Attribute_Value_update
     ,@User_Info_Id = 100,@EC_Contract_Attribute_Id = 7  
     
    SELECT * FROM EC_Contract_Attribute_Value WHERE EC_Contract_Attribute_Value_id in (@EC_Contract_Attribute_Value_id1,@EC_Contract_Attribute_Value_id2)    
  ROLLBACK TRAN  
 
                 
Author Initials:                  
    Initials	Name                  
----------------------------------------------------------------------------------------                    
	NR			Narayana Reddy                   
 Modifications:                  
    Initials        Date		Modification                  
----------------------------------------------------------------------------------------                    
    NR				2015-04-22  Created For AS400.             
                 
******/     
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Value_Upd]
      ( 
       @tvp_EC_Contract_Attribute_Value tvp_EC_Contract_Attribute_Value READONLY
      ,@User_Info_Id INT
      ,@EC_Contract_Attribute_Id INT )
AS 
BEGIN    
      SET NOCOUNT ON     
      BEGIN TRY        
         
            BEGIN TRAN 
            
            
            
            UPDATE
                  ecav
            SET   
                  EC_Contract_Attribute_Value = teca.EC_Contract_Attribute_Value
                 ,Updated_User_Id = @User_Info_Id
                 ,Last_Change_Ts = GETDATE()
            FROM
                  @tvp_EC_Contract_Attribute_Value teca
                  INNER JOIN EC_Contract_Attribute_Value ecav
                        ON ecav.EC_Contract_Attribute_Value_Id = teca.EC_Contract_Attribute_Value_Id
            WHERE
                  teca.EC_Contract_Attribute_Value_Id IS NOT NULL 
                  
                  
            INSERT      INTO dbo.EC_Contract_Attribute_Value
                        ( 
                         EC_Contract_Attribute_Value
                        ,EC_Contract_Attribute_Id
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                        SELECT
                              EC_Contract_Attribute_Value
                             ,@EC_Contract_Attribute_Id
                             ,@User_Info_Id
                             ,GETDATE()
                             ,@User_Info_Id
                             ,GETDATE()
                        FROM
                              @tvp_EC_Contract_Attribute_Value
                        WHERE
                              EC_Contract_Attribute_Value_Id IS NULL 
                        
                        
            COMMIT TRAN        
      END TRY        
      BEGIN CATCH        
            IF @@ROWCOUNT > 0 
                  ROLLBACK TRAN   
                 
            EXEC usp_RethrowError        
      END CATCH            
         
END 
        

;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Value_Upd] TO [CBMSApplication]
GO
