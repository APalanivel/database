
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
            
/******                      
            
NAME: [DBO].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id_Account_Id]              
                 
DESCRIPTION:            
            
 To Get determinant category buckets by invoice and priority            
                  
INPUT PARAMETERS:                      
NAME					   DATATYPE DEFAULT  DESCRIPTION                      
------------------------------------------------------------                      
@Cu_Invoice_Id			   INT            
@Category_Bucket_Master_Id  INT
@Priority_Order		   INT 
@Account_Id			   INT
            
OUTPUT PARAMETERS:            
NAME   DATATYPE DEFAULT  DESCRIPTION            
------------------------------------------------------------                      

USAGE EXAMPLES:                      
------------------------------------------------------------              
            
	EXEC Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id_Account_Id  7530068, 101015,1,337994           
         
 SELECT TOP 20 * FROM CU_INVOICE_DETERMINANT ORDER BY Cu_invoice_Id dESC 
 
 SELECT * FROM cu_INvoice_Service_Month WHERE Cu_invoice_id = 7530068
 
 SELECT * FROM Bucket_Category_Rule r WHERE r.Category_Bucket_Master_Id IN 
 (SELECT Bucket_Master_Id FROM Bucket_Master bm WHERE bm.Commodity_id = 290 AND bm.Bucket_Type_Cd = 100262)
               SELECT * FROM Code WHERE code_id = 100261
 
           
AUTHOR INITIALS:                      
INITIALS	  NAME                      
------------------------------------------------------------                      
PKY		  Pavan K Yadalam 
RKV       Ravi Kumar Vegesna           
            
MODIFICATIONS            
INITIALS DATE		MODIFICATION            
------------------------------------------------------------                      
PKY	    25-JUL-11	CREATED for POC of New Cost/usage aggregation 
RKV     2016-10-04  Added two Parameters Country_Id and State_Id,also added new join 
					Country_State_Category_Bucket_Aggregation_Rule as part of PF Enhancement.          
            
*/            
              
CREATE PROCEDURE [dbo].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id_Account_Id]
      ( 
       @Cu_Invoice_Id INT
      ,@Category_Bucket_Master_Id INT
      ,@Priority_Order INT
      ,@Account_Id INT
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN                  
		
      SET NOCOUNT ON
          
      SELECT
            r.Category_Bucket_Master_Id
           ,CASE WHEN cscbar.Category_Bucket_Master_Id IS NULL THEN cd.Code_Value
                 ELSE 'CountryStateAggregationRule'
            END AS Aggregation_Type_Cd
           ,ISNULL(cscbar.Is_Aggregate_Category_Bucket, r.Is_Aggregate_Category_Bucket) Is_Aggregate_Category_Bucket
           ,r.Child_Bucket_Master_Id
           ,d.CU_INVOICE_DETERMINANT_ID
           ,d.COMMODITY_TYPE_ID
           ,d.UNIT_OF_MEASURE_TYPE_ID
           ,d.DETERMINANT_NAME
           ,ISNULL(CONVERT(VARCHAR(30), da.DETERMINANT_VALUE), d.DETERMINANT_VALUE) AS DETERMINANT_VALUE
           ,d.CU_DETERMINANT_NO
           ,d.CU_DETERMINANT_CODE
           ,d.Bucket_Master_Id
           ,da.Determinant_Expression
           ,e.ENTITY_NAME
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Code cd
                  ON r.Aggregation_Type_CD = cd.Code_Id
            INNER JOIN dbo.Code aglvlCd
                  ON aglvlCd.Code_id = r.CU_Aggregation_Level_Cd
            LEFT JOIN ( dbo.CU_INVOICE_DETERMINANT d
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                              ON d.CU_INVOICE_DETERMINANT_ID = da.CU_INVOICE_DETERMINANT_ID
                                 AND @Account_Id = da.ACCOUNT_ID
                        INNER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = d.UNIT_OF_MEASURE_TYPE_ID )
                        ON r.Child_Bucket_Master_Id = d.Bucket_Master_Id
                           AND d.CU_INVOICE_ID = @Cu_Invoice_Id
            LEFT OUTER JOIN ( dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
                              INNER JOIN code csalvcd
                                    ON csalvcd.Code_Id = cscbar.CU_Aggregation_Level_Cd
                                       AND csalvcd.Code_Value = 'Invoice' )
                              ON r.Category_Bucket_Master_Id = cscbar.Category_Bucket_Master_Id
                                 AND ( @Country_Id IS NULL
                                       OR cscbar.Country_Id = @Country_Id )
                                 AND ( @State_Id IS NULL
                                       OR cscbar.State_Id = @Country_Id )
                                 AND cscbar.Priority_Order = @Priority_Order
      WHERE
            aglvlCd.Code_Value = 'Invoice'
            AND r.Category_Bucket_Master_Id = @Category_Bucket_Master_Id
            AND @Priority_Order = ISNULL(cscbar.Priority_Order, r.Priority_Order)
                                    

END;
;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_Determinant_Category_Buckets_By_Priority_Invoice_Id_Account_Id] TO [CBMSApplication]
GO
