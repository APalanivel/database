SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_ACCOUNT_METER_MAP_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	
	@rfpAccountId  	int       	          	
	@meterId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_ACCOUNT_METER_MAP_DETAILS_P
@rfpId int,
@rfpAccountId int,
@meterId int

as
set nocount on
select	count(*) 

from	SR_RFP_ACCOUNT_METER_MAP map, SR_RFP_ACCOUNT rfpAccount

where	rfpAccount.SR_RFP_ACCOUNT_ID = map.SR_RFP_ACCOUNT_ID
	and map.SR_RFP_ACCOUNT_ID= @rfpAccountId
	and  map.METER_ID = @meterId
	and  rfpAccount.SR_RFP_ID= @rfpId
	and rfpAccount.is_deleted  = 0
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_METER_MAP_DETAILS_P] TO [CBMSApplication]
GO
