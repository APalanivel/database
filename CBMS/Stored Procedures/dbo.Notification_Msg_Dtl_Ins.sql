
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:   [dbo].[Notification_Msg_Dtl_Ins]  
             
DESCRIPTION:               
			To insert notification msg for each entity 
             
INPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
@Notification_Msg_Queue_Id		INT
@Recipient_User_Id				INT  
@Recipient_Email_Address		NVARCHAR(200)  
@Msg_Delivery_Status			VARCHAR(25)		'Pending'

 OUTPUT PARAMETERS:              
Name							DataType		Default		Description    
------------------------------------------------------------------------    
    
           
 USAGE EXAMPLES:              
------------------------------------------------------------------------

BEGIN TRAN  
	DECLARE @New_Notification_Msg_Queue_Id INT  
  
	EXEC dbo.Notification_Msg_Dtl_Ins   
		@Notification_Queue_Id = 1
		,@Recipient_User_Id = 16
		,@Recipient_Email_Address = 'rvintha@ctepl.com'  

ROLLBACK  
  
 AUTHOR INITIALS:              
	Initials	Name              
------------------------------------------------------------------------
	RR			Raghu Reddy  
	NR			Narayana Reddy
             
 MODIFICATIONS:               
	Initials	Date		Modification              
------------------------------------------------------------------------  
	RR			2014-01-23  Created
	NR			2017-01-11	Added CC_email_Address For Invocie collection Project.

******/

CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Ins]
      ( 
       @Notification_Msg_Queue_Id INT
      ,@Recipient_User_Id INT
      ,@Recipient_Email_Address NVARCHAR(200)
      ,@Msg_Delivery_Status VARCHAR(25) = 'Pending'
      ,@CC_Email_Address NVARCHAR(MAX) = NULL )
AS 
BEGIN  
      SET NOCOUNT ON;

      DECLARE
            @Notification_Template_Id INT
           ,@Msg_Delivery_Status_Cd INT

      SELECT
            @Msg_Delivery_Status_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            INNER JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = @Msg_Delivery_Status
            AND cs.Codeset_Name = 'Message Delivery Status'

     
      BEGIN TRY  
            BEGIN TRAN 
    
            INSERT      INTO dbo.Notification_Msg_Dtl
                        ( 
                         Notification_Msg_Queue_Id
                        ,Recipient_User_Id
                        ,Recipient_Email_Address
                        ,Msg_Delivery_Status_Cd
                        ,Created_Ts
                        ,Last_Change_Ts
                        ,CC_Email_Address )
            VALUES
                        ( 
                         @Notification_Msg_Queue_Id
                        ,@Recipient_User_Id
                        ,@Recipient_Email_Address
                        ,@Msg_Delivery_Status_Cd
                        ,GETDATE()
                        ,GETDATE()
                        ,@CC_Email_Address )

            COMMIT  
      END TRY  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  ROLLBACK  
            EXEC dbo.usp_RethrowError   
   
      END CATCH  
   
END;

;
;
GO

GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Ins] TO [CBMSApplication]
GO
