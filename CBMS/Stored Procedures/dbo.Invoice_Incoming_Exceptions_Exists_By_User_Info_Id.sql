SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.Invoice_Incoming_Exceptions_Exists_By_User_Info_Id

DESCRIPTION:
    

INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC DBO.Invoice_Incoming_Exceptions_Exists_By_User_Info_Id 49
	EXEC DBO.Invoice_Incoming_Exceptions_Exists_By_User_Info_Id 5150
	EXEC DBO.Invoice_Incoming_Exceptions_Exists_By_User_Info_Id 45188
	EXEC DBO.Invoice_Incoming_Exceptions_Exists_By_User_Info_Id 45229

	SELECT * FROM dbo.USER_INFO WHERE USER_INFO_ID = 5150
	SELECT * FROM dbo.CU_EXCEPTION_DENORM WHERE QUEUE_ID = 5208
	

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	Created MAINT-3304 Move User to History CBMS enhancement


******/

CREATE PROCEDURE [dbo].[Invoice_Incoming_Exceptions_Exists_By_User_Info_Id] 
( @User_Info_Id AS INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Is_Exists BIT = 0
    
      SELECT
            @Is_Exists = 1
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @User_Info_Id
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.CU_EXCEPTION_DENORM ced
                         WHERE
                              ced.QUEUE_ID = ui.QUEUE_ID ) 
      SELECT
            @Is_Exists AS Is_Exists         
END

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Incoming_Exceptions_Exists_By_User_Info_Id] TO [CBMSApplication]
GO
