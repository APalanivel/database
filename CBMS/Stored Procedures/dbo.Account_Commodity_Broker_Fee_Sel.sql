SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
      dbo.Account_Commodity_Broker_Fee_Sel
   
 DESCRIPTION:   
      Gets the Broker Fee for a given Account.
      
 INPUT PARAMETERS:  
 Name			DataType	Default Description  
------------------------------------------------------------  
 @Account_Id      INT     

 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.Account_Commodity_Broker_Fee_Sel 10010
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------ 
 AKR		Ashok Kumar Raju
 

 MODIFICATIONS   
 Initials	Date		Modification
------------------------------------------------------------
 AKR        2012-09-28  Created for POCO

******/
CREATE  PROCEDURE dbo.Account_Commodity_Broker_Fee_Sel ( @Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
      
      SELECT
            acbf.Account_Id
           ,acbf.Commodity_Id
           ,acbf.Broker_Fee
           ,acbf.Currency_Unit_Id
           ,acbf.UOM_Entity_Id
      FROM
            dbo.Account_Commodity_Broker_Fee acbf
      WHERE
            acbf.Account_Id = @Account_Id
      
    
	  
END
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Broker_Fee_Sel] TO [CBMSApplication]
GO
