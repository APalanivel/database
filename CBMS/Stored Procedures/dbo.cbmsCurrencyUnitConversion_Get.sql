SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCurrencyUnitConversion_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@CurrencyGroupId	int       	          	
	@BaseUnitId    	int       	          	
	@ConvertedUnitId	int       	          	
	@Year          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsCurrencyUnitConversion_Get]
( 
	@MyAccountId int 
	,@CurrencyGroupId int
	,@BaseUnitId int
	,@ConvertedUnitId int
	,@Year int
)
AS
BEGIN
	declare @beginDate datetime
	declare @endDate datetime

	set @beginDate = '1/1/' + cast(@year as varchar(4))
	set @endDate = dateadd(year,1,@begindate)

	select conversion_factor
		,conversion_date
	from currency_unit_conversion
	where currency_group_id = @CurrencyGroupId
	and base_unit_id = @BaseUnitId
	and converted_unit_id = @ConvertedUnitId
	and conversion_date >= @beginDate
	and conversion_date < @endDate
	order by conversion_date asc
END
GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnitConversion_Get] TO [CBMSApplication]
GO
