SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Archive_Not_Managed_Account_Invoice_Collection_Queue
                
Description:                
   This sproc is used to update the status of ICQ to archive when the account is moved to not managed.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  exec dbo.Archive_Not_Managed_Account_Invoice_Collection_Queue

Author Initials:                
    Initials	Name                
--------------------------------------------------------------------------------------                  
	RKV			Ravi Kumar Vegesna  
 
 Modifications:                
    Initials        Date		Modification                
--------------------------------------------------------------------------------------                  
    RKV				2018-01-03  Created For SE2017-389.
   
               
******/   
CREATE PROCEDURE [dbo].[Archive_Not_Managed_Account_Invoice_Collection_Queue]
AS
BEGIN 

      SET NOCOUNT ON;

      CREATE TABLE #Invoice_Collection_Queue_Id
            (
             Invoice_Collection_Queue_Id INT
            ,Row_Num INT IDENTITY(1, 1) )

      CREATE TABLE #Not_Managed_Account_IC_Config_Id
            (
             Invoice_Collection_Account_Config_Id INT PRIMARY KEY CLUSTERED )

      DECLARE
            @Icr_Archive_Cd INT
           ,@Ice_Archive_Cd INT
           ,@ICR_Open_Status_Cd INT
           ,@ICE_Open_Status_Cd INT
           ,@Total_Rows INT
           ,@Current_Row_Num INT = 1
           ,@Invoice_Collection_Queue_Id INT
           ,@System_User_Id INT

      SELECT
            @ICR_Open_Status_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Open'
            AND cs.Codeset_Name = 'ICR Status';
 
      SELECT
            @ICE_Open_Status_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Open'
            AND cs.Codeset_Name = 'ICE Status';

      INSERT      INTO #Not_Managed_Account_IC_Config_Id
                  (Invoice_Collection_Account_Config_Id )
                  SELECT
                        iac.Invoice_Collection_Account_Config_Id
                  FROM
                        dbo.Invoice_Collection_Account_Config iac
                        INNER JOIN dbo.ACCOUNT ac
                              ON ac.ACCOUNT_ID = iac.Account_Id
                  WHERE
                        ac.NOT_MANAGED = 1

      INSERT      INTO #Invoice_Collection_Queue_Id
                  (Invoice_Collection_Queue_Id )
                  SELECT
                        icq.Invoice_Collection_Queue_Id
                  FROM
                        #Not_Managed_Account_IC_Config_Id ia
                        INNER JOIN dbo.Invoice_Collection_Queue icq
                              ON icq.Invoice_Collection_Account_Config_Id = ia.Invoice_Collection_Account_Config_Id
                  WHERE
                        icq.Status_Cd IN ( @ICE_Open_Status_Cd, @ICR_Open_Status_Cd )
      SELECT
            @Total_Rows = @@ROWCOUNT

      SELECT
            @Icr_Archive_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICR Status';
 
      SELECT
            @Ice_Archive_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Archived'
            AND cs.Codeset_Name = 'ICE Status';

      SELECT
            @System_User_Id = USER_INFO_ID
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = 'conversion'
		
		
      WHILE @Total_Rows >= @Current_Row_Num
            BEGIN
                  BEGIN TRY

                        SELECT
                              @Invoice_Collection_Queue_Id = Invoice_Collection_Queue_Id
                        FROM
                              #Invoice_Collection_Queue_Id
                        WHERE
                              Row_Num = @Current_Row_Num
   
                        UPDATE
                              icq
                        SET
                              Status_Cd = CASE WHEN icqt.Code_Value = 'ICR' THEN @Icr_Archive_Cd
                                               ELSE @Ice_Archive_Cd
                                          END
                             ,icq.Updated_User_Id = @System_User_Id
                             ,icq.Last_Change_Ts = GETDATE()
                        FROM
                              dbo.Invoice_Collection_Queue icq
                              INNER JOIN dbo.Code icqt
                                    ON icqt.Code_Id = icq.Invoice_Collection_Queue_Type_Cd
                        WHERE
                              icq.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id

                  END TRY

                  BEGIN CATCH
                        EXEC dbo.usp_RethrowError
                              @CustomMessage = '' -- varchar(2500)

                  END CATCH

                  SET @Current_Row_Num = @Current_Row_Num + 1

            END
            
      DROP TABLE #Invoice_Collection_Queue_Id
      DROP TABLE #Not_Managed_Account_IC_Config_Id

END

;
GO
GRANT EXECUTE ON  [dbo].[Archive_Not_Managed_Account_Invoice_Collection_Queue] TO [CBMSApplication]
GO
