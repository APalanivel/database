SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_Cancel_Renegotiation_Document_Del]  
     
DESCRIPTION: 
	It Deletes SR Cancel Renegotiation Documents for Selected Sr_Cancel_Renegotiation_Doc_Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
@Sr_Cancel_Renegotiation_Document_Id	INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN  
		EXEC SR_Cancel_Renegotiation_Document_Del  6383
	ROLLBACK TRAN
	
	SELECT * FROM SR_CANCEL_RENEGOTIATION_DOCUMENT
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			27-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_Cancel_Renegotiation_Document_Del
   (
    @Sr_Cancel_Renegotiation_Document_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT
	WHERE
		SR_CANCEL_RENEGOTIATION_DOCUMENT_ID = @Sr_Cancel_Renegotiation_Document_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_Cancel_Renegotiation_Document_Del] TO [CBMSApplication]
GO
