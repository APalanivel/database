SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE dbo.DELTOOL_METER_GETFORACCOUNT_P
	( @MyAccountId int
	, @account_id int
	, @site_id int = null
	)
AS
BEGIN

	   select m.meter_id
		, m.rate_id
		, r.rate_name
		, r.commodity_type_id
		, com.entity_name commodity_type
		, u.vendor_id utility_id
		, u.vendor_name utility_name
		, m.purchase_method_type_id
		, pm.entity_name purchase_method_type
		, m.account_id utility_account_id
		, ua.account_number utility_account_number
		, m.address_id
		, ad.address_line1
		, ad.address_line2
		, ad.city
		, st.state_id
		, st.state_name
		, m.meter_number
		, m.tax_exempt_status
		, m.is_history
		, s.site_id
		, s.site_name
	     from meter m
	     join vwAccountMeter am on am.meter_id = m.meter_id
	     join rate r on r.rate_id = m.rate_id
	     join entity com on com.entity_id = r.commodity_type_id
	     join vendor u on u.vendor_id = r.vendor_id
	     join entity pm on pm.entity_id = m.purchase_method_type_id
	     join account ua on ua.account_id = m.account_id
	     join address ad on ad.address_id = m.address_id
	     join state st on st.state_id = ad.state_id
	     join site s on s.site_id = am.site_id
	    where am.account_id = @account_id
	      and s.site_id = isNull(@site_id, s.site_id)
	      and (s.not_managed = 0 or s.not_managed is null)

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_METER_GETFORACCOUNT_P] TO [CBMSApplication]
GO
