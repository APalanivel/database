SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME: dbo.[Cbms_Image_Sel_By_Rate_Id]  
  
DESCRIPTION:  
  
 Select all the cbms image associated with the given rate id  
  
INPUT PARAMETERS:  
      Name                  DataType          Default     Description  
------------------------------------------------------------------  
      
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.Cbms_Image_Sel_By_Rate_Id @Rate_Id = 7423  
 EXEC dbo.Cbms_Image_Sel_By_Rate_Id @Rate_Id = 710  
   
 EXEC dbo.Cbms_Image_Sel_By_Rate_Id @Rate_Id = 710, @Rate_Schedule_id = 14921  
   
 SELECT * FROM Rate_schedule WHERE rate_id = 710  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 HG   Harihara Suthan G  
  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 HG   05/25/2011 Created to fix MAINT-488  
 HG   05/30/2011 Script modified to have @Rate_Schedule_id as a optional parameter.  
        As we have a option to delete rate schedule seperately added this parameter to fetch only the images associated with the given Rate schedule id.  
  
******/  
  
CREATE PROCEDURE dbo.Cbms_Image_Sel_By_Rate_Id
    @Rate_Id INT ,
    @Rate_Schedule_Id INT = NULL
AS 
    BEGIN  
  
        SET NOCOUNT ON ;  
   
        SELECT  ci.cbms_image_id
        FROM    dbo.Cbms_Image ci
                INNER JOIN dbo.Rate_Schedule rs ON rs.cbms_image_id = ci.cbms_image_id
        WHERE   rs.Rate_Id = @Rate_Id
                AND ( @Rate_Schedule_Id IS NULL
                      OR rs.RATE_SCHEDULE_ID = @Rate_Schedule_Id
                    )

END
GO

GRANT EXECUTE ON  [dbo].[Cbms_Image_Sel_By_Rate_Id] TO [CBMSApplication]
GO