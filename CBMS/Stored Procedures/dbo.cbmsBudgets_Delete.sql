SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    procedure [dbo].[cbmsBudgets_Delete]
	( @MyAccountId int
	, @budget_id int
	)
AS
BEGIN
	set nocount on

		delete from budget_detail where budget_id = @budget_id
	
		delete from budgets where budget_id = @budget_id
		
	set nocount off


END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_Delete] TO [CBMSApplication]
GO
