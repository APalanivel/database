SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   
    dbo.Report_DE_CEM_Sourcing_Analyst

DESCRIPTION:
	This procedure is to get All CEM and Sourcing analyst list available in CBMS

INPUT PARAMETERS:          
Name			 DataType	Default	Description          
------------------------------------------------------------          

              
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

Exec dbo.Cost_Usage_Account_Billing_Dtl_Sel 1,'2010-01-01'

AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
HG			Hariharasuthan Ganesan
  
MODIFICATIONS           
Initials	Date		    Modification          
------------------------------------------------------------          
HG			2016-03-08		Created for DDCR-16
*****/ 
CREATE PROCEDURE [dbo].[Report_DE_CEM_Sourcing_Analyst]
AS
BEGIN
      SET NOCOUNT ON;
	 
      DECLARE @CEM TABLE
            (
             Row_Num INT IDENTITY(1, 1)
                         PRIMARY KEY CLUSTERED
            ,CEM_CEA VARCHAR(100)
            ,User_Info_id INT );

      DECLARE @SA TABLE
            (
             Row_Num INT IDENTITY(1, 1)
                         PRIMARY KEY CLUSTERED
            ,Summit_Analysts VARCHAR(100)
            ,User_Info_id INT );

      INSERT      INTO @CEM
                  (CEM_CEA
                  ,User_Info_id )
                  SELECT
                        ( FIRST_NAME + ' ' + LAST_NAME ) AS [CEM/CEA List]
                       ,useri.USER_INFO_ID
                  FROM
                        CBMS.dbo.USER_INFO useri
                        INNER JOIN CBMS.dbo.RM_CEM_TEAM RCT
                              ON RCT.CEM_USER_ID = useri.USER_INFO_ID
                  WHERE
                        useri.EMAIL_ADDRESS LIKE '%Schneider%'
                  GROUP BY
                        ( FIRST_NAME + ' ' + LAST_NAME )
                       ,useri.USER_INFO_ID
                  ORDER BY
                        ( FIRST_NAME + ' ' + LAST_NAME );
      INSERT      INTO @SA
                  (Summit_Analysts
                  ,User_Info_id )
                  SELECT
                        ( FIRST_NAME + ' ' + LAST_NAME ) AS [Summit Analysts]
                       ,useri.USER_INFO_ID
                  FROM
                        CBMS.dbo.USER_INFO useri
                        INNER JOIN CBMS.dbo.SR_RFP rfp
                              ON rfp.INITIATED_BY = useri.USER_INFO_ID
                  WHERE
                        useri.EMAIL_ADDRESS LIKE '%Schneider%'
                  GROUP BY
                        FIRST_NAME + ' ' + LAST_NAME
                       ,useri.USER_INFO_ID
                  ORDER BY
                        ( FIRST_NAME + ' ' + LAST_NAME );
      SELECT
            c.CEM_CEA AS [CEM/CEA List]
           ,sa.Summit_Analysts AS [Summit Analysts]
      FROM
            @CEM c
            FULL OUTER JOIN @SA sa
                  ON sa.Row_Num = c.Row_Num
      ORDER BY
            ISNULL(c.Row_Num, sa.Row_Num);
END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_CEM_Sourcing_Analyst] TO [CBMSApplication]
GO
