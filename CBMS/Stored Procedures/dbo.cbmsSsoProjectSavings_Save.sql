SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure [dbo].[cbmsSsoProjectSavings_Save]
	( @MyAccountId int
	, @project_id int
	, @savings_id int
	)
AS
BEGIN

	set nocount on

	declare @RecordCount int

	  select @RecordCount = count(*)
	    from sso_project_savings_map
	   where sso_project_id = @project_id
	     and sso_savings_id = @savings_id

	if @RecordCount = 0
	begin

		insert into sso_project_savings_map
			(sso_project_id
			, sso_savings_id			
			)
		values
			(@project_id
			,@savings_id
			)
	end


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectSavings_Save] TO [CBMSApplication]
GO
