SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Ec_Account_Group_Type_Ins        
                
Description:                
		This sproc to dto save the group types.        
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Country_Id					INT  
	@State_Id					INT
	@Commodity_Id				INT
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  DECLARE  @tvp_Ec_Account_Group_Type dbo.tvp_Ec_Account_Group_Type
	INSERT @tvp_Ec_Account_Group_Type
		SELECT NULL,'Group one' 
		UNION ALL  
		SELECT NULL,'Group Two' 
  
  BEGIN TRAN
  
 SELECT * FROM dbo.Ec_Account_Group_Type eagt WHERE Group_Type_Name in ('Group one','Group Two')
  EXEC dbo.Ec_Account_Group_Type_Ins 
      @State_Id=1
     ,@Commodity_Id=290
     ,@tvp_Ec_Account_Group_Type = @tvp_Ec_Account_Group_Type
     ,@User_Info_Id=49
 SELECT * FROM dbo.Ec_Account_Group_Type eagt WHERE Group_Type_Name in ('Group one','Group Two')

  ROLLBACK TRAN
     
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Ec_Account_Group_Type_Ins]
      ( 
       @State_Id INT
      ,@Commodity_Id INT
      ,@tvp_Ec_Account_Group_Type tvp_Ec_Account_Group_Type READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      
      INSERT      INTO dbo.Ec_Account_Group_Type
                  ( 
                   State_Id
                  ,Commodity_Id
                  ,Group_Type_Name
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        @State_Id
                       ,@Commodity_Id
                       ,gtn.Group_Type_Name
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        @tvp_Ec_Account_Group_Type gtn
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Ec_Account_Group_Type eagt
                                     WHERE
                                          State_Id = @State_Id
                                          AND Commodity_Id = @Commodity_Id
                                          AND eagt.Group_Type_Name = gtn.Group_Type_Name )
END
      


;
GO
GRANT EXECUTE ON  [dbo].[Ec_Account_Group_Type_Ins] TO [CBMSApplication]
GO
