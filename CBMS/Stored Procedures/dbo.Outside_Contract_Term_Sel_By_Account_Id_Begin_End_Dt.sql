SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Outside_Contract_Term_Sel_By_Account_Id_Begin_End_Dt              
                          
 DESCRIPTION:                          
   To get Supplier Account Details                         
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
@Account_Id      INT  
@Start_Dt      Date  
@End_Dt       Date                           
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
                          
 USAGE EXAMPLES:                              
------------------------------------------------------------------------------       
  
EXEC dbo.Outside_Contract_Term_Sel_By_Account_Id_Begin_End_Dt  
    @Account_Id = 1741063  
    , @Start_Dt = '2019-08-20'  
    , @End_Dt = '2019-09-10'  
  
  
   
SELECT  
    cha.Account_Id  
    , cha.Supplier_Account_begin_Dt  
    , cha.Supplier_Account_End_Dt  
 ,cha.Supplier_Contract_ID  
FROM  
    Core.Client_Hier_Account cha  
WHERE  
    cha.Account_Id = 1741063;  
  
  
  
  
 AUTHOR INITIALS:          
         
 Initials              Name          
------------------------------------------------------------------------------       
 NR      Narayana Reddy  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
------------------------------------------------------------------------------       
 NR                    2019-05-29       Created for - Add Contract.  
                         
******/

CREATE PROCEDURE [dbo].[Outside_Contract_Term_Sel_By_Account_Id_Begin_End_Dt]
    (
        @Account_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
    )
AS
    BEGIN
        SET NOCOUNT ON;



        SELECT
            cha.Account_Id
            , cha.Supplier_Contract_ID
            , CASE WHEN aoctc.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_OCT_Rule_Exists
        FROM
            Core.Client_Hier_Account cha
            LEFT JOIN dbo.Account_Outside_Contract_Term_Config aoctc
                ON aoctc.Account_Id = cha.Account_Id
                   AND  cha.Supplier_Contract_ID = aoctc.Contract_Id
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID <> -1
            AND (   @Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                              AND     cha.Supplier_Account_End_Dt
                    OR  @End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                AND     cha.Supplier_Account_End_Dt
                    OR  cha.Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                    OR  cha.Supplier_Account_End_Dt BETWEEN @Start_Dt
                                                    AND     @End_Dt)
            AND (   cha.Supplier_Account_begin_Dt > @Start_Dt
                    OR  cha.Supplier_Account_End_Dt < @End_Dt)
        GROUP BY
            cha.Account_Id
            , cha.Supplier_Contract_ID
            , CASE WHEN aoctc.Account_Outside_Contract_Term_Config_Id IS NOT NULL THEN 1
                  ELSE 0
              END;




    END;


GO
GRANT EXECUTE ON  [dbo].[Outside_Contract_Term_Sel_By_Account_Id_Begin_End_Dt] TO [CBMSApplication]
GO
