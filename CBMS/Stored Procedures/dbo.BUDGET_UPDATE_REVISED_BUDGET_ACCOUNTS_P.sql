SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- exec dbo.BUDGET_UPDATE_REVISED_BUDGET_ACCOUNTS_P -1,-1,136,56
-- select * from budget_account

create  PROCEDURE dbo.BUDGET_UPDATE_REVISED_BUDGET_ACCOUNTS_P
@user_id varchar(10),
@session_id varchar(20),
@accountId int,
@budgetId int


AS
begin
set nocount on

	
	update 	BUDGET_ACCOUNT 
	set 	IS_REFORECAST = 1
	where 	ACCOUNT_ID = @accountId  
		and BUDGET_ID = @budgetId

	
				
end












GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_REVISED_BUDGET_ACCOUNTS_P] TO [CBMSApplication]
GO
