SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
NAME:                                  
    dbo.Contract_Sel_By_Commodity_Hedge_Type_Period                                  
                                  
DESCRIPTION:                                  
                             
                                  
INPUT PARAMETERS:                                  
 Name    DataType  Default Description                                  
---------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Start_Dt   DATE  
    @End_Dt    DATE  
    @Hedge_Type   INT  
                            
                         
OUTPUT PARAMETERS:                                  
 Name   DataType  Default Description                                  
---------------------------------------------------------------      
  
                               
 EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 11236  
  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2019-08-01',@End_Dt='2019-12-30',@Hedge_Type=586  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-12-30',@Hedge_Type=586  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-03-30',@Hedge_Type=586  
   
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2019-01-01',@End_Dt='2019-01-01',@Hedge_Type=586,@Cbms_Sitegroup_Ids='739'  
                  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RR   Raghu Reddy  
                     
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 RR   2018-10-25  Created For Risk Management  
  
******/
CREATE PROCEDURE [dbo].[Contract_Sel_By_Commodity_Hedge_Type_Period]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @Start_Dt DATE = '2018-01-01',
    @End_Dt DATE = '2018-12-30',
    @Hedge_Type INT,
    @Site_Ids VARCHAR(MAX) = NULL,
    @Sitegroup_Ids VARCHAR(MAX) = NULL,
    @Cbms_Sitegroup_Ids VARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;


    DECLARE @Hedge_Type_Input VARCHAR(200);

    SELECT @Hedge_Type_Input = ENTITY_NAME
    FROM dbo.ENTITY
    WHERE ENTITY_ID = @Hedge_Type;

    SELECT con.CONTRACT_ID,
           con.ED_CONTRACT_NUMBER + ' (' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
           + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
           + cha.Account_Vendor_Name AS Contract
    FROM Core.Client_Hier ch
        INNER JOIN Core.Client_Hier_Account cha
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN dbo.CONTRACT con
            ON con.CONTRACT_ID = cha.Supplier_Contract_ID
    WHERE ch.Client_Id = @Client_Id
          AND con.COMMODITY_TYPE_ID = @Commodity_Id
          AND
          (
              con.CONTRACT_START_DATE
          BETWEEN @Start_Dt AND @End_Dt
              OR con.CONTRACT_END_DATE
          BETWEEN @Start_Dt AND @End_Dt
              OR @Start_Dt
          BETWEEN con.CONTRACT_START_DATE AND con.CONTRACT_END_DATE
              OR @End_Dt
          BETWEEN con.CONTRACT_START_DATE AND con.CONTRACT_END_DATE
          )
          AND
          (
              EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard siteob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
        WHERE siteob.Client_Hier_Id = ch.Client_Hier_Id
              AND siteob.Commodity_Id = @Commodity_Id
              AND
              (
                  (
                      @Hedge_Type_Input = 'Physical'
                      AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                  )
                  OR
                  (
                      @Hedge_Type_Input = 'Financial'
                      AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
                  )
              )
              AND
              (
                  chhc.Config_Start_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR chhc.Config_End_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR @Start_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
                  OR @End_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              )
    )
              OR EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard clntob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier clch
                ON clntob.Client_Hier_Id = clch.Client_Hier_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
        WHERE clch.Sitegroup_Id = 0
              AND clch.Client_Id = ch.Client_Id
              AND clntob.Country_Id = ch.Country_Id
              AND clntob.Commodity_Id = @Commodity_Id
              AND
              (
                  (
                      @Hedge_Type_Input = 'Physical'
                      AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                  )
                  OR
                  (
                      @Hedge_Type_Input = 'Financial'
                      AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
                  )
              )
              AND
              (
                  chhc.Config_Start_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR chhc.Config_End_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR @Start_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
                  OR @End_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              )
              AND NOT EXISTS
        (
            SELECT 1
            FROM Trade.RM_Client_Hier_Onboard siteob1
            WHERE siteob1.Client_Hier_Id = ch.Client_Hier_Id
                  AND siteob1.Commodity_Id = @Commodity_Id
        )
    )
          )
          AND ch.Site_Id > 0
          AND
          (
              @Site_Ids IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Site_Ids, ',')
        WHERE CAST(Segments AS INT) = ch.Site_Id
    )
          )
          AND
          (
              @Sitegroup_Ids IS NULL
              OR EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@Sitegroup_Ids, ',')
        WHERE CAST(Segments AS INT) = ch.Sitegroup_Id
    )
          )
          AND
          (
              @Cbms_Sitegroup_Ids IS NULL
              OR
              (
                  EXISTS
    (
        SELECT 1
        FROM dbo.Cbms_Sitegroup_Participant csp
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = csp.Group_Participant_Type_Cd
            INNER JOIN dbo.ufn_split(@Cbms_Sitegroup_Ids, ',')
                ON csp.Cbms_Sitegroup_Id = CAST(Segments AS INT)
        WHERE cd.Code_Value = 'Contract'
              AND csp.Group_Participant_Id = cha.Supplier_Contract_ID
    )
                  OR EXISTS
    (
        SELECT 1
        FROM dbo.Cbms_Sitegroup_Participant csp
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = csp.Group_Participant_Type_Cd
            INNER JOIN dbo.ufn_split(@Cbms_Sitegroup_Ids, ',')
                ON csp.Cbms_Sitegroup_Id = CAST(Segments AS INT)
        WHERE cd.Code_Value = 'Site'
              AND csp.Group_Participant_Id = ch.Site_Id
    )
              )
          )
    GROUP BY con.CONTRACT_ID,
             con.ED_CONTRACT_NUMBER + ' (' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
             + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
             + cha.Account_Vendor_Name;




END;
GO
GRANT EXECUTE ON  [dbo].[Contract_Sel_By_Commodity_Hedge_Type_Period] TO [CBMSApplication]
GO
