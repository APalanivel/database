SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.RM_Client_Default_Hedge_Config_Del                    
                      
Description:                      
        To delete a hedge configuration
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Hedge_Config_Id	INT
    
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	@RM_Group_Id	INT  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	BEGIN TRANSACTION
		EXEC Trade.RM_Client_Default_Hedge_Config_Del 235,291,'4','2018-01-01'
		
	ROLLBACK TRANSACTION     
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-08-23     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [Trade].[RM_Client_Default_Hedge_Config_Del]
    (
        @RM_Client_Hier_Hedge_Config_Id INT
        , @RM_Client_Hier_Onboard_Id INT
        , @User_Info_Id INT
        , @RM_Default_Config_Start_Dt DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Does_not_hedge_Entity INT
            , @RM_Risk_Tolerance_Category_Undefined INT
            , @RM_Default_Config_End_Dt DATE;

        --SELECT
        --      @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
        --FROM
        --      dbo.App_Config
        --WHERE
        --      App_Config_Cd = 'RM_Default_Config_Start_Dt'
        --      AND App_Id = -1

        SELECT
            @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_End_Dt'
            AND App_Id = -1;

        SELECT
            @Does_not_hedge_Entity = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 273
            AND ENTITY_NAME = 'Does not hedge';


        SELECT
            @RM_Risk_Tolerance_Category_Undefined = RM_Risk_Tolerance_Category_Id
        FROM
            Trade.RM_Risk_Tolerance_Category
        WHERE
            RM_Risk_Tolerance_Category = 'Undefined';

        DELETE
        chhc
        FROM
            Trade.RM_Client_Hier_Hedge_Config chhc
        WHERE
            chhc.RM_Client_Hier_Hedge_Config_Id = @RM_Client_Hier_Hedge_Config_Id
            AND chhc.RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id;


        INSERT INTO Trade.RM_Client_Hier_Hedge_Config
             (
                 RM_Client_Hier_Onboard_Id
                 , Config_Start_Dt
                 , Config_End_Dt
                    --,RM_Forecast_UOM_Type_Id
                 , Hedge_Type_Id
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , Risk_Manager_User_Info_Id
                 , Contact_Info_Id
                 , Include_In_Reports
                 , Is_Default_Config
                 , Created_User_Id
                 , Created_Ts
                 , Last_Updated_By
                 , Last_Updated_Ts
             )
        SELECT
            @RM_Client_Hier_Onboard_Id
            , @RM_Default_Config_Start_Dt AS Config_Start_Dt
            , @RM_Default_Config_End_Dt AS Config_End_Dt
            --,NULL AS RM_Forecast_UOM_Type_Id
            , @Does_not_hedge_Entity AS Hedge_Type_Id
            , NULL AS Max_Hedge_Pct
            , @RM_Risk_Tolerance_Category_Undefined AS RM_Risk_Tolerance_Category_Id
            , NULL AS Risk_Manager_User_Info_Id
            , NULL AS Contact_Info_Id
            , 0 AS Include_In_Reports
            , 1 AS Is_Default_Config
            , @User_Info_Id AS Created_By
            , GETDATE() AS Created_Ts
            , @User_Info_Id AS Last_Updated_By
            , GETDATE() AS Last_Updated_Ts
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Client_Hier_Hedge_Config chhc1
                           WHERE
                                chhc1.RM_Client_Hier_Onboard_Id = @RM_Client_Hier_Onboard_Id);

    END;

GO
GRANT EXECUTE ON  [Trade].[RM_Client_Default_Hedge_Config_Del] TO [CBMSApplication]
GO
