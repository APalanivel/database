SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec dbo.GET_SUPPLIER_ACCOUNT_GROUP_COUNT_P 27

CREATE     PROCEDURE dbo.GET_SUPPLIER_ACCOUNT_GROUP_COUNT_P
	@site_id int
	AS
	begin
		set nocount on

		select count(accountGroup.ACCOUNT_GROUP_ID)
		FROM	account utilacc 
			join meter met on met.account_id = utilacc.account_id
			and utilacc.site_id = @site_id 
			join supplier_account_meter_map map on map.meter_id =  met.meter_id 
			and map.is_history<>1
			join account suppacc on suppacc.account_id = map.account_id
			join ACCOUNT_GROUP accountGroup on accountGroup.ACCOUNT_GROUP_ID = suppacc.ACCOUNT_GROUP_ID
	end
GO
GRANT EXECUTE ON  [dbo].[GET_SUPPLIER_ACCOUNT_GROUP_COUNT_P] TO [CBMSApplication]
GO
