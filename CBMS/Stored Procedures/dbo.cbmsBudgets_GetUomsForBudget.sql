SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsBudgets_GetUomsForBudget

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

create PROCEDURE [dbo].[cbmsBudgets_GetUomsForBudget]
	( @MyAccountId int)
AS
BEGIN

	   select entity_id
		, entity_name
		, entity_type
	     from entity
	    where entity_id in (
				select converted_unit_id from consumption_unit_conversion
					where base_unit_id = 25
					and conversion_factor <> 0
				)
	  order by entity_type desc




END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgets_GetUomsForBudget] TO [CBMSApplication]
GO
