
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_NG_LOAD_PROFILE_P

DESCRIPTION: 

INPUT PARAMETERS:    
    Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id							 int
	@sr_rfp_account_id				 int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
USE CBMS
    exec dbo.SR_RFP_CREATE_NG_LOAD_PROFILE_P 308, 1747


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR		Deana Ritter
	AP		Athmaram Pabbathi

MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	DR		08/04/2009   Removed Linked Server Updates
	SSR		03/12/2010   Removed nolock hints 
	SSR		03/18/2010   Removed Cu_invoice(Account_id) with cu_invoice_service_month(Account_id)	
						Removed the hard coded entity_type values
     AP		09/08/2011   Following Changes made as a part of Addl Data prj
						  - Replaced Cost_Usage table with Cost_Usage_Account_Dtl
						  - Created variable @NG_Commodity_Id to load Electric Power Commodity_Id
******/
CREATE PROCEDURE dbo.SR_RFP_CREATE_NG_LOAD_PROFILE_P
      ( 
       @RFP_Id INT
      ,@SR_RFP_Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE
            @account_id INT
           ,@from_month DATETIME
           ,@to_month DATETIME
           ,@no_of_months INT
           ,@temp_month DATETIME
           ,@no_of_max_historical_months INT
           ,@month_selector VARCHAR(20)
           ,@determinant_id INT
           ,@unit_type_id INT
           ,@h_usage_vol1 NUMERIC(32, 16)
           ,@h_usage_vol2 NUMERIC(32, 16)
           ,@a_usage_vol NUMERIC(32, 16)
           ,@a_service_month DATETIME
           ,@a_reading_type_id INT
           ,@service_month1 DATETIME
           ,@service_month2 DATETIME
           ,@start_month1 DATETIME
           ,@end_month1 DATETIME
           ,@start_month2 DATETIME
           ,@end_month2 DATETIME
           ,@temp_vol1 NUMERIC(32, 16)
           ,@temp_vol2 NUMERIC(32, 16)
           ,@rfp_account_id INT
           ,@month_selector_type_id INT
           ,@additional_rows INT
           ,@default_lp_setup_id INT
           ,@rfp_lp_setup_id INT
           ,@cnt INT
           ,@determinant_type_id INT
           ,@account_determinant_type_id INT
           ,@actual_read_type_id INT
           ,@actual_avg_read_type_id INT
           ,@historical_prior1_read_type_id INT
           ,@historical_prior2_read_type_id INT
           ,@lp_status_id INT
           ,@noOfYears INT
           ,@curr_date DATETIME
           ,@temp_utility_id INT
           ,@temp_rfp_account_id INT
           ,@NG_Commodity_Id INT
      
      SELECT
            @NG_Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas'


      SELECT
            @determinant_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'LP_DETERMINANT_STATUS'
            AND e.entity_name = 'RFP Level'
			
      SELECT
            @account_determinant_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'LP_DETERMINANT_STATUS'
            AND e.entity_name = 'Account Level'
		
      SELECT
            @actual_read_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'READ_TYPE_LP_DETERMINANT_VALUE'
            AND e.entity_name = 'Actual LP Value'
		
      SELECT
            @actual_avg_read_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'READ_TYPE_LP_DETERMINANT_VALUE'
            AND e.entity_name = 'Actual Avg LP Value'
		
      SELECT
            @historical_prior1_read_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'READ_TYPE_LP_DETERMINANT_VALUE'
            AND e.entity_name = 'Historical Previous LP Value'
		
      SELECT
            @historical_prior2_read_type_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'READ_TYPE_LP_DETERMINANT_VALUE'
            AND e.entity_name = 'Historical Previous To Previous LP Value'
		
      SELECT
            @lp_status_id = e.entity_id
      FROM
            dbo.entity e
      WHERE
            e.ENTITY_DESCRIPTION = 'Load Profile Status'
            AND e.entity_name = 'Not Sent'

      IF @sr_rfp_account_id > 0 
            BEGIN

                  DELETE
                        SR_RFP_LP_DETERMINANT_VALUES
                  FROM
                        dbo.SR_RFP_LP_DETERMINANT_VALUES VAL
                        INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT det
                              ON det.sr_rfp_load_profile_determinant_id = val.sr_rfp_load_profile_determinant_id
                        JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP SETUP
                              ON det.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                  WHERE
                        setup.sr_rfp_account_id = @sr_rfp_account_id

                  DECLARE C_TERMS CURSOR READ_ONLY
                  FOR
                  SELECT
                        account_term.sr_account_group_id AS sr_rfp_account_id
                       ,min(account_term.from_month) AS from_month
                       ,max(account_term.to_month) AS to_month
                       ,( datediff(MONTH, min(account_term.from_month), max(account_term.to_month)) + 1 ) AS no_of_months
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM ACCOUNT_TERM
                        INNER JOIN dbo.SR_RFP_TERM TERM
                              ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
                        JOIN dbo.SR_RFP_ACCOUNT RFP_ACCOUNT
                              ON rfp_account.sr_rfp_account_id = account_term.sr_account_group_id
                  WHERE
                        term.sr_rfp_id = @rfp_id
                        AND rfp_account.sr_rfp_account_id = @sr_rfp_account_id
                        AND account_term.is_bid_group = 0
                  GROUP BY
                        account_term.sr_account_group_id
                  UNION
                  SELECT
                        rfp_account.sr_rfp_account_id AS sr_rfp_account_id
                       ,min(account_term.from_month) AS from_month
                       ,max(account_term.to_month) AS to_month
                       ,( datediff(MONTH, min(account_term.from_month), max(account_term.to_month)) + 1 ) AS no_of_months
                  FROM
                        dbo.sr_rfp_account_term account_term
                        JOIN dbo.sr_rfp_term term
                              ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
                        JOIN dbo.sr_rfp_account rfp_account
                              ON rfp_account.sr_rfp_bid_group_id = account_term.sr_account_group_id
                  WHERE
                        term.sr_rfp_id = @rfp_id
                        AND rfp_account.sr_rfp_account_id = @sr_rfp_account_id
                        AND account_term.is_bid_group = 1
                  GROUP BY
                        rfp_account.sr_rfp_account_id
		
            END
      ELSE 
            BEGIN
                  DELETE
                        sr_rfp_lp_determinant_values
                  FROM
                        sr_rfp_lp_determinant_values val
                        JOIN dbo.sr_rfp_load_profile_determinant det
                              ON val.sr_rfp_load_profile_determinant_id = det.sr_rfp_load_profile_determinant_id
                        JOIN dbo.sr_rfp_load_profile_setup setup
                              ON det.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                        JOIN dbo.sr_rfp_account acct
                              ON setup.sr_rfp_account_id = acct.sr_rfp_account_id
                  WHERE
                        acct.sr_rfp_id = @rfp_id

                  DECLARE C_TERMS CURSOR READ_ONLY
                  FOR
                  SELECT
                        account_term.sr_account_group_id AS sr_rfp_account_id
                       ,min(account_term.from_month) AS from_month
                       ,max(account_term.to_month) AS to_month
                       ,( datediff(MONTH, min(account_term.from_month), max(account_term.to_month)) + 1 ) AS no_of_months
                  FROM
                        sr_rfp_account_term account_term
                        JOIN dbo.sr_rfp_term term
                              ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
                        JOIN dbo.sr_rfp_account rfp_account
                              ON rfp_account.sr_rfp_account_id = account_term.sr_account_group_id
                  WHERE
                        term.sr_rfp_id = @rfp_id
                        AND account_term.is_bid_group = 0
                  GROUP BY
                        account_term.sr_account_group_id
                  UNION
                  SELECT
                        rfp_account.sr_rfp_account_id AS sr_rfp_account_id
                       ,min(account_term.from_month) AS from_month
                       ,max(account_term.to_month) AS to_month
                       ,( datediff(MONTH, min(account_term.from_month), max(account_term.to_month)) + 1 ) AS no_of_months
                  FROM
                        sr_rfp_account_term account_term
                        JOIN dbo.sr_rfp_term term
                              ON account_term.sr_rfp_term_id = term.sr_rfp_term_id
                        JOIN dbo.sr_rfp_account rfp_account
                              ON rfp_account.sr_rfp_bid_group_id = account_term.sr_account_group_id
                  WHERE
                        term.sr_rfp_id = @rfp_id
                        AND account_term.is_bid_group = 1
                  GROUP BY
                        rfp_account.sr_rfp_account_id
            END

      OPEN C_TERMS
	
      FETCH NEXT FROM C_TERMS INTO @rfp_account_id, @from_month, @to_month, @no_of_months  
      SELECT
            @curr_date = getdate()
	
	
      SELECT
            @noOfYears = datediff(YEAR, getdate(), @from_month)
      IF @noOfYears < 0
            OR @noOfYears = 0 
            BEGIN
                  SET @noOfYears = 1
            END 
      ELSE 
            BEGIN
                  DECLARE
                        @year_cnt INT
                       ,@check BIT
                  SET @year_cnt = 1
                  SET @check = 0
                  WHILE ( @year_cnt <= @noOfYears ) 
                        BEGIN
                              IF ( @curr_date BETWEEN dateadd(yyyy, ( -1 * @year_cnt ), @from_month)
                                              AND     dateadd(yyyy, ( -1 * @year_cnt ), @to_month) ) 
                                    BEGIN
                                          SET @check = 1
                                    END
                              SET @year_cnt = @year_cnt + 1
                        END
                  IF @check = 0 
                        BEGIN
                              SELECT
                                    @noOfYears = @noOfYears + 1
                        END	
            END

      WHILE ( @@FETCH_STATUS <> -1 ) 
            BEGIN
                  IF ( @@FETCH_STATUS <> -2 ) 
                        BEGIN
						
                              SELECT
                                    @account_id = account_id
                              FROM
                                    sr_rfp_account
                              WHERE
                                    sr_rfp_account_id = @rfp_account_id
                                    AND sr_rfp_id = @rfp_id
                              IF ( SELECT
                                    count(sr_rfp_lp_account_summary_id)
                                   FROM
                                    sr_rfp_lp_account_summary
                                   WHERE
                                    sr_account_group_id = @rfp_account_id ) > 0 
                                    BEGIN
                                          UPDATE
                                                sr_rfp_lp_account_summary
                                          SET   
                                                date_created = getdate()
                                          WHERE
                                                sr_account_group_id = @rfp_account_id
				
                                    END
                              ELSE 
                                    BEGIN
                                          INSERT      INTO sr_rfp_lp_account_summary
                                                      ( 
                                                       sr_account_group_id
                                                      ,is_bid_group
                                                      ,status_type_id
                                                      ,date_created )
                                          VALUES
                                                      ( 
                                                       @rfp_account_id
                                                      ,0
                                                      ,@lp_status_id
                                                      ,getdate() )
                                    END
			
                              SET @rfp_lp_setup_id = NULL
                              IF ( SELECT
                                    count(sr_rfp_load_profile_setup_id)
                                   FROM
                                    sr_rfp_load_profile_setup lp_setup
                                   WHERE
                                    lp_setup.sr_rfp_account_id = @rfp_account_id ) = 0 
                                    BEGIN
                                          SELECT
                                                @month_selector_type_id = setup.month_selector_type_id
                                               ,@additional_rows = setup.additional_rows
                                               ,@default_lp_setup_id = setup.sr_load_profile_default_setup_id
                                          FROM
                                                sr_rfp rfp
                                                JOIN dbo.sr_rfp_account rfp_acc
                                                      ON rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                                                JOIN dbo.account acc
                                                      ON acc.account_id = rfp_acc.account_id
                                                JOIN dbo.sr_load_profile_default_setup setup
                                                      ON setup.commodity_type_id = rfp.commodity_type_id
                                                         AND setup.vendor_id = acc.vendor_id
                                          WHERE
                                                rfp.sr_rfp_id = @rfp_id
                                                AND rfp_acc.sr_rfp_account_id = @rfp_account_id
				
                                          INSERT      INTO sr_rfp_load_profile_setup
                                                      ( 
                                                       sr_rfp_account_id
                                                      ,sr_rfp_id
                                                      ,additional_rows
                                                      ,month_selector_type_id
                                                      ,updated_month_selector_type_id )
                                          VALUES
                                                      ( 
                                                       @rfp_account_id
                                                      ,@rfp_id
                                                      ,@additional_rows
                                                      ,@month_selector_type_id
                                                      ,@month_selector_type_id )

                                          SELECT
                                                @rfp_lp_setup_id = @@IDENTITY

                                          INSERT      INTO sr_rfp_load_profile_determinant
                                                      ( 
                                                       sr_rfp_load_profile_setup_id
                                                      ,determinant_name
                                                      ,determinant_unit_type_id
                                                      ,updated_determinant_unit_type_id
                                                      ,is_checked
                                                      ,updated_is_checked
                                                      ,determinant_type_id )
                                                      SELECT
                                                            @rfp_lp_setup_id
                                                           ,determinant.determinant_name
                                                           ,determinant.determinant_unit_type_id
                                                           ,determinant.determinant_unit_type_id
                                                           ,determinant.is_checked
                                                           ,determinant.is_checked
                                                           ,@determinant_type_id
                                                      FROM
                                                            sr_load_profile_determinant determinant
                                                      WHERE
                                                            determinant.sr_load_profile_default_setup_id = @default_lp_setup_id
				
                                          INSERT      INTO sr_rfp_lp_account_additional_row
                                                      ( 
                                                       sr_rfp_account_id
                                                      ,row_name
                                                      ,row_value
                                                      ,updated_row_name
                                                      ,updated_row_value
                                                      ,row_type_id )
                                                      SELECT
                                                            @rfp_account_id
                                                           ,additional_row.row_name
                                                           ,additional_row.row_value
                                                           ,additional_row.row_name
                                                           ,additional_row.row_value
                                                           ,@determinant_type_id
                                                      FROM
                                                            sr_load_profile_additional_row additional_row
                                                      WHERE
                                                            additional_row.sr_load_profile_default_setup_id = @default_lp_setup_id
                                    END
                              ELSE 
                                    BEGIN
                                          IF ( SELECT
                                                count(sr_rfp_lp_account_additional_row_id)
                                               FROM
                                                sr_rfp_lp_account_additional_row
                                               WHERE
                                                sr_rfp_account_id = @rfp_account_id ) > 0 
                                                BEGIN
                                                      UPDATE
                                                            sr_rfp_lp_account_additional_row
                                                      SET   
                                                            row_name = updated_row_name
                                                           ,row_value = updated_row_value
                                                           ,row_type_id = @determinant_type_id
                                                      WHERE
                                                            sr_rfp_account_id = @rfp_account_id
                                                            AND row_type_id != @account_determinant_type_id
					
                                                END
                                          ELSE 
                                                BEGIN
                                                      SELECT
                                                            @temp_utility_id = acc.vendor_id
                                                      FROM
                                                            sr_rfp_account rfp_acc
                                                            JOIN dbo.account acc
                                                                  ON acc.account_id = rfp_acc.account_id
                                                      WHERE
                                                            rfp_acc.sr_rfp_account_id = @rfp_account_id 
					
                                                      SELECT
                                                            @temp_rfp_account_id = additional_row.sr_rfp_account_id
                                                      FROM
                                                            sr_rfp_lp_account_additional_row additional_row
                                                            JOIN dbo.sr_rfp_account rfp_acc
                                                                  ON additional_row.sr_rfp_account_id = rfp_acc.sr_rfp_account_id
                                                            JOIN dbo.account acc
                                                                  ON rfp_acc.account_id = acc.account_id
                                                      WHERE
                                                            acc.vendor_id = @temp_utility_id

                                                      IF @temp_rfp_account_id > 0 
                                                            BEGIN
                                                                  INSERT      INTO sr_rfp_lp_account_additional_row
                                                                              ( 
                                                                               sr_rfp_account_id
                                                                              ,row_name
                                                                              ,row_value
                                                                              ,updated_row_name
                                                                              ,updated_row_value
                                                                              ,row_type_id )
                                                                              SELECT
                                                                                    @rfp_account_id
                                                                                   ,additional_row.row_name
                                                                                   ,additional_row.row_value
                                                                                   ,additional_row.row_name
                                                                                   ,additional_row.row_value
                                                                                   ,@determinant_type_id
                                                                              FROM
                                                                                    sr_rfp_lp_account_additional_row additional_row
                                                                              WHERE
                                                                                    additional_row.sr_rfp_account_id = @temp_rfp_account_id
				
                                                            END 
                                                      ELSE 
                                                            BEGIN
                                                                  SELECT
                                                                        @default_lp_setup_id = setup.sr_load_profile_default_setup_id
                                                                  FROM
                                                                        sr_rfp rfp
                                                                        JOIN dbo.sr_rfp_account rfp_acc
                                                                              ON rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                                                                        JOIN dbo.account acc
                                                                              ON acc.account_id = rfp_acc.account_id
                                                                        JOIN dbo.sr_load_profile_default_setup setup
                                                                              ON setup.commodity_type_id = rfp.commodity_type_id
                                                                                 AND setup.vendor_id = acc.vendor_id
                                                                  WHERE
                                                                        rfp.sr_rfp_id = @rfp_id
                                                                        AND rfp_acc.sr_rfp_account_id = @rfp_account_id
							
                                                                  INSERT      INTO sr_rfp_lp_account_additional_row
                                                                              ( 
                                                                               sr_rfp_account_id
                                                                              ,row_name
                                                                              ,row_value
                                                                              ,updated_row_name
                                                                              ,updated_row_value
                                                                              ,row_type_id )
                                                                              SELECT
                                                                                    @rfp_account_id
                                                                                   ,additional_row.row_name
                                                                                   ,additional_row.row_value
                                                                                   ,additional_row.row_name
                                                                                   ,additional_row.row_value
                                                                                   ,@determinant_type_id
                                                                              FROM
                                                                                    sr_load_profile_additional_row additional_row
                                                                              WHERE
                                                                                    additional_row.sr_load_profile_default_setup_id = @default_lp_setup_id
                                                            END
					
                                                END		
				
                                          SELECT
                                                @rfp_lp_setup_id = sr_rfp_load_profile_setup_id
                                          FROM
                                                sr_rfp_load_profile_setup
                                          WHERE
                                                sr_rfp_account_id = @rfp_account_id

                                          UPDATE
                                                sr_rfp_load_profile_setup
                                          SET   
                                                month_selector_type_id = updated_month_selector_type_id
                                          WHERE
                                                sr_rfp_account_id = @rfp_account_id

                                          IF ( SELECT
                                                count(sr_rfp_load_profile_determinant_id)
                                               FROM
                                                sr_rfp_load_profile_determinant
                                               WHERE
                                                sr_rfp_load_profile_setup_id = @rfp_lp_setup_id ) > 0 
                                                BEGIN
                                                      UPDATE
                                                            sr_rfp_load_profile_determinant
                                                      SET   
                                                            determinant_type_id = @determinant_type_id
                                                           ,determinant_unit_type_id = updated_determinant_unit_type_id
                                                           ,is_checked = updated_is_checked
                                                      WHERE
                                                            sr_rfp_load_profile_setup_id = @rfp_lp_setup_id
                                                            AND updated_determinant_unit_type_id IS NOT NULL
                                                            AND updated_is_checked IS NOT NULL

                                                END
                                          ELSE 
                                                BEGIN 
                                                      DECLARE
                                                            @max_rfp_lp_setup_id INT
                                                           ,@utility_id INT
                                                      SELECT
                                                            @utility_id = acc.vendor_id
                                                      FROM
                                                            sr_rfp_account rfp_acc
                                                            INNER JOIN dbo.account acc
                                                                  ON acc.account_id = rfp_acc.account_id
                                                      WHERE
                                                            rfp_acc.sr_rfp_account_id = @rfp_account_id
				

                                                      SELECT
                                                            @max_rfp_lp_setup_id = max(setup.sr_rfp_load_profile_setup_id)
                                                      FROM
                                                            sr_rfp_account rfp_acc
                                                            JOIN dbo.sr_rfp_load_profile_setup setup
                                                                  ON setup.sr_rfp_account_id = rfp_acc.sr_rfp_account_id
                                                            JOIN dbo.sr_rfp_load_profile_determinant det
                                                                  ON det.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                                                            JOIN dbo.account acc
                                                                  ON acc.account_id = rfp_acc.account_id
                                                      WHERE
                                                            rfp_acc.sr_rfp_id = @rfp_id
                                                            AND acc.vendor_id = @utility_id
					
                                                      IF @max_rfp_lp_setup_id > 0 
                                                            BEGIN	
                                                                  INSERT      INTO sr_rfp_load_profile_determinant
                                                                              ( 
                                                                               sr_rfp_load_profile_setup_id
                                                                              ,determinant_name
                                                                              ,determinant_unit_type_id
                                                                              ,updated_determinant_unit_type_id
                                                                              ,is_checked
                                                                              ,updated_is_checked
                                                                              ,determinant_type_id )
                                                                              SELECT
                                                                                    @rfp_lp_setup_id
                                                                                   ,determinant.determinant_name
                                                                                   ,determinant.determinant_unit_type_id
                                                                                   ,determinant.determinant_unit_type_id
                                                                                   ,determinant.is_checked
                                                                                   ,determinant.is_checked
                                                                                   ,@determinant_type_id
                                                                              FROM
                                                                                    sr_rfp_load_profile_determinant determinant
                                                                              WHERE
                                                                                    determinant.sr_rfp_load_profile_setup_id = @max_rfp_lp_setup_id
                                                            END
                                                      ELSE 
                                                            BEGIN
                                                                  SELECT
                                                                        @default_lp_setup_id = setup.sr_load_profile_default_setup_id
                                                                  FROM
                                                                        sr_rfp rfp
                                                                        JOIN dbo.sr_rfp_account rfp_acc
                                                                              ON rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                                                                        JOIN dbo.account acc
                                                                              ON acc.account_id = rfp_acc.account_id
                                                                        JOIN dbo.sr_load_profile_default_setup setup
                                                                              ON setup.commodity_type_id = rfp.commodity_type_id
                                                                                 AND setup.vendor_id = acc.vendor_id
                                                                  WHERE
                                                                        rfp.sr_rfp_id = @rfp_id
                                                                        AND rfp_acc.sr_rfp_account_id = @rfp_account_id
						
                                                                  INSERT      INTO sr_rfp_load_profile_determinant
                                                                              ( 
                                                                               sr_rfp_load_profile_setup_id
                                                                              ,determinant_name
                                                                              ,determinant_unit_type_id
                                                                              ,updated_determinant_unit_type_id
                                                                              ,is_checked
                                                                              ,updated_is_checked
                                                                              ,determinant_type_id )
                                                                              SELECT
                                                                                    @rfp_lp_setup_id
                                                                                   ,determinant.determinant_name
                                                                                   ,determinant.determinant_unit_type_id
                                                                                   ,determinant.determinant_unit_type_id
                                                                                   ,determinant.is_checked
                                                                                   ,determinant.is_checked
                                                                                   ,@determinant_type_id
                                                                              FROM
                                                                                    sr_load_profile_determinant determinant
                                                                              WHERE
                                                                                    determinant.sr_load_profile_default_setup_id = @default_lp_setup_id

                                                            END
				
                                                END 
                                    END
			
                              SELECT
                                    @month_selector = monthSelector.entity_name
                                   ,@additional_rows = setup.additional_rows
                                   ,@determinant_id = determinant.sr_rfp_load_profile_determinant_id
                                   ,@unit_type_id = determinant.determinant_unit_type_id
                              FROM
                                    sr_rfp_load_profile_setup setup
                                    JOIN dbo.entity monthSelector
                                          ON monthSelector.entity_id = setup.month_selector_type_id
                                             AND setup.sr_rfp_account_id = @rfp_account_id
                                    JOIN dbo.sr_rfp_load_profile_determinant determinant
                                          ON determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                                             AND determinant.is_checked = 1
                                             AND determinant.determinant_name = 'Total Usage'
			
                              IF @no_of_months > 12 
                                    BEGIN
                                          SET @no_of_max_historical_months = 12
                                    END
                              ELSE 
                                    BEGIN
                                          SET @no_of_max_historical_months = @no_of_months
                                    END
                              SET @temp_month = @from_month
                              SET @cnt = 0
                              SET @a_service_month = NULL 
                              SET @a_usage_vol = NULL

                              WHILE ( @cnt < @no_of_max_historical_months ) 
                                    BEGIN
				--//reset variables
                                          SET @h_usage_vol1 = NULL
                                          SET @h_usage_vol2 = NULL
                                          SET @a_usage_vol = NULL
                                          SET @a_service_month = NULL
                                          SET @service_month1 = NULL
                                          SET @service_month2 = NULL
                                          SET @start_month1 = NULL
                                          SET @end_month1 = NULL
                                          SET @start_month2 = NULL
                                          SET @end_month2 = NULL
					
                                          SELECT
                                                @a_service_month = month_identifier
                                               ,@a_usage_vol = lp_value
                                          FROM
                                                sr_rfp_lp_determinant_values
                                          WHERE
                                                sr_rfp_load_profile_determinant_id = @determinant_id
                                                AND convert(VARCHAR(10), month_identifier, 101) = convert(VARCHAR(10), @temp_month, 101)
                                                AND ( reading_type_id = @actual_read_type_id
                                                      OR reading_type_id = @actual_avg_read_type_id )

                                          IF ( @Month_Selector = 'System Bill Month' ) 
                                                BEGIN

                                                      SELECT
                                                            @Start_Month1 = max(case WHEN cu_in_ser.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month) THEN cu_in.begin_date
                                                                                END)
                                                           ,@End_Month1 = max(case WHEN cu_in_ser.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month) THEN cu_in.end_date
                                                                              END)
                                                           ,@Start_Month2 = max(case WHEN cu_in_ser.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) THEN cu_in.begin_date
                                                                                END)
                                                           ,@End_Month2 = max(case WHEN cu_in_ser.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) THEN cu_in.end_date
                                                                              END)
                                                      FROM
                                                            dbo.cu_invoice cu_in
                                                            JOIN dbo.CU_INVOICE_SERVICE_MONTH cu_in_ser
                                                                  ON cu_in_ser.cu_invoice_id = cu_in.cu_invoice_id
                                                      WHERE
                                                            cu_in_ser.account_id = @account_id
                                                            AND cu_in.is_default = 1
                                                            AND ( cu_in_ser.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month)
                                                                  OR cu_in_ser.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) )
                                                                  
                                                                  
                                                      SELECT
                                                            @H_Usage_Vol1 = isnull(sum(case WHEN CUAD.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month) THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                            ELSE 0
                                                                                       END), 0)
                                                           ,@Service_Month1 = max(case WHEN CUAD.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month) THEN CUAD.Service_Month
                                                                                  END)
                                                           ,@H_Usage_Vol2 = isnull(sum(case WHEN CUAD.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                            ELSE 0
                                                                                       END), 0)
                                                           ,@Service_Month2 = max(case WHEN CUAD.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) THEN CUAD.Service_Month
                                                                                  END)
                                                      FROM
                                                            dbo.Cost_Usage_Account_Dtl CUAD
                                                            INNER JOIN dbo.Bucket_Master BM
                                                                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                                            INNER JOIN dbo.Consumption_Unit_Conversion CUC
                                                                  ON CUC.Base_Unit_Id = CUAD.UOM_Type_Id
                                                      WHERE
                                                            CUAD.Account_Id = @Account_Id
                                                            AND CUC.Converted_Unit_Id = @Unit_Type_Id
                                                            AND ( CUAD.Service_Month = dateadd(yyyy, -1 * @NoOfYears, @Temp_Month)
                                                                  OR CUAD.Service_Month = dateadd(yyyy, -1 * ( @NoOfYears + 1 ), @Temp_Month) )
                                                            AND BM.Bucket_Name = 'Total Usage'
                                                            AND BM.Commodity_Id = @NG_Commodity_Id
                                                END 
                                          ELSE 
                                                IF ( @Month_Selector = 'Start Date' ) 
                                                      BEGIN
                                                            SELECT
                                                                  @H_Usage_Vol1 = isnull(sum(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.Begin_Date), @Temp_Month) = 0 THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                                  ELSE 0
                                                                                             END), 0)
                                                                 ,@Service_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.Begin_Date), @Temp_Month) = 0 THEN convert(DATETIME, convert(VARCHAR, datepart(m, CI.Begin_Date)) + '/01/' + convert(VARCHAR, datepart(yy, CI.Begin_Date)), 101)
                                                                                        END)
                                                                 ,@Start_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.Begin_Date), @Temp_Month) = 0 THEN CI.Begin_Date
                                                                                      END)
                                                                 ,@End_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.End_Date), @Temp_Month) = 0 THEN CI.End_Date
                                                                                    END)
                                                                 ,@H_Usage_Vol2 = isnull(sum(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.Begin_Date), @Temp_Month) = 0 THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                                  ELSE 0
                                                                                             END), 0)
                                                                 ,@Service_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.Begin_Date), @Temp_Month) = 0 THEN convert(DATETIME, convert(VARCHAR, datepart(m, CI.Begin_Date)) + '/01/' + convert(VARCHAR, datepart(yy, CI.Begin_Date)), 101)
                                                                                        END)
                                                                 ,@Start_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.Begin_Date), @Temp_Month) = 0 THEN CI.Begin_Date
                                                                                      END)
                                                                 ,@End_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.End_Date), @Temp_Month) = 0 THEN CI.End_Date
                                                                                    END)
                                                            FROM
                                                                  dbo.Cost_Usage_Account_Dtl CUAD
                                                                  INNER JOIN dbo.Bucket_Master BM
                                                                        ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                                                  INNER JOIN dbo.CU_Invoice_Service_Month CISM
                                                                        ON CISM.Account_Id = CUAD.Account_Id
                                                                  INNER JOIN dbo.CU_Invoice CI
                                                                        ON CI.CU_Invoice_Id = CISM.CU_Invoice_Id
                                                                  INNER JOIN dbo.Consumption_Unit_Conversion CUC
                                                                        ON CUC.Base_Unit_Id = CUAD.UOM_Type_Id
                                                            WHERE
                                                                  CUAD.Account_Id = @Account_Id
                                                                  AND CUC.Converted_Unit_Id = @Unit_Type_Id
                                                                  AND datediff(month, CUAD.Service_Month, CI.Begin_Date) = 0
                                                                  AND ( datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.Begin_Date), @Temp_Month) = 0
                                                                        OR datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.Begin_Date), @Temp_Month) = 0 )
                                                                  AND BM.Bucket_Name = 'Total Usage'
                                                                  AND BM.Commodity_Id = @NG_Commodity_Id
                                                      END 
                                                ELSE 
                                                      IF ( @Month_Selector = 'End Date' ) 
                                                            BEGIN
                                                                  SELECT
                                                                        @H_Usage_Vol1 = isnull(sum(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.End_Date), @Temp_Month) = 0 THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                                        ELSE 0
                                                                                                   END), 0)
                                                                       ,@Service_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.End_Date), @Temp_Month) = 0 THEN convert(DATETIME, convert(VARCHAR, datepart(m, CI.End_Date)) + '/01/' + convert(VARCHAR, datepart(yy, CI.End_Date)), 101)
                                                                                              END)
                                                                       ,@Start_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.Begin_Date), @Temp_Month) = 0 THEN CI.Begin_Date
                                                                                            END)
                                                                       ,@End_Month1 = max(case WHEN datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.End_Date), @Temp_Month) = 0 THEN CI.End_Date
                                                                                          END)
                                                                       ,@H_Usage_Vol2 = isnull(sum(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.End_Date), @Temp_Month) = 0 THEN CUAD.Bucket_Value * CUC.Conversion_Factor
                                                                                                        ELSE 0
                                                                                                   END), 0)
                                                                       ,@Service_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.End_Date), @Temp_Month) = 0 THEN convert(DATETIME, convert(VARCHAR, datepart(m, CI.End_Date)) + '/01/' + convert(VARCHAR, datepart(yy, CI.End_Date)), 101)
                                                                                              END)
                                                                       ,@Start_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.Begin_Date), @Temp_Month) = 0 THEN CI.Begin_Date
                                                                                            END)
                                                                       ,@End_Month2 = max(case WHEN datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.End_Date), @Temp_Month) = 0 THEN CI.End_Date
                                                                                          END)
                                                                  FROM
                                                                        dbo.Cost_Usage_Account_Dtl CUAD
                                                                        INNER JOIN dbo.Bucket_Master BM
                                                                              ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                                                        INNER JOIN dbo.CU_Invoice_Service_Month CISM
                                                                              ON CISM.Account_Id = CUAD.Account_Id
                                                                        INNER JOIN dbo.CU_Invoice CI
                                                                              ON CI.CU_Invoice_Id = CISM.CU_Invoice_Id
                                                                        INNER JOIN dbo.Consumption_Unit_Conversion CUC
                                                                              ON CUC.Base_Unit_Id = CUAD.UOM_Type_Id
                                                                  WHERE
                                                                        CUAD.Account_Id = @Account_Id
                                                                        AND CUC.Converted_Unit_Id = @Unit_Type_Id
                                                                        AND datediff(month, CUAD.Service_Month, CI.End_Date) = 0
                                                                        AND ( datediff(MONTH, dateadd(yyyy, @NoOfYears, CI.End_Date), @Temp_Month) = 0
                                                                              OR datediff(MONTH, dateadd(yyyy, ( @NoOfYears + 1 ), CI.End_Date), @Temp_Month) = 0 )
                                                                        AND BM.Bucket_Name = 'Total Usage'
                                                                        AND BM.Commodity_Id = @NG_Commodity_Id
                                                            END 

                                          IF @a_service_month IS NULL 
                                                BEGIN
                                                      IF ( @service_month1 IS NOT NULL ) 
                                                            BEGIN		
                                                                  INSERT      INTO sr_rfp_lp_determinant_values
                                                                              ( 
                                                                               sr_rfp_load_profile_determinant_id
                                                                              ,month_identifier
                                                                              ,lp_value
                                                                              ,reading_type_id )
                                                                  VALUES
                                                                              ( 
                                                                               @determinant_id
                                                                              ,@temp_month
                                                                              ,@h_usage_vol1
                                                                              ,@actual_read_type_id )
						
                                                            END
                                                      ELSE 
                                                            IF ( @service_month2 IS NOT NULL ) 
                                                                  BEGIN		
                                                                        INSERT      INTO sr_rfp_lp_determinant_values
                                                                                    ( 
                                                                                     sr_rfp_load_profile_determinant_id
                                                                                    ,month_identifier
                                                                                    ,lp_value
                                                                                    ,reading_type_id )
                                                                        VALUES
                                                                                    ( 
                                                                                     @determinant_id
                                                                                    ,@temp_month
                                                                                    ,@h_usage_vol2
                                                                                    ,@actual_read_type_id )
						
						
                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        INSERT      INTO sr_rfp_lp_determinant_values
                                                                                    ( 
                                                                                     sr_rfp_load_profile_determinant_id
                                                                                    ,month_identifier
                                                                                    ,reading_type_id )
                                                                        VALUES
                                                                                    ( 
                                                                                     @determinant_id
                                                                                    ,@temp_month
                                                                                    ,@actual_read_type_id )
                                                                  END 

                                                      INSERT      INTO sr_rfp_lp_determinant_values
                                                                  ( 
                                                                   sr_rfp_load_profile_determinant_id
                                                                  ,month_identifier
                                                                  ,lp_value
                                                                  ,meter_reading_from_date
                                                                  ,meter_reading_to_date
                                                                  ,reading_type_id )
                                                      VALUES
                                                                  ( 
                                                                   @determinant_id
                                                                  ,dateadd(yyyy, ( -1 * @noOfYears ), @temp_month)
                                                                  ,@h_usage_vol1
                                                                  ,@start_month1
                                                                  ,@end_month1
                                                                  ,@historical_prior1_read_type_id )
					
                                                      INSERT      INTO sr_rfp_lp_determinant_values
                                                                  ( 
                                                                   sr_rfp_load_profile_determinant_id
                                                                  ,month_identifier
                                                                  ,lp_value
                                                                  ,meter_reading_from_date
                                                                  ,meter_reading_to_date
                                                                  ,reading_type_id )
                                                      VALUES
                                                                  ( 
                                                                   @determinant_id
                                                                  ,dateadd(yyyy, ( -1 * ( @noOfYears + 1 ) ), @temp_month)
                                                                  ,@h_usage_vol2
                                                                  ,@start_month2
                                                                  ,@end_month2
                                                                  ,@historical_prior2_read_type_id )
					
                                                END
                                          ELSE 
                                                BEGIN
                                                      IF ( @service_month1 IS NOT NULL ) 
                                                            BEGIN		
                                                                  UPDATE
                                                                        sr_rfp_lp_determinant_values
                                                                  SET   
                                                                        lp_value = @h_usage_vol1
                                                                  WHERE
                                                                        month_identifier = @temp_month
                                                                        AND sr_rfp_load_profile_determinant_id = @determinant_id
                                                                        AND reading_type_id = @actual_read_type_id	
					
                                                            END
                                                      ELSE 
                                                            IF ( @service_month2 IS NOT NULL ) 
                                                                  BEGIN		

                                                                        UPDATE
                                                                              sr_rfp_lp_determinant_values
                                                                        SET   
                                                                              lp_value = @h_usage_vol2
                                                                        WHERE
                                                                              month_identifier = @temp_month
                                                                              AND sr_rfp_load_profile_determinant_id = @determinant_id
                                                                              AND reading_type_id = @actual_read_type_id	
						  
                                                                  END
                                                            ELSE 
                                                                  BEGIN
                                                                        UPDATE
                                                                              sr_rfp_lp_determinant_values
                                                                        SET   
                                                                              lp_value = @h_usage_vol1
                                                                        WHERE
                                                                              month_identifier = @temp_month
                                                                              AND sr_rfp_load_profile_determinant_id = @determinant_id
                                                                              AND reading_type_id = @actual_read_type_id	

                                                                  END
                                                      UPDATE
                                                            sr_rfp_lp_determinant_values
                                                      SET   
                                                            lp_value = @h_usage_vol1
                                                           ,meter_reading_from_date = @start_month1
                                                           ,meter_reading_to_date = @end_month1
                                                      WHERE
                                                            month_identifier = dateadd(yyyy, ( -1 * @noOfYears ), @temp_month)
                                                            AND sr_rfp_load_profile_determinant_id = @determinant_id
                                                            AND reading_type_id = @historical_prior1_read_type_id	

                                                      UPDATE
                                                            sr_rfp_lp_determinant_values
                                                      SET   
                                                            lp_value = @h_usage_vol2
                                                           ,meter_reading_from_date = @start_month2
                                                           ,meter_reading_to_date = @end_month2
                                                      WHERE
                                                            month_identifier = dateadd(yyyy, ( -1 * ( @noOfYears + 1 ) ), @temp_month)
                                                            AND sr_rfp_load_profile_determinant_id = @determinant_id
                                                            AND reading_type_id = @historical_prior2_read_type_id	
					      
                                                END 

                                          SET @temp_month = dateadd(mm, 1, @temp_month)
                                          SET @cnt = @cnt + 1
                                    END 
			
                              SET @temp_month = @from_month
                              SET @cnt = 0
                              WHILE ( @cnt < @no_of_max_historical_months ) 
                                    BEGIN

                                          SET @temp_vol1 = NULL
                                          SET @temp_vol2 = NULL
                                          SET @a_usage_vol = NULL

                                          SELECT
                                                @a_usage_vol = lp_value
                                          FROM
                                                sr_rfp_lp_determinant_values
                                          WHERE
                                                sr_rfp_load_profile_determinant_id = @determinant_id
                                                AND convert(VARCHAR(10), month_identifier, 101) = convert(VARCHAR(10), @temp_month, 101)
                                                AND reading_type_id = @actual_read_type_id
				
                                          IF @a_usage_vol IS NULL 
                                                BEGIN
                                                      SELECT
                                                            @temp_vol1 = lp_value
                                                      FROM
                                                            sr_rfp_lp_determinant_values
                                                      WHERE
                                                            sr_rfp_load_profile_determinant_id = @determinant_id
                                                            AND month_identifier = dateadd(mm, -1, @temp_month)
                                                            AND reading_type_id = @actual_read_type_id
						
                                                      IF @temp_vol1 > 0 
                                                            BEGIN
                                                                  SELECT
                                                                        @temp_vol2 = lp_value
                                                                  FROM
                                                                        sr_rfp_lp_determinant_values
                                                                  WHERE
                                                                        sr_rfp_load_profile_determinant_id = @determinant_id
                                                                        AND month_identifier = dateadd(mm, 1, @temp_month)
                                                                        AND reading_type_id = @actual_read_type_id
                                                            END	

                                                      IF @temp_vol1 > 0
                                                            AND @temp_vol2 > 0 
                                                            BEGIN	
                                                                  UPDATE
                                                                        sr_rfp_lp_determinant_values
                                                                  SET   
                                                                        lp_value = ( ( @temp_vol1 + @temp_vol2 ) / 2 )
                                                                       ,reading_type_id = @actual_avg_read_type_id
                                                                  WHERE
                                                                        sr_rfp_load_profile_determinant_id = @determinant_id
                                                                        AND month_identifier = @temp_month
                                                                        AND reading_type_id = @actual_read_type_id
						
                                                            END 
                                                END 

                                          SET @temp_month = dateadd(mm, 1, @temp_month)
                                          SET @cnt = @cnt + 1
                                    END 
                              SET @temp_month = dateadd(mm, 12, @from_month)
                              SET @cnt = 0
                              WHILE ( @cnt < ( @no_of_months - 12 ) ) 
                                    BEGIN
                                          SET @a_service_month = NULL
                                          SET @a_usage_vol = NULL
                                          SET @a_reading_type_id = NULL

                                          SELECT
                                                @a_service_month = month_identifier
                                               ,@a_usage_vol = lp_value
                                               ,@a_reading_type_id = reading_type_id
                                          FROM
                                                sr_rfp_lp_determinant_values
                                          WHERE
                                                sr_rfp_load_profile_determinant_id = @determinant_id
                                                AND convert(VARCHAR(10), month_identifier, 101) = convert(VARCHAR(10), dateadd(mm, -12, @temp_month), 101)
                                                AND ( reading_type_id = @actual_read_type_id
                                                      OR reading_type_id = @actual_avg_read_type_id ) 


                                          IF ( SELECT
                                                count(sr_rfp_lp_determinant_values_id)
                                               FROM
                                                sr_rfp_lp_determinant_values
                                               WHERE
                                                sr_rfp_load_profile_determinant_id = @determinant_id
                                                AND convert(VARCHAR(10), month_identifier, 101) = convert(VARCHAR(10), @temp_month, 101) ) > 0 
                                                BEGIN
                                                      UPDATE
                                                            sr_rfp_lp_determinant_values
                                                      SET   
                                                            lp_value = @a_usage_vol
                                                           ,reading_type_id = @a_reading_type_id
                                                      WHERE
                                                            month_identifier = @temp_month
                                                            AND sr_rfp_load_profile_determinant_id = @determinant_id 
					      
                                                END
                                          ELSE 
                                                BEGIN
                                                      INSERT      INTO sr_rfp_lp_determinant_values
                                                                  ( 
                                                                   sr_rfp_load_profile_determinant_id
                                                                  ,month_identifier
                                                                  ,lp_value
                                                                  ,reading_type_id )
                                                      VALUES
                                                                  ( 
                                                                   @determinant_id
                                                                  ,@temp_month
                                                                  ,@a_usage_vol
                                                                  ,@a_reading_type_id )
					
                                                END 

                                          SET @temp_month = dateadd(mm, 1, @temp_month)
                                          SET @cnt = @cnt + 1
                                    END 

                        END
                  FETCH NEXT FROM C_TERMS INTO @rfp_account_id, @from_month, @to_month, @no_of_months  
            END
      CLOSE C_TERMS
      DEALLOCATE C_TERMS

END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_NG_LOAD_PROFILE_P] TO [CBMSApplication]
GO
