
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Update_New_User_Email_Ts 
   
DESCRIPTION: This procedure used to update the user 'New_user_Email_Ts' time after sending an email.
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @UserInfoId        INT                                User ID 
    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------    
  USAGE EXAMPLES:    
--------------------------------------------------------------------    
   
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------     
 KVK K Vinay Kumar
 
 
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------
   KVK		2011-02-08	created
   KVK		04/11/2014	Since Audit columns exists added Last_Change_Ts in update statement
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Update_New_User_Email_Ts] ( @User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON
	
      UPDATE
            dbo.USER_INFO
      SET   
            New_user_Email_Ts = getdate()
           ,Last_Change_Ts = getdate()
      WHERE
            user_info_id = @User_Info_Id
		  
END

;
GO

GRANT EXECUTE ON  [dbo].[Update_New_User_Email_Ts] TO [CBMSApplication]
GO
