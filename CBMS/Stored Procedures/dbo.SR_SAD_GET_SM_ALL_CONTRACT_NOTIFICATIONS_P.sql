
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SM_ALL_CONTRACT_NOTIFICATIONS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@month_identifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.SR_SAD_GET_SM_ALL_CONTRACT_NOTIFICATIONS_P 75, '06/18/2012'
EXEC dbo.SR_SAD_GET_SM_ALL_CONTRACT_NOTIFICATIONS_P 35684,'2014-04-01'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AKR         Ashok Kumar Raju
    
MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
	        9/21/2010	Modify Quoted Identifier
DMR			09/10/2010  Modified for Quoted_Identifier
AKR         2012-10-29  Modified the Code to get the data based on Analyst Mapping Code
RR			2014-03-19	MAINT-2619 Script not returning/counting the contract expiring account(meter's account) if the account is having 
						active RFP for different commodity. Added commodity check condition.
RR			2016-04-02	Global Sourcing - Phase3 - GCS-505 Script modified to send/display notifications for all commodities, currently
							notifications works for EP&NG only
******/
CREATE  PROCEDURE [dbo].[SR_SAD_GET_SM_ALL_CONTRACT_NOTIFICATIONS_P]
      ( 
       @user_id VARCHAR(10)
      ,@month_identifier DATETIME )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE
            @EP_commodity_type_id INT
           ,@NG_commodity_type_id INT
           ,@Account_Service_Level_Cd INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Utility INT
           ,@month_end_identifier DATETIME
           ,@Bid_Status_Type_Id INT
  
      DECLARE @t_contract_notification TABLE
            ( 
             vendor_name VARCHAR(200)
            ,client_name VARCHAR(200)
            ,site_name VARCHAR(500)
            ,ACCOUNT_ID INT
            ,account_number VARCHAR(50)
            ,contract_id INT
            ,Analyst_Mapping_Cd INT
            ,Commodity_Id INT
            ,Account_Vendor_Id INT
            ,Client_Id INT
            ,Site_Id INT )
  
      SELECT
            @EP_commodity_type_id = max(case WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                        END)
           ,@NG_commodity_type_id = max(case WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                        END)
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )    

      SET @month_end_identifier = dateadd(mm, 1, @month_identifier) - 1    
        
      SELECT
            @Utility = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.entity_type = 111
            AND e.entity_name = 'Utility'       
                   
      SELECT
            @Account_Service_Level_Cd = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'D'
            AND e.ENTITY_DESCRIPTION = 'Other'
            AND e.ENTITY_TYPE = 708  
  
  
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'
            
      SELECT
            @Bid_Status_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'BID_STATUS'
            AND ENTITY_NAME = 'Closed';
      
      INSERT      INTO @t_contract_notification
                  ( 
                   vendor_name
                  ,client_name
                  ,site_name
                  ,ACCOUNT_ID
                  ,account_number
                  ,contract_id
                  ,Analyst_Mapping_Cd
                  ,Commodity_Id
                  ,Account_Vendor_Id
                  ,Client_Id
                  ,Site_Id )
                  SELECT
                        ven.vendor_name
                       ,ch.client_name
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' Site_Name
                       ,utilacc.ACCOUNT_ID
                       ,utilacc.account_number
                       ,dbo.SR_SAD_FN_GET_ALERT_CONTRACT(met.meter_id, @month_identifier, @month_end_identifier) AS contract_id
                       ,coalesce(utilacc.Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,con.COMMODITY_TYPE_ID Commodity_Id
                       ,utilacc.VENDOR_ID Account_Vendor_Id
                       ,ch.Client_Id
                       ,ch.Site_Id
                  FROM
                        dbo.account utilacc
                        JOIN dbo.meter met
                              ON met.account_id = utilacc.account_id
                        JOIN dbo.supplier_account_meter_map meterMap
                              ON meterMap.meter_id = met.meter_id
                                 AND meterMap.is_history = 0
                        JOIN dbo.account suppacc
                              ON meterMap.account_id = suppacc.account_id
                        JOIN dbo.contract con
                              ON meterMap.contract_id = con.contract_id
                        JOIN dbo.VENDOR ven
                              ON utilacc.VENDOR_ID = ven.VENDOR_ID
                        JOIN core.client_Hier ch
                              ON ch.site_Id = utilacc.Site_Id
                  WHERE
                        utilacc.not_managed = 0
                        AND utilacc.SERVICE_LEVEL_TYPE_ID != @Account_Service_Level_Cd
                        AND con.contract_end_date BETWEEN @month_identifier
                                                  AND     @month_end_identifier
                        AND utilacc.account_type_id = @Utility
                        --AND con.COMMODITY_TYPE_ID IN ( @EP_commodity_type_id, @NG_commodity_type_id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.SR_RFP_ACCOUNT AS rfp_account
                                          JOIN dbo.SR_RFP rfp
                                                ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID
                                         WHERE
                                          rfp_account.Account_Id = utilacc.Account_Id
                                          AND con.COMMODITY_TYPE_ID = rfp.COMMODITY_TYPE_ID
                                          AND rfp_account.IS_DELETED = 0
                                          AND rfp_account.BID_STATUS_TYPE_ID != @Bid_Status_Type_Id )
                  GROUP BY
                        ven.VENDOR_NAME
                       ,ch.Client_Name
                       ,ch.city
                       ,ch.state_name
                       ,ch.site_name
                       ,utilacc.ACCOUNT_ID
                       ,utilacc.ACCOUNT_NUMBER
                       ,utilacc.Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,con.COMMODITY_TYPE_ID
                       ,utilacc.VENDOR_ID
                       ,met.meter_id
                       ,ch.Client_Id
                       ,ch.Site_Id;
      WITH  cte_Account_User
              AS ( SELECT
                        acc.vendor_name
                       ,acc.client_name
                       ,acc.site_name
                       ,acc.account_number
                       ,acc.contract_id
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN vcam.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                   FROM
                        @t_contract_notification acc
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = acc.Client_Id
                                 AND ccc.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.COMMODITY_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id)
            SELECT
                  cau.vendor_name
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_number
                 ,con.ed_contract_number
                 ,con.contract_id
                 ,con.contract_end_date
            FROM
                  cte_Account_User cau
                  INNER JOIN dbo.SR_SA_SM_MAP sm
                        ON cau.Analyst_Id = sm.SOURCING_ANALYST_ID
                  INNER JOIN dbo.CONTRACT con
                        ON cau.contract_id = con.CONTRACT_ID
            WHERE
                  sm.SOURCING_MANAGER_ID = @user_id
                  OR cau.Analyst_Id = @user_id
            GROUP BY
                  cau.client_name
                 ,cau.site_name
                 ,cau.account_number
                 ,cau.vendor_name
                 ,con.ed_contract_number
                 ,con.contract_id
                 ,con.contract_end_date


END;
;

;
GO



GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SM_ALL_CONTRACT_NOTIFICATIONS_P] TO [CBMSApplication]
GO
