SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Deal_Ticket_Volume_Details_Del  

DESCRIPTION: It Deletes Rm deal ticket volume details for Selected Rm Deal Ticket Volume Details Id.
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_Deal_Ticket_Volume_Details_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Rm_Deal_Ticket_Volume_Details_Del  280682
	ROLLBACK TRAN

	SELECT TOP 10
		Rm_Deal_Ticket_Volume_Details_Id
	FROM
		dbo.Rm_deal_ticket_volume_details rdtv
		JOIN rm_deal_ticket_details rdtd
			 ON rdtv.Rm_Deal_Ticket_Details_Id = rdtd.Rm_Deal_Ticket_Details_Id
	WHERE
		rdtd.rm_deal_ticket_id = 106703

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			31-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Deal_Ticket_Volume_Details_Del
    (
      @Rm_Deal_Ticket_Volume_Details_Id		INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
	FROM
		dbo.Rm_Deal_Ticket_Volume_Details
	WHERE
		Rm_Deal_Ticket_Volume_Details_Id = @Rm_Deal_Ticket_Volume_Details_Id

END
GO
GRANT EXECUTE ON  [dbo].[Rm_Deal_Ticket_Volume_Details_Del] TO [CBMSApplication]
GO
