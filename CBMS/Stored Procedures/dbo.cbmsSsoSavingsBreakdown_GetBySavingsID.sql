SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoSavingsBreakdown_GetBySavingsID

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@savings_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE       PROCEDURE [dbo].[cbmsSsoSavingsBreakdown_GetBySavingsID]
	( @MyAccountId int
	, @savings_id int
	
	)
AS
BEGIN

	  
	   select sso_savings_detail_id
		, sso_savings_id
		, service_month
		, estimated_savings_value
		, actual_savings_value
	    from sso_savings_detail
	   where sso_savings_id = @savings_id
	order by service_month
END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoSavingsBreakdown_GetBySavingsID] TO [CBMSApplication]
GO
