SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		[dbo].[User_Audit_Log_Sel_By_User_Info_Id]

DESCRIPTION:

INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 500 AND ENTITY_ID = 1039

	SELECT * FROM dbo.ENTITY_AUDIT WHERE ENTITY_ID = 1039
	EXEC dbo.User_Audit_Log_Sel_By_User_Info_Id 37161
	EXEC dbo.User_Audit_Log_Sel_By_User_Info_Id 49
	

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	Created MAINT-3304 Move User to History CBMS enhancement


******/

CREATE PROCEDURE [dbo].[User_Audit_Log_Sel_By_User_Info_Id] ( @User_Info_Id AS INT )
AS 
BEGIN
    
      SET NOCOUNT ON;
    
    
      SELECT
            ui.FIRST_NAME + ' ' + ui.LAST_NAME AS [User]
           ,convert(VARCHAR(20), ea.MODIFIED_DATE, 101) AS [Date]
           ,case ea.AUDIT_TYPE
              WHEN 1 THEN 'Created'
              WHEN 2 THEN 'Updated'
              WHEN 3 THEN 'Deleted'
            END AS [Event]
      FROM
            dbo.ENTITY_AUDIT ea
            INNER JOIN dbo.USER_INFO ui
                  ON ea.USER_INFO_ID = ui.USER_INFO_ID
      WHERE
            ea.ENTITY_ID = 1039
            AND ea.ENTITY_IDENTIFIER = @User_Info_Id
END

;
GO
GRANT EXECUTE ON  [dbo].[User_Audit_Log_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
