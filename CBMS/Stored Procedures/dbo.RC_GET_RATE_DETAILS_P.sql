SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.RC_GET_RATE_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rateId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE [dbo].[RC_GET_RATE_DETAILS_P] 
@rateId integer 

AS
set nocount on
	select RATE_ID, RATE_NAME, RATE_REQUIREMENTS from RATE 
	
	where RATE_ID = @rateId
GO
GRANT EXECUTE ON  [dbo].[RC_GET_RATE_DETAILS_P] TO [CBMSApplication]
GO
