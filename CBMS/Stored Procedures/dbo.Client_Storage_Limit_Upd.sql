SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Client_Storage_Limit_Upd

DESCRIPTION:
		This procedure is used to Update the data into Client_Storage_Limit table
      
INPUT PARAMETERS:
	Name						DataType          Default     Description
------------------------------------------------------------------------------------------------------------------------



OUTPUT PARAMETERS:
	Name              DataType          Default     Description
------------------------------------------------------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------------------------------------------------------

 
		    
AUTHOR INITIALS:
	Initials		 Name			 Date
------------------------------------------------------------------------------------------------------------------------
	KVK				VInay K
                  
MODIFICATIONS
	Initials    Date        Modification
------------------------------------------------------------------------------------------------------------------------
	KVK			10/2/2014	track client usage
******/

CREATE PROCEDURE [dbo].[Client_Storage_Limit_Upd]
      (
       @Client_Id INT
      ,@Image_Type_Id INT
      ,@image_Size DECIMAL(32, 16)
      ,@UOM_Cd INT = 100714
      ,@Updated_User_Id INT = 49 )
AS
BEGIN

      SET NOCOUNT ON;

      UPDATE
            csl
      SET
            Storage_Space_Used = CASE WHEN Storage_Space_Used + @image_Size * guc.Conversion_Factor IS NULL THEN Storage_Space_Used
                                      ELSE Storage_Space_Used + @image_Size * guc.Conversion_Factor
                                 END
           ,Last_Change_Ts = getdate()
           ,Updated_User_Id = @Updated_User_Id
      FROM
            dbo.Client_Storage_Limit AS csl
            JOIN dbo.General_UOM_Conversion AS guc
                  ON guc.Converted_UOM_Cd = csl.Storage_UOM_Cd
                     AND guc.Base_UOM_Cd = @UOM_Cd
      WHERE
            csl.Client_Id = @Client_Id
            AND csl.Image_Type_Id = @Image_Type_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Client_Storage_Limit_Upd] TO [CBMSApplication]
GO
