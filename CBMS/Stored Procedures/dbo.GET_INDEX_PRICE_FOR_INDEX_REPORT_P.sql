SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.GET_INDEX_PRICE_FOR_INDEX_REPORT_P
@userId varchar,
@sessionId varchar,
@priceIndexId int,
@fromDate Varchar(12),
@toDate Varchar(12)

as
	set nocount on
select	CONVERT (Varchar(12), priceindex.INDEX_MONTH, 101)  MONTH_IDENTIFIER,
	 CAST(priceindex.INDEX_VALUE as decimal(8,3)) INDEX_PRICE

from	PRICE_INDEX_VALUE priceindex

where	priceindex.PRICE_INDEX_ID=@priceIndexId AND
	priceindex.INDEX_MONTH BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
GO
GRANT EXECUTE ON  [dbo].[GET_INDEX_PRICE_FOR_INDEX_REPORT_P] TO [CBMSApplication]
GO
