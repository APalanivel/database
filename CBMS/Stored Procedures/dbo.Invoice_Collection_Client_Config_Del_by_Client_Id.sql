SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Invoice_Collection_Client_Config_Del_by_Client_id

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@userId				VARCHAR(10)	        
  
	@Is_History			BIT				0		If 1, get the history
												If 0, get the present
	@SortColumn			VARCHAR(200)	'duedate,clientName,utility'
    @Sortindex			VARCHAR(20)		'Asc'
	@Total_RowCount		INT				0
	@Start_Index		INT				1
	@End_Index			INT				2147483647
        	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC Invoice_Collection_Client_Config_Del_by_Client_id 235


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV			Ravi Kumar Vegesna
	         


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV         2017-05-23  Created as Part of Maint-5180.			

******/



CREATE PROCEDURE [dbo].[Invoice_Collection_Client_Config_Del_by_Client_Id] ( @Client_Id INT )
AS 
BEGIN                      
      SET NOCOUNT ON       
      
      DECLARE @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients TABLE
            ( 
             CLIENT_ID INT
            ,Invoice_Collection_Client_Config_Id INT )

      INSERT      INTO @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients
                  ( 
                   CLIENT_ID
                  ,Invoice_Collection_Client_Config_Id )
                  SELECT
                        c.CLIENT_ID
                       ,ic.Invoice_Collection_Client_Config_Id
                  FROM
                        client c
                        INNER JOIN dbo.Invoice_Collection_Client_Config ic
                              ON ic.Client_Id = c.CLIENT_ID
                  WHERE
                        c.CLIENT_ID = @Client_Id
                  GROUP BY
                        c.CLIENT_ID
                       ,ic.Invoice_Collection_Client_Config_Id
                 
      DELETE
            icli
      FROM
            dbo.Invoice_Collection_LOA_Image icli
            INNER JOIN dbo.Invoice_Collection_LOA icl
                  ON icl.Invoice_Collection_LOA_Id = icli.Invoice_Collection_LOA_Id
            INNER JOIN @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients iccfn
                  ON iccfn.Invoice_Collection_Client_Config_Id = icl.Invoice_Collection_Client_Config_Id
            
      DELETE
            icl
      FROM
            dbo.Invoice_Collection_LOA icl
            INNER JOIN @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients iccfn
                  ON iccfn.Invoice_Collection_Client_Config_Id = icl.Invoice_Collection_Client_Config_Id
            
            
      DELETE
            icac
      FROM
            dbo.Invoice_Collection_Client_Config icac
            INNER JOIN @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients iccfn
                  ON iccfn.Invoice_Collection_Client_Config_Id = icac.Invoice_Collection_Client_Config_Id
                 
                 
      DELETE
            ccm
      FROM
            Client_Contact_Map ccm
            INNER JOIN @Invoice_Collection_Client_Config_Id_For_Not_Managed_Clients iccfn
                  ON iccfn.CLIENT_ID = ccm.CLIENT_ID       
END 
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Client_Config_Del_by_Client_Id] TO [CBMSApplication]
GO
