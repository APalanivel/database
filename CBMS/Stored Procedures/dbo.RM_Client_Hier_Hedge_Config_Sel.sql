SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Client_Hier_Hedge_Config_Sel                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name       DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id      INT  
    @Commodity_Id     INT    NULL  
    @Country_Id      VARCHAR(MAX) NULL  
    @State_Id      INT    NULL  
    @Division_Id   INT    NULL  
    @Site_Str      VARCHAR(50)  NULL  
    @Site_Not_Managed    BIT    NULL  
    @Apply_Default_Config   BIT    NULL  
    @RM_Forecast_UOM_Type_Id  VARCHAR(MAX) NULL  
    @Config_Start_Dt    DATE   NULL  
    @Config_End_Dt     DATE   NULL  
    @Hedge_Type_Id     VARCHAR(MAX) NULL  
    @RM_Risk_Tolerance_Category_Id VARCHAR(MAX) NULL  
    @Risk_Manager_User_Info_Id  VARCHAR(MAX) NULL  
    @Client_Contact_Info_Id   VARCHAR(MAX) NULL  
    @Include_In_Reports    BIT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 235  
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 218  
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 235,@StartIndex = 1,@EndIndex = 5,@Site_Str = 474124  
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 235,@Config_Start_Dt = '2018-08-02'  
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 235,@Config_Start_Dt = '2018-01-01',@Config_End_Dt = '2018-12-31'  
			EXEC dbo.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 11236  
                      
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
	PR			Pramod Reddy V       
                         
 Modifications:                        
    Initials	Date           Modification                        
--------------------------------------------------------------------------------  
    RR			08-08-2018     Global Risk Management - Created   
	PR          28-08-2018		Added Site_Not_Managed Flag            
                       
******/
CREATE PROCEDURE [dbo].[RM_Client_Hier_Hedge_Config_Sel]
      ( 
       @Client_Id INT
      ,@Commodity_Id INT = NULL
      ,@Country_Id VARCHAR(MAX) = NULL
      ,@State_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Site_Str VARCHAR(MAX) = NULL
      ,@Site_Not_Managed BIT = NULL
      ,@Apply_Default_Config BIT = NULL
      ,@RM_Forecast_UOM_Type_Id VARCHAR(MAX) = NULL
      ,@Config_Start_Dt DATE = NULL
      ,@Config_End_Dt DATE = NULL
      ,@Hedge_Type_Id VARCHAR(MAX) = NULL
      ,@RM_Risk_Tolerance_Category_Id VARCHAR(MAX) = NULL
      ,@Risk_Manager_User_Info_Id VARCHAR(MAX) = NULL
      ,@Client_Contact_Info_Id VARCHAR(MAX) = NULL
      ,@Include_In_Reports BIT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647
      ,@RM_Group_Id INT = NULL )
AS 
BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Client_Client_Hier_Id INT
            , @RM_Default_Config_Start_Dt DATE
            , @RM_Default_Config_End_Dt DATE
            , @Module_Cd INT;

        CREATE TABLE #Configs
             (
                 Client_Id INT
                 , RM_Client_Hier_Onboard_Id INT
                 , Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Sitegroup_Id INT
                 , Commodity_Id INT
                 , Commodity_Name VARCHAR(50)
                 , Country_Id INT
                 , COUNTRY_NAME VARCHAR(200)
                 , Is_Default BIT
                 , RM_Client_Hier_Hedge_Config_Id INT
                 , RM_Forecast_UOM_Type_Id INT
                 , RM_Forecast_UOM VARCHAR(200)
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Hedge_Type_Id INT
                 , Hedge_Type VARCHAR(200)
                 , Max_Hedge_Pct DECIMAL(5, 2)
                 , RM_Risk_Tolerance_Category_Id INT
                 , RM_Risk_Tolerance_Category VARCHAR(100)
                 , Risk_Manager_User_Info_Id INT
                 , Risk_Manager VARCHAR(100)
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Include_In_Reports VARCHAR(5)
                 , Is_Default_Config BIT
                 , Site_Not_Managed BIT
             );

        CREATE TABLE #Configs_Final
             (
                 Client_Id INT
                 , RM_Client_Hier_Onboard_Id INT
                 , Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Sitegroup_Id INT
                 , Commodity_Id INT
                 , Commodity_Name VARCHAR(50)
                 , Country_Id INT
                 , COUNTRY_NAME VARCHAR(200)
                 , Is_Default BIT
                 , RM_Client_Hier_Hedge_Config_Id INT
                 , RM_Forecast_UOM_Type_Id INT
                 , RM_Forecast_UOM VARCHAR(200)
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Hedge_Type_Id INT
                 , Hedge_Type VARCHAR(200)
                 , Max_Hedge_Pct DECIMAL(5, 2)
                 , RM_Risk_Tolerance_Category_Id INT
                 , RM_Risk_Tolerance_Category VARCHAR(100)
                 , Risk_Manager_User_Info_Id INT
                 , Risk_Manager VARCHAR(100)
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Include_In_Reports VARCHAR(5)
                 , Is_Default_Config BIT
                 , Site_Not_Managed BIT
                 , Row_Num INT
                 , Config_Order INT
             );

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
             );

        DECLARE @Total_Cnt INT;

        SELECT
            @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_Start_Dt'
            AND App_Id = -1;

        SELECT
            @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_End_Dt'
            AND App_Id = -1;

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        SELECT
            @Module_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Application Module'
            AND c.Code_Value = 'CBMSRiskManagement';

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = rgd.Group_Participant_Id
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
        WHERE
            (   rgd.Cbms_Sitegroup_Id = @RM_Group_Id
                AND grp.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND chautil.Account_Type = 'Utility');

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
             )
        SELECT
            ch.Site_Id
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
        WHERE
            (   rgd.Cbms_Sitegroup_Id = @RM_Group_Id
                AND grp.Code_Value = 'Site');



        ----------------------To get site own config  
        INSERT INTO #Configs
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Sitegroup_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
                 , Site_Not_Managed
             )
        SELECT
            ch.Client_Id
            , chob.RM_Client_Hier_Onboard_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , ch.Sitegroup_Id
            , chob.Commodity_Id
            , com.Commodity_Name
            , chob.Country_Id
            , con.COUNTRY_NAME
            , 0 AS Is_Default
            , chhc.RM_Client_Hier_Hedge_Config_Id
            , chob.RM_Forecast_UOM_Type_Id
            , uom.ENTITY_NAME AS RM_Forecast_UOM
            , chhc.Config_Start_Dt
            , chhc.Config_End_Dt
            , chhc.Hedge_Type_Id
            , hdgtyp.ENTITY_NAME AS Hedge_Type
            , chhc.Max_Hedge_Pct
            , chhc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
            , chhc.Risk_Manager_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Risk_Manager
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , CASE chhc.Include_In_Reports WHEN 1 THEN 'Yes'
                  ELSE 'No'
              END AS Include_In_Reports
            , chhc.Is_Default_Config
            , ch.Site_Not_Managed
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            LEFT JOIN dbo.ENTITY uom
                ON chob.RM_Forecast_UOM_Type_Id = uom.ENTITY_ID
            INNER JOIN dbo.ENTITY hdgtyp
                ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
            LEFT JOIN Trade.RM_Risk_Tolerance_Category rtc
                ON chhc.RM_Risk_Tolerance_Category_Id = rtc.RM_Risk_Tolerance_Category_Id
            LEFT JOIN dbo.Contact_Info ci
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
            LEFT JOIN dbo.Commodity com
                ON chob.Commodity_Id = com.Commodity_Id
            LEFT JOIN dbo.COUNTRY con
                ON chob.Country_Id = con.COUNTRY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON chhc.Risk_Manager_User_Info_Id = ui.USER_INFO_ID
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND (   @Commodity_Id IS NULL
                    OR  chob.Commodity_Id = @Commodity_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',')
                                   WHERE
                                        chob.Country_Id = CAST(Segments AS INT)))
            AND (   @State_Id IS NULL
                    OR  ch.State_Id = @State_Id)
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @RM_Forecast_UOM_Type_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@RM_Forecast_UOM_Type_Id, ',')
                                   WHERE
                                        chob.RM_Forecast_UOM_Type_Id = CAST(Segments AS INT)))
            AND (   @Config_Start_Dt IS NULL
                    OR  chhc.Config_Start_Dt >= @Config_Start_Dt)
            AND (   @Config_End_Dt IS NULL
                    OR  chhc.Config_End_Dt <= @Config_End_Dt)
            AND (   @Hedge_Type_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Hedge_Type_Id, ',')
                                   WHERE
                                        chhc.Hedge_Type_Id = CAST(Segments AS INT)))
            AND (   @RM_Risk_Tolerance_Category_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@RM_Risk_Tolerance_Category_Id, ',')
                                   WHERE
                                        chhc.RM_Risk_Tolerance_Category_Id = CAST(Segments AS INT)))
            AND (   @Risk_Manager_User_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Risk_Manager_User_Info_Id, ',')
                                   WHERE
                                        chhc.Risk_Manager_User_Info_Id = CAST(Segments AS INT)))
            AND (   @Client_Contact_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Contact_Info_Id, ',')
                                   WHERE
                                        chhc.Contact_Info_Id = CAST(Segments AS INT)))
            AND (   @Include_In_Reports IS NULL
                    OR  chhc.Include_In_Reports = @Include_In_Reports)
            AND (   @Apply_Default_Config IS NULL
                    OR  chhc.Is_Default_Config = @Apply_Default_Config)
            AND (   @Site_Str IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Str, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        ORDER BY
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')';



        ----------------------To get default config                         
        INSERT INTO #Configs
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Sitegroup_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
                 , Site_Not_Managed
             )
        SELECT
            ch.Client_Id
            , chob.RM_Client_Hier_Onboard_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , ch.Sitegroup_Id
            , chob.Commodity_Id
            , com.Commodity_Name
            , chob.Country_Id
            , con.COUNTRY_NAME
            , 1 AS Is_Default
            , chhc.RM_Client_Hier_Hedge_Config_Id
            , chob.RM_Forecast_UOM_Type_Id
            , uom.ENTITY_NAME AS RM_Forecast_UOM
            , chhc.Config_Start_Dt
            , chhc.Config_End_Dt
            , chhc.Hedge_Type_Id
            , hdgtyp.ENTITY_NAME AS Hedge_Type
            , chhc.Max_Hedge_Pct
            , chhc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
            , chhc.Risk_Manager_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Risk_Manager
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , CASE chhc.Include_In_Reports WHEN 1 THEN 'Yes'
                  ELSE 'No'
              END AS Include_In_Reports
            , chhc.Is_Default_Config
            , ch.Site_Not_Managed
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            LEFT JOIN dbo.ENTITY uom
                ON chob.RM_Forecast_UOM_Type_Id = uom.ENTITY_ID
            INNER JOIN dbo.ENTITY hdgtyp
                ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
            LEFT JOIN Trade.RM_Risk_Tolerance_Category rtc
                ON chhc.RM_Risk_Tolerance_Category_Id = rtc.RM_Risk_Tolerance_Category_Id
            LEFT JOIN dbo.Contact_Info ci
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
            LEFT JOIN dbo.Commodity com
                ON chob.Commodity_Id = com.Commodity_Id
            LEFT JOIN dbo.COUNTRY con
                ON chob.Country_Id = con.COUNTRY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON chhc.Risk_Manager_User_Info_Id = ui.USER_INFO_ID
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND (   @Commodity_Id IS NULL
                    OR  chob.Commodity_Id = @Commodity_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',')
                                   WHERE
                                        chob.Country_Id = CAST(Segments AS INT)))
            AND (   @State_Id IS NULL
                    OR  ch.State_Id = @State_Id)
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @RM_Forecast_UOM_Type_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@RM_Forecast_UOM_Type_Id, ',')
                                   WHERE
                                        chob.RM_Forecast_UOM_Type_Id = CAST(Segments AS INT)))
            AND (   @Config_Start_Dt IS NULL
                    OR  chhc.Config_Start_Dt >= @Config_Start_Dt)
            AND (   @Config_End_Dt IS NULL
                    OR  chhc.Config_End_Dt <= @Config_End_Dt)
            AND (   @Hedge_Type_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Hedge_Type_Id, ',')
                                   WHERE
                                        chhc.Hedge_Type_Id = CAST(Segments AS INT)))
            AND (   @RM_Risk_Tolerance_Category_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@RM_Risk_Tolerance_Category_Id, ',')
                                   WHERE
                                        chhc.RM_Risk_Tolerance_Category_Id = CAST(Segments AS INT)))
            AND (   @Risk_Manager_User_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Risk_Manager_User_Info_Id, ',')
                                   WHERE
                                        chhc.Risk_Manager_User_Info_Id = CAST(Segments AS INT)))
            AND (   @Client_Contact_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Contact_Info_Id, ',')
                                   WHERE
                                        chhc.Contact_Info_Id = CAST(Segments AS INT)))
            AND (   @Include_In_Reports IS NULL
                    OR  chhc.Include_In_Reports = @Include_In_Reports)
            AND (   @Apply_Default_Config IS NULL
                    OR  chhc.Is_Default_Config = @Apply_Default_Config)
            AND (   @Site_Str IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Str, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    siteonb.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id)
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        ORDER BY
            RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')';





        INSERT INTO #Configs_Final
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Sitegroup_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
                 , Site_Not_Managed
                 , Row_Num
                 , Config_Order
             )
        SELECT
            Client_Id
            , RM_Client_Hier_Onboard_Id
            , Client_Hier_Id
            , Site_Id
            , Site_name
            , Sitegroup_Id
            , Commodity_Id
            , Commodity_Name
            , Country_Id
            , COUNTRY_NAME
            , Is_Default
            , RM_Client_Hier_Hedge_Config_Id
            , RM_Forecast_UOM_Type_Id
            , RM_Forecast_UOM
            , Config_Start_Dt
            , Config_End_Dt
            , Hedge_Type_Id
            , Hedge_Type
            , Max_Hedge_Pct
            , RM_Risk_Tolerance_Category_Id
            , RM_Risk_Tolerance_Category
            , Risk_Manager_User_Info_Id
            , Risk_Manager
            , Client_Contact_Info_Id
            , Client_Contact
            , Include_In_Reports
            , Is_Default_Config
            , Site_Not_Managed
            , DENSE_RANK() OVER (ORDER BY
                                     Site_name)
            , RANK() OVER (PARTITION BY
                               Site_Id
                           ORDER BY
                               Config_Start_Dt)
        FROM
            #Configs;

        SELECT  @Total_Cnt = MAX(Row_Num)FROM   #Configs_Final;

        SELECT
            cf.Client_Id
            , cf.RM_Client_Hier_Onboard_Id
            , cf.Client_Hier_Id
            , cf.Site_Id
            , cf.Site_name
            , cf.Sitegroup_Id
            , cf.Commodity_Id
            , cf.Commodity_Name
            , cf.Country_Id
            , cf.COUNTRY_NAME
            , cf.Is_Default
            , CASE WHEN LEN(congrp.GROUP_NAME) > 1 THEN LEFT(congrp.GROUP_NAME, LEN(congrp.GROUP_NAME) - 1)
                  ELSE ''
              END + CASE WHEN LEN(congrp.GROUP_NAME) > 1
                              AND   LEN(sitegrp.GROUP_NAME) > 1 THEN ', '
                        ELSE ''
                    END
              + CASE WHEN LEN(sitegrp.GROUP_NAME) > 1 THEN LEFT(sitegrp.GROUP_NAME, LEN(sitegrp.GROUP_NAME) - 1)
                    ELSE ''
                END AS RM_Group
            , cf.RM_Client_Hier_Hedge_Config_Id
            , cf.RM_Forecast_UOM_Type_Id
            , cf.RM_Forecast_UOM
            , cf.Config_Start_Dt
            , NULLIF(cf.Config_End_Dt, @RM_Default_Config_End_Dt) AS Config_End_Dt
            , cf.Hedge_Type_Id
            , cf.Hedge_Type
            , cf.Max_Hedge_Pct
            , cf.RM_Risk_Tolerance_Category_Id
            , cf.RM_Risk_Tolerance_Category
            , cf.Risk_Manager_User_Info_Id
            , cf.Risk_Manager
            , cf.Client_Contact_Info_Id
            , cf.Client_Contact
            , cf.Include_In_Reports
            , cf.Is_Default_Config
            , cf.Site_Not_Managed
            , cf.Row_Num
            , @Total_Cnt AS Total_Cnt
            , cf.Config_Order
        FROM
            #Configs_Final cf
            CROSS APPLY
        (   SELECT
                ISNULL(rm.Group_Name, '') + ', '
            FROM
                Core.Client_Hier_Account chasupp
                INNER JOIN dbo.Cbms_Sitegroup_Participant rgd
                    ON chasupp.Supplier_Contract_ID = rgd.Group_Participant_Id
                INNER JOIN dbo.Code cd
                    ON cd.Code_Id = rgd.Group_Participant_Type_Cd
                INNER JOIN dbo.Cbms_Sitegroup rm
                    ON rgd.Cbms_Sitegroup_Id = rm.Cbms_Sitegroup_Id
            WHERE
                chasupp.Client_Hier_Id = cf.Client_Hier_Id
                AND cd.Code_Value = 'Contract'
                AND chasupp.Account_Type = 'Supplier'
                AND rm.Module_Cd = @Module_Cd
            GROUP BY
                rm.Group_Name
            FOR XML PATH('')) congrp(GROUP_NAME)
            CROSS APPLY
        (   SELECT
                ISNULL(rm.Group_Name, '') + ', '
            FROM
                Core.Client_Hier ch
                INNER JOIN dbo.Cbms_Sitegroup_Participant rgd
                    ON ch.Site_Id = rgd.Group_Participant_Id
                INNER JOIN dbo.Code cd
                    ON cd.Code_Id = rgd.Group_Participant_Type_Cd
                INNER JOIN dbo.Cbms_Sitegroup rm
                    ON rgd.Cbms_Sitegroup_Id = rm.Cbms_Sitegroup_Id
            WHERE
                ch.Client_Hier_Id = cf.Client_Hier_Id
                AND cd.Code_Value = 'Site'
                AND rm.Module_Cd = @Module_Cd
            GROUP BY
                rm.Group_Name
            FOR XML PATH('')) sitegrp(GROUP_NAME)
        WHERE
            cf.Row_Num BETWEEN @StartIndex
                       AND     @EndIndex
        ORDER BY
            cf.Row_Num
            , cf.Config_Start_Dt;

    END;



GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Hedge_Config_Sel] TO [CBMSApplication]
GO
