SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_RETURNING_TO_TARRIF_SWITCH_RATE_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_RFP_GET_RETURNING_TO_TARRIF_SWITCH_RATE_INFO_P 
@userId varchar,
@sessionId varchar

as
set nocount on
select	ENTITY_ID , 
	ENTITY_NAME 

from	ENTITY

where	ENTITY_TYPE=1034

order by ENTITY_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_RETURNING_TO_TARRIF_SWITCH_RATE_INFO_P] TO [CBMSApplication]
GO
