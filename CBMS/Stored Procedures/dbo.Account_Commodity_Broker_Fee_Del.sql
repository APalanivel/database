SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
      dbo.Account_Commodity_Broker_Fee_Del  
     
 DESCRIPTION:     
      Deletes the Broker Fee for a given Account and Commodity.  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
 @Account_Id      INT       
  
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
    Begin TRAN  
 EXEC dbo.Account_Commodity_Broker_Fee_Del 10010  
 End tran  
   
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------   
 AKR  Ashok Kumar Raju  
   
  
 MODIFICATIONS     
 Initials Date  Modification  
------------------------------------------------------------  
 AKR        2012-09-28  Created for POCO  
  
******/  
CREATE  PROCEDURE dbo.Account_Commodity_Broker_Fee_Del
      ( 
       @Account_Id INT
      ,@Commodity_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
      
      
      BEGIN TRY                
            BEGIN TRAN  
        
            DELETE
                  acbf
            FROM
                  dbo.Account_Commodity_Broker_Fee acbf
            WHERE
                  acbf.Account_Id = @Account_Id
                  AND ( @Commodity_Id IS NULL
                        OR acbf.Commodity_Id = @Commodity_Id )  
                    
            UPDATE
                  acc
            SET   
                  acc.Is_Broker_Account = 0
            FROM
                  dbo.ACCOUNT acc
            WHERE
                  @Commodity_Id IS NULL
                  AND acc.ACCOUNT_ID = @Account_Id  
        
            COMMIT TRAN                
      END TRY                
      BEGIN CATCH                
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                
                
            EXEC dbo.usp_RethrowError                
                
      END CATCH                
      
     
END  
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Broker_Fee_Del] TO [CBMSApplication]
GO
