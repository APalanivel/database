SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_ACCOUNT_EXISTENCY_ON_COMMODITY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountId     	int       	          	
	@commodityId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from account
--exec SR_RFP_CHECK_ACCOUNT_EXISTENCY_ON_COMMODITY_P 15416,290

CREATE     PROCEDURE DBO.SR_RFP_CHECK_ACCOUNT_EXISTENCY_ON_COMMODITY_P

@accountId int,
@commodityId int

AS
set nocount on
select count(1)
from account a, rate r, meter m
where m.account_id= a.account_id
	and m.rate_id = r.rate_id
	and a.account_id = @accountId
	and r.commodity_type_id <> @commodityId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_ACCOUNT_EXISTENCY_ON_COMMODITY_P] TO [CBMSApplication]
GO
