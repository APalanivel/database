SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_MAX_NONTIER_DISPLAY_ID_P
	@rateId  INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT MAX(CONVERT(INT,(SUBSTRING(charge_display_id,2,LEN(charge_display_id)))))
	FROM dbo.charge_master cm INNER JOIN dbo.Entity e ON e.entity_id = cm.charge_type_id
	WHERE cm.charge_parent_id= @rateId
		AND cm.charge_parent_type_id = 96
		AND e.entity_name ='NonTier'
		AND e.entity_type =113

END
GO
GRANT EXECUTE ON  [dbo].[GET_MAX_NONTIER_DISPLAY_ID_P] TO [CBMSApplication]
GO
