SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Rm_OnBoard_Hedge_Setup_Del]  

DESCRIPTION:

	It Deletes Rm_OnBoard_Hedge_Setup for the given Rm_Onboard_hedge_Setup
      
INPUT PARAMETERS:          
	NAME						DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rm_OnBoard_Hedge_Setup_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Rm_OnBoard_Hedge_Setup_Del 2005
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_OnBoard_Hedge_Setup_Del
    (
	   @Rm_OnBoard_Hedge_Setup_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
	FROM
		dbo.Rm_OnBoard_Hedge_Setup
	WHERE
		RM_ONBOARD_HEDGE_SETUP_ID = @Rm_OnBoard_Hedge_Setup_Id

END
GO
GRANT EXECUTE ON  [dbo].[Rm_OnBoard_Hedge_Setup_Del] TO [CBMSApplication]
GO
