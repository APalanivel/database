SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
              
NAME: Bucket_Category_Rule_Sel_By_Invoice_Account_For_days

DESCRIPTION:              
              
	To get the bucket category rule of aggregation level InvoiceDays for the given Invoice and account id

INPUT PARAMETERS:                        
NAME					DATATYPE DEFAULT  DESCRIPTION                        
------------------------------------------------------------                        
@Cu_Invoice_Id			INT              
@Account_Id				INT

OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
                     
------------------------------------------------------------                        
USAGE EXAMPLES:                        
------------------------------------------------------------                

    EXEC dbo.Bucket_Category_Rule_Sel_By_Invoice_Account_For_days 
      @Cu_Invoice_Id = 6618163
     ,@Account_Id = 441747

    EXEC dbo.Bucket_Category_Rule_Sel_By_Invoice_Account_For_days 
      @Cu_Invoice_Id = 6618166
     ,@Account_Id = 178680

    SELECT TOP 10 * FROM CU_INVOICE_SERVICE_MONTH WHERE YEAR(SERVICE_MONTH) = 2012

AUTHOR INITIALS:                        
INITIALS	NAME
------------------------------------------------------------                        
PKY			Pavan K Yadalam              

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PKY			2013-03-14	Created for Calendarization requirement

*/

CREATE PROCEDURE dbo.Bucket_Category_Rule_Sel_By_Invoice_Account_For_days
      (
       @CU_Invoice_id INT
      ,@Account_Id INT )
AS
BEGIN  
  
      SET NOCOUNT ON; 
      DECLARE @Invoice_Commodity TABLE
            ( 
             Commodity_Id INT PRIMARY KEY CLUSTERED )
      
      INSERT      INTO @Invoice_Commodity
                  ( 
                   Commodity_Id )
                  SELECT
                        det.COMMODITY_TYPE_ID
                  FROM
                        dbo.CU_INVOICE_DETERMINANT det
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                              ON det.CU_INVOICE_DETERMINANT_ID = da.CU_INVOICE_DETERMINANT_ID
                  WHERE
                        det.CU_INVOICE_ID = @CU_Invoice_id
                        AND da.ACCOUNT_ID = @Account_Id
                  GROUP BY
                        det.COMMODITY_TYPE_ID

      INSERT      INTO @Invoice_Commodity
                  ( 
                   Commodity_Id )
                  SELECT
                        chg.COMMODITY_TYPE_ID
                  FROM
                        dbo.CU_INVOICE_CHARGE chg
                        INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT chga
                              ON chg.CU_INVOICE_CHARGE_ID = chga.CU_INVOICE_CHARGE_ID
                  WHERE
                        chg.CU_INVOICE_ID = @CU_Invoice_id
                        AND chga.ACCOUNT_ID = @Account_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Invoice_Commodity ic
                                         WHERE
                                          ic.Commodity_Id = chg.COMMODITY_TYPE_ID )
                  GROUP BY
                        chg.COMMODITY_TYPE_ID

      SELECT
            bmc.Bucket_Name AS Category_Bucket_Name
           ,bms.Bucket_Name AS Child_Bucket_Name
           ,r.Category_Bucket_Master_Id
           ,r.Child_Bucket_Master_Id
           ,r.Priority_Order
           ,aggtyp.Code_Value AS Aggregation_Type
           ,bt.Code_Value AS Bucket_Type
           ,com.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Bucket_Master bmc
                  ON r.Category_Bucket_Master_Id = bmc.Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master bms
                  ON r.Child_Bucket_Master_Id = bms.Bucket_Master_Id
            INNER JOIN @Invoice_Commodity ic
                  ON ic.Commodity_Id = bmc.Commodity_Id
            INNER JOIN dbo.Code agglvl
                  ON r.CU_Aggregation_Level_Cd = agglvl.Code_Id
            INNER JOIN dbo.Code aggtyp
                  ON r.Aggregation_Type_CD = aggtyp.Code_Id
            INNER JOIN dbo.Commodity com
				ON com.Commodity_Id = bmc.Commodity_Id
			INNER JOIN dbo.Code bt
				ON bt.Code_Id = bmc.Bucket_Type_Cd
      WHERE
            agglvl.Code_Value = 'InvoiceDays'
      ORDER BY
            bmc.Bucket_Type_Cd DESC
           ,bmc.Sort_Order
           ,bmc.Bucket_Name
           ,r.Priority_Order

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Category_Rule_Sel_By_Invoice_Account_For_days] TO [CBMSApplication]
GO
