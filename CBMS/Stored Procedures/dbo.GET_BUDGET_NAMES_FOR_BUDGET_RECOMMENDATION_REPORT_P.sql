SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.GET_BUDGET_NAMES_FOR_BUDGET_RECOMMENDATION_REPORT_P 
@userId varchar,
@sessionId varchar,
@clientId int,
@divisionId int,
@siteId int,
@displayStatus int,
@groupId int

as

if @displayStatus=1
 
	select 	RM_BUDGET_ID,
		BUDGET_NAME
		
	from	RM_BUDGET  budget		
	
	where	budget.CLIENT_ID=@clientId AND
		budget.DIVISION_ID is null AND
		budget.SITE_ID is null AND
		budget.RM_GROUP_ID is null AND
		budget.IS_CLIENT_GENERATED=0


else if @displayStatus=2
 
	select 	RM_BUDGET_ID,
		BUDGET_NAME
		
	from	RM_BUDGET  budget			

	where	budget.CLIENT_ID=@clientId AND
		budget.DIVISION_ID =@divisionId AND
		budget.SITE_ID is null AND
		budget.RM_GROUP_ID is null AND
		budget.IS_CLIENT_GENERATED=0


else if @displayStatus=3
 
	select 	RM_BUDGET_ID,
		BUDGET_NAME
		
	from	RM_BUDGET  budget			

	where	budget.CLIENT_ID=@clientId AND
		budget.RM_GROUP_ID =@groupId AND
		budget.DIVISION_ID is null AND
		budget.SITE_ID is null AND
		budget.IS_CLIENT_GENERATED=0


else if @displayStatus=4
 
	select 	RM_BUDGET_ID,
		BUDGET_NAME
		
	from	RM_BUDGET  budget			
	
	where	budget.CLIENT_ID=@clientId AND		
		budget.SITE_ID =@siteId AND
		budget.IS_CLIENT_GENERATED=0
GO
GRANT EXECUTE ON  [dbo].[GET_BUDGET_NAMES_FOR_BUDGET_RECOMMENDATION_REPORT_P] TO [CBMSApplication]
GO
