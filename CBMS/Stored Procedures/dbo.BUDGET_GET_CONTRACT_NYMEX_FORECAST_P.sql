SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- exec BUDGET_GET_CONTRACT_NYMEX_FORECAST_P '','',29719, '11/01/2006', '03/01/2007'
CREATE PROCEDURE dbo.BUDGET_GET_CONTRACT_NYMEX_FORECAST_P
	@user_id varchar(10),
	@session_id varchar(20),
	@contract_id int,
	@contract_start_month datetime, 
	@contract_end_month datetime
	AS
	begin
	set nocount on

	declare @account_id int, @commodity_type_id int
	select top 1 @account_id= account_id, @commodity_type_id = commodity_type_id from budget_contract_vw where contract_id = @contract_id

	select	budget_nymex_forecast.month_identifier,
		budget_nymex_forecast.price
 
	from 	budget_nymex_forecast 
		join account on account.account_id = budget_nymex_forecast.account_id
		and budget_nymex_forecast.account_id = @account_id
		and budget_nymex_forecast.commodity_type_id = @commodity_type_id
		and budget_nymex_forecast.month_identifier between @contract_start_month and @contract_end_month
	end












GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CONTRACT_NYMEX_FORECAST_P] TO [CBMSApplication]
GO
