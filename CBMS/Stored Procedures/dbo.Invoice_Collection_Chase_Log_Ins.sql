SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Chase_Log_Ins           
                  
Description:                  
   This sproc is used to save the chase details.          
                               
 Input Parameters:                  
    Name          DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
  @Invoice_Collection_Queue_Id     VARCHAR(MAX)    
  @Invoice_Collection_Method_Of_Contact_Cd  INT    
  @Invoice_Chase_Comment       NVARCHAR(MAX)    
  @User_Info_Id          INT                          
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
DECLARE @Invoice_Collection_Activity_Id_Out INT     
    
EXEC Invoice_Collection_Chase_Log_Ins     
      @Invoice_Collection_Queue_Id = '143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153'    
     ,@Invoice_Collection_Method_Of_Contact_Cd = NULL    
     ,@Invoice_Chase_Comment = 'test'    
     ,@User_Info_Id = 49    
     ,@Invoice_Collection_Activity_Id_Out = @Invoice_Collection_Activity_Id_Out OUTPUT    
         
SELECT    
      @Invoice_Collection_Activity_Id_Out    
         
       
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna    
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
    RKV    2016-12-29  Created For Invoice_Collection.   
	RKV    2018-05-31  Create new temp table for collecting the distinct Invoice_Collection_Queue_Id's   
	RKV    2019-10-02  Added Is_Not_Default_Vendor column in order by clause.         
                 
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Chase_Log_Ins]
     (
         @Invoice_Collection_Queue_Id VARCHAR(MAX)
         , @Invoice_Collection_Method_Of_Contact_Cd INT = NULL
         , @Invoice_Chase_Comment NVARCHAR(MAX)
         , @User_Info_Id INT
         , @Contact_Level_Cd INT = NULL
         , @Invoice_Collection_Activity_Id_Out INT OUTPUT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @IC_Activity_Cd INT
            , @Invoice_Collection_Activity_Id INT
            , @Invoice_Collection_Chase_Log_Id INT
            , @IC_Chase_Open_Status_Cd INT
            , @loop INT = 1
            , @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT;


        SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                        END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';

        CREATE TABLE #Invoice_Collection_Chase_Contact_Method
             (
                 Invoice_Collection_Method_Of_Contact_Cd INT NOT NULL
                 , Invoice_Collection_Queue_Id INT
                 , Contact_Info_Id INT
                 , Collection_Start_Dt DATE
                 , Collection_End_Dt DATE
                 , sno INT
             );


        CREATE TABLE #Invoice_Collection_Queue
             (
                 Invoice_Collection_Queue_Id INT
             );


        INSERT INTO #Invoice_Collection_Queue
             (
                 Invoice_Collection_Queue_Id
             )
        SELECT
            ufn.Segments
        FROM
            dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
        GROUP BY
            ufn.Segments;


        SELECT
            @IC_Activity_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Activity_Type_Cd'
            AND c.Code_Value = 'Create Chase';

        SELECT
            @IC_Chase_Open_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Chase_Status_Cd'
            AND c.Code_Value = 'Open';



        INSERT INTO #Invoice_Collection_Chase_Contact_Method
             (
                 Invoice_Collection_Method_Of_Contact_Cd
                 , Invoice_Collection_Queue_Id
                 , Contact_Info_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , sno
             )
        SELECT
            @Invoice_Collection_Method_Of_Contact_Cd
            , icq.Invoice_Collection_Queue_Id
            , ci.Contact_Info_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , DENSE_RANK() OVER (ORDER BY
                                     @Invoice_Collection_Method_Of_Contact_Cd
                                     , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN icq.Manual_ICR_Vendor_Id
                                           ELSE ci.Contact_Info_Id
                                       END
                                     , icq.Is_Not_Default_Vendor)
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Queue ufn
                ON ufn.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  ci.Contact_Level_Cd = @Contact_Level_Cd)
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            @Invoice_Collection_Method_Of_Contact_Cd IS NOT NULL
            AND @Contact_Level_Cd IS NOT NULL;


        INSERT INTO #Invoice_Collection_Chase_Contact_Method
             (
                 Invoice_Collection_Method_Of_Contact_Cd
                 , Invoice_Collection_Queue_Id
                 , Contact_Info_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , sno
             )
        SELECT
            ISNULL(aics.Invoice_Source_Method_of_Contact_Cd, -1)
            , icq.Invoice_Collection_Queue_Id
            , ci.Contact_Info_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , DENSE_RANK() OVER (ORDER BY
                                     aics.Invoice_Source_Method_of_Contact_Cd
                                     , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN icq.Manual_ICR_Vendor_Id
                                           ELSE ci.Contact_Info_Id
                                       END
                                     , icq.Is_Not_Default_Vendor)
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Queue ufn
                ON ufn.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                   AND  aics.Is_Primary = 1
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd)))
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            @Invoice_Collection_Method_Of_Contact_Cd IS NULL
            AND @Contact_Level_Cd IS NULL;






        BEGIN TRY
            BEGIN TRAN;


            INSERT INTO dbo.Invoice_Collection_Activity
                 (
                     Invoice_Collection_Activity_Type_Cd
                     , Created_User_Id
                 )
            SELECT  @IC_Activity_Cd, @User_Info_Id;



            SELECT
                @Invoice_Collection_Activity_Id = IDENT_CURRENT('Invoice_Collection_Activity');



            WHILE @loop <= (SELECT  MAX(sno)FROM    #Invoice_Collection_Chase_Contact_Method)
                BEGIN



                    INSERT INTO dbo.Invoice_Collection_Chase_Log
                         (
                             Invoice_Collection_Activity_Id
                             , Invoice_Collection_Method_Of_Contact_Cd
                             , Contact_Info_Id
                             , Contact_Name
                             , Email_Address
                             , Phone_Number
                             , Fax_Number
                             , Invoice_Chase_Comment
                             , Status_Cd
                             , Created_User_Id
                             , Updated_User_Id
                         )
                    SELECT
                        @Invoice_Collection_Activity_Id
                        , MAX(COALESCE(
                                  @Invoice_Collection_Method_Of_Contact_Cd
                                  , icccm.Invoice_Collection_Method_Of_Contact_Cd))
                        , MAX(ci.Contact_Info_Id)
                        , MAX(ci.First_Name + ' ' + ci.Last_Name)
                        , MAX(ci.Email_Address)
                        , MAX(ci.Phone_Number)
                        , MAX(ci.Fax_Number)
                        , @Invoice_Chase_Comment
                        , @IC_Chase_Open_Status_Cd
                        , @User_Info_Id
                        , @User_Info_Id
                    FROM
                        #Invoice_Collection_Chase_Contact_Method icccm
                        LEFT OUTER JOIN dbo.Contact_Info ci
                            ON ci.Contact_Info_Id = icccm.Contact_Info_Id
                    WHERE
                        icccm.sno = @loop
                    GROUP BY
                        icccm.sno;



                    SELECT
                        @Invoice_Collection_Chase_Log_Id = IDENT_CURRENT('Invoice_Collection_Chase_Log');


                    INSERT INTO dbo.Invoice_Collection_Chase_Log_Queue_Map
                         (
                             Invoice_Collection_Chase_Log_Id
                             , Invoice_Collection_Queue_Id
                             , Collection_Start_Dt
                             , Collection_End_Dt
                         )
                    SELECT
                        @Invoice_Collection_Chase_Log_Id
                        , icccm.Invoice_Collection_Queue_Id
                        , icccm.Collection_Start_Dt
                        , icccm.Collection_End_Dt
                    FROM
                        #Invoice_Collection_Chase_Contact_Method icccm
                    WHERE
                        icccm.sno = @loop;





                    SELECT  @loop = @loop + 1;

                END;


            SELECT  @Invoice_Collection_Activity_Id_Out = @Invoice_Collection_Activity_Id;



            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;
        DROP TABLE #Invoice_Collection_Queue;

    END;


    ;





GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Chase_Log_Ins] TO [CBMSApplication]
GO
