SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Vendor_Del  
     
DESCRIPTION: 
	It Deletes	Vendor detail for Selected Vendor Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------         
	@Vendor_Id			INT
    
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Vendor_Del 7069

	ROLLBACK TRAN

	SELECT
		* FROM VENDOR v 
	WHERE Vendor_Type_id = 289
		and not exists(select 1 from account a where a.vendor_id = v.vendor_id)
		and not exists(select 1 from Rate a where a.vendor_id = v.vendor_id)
		and not exists(select 1 from Utility_Detail a where a.vendor_id = v.vendor_id)
		and not exists(select 1 from Vendor_Commodity_Map a where a.vendor_id = v.vendor_id)
		and not exists(select 1 from Vendor_State_Map a where a.vendor_id = v.vendor_id)
	
	
	SELECT * 
	DELETE FROM utility_Detail WHERE vendor_id = 7069
	
	DELETE FROM UTILITY_ADDITIONAL_DETAIL where utility_Detail_id = 6017
	DELETE FROM Vendor_Commodity_Map where vendor_id = 7069
		
	SELECT * FROM ENTITY WHERE ENTITY_DESCRIPTION like 'VEndor%'

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			24-August-10	CREATED

*/
CREATE PROCEDURE dbo.Vendor_Del
    (
       @Vendor_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Vendor
	WHERE
		VENDOR_ID = @Vendor_Id
END
GO
GRANT EXECUTE ON  [dbo].[Vendor_Del] TO [CBMSApplication]
GO
