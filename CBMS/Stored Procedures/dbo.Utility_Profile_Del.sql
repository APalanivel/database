SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Utility_Profile_Del

DESCRIPTION:
	
	Used to delete all the utility_profile for the given utility Vendor_Id and Commodity_id.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@Vendor_Id			INT
	@Commodity_Type_Id	INT

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Utility_Profile_Del 386, 290

	ROLLBACK TRAN

	SELECT
		* FROM VENDOR v 
	WHERE Vendor_Type_id = 289
		and exists(select 1 from Utility_Profile a where a.vendor_id = v.vendor_id)

	SELECT * FROM Utility_Profile WHERE vendor_id = 386

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created

*/
CREATE PROCEDURE dbo.Utility_Profile_Del
    (
       @Vendor_Id			INT
       ,@Commodity_Type_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.Utility_Profile
	WHERE
		Vendor_Id = @Vendor_Id
		AND COMMODITY_TYPE_ID = @Commodity_Type_Id

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Profile_Del] TO [CBMSApplication]
GO
