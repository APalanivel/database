SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Variance_Rule_Dtl_Account_Override_Del]  
     
DESCRIPTION: 
	To Delete Variance_Rule_Dtl_Account_Override_Del for given Account_Id and Variance_Rule_Dtl_Id.

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
-----------------------------------------------------------
@Variance_Rule_Dtl_Id	INT
@Account_Id				INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN

		EXEC Variance_Rule_Dtl_Account_Override_Del 23,	4575

	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Variance_Rule_Dtl_Account_Override_Del
    (
	@Variance_Rule_Dtl_Id	INT
	,@Account_Id			INT
    )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.Variance_Rule_Dtl_Account_Override
	WHERE
		Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
		AND ACCOUNT_ID = @Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[Variance_Rule_Dtl_Account_Override_Del] TO [CBMSApplication]
GO
