SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                                                  
NAME: dbo.cbmsUbmClientMap_GetForClientCode                                                   
                                                
DESCRIPTION:                                                     
                                                
                                               
                                                 
INPUT PARAMETERS:                                                    
Name     DataType Default   Description                                                    
-------------------------------------------------------------------------                                                    
 @MyAccountId		INT,
 @ubm_id			INT,
 @ubm_client_code	VARCHAR(200)                                                     
                                                
OUTPUT PARAMETERS:                                                    
Name     DataType Default   Description                                                    
-------------------------------------------------------------------------                                                    
                                                
USAGE EXAMPLES:                                                    
------------------------------------------------------------                                                    
                                                
EXEC cbmsUbmClientMap_GetForClientCode '49',1,1405                                             
                                              
AUTHOR INITIALS:                                                    
Initials Name                                                    
------------------------------------------------------------                      
NM Nagaraju Muppa                                                
                                                 
MODIFICATIONS                                                     
Initials Date  Modification                                                    
------------------------------------------------------------                                                                                                   
NM      2020-05-04  MAINT-10134  Added client table join,If Client is  not_managed then is_processed as FALSE else Is_processed as TRUE.                                                
******/
CREATE PROCEDURE [dbo].[cbmsUbmClientMap_GetForClientCode]
(
    @MyAccountId INT,
    @ubm_id INT,
    @ubm_client_code VARCHAR(200)
)
AS
BEGIN

    SELECT UCM.UBM_CLIENT_MAP_ID,
           UCM.UBM_ID,
           UCM.UBM_CLIENT_CODE,
           UCM.CLIENT_ID,
           CASE
               WHEN C.NOT_MANAGED = 1 THEN
                   0
               ELSE
                   UCM.IS_PROCESSED
           END AS IS_PROCESSED
    FROM UBM_CLIENT_MAP UCM
        INNER JOIN dbo.CLIENT C
            ON UCM.CLIENT_ID = C.CLIENT_ID
    WHERE UBM_ID = @ubm_id
          AND UBM_CLIENT_CODE = @ubm_client_code;

END;


GO
GRANT EXECUTE ON  [dbo].[cbmsUbmClientMap_GetForClientCode] TO [CBMSApplication]
GO
