SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.DELETE_FORECAST_VOLUME_FOR_OVERRIDE_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int



 AS


	delete from rm_forecast_volume_details where rm_forecast_volume_id in (
		select rm_forecast_volume_id from rm_forecast_volume where client_id=@clientId
	)
	and volume_type_id not in 
		
	(
		select entity_id from entity where entity_type=285 and 
		entity_name in 	('Edit_Actual_Contract','Previous_Edit_Actual_Contract','Manual_Forecast','Previous_Forecast')
	)
GO
GRANT EXECUTE ON  [dbo].[DELETE_FORECAST_VOLUME_FOR_OVERRIDE_P] TO [CBMSApplication]
GO
