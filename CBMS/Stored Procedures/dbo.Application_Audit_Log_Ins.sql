SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Application_Audit_Log_Ins]  
     
DESCRIPTION:

	To Insert Data into Application_Audit_Log

INPUT PARAMETERS:          
NAME					DATATYPE		DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Client_Name			VARCHAR(200)
@Application_Name		VARCHAR(60)
@Audit_Function			SMALLINT
@Table_Name				VARCHAR(255)
@Lookup_Value			XML
@Event_By_User			VARCHAR(81)
@Execution_Ts			DATETIME

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
   	
   	DECLARE @Lookup_Value XML = '<Site Site_Name="Milwaukee WI HQ (MKE)"><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/><ACCOUNT Account_Number="6236-294-096" Account_Id="245110"/></Site>'
   	DECLARE @Current_Ts	 DATETIME = GETDATE()
		
	EXEC dbo.Application_Audit_Log_Ins 'Saint-Gobain Corporation','Delete Account',-1,'ACCOUNT',Lookup_Value,'Jessica Kipper',@Current_Ts
	
	SELECT * FROM application_audit_log
     
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			09/JUL/10	Created

*/

CREATE PROCEDURE dbo.Application_Audit_Log_Ins
(
	 @Client_Name		VARCHAR(200)
	,@Application_Name	VARCHAR(60)
	,@Audit_Function	SMALLINT
	,@Table_Name		VARCHAR(255)
	,@Lookup_Value		XML
	,@Event_By_User		VARCHAR(81)
	,@Execution_Ts		DATETIME	= NULL
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @Execution_Ts = ISNULL(@Execution_Ts, GETDATE())

	INSERT INTO dbo.Application_Audit_Log
	(
		 Client_Name
		,Application_Name
		,Audit_Function
		,Table_Name
		,Lookup_Value
		,Event_By_User
		,Execution_Ts
	)
	VALUES
		(
		 @Client_Name
		,@Application_Name
		,@Audit_Function
		,@Table_Name
		,@Lookup_Value
		,@Event_By_User
		,@Execution_Ts
		)
		
END
GO
GRANT EXECUTE ON  [dbo].[Application_Audit_Log_Ins] TO [CBMSApplication]
GO
