SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_NEWLY_ADDED_SITE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(16)	          	
	@dealTicketId  	int       	          	
	@siteId        	int       	          	
	@hedgeVolume   	real      	          	
	@monthIdentifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE	PROCEDURE DBO.UPDATE_NEWLY_ADDED_SITE_P

@userId varchar(10),
@sessionId varchar(16),
@dealTicketId int,
@siteId int, 
@hedgeVolume real,
@monthIdentifier datetime


AS
set nocount on
declare	@rmDealTicketId int

select @rmDealTicketId = RM_DEAL_TICKET_DETAILS_ID FROM RM_DEAL_TICKET_DETAILS 
	WHERE RM_DEAL_TICKET_id = @dealTicketId and month_identifier = @monthIdentifier

IF @rmDealTicketId<>0

BEGIN
	INSERT INTO RM_DEAL_TICKET_VOLUME_DETAILS 
		(SITE_ID, RM_DEAL_TICKET_DETAILS_ID, HEDGE_VOLUME) 
	values
		(@siteId, @rmDealTicketId, @hedgeVolume)

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_NEWLY_ADDED_SITE_P] TO [CBMSApplication]
GO
