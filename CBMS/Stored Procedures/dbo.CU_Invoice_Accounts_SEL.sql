SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.CU_Invoice_Accounts_SEL      
     
DESCRIPTION:  This will fetch all the accounts for the cu_invoice_id ,to fill the accounts drop down in invoice details section 
    
INPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
@cu_invoice_id		int
       
OUTPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
USAGE EXAMPLES:   
	 EXEC CU_Invoice_Accounts_SEL 671  
	 
	 EXEC CU_Invoice_Accounts_SEL 2928560  
	 
	 EXEC CU_Invoice_Accounts_SEL 75252687
 
  EXEC CU_Invoice_Accounts_SEL 75254062

	 GRANTEXECUTE
------------------------------------------------------------      

AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
NK			Nageswara Rao Kosuri             
SSR			Sharad Srivastava
RKV         Ravi Kumar vegesna
NR			Narayana Reddy

Initials	 Date		Modification      
------------------------------------------------------------      
NK			12/17/2009  Created    
SSR			03/11/2010	Vendor_Name,Account_Number,Entity_name, supplier begin, End date  Added in Result Set
SSR			03/12/2010	Added table Owner name , GROUP BY clause added as we will have same account_id belongs to more than one service month in Cu_Invoice_service_Month table
SKA			05/27/2010	Added the Alias for Column in where clause.
SKA			07/01/2010	Appand the Supplier Begin/End date in case of Supplier account
RKV         2015-06-25  Added State_Id,used core.client_hier_account table instead of Account and vendor tables as part of AS400
NR			2019-10-01	Add Contract - Added Supplier dates filter for supplier account invoice when invoice is not resolved to month

******/
CREATE PROCEDURE [dbo].[CU_Invoice_Accounts_SEL]
      (
      @cu_invoice_id AS INT )
AS
      BEGIN

            SET NOCOUNT ON;

            SELECT
                        cism.Account_ID
                      , cism.CU_INVOICE_ID
                      , cism.Begin_Dt
                      , cism.End_Dt
                      , cism.Billing_Days
                      , cha.Display_Account_Number AS ACCOUNT_NUMBER
                      , cha.Account_Vendor_Name AS VENDOR_NAME
                      , cha.Supplier_Account_begin_Dt
                      , cha.Supplier_Account_End_Dt
                      , ent.ENTITY_NAME
            FROM        dbo.CU_INVOICE_SERVICE_MONTH cism
                        JOIN
                        Core.Client_Hier_Account cha
                              ON cha.Account_Id = cism.Account_ID
                        JOIN
                        dbo.ENTITY ent
                              ON ent.ENTITY_NAME = cha.Account_Type
                                 AND ent.ENTITY_DESCRIPTION = 'Account Type'
            WHERE       cism.CU_INVOICE_ID = @cu_invoice_id
                        AND
                              (     cism.SERVICE_MONTH IS NULL
                                    OR    ((     cha.Account_Type = 'Utility'
                                                 OR
                                                       (     cha.Account_Type = 'Supplier'
                                                             AND
                                                                   (     cism.Begin_Dt
                        BETWEEN cha.Supplier_Account_begin_Dt AND cha.Supplier_Account_End_Dt
                                                                         OR   cism.End_Dt
                        BETWEEN cha.Supplier_Account_begin_Dt AND cha.Supplier_Account_End_Dt
                                                                         OR   cha.Supplier_Account_begin_Dt
                        BETWEEN cism.Begin_Dt AND cism.End_Dt
                                                                         OR   cha.Supplier_Account_End_Dt
                        BETWEEN cism.Begin_Dt AND cism.End_Dt )))))
            GROUP BY    cism.Account_ID
                      , cism.CU_INVOICE_ID
                      , cism.Begin_Dt
                      , cism.End_Dt
                      , cism.Billing_Days
                      , cha.Display_Account_Number
                      , cha.Account_Vendor_Name
                      , cha.Supplier_Account_begin_Dt
                      , cha.Supplier_Account_End_Dt
                      , ent.ENTITY_NAME;

      END;



GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Accounts_SEL] TO [CBMSApplication]
GO
