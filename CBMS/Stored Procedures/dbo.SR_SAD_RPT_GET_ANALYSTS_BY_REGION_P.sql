SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.SR_SAD_RPT_GET_ANALYSTS_BY_REGION_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@regionIds     	varchar(25)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC dbo.SR_SAD_RPT_GET_ANALYSTS_BY_REGION_P '1'
	 EXEC dbo.SR_SAD_RPT_GET_ANALYSTS_BY_REGION_P '0'
	
	select IntValue  from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT(''+@regionIds+'')

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
	
******/

CREATE PROCEDURE dbo.SR_SAD_RPT_GET_ANALYSTS_BY_REGION_P @regionIds VARCHAR(25)
AS 
    BEGIN
        SET NOCOUNT ON ;
        
        IF ( @regionIds = ''
             OR @regionIds = '0'
           ) 
            BEGIN

                SELECT  ui.user_info_id,
                        ui.first_name + SPACE(1) + ui.last_name
                FROM    utility_detail detail,
                        vendor_state_map stateMap,
                        utility_analyst_map analystMap,
                        user_info ui,
                        region reg,
                        state st
                WHERE   ( ui.user_info_id = analystMap.gas_analyst_id
                          OR ui.user_info_id = analystMap.power_analyst_id
                        )
                        AND detail.utility_detail_id = analystMap.utility_detail_id
                        AND detail.vendor_id = stateMap.vendor_id
                        AND stateMap.state_id = st.state_id
                        AND reg.region_id = st.region_id
                GROUP BY ui.user_info_id,
                        ui.first_name + SPACE(1) + ui.last_name
		
            END

        IF ( @regionIds <> '' ) 
            BEGIN
                SELECT  ui.user_info_id,
                        ui.first_name + SPACE(1) + ui.last_name
                FROM    utility_detail detail,
                        vendor_state_map stateMap,
                        utility_analyst_map analystMap,
                        user_info ui,
                        region reg,
                        state st
                WHERE   ( ui.user_info_id = analystMap.gas_analyst_id
                          OR ui.user_info_id = analystMap.power_analyst_id
                        )
                        AND detail.utility_detail_id = analystMap.utility_detail_id
                        AND detail.vendor_id = stateMap.vendor_id
                        AND stateMap.state_id = st.state_id
                        AND reg.region_id = st.region_id
                        AND reg.region_id IN (
                        SELECT  IntValue
                        FROM    dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT(@regionIds) )
                GROUP BY ui.user_info_id,
                        ui.first_name + SPACE(1) + ui.last_name
            END		
    END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_ANALYSTS_BY_REGION_P] TO [CBMSApplication]
GO
