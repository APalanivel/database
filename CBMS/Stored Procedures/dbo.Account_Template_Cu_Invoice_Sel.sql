SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
    dbo.Account_Template_Cu_Invoice_Sel  
 
DESCRIPTION:   
    select * from variance_log  

INPUT PARAMETERS:  
 Name				DataType		Default		Description  
------------------------------------------------------------  
 @Queue_Id			INT                
 @Client_Name		VARCHAR(200)	NULL  
 @City				VARCHAR(200)	NULL  
 @State_Name		VARCHAR(50)		NULL  
 @Vendor_Name		VARCHAR(200)	NULL  
 @Account_Number	VARCHAR(200)	NULL  
 @Site_Name			VARCHAR(200)	NULL 

   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------  
 
	 EXEC dbo.Account_Template_Cu_Invoice_Sel '3779'
	 EXEC dbo.Account_Template_Cu_Invoice_Sel '3779,41581,468388'

------------------------------------------------------------
 AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
RR			Raghu Reddy
 
Initials	Date		Modification
------------------------------------------------------------
RR			2014-06-09  Created Data Operations-2

******/  

CREATE PROCEDURE [dbo].[Account_Template_Cu_Invoice_Sel]
      ( 
       @Account_Ids VARCHAR(MAX) )
AS 
BEGIN  
      SET NOCOUNT ON;

      SELECT
            acc.Template_Cu_Invoice_Id
           ,acc.Template_Cu_Invoice_Updated_User_Id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Name
           ,acc.Template_Cu_Invoice_Last_Change_Ts
      FROM
            dbo.ACCOUNT acc
            INNER JOIN dbo.ufn_split(@Account_Ids, ',') ids
                  ON acc.ACCOUNT_ID = ids.Segments
            LEFT JOIN dbo.USER_INFO ui
                  ON acc.Template_Cu_Invoice_Updated_User_Id = ui.USER_INFO_ID
      

END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Template_Cu_Invoice_Sel] TO [CBMSApplication]
GO
