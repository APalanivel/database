SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Invoice_Checklist_Comment_Del]  

DESCRIPTION: It Deletes Invoice Checklist Comment for Selected Invoice Checklist Comment Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@@Invoice_Checklist_Comment_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Invoice_Checklist_Comment_Del 1002
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Invoice_Checklist_Comment_Del
    (
      @Invoice_Checklist_Comment_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
	FROM
		dbo.INVOICE_CHECKLIST_COMMENT
	WHERE
		Invoice_Checklist_Comment_Id = @Invoice_Checklist_Comment_Id

END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Checklist_Comment_Del] TO [CBMSApplication]
GO
