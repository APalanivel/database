SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                     
/******                            
 NAME: [dbo].[Utility_Vendor_Sel_By_State_Commodity]                
                            
 DESCRIPTION:                            
   A smart search of all utility vendors in the system based on Country or state ,commodity ,vendorname                 
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
@Country_Id						INT				NULL        
@State_Id						INT				NULL  
@Commodity_Type_Id				INT  
@Vendor_Name_Str				VARCHAR(200)	NULL  
@Start_Index					INT				 1  
@End_Index						INT				2147483647  
@Total_Row_Count				INT				 0  
              
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
   EXEC [dbo].[Utility_Vendor_Sel_By_State_Commodity]     
      @State_Id = 5    
     ,@Commodity_Type_Id = 290    
     ,@Start_Index = 1    
     ,@End_Index = 25    
     ,@Total_Row_Count = 0  
     ,@Vendor_Name_Str='City'  
           
    
   EXEC [dbo].[Utility_Vendor_Sel_By_State_Commodity]     
      @State_Id = 5    
     ,@Commodity_Type_Id = 290    
     ,@Start_Index = 1    
     ,@End_Index = 25    
     ,@Total_Row_Count = 25         
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 SP                    Sandeep Pigilam              
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                    2014-02-24       Created                    
                           
******/     
CREATE PROCEDURE [dbo].[Utility_Vendor_Sel_By_State_Commodity]
      ( 
       @Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Type_Id INT = NULL
      ,@Vendor_Name_Str VARCHAR(200) = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0 )
AS 
BEGIN      
		SET NOCOUNT ON;
      WITH  Cte_Services
              AS ( SELECT
                        ven.VENDOR_ID
                       ,ven.VENDOR_NAME
                       ,ROW_NUMBER() OVER ( ORDER BY ven.VENDOR_NAME ) Row_Num
                       ,COUNT(1) OVER ( ) AS Total_Row_Count
                   FROM
                        dbo.VENDOR ven
                        INNER JOIN dbo.ENTITY typ
                              ON ven.VENDOR_TYPE_ID = typ.ENTITY_ID
                        INNER JOIN dbo.VENDOR_STATE_MAP map
                              ON map.VENDOR_ID = ven.VENDOR_ID
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP cmap
                              ON cmap.VENDOR_ID = map.VENDOR_ID
                        INNER JOIN dbo.STATE s
                              ON map.STATE_ID = s.STATE_ID
                   WHERE
                        ven.IS_HISTORY = 0
                        AND typ.ENTITY_NAME = 'Utility'
                        AND typ.ENTITY_DESCRIPTION = 'Vendor'
                        AND ( @State_Id IS NULL
                              OR map.STATE_ID = @State_Id )
                        AND ( @Country_Id IS NULL
                              OR s.COUNTRY_ID = @Country_Id )
                        AND ( @Vendor_Name_Str IS NULL
                              OR ven.VENDOR_NAME LIKE '%' + @Vendor_Name_Str + '%' )
                        AND ( @Commodity_Type_Id IS NULL
                              OR cmap.COMMODITY_TYPE_ID = @Commodity_Type_Id )
                   GROUP BY
                        ven.VENDOR_ID
                       ,ven.VENDOR_NAME)
            SELECT
                  cs.VENDOR_ID
                 ,cs.VENDOR_NAME
                 ,cs.Total_Row_Count
            FROM
                  Cte_Services cs
            WHERE
                  cs.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  cs.Row_Num    
                          
    
       
       
END 



;
GO
GRANT EXECUTE ON  [dbo].[Utility_Vendor_Sel_By_State_Commodity] TO [CBMSApplication]
GO
