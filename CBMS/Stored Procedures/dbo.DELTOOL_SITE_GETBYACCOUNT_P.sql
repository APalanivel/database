SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DELTOOL_SITE_GETBYACCOUNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	INT       	          	
	@account_id    	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.DELTOOL_SITE_GETBYACCOUNT_P -1,6789

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/23/2010	Double quotes used to set default value on @Sitename variable to annotate text replaced by Single quote to set quoted identifier on.

	
******/

CREATE PROCEDURE dbo.DELTOOL_SITE_GETBYACCOUNT_P
(
  @MyAccountId INT,
  @account_id  INT

)
AS 
BEGIN

    DECLARE @sitename AS VARCHAR(2000)
    SET @sitename = ''

    SELECT DISTINCT
            @sitename = @sitename + vw.site_name
    FROM    dbo.vwAccountMeter vam
            JOIN dbo.site s ON s.site_id = vam.site_id
            JOIN dbo.division d ON d.division_id = s.division_id
            JOIN dbo.vwSiteName vw ON vw.site_id = s.site_id
    WHERE   vam.account_id = @account_id

    SELECT  @sitename AS sitename

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_SITE_GETBYACCOUNT_P] TO [CBMSApplication]
GO
