SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 	dbo.User_Preference_Sel  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
	Name				DataType	Default		Description  
------------------------------------------------------------  
	@User_Info_Id		INT
    @User_Preference_Cd INT   
         
   
OUTPUT PARAMETERS:  
	 Name		DataType	Default		Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'User Preference keys', @Code_Value = 'RFP Checklist Columns'	
    
    SELECT TOP 10 * FROM dbo.User_Preference  
	
	EXEC dbo.User_Preference_Sel 1,'RFP Checklist Columns'
    
   
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
   
MODIFICATIONS  
  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2016-09-21	MAINT-4236 Created
	
******/
  
CREATE PROCEDURE [dbo].[User_Preference_Sel]
      ( 
       @User_Info_Id INT
      ,@User_Preference_Code_Value VARCHAR(25) )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
      
      DECLARE @User_Preference_Cd INT
      
      SELECT
            @User_Preference_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'User Preference keys'
            AND c.Code_Value = @User_Preference_Code_Value
  
      SELECT
            up.User_Info_Id
           ,up.User_Preference_Cd
           ,up.User_Preference_Value
      FROM
            dbo.User_Preference up
      WHERE
            up.User_Info_Id = @User_Info_Id
            AND up.User_Preference_Cd = @User_Preference_Cd
                 
END;
;
GO
GRANT EXECUTE ON  [dbo].[User_Preference_Sel] TO [CBMSApplication]
GO
