SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_OPEN_COUNT_FOR_DEAL_TICKET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketNumber	varchar(200)	          	
	@clientId      	int       	          	
	@siteId        	int       	          	
	@startDate     	datetime  	          	
	@endDate       	datetime  	          	
	@dealStatusTypeId	int       	          	
	@currentlyWithId	int       	          	
	@startDateFlag 	bit       	          	
	@endDateFlag   	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_OPEN_COUNT_FOR_DEAL_TICKET_P -1,-1,102140,'','','','','','',0,0
	EXEC dbo.GET_OPEN_COUNT_FOR_DEAL_TICKET_P -1,-1,101774,'','','1/1/2005','12/1/2005','','',1,1


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
******/

CREATE PROCEDURE dbo.GET_OPEN_COUNT_FOR_DEAL_TICKET_P
	@userId				VARCHAR(10), 
	@sessionId			VARCHAR(20), 
	@dealTicketNumber	VARCHAR(200),
	@clientId			INT, 
	@siteId				INT, 
	@startDate			DATETIME, 
	@endDate			DATETIME,  
	@dealStatusTypeId	INT, 
	@currentlyWithId	INT, 
	@startDateFlag		BIT, 
	@endDateFlag		BIT 
AS
BEGIN
	
	SET NOCOUNT ON ;
	
	DECLARE @selectClause	VARCHAR(8000)
	DECLARE @fromClause		VARCHAR(8000)
	DECLARE @whereClause	VARCHAR(8000)
	DECLARE @SQLStatement	VARCHAR(8000)
	
	SELECT @selectClause = ' rmdt.rm_deal_ticket_id, COUNT(1) as open_count '
	SELECT @fromClause = ' dbo.RM_DEAL_TICKET rmdt ' +
			     ' LEFT JOIN dbo.SITE s ON (rmdt.SITE_ID = s.SITE_ID) ' + 
			     ' LEFT JOIN dbo.vwSiteName viewSite ON (s.SITE_ID = viewSite.SITE_ID) ' + 
				' LEFT JOIN dbo.DIVISION d ON (rmdt.DIVISION_ID = d.DIVISION_ID) ' +				
				' LEFT JOIN dbo.USER_INFO ui ON (rmdt.QUEUE_ID = ui.QUEUE_ID) ' +
				' LEFT JOIN dbo.GROUP_INFO gi ON (rmdt.QUEUE_ID = gi.QUEUE_ID) ' +
				' LEFT JOIN dbo.ENTITY trans ON (rmdt.DEAL_TRANSACTION_TYPE_ID = trans.ENTITY_ID) ' +
				' LEFT JOIN dbo.RM_GROUP rmgroup ON (rmdt.RM_GROUP_ID = rmgroup.RM_GROUP_ID)   '+
				' JOIN dbo.ENTITY hedge ON (rmdt.HEDGE_LEVEL_TYPE_ID = hedge.ENTITY_ID) ' + 
				' JOIN dbo.CLIENT c ON (rmdt.CLIENT_ID = c.CLIENT_ID) ' + 
				' JOIN dbo.ENTITY e1 ON (rmdt.DEAL_TYPE_ID = e1.ENTITY_ID) ' + 
				' JOIN dbo.RM_DEAL_TICKET_TRANSACTION_STATUS rmdtts ON (rmdt.RM_DEAL_TICKET_ID = rmdtts.RM_DEAL_TICKET_ID) ' + 
				' JOIN dbo.RM_DEAL_TICKET_DETAILS rmdtd on (rmdt.RM_DEAL_TICKET_ID = rmdtd.RM_DEAL_TICKET_ID) ' +
				' JOIN dbo.ENTITY e2 on rmdtd.TRIGGER_STATUS_TYPE_ID = e2.ENTITY_ID ' 
				
	SELECT @whereClause = ' rmdtts.DEAL_TRANSACTION_DATE IN ' + 
				' (SELECT MAX(DEAL_TRANSACTION_DATE) ' +
				' FROM dbo.RM_DEAL_TICKET_TRANSACTION_STATUS  ' +
				' WHERE RM_DEAL_TICKET_ID = rmdt.RM_DEAL_TICKET_ID) ' + 	
				' AND e2.ENTITY_NAME like (''Open'') ' 


	IF @dealTicketNumber <> '' 
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND rmdt.DEAL_TICKET_NUMBER = ' + @dealTicketNumber 
		END 
	
	
	IF @clientId > 0 
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND rmdt.CLIENT_ID = ' + STR(@clientId) 
		END 

	
	IF @siteId > 0 
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND rmdt.SITE_ID = ' + STR(@siteId) 
		END 

	IF @startDateFlag <> 0 AND @endDateFlag <> 0 
		BEGIN
			SELECT @whereClause =  @whereClause + 
				' AND ((rmdt.HEDGE_START_MONTH >= '+''''+ CONVERT ( VARCHAR(10), @startDate,20 ) +''''+ '  AND rmdt.HEDGE_START_MONTH <= ' +''''+ CONVERT ( VARCHAR(10), @endDate, 20) +''''+ ')  ' +
					' OR '+								
				            '(rmdt.HEDGE_END_MONTH >= '+''''+ CONVERT ( VARCHAR(10), @startDate, 20 ) +''''+ '  AND rmdt.HEDGE_END_MONTH <= '+''''+ CONVERT ( VARCHAR(10), @endDate, 20 ) +''''+ ')  ' +
					' OR ' +
				             '(rmdt.HEDGE_START_MONTH <= '+''''+ CONVERT ( VARCHAR(10), @startDate,20 ) +''''+ '  AND rmdt.HEDGE_END_MONTH >= '+''''+ CONVERT ( VARCHAR(10), @startDate, 20) +''''+')  ' +
				             ' OR ' +	 				            
				             '(rmdt.HEDGE_START_MONTH <= '+''''+ CONVERT ( VARCHAR(10), @endDate,20 ) +''''+ ' AND rmdt.HEDGE_END_MONTH >= ' +'''' +CONVERT ( VARCHAR(10), @endDate, 20)+''''+ '))  ' 

		END


	IF @startDateFlag <> 0 AND @endDateFlag = 0 
		BEGIN
			SELECT @whereClause =  @whereClause + 
				' AND rmdt.HEDGE_START_MONTH >= '+''''+ CONVERT ( VARCHAR(10), @startDate, 20 ) +''''
		END

	IF @startDateFlag = 0 AND @endDateFlag <> 0 
		BEGIN
			SELECT @whereClause =  @whereClause + 
				' AND (rmdt.HEDGE_END_MONTH <= '+''''+ CONVERT ( VARCHAR(10), @endDate, 20 ) +''''+ ')'
		END

	IF @dealStatusTypeId > 0 
		BEGIN
			SELECT @whereClause =  @whereClause + 
				' AND rmdtts.DEAL_TRANSACTION_STATUS_TYPE_ID = ' + STR(@dealStatusTypeId) 
		END 

	IF @currentlyWithId > 0 
		BEGIN
			SELECT @whereClause =  @whereClause + 
				' AND ui.USER_INFO_ID =' + STR(@currentlyWithId) +  
				' AND rmdt.QUEUE_ID = ui.QUEUE_ID ' 
		END 

	SELECT @SQLStatement =	'SELECT ' + @selectClause + 'FROM ' + @fromClause + 'WHERE ' + 	@whereClause +
				' GROUP BY ' +	'rmdt.RM_DEAL_TICKET_ID '

	EXEC(@SQLStatement)
	
END
GO
GRANT EXECUTE ON  [dbo].[GET_OPEN_COUNT_FOR_DEAL_TICKET_P] TO [CBMSApplication]
GO
