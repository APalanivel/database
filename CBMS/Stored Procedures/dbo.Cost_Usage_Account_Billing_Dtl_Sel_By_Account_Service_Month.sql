SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.Cost_Usage_Account_Billing_Dtl_Sel_By_Account_Service_Month            
                        
 DESCRIPTION:                        
			To Get billing dtl.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Account_Id                  INT              
 @Service_Month              DATE                           
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[Cost_Usage_Account_Billing_Dtl_Sel_By_Account_Service_Month] 
      @Account_Id = 1148793
     ,@Service_Month = '2016-01-01'
 
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-11       Created for Variance Enhancement Phase II.                
                       
******/                 
                
CREATE PROCEDURE [dbo].[Cost_Usage_Account_Billing_Dtl_Sel_By_Account_Service_Month]
      ( 
       @Account_Id INT
      ,@Service_Month DATE )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      SELECT
            Billing_Days
      FROM
            Cost_Usage_Account_Billing_Dtl
      WHERE
            Account_Id = @Account_Id
            AND service_month = @Service_Month                
                                                                   
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Billing_Dtl_Sel_By_Account_Service_Month] TO [CBMSApplication]
GO
