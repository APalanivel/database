
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P]  
  
DESCRIPTION:  
     Gets the SR_RFP_SUPPLIER_PRICE_COMMENTS details
  
INPUT PARAMETERS:  
Name			DataType	Default Description  
-------------------------------------------------------------------------------------------  
 @userId       VARCHAR
 @sessionId    VARCHAR
 @bidId        INT
    
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P 1,1,931  
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P  1,1,1452893
	EXEC dbo.SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P  1,1,1452896
  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy

MODIFICATIONS	
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2016-04-20	GCS-758 Added Pricing_Comments to select list
*/  

CREATE    PROCEDURE [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@supplierPriceCommentsId INT )
AS 
BEGIN
      SET NOCOUNT ON;
      		
      SELECT
            SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE_ID
           ,PRICE_RESPONSE_COMMENTS
           ,ARCHIVED_ON_DATE
           ,Pricing_Comments
      FROM
            dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
      WHERE
            SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierPriceCommentsId
      ORDER BY
            ARCHIVED_ON_DATE ASC

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_BOC_PRICE_COMMENTS_ARCHIVE_P] TO [CBMSApplication]
GO
