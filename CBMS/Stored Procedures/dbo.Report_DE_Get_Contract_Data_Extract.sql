SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME: dbo.Report_DE_Get_Contract_Data_Extract 
  
DESCRIPTION:   
  this procedure is created to extract Contract data

INPUT PARAMETERS:      
      Name				DataType       Default   Description      
------------------------------------------------------------------------------------------------------------------------      
  
      
OUTPUT PARAMETERS:      
      Name                DataType          Default     Description      
------------------------------------------------------------------------------------------------------------------------     
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------------------------------------------------  
exec Report_DE_Get_Contract_Data_Extract 221,0
  
AUTHOR INITIALS:  
 Initials	Name
------------------------------------------------------------------------------------------------------------------------
	KVK		Vinay K

MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------------------------------------------------------------------
	KVK		02/11/2020		Created
******/
CREATE PROCEDURE [dbo].[Report_DE_Get_Contract_Data_Extract]
(
    @Client_Id INT,
    @Is_Active_Contracts BIT = 1
)
AS
BEGIN
    SET NOCOUNT ON;

    SELECT c.CONTRACT_ID Contract_Id,
           c.COMMODITY_TYPE_ID Commodity_Id,
           cha_Supplier.Account_Id AS Supplier_Account_Id,
           cha_Utility.Account_Id AS Utility_Account_Id,
           cha_Utility.Client_Hier_Id,
           c.CONTRACT_END_DATE
    INTO #Contract_Dtl
    FROM dbo.CONTRACT c
        INNER JOIN Core.Client_Hier_Account cha_Supplier
            ON cha_Supplier.Supplier_Contract_ID = c.CONTRACT_ID
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha_Supplier.Client_Hier_Id
        INNER JOIN Core.Client_Hier_Account cha_Utility
            ON cha_Supplier.Meter_Id = cha_Utility.Meter_Id
    WHERE ch.Client_Id = @Client_Id
          AND cha_Supplier.Account_Type = 'Supplier'
          AND cha_Utility.Account_Type = 'Utility'
    GROUP BY c.CONTRACT_ID,
             c.COMMODITY_TYPE_ID,
             cha_Supplier.Account_Id,
             cha_Utility.Account_Id,
             cha_Utility.Client_Hier_Id,
             c.CONTRACT_END_DATE;

    SELECT com.Commodity_Name Service,
           c.ED_CONTRACT_NUMBER Contract,
		   CONVERT(DATE,c.Contract_Created_Ts) [Contract Created Date],
           ch.Sitegroup_Name Division,
           ch.Site_name [Site Name],
           ch.Site_Address_Line1 Address,
           ch.City,
           ch.State_Name State,
           sv.VENDOR_NAME [Supplier Name],
           uv.VENDOR_NAME [Utility Name],
           CONVERT(   DATE,
                      CASE
                          WHEN DAY(CONVERT(DATE, c.CONTRACT_START_DATE)) >= 20 THEN
                              DATEADD(m, DATEDIFF(m, 0, DATEADD(m, 1, CONVERT(DATE, c.CONTRACT_START_DATE))), 0)
                          ELSE
                              DATEADD(m, DATEDIFF(m, 0, CONVERT(DATE, c.CONTRACT_START_DATE)), 0)
                      END
                  ) [Contract Start Date],
           CONVERT(
                      DATE,
                      CASE
                          WHEN DAY(CONVERT(DATE, c.CONTRACT_END_DATE)) < 15 THEN
                              DATEADD(
                                         d,
                                         -1,
                                         DATEADD(
                                                    m,
                                                    DATEDIFF(m, 0, DATEADD(m, -1, CONVERT(DATE, c.CONTRACT_END_DATE)))
                                                    + 1,
                                                    0
                                                )
                                     )
                          ELSE
                              CONVERT(DATE, c.CONTRACT_END_DATE)
                      END
                  ) [Contract End Date],
           c.CONTRACT_PRICING_SUMMARY [Contract Pricing Summary]
		   
    FROM #Contract_Dtl t
        JOIN dbo.CONTRACT c
            ON c.CONTRACT_ID = t.Contract_Id
        JOIN dbo.Commodity com
            ON com.Commodity_Id = c.COMMODITY_TYPE_ID
        JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = t.Client_Hier_Id
        JOIN dbo.ACCOUNT sa
            ON sa.ACCOUNT_ID = t.Supplier_Account_Id
        JOIN dbo.VENDOR sv
            ON sv.VENDOR_ID = sa.VENDOR_ID
        JOIN dbo.ACCOUNT ua
            ON ua.ACCOUNT_ID = t.Utility_Account_Id
        JOIN dbo.VENDOR uv
            ON uv.VENDOR_ID = ua.VENDOR_ID
    WHERE (
              @Is_Active_Contracts = 0
              OR
              (
                  @Is_Active_Contracts = 1
                  AND
                  (
                      CONVERT(DATE, c.CONTRACT_START_DATE) < CONVERT(DATE, GETDATE())
                      AND CONVERT(DATE, c.CONTRACT_END_DATE) >= CONVERT(DATE, GETDATE())
                  )
              )
          );

    DROP TABLE #Contract_Dtl;

END;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_Contract_Data_Extract] TO [CBMSApplication]
GO
