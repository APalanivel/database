SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
Name:   dbo.Invoice_Collection_Client_Id_Sel_By_Invoice_collection_Queue_Id                             
                                    
Description:                                    
   This sproc to get the Invoice Collection Client IDs for the specified Queue_Ids                          
                                                 
 Input Parameters:                                    
    Name         DataType        Default   Description                                      
-----------------------------------------------------------------------------------------------------------------                                      
 @tvp_IC_Queue_Id  tvp_Invoice_Collection_Queue                                            
        
 Output Parameters:                                          
    Name        DataType   Default   Description                                      
-----------------------------------------------------------------------------------------------------------------                                      
                                    
 Usage Examples:                                        
-----------------------------------------------------------------------------------------------------------------                                      
                      
   DECLARE          
      @tvp_IC_Queue_Id tvp_Invoice_Collection_Queue          
                 
           
 INSERT     INTO @tvp_IC_Queue_Id          
            ( Invoice_Collection_Queue_id)  
    SELECT  ( 1)             
 UNION   
 Select 2  
 union   
 Select 3  
             
 EXEC dbo.Invoice_Collection_Client_Id_Sel_By_Invoice_collection_Queue_Id        
      @tvp_IC_Queue_Id          
       
                       
                   
                                    
Author Initials:                                    
    Initials  Name                                    
----------------------------------------------------------------------------------------                                      
   SLP    Sri Lakshmi Pallikonda    
    
 Modifications:                                   
    Initials        Date		Modification                                    
----------------------------------------------------------------------------------------                                      
    SLP				2019-09-25 Created to select the Client_Ids by IC Queue_Ids                  
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Client_Id_Sel_by_Invoice_collection_Queue_Id]
(@tvp_IC_Queue_Id tvp_Invoice_Collection_Queue READONLY)
AS
BEGIN
    SELECT ch.Client_Id
    FROM dbo.Invoice_Collection_Queue icq
        JOIN dbo.Invoice_Collection_Account_Config ica
            ON ica.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
        INNER JOIN Core.Client_Hier_Account cha
            ON cha.Account_Id = ica.Account_Id
        INNER JOIN Core.Client_Hier ch
            ON ch.Client_Hier_Id = cha.Client_Hier_Id
    WHERE EXISTS
    (
        SELECT 1
        FROM @tvp_IC_Queue_Id tiqi
        WHERE icq.Invoice_Collection_Queue_Id = tiqi.Invoice_Collection_Queue_Id
    )
    GROUP BY ch.Client_Id;
END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Client_Id_Sel_by_Invoice_collection_Queue_Id] TO [CBMSApplication]
GO
