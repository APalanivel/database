SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_LOAD_PROFILE_STATUS_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_RFP_GET_LOAD_PROFILE_STATUS_INFO_P
@user_id varchar(10),
@session_id varchar(20)

AS
set nocount on
	SELECT 	ENTITY_ID,
			ENTITY_NAME
	
	FROM 		ENTITY 
	
	WHERE	ENTITY_TYPE=1006 
	
	ORDER BY 	ENTITY_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_LOAD_PROFILE_STATUS_INFO_P] TO [CBMSApplication]
GO
