SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE dbo.ADD_SAVINGS_DIVISION_MAP_P
	@saving_category_type_id int,
	@division_id int
	AS
	begin
		set nocount on

		insert into SAVINGS_CATEGORY_DIVISION_MAP 
			    (SAVINGS_CATEGORY_TYPE_ID ,
			     DIVISION_ID)
		values     (@saving_category_type_id,
			    @division_id)

	end



GO
GRANT EXECUTE ON  [dbo].[ADD_SAVINGS_DIVISION_MAP_P] TO [CBMSApplication]
GO
