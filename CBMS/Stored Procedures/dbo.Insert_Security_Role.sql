SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Insert_Security_Role   
   
DESCRIPTION: Insert a row in user_security_Role table.

 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
@user_info_id		INT
@security_role_id	INT
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------   

 
  USAGE EXAMPLES:    
--------------------------------------------------------------------    

  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
 KVK K Vinay Kumar
   
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------    
KVK 12/17/2010	Delete previous role for the user and assign a new role
******************************************************************************************************/

CREATE PROCEDURE [dbo].[Insert_Security_Role]
( 
 @user_info_id INT
,@security_role_id INT )
AS 
BEGIN    
    
    SET NOCOUNT ON ;            
	   
    DELETE
        dbo.User_Security_Role
    WHERE
        User_Info_Id = @user_info_id

    INSERT INTO
        dbo.User_Security_Role
        ( 
         User_Info_Id
        ,Security_Role_Id )
    VALUES
        (
         @user_info_id
        ,@security_role_id )  
       
END

GO
GRANT EXECUTE ON  [dbo].[Insert_Security_Role] TO [CBMSApplication]
GO
