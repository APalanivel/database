
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name						DataType	Default		Description    
----------------------------------------------------------------    
@Contract_Id				INT
@Notification_Msg_Queue_Id	INT				

OUTPUT PARAMETERS:    
 Name   DataType	Default		Description    
----------------------------------------------------------------    
 
USAGE EXAMPLES:    
----------------------------------------------------------------    
     SELECT DISTINCT TOP 10  fvl.* FROM dbo.Contract_Notification_Log fvl 
		JOIN dbo.Notification_Msg_Queue nmq ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
		JOIN dbo.Notification_Template nt ON nmq.Notification_Template_Id = nt.Notification_Template_Id
		JOIN Core.Client_Hier_Account cha ON fvl.Contract_Id = cha.Supplier_Contract_ID 
											AND fvl.Meter_Start_End_Date = cha.Supplier_Meter_Association_Date
		WHERE nt.Notification_Type_Cd = 102194

	SELECT * FROM  dbo.Notification_Template a JOIN dbo.Code c 
		ON a.Notification_Type_Cd = c.Code_Id

	EXEC dbo.Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 124295,8182,26958
	EXEC dbo.Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 120832,8149,21047
	EXEC dbo.Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend 116502,8213,26957

AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------    
RR			Raghu Reddy
NR			Narayana Reddy

MODIFICATIONS:
Initials	Date		Modification    
-----------------------------------------------------------------    
RR			2016-06-06	Global Sourcing - Phase5 - GCS-988 Created	
NR			2016-07-13	GCS - 5b - Added locale code					
RR			2016-07-22	GCS-1179 - Modified to get supplier contact mapped to commodity, state and country  of the expiring contract 
   
******/    
    
CREATE PROCEDURE [dbo].[Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend]
      ( 
       @Contract_Id INT
      ,@Notification_Msg_Queue_Id INT
      ,@Analyst_Id INT = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON;   
      
      DECLARE
            @Analyst_Name VARCHAR(200)
           ,@Analyst_Email_Address VARCHAR(150)
           ,@Phone_Number VARCHAR(50) 
        
      --SELECT
      --      @Execution_Date = ISNULL(@Execution_Date, CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101)));
       
      SELECT
            @Analyst_Id = ISNULL(@Analyst_Id, 49)
            
      DECLARE @Client_Con_Info TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Client_Id INT
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Start_Date DATETIME
            ,Commodity_Type_id INT
            ,Product_Type VARCHAR(25)
            ,Commodity_Name VARCHAR(50) );    
            
      DECLARE @Tbl_Regisrations TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Alternate_Account_Number NVARCHAR(200)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,START_DATE DATETIME
            ,Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,COMMODITY_TYPE_ID INT
            ,Commodity_Name VARCHAR(50)
            ,Product_Type VARCHAR(25) ); 
            
      DECLARE @Tbl_Site_Meter_Number TABLE
            ( 
             Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Site_Meter_Number VARCHAR(MAX)
            ,Contract_Id INT ); 
            
      SELECT
            @Analyst_Name = ui.FIRST_NAME + ' ' + ui.LAST_NAME
           ,@Analyst_Email_Address = ui.EMAIL_ADDRESS
           ,@Phone_Number = ui.PHONE_NUMBER
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @Analyst_Id         
                  
      INSERT      INTO @Client_Con_Info
                  ( 
                   Client_Name
                  ,Client_Id
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,Vendor_Id
                  ,Vendor_Name
                  ,Start_Date
                  ,Commodity_Type_id
                  ,Product_Type
                  ,Commodity_Name )
                  SELECT
                        ch.Client_Name
                       ,ch.Client_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Alternate_Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id AS vendor_id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Association_Date AS Start_Date
                       ,chautil.Commodity_Id AS COMMODITY_TYPE_ID
                       ,Product_Type.Code_Value AS Product_Type
                       ,c.Commodity_Name
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON chasupp.Supplier_Contract_ID = con.CONTRACT_ID
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.VENDOR_ID = chautil.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = chautil.Commodity_Id
                        LEFT JOIN dbo.Code Product_Type
                              ON Product_Type.Code_Id = con.Contract_Product_Type_Cd
                        INNER JOIN dbo.Commodity c
                              ON c.Commodity_Id = chautil.Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        --AND chasupp.Supplier_Meter_Association_Date >= @Execution_Date
                        --AND con.CONTRACT_END_DATE >= @Execution_Date
                        AND con.Is_Notification_Required_On_Registration_Validation = 1
                        AND chasupp.Supplier_Contract_ID = @Contract_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Contract_Notification_Log fvl
                                          INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                                                ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                                     WHERE
                                          fvl.Contract_Id = @Contract_Id
                                          AND fvl.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id
                                          AND fvmd.Meter_Id = chasupp.Meter_Id
                                          AND fvl.Meter_Term_Dt = chasupp.Supplier_Meter_Association_Date )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Client_Id
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Alternate_Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Association_Date
                       ,chautil.Commodity_Id
                       ,Product_Type.Code_Value
                       ,c.Commodity_Name;
                       
      INSERT      INTO @Tbl_Regisrations
                  ( 
                   Client_Name
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Alternate_Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,START_DATE
                  ,Vendor_Id
                  ,Vendor_Name
                  ,COMMODITY_TYPE_ID
                  ,Commodity_Name
                  ,Product_Type )
                  SELECT
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Site_Id
                       ,cn.Account_Number
                       ,cn.Alternate_Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,CONVERT(DATETIME, CONVERT(VARCHAR(10), cn.Start_Date, 101)) AS Start_Date
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Commodity_Type_id
                       ,cn.Commodity_Name
                       ,cn.Product_Type
                  FROM
                        @Client_Con_Info cn
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Core.Client_Hier_Account samesupp
                                     WHERE
                                          samesupp.Meter_Id = cn.Meter_Id
                                          AND samesupp.Account_Vendor_Id = cn.Vendor_Id
                                          AND samesupp.Supplier_Contract_ID <> cn.Contract_Id
                                          AND DATEADD(DAY, 1, samesupp.Supplier_Meter_Disassociation_Date) = cn.Start_Date )
                  GROUP BY
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Site_Id
                       ,cn.Account_Number
                       ,cn.Alternate_Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Commodity_Type_id
                       ,cn.Start_Date
                       ,cn.Product_Type
                       ,cn.Commodity_Name;
                       
      INSERT      INTO @Tbl_Site_Meter_Number
                  ( 
                   Site_Name
                  ,Site_Id
                  ,Site_Meter_Number
                  ,Contract_Id )
                  SELECT
                        cter.Site_Name
                       ,cter.Site_Id
                       ,cter.Site_Name + '~' + LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) + '~' + ISNULL(LEFT(accnum.accnums, LEN(accnum.accnums) - 1), '') + '~' + ISNULL(LEFT(altacc.altaccs, LEN(altacc.altaccs) - 1), '')
                       ,cter.Contract_Id
                  FROM
                        @Tbl_Regisrations cter
                        CROSS APPLY ( SELECT
                                          ct.Meter_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                      GROUP BY
                                          ct.Meter_Number
                                         ,ct.Meter_Id
                        FOR
                                      XML PATH('') ) mtrnum ( mtrnums )
                        CROSS APPLY ( SELECT
                                          ct.Account_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                      GROUP BY
                                          ct.Account_Number
                        FOR
                                      XML PATH('') ) accnum ( accnums )
                        CROSS APPLY ( SELECT
                                          ct.Alternate_Account_Number + ', '
                                      FROM
                                          @Tbl_Regisrations ct
                                      WHERE
                                          cter.Contract_Id = ct.Contract_Id
                                          AND cter.Site_Id = ct.Site_Id
                                          AND ct.Alternate_Account_Number IS NOT NULL
                                      GROUP BY
                                          ct.Alternate_Account_Number
                        FOR
                                      XML PATH('') ) altacc ( altaccs )
                  GROUP BY
                        cter.Site_Name
                       ,cter.Site_Id
                       ,cter.Site_Name + '~' + LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) + '~' + ISNULL(LEFT(accnum.accnums, LEN(accnum.accnums) - 1), '') + '~' + ISNULL(LEFT(altacc.altaccs, LEN(altacc.altaccs) - 1), '')
                       ,cter.Contract_Id;
            
      SELECT
            cter.Contract_Id
           ,cter.Ed_Contract_Number
           ,cter.START_DATE
           ,cter.Client_Name
           ,REPLACE(LEFT(sit.sites, LEN(sit.sites) - 1), '&amp;', '&') AS Site_Name
           ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Id
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) AS Meter_Number
           ,cter.Vendor_Name
           ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1) AS Vendor_Contact_User_Info_Id
           ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1) AS Registration_Contact_Email_Address
           ,@Analyst_Id AS Analyst_Id
           ,@Analyst_Name AS Analyst_Name
           ,@Analyst_Email_Address AS Analyst_Email_Address
           ,@Phone_Number AS Phone_Number
           ,cter.COMMODITY_TYPE_ID
           ,cter.Commodity_Name
           ,cter.Product_Type
           ,LEFT(sitemtr.sitemtrs, LEN(sitemtr.sitemtrs) - 1) AS Site_Meter_Number
           ,LEFT(locale.code, LEN(locale.code) - 1) AS Locale_Cd
      FROM
            @Tbl_Regisrations cter
            CROSS APPLY ( SELECT
                              ct.Site_Name + '^'
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                          GROUP BY
                              ct.Site_Name
            FOR
                          XML PATH('') ) sit ( sites )
            CROSS APPLY ( SELECT
                              CAST(ct.Meter_Id AS VARCHAR(20)) + ','
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Meter_Id IS NOT NULL
                          GROUP BY
                              ct.Meter_Id
            FOR
                          XML PATH('') ) mtr ( mtrs )
            CROSS APPLY ( SELECT
                              ct.Meter_Number + ', '
                          FROM
                              @Tbl_Regisrations ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Meter_Number IS NOT NULL
                          GROUP BY
                              ct.Meter_Number
                             ,ct.Meter_Id
            FOR
                          XML PATH('') ) mtrnum ( mtrnums )
            CROSS APPLY ( SELECT
                              CAST(ssci.USER_INFO_ID AS VARCHAR(20)) + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
                          GROUP BY
                              ssci.USER_INFO_ID
            FOR
                          XML PATH('') ) vcui ( vcuis )
            CROSS APPLY ( SELECT
                              ssci.Registration_Contact_Email_Address + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
                          GROUP BY
                              ssci.Registration_Contact_Email_Address
            FOR
                          XML PATH('') ) tcmail ( tcmails )
            CROSS APPLY ( SELECT
                              ct.Site_Meter_Number + '^'
                          FROM
                              @Tbl_Site_Meter_Number ct
                          WHERE
                              cter.Contract_Id = ct.Contract_Id
                              AND ct.Site_Meter_Number IS NOT NULL
                          GROUP BY
                              ct.Site_Meter_Number
            FOR
                          XML PATH('') ) sitemtr ( sitemtrs )
            CROSS APPLY ( SELECT TOP 1
                              CAST(Code_Id AS VARCHAR(10)) + '~' + cd.Code_Value + ','
                          FROM
                              dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                    ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN dbo.USER_INFO AS vci
                                    ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                              INNER JOIN dbo.Code cd
                                    ON cd.Code_Value = vci.locale_code
                              INNER JOIN dbo.Codeset cs
                                    ON cd.Codeset_Id = cs.Codeset_Id
                              INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                    ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                       AND metermap.STATE_ID = cha.Meter_State_Id
                                       AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                              INNER JOIN @Tbl_Regisrations mtr
                                    ON mtr.Meter_Id = cha.Meter_Id
                          WHERE
                              sscvm.IS_PRIMARY = 1
                              AND cter.Vendor_Id = sscvm.VENDOR_ID
                              AND ssci.Registration_Contact_Email_Address IS NOT NULL
                              AND ssci.Registration_Contact_Email_Address <> ''
                              AND vci.IS_HISTORY = 0
                              AND cs.Codeset_Name = 'LocalizationLanguage'
                              AND metermap.IS_PRIMARY = 0
                              AND cha.Supplier_Contract_ID = cter.Contract_Id
            FOR
                          XML PATH('') ) locale ( code )
      GROUP BY
            cter.Contract_Id
           ,cter.Ed_Contract_Number
           ,cter.START_DATE
           ,cter.Client_Name
           ,REPLACE(LEFT(sit.sites, LEN(sit.sites) - 1), '&amp;', '&')
           ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
           ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1)
           ,cter.Vendor_Name
           ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1)
           ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1)
           ,cter.COMMODITY_TYPE_ID
           ,cter.Commodity_Name
           ,cter.Product_Type
           ,LEFT(sitemtr.sitemtrs, LEN(sitemtr.sitemtrs) - 1)
           ,LEFT(locale.code, LEN(locale.code) - 1);

END;





;
GO

GRANT EXECUTE ON  [dbo].[Contract_Registration_Dtls_Sel_BY_Contract_Msg_Queue_Id_For_Resend] TO [CBMSApplication]
GO
