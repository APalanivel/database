SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

              
/******                        
              
NAME: [DBO].[Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id]                
                   
DESCRIPTION:              
              
 To Get determinant category buckets by invoice and priority              
                    
INPUT PARAMETERS:                        
NAME			 DATATYPE	DEFAULT  DESCRIPTION                        
------------------------------------------------------------                        
@Cu_Invoice_Id	INT              
@Category_Bucket_Master_Id INT
@Priority_Order INT
              
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
                     
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

 EXEC Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id   7530069,101025 ,1

AUTHOR INITIALS:                        
INITIALS NAME                        
------------------------------------------------------------                        
PKY   Pavan K Yadalam              
              
MODIFICATIONS              
INITIALS	DATE		MODIFICATION              
------------------------------------------------------------                        
PKY			25-JUL-11	Created for the additional data Cost/usage aggregation use

*/

CREATE PROCEDURE dbo.Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id
      ( 
       @Cu_Invoice_Id INT
      ,@Category_Bucket_Master_Id INT
      ,@Priority_Order INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            r.Category_Bucket_Master_Id
           ,cd.Code_Value Aggregation_Type_Cd
           ,r.Is_Aggregate_Category_Bucket
           ,r.Child_Bucket_Master_Id
           ,chg.CU_INVOICE_CHARGE_ID
           ,chg.COMMODITY_TYPE_ID
           ,chg.CHARGE_NAME
           ,chg.CHARGE_VALUE DETERMINANT_VALUE
           ,chg.CU_DETERMINANT_CODE
           ,chg.Bucket_Master_Id
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Code aglvlCd
                  ON aglvlCd.Code_id = r.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Code cd
                  ON r.Aggregation_Type_CD = cd.Code_Id
            LEFT OUTER JOIN dbo.CU_INVOICE_CHARGE chg
                  ON r.Child_Bucket_Master_Id = chg.Bucket_Master_Id
                     AND chg.CU_INVOICE_ID = @Cu_Invoice_Id
      WHERE
            aglvlCd.Code_Value = 'Invoice'
            AND r.Category_Bucket_Master_Id = @Category_Bucket_Master_Id
            AND r.Priority_Order = @Priority_Order    

END
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id] TO [CBMSApplication]
GO
