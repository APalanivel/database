SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.CU_Invoice_Service_month_Account_AccountId_UBMCode_UPD
DESCRIPTION:

Procedure is to get UBM_ACCOUNT_CODE of old Account(Account_id) and Update with New Account(Account_ID)
AND to Update Old Account_id of Cu_invoice_service_month with New Account_id

INPUT PARAMETERS:
	Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	@OldAccount_ID	INT
	@NewAccount_ID	INT
	@Cu_invoice_id	INT
	@Service_month	DATE	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:

	EXEC dbo.CU_Invoice_Service_month_Account_AccountId_UBMCode_UPD 141932,141930,2928352,'02/01/2010'
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
SSR		   03/23/2010   Created
SKA		   05/27/2010	Use Error Handling because of multipile update statement
SKA		   06/15/2010	Removed @Service_month from input parameter
						Update UBM_Id as well along with UBM_ACCOUNT_CODE
*/
CREATE PROCEDURE [dbo].[CU_Invoice_Service_month_Account_AccountId_UBMCode_UPD]
      (
       @OldAccount_ID INT
      ,@NewAccount_ID INT
      ,@Cu_invoice_id INT )
AS 
BEGIN

      SET NOCOUNT ON ;
	
      BEGIN TRY
            BEGIN TRAN
            DECLARE
                  @UBM_ACCOUNT_CODE VARCHAR(200)
                 ,@UBM_Id INT
	
            SELECT
                  @UBM_ACCOUNT_CODE = UBM_ACCOUNT_CODE
                 ,@UBM_Id = UBM_ID
            FROM
                  dbo.ACCOUNT
            WHERE
                  ACCOUNT_ID = @OldAccount_ID
	
            UPDATE
                  dbo.CU_INVOICE_SERVICE_MONTH
            SET   
                  Account_ID = @NewAccount_ID
            WHERE
                  CU_INVOICE_ID = @Cu_invoice_id
                  AND Account_ID = @OldAccount_ID

            UPDATE
                  dbo.Account
            SET   
                  UBM_ACCOUNT_CODE = @UBM_ACCOUNT_CODE
                 ,UBM_ID = @UBM_Id
            WHERE
                  ACCOUNT_ID = @NewAccount_ID
  
            UPDATE
                  dbo.Account
            SET   
                  UBM_ACCOUNT_CODE = NULL
            WHERE
                  ACCOUNT_ID = @OldAccount_ID
  
            COMMIT TRAN  
            
      END TRY
	
      BEGIN CATCH
            ROLLBACK TRAN
            EXEC usp_RethrowError	
      END CATCH		
END

GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_month_Account_AccountId_UBMCode_UPD] TO [CBMSApplication]
GO
