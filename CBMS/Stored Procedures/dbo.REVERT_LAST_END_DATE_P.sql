SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.REVERT_LAST_END_DATE_P
	@rateId INT,
	@rsEndDate DATETIME
AS
BEGIN

	SET NOCOUNT ON

	UPDATE rate_schedule 
		SET rs_end_date = @rsEndDate
	WHERE rate_id = @rateId 
		AND rs_end_date = @rsEndDate    

END
GO
GRANT EXECUTE ON  [dbo].[REVERT_LAST_END_DATE_P] TO [CBMSApplication]
GO
