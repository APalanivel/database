SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		Service_Broker_Message_Monitor_Upd

DESCRIPTION:
	Activated by dbo.Service_Broker_Message_Monitor_Queue, it processes messages
	of //Change_Control/Message/SB_Message_Monitor_Upd
	
	Procedure Only Responds with a message of type of //Change_Control/Message/SB_Message_Monitor_Upd


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	,@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed by 
XML Structure:
<Message_Monitor_Upd>
	<Queue_Name></Queue_Name>
	<Message_Type></Message_Type>
	<Conversation_Group_Id></Conversation_Group_Id>
	<Message_Status></Message_Status>
	<Message_Completed_Ts></Message_Completed_Ts>
	<Error_Text></Error_Text>
	<Message_Batch_Count></Message_Batch_Count>
</Message_Monitor_Upd>

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			06/03/2014		Created
******/
CREATE PROCEDURE [dbo].[Service_Broker_Message_Monitor_Upd]
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN
      SET NOCOUNT ON; 
	
/*
<Message_Monitor_Upd>
	<Queue_Name></Queue_Name>
	<Message_Type></Message_Type>
	<Message_Sequence_Number></Message_Sequence_Number>
	<Conversation_Group_Id></Conversation_Group_Id>
	<Conversation_Handle></Conversation_Handle>
	<Message_Status></Message_Status>
	<Message_Completed_Ts></Message_Completed_Ts>
	<Error_Text></Error_Text>
	<Message_Batch_Count></Message_Batch_Count>
</Message_Monitor_Upd>
*/
    
      DECLARE @Queue_Name VARCHAR(200)
      DECLARE @Conversation_Group_Id UNIQUEIDENTIFIER
      DECLARE @Message_Type VARCHAR(200)
      DECLARE @Message_Sequence_Number INT
      DECLARE @CHandle UNIQUEIDENTIFIER
      DECLARE @Message_Status VARCHAR(200)
      DECLARE @Message_Completed_Ts DATETIME
      DECLARE @Error_Text VARCHAR(MAX)
      DECLARE @Message_Batch_Count INT


      SELECT
            @Queue_Name = cng.ch.value('Queue_Name[1]', 'varchar(200)')
           ,@Message_Type = cng.ch.value('Message_Type[1]', 'varchar(200)')
           ,@Message_Sequence_Number = cng.ch.value('Message_Sequence_Number[1]', 'int')
           ,@CHandle = cng.ch.value('Conversation_Handle[1]', 'uniqueidentifier')
           ,@Conversation_Group_Id = cng.ch.value('Conversation_Group_Id[1]', 'uniqueidentifier')
           ,@Message_Status = cng.ch.value('Message_Status[1]', 'varchar(200)')
           ,@Message_Completed_Ts = cng.ch.value('Message_Completed_Ts[1]', 'datetime')
           ,@Error_Text = cng.ch.value('Error_Text[1]', 'varchar(max)')
           ,@Message_Batch_Count = cng.ch.value('Message_Batch_Count[1]', 'int')
      FROM
            @Message.nodes('/Message_Monitor_Upd') cng ( ch );
                        
                        
      UPDATE
            dbo.Service_Broker_Message_Monitor
      SET
            Message_Status = @Message_Status
           ,Error_Text = @Error_Text
           ,Message_Completed_Ts = @Message_Completed_Ts
           ,Message_Batch_Count = @Message_Batch_Count
      WHERE
            Queue_Name = @Queue_Name
            AND Conversation_Group_Id = @Conversation_Group_Id
            AND Conversation_Handle = @CHandle
            AND Message_Type = @Message_Type
            AND Message_Sequence_Number = @Message_Sequence_Number;

      ;
      END CONVERSATION @Conversation_Handle
	
END

;
GO
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Upd] TO [ETL_Execute]
GRANT EXECUTE ON  [dbo].[Service_Broker_Message_Monitor_Upd] TO [sb_Execute]
GO
