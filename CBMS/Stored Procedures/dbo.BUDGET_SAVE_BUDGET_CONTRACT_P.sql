SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE  PROCEDURE dbo.BUDGET_SAVE_BUDGET_CONTRACT_P
	@contract_id int,
	@comments varchar(4000),
	@budget_contract_id int out
	AS

	insert into budget_contract_budget(contract_id, comments) values(@contract_id, @comments)
	
	select @budget_contract_id = scope_identity()











GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_BUDGET_CONTRACT_P] TO [CBMSApplication]
GO
