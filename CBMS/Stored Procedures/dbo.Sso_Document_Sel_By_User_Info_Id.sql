
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
     dbo.Sso_Document_Sel_By_User_Info_Id  
 
DESCRIPTION:   
	 Based on User info Id to get the documenets and clients.  

INPUT PARAMETERS:  
 Name				DataType		Default				Description  
-----------------------------------------------------------------------  

   
 OUTPUT PARAMETERS:  
 Name				DataType		Default				Description  
-----------------------------------------------------------------------  

 USAGE EXAMPLES:  
-----------------------------------------------------------------------  
 
 EXEC dbo.Sso_Document_Sel_By_User_Info_Id @User_Info_Id=48675
 
 EXEC dbo.Sso_Document_Sel_By_User_Info_Id @User_Info_Id=49
 
 EXEC dbo.Sso_Document_Sel_By_User_Info_Id @User_Info_Id=1129
 

 AUTHOR INITIALS:
 Initials		Name
-----------------------------------------------------------------------  
 NR				Narayana Reddy

 
 Initials	Date			Modification
-----------------------------------------------------------------------  
 NR			2015-09-04		Created For MAINT-3634.
 NR			2016-05-30		MAINT-3974 - Added SSO_DOCUMENT_ID in Output List.		
 
******/  
CREATE PROCEDURE [dbo].[Sso_Document_Sel_By_User_Info_Id] ( @User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            sd.DOCUMENT_TITLE
           ,c.CLIENT_NAME
           ,sd.SSO_DOCUMENT_ID
      FROM
            dbo.SSO_DOCUMENT sd
            INNER JOIN dbo.CLIENT c
                  ON sd.Client_Id = c.CLIENT_ID
      WHERE
            ( sd.Created_User_Id = @User_Info_Id
              OR sd.Updated_User_Id = @User_Info_Id )
      ORDER BY
            sd.LAST_UPDATED_DATE DESC
            
END;

;
GO

GRANT EXECUTE ON  [dbo].[Sso_Document_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
