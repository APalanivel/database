SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.DMO_Config_Exists_For_Account       
              
Description:              
			To check account have DMO configurations
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
@Account_Id								INT

                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 

	SELECT Account_Id FROM dbo.Account_DMO_Config adc

	SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 1269921
	EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 95408
	EXEC dbo.Account_DMO_Config_Sel @Account_Id = 95408
	
	SELECT DISTINCT cha.Commodity_Id FROM Core.Client_Hier_Account cha WHERE cha.Account_Id = 1269921
	EXEC dbo.DMO_Config_Exists_For_Account @Account_Id = 1269921
	EXEC dbo.Account_DMO_Config_Sel @Account_Id = 1269921
      
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2017-02-22		created for Contract Placeholder
             
******/
CREATE PROCEDURE [dbo].[DMO_Config_Exists_For_Account] ( @Account_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @DMO_Config_Exists_For_Account BIT = 1
      
      DECLARE @Account_DMO_Config_Commodites AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Commodity_Id INT )
      
      DECLARE @DMO_Congfig_Services AS TABLE ( Commodity_Id INT )
      
      
      DECLARE
            @Client_Id INT
           ,@Sitegroup_Id INT
           ,@Site_Id INT
           ,@Client_Hier_Id INT
           ,@Account_Hier_level_Cd INT
      
      SELECT
            @Client_Id = NULLIF(ch.Client_Id, 0)
           ,@Sitegroup_Id = NULLIF(ch.Sitegroup_Id, 0)
           ,@Site_Id = NULLIF(ch.Site_Id, 0)
           ,@Client_Hier_Id = cha.Client_Hier_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            cha.Account_Id = @Account_Id
            
      SELECT
            @Account_Hier_level_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'HierLevel'
            AND cd.Code_Value = 'Account'
      
      
      INSERT      INTO @Account_DMO_Config_Commodites
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        
      
      INSERT      INTO @Account_DMO_Config_Commodites
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        
      
      INSERT      INTO @Account_DMO_Config_Commodites
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id )
                  SELECT
                        chdc.Client_Hier_Id
                       ,chdc.Commodity_Id
                       ,chdc.Client_Hier_DMO_Config_Id
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                  WHERE
                        @Client_Id IS NOT NULL
                        AND @Sitegroup_Id IS NOT NULL
                        AND @Site_Id IS NOT NULL
                        AND ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        
      INSERT      INTO @Account_DMO_Config_Commodites
                  ( 
                   Client_Hier_Id
                  ,Commodity_Id
                  ,Client_Hier_DMO_Config_Id )
                  SELECT
                        @Client_Hier_Id AS Client_Hier_Id
                       ,adc.Commodity_Id
                       ,NULL
                  FROM
                        dbo.Account_DMO_Config adc
                  WHERE
                        adc.Account_Id = @Account_Id
      
                     
      
      INSERT      INTO @DMO_Congfig_Services
                  ( 
                   Commodity_Id )
                  SELECT
                        adcc.Commodity_Id
                  FROM
                        @Account_DMO_Config_Commodites adcc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                     WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = 0
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = 0 )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config na
                                          INNER JOIN Core.Client_Hier ch
                                                ON na.Client_Hier_Id = ch.Client_Hier_Id
                                         WHERE
                                          na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id
                                          AND @Client_Id IS NOT NULL
                                          AND @Sitegroup_Id IS NOT NULL
                                          AND @Site_Id IS NOT NULL
                                          AND ch.Client_Id = @Client_Id
                                          AND ch.Sitegroup_Id = @Sitegroup_Id
                                          AND ch.Site_Id = @Site_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config na
                                         WHERE
                                          na.Account_Id = @Account_Id
                                          AND na.Client_Hier_DMO_Config_Id = adcc.Client_Hier_DMO_Config_Id )
                  GROUP BY
                        adcc.Commodity_Id
      
      SELECT
            @DMO_Config_Exists_For_Account = 0
      FROM
            core.Client_Hier_Account cha
      WHERE
            cha.Account_Id = @Account_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              @DMO_Congfig_Services dcs
                             WHERE
                              cha.Commodity_Id = dcs.Commodity_Id )
                              
                              
      SELECT
            @DMO_Config_Exists_For_Account DMO_Config_Exists_For_Account                        
END;
;
GO
GRANT EXECUTE ON  [dbo].[DMO_Config_Exists_For_Account] TO [CBMSApplication]
GO
