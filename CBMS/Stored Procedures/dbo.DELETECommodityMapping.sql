SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [dbo].[DELETECommodityMapping]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType		Default      Description    
---------------------------------------------------------------------------                  
@ACT_Commmodity_ID     INT

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 
 



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		   Naga Jyothi
   
MODIFICATIONS    
    
 Initials	Date		Modification    
--------------------------------------------------------------------------------------    
 NJ		   2019-07-06	 Created for SE2017-733 ACT Commodity Mapping within CBMS
******/
CREATE PROCEDURE [dbo].[DELETECommodityMapping]
    (
        @ACT_Commmodity_ID INT
    )
AS
    BEGIN
        DELETE  FROM
        dbo.Commodity_ACT_Commodity_Map
        WHERE
            ACT_Commodity_Id = @ACT_Commmodity_ID;

        DELETE  FROM dbo.ACT_Commodity WHERE ACT_Commodity_Id = @ACT_Commmodity_ID;

    END;
GO
GRANT EXECUTE ON  [dbo].[DELETECommodityMapping] TO [CBMSApplication]
GO
