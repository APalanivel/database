SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME: [Workflow].[Workflow_Queue_Search_Site]      
      
DESCRIPTION:    Selects the site name(combination city, state , address and actual sitename)  for the given client.    
     
INPUT PARAMETERS:    
 Name    DataType  Default   Description        
------------------------------------------------------------------------------        
  @Client_Id INT   = NULL,     
 @Site_Name VARCHAR(100) = ''    
     
OUTPUT PARAMETERS:        
 Name    DataType  Default   Description        
------------------------------------------------------------------------------        
    
USAGE EXAMPLES:    
------------------------------------------------------------------------------        
 
EXEC Workflow.Workflow_Queue_Search_Site @Client_Id = 0, -- int
                                         @Site_Name = '' -- varchar(100)     
AUTHOR INITIALS:    
    
Initials  Name     
------------------------------------------------------------    
BR    Bhaskar Rejintala    
      
MODIFICATIONS      
       
Initials Date   Modification        
------------------------------------------------------------        
BR  14-Jun-2019  created    
TRK AUG-2019 Migrated to Workflow Schema.
******/  
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_Site]      
(      
 @Client_Id INT   = NULL,       
 @Site_Name VARCHAR(100) = ''      
)      
AS      
BEGIN      
      
 SET NOCOUNT ON;      

 SELECT @Site_Name = REPLACE(@Site_Name,'''','''''');      
       
 WITH CTE AS (      
 SELECT         
  ch.Site_Id,           
  ch.Site_name      
 FROM      
  Core.Client_Hier ch       
  INNER JOIN Core.Client_Hier_Account AS cha       
   ON cha.Client_Hier_Id = ch.Client_Hier_Id      
      
    WHERE      
  (@Client_Id  IS NULL OR ch.Client_Id = @Client_Id)      
  and ch.site_id > 0       
 )      
 SELECT TOP (50)      
  Site_Id,       
  Site_name      
 FROM      
  CTE       
 WHERE      
  Site_name LIKE '%'+@Site_Name+'%'       
 GROUP BY      
  Site_Id,       
  Site_name      
 ORDER BY      
  Site_name;      
      
END;      
      

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_Site] TO [CBMSApplication]
GO
