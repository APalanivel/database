
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********       
NAME:      
 dbo.CBMS_UPDATE_ADDRESS_P      
  
DESCRIPTION:  This procedure used to insert the records in address table   
  
IINPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
@addressTypeId   int,    
@stateId    int,    
@addressLine1   varchar(200),    
@addressLine2   varchar(200),    
@city     varchar(80),    
@zipcode    varchar(20),    
@addressParentId  int,    
@addressParentTypeId int,    
@latitude    decimal(32,16),    
@longitude    decimal(32,16),    
@addressId    int    
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:      
------------------------------------------------------------      
   
  
Initials Name      
------------------------------------------------------------      
SS   Subhash Subramanyam           
HG   Harihara Suthan Ganeshan       
MA   Mohammed Aman  
SKA  Shobhit Kr Agrawal  
DR    Deana Ritter  
RKV   Ravi Kumar Vegesna  
  
Initials Date  Modification      
------------------------------------------------------------      
  
SKA 15/07/2009  Removed the column @squarefootage which was using as input paramater as per Bug #9936  
SKA 16/07/2009  Modified comment section as per changes  
          Added comment section  
DR  08/04/2009  Modified to remove SV linked server updates.   
DMR 09/10/2010 Modified for Quoted_Identifier  
RKV 2014-01-09 MAINT-2444 Added New Column @Is_System_Generated_Geocode           
      
      
            
******/  
CREATE      PROCEDURE [dbo].[CBMS_UPDATE_ADDRESS_P]
      @addressTypeId INT
     ,@stateId INT
     ,@addressLine1 VARCHAR(200)
     ,@addressLine2 VARCHAR(200)
     ,@city VARCHAR(80)
     ,@zipcode VARCHAR(20)
     ,@addressParentId INT
     ,@addressParentTypeId INT
     ,@latitude DECIMAL(32, 16)
     ,@longitude DECIMAL(32, 16)
     ,@addressId INT
     ,@Is_System_Generated_Geocode BIT=1
AS 
UPDATE
      address
SET   
      ADDRESS_TYPE_ID = @addressTypeId
     ,STATE_ID = @stateId
     ,ADDRESS_LINE1 = @addressLine1
     ,ADDRESS_LINE2 = @addressLine2
     ,CITY = @city
     ,ZIPCODE = @zipcode
     ,ADDRESS_PARENT_ID = @addressParentId
     ,ADDRESS_PARENT_TYPE_ID = @addressParentTypeId
     ,GEO_LAT = @latitude
     ,GEO_LONG = @longitude
     ,Is_System_Generated_Geocode = @Is_System_Generated_Geocode
WHERE
      ADDRESS_ID = @addressId  
  
;
GO

GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_ADDRESS_P] TO [CBMSApplication]
GO
