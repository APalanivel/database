
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_CREATE_SHORTLIST_ID_P

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                    DataType          Default     Description    
---------------------------------------------------------------------------------    
	@accountGroupId			INT
    @isBidGroup				INT
    @dueDate				DATETIME
    @Time_Zone_Id			INT			NULL
    @Revised_Due_Dt_By_Timezone		DATETIME
                          
                           
OUTPUT PARAMETERS:         
    Name                      DataType          Default     Description    
------------------------------------------------------------    
	@sr_rfp_sop_shortlist_id    int 


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRANSACTION
		DECLARE @Shortlist_ID INT
		SELECT * FROM dbo.SR_RFP_SOP_SHORTLIST a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10001695 AND IS_BID_GROUP = 1
		EXEC dbo.SR_RFP_CREATE_SHORTLIST_ID_P 10001695,1,'2016-03-21 12:00:00.000',@sr_rfp_sop_shortlist_id  =  @Shortlist_ID OUT,
				@Time_Zone_Id = 566, @Revised_Due_Dt_By_Timezone = '2016-03-21 02:30:00.000'
		SELECT * FROM dbo.SR_RFP_SOP_SHORTLIST a INNER JOIN dbo.Time_Zone b ON a.Time_Zone_Id = b.Time_Zone_Id
			WHERE SR_ACCOUNT_GROUP_ID = 10001695 AND IS_BID_GROUP = 1
	ROLLBACK TRANSACTION

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy
	
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-21	Global Sourcing - Phase3 - GCS-546 Added new time zone input parameters

******/
CREATE PROCEDURE [dbo].[SR_RFP_CREATE_SHORTLIST_ID_P]
      ( 
       @accountGroupId INT
      ,@isBidGroup INT
      ,@dueDate DATETIME
      ,@sr_rfp_sop_shortlist_id INT OUT
      ,@Time_Zone_Id INT = NULL
      ,@Revised_Due_Dt_By_Timezone DATETIME )
AS 
BEGIN
      SET NOCOUNT ON;
      

      INSERT      INTO dbo.SR_RFP_SOP_SHORTLIST
                  ( 
                   SR_ACCOUNT_GROUP_ID
                  ,IS_BID_GROUP
                  ,REVISED_DUE_DATE
                  ,Time_Zone_Id
                  ,Revised_Due_Dt_By_Timezone )
      VALUES
                  ( 
                   @accountGroupId
                  ,@isBidGroup
                  ,@dueDate
                  ,@Time_Zone_Id
                  ,@Revised_Due_Dt_By_Timezone )

      SELECT
            @sr_rfp_sop_shortlist_id = scope_identity()
      RETURN
END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_SHORTLIST_ID_P] TO [CBMSApplication]
GO
