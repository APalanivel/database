SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    dbo.RM_Client_Data_Last_Updated_Dtls
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.RM_Client_Data_Last_Updated_Dtls 11236
	Exec dbo.RM_Client_Data_Last_Updated_Dtls 10081
	Exec dbo.RM_Client_Data_Last_Updated_Dtls 10003
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-25	GRM Proejct.
     
    
******/

CREATE PROC [dbo].[RM_Client_Data_Last_Updated_Dtls]
     (
         @Client_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Groups_of_Contracts_Cd INT
            , @RM_Manual_Group_Cd INT
            , @RM_Smart_Group_Cd INT
            , @Module_Cd INT
            , @Manage_Client_Contacts_Last_Updated_Ts DATETIME
            , @Manage_Client_Contacts_Last_Updated_User VARCHAR(200)
            , @Manage_Risk_Management_Groups_Last_Updated_Ts DATETIME
            , @Manage_Risk_Management_Groups_Last_Updated_User VARCHAR(200)
            , @Manage_Risk_Management_Configurations_Last_Updated_Ts DATETIME
            , @Manage_Risk_Management_Configurations_Last_Updated_User VARCHAR(200)
            , @Manage_Deal_Ticket_Workflows_Last_Updated_Ts DATETIME
            , @Manage_Deal_Ticket_Workflows_Last_Updated_User VARCHAR(200)
            , @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts DATETIME
            , @Manage_Financial_Counterparty_Contacts_Last_Updated_User VARCHAR(200)
            , @Manage_Volume_Forecasts_Last_Updated_Ts DATETIME
            , @Manage_Volume_Forecasts_Last_Updated_User VARCHAR(200);



        SELECT
            @Groups_of_Contracts_Cd = MAX(CASE WHEN c.Code_Value = 'Contract' THEN c.Code_Id
                                          END)
            , @RM_Manual_Group_Cd = MAX(CASE WHEN c.Code_Value = 'RM Manual Group' THEN c.Code_Id
                                        END)
            , @RM_Smart_Group_Cd = MAX(CASE WHEN c.Code_Value = 'RM Smart Group' THEN c.Code_Id
                                       END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Risk Management Groups'
            AND cs.Std_Column_Name = 'RM_Group_Type_Cd'
            AND c.Code_Value IN ( 'Contract', 'RM Manual Group', 'RM Smart Group' );

        SELECT
            @Module_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Application Module'
            AND c.Code_Value = 'CBMSRiskManagement';


        SELECT
            @Manage_Client_Contacts_Last_Updated_Ts = MAX(ci.Last_Change_Ts)
        FROM
            dbo.Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                ON ccm.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cd
                ON ci.Contact_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Client Contact'
            AND ccm.CLIENT_ID = @Client_Id;

        SELECT
            @Manage_Client_Contacts_Last_Updated_User = ui.USERNAME
        FROM
            dbo.Client_Contact_Map ccm
            INNER JOIN dbo.Contact_Info ci
                ON ccm.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cd
                ON ci.Contact_Type_Cd = cd.Code_Id
            INNER JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
            INNER JOIN dbo.USER_INFO ui
                ON ci.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            cs.Codeset_Name = 'ContactType'
            AND cd.Code_Value = 'RM Client Contact'
            AND ccm.CLIENT_ID = @Client_Id
            AND ci.Last_Change_Ts = @Manage_Client_Contacts_Last_Updated_Ts;




        SELECT
            @Manage_Risk_Management_Groups_Last_Updated_Ts = MAX(rg.Last_Change_Ts)
        FROM
            dbo.Cbms_Sitegroup rg
        WHERE
            rg.Client_Id = @Client_Id
            AND rg.Module_Cd = @Module_Cd
            AND rg.Group_Type_Cd IN ( @Groups_of_Contracts_Cd, @RM_Manual_Group_Cd, @RM_Smart_Group_Cd );


        SELECT
            @Manage_Risk_Management_Groups_Last_Updated_User = ui.USERNAME
        FROM
            dbo.Cbms_Sitegroup rg
            INNER JOIN dbo.USER_INFO ui
                ON rg.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            rg.Client_Id = @Client_Id
            AND rg.Module_Cd = @Module_Cd
            AND rg.Group_Type_Cd IN ( @Groups_of_Contracts_Cd, @RM_Manual_Group_Cd, @RM_Smart_Group_Cd )
            AND rg.Last_Change_Ts = @Manage_Risk_Management_Groups_Last_Updated_Ts;


        SELECT
            @Manage_Risk_Management_Configurations_Last_Updated_Ts = MAX(conf.Last_Updated_Ts)
        FROM
            Trade.RM_Client_Hier_Hedge_Config conf
            INNER JOIN Trade.RM_Client_Hier_Onboard onb
                ON conf.RM_Client_Hier_Onboard_Id = onb.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON onb.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id;



        SELECT
            @Manage_Risk_Management_Configurations_Last_Updated_User = ui.USERNAME
        FROM
            Trade.RM_Client_Hier_Hedge_Config conf
            INNER JOIN Trade.RM_Client_Hier_Onboard onb
                ON conf.RM_Client_Hier_Onboard_Id = onb.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON onb.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.USER_INFO ui
                ON conf.Last_Updated_By = ui.USER_INFO_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND conf.Last_Updated_Ts = @Manage_Risk_Management_Configurations_Last_Updated_Ts;


        SELECT
            @Manage_Deal_Ticket_Workflows_Last_Updated_Ts = MAX(conf.Last_Change_Ts)
        FROM
            dbo.RM_Client_Workflow conf
            INNER JOIN Trade.RM_Client_Hier_Onboard onb
                ON conf.RM_Client_Hier_Onboard_Id = onb.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON onb.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id;



        SELECT
            @Manage_Deal_Ticket_Workflows_Last_Updated_User = ui.USERNAME
        FROM
            dbo.RM_Client_Workflow conf
            INNER JOIN Trade.RM_Client_Hier_Onboard onb
                ON conf.RM_Client_Hier_Onboard_Id = onb.RM_Client_Hier_Onboard_Id
            INNER JOIN Core.Client_Hier ch
                ON onb.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.USER_INFO ui
                ON conf.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND conf.Last_Change_Ts = @Manage_Deal_Ticket_Workflows_Last_Updated_Ts;

        SELECT
            @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts = MAX(ci.Last_Change_Ts)
        FROM
            Trade.Financial_Counterparty_Client_Contact_Map ccc
            INNER JOIN dbo.Contact_Info ci
                ON ccc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            ccc.Client_Id = @Client_Id;



        SELECT
            @Manage_Financial_Counterparty_Contacts_Last_Updated_User = ui.USERNAME
        FROM
            Trade.Financial_Counterparty_Client_Contact_Map ccc
            INNER JOIN dbo.Contact_Info ci
                ON ccc.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.USER_INFO ui
                ON ci.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            ccc.Client_Id = @Client_Id
            AND ci.Last_Change_Ts = @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts;


        SELECT
            @Manage_Volume_Forecasts_Last_Updated_Ts = MAX(fv.Last_Change_Ts)
        FROM
            Trade.RM_Client_Hier_Forecast_Volume fv
            INNER JOIN Core.Client_Hier ch
                ON fv.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            ch.Client_Id = @Client_Id;



        SELECT
            @Manage_Volume_Forecasts_Last_Updated_User = ui.USERNAME
        FROM
            Trade.RM_Client_Hier_Forecast_Volume fv
            INNER JOIN Core.Client_Hier ch
                ON fv.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.USER_INFO ui
                ON fv.Updated_User_Id = ui.USER_INFO_ID
        WHERE
            ch.Client_Id = @Client_Id
            AND fv.Last_Change_Ts = @Manage_Volume_Forecasts_Last_Updated_Ts;


        SELECT
            'Manage_Risk_Management_Groups' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Risk_Management_Groups_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Risk_Management_Groups_Last_Updated_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Risk_Management_Groups_Last_Updated_Ts, 100), 7)) + ' EST'
              + ' by ' + @Manage_Risk_Management_Groups_Last_Updated_User AS Data_Point_Updated_Ts_User
        UNION
        SELECT
            'Manage_Client_Contacts' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Client_Contacts_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Client_Contacts_Last_Updated_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Client_Contacts_Last_Updated_Ts, 100), 7)) + ' EST' + ' by '
              + @Manage_Client_Contacts_Last_Updated_User AS Data_Point_Updated_Ts_User
        UNION
        SELECT
            'Manage_Risk_Management_Configurations' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Risk_Management_Configurations_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Risk_Management_Configurations_Last_Updated_Ts, 106), ' ', '-')
              + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Risk_Management_Configurations_Last_Updated_Ts, 100), 7))
              + ' EST' + ' by ' + @Manage_Risk_Management_Configurations_Last_Updated_User AS Data_Point_Updated_Ts_User
        UNION
        SELECT
            'Manage_Deal_Ticket_Workflows' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Deal_Ticket_Workflows_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Deal_Ticket_Workflows_Last_Updated_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Deal_Ticket_Workflows_Last_Updated_Ts, 100), 7)) + ' EST'
              + ' by ' + @Manage_Deal_Ticket_Workflows_Last_Updated_User AS Data_Point_Updated_Ts_User
        UNION
        SELECT
            'Manage_Financial_Counterparty_Contacts' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts, 106), ' ', '-')
              + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Financial_Counterparty_Contacts_Last_Updated_Ts, 100), 7))
              + ' EST' + ' by ' + @Manage_Financial_Counterparty_Contacts_Last_Updated_User AS Data_Point_Updated_Ts_User
        UNION
        SELECT
            'Manage_Volume_Forecasts' AS Data_Point
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Volume_Forecasts_Last_Updated_Ts, 106), ' ', '-') AS Data_Point_Updated
            , REPLACE(CONVERT(VARCHAR(20), @Manage_Volume_Forecasts_Last_Updated_Ts, 106), ' ', '-') + ' at '
              + LTRIM(RIGHT(CONVERT(VARCHAR(20), @Manage_Volume_Forecasts_Last_Updated_Ts, 100), 7)) + ' EST' + ' by '
              + @Manage_Volume_Forecasts_Last_Updated_User AS Data_Point_Updated_Ts_User;





    END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Data_Last_Updated_Dtls] TO [CBMSApplication]
GO
