SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Instructions_Upd                    
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Invoice_Processing_Config_Id		INT
 @Config_Start_Dt							DATE
 @Config_End_Dt DATE						NULL
 @Processing_Instruction NVARCHAR(MAX)	    NULL
 @User_Info_Id								INT
		                   
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


BEGIN TRAN 

SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id=3
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=3


     

	 EXEC dbo.Account_Invoice_Processing_Config_Instructions_Upd
		   @Account_Invoice_Processing_Config_Id = 3
         , @Config_Start_Dt = '2018-02-01'
         , @Config_End_Dt  = '2018-12-31'
         , @Processing_Instruction  = 'Welcome Watch List B'
         , @User_Info_Id = 49


SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id=3
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=3


ROLLBACK TRAN
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Instructions_Upd]
    (
        @Account_Invoice_Processing_Config_Id INT
        , @Config_Start_Dt DATE
        , @Config_End_Dt DATE = NULL
        , @Processing_Instruction NVARCHAR(MAX) = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Instruction_Category VARCHAR(25)
            , @PI_Field_Name NVARCHAR(255);

        SELECT
            @Instruction_Category = cd.Code_Value
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.Code Cd
                ON Cd.Code_Id = aipc.Processing_Instruction_Category_Cd
        WHERE
            aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;



        CREATE TABLE #Account_Config_History_Values
             (
                 Account_Invoice_Processing_Config_Id INT
                 , Old_Config_Start_Dt DATE
                 , Old_Config_End_Dt DATE
                 , Old_Processing_Instruction VARCHAR(MAX)
                 , New_Config_Start_Dt DATE
                 , New_Config_End_Dt DATE
                 , New_Processing_Instruction VARCHAR(MAX)
             );

        DECLARE
            @Old_Config_Start_Dt DATE
            , @Old_Config_End_Dt DATE
            , @Old_Processing_Instruction VARCHAR(MAX)
            , @New_Config_Start_Dt DATE
            , @New_Config_End_Dt DATE
            , @New_Processing_Instruction VARCHAR(MAX)
            , @Previous_Value NVARCHAR(MAX) = NULL
            , @Current_Value NVARCHAR(MAX) = NULL;


        BEGIN TRY
            BEGIN TRAN;


            UPDATE
                aipc
            SET
                aipc.Config_Start_Dt = @Config_Start_Dt
                , aipc.Config_End_Dt = @Config_End_Dt
                , aipc.Processing_Instruction = @Processing_Instruction
                , aipc.Last_Change_Ts = GETDATE()
                , aipc.Updated_User_Id = @User_Info_Id
            OUTPUT
                @Account_Invoice_Processing_Config_Id
                , Inserted.Config_Start_Dt
                , Inserted.Config_End_Dt
                , Inserted.Processing_Instruction
                , Deleted.Config_Start_Dt
                , Deleted.Config_End_Dt
                , Deleted.Processing_Instruction
            INTO #Account_Config_History_Values (Account_Invoice_Processing_Config_Id
                                                 , New_Config_Start_Dt
                                                 , New_Config_End_Dt
                                                 , New_Processing_Instruction
                                                 , Old_Config_Start_Dt
                                                 , Old_Config_End_Dt
                                                 , Old_Processing_Instruction)
            FROM
                dbo.Account_Invoice_Processing_Config aipc
            WHERE
                aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                AND (   aipc.Processing_Instruction <> @Processing_Instruction
                        OR  aipc.Config_Start_Dt <> @Config_Start_Dt
                        OR  ISNULL(aipc.Config_End_Dt, '2099-12-31') <> ISNULL(@Config_End_Dt, '2099-12-31'));



            SELECT
                @Old_Config_Start_Dt = achv.Old_Config_Start_Dt
                , @Old_Config_End_Dt = achv.Old_Config_End_Dt
                , @Old_Processing_Instruction = achv.Old_Processing_Instruction
                , @New_Config_Start_Dt = achv.New_Config_Start_Dt
                , @New_Config_End_Dt = achv.New_Config_End_Dt
                , @New_Processing_Instruction = achv.New_Processing_Instruction
            FROM
                #Account_Config_History_Values achv;



            IF (@New_Processing_Instruction <> @Old_Processing_Instruction)
                BEGIN

                    SET @PI_Field_Name = @Instruction_Category + N' ' + N'Processing Instructions';


                    EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                        @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                        , @Field_Name = @PI_Field_Name
                        , @Change_Type = 'Edited'
                        , @Previous_Value = @Old_Processing_Instruction
                        , @Current_Value = @New_Processing_Instruction
                        , @Is_Updated_Using_Apply_All = 0
                        , @Event_By_User_Id = @User_Info_Id;

                END;


            IF (   @New_Config_Start_Dt <> @Old_Config_Start_Dt
                   OR   ISNULL(@New_Config_End_Dt, '2099-12-31') <> ISNULL(@Old_Config_End_Dt, '2099-12-31'))
                BEGIN




                    SET @Previous_Value = CONVERT(VARCHAR(20), @Old_Config_Start_Dt, 101) + N' - '
                                          + ISNULL(CONVERT(VARCHAR(20), @Old_Config_End_Dt, 101), ' Open End Date');
                    SET @Current_Value = CONVERT(VARCHAR(20), @New_Config_Start_Dt, 101) + N' - '
                                         + ISNULL(CONVERT(VARCHAR(20), @New_Config_End_Dt, 101), ' Open End Date');

                    EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                        @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                        , @Field_Name = 'Configuration Dates'
                        , @Change_Type = 'Edited'
                        , @Previous_Value = @Previous_Value
                        , @Current_Value = @Current_Value
                        , @Is_Updated_Using_Apply_All = 0
                        , @Event_By_User_Id = @User_Info_Id;

                END;






            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;

        DROP TABLE #Account_Config_History_Values;


    END;





GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Instructions_Upd] TO [CBMSApplication]
GO
