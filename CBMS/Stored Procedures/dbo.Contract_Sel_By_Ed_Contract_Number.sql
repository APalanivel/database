
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Sel_By_Ed_Contract_Number]  
     
DESCRIPTION: 
	To Get Contracts Information for the given Contract number
	      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Ed_Contract_NUMBER	VARCHAR(150)
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Contract_Sel_By_Ed_Contract_Number  '10924-0001'
	
	EXEC Contract_Sel_By_Ed_Contract_Number  '10874-0002'

	EXEC Contract_Sel_By_Ed_Contract_Number  '29933-0001'
	
    SELECT * FROM Contract WHERE contract_id in (10924, 23781)
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
	BCH			Balaraju
	SP		    Sandeep Pigilam           

          
MODIFICATIONS:
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			03-JUNE-10	CREATED  
	BCH			2012-02-07	Added Commodity_Id and Default_UOM_Entity_Type_Id to select list for Delete Tool requirement.
	SP			2014-09-09  For Data Transition used a table variable @Group_Legacy_Name to handle the hardcoded group names  
	
*/

CREATE PROCEDURE [dbo].[Contract_Sel_By_Ed_Contract_Number]
      ( 
       @Ed_Contract_Number VARCHAR(150) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Quality_Assurance' 

      SELECT
            cnt.ED_CONTRACT_NUMBER
           ,cnt.CONTRACT_ID
           ,bc.ED_CONTRACT_NUMBER Base_Contract_Number
           ,cnt.IS_CONTRACT_FULL_REQUIREMENT
           ,cnt.CONTRACT_START_DATE
           ,cnt.CONTRACT_END_DATE
           ,com.Default_UOM_Entity_Type_Id
           ,com.Commodity_Id
           ,com.Commodity_Name
           ,LEFT(Analyst.Managed_By, LEN(Analyst.Managed_By) - 1) Managed_By
      FROM
            dbo.CONTRACT cnt
            LEFT OUTER JOIN dbo.contract AS bc
                  ON bc.contract_id = cnt.base_contract_id
            JOIN dbo.Commodity com
                  ON cnt.COMMODITY_TYPE_ID = com.Commodity_Id
            CROSS APPLY ( SELECT
                              un.FIRST_NAME + SPACE(1) + un.LAST_NAME + ', '
                          FROM
                              dbo.supplier_account_meter_map samm
                              JOIN dbo.meter AS m
                                    ON m.meter_id = samm.meter_id
                              JOIN dbo.account AS ua
                                    ON ua.account_id = m.account_id
                              JOIN dbo.Utility_Detail ud
                                    ON ud.vendor_id = ua.Vendor_id
                              JOIN dbo.Utility_Detail_Analyst_Map UDAM
                                    ON udam.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                              JOIN dbo.GROUP_INFO GI
                                    ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID
                              INNER JOIN @Group_Legacy_Name gil
                                    ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                              JOIN dbo.USER_INFO un
                                    ON un.user_info_id = udam.Analyst_ID
                          WHERE
                              samm.Contract_ID = cnt.Contract_Id
                          GROUP BY
                              un.FIRST_NAME
                             ,un.LAST_NAME
            FOR
                          XML PATH('') ) Analyst ( Managed_By )
      WHERE
            cnt.ED_CONTRACT_NUMBER = @Ed_Contract_Number

END
;
GO


GRANT EXECUTE ON  [dbo].[Contract_Sel_By_Ed_Contract_Number] TO [CBMSApplication]
GO
