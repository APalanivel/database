SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_OPTIONS_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE       PROCEDURE dbo.GET_OPTIONS_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P

@userId varchar,
@sessionId varchar,
@dealTicketId int

AS
set nocount on
SELECT 
	rmdtod.RM_DEAL_TICKET_OPTION_DETAILS_ID, 
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID, 
	rmdtd.MONTH_IDENTIFIER,
	rmdtd.TOTAL_VOLUME,
	rmdtod.VOLUME, 
	rmdtod.STRIKE_PRICE, 
	rmdtod.PREMIUM_PRICE, 
	e1.ENTITY_NAME AS OPTION_TYPE,
	e2.ENTITY_NAME AS UNIT_TYPE,
	e1.DISPLAY_ORDER
FROM 
	RM_DEAL_TICKET rmdt, 
	RM_DEAL_TICKET_DETAILS rmdtd, 
	RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
	RM_DEAL_TICKET_OPTION_DETAILS rmdtod, 
	ENTITY e1, ENTITY e2
WHERE
	rmdt.RM_DEAL_TICKET_ID = @dealTicketId AND 
	rmdt.RM_DEAL_TICKET_ID = rmdtd.RM_DEAL_TICKET_ID AND 
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID = rmdtd.RM_DEAL_TICKET_DETAILS_ID AND 
	rmdtod.RM_DEAL_TICKET_DETAILS_ID = rmdtd.RM_DEAL_TICKET_DETAILS_ID AND 
	rmdtod.OPTION_TYPE_ID = e1.entity_id AND 
	rmdt.UNIT_TYPE_ID = e2.entity_id
GROUP BY
	rmdtod.RM_DEAL_TICKET_OPTION_DETAILS_ID, 
	rmdtvd.RM_DEAL_TICKET_DETAILS_ID, 
	rmdtd.MONTH_IDENTIFIER, 
	rmdtd.TOTAL_VOLUME, 
	rmdtod.volume, 
	rmdtod.strike_price, 
	rmdtod.premium_price, 
	e1.ENTITY_NAME,
	e1.DISPLAY_ORDER, 
	e2.ENTITY_NAME
ORDER BY 
	rmdtd.MONTH_IDENTIFIER, 
	e1.DISPLAY_ORDER
GO
GRANT EXECUTE ON  [dbo].[GET_OPTIONS_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_P] TO [CBMSApplication]
GO
