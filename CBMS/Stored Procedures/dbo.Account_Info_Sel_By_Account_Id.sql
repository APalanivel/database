SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******      
NAME:      
 dbo.Account_Info_Sel_By_Account_Id    
    
DESCRIPTION:  
  
   Selects the site name(combination city, state , address and actual sitename)  for the given client.  
   
INPUT PARAMETERS:  
 Name    DataType  Default   Description      
------------------------------------------------------------------------------      
 @Account_id   INT  
   
  
OUTPUT PARAMETERS:      
 Name    DataType  Default   Description      
------------------------------------------------------------------------------      
  
USAGE EXAMPLES:  
------------------------------------------------------------------------------      
  
 EXEC dbo.Account_Info_Sel_By_Account_Id 1654697   
  
   
    
   
AUTHOR INITIALS:  
  
Initials  Name   
------------------------------------------------------------  
RKV    Ravi Kumar Vegesna  
  
    
MODIFICATIONS    
     
Initials Date  Modification      
------------------------------------------------------------      
RKV  2019-05-23 created for Invoice Collection  
use cbms  
******/



CREATE PROCEDURE [dbo].[Account_Info_Sel_By_Account_Id]
     (
         @Account_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ch.Client_Name
            , ch.Site_name
            , cha.Meter_Address_Line_1 + ' ' + cha.Meter_Address_Line_2 [Address]
            , cha.Meter_City
            , cha.Meter_State_Name
            , cha.Account_Type
            , cha.Account_Vendor_Name
            , cha.Display_Account_Number
            , c.Commodity_Name
            , cha.Meter_Number
            , cha.Rate_Name
            , CASE WHEN ch.Client_Not_Managed = 1 THEN 1
                  WHEN ch.Site_Not_Managed = 1 THEN 1
                  ELSE cha.Account_Not_Managed
              END Account_Not_Managed
            , ch.Client_Hier_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
            , ch.Client_Id
        FROM
            Core.Client_Hier_Account AS cha
            INNER JOIN Core.Client_Hier AS ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity AS c
                ON c.Commodity_Id = cha.Commodity_Id
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ch.Client_Name
            , ch.Site_name
            , cha.Meter_Address_Line_1 + ' ' + cha.Meter_Address_Line_2
            , cha.Meter_City
            , cha.Meter_State_Name
            , cha.Account_Type
            , cha.Account_Vendor_Name
            , cha.Display_Account_Number
            , c.Commodity_Name
            , cha.Meter_Number
            , cha.Rate_Name
            , CASE WHEN ch.Client_Not_Managed = 1 THEN 1
                  WHEN ch.Site_Not_Managed = 1 THEN 1
                  ELSE cha.Account_Not_Managed
              END
            , ch.Client_Hier_Id
            , ch.Sitegroup_Id
            , ch.Site_Id
            , ch.Client_Id;

    END;





    ;


GO
GRANT EXECUTE ON  [dbo].[Account_Info_Sel_By_Account_Id] TO [CBMSApplication]
GO
