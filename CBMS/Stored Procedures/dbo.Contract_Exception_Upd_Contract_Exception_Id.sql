SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Exception_Upd_Contract_Exception_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name								DataType		Default		Description
-------------------------------------------------------------------------------------------
	@Account_Exception_Id				INT
    @User_Info_Id						INT
    @Exception_Status_Cd				INT

OUTPUT PARAMETERS:
	Name								DataType		Default		Description
-------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------------------
  
EXEC Contract_Exception_Upd_Contract_Exception_Id
    @Account_Exception_Id = 1
    , @User_Info_Id = 1
    , @Exception_Status_Cd = 1

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------------------
	Nr       	2019-07-19		Created for Add Contract.

******/

CREATE PROCEDURE [dbo].[Contract_Exception_Upd_Contract_Exception_Id]
    (
        @Contract_Exception_Id INT
        , @User_Info_Id INT
        , @Exception_Status_Cd INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        UPDATE
            ae
        SET
            ae.Last_Change_Ts = GETDATE()
            , ae.Updated_User_Id = @User_Info_Id
            , ae.Exception_Status_Cd = @Exception_Status_Cd
        FROM
            dbo.Contract_Exception ae
        WHERE
            ae.Contract_Exception_Id = @Contract_Exception_Id;
    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_Upd_Contract_Exception_Id] TO [CBMSApplication]
GO
