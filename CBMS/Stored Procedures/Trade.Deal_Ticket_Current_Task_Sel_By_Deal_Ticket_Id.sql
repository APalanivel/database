SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id
   
    
DESCRIPTION:   
   
  This procedure is to get deal ticket current task
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  195
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  198
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  291
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  288
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
------------------------------------------------------------------------------------------------       
	RR          2019-01-04  Global Risk Management - Created to get deal ticket current task
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      DECLARE @Tasks VARCHAR(200) = '';
      
      DECLARE @Tbl_Active_Tasks AS TABLE
            ( 
             Deal_Ticket_Id INT
            ,Tasks NVARCHAR(255)
            ,Workflow_Task_Id INT
            --,Is_Active BIT
            ,Task_Date DATETIME
            ,Task_Display_order INT
            ,Is_CBMS_Task BIT
            ,Updated_By VARCHAR(100) )
            
      DECLARE @Tbl_Completed_Tasks AS TABLE
            ( 
             Deal_Ticket_Id INT
            ,Tasks NVARCHAR(255)
            ,Workflow_Task_Id INT
            --,Is_Active BIT
            ,Task_Date DATETIME
            ,Task_Display_order INT
            ,Is_CBMS_Task BIT
            ,Updated_By VARCHAR(100)
            ,RowId INT )
      
      INSERT      INTO @Tbl_Active_Tasks
                  ( 
                   Deal_Ticket_Id
                  ,Tasks
                  ,Workflow_Task_Id
                  --,Is_Active
                  ,Task_Date
                  ,Task_Display_order
                  ,Is_CBMS_Task
                  ,Updated_By )
                  SELECT
                        @Deal_Ticket_Id AS Deal_Ticket_Id
                       ,tsk.Task_Name AS Tasks
                       ,tsk.Workflow_Task_Id
                       --,chws.Is_Active
                       ,NULL AS Task_Date
                       ,tsk.Display_order
                       ,tsk.Is_CBMS_Task
                       ,NULL AS Updated_By
                  FROM
                        Trade.Deal_Ticket dt
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN Trade.Workflow_Status_Map wsm
                              ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                                 AND dt.Workflow_Id = wsm.Workflow_Id
                        INNER JOIN Trade.Workflow_Task_Status_Map tsm
                              ON tsm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                        INNER JOIN Trade.Workflow_Task tsk
                              ON tsm.Workflow_Task_Id = tsk.Workflow_Task_Id
                  WHERE
                        dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                        AND chws.Is_Active = 1
                        AND tsk.Display_In_CBMS_Application = 1
                  GROUP BY
                        tsk.Task_Name
                       ,tsk.Workflow_Task_Id
                       --,chws.Is_Active
                       ,tsk.Display_order
                       ,tsk.Is_CBMS_Task

      INSERT      INTO @Tbl_Completed_Tasks
                  ( 
                   Deal_Ticket_Id
                  ,Tasks
                  ,Workflow_Task_Id
                  --,Is_Active
                  ,Task_Date
                  ,Task_Display_order
                  ,Is_CBMS_Task
                  ,Updated_By
                  ,RowId )
                  SELECT
                        @Deal_Ticket_Id AS Deal_Ticket_Id
                       ,tsk.Task_Name AS Tasks
                       ,tsk.Workflow_Task_Id
                       --,decwsm.Is_Active
                       ,decwsm.Created_Ts
                       ,tsk.Display_order
                       ,tsk.Is_CBMS_Task
                       ,uui.USERNAME AS Updated_By
                       ,ROW_NUMBER() OVER ( PARTITION BY tsk.Workflow_Task_Id ORDER BY decwsm.Created_Ts DESC ) AS RowId
                  FROM
                        Trade.Deal_Ticket dt
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN Trade.Workflow_Status_Map wsm
                              ON dt.Workflow_Id = wsm.Workflow_Id
                                 AND chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                        INNER JOIN Trade.Workflow_Status ws
                              ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                        INNER JOIN Trade.Workflow_Task_Status_Map tsm
                              ON tsm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                        INNER JOIN Trade.Workflow_Task tsk
                              ON tsm.Workflow_Task_Id = tsk.Workflow_Task_Id
                        INNER JOIN Trade.Workflow_Task_Status_Transition_Map tstm
                              ON tstm.Workflow_Task_Status_Map_Id = tsm.Workflow_Task_Status_Map_Id
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status decwsm
                              ON tstm.Descendant_Workflow_Status_Map_Id = decwsm.Workflow_Status_Map_Id
                                 AND chws.Deal_Ticket_Client_Hier_Id = decwsm.Deal_Ticket_Client_Hier_Id
                                 AND decwsm.Workflow_Task_Id = tsm.Workflow_Task_Id
                        INNER JOIN dbo.USER_INFO uui
                              ON uui.USER_INFO_ID = decwsm.Updated_User_Id
                  WHERE
                        dtch.Deal_Ticket_Id = @Deal_Ticket_Id
                        AND ws.Workflow_Status_Name <> 'DT Created'
                        AND tsk.Display_In_CBMS_Application = 1
                  GROUP BY
                        tsk.Task_Name
                       ,tsk.Workflow_Task_Id
                       --,decwsm.Is_Active
                       ,decwsm.Created_Ts
                       ,tsk.Display_order
                       ,tsk.Is_CBMS_Task
                       ,uui.USERNAME
      
            
      INSERT      INTO @Tbl_Active_Tasks
                  ( 
                   Deal_Ticket_Id
                  ,Tasks
                  ,Workflow_Task_Id
                  --,Is_Active
                  ,Task_Date
                  ,Task_Display_order
                  ,Is_CBMS_Task
                  ,Updated_By )
                  SELECT
                        Deal_Ticket_Id
                       ,Tasks
                       ,Workflow_Task_Id
                       --,Is_Active
                       ,Task_Date
                       ,Task_Display_order
                       ,Is_CBMS_Task
                       ,Updated_By
                  FROM
                        @Tbl_Completed_Tasks
                  WHERE
                        RowId = 1
                  
                  
              
      SELECT
            at.Deal_Ticket_Id
           ,at.Tasks
           ,at.Workflow_Task_Id
           ,1 AS Is_Active
           ,REPLACE(CONVERT(VARCHAR(20), MAX(at.Task_Date), 106), ' ', '-') AS Task_Date
           ,at.Task_Display_order
           ,REPLACE(CONVERT(VARCHAR(20), MAX(at.Task_Date), 106), ' ', '-') + ' at ' + LTRIM(RIGHT(CONVERT(VARCHAR(20), MAX(at.Task_Date), 100), 7)) + ' EST' AS Task_Date_Time
           ,MAX(at.Updated_By) AS Updated_By
           ,at.Is_CBMS_Task
      FROM
            @Tbl_Active_Tasks at
      GROUP BY
            at.Deal_Ticket_Id
           ,at.Tasks
           ,at.Workflow_Task_Id
           ,at.Task_Display_order
           ,at.Is_CBMS_Task
      ORDER BY
            at.Task_Display_order

      

END;



GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id] TO [CBMSApplication]
GO
