SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Cost_Usage_Account_Dtl_Id_Sel_By_Client_Hier_Account_Service_Month

DESCRIPTION:
	Used to get the cost_usage_account_dtl_id for the given account and period.

INPUT PARAMETERS:
	Name				   DataType		Default	Description
------------------------------------------------------------
	@Site_Client_Hier_Id   INT
	@Account_Id		   int
	@Begin_Dt			   Date
	@End_Dt			   Date

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
     EXEC dbo.Cost_Usage_Account_Dtl_Id_Sel_By_Client_Hier_Account_Service_Month 3661, 158072, '2010-01-01', '2010-01-01'  
     EXEC dbo.Cost_Usage_Account_Dtl_Id_Sel_By_Client_Hier_Account_Service_Month 112266, 258332, '2011-01-01', '2011-01-01'  


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AP			Athmaram Pabbathi
	RR			Raghu Reddy
	BCH			Balaraju Chalumuri
	SP			Sandeep Pigilam

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	AP			2012-03-27	Created to replace dbo.Cost_Usage_Account_Dtl_Id_Sel_By_Account_Service_Month
	RR			2012-05-31	Removed the filters, account type is utility or for supplier account begin/end date within input dates on client_hier_account
							table and managed state filter on client,division,site,account as the old procedure don't have all these filters
	BCH			2013-06-10  Enhancement-40, Added Variance_Effective_Dt column to the output.
	Sp			2016-11-15	Variance Enh - Removed Variance_Effective_Dt column in output list.						
******/

CREATE PROCEDURE [dbo].[Cost_Usage_Account_Dtl_Id_Sel_By_Client_Hier_Account_Service_Month]
      ( 
       @Site_Client_Hier_Id INT
      ,@Account_Id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      DECLARE @Client_Cost_Usage_Service TABLE ( Commodity_Id INT )  
      DECLARE @Client_Id INT  
        
      SELECT
            @Client_Id = Client_Id
      FROM
            Core.Client_Hier
      WHERE
            Client_Hier_Id = @Site_Client_Hier_Id  
  
      INSERT      INTO @Client_Cost_Usage_Service
                  ( 
                   Commodity_Id )
                  SELECT
                        cc.Commodity_Id
                  FROM
                        core.Client_Commodity cc
                        INNER JOIN dbo.Code cscd
                              ON cscd.Code_Id = cc.Commodity_Service_Cd
                  WHERE
                        cscd.Code_Value = 'Invoice'
                        AND cc.Client_Id = @Client_Id  
  
      SELECT
            cuad.Cost_Usage_Account_Dtl_id
           ,com.Commodity_Id
           ,com.Commodity_Name
           ,bm.Bucket_Name
           ,bkt_cd.Code_Value
           ,cuad.ACCOUNT_ID
           ,cuad.Service_Month
           ,ch.SITE_ID
           ,cuad.CURRENCY_UNIT_ID
           ,cuad.UOM_Type_Id
           ,ent.ENTITY_NAME AS UOM_Name
           ,com.Default_UOM_Entity_Type_Id
           ,du.ENTITY_NAME Default_Uom_Name
		   , bm.Bucket_Master_Id
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = cuad.Client_Hier_Id
                     AND cha.Account_Id = cuad.Account_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
            LEFT OUTER JOIN dbo.ENTITY ent
                  ON ent.ENTITY_ID = cuad.UOM_Type_Id
            LEFT OUTER JOIN dbo.ENTITY du
                  ON du.ENTITY_ID = com.Default_UOM_Entity_Type_Id
            LEFT OUTER JOIN @Client_Cost_Usage_Service cc
                  ON cc.Commodity_Id = com.Commodity_Id
      WHERE
            cuad.ACCOUNT_ID = @Account_Id
            AND cuad.Client_Hier_Id = @Site_Client_Hier_Id
            AND cuad.Service_Month BETWEEN @Begin_Dt AND @End_Dt
      GROUP BY
            cuad.Cost_Usage_Account_Dtl_id
           ,com.Commodity_Id
           ,com.Commodity_Name
           ,bm.Bucket_Name
           ,bkt_cd.Code_Value
           ,cuad.ACCOUNT_ID
           ,cuad.Service_Month
           ,ch.SITE_ID
           ,cuad.CURRENCY_UNIT_ID
           ,cuad.UOM_Type_Id
           ,ent.ENTITY_NAME
           ,com.Default_UOM_Entity_Type_Id
           ,du.ENTITY_NAME
		   ,bm.Bucket_Master_Id
      ORDER BY
            com.Commodity_Name
           ,bkt_cd.Code_Value
           ,bm.Bucket_Name  

  
END;
;

;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Id_Sel_By_Client_Hier_Account_Service_Month] TO [CBMSApplication]
GO
