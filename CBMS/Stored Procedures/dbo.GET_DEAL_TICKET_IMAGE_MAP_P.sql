SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_IMAGE_MAP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.GET_DEAL_TICKET_IMAGE_MAP_P

@dealTicketId int 

AS
BEGIN
		set nocount on
		SELECT CBMS_IMAGE_ID,  TRANSACTION_DATE 
		FROM RM_DEAL_TICKET_IMAGE_MAP 
		WHERE RM_DEAL_TICKET_ID = @dealTicketId 
	END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_IMAGE_MAP_P] TO [CBMSApplication]
GO
