
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
  
 NAME: dbo.Report_DE_Invoice_Aging  
  
 DESCRIPTION:   
    
  
 INPUT PARAMETERS:    
 Name        DataType   Default   Description    
---------------------------------------------------------------------------------------------------------------  
  
   
 OUTPUT PARAMETERS:    
  
 Name        DataType   Default   Description  
---------------------------------------------------------------------------------------------------------------  
   
 USAGE EXAMPLES:    
---------------------------------------------------------------------------------------------------------------  
  
  EXEC dbo.Report_DE_Invoice_Aging  
   
 AUTHOR INITIALS:    
 Initials    Name    
---------------------------------------------------------------------------------------------------------------  
 NR      Narayana Reddy  
   
   
 MODIFICATIONS     
 Initials    Date   Modification  
---------------------------------------------------------------------------------------------------------------  
 NR      2014-08-08  Created for invoice aging data reports.  
 AKR     2014-10-08  Modified to include Recalc
   
******/  
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Aging]
AS 
BEGIN  
      
      SET NOCOUNT ON;
      WITH  Queue_Name_CTE
              AS ( SELECT DISTINCT
                        q.queue_id
                       ,Ui.User_Info_Id
                       ,Queue_name
                       ,History = CASE WHEN ui.is_history = 1 THEN 'History'
                                       ELSE 'Active User'
                                  END
                       ,[User Name] = ui.USERNAME
                       ,[CEM] = CASE WHEN uigim.user_info_id IS NOT NULL THEN 'Yes'
                                     ELSE 'No'
                                END
                       ,[Ops] = CASE WHEN uigim2.user_info_id IS NOT NULL THEN 'Yes'
                                     ELSE 'No'
                                END
                       ,[External User Alert] = CASE WHEN ui.ACCESS_LEVEL = 1 THEN 'ALERT'
                                                     ELSE NULL
                                                END
                   FROM
                        vwCbmsQueueName q
                        JOIN USER_INFO ui
                              ON ui.QUEUE_ID = q.queue_id
                        LEFT JOIN user_info_group_info_map uigim
                              ON uigim.user_info_id = ui.user_info_id
                                 AND uigim.group_info_id = 2
                        LEFT JOIN user_info_group_info_map uigim2
                              ON uigim2.user_info_id = ui.user_info_id
                                 AND uigim2.group_info_id = 7
                   WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    CU_EXCEPTION_DENORM cued
                                 WHERE
                                    cued.QUEUE_ID = q.queue_id )
                        OR EXISTS ( SELECT
                                          1
                                    FROM
                                          dbo.Variance_Log vl
                                          INNER JOIN dbo.Commodity com
                                                ON vl.Commodity_ID = com.Commodity_Id
                                    WHERE
                                          vl.Partition_Key IS NULL
                                          AND vl.is_failure = 1
                                          AND com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
                                          AND vl.Queue_Id = q.queue_id )),
            Cte_Counts
              AS ( SELECT
                        t.QUEUE_ID
                       ,COUNT(CASE WHEN IS_MANUAL = 1 THEN 1
                              END) [Incoming Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - t.date_in_queue >= 5 ) THEN 1
                              END) [Incoming >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - t.date_in_queue >= 15 ) THEN 1
                              END) [Incoming >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 1
                                        AND ( GETDATE() - t.date_in_queue >= 30 ) THEN 1
                              END) [Incoming >30Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND exception_type != 'failed recalc' THEN 1
                              END) [Exception Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 5
                                              AND exception_type != 'failed recalc' ) THEN 1
                              END) [Exception >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 15
                                              AND exception_type != 'failed recalc' ) THEN 1
                              END) [Exception >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 30
                                              AND exception_type != 'failed recalc' ) THEN 1
                              END) [Exception >30Days]
                              --Recalc
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND exception_type = 'failed recalc' THEN 1
                              END) [Recalc Total]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 5
                                              AND exception_type = 'failed recalc' ) THEN 1
                              END) [Recalc >5Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 15
                                              AND exception_type = 'failed recalc' ) THEN 1
                              END) [Recalc >15Days]
                       ,COUNT(CASE WHEN IS_MANUAL = 0
                                        AND ( GETDATE() - t.date_in_queue >= 30
                                              AND exception_type = 'failed recalc' ) THEN 1
                              END) [Recalc >30Days]
                   FROM
                        dbo.cu_exception_denorm t
                   GROUP BY
                        t.QUEUE_ID),
            Variance_Count_CTE
              AS ( SELECT
                        COUNT(variance_count_Blue) AS [Variance Total]
                       ,queue_id
                   FROM
                        ( SELECT
                              COUNT(1) AS variance_count_blue
                             ,l.Queue_Id
                          FROM
                              dbo.variance_log l
                              INNER JOIN dbo.Commodity com
                                    ON l.Commodity_ID = com.Commodity_Id
                          WHERE
                              l.Partition_Key IS NULL
                              AND l.is_failure = 1
                              AND com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
                          GROUP BY
                              l.Account_ID
                             ,l.Service_Month
                             ,l.Site_ID
                             ,l.Queue_id ) vt
                   GROUP BY
                        vt.Queue_Id)
            SELECT
                  qc.queue_name [Queue Name]
                 ,qc.[User Name]
                 ,qc.[External User Alert]
                 ,qc.History
                 ,qc.CEM
                 ,qc.Ops
                 ,ISNULL(cc.[Incoming Total], 0) [Incoming Total]
                 ,ISNULL(cc.[Incoming >5Days], 0) [Incoming >5Days]
                 ,ISNULL(cc.[Incoming >15Days], 0) [Incoming >15Days]
                 ,ISNULL(cc.[Incoming >30Days], 0) [Incoming >30Days]
                 ,ISNULL(vc.[Variance Total], 0) [Variance Total]
                 ,ISNULL(cc.[Exception Total], 0) [Exception Total]
                 ,ISNULL(cc.[Exception >5Days], 0) [Exception >5Days]
                 ,ISNULL(cc.[Exception >15Days], 0) [Exception >15Days]
                 ,ISNULL(cc.[Exception >30Days], 0) [Exception >30Days]
                 ,ISNULL(cc.[Recalc Total], 0) [Recalc Total]
                 ,ISNULL(cc.[Recalc >5Days], 0) [Recalc >5Days]
                 ,ISNULL(cc.[Recalc >15Days], 0) [Recalc >15Days]
                 ,ISNULL(cc.[Recalc >30Days], 0) [Recalc >30Days]
            FROM
                  Queue_Name_CTE qc
                  LEFT JOIN Variance_Count_CTE vc
                        ON vc.Queue_Id = qc.queue_id
                  LEFT JOIN Cte_Counts cc
                        ON cc.Queue_Id = qc.queue_id
            ORDER BY
                  queue_name   
   
END                 
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Aging] TO [CBMS_SSRS_Reports]
GO
