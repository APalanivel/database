SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_HEDGE_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	varchar(200)	          	
	@hedgeVolume   	decimal(32,16)	          	
	@hedgePrice    	decimal(32,16)	          	
	@monthIdentifier	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE DBO.UPDATE_DEAL_TICKET_HEDGE_DETAILS_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId varchar(200), 
@hedgeVolume decimal(32,16), 
@hedgePrice decimal(32,16),
@monthIdentifier datetime

AS
set nocount on
DECLARE @hedgeId int 
SELECT @hedgeId = HEDGE_ID FROM HEDGE WHERE HEDGE_NAME = @dealTicketId

UPDATE HEDGE_DETAIL SET
	HEDGE_VOLUME = @hedgeVolume, 
	HEDGE_PRICE = @hedgePrice
WHERE   hedge_id = @hedgeId and
	MONTH_IDENTIFIER = @monthIdentifier

--select * from hedge_detail where hedge_id = 2468
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_HEDGE_DETAILS_P] TO [CBMSApplication]
GO
