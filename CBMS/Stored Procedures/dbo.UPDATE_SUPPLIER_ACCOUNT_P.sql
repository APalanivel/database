SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.UPDATE_SUPPLIER_ACCOUNT_P
	@invoice_source_type_id int,
	@account_number varchar(50),
	@account_id int
	AS
	begin
		set nocount on

		update	account  set invoice_source_type_id = @invoice_source_type_id, 
			    	     account_number = @account_number
		where	account_id = @account_id 

	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SUPPLIER_ACCOUNT_P] TO [CBMSApplication]
GO
