SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   
    dbo.Cost_Usage_Account_Billing_Dtl_Del       

DESCRIPTION:  
	This procedure deletes the billing detail saved at account level based on the given account_id and service_month.
      
INPUT PARAMETERS:          
Name			 DataType	Default	Description          
------------------------------------------------------------          
@Account_Id		 INT
@Service_Month	 DATE
         
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

select * from Cost_Usage_Account_Billing_Dtl where account_id=1 and service_month='2010-01-01'

Begin Tran
Exec dbo.Cost_Usage_Account_Billing_Dtl_Del 1,'2010-01-01'
select * from Cost_Usage_Account_Billing_Dtl where account_id=1 and service_month='2010-01-01'
Rollback Tran
         
USAGE EXAMPLES:          
------------------------------------------------------------ 
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
AP			Athmaram Pabbathi
BCH			Balaraju     

MODIFICATIONS           
Initials	Date		    Modification          
------------------------------------------------------------          
AP			09/14/2011    Created
BCH			2012-04-16	  Removed @Commodity_Id parameter.
*****/
CREATE PROCEDURE dbo.Cost_Usage_Account_Billing_Dtl_Del
      ( 
       @Account_Id INT
      ,@Service_Month DATE )
AS 
BEGIN
      SET NOCOUNT ON
   
      DELETE FROM
            dbo.Cost_Usage_Account_Billing_Dtl
      WHERE
            Account_Id = @Account_Id
            AND Service_Month = @Service_Month
    
END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Billing_Dtl_Del] TO [CBMSApplication]
GO
