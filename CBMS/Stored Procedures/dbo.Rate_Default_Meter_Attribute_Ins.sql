SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                      
NAME: dbo.Rate_Default_Meter_Attribute_Ins                                                    
                                                      
DESCRIPTION:                                                      
  Script to get the budget accounts for the status page grid                                                    
                                                      
INPUT PARAMETERS:                                                      
 Name        DataType Default Description                                                      
---------------------------------------------------------------                                                      
@Rate_Id     INT  
@Attribute_Id    INT  
@Start_Date     DATE  
@End_Date     DATE    
@Attribute_Values   NVARCHAR(255)  
@User_Info_Id    INT  
@Attribute_Value_Type_Id INT  
@Required_Option_Code_Id INT                                               
                                                      
OUTPUT PARAMETERS:                                                      
 Name     DataType  Default Description                                                      
------------------------------------------------------------                                                      
                                                       
USAGE EXAMPLES:                                                      
------------------------------------------------------------                                                      
EXEC [dbo].[Rate_Default_Meter_Attribute_Ins]    
       @Rate_Id =1  
         , @Attribute_Id =12  
         , @Start_Date ='2020-01-01'  
         , @End_Date DATE ='2020-12-01'   
         , @Attribute_Values ='29kwueh'  
         , @User_Info_Id =49  
         , @Attribute_Value_Type_Id =123  
         , @Required_Option_Code_Id =321                                       
                                                
AUTHOR INITIALS:                                                      
 Initials Name                       
------------------------------------------------------------                                                                     
SC  Sreenivasulu Cheerala  
  
  
MODIFICATIONS                                                      
 Initials Date  Modification                       
------------------------------------------------------------                                                      
SC   2020-04-01          Created                
                          
******/
CREATE PROCEDURE [dbo].[Rate_Default_Meter_Attribute_Ins]
    (
        @Rate_Id INT
        , @Attribute_Id INT
        , @Start_Date DATE
        , @End_Date DATE = NULL
        , @Attribute_Values NVARCHAR(255)
        , @User_Info_Id INT
        , @Attribute_Value_Type_Id INT
        , @Required_Option_Code_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        INSERT INTO Budget.Rate_Default_Meter_Attribute
             (
                 RATE_ID
                 , EC_Meter_Attribute_Id
                 , Attribute_Value_Type_Cd
                 , Start_Dt
                 , End_Dt
                 , EC_Meter_Attribute_Value
                 , Attribute_Value_Requirement_Option_Cd
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Rate_Id
            , @Attribute_Id
            , @Attribute_Value_Type_Id
            , @Start_Date
            , @End_Date
            , @Attribute_Values
            , @Required_Option_Code_Id
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE();
    END;
GO
GRANT EXECUTE ON  [dbo].[Rate_Default_Meter_Attribute_Ins] TO [CBMSApplication]
GO
