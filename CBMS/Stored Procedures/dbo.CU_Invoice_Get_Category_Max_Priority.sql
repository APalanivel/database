
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******                    
          
NAME: [DBO].[CU_Invoice_Get_Category_Max_Priority]            
               
DESCRIPTION:          
          
 To Get determinant category buckets by invoice and priority          
                
INPUT PARAMETERS:                    
NAME			DATATYPE	DEFAULT  DESCRIPTION                    
------------------------------------------------------------                    
@Cu_Invoice_Id	INT          
@Account_Id		INT			NULL          
          
OUTPUT PARAMETERS:          
NAME   DATATYPE DEFAULT  DESCRIPTION          
                 
------------------------------------------------------------                    
USAGE EXAMPLES:                    
------------------------------------------------------------            
	SET STATISTICS IO ON          
	EXEC CU_Invoice_Get_Category_Max_Priority  6964412

	SELECT * FROM bucket_Category_Rule r JOIN Bucket_Master bm WHERE bm.Bucket_Master_Id = 
	WHERE CU_Aggregation_Level_Cd = 100333 
               
AUTHOR INITIALS:                    
INITIALS	NAME                    
------------------------------------------------------------                    
PKY			Pavan K Yadalam          
HG			Harihara Suthan G
RKV         Ravi Kumar Vegesna
          
MODIFICATIONS          
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------                    
PKY			25-JUL-11	Created as a part of additional data requirement Cost/usage aggregation
HG			2014-08-04	Modified to return the categories of account commodity when invoice is not mapped to any services other than "No Other Commodity"
RKV         2016-40-04  Added two parameters Country_Id and State_Id table Country_State_Category_Bucket_Aggregation_Rule in the join to get the max priority Order as Part PF Enhancement
*/

CREATE PROCEDURE [dbo].[CU_Invoice_Get_Category_Max_Priority]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Invoice_Commodity TABLE ( Commodity_id INT )

      INSERT      INTO @Invoice_Commodity
                  ( 
                   Commodity_id )
                  SELECT
                        d.COMMODITY_TYPE_ID
                  FROM
                        dbo.CU_INVOICE_DETERMINANT d
                        INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                              ON d.CU_INVOICE_DETERMINANT_ID = da.CU_INVOICE_DETERMINANT_ID
                  WHERE
                        d.CU_INVOICE_ID = @Cu_Invoice_Id
                        AND ( @Account_Id IS NULL
                              OR @Account_Id = da.ACCOUNT_ID )
                  UNION
                  SELECT
                        chg.COMMODITY_TYPE_ID
                  FROM
                        dbo.CU_INVOICE_Charge chg
                        INNER JOIN dbo.CU_Invoice_Charge_Account chgA
                              ON chgA.CU_INVOICE_CHARGE_ID = chg.CU_INVOICE_CHARGE_ID
                  WHERE
                        chg.CU_INVOICE_ID = @Cu_Invoice_Id
                        AND ( @Account_Id IS NULL
                              OR @Account_Id = chgA.ACCOUNT_ID )
		
      INSERT      INTO @Invoice_Commodity
                  ( 
                   Commodity_id )
                  SELECT
                        Commodity_Id
                  FROM
                        core.Client_Hier_Account
                  WHERE
                        Account_Id = @Account_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Invoice_Commodity
                                         WHERE
                                          Commodity_Id <> -1 )
                  GROUP BY
                        Commodity_Id

      SELECT
            r.Category_Bucket_Master_Id
           ,bm.Bucket_Name
           ,MAX(ISNULL(cscbar.Priority_Order, r.Priority_Order)) maxpriority
           ,bm.Sort_Order
           ,cd.Code_Value Category_Bucket_Type
           ,ic.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Bucket_Category_rule r
            INNER JOIN dbo.Bucket_Master bm
                  ON r.Category_Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN @Invoice_Commodity ic
                  ON ic.Commodity_id = bm.Commodity_Id
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_id = ic.Commodity_id
            INNER JOIN dbo.Code cd
                  ON cd.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.Code aglvlCd
                  ON aglvlCd.Code_id = r.CU_Aggregation_Level_Cd
            LEFT OUTER JOIN dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
                  ON r.Category_Bucket_Master_Id = cscbar.Category_Bucket_Master_Id
                     AND ( @Country_Id IS NULL
                           OR cscbar.Country_Id = @Country_Id )
                     AND ( @State_Id IS NULL
                           OR cscbar.State_Id = @Country_Id )
      WHERE
            aglvlCd.Code_Value = 'Invoice'
      GROUP BY
            r.Category_Bucket_Master_Id
           ,bm.Bucket_Name
           ,bm.Sort_Order
           ,cd.Code_Value
           ,ic.Commodity_Id
           ,com.Commodity_Name
      ORDER BY
            cd.Code_Value DESC
           ,bm.Sort_Order ASC

END;

;
;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Get_Category_Max_Priority] TO [CBMSApplication]
GO
