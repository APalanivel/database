SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************
 NAME:  
	dbo.cbmsSsoDocument_GetSpecialDocuments
 
 DESCRIPTION:   
 INPUT PARAMETERS:  
 Name			DataType	Default Description  
------------------------------------------------------------  
@MyAccountId	int
@client_id		int			null
@division_id	int			null
@site_id		int			null
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  EXEC dbo.cbmsSsoDocument_GetSpecialDocuments 49

  
 AUTHOR INITIALS:  
	Initials		Name  
------------------------------------------------------------  
	CPE				Chaitanya Panduga Eshwar
	DRG				Dhilu Raichal George


 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 CPE	  03/25/2011	Modified the SP to replace vwCbmsSSOdocumentOwnerFlat with SSO_DOCUMENT_OWNER_MAP table
 DRG	  09/09/2014	Modified to get Category From new table dbo.Client_Document_Category           
******************************************************************************************************************/

CREATE PROCEDURE [dbo].[cbmsSsoDocument_GetSpecialDocuments]
      ( 
       @MyAccountId INT
      ,@client_id INT = NULL
      ,@division_id INT = NULL
      ,@site_id INT = NULL )
AS 
BEGIN

	
      SET NOCOUNT ON

      EXEC dbo.cbmsSecurity_GetClientAccess 
            @MyAccountId
           ,@client_id OUTPUT
           ,@division_id OUTPUT
           ,@site_id OUTPUT

    
      SELECT TOP 5
            SD.SSO_DOCUMENT_ID
           ,SD.DOCUMENT_TITLE
           ,SD.DOCUMENT_REFERENCE_DATE
           ,SD.CBMS_IMAGE_ID
           ,img.CBMS_DOC_ID
           ,left(cat.category, len(cat.category) - 1) AS category_type
      FROM
            dbo.SSO_DOCUMENT SD
            CROSS APPLY ( SELECT
                              cdc.Category_Name + ','
                          FROM
                              dbo.Client_Document_Category cdc
                              INNER  JOIN dbo.Client_Document_Category_map cdcm
                                    ON cdcm.Client_Document_Category_Id = cdc.Client_Document_Category_Id
                          WHERE
                              SD.SSO_Document_ID = cdcm.SSO_Document_ID
                              AND cdc.category_NAME <> 'SEP'
            FOR
                          XML PATH('') ) cat ( category )
            INNER JOIN ( SELECT DISTINCT
                        SSO_DOCUMENT_ID
                   FROM
                        dbo.SSO_DOCUMENT_OWNER_MAP SDOM
                        JOIN Core.Client_Hier CH
                              ON SDOM.Client_Hier_Id = CH.Client_Hier_Id
                   WHERE
                        ( @Client_Id IS NULL
                          OR CH.client_id = @client_id )
                        AND ( @division_id IS NULL
                              OR CH.Sitegroup_Id = @division_id )
                        AND ( @site_id IS NULL
                              OR CH.Site_Id = @site_id ) ) acc
                  ON SD.SSO_DOCUMENT_ID = acc.SSO_DOCUMENT_ID
            INNER JOIN dbo.cbms_image img
                  ON img.CBMS_IMAGE_ID = SD.CBMS_IMAGE_ID
      WHERE
            SD.IS_SPECIAL = 1
            AND cat.category IS NOT NULL
      ORDER BY
            SD.DOCUMENT_REFERENCE_DATE DESC
           ,SD.DOCUMENT_TITLE
	
END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_GetSpecialDocuments] TO [CBMSApplication]
GO
