SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******    
NAME:    
    dbo.Standing_Data_Exception_Group_Map_Upd   
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name    								DataType   		Default    		Description    
-----------  							------------  	-----------  	--------------------------             
@Managed_By_Group_Info_Id 				INT 
@Managed_By_User_Info_Id 				INT
@Updated_User_Id 						INT
@Standing_Data_Exception_Group_Map_Id   INT 


  
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
exec  Standing_Data_Exception_Group_Map_Upd 1111,2222,3333,4444 
    
    
AUTHOR INITIALS:    
 Initials  			Name    
-----------  		-------------------------------------------------    
 HKT   				Harish Kumar Tirumandyam  
     
MODIFICATIONS    
    
 Initials   			Date    			Modification    
-------------  			------------  		-----------------------------------    
HKT     				2020-04-23 		    created for updating table Standing_Data_Exception_Group_Map_Upd group and user 

         
******/

CREATE PROCEDURE [dbo].[Standing_Data_Exception_Group_Map_Upd]
     (
         @Managed_By_Group_Info_Id INT = NULL
         , @Managed_By_User_Info_Id INT = NULL
         , @Updated_User_Id INT
         , @Standing_Data_Exception_Group_Map_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;




        UPDATE
            SDEGM
        SET
            SDEGM.Managed_By_Group_Info_Id = @Managed_By_Group_Info_Id
            , SDEGM.Managed_By_User_Info_Id = @Managed_By_User_Info_Id
            , SDEGM.Updated_User_Id = @Updated_User_Id
            , SDEGM.Last_Change_Ts = GETDATE()
        FROM
            dbo.Standing_Data_Exception_Group_Map SDEGM
        WHERE
            SDEGM.Standing_Data_Exception_Group_Map_Id = @Standing_Data_Exception_Group_Map_Id;



    END;
GO
GRANT EXECUTE ON  [dbo].[Standing_Data_Exception_Group_Map_Upd] TO [CBMSApplication]
GO
