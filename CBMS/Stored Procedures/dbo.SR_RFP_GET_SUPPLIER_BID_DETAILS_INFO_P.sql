SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:            
 cbms_prod.dbo.SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P            
            
DESCRIPTION:            
            
            
INPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 @userId   VARCHAR            
 @sessionId   VARCHAR            
 @accountGroupId INT            
 @bidGroupId  INT            
 @supplierContactId INT            
 @rfpId    INT                     
            
OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 @priceCommentsidentityId int                               
            
            
USAGE EXAMPLES:            
------------------------------------------------------------            
EXEC dbo.SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P 1231,1,483,1,1745,19            
EXEC dbo.SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P 1,1,10011924,1,1,1    
EXEC dbo.SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P 1,1,12103458,0,1,1    
          
            
AUTHOR INITIALS:            
	Initials	Name            
------------------------------------------------------------            
	AKR			Ashok Kumar Raju 
	RR			Raghu Reddy           
            
MODIFICATIONS            
	Initials	Date		Modification            
------------------------------------------------------------            
	AKR			2012-10-12	Modified the code to include Broker_Included_Type_Id as a part of POCO            
	RR			2016-04-15	GCS-681 Added time zone columns  
	NR			2017-07-31	MAINT-5532 - Replaced varchar type in convert function insted of INT in  Broker_Fee_Concatenated Column.      
    RKV         2018-02-09	MAINT-6727 - Added another filter in on clause for table SR_RFP_LP_CLIENT_APPROVAL     
******/            
            
CREATE PROCEDURE [dbo].[SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P]
      ( 
       @userId VARCHAR
      ,@sessionId VARCHAR
      ,@accountGroupId INT
      ,@bidGroupId INT
      ,@supplierContactId INT
      ,@rfpId INT )
AS 
BEGIN        
      SET NOCOUNT ON;        
        
      DECLARE @STATUS VARCHAR(50)            
      DECLARE
            @duedate_noSuppContId VARCHAR(20)
           ,@Closed_Entity INT
           ,@Commodity_Id INT            
                 
            
      SELECT
            @Commodity_Id = COMMODITY_TYPE_ID
      FROM
            dbo.SR_RFP
      WHERE
            SR_RFP_ID = @rfpId            
            
      SELECT
            @Closed_Entity = ENTITY_ID
      FROM
            ENTITY
      WHERE
            ENTITY_NAME = 'Closed'
            AND ENTITY_TYPE = 1029            
            
      DECLARE @t_lp_size1 TABLE
            ( 
             [account_group_id] [int]
            ,[lp_size] [numeric](32, 16) )            
            
      DECLARE @Broker_Fee_Bid_Group VARCHAR(500) = ''
      SELECT
            @Broker_Fee_Bid_Group = @Broker_Fee_Bid_Group + ISNULL(CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acc_broker_fee.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) - 1)
                                                                        ELSE SUBSTRING(CAST(acc_broker_fee.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acc_broker_fee.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acc_broker_fee.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                                                                                WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acc_broker_fee.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acc_broker_fee.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                                                                                ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acc_broker_fee.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acc_broker_fee.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acc_broker_fee.Broker_Fee AS VARCHAR)))))))
                                                                                                                                                                                                              END
                                                                   END, '') + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name + ','
      FROM
            dbo.SR_RFP_ACCOUNT sra
            INNER JOIN dbo.Account_Commodity_Broker_Fee acc_broker_fee
                  ON acc_broker_fee.Account_Id = sra.ACCOUNT_ID
                     AND acc_broker_fee.Commodity_Id = @Commodity_Id
            LEFT JOIN dbo.CURRENCY_UNIT ecur
                  ON acc_broker_fee.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
            LEFT JOIN dbo.ENTITY euom
                  ON acc_broker_fee.UOM_Entity_Id = euom.ENTITY_ID
      WHERE
            sra.SR_RFP_BID_GROUP_ID = @accountGroupId
      GROUP BY
            acc_broker_fee.Broker_Fee
           ,ecur.CURRENCY_UNIT_NAME
           ,euom.Entity_Name
           
      
            
      INSERT      INTO @t_lp_size1
                  ( 
                   account_group_id
                  ,lp_size )
                  SELECT
                        Duplicate_group_id
                       ,lp_size
                  FROM
                        dbo.SR_RFP_FN_SIZE(@rfpId)            
            
      SELECT
            @duedate_noSuppContId = CAST(DATEPART(month, CONVERT(CHAR(20), MIN(DUE_DATE), 100)) AS VARCHAR) + '/' + CAST(DATEPART(day, CONVERT(CHAR(20), MIN(DUE_DATE), 100)) AS VARCHAR) + '/' + SUBSTRING(CAST(DATEPART(year, CONVERT(CHAR(20), MIN(DUE_DATE), 100)) AS VARCHAR), 3, 2) + ' ' + CAST(SUBSTRING(CONVERT(CHAR(20), MIN(DUE_DATE), 100), 12, 8) AS VARCHAR)
      FROM
            dbo.SR_RFP_SEND_SUPPLIER
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND is_bid_group = @bidGroupId            
            
      IF @bidGroupId = 0 
            BEGIN            
            
                  SELECT
                        ch.CLIENT_NAME
                       ,ch.SITE_ID
                       ,ch.SITE_NAME
                       ,acc.ACCOUNT_ID
                       ,acc.ACCOUNT_NUMBER
                       ,sra.SR_RFP_ACCOUNT_ID SR_ACCOUNT_GROUP_ID
                       ,NULL GROUP_NAME
                       ,ISNULL(CAST(DATEPART(month, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + CAST(DATEPART(day, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + SUBSTRING(CAST(DATEPART(year, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR), 3, 2) + ' ' + CAST(SUBSTRING(CONVERT(CHAR(20), DUE_DATE, 100), 12, 8) AS VARCHAR), @duedate_noSuppContId) DUE_DATE
                       ,ISNULL(lp.lp_size, -1) AS LP_SIZE
                       ,CASE srlp.IS_GROUP_LP
                          WHEN 1 THEN 'YES'
                          ELSE 'NO'
                        END IS_GROUP_LP
                       ,srlp.CBMS_IMAGE_ID
                       ,ISNULL(lp.lp_size, -1) AS SITE_LP_SIZE
                       ,CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acbf.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acbf.Broker_Fee AS VARCHAR)) - 1)
                             ELSE SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                 WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                 ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR)))))))
                                                                                                                                               END
                        END + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name AS Broker_Fee_Concatenated
                       ,acc.Is_Broker_Account
                       ,NULL AS Broker_Fee
                       ,Timezone.Due_Dt_By_Timezone
                       ,Timezone.Due_Dt_Time_Zone
                       ,Timezone.Due_Dt_Time_Zone_Code
                       ,Timezone.Due_Dt_GMT
                  FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.ACCOUNT acc
                              ON ch.Site_Id = acc.SITE_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT sra
                              ON sra.ACCOUNT_ID = acc.ACCOUNT_ID
                        LEFT JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                              ON sra.SR_RFP_ACCOUNT_ID = srss.SR_ACCOUNT_GROUP_ID
                                 AND sra.is_deleted = 0
                                 AND srss.IS_BID_GROUP = @bidGroupId
                                 AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                        LEFT JOIN @t_lp_size1 lp
                              ON lp.account_group_id = sra.SR_RFP_ACCOUNT_ID
                        LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srlp
                              ON ch.SITE_ID = srlp.SITE_ID
                                 AND srlp.SR_RFP_ID = sra.SR_RFP_ID
                                 AND srlp.IS_GROUP_LP = 1
                        LEFT JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON acbf.Account_Id = acc.ACCOUNT_ID
                                 AND acbf.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.CURRENCY_UNIT ecur
                              ON acbf.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
                        LEFT JOIN dbo.ENTITY euom
                              ON acbf.UOM_Entity_Id = euom.ENTITY_ID
                        LEFT JOIN ( SELECT
                                          srss.SR_ACCOUNT_GROUP_ID
                                         ,srss.Due_Dt_By_Timezone
                                         ,tz.Time_Zone AS Due_Dt_Time_Zone
                                         ,tz.Time_Zone_Code AS Due_Dt_Time_Zone_Code
                                         ,tz.GMT AS Due_Dt_GMT
                                         ,RANK() OVER ( PARTITION BY srss.SR_ACCOUNT_GROUP_ID ORDER BY tz.Time_Zone_Code ) AS Timezone_Cnt
                                    FROM
                                          dbo.SR_RFP_SEND_SUPPLIER srss
                                          LEFT JOIN dbo.Time_Zone tz
                                                ON srss.Time_Zone_Id = tz.Time_Zone_Id
                                    WHERE
                                          srss.Is_Bid_Group = 0
                                          AND srss.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                    GROUP BY
                                          srss.SR_ACCOUNT_GROUP_ID
                                         ,srss.Due_Dt_By_Timezone
                                         ,tz.Time_Zone
                                         ,tz.Time_Zone_Code
                                         ,tz.GMT ) Timezone
                              ON Timezone.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_ACCOUNT_ID
                                 AND Timezone.Timezone_Cnt = 1
                  WHERE
                        sra.SR_RFP_ACCOUNT_ID = @accountGroupId
                        AND sra.SR_RFP_BID_GROUP_ID IS NULL
                        AND sra.BID_STATUS_TYPE_ID != @Closed_Entity
                  UNION
                  SELECT
                        sraa.CLIENT_NAME
                       ,ch.SITE_ID
                       ,sraa.SITE_NAME
                       ,acc.ACCOUNT_ID
                       ,sraa.ACCOUNT_NUMBER
                       ,sraa.SR_RFP_ACCOUNT_ID SR_ACCOUNT_GROUP_ID
                       ,NULL GROUP_NAME
                       ,NULL DUE_DATE
                       ,ISNULL(lp.lp_size, -1) AS LP_SIZE
                       ,CASE srca.IS_GROUP_LP
                          WHEN 1 THEN 'YES'
                          ELSE 'NO'
                        END IS_GROUP_LP
                       ,srca.CBMS_IMAGE_ID
                       ,ISNULL(lp.lp_size, -1) AS SITE_LP_SIZE
                       ,CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acbf.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acbf.Broker_Fee AS VARCHAR)) - 1)
                             ELSE SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                 WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                 ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR)))))))
                                                                                                                                               END
                        END + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name AS Broker_Fee_Concatenated
                       ,acc.Is_Broker_Account
                       ,NULL AS Broker_Fee
                       ,NULL AS Due_Dt_By_Timezone
                       ,NULL AS Due_Dt_Time_Zone
                       ,NULL AS Due_Dt_Time_Zone_Code
                       ,NULL AS Due_Dt_GMT
                  FROM
                        core.Client_Hier ch
                        INNER JOIN dbo.ACCOUNT acc
                              ON ch.Site_Id = acc.SITE_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT sra
                              ON acc.ACCOUNT_ID = sra.ACCOUNT_ID
                        INNER JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT sraa
                              ON sra.SR_RFP_ACCOUNT_ID = sraa.SR_RFP_ACCOUNT_ID
                        LEFT JOIN @t_lp_size1 lp
                              ON lp.account_group_id = sra.SR_RFP_ACCOUNT_ID
                        LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srca
                              ON ch.SITE_ID = srca.SITE_ID
                                 AND srca.SR_RFP_ID = sra.SR_RFP_ID
                                 AND srca.IS_GROUP_LP = 1
                        LEFT JOIN dbo.Account_Commodity_Broker_Fee acbf
                              ON acbf.Account_Id = acc.ACCOUNT_ID
                                 AND acbf.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.CURRENCY_UNIT ecur
                              ON acbf.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
                        LEFT JOIN dbo.ENTITY euom
                              ON acbf.UOM_Entity_Id = euom.ENTITY_ID
                  WHERE
                        sra.is_deleted = 0
                        AND sra.SR_RFP_ID = @rfpId
                        AND sra.SR_RFP_ACCOUNT_ID = @accountGroupId
                        AND sra.SR_RFP_BID_GROUP_ID IS NULL
                        AND sra.BID_STATUS_TYPE_ID = @Closed_Entity            
            
            END            
            
      ELSE 
            IF @bidGroupId = 1 
                  BEGIN            
            
                        SELECT
                              @STATUS = ( SELECT
                                                dbo.SR_RFP_FN_GET_BID_GROUP_STATUS(@accountGroupId) )            
            
                        PRINT @STATUS            
            
                        IF @STATUS = 'Closed' 
                              BEGIN            
             
             
                                    ( SELECT
                                          sraa.CLIENT_NAME
                                         ,ch.SITE_ID
                                         ,sraa.SITE_NAME
                                         ,acc.ACCOUNT_ID
                                         ,sraa.ACCOUNT_NUMBER
                                         ,sra.SR_RFP_BID_GROUP_ID SR_ACCOUNT_GROUP_ID
                                         ,srbg.GROUP_NAME
                                         ,ISNULL(CAST(DATEPART(month, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + CAST(DATEPART(day, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + SUBSTRING(CAST(DATEPART(year, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR), 3, 2) + ' ' + CAST(SUBSTRING(CONVERT(CHAR(20), DUE_DATE, 100), 12, 8) AS VARCHAR), @duedate_noSuppContId) DUE_DATE
                                         ,ISNULL(lp.lp_size, -1) AS LP_SIZE
                                         ,CASE srca.IS_GROUP_LP
                                            WHEN 1 THEN 'YES'
                                            ELSE 'NO'
                                          END IS_GROUP_LP
                                         ,srca.CBMS_IMAGE_ID
                                         ,ISNULL(lp.lp_size, -1) AS SITE_LP_SIZE
                                         ,LEFT(@Broker_Fee_Bid_Group, LEN(NULLIF(@Broker_Fee_Bid_Group, '')) - 1) Broker_Fee_Concatenated
                                         ,acc.Is_Broker_Account
                                         ,CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acbf.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acbf.Broker_Fee AS VARCHAR)) - 1)
                                               ELSE SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                                   WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                                   ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR)))))))
                                                                                                                                                                 END
                                          END + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name AS Broker_Fee
                                         ,Timezone.Due_Dt_By_Timezone
                                         ,Timezone.Due_Dt_Time_Zone
                                         ,Timezone.Due_Dt_Time_Zone_Code
                                         ,Timezone.Due_Dt_GMT
                                      FROM
                                          core.Client_Hier ch
                                          INNER JOIN dbo.ACCOUNT acc
                                                ON ch.Site_Id = acc.SITE_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT sra
                                                ON acc.ACCOUNT_ID = sra.ACCOUNT_ID
                                          INNER JOIN dbo.SR_RFP_BID_GROUP srbg
                                                ON sra.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                                          LEFT JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                                ON sra.SR_RFP_BID_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                                   AND srss.IS_BID_GROUP = @bidGroupId
                                                   AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                                          LEFT JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT sraa
                                                ON sra.SR_RFP_ACCOUNT_ID = sraa.SR_RFP_ACCOUNT_ID
                                          LEFT JOIN @t_lp_size1 lp
                                                ON lp.account_group_id = sra.SR_RFP_ACCOUNT_ID
                                          LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srca
                                                ON ch.SITE_ID = srca.SITE_ID
                                                   AND srca.SR_RFP_ID = sra.SR_RFP_ID
                                                   AND srca.IS_GROUP_LP = 1
                                          LEFT JOIN dbo.Account_Commodity_Broker_Fee acbf
                                                ON acbf.Account_Id = acc.ACCOUNT_ID
                                                   AND acbf.Commodity_Id = @Commodity_Id
                                          LEFT JOIN dbo.CURRENCY_UNIT ecur
                                                ON acbf.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
                                          LEFT JOIN dbo.ENTITY euom
                                                ON acbf.UOM_Entity_Id = euom.ENTITY_ID
                                          LEFT JOIN ( SELECT
                                                            srss.SR_ACCOUNT_GROUP_ID
                                                           ,srss.Due_Dt_By_Timezone
                                                           ,tz.Time_Zone AS Due_Dt_Time_Zone
                                                           ,tz.Time_Zone_Code AS Due_Dt_Time_Zone_Code
                                                           ,tz.GMT AS Due_Dt_GMT
                                                           ,RANK() OVER ( PARTITION BY srss.SR_ACCOUNT_GROUP_ID ORDER BY tz.Time_Zone_Code ) AS Timezone_Cnt
                                                      FROM
                                                            dbo.SR_RFP_SEND_SUPPLIER srss
                                                            LEFT JOIN dbo.Time_Zone tz
                                                                  ON srss.Time_Zone_Id = tz.Time_Zone_Id
                                                      WHERE
                                                            srss.Is_Bid_Group = 1
                                                            AND srss.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                      GROUP BY
                                                            srss.SR_ACCOUNT_GROUP_ID
                                                           ,srss.Due_Dt_By_Timezone
                                                           ,tz.Time_Zone
                                                           ,tz.Time_Zone_Code
                                                           ,tz.GMT ) Timezone
                                                ON Timezone.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_BID_GROUP_ID
                                                   AND Timezone.Timezone_Cnt = 1
                                      WHERE
                                          sra.is_deleted = 0
                                          AND sra.SR_RFP_BID_GROUP_ID = @accountGroupId
                                          AND sra.SR_RFP_BID_GROUP_ID IS NOT NULL
                                      GROUP BY
                                          ch.CLIENT_ID
                                         ,sraa.CLIENT_NAME
                                         ,ch.SITE_ID
                                         ,sraa.SITE_NAME
                                         ,acc.ACCOUNT_ID
                                         ,sraa.ACCOUNT_NUMBER
                                         ,sra.SR_RFP_BID_GROUP_ID
                                         ,srbg.GROUP_NAME
                                         ,DUE_DATE
                                         ,srca.IS_GROUP_LP
                                         ,srca.CBMS_IMAGE_ID
                                         ,lp.lp_size
                                         ,acc.Is_Broker_Account
                                         ,acbf.Broker_Fee
                                         ,ecur.CURRENCY_UNIT_NAME
                                         ,euom.Entity_Name
                                         ,Timezone.Due_Dt_By_Timezone
                                         ,Timezone.Due_Dt_Time_Zone
                                         ,Timezone.Due_Dt_Time_Zone_Code
                                         ,Timezone.Due_Dt_GMT)
                                    ORDER BY
                                          GROUP_NAME            
                              END            
            
            
                        ELSE 
                              IF @STATUS = 'Not Closed' 
                                    BEGIN            
                                          PRINT 'INSIDE NOT CLOSED'            
            
                                          ( SELECT
                                                ch.CLIENT_NAME
                                               ,ch.SITE_ID
                                               ,ch.SITE_NAME
                                               ,acc.ACCOUNT_ID
                                               ,acc.ACCOUNT_NUMBER
                                               ,sra.SR_RFP_BID_GROUP_ID SR_ACCOUNT_GROUP_ID
                                               ,srbg.GROUP_NAME
                                               ,ISNULL(CAST(DATEPART(month, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + CAST(DATEPART(day, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + SUBSTRING(CAST(DATEPART(year, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR), 3, 2) + ' ' + CAST(SUBSTRING(CONVERT(CHAR(20), DUE_DATE, 100), 12, 8) AS VARCHAR), @duedate_noSuppContId) DUE_DATE
                                               ,ISNULL(lp.lp_size, -1) AS LP_SIZE
                                               ,CASE srca.IS_GROUP_LP
                                                  WHEN 1 THEN 'YES'
                                                  ELSE 'NO'
                                                END IS_GROUP_LP
                                               ,srca.CBMS_IMAGE_ID
                                               ,ISNULL(lp.lp_size, -1) AS SITE_LP_SIZE
                                               ,LEFT(@Broker_Fee_Bid_Group, LEN(NULLIF(@Broker_Fee_Bid_Group, '')) - 1) Broker_Fee_Concatenated
                                               ,acc.Is_Broker_Account
                                               ,CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acbf.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acbf.Broker_Fee AS VARCHAR)) - 1)
                                                     ELSE SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                                         WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                                         ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR)))))))
                                                                                                                                                                       END
                                                END + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name AS Broker_Fee
                                               ,Timezone.Due_Dt_By_Timezone
                                               ,Timezone.Due_Dt_Time_Zone
                                               ,Timezone.Due_Dt_Time_Zone_Code
                                               ,Timezone.Due_Dt_GMT
                                            FROM
                                                core.Client_Hier ch
                                                INNER JOIN dbo.ACCOUNT acc
                                                      ON ch.Site_Id = acc.SITE_ID
                                                INNER JOIN dbo.SR_RFP_ACCOUNT sra
                                                      ON acc.ACCOUNT_ID = sra.ACCOUNT_ID
                                                INNER JOIN dbo.SR_RFP_BID_GROUP srbg
                                                      ON sra.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                                                LEFT JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                                      ON sra.SR_RFP_BID_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                                         AND srss.IS_BID_GROUP = 1
                                                         AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                                                LEFT JOIN @t_lp_size1 lp
                                                      ON lp.account_group_id = sra.SR_RFP_ACCOUNT_ID
                                                LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srca
                                                      ON ch.SITE_ID = srca.SITE_ID
                                                         AND srca.SR_RFP_ID = sra.SR_RFP_ID
                                                         AND srca.IS_GROUP_LP = 1
                                                LEFT JOIN dbo.Account_Commodity_Broker_Fee acbf
                                                      ON acbf.Account_Id = acc.ACCOUNT_ID
                                                         AND acbf.Commodity_Id = @Commodity_Id
                                                LEFT JOIN dbo.CURRENCY_UNIT ecur
                                                      ON acbf.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
                                                LEFT JOIN dbo.ENTITY euom
                                                      ON acbf.UOM_Entity_Id = euom.ENTITY_ID
                                                LEFT JOIN ( SELECT
                                                                  srss.SR_ACCOUNT_GROUP_ID
                                                                 ,srss.Due_Dt_By_Timezone
                                                                 ,tz.Time_Zone AS Due_Dt_Time_Zone
                                                                 ,tz.Time_Zone_Code AS Due_Dt_Time_Zone_Code
                                                                 ,tz.GMT AS Due_Dt_GMT
                                                                 ,RANK() OVER ( PARTITION BY srss.SR_ACCOUNT_GROUP_ID ORDER BY tz.Time_Zone_Code ) AS Timezone_Cnt
                                                            FROM
                                                                  dbo.SR_RFP_SEND_SUPPLIER srss
                                                                  LEFT JOIN dbo.Time_Zone tz
                                                                        ON srss.Time_Zone_Id = tz.Time_Zone_Id
                                                            WHERE
                                                                  srss.Is_Bid_Group = 1
                                                                  AND srss.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                            GROUP BY
                                                                  srss.SR_ACCOUNT_GROUP_ID
                                                                 ,srss.Due_Dt_By_Timezone
                                                                 ,tz.Time_Zone
                                                                 ,tz.Time_Zone_Code
                                                                 ,tz.GMT ) Timezone
                                                      ON Timezone.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_BID_GROUP_ID
                                                         AND Timezone.Timezone_Cnt = 1
                                            WHERE
                                                sra.is_deleted = 0
                                                AND sra.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                AND sra.BID_STATUS_TYPE_ID != @Closed_Entity
                                            GROUP BY
                                                ch.CLIENT_NAME
                                               ,ch.SITE_ID
                                               ,ch.SITE_NAME
                                               ,acc.ACCOUNT_ID
                                               ,acc.ACCOUNT_NUMBER
                                               ,sra.SR_RFP_BID_GROUP_ID
                                               ,lp.lp_size
                                               ,srbg.GROUP_NAME
                                               ,srss.DUE_DATE
                                               ,srca.IS_GROUP_LP
                                               ,srca.CBMS_IMAGE_ID
                                               ,acc.Is_Broker_Account
                                               ,acbf.Broker_Fee
                                               ,ecur.CURRENCY_UNIT_NAME
                                               ,euom.Entity_Name
                                               ,Timezone.Due_Dt_By_Timezone
                                               ,Timezone.Due_Dt_Time_Zone
                                               ,Timezone.Due_Dt_Time_Zone_Code
                                               ,Timezone.Due_Dt_GMT)
                                          UNION
                                          ( SELECT
                                                ch.CLIENT_NAME
                                               ,ch.SITE_ID
                                               ,ch.SITE_NAME
                                               ,acc.ACCOUNT_ID
                                               ,sraa.ACCOUNT_NUMBER
                                               ,sra.SR_RFP_BID_GROUP_ID SR_ACCOUNT_GROUP_ID
                                               ,srbg.GROUP_NAME
                                               ,ISNULL(CAST(DATEPART(month, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + CAST(DATEPART(day, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR) + '/' + SUBSTRING(CAST(DATEPART(year, CONVERT(CHAR(20), DUE_DATE, 100)) AS VARCHAR), 3, 2) + ' ' + CAST(SUBSTRING(CONVERT(CHAR(20), DUE_DATE, 100), 12, 8) AS VARCHAR), @duedate_noSuppContId) DUE_DATE
                                               ,ISNULL(lp.lp_size, -1) AS LP_SIZE
                                               ,CASE srca.IS_GROUP_LP
                                                  WHEN 1 THEN 'YES'
                                                  ELSE 'NO'
                                                END IS_GROUP_LP
                                               ,srca.CBMS_IMAGE_ID
                                               ,ISNULL(lp.lp_size, -1) AS SITE_LP_SIZE
                                               ,LEFT(@Broker_Fee_Bid_Group, LEN(NULLIF(@Broker_Fee_Bid_Group, '')) - 1) Broker_Fee_Concatenated
                                               ,acc.Is_Broker_Account
                                               ,CASE WHEN ( PATINDEX('%.%[1-9]%', CAST(acbf.Broker_Fee AS VARCHAR)) = 0 ) THEN SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, PATINDEX('%.%', CAST(acbf.Broker_Fee AS VARCHAR)) - 1)
                                                     ELSE SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), 1, CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) - 1) + '.' + CASE LEN(REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))))
                                                                                                                                                                         WHEN 1 THEN REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR))))))) + '0'
                                                                                                                                                                         ELSE REVERSE(CONVERT(VARCHAR, CONVERT(BIGINT, REVERSE(SUBSTRING(CAST(acbf.Broker_Fee AS VARCHAR), CHARINDEX('.', CAST(acbf.Broker_Fee AS VARCHAR)) + 1, LEN(CAST(acbf.Broker_Fee AS VARCHAR)))))))
                                                                                                                                                                       END
                                                END + SPACE(1) + ecur.CURRENCY_UNIT_NAME + '/' + euom.Entity_Name AS Broker_Fee
                                               ,Timezone.Due_Dt_By_Timezone
                                               ,Timezone.Due_Dt_Time_Zone
                                               ,Timezone.Due_Dt_Time_Zone_Code
                                               ,Timezone.Due_Dt_GMT
                                            FROM
                                                core.Client_Hier ch
                                                INNER JOIN dbo.ACCOUNT acc
                                                      ON ch.Site_Id = acc.SITE_ID
                                                INNER JOIN dbo.SR_RFP_ACCOUNT sra
                                                      ON acc.ACCOUNT_ID = sra.ACCOUNT_ID
                                                INNER JOIN dbo.SR_RFP_BID_GROUP srbg
                                                      ON sra.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                                                LEFT JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                                      ON sra.SR_RFP_BID_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                                         AND srss.IS_BID_GROUP = 1
                                                         AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
                                                LEFT JOIN dbo.SR_RFP_ARCHIVE_ACCOUNT sraa
                                                      ON sra.SR_RFP_ACCOUNT_ID = sraa.SR_RFP_ACCOUNT_ID
                                                LEFT JOIN @t_lp_size1 lp
                                                      ON lp.account_group_id = sra.SR_RFP_ACCOUNT_ID
                                                LEFT JOIN dbo.SR_RFP_LP_CLIENT_APPROVAL srca
                                                      ON ch.SITE_ID = srca.SITE_ID
                                                       AND srca.SR_RFP_ID = sra.SR_RFP_ID
                                                         AND srca.IS_GROUP_LP = 1
                                                LEFT JOIN dbo.Account_Commodity_Broker_Fee acbf
                                                      ON acbf.Account_Id = acc.ACCOUNT_ID
                                                         AND acbf.Commodity_Id = @Commodity_Id
                                                LEFT JOIN dbo.CURRENCY_UNIT ecur
                                                      ON acbf.Currency_Unit_Id = ecur.CURRENCY_UNIT_ID
                                                LEFT JOIN dbo.ENTITY euom
                                                      ON acbf.UOM_Entity_Id = euom.ENTITY_ID
                                                LEFT JOIN ( SELECT
                                                                  srss.SR_ACCOUNT_GROUP_ID
                                                                 ,srss.Due_Dt_By_Timezone
                                                                 ,tz.Time_Zone AS Due_Dt_Time_Zone
                                                                 ,tz.Time_Zone_Code AS Due_Dt_Time_Zone_Code
                                                                 ,tz.GMT AS Due_Dt_GMT
                                                                 ,RANK() OVER ( PARTITION BY srss.SR_ACCOUNT_GROUP_ID ORDER BY tz.Time_Zone_Code ) AS Timezone_Cnt
                                                            FROM
                                                                  dbo.SR_RFP_SEND_SUPPLIER srss
                                                                  LEFT JOIN dbo.Time_Zone tz
                                                                        ON srss.Time_Zone_Id = tz.Time_Zone_Id
                                                            WHERE
                                                                  srss.Is_Bid_Group = 1
                                                                  AND srss.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                            GROUP BY
                                                                  srss.SR_ACCOUNT_GROUP_ID
                                                                 ,srss.Due_Dt_By_Timezone
                                                                 ,tz.Time_Zone
                                                                 ,tz.Time_Zone_Code
                                                                 ,tz.GMT ) Timezone
                                                      ON Timezone.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_BID_GROUP_ID
                                                         AND Timezone.Timezone_Cnt = 1
                                            WHERE
                                                sra.is_deleted = 0
                                                AND sra.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                AND sra.BID_STATUS_TYPE_ID = @Closed_Entity
                                            GROUP BY
                                                ch.CLIENT_NAME
                                               ,ch.SITE_ID
                                               ,ch.SITE_NAME
                                               ,acc.ACCOUNT_ID
                                               ,sraa.ACCOUNT_NUMBER
                                               ,sra.SR_RFP_BID_GROUP_ID
                                               ,srbg.GROUP_NAME
                                               ,DUE_DATE
                                               ,srca.IS_GROUP_LP
                                               ,srca.CBMS_IMAGE_ID
                                               ,lp.lp_size
                                               ,acc.Is_Broker_Account
                                               ,acbf.Broker_Fee
                                               ,ecur.CURRENCY_UNIT_NAME
                                               ,euom.Entity_Name
                                               ,Timezone.Due_Dt_By_Timezone
                                               ,Timezone.Due_Dt_Time_Zone
                                               ,Timezone.Due_Dt_Time_Zone_Code
                                               ,Timezone.Due_Dt_GMT)
                                          ORDER BY
                                                srbg.GROUP_NAME            
            
                                    END        
            
            
                  END         
END;

;
;
;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_BID_DETAILS_INFO_P] TO [CBMSApplication]
GO
