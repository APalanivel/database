SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




























/******                              
 
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	  [dbo].[Get_Variance_Resut_Exception_Variance_log_Ids]
		@Account_Id =1838636 , --1407581 1405819
		@Commodity_Id = 67,
		@Service_Month = N'2019-11-01'

				2019-11-01

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
  
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
Ap					march 5,2020		THe procedure is created to get the list of variance_log_ids
******/
CREATE PROCEDURE [dbo].[Get_Variance_Result_Exception_Variance_log_Ids]
      (
      @account_id    INT
    , @commodity_id  INT
    , @Service_Month DATE )
AS
      BEGIN

            SET NOCOUNT ON;
            SELECT       
                        vl.Variance_Log_Id
                      , vl.Account_ID
                      , vl.Commodity_ID
                      , vl.Service_Month
                      , vl.Category_Name
            FROM        dbo.Variance_Log vl
            WHERE       vl.Is_Failure = 1
                        AND   vl.Partition_Key IS NULL
                        AND   vl.Account_ID = @account_id
                        AND   vl.Commodity_ID = @commodity_id
                        AND   vl.Service_Month = @Service_Month
                        AND   EXISTS
                  (     SELECT
                              1
                        FROM  dbo.Variance_Result_Exception vc
                              JOIN
                              dbo.Code c
                                    ON c.Code_Id = vc.Category_id
                                       AND vl.Category_Name = c.Code_Value
                                       AND vl.Client_ID = vc.Client_id
                                       AND vl.Test_Description LIKE '%' + vc.Test_Description + '%' 
									   );

      END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Result_Exception_Variance_log_Ids] TO [CBMSApplication]
GO
