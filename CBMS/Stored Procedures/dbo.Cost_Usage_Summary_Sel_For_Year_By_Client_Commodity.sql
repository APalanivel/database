
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:              
 dbo.Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity            
            
DESCRIPTION:            
  Used to generate cost & usage monthly report based on the given input.            
             
 INPUT PARAMETERS:              
 Name					DataType  Default Description              
------------------------------------------------------------              
 @Client_Id				int                                 
 @Commodity_id			int                                 
 @Report_Year			SMALLINT               
 @currency_unit_id		int                                 
 @Uom_Id				int                                 
 @Sitegroup_id			int        null                     
 @Site_Not_Managed		int        -1   
 @Country_Id  int		null           
              
 OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------   
           
 USAGE EXAMPLES:            
------------------------------------------------------------      
	EXEC dbo.Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity 235,290,2009,3,12,NULL,-1
	EXEC dbo.Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity 235,290,2009,3,12,NULL,0
	EXEC Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity 141,67,2011,3,1409,NULL,-1,4  
	EXEC Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity 141,67,2011,3,1409,NULL,-1  
  
 SELECT DISTINCT TOP 10
      sd.Site_id
     ,bm.COmmodity_id
     ,year(Service_Month) Year
     ,s.Client_Id
 FROM
      dbo.Cost_Usage_Site_Dtl sd
      JOIN dbo.Bucket_Master bm
            ON bm.Bucket_Master_Id = sd.Bucket_Master_Id
      JOIN dbo.Site s
            ON s.Site_Id = sd.SIte_Id   
           
SELECT * FROM Commodity WHERE Commodity_id = 67  
             
AUTHOR INITIALS:              
 Initials	Name              
------------------------------------------------------------              
 HG			Hari             
 BCH		Balaraju            
 AP			Athmaram Pabbathi 
 RR			Raghu Reddy 
  
 MODIFICATIONS               
 Initials	Date		Modification              
------------------------------------------------------------              
 HG			03/31/2010 Created            
 HG			04/01/2010 Script modified to fix the issue with the fiscal year calculation.            
					   If fiscal start month is January then fiscal start date is 01-01-<Report Year> and End Date is 12-31-<Report Year>            
					   otherwise previous will be considered for the fiscal start and end date calculation.  
				  
						 Changed the condition in Cte_fy             
						 dd.Year_Num = @Report_Year to            
						   (CASE            
							 WHEN dd.Month_Num = 1 THEN @report_year            
							 ELSE @Report_Year - 1            
						   END            
							 )            
 BCH		10/10/2011 Added Data_Source_code column in the select list and Added code table with left outer join            
 AP			2011-12-22 Removed & modified logic for CTE_fy and replaced with @Service_Month; removed Data_Source_Cd changes  
 AP			2012-03-14 Following changes has been made as apart of Addl Data Prj  
						- Used table variable @Cost_Usage_Bucket_Id to store the Cost & Usage buckets for given @commodity_id  
						- Used Bucket_Master tables to calculate the total cost & usage values  
						- Removed @Service_Month table variable & used meta.Date_Dim  
						- Removed dbo.Site table and used Core.Client_Hier to get list of sites  
						- Replaced Site_Id with Client_Hier_Id on CUSD  
BCH			2012-08-22  (MAINT 1496 ). Added @Country_Id parameter as optional. 
RR			2013-02-26	Maint-1378/MAINT-1751(sub task for db) For @Site_Not_Managed, aplication is passing -1 for all sites 
						but in procdure the default value defined as null to get all sites, modified it to -1
 
******/    
CREATE PROCEDURE dbo.Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity
      ( 
       @Client_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@Currency_Unit_Id INT
      ,@UOM_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Not_Managed INT = -1
      ,@Country_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
  
      DECLARE
            @Currency_Group_Id INT
           ,@Begin_Dt DATE
           ,@End_Dt DATE  
  
      DECLARE @Client_Sites TABLE
            ( 
             Client_Hier_Id INT NOT NULL
            ,Site_Id INT NOT NULL
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) )  
  
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )  
              
      INSERT      INTO @Client_Sites
                  ( 
                   Client_Hier_Id
                  ,Site_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,ch.Site_Id
                  FROM
                        Core.Client_Hier ch
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ( @Sitegroup_Id IS NULL
                              OR ch.Sitegroup_Id = @Sitegroup_Id )
                        AND ( @Site_Not_Managed = -1
                              OR ch.Site_Not_Managed = @Site_Not_Managed )
                        AND ( @Country_Id IS NULL
                              OR ch.Country_Id = @Country_Id )
                        AND ch.Site_Id > 0  
                     
  
--Inserting Cost & Usage Buckets to table variable  
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id  
  
  
      SELECT
            @Currency_Group_id = ch.Client_Currency_Group_Id
           ,@Begin_Dt = dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))
           ,@End_Dt = dateadd(MONTH, -1, ( dateadd(YEAR, 1, dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))) ))
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0 ;  
  
  
      WITH  Cte_Ip_Data
              AS ( SELECT
                        ip.SERVICE_MONTH
                       ,max(case WHEN ip.Is_Expected = 1
                                      AND ip.Is_Received = 1 THEN 1
                                 ELSE 0
                            END) Is_Complete
                       ,max(convert(INT, ip.Is_Received)) Is_published
                       ,max(case WHEN ip.Recalc_Under_Review = 1
                                      OR ip.Variance_Under_Review = 1 THEN 1
                                 ELSE 0
                            END) Is_Under_Review
                   FROM
                        dbo.Invoice_Participation ip
                        JOIN @Client_Sites s
                              ON s.SITE_ID = ip.SITE_ID
                   WHERE
                        ip.SERVICE_MONTH BETWEEN @Begin_Dt AND @End_Dt
                   GROUP BY
                        ip.Service_Month),
            Cte_Cost_Usage_Summary
              AS ( SELECT
                        cus.Service_Month
                       ,sum(case WHEN CUB.Bucket_Type = 'Charge' THEN cus.Bucket_Value * cc.Conversion_Factor
                                 ELSE 0
                            END) AS Total_Cost
                       ,sum(case WHEN CUB.Bucket_Type = 'Determinant' THEN cus.Bucket_Value * uc.Conversion_Factor
                                 ELSE 0
                            END) AS Volume
                   FROM
                        dbo.Cost_Usage_Site_Dtl cus
                        JOIN @Client_Sites s
                              ON s.Client_Hier_Id = cus.Client_Hier_Id
                        JOIN @Cost_Usage_Bucket_Id CUB
                              ON CUB.Bucket_Master_Id = cus.Bucket_Master_Id
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_ID = cus.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = @UOM_Id
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON cc.Currency_Group_Id = @Currency_Group_id
                                 AND cc.Base_Unit_Id = cus.Currency_Unit_ID
                                 AND cc.Converted_Unit_Id = @Currency_Unit_Id
                                 AND cc.Conversion_Date = cus.Service_Month
                   WHERE
                        cus.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                   GROUP BY
                        cus.Service_Month)
            SELECT
                  dd.Date_D AS Service_Month
                 ,isnull(sum(cu.Volume), 0) AS Volume
                 ,isnull(sum(cu.Total_Cost), 0) AS Total_Cost
                 ,isnull(sum(cu.Total_Cost) / nullif(sum(cu.Volume), 0), 0) AS Unit_Cost
                 ,isnull(max(ip.Is_Complete), 0) AS Is_Complete
                 ,isnull(max(ip.Is_published), 0) AS Is_published
                 ,isnull(max(ip.Is_Under_Review), 0) AS Is_Under_Review
            FROM
                  meta.Date_Dim dd
                  LEFT JOIN Cte_Cost_Usage_Summary cu
                        ON cu.Service_Month = dd.Date_D
                  LEFT JOIN Cte_Ip_Data ip
                        ON ip.Service_Month = dd.Date_D
            WHERE
                  dd.Date_D BETWEEN @Begin_Dt AND @End_Dt
            GROUP BY
                  dd.Date_D
            ORDER BY
                  dd.Date_D  
  
END ;  
;
;
GO




GRANT EXECUTE ON  [dbo].[Cost_Usage_Summary_Sel_For_Year_By_Client_Commodity] TO [CBMSApplication]
GO
