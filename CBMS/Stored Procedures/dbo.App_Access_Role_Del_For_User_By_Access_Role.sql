SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 /******    
                       
 NAME: dbo.App_Access_Role_Del_For_User_By_Access_Role                   
                        
 DESCRIPTION:    
			To delete colums from  User_Info_Client_App_Access_Role_Map table.                        
                        
 INPUT PARAMETERS:    
                       
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
 @Client_App_Access_Role_Id         INT      
 @User_Info_Id                      INT
                      
 OUTPUT PARAMETERS:    
                             
 Name                               DataType          Default       Description    
---------------------------------------------------------------------------------------------------------------  
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------    
             
	 BEGIN TRAN      
	 SELECT * FROM dbo.User_Info_Client_App_Access_Role_Map where Client_App_Access_Role_Id=4 and  User_Info_Id=39134               
	 EXEC dbo.App_Access_Role_Del_For_User_By_Access_Role @Client_App_Access_Role_Id=4,@User_Info_Id=39134   
	 SELECT * FROM dbo.User_Info_Client_App_Access_Role_Map where Client_App_Access_Role_Id=4 and User_Info_Id=39134    
	 ROLLBACK               
 
                       
 AUTHOR INITIALS:  
     
 Initials               Name    
---------------------------------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                          
                         
 MODIFICATIONS:  
                         
 Initials               Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 NR                     2013-12-30      Created for RA Admin user management                      
                       
******/ 
 CREATE PROCEDURE [dbo].[App_Access_Role_Del_For_User_By_Access_Role]
      ( 
       @Client_App_Access_Role_Id INT
      ,@User_Info_Id INT )
 AS 
 BEGIN                      
      SET NOCOUNT ON;            
               
      DELETE FROM
            dbo.User_Info_Client_App_Access_Role_Map
      WHERE
            Client_App_Access_Role_Id = @Client_App_Access_Role_Id
            AND User_Info_Id = @User_Info_Id
                      
 END   


;
GO
GRANT EXECUTE ON  [dbo].[App_Access_Role_Del_For_User_By_Access_Role] TO [CBMSApplication]
GO
