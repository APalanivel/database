
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: dbo.Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month

DESCRIPTION: 
	To get the Previous received Currency_unitid for the given site, commodity and service month

INPUT PARAMETERS:
NAME					DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Site_Client_Hier_Id	INT
@Commodity_Id			INT
@Service_Month			DATE

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month 1130, 291,'2005-05-01'
	EXEC dbo.Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month1 1130, 291,'2005-05-01'
	exec Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month @Site_Client_Hier_Id=1130,@Commodity_Id=291,@service_month='2012-01-01 00:00:00'
	
	exec Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month @Site_Client_Hier_Id=1132,@Commodity_Id=290,@service_month='2012-01-01 00:00:00'

	SELECT TOP 10 * FROM dbo.Cost_Usage_Site_Dtl
	SELECT * FROM dbo.Bucket_Master where bucket_master_id = 110

AUTHOR INITIALS:
INITIALS	NAME
---------------------------------------------------------------
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
---------------------------------------------------------------
HG			2012-06-08	Created for additional data enhancement
HG			2013-06-13	Modified script to find the later month currency only if prior months are not present
						Multiple joins on Buckets_Master table replaced by table variable
HG			2014-03-13	MAINT-2663, Modified for performance
									- table variable used to get the charge buckets removed
									- Using Currency_Unit_Id NOT NULL to get the currency unit id
									- Currency id saved in to a variable and returned back to application , this was done to remove the GROUP BY on Currency_Unit_Id as we could have multiple charge buckets for same month
									- Scan count reduced to 2 from 30 and logical reads reduced to 26 from 242

*/

CREATE PROCEDURE dbo.Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month
      (
       @Site_Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Service_Month DATE )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Prior_Received_Month DATE
           ,@Later_Received_Month DATE
           ,@Most_Recent_Month DATE
           ,@Currency_Unit_Id INT

	-- Get the prior or later data received for the given site and service month
      SELECT
            @Prior_Received_Month = MAX(CASE WHEN cusd.Service_Month < @service_month THEN ( cusd.Service_Month )
                                        END)
           ,@Later_Received_Month = MIN(CASE WHEN cusd.Service_Month > @service_month THEN ( cusd.service_month )
                                        END)
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
      WHERE
            cusd.Client_Hier_Id = @Site_Client_Hier_Id
            AND cusd.CURRENCY_UNIT_ID IS NOT NULL

      SET @Most_Recent_Month = ISNULL(@Prior_Received_Month, @Later_Received_Month)

      SELECT
            @Currency_Unit_Id = cusd.currency_unit_id
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
      WHERE
            cusd.Client_Hier_Id = @Site_Client_Hier_Id
            AND cusd.service_month = @Most_Recent_Month
            AND cusd.CURRENCY_UNIT_ID IS NOT NULL

      SELECT
            @Currency_Unit_Id AS Currency_Unit_Id

END;
;
GO


GRANT EXECUTE ON  [dbo].[Get_Previous_Month_Currency_Unit_By_Client_Hier_Commodity_Service_Month] TO [CBMSApplication]
GO
