SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TOTAL_FORECAST_VOLUME_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@forecastYear  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE       PROCEDURE  dbo.GET_TOTAL_FORECAST_VOLUME_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@forecastYear int

  AS
set nocount on
select  
	sum(details.volume * conversion.CONVERSION_FACTOR)
from  
	rm_forecast_volume_details details(NOLOCK),
	rm_onboard_hedge_setup setup(NOLOCK),
	CONSUMPTION_UNIT_CONVERSION conversion(NOLOCK)
	   
where
 	details.rm_forecast_volume_id = (select 
					rm_forecast_volume_id 
				 from 
					rm_forecast_volume(NOLOCK) 
				 where  
					forecast_year = @forecastYear
					/*and forecast_as_of_date= (select 
									max(forecast_as_of_date) 
								  from 
									rm_forecast_volume 
								  where 
									forecast_year=@forecastYear
								             and client_id = @clientId
								  ) */
					and client_id = @clientId
				)
	and details.site_id = setup.site_id	
	and conversion.converted_unit_id = (select entity_id 
				       from   entity (NOLOCK)
                                       where  entity_type = 102 
					      and entity_name='MMBtu')

	and conversion.base_unit_id = setup.VOLUME_UNITS_TYPE_ID

group by  conversion.CONVERSION_FACTOR
GO
GRANT EXECUTE ON  [dbo].[GET_TOTAL_FORECAST_VOLUME_P] TO [CBMSApplication]
GO
