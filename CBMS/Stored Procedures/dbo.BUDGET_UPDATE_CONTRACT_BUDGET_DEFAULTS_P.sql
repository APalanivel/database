SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE    PROCEDURE dbo.BUDGET_UPDATE_CONTRACT_BUDGET_DEFAULTS_P
	@budget_contract_defaults_id int,
	@volume decimal(32,16),
	@market_id int,
	@fuel decimal(32,16),
	@multiplier decimal(32,16),
	@adder decimal(32,16),
	@volume_unit_type_id int,
	@tax decimal(32,16),
	@currency_unit_id int,
	@is_Nymex bit
	AS

	update budget_contract_defaults set volume = @volume, 
						 market_id = @market_id, 
						 fuel = @fuel, 
						 adder = @adder, 
						 tax = @tax, 
						 multiplier = @multiplier, 
						 volume_unit_type_id = @volume_unit_type_id, 
						 currency_unit_id = @currency_unit_id,
						 is_nymex_forecast = @is_Nymex

	where budget_contract_defaults_id = @budget_contract_defaults_id










GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_CONTRACT_BUDGET_DEFAULTS_P] TO [CBMSApplication]
GO
