SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE [dbo].[cbmsCuInvoiceChangeLog_Get]
	( @MyAccountId int
	, @cu_invoice_change_log_id int
	)
AS
BEGIN

	   select cl.cu_invoice_change_log_id
		, cl.cu_invoice_id
		, cl.change_type_id
		, cl.field_type_id
		, cl.field_name
		, cl.previous_value
		, cl.current_value
		, cl.change_date
	     from cu_invoice_change_log cl with (nolock)
	    where cl.cu_invoice_change_log_id = @cu_invoice_change_log_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceChangeLog_Get] TO [CBMSApplication]
GO
