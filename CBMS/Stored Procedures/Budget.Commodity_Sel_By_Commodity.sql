SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	 Budget.Commodity_Sel_By_Commodity

DESCRIPTION:
	

INPUT PARAMETERS:
		Name			          DataType		Default	Description
------------------------------------------------------------
     @Meter_Id                     INT
		
OUTPUT PARAMETERS:
	Name			       DataType		Default	Description
------------------------------------------------------------
    @Budget_Master_Id_Out   INT				OUTPUT
  , @Budget_Name_Out        VARCHAR(MAX)	OUTPUT

USAGE EXAMPLES:
------------------------------------------------------------

EXEC Budget.Commodity_Sel_By_Commodity 100552


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
     SP         Srinivas Patchava
MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
 SP			2019-05-22	Created
							
******/
CREATE PROCEDURE [Budget].[Commodity_Sel_By_Commodity]
     (
         @Meter_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            cha.Commodity_Id
            , c.Commodity_Name
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = cha.Commodity_Id
        WHERE
            cha.Meter_Id = @Meter_Id;
    END;


GO
GRANT EXECUTE ON  [Budget].[Commodity_Sel_By_Commodity] TO [CBMSApplication]
GO
