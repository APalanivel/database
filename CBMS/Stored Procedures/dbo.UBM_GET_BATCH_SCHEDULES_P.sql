
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******  
                     
 NAME: dbo.UBM_GET_BATCH_SCHEDULES_P          
                      
 DESCRIPTION:  
     To get the colums from  ubm_batch_schedules ,ubm table  .                      
                      
 INPUT PARAMETERS:  
                     
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
 
                      
 OUTPUT PARAMETERS:  
                           
 Name								DataType			Default			Description  
---------------------------------------------------------------------------------------------------------------
                      
 USAGE EXAMPLES:                          
---------------------------------------------------------------------------------------------------------------  
           
 EXEC dbo.UBM_GET_BATCH_SCHEDULES_P            
                     
 AUTHOR INITIALS:
   
 Initials				Name  
---------------------------------------------------------------------------------------------------------------
 NR                     Narayana Reddy                        
                       
 MODIFICATIONS:
                       
 Initials				Date			 Modification
---------------------------------------------------------------------------------------------------------------
 NR                     2014-03-14       Adding Header.
                                         MAINT-2661  adding Scheduled_Time_Three, Scheduled_Time_Four columns in selcect list.               
                     
******/  
 CREATE PROCEDURE dbo.UBM_GET_BATCH_SCHEDULES_P
 AS 
 SET nocount ON
 SELECT
      ubm_batch_schedules.ubm_id
     ,ubm.ubm_name
     ,ubm_batch_schedules.day_of_week_number
     ,ubm_batch_schedules.day_of_week
     ,ubm_batch_schedules.scheduled_time_one
     ,ubm_batch_schedules.scheduled_time_two
     ,ubm_batch_schedules.Scheduled_Time_Three
     ,ubm_batch_schedules.Scheduled_Time_Four
 FROM
      dbo.ubm_batch_schedules
     ,dbo.ubm
 WHERE
      ubm_batch_schedules.ubm_id = ubm.ubm_id





;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_BATCH_SCHEDULES_P] TO [CBMSApplication]
GO
