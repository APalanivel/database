SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
             
NAME: Inv_Sourced_Image_Sel_By_Image_Hash_Value

DESCRIPTION:

 To get the Inv_Sourced_Image_Id for the given Image_Hash_Value

INPUT PARAMETERS:
NAME							DATATYPE	DEFAULT  DESCRIPTION
----------------------------------------------------------------
@Image_Hash_Value				NVARCHAR(128)

OUTPUT PARAMETERS:
NAME     DATATYPE	DEFAULT		DESCRIPTION
---------------------------------------------------------------
USAGE EXAMPLES:
---------------------------------------------------------------


EXEC dbo.Inv_Sourced_Image_Sel_By_Image_Hash_Value
      @Image_Hash_Value = '01023N341UKL09OM12E7NGJ'


AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
HG			2014-06-20	Created for Invoice scraping image hash enh

*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Image_Sel_By_Image_Hash_Value]
      (
       @Image_Hash_Value NVARCHAR(128) )
AS
BEGIN

      SET NOCOUNT ON

      SELECT TOP 1
            Inv_Sourced_Image_Id
           ,Original_FileName
           ,Cbms_FileName
      FROM
            dbo.Inv_Sourced_Image
      WHERE
            Image_Hash_Value = @Image_Hash_Value

END
;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Image_Sel_By_Image_Hash_Value] TO [CBMSApplication]
GO
