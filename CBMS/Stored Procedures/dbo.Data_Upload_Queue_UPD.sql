
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/******************************************************************************************************        
NAME : dbo.Data_Upload_Queue_UPD    
       
DESCRIPTION:     
Stored Procedure is used to insert data into Data Upload Audit Log.  
   
 INPUT PARAMETERS:        
 Name             DataType               Default        Description        
--------------------------------------------------------------------     
@RequestId   INT  
@RequestType  VARCHAR(100)  
@RequestedBy  VARCHAR(100)  
@DataPath   VARCHAR(500)  
@Status    CHAR(1)   
  
 OUTPUT PARAMETERS:        
 Name   DataType    Default   Description        
--------------------------------------------------------------------        
@RequestId  INT  
  
    
  USAGE EXAMPLES:        
--------------------------------------------------------------------        

BEGIN TRAN  

EXEC Data_Upload_Queue_UPD 1815,100311

ROLLBACK TRAN
      
AUTHOR INITIALS:        
 Initials		Name        
-------------------------------------------------------------------        
	RT			Romy Thomas  
	RMG			Rani Mary George
       
 MODIFICATIONS         
 Initials		Date			Modification        
--------------------------------------------------------------------  
	RT			07/13/2011		Created  
	RMG			03/13/2013		Copied from DVDEHub
******************************************************************************************************/    
  
CREATE PROCEDURE [dbo].[Data_Upload_Queue_UPD]
      ( 
       @DataUploadQueueId INT
      ,@Status INT )
AS 
BEGIN
      SET NOCOUNT ON ;
      
      UPDATE
            dbo.DATA_UPLOAD_QUEUE
      SET   
            LAST_UPDATE_TS = getdate()
           ,STATUS_CD = @Status
      WHERE
            DATA_UPLOAD_QUEUE_ID = @DataUploadQueueId
END





;
GO

GRANT EXECUTE ON  [dbo].[Data_Upload_Queue_UPD] TO [CBMSApplication]
GO
