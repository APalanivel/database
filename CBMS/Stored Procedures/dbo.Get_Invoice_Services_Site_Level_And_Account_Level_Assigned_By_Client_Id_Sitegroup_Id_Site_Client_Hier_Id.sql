SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id

DESCRIPTION:
	Used to get all the site level services mapped to the client along with account level services assigned to the meters of the client/Sitegroup/Client

INPUT PARAMETERS:
	Name					DataType	Default		Description
------------------------------------------------------------------------------------
    @Client_Id				INT
    @Sitegroup_Id			INT			NULL
    @Site_Client_Hier_Id	INT			NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id 11239
	EXEC Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id 10069,567
	EXEC Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id 11231
	EXEC Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id 11231, NULL, 16934
	
	SELECT TOP 10 * FROM core.Client_HIer WHERE Client_id = 10069
	

	SELECT * FROM Core.Client_Commodity WHERE Client_id = 10069

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Harihara Suthan G

MODIFICATIONS
    Initials	Date		   Modification
------------------------------------------------------------
    HG        	2012-03-27	   Created for additional data enhancement
******/

CREATE PROCEDURE dbo.Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id
      ( 
       @Client_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Client_Hier_Id INT = NULL )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            cc.Commodity_Id
           ,com.Commodity_Name
           ,cc.Hier_Level_Cd
           ,heir_level.Code_Value Hier_level_Cd_Value
           ,com.Default_UOM_Entity_Type_Id
           ,bkt_agg_rule.Code_Value Bucket_Aggregation_Rule
           ,com.UOM_Entity_Type
      FROM
            Core.Client_Commodity cc
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_id = cc.Commodity_id
            INNER JOIN dbo.CODE heir_level
                  ON cc.hier_level_cd = heir_level.code_id
            INNER JOIN dbo.CODE service_type
                  ON cc.commodity_service_cd = service_type.code_id
            INNER JOIN dbo.Code lvl
                  ON lvl.Code_Id = cc.Hier_Level_Cd
            INNER JOIN dbo.Code bkt_agg_rule
                  ON bkt_agg_rule.Code_Id = com.Bucket_Aggregation_Cd
      WHERE
            cc.Client_Id = @Client_id
            AND service_type.Code_Value = 'Invoice'
            AND ( ( lvl.Code_Value = 'Site' )
                  OR ( lvl.Code_Value = 'Account'
                       AND EXISTS ( SELECT
                                          1
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN Core.Client_Hier_Account cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                    WHERE
                                          ch.Client_ID = cc.Client_id
                                          AND cha.Commodity_id = cc.Commodity_Id
                                          AND ( @Sitegroup_Id IS NULL
                                                OR ch.Sitegroup_Id = @Sitegroup_Id )
                                          AND ( @Site_Client_Hier_Id IS NULL
                                                OR ch.Client_Hier_Id = @Site_Client_Hier_Id ) ) ) )
      ORDER BY
            com.Commodity_Name

END
;
GO
GRANT EXECUTE ON  [dbo].[Get_Invoice_Services_Site_Level_And_Account_Level_Assigned_By_Client_Id_Sitegroup_Id_Site_Client_Hier_Id] TO [CBMSApplication]
GO
