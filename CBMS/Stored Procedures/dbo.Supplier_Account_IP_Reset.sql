SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  [dbo].[Supplier_Account_IP_Reset]  

 DESCRIPTION:   
			This procedure called to reset/recalculate INVOICE_PARTICIPATION/INVOICE_PARTICIPATION_Site
		
 
 INPUT PARAMETERS:
	Name					DataType	Default		Description
--------------------------------------------------------------
	@User_Info_Id			INT
    @Account_Id				INT
    @Current_Start_Date		DATETIME
    @Current_End_Date		DATETIME
 
 OUTPUT PARAMETERS:  
	Name   DataType  Default	Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------  

	BEGIN TRAN
	EXEC dbo.Supplier_Account_IP_Reset 49,'86437-0001',NULL,'2017-06-23'
	SELECT ACCOUNT_ID,SITE_ID,SERVICE_MONTH FROM dbo.INVOICE_PARTICIPATION WHERE ACCOUNT_ID = 447088
	--SELECT * FROM dbo.INVOICE_PARTICIPATION_SITE WHERE SITE_ID IN (37097,37300)
	ROLLBACK TRAN

 AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
 
 MODIFICATIONS:  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2017-03-06	Contract placeholder CP-8 Created
							Similar to dbo.Cbms_ContractDateChange_IpReset, but DMO supplier account IP reset process moved to queue. 
	
******/

CREATE PROCEDURE [dbo].[Supplier_Account_IP_Reset]
      ( 
       @User_Info_Id INT
      ,@Account_Id INT
      ,@Current_Start_Date DATETIME
      ,@Current_End_Date DATETIME )
AS 
BEGIN

      SET NOCOUNT ON;
      	
      DECLARE @Old_Start_Date DATETIME
      DECLARE @Old_End_Date DATETIME
      DECLARE @Date1 DATETIME
      DECLARE @Date2 DATETIME
      DECLARE @Will_Delete_Ip BIT

      DECLARE @Counter INT
      DECLARE @Site_id INT
      DECLARE @Insert_Month DATETIME
      DECLARE @Service_Month DATETIME
      

      DECLARE @Site_Date_Change_Period TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,site_id INT
            ,service_month DATETIME )
        
      DECLARE @tbl_Site_SupplierAccount TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,Site_Id INT
            ,Account_id INT )
		
      SELECT
            @Old_Start_Date = MIN(SERVICE_MONTH)
           ,@Old_End_Date = MAX(SERVICE_MONTH)
      FROM
            dbo.INVOICE_PARTICIPATION
      WHERE
            ACCOUNT_ID = @Account_Id 
      
      IF @Current_Start_Date IS NOT NULL 
            BEGIN  
                  IF @Current_Start_Date > @Old_Start_Date 
                        BEGIN

                              SET @Will_Delete_Ip = 1 --will need to DELETE invoice_participation.
                              -- Dates set to first day of month service months may miss if the date is not first day of month
                              SET @Date1 = CASE WHEN DATEPART(D, @Old_Start_Date) = 1 THEN @Old_Start_Date
                                                ELSE CAST(DATEPART(MONTH, @Old_Start_Date) AS VARCHAR(2)) + '/1/' + CAST(DATEPART(YEAR, @Old_Start_Date) AS VARCHAR(4))
                                           END
		                        
                              SET @Date2 = CASE WHEN DATEPART(D, @Current_Start_Date) = 1 THEN @Current_Start_Date
                                                ELSE CAST(DATEPART(MONTH, @Current_Start_Date) AS VARCHAR(2)) + '/1/' + CAST(DATEPART(YEAR, @Current_Start_Date) AS VARCHAR(4))
                                           END
                              SET @Date2 = @Date2 - 1 --subtract 1 day so that this month also doesn't get deleted.

                        END
                  ELSE 
                        BEGIN
                              SET @Date1 = @Current_Start_Date -- Setting it to first day of the month
                              SET @Date2 = @Old_Start_Date - 1
                              SET @Will_Delete_Ip = 0 --will need to insert invoice_participation
                        END
            END

      IF @Current_End_Date IS NOT NULL 
            BEGIN
                  IF @Current_End_Date < @Old_End_Date 
                        BEGIN
                              SET @Date1 = @Current_End_Date + 1 --add 1 day so that this month doesn't also get DELETEd
                              SET @Date2 = @Old_End_Date
                              SET @Will_Delete_Ip = 1 --will need to DELETE invoice_participation
                        END
                  ELSE 
                        BEGIN
                              SET @Date1 = @Old_End_Date -- Setting it to first day of the month
                              SET @Date2 = @Current_End_Date + 1 --add 1 day so that this month also added to ip_site table
                              SET @Will_Delete_Ip = 0 --will need to insert invoice_participation
                        END
            END

      BEGIN TRY

            BEGIN TRAN

                              
            INSERT      INTO @tbl_Site_SupplierAccount
                        ( 
                         Site_Id
                        ,Account_Id )
                        SELECT
                              ch.site_id
                             ,cha.Account_Id
                        FROM
                              core.Client_Hier ch
                              JOIN core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        WHERE
                              cha.Account_Id = @Account_Id
                        GROUP BY
                              ch.site_id
                             ,cha.Account_Id
                              


			-- DELETE invoice_participation
            IF @Will_Delete_Ip = 1 
                  BEGIN
                        INSERT      INTO @Site_Date_Change_Period
                                    ( 
                                     Site_id
                                    ,Service_Month )
                                    SELECT
                                          SITE_ID
                                         ,SERVICE_MONTH
                                    FROM
                                          dbo.INVOICE_PARTICIPATION ip
                                    WHERE
                                          ip.ACCOUNT_ID = @Account_Id
                                          AND ip.SERVICE_MONTH >= @Date1
                                          AND ip.SERVICE_MONTH <= @Date2
                                    GROUP BY
                                          SITE_ID
                                         ,SERVICE_MONTH

                        DELETE
                              ip
                        FROM
                              dbo.INVOICE_PARTICIPATION ip
                        WHERE
                              ip.ACCOUNT_ID = @Account_Id
                              AND service_month >= @Date1
                              AND service_month <= @Date2
						
						-- Deletes from INVOICE_PARTICIPATION_Site only if there is no record(s) for the same deleted service month(s) of site(s) present in INVOICE_PARTICIPATION
                        DELETE
                              ips
                        FROM
                              dbo.INVOICE_PARTICIPATION_Site ips
                              JOIN @Site_Date_Change_Period cd
                                    --(Deleted service month(s), site(s))
                                    ON ips.SITE_ID = cd.site_id
                                       AND cd.service_month = ips.SERVICE_MONTH
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Participation ip
                                                JOIN @Site_Date_Change_Period cdc
                                                      ON cdc.site_id = ip.SITE_ID
                                                         AND cdc.service_month = ip.SERVICE_MONTH
                                                         AND ips.Site_Id = ip.Site_Id
                                                         AND ips.SERVICE_MONTH = ip.Service_Month ) 
						
						
                        PRINT '' + CAST(@@rowcount AS VARCHAR(10)) + ' invoice_participation(s) Deleted'

                  END
            ELSE --@Will_Delete_Ip = 0, insert invoice_participation
                  BEGIN
                        SET @Insert_Month = @Date1
					--IF the @Insert_Month is not the first day of a month, change @Insert_Month to the first day of the month
                        SET @Insert_Month = CASE WHEN DATEPART(D, @Insert_Month) = 1 THEN @Date1
                                                 ELSE CAST(DATEPART(MONTH, @Insert_Month) AS VARCHAR(2)) + '/1/' + CAST(DATEPART(YEAR, @Insert_Month) AS VARCHAR(4))
                                            END				

					--for every month inserted, load the site_id AND service_month into the temp table
                        INSERT      INTO @Site_Date_Change_Period
                                    ( 
                                     site_id
                                    ,service_month )
                                    SELECT
                                          tss.Site_Id
                                         ,dd.Date_D
                                    FROM
                                          @tbl_Site_SupplierAccount tss
                                          CROSS JOIN meta.Date_Dim dd
                                    WHERE
                                          dd.Date_D > = @Insert_Month
                                          AND dd.Date_D < @Date2
                                    GROUP BY
                                          tss.Site_Id
                                         ,dd.Date_D

                        SET @Counter = 1

                        WHILE ( @Counter <= ( SELECT
                                                COUNT(1)
                                              FROM
                                                @tbl_Site_SupplierAccount ) ) 
                              BEGIN
	  
                                    SELECT
                                          @Site_id = site_id
                                         ,@Account_id = Account_Id
                                    FROM
                                          @tbl_Site_SupplierAccount
                                    WHERE
                                          id = @Counter

							--this sproc will add new invoice_participation to accomodate the expanded contract period
                                    
                                    EXEC Invoice_Participation_DMO_Supplier_Account_Ins 
                                          @User_Info_Id
                                         ,@Account_id
                                         ,@Site_id
                                         
                                    PRINT 'invoice_participation(s) in queue to be added'

                                    SET @Counter = @Counter + 1

                              END
                  END

		-- RECALCULATE TOTALS IN  INVOICE_PARTICIPATION_SITE

            SET @Counter = 1

            WHILE ( @Counter <= ( SELECT
                                    COUNT(1)
                                  FROM
                                    @Site_Date_Change_Period ) ) 
                  BEGIN

                        SELECT
                              @Site_id = site_id
                             ,@Service_Month = service_month
                        FROM
                              @Site_Date_Change_Period
                        WHERE
                              id = @Counter

                        EXEC cbmsInvoiceParticipationSite_Save 
                              @User_Info_Id
                             ,@Site_id
                             ,@Service_Month
                        PRINT 'invoice_participation_site saved'
   
                        SET @Counter = @Counter + 1

                  END
                  
                  /*-----------------------------------------------------------------*/
            
            COMMIT TRAN

      END TRY
      BEGIN CATCH

            ROLLBACK TRAN
            EXEC usp_Rethrowerror 
                  'An error occurred AND the transaction was rolled back'
            PRINT ( '-----------------------------------------------------' )
            PRINT ( 'An error occurred AND the transaction was rolled back' )

      END CATCH

END

;
GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_IP_Reset] TO [CBMSApplication]
GO
