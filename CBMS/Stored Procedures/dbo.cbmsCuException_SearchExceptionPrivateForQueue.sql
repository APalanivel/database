
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       

NAME:
 dbo.cbmsCuException_SearchExceptionPrivateForQueue    
       
DESCRIPTION:          
      
INPUT PARAMETERS:          
 Name                   DataType          Default     Description          
------------------------------------------------------------            
 @queue_id			    INT					NULL   
 @client_name		    VARCHAR(200)        NULL
 @city				    VARCHAR(200)        NULL
 @vendor_name		    VARCHAR(200)        NULL
 @account_number	    VARCHAR(200)        NULL
 @Site_Name			    VARCHAR(200)        NULL
 @Cu_Exception_Type_Id  INT                 NULL
 @Country_Id			INT				    NULL
 @Commodity_Id			INT                 NULL
 @Supplier_Name			VARCHAR(500)	    NULL
 @State_Id				INT					NULL
               
OUTPUT PARAMETERS:          
Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:      
------------------------------------------------------------        
 
 EXEC dbo.cbmsCuException_SearchExceptionPrivateForQueue 19997  
 
 EXEC dbo.cbmsCuException_SearchExceptionPrivateForQueue 13136
 
 EXEC dbo.cbmsCuException_SearchExceptionPrivateForQueue
	  @queue_id=49
	 ,@client_name=null
	 ,@city=null
	 ,@vendor_name=null
	 ,@account_number=null
     ,@Site_Name = NULL
     ,@Cu_Exception_Type_Id  = NULL

 EXEC dbo.cbmsCuException_SearchExceptionPrivateForQueue
	  @queue_id=152
	 ,@client_name=null
	 ,@city=null
	 ,@vendor_name= null
	 ,@account_number=null
	 ,@Site_Name = NULL
	 ,@Cu_Exception_Type_Id  = NULL
	 ,@Supplier_Name='Constellation New Energy'
	 
 EXEC dbo.cbmsCuException_SearchExceptionPrivateForQueue @queue_id=14248 


SELECT top 10 ced.* FROM Cu_Exception_Denorm ced inner join core.client_hier_account cha on ced.client_hier_id=cha.client_hier_id
 inner join core.client_hier_account scha on cha.client_hier_id=scha.client_hier_id and cha.meter_id=scha.meter_id
 WHERE Exists (SELECT 1 FROM CU_INVOICE_LABEL l WHERE l.Vendor_Name IS NOT NULL )
and ced.client_hier_id is not null
AND ced.Account_Id IS NULL
      
AUTHOR INITIALS:
INITIALS NAME  
------------------------------------------------------------  
SKA		 Shobhit Kumar Agrawal
PNR		 Pandarinath
RR		 Raghu Reddy
SP		 Sandeep Pigilam.
  
MODIFICATION:  
INITIALS DATE		MODIFICATION  
------------------------------------------------------------  
SKA		 10/21/2010 Created  
SKA		 01/13/2011 Added the search condition for Account Number Bug#20733.
PNR		 03/09/2011	Added LEFT OUTER JOIN with cu_invoice_label to fetch vendor_name when the invoice is not resolved to an account and if the value saved there(MAINT-495)
RR		 2013-08-13 MAINT-1895 VENDOR_NAME is returning blank even if the account is resolved and the exception is not Failed Recalc,
					the script modified to return Account_Vendor_Name from core.client_hier_account for all the resolved accounts
SP		2014-05-22	Data Operations Enhancement 4.2.2 ,Added condition exception_type <> 'Failed Recalc'.
					Data Operations Enhancement 4.2.3 added @Site_Name,@Cu_Exception_Type_Id  as input parameters.
SP		2017-05-17  small Enhancement,SE2017-124 added filters	@State_Id and removed @State_Name,@Country_Id INT = NULL,@Commodity_Id INT = NULL,@Supplier_Name VARCHAR(500) = NULL																

*/
CREATE PROCEDURE [dbo].[cbmsCuException_SearchExceptionPrivateForQueue]
      ( 
       @queue_id INT = NULL
      ,@client_name VARCHAR(200) = NULL
      ,@city VARCHAR(200) = NULL
      ,@vendor_name VARCHAR(200) = NULL
      ,@account_number VARCHAR(200) = NULL
      ,@Site_Name VARCHAR(200) = NULL
      ,@Cu_Exception_Type_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Supplier_Name VARCHAR(500) = NULL
      ,@State_Id INT = NULL )
AS 
BEGIN
   
      SET NOCOUNT ON;

      SET @client_name = REPLACE(@client_name, '''', '''''')
      SET @city = REPLACE(@city, '''', '''''')
      SET @vendor_name = REPLACE(@vendor_name, '''', '''''')
      SET @account_number = REPLACE(@account_number, '''', '''''')
      SET @account_number = REPLACE(@account_number, SPACE(1), SPACE(0))
      SET @account_number = REPLACE(@account_number, '-', SPACE(0))
      SET @account_number = REPLACE(@account_number, '/', SPACE(0));
      SET @Site_Name = REPLACE(@Site_Name, '''', '''''');

      SELECT
            ced.cbms_image_id
           ,ced.cbms_doc_id
           ,ced.cu_invoice_id
           ,ced.queue_id
           ,ced.exception_type
           ,ced.exception_status_type
           ,( CASE WHEN ced.exception_type = 'Wrong Account' THEN NULL
                   WHEN ag.ACCOUNT_GROUP_ID IS NULL THEN ISNULL(cha.ACCOUNT_NUMBER, ced.UBM_Account_Number)
                   ELSE COALESCE(ag.GROUP_BILLING_NUMBER, cha.ACCOUNT_NUMBER, ced.UBM_Account_Number)
              END ) Account_number
           ,( CASE WHEN ced.exception_type = 'Wrong Month' THEN NULL
                   WHEN ag.ACCOUNT_GROUP_ID IS NULL THEN CONVERT(VARCHAR(10), cism.SERVICE_MONTH, 101)
                   ELSE ced.service_month
              END ) service_month
           ,CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                 ELSE ced.UBM_Client_Name
            END AS Client_Name
           ,CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                 ELSE ced.UBM_Site_Name
            END site_name
           ,( CASE WHEN ag.ACCOUNT_GROUP_ID IS NOT NULL
                        AND ced.Account_Id IS NOT NULL
                        AND vage.ENTITY_NAME = 'Utility' THEN vag.VENDOR_NAME	-- Invoice resolved to an account and it is group account
                   WHEN cha.Account_Id IS NOT NULL THEN CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                                             WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                                                        END							-- Inovoice resolved to an account and it is not a group account
                   ELSE Inv_Lbl.VENDOR_NAME																	-- Invoice not resolved to an account
              END ) AS Utility_Vendor_Name
           ,ced.date_in_queue
           ,ced.sort_order
           ,rt.Code_Value AS contract_recalc_type
           ,com.commodity_Name
           ,ced.ACCOUNT_ID
           ,com.Commodity_Id
           ,cha.Account_Type
           ,ISNULL(( CASE WHEN ag.ACCOUNT_GROUP_ID IS NOT NULL
                               AND ced.Account_Id IS NOT NULL
                               AND vage.ENTITY_NAME = 'Supplier' THEN vag.VENDOR_NAME
                          WHEN cha.Account_Id IS NOT NULL
                               AND cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                     END ), scha.Account_Vendor_Name) AS Supplier_Vendor_Name
      FROM
            dbo.cu_exception_denorm ced
            INNER JOIN dbo.cu_invoice ci
                  ON ci.CU_INVOICE_ID = ced.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                     AND cism.Account_ID = ced.Account_ID
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP AG
                  ON AG.ACCOUNT_GROUP_ID = ci.ACCOUNT_GROUP_ID
            LEFT OUTER JOIN dbo.VENDOR vag
                  ON vag.VENDOR_ID = AG.VENDOR_ID
            LEFT JOIN dbo.ENTITY vage
                  ON vag.VENDOR_TYPE_ID = vage.ENTITY_ID
            LEFT OUTER JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = ced.Client_Hier_ID
            LEFT OUTER JOIN Core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ced.Client_Hier_ID
                     AND cha.Account_Id = ced.Account_ID
            LEFT OUTER JOIN dbo.Commodity com
                  ON com.Commodity_Id = cha.Commodity_Id
            LEFT OUTER JOIN dbo.Code rt
                  ON rt.Code_id = cha.Supplier_Account_Recalc_Type_Cd
            LEFT OUTER JOIN dbo.CU_INVOICE_LABEL Inv_Lbl
                  ON ci.CU_INVOICE_ID = Inv_Lbl.CU_INVOICE_ID
            LEFT  JOIN dbo.CU_EXCEPTION_TYPE cet
                  ON ced.EXCEPTION_TYPE = cet.EXCEPTION_TYPE
            LEFT JOIN core.Client_Hier_Account scha
                  ON cha.Client_Hier_Id = scha.Client_Hier_Id
                     AND cha.Meter_Id = scha.Meter_Id
                     AND scha.Account_Type = 'Supplier'
                     AND cism.SERVICE_MONTH BETWEEN scha.Supplier_Account_begin_Dt
                                            AND     scha.Supplier_Account_End_Dt
            OUTER APPLY ( SELECT
                              uac.Account_Vendor_Name + ', '
                          FROM
                              Core.Client_Hier_Account uac
                              INNER JOIN Core.Client_Hier_Account sac
                                    ON sac.Meter_Id = uac.Meter_Id
                          WHERE
                              uac.Account_Type = 'Utility'
                              AND sac.Account_Type = 'Supplier'
                              AND cha.Account_Type = 'Supplier'
                              AND sac.Account_Id = cha.Account_Id
                          GROUP BY
                              uac.Account_Vendor_Name
            FOR
                          XML PATH('') ) UV ( Vendor_Name )
      WHERE
            ced.is_manual = 0
            AND ( @queue_id IS NULL
                  OR ced.queue_id = CONVERT(VARCHAR, @queue_id) )
            AND ( ( @client_name IS NULL )
                  OR ( ISNULL(ch.Client_Name, ced.UBM_Client_Name) LIKE '%' + @client_name + '%' ) )
            AND ( ( @city IS NULL )
                  OR ( ISNULL(ch.City, ced.UBM_City) LIKE '%' + @city + '%' ) )
            AND ( ( @State_Id IS NULL )
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.STATE s
                              WHERE
                                    s.STATE_ID = @State_Id
                                    AND ( ISNULL(ch.State_Name, UBM_State_Name) = s.STATE_NAME ) ) )
            AND ( @vendor_name IS NULL
                  OR ( COALESCE(CASE WHEN vage.ENTITY_NAME = 'Utility' THEN vag.VENDOR_NAME
                                END, CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                          WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                                     END, Inv_Lbl.Vendor_Name) LIKE '%' + @vendor_name + '%' ) )
            AND ( ( @account_number IS NULL )
                  OR ( ( LEN(@account_number) >= 4 )
                       AND ( REPLACE(REPLACE(REPLACE(( CASE WHEN ag.ACCOUNT_GROUP_ID IS NULL THEN ISNULL(ag.GROUP_BILLING_NUMBER, ISNULL(cha.ACCOUNT_NUMBER, ced.UBM_Account_Number))
                                                            ELSE ag.GROUP_BILLING_NUMBER
                                                       END ), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) LIKE '%' + @account_number + '%' ) )
                  OR ( ( LEN(@account_number) <= 4 )
                       AND REPLACE(REPLACE(REPLACE(( CASE WHEN ag.ACCOUNT_GROUP_ID IS NULL THEN ISNULL(ag.GROUP_BILLING_NUMBER, ISNULL(cha.ACCOUNT_NUMBER, ced.UBM_Account_Number))
                                                          ELSE ag.GROUP_BILLING_NUMBER
                                                     END ), '/', SPACE(0)), SPACE(1), SPACE(0)), '-', SPACE(0)) = @account_number ) )
            AND ( ( @Site_Name IS NULL )
                  OR ( ISNULL(ch.Site_Name, ced.UBM_Site_Name) LIKE '%' + @Site_Name + '%' ) )
            AND ( @Cu_Exception_Type_Id IS NULL
                  OR cet.CU_EXCEPTION_TYPE_ID = @Cu_Exception_Type_Id )
            AND ( @Country_Id IS NULL
                  OR ch.Country_Id = @Country_Id )
            AND ( @Commodity_Id IS NULL
                  OR cha.Commodity_Id = @Commodity_Id )
            AND ( @Supplier_Name IS NULL
                  OR ( COALESCE(CASE WHEN vage.ENTITY_NAME = 'Supplier' THEN vag.VENDOR_NAME
                                END, CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                                     END, scha.Account_Vendor_Name) LIKE '%' + @Supplier_Name + '%' ) )
            AND ced.exception_type <> 'Failed Recalc'
      ORDER BY
            sort_order
           ,date_in_queue ASC    
    
END;





;
;
GO




GRANT EXECUTE ON  [dbo].[cbmsCuException_SearchExceptionPrivateForQueue] TO [CBMSApplication]
GO
