SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TOTAL_EXPECTED_FOR_SITE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@siteId        	int       	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelId  	int       	          	
	@conversionId  	int       	          	
	@fromDate      	datetime  	          	
	@toDate        	datetime  	          	
	@fromyear      	int       	          	
	@toYear        	int       	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--EXEC GET_TOTAL_EXPECTED_FOR_SITE_P 1,1,2244,638,593,264,'10-1-2004','6-1-2005',2004,2005,331

--SELECT * FROM SITE WHERE DIVISION_ID=87

--SELECT * FROM ENTITY WHERE ENTITY_ID=533
--SELECT * FROM RM_ONBOARD_CLIENT WHERE CLIENT_ID=228
--SELECT * FROM RM_ONBOARD_HEDGE_SETUP WHERE RM_ONBOARD_CLIENT_ID=63

CREATE  PROCEDURE dbo.GET_TOTAL_EXPECTED_FOR_SITE_P

@userId varchar(10),
@sessionId varchar(20),
@siteId integer,
@hedgeTypeId integer,--571
@hedgeLevelId integer,--531
@conversionId integer,--25
@fromDate datetime,
@toDate datetime,
@fromyear integer,
@toYear integer,
@clientId integer
 
AS
set nocount on
SELECT A.VOLUME,A.DEALMONTH,A.DEALYEAR,onBoardClient.max_hedge_percent,A.site_id

FROM

(


	SELECT 
	RM_FORECAST_VOLUME_DETAILS.VOLUME*CONVERSION_FACTOR VOLUME,
	DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) DEALMONTH,
	DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER) DEALYEAR,
	RM_FORECAST_VOLUME_DETAILS.site_id site_id
	
	FROM
	RM_ONBOARD_CLIENT,
	RM_ONBOARD_HEDGE_SETUP RIGHT JOIN RM_FORECAST_VOLUME_DETAILS ON
	(RM_ONBOARD_HEDGE_SETUP.SITE_ID=RM_FORECAST_VOLUME_DETAILS.SITE_ID),
	RM_FORECAST_VOLUME,
	CONSUMPTION_UNIT_CONVERSION

	WHERE
	RM_ONBOARD_CLIENT.CLIENT_ID=@clientId AND
	RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId AND
	RM_ONBOARD_HEDGE_SETUP.VOLUME_UNITS_TYPE_ID=CONSUMPTION_UNIT_CONVERSION.BASE_UNIT_ID AND
	CONSUMPTION_UNIT_CONVERSION.CONVERTED_UNIT_ID=@conversionId AND
	RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
	RM_FORECAST_VOLUME_DETAILS.RM_FORECAST_VOLUME_ID=RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID AND
	RM_FORECAST_VOLUME_DETAILS.SITE_ID=@siteId and (
	RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID in(
	(select entity_id from entity where entity_type=262 and entity_name='Corporate'),(select entity_id from entity where entity_type=262 and entity_name='Division'),(select entity_id from entity where entity_type=262 and entity_name='Group'),@hedgeLevelId))
	AND
	RM_FORECAST_VOLUME.RM_FORECAST_VOLUME_ID IN
	(
		SELECT 
			RM_FORECAST_VOLUME_ID from RM_FORECAST_VOLUME
		WHERE
			FORECAST_AS_OF_DATE IN
			(
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@fromYear AND CLIENT_ID=@clientId
				UNION 	
				select MAX(FORECAST_AS_OF_DATE) from  RM_FORECAST_VOLUME where FORECAST_YEAR =@toYear AND CLIENT_ID=@clientId
			)

		--FORECAST_YEAR=(SELECT DATEPART(YEAR, @monthIdentifier))AND 
		--FORECAST_AS_OF_DATE=(SELECT MAX(FORECAST_AS_OF_DATE)FROM RM_FORECAST_VOLUME where CLIENT_ID=@clientId)
	) 
	AND
		RM_FORECAST_VOLUME.CLIENT_ID=@clientId AND
		--RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER=@monthIdentifier
		RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER BETWEEN @fromDate AND @toDate

	--GROUP BY 
	--DATEPART(MONTH,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER),
	--DATEPART(YEAR,RM_FORECAST_VOLUME_DETAILS.MONTH_IDENTIFIER)
) AS A,RM_ONBOARD_CLIENT onBoardClient where onBoardClient.client_id=@clientId order by DEALYEAR,DEALMONTH asc
GO
GRANT EXECUTE ON  [dbo].[GET_TOTAL_EXPECTED_FOR_SITE_P] TO [CBMSApplication]
GO
