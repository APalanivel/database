
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
dbo.Report_Get_ClientAttribute_Details_By_Client_id

DESCRIPTION:

This procedure is to fetch all Metric Values of Client

INPUT PARAMETERS:
Name				DataType		Default	Description
----------------------------------------------------------
@Client_ID			INT

OUTPUT PARAMETERS:
Name			DataType		Default	Description
----------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------
EXEC Report_Get_ClientAttribute_Details_By_Client_id 235

AUTHOR INITIALS:
Initials	Name
----------------------------------------------------------
SSR			Sharad srivastava
AKR          Ashok Kumar Raju

MODIFICATIONS:
Initials	Date		Modification
----------------------------------------------------------
SSR        	01/10/2011	Created
LEC         03/16/2012  Added core.client_hier to produce correct join.
AKR         2014-06-11  Modified Attribute tables to refer DV2 on the same instance.(Changes for CBMS RO)

******/  
CREATE PROC [dbo].[Report_Get_ClientAttribute_Details_By_Client_id] @Client_id INT
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            0 Client_Attribute_id
           ,' None' Attribute_Name
      UNION ALL
      SELECT
            ca.Client_Attribute_id
           ,ca.Attribute_Name
      FROM
            dv2.Core.Client_Attribute_Tracking cat
            JOIN dv2.Core.Client_Attribute ca
                  ON ca.Client_Attribute_id = cat.Client_Attribute_id
            JOIN core.client_hier ch
                  ON ch.client_id = ca.client_id
                     AND ch.client_hier_id = cat.client_hier_id
      WHERE
            ca.CLIENT_ID = @Client_id
      GROUP BY
            ca.Client_Attribute_id
           ,ca.Attribute_Name
       
          
END


;
GO

GRANT EXECUTE ON  [dbo].[Report_Get_ClientAttribute_Details_By_Client_id] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Get_ClientAttribute_Details_By_Client_id] TO [CBMSApplication]
GO
