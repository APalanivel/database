SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_RFP_ACCOUNT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId int,
@accountId int,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@sr_rfp_account_id int out


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Insert on sr_rfp_
--exec dbo.SR_RFP_CREATE_RFP_ACCOUNT_P
--@rfpId = 1183,
--@accountId = 97,
--@sr_rfp_account_id = null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_CREATE_RFP_ACCOUNT_P
@rfpId int,
@accountId int,
@sr_rfp_account_id int out


AS


SET NOCOUNT ON

DECLARE @entityId int
SELECT @entityId = (select entity_id from entity where entity_type=1029 and entity_name='new')

DECLARE @accExistency int
SELECT @accExistency =  count(1) from sr_rfp_account where account_id = @accountId and sr_rfp_id = @rfpId and is_deleted  = 0


if(@accExistency < 1)
begin


	insert into SR_RFP_ACCOUNT(
		ACCOUNT_ID, 
		SR_RFP_ID,
		BID_STATUS_TYPE_ID
		)
	
	values (
		@accountId ,
		@rfpId,
		@entityId
		)
	
   
   Select @sr_rfp_account_id = scope_identity()
	
	declare @projectid int,
		     @siteid int

	set @siteid =(select distinct account.site_id from sr_rfp_account,account
		      where  sr_rfp_account.account_id = @accountId and account.account_id=@accountId
		      and sr_rfp_account.sr_rfp_id = @rfpId)

	if (select count(*) from RFP_SITE_PROJECT where RFP_ID = @rfpid and SITE_ID = @siteId )> 0
	begin
		set @projectId = (select SSO_PROJECT_ID from RFP_SITE_PROJECT where RFP_ID = @rfpid and SITE_ID = @siteid )
	
		update sso_project_step 
		set is_active=1 , is_complete = 0 
		where sso_project_id=@projectid and step_no=3

		update sso_project_step 
		set is_active=1 , is_complete = 0 
		where sso_project_id=@projectid and step_no=4

		update sso_project_step 
		set is_active=1 , is_complete = 0 
		where sso_project_id=@projectid and step_no=5
		--added for 18499
		update sso_project
		set project_status_type_id=(select entity_id from entity where entity_type = 601 and entity_name = 'Open')
		where sso_project_id=@projectid 
		

	end
		--ended by saritha
	
	return
	
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_RFP_ACCOUNT_P] TO [CBMSApplication]
GO
