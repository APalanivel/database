SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CBMS_CLIENT_CSA_MAP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@userInfoId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec CBMS_CLIENT_CSA_MAP_P 10154, 0

CREATE    PROC dbo.  CBMS_CLIENT_CSA_MAP_P

@clientId int,
@userInfoId int

AS
/*declare @is_replication varchar(5)
DECLARE @clientCEMMapid int
select @is_replication = entity_name from entity(nolock) where entity_type = 1040 */

if( @userInfoId = 0)
begin
delete from CLIENT_CSA_MAP where client_id = @clientId
end
else
begin

insert into CLIENT_CSA_MAP
(
	CLIENT_ID,
	USER_INFO_ID
) 
values
(
	@clientId,
	@userInfoId
)
end

/*if @is_replication = 'on' 
	BEGIN
		
		insert into [cbmsdb].sw_test_02_07.dbo.CLIENT_CEM_MAP
		(
			CLIENT_ID,
			USER_INFO_ID
		) 
		values
		(
			@clientId,
			@userInfoId
		)
		

	END*/
GO
GRANT EXECUTE ON  [dbo].[CBMS_CLIENT_CSA_MAP_P] TO [CBMSApplication]
GO
