SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Contract_Dtls_Sel_By_Contract_Id                     
                          
 DESCRIPTION:      
		Get the Suppler account associated invoices count based on Supplier Account.                   
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Sup_Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 


	
   EXEC dbo.Contract_Dtls_Sel_By_Contract_Id  @Contract_Id  = 163963      
    
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Contract_Dtls_Sel_By_Contract_Id]
     (
         @Contract_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , cha.Account_Id AS Supplier_Account_Id
            , cha.Account_Number AS Supplier_Account_Number
            , CASE WHEN LEN(cnt.Meter_Number) > 1 THEN LEFT(cnt.Meter_Number, LEN(cnt.Meter_Number) - 1)
                  ELSE cnt.Meter_Number
              END Meter_Number
            , cha.Supplier_Contract_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , COUNT(DISTINCT cism.CU_INVOICE_ID) AS Invoices
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.Account_ID = cha.Account_Id
                   AND  cism.SERVICE_MONTH BETWEEN cha.Supplier_Account_begin_Dt
                                           AND     ISNULL(cha.Supplier_Account_End_Dt, '2099-01-31')
            CROSS APPLY (   SELECT
                                chh.Meter_Number + ','
                            FROM
                                Core.Client_Hier_Account chh
                            WHERE
                                chh.Account_Id = cha.Account_Id
                                AND chh.Supplier_Contract_ID = @Contract_Id
                            GROUP BY
                                chh.Meter_Number
                            FOR XML PATH('')) cnt(Meter_Number)
        WHERE
            c.CONTRACT_ID = @Contract_Id
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , cha.Account_Id
            , cha.Account_Number
            , CASE WHEN LEN(cnt.Meter_Number) > 1 THEN LEFT(cnt.Meter_Number, LEN(cnt.Meter_Number) - 1)
                  ELSE cnt.Meter_Number
              END
            , cha.Supplier_Contract_ID
            , c.ED_CONTRACT_NUMBER
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;


    END;



GO
GRANT EXECUTE ON  [dbo].[Contract_Dtls_Sel_By_Contract_Id] TO [CBMSApplication]
GO
