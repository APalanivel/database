SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select * from user_info where client_id is null and email_address like '%@summitenergy%'
CREATE  PROCEDURE dbo.RA_GET_USER_NAMES_LIST_P
@userId varchar(10),
@sessionId varchar(10)
as
	set nocount on
select	USER_INFO_ID,
	FIRST_NAME+' '+LAST_NAME USER_INFO_NAME

from	USER_INFO

where client_id is null 
--and email_address like '%summitenergy%'

order by FIRST_NAME,LAST_NAME
GO
GRANT EXECUTE ON  [dbo].[RA_GET_USER_NAMES_LIST_P] TO [CBMSApplication]
GO
