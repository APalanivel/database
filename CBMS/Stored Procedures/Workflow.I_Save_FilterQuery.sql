SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[I_Save_FilterQuery]  
DESCRIPTION:   It'll save the Saved filter informtion for User filters
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @int_Id      AS INT = NULL ,          
 @int_Module_Id     AS INT,          
 @nvarchar_Custom_Filter_Name AS NVARCHAR(MAX),          
 @datetime_Expires_On   AS DATETIME =NULL,          
 @bit_No_Expiration_Date  AS BIT = 0,          
 @int_Created_User_Id   AS INT = 16,          
 --@int_Last_Modified_User_Id AS INT,          
 @bit_Is_Public_Filter   AS BIT = 0,          
 @TVP       AS Save_FilterQuery_SearchFilterValues READONLY,           
 @TVPIdValues     AS Save_FilterQuery_SearchFilterValues READONLY,        
 @bit_IsActive     AS BIT = 1 ,    
 @SubQueueName As NVARCHAR(MAX)     --saved filter, my exception 
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thuammala Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    02-08-2019 Created  
  
******/  
CREATE PROCEDURE [Workflow].[I_Save_FilterQuery]              
(              
 @int_Id      AS INT = NULL ,              
 @int_Module_Id     AS INT,              
 @nvarchar_Custom_Filter_Name AS NVARCHAR(MAX),              
 @datetime_Expires_On   AS DATETIME =NULL,              
 @bit_No_Expiration_Date  AS BIT = 0,              
 @int_Created_User_Id   AS INT = 16,                           
 @bit_Is_Public_Filter   AS BIT = 0,              
 @TVP       AS Save_FilterQuery_SearchFilterValues READONLY,               
 @TVPIdValues     AS Save_FilterQuery_SearchFilterValues READONLY,            
 @bit_IsActive     AS BIT = 1 ,        
 @SubQueueName As NVARCHAR(MAX)     --saved filter, my exception        
              
)              
      
AS              
BEGIN              
              
 DECLARE               
  @subQueue AS int        
           
 SELECT @subQueue = Workflow_Sub_Queue_Id         
  FROM  Workflow.Workflow_sub_queue           
  WHERE Workflow_Queue_Id = @int_Module_Id and Workflow_Sub_Queue_Name= @SubQueueName               
            
              
 SELECT SF.Workflow_Queue_Search_Filter_Id SearchFilters_Id,T.*              
   INTO #tvp            
  FROM  Workflow.workflow_queue_search_filter SF               
   JOIN Workflow.search_filter MSF  ON SF.Search_Filter_Id = MSF.Search_Filter_Id             
   JOIN  @TVP T ON T.SearchFilter_Name = MSF.Filter_Name            
 WHERE SF.Workflow_Queue_Id = @int_Module_Id  --AND SF.ISACTIVE = 1        
   
 DECLARE @Saved_Filter_Type_Cd INT  
 SELECT @Saved_Filter_Type_Cd=C.Code_Id  
 FROM dbo.Code C   
 INNER JOIN dbo.CodeSet CS ON CS.CodeSet_ID=C.CodeSet_ID WHERE CS.Codeset_Name='SavedFilter' AND CS.codeset_Dsc='Saved Filter Types'  
 AND C.Code_Dsc='SystemDefinedFilter' 
           
  INSERT INTO Workflow.Workflow_queue_saved_filter_query               
  (        
   Workflow_Sub_Queue_Id        
   ,Saved_Filter_Name        
   ,Saved_Filter_Type_Cd  --      
   ,Owner_User_Id        
   ,Expiration_Dt           
   ,Display_Seq        
   ,Created_User_Id        
   ,Created_Ts        
   ,Updated_User_Id        
   ,Last_Change_Ts        
   ,Is_Shared        
   --,Is_Expired            
  )              
  SELECT             
   @subQueue  ,        
   @nvarchar_Custom_Filter_Name,        
   @Saved_Filter_Type_Cd,  
   @int_Created_User_Id,            
   @datetime_Expires_On,         
   1,           
   @int_Created_User_Id,        
   GETDATE(),        
   @int_Created_User_Id,        
   GETDATE(),        
   @bit_Is_Public_Filter              
                 
                 
  SELECT @int_Id = SCOPE_IDENTITY()              
                 
              
  INSERT INTO Workflow.workflow_queue_saved_filter_query_value             
    (        
     Workflow_Queue_Saved_Filter_Query_Id        
     ,Workflow_Queue_Search_Filter_Id        
     ,Selected_Value        
     ,Selected_KeyValue        
     ,Created_User_Id        
     ,Created_Ts        
     ,Updated_User_Id        
     ,Last_Change_Ts)            
         
   SELECT         
     @int_Id,        
     v.SearchFilters_Id,         
     v.SearchFilterValue ,        
     iv.SearchFilterValue,        
     @int_Created_User_Id,        
     getdate(),        
     @int_Created_User_Id,        
     getdate() FROM #TVP v, @TVPIdValues iv           
   WHERE v.SearchFilter_Name = iv.SearchFilter_Name             
                 
             
             
 SELECT @int_Id AS ID              
 DROP TABLE #tvp             
              
             
END       
GO
GRANT EXECUTE ON  [Workflow].[I_Save_FilterQuery] TO [CBMSApplication]
GO
