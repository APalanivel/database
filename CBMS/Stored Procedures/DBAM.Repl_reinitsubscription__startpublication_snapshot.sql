SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: 
 
DESCRIPTION:  
    
INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------------------------------------------------------------------
  
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------------------------------------------------------------------
  
USAGE EXAMPLES:    
------------------------------------------------------------------------------------------------------------------------
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------------------------------------------------------------------
	KVK		Vinay Kacham 
MODIFICATIONS     

 Initials	Date		Modification    
------------------------------------------------------------------------------------------------------------------------
	KVK		04/23/2013	Created for SSIS package to initiate replication jobs
******/


CREATE PROCEDURE [dbam].[Repl_reinitsubscription__startpublication_snapshot]
      ( 
       @Publication SYSNAME
      ,@subscriber SYSNAME )
      WITH EXECUTE AS 'dbo'
AS 
BEGIN

      EXEC sp_reinitsubscription 
            @Publication = @Publication
           ,@article = 'ALL'
           ,@subscriber = @Subscriber
           
      EXEC sp_startpublication_snapshot 
            @publication = @Publication

END

;
GO
GRANT EXECUTE ON  [DBAM].[Repl_reinitsubscription__startpublication_snapshot] TO [ETL_Execute]
GO
