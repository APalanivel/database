
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******************************************************************************************************          
NAME : dbo.Data_Upload_Queue_INS      
         
DESCRIPTION:       
Stored Procedure is used to insert data into Data Upload Audit Log.    
     
 INPUT PARAMETERS:          
 Name             DataType               Default        Description          
--------------------------------------------------------------------       
@DataUploadRequestTypeId   INT    
@RequestedBy INT    
@CbmsImageId  INT    
@Status    INT     
@SourceDsc VARCHAR(100)  
    
 OUTPUT PARAMETERS:          
 Name   DataType    Default   Description          
--------------------------------------------------------------------          
@Data_Upload_Queue_Id  INT    
    
      
  USAGE EXAMPLES:          
--------------------------------------------------------------------    

BEGIN TRAN 
      
EXEC Data_Upload_Queue_INS 1,49, 116910, 100309,'User Data Entry Bulk Upload'   

ROLLBACK TRAN  
  
        
AUTHOR INITIALS:          
 Initials	Name          
-------------------------------------------------------------------          
	RT		Romy Thomas    
	PD		Padmanava Debnath
	HG		Harihara Suthan G
	MR		Meera RAmachandran
         
 MODIFICATIONS           
 Initials	Date		Modification          
--------------------------------------------------------------------    
RT			07/13/2011	Created    
PD			09/16/2011	Modified to include Source_Dsc
HG			2011-12-22	Modified to use SCOPE_IDENTITY to return the inserted identity value
MR			2013-03-04  Copied from DVDEHub
******/      

CREATE PROCEDURE [dbo].[Data_Upload_Queue_INS]
      ( 
       @DataUploadRequestTypeId INT
      ,@RequestedBy INT
      ,@CbmsImageId INT
      ,@Status INT
      ,@SourceDsc VARCHAR(100) = NULL )
AS 
BEGIN    

      SET NOCOUNT ON ;
      DECLARE @CurDtTime DATETIME = getdate()

      INSERT      INTO dbo.DATA_UPLOAD_QUEUE
                  ( 
                   DATA_UPLOAD_REQUEST_TYPE_ID
                  ,REQUESTED_USER_INFO_ID
                  ,CBMS_IMAGE_ID
                  ,STATUS_CD
                  ,CREATE_TS
                  ,LAST_UPDATE_TS
                  ,SOURCE_DSC )
      VALUES
                  ( 
                   @DataUploadRequestTypeId
                  ,@RequestedBy
                  ,@CbmsImageId
                  ,@Status
                  ,@CurDtTime
                  ,@CurDtTime
                  ,@SourceDsc )

      SELECT
            scope_identity() AS DataUploadQueueId

END



;
GO

GRANT EXECUTE ON  [dbo].[Data_Upload_Queue_INS] TO [CBMSApplication]
GO
