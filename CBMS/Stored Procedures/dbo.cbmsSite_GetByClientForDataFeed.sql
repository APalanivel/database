SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsSite_GetByClientForDataFeed

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       	          	
	@region_id     	int       	null      	
	@state_id      	int       	null      	
	@exclude_closed	bit       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
JR				Jon Ruel


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	JR			9/28/2009	Added Country	        	
******/

CREATE              PROCEDURE [dbo].[cbmsSite_GetByClientForDataFeed]
   (
     @MyAccountId int
   , @client_id int
   , @region_id int = null
   , @state_id int = null
   , @exclude_closed bit = null
	
   )
AS 
   BEGIN

      if @exclude_closed is null 
         set @exclude_closed = 0
      else 
         set @exclude_closed = null

      select   s.site_id
             , ad.city
             , st.state_name
             , ct.country_name
             , s.site_name
             , ad.address_line1
             , ad.address_line2
             , vw.site_name site_label
             , d.division_name
             , d.division_id
             , c.client_name
             , s.not_managed
             , s.closed
      from     division d
               join site s
                  on ( s.division_id = d.division_id )
               join address ad
                  on ( ad.address_id = s.primary_address_id
                       and ad.state_id = isNull(@state_id, ad.state_id)
                     )
               join state st
                  on st.state_id = ad.state_id
               join country ct
                  on ct.country_id = st.country_id
               join vwSiteName vw
                  on vw.site_id = s.site_id
               join client c
                  on c.client_id = d.client_id
      where    d.client_id = @client_id
      order by vw.site_name


   END
GO
GRANT EXECUTE ON  [dbo].[cbmsSite_GetByClientForDataFeed] TO [CBMSApplication]
GO
