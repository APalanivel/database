SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Workflow_Status_Message_Sel                    
                      
Description:                      
        To get deal ticket workflows
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Client_Workflow_Status_Message_Sel 235,1

Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2019-03-06     Global Risk Management - Created 
                     
******/

CREATE PROCEDURE [dbo].[RM_Client_Workflow_Status_Message_Sel]
      ( 
       @Client_Id INT
      ,@Workflow_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            wsm.Workflow_Status_Map_Id
           ,ws.Workflow_Status_Name
           ,ISNULL(cwm.Workflow_Status_Message, wsm.Workflow_Status_Default_Message) AS Workflow_Status_Message
      FROM
            dbo.Workflow wf
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON wf.Workflow_Id = wsm.Workflow_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN dbo.RM_Client_Workflow_Status_Message_Map cwm
                  ON cwm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                     AND cwm.Client_Id = @Client_Id
      WHERE
            wf.Workflow_Id = @Workflow_Id
            AND ws.Workflow_Status_Name <> 'DT Created'
      
     
END;
GO
GRANT EXECUTE ON  [dbo].[RM_Client_Workflow_Status_Message_Sel] TO [CBMSApplication]
GO
