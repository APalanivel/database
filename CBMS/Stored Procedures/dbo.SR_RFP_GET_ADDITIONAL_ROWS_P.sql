SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_GET_ADDITIONAL_ROWS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@sr_rfp_load_profile_setup_id int,
	@rfp_account_id int,
	@is_default bit
	AS
	set nocount on
	if @is_default = 1
	begin
		select	0 as row_id,
		       	row_name,
			row_value
		from	sr_load_profile_additional_row(nolock)
		where 	sr_load_profile_default_setup_id = @sr_rfp_load_profile_setup_id
	end
	else
	begin
		select	row.sr_rfp_lp_account_additional_row_id as row_id,
		       	row.updated_row_name as row_name,
			row.updated_row_value as row_value
		from	sr_rfp_lp_account_additional_row row(nolock), sr_rfp_account rfp_account(nolock)
		
		where 	row.sr_rfp_account_id = @rfp_account_id 
		        and rfp_account.sr_rfp_account_id = row.sr_rfp_account_id
		        and rfp_account.is_deleted = 0
			and row_type_id != (select entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'Account Level')
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ADDITIONAL_ROWS_P] TO [CBMSApplication]
GO
