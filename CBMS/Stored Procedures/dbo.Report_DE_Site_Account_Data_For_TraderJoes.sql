SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Report_DE_Site_Account_Data_For_TraderJoes             
                        
 DESCRIPTION:                        
			
                        
 INPUT PARAMETERS:                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

                        
 OUTPUT PARAMETERS:                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC Report_DE_Site_Account_Data_For_TraderJoes
                       
 AUTHOR INITIALS:       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
   KVK					VInay K  
                         
 MODIFICATIONS:          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
    KVK					04/05/2017		Created        
    lec					01/29/2018		Add Vendor/Commodity
	lec					03/11/2019		Fix confusing Account Expected issue - column was reporting backward data
	lec					03/11/2019		Removed depedency on IP as expected.
	lec					05/06/2019		Add Client name, Site Reference Number     
                       
******/                 

CREATE PROCEDURE [dbo].[Report_DE_Site_Account_Data_For_TraderJoes]
AS
BEGIN
	
      SET NOCOUNT ON;
      
    WITH  
        Multi_Commodities_Cte AS
            (SELECT DISTINCT account_id
                 FROM cbms.core.client_hier_account  cha
                 JOIN cbms.core.client_hier ch ON ch.Client_Hier_Id = cha.Client_Hier_Id
                 GROUP BY cha.account_id,ch.client_name
                 HAVING COUNT (DISTINCT commodity_id) > 1
                 AND ch.client_name = 'trader joe''s'
                 )
                 ,
                 Trader_Joe_Base_Data_CTE
              AS ( SELECT DISTINCT
					ch.client_name [Client]
                        ,ch.Site_name [Site]
                       ,ch.Site_Id [SiteID]
					    ,ch.Site_Reference_Number [Site Reference Number]
                       ,[BECS Site] = CASE WHEN ss.Sitegroup_id IS NOT NULL THEN 'Yes'
                                           ELSE 'No'
                                      END
                       ,cha.Account_Id [AccountID]
                       ,cha.Display_Account_Number [Account Number]
                       ,[Marked as Expected at Account Level] = CASE WHEN cha.Account_Not_Expected = 1 THEN 'No'
                                                                     ELSE 'Yes'
                                                                END
                       --,[Managed] = CASE WHEN cha.Account_Not_Managed = 1 THEN 'Not Managed'
                       --                  ELSE 'Managed'
                       --             END
                       ,[Currently Active] = CASE WHEN cha.Account_Type = 'utility'
                                                       AND cha.Account_Not_Managed = 0 THEN 'Active'
                                                  WHEN cha.Account_Type = 'Supplier'
                                                       AND cha.Account_Not_Managed = '0'
                                                       AND getdate() BETWEEN cha.Supplier_Account_begin_Dt
                                                                     AND     cha.Supplier_Account_End_Dt THEN 'Active'
                                                  ELSE 'Inactive'
                                             END
                       ,[Utility Account/Contract Added Date] = CASE WHEN cha.Account_Type = 'Utility' THEN ea.MODIFIED_DATE
                                                                     WHEN cha.Account_Type = 'Supplier' THEN ea1.MODIFIED_DATE
                                                                END
                       ,[IP Marked as Expected For Current Month] = CASE WHEN ip.IS_EXPECTED = 1 THEN 'Yes'
                                                                         ELSE 'No'
                                                                    END
                       ,[IP Marked as Received For Current Month] = CASE WHEN ip.IS_RECEIVED = 1 THEN 'Yes'
                                                ELSE 'No'
                                                                    END
                       ,[Vendor] = cha.Account_Vendor_Name
                       ,[Commodity]= CASE WHEN mc.Account_Id IS NULL THEN com.Commodity_Name
                       ELSE 'Multiple Commodities' end
                   FROM
                        cbms.Core.Client_Hier ch
                        JOIN cbms.Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT JOIN Multi_Commodities_Cte mc ON mc.Account_Id = cha.Account_Id
                        JOIN cbms.dbo.Commodity com ON com.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN cbms.dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_IDENTIFIER = cha.Account_Id
                                 AND ea.ENTITY_ID = 491
                                 AND ea.AUDIT_TYPE = 1
                        LEFT JOIN cbms.dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                              ON samm.ACCOUNT_ID = cha.Account_Id
                        LEFT JOIN cbms.dbo.CONTRACT c
                              ON c.CONTRACT_ID = samm.Contract_ID
                        LEFT JOIN cbms.dbo.ENTITY_AUDIT ea1
                              ON ea1.ENTITY_IDENTIFIER = c.CONTRACT_ID
                                 AND ea1.ENTITY_ID = 494
                                 AND ea1.AUDIT_TYPE = 1
                        LEFT JOIN cbms.dbo.INVOICE_PARTICIPATION ip
                              ON ip.ACCOUNT_ID = cha.Account_Id
                                 AND ip.SERVICE_MONTH = dateadd(MONTH, datediff(MONTH, 0, getdate()), 0)
                        LEFT JOIN cbms.dbo.Sitegroup_Site ss
                              ON ss.Site_id = ch.Site_Id
                                 AND ss.Sitegroup_id = 30019763 -- BECS Sites
                   WHERE
                        ch.Client_Name = 'Trader Joe''s')
                        
          
            
            SELECT DISTINCT
                 tc.Client
                  ,tc.[Site]
                 ,tc.[SiteID]
				 ,tc.[Site Reference Number]
                 ,[BECS Site]
                 ,tc.[AccountID]
                 ,tc.[Account Number]
                 ,tc.[Marked as Expected at Account Level]
                 --,tc.[Currently Active]
                 --,tc.[Managed]
                 ,[Utility Account/Contract Added Date]
                 ,[IP Marked as Expected For Current Month]
                 ,[IP Marked as Received For Current Month]
                 ,[tc].[Vendor]
                 ,[tc].[Commodity]
            FROM
                  Trader_Joe_Base_Data_CTE tc
            WHERE
                  tc.[Currently Active] = 'Active'
                  --AND tc.[IP Marked as Expected For Current Month] = 'Yes'
 
END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Site_Account_Data_For_TraderJoes] TO [CBMSApplication]
GO
