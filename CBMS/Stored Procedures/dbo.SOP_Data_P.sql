SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE     Procedure [dbo].[SOP_Data_P]
as
Begin
/**************************************************************************************
  This statement eliminates a lot of useless chatter from the procedures 
***************************************************************************************/

set nocount on

/**************************************************************************************
   Truncate Existing SOPS Table
***************************************************************************************/
Truncate Table SOPS

/**************************************************************************************
 Creating a work table to hold our result sets.  The identity column is necessary so
 that we can join back to the SOPS table.  This is a temporary table rather than the
 preferred table variable because we need to be able to truncate it.  A table variable
 can not be truncated.
***************************************************************************************/

Create Table #tmp1 
(BogusNum    Int Identity(1,1)
 , FieldValue   varchar(255))


/**************************************************************************************
 Pre populating the data for the maximum number of rows available in Excel.  We'll be
 checking that after each step.  We don need these numbers to be pre-populated.  If 
 we used an identity column and populated as we went along, then we run into issues 
 if one column contains more records than the previous and we wouldn't be able to join 
 back.  Then we'd be back in the multiple union queries.
***************************************************************************************/
Declare @cntr int
Set @cntr=1

While @cntr < 65536
   Begin
      Insert SOPS (BogusNum) Values (@cntr)
      Select @cntr = @cntr+1
   End


/**************************************************************************************
 Selecting our First Column.  We insert into the temp table for the unique Client Name
 ordering by client name so that it's in alphabetical order.
***************************************************************************************/
Insert #tmp1(FieldValue)
Select Distinct Client_Name
From Client
Where Not_Managed <> '1'
Order By Client_Name

/**************************************************************************************
 Now that we have the data, let's update the work table to the appropriate column.
 The only thing that will change from field to field is the Set <column_name>.  We join
 back to the work table on the generated number.  Basically, the generated number is
 nothing more than the Excel rownumber.
***************************************************************************************/
Update s
  Set ClientName = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
/**************************************************************************************
 We truncate the work table for a few reasons.  
   1. It's easier on the system to truncate a table 
   2. Truncate resets the identity counter of the BogusNumber back to 1
***************************************************************************************/
Truncate Table #tmp1

/**************************************************************************************
 Now repeat the following 3 statements for each of the remaining columns.
***************************************************************************************/
--City
Insert #tmp1(FieldValue)
Select Distinct City
From Address
Order By City

Update s
  Set City = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--State_Province
Insert #tmp1(FieldValue)
Select Distinct State_name
From State
Order By State_Name

Update s
  Set State_Province = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--SiteName
Insert #tmp1(FieldValue)
Select Distinct Site_Name
From Site
Where Not_Managed <> '1'
Order By Site_name

Update s
  Set SiteName = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1


--SiteType
Insert #tmp1(FieldValue)
Select Distinct Sty.entity_name
From Site s
join entity sty on sty.entity_id = s.site_type_id
Order By sty.entity_name

Update s
  Set SiteType = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--CommodityType
Insert #tmp1(FieldValue)
Select Distinct comm.entity_name
From Entity comm
where entity_type = '157'
Order By comm.entity_name

Update s
  Set CommodityType = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1


--Country
Insert #tmp1(FieldValue)
Select Distinct Country_Name
From Country Ctry
Order By Country_Name

Update s
  Set Country = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)Truncate Table #tmp1

--Region
Insert #tmp1(FieldValue)
Select Distinct Region_Name
From Region r
Order By Region_Name

Update s
  Set Region = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--AccountNumber
Insert #tmp1(FieldValue)
Select Distinct Account_Number
From Account Acct
Where Account_Type_Id = '38'
And   Acct.Not_Managed <> '1'
Order By Account_Number

Update s
  Set AccountNumber = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--UtilityAccountServiceLevel
Insert #tmp1(FieldValue)
Select Distinct USrv.Entity_Name
From Entity USrv
Where Entity_Type = '708'
Order By USrv.Entity_Name

Update s
  Set UtilityAccountServiceLevel = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1


--Utility
Insert #tmp1(FieldValue)
Select Distinct Vendor_Name
From Vendor v
Where v.Vendor_Type_ID = '289'
Order By Vendor_Name


Update s
  Set Utility = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1


--ContractNumber
Insert #tmp1(FieldValue)
Select Distinct Ed_Contract_Number
From Contract c
Order By Ed_Contract_Number


Update s
  Set ContractNumber = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--ContractType

Insert #tmp1(FieldValue)
Select Distinct Cty.Entity_Name
From Entity Cty
Where Cty.Entity_Type = '129'
Order By Cty.Entity_Name


Update s
  Set ContractType = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--ContractedVendor
Insert #tmp1(FieldValue)
Select Distinct V1.Vendor_Name
From Vendor V1
join Account acct1 on acct1.Vendor_id = V1.Vendor_Id
Where acct1.Account_Type_ID = '37'
Order By v1.Vendor_Name

Update s
  Set ContractedVendor = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--ContractStartDate
Insert #tmp1(FieldValue)
Select Distinct Contract_Start_Date
From Contract
Order By Contract_Start_Date

Update s
  Set ContractStartDate = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--ContractEndDate
Insert #tmp1(FieldValue)
Select Distinct Contract_End_Date
From Contract
Order By Contract_End_Date

Update s
  Set ContractEndDate = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1


--SupplierAccountNumber
Insert #tmp1(FieldValue)
Select Distinct Acct2.Account_Number
From Account acct2
Where acct2.Account_Type_Id = '37'
Order By Account_Number

Update s
  Set SupplierAccountNumber = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--SupplierAccountServiceLevel

Insert #tmp1(FieldValue)
Select Distinct SSrv.Entity_Name
From Entity SSrv
Where Entity_Type = '708'
Order By SSrv.Entity_Name

Update s
  Set SupplierAccountServiceLevel = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--RecalculationType
Insert #tmp1(FieldValue)
Select Distinct CR.Entity_Name
From Entity CR
Where Entity_Type = '301'
Order By CR.Entity_Name

Update s
  Set RecalculationType = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1



--GroupBillStatus -- 

Update SOPS
  Set GroupBillStatus = 'Yes' 
  Where BogusNum = 1

Update SOPS
  Set GroupBillStatus = 'No'
  Where BogusNum = 2
If @@RowCount > 65536 
   RaisError ('Rows Exceeded 65,536',16,1)
Truncate Table #tmp1

--This needs to just be 'Yes' or 'No'

/**************************************************************************************
Drop the Temporary Table.  This should be the last step before you return your results
***************************************************************************************/
Drop Table #tmp1

/**************************************************************************************
***************************************************************************************/
---NOT NEEDED FOR THIS ENHANCEMENT
/*
--SiteID --NotNeeded
Insert #tmp1(FieldValue)
Select Distinct Site_Id
From Site
Order By Site_Id

Update s
  Set SiteID = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/


--ClientNotManaged --NotNeeded
--This needs to just be 'Yes' or 'No'


--SiteNotManaged --NotNeeded
--This needs to just be 'Yes' or 'No'

--SiteClosed --NotNeeded
--This needs to just be 'Yes' or 'No'

/*
--AddressLine1 -- Not Needed
Insert #tmp1(FieldValue)
Select Distinct Address_Line1
From Address addr

Order By Address_line1

Update s
  Set AddressLine1 = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--AddressLine2 -- Not Needed
Insert #tmp1(FieldValue)
Select Distinct Address_Line2
From Address addr

Order By Address_line2

Update s
  Set AddressLine2 = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--ZipCode -- Not Needed
Insert #tmp1(FieldValue)
Select Distinct zipcode
From Address addr

Order By Zip_Code

Update s
  Set ZipCode = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

--AccountINvoiceNotExpected --Not Needed
--This needs to just be 'Yes' or 'No'

--AccountNotManaged -- Not Needed
--This needs to just be 'Yes' or 'No'

/*
--MeterNumber --NotNeeded
Insert #tmp1(FieldValue)
Select Distinct Meter_Number
From Meter m
Join Account acct on acct.Account_Id = m.Account_id
where acct.not_managed <> '1'
Order By Meter_Number

Update s
  Set MeterNumber = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--ContractID --Not Needed
Insert #tmp1(FieldValue)
Select Distinct Contract_Id
From Contract
Order By Contract_Id

Update s
  Set ContractID = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--BaseContractID --Not Needed
Insert #tmp1(FieldValue)
Select Distinct Base_Contract_ID
From Contract c
Order By Base_Contract_Id

Update s
  Set BaseContractID = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/


/*
--ContractPricingSummary --Not Needed
Insert #tmp1(FieldValue)
Select Distinct Contract_Pricing_Summary
From Contract
Order By Contract_Pricing_Summary

Update s
  Set ContractPricingSummary = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/


/*
--ContractComments --Not Needed
Insert #tmp1(FieldValue)
Select Distinct Contract_Comments
From Contract
Order By Contract_Comments

Update s
  Set ContractComments = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--Currency --Not Needed
Insert #tmp1(FieldValue)
Select Distinct Currency_Unit_Name
From Currency_Unit
Order By Currency_Unit_Name

Update s
  Set Currency = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

/*
--Rate -- Not Needed
Insert #tmp1(FieldValue)
Select Distinct Rate_Name
From Rate
Order By Rate_Name

Update s
  Set Rate = t.FieldValue
From #tmp1 t 
         inner join
     SOPS S on T.BogusNum = S.BogusNum

Truncate Table #tmp1
*/

--PurchaseMethod  -- Not Needed
----This needs to just be 'Yes' or 'No'

--FullRequirements -- Not Needed
--This needs to just be 'Yes' or 'No'


End












GO
GRANT EXECUTE ON  [dbo].[SOP_Data_P] TO [CBMSApplication]
GO
