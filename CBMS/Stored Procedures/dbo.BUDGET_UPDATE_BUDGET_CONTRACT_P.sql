SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE dbo.BUDGET_UPDATE_BUDGET_CONTRACT_P
	@budget_contract_id int,
	@comments varchar(4000)
	AS

	update budget_contract_budget set comments = @comments where budget_contract_budget_id = @budget_contract_id







GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_BUDGET_CONTRACT_P] TO [CBMSApplication]
GO
