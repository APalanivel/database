
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.cbmsGroupInfo_GetByQueueType            
                        
 DESCRIPTION:                        
			Returns Groups based on Queue Type                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @QueueType                  VARCHAR(150)                                   
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
    EXEC dbo.cbmsGroupInfo_GetByQueueType 
      @QueueType = 'InvoiceProcessingTeam'
           
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 KH					   K. Horton
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 KH					   10-8-2009        Created.
 SP                    2014-08-25       Header Added.
										For Data Transition ,Added Order by group_name.Added new columns Is_Shown_On_Watch_List,Is_Shown_On_VEN_CC_List 
										in Select list.
										
                       
******/ 

CREATE PROCEDURE [dbo].[cbmsGroupInfo_GetByQueueType]
      ( 
       @QueueType VARCHAR(150) )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            gi.group_info_id
           ,gi.group_name
           ,gi.group_description
           ,gi.Is_Shown_On_Watch_List
           ,gi.Is_Shown_On_VEN_CC_List
      FROM
            GROUP_INFO gi
            JOIN QUEUE q
                  ON q.queue_id = gi.QUEUE_ID
            JOIN Codeset cs
                  ON cs.Codeset_Id = q.QUEUE_TYPE_ID
      WHERE
            cs.Codeset_Name = @QueueType
      UNION
      SELECT
            gi.group_info_id
           ,gi.group_name
           ,gi.group_description
           ,0 AS Is_Shown_On_Watch_List
           ,0 AS Is_Shown_On_VEN_CC_List
      FROM
            GROUP_INFO gi
      WHERE
            gi.GROUP_NAME = 'operations'
      ORDER BY
            gi.GROUP_NAME

	
END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetByQueueType] TO [CBMSApplication]
GO
