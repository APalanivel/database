SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@priceIndexId  	int       	          	
	@clientId      	int       	          	
	@divisionId    	int       	          	
	@siteId        	int       	          	
	@hedgeStartMonth	datetime  	          	
	@hedgeEndMonth 	datetime  	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelTypeId	int       	          	
	@hedgeModeTypeId	int       	          	
	@currencyTypeId	int       	          	
	@unitTypeId    	int       	          	
	@frequencyTypeId	int       	          	
	@allocationTypeId	int       	          	
	@contactName   	varchar(200)	          	
	@contactPhone  	varchar(200)	          	
	@contactCell   	varchar(200)	          	
	@contactFax    	varchar(200)	          	
	@contactEmail  	varchar(200)	          	
	@counterpartyId	int       	          	
	@counterpartyContactName	varchar(200)	          	
	@counterpartyContactPhone	varchar(200)	          	
	@counterpartyContactCell	varchar(200)	          	
	@counterpartyContactFax	varchar(200)	          	
	@counterpartyContactEmail	varchar(1000)	          	
	@actionRequiredTypeId	int       	          	
	@predealComments	varchar(1000)	          	
	@dealTypeId    	int       	          	
	@dealTicketNumber	varchar(50)	          	
	@confirmationTypeId	int       	          	
	@isInputByClient	bit       	          	
	@isTriggerRollover	bit       	          	
	@dealInitiatedBy	varchar(200)	          	
	@dealInitiatedDate	datetime  	          	
	@orderPlacedBy 	int       	          	
	@orderPlacedDate	datetime  	          	
	@verbalConfirmationBy	varchar(200)	          	
	@verbalConfirmationDate	datetime  	          	
	@pricingRequestTypeId	int       	          	
	@contractId    	int       	          	
	@vendorId      	int       	          	
	@dealTransactionTypeId	int       	          	
	@groupId       	int       	          	
	@isBid         	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE DBO.CREATE_DEAL_TICKET_P 

@userId varchar(10),
@sessionId varchar(20),
@priceIndexId int, 
@clientId int, 
@divisionId int, 
@siteId int, 
@hedgeStartMonth datetime, 
@hedgeEndMonth datetime, 
@hedgeTypeId  int, 
@hedgeLevelTypeId int, 
@hedgeModeTypeId int, 
@currencyTypeId int, 
@unitTypeId int, 
@frequencyTypeId int, 
@allocationTypeId int, 
@contactName varchar(200), 
@contactPhone varchar(200), 
@contactCell varchar(200), 
@contactFax varchar(200), 
@contactEmail varchar(200), 
@counterpartyId  int, 
@counterpartyContactName varchar(200), 
@counterpartyContactPhone varchar(200), 
@counterpartyContactCell varchar(200),
@counterpartyContactFax varchar(200), 
@counterpartyContactEmail varchar(1000), 
@actionRequiredTypeId int, 
@predealComments varchar(1000), 
@dealTypeId int, 
@dealTicketNumber varchar(50), 
@confirmationTypeId int, 
@isInputByClient bit, 
@isTriggerRollover bit, 
@dealInitiatedBy varchar(200), 
@dealInitiatedDate datetime, 
@orderPlacedBy int, 
@orderPlacedDate datetime, 
@verbalConfirmationBy varchar(200), 
@verbalConfirmationDate datetime, 
@pricingRequestTypeId int,
@contractId int, 
@vendorId int, 
@dealTransactionTypeId int,
@groupId int,  
@isBid bit -- Added by Krishna Arava for Bz5754

AS
set nocount on
DECLARE @queueId int
select @queueId = QUEUE_ID FROM USER_INFO WHERE USER_INFO_ID = @userId

INSERT INTO RM_DEAL_TICKET 
(QUEUE_ID, PRICE_INDEX_ID, CLIENT_ID, DIVISION_ID, SITE_ID, HEDGE_START_MONTH, 
 HEDGE_END_MONTH, HEDGE_TYPE_ID, HEDGE_LEVEL_TYPE_ID, HEDGE_MODE_TYPE_ID,
 CURRENCY_TYPE_ID, UNIT_TYPE_ID, FREQUENCY_TYPE_ID, ALLOCATION_TYPE_ID, CONTACT_NAME,
 CONTACT_PHONE, CONTACT_CELL, CONTACT_FAX, CONTACT_EMAIL, RM_COUNTERPARTY_ID, 
 COUNTERPARTY_CONTACT_NAME, COUNTERPARTY_CONTACT_PHONE, COUNTERPARTY_CONTACT_CELL,
 COUNTERPARTY_CONTACT_FAX, COUNTERPARTY_CONTACT_EMAIL, ACTION_REQUIRED_TYPE_ID,
 PREDEAL_COMMENTS, DEAL_TYPE_ID, DEAL_TICKET_NUMBER, CONFIRMATION_TYPE_ID, IS_INPUT_BY_CLIENT,
 IS_TRIGGER_ROLLOVER, DEAL_INITIATED_BY, DEAL_INITIATED_DATE, ORDER_PLACED_BY, 
 ORDER_PLACED_DATE, VERBAL_CONFIRMATION_BY, VERBAL_CONFIRMATION_DATE,PRICING_REQUEST_TYPE_ID,
 CONTRACT_ID, VENDOR_ID, DEAL_TRANSACTION_TYPE_ID,RM_GROUP_ID,IS_BID)
VALUES(@queueId, @priceIndexId, @clientId, @divisionId, @siteId, @hedgeStartMonth, 
@hedgeEndMonth, @hedgeTypeId, @hedgeLevelTypeId, @hedgeModeTypeId, @currencyTypeId, 
@unitTypeId, @frequencyTypeId, @allocationTypeId, @contactName, @contactPhone, @contactCell, 
@contactFax, @contactEmail, @counterpartyId, @counterpartyContactName, @counterpartyContactPhone, 
@counterpartyContactCell, @counterpartyContactFax, @counterpartyContactEmail, 
@actionRequiredTypeId, @predealComments, @dealTypeId, @dealTicketNumber, 
@confirmationTypeId, @isInputByClient, @isTriggerRollover, @dealInitiatedBy, 
@dealInitiatedDate, @orderPlacedBy, @orderPlacedDate, @verbalConfirmationBy, 
@verbalConfirmationDate, @pricingRequestTypeId,@contractId, @vendorId, @dealTransactionTypeId,@groupId,@isBid)
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_P] TO [CBMSApplication]
GO
