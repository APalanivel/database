SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Dtl_Sel_By_Account_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
    @Account_Id					INT	
	@Contract_Id				INT				
    @Commodity_Id				INT

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
-----------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-----------------------------------------------------------------------------------------
  EXEC dbo.Contract_Dtl_Sel_By_Account_Id
      @Account_Id = 1527279	
	 

AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
	
MODIFICATIONS
	Initials	Date			Modification
-----------------------------------------------------------------------------------------
	NR       	2019-10-30		Created for Add Contract.
	 
******/

CREATE PROCEDURE [dbo].[Contract_Dtl_Sel_By_Account_Id]
    (
        @Account_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            cha.Supplier_Contract_ID
            , c2.ED_CONTRACT_NUMBER
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.CONTRACT c2
                ON c2.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
        GROUP BY
            cha.Supplier_Contract_ID
            , c2.ED_CONTRACT_NUMBER;






    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Dtl_Sel_By_Account_Id] TO [CBMSApplication]
GO
