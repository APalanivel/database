SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Contract_Recalc_Config_Ins      
              
Description:              
        To insert Data into Contract_Recalc_Config table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Id						INT
     @Commodity_Id						INT
     @Start_Dt							DATE
     @End_Dt							DATE				NULL
     @Invoice_Recalc_Type_Cd			INT
     @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Contract_Recalc_Config WHERE Contract_Id =1148520
    EXEC dbo.Contract_Recalc_Config_Ins 
      @Contract_Id =1148520
     ,@Determinant_Source_Cd = 290
     ,@Start_Dt = '2015-01-01'
     ,@End_Dt= NULL
     ,@Supplier_Recalc_Type_Cd = 102030
     ,@User_Info_Id = 49
	 ,@Commodity_Id = 290
    
	SELECT * FROM Contract_Recalc_Config WHERE Contract_Id =1148520
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/

CREATE PROCEDURE [dbo].[Contract_Recalc_Config_Ins]
    (
        @Contract_Id INT
        , @Commodity_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE = NULL
        , @Supplier_Recalc_Type_Cd INT
        , @User_Info_Id INT
        , @Determinant_Source_Cd INT
        , @Is_Consolidated_Contract_Config BIT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;



        INSERT INTO dbo.Contract_Recalc_Config
             (
                 Contract_Id
                 , Commodity_Id
                 , Determinant_Source_Cd
                 , Start_Dt
                 , End_Dt
                 , Supplier_Recalc_Type_Cd
                 , Created_Ts
                 , Created_User_Id
                 , Updated_User_Id
                 , Last_Change_Ts
                 , Is_Consolidated_Contract_Config
             )
        VALUES
            (@Contract_Id
             , @Commodity_Id
             , @Determinant_Source_Cd
             , @Start_Dt
             , @End_Dt
             , @Supplier_Recalc_Type_Cd
             , GETDATE()
             , @User_Info_Id
             , @User_Info_Id
             , GETDATE()
             , @Is_Consolidated_Contract_Config);





    END;






GO
GRANT EXECUTE ON  [dbo].[Contract_Recalc_Config_Ins] TO [CBMSApplication]
GO
