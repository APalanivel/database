SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_SAD_UPLOAD_CANCEL_RENEGOTIATION_DOCUMENT_P]

	@userId int,    
	@sessionId varchar(100),    
	--@cbmsImageDocId varchar(200),     
	--@cbmsImage image ,    
	--@contentType varchar(200),    
	@cbmsImageId int,    
	@accountId int,    
	@contractNumber varchar(25)    
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @entityId INT
		, @contractId INT
	
	SELECT @entityId = ENTITY_ID FROM dbo.Entity(NOLOCK) WHERE Entity_Name = 'Cancel Renegotiation ' AND Entity_Type = 100
    
	SELECT @contractId = CONTRACT_ID FROM dbo.Contract WHERE Ed_Contract_Number = @contractNumber

	/*    
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
	    
	declare @cbmsImageId int    
	select @cbmsImageId = @@Identity  */    
	    
	--added by Jaya for updating entityid
	
	UPDATE dbo.Cbms_Image SET Cbms_Image_Type_ID = @entityId WHERE Cbms_Image_ID = @cbmsImageId

	INSERT INTO dbo.SR_CANCEL_RENEGOTIATION_DOCUMENT(ACCOUNT_ID
		, CONTRACT_ID
		, CBMS_IMAGE_ID
		, UPLOADED_BY
		, UPLOADED_DATE)
	SELECT DISTINCT account_id
		, contract_id
		, @cbmsImageId
		, @userId
		, GETDATE()
	FROM dbo.SR_ALL_CONTRACT_VW
	WHERE contract_id = @contractId

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_UPLOAD_CANCEL_RENEGOTIATION_DOCUMENT_P] TO [CBMSApplication]
GO
