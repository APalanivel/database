SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[UPDATE_FORECAST_FOR_UNIT_CHANGE_P]
   @userId varchar(10)
 , @sessionId varchar(50)
 , @siteId int
 , @unitId int
AS 
   set nocount on
   declare @contractVolumeId int
    , @previousContractVolumeId int
    , @editContractVolumeId int
    , @previousEditContractVolumeId int

   select   @contractVolumeId = entity_id
   from     entity
   where    entity_name = 'Actual_Contract'
            and entity_type = 285
   select   @editContractVolumeId = entity_id
   from     entity
   where    entity_name = 'Editable_Actual_Contract'
            and entity_type = 285
   select   @previousContractVolumeId = entity_id
   from     entity
   where    entity_name = 'Previous_Contract'
            and entity_type = 285
   select   @previousEditContractVolumeId = entity_id
   from     entity
   where    entity_name = 'Previous_Editable_Actual_Contract'
            and entity_type = 285



   update   rm_forecast_volume_details
   set      volume = ( volume
                       * ( select   conversion.CONVERSION_FACTOR
                           from     RM_ONBOARD_HEDGE_SETUP hedge
                                  , CONSUMPTION_UNIT_CONVERSION conversion
                           where    conversion.base_unit_id = hedge.VOLUME_UNITS_TYPE_ID
                                    and conversion.converted_unit_id = @unitId
                                    and hedge.site_id = @siteId
                         ) )
   where    site_id = @siteId
            and volume_type_id in ( @contractVolumeId,
                                    @editContractVolumeId,
                                    @previousContractVolumeId,
                                    @previousEditContractVolumeId )


GO
GRANT EXECUTE ON  [dbo].[UPDATE_FORECAST_FOR_UNIT_CHANGE_P] TO [CBMSApplication]
GO
