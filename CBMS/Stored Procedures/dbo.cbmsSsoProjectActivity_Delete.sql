SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoProjectActivity_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@project_activity_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  procedure [dbo].[cbmsSsoProjectActivity_Delete]
	( @MyAccountId int
	, @project_activity_id int
	)
AS
BEGIN

	set nocount on

	delete from sso_project_activity where sso_project_activity_id = @project_activity_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectActivity_Delete] TO [CBMSApplication]
GO
