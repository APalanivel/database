
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
	dbo.Supplier_Account_Term_Sel_By_Contract_Id 

Description:
	To get supplier account(s) and date period associated to given contract id
   
Input Parameters:    
	Name			DataType	Default		Description    
-------------------------------------------------------------------------------------    
    @Contract_Id	INT
 
 Output Parameters:  
	Name			Datatype	Default		Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    

	Exec dbo.Supplier_Account_Term_Sel_By_Contract_Id 85003
	Exec dbo.Supplier_Account_Term_Sel_By_Contract_Id 81066
	Exec dbo.Supplier_Account_Term_Sel_By_Contract_Id 88906

Author Initials:    
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
	HG			Harihara Suthan Ganesan
 Modifications:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2011-12-30  Created(Needed for aggregation after deleting contract)
	HG			2012-04-11	Client_Hier_id column included as per Additional data enhancement

******/
CREATE PROCEDURE dbo.Supplier_Account_Term_Sel_By_Contract_Id 
( @Contract_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            cha.Account_Id
           ,cha.Supplier_Account_begin_Dt
           ,cha.Supplier_Account_End_Dt
           ,cha.Client_Hier_Id
      FROM
            Core.Client_Hier_Account cha
      WHERE
            cha.Supplier_Contract_ID = @Contract_Id
      GROUP BY
            cha.Account_Id
           ,cha.Supplier_Account_begin_Dt
           ,cha.Supplier_Account_End_Dt
           ,cha.Client_Hier_Id

END
;
GO

GRANT EXECUTE ON  [dbo].[Supplier_Account_Term_Sel_By_Contract_Id] TO [CBMSApplication]
GO
