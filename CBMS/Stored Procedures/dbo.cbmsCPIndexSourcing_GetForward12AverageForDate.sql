SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexSourcing_GetForward12AverageForDate

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index_id	int       	null      	
	@index_detail_date	datetime  	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsCPIndexSourcing_GetForward12AverageForDate]
	( @clearport_index_id int = null
	, @index_detail_date datetime = null
	)
AS
BEGIN


	
declare @dteFirstMonth datetime
declare @dteSettlementDate datetime

select @dteFirstMonth = month_identifier, @dteSettlementDate = settlement_date from rm_nymex_settlement with (nolock)
where month(settlement_date) = month(@index_detail_date)
and year(settlement_date) = year(@index_detail_date)

if @index_detail_Date > @dtesettlementdate
set @dteFirstMonth = dateadd(month,1,@dtefirstmonth)


	select cd.index_detail_date
		, avg(cd.index_detail_value) as average
		--, cm.clearport_index_month
		--, ci.clearport_index
	     from clearport_index_detail cd  with (nolock)
    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
   	     where cm.clearport_index_id = @clearport_index_id
		and cm.clearport_index_month <= dateadd(month,11,@dteFirstMonth)
		and cm.clearport_index_month >= @dteFirstMonth 
		and cd.index_Detail_date = @index_detail_date
	   group by cd.index_detail_date


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexSourcing_GetForward12AverageForDate] TO [CBMSApplication]
GO
