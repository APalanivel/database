SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.BUDGET_GET_BUDGETS_UNDER_CLIENT

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	
	@divisionId    	int       	          	
	@siteId        	int       	          	
	@commodityId   	int       	          	
	@year          	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec BUDGET_GET_BUDGETS_UNDER_CLIENT '', '' , 10009 , 141 , 18017 ,290, 2008
--exec BUDGET_GET_BUDGETS_UNDER_CLIENT '', '' , 228 , 0 , 0 , 291 , 2008

CREATE          PROCEDURE dbo.BUDGET_GET_BUDGETS_UNDER_CLIENT
@userId varchar(10),
@sessionId varchar(20),
@clientId int ,
@divisionId int , 
@siteId int , 
@commodityId int , 
@year int


AS
begin
set nocount on
--select *from budget_details
--select * from budget
if @divisionId = 0
set @divisionId = null
if @siteId = 0
set @siteId = null
select distinct  ba.budget_id
		,b.budget_name --as 'BUDGET_NAME'
		,b.date_posted 
		,b.posted_by
		,b.date_initiated
		,case 	when isnull(b.posted_by, 0) > 0
			then 1 
			else 0
			end is_posted_to_dv

	FROM account acct
	join budget_account ba on ba.account_id = acct.account_id
	JOIN SITE S WITH(NOLOCK) ON (ACCT.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
	JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
	JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))
	join budget b on ba.budget_id = b.budget_id	and b.commodity_type_id = @commodityId and b.BUDGET_START_YEAR = @year
order by b.date_initiated desc
 

end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGETS_UNDER_CLIENT] TO [CBMSApplication]
GO
