
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_AlternateFuel_CostData
	
DESCRIPTION:
	
INPUT PARAMETERS:
Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
@Client_id_list	VARCHAR(MAX)
@begin_date		DATETIME
@end_date			DATETIME
@Commodity_id		INT
@Account_Active	INT
@Site_Active		INT
@Currency_unit_id	INT 	

OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
	EXEC Report_AlternateFuel_CostData '11231','05/01/2010','12/01/2010',67,0,0,3 
	
AUTHOR INITIALS:
Initials	  Name
------------------------------------------------------------
SSR		  Sharad srivastava
AP		  Athmaram Pabbathi

MODIFICATIONS
Initials	  Date	    Modification
------------------------------------------------------------
SSR		  11/19/2010  Created
SKA		  11/30/2010  Qualified all the columns and tables.
SSR		  12/01/2010  Removed Service Month not null condition from WHERE Clause in Final select Query to get all the account details.
SSR		  02/02/2011  Added Meta.Date_dim in final select query to get all months 
AP		  2012-05-23  Addnl Data Changes
					   - Added Client_Hier_Id in the joins of CUAD and CTE
					   - Removed TotalAltFuelAccounts_CTE and used temporary table
					   - Removed dbo.Currency_Unit table from AltFuelUsage_CTE 
					   - Used Temp table in AltFuelUsage_CTE 
					   - Removed CTE_Final_Month_wise and modified the logic accordingly
*/
CREATE PROCEDURE [dbo].[Report_AlternateFuel_CostData]
      ( 
       @Client_id_list VARCHAR(MAX)
      ,@begin_date DATETIME
      ,@end_date DATETIME
      ,@Commodity_id INT
      ,@Account_Active INT
      ,@Site_Active INT
      ,@Currency_unit_id INT )
AS 
BEGIN   
  
      SET NOCOUNT ON;  
      DECLARE @Currency_Name VARCHAR(20)
  
      CREATE TABLE #TotalAltFuelAccounts
            ( 
             [Client] VARCHAR(200)
            ,[Division] VARCHAR(200)
            ,[Site] VARCHAR(200)
            ,[State] VARCHAR(200)
            ,[Country] VARCHAR(200)
            ,[Region] VARCHAR(200)
            ,[Account] VARCHAR(50)
            ,[Vendor] VARCHAR(200)
            ,[Account ID] INT
            ,[Service Level] VARCHAR(200)
            ,[Consumption Level] VARCHAR(100)
            ,[Data Entry Only] VARCHAR(10)
            ,[Account Type] VARCHAR(200)
            ,[Commodity] VARCHAR(200)
            ,Site_Not_Managed VARCHAR(10)
            ,Account_Not_Managed VARCHAR(10)
            ,Client_Currency_Group_Id INT
            ,Client_Hier_Id INT
            ,Commodity_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, [Account Id], Commodity_Id ) )
  
  
      SELECT
            @Currency_Name = a.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT a
      WHERE
            a.CURRENCY_UNIT_ID = @Currency_unit_id;

      INSERT      INTO #TotalAltFuelAccounts
                  ( 
                   Client
                  ,Division
                  ,[Site]
                  ,[State]
                  ,Country
                  ,Region
                  ,Account
                  ,Vendor
                  ,[Account ID]
                  ,[Service Level]
                  ,[Consumption Level]
                  ,[Data Entry Only]
                  ,[Account Type]
                  ,Commodity
                  ,Site_Not_Managed
                  ,Account_Not_Managed
                  ,Client_Currency_Group_Id
                  ,Client_Hier_Id
                  ,Commodity_Id )
                  SELECT
                        ch.Client_Name [Client]
                       ,ch.Sitegroup_Name [Division]
                       ,ch.Site_name [Site]
                       ,ca.Meter_State_Name [State]
                       ,ca.Meter_Country_Name [Country]
                       ,ch.REGION_NAME [Region]
                       ,ca.Account_Number [Account]
                       ,ca.Account_Vendor_Name [Vendor]
                       ,ca.Account_Id [Account ID]
                       ,e.ENTITY_NAME [Service Level]
                       ,vcl.Consumption_Level_Desc [Consumption Level]
                       ,case WHEN ca.Account_Is_Data_Entry_Only = 1 THEN 'Yes'
                             ELSE 'No'
                        END [Data Entry Only]
                       ,ca.Account_Type [Account Type]
                       ,com.Commodity_Name [Commodity]
                       ,case WHEN ch.Site_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END Site_Not_Managed
                       ,case WHEN ca.Account_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END Account_Not_Managed
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Hier_Id
                       ,com.Commodity_Id
                  FROM
                        Core.Client_Hier ch
                        JOIN Core.Client_Hier_Account ca
                              ON ca.Client_Hier_Id = ch.Client_Hier_Id
                        JOIN dbo.Commodity com
                              ON com.Commodity_Id = ca.Commodity_Id
                        JOIN dbo.[state] st
                              ON st.STATE_ID = ca.Meter_State_Id
                        JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = ca.Account_Service_level_Cd
                        JOIN dbo.Account_Variance_Consumption_Level avcl
                              ON avcl.ACCOUNT_ID = ca.Account_Id
                        JOIN dbo.Variance_Consumption_Level vcl
                              ON vcl.Commodity_Id = ca.Commodity_Id
                                 AND vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
                        JOIN dbo.ufn_split(@Client_id_list, ',') ufn_Client
                              ON cast(ufn_Client.Segments AS INT) = ch.Client_Id
                  WHERE
                        com.Commodity_Name NOT IN ( 'Electric Power', 'Natural Gas' )
                        AND com.Commodity_Id = @Commodity_id
                        AND ch.Site_Id != 0
                        AND ( ( @Account_Active = -1 )
                              OR ( ca.Account_Not_Managed = @Account_Active ) )
                        AND ( ( @Site_Active = -1 )
                              OR ( ch.Site_Not_Managed = @Site_Active ) )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.Site_name
                       ,ca.Meter_State_Name
                       ,ca.Meter_Country_Name
                       ,ch.REGION_NAME
                       ,ca.Account_Number
                       ,ca.Account_Vendor_Name
                       ,ca.Account_Id
                       ,e.ENTITY_NAME
                       ,vcl.Consumption_Level_Desc
                       ,case WHEN ca.Account_Is_Data_Entry_Only = 1 THEN 'Yes'
                             ELSE 'No'
                        END
                       ,ca.Account_Type
                       ,com.Commodity_Name
                       ,case WHEN ch.Site_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END
                       ,case WHEN ca.Account_Not_Managed = 1 THEN 'No'
                             ELSE 'Yes'
                        END
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Hier_Id
                       ,com.Commodity_Id;
                       
      WITH  AltFuelUsage_CTE
              AS ( SELECT
                        accts.Client
                       ,accts.Division
                       ,accts.[Site]
                       ,accts.Site_Not_Managed
                       ,accts.[State]
                       ,accts.Country
                       ,accts.Region
                       ,accts.Account
                       ,accts.Account_Not_Managed
                       ,accts.Vendor
                       ,accts.[Account ID]
                       ,accts.[Service Level]
                       ,accts.[Consumption Level]
                       ,accts.[Data Entry Only]
                       ,accts.[Account Type]
                       ,accts.Commodity
                       ,ad.bucket_value * cuc.Conversion_Factor AS CurrencyConvertedValue
                       ,ad.Client_Hier_Id
                       ,ad.Service_Month [Service Month]
                   FROM
                        #TotalAltFuelAccounts accts
                        JOIN dbo.Cost_Usage_Account_Dtl ad
                              ON accts.[Account Id] = ad.Account_Id
                                 AND accts.Client_Hier_Id = ad.Client_Hier_Id
                        JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = ad.Bucket_Master_Id
                                 AND bm.Commodity_Id = accts.Commodity_Id
                        JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = ad.CURRENCY_UNIT_ID
                                 AND cuc.CURRENCY_GROUP_ID = accts.Client_Currency_Group_Id
                                 AND cuc.CONVERTED_UNIT_ID = @Currency_unit_id
                                 AND cuc.CONVERSION_DATE = ad.Service_Month
                   WHERE
                        bm.Bucket_Name = 'Total Cost'
                        AND ad.Service_Month BETWEEN @begin_date
                                             AND     @end_date)
            SELECT
                  cte_fmw.Client
                 ,cte_fmw.Division
                 ,cte_fmw.[Site]
                 ,cte_fmw.Site_Not_Managed
                 ,cte_fmw.[State]
                 ,cte_fmw.Country
                 ,cte_fmw.Region
                 ,cte_fmw.Account
                 ,cte_fmw.Account_Not_Managed
                 ,cte_fmw.Vendor
                 ,cte_fmw.[Account ID]
                 ,cte_fmw.[Service Level]
                 ,cte_fmw.[Consumption Level]
                 ,cte_fmw.[Data Entry Only]
                 ,cte_fmw.[Account Type]
                 ,cte_fmw.Commodity
                 ,dd.Date_D [Service Month]
                 ,cte_fmw.CurrencyConvertedValue
                 ,@Currency_Name [Currency_Name]
            FROM
                  meta.Date_Dim dd
                  LEFT JOIN AltFuelUsage_CTE cte_fmw
                        ON dd.date_d = cte_fmw.[Service Month]
            WHERE
                  dd.Date_D BETWEEN @begin_date AND @end_date
            ORDER BY
                  dd.Date_D
                  
      DROP TABLE #TotalAltFuelAccounts
                 
END
;
GO

GRANT EXECUTE ON  [dbo].[Report_AlternateFuel_CostData] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_AlternateFuel_CostData] TO [CBMSApplication]
GO
