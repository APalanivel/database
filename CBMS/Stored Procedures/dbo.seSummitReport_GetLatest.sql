SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     Procedure dbo.seSummitReport_GetLatest
	( @ReportTypeId int 
	, @ClientName varchar(50) = null
	)
As
BEGIN
	set nocount on
	
		declare @StatusId int
		
		set @StatusId = dbo.dmlLookup_GetId ('Content Status', 'Approved')
	
	 select r.ReportId
				, r.ReportTypeId
				, r.ReportDate
				, r.Headline
				, r.DirFilename
				, r.StatusId
				, r.ContentTypeId
				, c.TypeLabel
				, c.TypeCode
				, r.HideFromDemo
				, r.ClientName
				, r.cbms_image_id
		 from seSummitReport r
		 join seContentType c on r.ContentTypeId = c.ContentTypeId
		where r.StatusId = @StatusId
		  and r.ReportTypeId = @ReportTypeId
		  and ((r.ClientName = @ClientName) or (r.ClientName is null and @ClientName is null))


 order by r.ReportDate desc

END
GO
GRANT EXECUTE ON  [dbo].[seSummitReport_GetLatest] TO [CBMSApplication]
GO
