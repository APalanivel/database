SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	dbo.Code_Sel_By_Std_Column_Name

DESCRIPTION:    
	Used to get CODE_ID,CODE_VALUE from  CODE and CODESET table  

INPUT PARAMETERS:    
Name				DataType		Default Description    
------------------------------------------------------------    
@STD_COLUMN_NAME    VARCHAR(255) 
@IS_ACTIVE			BIT				1
          
OUTPUT PARAMETERS:    
Name				DataType		Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------  

	EXEC dbo.Code_Sel_By_Std_Column_Name 'Source_Cd'

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
GB						Created  
SKA		17-JUL-09		Modified  as per coding standard
SKA		27-JUL-09		Added order by clause 'c.DISPLAY_SEQ' in the script
SKA		25-AUG-09		Added one more parameter @CODE_VALUE
  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE  PROCEDURE [dbo].[Code_Sel_By_Std_Column_Name]  
	@STD_COLUMN_NAME VARCHAR(255),
	@CODE_VALUE VARCHAR(25) = NULL,
	@IS_ACTIVE BIT = 1
AS  

BEGIN  

SET NOCOUNT ON;  
        
	SELECT CODE_ID,  
		CODE_VALUE  
	FROM dbo.CODE c   
	INNER JOIN dbo.CODESET cs  
		ON c.CODESET_ID = cs.CODESET_ID  
	WHERE c.IS_ACTIVE = @IS_ACTIVE
		AND cs.STD_COLUMN_NAME = @STD_COLUMN_NAME
		AND ((@Code_Value IS NULL) 
			OR (c.CODE_VALUE = @Code_Value))
	ORDER BY c.DISPLAY_SEQ
END
GO
GRANT EXECUTE ON  [dbo].[Code_Sel_By_Std_Column_Name] TO [CBMSApplication]
GO
