SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.Client_Hier_Account_Change_Control_Send_Changes    
    
DESCRIPTION:    
 packages changes between @Last_Version_Id     
 and @Current_Version_Id into an XML document and sends them to the     
 Target services listed in Change Control Target    
    
    
    
INPUT PARAMETERS:    
 Name					DataType					Default					Description    
-------------------------------------------------------------------------------------------    
@Last_Version_Id		BIGINT				The minimum Change tracking version Id to get     
@Current_Version_Id		BIGINT				The most current Change tracking Version Id    
    
OUTPUT PARAMETERS:    
 Name					DataType					Default					Description    
-------------------------------------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------------    
    
    
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------------------------------    
 CPE		Chaitanya Panduga Eshwar
 NR			Narayana Reddy
     
MODIFICATIONS    
    
 Initials	Date			Modification    
-------------------------------------------------------------------------------------------    
 CPE		2013-02-26		Created
 NR			2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.


******/
CREATE PROCEDURE [dbo].[Client_Hier_Account_Change_Control_Send_Changes]
    (
        @Last_Version_Id BIGINT
        , @Current_Version_Id BIGINT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Message XML
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255);

        -- Validate that the changes are continious with last execution    
        IF CHANGE_TRACKING_MIN_VALID_VERSION(OBJECT_ID('Core.Client_Hier_Account')) > @Last_Version_Id
            RAISERROR('Changes for Core.Client_Hier_Account is not concurrent', 16, 1) WITH LOG, NOWAIT;

        -- Force Sys_Change_Operation to Delete if the base record does not exists     
        SET @Message = (   SELECT
                                cng.SYS_CHANGE_OPERATION AS SYS_CHANGE_OPERATION
                                , cha.Account_Type
                                , cng.Client_Hier_Id
                                , cng.Account_Id
                                , cha.Account_Number
                                , cha.Account_Invoice_Source_Cd
                                , cha.Account_Is_Data_Entry_Only
                                , cha.Account_Not_Expected
                                , cha.Account_Not_Managed
                                , cha.Account_Service_level_Cd
                                , cha.Account_UBM_Id
                                , cha.Account_UBM_Name
                                , cha.Account_UBM_Account_Code
                                , cha.Account_Vendor_Id
                                , cha.Account_Vendor_Name
                                , cha.Account_Eligibility_Dt
                                , cha.Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY
                                , cng.Meter_Id
                                , cha.Meter_Number
                                , cha.Meter_Address_ID
                                , cha.Meter_Address_Line_1
                                , cha.Meter_Address_Line_2
                                , cha.Meter_City
                                , cha.Meter_ZipCode
                                , cha.Meter_State_Id
                                , cha.Meter_State_Name
                                , cha.Meter_Country_Id
                                , cha.Meter_Country_Name
                                , cha.Meter_Geo_Lat
                                , cha.Meter_Geo_Long
                                , cha.Commodity_Id
                                , cha.Rate_Id
                                , cha.Rate_Name
                                , cha.Supplier_Account_begin_Dt
                                , cha.Supplier_Account_End_Dt
                                , cha.Supplier_Contract_ID
                                , cha.Supplier_Account_Recalc_Type_Cd
                                , cha.Supplier_Account_Recalc_Type_Dsc
                                , cha.Last_Change_TS
                                , cha.Account_Group_ID
                                , cha.Account_Number_Search
                                , cha.Account_Vendor_Type_ID
                                , cha.Supplier_Meter_Association_Date
                                , cha.Supplier_Meter_Disassociation_Date
                                , cha.Meter_Number_Search
                                , cha.Display_Account_Number
                                , cha.Account_Not_Managed_Dt
                                , cha.Account_Created_Ts
                                , cha.Account_Not_Expected_Dt
                                , cha.Account_Analyst_Mapping_Cd
                                , cha.Account_Is_Broker
                           FROM
                                CHANGETABLE(CHANGES Core.Client_Hier_Account, @Last_Version_Id)cng
                                LEFT JOIN Core.Client_Hier_Account cha
                                    ON cha.Client_Hier_Id = cng.Client_Hier_Id
                                       AND  cha.Account_Id = cng.Account_Id
                                       AND  cha.Meter_Id = cng.Meter_Id
                           WHERE
                                cng.SYS_CHANGE_VERSION <= @Current_Version_Id
                           FOR XML PATH('Client_Hier_Account_Change'), ELEMENTS, ROOT('Client_Hier_Account_Changes'));

        -- MAke sure there is really data to send before we try sending anything    
        IF @Message.exist('/Client_Hier_Account_Changes') = 1
            BEGIN
                -- Cyce through the targets in Change_Control Targt and sent themessage to each    
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'Core.Client_Hier_Account');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        DECLARE @Conversation_Handle UNIQUEIDENTIFIER;
                        BEGIN DIALOG CONVERSATION @Conversation_Handle
                            FROM SERVICE [//Change_Control/Service/CBMS/Change_Control]
                            TO SERVICE @Target_Service
                            ON CONTRACT @Target_Contract;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Change_Control/Message/Client_Hier_Account_Changes]
                            (@Message);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;
            END; -- IF     
    END;

    ;

GO
