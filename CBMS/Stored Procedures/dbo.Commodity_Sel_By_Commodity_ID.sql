SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      

NAME:      
	dbo.Commodity_Sel_By_Commodity_ID
  
DESCRIPTION:    
	Used to select commodity name from COMMODITY table for the selected commodity Id.
  
INPUT PARAMETERS:      
	Name				DataType Default Description      
------------------------------------------------------------      
	@Commodity_ID		INT
            
OUTPUT PARAMETERS:      
Name       DataType Default Description      
------------------------------------------------------------      

USAGE EXAMPLES:      
------------------------------------------------------------    
  
	EXEC dbo.Commodity_Sel_By_Commodity_ID 290
	EXEC dbo.Commodity_Sel_By_Commodity_ID 291
  
AUTHOR INITIALS:      
	Initials	Name      
------------------------------------------------------------      
	PNR			Pandarinath
  
MODIFICATIONS:
	Initials	Date			Modification      
------------------------------------------------------------      
	PNR			10/07/2010		Created

******/  

CREATE PROCEDURE dbo.Commodity_Sel_By_Commodity_ID
(
	@Commodity_ID  INT
)
AS
BEGIN

	SET NOCOUNT ON ;      

	SELECT
		Commodity_Name
		,Is_Alternate_Fuel
		,Is_Sustainable
		,Default_UOM_Entity_Type_Id
		,UOM_Entity_Type
		,Bucket_Aggregation_Cd
	FROM
		dbo.Commodity
	WHERE
		Commodity_Id = @Commodity_ID

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Sel_By_Commodity_ID] TO [CBMSApplication]
GO
