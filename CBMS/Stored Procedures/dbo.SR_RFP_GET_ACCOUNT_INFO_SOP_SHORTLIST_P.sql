
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.SR_RFP_GET_ACCOUNT_INFO_SOP_SHORTLIST_P  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name						DataType				Default			Description  
----------------------------------------------------------------------------  
 @rfp_account_group_id		int                     
 @is_bid_group				bit                     
  
OUTPUT PARAMETERS:  
 Name						DataType				Default			Description  
----------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
----------------------------------------------------------------------------  

EXEC SR_RFP_GET_ACCOUNT_INFO_SOP_SHORTLIST_P 
      10004118
     ,1


  
AUTHOR INITIALS:  
 Initials		Name  
----------------------------------------------------------------------------  
 NR				Narayana Reddy
  
MODIFICATIONS  
  
 Initials	Date			Modification  
----------------------------------------------------------------------------  
			9/21/2010		Modify Quoted Identifier  
 DMR		09/10/2010		Modified for Quoted_Identifier 
 NR			2016-06-01		GCS Phase-5 (GCS-1007) Added meter number,Alternate_Account_Number 
  
  
******/  
  
CREATE  PROCEDURE [dbo].[SR_RFP_GET_ACCOUNT_INFO_SOP_SHORTLIST_P]
      ( 
       @rfp_account_group_id INT
      ,@is_bid_group BIT )
AS 
BEGIN

      SET NOCOUNT ON  
  
      IF @is_bid_group = 1 
            SELECT
                  ch.Client_Name
                 ,ch.Site_Id
                 ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
                 ,cha.Account_Number
                 ,srbg.Group_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Number
                 ,cha.Alternate_Account_Number
            FROM
                  dbo.SR_RFP_ACCOUNT sra
                  INNER JOIN dbo.SR_RFP_BID_GROUP srbg
                        ON sra.SR_RFP_BID_GROUP_ID = srbg.SR_RFP_BID_GROUP_ID
                  INNER JOIN core.Client_Hier_Account cha
                        ON sra.ACCOUNT_ID = cha.Account_Id
                  INNER JOIN core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  INNER JOIN dbo.SR_RFP_SOP_SHORTLIST srss
                        ON srss.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_BID_GROUP_ID
                  CROSS APPLY ( SELECT
                                    CAST(m.METER_NUMBER AS VARCHAR(200)) + ', '
                                FROM
                                    dbo.METER m
                                WHERE
                                    m.ACCOUNT_ID = cha.Account_Id
                                GROUP BY
                                    m.METER_NUMBER
                  FOR
                                XML PATH('') ) mtr ( mtrs )
            WHERE
                  srss.SR_ACCOUNT_GROUP_ID = @rfp_account_group_id
                  AND srss.IS_BID_GROUP = @is_bid_group
                  AND sra.SR_RFP_BID_GROUP_ID = @rfp_account_group_id
                  AND sra.IS_DELETED = 0
            GROUP BY
                  ch.Client_Name
                 ,ch.Site_Id
                 ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                 ,cha.Account_Number
                 ,srbg.Group_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                 ,cha.Alternate_Account_Number
      ELSE 
            SELECT
                  ch.Client_Name
                 ,ch.Site_Id
                 ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
                 ,cha.Account_Number
                 ,NULL AS Group_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Number
                 ,cha.Alternate_Account_Number
            FROM
                  dbo.SR_RFP_ACCOUNT sra
                  INNER JOIN core.Client_Hier_Account cha
                        ON sra.ACCOUNT_ID = cha.Account_Id
                  INNER JOIN core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  INNER JOIN dbo.SR_RFP_SOP_SHORTLIST srss
                        ON srss.SR_ACCOUNT_GROUP_ID = sra.SR_RFP_ACCOUNT_ID
                  CROSS APPLY ( SELECT
                                    CAST(m.METER_NUMBER AS VARCHAR(200)) + ', '
                                FROM
                                    dbo.METER m
                                WHERE
                                    m.ACCOUNT_ID = cha.Account_Id
                                GROUP BY
                                    m.METER_NUMBER
                  FOR
                                XML PATH('') ) mtr ( mtrs )
            WHERE
                  srss.SR_ACCOUNT_GROUP_ID = @rfp_account_group_id
                  AND srss.IS_BID_GROUP = @is_bid_group
                  AND sra.SR_RFP_ACCOUNT_ID = @rfp_account_group_id
                  AND sra.IS_DELETED = 0
                  AND sra.SR_RFP_BID_GROUP_ID IS NULL
            GROUP BY
                  ch.Client_Name
                 ,ch.Site_Id
                 ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
                 ,cha.Account_Number
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                 ,cha.Alternate_Account_Number


      
END 




;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_GET_ACCOUNT_INFO_SOP_SHORTLIST_P] TO [CBMSApplication]
GO
