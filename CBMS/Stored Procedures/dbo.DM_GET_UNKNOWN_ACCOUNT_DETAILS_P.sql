SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DM_GET_UNKNOWN_ACCOUNT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@fromDate      	varchar(20)	          	
	@toDate        	varchar(20)	          	
	@ubmId         	int       	          	
	@closed        	int       	          	
	@closedReason  	varchar(50)	          	
	@DNT           	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DM_GET_UNKNOWN_ACCOUNT_DETAILS_P '1','1','01/01/2010','2/28/2010',0,1,'Mapping Created',1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/24/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        	
	
******/

CREATE PROCEDURE dbo.DM_GET_UNKNOWN_ACCOUNT_DETAILS_P
    @userId			VARCHAR(20),
    @sessionId		VARCHAR(20),
    @fromDate		VARCHAR(20),
    @toDate			VARCHAR(20),
    @ubmId			INT,
    @closed			INT,
    @closedReason	VARCHAR(50),
    @DNT			INT
AS 
BEGIN

	SET NOCOUNT ON ;
	
    DECLARE @selectClause VARCHAR(8000)
    DECLARE @fromClause VARCHAR(8000)
    DECLARE @whereClause VARCHAR(8000)
    DECLARE @groupByClause VARCHAR(8000)
    DECLARE @SQLStatement VARCHAR(8000)

    SELECT  @selectClause = 'ubm.UBM_NAME UBM_NAME,
				--SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3) MONTH_IDENTIFIER,
				CONVERT(Varchar(12), masterLog.END_DATE,101) MONTH_IDENTIFIER,
				--COUNT(distinct exception_detail.CU_EXCEPTION_DETAIL_ID)	NO_OF_EXCEPTIONS
				COUNT(distinct invoice.UBM_INVOICE_ID) NO_OF_EXCEPTIONS
			       '	
    SELECT  @fromClause = '	UBM_INVOICE invoice,
				UBM_BATCH_MASTER_LOG masterLog,
				CU_INVOICE cuInvoice,
				CU_EXCEPTION exception,
				CU_EXCEPTION_DETAIL exception_detail,
				UBM ubm				
			     ' 

    SELECT  @groupByClause = ' --SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3),
				  CONVERT(Varchar(12), masterLog.END_DATE,101), 
				  ubm.UBM_NAME
			        ' 


	
    SELECT  @whereClause = ' exception_detail.EXCEPTION_TYPE_ID = (select CU_EXCEPTION_TYPE_ID from CU_EXCEPTION_TYPE where EXCEPTION_GROUP_TYPE_ID = (SELECT ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE = 712 AND ENTITY_NAME = ''Account Issues'') and EXCEPTION_TYPE = ''Unknown Account'') AND
				exception.CU_EXCEPTION_ID = exception_detail.CU_EXCEPTION_ID AND
				cuInvoice.CU_INVOICE_ID = exception.CU_INVOICE_ID AND
				invoice.CBMS_IMAGE_ID = cuInvoice.CBMS_IMAGE_ID AND
				invoice.UBM_INVOICE_ID = cuInvoice.UBM_INVOICE_ID AND
				invoice.IS_QUARTERLY = 0 AND
				masterLog.UBM_BATCH_MASTER_LOG_ID = invoice.UBM_BATCH_MASTER_LOG_ID AND
		                masterLog.UBM_ID = ubm.UBM_ID 
			      '

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            IF ( @fromDate = @toDate ) 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND CONVERT(Varchar(12), masterLog.END_DATE,101) = '
                            +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''
                END
            ELSE 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND masterLog.END_DATE BETWEEN '
                            +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''+
                            + ' AND '+''''+ CONVERT(VARCHAR(12), @toDate, 101)+ ''''
                END
        END 


    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NULL
              OR @toDate = ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE >= '
                    +''''+ CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        END 


    IF ( @fromDate IS NULL
         OR @fromDate = ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE <= '
                    +''''+ CONVERT(VARCHAR(12), @toDate, 101) + ''''
        END 


    IF @ubmId > 0 
        BEGIN
            SELECT  @whereClause = @whereClause + ' AND ubm.UBM_ID = '
                    + STR(@ubmId) 
        END 

    IF @closed > 0 
        BEGIN
            SELECT  @whereClause = @whereClause
                    + ' AND exception_detail.IS_CLOSED = 1 '
        END 


    IF @closedReason IS NOT NULL
        AND @closedReason <> '' 
        BEGIN
            SELECT  @whereClause = @whereClause
                    + ' AND exception_detail.CLOSED_REASON_TYPE_ID = (select entity_id from entity where entity_type = 713 and entity_name = '
                    +''''+ @closedReason +''''+ ')'
        END 
        
    IF @DNT > 0 
        BEGIN
            SELECT  @whereClause = @whereClause + ' AND cuInvoice.IS_DNT = 1'
        END 


    SELECT  @SQLStatement = 'SELECT ' + @selectClause + ' FROM ' + @fromClause
            + ' WHERE ' + @whereClause + ' GROUP BY ' + @groupByClause

   
   	PRINT @SQLStatement
   	
    EXEC ( @SQLStatement )
END
GO
GRANT EXECUTE ON  [dbo].[DM_GET_UNKNOWN_ACCOUNT_DETAILS_P] TO [CBMSApplication]
GO
