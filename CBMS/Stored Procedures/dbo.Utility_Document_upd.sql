SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Document_upd

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Utility_Document_Id	INT
	@Utility_Document_Type_Id	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Utility_Document_upd 20,100314
	ROLLBACK TRAN

	SELECT * FROM 	Utility_Document
		
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
      
CREATE PROC dbo.Utility_Document_upd
      (
       @Utility_Document_Id INT
      ,@Utility_Document_Type_Id INT )
AS 
BEGIN      
       
      SET nocount ON ;      
      
      UPDATE
            Utility_Document
      SET   
            Utility_Document_Type_Id = @Utility_Document_Type_Id
      WHERE
            Utility_Document_Id = @Utility_Document_Id  
END   

GO
GRANT EXECUTE ON  [dbo].[Utility_Document_upd] TO [CBMSApplication]
GO
