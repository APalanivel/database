SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                
                      
NAME: [DBO].[Bucket_Account_Interval_Dtl_Changes_Sel_For_Data_Transfer]                      
                      
DESCRIPTION:                       
      Select the changed Account Level data from Bucket_Account_Interval_Dtl table(using change tracking) to transfer            
      it to HUB                     
                            
INPUT PARAMETERS:                                
NAME		DATATYPE DEFAULT  DESCRIPTION                                
------------------------------------------------------------                                
@Min_CT_Ver BIGINT                
@Max_CT_Ver BIGINT                   
                                      
OUTPUT PARAMETERS:                                
NAME   DATATYPE DEFAULT  DESCRIPTION                         
                             
------------------------------------------------------------                                
USAGE EXAMPLES:                                
------------------------------------------------------------

 DECLARE
      @Min_CT_Ver BIGINT
     ,@Max_CT_Ver BIGINT    

 SET @Min_CT_Ver = CHANGE_TRACKING_MIN_VALID_VERSION(object_id('dbo.Bucket_Account_Interval_Dtl'));  
 SELECT
      @Max_CT_Ver = CHANGE_TRACKING_CURRENT_VERSION()

 EXEC dbo.Bucket_Account_Interval_Dtl_Changes_Sel_For_Data_Transfer
      @Min_CT_Ver
     ,@Max_CT_Ver
            
AUTHOR INITIALS:                                
INITIALS	NAME                                
------------------------------------------------------------                                
HG			Harihara Suthan G              
RKV			Ravi Kumar Vegesna            
                    
MODIFICATIONS                      
INITIALS	DATE(YYYY-MM-DD)	MODIFICATION                      
------------------------------------------------------------                      
RKV			2013-04-24			Created
RKV         2018-09-26			MAINT-7780 Added NOT NULL filters for I and U Sys_Change_Operation
*/
CREATE PROCEDURE [dbo].[Bucket_Account_Interval_Dtl_Changes_Sel_For_Data_Transfer]
     (
         @Min_CT_Ver BIGINT
         , @Max_CT_Ver BIGINT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        /*****************************************************************************************
   This is here entirely as a work around for SSIS and temporary tables.  This statement
   allows SSIS to be able to return the meta data about the stored procedure to map columns.
*****************************************************************************************/
        IF 1 = 2
            BEGIN

                SELECT
                    CAST(NULL AS INT) AS Client_Hier_Id
                    , CAST(NULL AS INT) AS Account_Id
                    , CAST(NULL AS INT) AS Bucket_Master_Id
                    , CAST(NULL AS INT) AS Data_Source_Cd
                    , CAST(NULL AS DATE) AS Service_Start_Dt
                    , CAST(NULL AS DATE) AS Service_End_Dt
                    , CAST(NULL AS DECIMAL(28, 10)) AS Bucket_Daily_Avg_Value
                    , CAST(NULL AS INT) AS Uom_Type_Id
                    , CAST(NULL AS INT) AS Currency_Unit_Id
                    , CAST(NULL AS INT) AS Created_User_Id
                    , CAST(NULL AS INT) AS Updated_User_Id
                    , CAST(NULL AS DATETIME) AS Created_Ts
                    , CAST(NULL AS DATETIME) AS Last_Changed_Ts
                    , CAST(NULL AS CHAR(1)) AS Sys_Change_Operation;


            END;


        DECLARE
            @CBMS_Data_Source_Cd INT
            , @Invoice_Data_Source_Cd INT
            , @DE_Data_Source_Cd INT;

        SELECT
            @Invoice_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'Invoice' THEN cd.Code_Id
                                          END)
            , @CBMS_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.Code_Id
                                         END)
            , @DE_Data_Source_Cd = MAX(CASE WHEN cd.Code_Value = 'DE' THEN cd.Code_Id
                                       END)
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Data_Source_Cd';



        CREATE TABLE #Bucket_Account_Interval_Dtl_Ct_Data
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Bucket_Master_Id INT
                 , Data_Source_Cd INT
                 , Service_Start_Dt DATE
                 , Service_End_Dt DATE
                 , Sys_Change_Operation NCHAR(1)
             );

        INSERT INTO #Bucket_Account_Interval_Dtl_Ct_Data
             (
                 Client_Hier_Id
                 , Account_Id
                 , Bucket_Master_Id
                 , Data_Source_Cd
                 , Service_Start_Dt
                 , Service_End_Dt
                 , Sys_Change_Operation
             )
        SELECT
            ct.Client_Hier_Id
            , ct.Account_Id
            , ct.Bucket_Master_Id
            , ct.Data_Source_Cd
            , ct.Service_Start_Dt
            , ct.Service_End_Dt
            , CAST(ct.SYS_CHANGE_OPERATION AS CHAR(1))
        FROM
            CHANGETABLE(CHANGES dbo.Bucket_Account_Interval_Dtl, @Min_CT_Ver) ct
        WHERE
            ct.SYS_CHANGE_VERSION <= @Max_CT_Ver;


        SELECT
            ct_data.Client_Hier_Id
            , ct_data.Account_Id
            , ct_data.Bucket_Master_Id
            , ct_data.Data_Source_Cd
            , ct_data.Service_Start_Dt
            , ct_data.Service_End_Dt
            , baid.Bucket_Daily_Avg_Value
            , baid.Uom_Type_Id
            , baid.Currency_Unit_Id
            , baid.Created_User_Id
            , baid.Updated_User_Id
            , baid.Created_Ts
            , baid.Last_Changed_Ts
            , ct_data.Sys_Change_Operation
        FROM
            #Bucket_Account_Interval_Dtl_Ct_Data ct_data
            LEFT OUTER JOIN dbo.Bucket_Account_Interval_Dtl baid
                ON baid.Client_Hier_Id = ct_data.Client_Hier_Id
                   AND  baid.Account_Id = ct_data.Account_Id
                   AND  baid.Bucket_Master_Id = ct_data.Bucket_Master_Id
                   AND  baid.Data_Source_Cd = ct_data.Data_Source_Cd
                   AND  baid.Service_End_Dt = ct_data.Service_End_Dt
                   AND  baid.Service_Start_Dt = ct_data.Service_Start_Dt
        WHERE
            (   (   ct_data.Sys_Change_Operation IN ( 'I', 'U' )
                    AND ct_data.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @Invoice_Data_Source_Cd )
                    AND baid.Bucket_Daily_Avg_Value IS NOT NULL
                    AND baid.Created_User_Id IS NOT NULL
                    AND baid.Updated_User_Id IS NOT NULL
                    AND baid.Last_Changed_Ts IS NOT NULL
                    AND baid.Created_Ts IS NOT NULL
                    AND baid.Data_Source_Cd IS NOT NULL) -- Records inserted/updated by CBMS/Invoice source
                OR  (   ct_data.Sys_Change_Operation = 'D' -- All the deleted invoice sources 
                        AND ct_data.Data_Source_Cd = @Invoice_Data_Source_Cd)
                OR  (   ct_data.Sys_Change_Operation = 'D' -- Deleted manual records only if no records present BAID
                        AND ct_data.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @DE_Data_Source_Cd )
                        AND NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Bucket_Account_Interval_Dtl baid1
                                           WHERE
                                                baid1.Client_Hier_Id = ct_data.Client_Hier_Id
                                                AND baid1.Account_Id = ct_data.Account_Id
                                                AND baid1.Bucket_Master_Id = ct_data.Bucket_Master_Id
                                                AND baid1.Data_Source_Cd IN ( @CBMS_Data_Source_Cd, @DE_Data_Source_Cd )
                                                AND baid1.Service_Start_Dt = ct_data.Service_Start_Dt
                                                AND baid1.Service_End_Dt = ct_data.Service_End_Dt)));


        DROP TABLE #Bucket_Account_Interval_Dtl_Ct_Data;

    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Bucket_Account_Interval_Dtl_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
