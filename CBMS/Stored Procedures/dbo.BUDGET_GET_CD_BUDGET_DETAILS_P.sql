SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
	dbo.BUDGET_GET_CD_BUDGET_DETAILS_P
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
	Name			DataType	Default		Description        
------------------------------------------------------------        
	@user_id		VARCHAR(10)
    @session_id		VARCHAR(20)
    @budgetId		INT 
        
OUTPUT PARAMETERS:        
	Name   DataType  Default Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:        
------------------------------------------------------------        
	
	EXEC dbo.BUDGET_GET_CD_BUDGET_DETAILS_P -1,-1,487


        
AUTHOR INITIALS:        
	Initials	Name        
------------------------------------------------------------        
	RR			Raghu Reddy

MODIFICATIONS                
	Initials	Date		Modification        
------------------------------------------------------------        
	RR			2018-03-19	Added description header
							SE2017-547 State details are returning only for the sites in USA & CANADA, other state sites are grouped as one 
							with the name as "Other" and setting up only one common conversion factor, now it is changed to list down states in 
							other countries also and also have its own conversion factor.
							Though the conversion factor captured at state level but saving it to respective account(dbo.BUDGET_ACCOUNT), so no
							need to change anything for existing budgets.

******/  
CREATE	PROCEDURE [dbo].[BUDGET_GET_CD_BUDGET_DETAILS_P]
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
     ,@budgetId INT
AS 
BEGIN
      SET NOCOUNT ON;	

      SELECT
            site.state_id
           ,site.state_name
           ,budget_acc.cd_create_multiplier
           ,budget.budget_start_month
           ,budget.budget_start_year
           ,budgetCommodity.entity_name AS commodity
      FROM
            dbo.vwSiteName site
            INNER JOIN dbo.ACCOUNT acc
                  ON site.site_id = acc.site_id
            INNER JOIN dbo.BUDGET_ACCOUNT budget_acc
                  ON acc.account_id = budget_acc.account_id
            INNER JOIN dbo.BUDGET_TARIFF_ACCOUNT_VW tariff
                  ON tariff.account_id = budget_acc.account_id
            INNER JOIN dbo.COUNTRY coun
                  ON coun.country_id = site.country_id
            INNER JOIN dbo.STATE state
                  ON site.state_id = state.state_id
            INNER JOIN dbo.BUDGET budget
                  ON budget.budget_id = budget_acc.budget_id
            INNER JOIN dbo.ENTITY budgetCommodity
                  ON budgetCommodity.entity_id = budget.commodity_type_id
      WHERE
            budget_acc.budget_id = @budgetId
            AND budgetCommodity.entity_type = 157
            AND acc.service_level_type_id IN ( 861, 1024 )
            AND budget_acc.is_deleted = 0
      GROUP BY
            site.state_id
           ,site.state_name
           ,budget_acc.cd_create_multiplier
           ,budget.budget_start_month
           ,budget.budget_start_year
           ,budgetCommodity.entity_name
     

  
END














GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CD_BUDGET_DETAILS_P] TO [CBMSApplication]
GO
