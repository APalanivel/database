SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       
NAME:  dbo.Variance_Consumption_Level_By_Account_Id_SEL      
     
DESCRIPTION:  
	
	Used to select variance consumption level for an account     
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
    @Account_id			int  
            
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:       
    
    EXEC dbo.Variance_Consumption_Level_By_Account_Id_SEL 139062
    EXEC dbo.Variance_Consumption_Level_By_Account_Id_SEL 158090
	EXEC dbo.Variance_Consumption_Level_By_Account_Id_SEL 54752

------------------------------------------------------------      
AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
NK			Nageswara Rao Kosuri
HG			Harihara Suthan G
    
Initials	Date		Modification      
------------------------------------------------------------      
NK			10/31/2009  created    
HG			02/01/2010	Added Consumpation_Level_id and Consumption_Level_Desc column for other fuel requirement.

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Variance_Consumption_Level_By_Account_Id_SEL
	@Account_Id INT
AS

BEGIN

	SET NOCOUNT ON;

	SELECT
		Commodity_Name + SPACE(3) + vcl.Consumption_Level_Desc as Consumption_Level
		,vcl.Variance_Consumption_Level_Id
		,vcl.Consumption_Level_Desc
	FROM
		dbo.Account_Variance_Consumption_Level avcl
		INNER JOIN dbo.Variance_Consumption_Level vcl
			ON vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
		INNER JOIN dbo.Commodity co
			ON co.Commodity_Id = vcl.Commodity_id
	WHERE
		avcl.account_id = @Account_Id

END
GO
GRANT EXECUTE ON  [dbo].[Variance_Consumption_Level_By_Account_Id_SEL] TO [CBMSApplication]
GO
