SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Get_User_Client_Role 
   
DESCRIPTION: This Procedure will return the user previous roleid

   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
@Userid			INT
@ClientId		INT

   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------  
Security_Role_Id	INT

  
  USAGE EXAMPLES:    
--------------------------------------------------------------------    

  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
 KVK K VINAY KUMAR
   
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------  
******************************************************************************************************/
CREATE PROCEDURE [dbo].[Get_User_Client_Role]  
(
		@Userid			INT  
  ,		@ClientId		INT  
)  
AS  
BEGIN  
 
	SET NOCOUNT ON;
	
		SELECT sr.Security_Role_Id  
		  FROM User_Security_Role usr  
         INNER JOIN Security_Role sr
            ON usr.Security_Role_Id = sr.Security_Role_Id  
		 WHERE usr.User_Info_Id = @Userid   
           AND Client_Id = @ClientId

		 
END

GO
GRANT EXECUTE ON  [dbo].[Get_User_Client_Role] TO [CBMSApplication]
GO
