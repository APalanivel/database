SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
  
NAME:  
     dbo.DMO_Supplier_Account_Conversion_Insert_New_Accounts    
         
DESCRIPTION:

INPUT PARAMETERS:            
	Name            DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        

OUTPUT PARAMETERS:            
	Name            DataType         Default       Description          
---------------------------------------------------------------------------------------------------------------        

 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------                              

	BEGIN TRAN  
	EXEC dbo.DMO_Supplier_Account_Conversion_Insert_New_Accounts  303
	ROLLBACK TRAN     

AUTHOR INITIALS:          
 Initials   Name          
---------------------------------------------------------------------------------------------------------------                        
 AKR        Ashok Kumar Raju
 RR			Raghu Reddy          

MODIFICATIONS:        
	Initials   Date				Modification        
---------------------------------------------------------------------------------------------------------------        
	RR			2015-11-30		SE2017-32 Created
	NR			2019-09-17		Add Contract -  Added New config table.
	RKV  2019-12-30	D20-1762  Added New table Account_Ubm_Account_Code_Map to insert the ubm details and removed from account table. 

*********/
CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Conversion_Insert_New_Accounts]
     (
         @Data_Conversion_Batch_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Min_Data_Conversion_Batch_Dtl_Id INT
            , @Max_Data_Conversion_Batch_Dtl_Id INT
            , @Client_Id INT
            , @Site_Id INT
            , @Commodity_Id INT
            , @DMO_Supp_Start_Dt DATE
            , @DMO_Supp_End_Dt DATE
            , @Meter_Id INT
            , @Meter_id_Status BIT = 0
            , @No_Recalc_Type_Id INT
            , @New_Account_ID INT
            , @Ubm_Id INT
            , @Ubm_Account_Id NVARCHAR(255)
            , @Variance_Consumption_Level_Id INT
            , @CommodityId INT
            , @Utility_Account_Type_Id INT
            , @Supplier_Account_Type_Id INT
            , @Min_Id INT
            , @Max_Id INT
            , @System_User_Id INT
            , @Current_DMO_Account_ID INT
            , @Current_DMO_Acc_Upd_End_Dt DATE
            , @Account_Config_Supplier_Account_Begin_Dt DATE
            , @Account_Config_Supplier_Account_End_Dt DATE
            , @Out_Supplier_Account_Config_Id INT;


        DECLARE @Tbl_Config AS TABLE
              (
                  Client_Hier_DMO_Config_Id INT
                  , Client_Hier_Id INT
                  , Hier_level_Cd INT
                  , Account_Id INT
                  , Account_DMO_Config_Id INT
                  , Code_Dsc VARCHAR(255)
                  , Commodity_Id INT
                  , Commodity_Name VARCHAR(50)
                  , DMO_Start_Dt VARCHAR(10)
                  , DMO_End_Dt VARCHAR(10)
                  , Updated_User VARCHAR(100)
                  , Last_Change_Ts DATETIME
              );

        DECLARE @Tbl_MtrDts AS TABLE
              (
                  Mtr_Start_Dt DATE
                  , Mtr_End_Dt DATE
              );

        DECLARE @Tbl_MeterDtaes AS TABLE
              (
                  Meter_Id INT
                  , Mtr_Start_Dt DATE
                  , Mtr_End_Dt DATE
              );

        DECLARE
            @Max_Mtr_Start_Dt DATE
            , @Min_Mtr_End_Dt DATE
            , @TestStr VARCHAR(MAX);

        DECLARE @Tbl_Unq_DMO AS TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , Account_Number VARCHAR(50)
                  , Vendor_Id INT
                  , DMO_Start_Dt DATE
                  , DMO_End_Dt DATE
                  , Setup_Invoice_Collection VARCHAR(50)
                  , Account_Id INT
                  , Supplier_Account_Config_Id INT
              );

        DECLARE @Tbl_Unq_DMO_Mtrs AS TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , Account_Number VARCHAR(50)
                  , Vendor_Id INT
                  , DMO_Start_Dt DATE
                  , DMO_End_Dt DATE
                  , Meter_Id INT
              );


        CREATE TABLE #New_Account_Id
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Setup_Invoice_Collection VARCHAR(50)
             );


        DECLARE
            @METER_ASSOCIATION_DT DATE
            , @METER_DISASSOCIATION_DT DATE
            , @Missing_IC_Data_Exception_Type_Cd INT
            , @Exception_Status_Cd_New INT;


        SELECT
            @System_User_Id = USER_INFO_ID
        FROM
            dbo.USER_INFO
        WHERE
            USERNAME = 'Conversion';
        SELECT
            @Missing_IC_Data_Exception_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cs.Codeset_Id = cd.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cd.Code_Value = 'Missing IC Data';

        SELECT
            @Exception_Status_Cd_New = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cs.Codeset_Id = cd.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';

        SELECT
            @Utility_Account_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Utility';

        SELECT
            @Supplier_Account_Type_Id = ENTITY_ID
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_DESCRIPTION = 'Account Type'
            AND ENTITY_NAME = 'Supplier';

        SELECT
            @No_Recalc_Type_Id = cd.Code_Id
        FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = 'No Recalculation'
            AND cs.Codeset_Name = 'Recalculation_Type';

        SELECT
            @Min_Data_Conversion_Batch_Dtl_Id = MIN(dcbd.Data_Conversion_Batch_Dtl_Id)
            , @Max_Data_Conversion_Batch_Dtl_Id = MAX(dcbd.Data_Conversion_Batch_Dtl_Id)
        FROM
            ETL.Data_Conversion_Batch_Dtl dcbd
        WHERE
            dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id;

        /* Please execute the below Queries in One Go*/

        WHILE (@Min_Data_Conversion_Batch_Dtl_Id <= @Max_Data_Conversion_Batch_Dtl_Id)
            BEGIN

                SELECT
                    @Client_Id = s.Client_ID
                    , @Site_Id = s.SITE_ID
                    , @Commodity_Id = dcbd.Commodity
                    , @DMO_Supp_Start_Dt = dcbd.DMO_Supplier_Account_Start_Date
                    , @DMO_Supp_End_Dt = dcbd.DMO_Supplier_Account_End_Date
                    , @Meter_Id = mtr.METER_ID
                    , @Variance_Consumption_Level_Id = dcbd.[Consumption Level ID]
                    , @CommodityId = dcbd.Commodity
                FROM
                    ETL.Data_Conversion_Batch_Dtl dcbd
                    INNER JOIN dbo.ACCOUNT acc
                        ON dcbd.Account_Id = acc.ACCOUNT_ID
                    INNER JOIN dbo.METER mtr
                        ON acc.ACCOUNT_ID = mtr.ACCOUNT_ID
                           AND  dcbd.METER = mtr.METER_NUMBER
                    INNER JOIN dbo.SITE s
                        ON acc.SITE_ID = s.SITE_ID
                WHERE
                    dcbd.Data_Conversion_Batch_Dtl_Id = @Min_Data_Conversion_Batch_Dtl_Id
                    AND dcbd.Is_Data_Validation_Passed = 1
                    AND acc.ACCOUNT_TYPE_ID = @Utility_Account_Type_Id;

                SELECT  @Meter_id_Status = 0;

                INSERT INTO @Tbl_Config
                     (
                         Client_Hier_DMO_Config_Id
                         , Client_Hier_Id
                         , Hier_level_Cd
                         , Account_Id
                         , Account_DMO_Config_Id
                         , Code_Dsc
                         , Commodity_Id
                         , Commodity_Name
                         , DMO_Start_Dt
                         , DMO_End_Dt
                         , Updated_User
                         , Last_Change_Ts
                     )
                EXEC dbo.DMO_Supplier_Account_Dates_Validation
                    @Client_Id = @Client_Id
                    , @Site_Id = @Site_Id
                    , @Commodity_Id = @Commodity_Id
                    , @DMO_Supp_Start_Dt = @DMO_Supp_Start_Dt
                    , @DMO_Supp_End_Dt = @DMO_Supp_End_Dt;

                UPDATE
                    dcbd
                SET
                    dcbd.Is_Data_Validation_Passed = 0
                    , dcbd.Error_Msg = 'DMO configuration does not exists for the given dates'
                FROM
                    ETL.Data_Conversion_Batch_Dtl dcbd
                WHERE
                    dcbd.Data_Conversion_Batch_Dtl_Id = @Min_Data_Conversion_Batch_Dtl_Id
                    AND dcbd.Is_Data_Validation_Passed = 1
                    AND NOT EXISTS (SELECT  1 FROM  @Tbl_Config);

                DELETE  FROM @Tbl_MtrDts;

                INSERT INTO @Tbl_MtrDts
                     (
                         Mtr_Start_Dt
                         , Mtr_End_Dt
                     )
                SELECT
                    CAST(DMO_Start_Dt AS DATE)
                    , CAST(DMO_End_Dt AS DATE)
                FROM
                    @Tbl_Config;

                INSERT INTO @Tbl_MtrDts
                     (
                         Mtr_Start_Dt
                         , Mtr_End_Dt
                     )
                SELECT  CAST(@DMO_Supp_Start_Dt AS DATE), CAST(@DMO_Supp_End_Dt AS DATE);

                SELECT
                    @Max_Mtr_Start_Dt = MAX(Mtr_Start_Dt)
                    , @Min_Mtr_End_Dt = MIN(Mtr_End_Dt)
                FROM
                    @Tbl_MtrDts;

                DELETE  FROM @Tbl_MeterDtaes;

                INSERT INTO @Tbl_MeterDtaes
                     (
                         Meter_Id
                         , Mtr_Start_Dt
                         , Mtr_End_Dt
                     )
                SELECT  @Meter_Id, @Max_Mtr_Start_Dt, @Min_Mtr_End_Dt;


                SELECT
                    @Meter_id_Status = 1
                FROM
                    dbo.CONTRACT con
                    INNER JOIN dbo.ENTITY typ
                        ON con.CONTRACT_TYPE_ID = typ.ENTITY_ID
                    INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                        ON con.CONTRACT_ID = map.Contract_ID
                WHERE
                    map.METER_ID = @Meter_Id
                    AND typ.ENTITY_NAME = 'Supplier'
                    AND typ.ENTITY_DESCRIPTION = 'Contract Type'
                    AND (   (   (   @Max_Mtr_Start_Dt > map.METER_ASSOCIATION_DATE
                                    AND @Max_Mtr_Start_Dt < map.METER_DISASSOCIATION_DATE)
                                OR  (   ISNULL(@Min_Mtr_End_Dt, '9999/12/31') > map.METER_ASSOCIATION_DATE
                                        AND ISNULL(@Min_Mtr_End_Dt, '9999/12/31') < map.METER_DISASSOCIATION_DATE))
                            OR  (   (   map.METER_ASSOCIATION_DATE > @Max_Mtr_Start_Dt
                                        AND map.METER_ASSOCIATION_DATE < ISNULL(@Min_Mtr_End_Dt, '9999/12/31'))
                                    OR  (   map.METER_DISASSOCIATION_DATE > @Max_Mtr_Start_Dt
                                            AND map.METER_DISASSOCIATION_DATE < ISNULL(@Min_Mtr_End_Dt, '9999/12/31')))
                            OR  (   map.METER_ASSOCIATION_DATE = @Max_Mtr_Start_Dt
                                    AND map.METER_DISASSOCIATION_DATE = ISNULL(@Min_Mtr_End_Dt, '9999/12/31')));

                SELECT
                    @Meter_id_Status = 1
                FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP map
                WHERE
                    map.METER_ID = @Meter_Id
                    AND map.Contract_ID = -1
                    AND (   (   @Min_Mtr_End_Dt IS NOT NULL
                                AND map.METER_DISASSOCIATION_DATE IS NOT NULL
                                AND (   (   (   @Max_Mtr_Start_Dt > map.METER_ASSOCIATION_DATE
                                                AND @Max_Mtr_Start_Dt < map.METER_DISASSOCIATION_DATE)
                                            OR  (   @Min_Mtr_End_Dt > map.METER_ASSOCIATION_DATE
                                                    AND @Min_Mtr_End_Dt < map.METER_DISASSOCIATION_DATE))
                                        OR  (   (   map.METER_ASSOCIATION_DATE > @Max_Mtr_Start_Dt
                                                    AND map.METER_ASSOCIATION_DATE < @Min_Mtr_End_Dt)
                                                OR  (   map.METER_DISASSOCIATION_DATE > @Max_Mtr_Start_Dt
                                                        AND map.METER_DISASSOCIATION_DATE < @Min_Mtr_End_Dt))
                                        OR  (   map.METER_ASSOCIATION_DATE = @Max_Mtr_Start_Dt
                                                AND map.METER_DISASSOCIATION_DATE = @Min_Mtr_End_Dt)))
                            OR  (   @Min_Mtr_End_Dt IS NOT NULL
                                    AND map.METER_DISASSOCIATION_DATE IS NULL
                                    AND (   map.METER_ASSOCIATION_DATE > @Max_Mtr_Start_Dt
                                            AND map.METER_ASSOCIATION_DATE < @Min_Mtr_End_Dt))
                            OR  (   @Min_Mtr_End_Dt IS NULL
                                    AND map.METER_DISASSOCIATION_DATE IS NOT NULL
                                    AND (   @Max_Mtr_Start_Dt > map.METER_ASSOCIATION_DATE
                                            AND @Max_Mtr_Start_Dt < map.METER_DISASSOCIATION_DATE))
                            OR  (   @Min_Mtr_End_Dt IS NULL
                                    AND map.METER_DISASSOCIATION_DATE IS NULL
                                    AND (@Max_Mtr_Start_Dt < map.METER_ASSOCIATION_DATE)));

                UPDATE
                    m2
                SET
                    m2.Is_Data_Validation_Passed = 0
                    , m2.Error_Msg = m1.METER + ' Meter dates are overlapping with the other account/contract'
                FROM
                    ETL.Data_Conversion_Batch_Dtl m1
                    INNER JOIN ETL.Data_Conversion_Batch_Dtl m2
                        ON m1.Data_Conversion_Batch_Id = m2.Data_Conversion_Batch_Id
                           AND  m1.Account_Id = m2.Account_Id
                           AND  m1.[Vendor ID] = m2.[Vendor ID]
                           AND  m1.DMO_Supplier_Account_Start_Date = m2.DMO_Supplier_Account_Start_Date
                           AND  ISNULL(m1.DMO_Supplier_Account_End_Date, '9999/12/31') = ISNULL(
                                                                                             m2.DMO_Supplier_Account_End_Date
                                                                                             , '9999/12/31')
                WHERE
                    m1.Data_Conversion_Batch_Dtl_Id = @Min_Data_Conversion_Batch_Dtl_Id
                    AND m1.Is_Data_Validation_Passed = 1
                    AND @Meter_id_Status = 1;

                DELETE  FROM @Tbl_Config;
                SELECT  @Meter_id_Status = 0;
                SET @Min_Data_Conversion_Batch_Dtl_Id = @Min_Data_Conversion_Batch_Dtl_Id + 1;
            END;

        INSERT INTO @Tbl_Unq_DMO
             (
                 Account_Number
                 , Vendor_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
                 , Setup_Invoice_Collection
             )
        SELECT
            dcbd.[ACCOUNT NUMBER]
            , dcbd.[Vendor ID]
            , dcbd.DMO_Supplier_Account_Start_Date
            , dcbd.DMO_Supplier_Account_End_Date
            , MAX(dcbd.Setup_Invoice_Collection)
        FROM
            ETL.Data_Conversion_Batch_Dtl dcbd
        WHERE
            dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id
            AND dcbd.Is_Data_Validation_Passed = 1
        GROUP BY
            dcbd.[ACCOUNT NUMBER]
            , dcbd.[Vendor ID]
            , dcbd.DMO_Supplier_Account_Start_Date
            , dcbd.DMO_Supplier_Account_End_Date;

        INSERT INTO @Tbl_Unq_DMO_Mtrs
             (
                 Account_Number
                 , Vendor_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
                 , Meter_Id
             )
        SELECT
            dcbd.[ACCOUNT NUMBER]
            , dcbd.[Vendor ID]
            , dcbd.DMO_Supplier_Account_Start_Date
            , dcbd.DMO_Supplier_Account_End_Date
            , mtr.METER_ID
        FROM
            ETL.Data_Conversion_Batch_Dtl dcbd
            INNER JOIN dbo.ACCOUNT acc
                ON dcbd.Account_Id = acc.ACCOUNT_ID
            INNER JOIN dbo.METER mtr
                ON acc.ACCOUNT_ID = mtr.ACCOUNT_ID
                   AND  dcbd.METER = mtr.METER_NUMBER
        WHERE
            dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id
            AND dcbd.Is_Data_Validation_Passed = 1
        GROUP BY
            dcbd.[ACCOUNT NUMBER]
            , dcbd.[Vendor ID]
            , dcbd.DMO_Supplier_Account_Start_Date
            , dcbd.DMO_Supplier_Account_End_Date
            , mtr.METER_ID;

        SELECT  @Max_Id = MAX(Id)FROM   @Tbl_Unq_DMO;

        SELECT  @Min_Id = 1;
        WHILE (@Min_Id <= @Max_Id)
            BEGIN

                INSERT INTO dbo.ACCOUNT
                     (
                         VENDOR_ID
                         , ACCOUNT_NUMBER
                         , ACCOUNT_TYPE_ID
                         , Supplier_Account_Begin_Dt
                         , Supplier_Account_End_Dt
                         , SERVICE_LEVEL_TYPE_ID
                         , INVOICE_SOURCE_TYPE_ID
                         , Supplier_Account_Recalc_Type_Cd
                         , Is_Data_Entry_Only
                         , Alternate_Account_Number
                     )
                SELECT
                    dcbd.[Vendor ID]
                    , dcbd.[ACCOUNT NUMBER]
                    , @Supplier_Account_Type_Id
                    , dcbd.DMO_Supplier_Account_Start_Date
                    , dcbd.DMO_Supplier_Account_End_Date
                    , sl.ENTITY_ID
                    , invsrc.ENTITY_ID
                    , @No_Recalc_Type_Id
                    , CASE WHEN dcbd.DEO = 'Yes' THEN 1
                          WHEN dcbd.DEO = 'No' THEN 0
                      END
                    , dcbd.Alternate_Account_Number
                FROM
                    @Tbl_Unq_DMO tud
                    INNER JOIN ETL.Data_Conversion_Batch_Dtl dcbd
                        ON tud.Account_Number = dcbd.[ACCOUNT NUMBER]
                           AND  tud.Vendor_Id = dcbd.[Vendor ID]
                           AND  tud.DMO_Start_Dt = dcbd.DMO_Supplier_Account_Start_Date
                           AND  ISNULL(tud.DMO_End_Dt, '9999/12/31') = ISNULL(
                                                                           dcbd.DMO_Supplier_Account_End_Date
                                                                           , '9999/12/31')
                    LEFT JOIN dbo.ENTITY sl
                        ON sl.ENTITY_NAME = dcbd.[Service LEVEL]
                           AND  sl.ENTITY_TYPE = 708
                    LEFT OUTER JOIN dbo.ENTITY invsrc
                        ON invsrc.ENTITY_NAME = dcbd.Invoice_Source
                           AND  invsrc.ENTITY_TYPE = 110
                WHERE
                    tud.Id = @Min_Id
                    AND dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id
                    AND dcbd.Is_Data_Validation_Passed = 1
                GROUP BY
                    dcbd.[Vendor ID]
                    , dcbd.[ACCOUNT NUMBER]
                    , dcbd.[UBM Account ID]
                    , dcbd.DMO_Supplier_Account_Start_Date
                    , dcbd.DMO_Supplier_Account_End_Date
                    , sl.ENTITY_ID
                    , invsrc.ENTITY_ID
                    , CASE WHEN dcbd.DEO = 'Yes' THEN 1
                          WHEN dcbd.DEO = 'No' THEN 0
                      END
                    , dcbd.Alternate_Account_Number;

                SELECT  @New_Account_ID = SCOPE_IDENTITY();
                SELECT
                    @Ubm_Account_Id = dcbd.[UBM Account ID]
                    , @Ubm_Id = u.UBM_ID
                FROM
                    @Tbl_Unq_DMO tud
                    INNER JOIN ETL.Data_Conversion_Batch_Dtl dcbd
                        ON tud.Account_Number = dcbd.[ACCOUNT NUMBER]
                           AND  tud.Vendor_Id = dcbd.[Vendor ID]
                           AND  tud.DMO_Start_Dt = dcbd.DMO_Supplier_Account_Start_Date
                           AND  ISNULL(tud.DMO_End_Dt, '9999/12/31') = ISNULL(
                                                                           dcbd.DMO_Supplier_Account_End_Date
                                                                           , '9999/12/31')
                    LEFT JOIN dbo.ENTITY sl
                        ON sl.ENTITY_NAME = dcbd.[Service LEVEL]
                           AND  sl.ENTITY_TYPE = 708
                    LEFT OUTER JOIN dbo.ENTITY invsrc
                        ON invsrc.ENTITY_NAME = dcbd.Invoice_Source
                           AND  invsrc.ENTITY_TYPE = 110
                    LEFT OUTER JOIN dbo.UBM u
                        ON u.UBM_NAME = dcbd.UBM
                WHERE
                    tud.Id = @Min_Id
                    AND dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id
                    AND dcbd.Is_Data_Validation_Passed = 1
                    AND dcbd.UBM IS NOT NULL
                GROUP BY
                    dcbd.[Vendor ID]
                    , dcbd.[ACCOUNT NUMBER]
                    , dcbd.[UBM Account ID]
                    , u.UBM_ID
                    , dcbd.DMO_Supplier_Account_Start_Date
                    , dcbd.DMO_Supplier_Account_End_Date
                    , sl.ENTITY_ID
                    , invsrc.ENTITY_ID
                    , CASE WHEN dcbd.DEO = 'Yes' THEN 1
                          WHEN dcbd.DEO = 'No' THEN 0
                      END
                    , dcbd.Alternate_Account_Number;



                INSERT INTO dbo.Account_Ubm_Account_Code_Map
                     (
                         Account_Id
                         , Ubm_Id
                         , Ubm_Account_Code
                     )
                SELECT
                    @New_Account_ID
                    , @Ubm_Id
                    , @Ubm_Account_Id
                WHERE
                    NOT EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Account_Ubm_Account_Code_Map auacm
                                   WHERE
                                        auacm.Account_Id = @New_Account_ID
                                        AND auacm.Ubm_Id = @Ubm_Id)
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Ubm_Account_Code_Map auacm
                                       WHERE
                                            auacm.Ubm_Account_Code = @Ubm_Account_Id
                                            AND auacm.Ubm_Id = @Ubm_Id);



                SELECT
                    @Account_Config_Supplier_Account_Begin_Dt = a.Supplier_Account_Begin_Dt
                    , @Account_Config_Supplier_Account_End_Dt = a.Supplier_Account_End_Dt
                FROM
                    dbo.ACCOUNT a
                WHERE
                    a.ACCOUNT_ID = @New_Account_ID;


                EXEC dbo.Supplier_Account_Config_Ins
                    @Account_Id = @New_Account_ID
                    , @Contract_Id = -1
                    , @Supplier_Account_Begin_Dt = @Account_Config_Supplier_Account_Begin_Dt
                    , @Supplier_Account_End_Dt = @Account_Config_Supplier_Account_End_Dt
                    , @User_Info_Id = 16
                    , @Supplier_Account_Config_Id = @Out_Supplier_Account_Config_Id OUT;




                UPDATE
                    tud
                SET
                    tud.Account_Id = @New_Account_ID
                    , tud.Supplier_Account_Config_Id = @Out_Supplier_Account_Config_Id
                FROM
                    @Tbl_Unq_DMO tud
                WHERE
                    tud.Id = @Min_Id;

                EXEC dbo.Account_Variance_Consumption_Level_INS_UPD
                    @AccountId = @New_Account_ID
                    , @Variance_Consumption_Level_Id = @Variance_Consumption_Level_Id
                    , @CommodityId = @CommodityId;

                EXEC dbo.Account_Variance_Consumption_Level_Rules_INS
                    @Account_id = @New_Account_ID;

                EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
                    @entity_identifier = @New_Account_ID
                    , @user_info_id = 16
                    , @audit_type = 1
                    , @entity_name = 'ACCOUNT_TABLE'
                    , @entity_type = 500;

                SELECT  @Ubm_Account_Id = NULL, @Ubm_Id = NULL;

                SET @Min_Id = @Min_Id + 1;
            END;

        SELECT  @Max_Id = MAX(Id)FROM   @Tbl_Unq_DMO_Mtrs;

        SELECT  @Min_Id = 1;
        WHILE (@Min_Id <= @Max_Id)
            BEGIN

                SELECT
                    @Meter_Id = mtr.Meter_Id
                    , @New_Account_ID = acc.Account_Id
                    , @Out_Supplier_Account_Config_Id = acc.Supplier_Account_Config_Id
                FROM
                    @Tbl_Unq_DMO acc
                    INNER JOIN @Tbl_Unq_DMO_Mtrs mtr
                        ON acc.Account_Number = mtr.Account_Number
                           AND  acc.Vendor_Id = mtr.Vendor_Id
                           AND  acc.DMO_Start_Dt = mtr.DMO_Start_Dt
                           AND  ISNULL(acc.DMO_End_Dt, '9999/12/31') = ISNULL(mtr.DMO_End_Dt, '9999/12/31')
                WHERE
                    mtr.Id = @Min_Id;

                SELECT
                    @METER_ASSOCIATION_DT = Mtr_Start_Dt
                    , @METER_DISASSOCIATION_DT = Mtr_End_Dt
                FROM
                    @Tbl_MeterDtaes
                WHERE
                    Meter_Id = @Meter_Id;

                SELECT
                    @Current_DMO_Account_ID = map.ACCOUNT_ID
                FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP map
                WHERE
                    map.METER_ID = @Meter_Id
                    AND map.Contract_ID = -1
                    AND map.METER_DISASSOCIATION_DATE IS NULL;

                SELECT  @Current_DMO_Acc_Upd_End_Dt = DATEADD(dd, -1, @METER_ASSOCIATION_DT);

                EXEC dbo.DMO_Supplier_Account_End_Dt_Upd
                    @Account_Id = @Current_DMO_Account_ID
                    , @Supplier_Account_End_Dt = @Current_DMO_Acc_Upd_End_Dt;

                EXEC dbo.Supplier_Account_IP_Upd
                    @User_Info_Id = 16
                    , @Account_Id = @Current_DMO_Account_ID;

                EXEC dbo.UTILITY_ACCOUNTS_TO_SUPP_ACCOUNT_METER_MAP_INS
                    @CONTRACT_ID = -1
                    , @ACCOUNT_ID = @New_Account_ID
                    , @METER_ID = @Meter_Id
                    -- int
                    , @METER_ASSOCIATION_DT = @METER_ASSOCIATION_DT
                    , @METER_DISASSOCIATION_DT = @METER_DISASSOCIATION_DT
                    , @IS_NEW_ACCOUNT = 1
                    , @Supplier_Account_Config_Id = @Out_Supplier_Account_Config_Id;

                SET @Min_Id = @Min_Id + 1;
            END;

        INSERT INTO #New_Account_Id
             (
                 Client_Hier_Id
                 , Account_Id
                 , Setup_Invoice_Collection
             )
        SELECT
            -1
            , Account_Id
            , Setup_Invoice_Collection
        FROM
            @Tbl_Unq_DMO
        WHERE
            Setup_Invoice_Collection IS NOT NULL;

        -- To create the standing data execption for the Invoice collection setup account
        INSERT INTO dbo.Account_Exception
             (
                 Account_Id
                 , Meter_Id
                 , Exception_Type_Cd
                 , Queue_Id
                 , Exception_Status_Cd
                 , Exception_By_User_Id
                 , Exception_Created_Ts
                 , Last_Change_Ts
                 , Updated_User_Id
                 , Commodity_Id
             )
        SELECT
            na.Account_Id
            , -1 AS Meter_ID
            , @Missing_IC_Data_Exception_Type_Cd AS Exception_Type_Cd
            , ui.QUEUE_ID
            , @Exception_Status_Cd_New AS Exception_Status_Cd
            , @System_User_Id Exception_By_User_Id
            , GETDATE() AS Exception_Created_Ts
            , GETDATE() AS Last_Change_Ts
            , @System_User_Id
            , -1 AS Commodity_Id
        FROM
            #New_Account_Id na
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = na.Account_Id
            INNER JOIN Core.Client_Hier c
                ON c.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Invoice_Collection_Client_Config cc
                ON cc.Client_Id = c.Client_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = cc.Invoice_Collection_Officer_User_Id
        WHERE
            na.Setup_Invoice_Collection = 'Yes';

    END;


GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Conversion_Insert_New_Accounts] TO [ETL_Execute]
GO
