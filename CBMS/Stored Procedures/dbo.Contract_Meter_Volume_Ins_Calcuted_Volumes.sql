SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Contract_Meter_Volume_Ins_Calcuted_Volumes]
           
DESCRIPTION:             
			To insert calculated contract meter volumes
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Meter_Id				INT
    @Contract_Id			INT
    @Volume					DECIMAL(32, 16)
    @Start_Dt				DATETIME
    @End_Dt					DATETIME
    @Unit_Type_Id			INT
    @Frequency_Type_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
BEGIN TRANSACTION
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Ins_Calcuted_Volumes 100,100,1200,'2016-01-01','2016-12-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Ins_Calcuted_Volumes 100,100,1235,'2016-01-01','2016-12-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Ins_Calcuted_Volumes 100,100,1235,'2016-01-01','2016-03-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Ins_Calcuted_Volumes 100,100,1235,'2016-01-01','2016-02-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
ROLLBACK TRANSACTION

BEGIN TRANSACTION
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	EXEC dbo.Contract_Meter_Volume_Ins_Calcuted_Volumes 100,100,123.1235,'2016-01-01','2016-03-01',20,239
	SELECT * FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
	SELECT sum(VOLUME) FROM dbo.CONTRACT_METER_VOLUME WHERE METER_ID = 100 AND CONTRACT_ID = 100
ROLLBACK TRANSACTION
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-01	Global Sourcing - Phase3 - GCS-537 Created
	RR			2020-05-09	GRMUER-81 - Added new optional input @Is_Edited	
	
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Ins_Calcuted_Volumes]
    (
        @Meter_Id INT
        , @Contract_Id INT
        , @Volume DECIMAL(32, 16)
        , @Start_Dt DATETIME
        , @End_Dt DATETIME
        , @Unit_Type_Id INT
        , @Frequency_Type_Id INT
        , @Is_Edited BIT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Short_Value DECIMAL(32, 16)
            , @Cnt INT
            , @Last_Row INT;

        DECLARE @Meter_Volumes TABLE
              (
                  Meter_Id INT
                  , Contract_Id INT
                  , Volume DECIMAL(32, 16)
                  , MONTH_IDENTIFIER DATETIME
                  , Unit_Type_Id INT
                  , Frequency_Type_Id INT
                  , Row_Num INT IDENTITY(1, 1)
              );

        SELECT  @Cnt = DATEDIFF(MONTH, @Start_Dt, @End_Dt) + 1;


        INSERT INTO @Meter_Volumes
             (
                 Meter_Id
                 , Contract_Id
                 , Volume
                 , MONTH_IDENTIFIER
                 , Unit_Type_Id
                 , Frequency_Type_Id
             )
        SELECT
            @Meter_Id
            , @Contract_Id
            , CAST(@Volume AS INT) / @Cnt
            , dd.DATE_D
            , @Unit_Type_Id
            , @Frequency_Type_Id
        --,Count_key = count(1) OVER ( PARTITION BY dd.DATE_D )
        FROM
            meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN DATEADD(dd, -DATEPART(dd, @Start_Dt) + 1, @Start_Dt)
                      AND     @End_Dt
        GROUP BY
            dd.DATE_D;

        SET @Last_Row = @@ROWCOUNT;

        SELECT
            @Short_Value = CAST(@Volume AS DECIMAL(32, 16)) - SUM(CAST(mv.Volume AS DECIMAL(32, 16)))
        FROM
            @Meter_Volumes mv;

        UPDATE
            mv
        SET
            Volume = Volume + @Short_Value
        FROM
            @Meter_Volumes mv
        WHERE
            mv.Row_Num = @Last_Row;

        DELETE  FROM
        dbo.CONTRACT_METER_VOLUME
        WHERE
            CONTRACT_ID = @Contract_Id
            AND METER_ID = @Meter_Id;

        INSERT INTO dbo.CONTRACT_METER_VOLUME
             (
                 METER_ID
                 , CONTRACT_ID
                 , VOLUME
                 , MONTH_IDENTIFIER
                 , UNIT_TYPE_ID
                 , FREQUENCY_TYPE_ID
                 , Is_Edited
             )
        SELECT
            Meter_Id
            , Contract_Id
            , Volume
            , MONTH_IDENTIFIER
            , Unit_Type_Id
            , Frequency_Type_Id
            , ISNULL(@Is_Edited, 0)
        FROM
            @Meter_Volumes;

    END;
    ;

GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Ins_Calcuted_Volumes] TO [CBMSApplication]
GO
