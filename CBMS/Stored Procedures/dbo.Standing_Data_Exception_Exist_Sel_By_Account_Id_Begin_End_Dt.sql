SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt              
                          
 DESCRIPTION:                          
   To get Supplier Account Details                         
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
@Account_Id      INT  
@Start_Dt      Date  
@End_Dt       Date                           
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
                          
 USAGE EXAMPLES:                              
------------------------------------------------------------------------------       
   
EXEC dbo.Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt  
    @Account_Id = 123  
    , @Start_Dt = '2019-01-01'  
    , @End_Dt ='2019-01-01'  
  
EXEC dbo.Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt  
    @Account_Id = 4897  
    , @Start_Dt = '2002-12-01'  
    , @End_Dt = '2003-12-01'  
  
 EXEC Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt  
  @Account_Id = 1741060    
    , @Start_Dt = '2019-03-01'    
    , @End_Dt = '2019-03-31'  
  
EXEC dbo.Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt  
    @Account_Id = 1741192  
    , @Contract_Id = 10  
	,@Cu_Invoice_Id = 1
  
               
 AUTHOR INITIALS:          
         
 Initials              Name          
------------------------------------------------------------------------------       
 NR      Narayana Reddy  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
------------------------------------------------------------------------------       
 NR                    2019-05-29       Created for - Add Contract.  
                         
******/

CREATE PROCEDURE [dbo].[Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @Cu_Invoice_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Missing_Contract_Exception_Type_Cd INT
            , @ValCon_Setup_Exception_Type_Cd INT
            , @New_Exception_Status_Cd INT
            , @Inprogress_Exception_Status_Cd INT
            , @Is_Standing_Data_Exception_Exists BIT = 0
            , @Outside_Contract_Term_Exception_Type_Cd INT;


        SELECT
            @Missing_Contract_Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Missing Contract'
            AND cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd';




        SELECT
            @ValCon_Setup_Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Missing Valcon Setup'
            AND cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd';




        SELECT
            @Outside_Contract_Term_Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd'
            AND c.Code_Value = 'Outside Contract Term ';




        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @Inprogress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                                    END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value IN ( 'New', 'In Progress' )
            AND cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd';




        SELECT
            @Is_Standing_Data_Exception_Exists = 1
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aecim
                ON aecim.Account_Exception_Id = ae.Account_Exception_Id
        WHERE
            ae.Account_Id = @Account_Id
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @Inprogress_Exception_Status_Cd )
            AND ae.Exception_Type_Cd IN ( @Missing_Contract_Exception_Type_Cd, @Outside_Contract_Term_Exception_Type_Cd )
            AND @Is_Standing_Data_Exception_Exists = 0
            AND aecim.Cu_Invoice_Id = @Cu_Invoice_Id;



        SELECT
            @Is_Standing_Data_Exception_Exists = 1
        FROM
            dbo.Contract_Exception ae
        WHERE
            ae.Contract_Id = @Contract_Id
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @Inprogress_Exception_Status_Cd )
            AND ae.Exception_Type_Cd IN ( @ValCon_Setup_Exception_Type_Cd )
            AND @Is_Standing_Data_Exception_Exists = 0;





        SELECT
            @Is_Standing_Data_Exception_Exists AS Is_Standing_Data_Exception_Exists;


    END;







GO
GRANT EXECUTE ON  [dbo].[Standing_Data_Exception_Exist_Sel_By_Account_Id_Begin_End_Dt] TO [CBMSApplication]
GO
