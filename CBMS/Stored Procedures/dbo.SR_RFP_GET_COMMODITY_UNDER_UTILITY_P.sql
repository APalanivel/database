SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_GET_COMMODITY_UNDER_UTILITY_P
	@user_id varchar(10),
	@session_id varchar(20),
	@utility_id int
	AS
	set nocount on
	select	en.entity_name as commodity_name,
		en.entity_id as commodity_id
	from 
		vendor_commodity_map map(nolock),
		entity en(nolock)
	where 	map.vendor_id = @utility_id
		and map.commodity_type_id = en.entity_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_COMMODITY_UNDER_UTILITY_P] TO [CBMSApplication]
GO
