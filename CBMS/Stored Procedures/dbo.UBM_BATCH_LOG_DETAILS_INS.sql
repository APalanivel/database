
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******  
NAME:  
 
 dbo.UBM_BATCH_LOG_DETAILS_INS
 
 DESCRIPTION:   
 
 Inserts a record into the UBM_BATCH_LOG_DETAILS table.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@Ubm_Batch_Master_Log_Id Integer

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.UBM_BATCH_LOG_DETAILS_INS 3256

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page    10/08/2009

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    
******/


CREATE PROCEDURE [dbo].[UBM_BATCH_LOG_DETAILS_INS]
      (
       @Ubm_Batch_Master_Log_Id INTEGER
      ,@Table_Name VARCHAR(60)
      ,@Expected_Data_Records INTEGER
      ,@Actual_Data_Records INTEGER
      ,@Expected_Image_Records INTEGER
      ,@Actual_Image_Records INTEGER
      ,@Expected_Dollar_Amount DECIMAL(32, 16)
      ,@Actual_Dollar_Amount DECIMAL(32, 16)
      ,@Actual_Image_Bytes INTEGER
      ,@Expected_Image_Bytes INTEGER )
AS
BEGIN

      SET NOCOUNT ON;		
	  
      INSERT      INTO UBM_BATCH_LOG_DETAILS
                  ( UBM_BATCH_LOG_ID
                  ,EXPECTED_DATA_RECORDS
                  ,ACTUAL_DATA_RECORDS
                  ,EXPECTED_IMAGE_RECORDS
                  ,ACTUAL_IMAGE_RECORDS
                  ,EXPECTED_DOLLAR_AMOUNT
                  ,ACTUAL_DOLLAR_AMOUNT
                  ,ACTUAL_IMAGE_BYTES
                  ,EXPECTED_IMAGE_BYTES )
                  SELECT
                        UBM_BATCH_LOG_ID
                       ,@Expected_Data_Records
                       ,@Actual_Data_Records
                       ,@Expected_Image_Records
                       ,@Actual_Image_Records
                       ,@Expected_Dollar_Amount
                       ,@Actual_Dollar_Amount
                       ,@Actual_Image_Bytes
                       ,@Expected_Image_Bytes
                  FROM
                        UBM_BATCH_LOG ubl
                  WHERE
                        ubl.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id
                        AND ubl.CASS_TABLE_NAME = @Table_Name;
END;



GO

GRANT EXECUTE ON  [dbo].[UBM_BATCH_LOG_DETAILS_INS] TO [CBMSApplication]
GO
