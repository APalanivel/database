SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     PROCEDURE [dbo].[cbmsSsoProjectDocument_RemoveDocument]
	( @MyAccountId int
	, @project_id int
	, @document_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_project_document_map
	  where sso_project_id = @project_id
	    and sso_document_id = @document_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectDocument_RemoveDocument] TO [CBMSApplication]
GO
