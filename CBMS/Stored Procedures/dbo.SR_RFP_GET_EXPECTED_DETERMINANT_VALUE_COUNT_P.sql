
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
NAME:  dbo.SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P                        
                              
DESCRIPTION:  
			
                               
                              
INPUT PARAMETERS:              
	Name				DataType			Default					Description              
-----------------------------------------------------------------------------------  
	@rfp_account_id		INT

                                          
OUTPUT PARAMETERS:                   
 Name					DataType			Default					Description              
-----------------------------------------------------------------------------------  
                              

USAGE EXAMPLES:              
-----------------------------------------------------------------------------------  
	SELECT top 20 * FROM dbo.SR_RFP_ACCOUNT WHERE SR_RFP_ID>12500
	  
	EXEC dbo.SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P 12096309
	EXEC dbo.SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P 12096323
  
  
 AUTHOR INITIALS:              
             
	Initials	Name              
-----------------------------------------------------------------------------------  
	RR			Raghu Reddy                                    
                               
 MODIFICATIONS:            
             
	Initials	Date        Modification            
-----------------------------------------------------------------------------------  
	RR			2016-04-15	GCS-690 Modified logic to get months count
                             
******/ 
CREATE  PROCEDURE [dbo].[SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P] ( @rfp_account_id INT )
AS 
BEGIN
      SET nocount ON
      DECLARE @sr_rfp_bid_group_id INT

      SELECT
            @sr_rfp_bid_group_id = sr_rfp_bid_group_id
      FROM
            sr_rfp_account
      WHERE
            sr_rfp_account_id = @rfp_account_id
            AND is_deleted = 0

      IF @sr_rfp_bid_group_id > 0 
            BEGIN
                  SELECT
                        max(NO_OF_MONTHS) AS expected_count
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        sr_account_group_id = @sr_rfp_bid_group_id
                        AND is_bid_group = 1
                        AND is_sdp = 0
            END
      ELSE 
            BEGIN
                  SELECT
                        max(NO_OF_MONTHS) AS expected_count
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        sr_account_group_id = @rfp_account_id
                        AND is_bid_group = 0
                        AND is_sdp = 0
            END

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_EXPECTED_DETERMINANT_VALUE_COUNT_P] TO [CBMSApplication]
GO
