
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.cbmsCommodity_GetAll                    
                  
Description:                  
		       
                  
 Input Parameters:                  
 Name                       DataType				Default        Description                    
------------------------------------------------------------------------------------  
 @MyAccountId				INT
 				            
 			 
                  
 Output Parameters:                        
 Name                       DataType				Default        Description                    
------------------------------------------------------------------------------------  
                  
 Usage Examples:                      
------------------------------------------------------------------------------------             


EXEC dbo.cbmsCommodity_GetAll 49
     
 Author Initials:                  
  Initials        Name                  
------------------------------------------------------------------------------------             
  NR              Narayana Reddy   
                   
 Modifications:                  
  Initials        Date              Modification                  
------------------------------------------------------------------------------------             
  NR              2016-05-31		MAINT-3789 Added Header and Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity.            
                 
******/    
CREATE  PROCEDURE [dbo].[cbmsCommodity_GetAll] ( @MyAccountId INT )
AS 
BEGIN

      
      SELECT
            Commodity_Id AS commodity_type_id
           ,Commodity_Name AS commodity_type
           ,UOM_Entity_Type AS unit_entity_type
           ,Default_UOM_Entity_Type_Id AS default_unit_id
      FROM
            dbo.Commodity c
      ORDER BY
            Commodity_Name


END






;
GO

GRANT EXECUTE ON  [dbo].[cbmsCommodity_GetAll] TO [CBMSApplication]
GO
