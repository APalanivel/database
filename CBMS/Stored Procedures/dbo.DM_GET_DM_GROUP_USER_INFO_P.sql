SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DM_GET_DM_GROUP_USER_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dmgroup       	varchar(30)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--EXEC DBO.DM_GET_DM_GROUP_USER_INFO_P 'datamanagment'


CREATE      PROCEDURE [dbo].[DM_GET_DM_GROUP_USER_INFO_P] 
@dmgroup varchar(30)

AS
SELECT     USER_INFO.FIRST_NAME + ' ' + USER_INFO.LAST_NAME AS USERNAME, USER_INFO.USER_INFO_ID AS USER_INFO_ID
FROM         USER_INFO INNER JOIN
                      USER_INFO_GROUP_INFO_MAP ON USER_INFO.USER_INFO_ID = USER_INFO_GROUP_INFO_MAP.USER_INFO_ID INNER JOIN
                      GROUP_INFO ON USER_INFO_GROUP_INFO_MAP.GROUP_INFO_ID = GROUP_INFO.GROUP_INFO_ID
WHERE     (GROUP_INFO.GROUP_NAME = @dmgroup)
  order by USERNAME
GO
GRANT EXECUTE ON  [dbo].[DM_GET_DM_GROUP_USER_INFO_P] TO [CBMSApplication]
GO
