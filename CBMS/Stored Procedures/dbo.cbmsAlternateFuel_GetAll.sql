SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsAlternateFuel_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsAlternateFuel_GetAll]
	( @MyAccountId int
	
	)
AS
BEGIN

	   select * from entity where entity_type = 116

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAlternateFuel_GetAll] TO [CBMSApplication]
GO
