
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
                    
 NAME: dbo.Report_DE_ISS_Invoice  
  
 DESCRIPTION:        
    To get the Iss Invoice list processed during a month  
  
   - Get the Last run month from App_Config  
   - Get all the SE UBM batches ran for the month after the last run  
   - Get the invoice and images related to those batches.  
  
 INPUT PARAMETERS:  
         
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------  
 @Client_Name      VARCHAR(200)  
  
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------  
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------  
  
 BEGIN TRAN  
  EXEC Report_DE_ISS_Invoice  
 ROLLBACK TRAN  
      
 BEGIN TRAN  
 SELECT * FROM REPTMGR.dbo.Client_Report_Config  
 EXEC Report_DE_Backed_Off_Invoice  @Client_Name = 'Philips','2014-03-01','2014-09-01'  
 SELECT * FROM REPTMGR.dbo.Client_Report_Config   
 ROLLBACK TRAN        
     
     
   SELECT * FROM Client WHERE Client_id = 12344  
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
---------------------------------------------------------------------------------------  
 HG       Harihara Suthan  
 NR       Narayana Reddy  
  
 MODIFICATIONS:  
  
 Initials               Date            Modification  
---------------------------------------------------------------------------------------  
 HG                     2016-05-24      Created for ISS requirement  
 NR						2017-05-19		MAINT-5378 - Change ISS data export stored procedure   
										to look for invoices received from Telamon insted of Schneider Electric.  
******/  
CREATE   PROCEDURE [dbo].[Report_DE_ISS_Invoice]
      ( 
       @Begin_Dt DATE = NULL
      ,@End_Dt DATE = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
        
        
      DECLARE @Ubm_Name VARCHAR(100)   
        
      SELECT
            @Ubm_Name = ac.App_Config_Value
      FROM
            dbo.App_Config ac
      WHERE
            ac.App_Config_Cd = 'ISS_UBM_Name'
            AND ac.Is_Active = 1  
  
      DECLARE @Ubm_Batch_Master_Log TABLE
            ( 
             Ubm_Batch_Master_Log_Id INT );  
  
  -- When @Begin_Dt is not available takes the previous month   
      SET @Begin_Dt = ISNULL(@Begin_Dt, DATEADD(mm, -1, DATEADD(dd, ( DAY(GETDATE() - 1) ) * -1, GETDATE())));  
  
      SET @End_Dt = ISNULL(@End_Dt, DATEADD(dd, -1, DATEADD(mm, 1, @Begin_Dt)));  
  
      INSERT      INTO @Ubm_Batch_Master_Log
                  ( 
                   Ubm_Batch_Master_Log_Id )
                  SELECT
                        ml.UBM_BATCH_MASTER_LOG_ID
                  FROM
                        dbo.UBM_BATCH_MASTER_LOG ml
                        INNER JOIN dbo.UBM u
                              ON u.UBM_ID = ml.UBM_ID
                        INNER JOIN dbo.ufn_split(@Ubm_Name, '|') ufn
                              ON ufn.Segments = u.UBM_NAME
                  WHERE
                        ml.START_DATE BETWEEN @Begin_Dt AND @End_Dt;  
  
      SELECT
            cu.CU_INVOICE_ID
           ,img.CBMS_DOC_ID AS Image_File_Name
           ,ch.Client_Name
           ,ch.Site_name
           ,cha.Account_Number
           ,cha.Account_Type
           ,com.Commodity_Name
           ,cha.Account_Vendor_Name
           ,cu.BEGIN_DATE
           ,cu.END_DATE
           ,cu.IS_DUPLICATE
           ,cu.IS_DNT
           ,cu.IS_PROCESSED
           ,cu.IS_REPORTED
           ,cu.UPDATED_DATE AS Last_Updated_Ts
      FROM
            dbo.CU_INVOICE cu
            INNER JOIN dbo.UBM_INVOICE ui
                  ON ui.UBM_INVOICE_ID = cu.UBM_INVOICE_ID
            INNER JOIN dbo.cbms_image img
                  ON cu.CBMS_IMAGE_ID = img.CBMS_IMAGE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH sm
                  ON sm.CU_INVOICE_ID = cu.CU_INVOICE_ID
            LEFT OUTER JOIN ( Core.Client_Hier_Account cha
                              INNER JOIN Core.Client_Hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                              INNER JOIN dbo.Commodity com
                                    ON com.Commodity_Id = cha.Commodity_Id )
                              ON cha.Account_Id = sm.Account_ID
      WHERE
            img.CBMS_DOC_ID LIKE '%_issfs%'
            AND EXISTS ( SELECT
                              1
                         FROM
                              @Ubm_Batch_Master_Log ubml
                         WHERE
                              ubml.Ubm_Batch_Master_Log_Id = ui.UBM_BATCH_MASTER_LOG_ID )
      GROUP BY
            cu.CU_INVOICE_ID
           ,img.CBMS_DOC_ID
           ,ch.Client_Name
           ,ch.Site_name
           ,cha.Account_Number
           ,cha.Account_Type
           ,com.Commodity_Name
           ,cha.Account_Vendor_Name
           ,cu.BEGIN_DATE
           ,cu.END_DATE
           ,cu.IS_DUPLICATE
           ,cu.IS_DNT
           ,cu.IS_PROCESSED
           ,cu.IS_REPORTED
           ,cu.UPDATED_DATE;  
  
END;  
  
;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_ISS_Invoice] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_ISS_Invoice] TO [CBMSApplication]
GO
