
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Rfp_Bid_Id		INT
    @Language_CD		INT			NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT * FROM 	dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id  1452893
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id  1452896
	EXEC dbo.Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id 1455258
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-05	Global Sourcing - Phase3 - GCS-478 Created
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id]
      ( 
       @Sr_Rfp_Bid_Id INT
      ,@Language_CD INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Acc_Country_Commodity AS TABLE
            ( 
             Country_Id INT
            ,Commodity_Id INT )
      
      SELECT
            @Language_CD = ISNULL(@Language_CD, cd.Code_Id)
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'en-US'
            AND cs.Codeset_Name = 'LocalizationLanguage';
            
            
      WITH  Cte_Qtns
              AS ( SELECT
                        sscc.Sr_Service_Condition_Category_Id
                       ,ISNULL(sscclv.Category_Name_Locale_Value, sscc.Category_Name) AS Category_Name_Locale_Value
                       ,ISNULL(sscqlv.Question_Label_Locale_Value, sscq.Question_Label) AS Question_Label_Locale_Value
                       ,sscq.Is_Comment_Shown
                       ,ISNULL(sscqlv.Comment_Label_Locale_Value, sscq.Comment_Label) AS Comment_Label_Locale_Value
                       ,cd.Code_Value AS Response_Type
                       ,rfptqm.Category_Display_Seq
                       ,rfptqm.Question_Display_Seq
                       ,rfptqm.Sr_Service_Condition_Question_Id
                       ,1 AS Is_Post_To_Supplier
                       ,rfptqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                   FROM
                        dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map rfptqm
                        INNER JOIN dbo.Sr_Service_Condition_Question sscq
                              ON rfptqm.Sr_Service_Condition_Question_Id = sscq.Sr_Service_Condition_Question_Id
                        INNER JOIN dbo.Code cd
                              ON cd.Code_Id = sscq.Response_Type_Cd
                        LEFT JOIN dbo.Sr_Service_Condition_Question_Locale_Value sscqlv
                              ON sscq.Sr_Service_Condition_Question_Id = sscqlv.Sr_Service_Condition_Question_Id
                                 AND sscqlv.Language_Cd = @Language_CD
                        INNER JOIN dbo.Sr_Service_Condition_Category sscc
                              ON rfptqm.Sr_Service_Condition_Category_Id = sscc.Sr_Service_Condition_Category_Id
                        LEFT JOIN dbo.Sr_Service_Condition_Category_Locale_Value sscclv
                              ON sscc.Sr_Service_Condition_Category_Id = sscclv.Sr_Service_Condition_Category_Id
                                 AND sscclv.Language_Cd = @Language_CD
                   WHERE
                        rfptqm.Sr_Rfp_Bid_Id = @Sr_Rfp_Bid_Id),
            Cte_SSCQ
              AS ( SELECT
                        qtns.Sr_Service_Condition_Category_Id
                       ,qtns.Category_Name_Locale_Value
                       ,qtns.Question_Label_Locale_Value
                       ,qtns.Is_Comment_Shown
                       ,qtns.Comment_Label_Locale_Value
                       ,qtns.Response_Type
                       ,qtns.Category_Display_Seq
                       ,qtns.Question_Display_Seq
                       ,qtns.Sr_Service_Condition_Question_Id
                       ,ISNULL(Response_Controls_Cnt, 1) AS Response_Controls_Cnt
                       ,LEFT(resp.Options, LEN(resp.Options) - 1) AS Response_Options
                       ,'Response_Option_Id^Option_Value^Is_Comment_Required^Display_Seq' AS Response_Options_Field_Dtls
                       ,rfpresponse.Response_Text_Value
                       ,rfpresponse.Comment
                       ,1 AS Is_Response_Required
                       ,qtns.Is_Post_To_Supplier
                   FROM
                        Cte_Qtns qtns
                        LEFT JOIN ( SELECT
                                          COUNT(sscro.Sr_Service_Condition_Response_Option_Id) AS Response_Controls_Cnt
                                         ,sscro.Sr_Service_Condition_Question_Id
                                    FROM
                                          dbo.Sr_Service_Condition_Response_Option sscro
                                    GROUP BY
                                          sscro.Sr_Service_Condition_Question_Id ) Cnt
                              ON qtns.Sr_Service_Condition_Question_Id = Cnt.Sr_Service_Condition_Question_Id
                        CROSS APPLY ( SELECT
                                          CAST(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + sscrlv.Option_Locale_Value + '^' + CAST(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + CAST(sscro.Display_Seq AS VARCHAR(10)) + ','
                                      FROM
                                          dbo.Sr_Service_Condition_Response_Option sscro
                                          LEFT JOIN dbo.Sr_Service_Condition_Response_Option_Locale_Value sscrlv
                                                ON sscro.Sr_Service_Condition_Response_Option_Id = sscrlv.Sr_Service_Condition_Response_Option_Id
                                      WHERE
                                          sscro.Sr_Service_Condition_Question_Id = qtns.Sr_Service_Condition_Question_Id
                                          AND sscrlv.Language_Cd = @Language_CD
                                      GROUP BY
                                          CAST(sscro.Sr_Service_Condition_Response_Option_Id AS VARCHAR(10)) + '^' + sscrlv.Option_Locale_Value + '^' + CAST(sscro.Is_Comment_Required AS VARCHAR(10)) + '^' + CAST(sscro.Display_Seq AS VARCHAR(10))
                                         ,sscro.Display_Seq
                                      ORDER BY
                                          sscro.Display_Seq
                        FOR
                                      XML PATH('') ) resp ( Options )
                        LEFT JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response rfpresponse
                              ON qtns.Sr_Rfp_Service_Condition_Template_Question_Map_Id = rfpresponse.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                                 AND rfpresponse.SR_RFP_BID_ID = @Sr_Rfp_Bid_Id)
            SELECT
                  Sr_Service_Condition_Category_Id
                 ,Category_Name_Locale_Value
                 ,Category_Display_Seq
                 ,Sr_Service_Condition_Question_Id
                 ,Question_Label_Locale_Value
                 ,Question_Display_Seq
                 ,Is_Comment_Shown
                 ,Comment_Label_Locale_Value
                 ,Response_Type
                 ,Response_Controls_Cnt
                 ,Response_Options
                 ,Response_Options_Field_Dtls
                 ,Response_Text_Value
                 ,Comment
                 ,Is_Response_Required
                 ,Is_Post_To_Supplier
                 ,Controls.Cnt AS Controls_Count
            FROM
                  Cte_SSCQ sscq
                  CROSS APPLY ( SELECT
                                    MAX(Response_Controls_Cnt) + MAX(CAST(Is_Comment_Shown AS INT)) AS Cnt
                                FROM
                                    Cte_SSCQ ) Controls
            
END;


;
GO

GRANT EXECUTE ON  [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response_Sel_By_Sr_Rfp_Bid_Id] TO [CBMSApplication]
GO
