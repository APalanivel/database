SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_Utility_Budget_Comments
	
DESCRIPTION:

		All of the budget comments for the utilities associated to a selected client.
	
INPUT PARAMETERS:
Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
@Client_id_List	VARCHAR(MAX)
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_Utility_Budget_Comments '223'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   08/19/2010	Created
 */
CREATE PROC [dbo].[Report_Utility_Budget_Comments]
    @Client_id_List VARCHAR(MAX)
AS 
    BEGIN

        SET NOCOUNT ON
        
        DECLARE @Account_type_id INT 
        
        SELECT
            @Account_type_id = ent.Entity_ID
        FROM
            ENTITY ent
        WHERE
            ent.ENTITY_NAME = 'Utility'
            AND ent.ENTITY_DESCRIPTION = 'Account Type'
        
        SELECT
            v.vendor_name [Utility]
          , com.Commodity_Name [Commodity]
          , ubc.Comments
          , ubc.Comment_date [Comment Date]
        FROM
            dbo.vendor v
            JOIN dbo.utility_budget_comments ubc
                ON ubc.vendor_id = v.vendor_id
            JOIN dbo.Commodity com
                ON com.Commodity_Id = ubc.COMMODITY_TYPE_ID
            JOIN dbo.account acct
                ON acct.vendor_id = v.vendor_id
            JOIN dbo.site s
                ON s.site_id = acct.site_id
            JOIN dbo.client cl
                ON cl.client_id = s.client_id
            JOIN dbo.ufn_split(@Client_id_List, ',') ufn_Client_tmp
                ON CAST(ufn_Client_tmp.Segments AS INT) = cl.CLIENT_ID
        WHERE
            comments IS NOT NULL
            AND acct.account_type_id = @Account_type_id
        GROUP BY
            v.vendor_name
          , com.Commodity_Name
          , ubc.Comments
          , ubc.Comment_date 
 
    END 


GO
GRANT EXECUTE ON  [dbo].[Report_Utility_Budget_Comments] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_Utility_Budget_Comments] TO [CBMSApplication]
GO
