SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
     
DESCRIPTION: GetVendorOnlineReadyToChaseRecordsList    
      
        
 INPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
     
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
    
     
 USAGE EXAMPLES:        
------------------------------------------------------------        
    exec [dbo].[GetVendorOnlineReadyToChaseRecordsList]
        
        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------        
 ABK  Aditya    
     
 MODIFICATIONS         
     
 Initials	Date			Modification        
------------------------------------------------------------        
 ABK		2019-12-17		Created    
 NR			2020-06-08		SE2017-963 BOT Process - Retuen Enroll_In_Bot, IC_Bots_Process_Id flag from new table.            

    
******/
CREATE PROCEDURE [dbo].[GetVendorOnlineReadyToChaseRecordsList]
AS
    BEGIN



        DECLARE
            @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT;



        SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                        END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact' );


        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';

        CREATE TABLE #Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Service_Start_Dt DATE
                 , Invoice_Collection_Service_End_Dt DATE
                 , PRIMARY KEY CLUSTERED
                   (
                       Invoice_Collection_Account_Config_Id
                   )
             );

        DECLARE @Latest_Last_BOT_Attempt_Date_Invoice_Collection_Account_Config_Ids TABLE
              (
                  Invoice_Collection_Account_Config_Id INT
                  , Last_BOT_Attempt_Date DATE
                  , PRIMARY KEY CLUSTERED
                    (
                        Invoice_Collection_Account_Config_Id
                    )
              );
        CREATE TABLE #Client_hier_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE TABLE #Consolidated_Billing_Account_Id
             (
                 Account_Id INT
             );


        CREATE TABLE #Vendor_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );


        CREATE TABLE #Ic_Account_Details
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE TABLE #Invoice_Collection_Accounts
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Account_Id INT
                 , Invoice_Collection_Service_End_Dt DATE
                 , Account_Vendor_Name VARCHAR(200)
                 , Account_Type CHAR(8)
                 , Account_Vendor_Id INT
             );

        CREATE TABLE #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id INT NOT NULL PRIMARY KEY CLUSTERED
                 , Client_Name VARCHAR(200)
                 , Country_Name VARCHAR(200)
                 , Account_Vendor_Name VARCHAR(255)
                 , Account_Vendor_Id INT
                 , Account_Number VARCHAR(255)
                 , Account_Id INT
                 , Alternate_Account_Number VARCHAR(255)
                 , Invoice_Frequency VARCHAR(255)
                 , Date_In_Queue DATE
                 , Last_Action_Date DATE
                 , Invoice_Source VARCHAR(255)
                 , Invoice_Source_Type VARCHAR(25)
                 , Invoice_Source_Method_Of_Contact VARCHAR(25)
                 , Invoice_Collection_Account_Config_Id INT
                 , Beginning_Date DATE
                 , End_Date DATE
                 , Account_Invoice_Collection_Source_Id BIT
                 , Invoice_File_Name NVARCHAR(255)
                 , Received_Status_Updated_Dt DATE
                 , Next_Action_Dt DATE
                 , Download_Attempt_Cnt INT
                 , Last_BOT_Attempt_Date DATE
             );


        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Vendor_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE CLUSTERED INDEX cx_#Vendor_Dtls
            ON #Vendor_Dtls
        (
            Invoice_Collection_Account_Config_Id
        )   ;

        CREATE TABLE #Contact_Dtls
             (
                 Invoice_Collection_Account_Config_Id INT
                 , Contact_Level_Cd INT
                 , Contact_Info_Id INT
                 , Is_Primary BIT
             );

        CREATE CLUSTERED INDEX cx_#Contact_Dtls
            ON #Contact_Dtls
        (
            Invoice_Collection_Account_Config_Id
        )   ;






        INSERT INTO #Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id
                 , Account_Id
                 , Invoice_Collection_Service_Start_Dt
                 , Invoice_Collection_Service_End_Dt
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                       WHERE
                            ibpicacm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                            AND ibpicacm.Enroll_In_Bot = 1)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;


        INSERT INTO @Latest_Last_BOT_Attempt_Date_Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Account_Config_Id
                 , Last_BOT_Attempt_Date
             )
        SELECT
            icaci.Invoice_Collection_Account_Config_Id
            , MAX(icq.Last_BOT_Attempt_Date)
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
                ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
        GROUP BY
            icaci.Invoice_Collection_Account_Config_Id;

        INSERT INTO #Invoice_Collection_Accounts
             (
                 Invoice_Collection_Account_Config_Id
                 , Account_Id
                 , Invoice_Collection_Service_End_Dt
                 , Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_End_Dt
            , ucha.Account_Vendor_Name
            , ucha.Account_Type
            , ucha.Account_Vendor_Id
        FROM
            #Invoice_Collection_Account_Config_Ids icac
            INNER JOIN Core.Client_Hier_Account ucha
                ON icac.Account_Id = ucha.Account_Id
        WHERE
            ucha.Account_Type = 'Utility'
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Account_Id
            , icac.Invoice_Collection_Service_End_Dt
            , ucha.Account_Vendor_Name
            , ucha.Account_Type
            , ucha.Account_Vendor_Id;

        INSERT INTO #Client_hier_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            scha.Account_Vendor_Name
            , scha.Account_Type
            , scha.Account_Vendor_Id
            , ucha1.Account_Id
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            Core.Client_Hier_Account scha
            INNER JOIN Core.Client_Hier_Account ucha1
                ON ucha1.Meter_Id = scha.Meter_Id
                   AND  ucha1.Account_Type = 'Utility'
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = scha.Supplier_Contract_ID
            INNER JOIN dbo.ENTITY ce
                ON c.CONTRACT_TYPE_ID = ce.ENTITY_ID
                   AND  ce.ENTITY_NAME = 'Supplier'
                   AND  scha.Account_Type = 'Supplier'
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icac.Account_Id = ucha1.Account_Id
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   icac.Invoice_Collection_Service_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN scha.Supplier_Meter_Association_Date
                              AND     scha.Supplier_Meter_Disassociation_Date
        GROUP BY
            scha.Account_Vendor_Name
            , scha.Account_Type
            , scha.Account_Vendor_Id
            , ucha1.Account_Id
            , icac.Invoice_Collection_Account_Config_Id;



        INSERT INTO #Consolidated_Billing_Account_Id
             (
                 Account_Id
             )
        SELECT
            asbv1.Account_Id
        FROM
            dbo.Account_Consolidated_Billing_Vendor asbv1
            INNER JOIN dbo.ENTITY e1
                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                   AND  e1.ENTITY_NAME = 'Supplier'
            LEFT OUTER JOIN dbo.VENDOR v1
                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Invoice_Collection_Accounts icac
                       WHERE
                            asbv1.Account_Id = icac.Account_Id)
        GROUP BY
            asbv1.Account_Id;


        INSERT INTO #Vendor_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            v.VENDOR_NAME
            , e.ENTITY_NAME
            , v.VENDOR_ID
            , asbv.Account_Id
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Account_Consolidated_Billing_Vendor asbv
            INNER JOIN dbo.ENTITY e
                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                   AND  e.ENTITY_NAME = 'Supplier'
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icac.Account_Id = asbv.Account_Id
                   AND  (CASE WHEN icac.Invoice_Collection_Service_End_Dt IS NULL
                                   OR   asbv.Billing_End_Dt > GETDATE() THEN GETDATE()
                             ELSE icac.Invoice_Collection_Service_End_Dt
                         END) BETWEEN asbv.Billing_Start_Dt
                              AND     ISNULL(asbv.Billing_End_Dt, '9999-12-31')
        GROUP BY
            v.VENDOR_NAME
            , e.ENTITY_NAME
            , v.VENDOR_ID
            , asbv.Account_Id
            , icac.Invoice_Collection_Account_Config_Id;


        INSERT INTO #Ic_Account_Details
             (
                 Account_Vendor_Name
                 , Account_Type
                 , Account_Vendor_Id
                 , Account_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            icav.VENDOR_NAME
            , 'Supplier'
            , icav.VENDOR_ID
            , icac.Account_Id
            , aics.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Contact icc
            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
            INNER JOIN dbo.Vendor_Contact_Map vcm
                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
            INNER JOIN dbo.VENDOR icav
                ON icav.VENDOR_ID = vcm.VENDOR_ID
            INNER JOIN dbo.ENTITY ve
                ON icav.VENDOR_TYPE_ID = ve.ENTITY_ID
            INNER JOIN #Invoice_Collection_Accounts icac
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  icc.Is_Primary = 1
        GROUP BY
            icav.VENDOR_NAME
            , icav.VENDOR_ID
            , icac.Account_Id
            , aics.Invoice_Collection_Account_Config_Id;



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            avd.Account_Vendor_Name
            , avd.Account_Type
            , avd.Account_Vendor_Id
            , avd.Invoice_Collection_Account_Config_Id
        FROM
            #Client_hier_Account_Details avd
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Consolidated_Billing_Account_Id cbai
                       WHERE
                            avd.Account_Id = cbai.Account_Id);



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            vad.Account_Vendor_Name
            , vad.Account_Type
            , vad.Account_Vendor_Id
            , vad.Invoice_Collection_Account_Config_Id
        FROM
            #Vendor_Account_Details vad
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Vendor_Dtls vd
                           WHERE
                                vad.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            icd.Account_Vendor_Name
            , icd.Account_Type
            , icd.Account_Vendor_Id
            , icd.Invoice_Collection_Account_Config_Id
        FROM
            #Ic_Account_Details icd
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Consolidated_Billing_Account_Id cbai
                       WHERE
                            icd.Account_Id = cbai.Account_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Vendor_Dtls vd
                               WHERE
                                    icd.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);



        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            ica.Account_Vendor_Name
            , ica.Account_Type
            , ica.Account_Vendor_Id
            , ica.Invoice_Collection_Account_Config_Id
        FROM
            #Invoice_Collection_Accounts ica
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Vendor_Dtls vd
                           WHERE
                                ica.Invoice_Collection_Account_Config_Id = vd.Invoice_Collection_Account_Config_Id);


        INSERT INTO #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id
                 , Client_Name
                 , Country_Name
                 , Account_Vendor_Name
                 , Account_Vendor_Id
                 , Account_Number
                 , Account_Id
                 , Alternate_Account_Number
                 , Invoice_Frequency
                 , Date_In_Queue
                 , Last_Action_Date
                 , Invoice_Source
                 , Invoice_Source_Type
                 , Invoice_Source_Method_Of_Contact
                 , Invoice_Collection_Account_Config_Id
                 , Beginning_Date
                 , End_Date
                 , Invoice_File_Name
                 , Received_Status_Updated_Dt
                 , Next_Action_Dt
                 , Download_Attempt_Cnt
                 , Last_BOT_Attempt_Date
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Name
            , ch.Country_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END Account_Vendor_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END AS Account_Vendor_Id
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ISNULL(ifc.Code_Value, icgc.Code_Value) Invoice_Frequency
            , icq.Created_Ts Date_In_Queue
            , MAX(icq.Last_Change_Ts) Last_Action_Date
            , cics.Code_Value AS Invoice_Source
            , cicst.Code_Value AS Invoice_Source_Type
            , cicsm.Code_Value AS Invoice_Source_Method_Of_Contact
            , icac.Invoice_Collection_Account_Config_Id
            , icq.Collection_Start_Dt Beginning_Date
            , icq.Collection_End_Dt End_Date
            , icq.Invoice_File_Name
            , icq.Received_Status_Updated_Dt
            , icq.Next_Action_Dt
            , icq.Download_Attempt_Cnt
            , llbadicaci.Last_BOT_Attempt_Date
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN Code icqst
                ON icq.Status_Cd = icqst.Code_Id
            LEFT OUTER JOIN dbo.VENDOR v
                ON v.VENDOR_ID = icq.Manual_ICR_Vendor_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN #Invoice_Collection_Account_Config_Ids icaci
                ON icac.Invoice_Collection_Account_Config_Id = icaci.Invoice_Collection_Account_Config_Id
            INNER JOIN @Latest_Last_BOT_Attempt_Date_Invoice_Collection_Account_Config_Ids llbadicaci
                ON llbadicaci.Invoice_Collection_Account_Config_Id = icaci.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            INNER JOIN(dbo.Account_Invoice_Collection_Month aicm
                       LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aicfe
                           ON aicm.Account_Invoice_Collection_Frequency_Id = aicfe.Account_Invoice_Collection_Frequency_Id
                       LEFT OUTER JOIN dbo.Code ifc
                           ON ifc.Code_Id = aicfe.Invoice_Frequency_Cd
                       LEFT OUTER JOIN dbo.Invoice_Collection_Global_Config_Value icgcv
                           ON aicm.Invoice_Collection_Global_Config_Value_Id = icgcv.Invoice_Collection_Global_Config_Value_Id
                       LEFT OUTER JOIN dbo.Code icgc
                           ON icgc.Code_Id = icgcv.Invoice_Frequency_Cd)
                ON icqmm.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
                ON aics.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                   AND  aics.Is_Primary = 1
            LEFT OUTER JOIN dbo.Code cics
                ON cics.Code_Id = aics.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code cicst
                ON cicst.Code_Id = aics.Invoice_Source_Type_Cd
            LEFT OUTER JOIN dbo.Code cicsm
                ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd
        WHERE
            icqst.Code_Value = 'open'
            AND icq.Invoice_Collection_Queue_Type_Cd = 102325
        GROUP BY
            icq.Invoice_Collection_Queue_Id
            , ch.Client_Name
            , ch.Country_Name
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_NAME
                  ELSE ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
              END
            , CASE WHEN icq.Is_Not_Default_Vendor = 1 THEN v.VENDOR_ID
                  ELSE ISNULL(vd.Account_Vendor_Id, cha.Account_Vendor_Id)
              END
            , cha.Account_Number
            , cha.Account_Id
            , icac.Invoice_Collection_Alternative_Account_Number
            , ISNULL(ifc.Code_Value, icgc.Code_Value)
            , icq.Created_Ts
            , cics.Code_Value
            , cicst.Code_Value
            , cicsm.Code_Value
            , icac.Invoice_Collection_Account_Config_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , icq.Invoice_File_Name
            , icq.Received_Status_Updated_Dt
            , icq.Next_Action_Dt
            , icq.Download_Attempt_Cnt
            , llbadicaci.Last_BOT_Attempt_Date;

        SELECT  DISTINCT
                icq.Date_In_Queue
                , icq.Client_Name
                , icq.Country_Name
                , icq.Account_Vendor_Name
                , icq.Account_Vendor_Id
                , icq.Account_Number
                , icq.Account_Id
                , icq.Alternate_Account_Number
                , icq.Invoice_Frequency
                , icq.Beginning_Date
                , icq.End_Date
                , CASE WHEN (DAY(icq.End_Date) < CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                          aicf.Expected_Invoice_Received_Day
                                                     ELSE aicf.Expected_Invoice_Raised_Day
                                                 END)
                            AND (CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                          aicf.Expected_Invoice_Received_Day
                                     ELSE aicf.Expected_Invoice_Raised_Day
                                 END = 31)
                            AND (MONTH(icq.End_Date) IN ( 4, 6, 9, 11 )) THEN
                           CAST(YEAR(icq.End_Date) AS VARCHAR(10)) + '-'
                           + CASE WHEN LEN(CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))) = 1 THEN
                                      '0' + CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))
                                 ELSE CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))
                             END + '-'
                           + CAST((CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                            aicf.Expected_Invoice_Received_Day
                                       ELSE aicf.Expected_Invoice_Raised_Day
                                   END) - 30 AS VARCHAR(10))
                      WHEN (DAY(icq.End_Date) < CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                         aicf.Expected_Invoice_Received_Day
                                                    ELSE aicf.Expected_Invoice_Raised_Day
                                                END)
                           AND  (CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                          aicf.Expected_Invoice_Received_Day
                                     ELSE aicf.Expected_Invoice_Raised_Day
                                 END IN ( 29, 30, 31 ))
                           AND  (MONTH(icq.End_Date) = 2) THEN
                          CAST(YEAR(icq.End_Date) AS VARCHAR(10)) + '-'
                          + CASE WHEN LEN(CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))) = 1 THEN
                                     '0' + CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))
                                ELSE CAST(MONTH(icq.End_Date) + 1 AS VARCHAR(10))
                            END + '-'
                          + CAST((CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                           aicf.Expected_Invoice_Received_Day
                                      ELSE aicf.Expected_Invoice_Raised_Day
                                  END) - 28 AS VARCHAR(10))
                      WHEN DAY(icq.End_Date) < CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                        aicf.Expected_Invoice_Received_Day
                                                   ELSE aicf.Expected_Invoice_Raised_Day
                                               END THEN
                          CAST(YEAR(icq.End_Date) AS VARCHAR(10)) + '-'
                          + CASE WHEN LEN(CAST(MONTH(icq.End_Date) AS VARCHAR(10))) = 1 THEN
                                     '0' + CAST(MONTH(icq.End_Date) AS VARCHAR(10))
                                ELSE CAST(MONTH(icq.End_Date) AS VARCHAR(10))
                            END + '-'
                          + CASE WHEN LEN(CAST((CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                         aicf.Expected_Invoice_Received_Day
                                                    ELSE aicf.Expected_Invoice_Raised_Day
                                                END) AS VARCHAR(10))) = 1 THEN
                                     '0'
                                     + CAST((CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                      aicf.Expected_Invoice_Received_Day
                                                 ELSE aicf.Expected_Invoice_Raised_Day
                                             END) AS VARCHAR(10))
                                ELSE
                                    CAST((CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                                   aicf.Expected_Invoice_Received_Day
                                              ELSE aicf.Expected_Invoice_Raised_Day
                                          END) AS VARCHAR(10))
                            END
                      ELSE
                          CAST(DATEADD(
                                   DAY
                                   , (CASE WHEN aicf.Expected_Invoice_Received_Day IS NOT NULL THEN
                                               aicf.Expected_Invoice_Received_Day
                                          ELSE aicf.Expected_Invoice_Raised_Day
                                      END)
                                   , CAST(DATEADD(DAY, -1, DATEADD(mm, DATEDIFF(m, 0, icq.End_Date) + 1, 0)) AS DATE)) AS VARCHAR(10))
                  END [Expected Day]
                , icq.Invoice_Source + '-' + ISNULL(icq.Invoice_Source_Type, '') + '-'
                  + ISNULL(icq.Invoice_Source_Method_Of_Contact, '') AS Invoice_Collection_Sources
                , COALESCE(aicdfefsd.URL, aicdfeisd.URL, aicosd.URL) [URL]
                , COALESCE(aicdfefsd.Login_Name, aicdfeisd.Login_Name, aicosd.Login_Name) UserName
                , COALESCE(aicdfefsd.Passcode, aicdfeisd.Passcode, aicosd.Passcode) [Password]
                , icq.Last_Action_Date
                , icq.Invoice_Collection_Queue_Id
                , CASE WHEN aicmcim.Account_Invoice_Collection_Month_Id IS NULL THEN 0
                      ELSE 1
                  END Invoice_Received
                , icq.Invoice_File_Name [File Name]
                , icq.Received_Status_Updated_Dt [Date Received]
                , aicf.Seq_No [Sequence ID]
                , icq.Next_Action_Dt [Next Action Date]
                , icq.Download_Attempt_Cnt [Downloads]
                , icq.Last_BOT_Attempt_Date
                , ibpicacm.IC_Bots_Process_Id
                , ibp.IC_Bots_Process_Name
        FROM
            #Invoice_Collection_Queue_Ids icq
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Source aics
                ON aics.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                   AND  aics.Is_Primary = 1
            LEFT OUTER JOIN dbo.Invoice_Collection_Issue_Log icil
                ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Account_Invoice_Collection_Frequency aicf
                ON aicf.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl aicdfefsd
                ON aicdfefsd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Data_Feed_Etl_Image_Source_Dtl aicdfeisd
                ON aicdfeisd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl aicosd
                ON aicosd.Account_Invoice_Collection_Source_Id = aics.Account_Invoice_Collection_Source_Id
            LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION ip
                ON icq.Account_Id = ip.ACCOUNT_ID
            LEFT OUTER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm
                ON icq.Invoice_Collection_Queue_Id = icqmm.Invoice_Collection_Queue_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Month aicm
                ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Month_Cu_Invoice_Map aicmcim
                ON aicmcim.Account_Invoice_Collection_Month_Id = aicm.Account_Invoice_Collection_Month_Id
            LEFT OUTER JOIN IC_Bots_Process_Invoice_Collection_Account_Config_Map ibpicacm
                ON ibpicacm.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.IC_Bots_Process ibp
                ON ibp.IC_Bots_Process_Id = ibpicacm.IC_Bots_Process_Id
        WHERE
            CASE WHEN aicmcim.Account_Invoice_Collection_Month_Id IS NULL THEN 0
                ELSE 1
            END = 0
            AND ip.IS_EXPECTED = 1
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Issue_Log icil2
                               WHERE
                                    icil2.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                    AND icil2.Is_Blocker = 1);


        DROP TABLE
            #Invoice_Collection_Queue_Ids
            , #Invoice_Collection_Account_Config_Ids
            , #Client_hier_Account_Details
            , #Consolidated_Billing_Account_Id
            , #Vendor_Account_Details
            , #Ic_Account_Details
            , #Invoice_Collection_Accounts
            , #Contact_Dtls
            , #Vendor_Dtls;




    END;



GO
GRANT EXECUTE ON  [dbo].[GetVendorOnlineReadyToChaseRecordsList] TO [CBMSApplication]
GO
