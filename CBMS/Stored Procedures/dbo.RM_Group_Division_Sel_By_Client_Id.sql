SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
   
 dbo.RM_Group_Division_Sel_By_Client_Id
   
 DESCRIPTION:     
   
 This procedure gets all the Supplier name associated to the user.  
   
 INPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
 @Client_Id			INT
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Group_Division_Sel_By_Client_Id 235 
	   
	
  
AUTHOR INITIALS:    
 Initials		Name    
------------------------------------------------------------    
NR				Narayana Reddy
    
 MODIFICATIONS     
 Initials			Date			Modification    
------------------------------------------------------------    
   NR				25-07-2018		Created For Risk managemnet.		


******/
CREATE PROCEDURE [dbo].[RM_Group_Division_Sel_By_Client_Id]
     (
         @Client_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            'Division' AS Filter_Name
            , CH.Sitegroup_Id AS Filter_Value
            , CH.Sitegroup_Name AS Display_Filter_Name
        FROM
            Core.Client_Hier CH
        WHERE
            CH.Client_Id = @Client_Id
            AND CH.Sitegroup_Type_Name = 'Division'
            AND CH.Site_Id = 0
            AND EXISTS (SELECT  1 FROM  dbo.Sitegroup_Site SS WHERE CH.Sitegroup_Id = SS.Sitegroup_id)
        GROUP BY
            CH.Sitegroup_Id
            , CH.Sitegroup_Name
        ORDER BY
            CH.Sitegroup_Name;

    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Group_Division_Sel_By_Client_Id] TO [CBMSApplication]
GO
