SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_UPLOADED_RC_ACCOUNTS_UNDER_SITES_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@sites         	varchar(500)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_UPLOADED_RC_ACCOUNTS_UNDER_SITES_INFO_P
@sites varchar(500)
AS
set nocount on	
	DECLARE @SQL varchar(600)

	SET @SQL = 
	'select  a.account_id,
	a.account_number	

	from	client c(nolock), 
		vwSiteName vwSite(nolock), 	
		account a(nolock),			
		sr_rc_contract_document_accounts_map accMap (nolock),
		site s(nolock)
		
		
	where  	 vwSite.site_id = a.site_id
		and c.client_id = vwSite.client_id	
		and accMap.sr_rc_contract_document_id is not null
		and accMap.account_id = a.account_id
		and vwSite.site_id = s.site_id
		and s.site_id in (' + @sites + ')

	group by  a.account_id ,a.account_number '

	
	EXEC(@SQL)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_UPLOADED_RC_ACCOUNTS_UNDER_SITES_INFO_P] TO [CBMSApplication]
GO
