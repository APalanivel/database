SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Supplier_Account_Info_Sel_By_Contract_Id

DESCRIPTION:

INPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
 @Supplier_Account_Config_Id		INT					
 
 
OUTPUT PARAMETERS:
Name								DataType		Default			Description
-------------------------------------------------------------------------------
	
USAGE EXAMPLES:
-------------------------------------------------------------------------------
USE CBMS

EXEC Supplier_Account_Info_Sel_By_Contract_Id
    @Contract_Id = 1


AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------
	NR			Narayana Reddy	
	
MODIFICATIONS
	Initials	Date			Modification
-------------------------------------------------------------------------------
    NR			2019-06-26		Created For Add contract.   
	NR			2020-01-28		MAINT- Added Nullif Condition on end date to return Null.

******/

CREATE PROCEDURE [dbo].[Supplier_Account_Info_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_begin_Dt
            , NULLIF(cha.Supplier_Account_End_Dt, '9999-12-31') AS Supplier_Account_End_Dt
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = -1
            AND EXISTS (   SELECT
                                1
                           FROM
                                Core.Client_Hier_Account cha_sup
                           WHERE
                                cha_sup.Client_Hier_Id = cha.Client_Hier_Id
                                AND cha_sup.Account_Id = cha.Account_Id
                                AND cha_sup.Supplier_Contract_ID = @Contract_Id)
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_begin_Dt
            , NULLIF(cha.Supplier_Account_End_Dt, '9999-12-31');
    END;







GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Info_Sel_By_Contract_Id] TO [CBMSApplication]
GO
