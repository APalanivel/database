SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
 NAME: dbo.[Invoice_Collection_Queue_Resolved_With_No_Invoice_Sel_By_Invoice_Collection_Account_Config_Id]                      
                                  
 DESCRIPTION:                                  
   To get the details of invoice for the given account_config                   
                                  
 INPUT PARAMETERS:                    
                               
 Name                        DataType         Default       Description                  
---------------------------------------------------------------------------------------------------------------                
@Invoice_Collection_Account_Config_Id INT  NULL           
                                        
 OUTPUT PARAMETERS:                    
                                     
 Name                        DataType         Default       Description                  
---------------------------------------------------------------------------------------------------------------                
                                  
 USAGE EXAMPLES:                                      
---------------------------------------------------------------------------------------------------------------                                      
              
          
 Exec Invoice_Collection_Queue_Dtls_Sel_Invoice_Collection_Account_Config_Id 64295  
Exec Invoice_Collection_Queue_Resolved_With_No_Invoice_Sel_By_Invoice_Collection_Account_Config_Id 64295  
select * from invoice_collection_queue where invoice_collection_account_config_id=64295   
  
          
                                 
 AUTHOR INITIALS:                  
                 
 Initials              Name                  
---------------------------------------------------------------------------------------------------------------                                
 RKV                  Ravi Kumar Vegesna          
                                   
 MODIFICATIONS:                
                    
 Initials              Date             Modification                
---------------------------------------------------------------------------------------------------------------                
    RKV    2019-08-21  Created For SE.                   
                                 
******/  
  
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Resolved_With_No_Invoice_Sel_By_Invoice_Collection_Account_Config_Id]  
     (  
         @Invoice_Collection_Account_Config_Id INT  
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
  
  
  
        SELECT  
            icq.Invoice_Collection_Queue_Id  
            , aicm.Account_Invoice_Collection_Month_Id  
            , aicm.Service_Month  
   ,icq.Collection_Start_Dt  
   ,icq.Collection_End_Dt  
   ,icq.Is_Locked
   ,icq.Invoice_File_Name
   ,icq.Received_Status_Updated_Dt
   ,cc.Code_Value as Invoice_Collection_Queue_type
   ,icq.Invoice_Collection_Queue_Type_Cd
   ,icq.Invoice_Collection_Exception_Type_Cd
        FROM  
            dbo.Invoice_Collection_Account_Config icac  
            INNER JOIN dbo.Invoice_Collection_Queue icq  
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id  
            INNER JOIN dbo.Invoice_Collection_Queue_Month_Map icqmm  
                ON icqmm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id  
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm  
                ON aicm.Account_Invoice_Collection_Month_Id = icqmm.Account_Invoice_Collection_Month_Id  
            INNER JOIN Code c  
                ON icq.Status_Cd = c.Code_Id
			INNER JOIN Code cc
                ON icq.Invoice_Collection_Queue_Type_Cd = cc.Code_Id
        WHERE  
            c.Code_Value = 'Resolved'  
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id  
            AND NOT EXISTS (   SELECT  
                                    1  
                               FROM  
                                    dbo.CU_INVOICE_SERVICE_MONTH cism  
                               WHERE  
                                    cism.Account_ID = icac.Account_Id  
                                    AND cism.SERVICE_MONTH = aicm.Service_Month);  
    END;  
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Resolved_With_No_Invoice_Sel_By_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
