SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 dbo.Vendor_Name_Search_By_Keyword    
    
DESCRIPTION: 
			Vendor name Smart search implemented.
  
INPUT PARAMETERS:  
 Name				DataType			Default			Description      
------------------------------------------------------------------------    
@Vendor_Name		VARCHAR(200)		NULL
@Vendor_Type_Id		INT					NULL
  
OUTPUT PARAMETERS:      
 Name				DataType			Default			Description      
------------------------------------------------------------------------    
  
USAGE EXAMPLES:  
------------------------------------------------------------------------    
  
 EXEC dbo.Vendor_Name_Search_By_Keyword 'America','Utility'
 EXEC dbo.Vendor_Name_Search_By_Keyword 'American Building Maintena','Utility'
 EXEC dbo.Vendor_Name_Search_By_Keyword '1-800'
 
   
   
Author Initials:          
 Initials		Name        
------------------------------------------------------------------------    
 NR				Narayana Reddy
         
 Modifications :          
 Initials   Date			Modification          
------------------------------------------------------------------------    
 NR			2016-04-20		Created For Global Sourcing Phase-3.

******/  

CREATE PROCEDURE [dbo].[Vendor_Name_Search_By_Keyword]
      ( 
       @Vendor_Name VARCHAR(200) = NULL
      ,@Vendor_Type VARCHAR(200) = NULL )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Vendor_Type_Id INT= NULL

      SELECT
            @Vendor_Type_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            ENTITY_NAME = @Vendor_Type
            AND ENTITY_TYPE = 155

      SELECT
            V.VENDOR_ID
           ,V.VENDOR_NAME
      FROM
            dbo.VENDOR v
            INNER JOIN dbo.ENTITY e
                  ON v.VENDOR_TYPE_ID = e.ENTITY_ID
      WHERE
            ( @Vendor_Name IS NULL
              OR v.VENDOR_NAME LIKE + @Vendor_Name + '%' )
            AND ( @Vendor_Type_Id IS NULL
                  OR v.VENDOR_TYPE_ID = @Vendor_Type_Id )
            AND v.IS_HISTORY = 0
      ORDER BY
            v.VENDOR_NAME
      
END 

;
GO
GRANT EXECUTE ON  [dbo].[Vendor_Name_Search_By_Keyword] TO [CBMSApplication]
GO
