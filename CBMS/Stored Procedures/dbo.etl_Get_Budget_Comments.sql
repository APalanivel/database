SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	cmbs.dbo.etl_Get_Budget_Comments


DESCRIPTION:
	Gets all changes made between the MinDBTS and the MaxDBTS. 
	This procedure is stricly used for ETL applications 


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MinDBTS		bigint					minimum row version 
	@MaxDBTS		bigint        	        max row version   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.etl_Get_Budget_Comments 1, 100

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			06/08/2009	Created        	
******/
CREATE PROCEDURE dbo.etl_Get_Budget_Comments    
(     
	@MinDBTS bigint    
	,@MaxDBTS bigint    
)    
AS     
BEGIN     
	SET NOCOUNT ON     

	SELECT     
		ba.ACCOUNT_ID    
		,ba.Budget_Id    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.VARIABLE_COMMENTS, '') ELSE '' END  as variable_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.transportation_comments, '') ELSE '' END  as transportation_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.distribution_comments, '') ELSE '' END  as distribution_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.transmission_comments, '') ELSE '' END  as transmission_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.other_bundled_comments, '') ELSE '' END  as other_bundled_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.other_fixed_costs_comments, '') ELSE '' END  as other_fixed_costs_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.sourcing_tax_comments, '') ELSE '' END  as sourcing_tax_comments    
		,CASE WHEN (b.is_shown_comments_on_dv = 1 or bdc.is_variable_on_dv = 1) THEN isnull(bdc.rates_tax_comments, '') ELSE '' END  as rates_tax_comments    
		,CASE WHEN (b.is_Shown_Comments_On_Dv = 1) THEN b.Comments ELSE '' END as Budget_Comments    
	FROM     
		BUDGET b (NOLOCK)   
		INNER JOIN budget_account ba (NOLOCK) 
			ON b.BUDGET_ID = ba.BUDGET_ID    
		INNER JOIN budget_detail_comments bdc (NOLOCK) 
			ON ba.BUDGET_ACCOUNT_ID = bdc.BUDGET_ACCOUNT_ID    
	WHERE    
		bdc.ROW_VERSION between @MinDBTS and @MaxDBTS    
		AND ba.is_deleted = 0    
END 


GO
GRANT EXECUTE ON  [dbo].[etl_Get_Budget_Comments] TO [CBMSApplication]
GO
