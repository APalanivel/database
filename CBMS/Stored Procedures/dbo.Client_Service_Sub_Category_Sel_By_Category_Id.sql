SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Client_Service_Sub_Category_Sel_By_Category_Id

DESCRIPTION:
	Used to select the Sub_Client_Service_Category_Id and Service_Category_Name

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------

	@Client_Service_Category_Id INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

Exec Client_Service_Sub_Category_Sel_By_Category_Id  64
Exec Client_Service_Sub_Category_Sel_By_Category_Id  111

AUTHOR INITIALS:

	Initials	Name
-----------------------------------------------------------
	
	BCH			Balaraju
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 BCH			2011-10-19	created

******/

CREATE PROCEDURE dbo.Client_Service_Sub_Category_Sel_By_Category_Id
      ( 
       @Client_Service_Category_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            cscc.Sub_Client_Service_Category_Id
           ,csc.Service_Category_Name
      FROM
            dbo.Client_Service_Category_Commodity cscc
            JOIN dbo.Client_Service_Category csc
                  ON cscc.Client_Service_Category_Id = csc.Client_Service_Category_Id
      WHERE
            cscc.Client_Service_Category_Id = @Client_Service_Category_Id
      GROUP BY
            cscc.Sub_Client_Service_Category_Id
           ,csc.Service_Category_Name

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Sub_Category_Sel_By_Category_Id] TO [CBMSApplication]
GO
