SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSite_GetByClient

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       	          	
	@region_id     	int       	null      	
	@state_id      	int       	null      	
	@exclude_closed	bit       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE               PROCEDURE [dbo].[cbmsSite_GetByClient]
	( @MyAccountId int
	, @client_id int
	, @region_id int = null
	, @state_id int = null
	, @exclude_closed bit = null
	)
AS
BEGIN

if @exclude_closed is null
	set @exclude_closed = 0
else
	set @exclude_closed = null

	   select s.site_id
		, ad.city
		, st.state_name
		, s.site_name
		, ad.address_line1
		, ad.address_line2
		, vw.site_name site_label
		, d.division_name
		, d.division_id
		, c.client_name
		, s.not_managed
	     from division d with (nolock)
	     join site s with (nolock) on (s.division_id = d.division_id)-- and s.closed = coalesce(@exclude_closed, s.closed)
--			and s.not_managed = coalesce(@exclude_closed, s.not_managed))
	     join address ad with (nolock) on (ad.address_id = s.primary_address_id and ad.state_id = isNull(@state_id, ad.state_id))
	     join state st with (nolock) on st.state_id = ad.state_id
	     join vwSiteName vw with (nolock) on vw.site_id = s.site_id
	     join client c with (nolock) on c.client_id = d.client_id
	    where d.client_id = @client_id    
	 order by vw.site_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSite_GetByClient] TO [CBMSApplication]
GO
