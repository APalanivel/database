SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [DBO].[SUPPLIER_ACCOUNT_DETAILS_SEL]    
    
DESCRIPTION:    
 GETS ALL THE SUPPLIER ACCOUNT DETAILS FOR THE SELECTED CONTRACTID - EXTEND CONTRACT  
    
    
INPUT PARAMETERS:    
NAME				DATATYPE				DEFAULT				DESCRIPTION    
----------------------------------------------------------------------------------------    
@CONTRACT_ID		INT    
  
    
    
OUTPUT PARAMETERS:    
NAME				DATATYPE				DEFAULT				DESCRIPTION    
----------------------------------------------------------------------------------------    
  
    
USAGE EXAMPLES:    
----------------------------------------------------------------------------------------    
    
	EXEC SUPPLIER_ACCOUNT_DETAILS_SEL 36608  
    
AUTHOR INITIALS:    
	INITIALS	NAME    
----------------------------------------------------------------------------------------    
	MGB			BHASKARAN GOPALAKRISHNAN    
	HG			Hari  
	SP			Sandeep Pigilam
	RR			Raghu Reddy
	SP			Sandeep Pigilam
	NR			Narayana Reddy
    
MODIFICATIONS    
	INITIALS	DATE		MODIFICATION    
----------------------------------------------------------------------------------------    
	MGB			06-OCT-09	CREATED    
	HG			26-OCT-09	Contract table join removed as the contract_id available in Supplier_Acccount_meter_map table.    
	MGB			29-OCT-09	Added New_Supplier_Account_Start_dt column in the select clause.  
	HG			11/05/2009	Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity  
							Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd   
	HG			11/07/2009	Classification_type_id column moved to Contract table from account as it is specific to contract  
							Column renamed to contract_classification_cd  
							Added Contract table join to pull the contract_classification_cd value from contract table.  
	HG			11/11/2009	Added samm.Is_History filter condition.  
	NK			11/12/2009	Added processing instructions, watchlist group
	SP			2014-08-13	Added Is_Invoice_Posting_Blocked column in select list.
	RR			2015-07-24	Global Sourcing - Added Alternate_Account_Number to select list
    SP			2017-03-17	Contract Placeholder project change,removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY. 	
	NR			2019-04-03	Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.	
	RKV			2019-12-30	D20-1762  removed UBM details 
				

***********************/

CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_DETAILS_SEL]
    (
        @CONTRACT_ID INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  DISTINCT
                ACC.ACCOUNT_ID ACCOUNT_ID
                , ACC.VENDOR_ID VENDOR_ID
                , ACC.INVOICE_SOURCE_TYPE_ID INVOICE_SOURCE_TYPE_ID
                , ACC.ACCOUNT_NUMBER ACCOUNT_NUMBER
                , ACC.ACCOUNT_TYPE_ID ACCOUNT_TYPE_ID
                , ACC.NOT_EXPECTED NOT_EXPECTED
                , ACC.NOT_MANAGED NOT_MANAGED
                
                , ACC.SITE_ID SITE_ID
                , ACC.SERVICE_LEVEL_TYPE_ID SERVICE_LEVEL_TYPE_ID
                , ACC.ELIGIBILITY_DATE ELIGIBILITY_DATE
                , con.Contract_Classification_Cd
                , ACC.PROCESSING_INSTRUCTIONS_UPDATED_USER_ID PROCESSING_INSTRUCTIONS_UPDATED_USER_ID
                , ACC.PROCESSING_INSTRUCTIONS_UPDATED_DATE PROCESSING_INSTRUCTIONS_UPDATED_DATE
                , ACC.Supplier_Account_Begin_Dt SUPPLIER_ACCOUNT_BEGIN_DT
                , ACC.Supplier_Account_End_Dt SUPPLIER_ACCOUNT_END_DT
                , ACC.Supplier_Account_Recalc_Type_Cd
                , ACC.Supplier_Account_Is_Default_Setting SUPPLIER_ACCOUNT_IS_DEFAULT_SETTING
                , ACC.Supplier_Account_End_Dt + 1 NEW_SUPPLIER_ACCOUNT_START_DT
                , ACC.Is_Data_Entry_Only
                , ACC.Is_Invoice_Posting_Blocked
                , ACC.Alternate_Account_Number
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP SAM
            JOIN dbo.ACCOUNT ACC
                ON ACC.ACCOUNT_ID = SAM.ACCOUNT_ID
		    
            JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = SAM.Contract_ID
        WHERE
            SAM.Contract_ID = @CONTRACT_ID
            AND SAM.IS_HISTORY = 0;
    END;


    ;

    ;

GO



GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_DETAILS_SEL] TO [CBMSApplication]
GO
