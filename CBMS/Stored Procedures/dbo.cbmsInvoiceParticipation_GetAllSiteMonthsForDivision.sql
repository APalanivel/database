SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForDivision]
	( @MyAccountId int
	, @division_id int
	)
AS
BEGIN


   select distinct ip.site_id
	, ip.service_month
     from division d 
     join site s with (nolock) on s.division_id = d.division_id
     join invoice_participation ip on ip.site_id = s.site_id
    where d.division_id = @division_id

END



GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetAllSiteMonthsForDivision] TO [CBMSApplication]
GO
