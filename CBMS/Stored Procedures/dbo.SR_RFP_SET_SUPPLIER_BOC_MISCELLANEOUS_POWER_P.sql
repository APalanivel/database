SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_MISCELLANEOUS_POWER_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@miscId int,
@blockSizes varchar(100),
@priceIncludesGreenEnergyTypeId varchar(10),
@percentGreenEnergy varchar(100),
@convertProductFixedPriceTypeId varchar(10),
@convertProductFixedPriceComments varchar(4000),
@buyerBlockHedgesTypeId varchar(10),
@buyerBlockHedgesComments varchar(4000),
@heatPricingTriggersTypeId varchar(10),
@heatPricingTriggersComments varchar(4000),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@miscIdentityId int outPUT


USAGE EXAMPLES:
------------------------------------------------------------
----TEST 1
----EXEC  SR_RFP_GET_SUPPLIER_BOC_MISCELLANEOUS_POWER_P 1,1,931

---- TEST 2
---- Check values at SV before and after a test insert
--SELECT * FROM SV..SR_RFP_MISCELLANEOUS_POWER WHERE SR_RFP_MISCELLANEOUS_POWER_ID = (SELECT MAX(SR_RFP_MISCELLANEOUS_POWER_ID) FROM SV..SR_RFP_MISCELLANEOUS_POWER)
----196472

---- Make a test insert
--EXEC [dbo].[SR_RFP_SET_SUPPLIER_BOC_MISCELLANEOUS_POWER_P] 
--@miscId =0,
--@blockSizes = null,
--@priceIncludesGreenEnergyTypeId = null,
--@percentGreenEnergy = null,
--@convertProductFixedPriceTypeId = null,
--@convertProductFixedPriceComments = 'Test Data',
--@buyerBlockHedgesTypeId = null,
--@buyerBlockHedgesComments = null,
--@heatPricingTriggersTypeId = null,
--@heatPricingTriggersComments = 'Test Data',
--@miscIdentityId = null

---- Delete the inserted row
--DELETE SV..SR_RFP_MISCELLANEOUS_POWER WHERE SR_RFP_MISCELLANEOUS_POWER_ID = (SELECT MAX(SR_RFP_MISCELLANEOUS_POWER_ID) FROM SV..SR_RFP_MISCELLANEOUS_POWER)


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_MISCELLANEOUS_POWER_P] 

@miscId int,
@blockSizes varchar(100),
@priceIncludesGreenEnergyTypeId varchar(10),
@percentGreenEnergy varchar(100),
@convertProductFixedPriceTypeId varchar(10),
@convertProductFixedPriceComments varchar(4000),
@buyerBlockHedgesTypeId varchar(10),
@buyerBlockHedgesComments varchar(4000),
@heatPricingTriggersTypeId varchar(10),
@heatPricingTriggersComments varchar(4000),
@miscIdentityId int outPUT

as
	
set nocount on
	
if @miscId=0

begin
		
INSERT INTO SR_RFP_MISCELLANEOUS_POWER
(
	BLOCK_SIZES,
	PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID,
	PERCENT_GREEN_ENERGY,
	CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID,
	CONVERT_PRODUCT_FIXED_PRICE_COMMENTS,
	BUYER_BLOCK_HEDGES_TYPE_ID,
	BUYER_BLOCK_HEDGES_COMMENTS,
	HEAT_PRICING_TRIGGERS_TYPE_ID,
	HEAT_PRICING_TRIGGERS_COMMENTS
)
VALUES
(
	@blockSizes,
	@priceIncludesGreenEnergyTypeId,
	@percentGreenEnergy,
	@convertProductFixedPriceTypeId,
	@convertProductFixedPriceComments,
	@buyerBlockHedgesTypeId,
	@buyerBlockHedgesComments,
	@heatPricingTriggersTypeId,
	@heatPricingTriggersComments
)

   SELECT @miscIdentityId= SCOPE_IDENTITY()
end

if @miscId>0
begin

	UPDATE SR_RFP_MISCELLANEOUS_POWER SET
	BLOCK_SIZES=@blockSizes,
	PRICE_INCLUDES_GREEN_ENERGY_TYPE_ID=@priceIncludesGreenEnergyTypeId,
	PERCENT_GREEN_ENERGY=@percentGreenEnergy,
	CONVERT_PRODUCT_FIXED_PRICE_TYPE_ID=@convertProductFixedPriceTypeId,
	CONVERT_PRODUCT_FIXED_PRICE_COMMENTS=@convertProductFixedPriceComments,
	BUYER_BLOCK_HEDGES_TYPE_ID=@buyerBlockHedgesTypeId,
	BUYER_BLOCK_HEDGES_COMMENTS=@buyerBlockHedgesComments,
	HEAT_PRICING_TRIGGERS_TYPE_ID=@heatPricingTriggersTypeId,
	HEAT_PRICING_TRIGGERS_COMMENTS=@heatPricingTriggersComments
	WHERE SR_RFP_MISCELLANEOUS_POWER_ID=@miscId

   SELECT @miscIdentityId= @miscId

end

return
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_MISCELLANEOUS_POWER_P] TO [CBMSApplication]
GO
