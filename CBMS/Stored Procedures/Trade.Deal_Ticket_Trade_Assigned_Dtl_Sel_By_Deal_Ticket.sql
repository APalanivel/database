SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    Trade.Deal_Ticket_Trade_Assigned_Dtl_Sel_By_Deal_Ticket  
     
      
DESCRIPTION:     
     
      
INPUT PARAMETERS:      
      Name          DataType       Default        Description      
-----------------------------------------------------------------------------      
   
    
      
OUTPUT PARAMETERS:    
      
 Name     DataType   Default  Description      
-----------------------------------------------------------------------------      
      
      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------------      
   
 Exec Trade.Deal_Ticket_Trade_Assigned_Dtl_Sel_By_Deal_Ticket '132178,132179'  
  
   
   
         
AUTHOR INITIALS:       
 Initials    Name  
-----------------------------------------------------------------------------         
 PRV          Pramod Reddy V  
  
      
MODIFICATIONS       
 Initials    Date        Modification        
-----------------------------------------------------------------------------         
 PRV          2019-06-25 GRM Proejct.  
******/
CREATE PROC [Trade].[Deal_Ticket_Trade_Assigned_Dtl_Sel_By_Deal_Ticket]
     (
         @Deal_Ticket_Id VARCHAR(MAX) = NULL
     )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Ticket_Id VARCHAR(100);

        DECLARE @Deal_Tickets TABLE
              (
                  Deal_Ticket_Id INT
              );


        IF @Deal_Ticket_Id IS NOT NULL
            BEGIN
                INSERT INTO @Deal_Tickets
                     (
                         Deal_Ticket_Id
                     )
                SELECT  us.Segments FROM    dbo.ufn_split(@Deal_Ticket_Id, ',') us;
            END;

        ELSE
            BEGIN

                SET @Ticket_Id = @Deal_Ticket_Id;

            END;


        SELECT
            dt.Deal_Ticket_Id
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END AS CM_Name
            , LEFT(DATENAME(MONTH, dt.Hedge_Start_Dt), 3) + ' '
              + RIGHT('00' + CAST(YEAR(dt.Hedge_Start_Dt) AS VARCHAR), 2) + ' - '
              + LEFT(DATENAME(MONTH, dt.Hedge_End_Dt), 3) + ' '
              + RIGHT('00' + CAST(YEAR(dt.Hedge_End_Dt) AS VARCHAR), 2) AS Term
            , Action_co.Code_Value + ' - ' + DT_co.Code_Value AS FixType_Transaction
            , ch.Client_Name
            , CASE WHEN LEN(CParty.Counter_Party) > 0 THEN LEFT(CParty.Counter_Party, LEN(CParty.Counter_Party) - 1)
              END AS Counter_party_Supplier
            , SUM(vol.Total_Volume) AS Total_Volume
            , com.Commodity_Name AS Commodity
            , cu.CURRENCY_UNIT_NAME Units_Currency
            , freq_co.Code_Value AS Volume_Frequency
            , consm.ENTITY_NAME AS Units
            , Pr_Co.Code_Value AS Pricing_Applied
            , pid.PRICING_POINT AS Price_Point
            , vol.Trade_Number AS Trade_Order
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END AS CM_Email
            , NULL AS RequestDate
            , chws.Workflow_Status_Map_Id AS Status_Id
            , ui2.EMAIL_ADDRESS AS Initiator_Email
            , ui2.FIRST_NAME + ' ' + ui2.LAST_NAME AS Initiator_Name
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.ENTITY dttyp
                ON dt.Hedge_Type_Cd = dttyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dtch.Client_Hier_Id = vol.Client_Hier_Id
                   AND  dtch.Deal_Ticket_Id = vol.Deal_Ticket_Id
                   AND  ISNULL(chws.Contract_Id, -1) = vol.Contract_Id
                   AND  chws.Trade_Month = vol.Deal_Month
            INNER JOIN dbo.Code Action_co
                ON dt.Trade_Action_Type_Cd = Action_co.Code_Id
            INNER JOIN dbo.Code DT_co
                ON dt.Deal_Ticket_Type_Cd = DT_co.Code_Id
            INNER JOIN dbo.Code freq_co
                ON dt.Deal_Ticket_Frequency_Cd = freq_co.Code_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = dt.Currency_Unit_Id
            INNER JOIN dbo.ENTITY consm
                ON consm.ENTITY_ID = dt.Uom_Type_Id
            INNER JOIN dbo.Code Pr_Co
                ON dt.Trade_Pricing_Option_Cd = Pr_Co.Code_Id
            INNER JOIN dbo.PRICE_INDEX pid
                ON dt.Price_Index_Id = pid.PRICE_INDEX_ID
            CROSS APPLY
        (   SELECT
                ui.EMAIL_ADDRESS + ','
            FROM
                dbo.USER_INFO ui
            WHERE
                dt.Queue_Id = ui.QUEUE_ID
            FOR XML PATH('')) CQ(CurrentQ_Email)
            CROSS APPLY
        (   SELECT
                ui.EMAIL_ADDRESS + ','
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
            WHERE
                ccm.CLIENT_ID = dt.Client_Id
            FOR XML PATH('')) CM(CM_Email)
            CROSS APPLY
        (   SELECT
                ui.FIRST_NAME + ' ' + ui.LAST_NAME + ', '
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
            WHERE
                ccm.CLIENT_ID = dt.Client_Id
            GROUP BY
                ui.FIRST_NAME + ' ' + ui.LAST_NAME
            FOR XML PATH('')) CMName(CM_Name)
            CROSS APPLY
        (   SELECT
                ISNULL(cha.VENDOR_NAME, cp.COUNTERPARTY_NAME) + ', '
            FROM
                Trade.Deal_Ticket_Client_Hier_TXN_Status trd
                LEFT JOIN dbo.VENDOR cha
                    ON cha.VENDOR_ID = trd.Cbms_Counterparty_Id
                LEFT JOIN dbo.RM_COUNTERPARTY cp
                    ON cp.RM_COUNTERPARTY_ID = trd.Cbms_Counterparty_Id
            WHERE
                trd.Cbms_Trade_Number = vol.Trade_Number
                AND ISNULL(cha.VENDOR_NAME, cp.COUNTERPARTY_NAME) IS NOT NULL
            GROUP BY
                ISNULL(cha.VENDOR_NAME, cp.COUNTERPARTY_NAME)
            FOR XML PATH('')) CParty(Counter_Party)
            LEFT JOIN dbo.USER_INFO ui2
                ON dt.Created_User_Id = ui2.USER_INFO_ID
        WHERE
            chws.Is_Active = 1
            AND chws.Last_Notification_Sent_Ts IS NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                Trade.Workflow_Status_Map wsm
                                INNER JOIN Trade.Workflow_Status ws
                                    ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                           WHERE
                                chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                                AND ws.Workflow_Status_Name = 'Assigned To Trader')
            AND (   @Ticket_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  @Deal_Tickets us WHERE  us.Deal_Ticket_Id = dt.Deal_Ticket_Id))
        GROUP BY
            dt.Deal_Ticket_Id
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END
            , dt.Hedge_Start_Dt
            , dt.Hedge_End_Dt
            , Action_co.Code_Value + ' - ' + DT_co.Code_Value
            , ch.Client_Name
            , CASE WHEN LEN(CParty.Counter_Party) > 0 THEN LEFT(CParty.Counter_Party, LEN(CParty.Counter_Party) - 1)
              END
            , com.Commodity_Name
            , cu.CURRENCY_UNIT_NAME
            , consm.ENTITY_NAME
            , Pr_Co.Code_Value
            , pid.PRICING_POINT
            , freq_co.Code_Value
            , vol.Trade_Number
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END
            , chws.Workflow_Status_Map_Id
            , ui2.EMAIL_ADDRESS
            , ui2.FIRST_NAME
            , ui2.LAST_NAME;

    END;





GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Trade_Assigned_Dtl_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
