SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.UPDATE_CBMS_IMAGE_LOCATION_P(@cbms_image_id INT
	, @CBMS_Image_Directory VARCHAR(255)
	, @CBMS_Image_FileName VARCHAR(255)
	, @CBMS_Image_Location_Id INT )
	
AS
BEGIN
  
	SET NOCOUNT ON;  
   
	UPDATE dbo.CBMS_Image
		SET CBMS_Image_Directory= @CBMS_Image_Directory
			, CBMS_Image_FileName = @CBMS_Image_FileName
			, CBMS_Image_Location_Id = @CBMS_Image_Location_Id
	WHERE Cbms_Image_Id = @Cbms_Image_Id
	
	UPDATE dbo.CBMS_Image_Location
		SET Active_File_Cnt = ISNULL(Active_File_Cnt,0) + 1
	WHERE CBMS_Image_Location_Id = @CBMS_Image_Location_Id
		AND Active_Directory = @CBMS_Image_Directory

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_CBMS_IMAGE_LOCATION_P] TO [CBMSApplication]
GO
