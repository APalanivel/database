SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Participant_Budget_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Site_Participant_Budget_Sel  @Client_Id = 1005,@Start_Index=1,@End_Index = 25
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Participant_Budget_Sel]
    (
        @Client_Id INT
        , @Commodity_Id INT = NULL
        , @Country_Id NVARCHAR(MAX) = NULL
        , @State_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @Site_Not_Managed INT = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
        , @Currency_Unit_Id INT = NULL
        , @Uom_Id INT = NULL
        , @Index_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH Cte_Sites
        AS (
               SELECT
                    RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_Name
                    , ch.Client_Hier_Id
                    , rb.Rm_Budget_Id
                    , rb.Budget_NAME
                    , rb.Budget_Start_Dt
                    , rb.Budget_End_Dt
                    , indx.ENTITY_NAME AS Index_Name
                    , uom.ENTITY_NAME AS UOM_Name
                    , cu.CURRENCY_UNIT_NAME
                    , REPLACE(CONVERT(VARCHAR(20), rb.Created_Ts, 106), ' ', '-') AS Last_Updated
                    , DENSE_RANK() OVER (ORDER BY
                                             RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')') AS Site_Rnk
                    , rbsdbc.Start_Dt
                    , rbsdbc.End_Dt
                    , rbsdbc.Rm_Budget_Site_Default_Budget_Config_Id
                    , CASE rb.Is_Client_Generated WHEN 1 THEN 'Yes'
                          WHEN 0 THEN 'No'
                          ELSE NULL
                      END AS Is_Client_Generated
               FROM
                    Core.Client_Hier ch
                    LEFT JOIN Trade.Rm_Budget_Site_Default_Budget_Config rbsdbc
                        ON rbsdbc.Client_Hier_Id = ch.Client_Hier_Id
                    LEFT JOIN Trade.Rm_Budget rb
                        ON rb.Rm_Budget_Id = rbsdbc.Rm_Budget_Id
                    LEFT JOIN dbo.ENTITY indx
                        ON indx.ENTITY_ID = rb.Index_Id
                    LEFT JOIN dbo.ENTITY uom
                        ON rb.Uom_Type_Id = uom.ENTITY_ID
                    LEFT JOIN dbo.CURRENCY_UNIT cu
                        ON rb.Currency_Unit_Id = cu.CURRENCY_UNIT_ID
               WHERE
                    (   @Client_Id IS NULL
                        OR  ch.Client_Id = @Client_Id)
                    AND ch.Site_Id > 0
                    AND (   @Country_Id IS NULL
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.ufn_split(@Country_Id, '') us
                                           WHERE
                                                CAST(us.Segments AS INT) = ch.Country_Id))
                    AND (   @State_Id IS NULL
                            OR  ch.State_Id = @State_Id)
                    AND (   @Division_Id IS NULL
                            OR  ch.Sitegroup_Id = @Division_Id)
                    AND (   @Site_Id IS NULL
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.ufn_split(@Site_Id, '') us
                                           WHERE
                                                CAST(us.Segments AS INT) = ch.Site_Id))
                    AND (   @Commodity_Id IS NULL
                            OR  rb.Commodity_Id = @Commodity_Id)
                    AND (   @Site_Not_Managed IS NULL
                            OR  ch.Site_Not_Managed = @Site_Not_Managed)
                    AND (   (   @Start_Dt IS NULL
                                AND @End_Dt IS NULL)
                            OR  (   @Start_Dt IS NOT NULL
                                    AND @End_Dt IS NULL
                                    AND @Start_Dt BETWEEN rb.Budget_Start_Dt
                                                  AND     rb.Budget_End_Dt)
                            OR  (   @Start_Dt IS NULL
                                    AND @End_Dt IS NOT NULL
                                    AND @End_Dt BETWEEN rb.Budget_Start_Dt
                                                AND     rb.Budget_End_Dt)
                            OR  (   @Start_Dt BETWEEN rb.Budget_Start_Dt
                                              AND     rb.Budget_End_Dt
                                    OR  @End_Dt BETWEEN rb.Budget_Start_Dt
                                                AND     rb.Budget_End_Dt))
                    AND (   @Index_Id IS NULL
                            OR  rb.Index_Id = @Index_Id)
                    AND (   @Currency_Unit_Id IS NULL
                            OR  rb.Currency_Unit_Id = @Currency_Unit_Id)
                    AND (   @Uom_Id IS NULL
                            OR  rb.Uom_Type_Id = @Uom_Id)
           )
        SELECT
            Site_Name
            , Client_Hier_Id
            , Rm_Budget_Id
            , Budget_NAME
            , Budget_Start_Dt
            , Budget_End_Dt
            , Index_Name
            , UOM_Name
            , CURRENCY_UNIT_NAME
            , Last_Updated
            , Site_Rnk
            , Start_Dt
            , End_Dt
            , Rm_Budget_Site_Default_Budget_Config_Id
            , Is_Client_Generated
        FROM
            Cte_Sites
        WHERE
            Cte_Sites.Site_Rnk BETWEEN @Start_Index
                               AND     @End_Index;

    END;

GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Participant_Budget_Sel] TO [CBMSApplication]
GO
