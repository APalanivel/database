SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.SR_SAD_GET_SITES_UNDER_CLIENT_P 
@clientId int

as
	set nocount on
select	site.SITE_ID,
	viewSite.SITE_NAME
	--,e.entity_name

from	SITE site,	
	DIVISION division ,
	VWSITENAME viewSite,
	client c
	--,entity e

where 	c.CLIENT_ID= @clientId 
	AND site.DIVISION_ID=division.DIVISION_ID 	
	and site.SITE_ID = viewSite.SITE_ID
	and site.closed=0 and site.not_managed=0
	and division.client_id = c.client_id
	--and site.site_type_id = e.entity_id

order by viewSite.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
