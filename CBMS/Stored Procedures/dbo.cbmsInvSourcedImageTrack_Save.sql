SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[cbmsInvSourcedImageTrack_Save]
	( @MyAccountId int
	, @inv_sourced_image_track_id int = null
	, @inv_sourced_image_id int
	, @original_filename varchar(200)
	, @prokarma_filename varchar(200) = null
	, @is_received bit = 0
	, @cu_invoice_id int = null
	, @track_comments varchar(2000) = null
	)
AS
BEGIN

	declare @this_id int

	set @this_id = @inv_sourced_image_track_id

	set nocount on

	if @this_id is null
	begin

	   select @this_id = inv_sourced_image_track_id
	     from inv_sourced_image_track
	    where inv_sourced_image_id = @inv_sourced_image_id
	      and original_filename = @original_filename
	      and (prokarma_filename = @prokarma_filename or (prokarma_filename is null and @prokarma_filename is null))

	end

	if @this_id is null
	begin

		insert into inv_sourced_image_track
			( inv_sourced_image_id
			, original_filename
			, prokarma_filename
			, is_received
			, cu_invoice_id
			, track_comments
			)
		values
			( @inv_sourced_image_id
			, @original_filename
			, @prokarma_filename
			, @is_received
			, @cu_invoice_id
			, @track_comments
			)

		set @this_id = @@IDENTITY

	end
	else
	begin
	   update inv_sourced_image_track
	      set inv_sourced_image_id = @inv_sourced_image_id
		, original_filename = @original_filename
		, prokarma_filename = @prokarma_filename
		, is_received = @is_received
		, cu_invoice_id = @cu_invoice_id
		, track_comments = @track_comments
	    where inv_sourced_image_track_id = @this_id
	end

--	set nocount off

	exec cbmsInvSourcedImageTrack_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageTrack_Save] TO [CBMSApplication]
GO
