SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCPIndexSourcing_GetForDate

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clearport_index	varchar(200)	null      	
	@index_detail_date	datetime  	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsCPIndexSourcing_GetForDate] 
	( @clearport_index varchar(200) = null
	, @index_detail_date datetime = null
	)
AS
BEGIN

	select cd.index_detail_date
		, cd.index_detail_value
		, cm.clearport_index_month
		, ci.clearport_index
	     from clearport_index_detail cd  with (nolock)
    	      join clearport_index_months cm with (nolock) on cm.clearport_index_month_id = cd.clearport_index_month_id
  	      join clearport_index ci with (nolock) on ci.clearport_index_id = cm.clearport_index_id 
   	     where ci.clearport_index = @clearport_index
  	       and day(cd.index_detail_date) = day(@index_detail_date)
		and month(cd.index_detail_date) = month(@index_detail_date)
		and year(cd.index_detail_date) = year(@index_detail_date)
		order by clearport_index_month


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexSourcing_GetForDate] TO [CBMSApplication]
GO
