SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Update_Cu_Invoice_By_Account_ID

DESCRIPTION:
			To update invoice vendor of the given account

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_ID     INT 
	@Old_Vendor_Id  INT    
	@New_Vendor_Id	INT     	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT DISTINCT TOP 10 a.CU_INVOICE_ID,b.Account_ID,a.VENDOR_ID 
	FROM dbo.CU_INVOICE a JOIN dbo.CU_INVOICE_SERVICE_MONTH b ON a.CU_INVOICE_ID = b.CU_INVOICE_ID AND b.SERVICE_MONTH>='01/01/2012'
	
	BEGIN TRANSACTION
		SELECT DISTINCT a.CU_INVOICE_ID,b.Account_ID,a.VENDOR_ID 
			FROM dbo.CU_INVOICE a JOIN dbo.CU_INVOICE_SERVICE_MONTH b ON a.CU_INVOICE_ID = b.CU_INVOICE_ID WHERE a.CU_INVOICE_ID = 6830972
		--EXEC dbo.Update_Cu_Invoice_By_Account_ID 156664,398,6835  --3408004
		--EXEC dbo.Update_Cu_Invoice_By_Account_ID 354808,7091,5243  --6621551
		EXEC dbo.Update_Cu_Invoice_By_Account_ID 390660,7041,6233  --6830972
		SELECT DISTINCT a.CU_INVOICE_ID,b.Account_ID,a.VENDOR_ID 
			FROM dbo.CU_INVOICE a JOIN dbo.CU_INVOICE_SERVICE_MONTH b ON a.CU_INVOICE_ID = b.CU_INVOICE_ID WHERE a.CU_INVOICE_ID = 6830972
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2013-10-21	MAINT-2127 Created

******/
CREATE PROCEDURE dbo.Update_Cu_Invoice_By_Account_ID
      ( 
       @Account_ID INT
      ,@Old_Vendor_Id INT
      ,@New_Vendor_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            ci
      SET   
            ci.VENDOR_ID = @New_Vendor_Id
      FROM
            dbo.CU_INVOICE ci
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON ci.CU_INVOICE_ID = cism.CU_INVOICE_ID
      WHERE
            cism.Account_ID = @Account_ID
            AND ci.VENDOR_ID = @Old_Vendor_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Update_Cu_Invoice_By_Account_ID] TO [CBMSApplication]
GO
