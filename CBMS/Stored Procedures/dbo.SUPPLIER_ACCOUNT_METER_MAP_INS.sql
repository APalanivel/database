SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[DBO].[SUPPLIER_ACCOUNT_METER_MAP_INS]

DESCRIPTION:
	INSERTS SUPPLIER ACCOUNT DETAILS INTO ACCOUNT TABLE

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@CONTRACTID		INT,  
@ACCOUNTID		INT,    
@METERID		INT,    
@METERASSOCIATIONDATE	DATETIME,    
@METERDISASSOCIATIONDATE DATETIME    


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
MGB			Bhaskar Gopalakrishnan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
MGB			10/12/2009	Created
HG			10/29/2009	Removed ROWLOCK hint used.
MGB			12/09/2009	Script added to get the Site_id for the given meter
						cbmsInvoiceParticipation_InsertSupplierAccount procedure called to insert the supplier account invoice participation dtls.
MGB			12/15/2009	Reverted the changes made on 12/09/2009 as we have the same functionality in procedure UPDATE_SUPPLIER_ACCOUNT_METER_MAP_P.
NR			2019-10-23  Added @Supplier_Account_Config_Id Parameter
*/


CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_METER_MAP_INS]
    (
        @CONTRACTID INT
        , @ACCOUNTID INT
        , @METERID INT
        , @METERASSOCIATIONDATE DATETIME
        , @METERDISASSOCIATIONDATE DATETIME
        , @Supplier_Account_Config_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @site_id INT;

        INSERT INTO dbo.SUPPLIER_ACCOUNT_METER_MAP
             (
                 Contract_ID
                 , ACCOUNT_ID
                 , METER_ID
                 , METER_ASSOCIATION_DATE
                 , METER_DISASSOCIATION_DATE
                 , Supplier_Account_Config_Id
             )
        VALUES
            (@CONTRACTID
             , @ACCOUNTID
             , @METERID
             , @METERASSOCIATIONDATE
             , @METERDISASSOCIATIONDATE
             , @Supplier_Account_Config_Id);

    END;

GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_METER_MAP_INS] TO [CBMSApplication]
GO
