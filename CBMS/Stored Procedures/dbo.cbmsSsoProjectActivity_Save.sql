SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE       PROCEDURE [dbo].[cbmsSsoProjectActivity_Save]
	( @MyAccountID int 
	, @sso_project_activity_id int
	, @created_by_id int
	, @project_id int
	, @activity_date datetime
	, @activity_description varchar(4000)=null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @sso_project_activity_id

	if @this_id is null
	begin

		insert into sso_project_activity
			( 
			created_by_id
			, sso_project_id
			, activity_date
			, activity_description
			 )
		 values
			(  
			@created_by_id
			, @project_id
			, @activity_date
			, @activity_description
			)

		set @this_id = @@IDENTITY

	end
	else
	begin

		   update sso_project_activity
		      set created_by_id = @created_by_id
			, sso_project_id = @project_id
			, activity_date = @activity_date
			, activity_description = @activity_description
		
		    where sso_project_activity_id = @this_id

	end

	exec cbmsSsoProjectActivity_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectActivity_Save] TO [CBMSApplication]
GO
