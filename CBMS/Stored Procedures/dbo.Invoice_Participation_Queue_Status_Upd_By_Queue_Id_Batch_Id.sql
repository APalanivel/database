SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:	dbo.Invoice_Participation_Queue_Status_Upd_By_Queue_Id_Batch_Id   

DESCRIPTION:     

	Update PArcipation batch queue eithe complatd or pending.
	
INPUT PARAMETERS:    
Name								DataType		Default		Description    
----------------------------------------------------------------------------    
@Invoice_Participation_Queue_Id 	INT     
@Invoice_Participation_Batch_Id 	INT      
@Staus_Cd							INT  
@Error_Desc							NVARCHAR(MAX)  

OUTPUT PARAMETERS:    
Name					DataType		Default		Description    
----------------------------------------------------------------------------    

USAGE EXAMPLES:    
----------------------------------------------------------------------------  
  
EXEC Invoice_Participation_Queue_Status_Upd_By_Queue_Id_Batch_Id 
      @Invoice_Participation_Queue_Id =101
     ,@Invoice_Participation_Batch_Id=1
     ,@Staus_Cd =10
     ,@Error_Desc = NULL 


AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------------------    
NR			Narayana Reddy
 
MODIFICATIONS     
Initials	Date			Modification    
------------------------------------------------------------    
NR			2017-04-24		Created for MAINT - 5046(5244).
******/
CREATE  PROCEDURE [dbo].[Invoice_Participation_Queue_Status_Upd_By_Queue_Id_Batch_Id]
      ( 
       @Invoice_Participation_Queue_Id INT
      ,@Invoice_Participation_Batch_Id INT
      ,@Staus_Cd INT
      ,@Error_Desc NVARCHAR(MAX) = NULL )
AS 
BEGIN      
      SET NOCOUNT ON;   
      
      
      UPDATE
            ipq
      SET   
            ipq.Status_Cd = @Staus_Cd
           ,ipq.Error_Desc = @Error_Desc
      FROM
            dbo.INVOICE_PARTICIPATION_QUEUE ipq
      WHERE
            ipq.INVOICE_PARTICIPATION_QUEUE_ID = @Invoice_Participation_Queue_Id
            AND ipq.INVOICE_PARTICIPATION_BATCH_ID = @Invoice_Participation_Batch_Id
   
 

END;
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Queue_Status_Upd_By_Queue_Id_Batch_Id] TO [CBMSApplication]
GO
