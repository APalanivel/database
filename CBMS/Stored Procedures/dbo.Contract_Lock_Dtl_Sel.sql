SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                       
NAME:  dbo.Contract_Lock_Dtl_Sel                     
                     
DESCRIPTION:                 
                
Used to Insert/update the data into dbo.Site_Commodity_Analyst_Merge table.                
                    
INPUT PARAMETERS:                        
      Name     DataType          Default     Description                        
------------------------------------------------------------                        
   @Contract_Id   INT                
   @Start_Dt    DATE                 
   @Expiration_Dt      DATE                 
   @Nymex_Price   DECIMAL (28, 10)                 
   @Contract_Cost         DECIMAL (28, 10)                
   @Commodity_Cost        DECIMAL (28, 10)                 
   @Broker_Fee            DECIMAL (28, 10)                 
   @Percentage_Locked     DECIMAL (5, 2)                 
                        
                        
OUTPUT PARAMETERS:                        
      Name              DataType          Default     Description                        
------------------------------------------------------------                        
                        
USAGE EXAMPLES:                   
                
select * from dbo.Contract where Contract_Id = 10961                
EXEC DBO.Contract_Lock_Dtl_Sel 82530                
                
                
------------------------------------------------------------                  
                    
AUTHOR INITIALS:                      
Initials Name                      
------------------------------------------------------------                      
AKR   Ashok Kumar Raju                
                    
                    
Initials Date  Modification                      
------------------------------------------------------------                      
AKR  2012-09-20  Created new sp (for POCO Project)                
******/                
                
CREATE PROCEDURE dbo.Contract_Lock_Dtl_Sel ( @Contract_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON ;                
                      
      SELECT
            cld.Contract_Lock_Dtl_Id
           ,cld.Contract_Id
           ,cld.Start_Dt
           ,cld.Expiration_Dt
           ,cast(cld.Nymex_Price AS VARCHAR) AS Nymex_Price
           ,cld.Percentage_Locked
           ,( isnull(cld.Nymex_Price, 0) * isnull(c.Heat_Rate, 0) ) + isnull(c.Retail_Adder, 0) AS Calculated_Cost
      FROM
            dbo.Contract_Lock_Dtl cld
            INNER JOIN dbo.CONTRACT c
                  ON cld.Contract_Id = c.CONTRACT_ID
      WHERE
            cld.Contract_Id = @Contract_Id                
                                  
END 
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Lock_Dtl_Sel] TO [CBMSApplication]
GO
