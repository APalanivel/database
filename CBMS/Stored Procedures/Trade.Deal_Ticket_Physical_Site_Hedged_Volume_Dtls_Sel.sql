SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Deal_Ticket_Physical_Site_Hedged_Volume_Dtls_Sel                
                          
Description:                          
          
                          
Input Parameters:                          
    Name				DataType    Default     Description                            
--------------------------------------------------------------------------------    
	@Deal_Ticket_Id		INT  
    @Start_Index		INT			1  
    @End_Index			INT			2147483647  
                          
Output Parameters:                                
 Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
                        
Usage Examples:                              
--------------------------------------------------------------------------------    
   
 EXEC Trade.Deal_Ticket_Physical_Site_Hedged_Volume_Dtls_Sel 131849  
 EXEC Trade.Deal_Ticket_Physical_Site_Hedged_Volume_Dtls_Sel 131856  
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR          2019-09-12  Created GRM-1491
	RR			2109-09-25	GRM-1530 - Added Site_Forecast_Volume to select list
							Modified New_TotalPercent_Of_RM_Forecast formula
	RR			18-10-2019	GRM-1575 Modified to retrun Site_Forecast_Volume as per deal ticket volume frequency                         
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Physical_Site_Hedged_Volume_Dtls_Sel]
     (
         @Deal_Ticket_Id INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
         , @Hedge_Volume_Over_Contract_Volume BIT = NULL
         , @Contract_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;


        DECLARE
            @Total_Cnt INT
            , @Client_Id INT
            , @Start_Dt DATE
            , @End_Dt DATE
            , @Commodity_Id INT
            , @Hedge_Type INT
            , @Uom_Type_Id INT
            , @Hedge_Type_Input VARCHAR(200)
            , @Hedge_Start_Dt DATE
            , @Hedge_End_Dt DATE
            , @Hedge_Mode_Type_Id INT
            , @Index_Id INT;

        DECLARE
            @Hedge_Mode_Type VARCHAR(200)
            , @Hedge_Index VARCHAR(200);

        DECLARE @RM_Group_Sites INT;

        DECLARE @Deal_Ticket_Frequency VARCHAR(25);

        CREATE TABLE #Site_Hedge_Config
             (
                 Client_Hier_Id INT
                 , Service_Month DATE
                 , Max_Hedge_Percentage DECIMAL(5, 2)
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Hedge_Type VARCHAR(25)
             );



        CREATE TABLE #Volumes
             (
                 Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(50)
                 , Contract_Vendor VARCHAR(500)
                 , Service_Month DATE
                 , Current_Deal_Volume DECIMAL(28, 0)
                 , Forecast_Volume DECIMAL(28, 0)
                 , Hedged_Volume DECIMAL(28, 0)
                 , Max_Hedge_Volume DECIMAL(28, 0)
                 , Max_Hedge_Percentage DECIMAL(5, 2)
                 , Do_Not_Hedge BIT
                 , Site_Not_Managed BIT
                 , Contract_Id INT
                 , Contract_Volume DECIMAL(28, 0)
                 , Is_Hedged BIT
                 , Site_Forecast_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #DT_Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Account_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
                 , Deal_Ticket_Id INT
                 , Contract_Id INT
             );

        CREATE TABLE #Hedged_Volumes
             (
                 Client_Hier_Id INT
                 , Deal_Month DATE
                 , Hedged_Volume DECIMAL(28, 0)
             );

        CREATE TABLE #DT_Participant_Sites
             (
                 [Site_Id] INT
                 , Contract_Id INT
             );

        DECLARE @Tbl_Is_DT_Trade_Complete AS TABLE
              (
                  Is_Complete_Order_Executed BIT
              );

        INSERT INTO @Tbl_Is_DT_Trade_Complete
             (
                 Is_Complete_Order_Executed
             )
        EXECUTE Trade.Deal_Ticket_Is_Complete_Order_Executed
            @Deal_Ticket_Id = @Deal_Ticket_Id;



        SELECT
            @Hedge_Mode_Type = e.ENTITY_NAME
            , @Hedge_Index = e2.ENTITY_NAME
            , @Deal_Ticket_Frequency = cd.Code_Value
            , @Hedge_Type = dt.Hedge_Type_Cd
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.PRICE_INDEX pi
                ON pi.PRICE_INDEX_ID = dt.Price_Index_Id
            INNER JOIN dbo.ENTITY e2
                ON pi.INDEX_ID = e2.ENTITY_ID
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_ID = dt.Hedge_Mode_Type_Id
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = dt.Deal_Ticket_Frequency_Cd
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;


        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;




        INSERT INTO #DT_Participant_Sites
             (
                 Site_Id
                 , Contract_Id
             )
        SELECT
            ch.Site_Id
            , c.CONTRACT_ID
        FROM
            dbo.CONTRACT c
            INNER JOIN Core.Client_Hier_Account chasupp
                ON chasupp.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN Core.Client_Hier_Account chautil
                ON chasupp.Meter_Id = chautil.Meter_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = chasupp.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Filter_Participant dtp
                ON dtp.Participant_Id = c.CONTRACT_ID
            INNER JOIN dbo.Code pcd
                ON dtp.Deal_Participant_Type_Cd = pcd.Code_Id
        WHERE
            dtp.Deal_Ticket_Id = @Deal_Ticket_Id
            AND pcd.Code_Value = 'Contract'
            AND chasupp.Account_Type = 'Supplier'
            AND chautil.Account_Type = 'Utility'
            AND EXISTS (SELECT  1 FROM  @Tbl_Is_DT_Trade_Complete WHERE Is_Complete_Order_Executed = 1)
        GROUP BY
            ch.Site_Id
            , c.CONTRACT_ID;

        INSERT INTO #DT_Participant_Sites
             (
                 Site_Id
                 , Contract_Id
             )
        SELECT
            ch.Site_Id
            , -1
        FROM
            dbo.Cbms_Sitegroup_Participant rgd
            INNER JOIN dbo.Code grp
                ON grp.Code_Id = rgd.Group_Participant_Type_Cd
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = rgd.Group_Participant_Id
            INNER JOIN Trade.Deal_Ticket_Filter_Participant dtp
                ON dtp.Participant_Id = rgd.Cbms_Sitegroup_Id
            INNER JOIN dbo.Code pcd
                ON dtp.Deal_Participant_Type_Cd = pcd.Code_Id
        WHERE
            dtp.Deal_Ticket_Id = @Deal_Ticket_Id
            AND pcd.Code_Value = 'RM Group'
            AND grp.Code_Value = 'Site'
            AND EXISTS (SELECT  1 FROM  @Tbl_Is_DT_Trade_Complete WHERE Is_Complete_Order_Executed = 1)
        GROUP BY
            ch.Site_Id;


        INSERT INTO #DT_Participant_Sites
             (
                 Site_Id
                 , Contract_Id
             )
        SELECT
            ch.Site_Id
            , -1
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Filter_Participant dtp
                ON dtp.Participant_Id = ch.Sitegroup_Id
            INNER JOIN dbo.Code pcd
                ON dtp.Deal_Participant_Type_Cd = pcd.Code_Id
        WHERE
            dtp.Deal_Ticket_Id = @Deal_Ticket_Id
            AND ch.Site_Id > 0
            AND pcd.Code_Value = 'Division'
            AND EXISTS (SELECT  1 FROM  @Tbl_Is_DT_Trade_Complete WHERE Is_Complete_Order_Executed = 1)
        GROUP BY
            ch.Site_Id;


        INSERT INTO #DT_Participant_Sites
             (
                 Site_Id
                 , Contract_Id
             )
        SELECT
            ch.Site_Id
            , vol.Contract_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON ch.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dtch.Deal_Ticket_Id = vol.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = vol.Client_Hier_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            ch.Site_Id
            , vol.Contract_Id;


        SELECT
            @Client_Id = Client_Id
            , @Start_Dt = Hedge_Start_Dt
            , @End_Dt = Hedge_End_Dt
            , @Commodity_Id = Commodity_Id
            , @Hedge_Type = Hedge_Type_Cd
            , @Hedge_Start_Dt = Hedge_Start_Dt
            , @Hedge_End_Dt = Hedge_End_Dt
            , @Commodity_Id = Commodity_Id
            , @Uom_Type_Id = Uom_Type_Id
        FROM
            Trade.Deal_Ticket
        WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id;



        SELECT
            @Hedge_Type_Input = ENTITY_NAME
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_ID = @Hedge_Type;

        INSERT INTO #Site_Hedge_Config
             (
                 Client_Hier_Id
                 , Service_Month
                 , Max_Hedge_Percentage
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type
             )
        SELECT
            ch.Client_Hier_Id
            , dd.DATE_D
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , MIN(chhc.Config_Start_Dt)
            , MAX(chhc.Config_End_Dt)
            , et.ENTITY_NAME
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND EXISTS (SELECT  1 FROM  #DT_Participant_Sites dps WHERE dps.Site_Id = ch.Site_Id)
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D
            , chhc.Max_Hedge_Pct
            , et.ENTITY_NAME;

        INSERT INTO #Site_Hedge_Config
             (
                 Client_Hier_Id
                 , Service_Month
                 , Max_Hedge_Percentage
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type
             )
        SELECT
            ch.Client_Hier_Id
            , dd.DATE_D
            , chhc.Max_Hedge_Pct AS Max_Hedge_Percentage
            , MIN(chhc.Config_Start_Dt)
            , MAX(chhc.Config_End_Dt)
            , et.ENTITY_NAME
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Site_Id > 0
            AND ch.Client_Id = @Client_Id
            AND chclient.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Start_Dt BETWEEN chhc.Config_Start_Dt
                              AND     chhc.Config_End_Dt
                    OR  @End_Dt BETWEEN chhc.Config_Start_Dt
                                AND     chhc.Config_End_Dt
                    OR  chhc.Config_Start_Dt BETWEEN @Start_Dt
                                             AND     @End_Dt
                    OR  chhc.Config_End_Dt BETWEEN @Start_Dt
                                           AND     @End_Dt)
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob1
                               WHERE
                                    siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob1.Commodity_Id = chob.Commodity_Id)
            AND EXISTS (SELECT  1 FROM  #DT_Participant_Sites dps WHERE dps.Site_Id = ch.Site_Id)
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D
            , chhc.Max_Hedge_Pct
            , et.ENTITY_NAME;

        INSERT INTO #DT_Hedged_Volumes
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Hedged_Volume
                 , Deal_Ticket_Id
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , CASE WHEN frq.Code_Value = 'Monthly' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                                 WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                             END) * dtcuc.CONVERSION_FACTOR)
                  WHEN frq.Code_Value = 'Daily' THEN MAX((CASE WHEN trdact.Code_Value = 'Buy' THEN dtv.Total_Volume
                                                              WHEN trdact.Code_Value = 'Sell' THEN -dtv.Total_Volume
                                                          END) * dtcuc.CONVERSION_FACTOR) * dd.DAYS_IN_MONTH_NUM
              END AS Hedged_Volume
            , dtv.Deal_Ticket_Id
            , dtv.Contract_Id
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtv
            INNER JOIN Trade.Deal_Ticket dt1
                ON dtv.Deal_Ticket_Id = dt1.Deal_Ticket_Id
                   AND  dt1.Commodity_Id = @Commodity_Id
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION dtcuc
                ON dtcuc.BASE_UNIT_ID = dtv.Uom_Type_Id
                   AND  dtcuc.CONVERTED_UNIT_ID = @Uom_Type_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON pridx.PRICE_INDEX_ID = dt1.Price_Index_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.ENTITY hmt
                ON dt1.Hedge_Mode_Type_Id = hmt.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtv.Client_Hier_Id = dtv.Client_Hier_Id
                   AND  dt1.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Trade.Trade_Price tp
                ON dtv.Trade_Price_Id = tp.Trade_Price_Id
            INNER JOIN dbo.Code trdact
                ON dt1.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.Code frq
                ON dt1.Deal_Ticket_Frequency_Cd = frq.Code_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            dt1.Client_Id = @Client_Id
            AND dtv.Deal_Ticket_Id <> @Deal_Ticket_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                #DT_Participant_Sites dps
                                INNER JOIN Core.Client_Hier ch
                                    ON ch.Site_Id = dps.Site_Id
                           WHERE
                                dtv.Client_Hier_Id = ch.Client_Hier_Id)
            AND dtv.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND dt1.Commodity_Id = @Commodity_Id
            AND (   (   @Hedge_Mode_Type = 'Index'
                        AND @Hedge_Index = 'Nymex'
                        AND hmt.ENTITY_NAME = 'Index')
                    OR  (   @Hedge_Mode_Type = 'Basis'
                            AND hmt.ENTITY_NAME = 'Basis')
                    OR  (   @Hedge_Mode_Type = 'Index'
                            AND @Hedge_Index <> 'Nymex'
                            AND hmt.ENTITY_NAME = 'Index'))
            AND tp.Trade_Price IS NOT NULL
        GROUP BY
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dd.DAYS_IN_MONTH_NUM
            , dtv.Deal_Ticket_Id
            , frq.Code_Value
            , dtv.Contract_Id;

        INSERT INTO #Hedged_Volumes
             (
                 Client_Hier_Id
                 , Deal_Month
                 , Hedged_Volume
             )
        SELECT
            Client_Hier_Id
            , Deal_Month
            , SUM(Hedged_Volume) Hedged_Volume
        FROM
            #DT_Hedged_Volumes
        GROUP BY
            Client_Hier_Id
            , Deal_Month;


        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Current_Deal_Volume
                 , Forecast_Volume
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Site_Not_Managed
                 , Contract_Id
                 , Contract_Volume
                 , Do_Not_Hedge
                 , Is_Hedged
                 , Site_Forecast_Volume
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name AS Contract_Vendor
            , dd.DATE_D
            , chvd.Total_Volume AS Current_Deal_Volume
            , MAX(afv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , MAX(dtv1.Hedged_Volume) AS Hedged_Volume
            , MAX((shc.Max_Hedge_Percentage * afv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , shc.Max_Hedge_Percentage
            , ch.Site_Not_Managed
            , dps.Contract_Id
            , CASE WHEN frq.ENTITY_NAME = 'Daily' THEN
                       MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM, 0))
                  ELSE MAX(ISNULL(cmv.VOLUME * mvcuc.CONVERSION_FACTOR, 0))
              END AS Contract_Volume
            , CASE WHEN shc.Hedge_Type = 'Does Not Hedge' THEN 1
                  ELSE 0
              END
            , CASE WHEN tp.Trade_Price IS NULL THEN 0
                  WHEN tp.Trade_Price IS NOT NULL THEN 1
              END
            , MAX(rchfv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Site_Forecast_Volume
        FROM
            #DT_Participant_Sites dps
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = dps.Site_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                   AND  cha.Account_Type = 'Utility'
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = dps.Contract_Id
            INNER JOIN Core.Client_Hier_Account suppacc
                ON cha.Meter_Id = suppacc.Meter_Id
                   AND  suppacc.Supplier_Contract_ID = dps.Contract_Id
                   AND  suppacc.Account_Type = 'Supplier'
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN #Site_Hedge_Config shc
                ON shc.Client_Hier_Id = ch.Client_Hier_Id
                   AND  shc.Service_Month = dd.DATE_D
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl chvd
                ON chvd.Deal_Ticket_Id = @Deal_Ticket_Id
                   AND  chvd.Client_Hier_Id = ch.Client_Hier_Id
                   AND  chvd.Account_Id = cha.Account_Id
                   AND  dd.DATE_D = chvd.Deal_Month
                   AND  chvd.Contract_Id = dps.Contract_Id
            LEFT JOIN Trade.RM_Account_Forecast_Volume afv
                ON ch.Client_Hier_Id = afv.Client_Hier_Id
                   AND  cha.Account_Id = afv.Account_Id
                   AND  afv.Commodity_Id = @Commodity_Id
                   AND  chvd.Deal_Month = afv.Service_Month
            LEFT JOIN Trade.RM_Account_Forecast_Volume afvsitecheck
                ON afvsitecheck.Client_Hier_Id = cha.Client_Hier_Id
                   AND  afvsitecheck.Commodity_Id = @Commodity_Id
                   AND  chvd.Deal_Month = afvsitecheck.Service_Month
                   AND  afvsitecheck.Service_Month BETWEEN shc.Config_Start_Dt
                                                   AND     shc.Config_End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN @Hedge_Start_Dt
                                                   AND     @Hedge_End_Dt
                   AND  afvsitecheck.Service_Month BETWEEN con.CONTRACT_START_DATE
                                                   AND     con.CONTRACT_END_DATE
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN #Hedged_Volumes dtv1
                ON chvd.Client_Hier_Id = dtv1.Client_Hier_Id
                   AND  dd.DATE_D = dtv1.Deal_Month
            LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                ON cha.Meter_Id = cmv.METER_ID
                   AND  suppacc.Supplier_Contract_ID = cmv.CONTRACT_ID
                   AND  CAST(cmv.MONTH_IDENTIFIER AS DATE) = dd.DATE_D
            LEFT JOIN dbo.ENTITY frq
                ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION mvcuc
                ON mvcuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                   AND  mvcuc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = chvd.Trade_Price_Id
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume rchfv
                ON ch.Client_Hier_Id = rchfv.Client_Hier_Id
                   AND  rchfv.Commodity_Id = @Commodity_Id
                   AND  chvd.Deal_Month = rchfv.Service_Month
        WHERE
            @Hedge_Type_Input = 'Physical'
            AND dd.DATE_D BETWEEN @Hedge_Start_Dt
                          AND     @Hedge_End_Dt
            AND (   @Contract_Id IS NULL
                    OR  dps.Contract_Id = @Contract_Id)
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , dd.DATE_D
            , con.ED_CONTRACT_NUMBER + '(' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_START_DATE, 106), ' ', '-')
              + ' - ' + REPLACE(CONVERT(VARCHAR(11), con.CONTRACT_END_DATE, 106), ' ', '-') + ')/'
              + suppacc.Account_Vendor_Name
            , ch.Site_Not_Managed
            , dps.Contract_Id
            , chvd.Total_Volume
            , shc.Max_Hedge_Percentage
            , frq.ENTITY_NAME
            , tp.Trade_Price
            , shc.Hedge_Type;

        INSERT INTO #Volumes
             (
                 Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Account_Id
                 , Account_Number
                 , Contract_Vendor
                 , Service_Month
                 , Current_Deal_Volume
                 , Forecast_Volume
                 , Hedged_Volume
                 , Max_Hedge_Volume
                 , Max_Hedge_Percentage
                 , Site_Not_Managed
                 , Contract_Id
                 , Contract_Volume
                 , Do_Not_Hedge
                 , Is_Hedged
                 , Site_Forecast_Volume
             )
        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , NULL
            , NULL
            , NULL
            , dd.DATE_D
            , chvd.Total_Volume AS Current_Deal_Volume
            , MAX(afv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
            , MAX(dtv1.Hedged_Volume) AS Hedged_Volume
            , MAX((shc.Max_Hedge_Percentage * afv.Forecast_Volume * cuc.CONVERSION_FACTOR) / 100) AS Max_Hedge_Volume
            , shc.Max_Hedge_Percentage
            , ch.Site_Not_Managed
            , NULL
            , NULL
            , CASE WHEN shc.Hedge_Type = 'Does Not Hedge' THEN 1
                  ELSE 0
              END
            , CASE WHEN tp2.Trade_Price IS NULL THEN 0
                  WHEN tp2.Trade_Price IS NOT NULL THEN 1
              END
            , MAX(afv.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Site_Forecast_Volume
        FROM
            #DT_Participant_Sites dps
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = dps.Site_Id
            CROSS JOIN meta.DATE_DIM dd
            LEFT JOIN #Site_Hedge_Config shc
                ON shc.Client_Hier_Id = ch.Client_Hier_Id
                   AND  shc.Service_Month = dd.DATE_D
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl chvd
                ON chvd.Deal_Ticket_Id = @Deal_Ticket_Id
                   AND  chvd.Client_Hier_Id = ch.Client_Hier_Id
                   AND  dd.DATE_D = chvd.Deal_Month
            LEFT JOIN Trade.RM_Client_Hier_Forecast_Volume afv
                ON afv.Client_Hier_Id = ch.Client_Hier_Id
                   AND  afv.Commodity_Id = @Commodity_Id
                   AND  chvd.Deal_Month = afv.Service_Month
                   AND  afv.Service_Month BETWEEN shc.Config_Start_Dt
                                          AND     shc.Config_End_Dt
                   AND  afv.Service_Month BETWEEN @Hedge_Start_Dt
                                          AND     @Hedge_End_Dt
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = afv.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN #Hedged_Volumes dtv1
                ON chvd.Client_Hier_Id = dtv1.Client_Hier_Id
                   AND  dd.DATE_D = dtv1.Deal_Month
            LEFT JOIN Trade.Trade_Price tp2
                ON tp2.Trade_Price_Id = chvd.Trade_Price_Id
        WHERE
            @Hedge_Type_Input = 'Financial'
            AND dd.DATE_D BETWEEN @Hedge_Start_Dt
                          AND     @Hedge_End_Dt
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , dd.DATE_D
            , ch.Site_Not_Managed
            , chvd.Contract_Id
            , chvd.Total_Volume
            , shc.Max_Hedge_Percentage
            , tp2.Trade_Price
            , shc.Hedge_Type;




        SELECT
            Client_Hier_Id
            , Site_Id
            , Site_name
            , Account_Id
            , Account_Number
            , Contract_Vendor
            , Service_Month
            , Current_Deal_Volume
            , CAST(ISNULL((Forecast_Volume), 0) AS DECIMAL(28, 0)) AS Forecast_Volume
            , CAST(ISNULL(Hedged_Volume, 0) AS DECIMAL(28, 0)) AS Hedged_Volume
            , CAST(ISNULL(((CASE WHEN Is_Hedged = 1 THEN ISNULL(Hedged_Volume, 0) + ISNULL(Current_Deal_Volume, 0)
                                WHEN Is_Hedged = 0 THEN ISNULL(Hedged_Volume, 0)
                            END) / NULLIF(Forecast_Volume, 0)) * 100, 0) AS DECIMAL(28, 0)) AS Percent_RM_Forecast_Hedged
            , CAST(ISNULL((Max_Hedge_Volume), 0) AS DECIMAL(28, 0)) AS Max_Hedge_Volume
            , ISNULL((Max_Hedge_Percentage), 0) AS Max_Hedge_Percentage
            , Do_Not_Hedge
            , Site_Not_Managed
            , Contract_Id
            , DENSE_RANK() OVER (ORDER BY
                                     Site_name
                                     , Account_Id
                                     , Contract_Id) Row_Num
            , Contract_Volume
            , Is_Hedged
            , Site_Forecast_Volume
        INTO
            #Final_Volumes
        FROM
            #Volumes
        WHERE
            (NULLIF(@Hedge_Volume_Over_Contract_Volume, 0) IS NULL)
            OR  (   @Hedge_Volume_Over_Contract_Volume = 1
                    AND Current_Deal_Volume > Contract_Volume)
        ORDER BY
            Service_Month;



        SELECT  @Total_Cnt = MAX(fv.Row_Num)FROM    #Final_Volumes fv;

        WITH Cte_Vol
        AS (
               SELECT
                    fv.Client_Hier_Id
                    , fv.Site_Id
                    , fv.Site_name
                    , fv.Service_Month
                    , SUM(ISNULL(Current_Deal_Volume, 0)) AS Current_Deal_Volume
                    , SUM(ISNULL(
                              CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN fv.Forecast_Volume
                                  WHEN @Deal_Ticket_Frequency = 'Daily' THEN fv.Forecast_Volume / dd.DAYS_IN_MONTH_NUM
                              END, 0)) AS Forecast_Volume
                    , MAX(ISNULL(CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN fv.Hedged_Volume
                                     WHEN @Deal_Ticket_Frequency = 'Daily' THEN fv.Hedged_Volume / dd.DAYS_IN_MONTH_NUM
                                 END, 0)) AS Hedged_Volume
                    , fv.Do_Not_Hedge
                    , fv.Site_Not_Managed
                    , ch.Sitegroup_Id
                    , ch.State_Id
                    , ch.Client_Id
                    , MAX(CAST(fv.Is_Hedged AS INT)) AS Is_Hedged
                    , MAX(CAST(CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN fv.Site_Forecast_Volume
                                   WHEN @Deal_Ticket_Frequency = 'Daily' THEN
                                       fv.Site_Forecast_Volume / dd.DAYS_IN_MONTH_NUM
                               END AS DECIMAL(28, 0))) AS Site_Forecast_Volume
                    , DENSE_RANK() OVER (ORDER BY
                                             fv.Site_name) Row_Num
               FROM
                    #Final_Volumes fv
                    INNER JOIN Core.Client_Hier ch
                        ON ch.Client_Hier_Id = fv.Client_Hier_Id
                    LEFT JOIN meta.DATE_DIM dd
                        ON dd.DATE_D = fv.Service_Month
               GROUP BY
                   fv.Client_Hier_Id
                   , fv.Site_Id
                   , fv.Site_name
                   , fv.Service_Month
                   , fv.Do_Not_Hedge
                   , fv.Site_Not_Managed
                   , ch.Sitegroup_Id
                   , ch.State_Id
                   , ch.Client_Id
           )
        SELECT
            fv.Client_Hier_Id
            , fv.Site_Id
            , fv.Site_name
            , fv.Service_Month
            , fv.Current_Deal_Volume
            , fv.Forecast_Volume
            , fv.Hedged_Volume
            , fv.Do_Not_Hedge
            , fv.Site_Not_Managed
            , fv.Sitegroup_Id
            , fv.State_Id
            , fv.Client_Id
            , fv.Row_Num
            , tc.Total_Cnt
            , fv.Hedged_Volume + fv.Current_Deal_Volume AS New_Total_Volume
            , CAST(ISNULL(((fv.Hedged_Volume + fv.Current_Deal_Volume) / NULLIF(fv.Site_Forecast_Volume, 0)) * 100, 0) AS DECIMAL(28, 0)) AS New_TotalPercent_Of_RM_Forecast
            , fv.Site_Forecast_Volume
        FROM
            Cte_Vol fv
            CROSS JOIN
            (   SELECT
                    MAX(Row_Num) AS Total_Cnt
                FROM
                    Cte_Vol) tc
        WHERE
            fv.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            fv.Row_Num;

        DROP TABLE #Site_Hedge_Config;
        DROP TABLE #Volumes;
        DROP TABLE #DT_Participant_Sites;
        DROP TABLE #Final_Volumes;
        DROP TABLE #DT_Hedged_Volumes;
        DROP TABLE #Hedged_Volumes;


    END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Physical_Site_Hedged_Volume_Dtls_Sel] TO [CBMSApplication]
GO
