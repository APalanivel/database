SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Contact_Info_Sel_By_Client_Vendor            
                        
 DESCRIPTION:                        
		This sp is used to get the Contact Info Based on the client and Vendor	                 
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Contact_Level_Cd					INT
,@Contact_Type_Cd					INT
,@Client_Id							INT
,@Vendor_Id							INT
,@Is_Active							INT
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

 --Client Level
 
  EXEC [dbo].[Contact_Info_Sel_By_Client_Vendor] 
      @Client_Id = 14525
     ,@Is_Active = 1
     ,@Contact_Level_Cd =102255
      ,@Contact_Type_Cd =102256
      
--Vendor level

  EXEC [dbo].[Contact_Info_Sel_By_Client_Vendor] 
	   @Vendor_Id = 14525
      ,@Is_Active = 1
      ,@Contact_Level_Cd =102255
      ,@Contact_Type_Cd =102257
--Account level

  EXEC [dbo].[Contact_Info_Sel_By_Client_Vendor] 
	   @Client_Id = 14525
      ,@Is_Active = 1
      ,@Contact_Level_Cd =102254
      ,@Contact_Type_Cd =102256
      
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 RKV                    Ravi kumar vegesna
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------
 RKV                   2017-01-21       Created for Invoice Tracking
******/

CREATE PROCEDURE [dbo].[Contact_Info_Sel_By_Client_Vendor]
      (
       @Contact_Level_Cd INT
      ,@Contact_Type_Cd INT
      ,@Client_Id VARCHAR(MAX) = NULL
      ,@Vendor_Id VARCHAR(MAX) = NULL
      ,@Is_Active INT )
AS
BEGIN                
      SET NOCOUNT ON;  

      SELECT
            ci.Contact_Info_Id
           ,ci.Email_Address
           ,ci.Fax_Number
           ,ci.Phone_Number
           ,ci.First_Name + '' + ci.Last_Name Username
           ,ci.Job_Position
           ,ci.Location
      FROM
            dbo.Contact_Info ci
      WHERE
            ci.Contact_Level_Cd = @Contact_Level_Cd
            AND ci.Contact_Type_Cd = @Contact_Type_Cd
            AND ci.Is_Active = @Is_Active
            AND ( @Client_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@Client_Id, ',') ufn
                                    INNER JOIN dbo.Client_Contact_Map cc
                                          ON cc.CLIENT_ID = ufn.Segments
                              WHERE
                                    cc.Contact_Info_Id = ci.Contact_Info_Id ) )
            AND ( @Vendor_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.ufn_split(@Vendor_Id, ',') ufn
                                    INNER JOIN dbo.Vendor_Contact_Map vc
                                          ON vc.VENDOR_ID = ufn.Segments
                              WHERE
                                    vc.Contact_Info_Id = ci.Contact_Info_Id ) )
      GROUP BY
            ci.Contact_Info_Id
           ,ci.Email_Address
           ,ci.Fax_Number
           ,ci.Phone_Number
           ,ci.First_Name
           ,ci.Last_Name
           ,ci.Job_Position
           ,ci.Location;
                                   
END;
;
GO
GRANT EXECUTE ON  [dbo].[Contact_Info_Sel_By_Client_Vendor] TO [CBMSApplication]
GO
