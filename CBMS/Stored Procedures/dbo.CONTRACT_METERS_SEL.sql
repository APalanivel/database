SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 cbms_prod.dbo.CONTRACT_METERS_SEL     
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name					DataType  Default Description    
------------------------------------------------------------    
	@Client_id			INT
	@State_Id			INT  
	@Commodity_Id		INT
	@City				VARCHAR(200)	= NULL
	@Account_Number		VARCHAR(50)		= NULL
	@Meter_Number		VARCHAR(50)		= NULL
	@Contract_Id		INT
	@utilityId			INT				= NULL  
	@divisionId			INT				= NULL  
	@siteName			VARCHAR(50)		= NULL 
	@siteId				INT				= NULL
	@sortIndex			VARCHAR(20)		= 'Asc'			Asc/Desc
	@sortColumn			VARCHAR(20)		= 'Site_name'	All displayed columns can be used for sorting
	@StartIndex			INT = 1  
	@EndIndex			INT = 2147483647	
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 
	EXEC CONTRACT_METERS_SEL 11231,28,290,null, null,null,79532,NULL,NULL,NULL,NULL,1,'Asc','Site_name',1
	EXEC CONTRACT_METERS_SEL 11231,28,290,null, null,null,79532,NULL,NULL,NULL,NULL,0,'Asc','Site_name',1,50
	EXEC CONTRACT_METERS_SEL  218,28,290,null,'59015',null,68358,NULL,1780,NULL,NULL,0,'Asc','Site_name',1
	EXEC CONTRACT_METERS_SEL  10078,16,290,null,null,null,37296,NULL,NULL,NULL,NULL,0,'Asc','Site_name',1,50
	
	EXEC CONTRACT_METERS_SEL  11506,15,290,null,null,null,82515,NULL,NULL,NULL,NULL,0,'Asc','Site_name',1,50
	EXEC CONTRACT_METERS_SEL  218,23,290,null,null,null,30115,NULL,NULL,NULL,NULL,0,'Asc','Site_name',1,50
	EXEC CONTRACT_METERS_SEL  235,36,291,null,null,null,72132,NULL,NULL,NULL,NULL,0,'Asc','Site_name',1,50
	EXEC CONTRACT_METERS_SEL  235,null,291,null,null,null,72132,NULL,NULL,NULL,NULL,0,'Asc','Site_name'

     
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	MGB			Bhaskaran Gopalakrishnan
	RR			Raghu Reddy    

MODIFICATIONS    
	Initials	Date			Modification    
------------------------------------------------------------    
				10/28/2009		Created    
	SSR			04/28/2010	    Added Input parameters for pagination and as per requirement,
								Converted Static to dynamic as user has ability to choose Sorting on Columns
								Used CTE for pagination concept, and the same we used again avoid ambiguous column error	
								Removed the reference of Client & SiteGroup, as necessary info can be fetch from Site table
								@City,@Account_Number,@Meter_Number defined as null, as it is not mandatory
	SKA			06/09/2010		Added one more input parameter Is_Contract_Meter which is calulated on some case conditions.
								Added two more columns METER_ASSOCIATION_DATE & METER_DISASSOCIATION_DATE
	SKA			06/10/2010		Added the join with contrcat table and show contract start/end date in case meter ASSOCIATION/DESSOCIATION both are null								
	RR			2015-09-15		Global Sourcing - Phase2 - Added new input optional parameters Meter_Type and Alternate_Account_Number and also
								added the same fields including country,ED_CONTRACT_NUMBER to select list 	
								Input parameter State_Id made as optional and also added Country.								
	NR			2017-05-30		SE2017-31(170)- Added Min_Service_Month,Max_Service_Month column in Output list.
	NR			2018-08-08		Add Contract - Added Is_Rolling_Meter in Out put list.	
	NR			2020-05-07		MAINT-10276 - Added filtered time band to get the max and min insted of account wise.	
******/
CREATE PROCEDURE [dbo].[CONTRACT_METERS_SEL]
    (
        @Client_id INT
        , @State_Id INT = NULL
        , @Commodity_Id INT
        , @City VARCHAR(200) = NULL
        , @Account_Number VARCHAR(50) = NULL
        , @Meter_Number VARCHAR(50) = NULL
        , @Contract_Id INT
        , @utilityId INT = NULL
        , @divisionId INT = NULL
        , @siteName VARCHAR(50) = NULL
        , @siteId INT = NULL
        , @Is_Contract_Meter BIT = 0
        , @sortIndex VARCHAR(20) = 'Asc'
        , @sortColumn VARCHAR(20) = 'Site_name'
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
        , @Meter_Type_Cd INT = NULL
        , @Alternate_Account_Number NVARCHAR(400) = NULL
        , @Country_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @Sql VARCHAR(8000);

        SET @Sql = ';WITH Cte_Contract_sel
			AS
			(
		SELECT     
			ch.Site_Id
			,ch.Site_name
			,cha.Account_Id
			,cha.Account_Number
			,cha.Meter_Id 
			,cha.Meter_Number
			,cha.Rate_Id 
			,cha.Rate_Name 
			,cha.Account_Vendor_Id AS vendor_id
			,cha.Account_Vendor_Name AS vendor_name
			,ch.City
			,ch.State_Id 
			,ch.State_Name    
			,chasupp.Display_Account_Number AS Supplier_Account_Number  
			,(CASE WHEN chasupp.Supplier_Meter_Association_Date IS NULL AND chasupp.Supplier_Meter_Disassociation_Date IS NULL THEN cont.CONTRACT_START_DATE ELSE chasupp.Supplier_Meter_Association_Date END) METER_ASSOCIATION_DATE
			,(CASE WHEN chasupp.Supplier_Meter_Association_Date IS NULL AND chasupp.Supplier_Meter_Disassociation_Date IS NULL THEN cont.CONTRACT_END_DATE ELSE chasupp.Supplier_Meter_Disassociation_Date END) METER_DISASSOCIATION_DATE
			,(CASE WHEN chasupp.METER_ID IS NULL THEN 0 ELSE 1 END) AS Is_Contract_Meter
			,mtrtyp.Code_Value AS Meter_Type
			,cha.Alternate_Account_Number
			,ch.Country_Name
			,cont.ED_CONTRACT_NUMBER
			,MIN(cuad.Service_Month) AS Min_Service_Month
            ,MAX(cuad.Service_Month) AS Max_Service_Month
			,MAX(CAST(Isnull(samm.Is_Rolling_Meter,0) AS INT)) As Is_Rolling_Meter
		FROM     
			Core.Client_Hier ch
                INNER JOIN Core.Client_Hier_Account cha
                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
				LEFT JOIN dbo.Code mtrtyp
					ON cha.Meter_Type_Cd = mtrtyp.Code_Id  
			LEFT JOIN (Core.Client_Hier_Account chasupp
							INNER JOIN dbo.Contract cont
                                    ON cont.CONTRACT_ID = chasupp.Supplier_Contract_ID
									INNER JOIN SUPPLIER_ACCOUNT_METER_MAP samm
									ON Samm.Meter_Id = chasupp.Meter_Id 
									AND samm.CONTRACT_ID = cont.CONTRACT_ID)
						ON cha.Meter_Id = chasupp.Meter_Id 
						AND chasupp.Account_Type = ''Supplier''
						AND chasupp.Supplier_Contract_ID = ' + CAST(@Contract_Id AS VARCHAR)
                   + ' 
		
		LEFT JOIN dbo.Cost_Usage_Account_Dtl cuad
                        ON chasupp.Account_Id = cuad.Account_ID
						AND cuad.Service_Month BETWEEN chasupp.Supplier_Account_begin_Dt AND chasupp.Supplier_Account_End_Dt
		
		WHERE cha.Account_Type = ''Utility'' and 
			ch.Client_Id = ' + CAST(@Client_id AS VARCHAR) + '
			AND cha.Commodity_Id = ' + CAST(@Commodity_Id AS VARCHAR);
        IF @State_Id IS NOT NULL
            SET @Sql = @Sql + ' AND (ch.State_Id = ' + CAST(@State_Id AS VARCHAR) + ')';
        IF @City IS NOT NULL
            SET @Sql = @Sql + ' AND (ch.City LIKE ''%' + @City + '%'')';
        IF @Account_Number IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Account_Number LIKE ''' + @Account_Number + '%'')';
        IF @Meter_Number IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Meter_Number LIKE ''' + @Meter_Number + '%'')';
        IF @siteName IS NOT NULL
            SET @Sql = @Sql + ' AND (ch.Site_name LIKE ''%' + @siteName + '%'')';
        IF @siteId IS NOT NULL
            SET @Sql = @Sql + ' AND (ch.Site_Id = ' + CAST(@siteId AS VARCHAR) + ')';
        IF @divisionId IS NOT NULL
            SET @Sql = @Sql + ' AND (ch.Sitegroup_Id = ' + CAST(@divisionId AS VARCHAR) + ')';
        IF @utilityId IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Account_Vendor_Id = ' + CAST(@utilityId AS VARCHAR) + ')';
        IF @Meter_Type_Cd IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Meter_Type_Cd = ' + CAST(@Meter_Type_Cd AS VARCHAR) + ')';
        IF @Alternate_Account_Number IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Alternate_Account_Number LIKE ''%' + @Alternate_Account_Number + '%'')';
        IF @Country_Id IS NOT NULL
            SET @Sql = @Sql + ' AND (cha.Meter_Country_Id = ' + CAST(@Country_Id AS VARCHAR) + ')';

        SET @Sql = @Sql
                   + '
					   GROUP BY
							ch.Site_Id
							,ch.Site_name
							,cha.Account_Id
							,cha.Account_Number
							,cha.Meter_Id 
							,cha.Meter_Number
							,cha.Rate_Id 
							,cha.Rate_Name 
							,cha.Account_Vendor_Id
							,cha.Account_Vendor_Name
							,ch.City
							,ch.State_Id 
							,ch.State_Name   
							,chasupp.Display_Account_Number
							,chasupp.METER_ID
							,chasupp.Supplier_Meter_Association_Date
							,chasupp.Supplier_Meter_Disassociation_Date
							,cont.CONTRACT_START_DATE
							,cont.CONTRACT_END_DATE
							,mtrtyp.Code_Value
							,cha.Alternate_Account_Number
							,ch.Country_Name
							,cont.ED_CONTRACT_NUMBER
						) ,
						CTE_Contract_final
							AS
							(
							SELECT 
								  site_id
								, site_name    
								, account_id    
								, account_number    
								, meter_id    
								, meter_number    
								, rate_id    
								, rate_name    
								, vendor_id    
								, vendor_name    
								, city    
								, state_id    
								, state_name    
								, Supplier_Account_Number    
								, Is_Contract_Meter	
								, METER_ASSOCIATION_DATE
								, METER_DISASSOCIATION_DATE
							    , Row_Num = ROW_NUMBER() OVER(ORDER BY ' + @sortColumn + SPACE(1) + @sortIndex
                   + ')  
								, Total = COUNT(1) OVER()  
								, Meter_Type	
								, Alternate_Account_Number
								, Country_Name	
								, ED_CONTRACT_NUMBER
								, Min_Service_Month
								, Max_Service_Month
								,Is_Rolling_Meter
							FROM
								Cte_Contract_sel
							WHERE Is_Contract_Meter IN ( CASE WHEN ' + CAST(@Is_Contract_Meter AS VARCHAR)
                   + ' = 1 THEN 1 ELSE Is_Contract_Meter ' + 'END ))';

        SET @Sql = @Sql
                   + '
									SELECT
										  site_id
										, site_name    
										, account_id    
										, account_number    
										, meter_id    
										, meter_number    
										, rate_id    
										, rate_name    
										, vendor_id    
										, vendor_name    
										, city    
										, state_id    
										, state_name    
										, Supplier_Account_Number
										, Is_Contract_Meter	
										, METER_ASSOCIATION_DATE
										, METER_DISASSOCIATION_DATE
										, Row_Num 
										, Total
										, Meter_Type
										, Alternate_Account_Number	
										, Country_Name
										, ED_CONTRACT_NUMBER
										, Min_Service_Month
								        , Max_Service_Month
										,Is_Rolling_Meter
									FROM 
										CTE_Contract_final
									WHERE 
										Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR) + ' AND '
                   + CAST(@EndIndex AS VARCHAR);
        EXEC (@Sql);
    --PRINT @SQL

    END;




    ;


GO


GRANT EXECUTE ON  [dbo].[CONTRACT_METERS_SEL] TO [CBMSApplication]
GO
