SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: dbo.Report_DE_Invoice_Level_Data_For_Carrefour_By_Account_Commodity_Service_Month

DESCRIPTION: 
			TO Load the invoice data for carrefour.

INPUT PARAMETERS:
	Name						DataType		Default			Description
------------------------------------------------------------------------------
	@Client_Hier_Id				INT
    @Account_Id					INT
    @Commodity_Id				INT
    @Currency_Unit_Name			VARCHAR(25)
    @Start_Date					DATE
    @End_Date					DATE

OUTPUT PARAMETERS:
	Name						DataType		Default			Description
------------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------------


EXEC Report_DE_Invoice_Level_Data_For_Carrefour_By_Account_Commodity_Service_Month
       @Client_Hier_Id = 32176
      ,@Account_Id  = 159845
      ,@Commodity_Id  = 290
      ,@Currency_Unit_Name='EUR'
      ,@Start_Date  = '2015-01-01'
      ,@End_Date  = '2015-12-01'
  
EXEC dbo.Report_DE_Invoice_Level_Data_For_Carrefour_By_Account_Commodity_Service_Month   
      @Client_Hier_Id = 14408  
     ,@Account_Id = 53783  
     ,@Commodity_Id = 290  
     ,@Start_Date = '1/1/2012'  
     ,@End_Date = '12/1/2012'  
     ,@Currency_Unit_Name = 'EUR' 



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------
	RKV			Ravi Kumar Vegesna

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------------------------
	RKV			2016-07-18	Created
	RKV         2016-10-18  Maint-4506,Changed the date format from mm/dd/yyyy to dd/mm/yyyy
	HG			2020-03-16	MAINT-10060, modified the code to convert the usage and demand to default UOM Kwh & Kw.
******/
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Level_Data_For_Carrefour_By_Account_Commodity_Service_Month]
    (
        @Client_Hier_Id     INT
       ,@Account_Id         INT
       ,@Commodity_Id       INT
       ,@Currency_Unit_Name VARCHAR (25)
       ,@Start_Date         DATE
       ,@End_Date           DATE
    )
AS
BEGIN
	SET NOCOUNT ON 
    DECLARE
        @SQL_STR           VARCHAR (MAX)
       ,@Total_Rows        INT
       ,@Current_Row_Num   INT          = 1
       ,@Aggregation_Type  VARCHAR (25)
       ,@Bucket_Master_Id  INT
       ,@pivot_String      VARCHAR (MAX)
       ,@Currency_Group_Id INT
       ,@Currency_Unit_Id  INT;


    CREATE TABLE #Selected_Category_Buckets
        (
            Bucket_Master_Id    INT
           ,Bucket_Name         VARCHAR (200)
           ,Display_Bucket_Name VARCHAR (200)
           ,sno                 INT IDENTITY (1, 1)
        );


    CREATE TABLE #Usage_Data
        (
            Service_Month    DATE
           ,Bucket_Master_Id INT
           ,Bucket_Value     DECIMAL (28, 10)
           ,Uom_Type_Id      INT
        );

    CREATE TABLE #Selected_Child_Buckets
        (
            Bucket_Master_Id    INT
           ,Bucket_Name         VARCHAR (200)
           ,Display_Bucket_Name VARCHAR (200)
           ,sno                 INT IDENTITY (1, 1)
        );

    CREATE TABLE #Invoice_List
        (
            Cu_Invoice_Id            INT
           ,Service_Month            DATE
           ,Invoice_Currency_Unit_id INT
           ,Row_Num                  INT NULL
        );
    CREATE CLUSTERED INDEX ix_#IL ON #Invoice_List (Cu_Invoice_Id, Row_Num);


    CREATE TABLE #Bucket_Values
        (
            Category_Bucket_Master_Id INT
           ,Child_Bucket_Master_Id    INT
           ,Service_Month             DATE
           ,Bucket_Value              DECIMAL (32, 16)
           ,Uom_Type_Id               INT
           ,Aggregation_Type          VARCHAR (25)
           ,No_Of_Months              INT
        );
    CREATE TABLE #Bucket_Category_Aggregation_Rule
        (
            Commodity_Id                 INT
           ,Category_Bucket_Master_Id    INT
           ,Category_Bucket_Name         VARCHAR (200)
           ,Child_Bucket_Master_Id       INT
           ,Child_Bucket_Name            VARCHAR (200)
           ,Bucket_Type                  VARCHAR (25)
           ,Aggregation_Type             VARCHAR (25)
           ,Aggregation_Level            VARCHAR (25)
           ,Priority_Order               INT
           ,Is_Aggregate_Category_Bucket BIT
           ,Default_Uom_Type_Id          INT
        );

    DECLARE
        @Batch_Id              INT
       ,@Count                 INT
       ,@Forecast_Data_Type_Cd INT
       ,@Recalc_Data_Type_Cd   INT;


    SELECT @Currency_Group_Id = ch.Client_Currency_Group_Id
    FROM
        Core.Client_Hier ch
    WHERE
        ch.Client_Hier_Id = @Client_Hier_Id;

    SELECT @Currency_Unit_Id = CURRENCY_UNIT_ID
    FROM
        dbo.CURRENCY_UNIT cu
    WHERE
        CURRENCY_UNIT_NAME = @Currency_Unit_Name;

    INSERT INTO #Selected_Category_Buckets
    (
        Bucket_Master_Id
       ,Bucket_Name
       ,Display_Bucket_Name
    )
    SELECT
        Bucket_Master_Id
       ,Bucket_Name
       ,Bucket_Name
    FROM
        dbo.Bucket_Master bm
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = bm.Bucket_Type_Cd
    WHERE
        Bucket_Name IN ( 'Off-Peak Usage', 'On-Peak Usage', 'Total Usage' )
        AND Commodity_Id = @Commodity_Id
        AND bt.Code_Value = 'Determinant';

    INSERT INTO #Selected_Child_Buckets
    (
        Bucket_Master_Id
       ,Bucket_Name
       ,Display_Bucket_Name
    )
    SELECT
        Bucket_Master_Id
       ,Bucket_Name
       ,CASE
            WHEN Bucket_Name = 'Demand - On-Peak' THEN
                'On-Peak Demand'
            WHEN Bucket_Name = 'Demand - Off-Peak' THEN
                'Off-Peak Demand'
            WHEN Bucket_Name = 'Demand - Actual' THEN
                'Actual Demand'
        END AS Display_Bucket_Name
    FROM
        dbo.Bucket_Master bm
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = bm.Bucket_Type_Cd
    WHERE
        Bucket_Name IN ( 'Demand - On-Peak', 'Demand - Off-Peak', 'Demand - Actual' )
        AND Commodity_Id = @Commodity_Id
        AND bt.Code_Value = 'Determinant';


    INSERT INTO #Selected_Category_Buckets
    (
        Bucket_Master_Id
       ,Bucket_Name
       ,Display_Bucket_Name
    )
    SELECT
        Bucket_Master_Id
       ,Bucket_Name
       ,'Invoiced expenses' Display_Bucket_Name
    FROM
        dbo.Bucket_Master bm
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = bm.Bucket_Type_Cd
    WHERE
        Bucket_Name IN ( 'Total Cost' )
        AND Commodity_Id = @Commodity_Id
        AND bt.Code_Value = 'Charge';


    INSERT INTO #Invoice_List
    (
        Cu_Invoice_Id
       ,Service_Month
       ,Invoice_Currency_Unit_id
    )
    SELECT
        ci.CU_INVOICE_ID
       ,sm.SERVICE_MONTH
       ,ci.CURRENCY_UNIT_ID
    FROM
        dbo.CU_INVOICE_SERVICE_MONTH sm
        INNER JOIN dbo.CU_INVOICE ci
            ON ci.CU_INVOICE_ID = sm.CU_INVOICE_ID
    WHERE
        ci.IS_PROCESSED = 1
        AND ci.IS_REPORTED = 1
        AND ci.IS_DNT = 0
        AND ci.IS_DUPLICATE = 0
        AND sm.SERVICE_MONTH BETWEEN @Start_Date
                             AND     @End_Date
        AND sm.Account_ID = @Account_Id
        AND EXISTS (
                       SELECT 1
                       FROM
                           dbo.CU_INVOICE_DETERMINANT cid
                           INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                               ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                       WHERE
                           cid.CU_INVOICE_ID = ci.CU_INVOICE_ID
                           AND da.ACCOUNT_ID = sm.Account_ID
                           AND cid.COMMODITY_TYPE_ID = @Commodity_Id
                   )
    GROUP BY ci.CU_INVOICE_ID
            ,sm.SERVICE_MONTH
            ,ci.CURRENCY_UNIT_ID;

    INSERT INTO #Invoice_List
    (
        Cu_Invoice_Id
       ,Service_Month
       ,Invoice_Currency_Unit_id
    )
    SELECT
        ci.CU_INVOICE_ID
       ,sm.SERVICE_MONTH
       ,ci.CURRENCY_UNIT_ID
    FROM
        dbo.CU_INVOICE_SERVICE_MONTH sm
        INNER JOIN dbo.CU_INVOICE ci
            ON ci.CU_INVOICE_ID = sm.CU_INVOICE_ID
        INNER JOIN dbo.CU_INVOICE_CHARGE cic
            ON cic.CU_INVOICE_ID = ci.CU_INVOICE_ID
    WHERE
        ci.IS_PROCESSED = 1
        AND ci.IS_REPORTED = 1
        AND ci.IS_DNT = 0
        AND ci.IS_DUPLICATE = 0
        AND sm.Account_ID = @Account_Id
        AND sm.SERVICE_MONTH BETWEEN @Start_Date
                             AND     @End_Date
        AND EXISTS (
                       SELECT 1
                       FROM
                           dbo.CU_INVOICE_CHARGE cic
                           INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT ca
                               ON ca.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
                       WHERE
                           cic.CU_INVOICE_ID = ci.CU_INVOICE_ID
                           AND ca.ACCOUNT_ID = sm.Account_ID
                           AND cic.COMMODITY_TYPE_ID = @Commodity_Id
                   )
        AND NOT EXISTS (
                           SELECT 1
                           FROM
                               #Invoice_List il
                           WHERE
                               il.Cu_Invoice_Id = ci.CU_INVOICE_ID
                               AND il.Service_Month = sm.SERVICE_MONTH
                       )
    GROUP BY ci.CU_INVOICE_ID
            ,sm.SERVICE_MONTH
            ,ci.CURRENCY_UNIT_ID;




    INSERT INTO #Bucket_Category_Aggregation_Rule
    (
        Commodity_Id
       ,Category_Bucket_Master_Id
       ,Category_Bucket_Name
       ,Child_Bucket_Master_Id
       ,Child_Bucket_Name
       ,Bucket_Type
       ,Aggregation_Type
       ,Aggregation_Level
       ,Priority_Order
       ,Is_Aggregate_Category_Bucket
       ,Default_Uom_Type_Id
    )
    SELECT
        catbm.Commodity_Id
       ,catbm.Bucket_Master_Id
       ,catbm.Bucket_Name AS Category_Bucket_Name
       ,chldbm.Bucket_Master_Id Child_Bucket_Master_Id
       ,chldbm.Bucket_Name AS Child_Bucket_Name
       ,bt.Code_Value AS Bucket_Type
       ,CASE
            WHEN agg.Code_Value = 'Recent' THEN
                'Max'
            ELSE
                agg.Code_Value
        END AS Aggregation_Type
       ,lvl.Code_Value AS Agg_Level
       ,bcr.Priority_Order
       ,bcr.Is_Aggregate_Category_Bucket
       ,catbm.Default_Uom_Type_Id
    FROM
        dbo.Bucket_Category_Rule bcr
        INNER JOIN dbo.Bucket_Master catbm
            ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
        INNER JOIN dbo.Bucket_Master chldbm
            ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
        INNER JOIN #Selected_Category_Buckets sb
            ON sb.Bucket_Master_Id = catbm.Bucket_Master_Id
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = catbm.Bucket_Type_Cd
        INNER JOIN dbo.Code agg
            ON agg.Code_Id = bcr.Aggregation_Type_CD
        INNER JOIN dbo.Code lvl
            ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
    WHERE
        lvl.Code_Value = 'Invoice'
        AND catbm.Is_Active = 1
        AND chldbm.Is_Active = 1
        AND (
                catbm.Bucket_Name <> 'Load Factor' -- This was done to exclude the load factor calculation based on the total usage.
                OR (
                       catbm.Bucket_Name = 'Load Factor'
                       AND bcr.Priority_Order = 1
                   )
            )
    ORDER BY catbm.Bucket_Name
            ,bcr.Priority_Order;

    INSERT INTO #Bucket_Category_Aggregation_Rule
    (
        Commodity_Id
       ,Category_Bucket_Master_Id
       ,Category_Bucket_Name
       ,Child_Bucket_Master_Id
       ,Child_Bucket_Name
       ,Bucket_Type
       ,Aggregation_Type
       ,Aggregation_Level
       ,Priority_Order
       ,Is_Aggregate_Category_Bucket
       ,Default_Uom_Type_Id
    )
    SELECT
        catbm.Commodity_Id
       ,catbm.Bucket_Master_Id
       ,catbm.Bucket_Name AS Category_Bucket_Name
       ,chldbm.Bucket_Master_Id Child_Bucket_Master_Id
       ,chldbm.Bucket_Name AS Child_Bucket_Name
       ,bt.Code_Value AS Bucket_Type
       ,CASE
            WHEN agg.Code_Value = 'Recent' THEN
                'Max'
            ELSE
                agg.Code_Value
        END AS Aggregation_Type
       ,lvl.Code_Value AS Agg_Level
       ,bcr.Priority_Order
       ,bcr.Is_Aggregate_Category_Bucket
       ,catbm.Default_Uom_Type_Id
    FROM
        dbo.Bucket_Category_Rule bcr
        INNER JOIN dbo.Bucket_Master catbm
            ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
        INNER JOIN dbo.Bucket_Master chldbm
            ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
        INNER JOIN #Selected_Child_Buckets sb
            ON sb.Bucket_Master_Id = chldbm.Bucket_Master_Id
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = catbm.Bucket_Type_Cd
        INNER JOIN dbo.Code agg
            ON agg.Code_Id = bcr.Aggregation_Type_CD
        INNER JOIN dbo.Code lvl
            ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
    WHERE
        lvl.Code_Value = 'Invoice'
        AND catbm.Is_Active = 1
        AND chldbm.Is_Active = 1
        AND (
                catbm.Bucket_Name <> 'Load Factor' -- This was done to exclude the load factor calculation based on the total usage.
                OR (
                       catbm.Bucket_Name = 'Load Factor'
                       AND bcr.Priority_Order = 1
                   )
            )
    ORDER BY catbm.Bucket_Name
            ,bcr.Priority_Order;

    INSERT INTO #Bucket_Category_Aggregation_Rule
    (
        Commodity_Id
       ,Category_Bucket_Master_Id
       ,Category_Bucket_Name
       ,Child_Bucket_Master_Id
       ,Child_Bucket_Name
       ,Bucket_Type
       ,Aggregation_Type
       ,Aggregation_Level
       ,Priority_Order
       ,Is_Aggregate_Category_Bucket
    )
    SELECT
        catbm.Commodity_Id
       ,tcbm.Bucket_Master_Id
       ,tcbm.Bucket_Name AS Category_Bucket_Name
       ,chldbm.Bucket_Master_Id Child_Bucket_Master_Id
       ,chldbm.Bucket_Name AS Child_Bucket_Name
       ,bt.Code_Value AS Bucket_Type
       ,CASE
            WHEN agg.Code_Value = 'Recent' THEN
                'Max'
            ELSE
                agg.Code_Value
        END AS Aggregation_Type
       ,lvl.Code_Value AS Agg_Level
       ,bcr.Priority_Order
       ,bcr.Is_Aggregate_Category_Bucket
    FROM
        dbo.Bucket_Category_Rule bcr
        INNER JOIN dbo.Bucket_Master catbm
            ON catbm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
        INNER JOIN dbo.Bucket_Master chldbm
            ON chldbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
        INNER JOIN dbo.Code bt
            ON bt.Code_Id = catbm.Bucket_Type_Cd
        INNER JOIN dbo.Code agg
            ON agg.Code_Id = bcr.Aggregation_Type_CD
        INNER JOIN dbo.Code lvl
            ON lvl.Code_Id = bcr.CU_Aggregation_Level_Cd
        INNER JOIN dbo.Bucket_Master tcbm
            ON tcbm.Bucket_Name = 'Total Cost'
               AND tcbm.Commodity_Id = catbm.Commodity_Id
        INNER JOIN dbo.Code tcbt
            ON tcbt.Code_Id = tcbm.Bucket_Type_Cd
    WHERE
        lvl.Code_Value = 'Invoice'
        AND tcbt.Code_Value = 'Charge'
        AND catbm.Bucket_Name = 'Taxes'
        AND catbm.Is_Active = 1
        AND chldbm.Is_Active = 1
        AND (
                catbm.Bucket_Name <> 'Load Factor' -- This was done to exclude the load factor calculation based on the total usage.
                OR (
                       catbm.Bucket_Name = 'Load Factor'
                       AND bcr.Priority_Order = 1
                   )
            )
    ORDER BY tcbm.Bucket_Name
            ,bcr.Priority_Order;




    WITH Cte_Invoice_Row_Num
    AS (
       SELECT
           Cu_Invoice_Id
          ,Service_Month
          ,DENSE_RANK() OVER (PARTITION BY Cu_Invoice_Id
                              ORDER BY Service_Month DESC
                             ) Row_Num -- Using Dense rank so that if more than one invoice recieved for a single month both are considered.
       FROM
           #Invoice_List il
       )
    UPDATE il
    SET il.Row_Num = rn.Row_Num
    FROM
        #Invoice_List il
        INNER JOIN Cte_Invoice_Row_Num rn
            ON rn.Cu_Invoice_Id = il.Cu_Invoice_Id
               AND rn.Service_Month = il.Service_Month;




    INSERT INTO #Bucket_Values
    (
        Category_Bucket_Master_Id
       ,Child_Bucket_Master_Id
       ,Service_Month
       ,Bucket_Value
       ,Uom_Type_Id
       ,Aggregation_Type
       ,No_Of_Months
    )
    SELECT
        dat.Category_Bucket_Master_Id
       ,dat.Child_Bucket_Master_Id
       ,dat.Service_Month
       ,dat.Bucket_Value
       ,dat.Uom_Type_Id
       ,dat.Aggregation_Type
       ,dat.Month_Cnt
    FROM (
             SELECT
                 cat.Category_Bucket_Master_Id
                ,cat.Category_Bucket_Name
                ,cid.Bucket_Master_Id AS Child_Bucket_Master_Id
                ,il.Service_Month
                ,(CASE
                      WHEN NULLIF(da.Determinant_Expression, '') IS NULL THEN
                (CONVERT(
                            DECIMAL (28, 10)
                           ,REPLACE(
                                       REPLACE(
                                                  REPLACE(
                                                             REPLACE(
                                                                        REPLACE(
                                                                                   REPLACE(
                                                                                              (CASE
                                                                                                   WHEN (PATINDEX(
                                                                                                                     '%[0-9]-'
                                                                                                                    ,cid.DETERMINANT_VALUE
                                                                                                                 )
                                                                                                        ) > 0 THEN
                                                                                                       STUFF(
                                                                                                                cid.DETERMINANT_VALUE
                                                                                                               ,PATINDEX(
                                                                                                                            '%[0-9]-'
                                                                                                                           ,cid.DETERMINANT_VALUE
                                                                                                                        )
                                                                                                                + 1
                                                                                                               ,1
                                                                                                               ,''
                                                                                                            )
                                                                                                   ELSE
                                                                                                       cid.DETERMINANT_VALUE
                                                                                               END
                                                                                              )
                                                                                             ,','
                                                                                             ,''
                                                                                          )
                                                                                  ,'0-'
                                                                                  ,0
                                                                               )
                                                                       ,'+'
                                                                       ,''
                                                                    )
                                                            ,'$'
                                                            ,''
                                                         )
                                                 ,')'
                                                 ,''
                                              )
                                      ,'('
                                      ,''
                                   )
                        )
                )
                      ELSE
                          da.Determinant_Value 
                  END
                 ) * cuc.CONVERSION_FACTOR AS Bucket_Value
                ,cat.Default_Uom_Type_Id AS Uom_Type_Id
                ,DENSE_RANK() OVER (PARTITION BY
                                        bcr.Category_Bucket_Master_Id
                                       ,il.Service_Month
                                    ORDER BY bcr.Category_Bucket_Master_Id
                                            ,bcr.Priority_Order
                                   ) AS Priority_Order
                ,bcr.Aggregation_Type
                ,mc.Month_Cnt
             FROM
                 dbo.CU_INVOICE_DETERMINANT cid
                 INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT da
                     ON da.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
                 INNER JOIN dbo.CU_INVOICE ci
                     ON ci.CU_INVOICE_ID = cid.CU_INVOICE_ID
                 INNER JOIN #Invoice_List il
                     ON il.Cu_Invoice_Id = cid.CU_INVOICE_ID
                        AND il.Row_Num = 1
                 INNER JOIN #Bucket_Category_Aggregation_Rule bcr
                     ON cid.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
                 INNER JOIN #Bucket_Category_Aggregation_Rule cat
                     ON cat.Category_Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                 INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                     ON cuc.BASE_UNIT_ID = cid.UNIT_OF_MEASURE_TYPE_ID
                        AND cuc.CONVERTED_UNIT_ID = cat.Default_Uom_Type_Id
                 OUTER APPLY (
                                 SELECT COUNT(DISTINCT sm.SERVICE_MONTH)
                                 FROM
                                     dbo.CU_INVOICE_SERVICE_MONTH sm
                                 WHERE
                                     sm.Account_ID = @Account_Id
                                     AND sm.CU_INVOICE_ID = cid.CU_INVOICE_ID
                             ) mc(Month_Cnt)
             WHERE
                 da.ACCOUNT_ID = @Account_Id
                 AND cid.COMMODITY_TYPE_ID = @Commodity_Id
         ) dat
    WHERE
        dat.Priority_Order = 1 -- First available priority buckets will be selected
    GROUP BY dat.Category_Bucket_Master_Id
            ,dat.Child_Bucket_Master_Id
            ,dat.Service_Month
            ,dat.Bucket_Value
            ,dat.Uom_Type_Id
            ,dat.Aggregation_Type
            ,dat.Month_Cnt;



    INSERT INTO #Bucket_Values
    SELECT
        dat.Category_Bucket_Master_Id
       ,dat.Child_Bucket_Master_Id
       ,dat.Service_Month
       ,dat.Bucket_Value
       ,dat.Uom_Type_Id
       ,dat.Aggregation_Type
       ,dat.Month_Cnt
    FROM (
             SELECT
                 cat.Category_Bucket_Master_Id
                ,cat.Category_Bucket_Name
                ,cic.Bucket_Master_Id AS Child_Bucket_Master_Id
                ,il.Service_Month
                ,(CASE
                      WHEN NULLIF(cica.Charge_Expression, '') IS NULL THEN
                (CONVERT(
                            DECIMAL (28, 10)
                           ,REPLACE(
                                       REPLACE(
                                                  REPLACE(
                                                             REPLACE(
                                                                        REPLACE(
                                                                                   REPLACE(
                                                                                              (CASE
                                                                                                   WHEN (PATINDEX(
                                                                                                                     '%[0-9]-'
                                                                                                                    ,cic.CHARGE_VALUE
                                                                                                                 )
                                                                                                        ) > 0 THEN
                                                                                                       STUFF(
                                                                                                                cic.CHARGE_VALUE
                                                                                                               ,PATINDEX(
                                                                                                                            '%[0-9]-'
                                                                                                                           ,cic.CHARGE_VALUE
                                                                                                                        )
                                                                                                                + 1
                                                                                                               ,1
                                                                                                               ,''
                                                                                                            )
                                                                                                   ELSE
                                                                                                       cic.CHARGE_VALUE
                                                                                               END
                                                                                              )
                                                                                             ,','
                                                                                             ,''
                                                                                          )
                                                                                  ,'0-'
                                                                                  ,0
                                                                               )
                                                                       ,'+'
                                                                       ,''
                                                                    )
                                                            ,'$'
                                                            ,''
                                                         )
                                                 ,')'
                                                 ,''
                                              )
                                      ,'('
                                      ,''
                                   )
                        )
                )
                      ELSE
                          cica.Charge_Value
                  END * cuc.CONVERSION_FACTOR
                 ) Bucket_Value
                ,NULL Uom_Type_Id
                ,DENSE_RANK() OVER (PARTITION BY
                                        bcr.Category_Bucket_Master_Id
                                       ,il.Service_Month
                                    ORDER BY bcr.Category_Bucket_Master_Id
                                            ,bcr.Priority_Order
                                   ) AS Priority_Order
                ,bcr.Aggregation_Type
                ,mc.Month_Cnt
             FROM
                 dbo.CU_INVOICE_CHARGE cic
                 INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cica
                     --da
                     ON cica.CU_INVOICE_CHARGE_ID = cic.CU_INVOICE_CHARGE_ID
                 INNER JOIN dbo.CU_INVOICE ci
                     ON ci.CU_INVOICE_ID = cic.CU_INVOICE_ID
                 INNER JOIN #Invoice_List il
                     ON il.Cu_Invoice_Id = cic.CU_INVOICE_ID
                        AND il.Row_Num = 1
                 INNER JOIN #Bucket_Category_Aggregation_Rule bcr
                     ON cic.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
                 INNER JOIN #Bucket_Category_Aggregation_Rule cat
                     ON cat.Category_Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                 LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                     ON cuc.CURRENCY_GROUP_ID = @Currency_Group_Id
                        AND cuc.BASE_UNIT_ID = il.Invoice_Currency_Unit_id
                        AND cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
                        AND cuc.CONVERSION_DATE = il.Service_Month
                 OUTER APPLY (
                                 SELECT COUNT(DISTINCT sm.SERVICE_MONTH)
                                 FROM
                                     dbo.CU_INVOICE_SERVICE_MONTH sm
                                 WHERE
                                     sm.Account_ID = @Account_Id
                                     AND sm.CU_INVOICE_ID = cic.CU_INVOICE_ID
                             ) mc(Month_Cnt)
             WHERE
                 cica.ACCOUNT_ID = @Account_Id
                 AND cic.COMMODITY_TYPE_ID = @Commodity_Id
         ) dat
    WHERE
        dat.Priority_Order = 1 -- First available priority buckets will be selected
    GROUP BY dat.Category_Bucket_Master_Id
            ,dat.Child_Bucket_Master_Id
            ,dat.Service_Month
            ,dat.Bucket_Value
            ,dat.Uom_Type_Id
            ,dat.Aggregation_Type
            ,dat.Month_Cnt;




    SELECT @Total_Rows = MAX(sno)
    FROM
        #Selected_Category_Buckets;



    WHILE @Current_Row_Num <= @Total_Rows
    BEGIN

        SELECT
            @Bucket_Master_Id = bat.Bucket_Master_Id
           ,@Aggregation_Type = MAX(bv.Aggregation_Type)
        FROM
            #Selected_Category_Buckets bat
            INNER JOIN #Bucket_Values bv
                ON bv.Category_Bucket_Master_Id = bat.Bucket_Master_Id
        WHERE
            bat.sno = @Current_Row_Num
        GROUP BY bat.Bucket_Master_Id;



        SET @SQL_STR
            = '
            INSERT INTO #Usage_Data
							(Service_Month
							,Bucket_Master_Id
							,Bucket_Value
							,Uom_Type_Id)
						SELECT
							bv.Service_Month
							,bv.Category_Bucket_Master_Id
							,' + @Aggregation_Type
              + '( ((bv.Bucket_Value/bv.No_Of_Months)) ) AS Bucket_Value
							,bv.Uom_Type_Id
						FROM
							#Bucket_Values bv
						WHERE
							bv.Category_Bucket_Master_Id = ' + CAST(@Bucket_Master_Id AS VARCHAR (20))
              + '
						GROUP BY
							bv.Service_Month
							,bv.Category_Bucket_Master_Id
							,bv.Uom_Type_Id';



        EXEC (@SQL_STR);





        SET @Current_Row_Num = @Current_Row_Num + 1;

    END;

    SELECT @Total_Rows = MAX(sno)
    FROM
        #Selected_Child_Buckets;


    SET @Current_Row_Num = 1;

    WHILE @Current_Row_Num <= @Total_Rows
    BEGIN

        SELECT
            @Bucket_Master_Id = bat.Bucket_Master_Id
           ,@Aggregation_Type = MAX(bv.Aggregation_Type)
        FROM
            #Selected_Child_Buckets bat
            INNER JOIN #Bucket_Values bv
                ON bv.Child_Bucket_Master_Id = bat.Bucket_Master_Id
        WHERE
            bat.sno = @Current_Row_Num
        GROUP BY bat.Bucket_Master_Id;



        SET @SQL_STR
            = '
            INSERT INTO #Usage_Data
             (Service_Month
             ,Bucket_Master_Id
             ,Bucket_Value
             ,Uom_Type_Id)
            SELECT
                        bv.Service_Month
                       ,bv.Child_Bucket_Master_Id
                       ,' + @Aggregation_Type
              + '( ((bv.Bucket_Value/bv.No_Of_Months)) ) AS Bucket_Value
                       ,bv.Uom_Type_Id
                  FROM
                        #Bucket_Values bv
                  WHERE
						bv.Child_Bucket_Master_Id = ' + CAST(@Bucket_Master_Id AS VARCHAR (20))
              + '
                  GROUP BY
                       bv.Service_Month
                       ,bv.Child_Bucket_Master_Id
                       ,bv.Uom_Type_Id';



        EXEC (@SQL_STR);





        SET @Current_Row_Num = @Current_Row_Num + 1;

    END;


    INSERT INTO #Selected_Category_Buckets
    (
        Bucket_Master_Id
       ,Bucket_Name
       ,Display_Bucket_Name
    )
    SELECT
        -1
       ,'Calculated Expenses'
       ,'Calculated Expenses';


    INSERT INTO #Usage_Data
    (
        Service_Month
       ,Bucket_Master_Id
       ,Bucket_Value
       ,Uom_Type_Id
    )
    SELECT
        il.Service_Month
       ,-1
       ,rh.GRAND_TOTAL * cuc.CONVERSION_FACTOR
       ,NULL
    FROM
        dbo.Cu_Invoice_Recalc_Response cirr
        INNER JOIN dbo.RECALC_HEADER rh
            ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
        INNER JOIN #Invoice_List il
            ON il.Cu_Invoice_Id = rh.CU_INVOICE_ID
               AND il.Row_Num = 1
        LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
            ON cuc.CURRENCY_GROUP_ID = @Currency_Group_Id
               AND cuc.BASE_UNIT_ID = il.Invoice_Currency_Unit_id
               AND cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
               AND cuc.CONVERSION_DATE = il.Service_Month
    WHERE
        rh.ACCOUNT_ID = @Account_Id
        AND rh.Commodity_Id = @Commodity_Id
    GROUP BY il.Service_Month
            ,rh.GRAND_TOTAL
            ,cuc.CONVERSION_FACTOR;



    SELECT DISTINCT
           @pivot_String = STUFF((
                                     SELECT ',[' + CONVERT(VARCHAR (12), dd.DATE_D, 103) + ']'
                                     FROM
                                         meta.DATE_DIM dd
                                     WHERE
                                         dd.DATE_D BETWEEN @Start_Date
                                                   AND     @End_Date
                                     FOR XML PATH('')
                                 )
                                ,1
                                ,1
                                ,''
                                );


    SELECT
        @SQL_STR
        = 'SELECT
						Bucket_Name,' + @pivot_String
          + ' 
				  FROM
						(SELECT	sb.Display_Bucket_Name as Bucket_Name,
								CONVERT(VARCHAR(12),Service_Month,103) Service_Month,
								ud.Bucket_Value 
						FROM #Usage_Data ud 
						right Outer JOIN (SELECT Bucket_Master_Id,Bucket_Name,Display_Bucket_Name FROM #Selected_Category_Buckets 
											union all 
										  SELECT Bucket_Master_Id,Bucket_Name,Display_Bucket_Name FROM #Selected_Child_Buckets) sb 	
								on sb.Bucket_Master_Id = ud.Bucket_Master_Id ) as t
						PIVOT( MAX(Bucket_Value ) FOR Service_Month IN ( ' + @pivot_String + '  ) ) AS pvt ';





    EXECUTE (@SQL_STR);




    DROP TABLE #Bucket_Values;
    DROP TABLE #Bucket_Category_Aggregation_Rule;
    DROP TABLE #Selected_Category_Buckets;
    DROP TABLE #Invoice_List;
    DROP TABLE #Selected_Child_Buckets;
    DROP TABLE #Usage_Data;

END;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Level_Data_For_Carrefour_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
