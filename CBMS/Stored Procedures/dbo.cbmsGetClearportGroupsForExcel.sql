SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	CBMS.dbo.cbmsGetClearportGroupsForExcel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsGetClearportGroupsForExcel

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/29/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.	        	
	
******/


CREATE PROCEDURE [dbo].[cbmsGetClearportGroupsForExcel]
AS 
       BEGIN
             SET nocount ON
             DECLARE @indexid INT
             DECLARE @run VARCHAR(500)
             DECLARE @filepath VARCHAR(1000)
             DECLARE @filename VARCHAR(200)
             DECLARE @sheetname VARCHAR(100)
             DECLARE @basetablesql VARCHAR(500)
             DECLARE @tablesql VARCHAR(500)
             SET @filename = 'marktest.xls'
             SET @sheetname = 'test'
             SET @filepath = '\\01summit_master\Data\Energy Operations\Energy DB\'
             SET @basetablesql = '(clearport_index VarChar (200) , index_detail_date DateTime , clearport_index_month DateTime , index_detail_value Decimal (28,16))'
             DECLARE clearport CURSOR fast_forward
                     FOR SELECT
                            clearport_index_id
                         FROM
                            dbo.clearport_index
                         WHERE
                            clearport_index_id IN ( 1, 2, 3 )

             OPEN clearport
             FETCH NEXT FROM clearport INTO @indexid--,@filename,@sheetname
             WHILE @@fetch_status = 0
                   BEGIN
                         IF @indexid = 1 
                            SELECT
                                @sheetname = 'test'
                         IF @indexid = 2 
                            SELECT
                                @sheetname = 'test2'
                         IF @indexid = 3 
                            SELECT
                                @sheetname = 'test3'

                         SET @tablesql = 'CREATE TABLE ' + @sheetname + ' '
                             + @basetablesql
                         SET @run = 'DTSRun /S "(local)" /N "Clearport Index Export" /A "filepath":"8"=" '
                             + @filepath + @filename + ' " /A "indexid":"3"="'
                             + CAST(@indexid AS VARCHAR)
                             + '" /A "sheetname":"8"="' + @sheetname
                             + '" /A "tablesql":"8"="' + @tablesql + '" /E'
                         EXEC master..xp_cmdshell @run
                         SELECT
                            @run
                         FETCH NEXT FROM clearport INTO @indexid--,@filename,@sheetname

                   END

             CLOSE clearport
             DEALLOCATE clearport

       END
GO
GRANT EXECUTE ON  [dbo].[cbmsGetClearportGroupsForExcel] TO [CBMSApplication]
GO
