
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME: dbo.User_Security_Role_DEL_For_Client_Map   

DESCRIPTION:     
Deleting Clients Corporate Security role from User_Security_Role table (Only External Users) whenever the client is deleted from the Portfolio.
	
INPUT PARAMETERS:    
Name		           DataType        Default         Description    
----------------------------------------------------------------    
@Client_Id               INT
               
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    
SELECT
      *
FROM
      dbo.User_Security_Role USR
      JOIN dbo.User_Info UI
            ON USR.User_Info_Id = UI.User_Info_Id
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      SR.Client_Id = 102
      AND Is_Corporate = 1
      AND UI.Access_Level = 1
      
BEGIN TRAN
EXEC dbo.User_Security_Role_DEL_For_Client_Map 102

SELECT
      *
FROM
      dbo.User_Security_Role USR
      JOIN dbo.User_Info UI
            ON USR.User_Info_Id = UI.User_Info_Id
      JOIN dbo.Security_Role SR
            ON USR.Security_Role_Id = SR.Security_Role_Id
WHERE
      SR.Client_Id = 102
      AND Is_Corporate = 1
      AND UI.Access_Level = 1

ROLLBACK

AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RK          Rajesh Kasoju
 
MODIFICATIONS     
Initials	       Date		       Modification    
------------------------------------------------------------    
 RK                07/03/2012        Created    
 CPE				2012-04-06		Added the condition to check that the user is a user of the portfolio of the affected client.(MAINT - 1256)
    
******/

CREATE PROCEDURE [dbo].[User_Security_Role_DEL_For_Client_Map] ( @Client_Id INT )
AS 
BEGIN     
     
      SET NOCOUNT ON ;

      DELETE
            USR
      FROM
            dbo.User_Security_Role USR
            JOIN dbo.Security_Role SR
                  ON USR.Security_Role_Id = SR.Security_Role_Id
            JOIN User_Info UI
                  ON USR.User_Info_Id = UI.User_Info_Id
            JOIN core.Client_Hier CH
                  ON SR.CLIENT_ID = CH.Client_Id
                     AND UI.CLIENT_ID = CH.Portfolio_Client_Id
      WHERE
            UI.Access_Level = 1
            AND SR.Client_Id = @Client_Id
            AND SR.Is_Corporate = 1
            AND CH.Sitegroup_Id = 0
  
END 


;
GO

GRANT EXECUTE ON  [dbo].[User_Security_Role_DEL_For_Client_Map] TO [CBMSApplication]
GO
