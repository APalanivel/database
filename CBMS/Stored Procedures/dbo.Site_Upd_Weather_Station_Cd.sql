SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.Site_Upd_Weather_Station_Cd
    
DESCRIPTION:   
		Update Weather Station Codes and Time zones of a Site.
  
INPUT PARAMETERS:        
 Name									DataType			Default			Description        
----------------------------------------------------------------------------------------- 
 @Site_Id								INT
 @Weahter_Station_Cd					VARCHAR(10)
 @Is_System_Generated_Weather_Station	BIT	
 @Time_Zone								NVARCHAR(50)
 @Is_System_Generated_Timezone			BIT 

OUTPUT PARAMETERS:        
 Name									DataType			Default			Description        
----------------------------------------------------------------------------------------- 
    
USAGE EXAMPLES:    
----------------------------------------------------------------------------------------- 
BEGIN TRAN    
    
 Exec dbo.Site_Upd_Weather_Station_Cd 27, 'KUXL'
  
ROLLBACK TRAN    
    
AUTHOR INITIALS:    
 Initials	Name  
----------------------------------------------------------------------------------------- 
 CPE		Chaitanya Panduga Eshwar
 NR			Narayana Reddy
  
MODIFICATIONS  
  
 Initials	Date			Modification  
----------------------------------------------------------------------------------------- 
 CPE		2013-01-15		Created 
 NR			2017-02-21		MAINT-4853 Added Time zones and weather station paraamters.
 RKV        2018-09-18      MAINT-7708	update the timezone to "GMT Standard Time" for the sites where the Country is "United Kingdom"
******/

CREATE PROCEDURE [dbo].[Site_Upd_Weather_Station_Cd]
     (
         @Site_Id INT
         , @Weather_Station_Cd VARCHAR(10) = NULL
         , @Is_System_Generated_Weather_Station BIT = 1
         , @Time_Zone NVARCHAR(50) = NULL
         , @Is_System_Generated_Timezone BIT = 1
     )
AS
    BEGIN

        SET NOCOUNT ON;


        DECLARE @Time_Zone_Id INT;




        SELECT
            @Time_Zone_Id = tz.Time_Zone_Id
        FROM
            dbo.Time_Zone tz
        WHERE
            tz.Time_Zone = @Time_Zone;


        SELECT
            @Time_Zone_Id = tz.Time_Zone_Id
        FROM
            Core.Client_Hier ch
            CROSS JOIN dbo.Time_Zone tz
        WHERE
            ch.Site_Id = @Site_Id
            AND ch.Country_Name = 'United Kingdom'
            AND tz.Time_Zone = 'GMT Standard Time'
            AND @Is_System_Generated_Timezone = 1;


        UPDATE
            s
        SET
            s.Weather_Station_Code = CASE WHEN s.Weather_Station_Code IS NULL
                                               OR   @Weather_Station_Cd IS NULL
                                               OR   s.Weather_Station_Code <> @Weather_Station_Cd THEN
                                              @Weather_Station_Cd
                                         ELSE s.Weather_Station_Code
                                     END
            , s.Is_System_Generated_Weather_Station = @Is_System_Generated_Weather_Station
            , s.Time_Zone_Id = CASE WHEN @Time_Zone_Id IS NULL
                                         OR s.Time_Zone_Id IS NULL
                                         OR s.Time_Zone_Id <> @Time_Zone_Id THEN @Time_Zone_Id
                                   ELSE s.Time_Zone_Id
                               END
            , s.Is_System_Generated_Timezone = @Is_System_Generated_Timezone
        FROM
            dbo.SITE s
        WHERE
            s.SITE_ID = @Site_Id;



    END;




    ;


GO

GRANT EXECUTE ON  [dbo].[Site_Upd_Weather_Station_Cd] TO [CBMSApplication]
GO
