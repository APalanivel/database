SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  
    dbo.Invoice_Data_Quality_Log_INS  
 
DESCRIPTION:  
    Used to insert log details into Data_Quality_Log table

INPUT PARAMETERS:    
      Name							 DataType						  Default     Description    
------------------------------------------------------------------------------------------------------   
	@Invoice_Data_Quality_Log_INS     Invoice_Data_Quality_Log_INS]
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------------    
    
USAGE EXAMPLES:   
------------------------------------------------------------    
 
DECLARE @Tvp_Invoice_Data_Quality_Log Tvp_Invoice_Data_Quality_Log
INSERT INTO @Tvp_Invoice_Data_Quality_Log
     (
          Account_ID
                 , Commodity_ID
                 , Service_Month
                 , UOM_Id
                 , Currency_Unit_Id
				 , Baseline_UOM_Id
                 , Test_Description
                 , Test_Category
				 ,Execution_User_Info_Id
                 , Execution_Ts
            
                 , Is_Failure
				 , Is_Closed
                 , Current_Value
                 , Baseline_Value
                 , Is_Inclusive
                 , Is_Multiple_Condition
                 , Baseline_Desc
                 , Baseline_Start_Service_Month
                 , Baseline_End_Service_Month
                 , Partition_Key
     )
VALUES
		    ( 100            -- Account_ID - int
		     , 290            -- Commodity_ID - int
		     , GETDATE()    -- Service_Month - date
		     , null            -- UOM_Cd - int
		     , null          -- UOM_Label - nvarchar(255)
			 ,null
		     , N'Tot'          -- Test_Description - nvarchar(max)
		     , N'Total USage'          -- Test_Category - nvarchar(255)
			 ,49				-- Execution_User_Info_Id -INT
			 , GETDATE()    -- Execution_Ts - datetime
		
		     , 0         -- Is_Failure - bit
			 , 0
		     , NULL         -- Current_Value - decimal(16, 4)
		     , NULL         -- Baseline_Value - decimal(16, 4)
		     , 0         -- Is_Inclusive - bit
		     , 0         -- Is_Multiple_Condition - bit
		     , N'working'          -- Baseline_Desc - nvarchar(100)
		     , GETDATE()    -- Baseline_Start_Service_Month - date
		     , GETDATE()    -- Baseline_End_Service_Month - date
		     , GETDATE()    -- Partition_Key - date
		    )

Exec dbo.Invoice_Data_Quality_Log_INS
    @Tvp_Invoice_Data_Quality_Log = @Tvp_Invoice_Data_Quality_Log
Select  * from  LOGDB.dbo.Invoice_Data_Quality_Log
Select  * from  dbo.Invoice_Data_Quality_Log
	
AUTHOR INITIALS:

Initials Name
------------------------------------------------------------
NR		Narayana Reddy


Initials  Date		    Modification  
------------------------------------------------------------  
NR	      28/03/2019	 Created
			
******/

CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Log_Ins]
    (
        @Tvp_Invoice_Data_Quality_Log AS Tvp_Invoice_Data_Quality_Log READONLY
        , @User_Info_Id INT
        , @Cu_Invoice_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Max_Execution_Group_No INT;

        SELECT
            @Max_Execution_Group_No = ISNULL(MAX(idql.Execution_Group_No), 0) + 1
        FROM
            LOGDB.dbo.Invoice_Data_Quality_Log idql
        WHERE
            idql.Cu_Invoice_Id = @Cu_Invoice_Id;



        INSERT INTO LOGDB.dbo.Invoice_Data_Quality_Log
             (
                 Cu_Invoice_Id
                 , Account_ID
                 , Commodity_ID
                 , Service_Month
                 , UOM_Id
                 , Currency_Unit_Id
                 , Baseline_UOM_Id
                 , Test_Description
                 , Data_Category
                 , Execution_User_Info_Id
                 , Execution_Ts
                 , Execution_Group_No
                 , Is_Failure
                 , Is_Closed
                 , Current_Value
                 , Baseline_Value
                 , Is_Inclusive
                 , Is_Multiple_Condition
                 , Baseline_Desc
                 , Baseline_Start_Service_Month
                 , Baseline_End_Service_Month
                 , Partition_Key
             )
        SELECT
            @Cu_Invoice_Id AS Cu_Invoice_Id
            , tidql.Account_ID
            , tidql.Commodity_ID
            , tidql.Service_Month
            , tidql.UOM_Id
            , tidql.Currency_Unit_Id
            , tidql.Baseline_UOM_Id
            , tidql.Test_Description
            , tidql.Test_Category
            , @User_Info_Id AS Execution_User_Info_Id
            , tidql.Execution_Ts
            , @Max_Execution_Group_No AS Execution_Group_No
            , tidql.Is_Failure
            , tidql.Is_Closed
            , tidql.Current_Value
            , tidql.Baseline_Value
            , tidql.Is_Inclusive
            , tidql.Is_Multiple_Condition
            , tidql.Baseline_Desc
            , tidql.Baseline_Start_Service_Month
            , tidql.Baseline_End_Service_Month
            , tidql.Partition_Key
        FROM
            @Tvp_Invoice_Data_Quality_Log tidql;


    END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Log_Ins] TO [CBMSApplication]
GO
