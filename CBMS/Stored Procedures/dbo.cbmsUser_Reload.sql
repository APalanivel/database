SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******  
NAME:  
 
 dbo.cbmsUser_Reload
 
 DESCRIPTION:   
 
	This procedure used to identify the user who logged into the system. 
 
 INPUT PARAMETERS:

 Name			DataType	Default		Description
------------------------------------------------------------
 @MyAccountId 	INT

 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

 
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsUser_Reload 49
	EXEC dbo.cbmsUser_Reload 24554
	EXEC dbo.cbmsUser_Reload 3526

AUTHOR INITIALS:

Initials	Name
------------------------------------------------------------
 HG			Harihara Suthan G

MODIFICATIONS   
 Initials	Date			Modification
------------------------------------------------------------
 HG			11/09/2010		Comments header added
							Removed the passcode column from the select list as it is not used by the applcation , changes made as a prt dv/sv users pwd expiration enhancement.

******/
CREATE PROCEDURE dbo.cbmsUser_Reload
(
	@MyAccountId INT
)
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		ui.user_info_id
		,ui.username
		,ui.queue_id
		,ui.first_name
		,ui.middle_name
		,ui.last_name
		,ui.email_address
		,ui.is_history
		,ui.access_level
		,ui.client_id
		,ui.division_id
		,ui.site_id
	FROM
		dbo.USER_INFO ui
	WHERE
		ui.USER_INFO_ID = @MyAccountId
		AND ui.IS_HISTORY = 0

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUser_Reload] TO [CBMSApplication]
GO
