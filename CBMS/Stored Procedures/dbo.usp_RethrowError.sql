SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.usp_RethrowError

DESCRIPTION:
	Standard error handling process to be used in try catch blocks
	
INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@SiteGroup_Id	INT				


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRY
	RaisError('This is a Test Error', 16, 1)
END TRY 
BEGIN CATCH 
	EXEC usp_RethrowError
END CATCH


-- Custom Message
BEGIN TRY
	RaisError('This is a Test Error', 16, 1)
END TRY 
BEGIN CATCH 
	EXEC usp_RethrowError 'This is a Custom Message to show with Error'
END CATCH


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			06/30/2009	Created from BOL example
	CMH			07/15/2009	Modified to allow custom error messages

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.usp_RethrowError 
(
	@CustomMessage	Varchar(2500) = NULL
)
AS
    
-- Return if there is no error information to retrieve.
    IF ERROR_NUMBER() IS NULL
        RETURN;

    DECLARE 
        @ErrorMessage    NVARCHAR(4000),
        @ErrorNumber     INT,
        @ErrorSeverity   INT,
        @ErrorState      INT,
        @ErrorLine       INT,
        @ErrorProcedure  NVARCHAR(200);

    -- Assign variables to error-handling functions that 
    -- capture information for RAISERROR.
    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

    -- Build the message string that will contain original
    -- error information.
    SET @CustomMessage = @CustomMessage + CHAR(10)
    SELECT @ErrorMessage = ISNULL(@CustomMessage, '') + 
        N' Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();

    -- Raise an error: msg_str parameter of RAISERROR will contain
    -- the original error information.
    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
GO
GRANT EXECUTE ON  [dbo].[usp_RethrowError] TO [CBMSApplication]
GO
