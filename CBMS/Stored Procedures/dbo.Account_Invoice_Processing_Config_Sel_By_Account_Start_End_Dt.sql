SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Sel_By_Account_Start_End_Dt                    
                  
Description:                  
			Showing Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Id  								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


EXEC dbo.Account_Invoice_Processing_Config_Sel_By_Account_Start_End_Dt
    @Account_Id = 1740579
    , @Start_Dt = '2005-01-01'
    , @End_Dt = null


                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Sel_By_Account_Start_End_Dt]
    (
        @Account_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            aipc.Account_Invoice_Processing_Config_Id
            , aipc.Account_Id
            , aipc.Config_Start_Dt
            , aipc.Config_End_Dt
            , aipc.Watch_List_Group_Info_Id
            , gi.GROUP_NAME
            , aipc.Processing_Instruction_Category_Cd
            , Instruction_Category.Code_Value AS Instruction_Category
            , aipc.Processing_Instruction
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_By
            , aipc.Last_Change_Ts
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.Code Instruction_Category
                ON Instruction_Category.Code_Id = aipc.Processing_Instruction_Category_Cd
            LEFT JOIN dbo.GROUP_INFO gi
                ON gi.GROUP_INFO_ID = aipc.Watch_List_Group_Info_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = aipc.Updated_User_Id
        WHERE
            aipc.Account_Id = @Account_Id
            AND aipc.Is_Active = 1
            AND (   aipc.Config_Start_Dt BETWEEN @Start_Dt
                                         AND     ISNULL(@End_Dt, '9999-12-31')
                    OR  ISNULL(aipc.Config_End_Dt, '9999-12-31') BETWEEN @Start_Dt
                                                                 AND     ISNULL(@End_Dt, '9999-12-31')
                    OR  @Start_Dt BETWEEN aipc.Config_Start_Dt
                                  AND     ISNULL(aipc.Config_End_Dt, '9999-12-31')
                    OR  ISNULL(@End_Dt, '9999-12-31') BETWEEN aipc.Config_Start_Dt
                                                      AND     ISNULL(aipc.Config_End_Dt, '9999-12-31'));


    END;



GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Sel_By_Account_Start_End_Dt] TO [CBMSApplication]
GO
