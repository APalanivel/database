SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.UPDATE_RM_REPORT_P
@userId varchar(10),
@sessionId varchar(10),
@reportListId int,
@reportXml text

as
	set nocount on
insert into RM_REPORT(REPORT_LIST_ID, REPORT_XML, LAST_UPDATED_DATE) values(@reportListId, @reportXml, getdate())
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_REPORT_P] TO [CBMSApplication]
GO
