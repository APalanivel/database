SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.SR_RFP_GET_UTILITIES_UNDER_STATE_P 
@userId varchar,
@sessionId varchar,
@stateId int,
@commodity int

as
	set nocount on
select	v.VENDOR_ID,
	v.VENDOR_NAME 

from 	VENDOR v,
	VENDOR_STATE_MAP vsm ,
	entity venType ,
 	vendor_commodity_map map

where 	vsm.STATE_ID = @stateId  and
	v.VENDOR_ID = vsm.VENDOR_ID and 
	v.vendor_type_id = venType.entity_id and
	v.vendor_id=map.vendor_id and
	map.commodity_type_id = @commodity 
	and venType.entity_type = 155
	AND venType.entity_name = 'Utility'

order by v.vendor_name
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_UTILITIES_UNDER_STATE_P] TO [CBMSApplication]
GO
