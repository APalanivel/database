SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_COMMUNICATIONS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@sessionId     	int       	          	
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_GET_COMMUNICATIONS_P

@userId int,
@sessionId int,
@rfpId int

AS
set nocount on
declare @entityId int
select @entityId =( SELECT ENTITY_ID FROM ENTITY where entity_name='Communications' and entity_type=100)

SELECT I.CBMS_DOC_ID,  C.CBMS_IMAGE_ID, I.CONTENT_TYPE, U.USERNAME, C.UPLOADED_DATE, C.COMMENTS
FROM 
	SR_RFP_COMMUNICATIONS C, CBMS_IMAGE I, USER_INFO U
WHERE 
	C.SR_RFP_ID=@rfpId
	AND 
	C.CBMS_IMAGE_ID = I.CBMS_IMAGE_ID
	AND
        C.UPLOADED_BY = U.USER_INFO_ID
	AND
	I.CBMS_IMAGE_TYPE_ID = @entityId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_COMMUNICATIONS_P] TO [CBMSApplication]
GO
