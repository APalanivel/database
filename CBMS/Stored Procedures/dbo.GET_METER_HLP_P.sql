SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE dbo.GET_METER_HLP_P
	@meterId int
	as
	begin
	set nocount on
	
		select 	a.HISTORIC_LOAD_PROFILE_ID ,
			a.METER_ID,a.UNIT_ID,
			a.HLP_FROM_DATE,
			a.HLP_TO_DATE,
			a.USAGE_COST,
			a.HLP_VOLUME,
			b.entity_name 
		from    historic_load_profile a
			join entity b on b.entity_id = a.UNIT_ID
			and a.meter_id = @meterId 

	end
GO
GRANT EXECUTE ON  [dbo].[GET_METER_HLP_P] TO [CBMSApplication]
GO
