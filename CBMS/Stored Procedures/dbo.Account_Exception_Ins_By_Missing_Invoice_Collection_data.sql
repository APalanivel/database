SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Account_Exception_Ins_By_Missing_Invoice_Collection_data              
                          
 DESCRIPTION:                          
   To raise invoice exceptions on collection of invoice data            
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
-------------------------------------------------------------------------------------------------------------        
@Account_Id      INT  
@User_Info_Id     INT     
  
                             
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
-------------------------------------------------------------------------------------------------------------        
                          
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------------------------------                              
      
BEGIN TRAN  
SELECT * FROM Account_Exception WHERE ACCOUNT_ID=1342230  
  EXEC dbo.Account_Exception_Ins_By_Missing_Invoice_Collection_data_1  
      @Account_Id = 28826  
      ,@User_Info_Id=71920  
      ,@Invoice_Collection_Is_Chased=1  
      ,@Invoice_Collection_Is_Chased_Changed=1  
SELECT * FROM Account_Exception WHERE ACCOUNT_ID=1342230        
ROLLBACK    
      
BEGIN TRAN  
SELECT * FROM Account_Exception WHERE ACCOUNT_ID=1342210  
  EXEC dbo.Account_Exception_Ins_By_Missing_Invoice_Collection_data   
      @Account_Id = 1342210  
      ,@User_Info_Id=49  
SELECT * FROM Account_Exception WHERE ACCOUNT_ID=1342210        
ROLLBACK    
      
  exec Account_Exception_Ins_By_Missing_Invoice_Collection_data  
    @Invoice_Collection_Account_Config_Id_Input=182  
    ,@Account_Id=62087  
    ,@User_Info_Id=69806  
      
   SELECT  
      CHA.COMMODITY_ID,cha.*  
FROM  
      core.Client_Hier_Account cha  
      INNER JOIN core.Client_Hier ch  
            ON cha.Client_Hier_Id = ch.Client_Hier_Id  
WHERE  
      ch.Client_Name = 'tyson'  
ORDER BY CHA.Account_Type        
  
  SELECT * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=1342210  
  
  
 EXEC dbo.Account_Exception_Ins_By_Missing_Invoice_Collection_data   
      @Invoice_Collection_Account_Config_Id_Input=1342354  
      ,@User_Info_Id=69806  
        
                            
 AUTHOR INITIALS:          
         
 Initials              Name          
-------------------------------------------------------------------------------------------------------------                        
 SP                    Sandeep Pigilam
 NM					   Nagaraju Muppa            
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
-------------------------------------------------------------------------------------------------------------        
 SP                    2016-12-08       Created   for Invoice tracking  
 SP						2017-04-18  Hot fix, for MAINT-5195.
 NM						2020-03-02	MAINT-9628 updated Invoice_Collection_Account_Config_Id as NUll 
						if config_id not available in Invoice_Collection_Account_Config table to delete exception in exception table.    
               
                         
******/

CREATE PROCEDURE [dbo].[Account_Exception_Ins_By_Missing_Invoice_Collection_data]
     (
         @Account_Id INT = NULL
         , @User_Info_Id INT
         , @Invoice_Collection_Is_Chased BIT = 0
         , @Invoice_Collection_Is_Chased_Changed BIT = 0
         , @Invoice_Collection_Account_Config_Id_Input INT = NULL
         , @Previous_IC_Officer_User_Id INT = NULL
         , @Ic_Supplier_Account_Config_Dates_Update BIT = 0
     )
AS
    BEGIN


        DECLARE
            @Exception_Status_Cd INT
            , @Queue_User_Info_Id INT
            , @Group_Legacy_Name_Cd INT
            , @Exception_Type_Cd INT
            , @Meter_Id INT
            , @Commodity_Id INT
            , @Invoice_Collection_Account_Config_Id INT
            , @Invoice_Collection_Officer_User_Id INT
            , @Exception_Cnt INT = 0
            , @Client_Id INT
            , @No_Config BIT = 0
            , @Queue_Id INT
            , @Previous_IC_Officer_Queue_Id INT;



        DECLARE @Exceptions TABLE
              (
                  ID INT
                  , Exception_Type VARCHAR(MAX)
              );

        DECLARE @Invoice_Collections_Account TABLE
              (
                  ID INT IDENTITY(1, 1)
                  , Invoice_Collection_Account_Config_Id INT
                  , Invoice_Collection_Officer_User_Id INT
              );


        SELECT
            @Exception_Type_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            Std_Column_Name = 'Exception_Type_Cd'
            AND c.Code_Value = 'Missing IC Data';


        SELECT
            @Previous_IC_Officer_Queue_Id = QUEUE_ID
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USER_INFO_ID = @Previous_IC_Officer_User_Id;



        IF (   (   @Invoice_Collection_Is_Chased = 1
                   AND  @Invoice_Collection_Is_Chased_Changed = 1)
               OR   @Invoice_Collection_Account_Config_Id_Input IS NOT NULL
               OR   @Ic_Supplier_Account_Config_Dates_Update = 1)
            BEGIN


                UPDATE
                    ICA
                SET
                    ICA.Is_Chase_Activated = 1
                FROM
                    dbo.Invoice_Collection_Account_Config ICA
                WHERE
                    ICA.Account_Id = @Account_Id
                    AND @Invoice_Collection_Account_Config_Id_Input IS NULL
                    AND @Ic_Supplier_Account_Config_Dates_Update = 0;





                SELECT
                    @Client_Id = MAX(ch.Client_Id)
                FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                WHERE
                    cha.Account_Id = @Account_Id;


                SELECT
                    @Invoice_Collection_Officer_User_Id = Invoice_Collection_Officer_User_Id
                FROM
                    dbo.Invoice_Collection_Client_Config icc
                WHERE
                    icc.Client_Id = @Client_Id;

                SELECT
                    @Invoice_Collection_Account_Config_Id_Input = NULL
                WHERE
                    NOT EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Invoice_Collection_Account_Config icac
                                   WHERE
                                        icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id_Input);

                SELECT
                    @Invoice_Collection_Officer_User_Id = Invoice_Collection_Officer_User_Id
                    , @Account_Id = ISNULL(@Account_Id, Account_Id)
                FROM
                    dbo.Invoice_Collection_Account_Config icc
                WHERE
                    Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id_Input;



                SELECT
                    @Meter_Id = CASE WHEN COUNT(cha.Meter_Id) = 1 THEN MAX(cha.Meter_Id)
                                    ELSE -1
                                END
                    , @Commodity_Id = CASE WHEN COUNT(cha.Commodity_Id) = 1 THEN MAX(cha.Commodity_Id)
                                          ELSE -1
                                      END
                FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                WHERE
                    cha.Account_Id = @Account_Id;


                SELECT
                    @Exception_Status_Cd = cd.Code_Id
                FROM
                    dbo.Code cd
                    JOIN dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
                WHERE
                    cs.Codeset_Name = 'Exception Status'
                    AND cd.Code_Value = 'New';

                SELECT
                    @Group_Legacy_Name_Cd = cd.Code_Id
                FROM
                    dbo.Code cd
                    JOIN dbo.Codeset cs
                        ON cd.Codeset_Id = cs.Codeset_Id
                WHERE
                    cs.Codeset_Name = 'Group_Legacy_Name'
                    AND cd.Code_Value = 'Standing Data Team';


                INSERT INTO @Invoice_Collections_Account
                     (
                         Invoice_Collection_Account_Config_Id
                         , Invoice_Collection_Officer_User_Id
                     )
                SELECT
                    ica.Invoice_Collection_Account_Config_Id
                    , ica.Invoice_Collection_Officer_User_Id
                FROM
                    dbo.Invoice_Collection_Account_Config ica
                WHERE
                    (   ica.Account_Id = @Account_Id
                        AND @Invoice_Collection_Account_Config_Id_Input IS NULL)
                    OR  ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id_Input
                ORDER BY
                    ica.Invoice_Collection_Account_Config_Id;

                SELECT
                    @No_Config = 1
                WHERE
                    NOT EXISTS (SELECT  1 FROM  @Invoice_Collections_Account);

                DECLARE
                    @Init INT
                    , @Total_Rows INT;

                SELECT  @Init = MIN(ID)FROM @Invoice_Collections_Account;

                SELECT  @Total_Rows = MAX(ID)FROM   @Invoice_Collections_Account;

                WHILE @Init <= @Total_Rows
                    BEGIN

                        DELETE  FROM @Exceptions;
                        SELECT
                            @Queue_User_Info_Id = Invoice_Collection_Officer_User_Id
                            , @Invoice_Collection_Account_Config_Id = Invoice_Collection_Account_Config_Id
                            , @Queue_Id = ui.QUEUE_ID
                        FROM
                            @Invoice_Collections_Account ic
                            INNER JOIN dbo.USER_INFO ui
                                ON ic.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID
                        WHERE
                            ID = @Init;

                        INSERT INTO @Exceptions
                        EXEC dbo.Missing_Invoice_Collection_Account_Config_Exception_Generate
                            @Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

                        SELECT  @Exception_Cnt = COUNT(1)FROM   @Exceptions;


                        DELETE
                        ae
                        FROM
                            dbo.Account_Exception ae
                        WHERE
                            Account_Id = @Account_Id
                            AND @Exception_Cnt = 0
                            AND Exception_Type_Cd = @Exception_Type_Cd
                            AND Queue_Id = @Queue_Id
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Invoice_Collection_Account_Config icac
                                                    INNER JOIN dbo.USER_INFO ui
                                                        ON icac.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID
                                               WHERE
                                                    icac.Account_Id = ae.Account_Id
                                                    AND ae.QUEUE_ID = ui.QUEUE_ID
                                                    AND icac.Invoice_Collection_Account_Config_Id <> @Invoice_Collection_Account_Config_Id
                                                    AND icac.is_Config_Complete = 0);
                        UPDATE
                            ae
                        SET
                            ae.Meter_Id = @Meter_Id
                            , ae.Queue_Id = @Queue_Id
                            , ae.Exception_By_User_Id = @User_Info_Id
                            , ae.Last_Change_Ts = GETDATE()
                            , ae.Updated_User_Id = @User_Info_Id
                            , ae.Commodity_Id = @Commodity_Id
                        FROM
                            dbo.Account_Exception ae
                        WHERE
                            Account_Id = @Account_Id
                            AND @Exception_Cnt > 0
                            AND Exception_Type_Cd = @Exception_Type_Cd
                            AND Queue_Id = ISNULL(@Previous_IC_Officer_Queue_Id, @Queue_Id)
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Invoice_Collection_Account_Config ica
                                               WHERE
                                                    ica.Account_Id = @Account_Id
                                                    AND ica.Invoice_Collection_Account_Config_Id <> @Invoice_Collection_Account_Config_Id
                                                    AND ica.is_Config_Complete = 0
                                                    AND @Invoice_Collection_Account_Config_Id_Input IS NOT NULL);



                        INSERT INTO dbo.Account_Exception
                             (
                                 Account_Id
                                 , Meter_Id
                                 , Exception_Type_Cd
                                 , Queue_Id
                                 , Exception_Status_Cd
                                 , Exception_By_User_Id
                                 , Exception_Created_Ts
                                 , Last_Change_Ts
                                 , Updated_User_Id
                                 , Commodity_Id
                             )
                        SELECT
                            @Account_Id
                            , @Meter_Id
                            , @Exception_Type_Cd
                            , @Queue_Id
                            , @Exception_Status_Cd
                            , @User_Info_Id
                            , GETDATE()
                            , GETDATE()
                            , @User_Info_Id
                            , @Commodity_Id
                        WHERE
                            @Exception_Cnt > 0
                            AND NOT EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.Account_Exception ae
                                               WHERE
                                                    ae.Account_Id = @Account_Id
                                                    AND ae.Exception_Type_Cd = @Exception_Type_Cd
                                                    AND ae.Queue_Id = @Queue_Id);

                        UPDATE
                            ica
                        SET
                            ica.is_Config_Complete = 1
                        FROM
                            dbo.Invoice_Collection_Account_Config ica
                        WHERE
                            ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                            AND @Exception_Cnt = 0;

                        UPDATE
                            ica
                        SET
                            ica.is_Config_Complete = 0
                        FROM
                            dbo.Invoice_Collection_Account_Config ica
                        WHERE
                            ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                            AND @Exception_Cnt > 0
                            AND ica.is_Config_Complete <> 0;

                        SET @Init = @Init + 1;
                    END;


                DELETE
                ae
                FROM
                    dbo.Account_Exception ae
                WHERE
                    Account_Id = @Account_Id
                    AND Exception_Type_Cd = @Exception_Type_Cd
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Invoice_Collection_Account_Config icac
                                            INNER JOIN dbo.USER_INFO ui
                                                ON icac.Invoice_Collection_Officer_User_Id = ui.USER_INFO_ID
                                       WHERE
                                            icac.Account_Id = ae.Account_Id
                                            AND ae.QUEUE_ID = ui.QUEUE_ID);





                INSERT INTO dbo.Account_Exception
                     (
                         Account_Id
                         , Meter_Id
                         , Exception_Type_Cd
                         , Queue_Id
                         , Exception_Status_Cd
                         , Exception_By_User_Id
                         , Exception_Created_Ts
                         , Last_Change_Ts
                         , Updated_User_Id
                         , Commodity_Id
                     )
                SELECT
                    @Account_Id
                    , @Meter_Id
                    , @Exception_Type_Cd
                    , ui.QUEUE_ID
                    , @Exception_Status_Cd
                    , @User_Info_Id
                    , GETDATE()
                    , GETDATE()
                    , @User_Info_Id
                    , @Commodity_Id
                FROM
                    dbo.USER_INFO ui
                WHERE
                    ui.USER_INFO_ID = @Invoice_Collection_Officer_User_Id
                    AND @No_Config = 1;



            END;


        IF (   @Invoice_Collection_Is_Chased = 0
               AND  @Invoice_Collection_Is_Chased_Changed = 1)
            BEGIN



                DELETE  FROM
                dbo.Account_Exception
                WHERE
                    Account_Id = @Account_Id
                    AND Exception_Type_Cd = @Exception_Type_Cd;

                UPDATE
                    ica
                SET
                    ica.Is_Chase_Activated = 0
                    , Chase_Status_Updated_User_Id = @User_Info_Id
                    , Chase_Status_Lat_Updated_Ts = GETDATE()
                FROM
                    dbo.Invoice_Collection_Account_Config ica
                WHERE
                    ica.Account_Id = @Account_Id
                    AND ica.Is_Chase_Activated = 1;


            END;




    END;

    ;

    ;
GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Ins_By_Missing_Invoice_Collection_data] TO [CBMSApplication]
GO
