SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      PROCEDURE [dbo].[cbmsBudgetDetail_GetForBudget]
	( @myaccountid int = null
	, @budget_id int = null
	)
AS
BEGIN

	   select bd.budget_detail_id
		, bd.budget_id
		, bd.account_id
		, bd.date
		, bd.usage
		, bd.variable
		, bd.multiplier
		, bd.adder
		, bd.transportation	
		, bd.transmission
		, bd.distribution
		, bd.other_unit
		, bd.other_fixed
		, bd.commodity_type_id
		, b.client_id
		, b.budget_start_year
		, b.budget_start_month
		, b.cbms_image_id
	     from budget_detail bd
	     join budgets b on b.budget_id = bd.budget_id
	    where bd.budget_id = @budget_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetail_GetForBudget] TO [CBMSApplication]
GO
