SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
dbo.Exception_Detail_Opened_Date_Sel_By_cu_invoice_id_Account_ID_Commodity_id
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name				DataType Default Description  
------------------------------------------------------------  
 @cu_invoice_id		INT                     
 @Account_ID		INT		NULL  
 @Commodity_id		INT		NULL
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC Exception_Detail_Opened_Date_Sel_By_cu_invoice_id_Account_ID_Commodity_id 28694669,899843,291  

AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RKV   Ravi Kumar Vegesna  
  
MODIFICATIONS:  
 Initials Date  Modification  
------------------------------------------------------------  
  RKV        2016-01-05  Added two more fileds commodity_name  as part of AS400-PII  
******/  
  
CREATE PROCEDURE [dbo].[Exception_Detail_Opened_Date_Sel_By_cu_invoice_id_Account_ID_Commodity_id]
      ( 
       @cu_invoice_id INT
      ,@Account_ID INT = NULL
      ,@Commodity_id INT = NULL )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
   
      SELECT
            ced.OPENED_DATE
      FROM
            dbo.CU_EXCEPTION_DETAIL ced
            INNER JOIN dbo.CU_EXCEPTION ce
                  ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
      WHERE
            ( ce.cu_invoice_id = @cu_invoice_id )
            AND ( @Account_ID IS NULL
                  OR ced.ACCOUNT_ID = @Account_ID )
            AND ( @Commodity_id IS NULL
                  OR ced.Commodity_ID = @Commodity_id )  
  
END;  
  

;
GO
GRANT EXECUTE ON  [dbo].[Exception_Detail_Opened_Date_Sel_By_cu_invoice_id_Account_ID_Commodity_id] TO [CBMSApplication]
GO
