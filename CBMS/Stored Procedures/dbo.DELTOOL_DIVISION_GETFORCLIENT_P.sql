SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_DIVISION_GETFORCLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@client_id     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--exec DELTOOL_DIVISION_GETFORCLIENT -1,10065
--exec DELTOOL_DIVISION_GETFORCLIENT -1,0


CREATE   PROCEDURE DBO.DELTOOL_DIVISION_GETFORCLIENT_P
	( @MyAccountId int
	, @client_id int
	)
AS
BEGIN
	set nocount on

	   select division_id
		, division_name
	     from division with (nolock)
	    where client_id = @client_id
	 order by division_name


END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_DIVISION_GETFORCLIENT_P] TO [CBMSApplication]
GO
