SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE   PROCEDURE dbo.UPDATE_CONTRACT_NUMBER_P
	@ed_contract_number VARCHAR(150),
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON
	
	UPDATE dbo.[contract]
		SET ed_contract_number = @ed_contract_number
	WHERE contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_CONTRACT_NUMBER_P] TO [CBMSApplication]
GO
