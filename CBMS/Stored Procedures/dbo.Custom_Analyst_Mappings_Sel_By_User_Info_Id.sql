SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------
    @User_Info_Id	INT
      
OUTPUT PARAMETERS:
	Name            DataType        Default    Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 26613
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 31014
	
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 46946
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 46907
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 30947
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 33290
	EXEC dbo.Custom_Analyst_Mappings_Sel_By_User_Info_Id 45188

AUTHOR INITIALS:
    Initials    Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-01-05	Created MAINT-3304 Move User to History CBMS enhancement

******/

CREATE PROCEDURE [dbo].[Custom_Analyst_Mappings_Sel_By_User_Info_Id] ( @User_Info_Id AS INT )
AS 
BEGIN
    
      SET NOCOUNT ON;
    
      WITH  Cte
              AS ( SELECT
                        ch.Client_Name
                       ,NULL AS Site_Name
                       ,NULL AS Account_Number
                   FROM
                        dbo.Client_Commodity_Analyst cca
                        INNER JOIN Core.Client_Commodity cc
                              ON cca.Client_Commodity_Id = cc.Client_Commodity_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cc.Client_Id = ch.Client_Id
                   WHERE
                        cca.Analyst_User_Info_Id = @User_Info_Id
                        AND ch.Sitegroup_Id = 0
                   GROUP BY
                        ch.Client_Name
                   UNION ALL
                   SELECT
                        cc.Client_Name
                       ,cc.Site_name AS Site_Name
                       ,NULL AS Account_Number
                   FROM
                        dbo.Site_Commodity_Analyst sca
                        INNER JOIN Core.Client_Hier cc
                              ON sca.Site_Id = cc.Site_Id
                   WHERE
                        sca.Analyst_User_Info_Id = @User_Info_Id
                   GROUP BY
                        cc.Client_Name
                       ,cc.Site_name
                   UNION ALL
                   SELECT
                        ch.Client_Name
                       ,ch.Site_name AS Site_Name
                       ,cha.Account_Number AS Account_Number
                   FROM
                        dbo.Account_Commodity_Analyst aca
                        INNER JOIN Core.Client_Hier_Account cha
                              ON aca.Account_Id = cha.Account_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                   WHERE
                        aca.Analyst_User_Info_Id = @User_Info_Id
                   GROUP BY
                        ch.Client_Name
                       ,ch.Site_name
                       ,cha.Account_Number )
            SELECT
                  Client_Name
                 ,Site_Name
                 ,Account_Number
            FROM
                  Cte
            ORDER BY
                  Client_Name
                 ,Site_Name
                 ,Account_Number
            
            
END


;
GO
GRANT EXECUTE ON  [dbo].[Custom_Analyst_Mappings_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
