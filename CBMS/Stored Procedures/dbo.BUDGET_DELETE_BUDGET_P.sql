
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.BUDGET_DELETE_BUDGET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@Budget_Id				INT
	@Budget_Account_Id		INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN
		SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 11129
		SELECT count(1) FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 11129
		EXEC dbo.BUDGET_DELETE_BUDGET_P 11129
		SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 11129
		SELECT count(1) FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 11129
	ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-06-01	Added header
							SE2017-26 - Added script to deleted budget accounts from queue dbo.Budget_Account_Queue
	
******/
CREATE    PROCEDURE [dbo].[BUDGET_DELETE_BUDGET_P] ( @budgetId INT )
AS 
BEGIN
      SET NOCOUNT ON;

      DELETE FROM
            BUDGET_DETAIL_SNAP_SHOT
      WHERE
            budget_account_id IN ( SELECT
                                    budget_account_id
                                   FROM
                                    budget_account
                                   WHERE
                                    budget_id = @budgetId )
      DELETE FROM
            BUDGET_DETAIL_COMMENTS_OWNER_MAP
      WHERE
            budget_id = @budgetId
      DELETE FROM
            BUDGET_DETAIL_COMMENTS
      WHERE
            budget_account_id IN ( SELECT
                                    budget_account_id
                                   FROM
                                    budget_account
                                   WHERE
                                    budget_id = @budgetId )
      DELETE FROM
            BUDGET_CURRENCY_MAP
      WHERE
            budget_id = @budgetId
      DELETE FROM
            BUDGET_DETAILS
      WHERE
            budget_account_id IN ( SELECT
                                    budget_account_id
                                   FROM
                                    budget_account
                                   WHERE
                                    budget_id = @budgetId )
      
      
      DELETE
            baq
      FROM
            dbo.BUDGET_ACCOUNT ba
            INNER JOIN dbo.Budget_Account_Queue baq
                  ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
      WHERE
            ba.BUDGET_ID = @budgetId
            
            
      DELETE FROM
            BUDGET_ACCOUNT
      WHERE
            budget_id = @budgetId
      DELETE FROM
            BUDGET
      WHERE
            budget_id = @budgetId


END








;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_DELETE_BUDGET_P] TO [CBMSApplication]
GO
