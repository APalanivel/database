SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_DIVISIONS_SITES_UNDER_CLIENT_P 
@userId varchar,
@sessionId varchar,
@clientId int
as
	set nocount on
select	DISTINCT site.SITE_ID,
		division.DIVISION_ID,
		viewSite.SITE_NAME

from	SITE site,
	RM_ONBOARD_HEDGE_SETUP hedge,
	DIVISION division,
	VWSITENAME viewSite

where 	division.DIVISION_ID IN(select	DISTINCT DIVISION_ID from DIVISION where CLIENT_ID=@clientId) AND
	site.DIVISION_ID=division.DIVISION_ID AND
	hedge.SITE_ID=site.SITE_ID AND
	hedge.INCLUDE_IN_REPORTS=1 AND
	site.SITE_ID = viewSite.SITE_ID
	and site.closed= 0
	and site.not_managed = 0

order by viewSite.SITE_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_DIVISIONS_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
