SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                  
NAME: dbo.Meter_Dtl_Sel_By_Account_List                                                  
                                                   
                                                  
DESCRIPTION:                                                  
       This sproc is to get the Meter List.                                           
                     
                                                  
INPUT PARAMETERS:                                                  
 Name      DataType  Default   Description                                                  
------------------------------------------------------------------------------------                                                  
 @Country_Id    INT    NULL          
       
           
                                                  
OUTPUT PARAMETERS:                                                  
 Name       DataType  Default   Description                                                  
------------------------------------------------------------------------------------                                                  
                                                
                                                  
USAGE EXAMPLES:                                                  
------------------------------------------------------------------------------------                                                  
                                           
EXEC dbo.Meter_Dtl_Sel_By_Account_List    @Account_Id = '611973,452845,31176,439141'     
     
	
	select top 1 * from core.client_hier_Account 
	  
               
  
                                     
AUTHOR INITIALS:                                                  
 Initials		Name                                                  
------------------------------------------------------------------------------------                                                  
 NR				Narayana Reddy
       
MODIFICATIONS                                                  
                                                  
 Initials	Date			Modification                                                  
------------------------------------------------------------------------------------                               
 NR		2019-11-19		Created For Add contract      
******/

CREATE PROCEDURE [dbo].[Meter_Dtl_Sel_By_Account_List]
    (
        @Account_Id VARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Account_Vendor_Id
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Account_Id, ',') us
                ON us.Segments = cha.Account_Id
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID <> -1
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Contract_ID
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Account_Vendor_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[Meter_Dtl_Sel_By_Account_List] TO [CBMSApplication]
GO
