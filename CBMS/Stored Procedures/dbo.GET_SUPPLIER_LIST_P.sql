SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_SUPPLIER_LIST_P 
@userId varchar,
@sessionId varchar

as
	set nocount on
select	vendor.VENDOR_ID,
	vendor.VENDOR_NAME

from	VENDOR vendor

WHERE	vendor.VENDOR_TYPE_ID IN( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_NAME ='Supplier'
				) 

ORDER BY vendor.VENDOR_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_SUPPLIER_LIST_P] TO [CBMSApplication]
GO
