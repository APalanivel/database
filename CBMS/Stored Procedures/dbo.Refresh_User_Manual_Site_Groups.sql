
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************      
NAME : dbo.Refresh_User_Manual_Site_Groups    
     
DESCRIPTION:   

This Procedure is used to 
?	Refresh user site groups
     
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   
@UserInfoId      INT         User ID   
@ClientId   INT         Client ID      
      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
--------------------------------------------------------------------      
  USAGE EXAMPLES:      
--------------------------------------------------------------------      

EXEC    Refresh_User_Manual_Site_Groups 2, 'Manual_Sitegroup_Refresh_Last_Ts'  
    
AUTHOR INITIALS:      
 Initials Name      
-------------------------------------------------------------------      
 KVK	Vinay Kumar K
 HG     Harihara Suthan G
     
 MODIFICATIONS       
 Initials Date  Modification      
--------------------------------------------------------------------      
 KVK	12/16/2010		Refresh only manual site groups, not all. 
 HG		12/30/2010		Changed the logic of Mod_Dt column update to update only the user groups affected with the role changes.
 KVK	12/16/2010		Now this Sp will not call from the CBMS application when the user changes the sites and divisions.
						This sp will call in any Batch process once in a day(Night)

******************************************************************************************************/  

CREATE PROCEDURE [dbo].[Refresh_User_Manual_Site_Groups]  
(  
  
	@AppId								INT   
	,@ManualSitegroupRefreshParamName	VARCHAR(255)
)  
  
AS  
BEGIN  
  
	SET NOCOUNT ON ;

	-- To store the list of groups refreshed with the role changes.
        DECLARE @Refreshed_User_Groups TABLE ( Sitegroup_Id INT )
		
	-- declare required parameters
        DECLARE @RoleId			INT
		,		@System_User_Id	INT
        ,		@SiteGroup_Type_Name VARCHAR(25)	= 'User'

		DECLARE @LastRunTs DATETIME
		,		@CurrentTs VARCHAR(255)

	BEGIN TRY

		SELECT @CurrentTs =	CAST (GETDATE() AS VARCHAR)

	--Get Last run time stamp from App_Config
		SELECT @LastRunTs =CAST (App_Config_Value AS DATETIME)
		FROM dbo.App_Config 
		WHERE App_Id = @AppId
		AND App_Config_Cd = @ManualSitegroupRefreshParamName 
		
		
		SELECT @System_User_Id = User_Info_Id
		FROM dbo.USER_INFO 
		WHERE FIRST_NAME = 'System'
		AND Last_Name = 'Conversion'

	-- delete the rows from 'Sitegroup_Site' table for that perticular group and
	-- the row should not assign to the user
		DELETE SS
		OUTPUT DELETED.Sitegroup_Id
			   INTO @Refreshed_User_Groups
		FROM   dbo.Sitegroup_Site SS
			   INNER JOIN dbo.SiteGroup sg 
			   ON SS.Sitegroup_id = sg.Sitegroup_Id
			   INNER JOIN dbo.Code cd 
			   ON cd.Code_Id = sg.Sitegroup_Type_Cd
			   INNER JOIN 
			   (	
				SELECT ur.user_info_id, ur.security_role_id, sr.client_id 
				  FROM dbo.user_security_role ur
					   INNER JOIN dbo.security_role sr 
					   ON sr.security_role_id = ur.security_role_id
				 WHERE ur.last_change_ts > @LastRunTs
				) usr 
				ON (	 usr.user_info_id = sg.Owner_User_Id 
					 AND sg.client_id = usr.client_id
					)
		WHERE  cd.Code_Value = @SiteGroup_Type_Name
		  AND  sg.is_smart_group = 0
		  AND  NOT EXISTS ( 
							 SELECT 1
							   FROM [core].[client_hier] ch
									INNER JOIN dbo.Security_Role_Client_Hier srch 
									ON ch.Client_Hier_Id = srch.Client_Hier_Id
							  WHERE srch.Security_Role_Id = usr.security_role_id
								AND ch.Site_Id = SS.Site_id 
							)

		UPDATE sg
		   SET sg.Mod_Dt		= GETDATE() 
		,	   sg.Mod_User_Id = @System_User_Id
		  FROM dbo.SiteGroup sg
			   INNER JOIN @Refreshed_User_Groups rug 
			   ON rug.Sitegroup_Id = sg.Sitegroup_Id

	--Update  with @CurrentTs
			
		UPDATE dbo.App_Config
		SET App_Config_Value = @CurrentTs
		WHERE App_Id = @AppId
		AND App_Config_Cd = @ManualSitegroupRefreshParamName 
  

	END TRY
	BEGIN CATCH

		EXEC usp_RethrowError  

	END CATCH  
  
END

GO

GRANT EXECUTE ON  [dbo].[Refresh_User_Manual_Site_Groups] TO [CBMSApplication]
GO
