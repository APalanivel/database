
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   dbo.SR_RFP_GET_MY_TASK_MODIFIED_DATE_P     

    
DESCRIPTION:  
				Script to get RFP tasks last modified/updated date 
      
INPUT PARAMETERS:          
	Name				DataType	Default		Description          
------------------------------------------------------------          
	@user_id			VARCHAR(10)
    @session_id			VARCHAR(20)
    @rfp_id				INT
    @entity_identifier	VARCHAR(200)
                
OUTPUT PARAMETERS:          
	Name				DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

	EXEC GET_ENTITIY_DETAILS_FOR_YES_NO_NA_P 1046
	
	EXEC dbo.SR_RFP_GET_MY_TASK_MODIFIED_DATE_P NULL,NULL,12873,'Manage Term'
	EXEC dbo.SR_RFP_GET_MY_TASK_MODIFIED_DATE_P NULL,NULL,12873,'Create Pricing'
	EXEC dbo.SR_RFP_GET_MY_TASK_MODIFIED_DATE_P NULL,NULL,12873,NULL
	
     
AUTHOR INITIALS:          
	Initials	Name          
------------------------------------------------------------          
	RR			Raghu Reddy 
     
     
MODIFICATIONS           
	Initials	Date		Modification          
------------------------------------------------------------          
	RR			2016-09-21	MAINT-4236 Modified to return all tasks dates if @entity_identifier input value is not passed, this is to
							avoid repeated calls from application in loop for each task

*****/ 

CREATE  PROCEDURE [dbo].[SR_RFP_GET_MY_TASK_MODIFIED_DATE_P]
    (
      @user_id VARCHAR(10) ,
      @session_id VARCHAR(20) ,
      @rfp_id INT ,
      @entity_identifier VARCHAR(200)
    )
AS
    BEGIN
        SET NOCOUNT ON;
    
        DECLARE @entity_id INT;
    
        SELECT  @entity_id = ENTITY_ID
        FROM    dbo.ENTITY
        WHERE   ENTITY_TYPE = 1046
                AND ENTITY_NAME = @entity_identifier

        SELECT  ea.MODIFIED_DATE ,
                en.ENTITY_NAME AS Entity_Identifier
        FROM    dbo.ENTITY_AUDIT ea
                INNER JOIN dbo.ENTITY en ON en.ENTITY_ID = ea.ENTITY_ID
        WHERE   en.ENTITY_TYPE = 1046
                AND ( @entity_id IS NULL
                      OR ea.ENTITY_ID = @entity_id
                    )
                AND ENTITY_IDENTIFIER = @rfp_id

    END	
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_MY_TASK_MODIFIED_DATE_P] TO [CBMSApplication]
GO
