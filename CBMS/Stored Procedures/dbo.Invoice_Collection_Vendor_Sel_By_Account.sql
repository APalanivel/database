SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Vendor_Sel_By_Account            
                        
 DESCRIPTION:                        
			To get the details of invoice collection vendors based on account                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Account_Id						INT

                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


 
  EXEC [dbo].[Invoice_Collection_Vendor_Sel_By_Account] 
	   @Account_Id = 1342210
	   
  EXEC [dbo].[Invoice_Collection_Vendor_Sel_By_Account] 
	   @Account_Id = 1342228

  EXEC [dbo].[Invoice_Collection_Vendor_Sel_By_Account] 
	   @Account_Id = 1342214
                    
                       
 AUTHOR INITIALS:        
       
	Initials    Name        
---------------------------------------------------------------------------------------------------------------                      
	RK          Ravi Kumar V    
                         
 MODIFICATIONS:      
          
	Initials    Date             Modification      
---------------------------------------------------------------------------------------------------------------      
	RK          2017-02-29       Created for Invoice Tracking               
                       
******/ 
CREATE PROCEDURE [dbo].[Invoice_Collection_Vendor_Sel_By_Account] ( @Account_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;  


      SELECT
            e.ENTITY_NAME
           ,v.VENDOR_NAME
           ,icac.Invoice_Collection_Service_Start_Dt
           ,icac.Invoice_Collection_Service_End_Dt
      FROM
            dbo.Invoice_Collection_Account_Contact icacc
            INNER JOIN dbo.Vendor_Contact_Map vc
                  ON icacc.Contact_Info_Id = vc.Contact_Info_Id
            INNER JOIN dbo.VENDOR v
                  ON vc.VENDOR_ID = v.VENDOR_ID
            INNER JOIN dbo.ENTITY e
                  ON v.VENDOR_TYPE_ID = e.ENTITY_ID
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                  ON icacc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
      WHERE
            icac.Account_Id = @Account_Id
      GROUP BY
            e.ENTITY_NAME
           ,v.VENDOR_NAME
           ,icac.Invoice_Collection_Service_Start_Dt
           ,icac.Invoice_Collection_Service_End_Dt
      ORDER BY
            e.ENTITY_NAME
           ,v.VENDOR_NAME
           ,icac.Invoice_Collection_Service_Start_Dt
               


END


;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Vendor_Sel_By_Account] TO [CBMSApplication]
GO
