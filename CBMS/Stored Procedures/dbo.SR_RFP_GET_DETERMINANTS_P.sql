SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.SR_RFP_GET_DETERMINANTS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@sr_rfp_load_profile_setup_id int,
	@is_default bit
	AS
	set nocount on
	if @is_default = 1
	begin
		select	
			0 as sr_rfp_load_profile_determinant_id,
		       	determinant_name,
			determinant_unit_type_id,
			is_checked
		from
			sr_load_profile_determinant(nolock)
		where 
			sr_load_profile_default_setup_id = @sr_rfp_load_profile_setup_id
	end
	else
	begin
		select	
			sr_rfp_load_profile_determinant_id,
		       	determinant_name,
			updated_determinant_unit_type_id as determinant_unit_type_id,
			updated_is_checked as is_checked
		
		from	sr_rfp_load_profile_determinant (nolock)
		
		where 	sr_rfp_load_profile_setup_id = @sr_rfp_load_profile_setup_id
			and determinant_type_id != (select entity_id from entity(nolock) where entity_type = 1030 and entity_name = 'Account Level')
	end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_DETERMINANTS_P] TO [CBMSApplication]
GO
