SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Account_DMO_Config_Dates_Validation
           
DESCRIPTION:             
			To insert entity DMO configurations
			
INPUT PARAMETERS:            
	Name				DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Client_Hier_Id		INT
    @Commodity_Id		INT
    @DMO_Start_Dt		DATE
    @DMO_End_Dt			DATE
    @User_Info_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT TOP 10 * FROM dbo.Account_DMO_Config
            
	EXEC dbo.Account_DMO_Config_Dates_Validation 63,'2018-01-01','2018-12-31'
	EXEC dbo.Account_DMO_Config_Dates_Validation 49,'2018-01-01','2018-12-31'
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-01-27	Contract placeholder - CP-7 Created
******/

CREATE PROCEDURE [dbo].[Account_DMO_Config_Dates_Validation]
      ( 
       @Account_DMO_Config_Id INT
      ,@DMO_Start_Dt DATE
      ,@DMO_End_Dt DATE )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Is_DMO_Config_New_Dates_Valid BIT = 1
      DECLARE @Is_DMO_Config_Has_Dependency BIT = 1
      DECLARE
            @DMO_Supplier_Accounts_Exists BIT = 0
           ,@Consolidated_Billing_Config_Exists BIT = 0
      
      DECLARE @Tbl_DMO_Supplier_Accounts AS TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,Account_Id INT
            ,Supplier_Account_begin_Dt DATE
            ,Supplier_Account_End_Dt DATE )
            
      DECLARE @Tbl_Consolidated_Billing_Accounts AS TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,Account_Id INT
            ,Billing_Start_Dt DATE
            ,Billing_End_Dt DATE )
            
      DECLARE
            @Account_Id INT
           ,@Commodity_Id INT
           
      SELECT
            @DMO_End_Dt = DATEADD(DD, -DATEPART(DD, @DMO_End_Dt), DATEADD(mm, 1, @DMO_End_Dt))
      WHERE
            @DMO_End_Dt IS NOT NULL
      
      SELECT
            @Account_Id = adc.Account_Id
           ,@Commodity_Id = adc.Commodity_Id
      FROM
            dbo.Account_DMO_Config adc
      WHERE
            adc.Account_DMO_Config_Id = @Account_DMO_Config_Id
            
            
            
      INSERT      INTO @Tbl_DMO_Supplier_Accounts
                  ( 
                   Account_Id
                  ,Supplier_Account_begin_Dt
                  ,Supplier_Account_End_Dt )
                  SELECT
                        supp.Account_Id
                       ,suppacc.Supplier_Account_begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
                  FROM
                        Core.Client_Hier_Account utlt
                        INNER JOIN Core.Client_Hier_Account supp
                              ON utlt.Meter_Id = supp.Meter_Id
                        INNER JOIN dbo.ACCOUNT suppacc
                              ON supp.Account_Id = suppacc.ACCOUNT_ID
                  WHERE
                        utlt.Account_Id = @Account_Id
                        AND utlt.Account_Type = 'Utility'
                        AND utlt.Commodity_Id = @Commodity_Id
                        AND supp.Account_Type = 'Supplier'
                        AND supp.Supplier_Contract_ID = -1
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_DMO_Config cdc
                                     WHERE
                                          cdc.Account_DMO_Config_Id = @Account_DMO_Config_Id
                                          AND ( ( cdc.DMO_End_Dt IS NULL
                                                  AND suppacc.Supplier_Account_End_Dt IS NULL
                                                  AND cdc.DMO_Start_Dt <= suppacc.Supplier_Account_begin_Dt )
                                                OR ( cdc.DMO_End_Dt IS NULL
                                                     AND suppacc.Supplier_Account_End_Dt IS NOT NULL
                                                     AND cdc.DMO_Start_Dt <= suppacc.Supplier_Account_begin_Dt
                                                     AND cdc.DMO_Start_Dt <= suppacc.Supplier_Account_End_Dt )
                                                OR ( cdc.DMO_End_Dt IS NOT NULL
                                                     AND suppacc.Supplier_Account_End_Dt IS NOT NULL
                                                     AND suppacc.Supplier_Account_begin_Dt BETWEEN cdc.DMO_Start_Dt
                                                                                           AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt))
                                                     AND suppacc.Supplier_Account_End_Dt BETWEEN cdc.DMO_Start_Dt
                                                                                         AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt)) ) ) )
                  GROUP BY
                        supp.Account_Id
                       ,suppacc.Supplier_Account_begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
            
            
      SELECT
            @Is_DMO_Config_New_Dates_Valid = 0
      FROM
            @Tbl_DMO_Supplier_Accounts tc1
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @Tbl_DMO_Supplier_Accounts tc
                         WHERE
                              tc.Account_Id = tc1.Account_Id
                              AND ( ( @DMO_End_Dt IS NULL
                                      AND tc.Supplier_Account_End_Dt IS NULL
                                      AND @DMO_Start_Dt <= tc.Supplier_Account_begin_Dt )
                                    OR ( @DMO_End_Dt IS NULL
                                         AND tc.Supplier_Account_End_Dt IS NOT NULL
                                         AND @DMO_Start_Dt <= tc.Supplier_Account_begin_Dt
                                         AND @DMO_Start_Dt <= tc.Supplier_Account_End_Dt )
                                    OR ( @DMO_End_Dt IS NOT NULL
                                         AND tc.Supplier_Account_End_Dt IS NOT NULL
                                         AND tc.Supplier_Account_begin_Dt BETWEEN @DMO_Start_Dt
                                                                          AND     @DMO_End_Dt
                                         AND tc.Supplier_Account_End_Dt BETWEEN @DMO_Start_Dt
                                                                        AND     @DMO_End_Dt ) ) )
      
      
      INSERT      INTO @Tbl_Consolidated_Billing_Accounts
                  ( 
                   Account_Id
                  ,Billing_Start_Dt
                  ,Billing_End_Dt )
                  SELECT
                        acbv.Account_Id
                       ,acbv.Billing_Start_Dt
                       ,acbv.Billing_End_Dt
                  FROM
                        Core.Client_Hier_Account utlt
                        INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                              ON utlt.Account_Id = acbv.Account_Id
                        INNER JOIN dbo.ENTITY invtyp
                              ON acbv.Invoice_Vendor_Type_Id = invtyp.ENTITY_ID
                  WHERE
                        invtyp.ENTITY_NAME = 'Supplier'
                        AND acbv.Supplier_Vendor_Id IS NOT NULL
                        AND acbv.Account_Id = @Account_Id
                        AND utlt.Commodity_Id = @Commodity_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Account_DMO_Config cdc
                                     WHERE
                                          cdc.Account_DMO_Config_Id = @Account_DMO_Config_Id
                                          AND ( ( cdc.DMO_End_Dt IS NULL
                                                  AND acbv.Billing_End_Dt IS NULL
                                                  AND cdc.DMO_Start_Dt <= acbv.Billing_Start_Dt )
                                                OR ( cdc.DMO_End_Dt IS NULL
                                                     AND acbv.Billing_End_Dt IS NOT NULL
                                                     AND cdc.DMO_Start_Dt <= acbv.Billing_Start_Dt
                                                     AND cdc.DMO_Start_Dt <= acbv.Billing_End_Dt )
                                                OR ( cdc.DMO_End_Dt IS NOT NULL
                                                     AND acbv.Billing_End_Dt IS NOT NULL
                                                     AND acbv.Billing_Start_Dt BETWEEN cdc.DMO_Start_Dt
                                                                               AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt))
                                                     AND acbv.Billing_End_Dt BETWEEN cdc.DMO_Start_Dt
                                                                             AND     DATEADD(DD, -DATEPART(DD, cdc.DMO_End_Dt), DATEADD(mm, 1, cdc.DMO_End_Dt)) ) ) )
                  GROUP BY
                        acbv.Account_Id
                       ,acbv.Billing_Start_Dt
                       ,acbv.Billing_End_Dt
            
            
      SELECT
            @Is_DMO_Config_New_Dates_Valid = 0
      FROM
            @Tbl_Consolidated_Billing_Accounts tc1
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @Tbl_Consolidated_Billing_Accounts tc
                         WHERE
                              tc.Account_Id = tc1.Account_Id
                              AND ( ( @DMO_End_Dt IS NULL
                                      AND tc.Billing_End_Dt IS NULL
                                      AND @DMO_Start_Dt <= tc.Billing_Start_Dt )
                                    OR ( @DMO_End_Dt IS NULL
                                         AND tc.Billing_End_Dt IS NOT NULL
                                         AND @DMO_Start_Dt <= tc.Billing_Start_Dt
                                         AND @DMO_Start_Dt <= tc.Billing_End_Dt )
                                    OR ( @DMO_End_Dt IS NOT NULL
                                         AND tc.Billing_End_Dt IS NOT NULL
                                         AND tc.Billing_Start_Dt BETWEEN @DMO_Start_Dt
                                                                 AND     @DMO_End_Dt
                                         AND tc.Billing_End_Dt BETWEEN @DMO_Start_Dt
                                                               AND     @DMO_End_Dt ) ) )
                                                                   
      
      SELECT
            @Is_DMO_Config_Has_Dependency = 0
           ,@DMO_Supplier_Accounts_Exists = 1
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @Tbl_DMO_Supplier_Accounts )
      SELECT
            @Is_DMO_Config_Has_Dependency = 0
           ,@Consolidated_Billing_Config_Exists = 1
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @Tbl_Consolidated_Billing_Accounts )
                        
                        
      SELECT
            @Is_DMO_Config_New_Dates_Valid AS Is_DMO_Config_Dates_Valid
           ,@Is_DMO_Config_New_Dates_Valid AS Edit_Allowed
           ,@Is_DMO_Config_Has_Dependency AS Delete_Allowed
           ,@DMO_Supplier_Accounts_Exists AS DMO_Supplier_Accounts_Exists
           ,@Consolidated_Billing_Config_Exists AS Consolidated_Billing_Config_Exists                                         
                                                             
                                                             
END;
;




;
GO
GRANT EXECUTE ON  [dbo].[Account_DMO_Config_Dates_Validation] TO [CBMSApplication]
GO
