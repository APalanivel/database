
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:  dbo.Cost_Usage_Aggregate_Sel_For_Site_By_Commodity
   
DESCRIPTION:  Used to aggregate Site level C&U  
  
INPUT PARAMETERS:      
 Name			    DataType          Default     Description      
------------------------------------------------------------      
 @Client_Hier_Id	    int  
 @Commodity_id		    int  
 @Begin_Dt		    Date  
 @End_Dt			    Date  
 @Uom_Type_Id		    int  
 @Currency_Unit_Id	    int  
      
OUTPUT PARAMETERS:      
Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:
------------------------------------------------------------    

	EXEC dbo.Cost_Usage_Aggregate_Sel_For_Site_By_Commodity 1131,290,'01/01/2005','12/01/2005',11,3
	EXEC dbo.Cost_Usage_Aggregate_Sel_For_Site_By_Commodity 1135,290,'1/1/2009','12/1/2009',11,3
   

AUTHOR INITIALS:   
Initials	  Name    
------------------------------------------------------------    
SSR		  Sharad Srivastava
HG		  Harihara Suthan G
AP		  Athmaram Pabbathi
  
Initials	  Date	    Modification    
------------------------------------------------------------    
SSR		  02/17/2010  Created
HG		  03/26/2010  ISNULL function used to determine the conversion factor replaced and used CASE statement and bucket_type to determine the conversion factor.
					Subselect used to get the account dtl moved to CTE.
HG		  04/09/2010  Account_Type name added in the select clause.
AP		  04/02/2012  Addnl Data Changes
					   --Replaced @Site_Id parameter with @Client_Hier_Id
					   --Removed Account, Site, Client, SAMM base table references and used Client_Hier & Client_Hier_Account
******/    
CREATE PROCEDURE dbo.Cost_Usage_Aggregate_Sel_For_Site_By_Commodity
      ( 
       @Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Uom_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON ;

      DECLARE
            @Supplier_Account_Type_Id INT
           ,@Utility_Account_Type_Id INT

      SELECT
            @Supplier_Account_Type_Id = max(case WHEN at.Entity_Name = 'Supplier' THEN at.Entity_Id
                                            END)
           ,@Utility_Account_Type_Id = max(case WHEN at.Entity_Name = 'Utility' THEN at.Entity_Id
                                           END)
      FROM
            dbo.Entity at
      WHERE
            at.Entity_Description = 'Account Type'
            AND at.Entity_Name IN ( 'Supplier', 'Utility' )


      SELECT
            cha.Client_Hier_Id
           ,cha.Account_Id
           ,cuad.Service_Month
           ,( case WHEN cha.Account_Type = 'Supplier' THEN @Supplier_Account_Type_Id
                   WHEN cha.Account_Type = 'Utility' THEN @Utility_Account_Type_Id
              END ) AS Account_Type_Id
           ,cha.Account_Type AS Account_Type_Name
           ,bkt_cd.Code_Value Bucket_Type
           ,bm.Bucket_Name
           ,cuad.Bucket_Value * ( case WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                       WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                  END ) Bucket_Value
           ,bm.Bucket_Master_Id
      FROM
            ( SELECT
                  ch.Client_Hier_Id
                 ,cha.Account_Id
                 ,ch.Client_Currency_Group_Id
                 ,cha.Account_Type
              FROM
                  Core.Client_Hier ch
                  INNER JOIN Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
              WHERE
                  ch.Client_Hier_Id = @Client_Hier_Id
              GROUP BY
                  ch.Client_Hier_Id
                 ,cha.Account_Id
                 ,ch.Client_Currency_Group_Id
                 ,cha.Account_Type ) cha
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON cuad.Account_Id = cha.Account_Id
                     AND cuad.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            INNER JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            LEFT JOIN dbo.Consumption_Unit_Conversion uc
                  ON uc.Base_Unit_Id = cuad.UOM_Type_Id
                     AND uc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN dbo.Currency_Unit_Conversion cc
                  ON cc.Currency_Group_Id = cha.Client_Currency_Group_Id
                     AND cc.Base_Unit_Id = cuad.Currency_Unit_Id
                     AND cc.Converted_Unit_Id = @Currency_Unit_Id
                     AND cc.Conversion_Date = cuad.Service_Month
      WHERE
            bm.Commodity_Id = @Commodity_id
            AND cuad.Service_Month BETWEEN @Begin_Dt AND @End_Dt
      ORDER BY
            cuad.Service_Month
           ,bkt_cd.Code_Value DESC
           ,bm.Bucket_Name

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Aggregate_Sel_For_Site_By_Commodity] TO [CBMSApplication]
GO
