SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_CBMS_IMAGE_ID_FOR_RISK_DOCUMENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@riskDocumentId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE dbo.GET_CBMS_IMAGE_ID_FOR_RISK_DOCUMENT_P
@userId varchar(10),
@sessionId varchar(20),
@riskDocumentId int
AS
select cbms_image_id from RM_RISK_PLAN_PROFILE 
	where RM_RISK_PLAN_PROFILE_ID = @riskDocumentId
GO
GRANT EXECUTE ON  [dbo].[GET_CBMS_IMAGE_ID_FOR_RISK_DOCUMENT_P] TO [CBMSApplication]
GO
