SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.SAVE_MARKET_PRICE_POINT_STATE_MAP_P
@marketPricePointID int,
@stateID int
AS
begin
insert into  market_price_point_state_map(market_price_point_id,state_id) values(@marketPricePointID,@stateID) 
end




GO
GRANT EXECUTE ON  [dbo].[SAVE_MARKET_PRICE_POINT_STATE_MAP_P] TO [CBMSApplication]
GO
