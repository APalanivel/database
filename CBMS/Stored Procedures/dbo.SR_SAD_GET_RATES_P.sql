SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_RATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@utilityId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_RATES_P
@userId varchar(10),
@sessionId varchar(20),
@utilityId int
AS
set nocount on
IF(@utilityId>0)
BEGIN
	select rate_id,rate_name from rate
	where rate.vendor_id=@utilityId
END

else
begin
	select rate_id,rate_name from rate
end
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RATES_P] TO [CBMSApplication]
GO
