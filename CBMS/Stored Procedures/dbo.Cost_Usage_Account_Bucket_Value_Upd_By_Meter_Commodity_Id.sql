
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
     [dbo].[Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id]   
  
DESCRIPTION:  
	Updating account bucket values by meter and commodity
	Using for aggregation
        
INPUT PARAMETERS:  
Name            DataType          Default     Description  
-----------------------------------------------------------  
@Meter_Id		INT
@Commodity_id	INT		
     
OUTPUT PARAMETERS:  
Name			DataType          Default     Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------      
BEGIN TRAN
	EXEC dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id 25704,291 --13649
	SELECT cha.account_id,cha.meter_id,cu.SERVICE_MONTH,cu.el_cost,cu.el_usage,cu.el_tax,cu.ng_cost,cu.ng_usage,cu.ng_tax  
	FROM dbo.Cost_Usage cu JOIN Core.Client_Hier_Account cha ON cha.Account_Id = cu.ACCOUNT_ID AND cha.Account_Type = 'Utility'
		WHERE  cha.Meter_Id = 25704 --13649
ROLLBACK TRAN
BEGIN TRAN
	EXEC dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id 25703,290
	SELECT cha.account_id,cha.meter_id,cu.SERVICE_MONTH,cu.el_cost,cu.el_usage,cu.el_tax,cu.ng_cost,cu.ng_usage,cu.ng_tax  
	FROM dbo.Cost_Usage cu JOIN Core.Client_Hier_Account cha ON cha.Account_Id = cu.ACCOUNT_ID AND cha.Account_Type = 'Utility'
		WHERE  cha.Meter_Id = 25703
ROLLBACK TRAN
BEGIN TRAN
	EXEC dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id 322641,67
	SELECT cha.account_id,cha.meter_id,cu.SERVICE_MONTH,cu.Bucket_Value,cha.Commodity_Id
	FROM dbo.Cost_Usage_Account_Dtl cu JOIN Core.Client_Hier_Account cha ON cha.Account_Id = cu.ACCOUNT_ID AND cha.Account_Type = 'Utility'
	JOIN dbo.Bucket_Master bm ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
		WHERE cha.Meter_Id = 322641 AND bm.Commodity_Id= 67
ROLLBACK TRAN
        
AUTHOR INITIALS:  
 Initials    Name   
------------------------------------------------------------  
 BCH		 Balarjau     
 AP		 Athmaram Pabbathi
                    
MODIFICATIONS:  
 Initials   Date	    Modification  
------------------------------------------------------------  
 BCH		  2011-02-01  Created  
 AP		  2011-02-01  Removed IF statement for Electric Power & Natrual Gas
******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id
      @Meter_Id INT
     ,@Commodity_id INT
AS 
BEGIN    
      SET NOCOUNT ON ;
	      
      UPDATE
            cuad
      SET   
            cuad.Bucket_Value = 0
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN Core.Client_Hier_Account cha
                  ON cha.Account_Id = cuad.ACCOUNT_ID
                     AND cha.Account_Type = 'Utility'
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
      WHERE
            bm.Commodity_Id = @Commodity_id
            AND cha.Meter_Id = @Meter_Id
              
END 
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Bucket_Value_Upd_By_Meter_Commodity_Id] TO [CBMSApplication]
GO
