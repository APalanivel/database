SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_REASON_FOR_NOT_WINNING_COMMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@isBidGroup    	int       	          	
	@supplierContactVendorMapId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	@comments      	varchar(2000)	          	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_GET_REASON_FOR_NOT_WINNING_COMMENTS_P

@accountGroupId int,
@isBidGroup int,
@supplierContactVendorMapId int,
@comments varchar(2000) OUT

AS
set nocount on
SELECT @comments = (select 	comments

from 	sr_rfp_reason_not_winning

where	sr_account_group_id = @accountGroupId
	and is_bid_group = @isBidGroup
	and sr_rfp_supplier_contact_vendor_map_id = @supplierContactVendorMapId
	)

return
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_REASON_FOR_NOT_WINNING_COMMENTS_P] TO [CBMSApplication]
GO
