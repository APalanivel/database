SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:	dbo.[Utility_Dtl_History_Del_By_Vendor_Id]

DESCRIPTION: 

	Created to delete the summary and highlights history added for a utility.

INPUT PARAMETERS:
    Name				DataType          Default     Description
---------------------------------------------------------------------------------
	@Vendor_Id			INT

OUTPUT PARAMETERS:
      Name              DataType          Default     Description
------------------------------------------------------------    

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Utility_Dtl_History_Del_By_Vendor_Id 3835
	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			05/05/2011	Created to fix MAINT-596

******/

CREATE PROCEDURE dbo.Utility_Dtl_History_Del_By_Vendor_Id ( @Vendor_Id INT )
AS
BEGIN

      SET NOCOUNT ON ;

      DECLARE @Utility_Document TABLE
		( Utility_Document_id INT )
      DECLARE @Utility_Dtl_Election TABLE
		( Utility_Dtl_Election_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Transport TABLE
		( Utility_Dtl_Transport_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Balancing_Requirement TABLE
		( Utility_Dtl_Balancing_Requirement_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Service_Level TABLE
		( Utility_Dtl_Service_Level_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Notification TABLE
		( Utility_Dtl_Notification_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Volume_Requirement TABLE
		( Utility_Dtl_Volume_Requirement_Id INT )

      DECLARE @Utility_Dtl_Telemetering TABLE
		( Utility_Dtl_Telemetering_Id INT
		,Comment_Id INT )
      DECLARE @Utility_Dtl_Opportunity TABLE
		( Utility_Dtl_Opportunity_Id INT
		,Comment_Id INT )
      DECLARE @Vendor_Commodity_Map_List TABLE
		( Vendor_Commodity_Map_Id INT )

	  DECLARE @Comment TABLE (Comment_id INT)

      DECLARE
            @Utility_Document_Id INT
           ,@Utility_Dtl_Election_Id INT
           ,@Utility_Dtl_Transport_Id INT
           ,@Utility_Dtl_Balancing_Requirement_Id INT
           ,@Utility_Dtl_Service_Level_Id INT
           ,@Utility_Dtl_Notification_Id INT
           ,@Utility_Dtl_Volume_Requirement_Id INT
           ,@Utility_Dtl_Telemetering_Id INT
           ,@Utility_Dtl_Opportunity_Id INT
           ,@Comment_Id INT
	
      INSERT INTO
            @Vendor_Commodity_Map_List
            ( 
             Vendor_Commodity_Map_Id )
            SELECT
                  Vendor_Commodity_Map_Id
            FROM
                  dbo.VENDOR_COMMODITY_MAP
            WHERE
                  Vendor_Id = @Vendor_ID

      INSERT INTO
            @Utility_Document ( Utility_Document_id )
            SELECT
                  Utility_Document_id
            FROM
                  dbo.Utility_Document
            WHERE
                  Vendor_Id = @Vendor_Id

      INSERT INTO
            @Utility_Dtl_Election
            ( 
             Utility_Dtl_Election_Id
            ,Comment_Id )
            SELECT
                  ude.Utility_Dtl_Election_Id
                  ,ude.Comment_ID
            FROM
                  dbo.Utility_Dtl_Election ude
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = ude.Vendor_Commodity_Map_Id

      INSERT INTO
            @Utility_Dtl_Transport
            ( 
             Utility_Dtl_Transport_Id
            ,Comment_Id )
            SELECT
                  udt.Utility_Dtl_Transport_Id
                  ,udt.Comment_ID
            FROM
                  dbo.Utility_Dtl_Transport udt
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = udt.Vendor_Commodity_Map_Id

      INSERT INTO
            @Utility_Dtl_Balancing_Requirement
            ( 
             Utility_Dtl_Balancing_Requirement_Id
            ,Comment_Id )
            SELECT
                  ubr.Utility_Dtl_Balancing_Requirement_Id
                  ,ubr.Comment_ID
            FROM
                  dbo.Utility_Dtl_Balancing_Requirement ubr
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = ubr.Vendor_Commodity_Map_Id


      INSERT INTO
            @Utility_Dtl_Service_Level
            ( 
             Utility_Dtl_Service_Level_Id
            ,Comment_Id )
            SELECT
                  usl.Utility_Dtl_Service_Level_Id
                  ,usl.Comment_ID
            FROM
                  dbo.Utility_Dtl_Service_Level usl
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = usl.Vendor_Commodity_Map_Id


      INSERT INTO
            @Utility_Dtl_Notification
            ( 
             Utility_Dtl_Notification_Id
            ,Comment_Id )
            SELECT
                  udn.Utility_Dtl_Notification_Id
                  ,udn.Comment_ID
            FROM
                  dbo.Utility_Dtl_Notification udn
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = udn.Vendor_Commodity_Map_Id


      INSERT INTO
            @Utility_Dtl_Volume_Requirement
            ( 
             Utility_Dtl_Volume_Requirement_Id )
            SELECT
                  Utility_Dtl_Volume_Requirement_Id
            FROM
                  dbo.Utility_Dtl_Volume_Requirement uvr
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = uvr.Vendor_Commodity_Map_Id

      INSERT INTO
            @Utility_Dtl_Telemetering
            (
             Utility_Dtl_Telemetering_Id
            ,Comment_Id )
            SELECT
                  ut.Utility_Dtl_Telemetering_Id
                  ,ut.Comment_ID
            FROM
                  dbo.Utility_Dtl_Telemetering ut
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = ut.Vendor_Commodity_Map_Id


      INSERT INTO
            @Utility_Dtl_Opportunity
            ( 
             Utility_Dtl_Opportunity_Id
            ,Comment_Id )
            SELECT
                  uo.Utility_Dtl_Opportunity_Id
                  ,uo.Comment_ID
            FROM
                  dbo.Utility_Dtl_Opportunity uo
                  INNER JOIN @Vendor_Commodity_Map_List vcm
                        ON vcm.Vendor_Commodity_Map_Id = uo.Vendor_Commodity_Map_Id

	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Election
		  WHERE
				Comment_ID IS NOT NULL
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Transport
		  WHERE
				Comment_ID IS NOT NULL 
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Balancing_Requirement
		  WHERE
				Comment_ID IS NOT NULL 
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Service_Level
		  WHERE
				Comment_ID IS NOT NULL 
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Notification
		  WHERE
				Comment_ID IS NOT NULL 
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Telemetering
		  WHERE
				Comment_ID IS NOT NULL 
	INSERT INTO
		  @Comment ( Comment_Id )
		  SELECT
				Comment_Id
		  FROM
				@Utility_Dtl_Opportunity
		  WHERE
				Comment_ID IS NOT NULL

      BEGIN TRY
            BEGIN TRANSACTION

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Election )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Election_Id = Utility_Dtl_Election_Id
                        FROM
                              @Utility_Dtl_Election

                        EXEC dbo.Utility_Dtl_Election_del @Utility_Dtl_Election_Id

                        DELETE FROM
                              @Utility_Dtl_Election
                        WHERE
                              Utility_Dtl_Election_Id = @Utility_Dtl_Election_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Transport )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Transport_Id = Utility_Dtl_Transport_Id
                        FROM
                              @Utility_Dtl_Transport

                        EXEC dbo.Utility_Dtl_Transport_del @Utility_Dtl_Transport_Id

                        DELETE FROM
                              @Utility_Dtl_Transport
                        WHERE
                              Utility_Dtl_Transport_Id = @Utility_Dtl_Transport_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Balancing_Requirement )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Balancing_Requirement_Id = Utility_Dtl_Balancing_Requirement_Id
                        FROM
                              @Utility_Dtl_Balancing_Requirement

                        EXEC dbo.Utility_Dtl_Balancing_Requirement_del @Utility_Dtl_Balancing_Requirement_Id

                        DELETE FROM
                              @Utility_Dtl_Balancing_Requirement
                        WHERE
                              Utility_Dtl_Balancing_Requirement_Id = @Utility_Dtl_Balancing_Requirement_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Service_Level )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Service_Level_Id = Utility_Dtl_Service_Level_Id
                        FROM
                              @Utility_Dtl_Service_Level

                        EXEC dbo.Utility_Dtl_Service_Level_del @Utility_Dtl_Service_Level_Id

                        DELETE FROM
                              @Utility_Dtl_Service_Level
                        WHERE
                              Utility_Dtl_Service_Level_Id = @Utility_Dtl_Service_Level_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Notification )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Notification_Id = Utility_Dtl_Notification_Id
                        FROM
                              @Utility_Dtl_Notification

                        EXEC dbo.Utility_Dtl_Notification_Del @Utility_Dtl_Notification_Id

                        DELETE FROM
                              @Utility_Dtl_Notification
                        WHERE
                              Utility_Dtl_Notification_Id = @Utility_Dtl_Notification_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Volume_Requirement )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Volume_Requirement_Id = Utility_Dtl_Volume_Requirement_Id
                        FROM
                              @Utility_Dtl_Volume_Requirement

                        EXEC dbo.Utility_Dtl_Volume_Requirement_Del @Utility_Dtl_Volume_Requirement_Id

                        DELETE FROM
                              @Utility_Dtl_Volume_Requirement
                        WHERE
                              Utility_Dtl_Volume_Requirement_Id = @Utility_Dtl_Volume_Requirement_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Telemetering )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Telemetering_Id = Utility_Dtl_Telemetering_Id
                        FROM
                              @Utility_Dtl_Telemetering

                        EXEC dbo.Utility_Dtl_Telemetering_Del @Utility_Dtl_Telemetering_Id

                        DELETE FROM
                              @Utility_Dtl_Telemetering
                        WHERE
                              Utility_Dtl_Telemetering_Id = @Utility_Dtl_Telemetering_Id

                  END

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Dtl_Opportunity )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Dtl_Opportunity_Id = Utility_Dtl_Opportunity_Id
                        FROM
                              @Utility_Dtl_Opportunity

                        EXEC dbo.Utility_Dtl_Opportunity_Del @Utility_Dtl_Opportunity_Id

                        DELETE FROM
                              @Utility_Dtl_Opportunity
                        WHERE
                              Utility_Dtl_Opportunity_Id = @Utility_Dtl_Opportunity_Id

                  END


            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Utility_Document )
                  BEGIN

                        SELECT TOP 1
                              @Utility_Document_Id = Utility_Document_Id
                        FROM
                              @Utility_Document

                        EXEC dbo.Utility_Document_Del @Utility_Document_Id

                        DELETE FROM
                              @Utility_Document
                        WHERE
                              Utility_Document_Id = @Utility_Document_Id

                  END

            WHILE EXISTS ( SELECT
                              1
                           FROM
                              @Comment )
                  BEGIN

                        SELECT TOP 1
                              @Comment_Id = Comment_Id
                        FROM
                              @Comment

                        EXEC dbo.Comment_Del @Comment_Id

                        DELETE FROM
                              @Comment
                        WHERE
                              Comment_Id = @Comment_Id

                  END

            COMMIT TRANSACTION
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC dbo.usp_RethrowError @CustomMessage = 'Error when deleting Utility Summary and highlight history'

      END CATCH

END

GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_History_Del_By_Vendor_Id] TO [CBMSApplication]
GO
GO