
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.[CBMS_UPDATE_METER_P]


DESCRIPTION: Updates information Meter information

INPUT PARAMETERS:    
    Name						DataType        Default     Description    
----------------------------------------------------------------------------    
	@rateId						int
	@purchaseMethodTypeId		int
	@addressId					int
	@meterNumber				varchar(50)
	@taxExemptStatus			numeric(3216)
	@taxExemptStatusId			int
	@meterId					int
	@taxreviewDate				datetime
	@expirationDate				datetime
	@comments					varchar(4000)
	@Meter_Type_Cd				INT				NULL
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
----------------------------------------------------------------------------    


USAGE EXAMPLES:
----------------------------------------------------------------------------

	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'Meter Type'

	BEGIN TRANSACTION
		DECLARE @Meter_Id INT, @meterNumber VARCHAR(50) = 'Testing New - '+cast(getdate() AS VARCHAR(50))
		EXEC dbo.CBMS_ADD_UTILITY_ACCOUNT_METER_P @rateId =8233,@purchaseMethodTypeId =241,@accountId =122169,@addressId =1,
				@meterNumber = @meterNumber,@taxExemptStatus = null, @taxExemptionStatusId =null,@expirationDate =null,
				@comments  = 'Testing New',@meterId = @Meter_Id OUT, @Meter_Type_Cd = 102040
		SELECT * FROM dbo.METER WHERE METER_ID = @Meter_Id
		SET @meterNumber = 'Testing Update - '+cast(getdate() AS VARCHAR(50))
		EXEC dbo.CBMS_UPDATE_METER_P @rateId =8233,@purchaseMethodTypeId =241,@addressId =1,@meterNumber = @meterNumber,
				@taxExemptStatus = null, @taxExemptStatusId =null,@taxreviewDate = NULL,@expirationDate = NULL,
				@comments = 'Testing Update',@meterId = @Meter_Id, @Meter_Type_Cd = 102041
		SELECT * FROM dbo.METER WHERE METER_ID = @Meter_Id
	ROLLBACK TRANSACTION
	


AUTHOR INITIALS:
	Initials	Name
----------------------------------------------------------------------------
		DR		Deana Ritter
		NK		Nageswara Rao Kosuri
		RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
----------------------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	NK			01/08/2010	Changed the @meterNumber column size from 40 to 50 characters	
	RR			2015-10-06	Global Sourcing - Phase2 - Added new input type Meter Type
******/


CREATE PROCEDURE [dbo].[CBMS_UPDATE_METER_P]
      ( 
       @rateId INT
      ,@purchaseMethodTypeId INT
      ,@addressId INT
      ,@meterNumber VARCHAR(50)
      ,@taxExemptStatus NUMERIC(32, 16)
      ,@taxExemptStatusId INT
      ,@meterId INT
      ,@taxreviewDate DATETIME
      ,@expirationDate DATETIME
      ,@comments VARCHAR(4000)
      ,@Meter_Type_Cd INT = NULL )
AS 
BEGIN
      UPDATE
            METER
      SET   
            RATE_ID = @rateId
           ,PURCHASE_METHOD_TYPE_ID = @purchaseMethodTypeId
           ,ADDRESS_ID = @addressId
           ,METER_NUMBER = @meterNumber
           ,TAX_EXEMPT_STATUS = @taxExemptStatus
           ,TAX_EXEMPTION_ID = @taxExemptStatusId
           ,EXPIRATION_DATE = @expirationDate
           ,TAX_REVIEW_DATE = @taxreviewDate
           ,COMMENTS = @comments
           ,Meter_Type_Cd = @Meter_Type_Cd
      WHERE
            METER_ID = @meterId

      RETURN
END

GO

GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_METER_P] TO [CBMSApplication]
GO
