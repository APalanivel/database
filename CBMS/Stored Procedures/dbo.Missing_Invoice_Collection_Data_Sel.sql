SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.Missing_Invoice_Collection_Data_Sel            
                        
 DESCRIPTION:                        
			To get the details of Missing_Invoice_Collection_Data               
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id		INT  
                            
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


 
  EXEC [dbo].[Missing_Invoice_Collection_Data_Sel] 
      @Invoice_Collection_Account_Config_Id=1
  
  EXEC [dbo].[Missing_Invoice_Collection_Data_Sel] 
      @Invoice_Collection_Account_Config_Id=56
                           
     SELECT * FROM dbo.Account_Exception WHERE Account_Id=1342210
SELECT TOP 1 * FROM dbo.Invoice_Collection_Account_Config WHERE Account_Id=1342210 ORDER BY Invoice_Collection_Account_Config_Id DESC
                  
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-12-08       Created for Invoice Tracking   
 RKV                   2019-07-22		Added new exception Service_Type             
                       
******/  

CREATE PROCEDURE [dbo].[Missing_Invoice_Collection_Data_Sel]
      ( 
       @Invoice_Collection_Account_Config_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;  
      

      CREATE TABLE #Exceptions
            ( 
             Id INT
            ,Exception_Name VARCHAR(255) )

      CREATE TABLE #AllExceptions
            ( 
             Id INT
            ,Exception_Name VARCHAR(255) )

      INSERT      INTO #AllExceptions
                  ( Id, Exception_Name )
      VALUES
                  ( 1, 'Active is blank' )
                       ,
                  ( 2, 'Invoice collection start date is blank' )
                       ,
                  ( 3, 'Invoice collection end date is blank' )
                       ,
                  ( 4, 'Invoice collection officer is blank' )
                       ,
                  ( 5, 'Client Primary Contact is blank' )
                       ,
                  ( 6, 'Account Primary Contact is blank' )
                       ,
                  ( 7, 'Vendor Primary Contact is blank' )
                       ,
                  ( 8, '(Primary) Invoice Frequency is Blank' )
                       ,
                  ( 9, '(Primary) Frequency Pattern is Blank' )
                       ,
                  ( 10, 'Chase Priority is blank' )
                       ,
                  ( 11, 'Invoice Pattern is blank' )
						,
                  ( 12, 'No source (out of the seven options) is checked as Primary for invoice collection' )
				  		,
                  ( 13, 'Service Type is Blank' )		
					

      INSERT      INTO #Exceptions
                  ( 
                   Id
                  ,Exception_Name )
                  SELECT
                        Id
                       ,Exception_Name
                  FROM
                        #AllExceptions
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Invoice_Collection_Account_Config ica
                                     WHERE
                                          ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id )
                              
                              
            
            
      INSERT      INTO #Exceptions
                  ( 
                   ID
                  ,Exception_Name )
                  EXEC dbo.Missing_Invoice_Collection_Account_Config_Exception_Generate 
                        @Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id

      SELECT
            ID
           ,Exception_Name
      FROM
            #Exceptions

      DROP TABLE #Exceptions
      DROP TABLE #AllExceptions
END 



;
GO
GRANT EXECUTE ON  [dbo].[Missing_Invoice_Collection_Data_Sel] TO [CBMSApplication]
GO
