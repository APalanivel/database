SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_VIEW_PROFILE_DOCUMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE [dbo].[SR_SAD_VIEW_PROFILE_DOCUMENTS_P] 

@vendorId int

AS
set nocount on
DECLARE @profileId  int 

SELECT @profileId = (SELECT SR_SUPPLIER_PROFILE_ID FROM  SR_SUPPLIER_PROFILE WHERE VENDOR_ID = @vendorId)

SELECT     ENTITY.ENTITY_NAME AS CATEGORY,
           SR_SUPPLIER_PROFILE_DOCUMENTS.CBMS_IMAGE_ID, 
	   CBMS_IMAGE.CBMS_DOC_ID AS NAME, 
           SR_SUPPLIER_PROFILE_DOCUMENTS.UPLOADED_DATE AS UPLOAD_DATE, 
           SR_SUPPLIER_PROFILE_DOCUMENTS.REVIEW_DATE AS REVIEW_DATE
           
FROM       SR_SUPPLIER_PROFILE_DOCUMENTS INNER JOIN
           CBMS_IMAGE ON SR_SUPPLIER_PROFILE_DOCUMENTS.CBMS_IMAGE_ID = CBMS_IMAGE.CBMS_IMAGE_ID INNER JOIN
           ENTITY ON CBMS_IMAGE.CBMS_IMAGE_TYPE_ID = ENTITY.ENTITY_ID
WHERE     (SR_SUPPLIER_PROFILE_DOCUMENTS.SR_SUPPLIER_PROFILE_ID = @profileId)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_VIEW_PROFILE_DOCUMENTS_P] TO [CBMSApplication]
GO
