SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_SELECTED_PRICE_PRODUCTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@priceProductId            int,
@productType               int,
@productDetails            varchar(4000)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec SR_RFP_UPDATE_SELECTED_PRICE_PRODUCTS_P 155,0,'222222222'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_SELECTED_PRICE_PRODUCTS_P] 

@priceProductId int,
@productType int,
@productDetails varchar(4000)

AS
	
SET NOCOUNT ON


if @productType=1

   begin 

      UPDATE  SR_RFP_SELECTED_PRODUCTS 

      SET 
	      PRODUCT_DETAILS=@productDetails 
      WHERE 
	      PRICING_PRODUCT_ID=@priceProductId AND
	      IS_SYSTEM_PRODUCT=1
   end 

else if @productType=0

begin 

UPDATE  SR_RFP_SELECTED_PRODUCTS 

SET 
	PRODUCT_DETAILS=@productDetails 
WHERE 
	PRICING_PRODUCT_ID=@priceProductId AND
	IS_SYSTEM_PRODUCT is null
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SELECTED_PRICE_PRODUCTS_P] TO [CBMSApplication]
GO
