SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name: dbo.EC_Contract_Attribute_Upd             
              
Description:              
        To update Data to EC_Contract_Attribute table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------        
    @EC_Contract_Attribute_Id			INT
    @State_Id							INT
    @Commodity_Id						INT
    @EC_Contract_Attribute_Name			NVARCHAR(510)
    @Attribute_Type_Cd					INT
    @User_Info_Id						INT    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	DECLARE  @EC_Contract_Attribute_Id_Out INT 
	BEGIN TRAN  
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS'
    EXEC dbo.EC_Contract_Attribute_Ins 
      @State_Id = 1
     ,@Commodity_Id = 291
     ,@EC_Contract_Attribute_Name = 'Test_AS'
     ,@Attribute_Type_Cd = 100026
     ,@User_Info_Id = 49
     ,@EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id_Out OUTPUT
      
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Name='Test_AS'
	 
     EXEC dbo.EC_Contract_Attribute_Upd 
      @EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id_Out
     ,@State_Id = 1
     ,@Commodity_Id = 291
     ,@EC_Contract_Attribute_Name = 'Test_AS400_Updated'
     ,@Attribute_Type_Cd = 100026
     ,@User_Info_Id = 49
     
	SELECT * FROM EC_Contract_Attribute WHERE EC_Contract_Attribute_Id=@EC_Contract_Attribute_Id_Out
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Upd]
      ( 
       @EC_Contract_Attribute_Id INT
      ,@State_Id INT
      ,@Commodity_Id INT
      ,@EC_Contract_Attribute_Name NVARCHAR(255)
      ,@Attribute_Type_Cd INT
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      UPDATE
            dbo.EC_Contract_Attribute
      SET   
            State_Id = @State_Id
           ,Commodity_Id = @Commodity_Id
           ,EC_Contract_Attribute_Name = @EC_Contract_Attribute_Name
           ,Attribute_Type_Cd = @Attribute_Type_Cd
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            EC_Contract_Attribute_Id = @EC_Contract_Attribute_Id
            
END




;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Upd] TO [CBMSApplication]
GO
