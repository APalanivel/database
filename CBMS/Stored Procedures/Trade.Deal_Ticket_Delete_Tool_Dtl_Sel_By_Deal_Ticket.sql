SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Delete_Tool_Dtl_Sel_By_Deal_Ticket
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Delete_Tool_Dtl_Sel_By_Deal_Ticket 131474
	
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy

    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-05-22	GRM Proejct.


     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Delete_Tool_Dtl_Sel_By_Deal_Ticket] ( @Deal_Ticket_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            dt.Deal_Ticket_Id
           ,ch.Client_Name
           ,CASE WHEN COUNT(DISTINCT ch.City) > 1 THEN 'Multiple'
                 ELSE MAX(ch.City)
            END AS City
           ,CASE WHEN COUNT(DISTINCT ch.State_Name) > 1 THEN 'Multiple'
                 ELSE MAX(ch.State_Name)
            END AS State_Name
           ,REPLACE(CONVERT(VARCHAR(20), ( dt.Hedge_Start_Dt ), 106), ' ', '-') AS Hedge_Start_Dt
           ,REPLACE(CONVERT(VARCHAR(20), ( dt.Hedge_End_Dt ), 106), ' ', '-') AS Hedge_End_Dt
           ,CASE WHEN MAX(reclc.DEAL_TICKET_NUMBER) IS NOT NULL THEN 'Yes'
                 ELSE 'No'
            END AS Recalc
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Id = dt.Client_Id
                     AND ch.Client_Hier_Id = dtch.Client_Hier_Id
            LEFT JOIN dbo.RECALC_PHYSICAL_HEDGE reclc
                  ON reclc.DEAL_TICKET_NUMBER = dt.Deal_Ticket_Id
      WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
      GROUP BY
            dt.Deal_Ticket_Id
           ,ch.Client_Name
           ,dt.Hedge_Start_Dt
           ,dt.Hedge_End_Dt
           

END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Delete_Tool_Dtl_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
