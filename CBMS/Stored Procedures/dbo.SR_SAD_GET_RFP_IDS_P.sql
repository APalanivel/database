SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_RFP_IDS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@utility_id    	int       	          	
	@commodity_id  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec SR_SAD_GET_RFP_IDS_P '','',0,291

-- THIS IS USED TO GET rfp ids for that commodity, utility or all

CREATE PROCEDURE dbo.SR_SAD_GET_RFP_IDS_P
@userId varchar(10),
@sessionId varchar(20),
@utility_id int,
@commodity_id int

AS
set nocount on

if @commodity_id > 0 and @utility_id = 0 
begin

	select 	rfp_account.sr_rfp_id SR_RFP_ID
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_account_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 0
		and rfp_account.is_deleted = 0
		join sr_rfp rfp on rfp.sr_rfp_id = rfp_account.sr_rfp_id
		and rfp.commodity_type_id = @commodity_id
		
	
	union
	
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_bid_group_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 1
		and rfp_account.is_deleted = 0
		join sr_rfp rfp on rfp.sr_rfp_id = rfp_account.sr_rfp_id
		and rfp.commodity_type_id = @commodity_id


end
else if @commodity_id = 0 and @utility_id > 0 
begin
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_account_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 0
		and rfp_account.is_deleted = 0
		join account on rfp_account.account_id=account.account_id
		join vendor on vendor.vendor_id=account.vendor_id and vendor.vendor_id=@utility_id

		
	
	union
	
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_bid_group_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 1
		and rfp_account.is_deleted = 0
		join account on rfp_account.account_id=account.account_id
		join vendor on vendor.vendor_id=account.vendor_id and vendor.vendor_id=@utility_id
end
else if @commodity_id > 0 and @utility_id > 0 
begin
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_account_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 0
		and rfp_account.is_deleted = 0
		join account on rfp_account.account_id=account.account_id
		join vendor on vendor.vendor_id=account.vendor_id and vendor.vendor_id=@utility_id
		join sr_rfp rfp on rfp.sr_rfp_id = rfp_account.sr_rfp_id
		and rfp.commodity_type_id = @commodity_id
		
	
	union
	
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_bid_group_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 1
		and rfp_account.is_deleted = 0
		join account on rfp_account.account_id=account.account_id
		join vendor on vendor.vendor_id=account.vendor_id and vendor.vendor_id=@utility_id
		join sr_rfp rfp on rfp.sr_rfp_id = rfp_account.sr_rfp_id
		and rfp.commodity_type_id = @commodity_id
end
else 
begin
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_account_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 0
		and rfp_account.is_deleted = 0
	
		
	
	union
	
	select 	rfp_account.sr_rfp_id
	
	from	sr_rfp_reason_not_winning not_winning 
		join sr_rfp_account rfp_account 
		on rfp_account.sr_rfp_bid_group_id = not_winning.sr_account_group_id and not_winning.is_bid_group = 1
		and rfp_account.is_deleted = 0
	
end
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RFP_IDS_P] TO [CBMSApplication]
GO
