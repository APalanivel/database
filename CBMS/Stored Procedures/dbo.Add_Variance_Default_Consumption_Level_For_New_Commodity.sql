
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
  
NAME: [DBO].[Add_Variance_Default_Consumption_Level_For_New_Commodity]  
  
DESCRIPTION:  
  
 This procedure used to load the default variance consumption level tests for new services.  
  
INPUT PARAMETERS:  
 NAME       DATATYPE DEFAULT  DESCRIPTION  
-----------------------------------------------------------------------  
 @Commodity_Id     INT  
  
OUTPUT PARAMETERS:  
 NAME       DATATYPE DEFAULT  DESCRIPTION  
-----------------------------------------------------------------------  
  
 BEGIN TRAN  
   
  EXEC dbo.Add_Variance_Default_Consumption_Level_For_New_Commodity @Commodity_Id =57  
    
 ROLLBACK TRAN  
  
USAGE EXAMPLES:  
-----------------------------------------------------------------------  
  
AUTHOR INITIALS:  
 INITIALS NAME  
-----------------------------------------------------------------------  
 HG   Harihara Suthan G  
 AKR  Ashok Kumar Raju 
MODIFICATIONS:  
 INITIALS DATE  MODIFICATION  
-----------------------------------------------------------------------  
 HG   2013-04-22 Created for manage services requirement  
 AKR  2013-07-15 Modified for Detailed Variance 
*/  
CREATE PROCEDURE dbo.Add_Variance_Default_Consumption_Level_For_New_Commodity ( @Commodity_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      BEGIN TRY  
            BEGIN TRAN  
  
            INSERT      INTO dbo.Variance_Consumption_Level
                        ( 
                         Consumption_Level_Desc
                        ,Commodity_Id )
                        SELECT
                              'All' Consumption_Level_Desc
                             ,@Commodity_id
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Variance_Consumption_Level vcl
                                           WHERE
                                                vcl.Commodity_Id = @Commodity_Id
                                                AND vcl.Consumption_Level_Desc = 'All' )  
  
            INSERT      INTO dbo.Variance_Consumption_Level
                        ( 
                         Consumption_Level_Desc
                        ,Commodity_Id )
                        SELECT
                              'No Variance Testing' Consumption_Level_Desc
                             ,@Commodity_id
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Variance_Consumption_Level vcl
                                           WHERE
                                                vcl.Commodity_Id = @Commodity_Id
                                                AND vcl.Consumption_Level_Desc = 'No Variance Testing' )  
  
            INSERT      INTO dbo.Variance_Parameter_Commodity_Map
                        ( 
                         Commodity_Id
                        ,Variance_Parameter_Id )
                        SELECT
                              k.Commodity_Id
                             ,k.Variance_Parameter_Id
                        FROM
                              ( SELECT
                                    @Commodity_Id Commodity_Id
                                   ,vp.Variance_Parameter_Id
                                FROM
                                    dbo.Variance_Parameter vp
                                WHERE
                                    vp.Parameter IN ( 'Total Usage', 'Seasonal Usage', 'Total Cost', 'Unit Cost', 'Seasonal Cost', 'Tax Rate', 'Late Fees', 'Average Usage of Last 12 months' ) ) k
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.Variance_Parameter_Commodity_Map v
                                           WHERE
                                                v.Variance_Parameter_Id = k.Variance_Parameter_Id
                                                AND k.Commodity_Id = v.Commodity_Id ) 
  
            COMMIT TRAN  
  
      END TRY  
  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  BEGIN  
                        ROLLBACK TRAN  
                  END  
  
            EXEC usp_RethrowError  
  
      END CATCH  
     
END ;


;
GO

GRANT EXECUTE ON  [dbo].[Add_Variance_Default_Consumption_Level_For_New_Commodity] TO [CBMSApplication]
GO
