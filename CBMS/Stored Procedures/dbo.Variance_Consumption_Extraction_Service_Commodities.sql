SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Variance_Consumption_Extraction_Service_Commodities      
              
Description:              
			This sproc to update Invoice_Collection_Queue
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  
                 
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
---------------------------------------------------------------------------------------- 
EXEC Variance_Consumption_Extraction_Service_Commodities 

    
      
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	RKV             2020-01-09      Created for AVT.       
             
******/
CREATE PROCEDURE [dbo].[Variance_Consumption_Extraction_Service_Commodities]
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            C.Commodity_Id
            , C.Commodity_Name
        FROM
            Variance_Consumption_Extraction_Service_Param_Value VCESPV
            INNER JOIN Variance_Consumption_Level VCL
                ON VCESPV.Variance_Consumption_Level_Id = VCL.Variance_Consumption_Level_Id
            INNER JOIN Commodity C
                ON VCL.Commodity_Id = C.Commodity_Id
        GROUP BY
            C.Commodity_Id
            , C.Commodity_Name
        ORDER BY
            CASE WHEN C.Commodity_Name = 'Electric Power' THEN 1
                WHEN C.Commodity_Name = 'Natural Gas' THEN 2
                ELSE 3
            END;
    END;
GO
GRANT EXECUTE ON  [dbo].[Variance_Consumption_Extraction_Service_Commodities] TO [CBMSApplication]
GO
