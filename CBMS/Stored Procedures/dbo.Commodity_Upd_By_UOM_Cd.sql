SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    


NAME:    
dbo.Commodity_Upd_By_UOM_Cd   

DESCRIPTION:    
Used to get UOM Conversion from COMMODITY_UOM_CONVERSION table  

INPUT PARAMETERS:    
Name			DataType Default Description    
------------------------------------------------------------    
@Commodity_Id	INT,  
@Commodity_Name VARCHAR(50),  
@Base_UOM_Cd	INT,  
@Base_UOM_Value VARCHAR(25)  
          
OUTPUT PARAMETERS:    
Name			DataType Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC dbo.Commodity_Upd_By_UOM_Cd 290,82 

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari  
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
GB		 28/04/09		created  
SKA		 01/07/09		Modified as per the coding standards  
SS		 14/07/09		Eliminated Subselect
SKA		 02/09/09		Removed the subselect and used the ANSI joins.
GB		 16/09/09		Removed @Commodity_Name, @Base_UOM_Value from input parameter
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Commodity_Upd_By_UOM_Cd]
	@Base_Commodity_Id AS INT,  
	@Base_UOM_Cd AS INT 
AS  

BEGIN  
  
	SET NOCOUNT ON   

	UPDATE
		dbo.COMMODITY SET  BASE_UOM_CD = @Base_UOM_Cd
	WHERE
		COMMODITY_ID = @Base_Commodity_Id

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_Upd_By_UOM_Cd] TO [CBMSApplication]
GO
