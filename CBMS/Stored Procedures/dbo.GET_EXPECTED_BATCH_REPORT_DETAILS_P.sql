SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_EXPECTED_BATCH_REPORT_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE         PROCEDURE dbo.GET_EXPECTED_BATCH_REPORT_DETAILS_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int
 AS
set nocount on
select 
	category.report_category_name,
	list.report_name,
	cli.client_name,
	cli.client_id,
	div.division_name,
	div.division_id,
	vw.site_name,
	sit.site_id,
	addr.address_line1
from 
	rm_batch_configuration config, 
	rm_batch_configuration_details details 
	left join division div on (div.division_id = details.division_id)
	left join site sit on (sit.site_id = details.site_id)
	left join vwSiteName vw on(vw.site_id = sit.site_id)
	left join address addr on(addr.address_id = sit.primary_address_id),
	rm_report_batch batch,
	report_list list,
	report_category category,
	report_list_report_category_map map,
	client cli

 
where 
	config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
	and details.rm_batch_configuration_id = config.rm_batch_configuration_id
	and list.report_list_id = config.report_list_id
	and map.report_list_id = list.report_list_id
	and category.report_category_id = map.report_category_id
	and cli.client_id = config.client_id
	and batch.rm_report_batch_id = @batchId

group by 
	category.report_category_name,
	list.report_name,
	cli.client_name,
	cli.client_id,
	div.division_name,
	div.division_id,
	sit.site_id,
	vw.site_name,
	addr.address_line1,
	config.rm_batch_configuration_master_id

order by 
	category.report_category_name,
	list.report_name,
	cli.client_name,
	div.division_name,
	vw.site_name
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_BATCH_REPORT_DETAILS_P] TO [CBMSApplication]
GO
