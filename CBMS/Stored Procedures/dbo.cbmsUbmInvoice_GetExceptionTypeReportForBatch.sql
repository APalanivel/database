SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[cbmsUbmInvoice_GetExceptionTypeReportForBatch]
	( @ubm_batch_master_log_id int )
AS
BEGIN

	   select t.exception_type
		, case when cu.is_processed = 1 then 'Yes' else 'No' end is_processed
		, case when cu.is_reported = 1 then 'Yes' else 'No' end is_reported
		, case when d.is_closed = 1 then 'Yes' else 'No' end exception_is_closed
		, cr.entity_name closed_reason
		, count(*) invoice_count
	     from ubm_invoice ui with (nolock)
	     join cu_invoice cu with (nolock) on cu.ubm_invoice_id = ui.ubm_invoice_id
	     join vwCbmsCuInvoiceDetails vw with (nolock) on vw.cu_invoice_id = cu.cu_invoice_id
	     join cu_exception ex with (nolock) on ex.cu_invoice_id = cu.cu_invoice_id
	     join cu_exception_detail d with (nolock) on d.cu_exception_id = ex.cu_exception_id
	     join cu_exception_type t with (nolock) on t.cu_exception_type_id = d.exception_type_id
  left outer join entity cr with (nolock) on cr.entity_id = d.closed_reason_type_id
	    where ui.ubm_batch_master_log_id = @ubm_batch_master_log_id
	 group by t.exception_type
		, cu.is_processed
		, cu.is_reported
		, d.is_closed
		, cr.entity_name

	 order by t.exception_type
		, cr.entity_name

END


GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetExceptionTypeReportForBatch] TO [CBMSApplication]
GO
