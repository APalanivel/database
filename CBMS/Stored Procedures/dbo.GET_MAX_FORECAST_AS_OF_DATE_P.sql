SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_MAX_FORECAST_AS_OF_DATE_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int
AS
set nocount on
select MAX(forecast_as_of_date) from rm_forecast_volume 
					       where client_id = @clientId
GO
GRANT EXECUTE ON  [dbo].[GET_MAX_FORECAST_AS_OF_DATE_P] TO [CBMSApplication]
GO
