SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
NAME:                  
 [dbo].[Currency_Sel_By_Budget_Id]                  
                  
DESCRIPTION:                  
 This procedure gets country -currency mapping associated with a particular budget id                  
                             
INPUT PARAMETERS:                  
 Name              DataType          Default     Description                  
----------------------------------------------------------------                  
   @budget_id       INT                  
                       
OUTPUT PARAMETERS:                  
 Name              DataType          Default     Description                  
----------------------------------------------------------------                  
                  
USAGE EXAMPLES:                  
------------------------------------------------------------                  
 EXEC dbo.Currency_Sel_By_Budget_Id 8990              
                           
AUTHOR INITIALS:                  
      Initials    Name   Date                     
------------------------------------------------------------                  
  VN  Varada Nair   07-May-2013                  
                                   
MODIFICATIONS:                  
 Initials      Date           Modification                  
------------------------------------------------------------                  
     VN       05/09/2013      Removed unwanted joins, join with view            
******/                  
           
CREATE PROCEDURE [dbo].[Currency_Sel_By_Budget_Id] 
( 
@budget_id INT 
)
AS 
BEGIN                  
  
      SET NOCOUNT ON                
  
      SELECT
            c.COUNTRY_NAME
           ,cu.CURRENCY_UNIT_NAME
      FROM
            dbo.BUDGET bdg
            INNER JOIN dbo.budget_account ba
                  ON ba.budget_id = bdg.budget_id
            INNER JOIN dbo.account acc
                  ON acc.account_id = ba.account_id
            INNER JOIN Core.Client_Hier ch
                  ON ch.site_id = acc.site_id
            INNER JOIN dbo.COUNTRY c
                  ON c.COUNTRY_ID = ch.country_id
            LEFT JOIN dbo.CURRENCY_UNIT_COUNTRY_MAP Cucm
                  ON Cucm.COUNTRY_ID = C.country_id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                  ON cu.CURRENCY_UNIT_ID = cucm.currency_unit_id
      WHERE
            bdg.budget_id = @budget_id
            AND ba.IS_DELETED = 0
      GROUP BY
            C.COUNTRY_NAME
           ,CU.CURRENCY_UNIT_NAME   
        
END ;


;
GO
GRANT EXECUTE ON  [dbo].[Currency_Sel_By_Budget_Id] TO [CBMSApplication]
GO
