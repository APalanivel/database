SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 dbo.Invoice_Participation_Completed_Status_Summary_Sel  
   
 Description:    
   
 Input Parameters:    
    Name       DataType  Default Description    
------------------------------------------------------------------------    
    @Report_Year     INT   NULL  
    @Client_Id      INT   NULL  
    @Division_Id     INT   NULL  
    @Site_Client_Hier_Id INT   NULL  
    @RegionId      INT   NULL  
    @Country_Id     INT   NULL  
    @Site_Not_Managed   INT   NULL  
    @Start_Index     INT   1  
    @End_Index      INT   2147483647   
   
 Output Parameters:    
 Name  Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
EXECUTE dbo.Invoice_Participation_Completed_Status_Summary_Sel 2011, 170  
  
   
Author Initials:    
 Initials   Name  
------------------------------------------------------------  
 HG			Harihara Suthan G
 NR			Narayana Reddy  
   
 Modifications :    
 Initials   Date     Modification    
------------------------------------------------------------    
  HG    2012-07-18 MAINT-1362, Created based on dbo.Invoice_Participation_Completed_Status_Sel_By_Account  
       -- Values are pivoted as we have performance issue in application side if all the service months values are returned in a different rows. 
  NR	2020-07-02	SE2017-512 - Added @Rate_Id as input parameter. 
  
******/
CREATE PROCEDURE [dbo].[Invoice_Participation_Completed_Status_Summary_Sel]
    (
        @Report_Year INT
        , @Client_Id INT = NULL
        , @Division_Id INT = NULL
        , @Site_Client_Hier_Id INT = NULL
        , @Region_Id INT = NULL
        , @State_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Site_Type_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Rate_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Start_Month DATE
            , @End_Month DATE
            , @Usd_Currency_Unit_Id INT
            , @EP_Commodity_Id INT
            , @NG_Commodity_Id INT
            , @Site_Type_Name VARCHAR(200);

        CREATE TABLE #Account_Dtl
             (
                 Client_Name VARCHAR(200)
                 , Sitegroup_Name VARCHAR(200)
                 , Site_Id INT
                 , Client_Hier_Id INT
                 , City VARCHAR(200)
                 , State_Name VARCHAR(20)
                 , Region_Name VARCHAR(200)
                 , Address_Line1 VARCHAR(200)
                 , Account_Id INT
                 , Account_Number VARCHAR(200)
                 , Vendor_Name VARCHAR(200)
                 , Client_Currency_Group_Id INT
                 , Commodity_Id INT
                 , Total_Rows INT
                 , Rate_Name VARCHAR(200)
             );

        CREATE TABLE #IP_Data
             (
                 Account_Id INT
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Is_Complete INT
                 , PRIMARY KEY
                   (
                       Account_Id
                       , Client_Hier_Id
                       , Service_Month
                   )
             );

        CREATE CLUSTERED INDEX IDX_Account_Dtl_Account_Id
            ON #Account_Dtl
        (
            Site_Id
            , Account_Id
            , Commodity_Id
        )   ;

        DECLARE @Service_Months TABLE
              (
                  Service_Month DATE PRIMARY KEY CLUSTERED
                  , Month_Num SMALLINT
              );


        SELECT
            @Usd_Currency_Unit_Id = CU.CURRENCY_UNIT_ID
        FROM
            dbo.CURRENCY_UNIT CU
        WHERE
            CU.SYMBOL = 'USD';

        SELECT
            @EP_Commodity_Id = MAX(CASE WHEN com.Commodity_Name = 'Electric Power' THEN com.Commodity_Id
                                   END)
            , @NG_Commodity_Id = MAX(CASE WHEN com.Commodity_Name = 'Natural Gas' THEN com.Commodity_Id
                                     END)
        FROM
            dbo.Commodity com
        WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' );

        SELECT
            @Site_Type_Name = st.ENTITY_NAME
        FROM
            dbo.ENTITY st
        WHERE
            st.ENTITY_ID = @Site_Type_Id;

        INSERT INTO @Service_Months
             (
                 Service_Month
                 , Month_Num
             )
        SELECT
            dd.DATE_D
            , ROW_NUMBER() OVER (ORDER BY
                                     dd.DATE_D)
        FROM    (   SELECT
                        DATEADD(m, ch.Client_Fiscal_Offset, CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                    FROM
                        Core.Client_Hier ch
                    WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                    UNION ALL
                    SELECT
                        CAST('1/1/' + CAST(@Report_Year AS VARCHAR(4)) AS VARCHAR(10))
                    WHERE
                        @Client_Id IS NULL) X
                CROSS JOIN meta.DATE_DIM dd
        WHERE
            dd.DATE_D BETWEEN X.Start_month
                      AND     DATEADD(MONTH, -1, (DATEADD(YEAR, 1, X.Start_month)));

        SELECT
            @Start_Month = MIN(sm.Service_Month)
            , @End_Month = MAX(sm.Service_Month)
        FROM
            @Service_Months sm;

        WITH CTE_Account_Dtl
        AS (
               SELECT
                    CH.Client_Name
                    , CH.Sitegroup_Name
                    , CH.Site_Id
                    , CH.Client_Hier_Id
                    , CH.City
                    , CH.State_Name
                    , CH.Region_Name
                    , CH.Site_Address_Line1
                    , CHA.Account_Id
                    , CHA.Display_Account_Number AS Account_Number
                    , CHA.Account_Vendor_Name
                    , CH.Client_Currency_Group_Id
                    , CHA.Commodity_Id
                    , CHA.Rate_Name
                    , ROW_NUMBER() OVER (ORDER BY
                                             CH.Client_Name
                                             , CH.Sitegroup_Name
                                             , CH.City
                                             , CHA.Account_Id ASC) AS Row_Num
                    , COUNT(1) OVER () AS Total_Rows
               FROM
                    Core.Client_Hier CH
                    INNER JOIN Core.Client_Hier_Account CHA
                        ON CH.Client_Hier_Id = CHA.Client_Hier_Id
               WHERE
                    (   @Client_Id IS NULL
                        OR  CH.Client_Id = @Client_Id)
                    AND (   @Division_Id IS NULL
                            OR  CH.Sitegroup_Id = @Division_Id)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  CH.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND (   @Site_Type_Id IS NULL
                            OR  CH.Site_Type_Name = @Site_Type_Name)
                    AND (   @State_Id IS NULL
                            OR  CH.State_Id = @State_Id)
                    AND (   @Region_Id IS NULL
                            OR  CH.Region_ID = @Region_Id)
                    AND (   @Vendor_Id IS NULL
                            OR  CHA.Account_Vendor_Id = @Vendor_Id)
                    AND CH.Client_Not_Managed = 0
                    AND CH.Site_Not_Managed = 0
                    AND CH.Site_Closed = 0
                    AND CH.Division_Not_Managed = 0
                    AND CHA.Account_Not_Managed = 0
                    AND (   CHA.Supplier_Account_begin_Dt IS NULL
                            OR  CHA.Supplier_Account_begin_Dt <= @End_Month)
                    AND (   CHA.Supplier_Account_End_Dt IS NULL
                            OR  CHA.Supplier_Account_End_Dt >= @Start_Month)
                    AND (   @Rate_Id IS NULL
                            OR  CHA.Rate_Id = @Rate_Id)
               GROUP BY
                   CH.Client_Name
                   , CH.Sitegroup_Name
                   , CH.Site_Id
                   , CH.Client_Hier_Id
                   , CH.City
                   , CH.State_Name
                   , CH.Region_Name
                   , CH.Site_Address_Line1
                   , CHA.Account_Id
                   , CHA.Display_Account_Number
                   , CHA.Account_Vendor_Name
                   , CH.Client_Currency_Group_Id
                   , CHA.Commodity_Id
                   , CHA.Rate_Name
           )
        INSERT INTO #Account_Dtl
             (
                 Client_Name
                 , Sitegroup_Name
                 , Site_Id
                 , Client_Hier_Id
                 , City
                 , State_Name
                 , Region_Name
                 , Address_Line1
                 , Account_Id
                 , Account_Number
                 , Vendor_Name
                 , Client_Currency_Group_Id
                 , Commodity_Id
                 , Total_Rows
                 , Rate_Name
             )
        SELECT
            ad.Client_Name
            , ad.Sitegroup_Name
            , ad.Site_Id
            , ad.Client_Hier_Id
            , ad.City
            , ad.State_Name
            , ad.Region_Name
            , ad.Site_Address_Line1
            , ad.Account_Id
            , ad.Account_Number
            , ad.Account_Vendor_Name
            , ad.Client_Currency_Group_Id
            , ad.Commodity_Id
            , ad.Total_Rows
            , ad.Rate_Name
        FROM
            CTE_Account_Dtl ad
        WHERE
            ad.Row_Num BETWEEN @Start_Index
                       AND     @End_Index;

        INSERT  #IP_Data
             (
                 Account_Id
                 , Client_Hier_Id
                 , Service_Month
                 , Is_Complete
             )
        SELECT
            AD.Account_Id
            , AD.Client_Hier_Id
            , sm.Service_Month
            , (CASE WHEN SUM(CAST(IP.IS_EXPECTED AS INT)) = SUM(CAST(IP.IS_RECEIVED AS INT)) THEN 1
                   WHEN SUM(CAST(IP.IS_EXPECTED AS INT)) != SUM(CAST(IP.IS_RECEIVED AS INT)) THEN 0
               END) AS Complete
        FROM
            #Account_Dtl AD
            CROSS JOIN @Service_Months sm
            LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION IP
                ON IP.SITE_ID = AD.Site_Id
                   AND  IP.ACCOUNT_ID = AD.Account_Id
                   AND  IP.SERVICE_MONTH = sm.Service_Month
                   AND  AD.Commodity_Id IN ( @EP_Commodity_Id, @NG_Commodity_Id )
        GROUP BY
            AD.Account_Id
            , AD.Client_Hier_Id
            , sm.Service_Month;
        WITH CTE_Pivoted_IP_Data
        AS (
               SELECT
                    IP.Account_Id
                    , IP.Client_Hier_Id
                    , MAX(CASE WHEN dd.Month_Num = 1
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month1_Complete
                    , MAX(CASE WHEN dd.Month_Num = 2
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month2_Complete
                    , MAX(CASE WHEN dd.Month_Num = 3
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month3_Complete
                    , MAX(CASE WHEN dd.Month_Num = 4
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month4_Complete
                    , MAX(CASE WHEN dd.Month_Num = 5
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month5_Complete
                    , MAX(CASE WHEN dd.Month_Num = 6
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month6_Complete
                    , MAX(CASE WHEN dd.Month_Num = 7
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month7_Complete
                    , MAX(CASE WHEN dd.Month_Num = 8
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month8_Complete
                    , MAX(CASE WHEN dd.Month_Num = 9
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month9_Complete
                    , MAX(CASE WHEN dd.Month_Num = 10
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month10_Complete
                    , MAX(CASE WHEN dd.Month_Num = 11
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month11_Complete
                    , MAX(CASE WHEN dd.Month_Num = 12
                                    AND IP.Service_Month = dd.Service_Month THEN IP.Is_Complete
                          END) AS Month12_Complete
               FROM
                    #IP_Data IP
                    CROSS JOIN (   SELECT
                                        dd.DATE_D AS Service_Month
                                        , ROW_NUMBER() OVER (ORDER BY
                                                                 dd.DATE_D) Month_Num
                                   FROM
                                        meta.DATE_DIM dd
                                   WHERE
                                        dd.DATE_D BETWEEN @Start_Month
                                                  AND     @End_Month) dd
               GROUP BY
                   IP.Account_Id
                   , IP.Client_Hier_Id
           )
        SELECT
            AD.Client_Name
            , AD.Sitegroup_Name AS Division_Name
            , AD.City
            , AD.State_Name
            , AD.Region_Name
            , AD.City AS Site_Name
            , AD.Address_Line1
            , AD.Account_Id
            , AD.Account_Number
            , AD.Vendor_Name
            , AD.Rate_Name
            , CASE WHEN CTE.Month1_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month1_Complete
            , CASE WHEN CTE.Month2_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month2_Complete
            , CASE WHEN CTE.Month3_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month3_Complete
            , CASE WHEN CTE.Month4_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month4_Complete
            , CASE WHEN CTE.Month5_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month5_Complete
            , CASE WHEN CTE.Month6_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month6_Complete
            , CASE WHEN CTE.Month7_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month7_Complete
            , CASE WHEN CTE.Month8_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month8_Complete
            , CASE WHEN CTE.Month9_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month9_Complete
            , CASE WHEN CTE.Month10_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month10_Complete
            , CASE WHEN CTE.Month11_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month11_Complete
            , CASE WHEN CTE.Month12_Complete = 1 THEN 'Yes'
                  ELSE 'No'
              END AS Month12_Complete
            , @Start_Month AS Fiscal_Start
        FROM
            #Account_Dtl AD
            LEFT JOIN CTE_Pivoted_IP_Data CTE
                ON AD.Account_Id = CTE.Account_Id
                   AND  AD.Client_Hier_Id = CTE.Client_Hier_Id
        ORDER BY
            AD.Client_Name
            , AD.Sitegroup_Name
            , AD.City;
        DROP TABLE #Account_Dtl;
    END;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Completed_Status_Summary_Sel] TO [CBMSApplication]
GO
