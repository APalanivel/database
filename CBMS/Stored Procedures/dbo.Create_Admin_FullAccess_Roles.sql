SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: [dbo].Create_Admin_FullAccess_Roles            
                        
 DESCRIPTION:                        
				Creates Two Default Roles (Admin,Full Access(Non Admin)) when Client_Group_Info_Map is Loaded.                       
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_Id                  INT              
 @Group_Info_Id              VARCHAR(MAX)       
 @Assigned_User_Id           INT                      
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
BEGIN TRAN
select * from client_app_access_role where client_id=235   
select * from client_app_access_role_group_info_map where client_app_access_role_id  in (select client_app_access_role_id from client_app_access_role where client_id=235)
select * from user_info_client_app_access_role_map where client_app_access_role_id  in (select client_app_access_role_id from client_app_access_role where client_id=235)                        

 EXEC dbo.Create_Admin_FullAccess_Roles 
      @Client_Id = 235
     ,@Group_Info_Id = '232,391,264,265,338,339,219,209'
      ,@Assigned_User_Id=39151
      
select * from client_app_access_role where client_id=235   
select * from client_app_access_role_group_info_map where client_app_access_role_id  in (select client_app_access_role_id from client_app_access_role where client_id=235)
select * from user_info_client_app_access_role_map where client_app_access_role_id  in (select client_app_access_role_id from client_app_access_role where client_id=235)                        
ROLLBACK

 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                     2013-11-25       Created for RA Admin user management                          

                       
******/                        
                     
CREATE PROCEDURE dbo.Create_Admin_FullAccess_Roles
      ( 
       @Client_Id INT
      ,@Group_Info_Id VARCHAR(MAX)
      ,@Assigned_User_Id INT )
AS 
BEGIN 
      BEGIN TRY 
            BEGIN TRAN           
            SET NOCOUNT ON;           
                      
            DECLARE
                  @Admin_Function_CSV VARCHAR(MAX)= ''
                 ,@Full_Access_Non_Admin_Function_CSV VARCHAR(MAX)= ''
                 ,@ExistingAdminRoleId INT= 0
                 ,@ExistingFullAccessRoleId INT= 0
                 ,@AdminCodeValue VARCHAR(25)
                 ,@FullAccessCodeValue VARCHAR(25)
                 ,@AdminDesc [varchar](255)
                 ,@FullAccessDesc [varchar](255)
                 ,@AdminCodeId INT
                 ,@FullAccessCodeId INT
				 

            SELECT
                  @AdminCodeId = MAX(CASE WHEN cd.Code_Value = 'Admin' THEN cd.Code_Id
                                        END)
                 ,@FullAccessCodeId = MAX(CASE WHEN cd.Code_Value = 'Full Access (Non Admin)' THEN cd.Code_Id
                                             END)
                 ,@AdminDesc = MAX(CASE WHEN cd.Code_Value = 'Admin' THEN cd.Code_Dsc
                                   END)
                 ,@FullAccessDesc = MAX(CASE WHEN cd.Code_Value = 'Full Access (Non Admin)' THEN cd.Code_Dsc
                                        END)
                 ,@AdminCodeValue = MAX(CASE WHEN cd.Code_Value = 'Admin' THEN cd.Code_Value
                                        END)
                 ,@FullAccessCodeValue = MAX(CASE WHEN cd.Code_Value = 'Full Access (Non Admin)' THEN cd.Code_Value
                                             END)
            FROM
                  dbo.Codeset cs
                  INNER JOIN dbo.Code cd
                        ON cd.Codeset_Id = cs.Codeset_Id
            WHERE
                  cs.Codeset_Name = 'App_Access_Role_Type'
                  AND cd.Code_Value IN ( 'Admin', 'Full Access (Non Admin)' )
                                        
            SELECT
                  @Admin_Function_CSV = ufn.Segments + ',' + @Admin_Function_CSV
            FROM
                  dbo.ufn_split(@Group_Info_Id, ',') ufn
                  INNER JOIN dbo.Group_Info_App_Access_Role_Type_Cd gcd
                        ON gcd.Group_Info_Id = ufn.Segments
                  INNER JOIN dbo.Code c
                        ON c.Code_Id = gcd.App_Access_Role_Type_Cd
            WHERE
                  c.Code_Value = @AdminCodeValue
             
            SELECT
                  @Admin_Function_CSV = LEFT(@Admin_Function_CSV, LEN(@Admin_Function_CSV) - 1)
            WHERE
                  LEN(@Admin_Function_CSV) > 1

            SELECT
                  @Full_Access_Non_Admin_Function_CSV = ufn.Segments + ',' + @Full_Access_Non_Admin_Function_CSV
            FROM
                  dbo.ufn_split(@Group_Info_Id, ',') ufn
                  INNER JOIN dbo.Group_Info_App_Access_Role_Type_Cd gcd
                        ON gcd.Group_Info_Id = ufn.Segments
                  INNER JOIN dbo.Code c
                        ON c.Code_Id = gcd.App_Access_Role_Type_Cd
            WHERE
                  c.Code_Value = @FullAccessCodeValue 
            SELECT
                  @Full_Access_Non_Admin_Function_CSV = LEFT(@Full_Access_Non_Admin_Function_CSV, LEN(@Full_Access_Non_Admin_Function_CSV) - 1)
            WHERE
                  LEN(@Full_Access_Non_Admin_Function_CSV) > 1

            SELECT
                  @ExistingAdminRoleId = Client_App_Access_Role_Id
            FROM
                  dbo.Client_App_Access_Role
            WHERE
                  Client_Id = @Client_Id
                  AND App_Access_Role_Name = @AdminCodeValue
            SELECT
                  @ExistingFullAccessRoleId = Client_App_Access_Role_Id
            FROM
                  dbo.Client_App_Access_Role
            WHERE
                  Client_Id = @Client_Id
                  AND App_Access_Role_Name = @FullAccessCodeValue
                                 


            IF ( @ExistingAdminRoleId = 0
                 AND @Admin_Function_CSV <> '' ) 
                  BEGIN
                        EXEC dbo.Client_App_Access_Role_Ins 
                              @App_Access_Role_Name = @AdminCodeValue
                             ,@App_Access_Role_Dsc = @AdminCodeValue
                             ,@Client_Id = @Client_Id
                             ,@Created_User_Id = @Assigned_User_Id
                             ,@Group_Info_Id = @Admin_Function_CSV
                             ,@App_Access_Role_Type_Cd=@AdminCodeId
                  END     
            IF ( @ExistingAdminRoleId > 0
                 AND @Admin_Function_CSV <> '' ) 
                  BEGIN

                        EXEC dbo.Client_App_Access_Role_Group_Info_Map_Merge 
                              @Client_App_Access_Role_Id = @ExistingAdminRoleId
                             ,@Group_Info_Id = @Admin_Function_CSV
                             ,@Assigned_User_Id = @Assigned_User_Id 
                  END
            IF ( @ExistingAdminRoleId > 0
                 AND @Admin_Function_CSV = '' ) 
                  BEGIN
                        EXEC dbo.Client_App_Access_Role_Del 
                              @Client_App_Access_Role_Id = @ExistingAdminRoleId
	    
                  END	


            IF ( @ExistingFullAccessRoleId = 0
                 AND @Full_Access_Non_Admin_Function_CSV <> '' ) 
                  BEGIN
                        EXEC dbo.Client_App_Access_Role_Ins 
                              @App_Access_Role_Name = @FullAccessCodeValue
                             ,@App_Access_Role_Dsc = @FullAccessCodeValue
                             ,@Client_Id = @Client_Id
                             ,@Created_User_Id = @Assigned_User_Id
                             ,@Group_Info_Id = @Full_Access_Non_Admin_Function_CSV
                             ,@App_Access_Role_Type_Cd=@FullAccessCodeId
                  END     
            IF ( @ExistingFullAccessRoleId > 0
                 AND @Full_Access_Non_Admin_Function_CSV <> '' ) 
                  BEGIN
                        PRINT @Full_Access_Non_Admin_Function_CSV
                        EXEC dbo.Client_App_Access_Role_Group_Info_Map_Merge 
                              @Client_App_Access_Role_Id = @ExistingFullAccessRoleId
                             ,@Group_Info_Id = @Full_Access_Non_Admin_Function_CSV
                             ,@Assigned_User_Id = @Assigned_User_Id 
                  END
            IF ( @ExistingFullAccessRoleId > 0
                 AND @Full_Access_Non_Admin_Function_CSV = '' ) 
                  BEGIN
                        EXEC dbo.Client_App_Access_Role_Del 
                              @Client_App_Access_Role_Id = @ExistingFullAccessRoleId
	    
                  END	

     
            COMMIT TRANSACTION       
                                     
      END TRY                      
      BEGIN CATCH                      
            IF @@TRANCOUNT > 0 
                  BEGIN      
                        ROLLBACK TRANSACTION      
                  END                     
            EXEC dbo.usp_RethrowError                      
      END CATCH                                    
                             
END;

;
GO
GRANT EXECUTE ON  [dbo].[Create_Admin_FullAccess_Roles] TO [CBMSApplication]
GO
