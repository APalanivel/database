SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_CHECK_FOR_INDUSTRIAL_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@siteId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_SAD_CHECK_FOR_INDUSTRIAL_P 

@siteId int

as
set nocount on
DECLARE @entityId int
declare @cnt int
SELECT @entityId = (select entity_id from entity where entity_type = 150 and entity_name='Industrial' )

select @cnt = (select count(1) from site where site.site_type_id = @entityId and site.site_id = @siteId)

RETURN  @cnt
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_CHECK_FOR_INDUSTRIAL_P] TO [CBMSApplication]
GO
