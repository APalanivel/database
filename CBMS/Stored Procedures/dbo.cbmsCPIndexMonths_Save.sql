SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE         PROCEDURE [dbo].[cbmsCPIndexMonths_Save] 
	( @clearport_index_month_id int = null
	, @clearport_index_id int = null
	, @clearport_index_month datetime = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @clearport_index_month_id 



	if @this_id is null
	begin
		select @this_id = clearport_index_month_id from clearport_index_months 
		 where clearport_index_id = @clearport_index_id 
		   and clearport_index_month = @clearport_index_month

	end


	if @this_id is null
	begin

		insert into clearport_index_months
			( clearport_index_id
			, clearport_index_month
			)
		 values
			( @clearport_index_id
			, @clearport_index_month
			)

		set @this_id = @@IDENTITY
--		select @@IDENTITY as clearport_index_month_id

	end
	else
	begin

		   update clearport_index_months  with (rowlock)
		      set clearport_index_month = @clearport_index_month
			, clearport_index_id = @clearport_index_id
		    where clearport_index_month_id = @this_id

	end

	set nocount off

	exec cbmsCPIndexMonths_Get @this_id


END












SET QUOTED_IDENTIFIER ON 













GO
GRANT EXECUTE ON  [dbo].[cbmsCPIndexMonths_Save] TO [CBMSApplication]
GO
