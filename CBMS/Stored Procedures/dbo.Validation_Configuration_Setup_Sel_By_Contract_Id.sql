SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
      dbo.Validation_Configuration_Setup_Sel_By_Contract_Id
       
 DESCRIPTION:       
      Gets the Test details based on commodity.    
          
 INPUT PARAMETERS:      
 Name					DataType			Default			Description      
----------------------------------------------------------------------------     
 @Contract_Id			INT       
    
 OUTPUT PARAMETERS:    
 Name					DataType			Default			Description      
----------------------------------------------------------------------------     

 USAGE EXAMPLES:      
----------------------------------------------------------------------------     
    
  EXEC dbo.Validation_Configuration_Setup_Sel_By_Contract_Id
   @Contract_Id =172894
   

EXEC dbo.Validation_Configuration_Setup_Sel_By_Contract_Id
   @Contract_Id =52547

   
    
AUTHOR INITIALS:    
Initials		Name    
----------------------------------------------------------------------------     
 NR				Narayana Reddy    
     
    
 MODIFICATIONS       
 Initials	Date			Modification    
----------------------------------------------------------------------------    
 NR        2019-03-28		Created for Add Contract.    
    
******/



CREATE PROCEDURE [dbo].[Validation_Configuration_Setup_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Is_Mapped_Utility_Account BIT = 0
            , @Is_Mapped_Consolidated_Account BIT = 0
            , @Is_Mapped_Supplier_Account BIT = 0;





        SELECT
            @Is_Mapped_Supplier_Account = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND cha.Account_Type = 'Supplier';



        SELECT
            @Is_Mapped_Consolidated_Account = 1
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Account_Consolidated_Billing_Vendor acbv
                           WHERE
                                utility_cha.Account_Id = acbv.Account_Id);



        SELECT
            @Is_Mapped_Utility_Account = 1
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account utility_cha
                ON utility_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Account_Consolidated_Billing_Vendor acbv
                               WHERE
                                    utility_cha.Account_Id = acbv.Account_Id);


        SELECT
            @Is_Mapped_Supplier_Account AS Is_Mapped_Supplier_Account
            , @Is_Mapped_Consolidated_Account AS Is_Mapped_Consolidated_Account
            , @Is_Mapped_Utility_Account AS Is_Mapped_Utility_Account;

    END;

GO
GRANT EXECUTE ON  [dbo].[Validation_Configuration_Setup_Sel_By_Contract_Id] TO [CBMSApplication]
GO
