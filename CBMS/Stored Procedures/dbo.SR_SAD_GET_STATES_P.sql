SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_STATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_STATES_P  AS
set nocount on
select	STATE_ID,
	STATE_NAME

from	STATE

order by STATE_NAME
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_STATES_P] TO [CBMSApplication]
GO
