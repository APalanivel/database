SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.UPDATE_RM_SUCCESS_REPORT_P

@userId varchar(10),
@sessionId varchar(10),
@reportXml text,
@batchIdentity int

as
	set nocount on
update RM_REPORT set REPORT_XML=@reportXml, LAST_UPDATED_DATE = getdate() where RM_REPORT_ID=@batchIdentity
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RM_SUCCESS_REPORT_P] TO [CBMSApplication]
GO
