SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                      
Name:   dbo.EC_Meter_Attribute_Tracking_For_Excel_Ins                                   
                        
Description:                                      
        To insert Data to EC_Meter_Attribute_Tracking table through the excel template                                    
                        
 Input Parameters:                                      
     Name      DataType   Default   Description                                        
----------------------------------------------------------------------------------------                                        
     @Meter_Id     INT                        
     @EC_Meter_Attribute_Id  INT                        
     @Start_Dt     DATE                        
     @End_Dt     DATE                        
     @EC_Meter_Attribute_Value  NVARCHAR(255)                        
     @User_Info_Id    INT             
  @Meter_Attribute_Type_Value NVARCHAR(255)                 
     @Vendor_Type_Value    NVARCHAR(255)                       
                        
 Output Parameters:                                            
    Name        DataType   Default   Description                                        
----------------------------------------------------------------------------------------                                        
                        
 Usage Examples:                                          
----------------------------------------------------------------------------------------                                        
                   
                        
 BEGIN TRAN                         
                       
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='AIESH' and meter_id=1442723                       
  exec dbo.EC_Meter_Attribute_Tracking_For_Excel_Ins @Meter_Id=1442723,@EC_Meter_Attribute_Id=30,@EC_Meter_Attribute_Value=N'AIESH',          
  @Start_Dt='2020-04-29',@End_Dt='2020-04-29'          
,@User_Info_Id=49,@Meter_Attribute_Type_Value=N'Forecasted',@Vendor_Type_Value=N'Distributor'                     
                       
  SELECT * FROM dbo.EC_Meter_Attribute_Tracking WHERE EC_Meter_Attribute_Value='AIESH' and meter_id=1442723                        
 ROLLBACK TRAN                                
                        
Author Initials:                                      
    Initials  Name                                      
----------------------------------------------------------------------------------------                                        
 SLP     Sri Lakshmi Pallikonda          
                                    
 Modifications:                                      
    Initials        Date   Modification                                      
----------------------------------------------------------------------------------------                                        
    SLP    2020-01-16  Created for Bulk upload Tool : Meter Attributes  through Excel template        
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Ins]
    (
        @Meter_Id INT
        , @EC_Meter_Attribute_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @EC_Meter_Attribute_Value NVARCHAR(400)
        , @User_Info_Id INT
        , @Meter_Attribute_Type_Value NVARCHAR(400)
        , @Vendor_Type_Value VARCHAR(25) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @New_Exception_Status_Cd INT
            , @Closed_Exception_Status_Cd INT
            , @Account_Id INT
            , @Missing_Meter_Attribute_Exception_Type_Cd INT
            , @Actual_Attribute_Type_Cd INT
            , @Vendor_Type_Cd INT
            , @Meter_Attribute_Type_Cd INT;

        SELECT
            @Actual_Attribute_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = 'Actual'
            AND cs.Codeset_Name = 'MeterAttributeType';


        SELECT
            @Vendor_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = @Vendor_Type_Value
            AND cs.Codeset_Name = 'VendorType';


        SELECT
            @Meter_Attribute_Type_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cd.Code_Value = @Meter_Attribute_Type_Value
            AND cs.Codeset_Name = 'MeterAttributeType';

        SELECT
            @New_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'New';

        SELECT
            @Closed_Exception_Status_Cd = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cd.Code_Value = 'Closed';


        SELECT
            @Account_Id = Account_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            Meter_Id = @Meter_Id
            AND Account_Type = 'Utility';


        SELECT
            @Missing_Meter_Attribute_Exception_Type_Cd = C.Code_Id
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset CS
                ON C.Codeset_Id = CS.Codeset_Id
        WHERE
            CS.Codeset_Name = 'Exception Type'
            AND C.Code_Value = 'Missing Meter Attribute';

        INSERT INTO dbo.EC_Meter_Attribute_Tracking
             (
                 Meter_Id
                 , EC_Meter_Attribute_Id
                 , Start_Dt
                 , End_Dt
                 , EC_Meter_Attribute_Value
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , Meter_Attribute_Type_Cd
             )
        VALUES
            (@Meter_Id
             , @EC_Meter_Attribute_Id
             , @Start_Dt
             , @End_Dt
             , @EC_Meter_Attribute_Value
             , @User_Info_Id
             , GETDATE()
             , @User_Info_Id
             , GETDATE()
             , @Meter_Attribute_Type_Cd);


        EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P
            @entity_identifier = @Account_Id
            , @user_info_id = @User_Info_Id
            , @audit_type = 2
            , @entity_name = 'ACCOUNT_TABLE'
            , @entity_type = 500;


        --Close the Missing meter attribute exception only if actual meter attributes are configured        
        UPDATE
            ae
        SET
            ae.Exception_Status_Cd = @Closed_Exception_Status_Cd
            , ae.Closed_By_User_Id = @User_Info_Id
            , ae.Closed_Ts = GETDATE()
            , ae.Last_Change_Ts = GETDATE()
            , ae.Updated_User_Id = @User_Info_Id
        FROM
            dbo.Account_Exception ae
        WHERE
            ae.Meter_Id = @Meter_Id
            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd
            AND ae.Exception_Type_Cd = @Missing_Meter_Attribute_Exception_Type_Cd
            AND ae.Closed_By_User_Id IS NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.EC_Meter_Attribute_Tracking emat
                           WHERE
                                emat.Meter_Attribute_Type_Cd = @Actual_Attribute_Type_Cd
                                AND emat.Meter_Id = @Meter_Id);


    END;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_For_Excel_Ins] TO [CBMSApplication]
GO
