SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME:
	dbo.cbmsSSOSavings_GetByCategoryTotal

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    @client_id		INT    	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSSOSavings_GetByCategoryTotal 207

	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			03/31/2011	Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
******/

CREATE PROCEDURE dbo.cbmsSSOSavings_GetByCategoryTotal
( 
 @client_id INT )
AS 
BEGIN
   
      SELECT
            SS.SAVINGS_CATEGORY_TYPE_ID
           ,SUM(DISTINCT SS.TOTAL_ESTIMATED_SAVINGS) AS TOTAL_ESTIMATED_SAVINGS
           ,@client_id
           ,CAT.ENTITY_NAME AS SAVINGS_CATEGORY_TYPE
      FROM
            dbo.SSO_SAVINGS SS
            JOIN dbo.ENTITY CAT
                  ON SS.SAVINGS_CATEGORY_TYPE_ID = CAT.ENTITY_ID
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        dbo.SSO_SAVINGS_OWNER_MAP SSOM
                        JOIN Core.Client_Hier CH
                              ON CH.Client_Hier_Id = SSOM.Client_Hier_Id
                     WHERE
                        CH.Client_Id = @client_id
                        AND SS.SSO_SAVINGS_ID = SSOM.SSO_SAVINGS_ID )
      GROUP BY
            SS.SAVINGS_CATEGORY_TYPE_ID
           ,CAT.ENTITY_NAME
END

GO
GRANT EXECUTE ON  [dbo].[cbmsSSOSavings_GetByCategoryTotal] TO [CBMSApplication]
GO
GO