SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.INSERT_FORECAST_VOLUME_MANAGE_OVERRIDE_P

@userId varchar(10),
@sessionId varchar(20),
@siteId int,
@clientId int,
@year int,
@monthIdentifier datetime,
@forecastStatus varchar(100)

as
	set nocount on
declare @forecastId int, @divisionId int , @volumeTypeId int


	select @forecastId=rm_forecast_volume_id from rm_forecast_volume where 
		client_id=@clientId and forecast_year=@year
	
	select @divisionId=division_id from site where site_id=@siteId

	select @volumeTypeId=entity_id from entity where entity_type=285 and entity_name =@forecastStatus

	insert into rm_forecast_volume_details (rm_forecast_volume_id,site_id,division_id,volume,month_identifier,volume_type_id) 
		values
		(@forecastId,@siteId,@divisionId,0,@monthIdentifier,@volumeTypeId)

--select * from rm_forecast_volume_details where rm_forecast_volume_id =4986
-- select * from entity where entity_type=285
GO
GRANT EXECUTE ON  [dbo].[INSERT_FORECAST_VOLUME_MANAGE_OVERRIDE_P] TO [CBMSApplication]
GO
