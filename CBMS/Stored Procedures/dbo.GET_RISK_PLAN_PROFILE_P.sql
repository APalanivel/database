SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_RISK_PLAN_PROFILE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(50)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE  dbo.GET_RISK_PLAN_PROFILE_P
@userId varchar(10),
@sessionId varchar(50),
@clientId int

 AS
select 
	risk.rm_risk_plan_profile_id,
	risk.site_id, 
	risk.division_id, 
	risk.document_type_id, 
	risk.cbms_image_id, 
	cimage.cbms_doc_id,
	risk.document_level_type_id   
from 
client cli, CBMS_IMAGE cimage, ENTITY ent,
RM_RISK_PLAN_PROFILE risk 
left join division div on risk.division_id = div.division_id  and div.client_id =  @clientId
left join site sit on risk.site_id = sit.site_id and sit.division_id = div.division_id
where
risk.client_id = cli.client_id  
and cli.client_id =  @clientId
and risk.cbms_image_id = cimage.cbms_image_id 
and risk.document_type_id = ent.entity_id
GO
GRANT EXECUTE ON  [dbo].[GET_RISK_PLAN_PROFILE_P] TO [CBMSApplication]
GO
