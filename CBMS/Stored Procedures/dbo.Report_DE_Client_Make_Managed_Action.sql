SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_DE_Client_Make_Managed_Action 

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default	Description
---------------------------------------------------------------
	@Client_Name                                varchar(200)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------
   execute dbo.Report_DE_Client_Make_Managed_Action 'Novartis'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AB			Aditya Bharadwaj
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	AB       	2018-10-25	Created 

******/

CREATE PROCEDURE [dbo].[Report_DE_Client_Make_Managed_Action]
    (@Client_Name VARCHAR(200))
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @entity_id_client   INT,
            @entity_id_division INT,
            @user_info_id       INT,
            @audit_type         INT,
            @id                 INT;

        SET @id = 1;
        SET @audit_type = 2;

        SELECT
            @entity_id_client = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'CLIENT_TABLE'
            AND e.ENTITY_TYPE = 500
            AND e.ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @entity_id_division = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'DIVISION_TABLE'
            AND e.ENTITY_TYPE = 500
            AND e.ENTITY_DESCRIPTION = 'Table_Type';

        SELECT
            @user_info_id = ui.USER_INFO_ID
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USERNAME = 'conversion'
            AND ui.FIRST_NAME = 'System';

        -- pulling client id into temp table

        SELECT
            CLIENT_ID
        INTO
            #temp
        FROM
            dbo.CLIENT
        WHERE
            CLIENT_NAME = @Client_Name
            AND NOT_MANAGED = 1;

        -- updating client table 

        UPDATE
            dbo.CLIENT
        SET
            NOT_MANAGED = 0,
            NOT_MANAGED_BY_ID = NULL,
            NOT_MANAGED_DATE = NULL
        WHERE
            CLIENT_ID =
            (
                SELECT
                    CLIENT_ID
                FROM
                    #temp
            );

        -- inserting the changes into entity audit table

        INSERT INTO dbo.ENTITY_AUDIT
            (
                ENTITY_ID,
                ENTITY_IDENTIFIER,
                USER_INFO_ID,
                AUDIT_TYPE,
                MODIFIED_DATE
            )
                    SELECT
                        @entity_id_client,
                        CLIENT_ID,
                        @user_info_id,
                        @audit_type,
                        GETDATE()
                    FROM
                        #temp;

        -- pulling sitegroup ids for above client into temp1 table

        SELECT DISTINCT
               SiteGroup_Id
        INTO
               #temp1
        FROM
               dbo.Division_Dtl d
        WHERE
               CLIENT_ID =
            (
                SELECT
                    CLIENT_ID
                FROM
                    #temp
            )
               AND d.NOT_MANAGED = 1;

        -- updating division_dtl table

        WHILE (@id <=
                  (
                      SELECT
                          COUNT(*)
                      FROM
                          #temp1
                  )
              )
            BEGIN
                BEGIN TRY
                    BEGIN TRANSACTION;

                    UPDATE
                        dbo.Division_Dtl
                    SET
                        NOT_MANAGED = 0,
                    NOT_MANAGED_BY_ID = NULL,
                        NOT_MANAGED_DATE = NULL
                    WHERE
                        CLIENT_ID =
                        (
                            SELECT
                                CLIENT_ID
                            FROM
                                #temp
                        )
                        AND Division_Dtl.NOT_MANAGED = 1;


                    -- inserting the changes into entity audit table

                    INSERT INTO dbo.ENTITY_AUDIT
                        (
                            ENTITY_ID,
                            ENTITY_IDENTIFIER,
                            USER_INFO_ID,
                            AUDIT_TYPE,
                            MODIFIED_DATE
                        )
                                SELECT
                                    @entity_id_division,
                                    SiteGroup_Id,
                                    @user_info_id,
                                    @audit_type,
                                    GETDATE()
                                FROM
                                    #temp1;
                    COMMIT;
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK;
                        END;
                END CATCH;
                SET @id = @id + 1;
            END;
    END;


GO
