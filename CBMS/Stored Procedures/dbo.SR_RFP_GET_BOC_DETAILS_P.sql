SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_BOC_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec  dbo.SR_RFP_GET_BOC_DETAILS_P 1
CREATE       PROCEDURE dbo.SR_RFP_GET_BOC_DETAILS_P 

@accountGroupId int 

as
set nocount on
declare @isgroup int
declare @account int
set @isgroup = 0
set @account = @accountGroupId

select @isgroup =sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_account_id = @accountGroupId and is_deleted = 0

if @isgroup > 0
begin
set @account = @isgroup
end





SELECT     E1.ENTITY_NAME AS service_level, 
	   bid_req.DELIVERY_POINT,
           price_comments.PRICE_RESPONSE_COMMENTS AS price, 
           E4.ENTITY_NAME AS inclusive_of_fuel, 
	   E2.ENTITY_NAME AS supplier_responsible_for_nomination, 
           E3.ENTITY_NAME AS supplier_responsible_for_balancing

FROM         dbo.sr_rfp_sop_details sop_details INNER JOIN
	      dbo.SR_RFP_ACCOUNT_TERM acc_term ON sop_details.SR_RFP_ACCOUNT_TERM_ID = acc_term.SR_RFP_ACCOUNT_TERM_ID and acc_term.is_sop = 1
		and sop_details.is_recommended = 1 INNER JOIN
	      dbo.SR_RFP_BID bid ON sop_details.SR_RFP_BID_ID = bid.SR_RFP_BID_ID INNER JOIN
	      dbo.SR_RFP_BID_REQUIREMENTS bid_req ON bid.SR_RFP_BID_REQUIREMENTS_ID = bid_req.SR_RFP_BID_REQUIREMENTS_ID INNER JOIN
	      dbo.SR_RFP_SUPPLIER_SERVICE supp_ser ON bid.SR_RFP_SUPPLIER_SERVICE_ID = supp_ser.SR_RFP_SUPPLIER_SERVICE_ID INNER JOIN
	      dbo.SR_RFP_DELIVERY_FUEL del_fuel ON supp_ser.SR_RFP_DELIVERY_FUEL_ID = del_fuel.SR_RFP_DELIVERY_FUEL_ID Left JOIN
	      dbo.ENTITY E1 ON bid_req.TRANSPORTATION_LEVEL_TYPE_ID = E1.ENTITY_ID Left JOIN
	      dbo.ENTITY E3 ON bid_req.BALANCING_TYPE_ID = E3.ENTITY_ID INNER JOIN
	      dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS price_comments ON 
	      bid.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = price_comments.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID Left JOIN
	      dbo.ENTITY E4 ON del_fuel.FUEL_IN_BID_TYPE_ID = E4.ENTITY_ID Left JOIN
	      dbo.ENTITY E2 ON bid_req.NOMINATION_TYPE_ID = E2.ENTITY_ID
WHERE     (acc_term.SR_ACCOUNT_GROUP_ID = @account)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_BOC_DETAILS_P] TO [CBMSApplication]
GO
