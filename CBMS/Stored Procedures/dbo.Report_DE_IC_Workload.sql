SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Report_DE_IC_Workload            
                        
 DESCRIPTION:                        
			This will allow the managers to gain a better understanding of the scope of officers and their allocations of  chase.
			                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            


   EXEC [dbo].[Report_DE_IC_Workload] 
       @Client_Id=-1
       
    --A J Rorato   
    
   EXEC [dbo].[Report_DE_IC_Workload] 
       @Client_Id=12628

      
   EXEC [dbo].[Report_DE_IC_Workload] 
       @Client_Id=141

 
       SELECT top 1 ch.Client_Id FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'A J Rorato'                    
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam  
 RKV                   Ravi Kumar Vegesna     
 NR					   Narayana Reddy   
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2017-01-25       Created for Invoice Tracking.
 RKV                   2017-12-05       SE2017-313,Added new column to the result set Invoice_Collection_Owner
 NR					   2018-01-02       SE2017-388 - IC Reports - To excluded the  Demo Clients.                 
               
                       
******/
CREATE PROCEDURE [dbo].[Report_DE_IC_Workload]
    (
        @Client_Id INT = -1
        , @Country_Id INT = -1
        , @State_Id INT = -1
        , @Client_Invoice_Collection_Officer_User_Id INT = -1
        , @Invoice_Collection_Officer_User_Id INT = -1
        , @Client_Invoice_Collection_Coordinator_User_Id INT = -1
        , @Invoice_Collection_Owner_Id INT = -1
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Data_Feed_Source INT
            , @Vendor_Source INT
            , @Client_Source INT
            , @Online INT
            , @ETL INT
            , @Partner INT
            , @Vendor_Primary_Contact INT
            , @Client_Primary_Contact INT
            , @Account_Primary_Contact INT
            , @Mail_Redirect INT
            , @Today_Date DATE = GETDATE()
            , @Issue_Open_Status INT
            , @Exclude_Demo_Client_Type_Id INT;

        SELECT
            @Exclude_Demo_Client_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            ENTITY_NAME = 'Demo'
            AND ENTITY_TYPE = 125
            AND ENTITY_DESCRIPTION = 'Client Type';


        SELECT
            @Data_Feed_Source = MAX(CASE WHEN c.Code_Value = 'Data Feed' THEN c.Code_Id
                                    END)
            , @Vendor_Source = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                   END)
            , @Client_Source = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                   END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceCollectionSource'
            AND c.Code_Value IN ( 'Data Feed', 'Vendor', 'Client' );

        SELECT
            @Issue_Open_Status = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            Code_Value = 'Open'
            AND cs.Codeset_Name = 'IC Chase Status';

        SELECT
            @Online = MAX(CASE WHEN c.Code_Value = 'Online' THEN c.Code_Id
                          END)
            , @ETL = MAX(CASE WHEN c.Code_Value = 'ETL' THEN c.Code_Id
                         END)
            , @Partner = MAX(CASE WHEN c.Code_Value = 'Partner' THEN c.Code_Id
                             END)
            , @Vendor_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                            END)
            , @Client_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                            END)
            , @Account_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                             END)
            , @Mail_Redirect = MAX(CASE WHEN c.Code_Value = 'Mail Redirect' THEN c.Code_Id
                                   END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Online', 'Vendor Primary Contact', 'Client Primary Contact'
                                  , 'Account Primary Contact', 'Mail Redirect', 'ETL', 'Partner' );
        WITH cte_ss
        AS (
               SELECT
                    ch.Client_Name AS Client
                    , ch.Country_Name AS Country
                    , ch.State_Name
                    , REPLACE(CONVERT(NVARCHAR, iccc.Invoice_Collection_Service_Start_Dt, 106), ' ', '-') AS [Invoice Collection Service Start Date]
                    , REPLACE(CONVERT(NVARCHAR, iccc.Invoice_Collection_Service_End_Dt, 106), ' ', '-') AS [Invoice Collection Service End Date]
                    , co.FIRST_NAME + ' ' + co.LAST_NAME AS [Main Invoice Collection Officer]
                    , ao.FIRST_NAME + ' ' + ao.LAST_NAME AS [Invoice Collection Officer]
                    , cc.FIRST_NAME + ' ' + cc.LAST_NAME AS [Invoice Collection Coordinator]
                    , st.Code_Value AS [Invoice Collection Service Level]
                    , ISNULL((icsc.Cnt), 0) AS [No of Chase Accounts (Current)]
                    , ISNULL((icsci.Cnt), 0) AS [No of Chase Accounts (Issue)]
                    , ISNULL((icsd.Cnt), 0) AS [No of Download Accounts (Current)]
                    , ISNULL((icsdi.Cnt), 0) AS [No of Download Accounts (Issues)]
                    , (ISNULL(icsu.Cnt, 0)) AS [No of Data Feed Partner Accounts (Current)]
                    , ISNULL((icsui.Cnt), 0) AS [No of Data Feed Partner Accounts (Issues)]
					 , (ISNULL(icsetl.Cnt, 0)) AS [No of Data Feed ETL Accounts (Current)]
                    , ISNULL((icsetli.Cnt), 0) AS [No of Data Feed ETL Accounts (Issues)]
                    , ISNULL((icsns.Cnt), 0) AS [No of Undefined Accounts (Current)]
                    , ISNULL((icsnsi.Cnt), 0) AS [No of Undefined Accounts (Issues)]
                    , ISNULL((excp.Cnt), 0) AS [Number of Open Exceptions in Current Period]
                    , icac.Invoice_Collection_Account_Config_Id
                    , COUNT(DISTINCT cha.Account_Id) AS Accounts
                    , COUNT(icac.Invoice_Collection_Account_Config_Id) AS icac
                    , cha.Account_Id
                    , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME AS Invoice_Collection_Owner
                    , stc.Code_Value Service_Type
               FROM
                    Core.Client_Hier ch
                    INNER JOIN Core.Client_Hier_Account cha
                        ON ch.Client_Hier_Id = cha.Client_Hier_Id
                    INNER JOIN dbo.Invoice_Collection_Client_Config iccc
                        ON ch.Client_Id = iccc.Client_Id
                    INNER JOIN dbo.Invoice_Collection_Account_Config icac
                        ON cha.Account_Id = icac.Account_Id
                    LEFT JOIN dbo.USER_INFO co
                        ON iccc.Invoice_Collection_Officer_User_Id = co.USER_INFO_ID
                    LEFT JOIN dbo.USER_INFO cc
                        ON iccc.Invoice_Collection_Coordinator_User_Id = cc.USER_INFO_ID
                    LEFT JOIN dbo.USER_INFO ao
                        ON icac.Invoice_Collection_Officer_User_Id = ao.USER_INFO_ID
                    LEFT JOIN dbo.USER_INFO icowner
                        ON icac.Invoice_Collection_Owner_User_Id = icowner.USER_INFO_ID
                    LEFT JOIN dbo.Code st
                        ON iccc.Invoice_Collection_Service_Level_Cd = st.Code_Id
                    LEFT JOIN --Primary contacts
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            (   (   ics.Invoice_Collection_Source_Cd = @Vendor_Source
                                    AND ics.Invoice_Source_Type_Cd IN ( @Vendor_Primary_Contact, @Mail_Redirect ))
                                OR  (   ics.Invoice_Collection_Source_Cd = @Client_Source
                                        AND ics.Invoice_Source_Type_Cd IN ( @Client_Primary_Contact
                                                                            , @Account_Primary_Contact )))
                            AND ics.Is_Primary = 1
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsc
                        ON icsc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --Primary contacts with issues
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            (   (   ics.Invoice_Collection_Source_Cd = @Vendor_Source
                                    AND ics.Invoice_Source_Type_Cd IN ( @Vendor_Primary_Contact, @Mail_Redirect ))
                                OR  (   ics.Invoice_Collection_Source_Cd = @Client_Source
                                        AND ics.Invoice_Source_Type_Cd IN ( @Client_Primary_Contact
                                                                            , @Account_Primary_Contact )))
                            AND ics.Is_Primary = 1
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Issue_Log icil
                                                INNER JOIN dbo.Invoice_Collection_Queue icq
                                                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                           WHERE
                                                icq.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
                                                AND icil.Issue_Status_Cd = @Issue_Open_Status)
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsci
                        ON icsci.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --online
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            (   (   ics.Invoice_Collection_Source_Cd = @Vendor_Source
                                    AND ics.Invoice_Source_Type_Cd = @Online)
                                OR  (   ics.Invoice_Collection_Source_Cd = @Client_Source
                                        AND ics.Invoice_Source_Type_Cd = @Online))
                            AND ics.Is_Primary = 1
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsd
                        ON icsd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --online with issues
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            (   (   ics.Invoice_Collection_Source_Cd = @Vendor_Source
                                    AND ics.Invoice_Source_Type_Cd = @Online)
                                OR  (   ics.Invoice_Collection_Source_Cd = @Client_Source
                                        AND ics.Invoice_Source_Type_Cd = @Online))
                            AND ics.Is_Primary = 1
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Issue_Log icil
                                                INNER JOIN dbo.Invoice_Collection_Queue icq
                                                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                           WHERE
                                                icq.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
                                                AND icil.Issue_Status_Cd = @Issue_Open_Status)
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsdi
                        ON icsdi.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --DATA Feed Partner
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            ics.Invoice_Collection_Source_Cd = @Data_Feed_Source
                            AND ics.Invoice_Source_Type_Cd = @Partner
                            AND ics.Is_Primary = 1
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsu
                        ON icsu.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --DATA Feed Partner with issues
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            ics.Invoice_Collection_Source_Cd = @Data_Feed_Source
                            AND ics.Invoice_Source_Type_Cd = @Partner
                            AND ics.Is_Primary = 1
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Issue_Log icil
                                                INNER JOIN dbo.Invoice_Collection_Queue icq
                                                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                           WHERE
                                                icq.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
                                                AND icil.Issue_Status_Cd = @Issue_Open_Status)
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsui
                        ON icsui.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                     LEFT JOIN --DATA Feed ETL
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            ics.Invoice_Collection_Source_Cd = @Data_Feed_Source
                            AND ics.Invoice_Source_Type_Cd = @ETL
                            AND ics.Is_Primary = 1
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsetl
                        ON icsetl.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --DATA Feed Partner with issues
                    (   SELECT
                            1 AS Cnt
                            , ics.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Account_Invoice_Collection_Source ics
                        WHERE
                            ics.Invoice_Collection_Source_Cd = @Data_Feed_Source
                            AND ics.Invoice_Source_Type_Cd = @ETL
                            AND ics.Is_Primary = 1
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Issue_Log icil
                                                INNER JOIN dbo.Invoice_Collection_Queue icq
                                                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                           WHERE
                                                icq.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
                                                AND icil.Issue_Status_Cd = @Issue_Open_Status)
                        GROUP BY
                            ics.Invoice_Collection_Account_Config_Id) icsetli
                        ON icsetli.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
					
					LEFT JOIN --NO SOURCE 
                    (   SELECT
                            1 AS Cnt
                            , ica.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Invoice_Collection_Account_Config ica
                        WHERE
                            NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Account_Invoice_Collection_Source ics1
                                           WHERE
                                                ics1.Invoice_Collection_Account_Config_Id = ica.Invoice_Collection_Account_Config_Id)) icsns
                        ON icsns.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN --NO SOURCE with issues
                    (   SELECT
                            1 AS Cnt
                            , ica.Invoice_Collection_Account_Config_Id
                        FROM
                            dbo.Invoice_Collection_Account_Config ica
                        WHERE
                            NOT EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Account_Invoice_Collection_Source ics1
                                           WHERE
                                                ics1.Invoice_Collection_Account_Config_Id = ica.Invoice_Collection_Account_Config_Id)
                            AND EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Invoice_Collection_Issue_Log icil
                                                INNER JOIN dbo.Invoice_Collection_Queue icq
                                                    ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                                           WHERE
                                                icq.Invoice_Collection_Account_Config_Id = ica.Invoice_Collection_Account_Config_Id
                                                AND icil.Issue_Status_Cd = @Issue_Open_Status)) icsnsi
                        ON icsnsi.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT JOIN
                    (   SELECT
                            icq.Invoice_Collection_Account_Config_Id
                            , COUNT(icq.Invoice_Collection_Queue_Id) AS Cnt
                        FROM
                            dbo.Invoice_Collection_Queue icq
                            INNER JOIN dbo.Code c
                                ON icq.Invoice_Collection_Queue_Type_Cd = c.Code_Id
                            INNER JOIN dbo.Code icqs
                                ON icq.Status_Cd = icqs.Code_Id
                        WHERE
                            c.Code_Value = 'ICE'
                            AND icqs.Code_Value = 'Open'
                        GROUP BY
                            icq.Invoice_Collection_Account_Config_Id) excp
                        ON excp.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    LEFT OUTER JOIN dbo.Code AS stc
                        ON stc.Code_Id = icac.Service_Type_Cd
               WHERE
                    (   @Client_Id = -1
                        OR  ch.Client_Id = @Client_Id)
                    AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
                    AND (   @Country_Id = -1
                            OR  ch.Country_Id = @Country_Id)
                    AND (   @State_Id = -1
                            OR  ch.State_Id = @State_Id)
                    AND (   @Client_Invoice_Collection_Officer_User_Id = -1
                            OR  iccc.Invoice_Collection_Officer_User_Id = @Client_Invoice_Collection_Officer_User_Id)
                    AND (   @Invoice_Collection_Officer_User_Id = -1
                            OR  icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_User_Id)
                    AND (   @Client_Invoice_Collection_Coordinator_User_Id = -1
                            OR  iccc.Invoice_Collection_Coordinator_User_Id = @Client_Invoice_Collection_Coordinator_User_Id)
                    AND (   @Invoice_Collection_Owner_Id = -1
                            OR  icac.Invoice_Collection_Owner_User_Id = @Invoice_Collection_Owner_Id)
                    AND (   icac.Invoice_Collection_Account_Config_Id = (   SELECT  TOP 1
                                                                                    Invoice_Collection_Account_Config_Id
                                                                            FROM
                                                                                dbo.Invoice_Collection_Account_Config icac1
                                                                            WHERE
                                                                                icac1.Account_Id = cha.Account_Id
                                                                                AND icac1.Is_Chase_Activated = 1
                                                                                AND icac1.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                AND @Today_Date BETWEEN icac1.Invoice_Collection_Service_Start_Dt
                                                                                                AND     icac1.Invoice_Collection_Service_End_Dt)
                            OR  icac.Invoice_Collection_Account_Config_Id = (   SELECT  TOP 1
                                                                                        Invoice_Collection_Account_Config_Id
                                                                                FROM
                                                                                    dbo.Invoice_Collection_Account_Config icac2
                                                                                WHERE
                                                                                    icac2.Account_Id = cha.Account_Id
                                                                                    AND icac2.Is_Chase_Activated = 1
                                                                                    AND icac2.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                    AND icac2.Invoice_Collection_Service_Start_Dt > @Today_Date
                                                                                    AND icac2.Invoice_Collection_Account_Config_Id <> ISNULL(
                                                                                                                                      (   SELECT    TOP 1
                                                                                                                                                    Invoice_Collection_Account_Config_Id
                                                                                                                                          FROM
                                                                                                                                                dbo.Invoice_Collection_Account_Config icac11
                                                                                                                                          WHERE
                                                                                                                                              icac11.Account_Id = cha.Account_Id
                                                                                                                                              AND icac11.Is_Chase_Activated = 1
                                                                                                                                              AND icac11.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                                                                              AND @Today_Date BETWEEN icac11.Invoice_Collection_Service_Start_Dt
                                                                                                                                                              AND            icac11.Invoice_Collection_Service_End_Dt)
                                                                                                                                      , 0)
                                                                                ORDER BY
                                                                                    icac2.Invoice_Collection_Service_Start_Dt DESC)
                            OR  icac.Invoice_Collection_Account_Config_Id = ISNULL(
                                                                            (   SELECT      TOP 1
                                                                                            Invoice_Collection_Account_Config_Id
                                                                                FROM
                                                                                    dbo.Invoice_Collection_Account_Config icac1
                                                                                WHERE
                                                                                    icac1.Account_Id = cha.Account_Id
                                                                                    AND icac1.Is_Chase_Activated = 1
                                                                                    AND icac1.Invoice_Collection_Service_Start_Dt < @Today_Date
                                                                                    AND icac1.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                    AND (   icac1.Invoice_Collection_Account_Config_Id <> ISNULL(
                                                                                                                                          (   SELECT    TOP 1
                                                                                                                                                        Invoice_Collection_Account_Config_Id
                                                                                                                                              FROM
                                                                                                                                                    dbo.Invoice_Collection_Account_Config icac2
                                                                                                                                              WHERE
                                                                                                                                                  icac2.Account_Id = cha.Account_Id
                                                                                                                                                  AND icac2.Is_Chase_Activated = 1
                                                                                                                                                  AND icac2.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                                                                                  AND icac2.Invoice_Collection_Service_Start_Dt > @Today_Date
                                                                                                                                                  AND icac2.Invoice_Collection_Account_Config_Id <> ISNULL(
                                                                                                                                                                                                    (   SELECT  TOP 1
                                                                                                                                                                                                                Invoice_Collection_Account_Config_Id
                                                                                                                                                                                                        FROM
                                                                                                                                                                                                            dbo.Invoice_Collection_Account_Config icac11
                                                                                                                                                                                                        WHERE
                                                                                                                                                                                                            icac11.Account_Id = icac2.Account_Id
                                                                                                                                                                                                            AND icac11.Is_Chase_Activated = 1
                                                                                                                                                                                                            AND icac11.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                                                                                                                                            AND @Today_Date BETWEEN icac11.Invoice_Collection_Service_Start_Dt
                                                                                                                                                                                                                            AND               icac11.Invoice_Collection_Service_End_Dt)
                                                                                                                                                                                                    , 0)
                                                                                                                                              ORDER BY
                                                                                                                                                  icac2.Invoice_Collection_Service_Start_Dt DESC)
                                                                                                                                          , 0)
                                                                                            OR  icac1.Invoice_Collection_Account_Config_Id <> ISNULL(
                                                                                                                                              (   SELECT    TOP 1
                                                                                                                                                            Invoice_Collection_Account_Config_Id
                                                                                                                                                  FROM
                                                                                                                                                        dbo.Invoice_Collection_Account_Config icac1
                                                                                                                                                  WHERE
                                                                                                                                                      icac1.Account_Id = cha.Account_Id
                                                                                                                                                      AND icac1.Is_Chase_Activated = 1
                                                                                                                                                      AND icac1.Invoice_Collection_Officer_User_Id = icac.Invoice_Collection_Officer_User_Id
                                                                                                                                                      AND @Today_Date BETWEEN icac1.Invoice_Collection_Service_Start_Dt
                                                                                                                                                                      AND             icac1.Invoice_Collection_Service_End_Dt)
                                                                                                                                              , 0))
                                                                                ORDER BY
                                                                                    icac1.Invoice_Collection_Service_Start_Dt DESC)
                                                                            , 0))
               GROUP BY
                   ch.Client_Name
                   , ch.Country_Name
                   , ch.State_Name
                   , REPLACE(CONVERT(NVARCHAR, iccc.Invoice_Collection_Service_Start_Dt, 106), ' ', '-')
                   , REPLACE(CONVERT(NVARCHAR, iccc.Invoice_Collection_Service_End_Dt, 106), ' ', '-')
                   , co.FIRST_NAME + ' ' + co.LAST_NAME
                   , ao.FIRST_NAME + ' ' + ao.LAST_NAME
                   , cc.FIRST_NAME + ' ' + cc.LAST_NAME
                   , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME
                   , st.Code_Value
                   , ISNULL((icsc.Cnt), 0)
                   , ISNULL((icsci.Cnt), 0)
                   , ISNULL((icsd.Cnt), 0)
                   , ISNULL((icsdi.Cnt), 0)
                   , (ISNULL(icsu.Cnt, 0))
                   , ISNULL((icsui.Cnt), 0)
                   , ISNULL((icsns.Cnt), 0)
                   , ISNULL((icsnsi.Cnt), 0)
                   , ISNULL((excp.Cnt), 0)
				   , ISNULL(icsetl.Cnt, 0) 
                    , ISNULL(icsetli.Cnt, 0)
                   , icac.Invoice_Collection_Account_Config_Id
                   , cha.Account_Id
                   , stc.Code_Value
           )
        SELECT
            Client
            , Country
            , State_Name
            , [Invoice Collection Service Start Date]
            , [Invoice Collection Service End Date]
            , [Main Invoice Collection Officer]
            , [Invoice Collection Officer]
            , [Invoice Collection Coordinator]
            , [Invoice Collection Service Level]
            , SUM([No of Chase Accounts (Current)]) [No of Chase Accounts (Current)]
            , SUM([No of Chase Accounts (Issue)]) [No of Chase Accounts (Issue)]
            , SUM([No of Download Accounts (Current)]) [No of Download Accounts (Current)]
            , SUM([No of Download Accounts (Issues)]) [No of Download Accounts (Issues)]
            , SUM([No of Data Feed Partner Accounts (Current)]) [No of Data Feed Partner Accounts (Current)]
            , SUM([No of Data Feed Partner Accounts (Issues)]) [No of Data Feed Partner Accounts (Issues)]
			 ,SUM([No of Data Feed ETL Accounts (Current)]) [No of Data Feed ETL Accounts (Current)]
            , SUM([No of Data Feed ETL Accounts (Issues)]) [No of Data Feed ETL Accounts (Issues)]
            , SUM([No of Undefined Accounts (Current)]) [No of Undefined Accounts (Current)]
            , SUM([No of Undefined Accounts (Issues)]) [No of Undefined Accounts (Issues)]
            , SUM([Number of Open Exceptions in Current Period]) [Number of Open Exceptions in Current Period]
            , COUNT(Invoice_Collection_Account_Config_Id) AS Invoice_Collection_Account_Config_Id
            , COUNT(Account_Id) AS Accounts
            , Invoice_Collection_Owner AS [Invoice Collection Owner]
            , cte_ss.Service_Type
        FROM
            cte_ss
        GROUP BY
            Client
            , Country
            , State_Name
            , [Invoice Collection Service Start Date]
            , [Invoice Collection Service End Date]
            , [Main Invoice Collection Officer]
            , [Invoice Collection Officer]
            , [Invoice Collection Coordinator]
            , [Invoice Collection Service Level]
            , Invoice_Collection_Owner
            , cte_ss.Service_Type;
    END;


    ;

GO

GRANT EXECUTE ON  [dbo].[Report_DE_IC_Workload] TO [CBMSApplication]
GO
