SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sourcing_Analyst_Manager_Unassigned_Dtl_Sel]
           
DESCRIPTION:             
			To get Sourcing Analyst without a Manager
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Country_Id		INT			NULL
    @Commodity_Id	INT			NULL
    @StartIndex		INT			1
    @EndIndex		INT			2147483647


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

	EXEC dbo.Sourcing_Analyst_Manager_Unassigned_Dtl_Sel


 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date			Modification
------------------------------------------------------------
	RR			2015-07-20		Global Sourcing - Created
								
******/
CREATE PROCEDURE [dbo].[Sourcing_Analyst_Manager_Unassigned_Dtl_Sel]
      ( 
       @StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      WITH  Cte_SA_SM
              AS ( SELECT
                        saui.USER_INFO_ID AS Sourcing_Analyst_Id
                       ,saui.FIRST_NAME + ' ' + saui.LAST_NAME AS Sourcing_Analyst_Name
                       ,Row_Num = row_number() OVER ( ORDER BY saui.FIRST_NAME + ' ' + saui.LAST_NAME )
                       ,Total = count(1) OVER ( )
                   FROM
                        dbo.USER_INFO saui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                              ON saui.USER_INFO_ID = uigim.USER_INFO_ID
                        INNER JOIN dbo.GROUP_INFO gi
                              ON uigim.GROUP_INFO_ID = gi.GROUP_INFO_ID
                   WHERE
                        gi.GROUP_NAME = 'supply'
                        AND saui.IS_HISTORY = 0
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.SR_SA_SM_MAP sasm
                                         WHERE
                                          sasm.SOURCING_ANALYST_ID = saui.USER_INFO_ID ))
            SELECT
                  css.Sourcing_Analyst_Id
                 ,css.Sourcing_Analyst_Name
                 ,css.Row_Num
                 ,css.Total
            FROM
                  Cte_SA_SM css
            WHERE
                  css.Row_Num BETWEEN @StartIndex AND @EndIndex
            
                         
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sourcing_Analyst_Manager_Unassigned_Dtl_Sel] TO [CBMSApplication]
GO
