SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [dbo].[Invoice_Collection_Queue_Month_Map_Update]
    
DESCRIPTION:          
	
	    
INPUT PARAMETERS:    
 NAME											DATATYPE		 DEFAULT           DESCRIPTION    
-------------------------------------------------------------------------------------------------  
 @Invoice_Collection_Queue_id					INT
 @Old_Account_Invoice_Collection_Month_Id		INT
 @New_Account_Invoice_Collection_Month_Id		INT
                 
OUTPUT PARAMETERS:              
 NAME											DATATYPE		 DEFAULT           DESCRIPTION    
-------------------------------------------------------------------------------------------------  


USAGE EXAMPLES:              
--------------------------------------------------------------------------------------------  
    
     EXEC [dbo].[Invoice_Collection_Queue_Month_Map_Update] 
      @Invoice_Collection_Queue_id = 1
     ,@Old_Account_Invoice_Collection_Month_Id = 100
     ,@New_Account_Invoice_Collection_Month_Id = 200
     

      
AUTHOR INITIALS:              
  INITIALS				NAME              
--------------------------------------------------------------------------------------------  
  PR					Pradeep Rajput
              
MODIFICATIONS:    
 INITIALS			DATE			MODIFICATION              
--------------------------------------------------------------------------------------------  
 PR					2018-02-19		Created .
            

*****/ 

CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Month_Map_Update]
      ( 
       @Invoice_Collection_Queue_id INT
      ,@Old_Account_Invoice_Collection_Month_Id INT
      ,@New_Account_Invoice_Collection_Month_Id INT )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
    
      UPDATE
            Invoice_Collection_Queue_Month_Map
      SET   
            Account_Invoice_Collection_Month_Id = @New_Account_Invoice_Collection_Month_Id
      WHERE
            Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_id
            AND Account_Invoice_Collection_Month_Id = @Old_Account_Invoice_Collection_Month_Id  
         
END;    
    
;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Month_Map_Update] TO [CBMSApplication]
GO
