SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                  
NAME:                                  
    Trade.Division_Sel_By_Commodity_Hedge_Type_Period                                  
                                  
DESCRIPTION:                                  
                             
                                  
INPUT PARAMETERS:                                  
 Name    DataType  Default Description                                  
---------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Start_Dt   DATE  
    @End_Dt    DATE  
    @Hedge_Type   INT  
                            
                         
OUTPUT PARAMETERS:                                  
 Name   DataType  Default Description                                  
---------------------------------------------------------------      
  
                               
 EXEC Trade.RM_Client_Hier_Hedge_Config_Sel @Client_Id = 11236  
  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-12-30',@Hedge_Type=586  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2018-01-01',@End_Dt='2018-03-30',@Hedge_Type=586  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291,@Start_Dt='2019-01-01',@End_Dt='2019-12-30',@Hedge_Type=586  
   
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2019-01-01','2019-12-30',586  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2019-01-01','2019-12-30',586,'Adh'  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2019-01-01','2019-12-30',586,'metic'  
 EXEC Trade.Division_Sel_By_Commodity_Hedge_Type_Period 11236,291,'2019-01-01','2019-12-30',586,'Sun'  
                  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 RR   Raghu Reddy  
                     
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 RR   2018-10-25  Created For Risk Management  
  
******/
CREATE PROCEDURE [Trade].[Division_Sel_By_Commodity_Hedge_Type_Period]
(
    @Client_Id INT,
    @Commodity_Id INT,
    @Start_Dt DATE,
    @End_Dt DATE,
    @Hedge_Type INT,
    @Keyword VARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Hedge_Type_Input VARCHAR(200),
            @Search_Str VARCHAR(MAX);

    SELECT @Hedge_Type_Input = ENTITY_NAME
    FROM dbo.ENTITY
    WHERE ENTITY_ID = @Hedge_Type;

    SELECT @Search_Str = '%' + @Keyword + '%';

    SELECT ch.Sitegroup_Id,
           ch.Sitegroup_Name
    FROM Core.Client_Hier ch
    WHERE ch.Client_Id = @Client_Id
          AND
          (
              EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard siteob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
        WHERE siteob.Client_Hier_Id = ch.Client_Hier_Id
              AND siteob.Commodity_Id = @Commodity_Id
              AND
              (
                  (
                      @Hedge_Type_Input = 'Physical'
                      AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                  )
                  OR
                  (
                      @Hedge_Type_Input = 'Financial'
                      AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
                  )
              )
            AND
              (
                  chhc.Config_Start_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR chhc.Config_End_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR @Start_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
                  OR @End_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              )
    )
              OR EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard clntob
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN Core.Client_Hier clch
                ON clntob.Client_Hier_Id = clch.Client_Hier_Id
        WHERE clch.Sitegroup_Id = 0
              AND clch.Client_Id = ch.Client_Id
              AND clntob.Country_Id = ch.Country_Id
              AND clntob.Commodity_Id = @Commodity_Id
              AND
              (
                  (
                      @Hedge_Type_Input = 'Physical'
                      AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' )
                  )
                  OR
                  (
                      @Hedge_Type_Input = 'Financial'
                      AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' )
                  )
              )
              AND
              (
                  chhc.Config_Start_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR chhc.Config_End_Dt
              BETWEEN @Start_Dt AND @End_Dt
                  OR @Start_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
                  OR @End_Dt
              BETWEEN chhc.Config_Start_Dt AND chhc.Config_End_Dt
              )
              AND NOT EXISTS
        (
            SELECT 1
            FROM Trade.RM_Client_Hier_Onboard siteob1
            WHERE siteob1.Client_Hier_Id = ch.Client_Hier_Id
                  AND siteob1.Commodity_Id = @Commodity_Id
        )
    )
          )
          AND ch.Site_Id > 0
          AND
          (
              @Keyword IS NULL
              OR ch.Sitegroup_Name LIKE @Search_Str
          )
    GROUP BY ch.Sitegroup_Id,
             ch.Sitegroup_Name;


END;
GO
GRANT EXECUTE ON  [Trade].[Division_Sel_By_Commodity_Hedge_Type_Period] TO [CBMSApplication]
GO
