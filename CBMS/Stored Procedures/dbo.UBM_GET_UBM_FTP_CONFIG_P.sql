
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME: dbo.UBM_GET_UBM_FTP_CONFIG_P

DESCRIPTION:      
      
	To get the UBM Ftp details for the given ubm name

INPUT PARAMETERS:         
    Name			DataType				Default     Description
-------------------------------------------------------------------            
	@ubmname		VARCHAR(10)

OUTPUT PARAMETERS:
      Name			DataType          Default     Description
----------------------------------------------------------------    

USAGE EXAMPLES:
----------------------------------------------------------------

	EXEC dbo.UBM_GET_UBM_FTP_CONFIG_P  @UbmName = 'AS400'
	EXEC dbo.UBM_GET_UBM_FTP_CONFIG_P  @UbmName = 'Schneider Electric'
	EXEC dbo.UBM_GET_UBM_FTP_CONFIG_P  @UbmName = 'Prokarma'
	EXEC dbo.UBM_GET_UBM_FTP_CONFIG_P  @UbmName = 'Avista'
	EXEC dbo.UBM_GET_UBM_FTP_CONFIG_P  @UbmName = 'Cass'

AUTHOR INITIALS:
Initials	Name     
------------------------------------------------------------        
HG			Harihara Suthan G

MODIFICATIONS

Initials	Date		Modification
------------------------------------------------------------
HG			2014-09-24	Comments header added and modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma
							-- Added separate block for Schneider Electric and Prokarma and replaced the query to select the data based on Entity_Description instead of Entity_Type
******/

CREATE PROCEDURE [dbo].[UBM_GET_UBM_FTP_CONFIG_P] @UBMName VARCHAR(200)
AS 
BEGIN

      SET NOCOUNT ON

      IF @UBMName = 'Cass' 
            BEGIN
	 
                  SELECT
                        entity_name
                       ,entity_Type
                       ,ENTITY_DESCRIPTION
                  FROM
                        dbo.Entity
                  WHERE
                        ENTITY_DESCRIPTION IN ( 'UBM System Account', 'UBM_CASS_FTP_LOCATION', 'UBM_CASS_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'DataManagement Group Email Alias' )
                  ORDER BY
                        entity_type
		
            END
      ELSE 
            IF @UBMName = 'Avista' 
                  BEGIN
	 
                        SELECT
                              entity_name
                             ,entity_type
                             ,entity_description
                        FROM
                              dbo.Entity
                        WHERE
                              ENTITY_DESCRIPTION IN ( 'UBM System Account', 'UBM_AVISTA_FTP_LOCATION', 'UBM_AVISTA_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'DataManagement Group Email Alias' )
                        ORDER BY
                              entity_type
                  END
            ELSE 
                  IF @UBMName = 'Schneider Electric' 
                        BEGIN
							
	 
                              SELECT
                                    entity_name
                                   ,entity_type
                                   ,entity_description
                              FROM
                                    dbo.Entity
                              WHERE
                                    ENTITY_DESCRIPTION IN ( 'UBM System Account', 'UBM_SCHNEIDER_FTP_LOCATION', 'UBM_SCHNEIDER_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'DataManagement Group Email Alias' )
                              ORDER BY
                                    entity_type
		
                        END
                  ELSE 
                        IF @UBMName = 'Prokarma' 
                              BEGIN
	 
                                    SELECT
                                          entity_name
                                         ,entity_type
                                         ,entity_description
                                    FROM
                                          dbo.Entity
                                    WHERE
                                          ENTITY_DESCRIPTION IN ( 'UBM System Account', 'UBM_PROKARMA_FTP_LOCATION', 'UBM_PROKARMA_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'DataManagement Group Email Alias' )
                                    ORDER BY
                                          entity_type
		
                              END
                        ELSE 
                              IF @UBMName = 'AS400' 
                                    BEGIN
	 
                                          SELECT
                                                entity_name
                                               ,entity_type
                                               ,entity_description
                                          FROM
                                                dbo.Entity
                                          WHERE
                                                ENTITY_DESCRIPTION IN ( 'UBM System Account', 'UBM_AS400_FTP_LOCATION', 'UBM_AS400_FTP_ARCHIVE_LOCATION', 'UBM_FTP_SERVER_NAME', 'UBM_FTP_USERNAME', 'UBM_FTP_PASSWORD', 'DataManagement Group Email Alias' )
                                          ORDER BY
                                                entity_type
		
                                    END

END
;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_FTP_CONFIG_P] TO [CBMSApplication]
GO
