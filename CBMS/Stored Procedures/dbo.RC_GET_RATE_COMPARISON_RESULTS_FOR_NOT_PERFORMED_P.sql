SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******
NAME:
	CBMS.dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@siteId        	int       	          	
	@regionId      	int       	          	
	@stateId       	int       	          	
	@city          	varchar(50)	          	
	@utilityId     	int       	          	
	@rateId        	int       	          	
	@accountNumber 	varchar(50)	          	
	@meterNumber   	varchar(50)	
	@StartIndex		INT = 1
    @EndIndex		INT = 2147483647
          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P  223, 42831, 0, 0, 'Livorno', 0, 15567, '32180101', 'D32180101',1,1

	EXEC dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P  223, 0, 0, 0, '0', 0, 0, '', '',1,1000
	EXEC dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P  11231, 0, 0, 0, '0', 0, 0, '', '',1,1000

	EXEC dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P 0, 0, 0, 0, 0, 0, 0, '', '',1,200	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath
	SKA			Shobhit Agrawal
	HG			Harihara Suthan G
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	SKA			11/02/2010	Added pagination against Bug 21462
	HG			11/15/2010	Converted Non Ansi joins in to Ansi join and using Client_hier/Client_Hier_Account in place of client, Division, Site, Account, Meter and Address

******/

CREATE PROCEDURE dbo.RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P
	@clientId		INT
	,@siteId		INT
	,@regionId		INT
	,@stateId		INT
	,@city			VARCHAR(50)
	,@utilityId		INT
	,@rateId		INT
	,@accountNumber	VARCHAR(50)
	,@meterNumber	VARCHAR(50)
    ,@StartIndex	INT = 1
    ,@EndIndex		INT = 2147483647
AS
BEGIN

	SET NOCOUNT ON ;

	DECLARE @selectClause	VARCHAR(2000)
		,@fromClause		VARCHAR(2000)
		,@whereClause		VARCHAR(3000)
		,@SQLStatement		VARCHAR(8000)

	SELECT @selectClause = '
				WITH GetRCData AS
				(
					SELECT DISTINCT
						vendor.VENDOR_NAME							AS	UTILITY_NAME
						,ISNULL(ch.CLIENT_NAME,'''' )				AS	CLIENT_NAME
						,ch.SITE_ID
						,ISNULL(region.REGION_DESCRIPTION,'''' )	AS	REGION_NAME
						,ISNULL(state.STATE_NAME,'''' )				AS	STATE_NAME
						,ISNULL(rate.RATE_NAME,'''' )				AS	RATE_NAME
						,ISNULL(ch.ACCOUNT_NUMBER,'''' )			AS	ACCOUNT_NUMBER
						,ISNULL(ch.METER_NUMBER,'''' )				AS	METER_NUMBER
						,ISNULL(ch.Meter_City,'''' )				AS	CITY
						,ch.Site_Closed								AS	CLOSED
						,Row_Num = ROW_NUMBER() OVER ( ORDER BY ch.SITE_ID)
						,Total = COUNT(1) OVER ( )
			       '

	SELECT @fromClause = '
				(
					SELECT
						ch.Client_Name
						,ch.Site_Id
						,ch.Site_Closed
						,cha.Account_Number
						,cha.Meter_Id
						,cha.Meter_Number
						,cha.Meter_City
						,cha.Rate_Id
						,cha.Account_Service_level_Cd
					FROM
						core.Client_Hier ch
						INNER JOIN core.Client_Hier_Account cha
							ON cha.Client_Hier_Id = ch.Client_Hier_Id
					WHERE
						(' + STR(@clientId) + ' = ' + ' 0 OR ch.Client_Id = ' + STR(@clientId) + ')' + '
						AND cha.Account_Not_Managed = 0
						AND ch.Site_Closed = 0
					GROUP BY
						ch.Client_Name
						,ch.Site_Id
						,ch.Site_Closed
						,cha.Account_Number
						,cha.Meter_Id
						,cha.Meter_Number
						,cha.Meter_City
						,cha.Rate_Id
						,cha.Account_Service_level_Cd
						,cha.Account_Not_Managed
				) ch

				INNER JOIN dbo.RATE rate
					ON rate.RATE_ID = ch.RATE_ID
				INNER JOIN dbo.VENDOR vendor
					ON rate.VENDOR_ID = vendor.VENDOR_ID
				INNER JOIN dbo.VENDOR_STATE_MAP vendorStateMap
					ON vendor.VENDOR_ID = vendorStateMap.VENDOR_ID
				INNER JOIN dbo.STATE state
					ON state.STATE_ID = vendorStateMap.STATE_ID
				INNER JOIN dbo.REGION region
					ON state.REGION_ID = region.REGION_ID

				INNER JOIN dbo.Entity AccTyp
					ON AccTyp.ENtity_Id = ch.Account_Service_level_Cd
			     '

	SELECT @whereClause = '
							 NOT EXISTS (SELECT
											METER_ID
										 FROM
											dbo.RC_RATE_COMPARISON rc
										 WHERE
											rc.Meter_Id = ch.METER_ID
											)
							AND (AccTyp.ENTITY_NAME = ''A'' OR AccTyp.ENTITY_NAME = ''B'')
						'

	IF @meterNumber <> ''
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND ch.METER_NUMBER LIKE '+'''' + '%' + @meterNumber + '%' + ''''
		END

	IF @accountNumber <> ''
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND ch.ACCOUNT_NUMBER LIKE ' + '''' + '%' + @accountNumber + '%' + ''''
		END

	IF @stateId > 0
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND state.STATE_ID = ' + STR(@stateId)
		END

	IF @utilityId > 0
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND vendor.VENDOR_ID = ' + STR(@utilityId)
		END

	IF @rateId > 0
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND rate.RATE_ID = ' + STR(@rateId)
		END

	IF @siteId > 0
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND ch.SITE_ID = ' + STR(@siteId)
		END

	IF @regionId > 0
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND region.REGION_ID= ' + STR(@regionId)
		END

	IF @city <> '0'
		BEGIN
			SELECT @whereClause =  @whereClause + ' AND ch.Meter_City = ' +''''+ @city +''''
		END

	SELECT @SQLStatement =
				@selectClause
				+ ' FROM '
					+ @fromClause
				+ ' WHERE '
					+ @whereClause
				+ ' ) '
				+ 'SELECT
						UTILITY_NAME
						,CLIENT_NAME
						,SITE_ID
						,REGION_NAME
						,STATE_NAME
						,RATE_NAME
						,ACCOUNT_NUMBER
						,METER_NUMBER
						,CITY

						,'''' CREATED_BY
						,'''' RATE_COMPARISON_NAME
						,'''' CREATION_DATE
						,'''' STATUS_NAME
						,'''' RESULT_NAME
						,'''' REVIEWED_BY

						,CLOSED
						,Total
				FROM
						GetRCData
				WHERE
						Row_Num BETWEEN ' + CAST(@StartIndex AS VARCHAR) + ' AND ' + CAST(@EndIndex AS VARCHAR)	+ '
				ORDER BY
						Row_Num
				'		

	EXEC(@SQLStatement)
	
END	

GO
GRANT EXECUTE ON  [dbo].[RC_GET_RATE_COMPARISON_RESULTS_FOR_NOT_PERFORMED_P] TO [CBMSApplication]
GO
