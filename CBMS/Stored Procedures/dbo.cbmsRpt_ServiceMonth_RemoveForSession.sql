SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  procedure [dbo].[cbmsRpt_ServiceMonth_RemoveForSession]
	( @MyAccountId int
	, @session_uid uniqueidentifier
	)
AS
BEGIN

set nocount on

   delete rpt_service_month WITH (ROWLOCK)
    where session_uid = @session_uid

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRpt_ServiceMonth_RemoveForSession] TO [CBMSApplication]
GO
