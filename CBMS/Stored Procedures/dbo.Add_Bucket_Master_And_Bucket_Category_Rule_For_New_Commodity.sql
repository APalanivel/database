SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
     
/******      
      
NAME: [DBO].[Add_Bucket_Master_And_Bucket_Category_Rule_For_New_Commodity]      
      
DESCRIPTION:      
      
 This stored procedure used to save the Bucket_Master and Bucket_Category_Rule for the given commodity_id      
      
INPUT PARAMETERS:      
 NAME       DATATYPE DEFAULT  DESCRIPTION      
-----------------------------------------------------------------------      
 @Commodity_Id     INT      
      
OUTPUT PARAMETERS:      
 NAME       DATATYPE DEFAULT  DESCRIPTION      
-----------------------------------------------------------------------      
      
USAGE EXAMPLES:      
-----------------------------------------------------------------------      
      
SELECT * FROM dbo.Bucket_Master WHERE Commodity_id = 57      
      
BEGIN TRAN      
      
 EXEC dbo.Add_Bucket_Master_And_Bucket_Category_Rule_For_New_Commodity @Commodity_Id = 57      
 SELECT * FROM dbo.Bucket_Master WHERE Commodity_id = 57      
      
ROLLBACK TRAN      
      
AUTHOR INITIALS:      
 INITIALS NAME      
-----------------------------------------------------------------------      
 HG   Harihara Suthan G      
 TP   Anoop     
 VRV Venkata Reddy Vanga     
      
MODIFICATIONS:      
INITIALS DATE  MODIFICATION      
-----------------------------------------------------------------------      
 HG   2013-04-22 Created for manage services requirement      
 HG   2013-06-14 New bucket category rules added for calendarization enhancement      
 TP   2015-07-27 Replaced the area where filtering codeset based on Std_Column_Name,       
                 because the Std_Column_Name value ('Aggregation_Type_CD') was repeating for different codesets. Instead we have to use codeset    
VRV   2019-12-27 SE-879 Added insert script for billed buckets for new Commodity       
*/      
CREATE PROCEDURE [dbo].[Add_Bucket_Master_And_Bucket_Category_Rule_For_New_Commodity] ( @Commodity_Id INT )      
AS       
BEGIN      
      
      SET NOCOUNT ON      
      
      DECLARE @Bucket_Master TABLE      
            (       
             Bucket_Name VARCHAR(255)      
            ,Is_Category BIT      
            ,Bucket_Type_Cd INT      
            ,Aggregation_Level_Cd INT      
            ,Is_Shown_on_Site BIT      
            ,Is_Shown_On_Account BIT      
            ,Is_Shown_On_Invoice BIT      
            ,Is_Required_For_Variance_Test BIT      
            ,Sort_Order INT      
            ,Is_Active BIT      
            ,Default_Uom_Type_Id INT      
            ,Is_Reported BIT )      
      
      DECLARE @Other_Service_Bucket_Category_Rules TABLE      
            (       
             Category_Bucket_Name VARCHAR(200)      
            ,Child_Bucket_Name VARCHAR(200)      
            ,Priority_Order SMALLINT      
            ,Aggregation_Type VARCHAR(25)      
            ,Cu_Aggregation_Level VARCHAR(200)      
            ,Is_Aggregate_Category_Bucket BIT      
            ,Bucket_Type VARCHAR(200) )      
      
      DECLARE      
            @Charge_Bkt_Type_Cd INT      
           ,@Determinant_Bkt_Type_Cd INT      
           ,@Invoice_Aggregation_Cd INT      
      
      SELECT      
            @Charge_Bkt_Type_Cd = cd.Code_Id      
      FROM      
            dbo.Codeset cs      
            INNER JOIN dbo.Code cd      
                  ON cd.Codeset_Id = cs.Codeset_Id      
      WHERE      
            cs.Codeset_Name = 'Bucket Type'      
            AND cd.Code_Value = 'Charge'      
      
      SELECT      
            @Determinant_Bkt_Type_Cd = cd.Code_Id      
      FROM      
            dbo.Codeset cs      
            INNER JOIN dbo.Code cd      
                  ON cd.Codeset_Id = cs.Codeset_Id      
      WHERE      
            cs.Codeset_Name = 'Bucket Type'      
            AND cd.Code_Value = 'Determinant'      
      
      SELECT      
            @Invoice_Aggregation_Cd = cd.Code_Id      
      FROM      
            dbo.Codeset cs      
            INNER JOIN dbo.Code cd      
                  ON cd.Codeset_Id = cs.Codeset_Id      
      WHERE      
            cs.Codeset_Name = 'Aggregation Level'      
            AND cd.Code_Value = 'Invoice'      
      
      INSERT      INTO @Bucket_Master      
                  ( Bucket_Name, Is_Category, Bucket_Type_Cd, Aggregation_Level_Cd, Is_Shown_on_Site, is_Shown_On_Account, Is_Shown_On_Invoice, Is_Required_For_Variance_Test, Sort_Order, Is_Active, Default_Uom_Type_Id, Is_Reported )      
      VALUES      
         ( 'Volume', 0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 1, 1, 0, 1, 1, 1, NULL, 1 )      
   ,     ( 'Units', 0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 2, 1, NULL, 0 )      
   ,     ( 'Demand', 0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 3, 1, NULL, 0 )      
   ,     ( 'Heating Value', 0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 4, 1, NULL, 0 )      
   ,     ( 'Other', 0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 5, 1, NULL, 0 )      
      
   ,     ( 'Before Taxes', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 1, 1, 0, 1, 1, 1, NULL, 1 )      
   ,     ( 'Tax', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 1, 1, 1, 1, 2, 1, NULL, 1 )      
   ,     ( 'Total Cost', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 1, 1, 0, 1, 3, 1, NULL, 1 )      
   ,     ( 'Late Fees', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 1, 1, 1, 4, 1, NULL, 0 )      
      
   ,     ( 'Bundled', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 5, 1, NULL, 0 )      
   ,     ( 'Commodity', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 6, 1, NULL, 0 )      
   ,     ( 'Delivery', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 7, 1, NULL, 0 )      
   ,     ( 'Demand', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 8, 1, NULL, 0 )      
   ,     ( 'Deposit', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 9, 1, NULL, 0 )      
   ,     ( 'Other', 0, @Charge_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0, 0, 1, 0, 10, 1, NULL, 0 )    
    
------Billed volume buckets    
    
   ,     ('Billed Units',  0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 0,0, 1, 0 , 11, 1, NULL, 0)    
   ,     ('Billed Volume',  0, @Determinant_Bkt_Type_Cd, @Invoice_Aggregation_Cd, 1,1, 0, 0 , 12, 1, NULL, 1)      
      
      INSERT      INTO @Other_Service_Bucket_Category_Rules      
                  ( Category_Bucket_Name, Child_Bucket_Name, Priority_Order, Aggregation_Type, Cu_Aggregation_Level, Is_Aggregate_Category_Bucket, Bucket_Type )      
      VALUES      
                  ( 'Volume', 'Units', 1, 'Sum', 'Invoice', 0, 'Determinant' )      
                  ,( 'Before Taxes', 'Bundled', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Before Taxes', 'Commodity', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Before Taxes', 'Delivery', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Before Taxes', 'Demand', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Before Taxes', 'Other', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Tax', 'Tax', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Total Cost', 'Before Taxes', 1, 'Sum', 'Invoice', 1, 'Charge' )      
                  ,( 'Late Fees', 'Late Fees', 1, 'Sum', 'Invoice', 0, 'Charge' )      
                  ,( 'Volume', 'Volume', 1, 'Sum', 'Account', 1, 'Determinant' )      
                  ,( 'Before Taxes', 'Before Taxes', 1, 'Sum', 'Account', 1, 'Charge' )      
                  ,( 'Tax', 'Tax', 1, 'Sum', 'Account', 1, 'Charge' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Sum', 'Account', 1, 'Charge' )      
                  ,( 'Late Fees', 'Late Fees', 1, 'Sum', 'Account', 1, 'Charge' )      
                  ,( 'Volume', 'Volume', 1, 'SiteSum', 'Site', 0, 'Determinant' )      
                  ,( 'Before Taxes', 'Before Taxes', 1, 'Sum', 'Site', 0, 'Charge' )      
                  ,( 'Tax', 'Tax', 1, 'Sum', 'Site', 0, 'Charge' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Sum', 'Site', 0, 'Charge' )      
                  ,( 'Late Fees', 'Late Fees', 1, 'Sum', 'Site', 0, 'Charge' )      
                  ,( 'Volume', 'Volume', 1, 'Divide', 'MultiMonth', 1, 'Determinant' )      
                  ,( 'Before Taxes', 'Before Taxes', 1, 'Divide', 'MultiMonth', 1, 'Charge' )     
                  ,( 'Tax', 'Tax', 1, 'Divide', 'MultiMonth', 1, 'Charge' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Divide', 'MultiMonth', 1, 'Charge' )      
                  ,( 'Late Fees', 'Late Fees', 1, 'Divide', 'MultiMonth', 1, 'Charge' )      
      
-- Rules for calendarized CU calculation for other services    
      
                  ,( 'Volume', 'Volume', 1, 'Divide', 'InvoiceDays', 0, 'Determinant' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Divide', 'InvoiceDays', 0, 'Charge' )      
                  ,( 'Volume', 'Volume', 1, 'Sum', 'AccountDays', 0, 'Determinant' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Sum', 'AccountDays', 0, 'Charge' )      
                  ,( 'Volume', 'Volume', 1, 'SiteSum', 'SiteDays', 0, 'Determinant' )      
                  ,( 'Total Cost', 'Total Cost', 1, 'Sum', 'SiteDays', 0, 'Charge' )    
    
------Rules for Billed volume buckets    
    
   ,('Volume'            ,'Billed Units'    ,2    ,'Sum'         ,'Invoice'       ,0    ,'Determinant')  
  ,('Billed Volume'      ,'Billed Volume'   ,1 ,'Sum'         ,'Account'       ,1    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Units'    ,1 ,'Sum'         ,'Invoice'       ,0    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'Sum'         ,'Invoice'       ,0    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'Divide'      ,'MultiMonth'    ,1    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'SiteSum'     ,'Site'          ,0    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'Sum'         ,'AccountDays'   ,0    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'Divide'      ,'InvoiceDays'   ,0    ,'Determinant')  
  ,('Billed Volume'    ,'Billed Volume'   ,1 ,'SiteSum'     ,'SiteDays'      ,0    ,'Determinant')  
  
  
  
      
      BEGIN TRY      
            BEGIN TRAN      
      
            MERGE INTO dbo.Bucket_Master tgt      
                  USING       
                        ( SELECT      
                              bm.Bucket_Name      
                             ,bm.Is_Category      
                             ,bm.Bucket_Type_Cd      
                             ,bm.Aggregation_Level_Cd      
                             ,@Commodity_Id AS Commodity_Id      
                             ,bm.Is_Shown_on_Site      
                             ,bm.is_Shown_On_Account      
                             ,bm.Is_Shown_On_Invoice      
                             ,bm.Is_Required_For_Variance_Test      
                             ,bm.Sort_Order      
                             ,bm.Is_Active      
                             ,case WHEN bm.Bucket_Name in( 'Volume','Billed Volume') THEN com.Default_UOM_Entity_Type_Id      
                              END AS Default_Uom_Type_Id      
                             ,bm.Is_Reported      
                          FROM      
                              @Bucket_Master bm      
                              INNER JOIN dbo.Commodity com      
                                    ON com.Commodity_Id = @Commodity_Id ) src      
                  ON src.Commodity_Id = tgt.Commodity_Id      
                        AND src.Bucket_Name = tgt.Bucket_Name      
                        AND src.Bucket_Type_Cd = tgt.Bucket_Type_Cd      
                  WHEN NOT MATCHED       
                        THEN       
     INSERT      
                              (       
                               Commodity_Id      
                              ,Bucket_Name      
                              ,Is_Category      
                              ,Bucket_Type_Cd      
                              ,Aggregation_Level_Cd      
                              ,Is_Shown_on_Site      
                              ,Is_Shown_On_Account      
                              ,Is_Shown_On_Invoice      
                              ,Is_Required_For_Variance_Test      
                              ,Sort_Order      
                              ,Is_Active      
                              ,Default_Uom_Type_Id      
                              ,Is_Reported )      
                         VALUES      
                              (       
                               src.Commodity_Id      
                              ,src.Bucket_Name      
                              ,src.Is_Category      
                              ,src.Bucket_Type_Cd      
                              ,src.Aggregation_Level_Cd      
                              ,src.Is_Shown_on_Site      
                              ,src.Is_Shown_On_Account      
                              ,src.Is_Shown_On_Invoice      
                              ,src.Is_Required_For_Variance_Test      
                              ,src.Sort_Order      
                              ,src.Is_Active      
                              ,src.Default_Uom_Type_Id      
                              ,src.Is_Reported )      
                  WHEN MATCHED       
                        THEN UPDATE      
                         SET        
                              tgt.Default_Uom_Type_Id = src.Default_Uom_Type_Id      
                             ,tgt.Sort_Order = src.Sort_Order;      
      
            MERGE INTO dbo.Bucket_Category_Rule tgt      
                  USING       
                        ( SELECT      
                              catbm.Bucket_Master_Id AS Category_Bucket_Master_Id      
                             ,chbm.Bucket_Master_Id AS Child_Bucket_Master_Id      
                             ,bmn.Priority_Order      
                             ,agtyp.Code_Id AS Aggregation_Type_Cd      
                             ,Aglvl.Code_Id AS CU_Aggregation_Level_Cd      
                             ,bmn.Is_Aggregate_Category_Bucket      
                          FROM      
                              @Other_Service_Bucket_Category_Rules bmn      
                              INNER JOIN dbo.Code cd      
                                    ON cd.Code_value = bmn.Bucket_Type      
                              INNER JOIN dbo.Codeset cs      
                                    ON cs.codeset_id = cd.Codeset_Id      
                              INNER JOIN dbo.Code AgTyp      
                                    ON agtyp.Code_Value = bmn.Aggregation_Type      
                              INNER JOIN dbo.Codeset agTypCs      
                                    ON agtypCs.Codeset_Id = agtyp.Codeset_Id      
                              INNER JOIN dbo.Code Aglvl      
                                    ON Aglvl.Code_Value = bmn.Cu_Aggregation_Level      
                              INNER JOIN dbo.Codeset AglvlCs      
                                    ON AglvlCs.Codeset_Id = aglvl.Codeset_Id      
                              LEFT OUTER JOIN dbo.Bucket_Master Catbm      
                                    ON Catbm.Bucket_Name = bmn.Category_Bucket_Name      
                                       AND catbm.Commodity_Id = @Commodity_Id      
                                       AND catbm.Bucket_Type_Cd = cd.Code_Id      
                              LEFT OUTER JOIN dbo.Bucket_Master Chbm      
                                    ON chBm.Bucket_Name = bmn.Child_Bucket_Name      
                                       AND chbm.Commodity_Id = @Commodity_Id      
                                       AND chbm.Bucket_Type_Cd = cd.Code_Id      
                          WHERE      
                              cs.Codeset_Name = 'Bucket Type'      
                              AND agtypcs.Codeset_Name = 'CuAggregataionType'      
                              AND aglvlcs.Codeset_Name = 'BucketAggregationLevel' ) src      
               ON src.Category_Bucket_Master_Id = tgt.Category_Bucket_Master_Id      
                        AND src.Child_Bucket_Master_Id = tgt.Child_Bucket_Master_Id      
                        AND src.CU_Aggregation_Level_CD = tgt.CU_Aggregation_Level_Cd      
                  WHEN NOT MATCHED       
                        THEN      
  INSERT      
                              (       
                               Category_Bucket_Master_Id      
                              ,Child_Bucket_Master_Id      
                              ,Priority_Order      
                              ,Aggregation_Type_CD      
                              ,CU_Aggregation_Level_Cd      
                              ,Is_Aggregate_Category_Bucket )      
                         VALUES      
                              (       
                               src.Category_Bucket_Master_Id      
                              ,src.Child_Bucket_Master_Id      
                              ,src.Priority_Order      
                              ,src.Aggregation_Type_CD      
                              ,src.CU_Aggregation_Level_Cd      
                              ,src.Is_Aggregate_Category_Bucket )      
                  WHEN MATCHED       
                        THEN      
  UPDATE           SET        
                              Priority_Order = src.Priority_Order      
                             ,Aggregation_Type_CD = src.Aggregation_Type_Cd      
                             ,Is_Aggregate_Category_Bucket = src.Is_Aggregate_Category_Bucket;      
      
            COMMIT TRAN      
      
      END TRY      
      
      BEGIN CATCH      
            IF @@TRANCOUNT > 0       
                  BEGIN      
                        ROLLBACK TRAN      
                  END      
      
            EXEC usp_RethrowError      
      
      END CATCH      
      
END;      
      
      
      
;    
GO


GRANT EXECUTE ON  [dbo].[Add_Bucket_Master_And_Bucket_Category_Rule_For_New_Commodity] TO [CBMSApplication]
GO
