
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
/*********  
NAME:  dbo.cbmsCuInvoice_GetManualForProKarmaUpload4    
   
DESCRIPTION:  
 procedure is used for data extracts via DTS packages  
  
INPUT PARAMETERS:      
      Name     DataType          Default     DescriptiON      
------------------------------------------------------------      
     @start_letter   char(1)     'a'  
  @end_letter   char(1)           'z'  
  
OUTPUT PARAMETERS:  
      Name              DataType          Default     Description  
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 EXEC dbo.cbmsCuInvoice_GetManualForProKarmaUpload4   
   
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------          
HG   Harihara Suthan G  
DMR      Deana Ritter  
PD	 Padmanava Debnath
	  
Initials Date  ModificatiON    
------------------------------------------------------------    
   07/20/2009 Created  
 HG   03/23/2010 Bucket_type_id column renamed to Bucket_Master_id in Cu_Invoice_Charge and Cu_Invoice_Determinant table.  
      Entity table reference to get Bucket_name replaced by Bucket_Master table  
      Entity table reference to get Commodity_name replaced by Commodity table.  
 DMR    08/20/2010   Removed Meter table from Account CTE as this was generating multiple results unnecessarily.  This cause invoices to be  
                     incorrectly reported as group accounts.  
 PD		11/02/2010	 Added WHERE clause in the CTE to filter out AT&T data
 DMR    06/20/2011   Removed join to Invoice Charge account in pulling base invoice as we now receive determinants with no charges. 
                     Per Erin Dierking 6/20/2011.
 
******/  
  
CREATE PROCEDURE [dbo].[cbmsCuInvoice_GetManualForProKarmaUpload4]
      ( 
       @start_letter CHAR(1) = 'a'
      ,@end_letter CHAR(1) = 'z' )
AS 
BEGIN  
  
      SET NOCOUNT ON  
      SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
      DECLARE @session_uid UNIQUEIDENTIFIER  
      SET @session_uid = newid()  
  
      INSERT      INTO dbo.RPT_ACCOUNT_CU_INVOICE
                  ( 
                   session_uid
                  ,account_id
                  ,cu_invoice_id )
                  SELECT
                        @session_uid
                       ,sm.account_id
                       ,max(cu.cu_invoice_id) cu_invoice_id
                  FROM
                        cu_invoice cu
                        JOIN cu_invoice_service_month sm
                              ON sm.cu_invoice_id = cu.cu_invoice_id
                        JOIN ( SELECT
                                    sm.account_id
                                   ,max(cu.updated_date) updated_date
                               FROM
                                    cu_invoice cu
                                    JOIN cu_invoice_service_month sm
                                          ON sm.cu_invoice_id = cu.cu_invoice_id
                               WHERE
                                    cu.is_processed = 1
                                    AND ( cu.ubm_id IS NULL
                                          OR cu.ubm_id IN ( 1, 2 ) )
                               GROUP BY
                                    SM.account_id ) x
                              ON x.account_id = sm.account_id
                                 AND x.updated_date = cu.updated_date
                  WHERE
                        cu.is_processed = 1
                        AND ( cu.ubm_id IS NULL
                              OR cu.ubm_id IN ( 1, 2 ) )
                        AND cu.updated_date >= getdate() - 181
                  GROUP BY
                        sm.account_id  
  
      INSERT      INTO rpt_cu_invoice_detail
                  ( 
                   session_uid
                  ,item_type
                  ,cu_invoice_id
                  ,commodity_type
                  ,item_name
                  ,item_code
                  ,unit_of_measure
                  ,item_value )
                  SELECT
                        @session_uid
                       ,'Determinant' item_type
                       ,d.cu_invoice_id
                       ,c.Commodity_Name commodity_type
                       ,d.determinant_name item_name
                       ,bm.Bucket_Name item_code
                       ,u.entity_name unit_of_measure
                       ,d.determinant_value item_value
                  FROM
                        rpt_account_cu_invoice cu
                        JOIN cu_invoice_determinant d
                              ON cu.cu_invoice_id = d.cu_invoice_id
                        JOIN Commodity c
                              ON c.Commodity_Id = d.commodity_type_id
                        JOIN Bucket_Master bm
                              ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                        JOIN entity u
                              ON u.entity_id = d.unit_of_measure_type_id
                  WHERE
                        cu.session_uid = @session_uid
                        AND d.determinant_value IS NOT NULL  
  
      INSERT      INTO rpt_cu_invoice_detail
                  ( 
                   session_uid
                  ,item_type
                  ,cu_invoice_id
                  ,commodity_type
                  ,item_name
                  ,item_code
                  ,unit_of_measure
                  ,item_value )
                  SELECT
                        @session_uid
                       ,'Charge' item_type
                       ,d.cu_invoice_id
                       ,c.Commodity_Name commodity_type
                       ,d.charge_name item_name
                       ,bm.Bucket_Name item_code
                       ,NULL unit_of_measure
                       ,d.charge_value item_value
                  FROM
                        dbo.rpt_account_cu_invoice cu
                        JOIN dbo.cu_invoice_charge d
                              ON d.cu_invoice_id = cu.cu_invoice_id
                        JOIN dbo.Commodity c
                              ON c.Commodity_Id = d.commodity_type_id
                        JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = d.Bucket_Master_Id
                  WHERE
                        cu.session_uid = @session_uid
                        AND d.charge_value IS NOT NULL ;  
  
      WITH  Cte_Account_Dtl
              AS ( SELECT
                        a.Site_id
                       ,a.account_id
                       ,a.account_type_id
                       ,a.account_number
                       ,a.VENDOR_ID
                   FROM
                        dbo.account a
                        JOIN dbo.meter m
                              ON m.account_id = a.account_id
                        JOIN dbo.SITE s
                              ON s.SITE_ID = a.SITE_ID
                   WHERE
                        s.Client_ID != 11231
                        AND a.not_managed = 0
                   UNION ALL
                   SELECT
                        ad.address_parent_id site_id
                       ,a.account_id
                       ,a.account_type_id
                       ,a.ACCOUNT_NUMBER
                       ,a.VENDOR_ID
                   FROM
                        dbo.account a
                        JOIN dbo.supplier_account_meter_map map
                              ON map.account_id = a.account_id
                        JOIN dbo.meter m
                              ON m.meter_id = map.meter_id
                        JOIN dbo.address ad
                              ON ad.address_id = m.address_id
                        JOIN dbo.SITE s
                              ON s.SITE_ID = ad.ADDRESS_PARENT_ID
                   WHERE
                        s.Client_ID != 11231
                        AND map.is_history = 0
                        AND a.not_managed = 0
                   GROUP BY
                        ad.address_parent_id
                       ,a.account_id
                       ,a.account_type_id
                       ,a.account_number
                       ,a.VENDOR_ID )
            SELECT  
  DISTINCT
                  cl.client_id AS 'Client Id'
                 ,cl.client_name AS 'Client'
                 ,vws.site_id AS 'Site Id'
                 ,vws.site_name AS 'Site'
                 ,vws.state_name AS 'State'
                 ,vam.account_id AS 'Account Id'
                 ,vam.account_number AS 'Account Number'
                 ,v.vendor_id AS 'Vendor Id'
                 ,v.vendor_name AS 'Vendor Name'
                 ,act.entity_name AS 'Account Type'
                 ,cu.cu_invoice_id AS 'Invoice Id'
                 ,convert(VARCHAR, sm.service_month, 101) AS 'Service Month'
                 ,cur.currency_unit_name AS 'Currency'
                 ,isnull(convert(VARCHAR, x.item_type), '') AS 'Item Type'
                 ,isnull(convert(VARCHAR, x.commodity_type), '') AS 'Commodity Type'
                 ,isnull(convert(VARCHAR, x.item_name), '') AS 'Item Name'
                 ,isnull(convert(VARCHAR, x.item_code), '') AS 'Item Code'
                 ,isnull(convert(VARCHAR, x.unit_of_measure), '') AS 'Unit of Measure'
                 ,isnull(convert(VARCHAR, x.item_value), '') AS 'Item Value'
                 ,isnull(convert(VARCHAR, ui.first_name + space(1) + ui.last_name), '') AS 'Updated By'
            FROM
                  dbo.rpt_account_cu_invoice cu
                  JOIN Cte_Account_Dtl vam
                        ON vam.account_id = cu.account_id
                  JOIN vwSiteName vws
                        ON vws.site_id = vam.site_id
                  JOIN client cl
                        ON cl.client_id = vws.client_id
                  JOIN vendor v
                        ON v.vendor_id = vam.vendor_id
                  JOIN entity act
                        ON act.entity_id = vam.account_type_id
                  JOIN cu_invoice i
                        ON i.cu_invoice_id = cu.cu_invoice_id
                  JOIN currency_unit cur
                        ON cur.currency_unit_id = i.currency_unit_id
                  JOIN user_info ui
                        ON ui.user_info_id = i.updated_by_id
                  JOIN cu_invoice_service_month sm
                        ON sm.cu_invoice_id = cu.cu_invoice_id
                  JOIN rpt_cu_invoice_detail x
                        ON x.cu_invoice_id = cu.cu_invoice_id
            WHERE
                  cu.session_uid = @session_uid
                  AND x.session_uid = @session_uid
                  AND cl.not_managed = 0
            ORDER BY
                  cl.client_name ASC
                 ,vws.site_name
                 ,vam.account_number
                 ,isnull(convert(VARCHAR, x.item_type), '') DESC
                 ,isnull(convert(VARCHAR, x.commodity_type), '') ASC
                 ,isnull(convert(VARCHAR, x.item_name), '')  
  
  
      DELETE
            rpt_account_cu_invoice
      WHERE
            session_uid = @session_uid  
  
      DELETE
            rpt_cu_invoice_detail
      WHERE
            session_uid = @session_uid  
  
END  
  
  
  
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoice_GetManualForProKarmaUpload4] TO [CBMSApplication]
GO
