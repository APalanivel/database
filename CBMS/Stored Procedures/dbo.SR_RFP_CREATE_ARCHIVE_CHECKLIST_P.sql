SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_ARCHIVE_CHECKLIST_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@RFP_ID int,
@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test archive creation
--exec [DBO].[SR_RFP_CREATE_ARCHIVE_CHECKLIST_P] 
--@RFP_ID = 5,
--@sr_rfp_account_id = 164

---- Test if the new archived record has updated vendor data
--exec SR_RFP_CREATE_ARCHIVE_CHECKLIST_P 197, 944

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [DBO].[SR_RFP_CREATE_ARCHIVE_CHECKLIST_P] 

@RFP_ID int,
@sr_rfp_account_id int

AS
	
set nocount on


	declare @current_date datetime
	select @current_date =  convert(datetime, convert( varchar(10), getdate(), 101 ))

	declare @cnt int
	select @cnt = count(1) from sr_rfp_archive_checklist where sr_rfp_account_id = 	@sr_rfp_account_id
	
	if(@cnt > 0)
	begin
	
		DELETE FROM sr_rfp_archive_checklist 
		WHERE sr_rfp_account_id = 	@sr_rfp_account_id 		
		
	end
	
DECLARE @sr_rfp_archive_checklist_id int, @rfp_account_id int, @vendor_name varchar(100), @contract_expiry_date datetime, 
	@contract_notice_date datetime, @is_notice_given bit, @is_contract_reviewed bit, @rfp_initiated_date datetime, 
	@lp_sent_to_client_date datetime, @lp_approved_date datetime, @rfp_sent_date datetime, @is_rc_completed bit, 
	@is_client_credit bit, @is_supplier_credit bit,	@is_supplier_docs bit, @sent_reco_date datetime, 
	@is_client_approved bit, @is_contract_admin_initiated bit, @new_supplier varchar(200), @contract_start_date datetime,
	@contract_end_date datetime, @utility_switch_supplier_date datetime, @utility_switch_deadline_date datetime, @is_switch_notice_given bit, 
	@is_verify_flow bit, @new_contract_number varchar(50), @sa_savings varchar(1000), @is_awarded_email_sent bit ,@contract_number varchar(150)


DECLARE C_ARC_CHECK CURSOR FAST_FORWARD
FOR select  
	rfp_account.SR_RFP_ACCOUNT_ID,
	v.VENDOR_NAME,
	checkList.IS_NOTICE_GIVEN,
	checkList.IS_CONTRACT_REVIEWED,
	checkList.RFP_INITIATED_DATE,
	checkList.LP_SENT_TO_CLIENT_DATE,
	checkList.LP_APPROVED_DATE,
	checkList.RFP_SENT_DATE,
	checkList.IS_RC_COMPLETED,
	checkList.IS_CLIENT_CREDIT,
	checkList.IS_SUPPLIER_CREDIT,
	checkList.IS_SUPPLIER_DOCS,
	checkList.SENT_RECO_DATE,
	checkList.IS_CLIENT_APPROVED,
	checkList.IS_CONTRACT_ADMIN_INITIATED,
	dbo.SR_RFP_FN_GET_VENDOR_NAME(checklist.NEW_SUPPLIER_ID) as newSupplier,
	checkList.CONTRACT_START_DATE,
	checkList.CONTRACT_END_DATE,
	checkList.UTILITY_SWITCH_DEADLINE_DATE,
	checkList.UTILITY_SWITCH_SUPPLIER_DATE,
	checkList.IS_SWITCH_NOTICE_GIVEN,
	checkList.IS_VERIFY_FLOW,
	dbo.SR_RFP_FN_GET_CONTRACT_NUMBER(checklist.NEW_CONTRACT_ID) as newContract,
	checkList.SA_SAVINGS,
	checkList.IS_AWARDED_EMAIL_SENT

from
	sr_rfp_account rfp_account,
	sr_rfp_closure closure,
	vendor v ,
	sr_rfp_checklist checkList,
	account a

where
	rfp_account.sr_rfp_id = @RFP_ID	
	and closure.sr_account_group_id = rfp_account.sr_rfp_account_id 
	and rfp_account.is_deleted = 0
	and checkList.sr_rfp_account_id = @sr_rfp_account_id
	and rfp_account.sr_rfp_account_id = checkList.sr_rfp_account_id
	and rfp_account.account_id = a.account_id
	and a.vendor_id = v.vendor_id

OPEN C_ARC_CHECK

FETCH NEXT FROM C_ARC_CHECK INTO @rfp_account_id, @vendor_name, 
				@is_notice_given, @is_contract_reviewed, @rfp_initiated_date, @lp_sent_to_client_date,
				@lp_approved_date, @rfp_sent_date, @is_rc_completed, @is_client_credit, @is_supplier_credit,
				@is_supplier_docs, @sent_reco_date, @is_client_approved, @is_contract_admin_initiated,
				@new_supplier, @contract_start_date, @contract_end_date, @utility_switch_deadline_date, @utility_switch_supplier_date,
				@is_switch_notice_given, @is_verify_flow, @new_contract_number, @sa_savings, @is_awarded_email_sent
	
				
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN
	
	select @contract_number = contract_number , @contract_expiry_date = contract_end_date, @contract_notice_date =contract_notice_date , @vendor_name = vendor_name
	from dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_ARCHIEVING(@sr_rfp_account_id,@current_date , @RFP_ID )  

		INSERT INTO  SR_RFP_ARCHIVE_CHECKLIST 
				( 
					SR_RFP_ACCOUNT_ID, CURRENT_SUPPLIER, CONTRACT_EXPIRY_DATE, CONTRACT_NOTICE_DATE, IS_NOTICE_GIVEN,
					IS_CONTRACT_REVIEWED, RFP_INITIATED_DATE, LP_SENT_TO_CLIENT_DATE, LP_APPROVED_DATE,
					RFP_SENT_DATE, IS_RC_COMPLETE, IS_CLIENT_CREDIT, IS_SUPPLIER_CREDIT, IS_SUPPLIER_DOCS,
					SENT_RECO_DATE, IS_CLIENT_APPROVED, IS_CONTRACT_ADMIN_INITIATED, NEW_SUPPLIER_NAME,
					CONTRACT_START_DATE, CONTRACT_END_DATE, UTILITY_SWITCH_DEADLINE_DATE, UTILITY_SWITCH_SUPPLIER_DATE, IS_SWITCH_NOTICE_GIVEN,
					IS_VERIFY_FLOW, NEW_CONTRACT_NUMBER, SA_SAVINGS, IS_AWARDED_EMAIL_SENT ,ED_CONTRACT_NUMBER
				)

		VALUES	( 	
					@rfp_account_id, @vendor_name, @contract_expiry_date, @contract_notice_date,
					@is_notice_given, @is_contract_reviewed, @rfp_initiated_date, @lp_sent_to_client_date,
					@lp_approved_date, @rfp_sent_date, @is_rc_completed, @is_client_credit, @is_supplier_credit,
					@is_supplier_docs, @sent_reco_date, @is_client_approved, @is_contract_admin_initiated,
					@new_supplier, @contract_start_date, @contract_end_date, @utility_switch_deadline_date, @utility_switch_supplier_date,
					@is_switch_notice_given, @is_verify_flow, @new_contract_number, @sa_savings, @is_awarded_email_sent,@contract_number 
				)
	
		SELECT @sr_rfp_archive_checklist_id = SCOPE_IDENTITY()
		
	END -- IF
		FETCH NEXT FROM C_ARC_CHECK INTO @rfp_account_id, @vendor_name,
						@is_notice_given, @is_contract_reviewed, @rfp_initiated_date, @lp_sent_to_client_date,
						@lp_approved_date, @rfp_sent_date, @is_rc_completed, @is_client_credit, @is_supplier_credit,
						@is_supplier_docs, @sent_reco_date, @is_client_approved, @is_contract_admin_initiated,
						@new_supplier, @contract_start_date, @contract_end_date, @utility_switch_deadline_date, @utility_switch_supplier_date,
						@is_switch_notice_given, @is_verify_flow, @new_contract_number, @sa_savings, @is_awarded_email_sent
END 
CLOSE C_ARC_CHECK
DEALLOCATE C_ARC_CHECK
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_ARCHIVE_CHECKLIST_P] TO [CBMSApplication]
GO
