SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE     PROCEDURE [dbo].[cbmsDoNotTrack_GetForProkarmaUpload]  
AS  
BEGIN  
  
select distinct isNull(x.client_name, '') as 'Client'  
  , isNull(x.client_city, '') as 'City'  
  , isNull(st.state_name, '') as 'State'  
  , isNull(x.account_number, '') as 'Account Number'  
  , isNull(x.vendor_name , '') as 'Vendor'  
      from do_not_track x  
      join state st on st.state_id = x.state_id
      where x.CLIENT_NAME NOT LIKE 'AT&T%'  
/**********************************************************************************************************  
    3/10/2009    DMR  
    Eliminated Union All and entire second query.  This was producing duplicates and no data from the   
    Account table was actually used.  This modification produced the same result set as removing just  
    the "all" from the union query and performs significantly faster.  
    11/2/2010	PD
    Added WHERE clause to eliminate AT&T data
***********************************************************************************************************/  
  
--      where account_id is null  
--   
--  union all  
--   
--     select distinct isNull(x.client_name, '') as 'Client'  
--   , isNull(x.client_city, '') as 'City'  
--   , isNull(st.state_name, '') as 'State'  
--   , isNull(x.account_number, '') as 'Account Number'  
--   , isNull(x.vendor_name , '') as 'Vendor'  
--       from do_not_track x  
--       join account a on a.account_id = x.account_id  
--       join state st on st.state_id = x.state_id  
  
  
END  
  
GO
GRANT EXECUTE ON  [dbo].[cbmsDoNotTrack_GetForProkarmaUpload] TO [CBMSApplication]
GO
