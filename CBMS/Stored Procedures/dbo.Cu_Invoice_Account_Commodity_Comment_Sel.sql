SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
NAME:                
 CBMS.dbo.Cu_Invoice_Account_Commodity_Comment_Sel                
                
DESCRIPTION:                
                
INPUT PARAMETERS:                
 Name     DataType  Default     Description                
---------------------------------------------------------------------------------------------------                
 @Account_Id    INT                
 @Cu_Invoice_Id   INT                
 @Commodity_Id   INT                
 @User_Info_Id   INT                
                 
OUTPUT PARAMETERS:                
 Name     DataType  Default     Description                
---------------------------------------------------------------------------------------------------                
                 
USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------                
                
 SELECT * FROM dbo.Cu_Invoice_Account_Commodity_Comment                
                
                           
    EXEC dbo.Cu_Invoice_Account_Commodity_Comment_Sel                 
      @Cu_Invoice_Id = 45327839                
     ,@Account_Id = 1114950                
     ,@Commodity_Id = 291    
     , @User_Info_Id = 16      
          
     ,@Comment_type = 'Internal Recalc Comment'                
                     
AUTHOR INITIALS:                
 Initials Name                
---------------------------------------------------------------------------------------------------                
 NR   Narayana Reddy          
 KAB Aditya Bharadwaj                
                 
MODIFICATIONS                
 Initials Date  Modification                
---------------------------------------------------------------------------------------------------                
 NR   2015-05-19 Created for AS400 Phase -2                
 HG   2016-06-29 Removed the filter on @User_Info_id , keeping it in the parameters list as the app code is not changed.          
 KAB  2019-06-10 SE2017-687: Added @comment_type parameter    
 KAB  2019-06-14 SE2017-687: Added Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map table and   Recalc_Status,Recalc_Status_Type columns    
******/

CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Comment_Sel]
     (
         @Cu_Invoice_Id INT
         , @Account_Id INT
         , @Commodity_Id INT
         , @User_Info_Id INT
         , @Comment_type VARCHAR(25)
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Comment_Type_CD INT;

        SELECT
            @Comment_Type_CD = cd.Code_Id
        FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                ON cd.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = @Comment_type;


        SELECT
            ciac.Cu_Invoice_Id
            , ciac.Account_Id
            , ciac.Commodity_Id
            , ciac.Comment_Id
            , comm.Comment_Text
            , comm.Comment_Dt
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Commented_By
            , c.Code_Value AS Comment_Type
            , e.ENTITY_NAME AS Recalc_Status
            , e.ENTITY_DESCRIPTION AS Recalc_Status_Type
        FROM
            dbo.Cu_Invoice_Account_Commodity_Comment ciac
            INNER JOIN dbo.Comment comm
                ON ciac.Comment_Id = comm.Comment_ID
            INNER JOIN dbo.USER_INFO ui
                ON comm.Comment_User_Info_Id = ui.USER_INFO_ID
            INNER JOIN dbo.Code c
                ON c.Code_Id = comm.Comment_Type_CD
            LEFT JOIN dbo.Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map ciaccrstm
                ON ciaccrstm.Cu_Invoice_Account_Commodity_Comment_Id = ciac.Cu_Invoice_Account_Commodity_Comment_Id
            LEFT JOIN dbo.ENTITY e
                ON e.ENTITY_ID = ciaccrstm.Recalc_Status_Type_Id
        WHERE
            ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ciac.Account_Id = @Account_Id
            AND ciac.Commodity_Id = @Commodity_Id
            AND comm.Comment_Type_CD = @Comment_Type_CD
        GROUP BY
            ciac.Cu_Invoice_Id
            , ciac.Account_Id
            , ciac.Commodity_Id
            , ciac.Comment_Id
            , comm.Comment_Text
            , comm.Comment_Dt
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , c.Code_Value
            , e.ENTITY_NAME
            , e.ENTITY_DESCRIPTION
        ORDER BY
            comm.Comment_Dt ASC;


    END;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Comment_Sel] TO [CBMSApplication]
GO
