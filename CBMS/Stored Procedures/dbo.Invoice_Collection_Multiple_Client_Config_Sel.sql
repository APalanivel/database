SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Client_Config_Sel            
                        
 DESCRIPTION:                        
			To get the details of invoice collection Client level config                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id								INT
@Is_Active								INT
@Invoice_Collection_Client_Config_Id	INT		NULL 
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
  BEGIN TRAN
  DECLARE @Id INT
  EXEC [dbo].[Invoice_Collection_Client_Config_Merge] 
      @Client_Id = 235
     ,@Is_Active = 1
     ,@Invoice_Collection_Service_Start_Dt = '2016-01-01'
     ,@Invoice_Collection_Service_End_Dt = '2016-12-01'
     ,@Invoice_Collection_Officer_User_Id = 49
     ,@Invoice_Collection_Coordinator_User_Id = 49
     ,@Invoice_Collection_Service_Level_Cd = 102252
     ,@User_Info_Id = 49
      
  SELECT
      @Id = Invoice_Collection_Client_Config_Id
  FROM
      dbo.Invoice_Collection_Client_Config
  WHERE
      Client_Id = 235
    
 --SAMPLE 1
  EXEC [dbo].[Invoice_Collection_Client_Config_Sel] 
      @Client_Id = 235
     ,@Is_Active = 1
      
--SAMPLE 2
  EXEC [dbo].[Invoice_Collection_Multiple_Client_Config_Sel] 
  @Client_Id = '11278,12327'
     ,@Is_Active = 1
      ,@Invoice_Collection_Client_Config_Id = @Id
  ROLLBACK         
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 
 SLP					2019-10-24				Created
                       
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Multiple_Client_Config_Sel]
     (
         @Client_Id VARCHAR(MAX)
         , @Is_Active INT
         , @Invoice_Collection_Client_Config_Id INT = NULL
         , @Invoice_Collection_Service_Start_Dt DATE = NULL
         , @Invoice_Collection_Service_End_Dt DATE = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Client_ids
             (
                 Client_Id INT
                 , Client_name VARCHAR(255)
             );

        INSERT INTO #Client_ids
        SELECT
            Segments
            , MAX(Client_Name)
        FROM
            dbo.ufn_split(@Client_Id, ',') us
            INNER JOIN Core.Client_Hier ch
                ON us.Segments = ch.Client_Id
        GROUP BY
            Segments;


        SELECT
            iccc.Invoice_Collection_Client_Config_Id
            , iccc.Client_Id
            , iccc.Is_Active
            , iccc.Invoice_Collection_Service_Start_Dt
            , iccc.Invoice_Collection_Service_End_Dt
            , iccc.Invoice_Collection_Officer_User_Id
            , iccc.Invoice_Collection_Coordinator_User_Id
            , iccc.Invoice_Collection_Service_Level_Cd
            , iccc.Invoice_Collection_Setup_Instruction_Comment
            , uio.FIRST_NAME + ' ' + uio.LAST_NAME AS Officer
            , uic.FIRST_NAME + ' ' + uic.LAST_NAME AS Coordinator
            , c.Client_name AS Client_Name
        FROM
            dbo.Invoice_Collection_Client_Config iccc
            INNER JOIN #Client_ids c
                ON iccc.Client_Id = c.Client_Id
                   AND  iccc.Is_Active = 1
            LEFT JOIN dbo.USER_INFO uio
                ON iccc.Invoice_Collection_Officer_User_Id = uio.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO uic
                ON iccc.Invoice_Collection_Officer_User_Id = uic.USER_INFO_ID
        WHERE
            (   ( --iccc.Client_Id = @Client_Id
                --AND 
                iccc.Is_Active = 1)
                OR  Invoice_Collection_Client_Config_Id = @Invoice_Collection_Client_Config_Id)
            AND (   @Invoice_Collection_Service_Start_Dt IS NULL
                    OR  (   Invoice_Collection_Service_Start_Dt BETWEEN @Invoice_Collection_Service_Start_Dt
                                                                AND     @Invoice_Collection_Service_End_Dt
                            OR  Invoice_Collection_Service_End_Dt BETWEEN @Invoice_Collection_Service_End_Dt
                                                                  AND     @Invoice_Collection_Service_End_Dt
                            OR  @Invoice_Collection_Service_Start_Dt BETWEEN Invoice_Collection_Service_Start_Dt
                                                                     AND     Invoice_Collection_Service_End_Dt
                            OR  @Invoice_Collection_Service_End_Dt BETWEEN Invoice_Collection_Service_End_Dt
                                                                   AND     Invoice_Collection_Service_End_Dt));

        DROP TABLE #Client_ids;

    END;

    ;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Multiple_Client_Config_Sel] TO [CBMSApplication]
GO
