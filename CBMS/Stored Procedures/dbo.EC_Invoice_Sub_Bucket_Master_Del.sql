
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Invoice_Sub_Bucket_Master_Del           
              
Description:              
        To delete Data to EC_Invoice_Sub_Bucket_Master table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
 	@EC_Invoice_Sub_Bucket_Master_Id	INT               
       
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------  
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	BEGIN TRAN  
	SELECT * FROM EC_Invoice_Sub_Bucket_Master WHERE EC_Invoice_Sub_Bucket_Master_Id=5
	EXEC dbo.EC_Invoice_Sub_Bucket_Master_Del 
      @EC_Invoice_Sub_Bucket_Master_Id = 5
      SELECT * FROM EC_Invoice_Sub_Bucket_Master WHERE EC_Invoice_Sub_Bucket_Master_Id=5
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
	RKV             Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
    RKV             2015-11-10      Added delete statement to delete the data in Ec_Calc_Val_Bucket_Sub_Bucket_Map as part of AS400-PII
******/ 
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Master_Del]
      ( 
       @EC_Invoice_Sub_Bucket_Master_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DELETE
            ecvbs
      FROM
            dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbs
      WHERE
            ecvbs.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id
      
      DELETE
            eisbm
      FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
      WHERE
            eisbm.EC_Invoice_Sub_Bucket_Master_Id = @EC_Invoice_Sub_Bucket_Master_Id
END;
;
GO

GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Master_Del] TO [CBMSApplication]
GO
