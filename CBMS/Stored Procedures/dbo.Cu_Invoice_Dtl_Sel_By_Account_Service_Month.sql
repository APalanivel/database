SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********         
NAME:  dbo.Cu_Invoice_Dtl_Sel_By_Account_Service_Month
       
DESCRIPTION:   
      
INPUT PARAMETERS:          
 Name              DataType          Default     Description          
----------------------------------------------------------------------------------------------          
 @Account_Id		INT
 @Service_Month		VARCHAR(MAX)
          
OUTPUT PARAMETERS:          
Name              DataType          Default     Description          
----------------------------------------------------------------------------------------------          
USAGE EXAMPLES: 
SELECT * FROM dbo.CU_INVOICE_SERVICE_MONTH cism WHERE cism.CU_INVOICE_ID =69668881
SELECT * FROM dbo.CU_INVOICE cism WHERE cism.CU_INVOICE_ID =69668881

EXEC dbo.Cu_Invoice_Dtl_Sel_By_Account_Service_Month
    @Account_Id = 1394663
    , @Service_Month = '2017-11-01 00:00:00.000,2017-12-01 00:00:00.000'



AUTHOR INITIALS:        
Initials	Name        
----------------------------------------------------------------------------------------------          
NR			Narayana Reddy
      
Initials	Date		Modification        
----------------------------------------------------------------------------------------------          
NR			2018-13-11	Created Data2.0.    
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Dtl_Sel_By_Account_Service_Month]
    (
        @Account_Id INT
        , @Service_Month VARCHAR(MAX)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ci.CU_INVOICE_ID
            , cism.Account_ID
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , cism.Billing_Days
        FROM
            dbo.CU_INVOICE ci
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
        WHERE
            cism.Account_ID = @Account_Id
            AND ci.IS_DEFAULT = 1
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Service_Month, ',') ufn
                           WHERE
                                cism.SERVICE_MONTH = ufn.Segments);



    END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Dtl_Sel_By_Account_Service_Month] TO [CBMSApplication]
GO
