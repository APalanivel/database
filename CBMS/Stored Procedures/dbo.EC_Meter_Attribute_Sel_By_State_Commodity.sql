SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                                  
Name:   dbo.EC_Meter_Attribute_Sel_By_State_Commodity                           
                                  
Description:                                  
   This sproc get the attribute details for the given state and commodity.                                   
                                  
 Input Parameters:                                  
    Name        DataType   Default   Description                                    
----------------------------------------------------------------------------------------                                    
    @Commodity_Id      INT                    
    @State_Id       INT                            
                        
 Output Parameters:                                        
    Name        DataType   Default   Description                                    
----------------------------------------------------------------------------------------                                    
                                  
 Usage Examples:                                      
----------------------------------------------------------------------------------------                       
                    
   Exec dbo.EC_Meter_Attribute_Sel_By_State_Commodity 7,291,NULL,100026 ,NULL                  
   Exec dbo.EC_Meter_Attribute_Sel_By_State_Commodity 236,290                    
   Exec dbo.EC_Meter_Attribute_Sel_By_State_Commodity 236,290,1                    
   Exec dbo.EC_Meter_Attribute_Sel_By_State_Commodity 73,290                      
                       
                    
Author Initials:                                  
    Initials  Name                                  
----------------------------------------------------------------------------------------                                    
 NR  Narayana Reddy                   
 SC  Sreenivasulu Cheerala    
 SLP Sri Lakshmi Pallikonda                                
 Modifications:                                  
    Initials        Date   Modification                                  
----------------------------------------------------------------------------------------                                    
    NR    2015-04-22 Created For AS400.                             
    SC    2019-12-05 Added New Parameter @Vendor_Type_cd and  @Meter_Attribute_Type_Cd in the filer list.  
    SLP	  2020-05-29 Included the logic to search for Vendor type as below   
					 if Blank : no attributes
					 if Distributor : MA that are tagged as Distributor & Both       
					 if Supplier	: MA that are tagged as Supplier & Both       
					 if Both		: all MA 
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Sel_By_State_Commodity]
     (
         @State_Id INT
         , @Commodity_Id INT
         , @Is_Used_In_Calc_Vals BIT = NULL
         , @Vendor_Type_Cd INT = NULL
         , @Meter_Attribute_Type_Cd INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Vendor_Type_Cd_Both INT;
        SELECT
            @Vendor_Type_Cd_Both = c.Code_Id
        FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            c2.Codeset_Name = 'VendorType'
            AND c.Code_Value = 'Both';

		--Incase @Vendor_Type_Cd=Both then all meter attributes must be displayed.
		IF (@Vendor_Type_Cd=@Vendor_Type_Cd_Both) 
			SET @Vendor_Type_Cd=NULL

        SELECT
            ema.EC_Meter_Attribute_Id
            , ema.State_Id
            , ema.Commodity_Id
            , ema.EC_Meter_Attribute_Name
            , ema.Attribute_Type_Cd
            , c.Code_Value AS Attribute_Type
            , ema.Is_Used_In_Calc_Vals
            , ema.Vendor_Type_Cd
        FROM
            dbo.EC_Meter_Attribute ema
            INNER JOIN dbo.Code c
                ON c.Code_Id = ema.Attribute_Type_Cd
            LEFT JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON emat.EC_Meter_Attribute_Id = ema.EC_Meter_Attribute_Id
        WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id
            AND (   @Is_Used_In_Calc_Vals IS NULL
                    OR  ema.Is_Used_In_Calc_Vals = @Is_Used_In_Calc_Vals)
            AND (   @Meter_Attribute_Type_Cd IS NULL
                    OR  ema.Attribute_Type_Cd = @Meter_Attribute_Type_Cd)
            AND (   @Vendor_Type_Cd IS NULL
                    OR  ema.Vendor_Type_Cd IN ( @Vendor_Type_Cd, @Vendor_Type_Cd_Both ))
        GROUP BY
            ema.EC_Meter_Attribute_Id
            , ema.State_Id
            , ema.Commodity_Id
            , ema.EC_Meter_Attribute_Name
            , ema.Attribute_Type_Cd
            , c.Code_Value
            , ema.Is_Used_In_Calc_Vals
            , ema.Vendor_Type_Cd;
    END;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Sel_By_State_Commodity] TO [CBMSApplication]
GO
