SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




  
  
     
       
/******                  
NAME:    [Workflow].[Get_List_Of_Invoices]              
DESCRIPTION: This is the master stored procedure created to display the result set                  
  the inputs will come with pipe delimeted if filter type is multi select              
------------------------------------------------------------                 
 INPUT PARAMETERS:                  
 Name   DataType  Default Description                  
  @MODULE_ID INT,          
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,          
    @Logged_In_User INT,          
    @Queue_id VARCHAR(MAX) = NULL, ---  User selected from drop down Id 49,1,2,3                    
    @Exception_Type VARCHAR(MAX) = NULL,          
    @Account_Number VARCHAR(500) = NULL,          
    @Client VARCHAR(500) = NULL,          
    @Site VARCHAR(500) = NULL,          
    @Country VARCHAR(500) = NULL,          
    @State VARCHAR(500) = NULL,          
    @City VARCHAR(500) = NULL,          
    @Commodity VARCHAR(500) = NULL,          
    @Invoice_ID VARCHAR(500) = NULL,          
    @Priority INT = 0,             --- Yes                    
    @Exception_Status INT = NULL,          
    @Comments VARCHAR(500) = NULL,          
    @Start_Date_in_Queue DATETIME = NULL,          
    @End_Date_in_Queue DATETIME = NULL,          
    @Start_Date_in_CBMS DATETIME = NULL,          
    @End_Date_in_CBMS DATETIME = NULL,          
    @Data_Source INT = NULL,          
    @Vendor VARCHAR(100) = NULL,          
    @Vendor_Type VARCHAR(100) = NULL,          
    @Month DATETIME = NULL,          
    @Filename VARCHAR(100) = NULL,          
    @invoice_list VARCHAR(MAX),          
    @User_Group_Id_List VARCHAR(MAX),          
    @Invoice_List_TVP Invoice_List READONLY          
------------------------------------------------------------                  
 OUTPUT PARAMETERS:                  
 Name   DataType  Default Description                  
               
------------------------------------------------------------                  
 USAGE EXAMPLES:                
DECLARE       @return_value int,                
             @TOTAL_COUNT INT,                
    @PAGE_ROW_COUNT INT                 
                
EXEC   @return_value = [Workflow].[Get_List_Of_Invoices]                
             @MODULE_ID = 1, --- workflow module id                
             @Workflow_Queue_Saved_Filter_Query_Id = 147, --                 
    @Logged_In_User =49,--user id done                
             @Queue_id = '49',-- done  -- queue_id from user_info table comma separated                
             @Exception_Type =null, -- done multiple exception type with comma separated                
             @Account_Number =null, -- done                
             @Client = null, -- done                
             @Site = null, --done                
             @Country = null, --multiple country id (1,4) comma separated done                
             @State =null, --state id comma separated done                
             @City = NULL, --done                 
             @Commodity =null,-- done                
             @Invoice_ID = null, --invoice id string done                
             @Priority = null, --- Yes/No/NULL done                
             @exception_Status = null, --done                 
    @Comments =null, -- DONE                 
             @Start_Date_in_Queue = '9/20/2019', -- done                
             @End_Date_in_Queue = '10/21/2019',-- done                
             @Start_Date_in_CBMS = null,-- done                
             @End_Date_in_CBMS = null,-- done                
             @Data_Source = null, --done                
             @Vendor = null, --done                
    @Vendor_type = null, --done                
             @Month = null , --done                
             @Filename =null, -- done                  
    @startindex =1,                  
             @endindex = 50,                
             @ONLY_COUNT_NEEDED = 0,                
             @TOTAL_COUNT = @TOTAL_COUNT OUTPUT,                
    @PAGE_ROW_COUNT= @PAGE_ROW_COUNT  OUTPUT                
     
SELECT @TOTAL_COUNT as '@TOTAL_COUNT' ,   @PAGE_ROW_COUNT as '@PAGE_ROW_COUNT'                
                
SELECT 'Return Value' = @return_valueUSE [CBMS]          
GO          
           
DECLARE       @return_value int          
          
DECLARE @Invoice_List_TVP1 AS Invoice_List;           
             
          
EXEC    [Workflow].[Get_List_Of_Invoices]          
             @MODULE_ID = 1, --- workflow module id          
             @Workflow_Queue_Saved_Filter_Query_Id = 273, --           
    @Logged_In_User =71920,--user id done          
             @Queue_id = '122794,72546,102540,47128,118572,56113,64101,71929,59609',-- done  -- queue_id from user_info table comma separated          
             @Exception_Type =6, -- done multiple exception type with comma separated          
             @Account_Number =null, -- done          
             @Client = null, -- done          
             @Site = null, --done          
             @Country = null, --multiple country id (1,4) comma separated done          
             @State =null, --state id comma separated done          
             @City = NULL, --done           
             @Commodity =null,-- done          
             @Invoice_ID = null, --invoice id string done          
             @Priority = null, --- Yes/No/NULL done          
             @exception_Status = null, --done           
    @Comments =null, -- DONE           
             @Start_Date_in_Queue = null, -- done          
             @End_Date_in_Queue = null,-- done          
             @Start_Date_in_CBMS = null,-- done          
             @End_Date_in_CBMS = null,-- done          
             @Data_Source = null, --done          
             @Vendor = null, --done          
    @Vendor_type = null, --done          
             @Month = null , --done          
             @Filename =null, -- done           
            @invoice_list ='81154060,81153909',          
    @User_Group_Id_List ='225,226',          
    @Invoice_List_TVP  =@Invoice_List_TVP1           
            
SELECT 'Return Value' = @return_value          
          
GO           
          
           
 ------------------------------------------------------------                  
AUTHOR INITIALS:                  
Initials Name                  
------------------------------------------------------------                  
AKP    Arunkumar Palanivel Summit Energy               
TRK    Ramakrishna  Summit Energy              
 MODIFICATIONS                   
 Initials Date   Modification                  
------------------------------------------------------------                  
 AKP   Sep 30,2019 Created          
 TRK   15-Oct-2019 Updated the @Queue_id = '-2' to  @Queue_id = '-1,0'             
******/          
CREATE PROCEDURE [Workflow].[Get_List_Of_Invoices]          
    -- Add the parameters for the stored procedure here                     
    @MODULE_ID INT,          
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,          
    @Logged_In_User INT,          
    @Queue_id VARCHAR(MAX) = NULL, ---  User selected from drop down Id 49,1,2,3                    
    @Exception_Type VARCHAR(MAX) = NULL,          
    @Account_Number VARCHAR(500) = NULL,          
    @Client VARCHAR(500) = NULL,          
    @Site VARCHAR(500) = NULL,          
    @Country VARCHAR(500) = NULL,          
    @State VARCHAR(500) = NULL,          
    @City VARCHAR(500) = NULL,          
    @Commodity VARCHAR(500) = NULL,          
    @Invoice_ID VARCHAR(500) = NULL,          
    @Priority INT = 0,             --- Yes                    
    @Exception_Status INT = NULL,          
    @Comments VARCHAR(500) = NULL,          
    @Start_Date_in_Queue DATETIME = NULL,          
    @End_Date_in_Queue DATETIME = NULL,          
    @Start_Date_in_CBMS DATETIME = NULL,          
    @End_Date_in_CBMS DATETIME = NULL,      
    @Data_Source INT = NULL,          
    @Vendor VARCHAR(100) = NULL,          
    @Vendor_Type VARCHAR(100) = NULL,          
    @Month DATETIME = NULL,          
    @Filename VARCHAR(100) = NULL,          
    @invoice_list VARCHAR(MAX),          
    @User_Group_Id_List VARCHAR(MAX),          
    @Invoice_List_TVP Invoice_List READONLY          
AS          
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from                     
    -- interfering with SELECT statements.                     
          
          
    SET NOCOUNT ON;          
          
    DECLARE @PROC_NAME VARCHAR(100) = 'Get_List_Of_Invoices',          
            @INPUT_PARAMS VARCHAR(1000),          
            @ERROR_LINE INT,          
            @ERROR_MESSAGE VARCHAR(3000),          
            @SQL NVARCHAR(MAX) ,  
   @SQL1 NVARCHAR(MAX) ,  
   @SQL2 NVARCHAR(MAX) ,  
   @SQL3 NVARCHAR(MAX) ,  
   @SQL4 NVARCHAR(MAX) ,  
   @SQL5 NVARCHAR(MAX)          
          
      
 set @End_Date_in_Queue = dateadd (d, 1, @End_Date_in_Queue )         
   set @End_Date_in_CBMS =dateadd (d,1 , @End_Date_in_CBMS )        
          
          
   BEGIN TRY          
          
        /* STEP 1 FORMAT THE INPUTS. IF INPUT IS COMMA SEPARATED, SPILT THE STRING USING BELOW FORMULA*/          
          
          
        CREATE TABLE #temp_user_info          
        (          
            User_info_id INT          
        );          
          
        CREATE table #temp_Exception_Type            
        (          
            exception_type INT          
        );          
          
        CREATE TABLE #temp_User_Group          
        (          
            User_Group_Id INT          
        );          
          
          
          
        CREATE table #System_group            
        (          
            ubmid INT NULL,          
            ubmaccountcode VARCHAR(200) NULL,          
            Exception_Type VARCHAR(500)          
        );          
          
        IF (@Queue_id IS NOT NULL)          
       BEGIN          
          
            INSERT #temp_user_info          
            SELECT CAST(Segments AS INT) AS User_info_id          
            FROM dbo.ufn_split(@Queue_id, ',');          
        END;          
          
        IF (@Exception_Type IS NOT NULL)          
        BEGIN          
          
            INSERT #temp_Exception_Type          
            SELECT CAST(Segments AS INT) AS exception_type          
            FROM dbo.ufn_split(@Exception_Type, ',');          
        END;          
          
          
        IF (@User_Group_Id_List IS NOT NULL)          
        BEGIN          
          
            INSERT #temp_User_Group          
            SELECT CAST(Segments AS INT) AS User_Group_Id          
            FROM dbo.ufn_split(@User_Group_Id_List, ',');          
        END;          
          
          
          
        IF EXISTS (SELECT 1 FROM @Invoice_List_TVP)          
        BEGIN          
          
            INSERT INTO #System_group          
            (          
                ubmid,          
                ubmaccountcode,          
                Exception_Type          
            )          
            SELECT CI.UBM_ID,          
                   CI.UBM_ACCOUNT_CODE,          
                   I.Exception_type          
            FROM dbo.CU_INVOICE CI          
                JOIN @Invoice_List_TVP I          
                    ON CI.CU_INVOICE_ID = I.invoice_id          
                JOIN dbo.CU_EXCEPTION_DENORM D          
                    ON D.CU_INVOICE_ID = I.invoice_id          
                       AND D.EXCEPTION_TYPE = I.Exception_type;          
        END;          
          
        CREATE TABLE #Invoices_List_User          
        (          
            Row_num INT NOT NULL,          
            [CU_INVOICE_ID] [INT] NULL,          
            [COMMENTS] VARCHAR(MAX) NULL          
        );          
          
          
        CREATE TABLE #Invoices_List_System          
        (          
     row_num INT NOT NULL,          
            [CU_INVOICE_ID] [INT] NULL,          
            [COMMENTS] VARCHAR(MAX) NULL          
        );          
          
        DECLARE @Invoices_Master_List TABLE          
        (          
            [CU_INVOICE_ID] [INT] NULL          
        );          
        --first insert user group invoices          
          
        IF EXISTS (SELECT 1 FROM #temp_User_Group)          
        BEGIN          
            SET @SQL1          
                = '   insert #Invoices_List_User                    
     SELECT                     
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (select null)    ) AS RN,                     
   CUEX.CU_INVOICE_ID                    
   , null                               
FROM  DBO.CU_EXCEPTION_DENORM CUEX                  
JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID                  
                 INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID                  
      JOIN                  
      DBO.CU_INVOICE CI                  
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                  
   JOIN WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP_INVOICE_MAP WQI                  
   ON WQI.CU_INVOICE_ID = CI.CU_INVOICE_ID                  
   JOIN WORKFLOW.WORKFLOW_QUEUE_SEARCH_FILTER_GROUP WQG                  
   ON WQG.Workflow_Queue_Search_Filter_Group_Id = wqi.Workflow_Queue_Search_Filter_Group_Id          
   join #temp_User_Group G           
   on g.User_Group_Id = WQG.Workflow_Queue_Search_Filter_Group_Id  
   LEFT JOIN CLIENT CL 
			on CL.CLIENT_ID = CI.CLIENT_ID                 
 LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA                   
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_SERVICE_MONTH SM                  
            ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                  
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID                  
      LEFT OUTER JOIN                  
      DBO.USER_INFO UI                  
            ON UI.QUEUE_ID = CUEX.QUEUE_ID                  
      LEFT JOIN                  
      DBO.CU_EXCEPTION_TYPE CET                  
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE                  
      LEFT JOIN                  
      DBO.ACCOUNT_GROUP AG                  
            ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                  
      LEFT JOIN                  
      (DBO.VENDOR V                  
       INNER JOIN                  
       DBO.ENTITY VT                  
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)                  
            ON V.VENDOR_ID = AG.VENDOR_ID                  
                  
      LEFT JOIN                  
      CORE.CLIENT_HIER_ACCOUNT CHA                  
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                  
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                   
       LEFT JOIN                  
      (DBO.VENDOR AV                  
       INNER JOIN                  
       DBO.ENTITY AVT                  
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)                  
            ON AV.VENDOR_ID = CHA.Account_Vendor_Id                  
      LEFT JOIN                  
      CORE.CLIENT_HIER CH                  
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                   
 LEFT JOIN COUNTRY c                  
            ON CH.COUNTRY_ID = C.COUNTRY_ID                   
   LEFT JOIN state s                  
            ON CH.state_id = s.state_id                     
  LEFT JOIN                  
      DBO.COMMODITY COM                  
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_LABEL INV_LBL                  
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.UBM UBM                  
            ON UBM.UBM_ID = CI.UBM_ID                
  LEFT JOIN dbo.Entity status_type ON status_type.ENTITY_name = cuex.EXCEPTION_STATUS_TYPE                 
  AND STATUS_TYPE.ENTITY_description =' + '''' + 'UBM Exception Status' + ''''          
                  + ' OUTER APPLY                  
      (     SELECT                  
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''          
                  + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC                  
                        INNER JOIN                  
      CORE.CLIENT_HIER_ACCOUNT SAC                  
                              ON SAC.METER_ID = UAC.METER_ID                  
            WHERE       UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''          
                  + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''          
                  + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID                  
     GROUP BY    UAC.ACCOUNT_VENDOR_NAME                  
            FOR XML PATH(' + '''' + '''' + ')) UV(VENDOR_NAME)                  
   WHERE ' + '    CUEX.EXCEPTION_TYPE <> ' + '''' + 'FAILED RECALC' + ''''          
                   
       
     set @sql2 =  CASE          
                        WHEN @Account_Number IS NOT NULL THEN          
                            ' and ( CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG ACCOUNT' + ''''          
                            + ' THEN NULL                  
             WHEN AG.ACCOUNT_GROUP_ID IS NULL                  
                   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                  
             ELSE  COALESCE(AG.GROUP_BILLING_NUMBER, CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                      
 END )   like'                                                                    + '''%' + @Account_Number          
                            + '%'''          
                        ELSE          
                            ''          
                    END + CASE          
                              WHEN @Queue_id IS NOT NULL          
                                   AND @Queue_id = '-1' THEN          
                                  'AND qt.[Entity_Name] = ' + '''' + 'Private' + ''''          
                              WHEN @Queue_id IS NOT NULL          
                                   AND @Queue_id = '-1,0' THEN          
                                  ''          
                              WHEN @Queue_id IS NOT NULL          
                                   AND @Queue_id = '0' THEN          
                                  'AND qt.[Entity_Name] = ' + '''' + 'Public' + ''''          
                    WHEN @Queue_id IS NOT NULL          
                                   AND @Queue_id != '-1'          
                                   AND @Queue_id != '0' THEN          
                                  'and cuex.queue_id in (select user_info_id from #temp_user_info)'          
                              ELSE          
                                  ''          
                          END          
                  + CASE          
                        WHEN @Client IS NOT NULL THEN          
                            'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN CH.CLIENT_NAME                  
           ELSE   coalesce(CL.CLient_Name,CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                   
      END like ' + '''%' + @Client + '%'''          
                        ELSE          
                            ''          
                    END          
                  + CASE          
                        WHEN @Commodity IS NOT NULL THEN          
                            'and ISNULL(COM.COMMODITY_NAME , INV_LBL.COMMODITY) like  ' + '''%' + @Commodity + '%'''          
                        ELSE          
                            ''          
                    END +
					 CASE                
					    WHEN @Data_Source IS NOT NULL AND @Data_Source = 0 THEN   
								'and CI.UBM_Invoice_id is null'
                                WHEN @Data_Source IS NOT NULL AND @data_source <> 0 THEN                
            'and ubm.UBM_ID=' + CAST(@Data_Source AS VARCHAR)                
                                ELSE                
                                    ''                
                            END         
                  + CASE          
                        WHEN @Start_Date_in_CBMS IS NOT NULL THEN          
                            ' and cuex.Date_In_CBMS between ''' + CAST(@Start_Date_in_CBMS AS VARCHAR) + ''''          
                            + ' and ''' + CAST(@End_Date_in_CBMS AS VARCHAR) + ''''          
                        ELSE          
                            ''          
                    END          
                  + CASE          
                        WHEN @Start_Date_in_Queue IS NOT NULL THEN          
                            'and cuex.DATE_IN_QUEUE between ''' + CAST(@Start_Date_in_Queue AS VARCHAR) + ''''          
                            + ' and ''' + CAST(@End_Date_in_Queue AS VARCHAR) + ''''          
                        ELSE          
  ''          
                    END + CASE          
                              WHEN @Filename IS NOT NULL THEN          
                                  'and CUEX.CBMS_DOC_ID like ' + '''%' + @Filename + '%'''          
                              ELSE          
                                  ''          
                          END + CASE          
                                    WHEN @Invoice_ID IS NOT NULL THEN          
                                        'and cuex.CU_INVOICE_ID=' + @Invoice_ID          
                                    ELSE          
                                        ''          
                                END + CASE          
                                          WHEN @Exception_Status IS NOT NULL THEN          
                                              'and STATUS_TYPE.ENTITY_ID=' + CAST(@Exception_Status AS VARCHAR)          
                                          ELSE          
                      ''          
                                      END          
                  + CASE          
                        WHEN @Month IS NOT NULL THEN          
                            'and CASE WHEN CUEX.EXCEPTION_TYPE = ' + '''' + 'WRONG MONTH' + ''''          
                            + 'THEN NULL                  
             WHEN AG.ACCOUNT_GROUP_ID IS NULL and SM.SERVICE_MONTH is not null THEN CONVERT(VARCHAR(10),  SM.SERVICE_MONTH , 101)                  
             when CUEX.SERVICE_MONTH is not null then CUEX.SERVICE_MONTH              
     else null                
    END'          
                            + '=' + '''' + CONVERT(VARCHAR(10), @Month, 101) + ''''          
                        ELSE          
                            ''          
                    END          
                  + CASE          
                        WHEN @Site IS NOT NULL THEN          
                            'and CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                  
                 THEN isnull(CH.SITE_NAME , inv_lbl.site_name)                 
           ELSE  isnull(CUEX.UBM_SITE_NAME  , inv_lbl.site_name)                
      END   like' + '''%' + @Site + '%'''          
                        ELSE          
                            ''          
                    END + CASE          
                              WHEN @Exception_Type IS NOT NULL THEN          
                                  'and cet.cu_exception_type_id in (select exception_type from #temp_Exception_Type)'          
                              ELSE          
                                  ''          
                          END + CASE          
                                    WHEN @City IS NOT NULL THEN          
                                        'and ch.city=' + '''' + @City + ''''          
                                    ELSE          
                                        ''          
                                END + CASE          
                                          WHEN @Priority IS NOT NULL          
  AND @Priority = 1 THEN          
                                              'and    CIA.Is_Priority =1'          
                                                          
                                          ELSE          
                                              ''          
                                      END          
                  + CASE          
                        WHEN @Vendor IS NOT NULL THEN          
                            'and   CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(CHA.Account_Vendor_Name  ,INV_LBL.VENDOR_NAME )                
     WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(V.VENDOR_NAME  ,INV_LBL.VENDOR_NAME )                 
     ELSE INV_LBL.VENDOR_NAME                  
     end like' + '''%' + @Vendor + '%'''          
                        ELSE          
                            ''          
              END          
                  + CASE          
                        WHEN @Vendor_Type IS NOT NULL THEN          
                            'and CASE WHEN  CI.ACCOUNT_GROUP_ID IS NULL THEN isnull(AVT.ENTITY_NAME ,inv_lbl.vendor_type )                 
   WHEN CI.ACCOUNT_GROUP_ID IS NOT NULL THEN isnull(VT.ENTITY_NAME  ,inv_lbl.vendor_type )                 
     ELSE inv_lbl.vendor_type                  
     end like' + '''%' + @Vendor_Type + '%'''          
                        ELSE          
                            ''          
                    END + CASE          
                              WHEN @Country IS NOT NULL THEN          
                                  'and isnull( C.country_name, INV_LBL.COUNTRY) like ' + '''%' + @Country + '%'''          
                              ELSE          
                                  ''          
                          END + CASE          
                                    WHEN @State IS NOT NULL THEN          
                                        'and isnull(S.state_name, INV_LBL.STATE_NAME) like ' + '''%' + @State + '%'''          
                                    ELSE          
                                        ''          
                                END;          
          
           set @SQL = @sql1 + @sql2              
            EXECUTE sp_executesql @SQL;   
     
    
          
          
          
            DELETE FROM #Invoices_List_User          
            WHERE Row_num <> 1;          
          
            IF (@Comments IS NOT NULL)          
            BEGIN          
          
                UPDATE DFG          
                SET DFG.COMMENTS =          
                    (          
                        SELECT TOP (1)          
                               IMAGE_COMMENT          
                        FROM dbo.CU_INVOICE_COMMENT CUI          
                        WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID          
                              AND CUI.IMAGE_COMMENT = @Comments          
                        ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC          
                    )          
                FROM #Invoices_List_User DFG;          
          
                DELETE FROM #Invoices_List_User          
                WHERE COMMENTS IS NULL;          
          
            END;          
          
            ELSE          
            BEGIN          
         
                UPDATE DFG          
                SET DFG.COMMENTS =          
                    (          
                        SELECT TOP (1)          
                               IMAGE_COMMENT          
                        FROM CU_INVOICE_COMMENT CUI          
                        WHERE CUI.CU_INVOICE_ID = DFG.CU_INVOICE_ID          
                        ORDER BY CUI.CU_INVOICE_COMMENT_ID DESC          
                    )          
                FROM #Invoices_List_User DFG;          
          
            END;          
          
        END; --- user group           
          
          
  
  /*  
  
  for system group we dont need any filter condition except generic items hence updating sql4 to null  
  
  */  
  
    
        IF EXISTS (SELECT 1 FROM #System_group)          
        BEGIN          
            SET @SQL3          
                = '   insert #Invoices_List_System                    
     SELECT                     
     ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (select null)    ) AS RN,                     
   CUEX.CU_INVOICE_ID                    
    ,null                             
FROM  DBO.CU_EXCEPTION_DENORM CUEX                  
JOIN dbo.QUEUE q ON q.QUEUE_ID = CUEX.QUEUE_ID                  
                 INNER JOIN dbo.Entity qt ON qt.ENTITY_ID = q.QUEUE_TYPE_ID                  
      JOIN                DBO.CU_INVOICE CI                  
            ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID           
   join  #System_group S1           
   on s1.ubmid = CI.UBM_ID          
   and s1.ubmaccountcode =  CI.UBM_ACCOUNT_CODE           
   and cuex.EXCEPTION_TYPE = S1.Exception_Type               
 LEFT JOIN WORKFLOW.CU_INVOICE_ATTRIBUTE CIA                   
 ON  CIA.CU_INVOICE_ID = cuex.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_SERVICE_MONTH SM                  
        ON SM.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                  
               AND SM.ACCOUNT_ID = CUEX.ACCOUNT_ID                  
      LEFT OUTER JOIN                  
      DBO.USER_INFO UI                  
            ON UI.QUEUE_ID = CUEX.QUEUE_ID                  
      LEFT JOIN                  
      DBO.CU_EXCEPTION_TYPE CET                  
            ON CUEX.EXCEPTION_TYPE = CET.EXCEPTION_TYPE                  
      LEFT JOIN                  
      DBO.ACCOUNT_GROUP AG                  
            ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                  
                  
      LEFT JOIN                  
      (DBO.VENDOR V                  
       INNER JOIN                  
       DBO.ENTITY VT                  
             ON VT.ENTITY_ID = V.VENDOR_TYPE_ID)                  
            ON V.VENDOR_ID = AG.VENDOR_ID                  
                  
      LEFT JOIN                  
      CORE.CLIENT_HIER_ACCOUNT CHA                  
            ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                  
               AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                   
      LEFT JOIN                  
      (DBO.VENDOR AV                  
       INNER JOIN                  
       DBO.ENTITY AVT                  
             ON AVT.ENTITY_ID = AV.VENDOR_TYPE_ID)                  
           ON AV.VENDOR_ID = CHA.Account_Vendor_Id                  
      LEFT JOIN                  
      CORE.CLIENT_HIER CH                  
            ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                  
   LEFT JOIN COUNTRY c                  
            ON CH.COUNTRY_ID = C.COUNTRY_ID                   
   LEFT JOIN state s                  
            ON CH.state_id = s.state_id                   
      LEFT JOIN                  
      DBO.COMMODITY COM                  
            ON COM.COMMODITY_ID = CHA.COMMODITY_ID                  
      LEFT JOIN                  
      DBO.CU_INVOICE_LABEL INV_LBL                  
            ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                  
      LEFT JOIN                  
      DBO.UBM UBM                  
            ON UBM.UBM_ID = CI.UBM_ID                
  LEFT JOIN dbo.Entity status_type ON status_type.ENTITY_name = cuex.EXCEPTION_STATUS_TYPE                 
  AND STATUS_TYPE.ENTITY_description =' + '''' + 'UBM Exception Status' + ''''          
                  + ' OUTER APPLY                  
      (     SELECT                  
                        UAC.ACCOUNT_VENDOR_NAME + ' + '''' + ', ' + ''''          
                  + 'FROM        CORE.CLIENT_HIER_ACCOUNT UAC                  
             INNER JOIN                  
                        CORE.CLIENT_HIER_ACCOUNT SAC                  
                              ON SAC.METER_ID = UAC.METER_ID                  
            WHERE       UAC.ACCOUNT_TYPE = ' + '''' + 'UTILITY' + '''' + 'AND   SAC.ACCOUNT_TYPE = ' + ''''          
                  + 'SUPPLIER' + '''' + 'AND   CHA.ACCOUNT_TYPE = ' + '''' + 'SUPPLIER' + ''''          
                  + 'AND   SAC.ACCOUNT_ID = CHA.ACCOUNT_ID                  
            GROUP BY    UAC.ACCOUNT_VENDOR_NAME                  
            FOR XML PATH(' + '''' + '''' + ')) UV(VENDOR_NAME)                  
   WHERE  CUEX.EXCEPTION_TYPE <> ' + '''' + 'FAILED RECALC' + ''''          
                    
      
            EXECUTE sp_executesql @sql3;          
          
          
            DELETE FROM #Invoices_List_System          
            WHERE row_num <> 1;           
          
        END;          
          
        INSERT @Invoices_Master_List          
        (          
            CU_INVOICE_ID          
        )          
        SELECT CU_INVOICE_ID          
        FROM #Invoices_List_User          
        UNION          
        SELECT CU_INVOICE_ID          
        FROM #Invoices_List_System          
        UNION          
        SELECT cast(Segments AS INT) AS CU_INVOICE_ID          
        FROM dbo.ufn_split(@invoice_list, ',');          
          
        SELECT CU_INVOICE_ID          
        FROM @Invoices_Master_List;          
          
        DROP TABLE #Invoices_List_User;          
        DROP TABLE #Invoices_List_System;          
        DROP TABLE #temp_User_Group;          
        DROP TABLE #temp_user_info;          
  DROP TABLE #temp_Exception_Type;          
  DROP TABLE #System_group;          
          
    END TRY          
    BEGIN CATCH          
          
          
        -- Entry made to the logging SP to capture the errors.                    
        SELECT @ERROR_LINE = error_line(),          
               @ERROR_MESSAGE = error_message();          
          
        INSERT INTO StoredProc_Error_Log          
        (          
            StoredProc_Name,          
            Error_Line,          
            Error_message,          
            Input_Params          
        )          
        VALUES          
        (@PROC_NAME, @ERROR_LINE, @ERROR_MESSAGE, @INPUT_PARAMS);          
          
        EXEC [dbo].[usp_RethrowError] @ERROR_MESSAGE;          
          
    END CATCH;          
END;           
           
 
GO
GRANT EXECUTE ON  [Workflow].[Get_List_Of_Invoices] TO [CBMSApplication]
GO
