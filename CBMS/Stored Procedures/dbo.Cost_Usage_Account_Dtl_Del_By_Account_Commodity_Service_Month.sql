SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Cost_Usage_Account_Dtl_Del_By_Account_Commodity_Service_Month

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Account_Dtl_Del_By_Account_Commodity_Service_Month 1732,57,'2010-01-01'

	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/16/2010	Created

******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Del_By_Account_Commodity_Service_Month
	@Account_id		INT
	,@Commodity_id	INT
	,@Service_Month	DATE
AS
BEGIN

	SET NOCOUNT ON

	DELETE
		cua
	FROM
		dbo.Cost_Usage_Account_Dtl cua
		JOIN dbo.Bucket_Master bm
			ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
	WHERE
		cua.ACCOUNT_ID = @Account_id
		AND cua.Service_Month = @Service_Month
		AND bm.Commodity_Id = @Commodity_id

END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Del_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
