SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








--select * from client where client_name like '%azz%'

--EXEC BUDGET_GET_REVISED_BUDGET_NAME_P -1,-1,201


CREATE     PROCEDURE dbo.BUDGET_GET_REVISED_BUDGET_NAME_P
	@userId varchar(10),
	@sessionId varchar(20),
        @clientId int,
	@budgetId int
	AS
	begin
	set nocount on

       		select budget_name 
          	from budget 
		where client_id=@clientId	
		     and budget_id not in(@budgetId)
	/*declare @revised_id int
	select @revised_id=budget.original_budget_id
		from  budget
		where budget.budget_id = @budgetId
		
		select budget.budget_name
		from  budget
		where  budget.original_budget_id=@revised_id*/
	end












GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_REVISED_BUDGET_NAME_P] TO [CBMSApplication]
GO
