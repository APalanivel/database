SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Comment_Del]  

DESCRIPTION: 
	It Deletes Comment for Selected Comment Id.
	Invoices backed out as Not reported should be removed along with the account delete, thus requires deletion of Cost_Usage_comment_Map and this Comment
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Comment_ID			INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Comment_Del 142067
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Comment_Del
    (
      @Comment_ID INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Comment 
	WHERE
		Comment_ID = @Comment_ID

END
GO
GRANT EXECUTE ON  [dbo].[Comment_Del] TO [CBMSApplication]
GO
