SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME:      
 dbo.cbmsMarketIntel_Save    
     
DESCRIPTION:      
 Used to insert data into market_intel table    
      
INPUT PARAMETERS:      
 Name      DataType Default Description      
------------------------------------------------------------      
@publish_date  DATETIME    
@market_intel_title VARCHAR(250)    
@market_intel  TEXT    
@publication_cd  INT    
                
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      
  
USAGE EXAMPLES:      
------------------------------------------------------------    
     
 EXEC dbo.cbmsMarketIntel_Save -1,500,'2009-05-29 10:21:00','Rate Change Notification - Pepco of D.C.','  
    
    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
GB   Geetansu Behera      
CMH  Chad Hattabaugh       
HG   Hari    
SKA  Shobhit Kr Agrawal  
     
MODIFICATIONS       
Initials Date  Modification      
------------------------------------------------------------      
GB				Created    
SKA  08/07/09	Modified as per coding standard  
SS	 14/07/09	Removed additional select statement that returns value of Scope identity
GB   8 Aug 2009	Removed the Select statement in last from the table
    
******/    
    
    
CREATE PROCEDURE [dbo].[cbmsMarketIntel_Save]   
(    
  @publish_date DATETIME,    
  @market_intel_title VARCHAR(250),    
  @market_intel TEXT,    
  @publication_cd INT    
)      
AS    
BEGIN    
    
	 SET NOCOUNT ON      

	 DECLARE @Market_Intel_Id INT      

	 INSERT INTO dbo.MARKET_INTEL(   
		  PUBLISH_DATE,   
		  MARKET_INTEL_TITLE,   
		  MARKET_INTEL,   
		  PUBLICATION_CD)      
	 VALUES(  
		  @publish_date,   
		  @market_intel_title,   
		  @market_intel,   
		  @publication_cd)      

	 SET @Market_Intel_Id = SCOPE_IDENTITY()      

	 SELECT @Market_Intel_Id AS MARKET_INTEL_ID

      
END 
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntel_Save] TO [CBMSApplication]
GO
