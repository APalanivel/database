SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_ONBOARD_CLIENT_LIST
@userId varchar(10),
@sessionId varchar(20)
AS
	set nocount on
	select distinct client_id from rm_onboard_client
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_CLIENT_LIST] TO [CBMSApplication]
GO
