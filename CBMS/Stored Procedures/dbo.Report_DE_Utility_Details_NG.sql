SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******  
NAME:  
CBMS.dbo.Report_Utility_Details_NG

      
DESCRIPTION:   INPUT PARAMETERS:  
Name    DataType  Default Description  
------------------------------------------------------------  


OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  


USAGE EXAMPLES:  
------------------------------------------------------------  
EXEC dbo.Report_Utility_Details_NG 

AUTHOR INITIALS:  
Initials   Name  
-----------------------------------------------------------  
AKR       Ashok Kumar Raju

MODIFICATIONS   
Initials Date		Modification  
------------------------------------------------------------  
 AKR	2012-08-01  Create the SP
******/  
  
CREATE PROCEDURE [dbo].[Report_DE_Utility_Details_NG]
      ( 
       @Utility VARCHAR(MAX)
      ,@Country VARCHAR(MAX) )
AS 
BEGIN            
      SET NOCOUNT ON ;   
      
      DECLARE
            @Commodity_Id_Ng INT
           ,@Commodity_Id_EP INT
           ,@Vendor_Type_Id INT
      DECLARE @Utility_List TABLE ( Utility_ID INT )
      DECLARE @Country_List TABLE ( Country_Id INT )
      
      SELECT
            @Commodity_Id_EP = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Electric Power'
      
      SELECT
            @Commodity_Id_Ng = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = 'Natural Gas'
      SELECT
            @Vendor_Type_Id = e.Entity_Id
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = 'Utility'
            AND e.ENTITY_DESCRIPTION = 'Vendor'
            
      INSERT      INTO @Utility_List
                  ( 
                   Utility_ID )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Utility, ',') us
        
        
        
      INSERT      INTO @Country_List
                  ( 
                   Country_Id )
                  SELECT
                        cnt.Segments
                  FROM
                        dbo.ufn_split(@Country, ',') cnt
      
      
      SELECT
            Vendor_Name [Utility]
           ,STATE_NAME [State]
           ,COUNTRY_NAME [Country]
           ,[Electric Power] = case WHEN v.VENDOR_ID IN ( SELECT
                                                            vendor_id
                                                          FROM
                                                            VENDOR_COMMODITY_MAP
                                                          WHERE
                                                            Commodity_type_Id = @Commodity_Id_EP ) THEN 'Yes'
                                    ELSE 'No'
                               END
           ,[Natural Gas] = case WHEN v.VENDOR_ID IN ( SELECT
                                                            VENDOR_id
                                                       FROM
                                                            VENDOR_COMMODITY_MAP
                                                       WHERE
                                                            Commodity_type_Id = @Commodity_Id_NG ) THEN 'Yes'
                                 ELSE 'No'
                            END
           ,c.Code_Value [NG Ind Option To Transport]
           ,c1.Code_Value [NG Com Option To Transport]
           ,cast(comm.Comment_Text AS VARCHAR(4000)) [NG Option to Transport Comments]
           ,max(case WHEN q.Question_Label = 'Industrial' THEN q.Utility_Volume_Dsc
                END) [NG Ind Volume Requirement]
           ,max(case WHEN q.Question_Label = 'Industrial' THEN q.Code_Value
                END) [NG Ind UOM]
           ,max(case WHEN q.Question_Label = 'Industrial' THEN q.Time_Period
                END) [NG Ind Time Period]
           ,max(case WHEN q.Question_Label = 'Industrial' THEN q.Time_Req
                END) [NG Ind Time Requirements]
           ,max(case WHEN q.Question_Label = 'Industrial' THEN q.Other_Req
                END) [NG Ind Other Requirements or stipulations to transport]
           ,max(case WHEN q.Question_Label = 'Commercial' THEN q.Utility_Volume_Dsc
                END) [NG Com Volume Requirement]
           ,max(case WHEN q.Question_Label = 'Commercial' THEN q.Code_Value
                END) [NG Com UOM]
           ,max(case WHEN q.Question_Label = 'Commercial' THEN q.Time_Period
                END) [NG Com Time Period]
           ,max(case WHEN q.Question_Label = 'Commercial' THEN q.Time_Req
                END) [NG Com Time Requirements]
           ,max(case WHEN q.Question_Label = 'Commercial' THEN q.Other_Req
                END) [NG Com Other Requirements or stipulations to transport]
           ,max(case WHEN q.Question_Label = 'Utility Restrictions' THEN q.Utility_Volume_Dsc
                END) [NG Utility Restrictions Volume Requirement]
           ,max(case WHEN q.Question_Label = 'Utility Restrictions' THEN q.Code_Value
                END) [NG Utility Restrictions UOM]
           ,max(case WHEN q.Question_Label = 'Utility Restrictions' THEN q.Time_Period
                END) [NG Utility Restrictions Time Period]
           ,max(case WHEN q.Question_Label = 'Utility Restrictions' THEN q.Time_Req
                END) [NG Utility Restrictions Time Requirements]
           ,max(case WHEN q.Question_Label = 'Utility Restrictions' THEN q.Other_Req
                END) [NG Utility Restrictions Other Requirements or stipulations to transport]
           ,max(case WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Utility_Volume_Dsc
                END) [NG Supplier Restrictions Volume Requirement]
           ,max(case WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Code_Value
                END) [NG Supplier Restrictions UOM]
           ,max(case WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Time_Period
                END) [NG Supplier Restrictions Time Period]
           ,max(case WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Time_Req
                END) [NG Supplier Restrictions Time Requirements]
           ,max(case WHEN q.Question_Label = 'Supplier Restrictions' THEN q.Other_Req
                END) [NG Supplier Restrictions Other Requirements or stipulations to transport]
           ,max(case WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN q4.[Ind Telemetering]
                END) [NG Ind Telemetering Required]
           ,max(case WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN q4.[Com Telemetering]
                END) [NG Com Telemetering Required]
           ,max(case WHEN q4.Question_Label = 'Is telemetering required to transport?' THEN cast(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [NG Telemetering Required Comments]
           ,max(case WHEN q4.Question_Label = 'Is there an additional cost?' THEN q4.[Ind Telemetering]
                END) [NG Ind Telemetering Additional Cost]
           ,max(case WHEN q4.Question_Label = 'Is there an additional cost?' THEN q4.[Com Telemetering]
                END) [NG Com Telemetering Additional Cost]
           ,max(case WHEN q4.Question_Label = 'Is there an additional cost?' THEN cast(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [NG Telemetering Additional Cost Comments]
           ,max(case WHEN q4.Question_Label = 'How are meter readings obtained? (website, LDC software, etc.)' THEN cast(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [NG Telemetering Meter Read Comments]
           ,max(case WHEN q4.Question_Label = 'Online bill information availability?' THEN q4.[Ind Telemetering]
                END) [NG Ind Telemetering Online Bill]
           ,max(case WHEN q4.Question_Label = 'Online bill information availability?' THEN q4.[Com Telemetering]
                END) [NG Com Telemetering Online Bill]
           ,max(case WHEN q4.Question_Label = 'Online bill information availability?' THEN cast(q4.[Telemetering Comments] AS VARCHAR(4000))
                END) [NG Telemetering Online Bill Comments]
           ,max(case WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN q8.[Industrial]
                END) AS [NG Ind Firm & Int Transport]
           ,max(case WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN q8.[Commercial]
                END) AS [NG Com Firm & Int Transport]
           ,max(case WHEN q8.Question_Label = 'Does utility offer firm & interruptible transport rates?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Firm & Int Transport Comments]
           ,max(case WHEN q8.Question_Label = 'What are penalties if customer does not interrupt when called?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Penalties Comments]
           ,max(case WHEN q8.Question_Label = 'Is alternate fuel capabilities required for interruptible transport?' THEN q8.[Industrial]
                END) AS [NG Ind Int Alt Fuel Requirement]
           ,max(case WHEN q8.Question_Label = 'Is alternate fuel capabilities required for interruptible transport?' THEN q8.[Commercial]
                END) AS [NG Com Int Alt Fuel Requirement]
           ,max(case WHEN q8.Question_Label = 'Is alternate fuel capabilities required for interruptible transport?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Int Alt Fuel Requirement Comments]
           ,max(case WHEN q8.Question_Label = 'Ability to negotiate competitive costs based upon alternate fuel capability' THEN q8.[Industrial]
                END) AS [NG Ind Alt Fuel Negotiation]
           ,max(case WHEN q8.Question_Label = 'Ability to negotiate competitive costs based upon alternate fuel capability' THEN q8.[Commercial]
                END) AS [NG Com Alt Fuel Negotiation]
           ,max(case WHEN q8.Question_Label = 'Ability to negotiate competitive costs based upon alternate fuel capability' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Alt Fuel Negotiation Comments]
           ,max(case WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN q8.[Industrial]
                END) AS [NG Ind Monthly Switching]
           ,max(case WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN q8.[Commercial]
                END) AS [NG Com Monthly Switching]
           ,max(case WHEN q8.Question_Label = 'Does utility allow customers to switch between tariff and transport on a monthly basis?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Monthly Switching Comments]
           ,max(case WHEN q8.Question_Label = 'If this is rate specific, what rate schedule applies to this option?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Monthly Switching Rate Schedule Comments]
           ,max(case WHEN q8.Question_Label = 'Fuel Retention' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Fuel Retention Comments]
           ,max(case WHEN q8.Question_Label = 'Btu Factor' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Btu Factor Comments]
           ,max(case WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN q8.[Industrial]
                END) AS [NG Ind Hedge Ability]
           ,max(case WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN q8.[Commercial]
                END) AS [NG Com Hedge Ability]
           ,max(case WHEN q8.Question_Label = 'Ability to hedge through the utility?' THEN cast(q8.[Comments] AS VARCHAR(4000))
                END) [NG Hedge Ability Comments]
           ,max(case WHEN q14.Question_Label = 'Nominations' THEN cast(q14.[Comments] AS VARCHAR(4000))
                END) [NG Nominations Comments]
           ,max(case WHEN q14.Question_Label = 'Rate Changes' THEN cast(q14.[Comments] AS VARCHAR(4000))
                END) [NG Rate Changes Comments]
           ,max(case WHEN q14.Question_Label = 'Marketer Changes' THEN cast(q14.[Comments] AS VARCHAR(4000))
                END) [NG Marketer Changes Comments]
           ,max(case WHEN q14.Question_Label = 'Contract Changes' THEN cast(q14.[Comments] AS VARCHAR(4000))
                END) [NG Contract Changes Comments]
      FROM
            dbo.VENDOR v
            JOIN @Utility_List ul
                  ON v.VENDOR_ID = ul.Utility_ID
            JOIN dbo.VENDOR_STATE_MAP vsm
                  ON vsm.VENDOR_ID = v.VENDOR_ID
            JOIN dbo.STATE s
                  ON S.STATE_ID = vsm.STATE_ID
            JOIN dbo.COUNTRY ctry
                  ON ctry.COUNTRY_ID = s.COUNTRY_ID
            JOIN @Country_List cl
                  ON ctry.COUNTRY_ID = cl.COUNTRY_ID
            JOIN dbo.VENDOR_COMMODITY_MAP vcm
                  ON vcm.VENDOR_ID = v.VENDOR_ID
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN dbo.Utility_Dtl_Transport udt
                  ON udt.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
            LEFT JOIN dbo.Code c
                  ON c.Code_Id = udt.Industrial_Flag_Cd
            LEFT JOIN dbo.Code c1
                  ON c1.Code_Id = udt.Commercial_Flag_Cd
            LEFT JOIN dbo.Comment comm
                  ON comm.Comment_ID = udt.Comment_ID
  --Industrial
            LEFT JOIN ( SELECT
                              uv.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,uv.Other_Req
                             ,uv.Utility_Volume_Dsc
                             ,c1.Code_Value
                             ,qcm.Commodity_Id
                             ,uv.Time_Period
                             ,uv.Time_Req
                        FROM
                              dbo.Utility_Dtl_Volume_Requirement uv
                              LEFT JOIN dbo.Code c1
                                    ON c1.Code_Id = uv.Utility_UOM_Cd
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = uv.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                        WHERE
                              usq.Question_Label IN ( 'industrial', 'Commercial', 'Supplier Restrictions', 'Utility Restrictions' ) ) q
                  ON q.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              ut.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,c.Code_Value [Com Telemetering]
                             ,c1.Code_Value [Ind Telemetering]
                             ,comm.Comment_Text [Telemetering Comments]
                        FROM
                              dbo.Utility_Dtl_Telemetering ut
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = ut.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              JOIN dbo.Code c
                                    ON c.Code_Id = ut.Commercial_Flag_Cd
                              JOIN dbo.Code c1
                                    ON c1.Code_Id = ut.Industrial_Flag_Cd
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = ut.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Is telemetering required to transport?', 'Is there an additional cost?', 'How are meter readings obtained? (website, LDC software, etc.)', 'Online bill information availability?' ) ) q4
                  ON q4.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q4.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              ul.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,c.Code_Value [Commercial]
                             ,c1.Code_Value [Industrial]
                             ,comm.Comment_Text [Comments]
                        FROM
                              dbo.Utility_Dtl_Service_Level ul
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = ul.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              JOIN dbo.Code c
                                    ON c.Code_Id = ul.Commercial_Flag_Cd
                              JOIN dbo.Code c1
                                    ON c1.Code_Id = ul.Industrial_Flag_Cd
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = ul.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Does utility offer firm & interruptible transport rates?', 'What are penalties if customer does not interrupt when called?', 'Is alternate fuel capabilities required for interruptible transport?', 'Ability to negotiate competitive costs based upon alternate fuel capability', 'Does utility allow customers to switch between tariff and transport on a monthly basis?', 'If this is rate specific, what rate schedule applies to this option?', 'Fuel Retention', 'Btu Factor', 'Ability to hedge through the utility?' ) ) q8
                  ON q8.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q8.Commodity_Id = vcm.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              un.Vendor_Commodity_Map_Id
                             ,usq.Question_Label
                             ,qcm.Commodity_Id
                             ,comm.Comment_Text [Comments]
                        FROM
                              dbo.Utility_Dtl_Notification un
                              JOIN dbo.Question_Commodity_Map qcm
                                    ON qcm.Question_Commodity_Map_Id = un.Question_Commodity_Map_Id
                              JOIN dbo.Utility_Summary_Question usq
                                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                              LEFT JOIN dbo.Comment comm
                                    ON comm.Comment_ID = un.Comment_ID
                        WHERE
                              usq.Question_Label IN ( 'Nominations', 'Rate Changes', 'Marketer Changes', 'Contract Changes' ) ) q14
                  ON q14.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                     AND q14.Commodity_Id = vcm.COMMODITY_TYPE_ID
      WHERE
            vcm.COMMODITY_TYPE_ID = @Commodity_Id_Ng
            AND v.VENDOR_TYPE_ID = @Vendor_Type_Id
      GROUP BY
            Vendor_Name
           ,STATE_NAME
           ,COUNTRY_NAME
           ,v.VENDOR_ID
           ,Comm.Comment_Text
           ,c.Code_Value
           ,c1.Code_Value
      ORDER BY
            Utility
           ,[State]
           ,COUNTRY
         
     
END


;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Details_NG] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Utility_Details_NG] TO [CBMSApplication]
GO
