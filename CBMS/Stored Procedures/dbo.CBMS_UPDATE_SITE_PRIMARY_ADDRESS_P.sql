SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_UPDATE_SITE_PRIMARY_ADDRESS_P]


DESCRIPTION: Updates information for a site

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@addressId              int,
@siteId                 int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR		07/06/2009	modified for the sitegroup division table split and implementation of SV replication     	
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE dbo.CBMS_UPDATE_SITE_PRIMARY_ADDRESS_P
   @addressId int,
   @siteId int

as


update site 
set	primary_address_id = @addressId
where	site_id = @siteId
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_SITE_PRIMARY_ADDRESS_P] TO [CBMSApplication]
GO
