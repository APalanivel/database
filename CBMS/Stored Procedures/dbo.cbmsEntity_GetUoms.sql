SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsEntity_GetUoms

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[cbmsEntity_GetUoms]
	( @MyAccountId int)
AS
BEGIN

   select entity_id
		, entity_name
		, entity_type
	     from entity
	    where entity_id in (
			  7	
			, 8	
			, 9	
			, 10	
			, 11	
			, 12	
			, 15	
			, 16	
			, 17	
			, 18	
			, 20	
			, 21	
			, 22	
			, 23	
			, 24	
			, 25	
			, 26	
			, 28	
			)
	  order by entity_type asc



END
GO
GRANT EXECUTE ON  [dbo].[cbmsEntity_GetUoms] TO [CBMSApplication]
GO
