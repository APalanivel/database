SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[cbmsBudgetDetailComments_Save]
	( @MyAccountID int = null
	, @budget_detail_comments_owner_map_id int = null
	, @budget_id int = null
	, @budget_detail_id int = null
	, @owner_id int = null
	, @owner_type_id int = null
	, @budget_detail_comments varchar(2000) = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @budget_detail_comments_owner_map_id


	if @this_id is null
	begin

		insert into budget_detail_comments_owner_map
			( budget_id
			, budget_Detail_id
			, owner_id
			, owner_type_id
			, budget_detail_comments
			)
		 values
			( @budget_id
			, @budget_Detail_id
			, @owner_id
			, @owner_type_id
			, @budget_detail_comments
			)

		set @this_id = @@IDENTITY

	end
	else
	begin

		   update budget_detail_comments_owner_map
		      set budget_id = @budget_id
			, budget_detail_id = @budget_Detail_id
			, owner_id = owner_id
			, owner_type_id = @owner_type_id	
			, budget_Detail_comments = @budget_detail_comments
		    where budget_detail_comments_owner_map_id = @this_id

	end

	set nocount off

	exec cbmsBudgetDetailComments_Get @this_id


END


SET QUOTED_IDENTIFIER ON
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetailComments_Save] TO [CBMSApplication]
GO
