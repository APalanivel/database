
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
      dbo.GET_DETAILS_P
   
 DESCRIPTION:   
      Gets the Details for a given client.
      
 INPUT PARAMETERS:  
 Name			DataType	Default Description  
------------------------------------------------------------  
 @client_id      int                     


 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------
 USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.GET_DETAILS_P 235
	EXEC dbo.GET_DETAILS_P 11231

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------ 
 AKR		Ashok Kumar Raju
 RR			Raghu Reddy
 

 MODIFICATIONS   
 Initials	Date		Modification
------------------------------------------------------------
 AKR        2012-09-25  Modified to standards and added the Analyst_Mapping_Code
 RR			2014-06-25	MAINT-2824 Added new column Variance_Exception_Notice_Contact_Email to select list
 RR			2017-01-20	Contract placeholder - CP-4 Added Client_Hier_Id to select list

******/
CREATE  PROCEDURE [dbo].[GET_DETAILS_P] ( @client_id INT )
AS 
BEGIN
      SET nocount ON

      SELECT
            client.CLIENT_TYPE_ID
           ,ISNULL(client.UBM_SERVICE_ID, 0)
           ,client.REPORT_FREQUENCY_TYPE_ID
           ,client.CLIENT_NAME
           ,client.FISCALYEAR_STARTMONTH_TYPE_ID
           ,client.UBM_START_DATE
           ,client.DSM_STRATEGY
           ,client.IS_SEP_ISSUED
           ,client.SEP_ISSUE_DATE
           ,client.Not_Managed
           ,division.TRIGGER_RIGHTS
           ,client.RISK_PROFILE_TYPE_ID
           ,client.Analyst_Mapping_Cd
           ,client.Variance_Exception_Notice_Contact_Email
           ,ch.Client_Hier_Id
      FROM
            dbo.CLIENT client
            INNER JOIN dbo.DIVISION division
                  ON division.client_id = client.client_id
            INNER JOIN Core.Client_Hier ch
                  ON client.CLIENT_ID = ch.Client_Id
      WHERE
            client.CLIENT_ID = @client_id
            AND ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
	
END;

;

;
GO



GRANT EXECUTE ON  [dbo].[GET_DETAILS_P] TO [CBMSApplication]
GO
