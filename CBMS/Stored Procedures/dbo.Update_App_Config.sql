SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************      
NAME : dbo.Update_App_Config   
     
DESCRIPTION:   
Stored Procedure is used to update application config values
     
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   
   
      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
--------------------------------------------------------------------      

  
  USAGE EXAMPLES:      
--------------------------------------------------------------------      
     
    
AUTHOR INITIALS:      
 Initials Name      
-------------------------------------------------------------------      
 KVK Vinay Kumar K
     
 MODIFICATIONS       
 Initials Date  Modification      
--------------------------------------------------------------------
******************************************************************************************************/  

CREATE PROCEDURE [dbo].[Update_App_Config]
      (
       @AppId INT
      ,@AppConfigCode VARCHAR(255)
      ,@ConfigVal VARCHAR(255) )
AS 
BEGIN  
  
      SET NOCOUNT ON ; 
	
      UPDATE
            dbo.App_Config
      SET   
            App_Config_Value = @ConfigVal
      WHERE
            App_Id = @AppId
            AND App_Config_Cd = @AppConfigCode
	
END


GO
GRANT EXECUTE ON  [dbo].[Update_App_Config] TO [CBMSApplication]
GO
