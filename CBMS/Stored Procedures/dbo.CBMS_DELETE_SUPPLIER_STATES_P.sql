SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_DELETE_SUPPLIER_STATES_P]


DESCRIPTION: Deeltes data from the VENDOR_STATE_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_id              int      
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec [CBMS_DELETE_SUPPLIER_STATES_P] 3051


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[CBMS_DELETE_SUPPLIER_STATES_P] 

@vendor_id int

AS


DELETE FROM 
	VENDOR_STATE_MAP 
WHERE 
	VENDOR_ID = @vendor_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_DELETE_SUPPLIER_STATES_P] TO [CBMSApplication]
GO
