SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_FEED_FREQUENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE       PROCEDURE dbo.UBM_GET_FEED_FREQUENCY_P
@userId varchar(10),
@sessionId varchar(20)

AS
set nocount on
	SELECT 
		UBM_FEED_FREQUENCY_ID, 
		CLIENT_NAME,
		entity_name FREQUENCY,
		UBM_NAME, 
		UBM_CLIENT_ID 
	FROM 
		UBM_FEED_FREQUENCY feed, 
		UBM ubm, 
		ENTITY entity
	WHERE 
		feed.ubm_id=ubm.ubm_id 
		and entity.entity_id=feed.frequency_type_id 

	ORDER BY
		CLIENT_NAME
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_FEED_FREQUENCY_P] TO [CBMSApplication]
GO
