SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [workflow].[Cu_Invoice_Workflow_Action_Batch__Dtl_Status_Upd]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @Cu_Invoice_Workflow_Action_Batch_Id INT    
     ,@Cu_Invoice_Workflow_Action_Batch_Dtl_Id INT    
     ,@Cu_Invoice_Id INT    
     ,@Status_Cd INT    
     ,@Error_Msg VARCHAR = NULL      
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKP   Arunkumar Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AKP    Sep-2019  Created  
  
******/  
  
CREATE  PROCEDURE [Workflow].[Cu_Invoice_Workflow_Action_Batch__Dtl_Status_Upd]    
     (    
      @Cu_Invoice_Workflow_Action_Batch_Id INT    
     ,@Cu_Invoice_Workflow_Action_Batch_Dtl_Id INT    
     ,@Cu_Invoice_Id INT    
     ,@Status_Cd INT    
     ,@Error_Msg VARCHAR = NULL    
     )    
AS    
    BEGIN    
        SET NOCOUNT ON;    
        UPDATE    
             CIWABD  SET    
                     CIWABD.Error_Msg = @Error_Msg    
            ,CIWABD.Status_Cd = @Status_Cd    
            ,CIWABD.Last_Change_Ts = GETDATE()    
                     FROM    
                     Workflow.Cu_Invoice_Workflow_Action_Batch_Dtl  CIWABD    
        WHERE     
              CIWABD.Cu_Invoice_Workflow_Action_Batch_Id = @Cu_Invoice_Workflow_Action_Batch_Id AND    
              CIWABD.Cu_Invoice_Workflow_Action_Batch_Dtl_Id = @Cu_Invoice_Workflow_Action_Batch_Dtl_Id AND    
              CIWABD.Cu_Invoice_Id = @Cu_Invoice_Id    
    END;    
    

GO
GRANT EXECUTE ON  [Workflow].[Cu_Invoice_Workflow_Action_Batch__Dtl_Status_Upd] TO [CBMSApplication]
GO
