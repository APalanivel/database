
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.[Cu_Invoice_Recalc_Response_Net_Amount]             
                  
Description:                  
                         
                  
 Input Parameters:                  
    Name				DataType		Default				Description                    
----------------------------------------------------------------------------------------                    
  @Cu_Invoice_Id		INT     
  @Account_Id			INT    
  @Commodity_Id			INT    
            
 Output Parameters:                        
    Name				DataType		Default				Description                    
----------------------------------------------------------------------------------------                    
       
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------                    

 BEGIN TRAN      
     
 EXEC [Cu_Invoice_Recalc_Response_Net_Amount] 23154286,55263,291   
 EXEC [Cu_Invoice_Recalc_Response_Net_Amount] 29616330, 55262, 290

 ROLLBACK TRAN                 
                 
Author Initials:                  
    Initials		Name                  
----------------------------------------------------------------------------------------                    
	SP				SASI PURNIMA   
 Modifications:                  
    Initials        Date		Modification                  
----------------------------------------------------------------------------------------                    
    SP				2016-09-09  Created For AS400-II.             
                 
******/     
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_Net_Amount]
      (
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT )
AS
BEGIN

      SET NOCOUNT ON;    

      DECLARE @Late_Fee_Bucket_Master_Id INT; 
     
      SELECT
            @Late_Fee_Bucket_Master_Id = Bucket_Master_Id
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
      WHERE
            bm.Bucket_Name = 'Late Fees'
            AND bm.Commodity_Id = @Commodity_Id
            AND bt.Code_Value = 'Charge';

      SET @Late_Fee_Bucket_Master_Id = ISNULL(@Late_Fee_Bucket_Master_Id, 0);
          
      SELECT
            SUM(Net_Amount) AS Net_Amount
      FROM
            dbo.Cu_Invoice_Recalc_Response cirr
            INNER JOIN dbo.RECALC_HEADER rh
                  ON cirr.Recalc_Header_Id = rh.RECALC_HEADER_ID
      WHERE
            rh.CU_INVOICE_ID = @Cu_Invoice_Id
            AND rh.Commodity_Id = @Commodity_Id
            AND rh.ACCOUNT_ID = @Account_Id
            AND cirr.Bucket_Master_Id <> @Late_Fee_Bucket_Master_Id
      GROUP BY
            rh.CU_INVOICE_ID
           ,rh.Commodity_Id
           ,rh.ACCOUNT_ID;  
            
END;
;
GO

GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_Net_Amount] TO [CBMSApplication]
GO
