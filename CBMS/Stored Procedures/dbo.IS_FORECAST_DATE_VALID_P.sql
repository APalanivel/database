SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.IS_FORECAST_DATE_VALID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@forecastDate  	datetime  	          	
	@forecastType  	int       	          	
	@forecastTypeName	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.IS_FORECAST_DATE_VALID_P  

@userId varchar(10),
@sessionId varchar(20),
@forecastDate Datetime,
@forecastType int, 
@forecastTypeName varchar(200)


AS
set nocount on
	SELECT FORECAST_FROM_DATE FROM RM_FORECAST
	WHERE FORECAST_FROM_DATE>=@forecastDate 
		and forecast_type_id=(select  ENTITY_ID FROM ENTITY WHERE ENTITY_TYPE=@forecastType AND 
	ENTITY_NAME=@forecastTypeName)
GO
GRANT EXECUTE ON  [dbo].[IS_FORECAST_DATE_VALID_P] TO [CBMSApplication]
GO
