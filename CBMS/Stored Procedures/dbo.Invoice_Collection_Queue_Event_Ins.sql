SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Queue_Event_Ins       
              
Description:              
			This sproc is used in batch process to fill Invoice Collection Queue
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	                 
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Invoice_Collection_Queue_Event_Ins 1,'ICR Created',49 
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-27		Created For Invoice_Collection.         
             
******/
      
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Event_Ins]
      ( 
       @Invoice_Collection_Queue_Id INT
      ,@Event_Desc NVARCHAR(MAX)
      ,@User_Info_Id INT )
AS 
BEGIN
      INSERT      INTO dbo.Invoice_Collection_Queue_Event
                  ( 
                   Invoice_Collection_Queue_Id
                  ,Event_Desc
                  ,Event_By_User_Id )
                  SELECT
                        @Invoice_Collection_Queue_Id
                       ,@Event_Desc
                       ,@User_Info_Id
      
END


;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Event_Ins] TO [CBMSApplication]
GO
