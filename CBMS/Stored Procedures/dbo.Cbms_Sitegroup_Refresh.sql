SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.Cbms_Sitegroup_Refresh
 
DESCRIPTION:   
 
	This procedure used to refresh CBMS smart groups
 
INPUT PARAMETERS:  
	Name			DataType	Default		Description  
------------------------------------------------------------    
	
	
OUTPUT PARAMETERS:  
	Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
	

	EXEC dbo.Cbms_Sitegroup_Refresh
   

AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy    
 
MODIFICATIONS   
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			07-06-2019	Created for GRM.
  
******/

CREATE PROC [dbo].[Cbms_Sitegroup_Refresh]
      ( 
       @Cbms_Sitegroup_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Tbl_Cbms_Sitegroup AS TABLE
            ( 
             Cbms_Sitegroup_Id INT )
             
      DECLARE @Tbl_Cbms_Sitegroup_Sites AS TABLE
            ( 
             Site_Id INT
            ,Site_Name VARCHAR(200) )
             
      DECLARE @Tvp_RM_Group_Dtl AS dbo.Tvp_RM_Group_Dtl
             
      DECLARE
            @RM_Group_Participant_Site_Cd INT
           ,@System_User_Info_Id INT
           ,@Refesh_Cbms_Sitegroup_Id INT
           ,@Total_Row_Count INT
           ,@Row_Counter INT
           ,@RM_Group_Participant_Contract_Cd INT
           
      DECLARE @Cbms_Smart_Sitegroup_Lsit TABLE
            ( 
             Cbms_Sitegroup_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )
            
      DECLARE @Cbms_Contract_Sitegroup_Lsit TABLE
            ( 
             Cbms_Sitegroup_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )
      
      SELECT
            @System_User_Info_Id = USER_INFO_ID
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = 'Conversion'
             
      SELECT
            @RM_Group_Participant_Site_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'Site'
            AND cs.Codeset_Name = 'RM Group Participants'
            
            
      SELECT
            @RM_Group_Participant_Contract_Cd = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cd.Code_Value = 'Contract'
            AND cs.Codeset_Name = 'RM Group Participants'
            
      -----------Smart group refresh
      INSERT      INTO @Cbms_Smart_Sitegroup_Lsit
                  ( 
                   Cbms_Sitegroup_Id )
                  SELECT
                        sgrp.Cbms_Sitegroup_Id
                  FROM
                        dbo.Cbms_Sitegroup sgrp
                        INNER JOIN dbo.Code cd
                              ON sgrp.Group_Type_Cd = cd.Code_Id
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cs.Codeset_Id
                  WHERE
                        cs.Codeset_Name = 'Risk Management Groups'
                        AND cd.Code_Value = 'RM Smart Group'
                        AND ( @Cbms_Sitegroup_Id IS NULL
                              OR sgrp.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id )
                  GROUP BY
                        sgrp.Cbms_Sitegroup_Id
                        
            
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Cbms_Smart_Sitegroup_Lsit      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count ) 
            BEGIN      
                  SELECT
                        @Refesh_Cbms_Sitegroup_Id = Cbms_Sitegroup_Id
                  FROM
                        @Cbms_Smart_Sitegroup_Lsit
                  WHERE
                        Row_Num = @Row_Counter      
      
                  
                        
                  INSERT      INTO @Tbl_Cbms_Sitegroup_Sites
                              ( 
                               Site_Id
                              ,Site_Name )
                              EXEC dbo.Cbms_SmartGroup_Site_Sel 
                                    @Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id               
                        
                  INSERT      INTO @Tvp_RM_Group_Dtl --RM_Group_Participant_Id,RM_Group_Participant_Type_Cd
                              SELECT
                                    Site_Id AS RM_Group_Participant_Id
                                   ,@RM_Group_Participant_Site_Cd AS RM_Group_Participant_Type_Cd
                              FROM
                                    @Tbl_Cbms_Sitegroup_Sites
                        
                  EXEC dbo.Cbms_Sitegroup_Participant_Merge 
                        @Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                       ,@Tvp_RM_Group_Dtl = @Tvp_RM_Group_Dtl
                       ,@Include_New_Contract_Extensions = 0
                       ,@User_Info_Id = @System_User_Info_Id
          
                  UPDATE
                        dbo.Cbms_Sitegroup
                  SET   
                        Last_Refresh_Ts = GETDATE()
                  WHERE
                        Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                                
                  DELETE FROM
                        @Tbl_Cbms_Sitegroup_Sites
                  DELETE FROM
                        @Tvp_RM_Group_Dtl
                        
                  SET @Row_Counter = @Row_Counter + 1      
            END  
      
      
      -----------Contract group refresh -- Include new contract extensions
      SELECT
            @Total_Row_Count = 0
            
      INSERT      INTO @Cbms_Contract_Sitegroup_Lsit
                  ( 
                   Cbms_Sitegroup_Id )
                  SELECT
                        sgrp.Cbms_Sitegroup_Id
                  FROM
                        dbo.Cbms_Sitegroup sgrp
                        INNER JOIN dbo.Code cd
                              ON sgrp.Group_Type_Cd = cd.Code_Id
                        INNER JOIN dbo.Codeset cs
                              ON cd.Codeset_Id = cs.Codeset_Id
                        INNER JOIN dbo.Cbms_Sitegroup_Participant csp
                              ON sgrp.Cbms_Sitegroup_Id = csp.Cbms_Sitegroup_Id
                  WHERE
                        cs.Codeset_Name = 'Risk Management Groups'
                        AND cd.Code_Value = 'Contract'
                        AND csp.Include_New_Contract_Extensions = 1
                        AND ( @Cbms_Sitegroup_Id IS NULL
                              OR sgrp.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id )
                  GROUP BY
                        sgrp.Cbms_Sitegroup_Id
     
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Cbms_Contract_Sitegroup_Lsit 
                 
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count ) 
            BEGIN      
                  SELECT
                        @Refesh_Cbms_Sitegroup_Id = Cbms_Sitegroup_Id
                  FROM
                        @Cbms_Contract_Sitegroup_Lsit
                  WHERE
                        Row_Num = @Row_Counter;
                  WITH  Cte_Extended_Contracts
                          AS ( SELECT
                                    con.CONTRACT_ID
                               FROM
                                    dbo.Cbms_Sitegroup sgrp
                                    INNER JOIN dbo.Code cd
                                          ON sgrp.Group_Type_Cd = cd.Code_Id
                                    INNER JOIN dbo.Codeset cs
                                          ON cd.Codeset_Id = cs.Codeset_Id
                                    INNER JOIN dbo.Cbms_Sitegroup_Participant csp
                                          ON sgrp.Cbms_Sitegroup_Id = csp.Cbms_Sitegroup_Id
                                    INNER JOIN dbo.CONTRACT con
                                          ON con.BASE_CONTRACT_ID = csp.Group_Participant_Id
                               WHERE
                                    cs.Codeset_Name = 'Risk Management Groups'
                                    AND cd.Code_Value = 'Contract'
                                    AND sgrp.Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                               UNION ALL
                               SELECT
                                    con.CONTRACT_ID
                               FROM
                                    dbo.CONTRACT con
                                    INNER JOIN Cte_Extended_Contracts cec
                                          ON cec.CONTRACT_ID = con.BASE_CONTRACT_ID )
                        INSERT      INTO @Tvp_RM_Group_Dtl --RM_Group_Participant_Id,RM_Group_Participant_Type_Cd
                                    SELECT
                                          con.CONTRACT_ID AS RM_Group_Participant_Id
                                         ,@RM_Group_Participant_Contract_Cd AS RM_Group_Participant_Type_Cd
                                    FROM
                                          Cte_Extended_Contracts con
                                    GROUP BY
                                          con.CONTRACT_ID
                                          
                  /*Add the exisitng participant contracts as the merge will remove if not there in the input/source*/
                  INSERT      INTO @Tvp_RM_Group_Dtl --RM_Group_Participant_Id,RM_Group_Participant_Type_Cd
                              SELECT
                                    csp.Group_Participant_Id AS RM_Group_Participant_Id
                                   ,@RM_Group_Participant_Contract_Cd AS RM_Group_Participant_Type_Cd
                              FROM
                                    dbo.Cbms_Sitegroup sgrp
                                    INNER JOIN dbo.Code cd
                                          ON sgrp.Group_Type_Cd = cd.Code_Id
                                    INNER JOIN dbo.Codeset cs
                                          ON cd.Codeset_Id = cs.Codeset_Id
                                    INNER JOIN dbo.Cbms_Sitegroup_Participant csp
                                          ON sgrp.Cbms_Sitegroup_Id = csp.Cbms_Sitegroup_Id
                              WHERE
                                    cs.Codeset_Name = 'Risk Management Groups'
                                    AND cd.Code_Value = 'Contract'
                                    AND sgrp.Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                                    AND NOT EXISTS ( SELECT
                                                      1
                                                     FROM
                                                      @Tvp_RM_Group_Dtl grp1
                                                     WHERE
                                                      csp.Group_Participant_Id = grp1.RM_Group_Participant_Id )
                                                      
                  
                  EXEC dbo.Cbms_Sitegroup_Participant_Merge 
                        @Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                       ,@Tvp_RM_Group_Dtl = @Tvp_RM_Group_Dtl
                       ,@Include_New_Contract_Extensions = 1
                       ,@User_Info_Id = @System_User_Info_Id
          
                  UPDATE
                        dbo.Cbms_Sitegroup
                  SET   
                        Last_Refresh_Ts = GETDATE()
                  WHERE
                        Cbms_Sitegroup_Id = @Refesh_Cbms_Sitegroup_Id
                                
                  DELETE FROM
                        @Tbl_Cbms_Sitegroup_Sites
                  DELETE FROM
                        @Tvp_RM_Group_Dtl
                        
                  SET @Row_Counter = @Row_Counter + 1      
            END  
      
      
     
END;
GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Refresh] TO [CBMSApplication]
GO
