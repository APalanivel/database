SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MARKET_FORECAST_DATES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@marketName    	varchar(50)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec GET_MARKET_FORECAST_DATES_P -1,-1,2007

CREATE  PROCEDURE dbo.GET_MARKET_FORECAST_DATES_P

@userId varchar(10),
@sessionId varchar(20),
@marketName varchar(50)
--@forecastYear int

AS
begin

set nocount on

	select  distinct(AsofDate) 
	          from rm_market_forecast forecast,rm_market market
                  where forecast.rm_market_id=market.rm_market_id
                  and market.rm_market_name = @marketName
             --   ORDER BY AsofDate DESC
        
end
GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_FORECAST_DATES_P] TO [CBMSApplication]
GO
