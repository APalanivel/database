SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:      
    
 dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id    
    
DESCRIPTION:    
    
 To get the status of data presence for utility account of the given Site    
    
INPUT PARAMETERS:              
 Name					DataType	Default  Description              
------------------------------------------------------------
 @Site_Client_Hier_Id	INT
 @Service_Month			DATE
    
OUTPUT PARAMETERS:              
Name					DataType	Default  Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 23411, '2012-05-01'    
 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 4581, '2011-01-01'    
 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 1413, '2011-01-01'    
 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 256268, '2011-07-01'    
 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 261647,'11-1-2012'
 EXEC dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id 261647, '11/1/2012'    

 SELECT * FROM Cost_Usage_Account_Dtl WHERE Service_Month = '2012-05-01' 
    
AUTHOR INITIALS:              
Initials	Name              
------------------------------------------------------------    
HG			Harihara Suthan G    
    
MODIFICATIONS    
Initials	Date		Modification    
------------------------------------------------------------    
HG			2012-12-03  Created for Detail report

****************/
CREATE PROCEDURE dbo.Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id
      (
       @Site_Client_Hier_Id INT
      ,@Service_Month DATE )
AS
BEGIN
    
      SET NOCOUNT ON    
    
      SELECT
            com.Commodity_Id
           ,com.Commodity_Name
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            JOIN dbo.Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
            JOIN Core.Client_Hier_Account ucha
				ON ucha.Client_Hier_Id = cuad.Client_Hier_Id
					AND ucha.Account_Id = cuad.Account_Id
      WHERE
            ucha.Client_Hier_Id = @Site_Client_Hier_Id
            AND cuad.Service_Month = @Service_Month
            AND ucha.Account_Type = 'Utility'
      GROUP BY
            com.Commodity_Id
           ,com.Commodity_Name    
    
END
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Account_Data_Presence_Status_Per_Commodity_Sel_By_Client_Hier_Id] TO [CBMSApplication]
GO
