SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Forecast_Volume_Batch_Dtl_Ins                      
                        
Description:                        
        To load forecast volumes for on-boarded sites  
                        
Input Parameters:                        
    Name       DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @RM_Forecast_Volume_Batch_Id INT  
    @Batch_Status_Cd    INT  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
   
    
                         
 Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
    RR   2018-09-27     Global Risk Management - Created   
                       
******/
CREATE PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Dtl_Ins]
(
    @RM_Forecast_Volume_Batch_Id INT,
    @Batch_Status_Cd INT
)
AS
BEGIN
    SET NOCOUNT ON;

    --EXEC dbo.CODE_SEL_BY_CodeSet_Name   
    --      @CodeSet_Name = 'Batch Status'--, @Code_Value =''  

    DECLARE @Forecast_Start_Dt DATE,
            @Forecast_End_Dt DATE;

    DECLARE @RM_Sites_Forecast AS TABLE
    (
        Client_Hier_Id INT,
        Commodity_Id INT
    );

    SELECT @Forecast_Start_Dt = Forecast_Start_Dt,
           @Forecast_End_Dt = Forecast_End_Dt
    FROM Trade.RM_Forecast_Volume_Batch
    WHERE RM_Forecast_Volume_Batch_Id = @RM_Forecast_Volume_Batch_Id;

    INSERT INTO @RM_Sites_Forecast
    (
        Client_Hier_Id,
        Commodity_Id
    )
    SELECT chsites.Client_Hier_Id,
           chob.Commodity_Id
    FROM Trade.RM_Forecast_Volume_Batch batch
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON batch.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        INNER JOIN Core.Client_Hier chsites
            ON chob.Client_Hier_Id = chsites.Client_Hier_Id
    WHERE chsites.Site_Id > 0
          AND batch.RM_Forecast_Volume_Batch_Id = @RM_Forecast_Volume_Batch_Id
    GROUP BY chsites.Client_Hier_Id,
             chob.Commodity_Id;


    INSERT INTO @RM_Sites_Forecast
    (
        Client_Hier_Id,
        Commodity_Id
    )
    SELECT chsites.Client_Hier_Id,
           chob.Commodity_Id
    FROM Trade.RM_Forecast_Volume_Batch batch
        INNER JOIN Trade.RM_Client_Hier_Onboard chob
            ON batch.RM_Client_Hier_Onboard_Id = chob.RM_Client_Hier_Onboard_Id
        INNER JOIN Core.Client_Hier ch
            ON chob.Client_Hier_Id = ch.Client_Hier_Id
        INNER JOIN Core.Client_Hier chsites
            ON ch.Client_Id = chsites.Client_Id
               AND chob.Country_Id = chsites.Country_Id
    WHERE ch.Sitegroup_Id = 0
          AND chsites.Site_Id > 0
          AND batch.RM_Forecast_Volume_Batch_Id = @RM_Forecast_Volume_Batch_Id
          AND NOT EXISTS
    (
        SELECT 1
        FROM Trade.RM_Client_Hier_Onboard siteonb
        WHERE chsites.Client_Hier_Id = siteonb.Client_Hier_Id
              AND siteonb.Commodity_Id = chob.Commodity_Id
    )
    GROUP BY chsites.Client_Hier_Id,
             chob.Commodity_Id;

    INSERT INTO Trade.RM_Forecast_Volume_Batch_Dtl
    (
        RM_Forecast_Volume_Batch_Id,
        Client_Hier_Id,
        Forecast_Service_Month,
        Status_Cd,
        Error_Description,
        Created_Ts,
        Last_Change_Ts
    )
    SELECT @RM_Forecast_Volume_Batch_Id,
           rsf.Client_Hier_Id,
           dd.DATE_D,
           @Batch_Status_Cd,
           '',
           GETDATE(),
           GETDATE()
    FROM meta.DATE_DIM dd
        CROSS JOIN @RM_Sites_Forecast rsf
    WHERE dd.DATE_D
    BETWEEN @Forecast_Start_Dt AND @Forecast_End_Dt;
--AND NOT EXISTS ( SELECT  
--                  1  
--                 FROM  
--                  Trade.RM_Client_Hier_Forecast_Volume sitefv  
--                 WHERE  
--                  sitefv.Client_Hier_Id = rsf.Client_Hier_Id  
--                  AND sitefv.Commodity_Id = rsf.Commodity_Id  
--                  AND sitefv.Service_Month = dd.DATE_D )  



END;
GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Dtl_Ins] TO [CBMSApplication]
GO
