SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Vendor_Commodity_Analyst_Map_Del]  
     
DESCRIPTION:

	It Deletes	Vendor Commodity analyst Map detail for Selected Vendor_Commodity_Analyst_Map_Id.
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------         
	@Vendor_Commodity_Analyst_Map_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Vendor_Commodity_Analyst_Map_Del 1310										

	ROLLBACK TRAN

	BEGIN TRAN

		EXEC dbo.Vendor_Commodity_Analyst_Map_Del 1456										

	ROLLBACK TRAN

	SELECT * FROM VEndor_Commodity_Analyst_Map where vendor_Commodity_Map_Id NOT IN (SELECT Vendor_Commodity_Map_Id FROM Vendor_Commodity_map)
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			23-August-10	CREATED

*/
CREATE PROCEDURE dbo.Vendor_Commodity_Analyst_Map_Del
    (
       @Vendor_Commodity_Analyst_Map_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Vendor_Commodity_Analyst_Map
	WHERE
		Vendor_Commodity_Analyst_Map_Id = @Vendor_Commodity_Analyst_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Vendor_Commodity_Analyst_Map_Del] TO [CBMSApplication]
GO
