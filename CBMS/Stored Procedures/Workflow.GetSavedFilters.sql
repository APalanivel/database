SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[GetSavedFilters]  
DESCRIPTION:   
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
 @UserId int,          
 @Module_Id int  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:   EXEC [Workflow].[GetSavedFilters]  
 @UserId =49---int,          
 @Module_Id=1--- int   
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 TRK    SEP-2019 Created  
  
******/  
CREATE PROCEDURE [Workflow].[GetSavedFilters]      
 @UserId int,              
 @Module_Id int        
        
AS              
BEGIN           
       
        
 SELECT                
   Workflow_Queue_Saved_Filter_Query_Id AS Id,              
   Saved_Filter_Name AS Custom_Filter_Name,              
   Expiration_Dt AS Expires_On,              
   sf.Created_User_Id,         
   Workflow_Queue_Id Module_Id        
           
  FROM               
 Workflow.Workflow_queue_saved_filter_query sf,      
 Workflow.Workflow_sub_queue sq      
  WHERE sf.Created_User_Id = @UserId       
 AND sf.Workflow_Sub_Queue_Id = sq.Workflow_Sub_Queue_Id       
 AND sq.Workflow_Queue_Id = @Module_Id      
 AND sq.Workflow_Sub_Queue_Name = 'Saved Filters'        
  ORDER BY  Workflow_Queue_Saved_Filter_Query_Id          
        
        
END           
GO
GRANT EXECUTE ON  [Workflow].[GetSavedFilters] TO [CBMSApplication]
GO
