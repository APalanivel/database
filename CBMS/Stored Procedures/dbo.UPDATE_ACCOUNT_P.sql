SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.UPDATE_ACCOUNT_P
	@vendor_id int,
	@site_id int,
	@invoice_source_type_id int,
	@account_number varchar(50),
	@not_expected bit,
	@not_managed bit,
	@not_expected_date datetime,
	@not_expected_by_id int,
	@account_id int
	AS
	begin
		set nocount on
	
		update 	account
 
		set 	VENDOR_ID = @vendor_id,
			SITE_ID = @site_id,
			INVOICE_SOURCE_TYPE_ID = @invoice_source_type_id,
			ACCOUNT_NUMBER = @account_number,
			Not_Expected = @not_expected,
			Not_Managed = @not_managed,
			Not_Expected_Date = @not_expected_date,
			NOT_EXPECTED_BY_ID = @not_expected_by_id
		where 	ACCOUNT_ID = @account_id

	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_ACCOUNT_P] TO [CBMSApplication]
GO
