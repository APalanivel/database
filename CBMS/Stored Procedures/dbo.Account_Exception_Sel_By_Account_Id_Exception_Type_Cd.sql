SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Exception_Sel_By_Account_Id_Exception_Type_Cd       
              
Description:              
			This sproc to get all exceptions of a given account id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Account_Id							INT					
    @Queue_Id							INT					NULL
                          
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
	SELECT * FROM dbo.Account_Exception
	select * from code where codeset_id =219
	
	EXEC dbo.Account_Exception_Sel_By_Account_Id_Exception_Type_Cd 1342229
	EXEC dbo.Account_Exception_Sel_By_Account_Id_Exception_Type_Cd 1342212
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
	           
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	NR				2019-07-23		Add Contract -
******/

CREATE PROCEDURE [dbo].[Account_Exception_Sel_By_Account_Id_Exception_Type_Cd]
    (
        @Account_Id INT
        , @Exception_Type_Cd INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Code c
                ON c.Code_Id = ae.Exception_Status_Cd
        WHERE
            ae.Account_Id = @Account_Id
            AND ae.Exception_Type_Cd = @Exception_Type_Cd
            AND c.Code_Value IN ( 'New', 'In Progress' )
        GROUP BY
            ae.Account_Exception_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Exception_Sel_By_Account_Id_Exception_Type_Cd] TO [CBMSApplication]
GO
