SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

NAME: dbo.Rfp_Id_String_Sel_By_Account_Id  
  
  
DESCRIPTION: 

	It Returns Rfp Ids in Comma Seperated format for given Account Id where bid status for the given account is in 'New','Open','Bid Placed','Expired','Refresh','Processing'
  
INPUT PARAMETERS:      
	Name	            DataType       Default     Description      
--------------------------------------------------------------------
	@Account_Id			INTEGER

OUTPUT PARAMETERS:
	Name              DataType          Default     Description
---------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------

	EXEC dbo.Rfp_Id_String_Sel_By_Account_Id 79788
	EXEC dbo.Rfp_Id_String_Sel_By_Account_Id 26286
	EXEC dbo.Rfp_Id_String_Sel_By_Account_Id 57701

	SELECT
		sra.Account_id
	FROM
		dbo.Sr_Rfp_Account sra
		JOIN dbo.ENTITY ent
			ON sra.BId_STATUS_TYPE_ID = ent.ENTITY_ID
	WHERE
		ent.Entity_Name NOT IN ('Closed Awarded','Closed Not Awarded','Closed')

  
AUTHOR INITIALS:  
 Initials	Name
------------------------------------------------------------  
  PNR		PANDARINATH

MODIFICATIONS
  
 Initials	Date		Modification  
------------------------------------------------------------  
   PNR		06/30/2010  Created

******/
  
CREATE PROCEDURE dbo.Rfp_Id_String_Sel_By_Account_Id
	 @Account_Id	INTEGER
AS
BEGIN
 
	SET NOCOUNT ON;
		
	DECLARE @Rfp_Id_List VARCHAR(1000)
		, @Delimiter CHAR = ','

	SELECT
		@Rfp_Id_List = COALESCE(@Rfp_Id_List + @Delimiter, '') + SPACE(1) + CONVERT(VARCHAR(30),RfpAcc.SR_RFP_ID)
	FROM
		dbo.Sr_Rfp_Account RfpAcc
		JOIN dbo.ENTITY ent
			ON RfpAcc.BID_STATUS_TYPE_ID = ent.ENTITY_ID
	WHERE
		RfpAcc.ACCOUNT_ID = @Account_Id
		AND RfpAcc.IS_DELETED = 0
		AND ent.ENTITY_NAME IN ('New'
								,'Open'
								,'Bid Placed'
								,'Expired'
								,'Refresh'
								,'Processing'
								)

	SELECT @Rfp_Id_List AS Rfp_Id_List

END
GO
GRANT EXECUTE ON  [dbo].[Rfp_Id_String_Sel_By_Account_Id] TO [CBMSApplication]
GO
