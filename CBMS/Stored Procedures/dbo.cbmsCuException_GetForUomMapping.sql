SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.cbmsCuException_GetForUomMapping

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@ubm_id        	int       	          	
	@ubm_bucket_code	varchar(200)	          	
	@commodity_type_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCuException_GetForUomMapping 1, 2,'USAGE-Water',67
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
NR				Narayana Reddy


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	NR			2018-10-01	 D20-164 - Added Header and Replaced Unit of Measure Not Mapped to Mapping Required - Unit of Measure Not Mapped

******/
CREATE PROCEDURE [dbo].[cbmsCuException_GetForUomMapping]
    (
        @MyAccountId INT
        , @ubm_id INT
        , @ubm_unit_of_measure_code VARCHAR(200)
        , @commodity_type_id INT
    )
AS
    BEGIN

        DECLARE @exception_type_id INT;

        SET NOCOUNT ON;

        SELECT
            @exception_type_id = CU_EXCEPTION_TYPE_ID
        FROM
            dbo.CU_EXCEPTION_TYPE WITH (NOLOCK)
        WHERE
            EXCEPTION_TYPE = 'Mapping Required - Unit of Measure Not Mapped';

        SET NOCOUNT OFF;

        SELECT  DISTINCT
                i.CU_INVOICE_ID
                , e.CU_EXCEPTION_ID
        FROM
            dbo.CU_INVOICE i WITH (NOLOCK)
            JOIN dbo.CU_INVOICE_DETERMINANT d WITH (NOLOCK)
                ON d.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION e WITH (NOLOCK)
                ON e.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION_DETAIL ed WITH (NOLOCK)
                ON ed.CU_EXCEPTION_ID = e.CU_EXCEPTION_ID
        WHERE
            i.UBM_ID = @ubm_id
            AND d.COMMODITY_TYPE_ID = @commodity_type_id
            AND d.UBM_UNIT_OF_MEASURE_CODE = @ubm_unit_of_measure_code
            AND d.UNIT_OF_MEASURE_TYPE_ID IS NULL
            AND ed.EXCEPTION_TYPE_ID = @exception_type_id
            AND ed.IS_CLOSED = 0;

    END;


GO
GRANT EXECUTE ON  [dbo].[cbmsCuException_GetForUomMapping] TO [CBMSApplication]
GO
