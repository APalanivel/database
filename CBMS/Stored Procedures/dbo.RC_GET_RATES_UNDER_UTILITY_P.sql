SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.RC_GET_RATES_UNDER_UTILITY_P 
@userId varchar,
@sessionId varchar,
@vendorId int

as
	set nocount on
	select	RATE_ID,
		RATE_NAME
	
	from	RATE
	
	where	VENDOR_ID = @vendorId 
	
	order by RATE_NAME
GO
GRANT EXECUTE ON  [dbo].[RC_GET_RATES_UNDER_UTILITY_P] TO [CBMSApplication]
GO
