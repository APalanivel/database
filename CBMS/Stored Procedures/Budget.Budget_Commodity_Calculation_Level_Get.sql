SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                            
NAME: Budget.Budget_Commodity_Calculation_Level_Get                                                                  
                                                                            
DESCRIPTION: To Get Budget Calculation Levels based on Account Id.                                                                            
             Used at:-Client_Info-->Search Clinets --> Manage Clients --> Manage Sites --> Manage Utility Accounts --> Edit Utility Account                                                               
                                                                            
INPUT PARAMETERS:                                                                            
           Name         DataType  Default Description                                                                            
------------------------------------------------------------                                                                            
 @Account_Id    INT            
                                                                   
OUTPUT PARAMETERS:                                                                            
 Name   DataType  Default Description                                                                            
------------------------------------------------------------                                                                            
                                                                            
USAGE EXAMPLES:                                                                            
------------------------------------------------------------                                                                            
EXEC Budget.Budget_Commodity_Calculation_Level_Get 1741210    
EXEC Budget.Budget_Commodity_Calculation_Level_Get 206      
EXEC Budget.Budget_Commodity_Calculation_Level_Get 12       
                                                  
AUTHOR INITIALS:                                                                            
 Initials Name                                                                            
------------------------------------------------------------                                                                            
SC   Sreenivasulu Cheerala                                                      
                                                                            
MODIFICATIONS                                                                            
 Initials Date  Modification                                                                            
------------------------------------------------------------                                                                            
 SC   2019-10-30   Created                                                                              
                                                                            
******/
CREATE PROCEDURE [Budget].[Budget_Commodity_Calculation_Level_Get]
    (
        @Account_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;
        SELECT
            ISNULL(bccl.Account_Id, cha.Account_Id) Account_Id
            , ISNULL(bccl.Commodity_Id, cha.Commodity_Id) Commodity_Id
            , cm.Commodity_Name
            , c.Code_Id Meter_Calc_Level_Id
            , ISNULL(c.Code_Value, 'Account') Meter_Calc_Level_Name
        FROM
            Budget.Account_Commodity_Budget_Calc_Level bccl
            INNER JOIN dbo.Commodity cm
                ON cm.Commodity_Id = bccl.Commodity_Id
            LEFT JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = bccl.Account_Id
            LEFT JOIN dbo.Code c
                ON c.Code_Id = bccl.Budget_Calc_Level_Cd
        WHERE
            cha.Account_Id = @Account_Id
        GROUP BY
            ISNULL(bccl.Account_Id, cha.Account_Id)
            , ISNULL(bccl.Commodity_Id, cha.Commodity_Id)
            , cm.Commodity_Name
            , c.Code_Id
            , ISNULL(c.Code_Value, 'Account');

    END;

GO
GRANT EXECUTE ON  [Budget].[Budget_Commodity_Calculation_Level_Get] TO [CBMSApplication]
GO
