
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Bucket_Master_Uom_Map_Sel_By_Commodity_Id]

DESCRIPTION:

	To get the uoms mapped for all the buckets of the given commodity.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Commodity_Id	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC Bucket_Master_Uom_Map_Sel_By_Commodity_Id 290
	EXEC Bucket_Master_Uom_Map_Sel_By_Commodity_Id 291

     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
RK			Raghu Kalvapudi

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
RK			08/29/2013	Created
RK          11/11/2013 Fix for MAINT-2330(Validate Volume/Usage at Commodity level)

*****/

CREATE PROCEDURE [dbo].[Bucket_Master_Uom_Map_Sel_By_Commodity_Id] ( @Commodity_Id INT )
AS
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            bm.Bucket_Master_Id
           ,bm.Bucket_Name
           ,bmum.Uom_Type_Id
           ,e.Entity_Name AS Uom_Name
      FROM
            dbo.Bucket_Master_Uom_Map bmum
            INNER JOIN dbo.Bucket_Master bm
                  ON bmum.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.Entity e
                  ON e.Entity_id = bmum.Uom_Type_Id
      WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name NOT IN ( 'Total Usage', 'Volume' )
      UNION ALL
      SELECT
            bm.Bucket_Master_Id
           ,bm.Bucket_Name
           ,e.ENTITY_ID AS Uom_Type_Id
           ,e.Entity_Name AS Uom_Name
      FROM
            dbo.Commodity c
            INNER JOIN dbo.Bucket_Master bm
                  ON c.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.Entity e
                  ON e.ENTITY_TYPE = c.UOM_Entity_Type
      WHERE
            bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name IN ( 'Total Usage', 'Volume' ) 


END;
;
GO

GRANT EXECUTE ON  [dbo].[Bucket_Master_Uom_Map_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
