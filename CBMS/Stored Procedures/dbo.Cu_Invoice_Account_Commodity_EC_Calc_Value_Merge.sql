SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value_Merge           
              
Description:              
        To insert Data to Cu_Invoice_Account_Commodity_Recalc_Lock table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	@Cu_Invoice_Account_Commodity_Id	INT
    @Ec_Calc_Val_Id						INT
    @Calc_Value							DECIMAL(28, 10)		 NULL
    @Uom_Id								INT					 NULL
    @Currency_Unit_Id					INT					 NULL
    @Is_Value_Locked					BIT					 0
    @Comment_Id							INT					 NULL
    @User_Info_Id						INT
      
              
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                

    BEGIN TRAN
    
   DECLARE @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_EC_Calc_Value_Comment_is_Value_Locked
	INSERT      INTO @tvp_EC_Calc_Value_Comment_is_Value_Locked
            SELECT
                  468
                 ,172479128.0000000000
                 ,12
                 ,NULL
                 ,NULL
                 ,1
                 ,10,20,30,40
	EXEC dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value_Merge 
      @Cu_Invoice_Id = 23712332
     ,@Account_Id = 53492
     ,@Commodity_Id = 290
     ,@tvp_EC_Calc_Value_Comment_is_Value_Locked = @tvp_EC_Calc_Value_Comment_is_Value_Locked
     ,@User_Info_Id = 61212
     
     SELECT * FROM dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv 
	WHERE Cu_Invoice_Account_Commodity_Id = 15 AND Ec_Calc_Val_Id = 468
	
    ROLLBACK TRAN
       
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV			   Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-09-01		Maint-4109: Created For AS400-Enahancement. 
    NR				2018-03-15		Recalc_Data_Interval - Added New slots.        
             
******/ 
CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value_Merge]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_EC_Calc_Value_Comment_is_Value_Locked READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;  
            
      DECLARE
            @Cu_Invoice_Account_Commodity_Id_Out INT
           ,@Comment_Id INT
           ,@Counter INT = 1;

      CREATE TABLE #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments
            ( 
             Cu_Invoice_Account_Commodity_Id INT
            ,Ec_Calc_Val_Id INT
            ,Comment_Id INT );
        
      CREATE TABLE #Comment_Insert
            ( 
             id INT IDENTITY(1, 1)
            ,Ec_Calc_Val_Id INT
            ,Comment_Text VARCHAR(MAX) );     
      
      
      SELECT
            @Cu_Invoice_Account_Commodity_Id_Out = Cu_Invoice_Account_Commodity_Id
      FROM
            dbo.Cu_Invoice_Account_Commodity ciac
      WHERE
            ciac.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ciac.Account_Id = @Account_Id
            AND ciac.Commodity_Id = @Commodity_Id;
            
         
      
      IF @Cu_Invoice_Account_Commodity_Id_Out IS NULL 
            EXEC Cu_Invoice_Account_Commodity_Ins 
                  @Cu_Invoice_Id
                 ,@Account_Id
                 ,@Commodity_Id
                 ,@Cu_Invoice_Account_Commodity_Id = @Cu_Invoice_Account_Commodity_Id_Out OUTPUT;     


    
      INSERT      INTO #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments
                  ( 
                   Cu_Invoice_Account_Commodity_Id
                  ,Ec_Calc_Val_Id
                  ,Comment_Id )
                  SELECT
                        @Cu_Invoice_Account_Commodity_Id_Out
                       ,ISNULL(ciacecv.Ec_Calc_Val_Id, tvp.Ec_Calc_Val_Id)
                       ,ciacecv.Comment_Id
                  FROM
                        @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp
                        LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value ciacecv
                              ON ciacecv.Ec_Calc_Val_Id = tvp.Ec_Calc_Val_Id
                                 AND ciacecv.Cu_Invoice_Account_Commodity_Id = @Cu_Invoice_Account_Commodity_Id_Out;
     
     
      
      UPDATE
            c
      SET   
            c.Comment_Text = tvp_comments.Comment_Text
           ,c.Comment_Dt = GETDATE()
           ,c.Comment_User_Info_Id = @User_Info_Id
      FROM
            dbo.Comment c
            INNER JOIN #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
                  ON c.Comment_ID = cvc.Comment_Id
            INNER JOIN @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_comments
                  ON tvp_comments.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id
      WHERE
            tvp_comments.Comment_Text IS NOT NULL
            AND cvc.Comment_Id IS NOT NULL;
     
      
      DELETE
            c
      FROM
            dbo.Comment c
            INNER JOIN #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
                  ON c.Comment_ID = cvc.Comment_Id
            INNER JOIN @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_comments
                  ON tvp_comments.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id
      WHERE
            tvp_comments.Comment_Text IS NULL
            AND cvc.Comment_Id IS NOT NULL;
            
      
      INSERT      INTO #Comment_Insert
                  ( 
                   Ec_Calc_Val_Id
                  ,Comment_Text )
                  SELECT
                        cvc.Ec_Calc_Val_Id
                       ,tvp_comments.Comment_Text
                  FROM
                        #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
                        INNER JOIN @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_comments
                              ON tvp_comments.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id
                  WHERE
                        tvp_comments.Comment_Text IS NOT NULL
                        AND cvc.Comment_Id IS NULL;
      
     
      WHILE ( @Counter <= ( SELECT
                              COUNT(1)
                            FROM
                              #Comment_Insert ) ) 
            BEGIN
                  INSERT      INTO dbo.Comment
                              ( 
                               Comment_User_Info_Id
                              ,Comment_Text )
                              SELECT
                                    @User_Info_Id
                                   ,ci.Comment_Text
                              FROM
                                    #Comment_Insert ci
                              WHERE
                                    id = @Counter;
                  SELECT
                        @Comment_Id = SCOPE_IDENTITY();
            
            
                  UPDATE
                        cvc
                  SET   
                        Comment_Id = @Comment_Id
                  FROM
                        #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
                        INNER JOIN #Comment_Insert ci
                              ON ci.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id
                  WHERE
                        ci.id = @Counter;
                  
                  SET @Counter = @Counter + 1;
            
            END;
      
      
      
      UPDATE
            cvc
      SET   
            Comment_Id = NULL
      FROM
            #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
            INNER JOIN @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp_comments
                  ON tvp_comments.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id
      WHERE
            tvp_comments.Comment_Text IS NULL;
      
      
   
      MERGE INTO dbo.Cu_Invoice_Account_Commodity_EC_Calc_Value AS tgt
            USING 
                  ( SELECT
                        @Cu_Invoice_Account_Commodity_Id_Out AS Cu_Invoice_Account_Commodity_Id
                       ,tvp.Ec_Calc_Val_Id AS Ec_Calc_Val_Id
                       ,tvp.Calc_Value AS Calc_Value
                       ,tvp.Uom_Id AS Uom_Id
                       ,tvp.Currency_Unit_Id AS Currency_Unit_Id
                       ,tvp.Is_Value_Locked AS Is_Value_Locked
                       ,cvc.Comment_Id AS Comment_Id
                       ,@User_Info_Id User_Info_Id
                       ,tvp.Expected_Slots
                       ,tvp.Actual_Slots
                       ,tvp.Missing_Slots
                       ,tvp.Edited_Slots
                    FROM
                        @tvp_EC_Calc_Value_Comment_is_Value_Locked tvp
                        INNER JOIN #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments cvc
                              ON tvp.Ec_Calc_Val_Id = cvc.Ec_Calc_Val_Id ) AS src
            ON tgt.Cu_Invoice_Account_Commodity_Id = src.Cu_Invoice_Account_Commodity_Id
                  AND tgt.Ec_Calc_Val_Id = src.Ec_Calc_Val_Id
            WHEN NOT MATCHED 
                  THEN
            INSERT
                        ( 
                         Cu_Invoice_Account_Commodity_Id
                        ,Ec_Calc_Val_Id
                        ,Calc_Value
                        ,Uom_Id
                        ,Currency_Unit_Id
                        ,Is_Value_Locked
                        ,Comment_Id
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts
                        ,Expected_Slots
                        ,Actual_Slots
                        ,Missing_Slots
                        ,Edited_Slots )
                    VALUES
                        ( 
                         src.Cu_Invoice_Account_Commodity_Id
                        ,src.Ec_Calc_Val_Id
                        ,src.Calc_Value
                        ,src.Uom_Id
                        ,src.Currency_Unit_Id
                        ,src.Is_Value_Locked
                        ,src.Comment_Id
                        ,@User_Info_Id
                        ,GETDATE()
                        ,@User_Info_Id
                        ,GETDATE()
                        ,Expected_Slots
                        ,Actual_Slots
                        ,Missing_Slots
                        ,Edited_Slots )
            WHEN MATCHED AND src.Comment_Id IS NULL
                  AND src.Is_Value_Locked = 0
                  THEN
            DELETE
            WHEN MATCHED 
                  THEN
            UPDATE  SET 
                        Calc_Value = src.Calc_Value
                       ,Is_Value_Locked = src.Is_Value_Locked
                       ,Updated_User_Id = @User_Info_Id
                       ,Uom_Id = src.Uom_Id
                       ,Currency_Unit_Id = src.Currency_Unit_Id
                       ,Comment_Id = src.Comment_Id
                       ,Last_Change_Ts = GETDATE()
                       ,Expected_Slots = src.Expected_Slots
                       ,Actual_Slots = src.Actual_Slots
                       ,Missing_Slots = src.Missing_Slots
                       ,Edited_Slots = src.Edited_Slots;
                       
      DROP TABLE #Cu_Invoice_Account_Commodity_EC_Calc_Value_Comments;
      DROP TABLE  #Comment_Insert;             
                                   

END;


GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value_Merge] TO [CBMSApplication]
GO
