SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
        
/******           
           
 NAME: [dbo].[Get_User_Mapped_To_Access_Role_By_Client]        
                  
 DESCRIPTION:            
      To get the users for a given role id.          
                
 INPUT PARAMETERS:            
           
 Name                               DataType       Default          Description            
---------------------------------------------------------------------------------------------------------------          
 @Client_App_Access_Role_Id         INT          
            
 OUTPUT PARAMETERS:          
                   
 Name                               DataType       Default          Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------   
        
 EXEC dbo.Get_User_Mapped_To_Access_Role_By_Client    36 , 235        
           
 AUTHOR INITIALS:            
          
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:            
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2013-12-26      Created for RA Admin user management          
           
******/     
CREATE PROCEDURE [dbo].[Get_User_Mapped_To_Access_Role_By_Client]
      ( 
       @Client_App_Access_Role_Id INT
      ,@Client_Id INT )
AS 
BEGIN    
  
      SET NOCOUNT ON     
      SELECT
            Users.user_info_id
           ,Users.username
           ,Users.first_name
           ,Users.middle_name
           ,Users.last_name
           ,Users.FIRST_NAME + SPACE(1) + Users.LAST_NAME full_name
           ,Users.EMAIL_ADDRESS
           ,caar.Client_App_Access_Role_Id
      FROM
            dbo.Client_App_Access_Role caar
            INNER JOIN dbo.User_Info_Client_App_Access_Role_Map uicaar
                  ON caar.Client_App_Access_Role_Id = uicaar.Client_App_Access_Role_Id
            INNER JOIN dbo.USER_INFO Users
                  ON uicaar.USER_INFO_ID = Users.USER_INFO_ID
                     AND Users.IS_HISTORY = 0
                     AND Users.ACCESS_LEVEL = 1
                     AND Users.CLIENT_ID = @Client_Id
      WHERE
            caar.Client_App_Access_Role_Id = @Client_App_Access_Role_Id
            AND caar.Client_Id = @Client_Id
    
END  
;
GO
GRANT EXECUTE ON  [dbo].[Get_User_Mapped_To_Access_Role_By_Client] TO [CBMSApplication]
GO
