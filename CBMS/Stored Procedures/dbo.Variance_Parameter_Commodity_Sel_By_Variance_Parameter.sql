SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*********      
NAME:  dbo.Variance_Parameter_Commodity_Sel_By_Variance_Parameter        
       
DESCRIPTION:       
      
INPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
@Variance_Test_Id  INT  
@Commodity_Mapped_List BIT                 1  
          
OUTPUT PARAMETERS:          
      Name              DataType          Default     Description          
------------------------------------------------------------          
          
USAGE EXAMPLES:         
      
 EXEC dbo.Variance_Parameter_Commodity_Sel_By_Variance_Parameter 4
 EXEC dbo.Variance_Parameter_Commodity_Sel_By_Variance_Parameter 8
      
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
AKR      Ashok Kumar Raju
RR		 Raghu Reddy  
      
      
Initials	Date		Modification        
------------------------------------------------------------        
 AKR		2013-05-24	Created
 RR			2013-07-12	Modified order by clause, the desired order of services EP,NG,....remaning in alphabetical order is
						breaking if any service name starts with numerics
 RKV        2020-01-13  AVT Changes
 
******/      
      
CREATE PROCEDURE [dbo].[Variance_Parameter_Commodity_Sel_By_Variance_Parameter]
      ( 
       @Variance_Parameter_Id INT
	   ,@variance_process_Engine_id INT )
AS 
BEGIN      
      
      SET NOCOUNT ON ;
     
      SELECT
            vpcm.Variance_Parameter_Id
           ,com.Commodity_Id
           ,Com.Commodity_Name
      FROM
            dbo.Variance_Parameter_Commodity_Map vpcm
            INNER JOIN dbo.Commodity com
                  ON com.commodity_Id = vpcm.commodity_Id
				  JOIN dbo.Variance_Parameter vp
				  ON vp.Variance_Parameter_Id = vpcm.Variance_Parameter_Id
				  JOIN code c 
				  ON c.Code_Id = vp.Variance_Category_Cd
      WHERE
            vpcm.Variance_Parameter_Id = @Variance_Parameter_Id
			AND not (c.Code_Value ='Cost' AND com.Commodity_Name IN ('Electric Power' ,'Natural Gas' , 'Water', 'Waste Water'))
		--	AND vpcm.variance_process_Engine_id = @variance_process_Engine_id
      ORDER BY
            CASE WHEN Com.Commodity_Name = 'Electric Power' THEN 1
                 WHEN Com.Commodity_Name = 'Natural Gas' THEN 2
                 ELSE 3
            END
           ,Com.Commodity_Name      
      
END      


;
GO
GRANT EXECUTE ON  [dbo].[Variance_Parameter_Commodity_Sel_By_Variance_Parameter] TO [CBMSApplication]
GO
