SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.Cost_Usage_Site_Dtl_Merge 
  
 DESCRIPTION:   
 Used to insert the data in to Cost_Usage_Site_Dtl table  INPUT PARAMETERS:  
 
 Name     DataType  Default Description  
------------------------------------------------------------  
 @Service_Month   Date  
 ,@Bucket_Master_Id  int  
 ,@Bucket_Value   decimal(28,10)  
 ,@UOM_Type_Id   int  
 ,@CURRENCY_UNIT_ID      int  
 ,@Created_By_Id   int  
 ,@Client_hier_id    INT  
 ,@Created_Ts            datetime =NULL   
 ,@Updated_Ts   datetime =NULL  
 ,@Data_Source_Cd  INT  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:  
------------------------------------------------------------
select top 2 * from Cost_Usage_Site_Dtl where client_hier_id= 32140 and  Bucket_Master_Id=100029 and service_month='2009-12-01'

 begin tran   
  EXEC dbo.Cost_Usage_Site_Dtl_Merge  '2009-12-01',100029,22,null,3,49,32140,NULL,NULL,100350
    
	select top 2 * from Cost_Usage_Site_Dtl where client_hier_id= 32140 and  Bucket_Master_Id=100029 and service_month='2009-12-01'
 rollback tran
    
 EXEC dbo.Cost_Usage_Site_Dtl_Merge  '2009-12-01',100029,22,null,3,49,32140,NULL,NULL,100350  
 
 EXEC dbo.Cost_Usage_Site_Dtl_Merge  '2010-01-01',100029,22,null,3,49,32140,NULL,NULL,100350   
 EXEC dbo.Cost_Usage_Site_Dtl_Merge  '2010-01-01',100029,22,null,3,49,32140,100227,NULL,NULL,100350   
 
  
  select top 2 * from Cost_Usage_Site_Dtl where site_id = 43118
  
  AUTHOR INITIALS:  Initials Name  
------------------------------------------------------------  
 SSR   Sharad Srivastava  
 BCH   Balaraju 
 SP	   Sandeep Pigilam
 
 MODIFICATIONS   
 
 Initials	Date		Modification  
------------------------------------------------------------  
 SSR		03/04/2010	Created  
 HG			03/17/2010  Cbms_Image_Id column removed from Cost_Usage_Site_Dtl table and created new table Site_Commodity_Image to store image info.  
						return of SCOPE_IDENTITY() value removed as application is not required this.  
 BCH		2011-10-19  Added the @Data_Source_Cd input parameter and to store the data_source_cd value in Cost_Usage_Site_Dtl table  
 BCH		2012-03-16	I Have modified Client_hier_id intead of Site_id
 SP			2017-06-13	Data Estimation, Added @Data_Type,@Estimated_Bucket_Value,@Actual_Bucket_Value as Input.
 KVK		05/09/2019	Modified to treat Data_Type as 'Actual when it is NULL(MAINT-8560 )
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Site_Dtl_Merge]
      @Service_Month DATE
     ,@Bucket_Master_Id INT
     ,@Bucket_Value DECIMAL(28, 10)
     ,@UOM_Type_Id INT
     ,@CURRENCY_UNIT_ID INT
     ,@Created_By_Id INT
     ,@Client_Hier_Id INT
     ,@Created_Ts DATETIME = NULL
     ,@Updated_Ts DATETIME = NULL
     ,@Data_Source_Cd INT
     ,@Data_Type VARCHAR(25) = 'Actual'
     ,@Estimated_Bucket_Value DECIMAL(28, 10) = NULL
     ,@Actual_Bucket_Value DECIMAL(28, 10) = NULL
AS
BEGIN

      SET NOCOUNT ON  

      DECLARE @Data_Type_Cd INT

      SELECT
            @Data_Type_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Type_Cd'
            AND cs.Codeset_Name = 'Data_Type'
            AND c.Code_Value = ISNULL(@Data_Type, 'Actual')

	-- Added this condition to handle the site level data directly added from Cost/Usage tool in CBMS.
      IF ISNULL(@Data_Type,'Actual') = 'Actual'
            AND @Actual_Bucket_Value IS NULL
            AND @Estimated_Bucket_Value IS NULL
            AND @Bucket_Value IS NOT NULL
            BEGIN
                  SET @Actual_Bucket_Value = @Bucket_Value
            END

      MERGE INTO dbo.Cost_Usage_Site_Dtl TR_cusd
      USING
            ( SELECT
                  @Service_Month AS Service_Month
                 ,@Bucket_Master_Id AS Bucket_Master_Id
                 ,@Bucket_Value AS Bucket_Value
                 ,@UOM_Type_Id AS UOM_Type_Id
                 ,@CURRENCY_UNIT_ID AS CURRENCY_UNIT_ID
                 ,@Created_By_Id AS Created_By_Id
                 ,ISNULL(@Created_Ts, GETDATE()) AS Created_Ts
                 ,ISNULL(@Updated_Ts, GETDATE()) AS Updated_Ts
                 ,@Client_Hier_Id AS Client_Hier_Id
                 ,@Data_Source_Cd AS Data_Source_Cd
                 ,@Data_Type_Cd AS Data_Type_Cd
                 ,@Estimated_Bucket_Value AS Estimated_Bucket_Value
                 ,@Actual_Bucket_Value AS Actual_Bucket_Value ) AS SR_Cusd
      ON ( TR_cusd.Bucket_Master_Id = SR_Cusd.Bucket_Master_Id
           AND TR_cusd.Client_Hier_Id = SR_Cusd.Client_Hier_Id
           AND TR_cusd.Service_Month = SR_Cusd.Service_Month )
      WHEN MATCHED THEN
            UPDATE SET
                    TR_cusd.Bucket_Master_Id = SR_Cusd.Bucket_Master_Id
                   ,TR_cusd.Bucket_Value = SR_Cusd.Bucket_Value
                   ,TR_cusd.UOM_Type_Id = SR_Cusd.UOM_Type_Id
                   ,TR_cusd.CURRENCY_UNIT_ID = SR_Cusd.CURRENCY_UNIT_ID
                   ,TR_cusd.Updated_By_Id = SR_Cusd.Created_By_Id
                   ,TR_cusd.Updated_Ts = SR_Cusd.Updated_Ts
                   ,TR_cusd.Client_Hier_Id = SR_Cusd.Client_Hier_Id
                   ,TR_cusd.Data_Source_Cd = SR_Cusd.Data_Source_Cd
                   ,TR_cusd.Data_Type_Cd = SR_Cusd.Data_Type_Cd
                   ,TR_cusd.Estimated_Bucket_Value = SR_Cusd.Estimated_Bucket_Value
                   ,TR_cusd.Actual_Bucket_Value = SR_Cusd.Actual_Bucket_Value
      WHEN NOT MATCHED THEN
            INSERT
                   ( Service_Month
                   ,Bucket_Master_Id
                   ,Bucket_Value
                   ,UOM_Type_Id
                   ,CURRENCY_UNIT_ID
                   ,Created_By_Id
                   ,Created_Ts
                   ,Updated_Ts
                   ,Client_Hier_Id
                   ,Data_Source_Cd
                   ,Data_Type_Cd
                   ,Estimated_Bucket_Value
                   ,Actual_Bucket_Value )
            VALUES ( SR_Cusd.Service_Month
                   ,SR_Cusd.Bucket_Master_Id
                   ,SR_Cusd.Bucket_Value
                   ,SR_Cusd.UOM_Type_Id
                   ,SR_Cusd.CURRENCY_UNIT_ID
                   ,SR_Cusd.Created_By_Id
                   ,SR_Cusd.Created_Ts
                   ,SR_Cusd.Updated_Ts
                   ,SR_Cusd.Client_Hier_Id
                   ,SR_Cusd.Data_Source_Cd
                   ,SR_Cusd.Data_Type_Cd
                   ,SR_Cusd.Estimated_Bucket_Value
                   ,SR_Cusd.Actual_Bucket_Value );  
  
END;
;
GO



GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Merge] TO [CBMSApplication]
GO
