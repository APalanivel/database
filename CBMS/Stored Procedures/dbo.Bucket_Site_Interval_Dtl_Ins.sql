SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Bucket_Site_Interval_Dtl_Ins  
  
DESCRIPTION:  
  
 This procedure is to Insert the data into dbo.Bucket_Site_Interval_Dtl table  
  
  
INPUT PARAMETERS:  
   Name         DataType     Default     Description  
------------------------------------------------------------------------------------------------  
      @tvp_Bucket_Site_Interval_Dtl AS tvp_Bucket_Site_Interval_Dtl  
      @Created_User_Id      INT  
      @Created_Ts       DATETIME     NULL  
        
  
OUTPUT PARAMETERS:  
 Name          DataType     Default     Description  
-------------------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------------------  
  
Example1:- Starts here    
BEGIN TRAN  
 
	SELECT * FROM Bucket_Site_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND  Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'  
	                        AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350    
 
	DECLARE @tvp tvp_Bucket_Site_Interval_Dtl  
  	INSERT INTO @tvp values (100001,110,'1/1/2013','1/5/2013',100350,150,12,null)  
 
	EXEC  dbo.Bucket_Site_Interval_Dtl_Ins @tvp,49,null  
 
	SELECT * FROM Bucket_Site_Interval_Dtl WHERE  Client_Hier_Id = 100001 AND  Bucket_Master_Id = 110 AND Service_Start_Dt = '1/1/2013'  
	                        AND Service_End_Dt = '1/5/2013' AND Data_Source_Cd  = 100350    
 
 
ROLLBACK TRAN         
Example1:- Ends here
  
AUTHOR INITIALS:              
 Initials  Name  
------------------------------------------------------------------------------------------------  
   RKV     Ravi Kumar Vegesna
  
MODIFICATIONS  
 Initials  Date         Modification  
------------------------------------------------------------------------------------------------  
   RKV     2013-03-07   Created   
   
******/  
CREATE PROCEDURE dbo.Bucket_Site_Interval_Dtl_Ins
      ( 
       @Tvp_Bucket_Site_Interval_Dtl tvp_Bucket_Site_Interval_Dtl READONLY
      ,@Created_User_Id INT
      ,@Created_Ts DATETIME = NULL )
AS 
BEGIN  
      SET NOCOUNT ON ;  
  
      INSERT      INTO dbo.Bucket_Site_Interval_Dtl
                  ( 
                   Client_Hier_Id
                  ,Bucket_Master_Id
                  ,Service_Start_Dt
                  ,Service_End_Dt
                  ,Bucket_Daily_Avg_Value
                  ,Data_Source_Cd
                  ,Uom_Type_Id
                  ,Currency_Unit_Id
                  ,Created_User_Id
                  ,Updated_User_Id
                  ,Created_Ts
                  ,Last_Changed_Ts )
                  SELECT
                        Client_Hier_Id
                       ,Bucket_Master_Id
                       ,Service_Start_Dt
                       ,Service_End_Dt
                       ,Bucket_Daily_Avg_Value
                       ,Data_Source_Cd
                       ,Uom_Type_Id
                       ,Currency_Unit_Id
                       ,@Created_User_Id
                       ,@Created_User_Id
                       ,ISNULL(@Created_Ts, GETDATE())
                       ,ISNULL(@Created_Ts, GETDATE())
                  FROM
                        @tvp_Bucket_Site_Interval_Dtl  
  
END  
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Site_Interval_Dtl_Ins] TO [CBMSApplication]
GO
