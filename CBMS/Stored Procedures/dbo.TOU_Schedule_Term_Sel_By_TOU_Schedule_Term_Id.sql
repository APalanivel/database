SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Term_Id

DESCRIPTION:
	It is used to select tou schedule term by given tou term id.

INPUT PARAMETERS:
	Name						  DataType		Default	Description
------------------------------------------------------------
	@Time_Of_Use_Schedule_Term_Id INT


OUTPUT PARAMETERS:
	Name						  DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
					
Exec dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Term_Id 14
                   
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

	BCH			Balaraju Chalumuri

MODIFICATIONS

	Initials	Date		 Modification
-----------------------------------------------------------
	BCH			2012-07-017  Created
******/ 
CREATE PROCEDURE dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Term_Id
      @Time_Of_Use_Schedule_Term_Id INT
AS 
BEGIN
      SET NOCOUNT ON ;
      SELECT
            CONVERT(DATE, Start_Dt) AS Start_Dt
           ,CONVERT(DATE, End_Dt) AS End_Dt
           ,TOUST.CBMS_IMAGE_ID
      FROM
            dbo.Time_Of_Use_Schedule_Term TOUST
      WHERE
            Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
END
;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Sel_By_TOU_Schedule_Term_Id] TO [CBMSApplication]
GO
