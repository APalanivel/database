SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTRACT_TYPE_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_CONTRACT_TYPE_ID_P

as
set nocount on
DECLARE @contractTypeId int
SELECT @contractTypeId = (select entity_id 
	from entity 
	where entity_type = 129 and entity_name ='Supplier')



RETURN  @contractTypeId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTRACT_TYPE_ID_P] TO [CBMSApplication]
GO
