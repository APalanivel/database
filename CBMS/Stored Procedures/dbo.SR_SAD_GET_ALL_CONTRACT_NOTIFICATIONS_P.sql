
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 CBMS.dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name    DataType  Default Description  
------------------------------------------------------------  
 @user_id         INT                     
 @month_identifier DATETIME                
  
OUTPUT PARAMETERS:  
 Name    DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P 145,'03/01/2011'  
 EXEC dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P 18807,'04/01/2011'  
 EXEC dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P 33838,'2012-12-01'
 EXEC dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P 36847,'2013-11-28'
 EXEC dbo.SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P 35684,'2014-04-01' 

  
   
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 DMR		Deana  
 HG			Harihara Suthan G  
 BCH		Balaraju Chalumuri
 RR			Raghu Reddy  
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
			9/21/2010	Modify Quoted Identifier  
 DMR		09/10/2010  Modified for Quoted_Identifier  
 DMR		11/01/2010  Complete re-write for performance.  Eliminated Temp table that contained all sites  
						with no filter, scaler function in select clause, and use of views joining back to base tables.  
 HG			02/09/2011  Script modified to pull only the utility accounts to fix 22301  
 HG			02/16/2011  Vendor_Commodity_Map and Client_Hier_Account commodity id added as a part of JOIN clause to filter Commodities associated with the given analyst.  
 BCH        2012-10-26  Modified the code to consider Analyst Types and replaced hard coded values.  
 RR			2013-11-29	MAINT-2155 Selecting contract type to validate expiring contracts based on type also.
						Script is not differentiating type and if supplier contract is expiring but have future utility contract
						and this expiring supplier contract is not displaying in dashboard and vice versa.
 RR			2014-03-19	MAINT-2619 Script not returning/counting the contract expiring account(meter's account) if the account is having 
						active RFP for different commodity. Added commodity check condition.
 RR			2016-04-02	Global Sourcing - Phase3 - GCS-505 Script modified to send/display notifications for all commodities, currently
							notifications works for EP&NG only
						
******/  
CREATE PROCEDURE [dbo].[SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P]
      ( 
       @user_id INT
      ,@month_identifier DATETIME )
AS 
BEGIN  
  
      SET NOCOUNT ON;  
  
      DECLARE
            @month_end_identifier DATETIME
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Bid_Status_Type_Id INT
           ,@Service_Level_Type_Id INT  
             
      DECLARE @Accounts TABLE
            ( 
             Account_Id INT
            ,Vendor_name VARCHAR(200)
            ,client_name VARCHAR(200)
            ,site_name VARCHAR(200)
            ,account_number VARCHAR(50)
            ,CONTRACT_ID INT
            ,meter_id INT
            ,Analyst_Mapping_Cd INT
            ,Client_Id INT
            ,Site_Id INT
            ,Commodity_Id INT
            ,Account_Vendor_Id INT
            ,ED_CONTRACT_NUMBER VARCHAR(150)
            ,CONTRACT_START_DATE DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,CONTRACT_TYPE_ID INT ) 
             
             
      SET @month_end_identifier = dateadd(mm, 1, @month_identifier) - 1  
        
      SELECT
            @Default_Analyst = max(case WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = max(case WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'  
      SELECT
            @Bid_Status_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'BID_STATUS'
            AND ENTITY_NAME = 'Closed'  
             
              
      SELECT
            @Service_Level_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_DESCRIPTION = 'Other'
            AND ENTITY_NAME = 'D'  
      
      INSERT      INTO @Accounts
                  ( 
                   Account_Id
                  ,Vendor_name
                  ,client_name
                  ,site_name
                  ,account_number
                  ,CONTRACT_ID
                  ,meter_id
                  ,Analyst_Mapping_Cd
                  ,Client_Id
                  ,Site_Id
                  ,Commodity_Id
                  ,Account_Vendor_Id
                  ,ED_CONTRACT_NUMBER
                  ,CONTRACT_START_DATE
                  ,CONTRACT_END_DATE
                  ,CONTRACT_TYPE_ID )
                  SELECT
                        UCHA.Account_Id
                       ,UCHA.Account_vendor_name vendor_name
                       ,CH.client_name
                       ,rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')' site_name
                       ,UCHA.account_number
                       ,con.CONTRACT_ID
                       ,ucha.meter_id
                       ,coalesce(UCHA.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd
                       ,ch.Client_Id
                       ,ch.Site_Id
                       ,UCHA.Commodity_Id
                       ,ucha.Account_Vendor_Id
                       ,CON.ED_CONTRACT_NUMBER
                       ,CON.CONTRACT_START_DATE
                       ,CON.CONTRACT_END_DATE
                       ,CON.CONTRACT_TYPE_ID
                  FROM
                        Core.Client_Hier CH
                        INNER JOIN Core.Client_Hier_Account UCHA
                              ON CH.Client_Hier_Id = UCHA.Client_Hier_Id
                        INNER JOIN dbo.supplier_account_meter_map SAMM
                              ON UCHA.Meter_Id = SAMM.METER_ID
                        INNER JOIN Contract CON
                              ON SAMM.Contract_ID = CON.CONTRACT_ID
                  WHERE
                        UCHA.Account_Type = 'Utility'
                        AND con.contract_end_date >= @month_identifier
                        AND UCHA.Account_not_managed = 0
                        AND UCHA.Account_Service_level_Cd != @Service_Level_Type_Id
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.SR_RFP_ACCOUNT AS rfp_account
                                          JOIN dbo.SR_RFP rfp
                                                ON rfp_account.SR_RFP_ID = rfp.SR_RFP_ID
                                         WHERE
                                          rfp_account.Account_Id = UCHA.Account_Id
                                          AND UCHA.Commodity_Id = rfp.COMMODITY_TYPE_ID
                                          AND rfp_account.IS_DELETED = 0
                                          AND rfp_account.BID_STATUS_TYPE_ID != @Bid_Status_Type_Id )
                  GROUP BY
                        UCHA.Account_Id
                       ,UCHA.Account_vendor_name
                       ,CH.client_name
                       ,CH.City
                       ,CH.State_Name
                       ,CH.Site_name
                       ,UCHA.account_number
                       ,con.CONTRACT_ID
                       ,ucha.meter_id
                       ,UCHA.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,ch.Client_Id
                       ,ch.Site_Id
                       ,UCHA.Commodity_Id
                       ,ucha.Account_Vendor_Id
                       ,CON.ED_CONTRACT_NUMBER
                       ,CON.CONTRACT_START_DATE
                       ,CON.CONTRACT_END_DATE
                       ,CON.CONTRACT_TYPE_ID;
                       
      WITH  Cte_Accout_User
              AS ( SELECT
                        acc.vendor_name
                       ,acc.client_name
                       ,acc.site_name
                       ,acc.account_number
                       ,acc.contract_id
                       ,acc.meter_id
                       ,acc.ED_Contract_Number
                       ,acc.Contract_End_Date
                       ,case WHEN acc.Analyst_Mapping_Cd = @Default_Analyst THEN VCMap.Analyst_Id
                             WHEN acc.Analyst_Mapping_Cd = @Custom_Analyst THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS user_id
                       ,acc.CONTRACT_TYPE_ID
                   FROM
                        @Accounts acc
                        LEFT JOIN dbo.Vendor_Commodity_Map VCM
                              ON acc.Account_Vendor_ID = VCM.VENDOR_ID
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map VCMap
                              ON VCM.VENDOR_COMMODITY_MAP_ID = VCMap.Vendor_Commodity_Map_Id
                        JOIN core.Client_Commodity ccc
                              ON ccc.Commodity_Id = acc.Commodity_Id
                                 AND ccc.Client_Id = acc.Client_Id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = acc.Account_Id
                                 AND aca.Commodity_Id = acc.Commodity_Id
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON sca.Site_Id = acc.Site_Id
                                 AND sca.Commodity_Id = acc.Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON cca.Client_Commodity_Id = ccc.Client_Commodity_Id)
            SELECT
                  cau.Vendor_Name
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_number
                 ,cau.ED_Contract_Number
                 ,cau.contract_id
                 ,cau.Contract_End_Date
            FROM
                  Cte_Accout_User cau
                  JOIN ( SELECT
                              Meter_ID
                             ,max(CONTRACT_END_DATE) MaxEndDate
                             ,CONTRACT_TYPE_ID
                         FROM
                              Cte_Accout_User
                         GROUP BY
                              Meter_ID
                             ,CONTRACT_TYPE_ID ) cmme
                        ON cau.Meter_Id = cmme.Meter_Id
                           AND cau.CONTRACT_TYPE_ID = cmme.CONTRACT_TYPE_ID
                           AND cau.CONTRACT_END_DATE = cmme.MaxEndDate
            WHERE
                  cau.CONTRACT_END_DATE BETWEEN @month_identifier
                                        AND     @month_end_identifier
                  AND cau.User_Id = @user_Id
            GROUP BY
                  cau.Vendor_Name
                 ,cau.client_name
                 ,cau.site_name
                 ,cau.account_number
                 ,cau.ED_Contract_Number
                 ,cau.contract_id
                 ,cau.Contract_End_Date
                 ,cau.CONTRACT_TYPE_ID
            ORDER BY
                  cau.vendor_name
                 ,cau.site_name
                 ,cau.contract_id
      OPTION
                  ( RECOMPILE )  
  
  
END;
;

;
GO




GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ALL_CONTRACT_NOTIFICATIONS_P] TO [CBMSApplication]
GO
