SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Budget_Detail_Comments_Del]  
     
DESCRIPTION: 
	It Deletes Budget Detail Comments for Selected Budget Detail Comment Id.
      
INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------         
@Budget_Detail_Comments_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC Budget_Detail_Comments_Del 2051

	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Budget_Detail_Comments_Del
    (
      @Budget_Detail_Comments_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.BUDGET_DETAIL_COMMENTS
	WHERE
		BUDGET_DETAIL_COMMENTS_ID = @Budget_Detail_Comments_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Detail_Comments_Del] TO [CBMSApplication]
GO
