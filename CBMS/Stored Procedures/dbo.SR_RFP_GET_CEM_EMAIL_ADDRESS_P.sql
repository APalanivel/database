SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CEM_EMAIL_ADDRESS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@rfp_account_group_id	int       	          	
	@is_bid_group  	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_RFP_GET_CEM_EMAIL_ADDRESS_P '1', '1', 4935, 0

-- select * from sr_rfp_account where sr_rfp_bid_group_id = 1
-- select * from sr_rfp_bid_group where sr_rfp_bid_group_id = 1
-- select * from sr_rfp_account
-- select * from client where client_id = 10015
-- select * from sr_rfp_account where account_id in(6259, 12119)

CREATE    PROCEDURE dbo.SR_RFP_GET_CEM_EMAIL_ADDRESS_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_account_group_id int,
	@is_bid_group bit
	AS
set nocount on

	if @is_bid_group = 1
	
		select 	userinfo.EMAIL_ADDRESS
		
		from 	sr_rfp_account rfp_account(nolock),
			sr_rfp_bid_group bid_group(nolock),
			client c(nolock), 
			division d(nolock),
			site s(nolock), 
			vwSiteName vwSite(nolock),
			account a(nolock),
			CLIENT_CEM_MAP cemMap(nolock),
			USER_INFO userinfo(nolock)

		where 	rfp_account.sr_rfp_bid_group_id = @rfp_account_group_id 
			and bid_group.sr_rfp_bid_group_id = rfp_account.sr_rfp_bid_group_id
			and rfp_account.is_deleted  = 0
			and a.account_id = rfp_account.account_id
			and s.site_id = a.site_id
			and vwSite.site_id = s.site_id
			and d.division_id = s.division_id
			and c.client_id = d.client_id
			AND cemMap.CLIENT_ID = c.client_id 
			AND cemMap.USER_INFO_ID = userinfo.USER_INFO_ID


	else 
	
		select 	userinfo.EMAIL_ADDRESS
		
		from 	sr_rfp_account rfp_account(nolock),
			client c(nolock), 
			division d(nolock),
			site s(nolock), 
			vwSiteName vwSite(nolock),
			account a(nolock),
			CLIENT_CEM_MAP cemMap(nolock),
			USER_INFO userinfo(nolock)
			
			 
		where 	rfp_account.sr_rfp_account_id = @rfp_account_group_id
			and rfp_account.sr_rfp_bid_group_id is null
			and rfp_account.is_deleted  = 0
			and a.account_id = rfp_account.account_id
			and s.site_id = a.site_id
			and vwSite.site_id = s.site_id
			and d.division_id = s.division_id
			and c.client_id = d.client_id
			and cemMap.CLIENT_ID = c.client_id 
			and cemMap.USER_INFO_ID = userinfo.USER_INFO_ID
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CEM_EMAIL_ADDRESS_P] TO [CBMSApplication]
GO
