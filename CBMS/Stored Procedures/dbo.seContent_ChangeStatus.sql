SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   Procedure dbo.seContent_ChangeStatus
	(
		@ContentId int
	, @StatusId int
	, @AccountId int
	)
As
BEGIN

	set nocount on

		declare @OrigEntityId int
		declare @ThisVersion int
		declare @EntityTypeId int

		set @EntityTypeId = dbo.dmlLookup_GetId ('Audit Entity Type', 'Content')

		select @ThisVersion = Version
		  from seContent
		 where ContentId = @ContentId

	  select @OrigEntityId = s.ContentId
	    from seContent s
	    join (select ContentLocaleId
								 , ContentPlacementId
	            from seContent
	           where ContentId = @ContentId
	          ) x on s.ContentLocaleId = x.ContentLocaleId and s.ContentPlacementId = x.ContentPlacementId
	   where s.Version = 1
	
		update seContent
		   set CurrentStatusId = @StatusId
		 where ContentId = @ContentId

		exec dmlAuditLog_Save @EntityTypeId, @OrigEntityId, @ContentId, @ThisVersion, @StatusId, @AccountId
	
	exec seContent_Get @ContentId

END
GO
GRANT EXECUTE ON  [dbo].[seContent_ChangeStatus] TO [CBMSApplication]
GO
