SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Cu_Exception_Denorm_Queue_Upd_By_Invoice_Account_Commodity                     
                          
 DESCRIPTION:      
		  To update queue in CU_EXCEPTION_DENORM for the account,commodity,invoice                       
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
------------------------------------------------------------------------------------    
 @Cu_Invoice_Id						INT
 @Queue_id							INT
 @Account_ID						INT
 @Commodity_Id						INT
 @Exception_Type					VARCHAR(200)                 
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
------------------------------------------------------------------------------------    
               
	 BEGIN TRAN        
	 SELECT * FROM dbo.CU_EXCEPTION_DENORM where Account_Id=1021768 and Commodity_Id= 291 and exception_type='Failed Recalc' and cu_invoice_id=35159159             
	 
	 EXEC [dbo].[Cu_Exception_Denorm_Queue_Upd_By_Invoice_Account_Commodity]  
	  @Cu_Invoice_Id =35159159
      ,@Queue_id =153
      ,@Account_ID =1021768
      ,@Commodity_Id =291
      ,@Exception_Type ='Failed Recalc'
	 SELECT * FROM dbo.CU_EXCEPTION_DENORM where Account_Id=1021768 and Commodity_Id= 291 and exception_type='Failed Recalc' and cu_invoice_id=35159159             
	 ROLLBACK   
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
------------------------------------------------------------------------------------    
 NR                     2013-12-30      Created for MAINT-4115                     
                         
******/ 

 CREATE PROCEDURE [dbo].[Cu_Exception_Denorm_Queue_Upd_By_Invoice_Account_Commodity]
      ( 
       @Cu_Invoice_Id INT
      ,@Queue_id INT
      ,@Account_ID INT
      ,@Commodity_Id INT
      ,@Exception_Type VARCHAR(200) )
 AS 
 BEGIN  
  
      SET NOCOUNT ON  
      
      UPDATE
            ced
      SET   
            QUEUE_ID = @Queue_id
      FROM
            dbo.CU_EXCEPTION_DENORM ced
      WHERE
            ced.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ced.Account_ID = @Account_ID
            AND ced.Commodity_Id = @Commodity_Id
            AND ced.EXCEPTION_TYPE = @Exception_Type      
  
   
  
 END;

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Denorm_Queue_Upd_By_Invoice_Account_Commodity] TO [CBMSApplication]
GO
