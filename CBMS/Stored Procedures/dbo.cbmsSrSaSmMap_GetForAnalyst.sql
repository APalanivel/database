SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[cbmsSrSaSmMap_GetForAnalyst]
	( @MyAccountId int
	, @sourcing_analyst_id int
	)
AS
BEGIN
   select x.sourcing_analyst_id
	, a.email_address sourcing_analyst_email
	, a.first_name + ' ' + a.last_name sourcing_analyst
	, x.sourcing_manager_id
	, m.email_address sourcing_manager_email
	, m.first_name + ' ' + m.last_name sourcing_manager
     from sr_sa_sm_map x
     join user_info a on a.user_info_id = x.sourcing_analyst_id
     join user_info m on m.user_info_id = x.sourcing_manager_id
    where x.sourcing_analyst_id = @sourcing_analyst_id

END




GO
GRANT EXECUTE ON  [dbo].[cbmsSrSaSmMap_GetForAnalyst] TO [CBMSApplication]
GO
