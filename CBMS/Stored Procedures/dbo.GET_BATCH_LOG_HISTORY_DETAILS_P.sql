SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_BATCH_LOG_HISTORY_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_BATCH_LOG_HISTORY_DETAILS_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int
AS
select 
	list.report_name,
	cli.client_name,
	cli.client_id,
	div.division_name,
	div.division_id,
	sit.site_name,
	sit.site_id, 
	batchLog.log_time
from 
	rm_report_batch_log batchLog
	left join division div on(div.division_id = batchLog.division_id)
	left join site sit on(sit.site_id = batchLog.site_id),
	rm_report_batch batch,
	rm_report report,
	report_list list,
	client cli
where 
	batch.rm_report_batch_id = @batchId
	and batchLog.rm_report_batch_id = batch.rm_report_batch_id
	and report.rm_report_id = batchLog.rm_report_id
	and list.report_list_id = report.report_list_id
	and cli.client_id = batchLog.client_id
GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_LOG_HISTORY_DETAILS_P] TO [CBMSApplication]
GO
