SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 cbms_prod.dbo.cbmsCuException_GetForDetBucketMapping    
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @ubm_id         int                       
 @ubm_bucket_code varchar(200)                
 @commodity_type_id int                       
    
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
 EXEC dbo.cbmsCuException_GetForDetSubBucketMapping 2,'USAGE-Water',67, 'ABC' , 6    
 EXEC dbo.cbmsCuException_GetForDetSubBucketMapping 9, 'Energy Transmission',291    
    
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
SSR			Sharad srivastava    
RKV			Ravi Kumar Vegesna    
NR			Narayana Reddy    

MODIFICATIONS       
 Initials	Date  Modification    
------------------------------------------------------------    

******/
CREATE PROCEDURE [dbo].[cbmsCuException_GetForDetSubBucketMapping]
(
	@ubm_id				   INT
	, @ubm_bucket_code	   VARCHAR(200)
	, @commodity_type_id   INT
	, @Ubm_Sub_Bucket_Code VARCHAR(200)
	, @State_Id			   INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE
		@New_Exception_Status_Cd INT
		, @Sub_Bkt_Determinant_Mapping_Cd INT;

	SELECT
		@Sub_Bkt_Determinant_Mapping_Cd = cd.Code_Id
	FROM
		dbo.Codeset cs
		INNER JOIN dbo.Code cd
			ON cd.Codeset_Id = cs.Codeset_Id
	WHERE
		cd.Code_Value = 'Sub-Bkt Determnt Mapping' AND cs.Codeset_Name = 'Exception Type';

	SELECT
		@New_Exception_Status_Cd = cd.Code_Id
	FROM
		dbo.Codeset cs
		INNER JOIN dbo.Code cd
			ON cd.Codeset_Id = cs.Codeset_Id
	WHERE
		cd.Code_Value = 'New' AND cs.Codeset_Name = 'Exception Status';

	SELECT
		i.CU_INVOICE_ID
		, ed.Account_Id
		, ed.Commodity_Id AS commodity_id
		, e.Exception_Type_Cd
		, 'New' AS exception_type
	FROM
		dbo.CU_INVOICE i
		INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
			ON i.CU_INVOICE_ID = cism.CU_INVOICE_ID
		INNER JOIN Core.Client_Hier_Account cha
			ON cism.Account_ID = cha.Account_Id
		INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception e
			ON e.Cu_Invoice_Id = i.CU_INVOICE_ID
		INNER JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl ed
			ON ed.Cu_Invoice_Standing_Data_Exception_Id = e.Cu_Invoice_Standing_Data_Exception_Id
	WHERE
		i.UBM_ID = @ubm_id
		AND ed.Status_Cd = @New_Exception_Status_Cd
		AND e.Exception_Type_Cd = @Sub_Bkt_Determinant_Mapping_Cd
		AND (
				@State_Id IS NULL OR cha.Meter_State_Id = @State_Id
			)
		AND EXISTS (
					   SELECT
						   1
					   FROM
						   dbo.CU_INVOICE_DETERMINANT d
					   WHERE
						   d.CU_INVOICE_ID = i.CU_INVOICE_ID
						   AND d.COMMODITY_TYPE_ID = @commodity_type_id
						   AND d.UBM_BUCKET_CODE = @ubm_bucket_code
						   AND (
								   @Ubm_Sub_Bucket_Code IS NULL OR d.Ubm_Sub_Bucket_Code = @Ubm_Sub_Bucket_Code
							   )
						   AND (
								   d.Bucket_Master_Id IS NOT NULL AND d.EC_Invoice_Sub_Bucket_Master_Id IS NULL
							   )
				   )
	GROUP BY
		i.CU_INVOICE_ID
		, ed.Account_Id
		, ed.Commodity_Id
		, e.Exception_Type_Cd;


END;

GO
GRANT EXECUTE ON  [dbo].[cbmsCuException_GetForDetSubBucketMapping] TO [CBMSApplication]
GO
