SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
NAME:      
 [Workflow].[Client_Invoice_Queue_DynamicPriorityReport]    
      
DESCRIPTION:      
      
      
INPUT PARAMETERS:      
 Name     DataType   Default   Description      
----------------------------------------------------------------------------------      
 @account_id    int                         
      
  
OUTPUT PARAMETERS:      
 Name     DataType   Default   Description      
----------------------------------------------------------------------------------      
      
  
USAGE EXAMPLES:      
----------------------------------------------------------------------------------      
  
AUTHOR INITIALS:      
Initials Name  
----------------------------------------------------------------------------------      
  
TRK   Ramakrishna Thummala  
      
MODIFICATIONS      
      
Initials Date  Modification      
----------------------------------------------------------------------------------      
TRK   2020-02-03 Creation  
   
******/


CREATE PROCEDURE [Workflow].[Client_Invoice_Queue_DynamicPriorityReport]
AS
      BEGIN
            -- SET NOCOUNT ON added to prevent extra result sets from          
            -- interfering with SELECT statements.          
            SET NOCOUNT ON;

            DECLARE
                  @Code_Id    INT
                , @ReportDays INT;

            SELECT
                  @Code_Id = Code_Id
            FROM  Code
            WHERE Code_Value = 'Systempriorityclient';

            SELECT
                  @ReportDays = c.Code_Value
            FROM  Code c
                , Codeset cs
            WHERE c.Codeset_Id = cs.Codeset_Id
                  AND   Codeset_Name = 'Invoice Priority report';


            WITH cte
            AS ( SELECT
                        row_number() OVER  ( ORDER BY A.priority_int DESC
                                                    , A.[Last Updated On] DESC ) AS rn
                      , A.[Client Name]
                      , A.Priority
                      , A.priority_int
                      , A.[Priority Invoice Count (System Priority)]
                      , A.[Last Updated By]
                      , convert(VARCHAR, A.[Last Updated On], 106) AS [Last Updated On]
                 FROM
                        (     SELECT
                                    c.CLIENT_NAME AS [Client Name]
                                  , ( CASE WHEN Mark_Invoice_Exception_As_Priority = 1
                                                 THEN 'Yes'
                                           ELSE  'No'
                                      END ) AS [Priority]
                                  , Mark_Invoice_Exception_As_Priority AS priority_int
                                  , isnull(system_priority_total_count, 0) [Priority Invoice Count (System Priority)]
                                  , ui_create.FIRST_NAME + ' ' + ui_create.LAST_NAME AS [Last Updated By]
                                  , ca.Updated_Ts AS [Last Updated On]
                              FROM  [dbo].[Client_Attribute] ca
                                    INNER JOIN
                                    CLIENT AS c
                                          ON ca.Client_Id = c.CLIENT_ID
                                    INNER JOIN
                                    USER_INFO AS ui_update
                                          ON ui_update.USER_INFO_ID = ca.Updated_User_Id
                                    INNER JOIN
                                    USER_INFO AS ui_create
                                          ON ui_create.USER_INFO_ID = ca.Created_Uer_Id
                                    LEFT JOIN
                                          (     SELECT
                                                            ci.CLIENT_ID
                                                          , count(DISTINCT ci.CU_INVOICE_ID) AS system_priority_total_count
                                                FROM        [dbo].[Client_Attribute] ca
                                                            INNER JOIN
                                                            CU_INVOICE AS ci
                                                                  ON ci.CLIENT_ID = ca.Client_Id
                                                            LEFT JOIN
                                                            Workflow.Cu_Invoice_Attribute AS cia
                                                                  ON cia.CU_INVOICE_ID = ci.CU_INVOICE_ID
                                                WHERE       Source_Cd = @Code_Id
                                                            AND   isnull(Is_Priority, 0) = 1
                                                            AND   IS_PROCESSED != 1
                                                            AND   IS_REPORTED != 1
                                                GROUP BY    ci.CLIENT_ID ) AS client_pr
                                                ON client_pr.CLIENT_ID = c.CLIENT_ID
                              WHERE ca.Mark_Invoice_Exception_As_Priority = 1
                              UNION
                              SELECT
                                    c.CLIENT_NAME AS [Client Name]
                                  , ( CASE WHEN Mark_Invoice_Exception_As_Priority = 1
                                                 THEN 'Yes'
                                           ELSE  'No'
                                      END ) AS [Priority]
                                  , Mark_Invoice_Exception_As_Priority AS priority_int
                                  , isnull(system_priority_total_count, 0) [Priority Invoice Count (System Priority)]
                                  , ui_create.FIRST_NAME + ' ' + ui_create.LAST_NAME AS [Last Updated By]
                                  , ca.Updated_Ts AS [Last Updated On]
                              FROM  [dbo].[Client_Attribute] ca
                                    INNER JOIN
                                    CLIENT AS c
                                          ON ca.Client_Id = c.CLIENT_ID
                                    INNER JOIN
                                    USER_INFO AS ui_update
                                          ON ui_update.USER_INFO_ID = ca.Updated_User_Id
                                    INNER JOIN
                                    USER_INFO AS ui_create
                                          ON ui_create.USER_INFO_ID = ca.Created_Uer_Id
                                    LEFT JOIN
                                          (     SELECT
                                                            ci.CLIENT_ID
                                                          , count(DISTINCT ci.CU_INVOICE_ID) AS system_priority_total_count
                                                FROM        [dbo].[Client_Attribute] ca
                                                            INNER JOIN
                                                            CU_INVOICE AS ci
                                                                  ON ci.CLIENT_ID = ca.Client_Id
                                                            LEFT JOIN
                                                            Workflow.Cu_Invoice_Attribute AS cia
                                                                  ON cia.CU_INVOICE_ID = ci.CU_INVOICE_ID
                                                WHERE       Source_Cd = @Code_Id
                                                            AND   isnull(Is_Priority, 0) = 1
                                                            AND   IS_PROCESSED != 1
                                                            AND   IS_REPORTED != 1
                                                GROUP BY    ci.CLIENT_ID ) AS client_pr
                                                ON client_pr.CLIENT_ID = c.CLIENT_ID
                              WHERE datediff(dd, Updated_Ts, getdate()) <= @ReportDays
                                    AND   ca.Mark_Invoice_Exception_As_Priority = 0 ) A )
            SELECT
                        cte.[Client Name]
                      , cte.Priority
                      , cte.[Priority Invoice Count (System Priority)]
                      , cte.[Last Updated By]
                      , cte.[Last Updated On]
            FROM        cte
            ORDER BY    rn;



      END;
GO
GRANT EXECUTE ON  [Workflow].[Client_Invoice_Queue_DynamicPriorityReport] TO [CBMSApplication]
GO
