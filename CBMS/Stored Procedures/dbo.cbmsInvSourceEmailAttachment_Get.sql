SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[cbmsInvSourceEmailAttachment_Get]
	( @MyAccountId int
	, @inv_source_email_attachment_id int
	)
AS
BEGIN

	   select inv_source_email_attachment_id
		, inv_source_email_log_id
		, attachment_filename
		, final_filename
	     from inv_source_email_attachment with (nolock)
	    where inv_source_email_attachment_id = @inv_source_email_attachment_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourceEmailAttachment_Get] TO [CBMSApplication]
GO
