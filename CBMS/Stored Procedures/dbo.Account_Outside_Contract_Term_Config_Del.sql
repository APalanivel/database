SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Outside_Contract_Term_Config_Del          
              
Description:              
        To insert Data into  table. 
             
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Account_Outside_Contract_Term_Config_Id						INT
    
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  

	SELECT * FROM Account_Outside_Contract_Term_Config WHERE Account_Outside_Contract_Term_Config_Id =1148520

    EXEC dbo.Account_Outside_Contract_Term_Config_Del 
      @Account_Outside_Contract_Term_Config_Id =1148520    
    
	SELECT * FROM Account_Outside_Contract_Term_Config WHERE Account_Outside_Contract_Term_Config_Id =1148520

	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-06-26		Created For Add contract.   
             
******/

CREATE PROCEDURE [dbo].[Account_Outside_Contract_Term_Config_Del]
    (
        @Account_Outside_Contract_Term_Config_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        cloct
        FROM
            dbo.Account_Outside_Contract_Term_Config cloct
        WHERE
            cloct.Account_Outside_Contract_Term_Config_Id = @Account_Outside_Contract_Term_Config_Id;



    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Outside_Contract_Term_Config_Del] TO [CBMSApplication]
GO
