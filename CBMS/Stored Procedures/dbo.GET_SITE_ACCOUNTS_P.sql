SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--for volume variance report
CREATE   PROCEDURE dbo.GET_SITE_ACCOUNTS_P
@userId varchar,
@sessionId varchar,
@siteId int

as
	set nocount on
	select count(*) no_of_accounts , site_id from account acc,vendor_commodity_map map where site_id = @siteId 
	and not_expected = 0 
	and map.vendor_id=acc.vendor_id
	and map.commodity_type_id = (select ENTITY_ID from ENTITY WHERE ENTITY_NAME='Natural Gas' and ENTITY_TYPE= 157)

	group by site_id
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_ACCOUNTS_P] TO [CBMSApplication]
GO
