SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoProject_Delete

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_project_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    procedure [dbo].[cbmsSsoProject_Delete]
	( @MyAccountId int
	, @sso_project_id int
	)
AS
BEGIN

	set nocount on

	delete sso_project_owner_map where sso_project_id = @sso_project_id
	delete sso_project_activity where sso_project_id = @sso_project_id
	delete sso_project where sso_project_id = @sso_project_id

	exec cbmsEntityAudit_Log @MyAccountId, 'sso_project', @sso_project_id, 'Delete'
	

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_Delete] TO [CBMSApplication]
GO
