SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_CURRENCY_ID_P
	@contract_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT currency_unit_id
	FROM dbo.[contract]
	WHERE contract_id=@contract_id

END
GO
GRANT EXECUTE ON  [dbo].[GET_CURRENCY_ID_P] TO [CBMSApplication]
GO
