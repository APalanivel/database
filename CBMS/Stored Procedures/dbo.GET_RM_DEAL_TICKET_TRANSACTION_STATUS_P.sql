SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GET_RM_DEAL_TICKET_TRANSACTION_STATUS_P]
	@emailName VARCHAR(200),
	@emailType INT,   
	@dealTicketId INT,
	@dealTicketStatusType INT,
	@dealTicketStatus VARCHAR(200),   
	@emailTypeId INT OUTPUT,  
	@pkey INT OUTPUT,  
	@cbmsImageId INT OUTPUT
AS
BEGIN

	SET NOCOUNT ON
 
	DECLARE @dealTicketStatusId INT		
	
	--update cbms_image set cbms_image="" where cbms_image_id = 0
		
	SELECT @emailTypeId = ENTITY_ID FROM dbo.ENTITY (NOLOCK)
	WHERE ENTITY_TYPE = @emailType
		AND ENTITY_NAME = @emailName

	SELECT @dealTicketStatusId = ENTITY_ID FROM dbo.ENTITY (NOLOCK)
	WHERE ENTITY_TYPE = @dealTicketStatusType
		AND ENTITY_NAME = @dealTicketStatus

	SELECT @pkey = RM_DEAL_TICKET_TRANSACTION_STATUS_ID
		, @cbmsImageId = CBMS_IMAGE_ID
	FROM dbo.RM_DEAL_TICKET_TRANSACTION_STATUS
	WHERE RM_DEAL_TICKET_ID = @dealTicketId
		AND DEAL_TRANSACTION_STATUS_TYPE_ID = @dealTicketStatusId

	--SELECT @emailTypeId emailTypeId, @pkey pkey, @cbmsImageId cbmsImageId

	RETURN

END
GO
GRANT EXECUTE ON  [dbo].[GET_RM_DEAL_TICKET_TRANSACTION_STATUS_P] TO [CBMSApplication]
GO
