SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_SAD_GET_DEAL_TICKETS_COUNT_P
	@user_id varchar(10),
	@session_id varchar(20)
	AS
	set nocount on
	select 	count(deal_ticket.sr_deal_ticket_id) as deal_tickets
	from	sr_deal_ticket deal_ticket(nolock),
		sr_deal_ticket_details details(nolock)
	where	details.sr_deal_ticket_id = deal_ticket.sr_deal_ticket_id
		and details.with_whom = @user_id
		and details.deal_status_type_id != (select entity_id from entity(nolock) where entity_type = 1016 and entity_name = 'Closed/Cancelled')
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DEAL_TICKETS_COUNT_P] TO [CBMSApplication]
GO
