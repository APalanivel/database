SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Ubm_Client_Name_Sel_By_Cu_Invoice_Id]  
     
DESCRIPTION: 
	To Get Ubm Client Code and Ubm Client Name for the given Cu_Invoice Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Cu_Invoice_Id	INT

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------

	EXEC dbo.Ubm_Client_Name_Sel_By_Cu_Invoice_Id 123391
	
	EXEC dbo.Ubm_Client_Name_Sel_By_Cu_Invoice_Id 123395
    
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			Pandarinath
          
MODIFICATIONS           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			08/20/2010	Created
*/

CREATE PROCEDURE dbo.Ubm_Client_Name_Sel_By_Cu_Invoice_Id
	 @Cu_Invoice_Id		INT
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		 ci.UBM_CLIENT_CODE
		,ui.UBM_CLIENT_NAME
	FROM
		dbo.CU_INVOICE ci
		JOIN dbo.UBM_INVOICE ui
			 ON ci.UBM_INVOICE_ID = ui.UBM_INVOICE_ID
	WHERE
		ci.CU_INVOICE_ID = @Cu_Invoice_Id
END
GO
GRANT EXECUTE ON  [dbo].[Ubm_Client_Name_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
