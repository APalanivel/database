SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Workflow_Status_Message_Map_Ins                    
                      
Description:                      
        To get deal ticket workflows
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Client_Workflow_Status_Message_Map_Ins 235,1

Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2019-03-06     Global Risk Management - Created 
                     
******/

CREATE PROCEDURE [dbo].[RM_Client_Workflow_Status_Message_Map_Ins]
      ( 
       @Client_Id INT
      ,@Workflow_Id INT
      ,@tvp_RM_Client_Workflow_Status_Message_Map AS dbo.tvp_RM_Client_Workflow_Status_Message_Map READONLY
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      
      
      MERGE INTO dbo.RM_Client_Workflow_Status_Message_Map AS tgt
            USING 
                  ( SELECT
                        @Client_Id AS Client_Id
                       ,Workflow_Status_Map_Id
                       ,Workflow_Status_Message
                    FROM
                        @tvp_RM_Client_Workflow_Status_Message_Map
                    GROUP BY
                        Workflow_Status_Map_Id
                       ,Workflow_Status_Message ) AS src
            ON tgt.Client_Id = src.Client_Id
                  AND tgt.Workflow_Status_Map_Id = src.Workflow_Status_Map_Id
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Workflow_Status_Message = NULLIF(src.Workflow_Status_Message, '')
                       ,tgt.Updated_User_Id = @User_Info_Id
                       ,tgt.Last_Change_Ts = GETDATE()
            WHEN NOT MATCHED BY TARGET 
                  THEN
                   INSERT
                        ( 
                         Client_Id
                        ,Workflow_Status_Map_Id
                        ,Workflow_Status_Message
                        ,Created_User_Id
                        ,Created_Ts
                        ,Updated_User_Id
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Client_Id
                        ,src.Workflow_Status_Map_Id
                        ,NULLIF(src.Workflow_Status_Message, '')
                        ,@User_Info_Id
                        ,GETDATE()
                        ,@User_Info_Id
                        ,GETDATE() );
      
     
END;

GO
GRANT EXECUTE ON  [dbo].[RM_Client_Workflow_Status_Message_Map_Ins] TO [CBMSApplication]
GO
