
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
       
/******            
NAME:   etl.Site_Commodity_Image_Get_Changes         
  
      
DESCRIPTION:    
 Gets changes from Site_Commodity_image based on Change tracking          
   
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
 EXEC etl.Site_Commodity_Image_Get_Changes  
         
       
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
CMH   Chad Hattabaugh    
AKR   Ashok Kumar Raju
DMR   Deana Ritter
       
MODIFICATIONS             
Initials Date Modification            
------------------------------------------------------------            
CMH   12/29/2010 Created  
AKR   2012-08-29 Modified to include Is_Reported Flag
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Causing transaction logs to fill up.	
*****/   
CREATE PROCEDURE [ETL].[Site_Commodity_Image_Get_Changes]
      ( 
       @in_Last_Processed BIGINT = 0 )
AS 
BEGIN  
      
      SELECT
            ch.Client_Hier_Id
           ,0 AS Account_id
           ,0 AS CU_Invoice_Id
           ,cng.Commodity_id
           ,cng.Service_Month
           ,sci.CBMS_IMAGE_ID
           ,CONVERT(BIT, NULL) AS Is_Reported
           ,CONVERT(CHAR(1), cng.Sys_CHANGE_OPERATION) AS sys_Change_Operation
           ,'Site_Commodity_Image_Get_Changes' AS DATA_Source
      FROM
            CHANGETABLE(CHANGES Site_Commodity_Image, @in_last_Processed) cng
            INNER JOIN Core.Client_Hier ch
                  ON cng.SITE_id = ch.Site_id
            LEFT JOIN dbo.Site_Commodity_Image sci
                  ON sci.SITE_ID = cng.Site_id
                     AND sci.Commodity_Id = cng.Commodity_Id
                     AND sci.Service_Month = cng.Service_Month
      GROUP BY
            ch.Client_Hier_Id
           ,cng.Commodity_id
           ,cng.Service_Month
           ,sci.CBMS_IMAGE_ID
           ,cng.Sys_CHANGE_OPERATION   
   
END   


;
;
GO


GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_Get_Changes] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_Get_Changes] TO [ETL_Execute]
GO
