SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********        
NAME:        
 dbo.IntervalsForYear        

DESCRIPTION: get the conversion factor when commodity_id and emission_factor is modified from base     
         

INPUT PARAMETERS:        
 Name					DataType  Default Description        
------------------------------------------------------------        
@commodity_id				INT,
@base_commodity_uom_cd		INT,
@converted_commodity_uom_cd INT,
@base_emission_uom_cd		INT,
@converted_emission_uom_cd	INT 

OUTPUT PARAMETERS:

Name					DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
      
	exec dbo.commodity_UOM_conversion_Sel_By_emission_factor(1,90,90,94,148)        
      
     
 AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
 GB   Geetansu Behera    
   
 MODIFICATIONS:  
  
Initials	Date		Modification  
SKA		06-Sep-09	Eliminated  @conversion_Factor variable and show the value directly
					Removed the where clause from final select statement.
CMH		09/15/2009	Added parameter @Co2_UOM_Name to allow for conversion to different UOM's


      
------------------------------------------------------------        
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[commodity_UOM_conversion_Sel_By_emission_factor]
      (
       @commodity_id INT
      ,@base_commodity_uom_cd INT
      ,@converted_commodity_uom_cd INT
      ,@base_emission_uom_cd INT
      ,@converted_emission_uom_cd INT
      ,@C02_UOM_name varchar(25)
		
      )
AS 

BEGIN        
      DECLARE
            @Commodity_Conversion_factor DECIMAL(16, 8) = NULL
           ,@conversion_factor_to_co2e DECIMAL(16, 8)= NULL
           ,@conversion_factor_from_co2e DECIMAL(16, 8)= NULL
           ,@co2e_uom_cd int

      SELECT
            @Commodity_Conversion_factor = ROUND(Conversion_Factor, 8)
      FROM
            dbo.COMMODITY_UOM_CONVERSION
      WHERE
            Base_Commodity_Id = @commodity_id
            AND Base_UOM_Cd = @base_commodity_uom_cd
            AND Converted_Commodity_Id = @commodity_id
            AND Converted_UOM_Cd = @converted_commodity_uom_cd

      SELECT
            @co2e_uom_cd = c.CODE_ID
      from
            CODE c
            inner join CODESET cs ON c.CODESET_ID = cs.CODESET_ID
                                     AND cs.STD_COLUMN_NAME = 'UOM_cd'
                                     and c.CODE_VALUE = @C02_UOM_name
		
      SELECT
            @conversion_factor_to_co2e = ROUND(1 / Conversion_Factor, 8)
      FROM
            dbo.General_UOM_Conversion guc
      WHERE
            guc.BASE_UOM_CD = @co2e_uom_cd
            AND guc.Converted_UOM_Cd = @base_emission_uom_cd
		
      SELECT
            @conversion_factor_from_co2e = ROUND(Conversion_Factor, 8)
      FROM
            dbo.General_UOM_Conversion guc
      WHERE
            guc.BASE_UOM_CD = @co2e_uom_cd
            AND guc.Converted_UOM_Cd = @converted_emission_uom_cd

		
      SELECT
            ROUND(( @Commodity_Conversion_factor * @conversion_factor_to_co2e * @conversion_factor_from_co2e ), 8)
 
END
GO
GRANT EXECUTE ON  [dbo].[commodity_UOM_conversion_Sel_By_emission_factor] TO [CBMSApplication]
GO
