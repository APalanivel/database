SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DT_AUDIT_TRAIL_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@srDealTicketId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec dbo.SR_SAD_GET_DT_AUDIT_TRAIL_DETAILS_P  825
--select * from SR_DEAL_TICKET_TRACE_LOG where SR_DEAL_TICKET_ID = 825
--select * from entity where entity_id>=1007




CREATE  PROCEDURE dbo.SR_SAD_GET_DT_AUDIT_TRAIL_DETAILS_P
@srDealTicketId int
AS
set nocount on


select 
	distinct e1.ENTITY_NAME,
	e1.ENTITY_NAME as PRICED_AUDIT_NAME,
	sdtt1.UPDATE_DATE as PRICED_AUDIT_DATE,
	e2.ENTITY_NAME as AUTHORITY_AUDIT_NAME,
	sdtt2.UPDATE_DATE as AUTHORITY_AUDIT_DATE

from 
	SR_DEAL_TICKET sdt

 

left join SR_DEAL_TICKET_TRACE_LOG sdtt1 on sdtt1.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join SR_DEAL_TICKET_TRACE_LOG sdtt2 on sdtt2.SR_DEAL_TICKET_ID = sdt.SR_DEAL_TICKET_ID 
left join ENTITY e1 on e1.ENTITY_ID = sdtt1.DEAL_STATUS_TYPE_ID
left join ENTITY e2 on e2.ENTITY_ID = sdtt2.DEAL_STATUS_TYPE_ID


where
sdt.SR_DEAL_TICKET_ID = @srDealTicketId
group by sdt.SR_DEAL_TICKET_ID, e1.ENTITY_NAME,	sdtt1.UPDATE_DATE, e2.ENTITY_NAME, sdtt2.UPDATE_DATE
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DT_AUDIT_TRAIL_DETAILS_P] TO [CBMSApplication]
GO
