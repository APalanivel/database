SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_SHORTLIST_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@sopShortListId int,
@accountTermId int,
@selectedProductsId int,
@supplierContactVendorMapId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Delete the entries
--exec SR_RFP_DELETE_SHORTLIST_DETAILS_P
--@sopShortListId =388,
--@accountTermId = 24957,
--@selectedProductsId = 3869,
--@supplierContactVendorMapId = 36268

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE DBO.SR_RFP_DELETE_SHORTLIST_DETAILS_P

@sopShortListId int,
@accountTermId int,
@selectedProductsId int,
@supplierContactVendorMapId int


AS
	
SET NOCOUNT ON


DELETE FROM SR_RFP_SOP_SHORTLIST_DETAILS
WHERE	SR_RFP_SOP_SHORTLIST_ID = @sopShortListId
	AND SR_RFP_ACCOUNT_TERM_ID = @accountTermId 
	AND SR_RFP_SELECTED_PRODUCTS_ID = @selectedProductsId 
	AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SHORTLIST_DETAILS_P] TO [CBMSApplication]
GO
