SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_RATE_SCHEDULES_P 
	@rateId INT,  
	@startDate DATETIME,  
	@endDate DATETIME  
AS  
BEGIN  
  
	SET NOCOUNT ON  
  
	SELECT RATE_SCHEDULE_ID,  
		RS_START_DATE,   
		RS_END_DATE,   
		BILLING_DAYS_ADJUSTMENT,   
		RS_GRAND_TOTAL_EXPRESSION   
	FROM dbo.RATE_SCHEDULE rs  
	WHERE ( rs.rate_id=@rateId 
		AND ( (rs.rs_start_date <= @startDate AND rs.rs_end_date >= @startDate)
			  OR (rs.rs_start_date <= @endDate AND rs.rs_end_date >= @endDate)	    
			  OR (rs.rs_start_date >= @startDate AND rs.rs_end_date <= @endDate)
			  OR (rs.rs_start_date <= @startDate AND rs.rs_end_date >= @endDate)  
			  OR (rs.rs_start_date <= @endDate AND rs.rs_end_date IS NULL)  
		    )  
		  )     
	ORDER BY rs_start_date     
   
END
GO
GRANT EXECUTE ON  [dbo].[GET_RATE_SCHEDULES_P] TO [CBMSApplication]
GO
