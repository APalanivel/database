SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                    
Name:   dbo.[Invoice_Collection_Records_Queue_By_IC_Queue_Ids]                             
                                    
Description:To get the icq information based on the queue_Ids                                
                                                
 Input Parameters:                                    
    Name        DataType   Default   Description                                      
----------------------------------------------------------------------------------------                                      
 @Invoice_Collection_Queue_Id   VARCHAR(MAX)                                      
                       
 Output Parameters:                                          
    Name        DataType   Default   Description                                      
----------------------------------------------------------------------------------------                                      
                                    
 Usage Examples:                                        
----------------------------------------------------------------------------------------                         
 Declare @p1 dbo.tvp_Invoice_Collection_Queue
insert into @p1 values(2750622,NULL,NULL)
insert into @p1 values(2779482,NULL,NULL)
insert into @p1 values(2779483,NULL,NULL)
insert into @p1 values(2788810,NULL,NULL)
insert into @p1 values(2819435,NULL,NULL)
insert into @p1 values(2819436,NULL,NULL)
insert into @p1 values(2819437,NULL,NULL)
insert into @p1 values(2819438,NULL,NULL)
insert into @p1 values(2840531,NULL,NULL)                 
exec dbo.Invoice_Collection_Records_Queue_By_IC_Queue_Ids @tvp_Invoice_Collection_Queue_ID=@p1           
            
Author Initials:                                    
 Initials  Name                                    
----------------------------------------------------------------------------------------                                      
 NM   NagaRaju Muppa             
                      
 Modifications:                                    
    Initials        Date   Modification                                    
----------------------------------------------------------------------------------------                                      
            
******/      
CREATE PROCEDURE [dbo].[Invoice_Collection_Records_Queue_By_IC_Queue_Ids]      
     (      
         @tvp_Invoice_Collection_Queue_ID tvp_Invoice_Collection_Queue READONLY      
         , @Start_Index INT = 1      
         , @End_Index INT = 2147483647      
         , @Total_Count INT = 0      
     )      
AS      
    BEGIN      
        SET NOCOUNT ON;      
      
        WITH CTE      
        AS (      
               SELECT      
                    ROW_NUMBER() OVER (ORDER BY      
                                           Client_Name,
										   Invoice_Collection_Queue_Id      
                                           ) AS Row_num      
                    , COUNT(1) OVER () Total_Count      
                    , *      
               FROM (   SELECT      
                            cha.Account_Number      
                            , CONVERT(VARCHAR(12), icq.Collection_Start_Dt, 105) + ' , '      
                              + CONVERT(VARCHAR(12), icq.Collection_End_Dt, 105) + '/'      
                              + LEFT(ISNULL(C.Code_Value, ''), 1) Period_to_chase      
                            , icq.Invoice_Collection_Queue_Id      
                            , icac.Invoice_Collection_Account_Config_Id      
                            , ch.Client_Name      
                        FROM      
                            dbo.Invoice_Collection_Queue icq      
                            INNER JOIN dbo.Invoice_Collection_Account_Config icac      
                                ON icac.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id      
                            INNER JOIN Core.Client_Hier_Account cha      
                                ON cha.Account_Id = icac.Account_Id      
                            LEFT OUTER JOIN dbo.Code C      
                                ON C.Code_Id = icq.Invoice_Request_Type_Cd      
                            INNER JOIN @tvp_Invoice_Collection_Queue_ID tvp   
                                ON icq.Invoice_Collection_Queue_Id = tvp.Invoice_Collection_Queue_Id      
							INNER JOIN Core.Client_Hier ch      
                                ON cha.Client_Hier_Id = ch.Client_Hier_Id      
                     GROUP BY      
                            cha.Account_Number      
                            , icac.Invoice_Collection_Account_Config_Id      
                            , icq.Invoice_Collection_Queue_Id      
                            , icq.Collection_Start_Dt      
                            , icq.Collection_End_Dt      
                            , C.Code_Value      
                            , ch.Client_Name    
       ) dd      
           )      
        SELECT      
			Account_Number      
            , Period_to_chase      
            , Invoice_Collection_Queue_Id      
            , Invoice_Collection_Account_Config_Id      
            , Total_Count    
        FROM      
            CTE      
        WHERE      
            Row_num BETWEEN @Start_Index      
                    AND     @End_Index      
        ORDER BY      
            Client_Name,Invoice_Collection_Queue_Id;      
    END; 
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Records_Queue_By_IC_Queue_Ids] TO [CBMSApplication]
GO
