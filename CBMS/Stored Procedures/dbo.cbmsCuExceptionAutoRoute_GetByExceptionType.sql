SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsCuExceptionAutoRoute_GetByExceptionType]
	( @MyAccountId int
	, @cu_exception_type_id int
	)
AS
BEGIN


	   select r.cu_exception_type_id
		, r.queue_id
		, t.managed_by_group_info_id
	     from cu_exception_auto_route r with (nolock)
	     join cu_exception_type t with (nolock) on t.cu_exception_type_id = r.cu_exception_type_id
	    where r.cu_exception_type_id = @cu_exception_type_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionAutoRoute_GetByExceptionType] TO [CBMSApplication]
GO
