SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE PROCEDURE [dbo].[cbmsRate_GetRateSchedules]
(
	@MyAccountId int
	,@RateId int
)
AS

select r.rate_id
	,rs.rate_schedule_id
	,coalesce(convert(varchar(10),rs.rs_start_date,101),'Unspecified') + ' -- ' + coalesce(convert(varchar(10),rs.rs_end_date,101),'Unspecified') as rate_schedule
from rate r
left join rate_schedule rs on rs.rate_id = r.rate_id
where r.rate_id = @rateid
and (rs.rs_start_date is not null or rs.rs_end_date is not null)
order by rs.rs_start_date, rs.rs_end_date
GO
GRANT EXECUTE ON  [dbo].[cbmsRate_GetRateSchedules] TO [CBMSApplication]
GO
