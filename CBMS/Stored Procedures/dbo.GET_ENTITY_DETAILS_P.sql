SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ENTITY_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@entityType    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.GET_ENTITY_DETAILS_P 1014
	EXEC dbo.GET_ENTITY_DETAILS_P 1015
	EXEC dbo.GET_ENTITY_DETAILS_P 263
	EXEC dbo.GET_ENTITY_DETAILS_P 272
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-04-04	Global Sourcing - Phase3 - GCS-521 Added new entiy "Closed - Cloned" under entity type 1014(Closed RFP Status) but
							this should be hide to avoid selecting this in application in future
							GCS-577 Added new entities entity type 1015(Reasons For Not Winning) exsting entity "Price" is replaced with four
							price realted options, so "Price" should be hide to avoid selecting this in application in future
	RR			2018-11-12	Global Risk Management - Type "Upto %" removed from entity type ALLOCATION_TYPE(267)
	RR			2019-11-29	RM-Budgets Enahancement - Added entity to hide list(Basis - FORECAST_TYPE - 272)
******/

CREATE PROCEDURE [dbo].[GET_ENTITY_DETAILS_P]
    (
        @entityType INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        /*--------------------------------Entities_To_Hide------------------------------------*/
        DECLARE @Entities_To_Hide TABLE
              (
                  ENTITY_ID INT
                  , ENTITY_NAME VARCHAR(200)
                  , ENTITY_DESCRIPTION VARCHAR(1000)
              );

        /* GCS Phase 3, GCS-521
      New entiy "Closed - Cloned" under entity type 1014(Closed RFP Status) but this should be hide from application
      */
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 1014
            AND ENTITY_DESCRIPTION = 'Closed RFP Status'
            AND ENTITY_NAME = 'Closed - Cloned';
        /* GCS Phase 3, GCS-577
		Added new entities entity type 1015(Reasons For Not Winning) exsting entity "Price" is replaced with four
		price realted options, so "Price" should be hide to avoid selecting this in application in future
      */
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 1015
            AND ENTITY_DESCRIPTION = 'Reasons For Not Winning'
            AND ENTITY_NAME = 'Price';

        /* Global Risk Management - Type "Upto %" removed from entity type ALLOCATION_TYPE(267)
        SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 267                
		Only "Proportional" & "Manual" remains
      */
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 267
            AND ENTITY_DESCRIPTION = 'ALLOCATION_TYPE'
            AND ENTITY_NAME = 'Upto %';

        /* Global Risk Management - Type "Fixed" removed from entity type Deal Type(165)
			SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 165                
		*/
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 165
            AND ENTITY_DESCRIPTION = 'Deal Type'
            AND ENTITY_NAME = 'Fixed';

        /* Global Risk Management - GRM-536 - Type "Nymex" removed from entity type HEDGE_MODE_TYPE(236)
			SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 165                
		*/
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 263
            AND ENTITY_DESCRIPTION = 'HEDGE_MODE_TYPE'
            AND ENTITY_NAME = 'Nymex';

        /* RM-Budgets Enahancement - Type "Basis" removed from entity type FORECAST_TYPE(236)
			SELECT * FROM dbo.ENTITY WHERE ENTITY_TYPE = 272                
		*/
        INSERT INTO @Entities_To_Hide
             (
                 ENTITY_ID
                 , ENTITY_NAME
                 , ENTITY_DESCRIPTION
             )
        SELECT
            ENTITY_ID
            , ENTITY_NAME
            , ENTITY_DESCRIPTION
        FROM
            dbo.ENTITY
        WHERE
            ENTITY_TYPE = 272
            AND ENTITY_DESCRIPTION = 'FORECAST_TYPE'
            AND ENTITY_NAME = 'Basis';

        /*--------------------------------Entities_To_Hide------------------------------------*/



        SELECT
            ent.ENTITY_ID
            , ent.ENTITY_NAME
        FROM
            dbo.ENTITY ent
        WHERE
            ent.ENTITY_TYPE = @entityType
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Entities_To_Hide enthide
                               WHERE
                                    ent.ENTITY_ID = enthide.ENTITY_ID)
        ORDER BY
            ent.DISPLAY_ORDER;

    END;




GO

GRANT EXECUTE ON  [dbo].[GET_ENTITY_DETAILS_P] TO [CBMSApplication]
GO
