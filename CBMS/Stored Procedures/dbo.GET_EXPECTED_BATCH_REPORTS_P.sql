SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_EXPECTED_BATCH_REPORTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@batchId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE dbo.GET_EXPECTED_BATCH_REPORTS_P 
@userId varchar(10),
@sessionId varchar(20),
@batchId int
 AS
set nocount on
IF @batchId > 0
begin
	select batch.rm_report_batch_id,
		( count(details.rm_batch_configuration_details_id)
		  + (select  count(rm_batch_configuration_id) from rm_batch_configuration where corporate_report = 1 and rm_batch_configuration_master_id =  config.rm_batch_configuration_master_id)) 
	from 
		rm_batch_configuration config, 
		rm_batch_configuration_details details,
		rm_report_batch batch
	 
	where 
		config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
		and details.rm_batch_configuration_id = config.rm_batch_configuration_id
		and batch.rm_report_batch_id = @batchId
	
	group by 
		batch.rm_report_batch_id,
		config.rm_batch_configuration_master_id
end

ELSE

begin
	select batch.rm_report_batch_id,
		( count(details.rm_batch_configuration_details_id)
		  + (select  count(rm_batch_configuration_id) from rm_batch_configuration where corporate_report = 1 and rm_batch_configuration_master_id =  config.rm_batch_configuration_master_id)) 
	from 
		rm_batch_configuration config, 
		rm_batch_configuration_details details,
		rm_report_batch batch
	 
	where 
		config.rm_batch_configuration_master_id = batch.rm_batch_configuration_master_id
		and details.rm_batch_configuration_id = config.rm_batch_configuration_id
	
	group by 
		batch.rm_report_batch_id,
		config.rm_batch_configuration_master_id
end
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_BATCH_REPORTS_P] TO [CBMSApplication]
GO
