SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                                  
NAME: dbo.EC_Meter_Attribute_Tracking_Search_update                                                                                  
                                                                                   
                                                                                  
DESCRIPTION:                                                                                  
       This sproc is to get the Meter and attribute tracking details for the given search criteria                                                                            
                                                     
                                                                                  
INPUT PARAMETERS:                                                                                  
 Name      DataType  Default    Description                                                                                  
------------------------------------------------------------------------------------                                                                                  
 @Country_Id INT                        
 @State_Id INT                        
 @Commodity_Id INT                        
 @Client_Id INT = NULL                        
 @Sitegroup_Id INT = NULL                        
 @Site_Client_Hier_Id INT = NULL                        
 @zipcode VARCHAR(20) = NULL                        
 @EC_Meter_Attribute_Id_1 INT = NULL                        
 @EC_Meter_Attribute_Value NVARCHAR(255) = NULL                        
 @EC_Meter_Attribute_Id_2 INT                        
 @Start_Index INT = 1                        
 @End_Index INT = 2147483647                        
 @Total_Count INT = 0                        
 @Site_Status BIT = NULL                        
 @Vendor_Id INT = NULL                        
 @Contract_Number VARCHAR(150) = NULL                        
 @Vendor_Type_Cd INT = NULL                        
 @Meter_Attribute_Type_Cd INT = NULL                        
 @Tarrif_Rate INT = NULL                        
 @Is_Primary BIT = NULL                                          
                                           
                                                                                  
OUTPUT PARAMETERS:                                                                                  
 Name      DataType  Default    Description                                                                                  
------------------------------------------------------------------------------------                                                                                  
                                                                                
                                                                                  
USAGE EXAMPLES:                                                                                  
------------------------------------------------------------------------------------                                                                                  
                                                                           
EXEC dbo.EC_Meter_Attribute_Tracking_Search_update                                           
      @Country_Id = 27                                          
     ,@State_Id = 124                                          
     ,@Commodity_Id = 290                                          
     ,@EC_Meter_Attribute_Id_2 = 5                                          
                                              
                                               
EXEC dbo.EC_Meter_Attribute_Tracking_Search_update                                          
      @Country_Id = 27                                          
     ,@State_Id = 124                                          
     ,@Commodity_Id = 290                                          
     ,@EC_Meter_Attribute_Id_2 = 7                                          
                                               
EXEC dbo.EC_Meter_Attribute_Tracking_Search_update                                           
      @Country_Id = 27                           
     ,@State_Id = 124                                          
     ,@Commodity_Id = 290                                 
     ,@EC_Meter_Attribute_Id_1 = 7                                            
     ,@EC_Meter_Attribute_Id_2 = 38                                          
     ,@Start_Index = 1                                          
     ,@End_Index = 100                                         
     ,@Total_Count=0                                            
                                        
  EXEC dbo.EC_Meter_Attribute_Tracking_Search_Update @Country_Id=27,@State_Id=124,@Commodity_Id=290,@Client_Id=11278,                    
@EC_Meter_Attribute_Id_2=70, @EC_Meter_Attribute_Id_1=26                                      
                                  
exec dbo.EC_Meter_Attribute_Tracking_Search_update @Country_Id=27,@State_Id=124,@Commodity_Id=290,@Client_Id=11694,@Sitegroup_Id=NULL,@Site_Client_Hier_Id=542937                                 
,@zipcode=NULL,@EC_Meter_Attribute_Id_2=30,@Vendor_Id=NULL                                  
                                                        
                                                                    
AUTHOR INITIALS:                                                                                  
 Initials   Name                                                                                 
------------------------------------------------------------------------------------                                                                                  
 RKV   Ravi Kumar Vegesna                                        
 NR    Narayana Reddy                            
 SLP   Sri Lakshimi Pallikonda                          
                                                                                   
MODIFICATIONS                                                       
                                                                                  
 Initials Date   Modification                                                                         
------------------------------------------------------------------------------------                                                                               
 RKV   2015-05-19 Created For As400.                                          
 RKV   2016-09-13 MAINT-4274,Added two Columns Attribute_Searched,Current_Value to the result set.                                        
 NR    2017-04-27 Added  @Site_Status ,@Vendor_Id as optional parameter.                                      
 SLP   2020-01-15 Included Additional input parameters  @Vendor_Type_Cd  ,@Meter_Attribute_Type_Cd ,@Tarrif_Rate,  @Is_Primary                                  
                                         
******/
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Search_update]
    (
        @Country_Id INT
        , @State_Id INT
        , @Commodity_Id INT
        , @Client_Id INT = NULL
        , @Sitegroup_Id INT = NULL
        , @Site_Client_Hier_Id INT = NULL
        , @zipcode VARCHAR(20) = NULL
        , @EC_Meter_Attribute_Id_1 INT = NULL
        , @EC_Meter_Attribute_Value NVARCHAR(255) = NULL
        , @EC_Meter_Attribute_Id_2 INT
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Total_Count INT = 0
        , @Site_Status BIT = NULL
        , @Vendor_Id INT = NULL
        , @Contract_Number VARCHAR(150) = NULL
        , @Vendor_Type_Cd INT = NULL
        , @Meter_Attribute_Type_Cd INT = NULL
        , @Tarrif_Rate INT = NULL
        , @Is_Primary BIT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SET @Contract_Number = '%' + @Contract_Number + '%';

        DECLARE @Meter_Attribute_Type_Value VARCHAR(25);
        DECLARE
            @EC_Meter_Attribute_Name NVARCHAR(200)
            , @Attribute_Type VARCHAR(25)
            , @sitegroup_Name VARCHAR(200)
            , @Vendor_Type_Value_Update VARCHAR(25) = NULL
            , @Attribute_Searched NVARCHAR(400)
            , @Vendor_Type_Value VARCHAR(25) = NULL;

        SELECT
            @Attribute_Searched = ema.EC_Meter_Attribute_Name
        FROM
            dbo.EC_Meter_Attribute ema
        WHERE
            ema.State_Id = @State_Id
            AND ema.Commodity_Id = @Commodity_Id
            AND ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1;

        SELECT
            @Vendor_Type_Value = c.Code_Value
            , @Vendor_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            JOIN dbo.Codeset AS c2
                ON c2.Codeset_Id = c.Codeset_Id
                   AND  c2.Codeset_Name = 'VendorType'
            JOIN dbo.EC_Meter_Attribute AS ema
                ON c.Code_Id = ema.Vendor_Type_Cd
        WHERE
            (   @EC_Meter_Attribute_Id_1 IS NULL
                OR  ema.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1)
            AND (   @Vendor_Type_Cd IS NULL
                    OR  c.Code_Id = @Vendor_Type_Cd);


        SELECT
            @Meter_Attribute_Type_Value = Code_Value
        FROM
            dbo.Code
        WHERE
            Code_Id = @Meter_Attribute_Type_Cd;

        CREATE TABLE #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id INT
                 , Meter_Id INT
                 , Commodity_Id INT
                 , State_Id INT
                 , EC_Meter_Attribute_Value NVARCHAR(400)
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , Attribute_Type VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Vendor_Type_Cd INT
                 , Vendor_Type_Value VARCHAR(25)
                 , Meter_Attribute_Type_Cd INT
                 , Meter_Attribute_Type_Value VARCHAR(25)
             );

        CREATE TABLE #EC_Meter_Attribute_Tracking_Value
             (
                 EC_Meter_Attribute_Id INT
                 , EC_Meter_Attribute_Tracking_id INT
                 , Meter_Id INT
                 , Commodity_Id INT
                 , State_Id INT
                 , EC_Meter_Attribute_Value NVARCHAR(400)
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , Attribute_Type VARCHAR(25)
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Vendor_Type_Value VARCHAR(25)
                 , Meter_Attribute_Type_Cd INT
                 , Meter_Attribute_Type_Value VARCHAR(25)
             );


        INSERT INTO #EC_Meter_Attribute_Tracking_Search
             (
                 EC_Meter_Attribute_Id
                 , Meter_Id
                 , Commodity_Id
                 , State_Id
                 , EC_Meter_Attribute_Value
                 , EC_Meter_Attribute_Name
                 , Attribute_Type
                 , Start_Dt
                 , End_Dt
                 , Vendor_Type_Cd
                 , Vendor_Type_Value
                 , Meter_Attribute_Type_Cd
                 , Meter_Attribute_Type_Value
             )
        SELECT
            x.EC_Meter_Attribute_Id
            , x.Meter_Id
            , x.Commodity_Id
            , x.State_Id
            , x.EC_Meter_Attribute_Value
            , x.EC_Meter_Attribute_Name
            , x.Attribute_Type
            , x.Start_Dt
            , x.End_Dt
            , x.Vendor_Type_Cd
            , x.Vendor_Type_Value
            , x.Meter_Attribute_Type_Cd
            , x.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        emae.EC_Meter_Attribute_Id
                        , emat.Meter_Id
                        , emae.Commodity_Id
                        , emae.State_Id
                        , emat.EC_Meter_Attribute_Value
                        , emae.EC_Meter_Attribute_Name
                        , cd.Code_Value Attribute_Type
                        , emat.Start_Dt
                        , emat.End_Dt
                        , emae.Vendor_Type_Cd
                        , cdd.Code_Value Vendor_Type_Value
                        , emat.Meter_Attribute_Type_Cd
                        , co.Code_Value Meter_Attribute_Type_Value
                        , ROW_NUMBER() OVER (PARTITION BY
                                                 emat.Meter_Id                
                                             ORDER BY
                                                 ISNULL(emat.End_Dt, '2099-12-31') DESC) AS Row_Num
                    FROM
                        dbo.EC_Meter_Attribute emae
                        INNER JOIN dbo.EC_Meter_Attribute_Tracking emat
                            ON emae.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                        INNER JOIN dbo.Code cd
                            ON cd.Code_Id = emae.Attribute_Type_Cd
                        LEFT JOIN dbo.Code cdd
                            ON cdd.Code_Id = emae.Vendor_Type_Cd
                        LEFT JOIN dbo.Code co
                            ON co.Code_Id = emat.Meter_Attribute_Type_Cd
                    WHERE
                        (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR emae.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1)
                        AND (   @Meter_Attribute_Type_Cd IS NULL
                                OR emat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd)
                        AND (   @Vendor_Type_Cd IS NULL
                                OR emae.Vendor_Type_Cd = @Vendor_Type_Cd)
                        AND (   @EC_Meter_Attribute_Value IS NULL
                                OR emat.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value)) x
        WHERE
            x.Row_Num = 1;
        INSERT INTO #EC_Meter_Attribute_Tracking_Value
             (
                 EC_Meter_Attribute_Id
                 , EC_Meter_Attribute_Tracking_id
                 , Meter_Id
                 , Commodity_Id
                 , State_Id
                 , EC_Meter_Attribute_Value
                 , EC_Meter_Attribute_Name
                 , Attribute_Type
                 , Start_Dt
                 , End_Dt
                 , Vendor_Type_Value
                 , Meter_Attribute_Type_Cd
                 , Meter_Attribute_Type_Value
             )
        SELECT
            x.EC_Meter_Attribute_Id
            , x.EC_Meter_Attribute_Tracking_Id
            , x.Meter_Id
            , x.Commodity_Id
            , x.State_Id
            , x.EC_Meter_Attribute_Value
            , x.EC_Meter_Attribute_Name
            , x.Attribute_Type
            , x.Start_Dt
            , x.End_Dt
            , x.Vendor_Type_Value
            , x.Meter_Attribute_Type_Cd
            , x.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        emae.EC_Meter_Attribute_Id
                        , emat.EC_Meter_Attribute_Tracking_Id
                        , emat.Meter_Id
                        , emae.Commodity_Id
                        , emae.State_Id
                        , emat.EC_Meter_Attribute_Value
                        , emae.EC_Meter_Attribute_Name
                        , cd.Code_Value Attribute_Type
                        , emat.Start_Dt
                        , emat.End_Dt
                        , c.Code_Value Vendor_Type_Value
                        , avtc.Code_Id Meter_Attribute_Type_Cd
                        , avtc.Code_Value Meter_Attribute_Type_Value
                        , ROW_NUMBER() OVER (PARTITION BY
                                                 emat.Meter_Id
                                                 , emat.Meter_Attribute_Type_Cd
                                             ORDER BY
                                                 ISNULL(emat.End_Dt, '2099-12-31') DESC) AS Row_Num
                    FROM
                        dbo.EC_Meter_Attribute emae
                        INNER JOIN dbo.Code cd
                            ON cd.Code_Id = emae.Attribute_Type_Cd
                        LEFT OUTER JOIN dbo.Code c
                            ON c.Code_Id = emae.Vendor_Type_Cd
                        LEFT OUTER JOIN dbo.EC_Meter_Attribute_Tracking emat
                            ON emae.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                        LEFT OUTER JOIN dbo.Code avtc
                            ON avtc.Code_Id = emat.Meter_Attribute_Type_Cd
                    WHERE
                        emae.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_2
                        AND (   @Meter_Attribute_Type_Cd IS NULL
                                OR emat.Meter_Attribute_Type_Cd = @Meter_Attribute_Type_Cd)) x
        WHERE
            x.Row_Num = 1;

        SELECT
            @EC_Meter_Attribute_Name = EC_Meter_Attribute_Name
            , @Attribute_Type = Attribute_Type
            , @Vendor_Type_Value_Update = Vendor_Type_Value
        FROM
            #EC_Meter_Attribute_Tracking_Value;
        --SELECT                
        --    *                
        --FROM                
        --    #EC_Meter_Attribute_Tracking_Search AS emats                

        --SELECT                
        --    *                
        --FROM                
        --    #EC_Meter_Attribute_Tracking_Value AS ematv                



        SELECT
            @sitegroup_Name = s.Sitegroup_Name
        FROM
            dbo.Sitegroup s
        WHERE
            Sitegroup_Id = @Sitegroup_Id;

        IF @Total_Count = 0
            BEGIN
                SELECT
                    @Total_Count = COUNT(1) OVER ()
                FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                    INNER JOIN #EC_Meter_Attribute_Tracking_Value emat2
                        ON cha.Meter_Id = emat2.Meter_Id
                    LEFT OUTER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        ON cha.Meter_Id = samm.METER_ID
                    LEFT OUTER JOIN dbo.CONTRACT c
                        ON samm.Contract_ID = c.CONTRACT_ID
                WHERE
                    cha.Meter_Country_Id = @Country_Id
                    AND cha.Meter_State_Id = @State_Id
                    AND cha.Commodity_Id = @Commodity_Id
                    AND (   @zipcode IS NULL
                            OR  cha.Meter_ZipCode = @zipcode)
                    AND (   @Client_Id IS NULL
                            OR  ch.Client_Id = @Client_Id)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  ch.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND cha.Account_Type = 'utility'
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Sitegroup_Site sgs
                                        INNER JOIN dbo.Sitegroup sg
                                            ON sgs.Sitegroup_id = sg.Sitegroup_Id
                                   WHERE
                                        ch.Site_Id = sgs.Site_id
                                        AND (   @Sitegroup_Id IS NULL
                                                OR  sgs.Sitegroup_id = @Sitegroup_Id))
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        #EC_Meter_Attribute_Tracking_Search emat
                                   WHERE
                                        cha.Meter_Id = emat.Meter_Id
                                        AND emat.Commodity_Id = emat2.Commodity_Id
                                        AND emat.State_Id = emat2.State_Id
                                        AND (   @EC_Meter_Attribute_Value IS NULL
                                                OR  emat.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value))
                    AND (   @Site_Status IS NULL
                            OR  ch.Site_Not_Managed = @Site_Status)
                    AND (   @Vendor_Id IS NULL
                            OR  cha.Account_Vendor_Id = @Vendor_Id)
                    AND (   @Contract_Number IS NULL
                            OR  c.ED_CONTRACT_NUMBER LIKE @Contract_Number)
                GROUP BY
                    cha.Meter_State_Id
                    , ch.Client_Id
                    , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id)
                    , ch.Site_Id
                    , cha.Meter_Id
                    , cha.Display_Account_Number
                    , emat2.EC_Meter_Attribute_Value
                    , emat2.Start_Dt
                    , emat2.End_Dt;

            END;
        WITH Cte_Attribute_Tracking
        AS (
               SELECT   TOP (@End_Index)
                        cha.Meter_State_Id
                        , cha.Meter_State_Name
                        , ch.Client_Id
                        , ch.Client_Name
                        , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id) Sitegroup_id
                        , ISNULL(@sitegroup_Name, ch.Sitegroup_Name) Sitegroup_Name
                        , ch.Client_Hier_Id
                        , ch.Site_Id
                        , ch.Site_name
                        , cha.Meter_Id
                        , cha.Meter_Number
                        , cha.Display_Account_Number Account_Number
                        , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN @Attribute_Searched
                              WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Name
                              ELSE NULL
                          END AS Attribute_Searched
                        , emat.EC_Meter_Attribute_Value Current_Value
                        , @EC_Meter_Attribute_Id_2 EC_Meter_Attribute_Id
                        , @EC_Meter_Attribute_Name EC_Meter_Attribute_Name
                        , @Attribute_Type AS Meter_Attribute_Type
                        , emat2.EC_Meter_Attribute_Tracking_id
                        , emat2.EC_Meter_Attribute_Value
                        , emat2.Start_Dt
                        , emat2.End_Dt
                        , cha.Account_Vendor_Id
                        , cha.Account_Vendor_Name
                        , emat.Vendor_Type_Cd
                        , emat.Vendor_Type_Value AS Vendor_Type_Value
                        , cha.Rate_Name Tariff_Rate
                        , CASE WHEN cha.Meter_Id = acpm.Meter_Id THEN 1
                              ELSE 0
                          END AS Is_Primary
                        , @Vendor_Type_Value_Update Vendor_Type_Value_Update
                        , emat2.Meter_Attribute_Type_Value Meter_Attribute_Type_Value_Update
                        , ROW_NUMBER() OVER (ORDER BY
                                                 cha.Meter_Number
                                                 , emat2.Start_Dt
                                                 , emat2.End_Dt) AS Row_Num
               FROM
                    Core.Client_Hier_Account cha
                    INNER JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                    JOIN #EC_Meter_Attribute_Tracking_Value emat2
                        ON cha.Meter_Id = emat2.Meter_Id
                    LEFT JOIN #EC_Meter_Attribute_Tracking_Search emat
                        ON cha.Meter_Id = emat.Meter_Id
                           AND  emat.Commodity_Id = emat2.Commodity_Id
                           AND  emat.State_Id = emat2.State_Id
                    LEFT OUTER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        ON cha.Meter_Id = samm.METER_ID
                    LEFT OUTER JOIN dbo.CONTRACT c
                        ON samm.Contract_ID = c.CONTRACT_ID
                    LEFT JOIN Budget.Account_Commodity_Primary_Meter acpm
                        ON acpm.Account_Id = cha.Account_Id
                           AND  acpm.Commodity_Id = cha.Commodity_Id
                           AND  cha.Meter_Id = acpm.Meter_Id
               WHERE
                    cha.Meter_Country_Id = @Country_Id
                    AND cha.Meter_State_Id = @State_Id
                    AND cha.Commodity_Id = @Commodity_Id
                    AND (   @zipcode IS NULL
                            OR  cha.Meter_ZipCode = @zipcode)
                    AND (   @Client_Id IS NULL
                            OR  ch.Client_Id = @Client_Id)
                    AND (   @Site_Client_Hier_Id IS NULL
                            OR  ch.Client_Hier_Id = @Site_Client_Hier_Id)
                    AND cha.Account_Type = 'utility'
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Sitegroup_Site sgs
                                        INNER JOIN dbo.Sitegroup sg
                                            ON sgs.Sitegroup_id = sg.Sitegroup_Id
                                   WHERE
                                        ch.Site_Id = sgs.Site_id
                                        AND (   @Sitegroup_Id IS NULL
                                                OR  sgs.Sitegroup_id = @Sitegroup_Id))
                    AND (   @EC_Meter_Attribute_Id_1 IS NULL
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NULL
                                    AND NOT EXISTS (   SELECT
                                                            1
                                                       FROM
                                                            #EC_Meter_Attribute_Tracking_Search ea
                                                       WHERE
                                                            ea.Meter_Id = cha.Meter_Id
                                                            AND ea.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1))
                            OR  (   @EC_Meter_Attribute_Id_1 IS NOT NULL
                                    AND @EC_Meter_Attribute_Value IS NOT NULL
                                    AND EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        #EC_Meter_Attribute_Tracking_Search eam
                                                   WHERE
                                                        eam.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id_1
                                                        AND eam.Meter_Id = cha.Meter_Id
                                                        AND eam.EC_Meter_Attribute_Value = @EC_Meter_Attribute_Value
                                                        AND emat.EC_Meter_Attribute_Id = eam.EC_Meter_Attribute_Id
                                                        AND emat.EC_Meter_Attribute_Value = eam.EC_Meter_Attribute_Value)))
                    AND (   @Site_Status IS NULL
                            OR  ch.Site_Not_Managed = @Site_Status)
                    AND (   @Vendor_Id IS NULL
                            OR  cha.Account_Vendor_Id = @Vendor_Id)
                    AND (   @Contract_Number IS NULL
                            OR  c.ED_CONTRACT_NUMBER LIKE @Contract_Number)
                    AND (   @Tarrif_Rate IS NULL
                            OR  cha.Rate_Id = @Tarrif_Rate)
                    AND (   @Is_Primary IS NULL
                            OR  (   (   @Is_Primary = 0
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Budget.Account_Commodity_Primary_Meter acpm
                                                           WHERE
                                                                cha.Account_Id = acpm.Account_Id
                                                                AND cha.Commodity_Id = acpm.Commodity_Id
                                                                AND cha.Meter_Id = acpm.Meter_Id))
                                    OR  (   @Is_Primary = 1
                                            AND EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                Budget.Account_Commodity_Primary_Meter acpm
                                                           WHERE
                                                                cha.Account_Id = acpm.Account_Id
                                                                AND cha.Commodity_Id = acpm.Commodity_Id
                                                                AND cha.Meter_Id = acpm.Meter_Id))))
               GROUP BY
                   cha.Meter_State_Id
                   , cha.Meter_State_Name
                   , ch.Client_Id
                   , ch.Client_Name
                   , ISNULL(@Sitegroup_Id, ch.Sitegroup_Id)
                   , ISNULL(@sitegroup_Name, ch.Sitegroup_Name)
                   , ch.Client_Hier_Id
                   , ch.Site_Id
                   , ch.Site_name
                   , cha.Meter_Id
                   , cha.Meter_Number
                   , emat.EC_Meter_Attribute_Name
                   , emat.EC_Meter_Attribute_Value
                   , cha.Display_Account_Number
                   , emat2.EC_Meter_Attribute_Value
                   , emat2.EC_Meter_Attribute_Tracking_id
                   , emat.Vendor_Type_Cd
                   , emat.Vendor_Type_Value
                   , emat2.Start_Dt
                   , emat2.End_Dt
                   , cha.Account_Vendor_Id
                   , cha.Account_Vendor_Name
                   , cha.Rate_Name
                   , emat2.Meter_Attribute_Type_Value
                   , CASE WHEN cha.Meter_Id = acpm.Meter_Id THEN 1
                         ELSE 0
                     END
                   , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN @Attribute_Searched
                         WHEN @EC_Meter_Attribute_Id_1 IS NULL THEN emat.EC_Meter_Attribute_Name
                         ELSE NULL
                     END
           )
        SELECT
            cat.Meter_State_Id
            , cat.Meter_State_Name
            , cat.Client_Id
            , cat.Client_Name
            , cat.Sitegroup_id
            , cat.Sitegroup_Name
            , cat.Client_Hier_Id
            , cat.Site_Id
            , cat.Site_name
            , cat.Meter_Id
            , cat.Meter_Number
            , cat.Attribute_Searched
            , cat.Current_Value
            , cat.Account_Number
            , cat.EC_Meter_Attribute_Id
            , cat.EC_Meter_Attribute_Tracking_id
            , cat.EC_Meter_Attribute_Name
            , cat.Meter_Attribute_Type
            , cat.EC_Meter_Attribute_Value
            , cat.Start_Dt
            , cat.End_Dt
            , cat.Row_Num
            , @Total_Count AS Total_Rows
            , cat.Account_Vendor_Id
            , cat.Account_Vendor_Name
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN ISNULL(@Vendor_Type_Cd, cat.Vendor_Type_Cd)
                  ELSE NULL
              END Vendor_Type_Cd
            , CASE WHEN @EC_Meter_Attribute_Id_1 IS NOT NULL THEN ISNULL(@Vendor_Type_Value, cat.Vendor_Type_Value)
                  ELSE NULL
              END Vendor_Type_Value
            , cat.Tariff_Rate
            , cat.Is_Primary
            , cat.Meter_Attribute_Type_Value_Update
            , cat.Vendor_Type_Value_Update
        FROM
            Cte_Attribute_Tracking cat
        WHERE
            cat.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            Row_Num;
        DROP TABLE
            #EC_Meter_Attribute_Tracking_Search
            , #EC_Meter_Attribute_Tracking_Value;

    END;
GO


GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Search_update] TO [CBMSApplication]
GO
