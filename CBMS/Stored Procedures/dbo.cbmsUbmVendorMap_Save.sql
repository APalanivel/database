SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsUbmVendorMap_Save]
	( @MyAccountId int
	, @ubm_vendor_map_id int = null
	, @ubm_id int
	, @ubm_vendor_code varchar(200)
	, @vendor_id int
	)
AS
BEGIN

	declare @this_id int

	set nocount on

	set @this_id = @ubm_vendor_map_id

	if @this_id is null
	begin

	   select @this_id = ubm_vendor_map_id
	     from ubm_vendor_map
	    where ubm_id = @ubm_id
	      and ubm_vendor_code = @ubm_vendor_code
	      and vendor_id = @vendor_id

	end

	if @this_id is null
	begin

		insert into ubm_vendor_map
			( ubm_id
			, ubm_vendor_code
			, vendor_id
			)
		values
			( @ubm_id
			, @ubm_vendor_code
			, @vendor_id
			)

		set @this_id = @@IDENTITY


	   update cu_invoice
	      set vendor_id = @vendor_id
	    where ubm_vendor_code = @ubm_vendor_code
	      and vendor_id is null

	end

	exec cbmsUbmVendorMap_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmVendorMap_Save] TO [CBMSApplication]
GO
