SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: [dbo].Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id    
    
DESCRIPTION:    
 To Get Shared Supplier accounts that have CU data for given Utility Account  
    
INPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION     
------------------------------------------------------------    
@Account_Id  INT      Utility Account    
    
OUTPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
 EXEC Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id  101527  
 EXEC Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id  116867    
    
AUTHOR INITIALS:    
INITIALS NAME    
------------------------------------------------------------    
AKR     Ashok Kumar Raju  
    
MODIFICATIONS    
INITIALS DATE  MODIFICATION    
------------------------------------------------------------    
AKR   2014-08-06 Created        
*/    
    
CREATE PROCEDURE dbo.Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id ( @Account_Id INT )  
AS   
BEGIN    
    
      SET NOCOUNT ON;    
        
        
             
      DECLARE @Supplier_Account TABLE ( Account_Id INT )  
        
/*  
  Collect all the shared supplier account for given utility account  
*/        
  
      INSERT      INTO @Supplier_Account  
                  (   
                   Account_Id )  
                  SELECT  
                        cha2.Account_Id AS Supplier  
                  FROM  
                        dbo.METER AS m  
                        INNER JOIN core.Client_Hier_Account AS cha  
                              ON m.METER_ID = cha.Meter_Id  
                        INNER JOIN core.Client_Hier_Account AS cha2  
                              ON cha.Account_Id = cha2.Account_Id  
                  WHERE  
                        cha.Account_Type = 'Supplier'  
                        AND m.ACCOUNT_ID = @Account_Id  
                  GROUP BY  
                        cha2.Account_Id  
                  HAVING  
                        COUNT(DISTINCT cha2.Client_Hier_Id) > 1        
  
        
      SELECT  
            sc.Account_Id  
      FROM  
            @Supplier_Account sc  
      WHERE  
            EXISTS ( SELECT  
                        1  
                     FROM  
                        dbo.Cost_Usage_Account_Dtl cuad  
                     WHERE  
                        cuad.ACCOUNT_ID = sc.Account_Id )  
         
    
END;  
  
;
GO
GRANT EXECUTE ON  [dbo].[Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id] TO [CBMSApplication]
GO
