SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
NAME:                            
 [Get_Account_By_Charge_Type]                          
                             
DESCRIPTION:                            
  Get Account Number by charges, based on regulated / contract charges passed.                          
                            
INPUT PARAMETERS:                            
 Name   DataType  Default Description                            
------------------------------------------------------------                            
 @Regulated_Charge  BIT ,                          
 @Contract_Charge  BIT ,                          
 @Client_Id    INT ,                          
 @Site_Id    INT ,                          
 @Contract_ID   INT                 
 @Rate_ID  INT            
 @Is_ReCalc  BIT    -- 1 for Recalc , 0 for Budget                    
                                                                 
                            
OUTPUT PARAMETERS:                            
 Name   DataType  Default Description                            
------------------------------------------------------------                            
                            
USAGE EXAMPLES:                            
------------------------------------------------------------                            
 SET STATISTICS IO ON                            
 EXEC dbo.[Get_Account_By_Charge_Type] 0,1 ,102                           
 EXEC dbo.[Get_Account_By_Charge_Type] 0,1 ,102 ,0        
                           
AUTHOR INITIALS:                            
 Initials Name                            
------------------------------------------------------------                            
    PRG   Prasanna Raghavendra G                          
                            
MODIFICATIONS:                            
 Initials Date  Modification                            
------------------------------------------------------------                            
PRG  08-APR-2019 Created For Budget 2.0                            
RKV  2019-05-27  Added New parameter Rate_ID                          
SLP  2020-04-16  Added New parameter @Is_ReCalc & @Commodity_Id  
     Included logic to check the Supplier acct falls within the Term   
     selected for Recalc Contract option.  
             
                             
******/  
CREATE PROC [dbo].[Get_Account_By_Charge_Type]  
    (  
        @Regulated_Charge SMALLINT  
        , @Contract_Charge SMALLINT  
        , @Client_Id INT  
        , @Site_Id INT = NULL  
        , @Contract_ID INT = NULL  
        , @Rate_ID INT = NULL  
        , @Is_ReCalc BIT = 1  
        , @Commodity_Id INT = NULL  
        , @Start_Date DATE = NULL  
        , @End_Date DATE = NULL  
    )  
AS  
    BEGIN  
  
        DECLARE  
            @Account_Type VARCHAR(20)  
            , @SQL_Select NVARCHAR(MAX)  
            , @SQL_Where NVARCHAR(MAX)  
            , @SQL_Where_1 NVARCHAR(MAX)  
            , @SQL_Where_2 NVARCHAR(MAX) = N''  
            , @SQL_GroupBy NVARCHAR(MAX)  
            , @SQL_Join NVARCHAR(MAX);  
  
        SET @Account_Type = CASE WHEN @Is_ReCalc = 1 THEN  
                                     CASE WHEN (   (   @Regulated_Charge = 1  
                                                       AND COALESCE(@Contract_Charge, 0) = 0)  
                                                   OR  (   @Regulated_Charge = 1  
                                                           AND @Contract_Charge = 1)) THEN 'Utility'  
                                         WHEN (   @Contract_Charge = 1  
                                                  AND  COALESCE(@Regulated_Charge, 0) = 0) THEN 'Supplier'  
                                     END  
                                WHEN @Is_ReCalc = 0 THEN 'Utility'  
                            END;  
  
  
        SELECT  
            @SQL_Where_1 = CASE WHEN @Site_Id IS NOT NULL THEN +' and  ch.Site_Id =  ' + CAST(@Site_Id AS VARCHAR)  
                               ELSE ''  
                           END  
            + CASE WHEN @Contract_ID IS NOT NULL THEN  
                                      +' and c.CONTRACT_ID  =  ' + CAST(@Contract_ID AS VARCHAR)  
                                 ELSE ''  
                             END  
                           + CASE WHEN @Rate_ID IS NOT NULL THEN +' and   cha.Rate_Id =  ' + CAST(@Rate_ID AS VARCHAR)  
                                 ELSE ''  
                             END  
                           + CASE WHEN @Commodity_Id IS NOT NULL THEN  
                                      +' and   cha.Commodity_Id =  ' + CAST(@Commodity_Id AS VARCHAR)  
                                 ELSE ''  
                             END;  
  
  
        SELECT  
            @SQL_Join = CASE WHEN @Account_Type = 'Utility' THEN  
                                 ' LEFT JOIN Core.Client_Hier_Account AS sup                    
          ON  sup.Meter_Id=cha.Meter_Id                          
          AND sup.Account_Type=''Supplier''                  
           LEFT JOIN dbo.CONTRACT AS c                      
          ON sup.Supplier_Contract_ID = c.CONTRACT_ID'  
                            ELSE  
                                'LEFT JOIN dbo.CONTRACT AS c                      
          ON cha.Supplier_Contract_ID = c.CONTRACT_ID'  
                        END;  
  
  
  
        SELECT  
            @SQL_Select = N' SELECT                      
      cha.Account_Id                      
      ,cha.Display_Account_Number Account_Number                      
      FROM                      
       Core.Client_Hier AS ch                      
       INNER JOIN Core.Client_Hier_Account AS cha                      
        ON cha.Client_Hier_Id = ch.Client_Hier_Id    ';  
  
  
        SELECT  
            @SQL_Where = N'  WHERE                      
            ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR) + N'  AND cha.Account_Type  = '''  
                         + CAST(@Account_Type AS VARCHAR) + N'''';  
  
  
        SELECT
        @SQL_Where_2 = CASE WHEN @Account_Type = 'Supplier' THEN
                                ' and  (cha.Supplier_Account_begin_Dt between ' + '''' + CONVERT(VARCHAR, @Start_Date)
                                + '''' + ' and  ' + '''' + CONVERT(VARCHAR, @End_Date) + ''''
                                + ' or cha.Supplier_Account_end_Dt between ' + '''' + CONVERT(VARCHAR, @Start_Date)
                                + '''' + +' and  ' + '''' + CONVERT(VARCHAR, @End_Date) + '''' + ' or ' + ''''
                                + CONVERT(VARCHAR, @Start_Date) + ''''
                                + ' between  
                                    cha.Supplier_Account_begin_Dt  and cha.Supplier_Account_end_Dt ' + ' or ' + ''''
                                + CONVERT(VARCHAR, @End_Date) + ''''
                                + '   between  
                                    cha.Supplier_Account_begin_Dt  and cha.Supplier_Account_end_Dt )'
                       END;
  
  
        SELECT  
            @SQL_GroupBy = N' GROUP BY                      
        cha.Account_Id                      
        ,cha.Display_Account_Number                   
        ORDER BY                      
         cha.Display_Account_Number ';  
  
        --PRINT (@SQL_Select + ' ' + @sql_Join + '' + @SQL_Where + ' ' + @SQL_Where_1 + ' '+ @SQL_Where_2 + ' '+@SQL_GroupBy);    
        EXEC (@SQL_Select + ' ' + @SQL_Join + '' + @SQL_Where + ' ' + @SQL_Where_1 + ' ' + @SQL_Where_2 + ' ' + @SQL_GroupBy);  
  
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Account_By_Charge_Type] TO [CBMSApplication]
GO
