SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE PROCEDURE [dbo].[cbmsAppModule_GetAll]
	( @MyAccountId int )
AS
BEGIN

	   select app_module_id
		, display_text app_module_name
	     from app_module
	 order by display_text 

END










GO
GRANT EXECUTE ON  [dbo].[cbmsAppModule_GetAll] TO [CBMSApplication]
GO
