SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
  

NAME:        
dbo.Commodity_UOM_Conversion_Ins       

DESCRIPTION:        
	Used to insert/update UOM conversion into COMMODITY_UOM_CONVERSION table      

INPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
  @Base_Commodity_Id AS INT
, @Base_UOM_Cd AS INT
, @Converted_Commodity_Id AS INT
, @Converted_UOM_Cd AS INT
, @Conversion_Factor AS DECIMAL(   32, 16 )   
      
OUTPUT PARAMETERS:        
Name				DataType Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------      

EXEC dbo.Commodity_UOM_Conversion_Ins 57,102,1373,96,0.984206527611061


AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA Shobhit Kr Agrawal    

MODIFICATIONS         
Initials Date Modification        
------------------------------------------------------------        
GB    Created      
SKA  08/07/09 Modified    

GB  14/07/09 In Entity Table the Unit for Description is different for Different Commodity,   
			It does not follow standard pattern for electriv power, Natural gas and Alternative Natural Gas  
			Hence those are treated separately in the case  

    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[Commodity_UOM_Conversion_Ins]
  @Base_Commodity_Id AS INT
, @Base_UOM_Cd AS INT
, @Converted_Commodity_Id AS INT
, @Converted_UOM_Cd AS INT
, @Conversion_Factor AS DECIMAL(   32, 16 )

AS 

BEGIN
    SET  NOCOUNT ON;
    
    DECLARE  @TConversion_Factor AS DECIMAL(32, 16)
    BEGIN TRY
		BEGIN TRANSACTION
    
   
			IF NOT EXISTS (SELECT 1 FROM dbo.COMMODITY_UOM_CONVERSION
							 WHERE       Base_Commodity_Id = @Base_Commodity_Id AND
						Base_UOM_Cd = @Base_UOM_Cd AND
						Converted_Commodity_Id = @Converted_Commodity_Id AND
						Converted_UOM_Cd = @Converted_UOM_Cd)

				INSERT INTO dbo.COMMODITY_UOM_CONVERSION(   Base_Commodity_Id
					, Base_UOM_Cd
					, Converted_Commodity_Id
					, Converted_UOM_Cd
					, Conversion_Factor
					, Is_Active )
				VALUES  (   @Base_Commodity_Id
					, @Base_UOM_Cd
					, @Converted_Commodity_Id
					, @Converted_UOM_Cd
					, @Conversion_Factor
					, 1 )		        		       

				-- Insert  Converted Commodity one level with respect to base commodity conversion factor        

				SET @TConversion_Factor = 1 / @Conversion_Factor
		  
				IF NOT EXISTS (SELECT 1 FROM dbo.COMMODITY_UOM_CONVERSION 
								WHERE  Base_Commodity_Id = @Converted_Commodity_Id AND
									Base_UOM_Cd = @Converted_UOM_Cd AND
									Converted_Commodity_Id = @Base_Commodity_Id AND
									Converted_UOM_Cd = @Base_UOM_Cd)

					INSERT INTO dbo.COMMODITY_UOM_CONVERSION(   Base_Commodity_Id
						, Base_UOM_Cd
						, Converted_Commodity_Id
						, Converted_UOM_Cd
						, Conversion_Factor
						, Is_Active )
					VALUES  (   @Converted_Commodity_Id
						, @Converted_UOM_Cd
						, @Base_Commodity_Id
						, @Base_UOM_Cd
						, @TConversion_Factor
						, 1 )
	        
	
		COMMIT TRAN
	 END TRY
	 BEGIN CATCH
		 ROLLBACK TRAN
		 EXEC dbo.usp_RethrowError
	 END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[Commodity_UOM_Conversion_Ins] TO [CBMSApplication]
GO
