
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
              
/******                          
NAME:   dbo.Report_DE_RM_details              
                         
DESCRIPTION:                           
   Risk Management Report              
                         
INPUT PARAMETERS:                          
Name       DataType  Default  Description                
------------------------------------------------------------                
 @ClientId      INT              
 @divisionId     INT     0              
 @GroupId      INT     0              
 @StartDate      VARCHAR(12)              
 @EndDate      VARCHAR(12)              
 @unitId      INT              
 @currencyUnit     INT              
              
 OUTPUT PARAMETERS:                          
 Name      DataType  Default  Description                          
------------------------------------------------------------                         
       select * from client where client_name like '%patheon%'         
                       
 USAGE EXAMPLES:                          
------------------------------------------------------------                
              
dbo.[Report_DE_RM_details]              
     @ClientId = 11660              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2012'              
     ,@EndDate = '12/31/2012'              
     ,@unitId = 25            
     ,@currencyUnit=3              
                   
dbo.Report_DE_RM_details              
     @ClientId = 11660              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2013'              
     ,@EndDate = '12/01/2013'              
     ,@unitId = 25              
     ,@currencyUnit=3              
                   
dbo.Report_DE_RM_details              
     @ClientId = 11447              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2013'              
     ,@EndDate = '12/01/2013'              
     ,@unitId = 25              
     ,@currencyUnit=3              
              
dbo.Report_DE_RM_details              
     @ClientId = 11610              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2013'              
     ,@EndDate = '12/01/2013'              
     ,@unitId = 25              
     ,@currencyUnit=3              
              
              
dbo.Report_DE_RM_details              
      @ClientId = 11610              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2011'              
     ,@EndDate = '12/01/2011'              
     ,@unitId = 25              
     ,@currencyUnit=3              
              
dbo.Report_DE_RM_details              
      @ClientId = 11610              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '01/01/2011'              
     ,@EndDate = '12/01/2011'              
     ,@unitId = 25              
     ,@currencyUnit=3              
              
dbo.[Report_DE_RM_details]              
     @ClientId = 10005              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@StartDate = '2013-01-01'              
     ,@EndDate = '2013-12-31'              
     ,@unitId = 25              
     ,@currencyUnit=3              
              
 Report_DE_RM_details              
      @StartDate = '01/01/2012'              
     ,@EndDate = '12/01/2012'              
     ,@ClientId = 11540              
     ,@divisionId =0              
     ,@GroupId=0              
     ,@unitId = 25              
    ,@currencyUnit=3              
                  
 AUTHOR INITIALS:                          
 Initials Name                          
-------------------------------------------------------------                          
 SP  Sandeep Pigilam              
                         
 MODIFICATIONS:                           
 Initials  Date   Modification                          
------------------------------------------------------------                          
  SP   2013-10-21  Created     
  AKR  2014-08-01  Modified to use code instead of sproc to get contract volumes.(used round off to get data)  
  SP   2017-06-21  REPTMGR-71,Hedge_Type is making the duplicate records in final select aggregated hedge_type,hedge_price.       
                       
******/              
              
CREATE PROCEDURE [dbo].[Report_DE_RM_details]
      ( 
       @ClientId INT
      ,@divisionId INT = 0
      ,@GroupId INT = 0
      ,@StartDate DATE
      ,@EndDate DATE
      ,@unitId INT
      ,@currencyUnit INT )
AS 
BEGIN              
      SET NOCOUNT ON;              
              
      DECLARE
            @StartYear INT
           ,@EndYear INT
           ,@Client_Currency_Group_Id INT
           ,@Max_FORECAST_AS_OF_DATE DATETIME
           ,@Min_FORECAST_AS_OF_DATE DATETIME
           ,@Currency VARCHAR(10)
           ,@frequency_type VARCHAR(7) = 'Monthly'              
              
                         
                         
      CREATE TABLE #Forecast
            ( 
             Month_Identifier DATETIME NULL
            ,SiteId INT NULL
            ,Forecasted_Usage DECIMAL(20, 3) NULL )              
              
      CREATE TABLE #Hedge_Volume
            ( 
             Month_Identifier DATETIME NULL
            ,SiteId INT NULL
            ,Hedge_Volume DECIMAL(20, 3) NULL
            ,Hedge_Price DECIMAL(8, 3) NULL
            ,Hedge_Type VARCHAR(12) NULL )                
              
      CREATE TABLE #BudgetWACOGPRICE
            ( 
             Volume DECIMAL(32, 16) NULL
            ,PriceVolume DECIMAL(32, 16) NULL
            ,Month_Identifier DATETIME NULL
            ,SiteId INT NULL )              
              
      CREATE TABLE #RM_Budget
            ( 
             BudgetWACOGPRICE DECIMAL(8, 3) NULL
            ,Month_Identifier DATETIME NULL
            ,SiteId INT NULL
            ,Hedge_Type VARCHAR(12) NULL )              
                          
      CREATE TABLE #Sites
            ( 
             SiteId INT
            ,Hedge_Type VARCHAR(12) NULL )              
              
      CREATE TABLE #Hedge
            ( 
             RM_DEAL_TICKET_ID INT NULL
            ,SITE_ID INT NULL
            ,HEDGE_PRICE DECIMAL(8, 3) NULL
            ,PREMIUM DECIMAL(8, 3) NULL
            ,MONTH_IDENTIFIER VARCHAR(12) NULL
            ,OPTION_NAME VARCHAR(200) NULL
            ,HEDGE_TYPE VARCHAR(12) NULL
            ,MAX_HEDGE_PERCENT DECIMAL(8, 3) NULL
            ,CONVERSION_FACTOR DECIMAL(10, 6) NULL
            ,VOLUME_CONVERSION_FACTOR DECIMAL(10, 6) NULL
            ,CURRENCY_TYPE_ID INT NULL
            ,HEDGE_VOLUME DECIMAL(20, 2) NULL ) 
            
      CREATE TABLE #Hedge_with_Type
            ( 
             RM_DEAL_TICKET_ID INT NULL
            ,SITE_ID INT NULL
            ,HEDGE_PRICE DECIMAL(8, 3) NULL
            ,PREMIUM DECIMAL(8, 3) NULL
            ,MONTH_IDENTIFIER VARCHAR(12) NULL
            ,OPTION_NAME VARCHAR(200) NULL
            ,HEDGE_TYPE VARCHAR(12) NULL
            ,MAX_HEDGE_PERCENT DECIMAL(8, 3) NULL
            ,CONVERSION_FACTOR DECIMAL(10, 6) NULL
            ,VOLUME_CONVERSION_FACTOR DECIMAL(10, 6) NULL
            ,CURRENCY_TYPE_ID INT NULL
            ,HEDGE_VOLUME DECIMAL(20, 2) NULL )        
                       
      CREATE TABLE #Contract
            ( 
             Month_Identifier DATETIME NULL
            ,SiteId INT NULL
            ,ContractMeterVolume DECIMAL(20, 3) NULL )                           
      SET @StartYear = YEAR(@StartDate)               
      SET @EndYear = YEAR(@EndDate)              
                    
      SELECT
            @Max_FORECAST_AS_OF_DATE = MAX(FORECAST_AS_OF_DATE)
      FROM
            RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @StartYear
            AND CLIENT_ID = @ClientId              
                                                                                      
      SELECT
            @Min_FORECAST_AS_OF_DATE = MAX(FORECAST_AS_OF_DATE)
      FROM
            RM_FORECAST_VOLUME
      WHERE
            FORECAST_YEAR = @EndYear
            AND CLIENT_ID = @ClientId              
              
      SELECT
            @Currency = SYMBOL
      FROM
            CURRENCY_UNIT
      WHERE
            CURRENCY_UNIT_ID = @currencyUnit                   
              
      INSERT      INTO #BudgetWACOGPRICE
                  EXEC GET_DEAL_CALCULATOR_BUDGET_WACOG_FOR_SITES_P 
                        @clientId = @ClientId
                       ,@startDate = @StartDate
                       ,@endDate = @EndDate
                       ,@startYear = @StartYear
                       ,@endYear = @EndYear              
              
      INSERT      INTO #Forecast
                  ( 
                   Month_Identifier
                  ,SiteId
                  ,Forecasted_Usage )
                  SELECT
                        CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                       ,volumedetails.SITE_ID
                       ,CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) FORECASTED_USAGE
                  FROM
                        dbo.SITE s
                        INNER JOIN RM_FORECAST_VOLUME_DETAILS volumedetails
                              ON volumedetails.SITE_ID = s.SITE_ID
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                                 AND consumption.CONVERTED_UNIT_ID = @unitId
                  WHERE
                        s.Client_ID = @ClientId
                        AND ( @divisionId = 0
                              OR s.DIVISION_ID = @divisionId )
                        AND @GroupId = 0
                        AND onboard.INCLUDE_IN_REPORTS = 1
                        AND volume.FORECAST_AS_OF_DATE IN ( @Max_FORECAST_AS_OF_DATE, @Min_FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN @StartDate
                                                           AND     @EndDate
                  GROUP BY
                        volumedetails.MONTH_IDENTIFIER
                       ,volumedetails.SITE_ID               
              
                
                         
      INSERT      INTO #Forecast
                  ( 
                   Month_Identifier
                  ,SiteId
                  ,Forecasted_Usage )
                  SELECT
                        CONVERT (VARCHAR(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER
                       ,volumedetails.SITE_ID
                       ,CAST(SUM(volumedetails.VOLUME * consumption.CONVERSION_FACTOR) AS DECIMAL(15, 3)) FORECASTED_USAGE
                  FROM
                        RM_FORECAST_VOLUME_DETAILS volumedetails
                        INNER JOIN RM_FORECAST_VOLUME volume
                              ON volumedetails.RM_FORECAST_VOLUME_ID = volume.RM_FORECAST_VOLUME_ID
                        INNER JOIN RM_ONBOARD_HEDGE_SETUP onboard
                              ON volumedetails.SITE_ID = onboard.SITE_ID
                        INNER JOIN CONSUMPTION_UNIT_CONVERSION consumption
                              ON onboard.VOLUME_UNITS_TYPE_ID = consumption.BASE_UNIT_ID
                                 AND consumption.CONVERTED_UNIT_ID = @unitId
                        INNER JOIN dbo.RM_GROUP_SITE_MAP rgsm
                              ON rgsm.SITE_ID = volumedetails.SITE_ID
                  WHERE
                        rgsm.RM_GROUP_ID = @GroupId
                        AND @GroupId > 0
                        AND onboard.INCLUDE_IN_REPORTS = 1
                        AND volume.FORECAST_AS_OF_DATE IN ( @Max_FORECAST_AS_OF_DATE, @Min_FORECAST_AS_OF_DATE )
                        AND volumedetails.MONTH_IDENTIFIER BETWEEN @StartDate
                                                           AND     @EndDate
                  GROUP BY
                        volumedetails.MONTH_IDENTIFIER
                       ,volumedetails.SITE_ID                
                                 
          
              
      INSERT      INTO #Hedge_with_Type
                  ( 
                   RM_DEAL_TICKET_ID
                  ,SITE_ID
                  ,HEDGE_PRICE
                  ,PREMIUM
                  ,MONTH_IDENTIFIER
                  ,OPTION_NAME
                  ,HEDGE_TYPE
                  ,MAX_HEDGE_PERCENT
                  ,CONVERSION_FACTOR
                  ,VOLUME_CONVERSION_FACTOR
                  ,CURRENCY_TYPE_ID
                  ,HEDGE_VOLUME )
                  EXEC [dbo].[Report_DE_RM_Hedge] 
                        @fromDate = @StartDate
                       ,@toDate = @EndDate
                       ,@clientId = @ClientId
                       ,@divisionId = @divisionId
                       ,@unitId = @unitId
                       ,@groupId = @GroupId                




      INSERT      INTO #Hedge
                  ( 
                   RM_DEAL_TICKET_ID
                  ,SITE_ID
                  ,HEDGE_PRICE
                  ,PREMIUM
                  ,MONTH_IDENTIFIER
                  ,OPTION_NAME
                  ,HEDGE_TYPE
                  ,MAX_HEDGE_PERCENT
                  ,CONVERSION_FACTOR
                  ,VOLUME_CONVERSION_FACTOR
                  ,CURRENCY_TYPE_ID
                  ,HEDGE_VOLUME )
                  SELECT
                        RM_DEAL_TICKET_ID
                       ,SITE_ID
                       ,SUM(HEDGE_PRICE)
                       ,PREMIUM
                       ,MONTH_IDENTIFIER
                       ,OPTION_NAME
                       ,'' AS HEDGE_TYPE
                       ,MAX_HEDGE_PERCENT
                       ,CONVERSION_FACTOR
                       ,VOLUME_CONVERSION_FACTOR
                       ,CURRENCY_TYPE_ID
                       ,SUM(HEDGE_VOLUME)
                  FROM
                        #Hedge_with_Type
                  GROUP BY
                        RM_DEAL_TICKET_ID
                       ,SITE_ID
                       ,PREMIUM
                       ,MONTH_IDENTIFIER
                       ,OPTION_NAME
                       ,MAX_HEDGE_PERCENT
                       ,CONVERSION_FACTOR
                       ,VOLUME_CONVERSION_FACTOR
                       ,CURRENCY_TYPE_ID
                      
     
      INSERT      INTO #Contract
                  ( 
                   SiteId
                  ,Month_Identifier
                  ,ContractMeterVolume )
                  SELECT
                        sit.Site_Id
                       ,cmv.MONTH_IDENTIFIER
                       ,SUM(CASE WHEN freq_type.entity_name = 'Daily'
                                      AND @frequency_type = 'Monthly' THEN ( ( ROUND(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0) ) * dd.Days_In_Month_Num )
                                 WHEN freq_type.entity_name = CASE WHEN @frequency_type = 'Monthly' THEN 'Month'
                                                                   ELSE @frequency_type
                                                              END THEN ( ROUND(cmv.VOLUME * cuc.CONVERSION_FACTOR, 0) )
                                 ELSE NULL
                            END) AS ContractMeterVolume
                  FROM
                        dbo.CONTRACT con
                        JOIN dbo.supplier_account_meter_map map
                              ON map.contract_id = con.contract_id
                        JOIN dbo.ACCOUNT suppacc
                              ON suppacc.account_id = map.account_id
                        JOIN Core.Client_Hier_Account ca
                              ON ca.Meter_Id = map.METER_ID
                                 AND ca.Account_Type = 'Utility'
                        JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = ca.Client_Hier_Id
                        JOIN dbo.SITE sit
                              ON sit.SITE_ID = ch.Site_Id
                        JOIN dbo.Commodity cm
                              ON cm.Commodity_Id = con.COMMODITY_TYPE_ID
                                 AND cm.Commodity_Name = 'Natural Gas'
                        JOIN dbo.ENTITY S_type
                              ON S_type.ENTITY_ID = sit.SITE_TYPE_ID
                        LEFT JOIN dbo.CONTRACT_METER_VOLUME cmv
                              ON cmv.CONTRACT_ID = con.CONTRACT_ID
                                 AND cmv.METER_ID = ca.Meter_Id
                        LEFT JOIN dbo.ENTITY freq_type
                              ON freq_type.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
                        LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = @unitId
                        LEFT JOIN dbo.ENTITY ent_uofm
                              ON ent_uofm.ENTITY_ID = cmv.UNIT_TYPE_ID
                                 AND ent_uofm.ENTITY_DESCRIPTION IN ( 'Unit for Gas' )
                        INNER JOIN meta.Date_Dim dd
                              ON dd.Date_D = cmv.MONTH_IDENTIFIER
                  WHERE
                        map.is_history <> 1
                        AND CH.CLIENT_ID = @ClientId
                        AND cmv.MONTH_IDENTIFIER BETWEEN @StartDate
                                                 AND     @EndDate
                        AND ca.Account_Not_Managed = 0
                        AND ch.Site_Not_Managed = 0
                        AND freq_type.entity_name IS NOT NULL
                        AND cmv.VOLUME <> 0
                  GROUP BY
                        sit.Site_Id
                       ,cmv.MONTH_IDENTIFIER         
                       
                       
                       
               
      INSERT      INTO #Hedge_Volume
                  ( 
                   Month_Identifier
                  ,SiteId
                  ,Hedge_Volume
                  ,Hedge_Price
                  ,Hedge_Type )
                  SELECT
                        k.MONTH_IDENTIFIER
                       ,k.SITE_ID
                       ,SUM(k.HEDGE_VOLUME) HEDGE_VOLUME
                       ,SUM(HEDGE_PRICE) / SUM(HEDGE_VOLUME) HEDGE_PRICE
                       ,Hedge_Type
                  FROM
                        ( SELECT
                              Hedge.MONTH_IDENTIFIER
                             ,Hedge.SITE_ID
                             ,Hedge.HEDGE_VOLUME
                             ,CASE WHEN cu.CURRENCY_UNIT_Name != @Currency THEN CASE WHEN @Currency = 'USD' THEN CAST(( Hedge.HEDGE_VOLUME * ( Hedge.HEDGE_PRICE * Hedge.CONVERSION_FACTOR ) / hedge.VOLUME_CONVERSION_FACTOR ) AS DECIMAL(10, 3))
                                                                                     WHEN @Currency = 'CAN' THEN CAST(( Hedge.HEDGE_VOLUME * ( ( Hedge.HEDGE_PRICE / Hedge.CONVERSION_FACTOR ) * hedge.VOLUME_CONVERSION_FACTOR ) ) AS DECIMAL(10, 3))
                                                                                END
                                   ELSE CAST(( Hedge.HEDGE_VOLUME * ( Hedge.HEDGE_PRICE / hedge.VOLUME_CONVERSION_FACTOR ) ) AS DECIMAL(10, 3))
                              END AS HEDGE_PRICE
                             ,Hedge_Type
                          FROM
                              #Hedge Hedge
                              INNER JOIN dbo.CURRENCY_UNIT cu
                                    ON Hedge.CURRENCY_TYPE_ID = cu.CURRENCY_UNIT_ID ) k
                  GROUP BY
                        k.MONTH_IDENTIFIER
                       ,k.SITE_ID
                       ,k.Hedge_Type
                  HAVING
                        SUM(k.HEDGE_VOLUME) > 0                 
              
              
      INSERT      INTO #Sites
                  ( 
                   SiteId
                  ,Hedge_Type )
                  SELECT
                        SiteId
                       ,Hedge_Type
                  FROM
                        #Hedge_Volume
                  GROUP BY
                        SiteId
                       ,Hedge_Type            
                             
            
      CREATE TABLE #Dates
            ( 
             MONTH_IDENTIFIER DATETIME )                             
      INSERT      INTO #Dates
                  ( 
                   MONTH_IDENTIFIER )
                  SELECT
                        CAST(DATE_D AS DATETIME) AS MONTH_IDENTIFIER
                  FROM
                        meta.DATE_DIM
                  WHERE
                        DATE_D BETWEEN @StartDate AND @EndDate            
                        
                          
                              
                              
      INSERT      INTO #RM_Budget
                  ( 
                   BudgetWACOGPRICE
                  ,Month_Identifier
                  ,SiteId
                  ,Hedge_Type )
                  SELECT
                        CASE WHEN SUM(volume) = 0 THEN 0
                             ELSE CASE WHEN rm.siteid IS NULL THEN BudgetWACOGPRICE
                                       ELSE SUM(PriceVolume) / SUM(volume)
                                  END
                        END test
                       ,Alldays.Month_Identifier
                       ,s.SiteId
                       ,s.Hedge_Type
                  FROM
                        #Sites s
                        CROSS JOIN #Dates Alldays
                        LEFT JOIN #BudgetWACOGPRICE rm
                              ON s.SiteId = rm.SiteId
                                 AND Alldays.MONTH_IDENTIFIER = rm.MONTH_IDENTIFIER
                        LEFT JOIN ( SELECT
                                          CASE SUM(A.VOLUME)
                                            WHEN 0 THEN 0.0
                                            ELSE SUM(A.priceVolume) / SUM(A.VOLUME)
                                          END AS BudgetWACOGPRICE
                                         ,A.MONTH_IDENTIFIER MONTH_IDENTIFIER
                                    FROM
                                          #BudgetWACOGPRICE a
                                    GROUP BY
                                          a.MONTH_IDENTIFIER ) k
                              ON Alldays.MONTH_IDENTIFIER = k.MONTH_IDENTIFIER
                  GROUP BY
                        Alldays.MONTH_IDENTIFIER
                       ,s.SITEID
                       ,S.Hedge_Type
                       ,rm.siteid
                       ,BudgetWACOGPRICE          
                             
             
                             
                                
      SELECT
            ch.Client_Name
           ,ch.Country_Name
           ,ch.Site_name
           ,ch.Site_Id
           ,ch.Sitegroup_Id Division_Id
           ,ch.Sitegroup_Name Division_Name
           ,gn.GROUP_NAME
           ,gn.RM_GROUP_ID Group_Id
           ,'' AS Hedge_Type               --,rmb.Hedge_Type            
           ,dd.MONTH_IDENTIFIER        --,rmb.MONTH_IDENTIFIER            
           ,fc.Forecasted_Usage
           ,hv.Hedge_Volume
           ,hv.Hedge_Price
           ,ISNULL(hv.Hedge_Volume, 0) * ISNULL(hv.Hedge_Price, 0) AS HedgePriceTotal
           ,ISNULL(fc.Forecasted_Usage, 0) * ISNULL(rmb.BudgetWACOGPRICE, 0) AS BudgetWACOGPRICETotal
           ,rmb.BudgetWACOGPRICE
           ,ct.ContractMeterVolume
      FROM
            core.Client_Hier ch
            CROSS JOIN #Dates dd
            LEFT JOIN dbo.RM_ONBOARD_HEDGE_SETUP rhs
                  ON ch.Site_Id = rhs.SITE_ID
            INNER JOIN dbo.ENTITY enht
                  ON enht.ENTITY_ID = rhs.HEDGE_TYPE_ID
            LEFT JOIN #RM_Budget rmb
                  ON rmb.SiteId = ch.Site_Id
                     AND dd.MONTH_IDENTIFIER = rmb.MONTH_IDENTIFIER
            LEFT JOIN #Hedge_Volume hv
                  ON rmb.SiteId = hv.SiteId
                     AND rmb.MONTH_IDENTIFIER = hv.MONTH_IDENTIFIER
                     AND rmb.Hedge_Type = hv.Hedge_Type
            LEFT JOIN #Hedge h
                  ON rmb.SiteId = h.Site_Id
                     AND rmb.MONTH_IDENTIFIER = h.MONTH_IDENTIFIER
                     AND rmb.Hedge_Type = h.Hedge_Type
            LEFT JOIN #Forecast fc
                  ON fc.MONTH_IDENTIFIER = dd.MONTH_IDENTIFIER
                     AND fc.SiteId = ch.Site_Id
            LEFT JOIN #Contract ct
                  ON ct.MONTH_IDENTIFIER = dd.MONTH_IDENTIFIER
                     AND ct.SiteId = ch.Site_Id
            LEFT JOIN ( SELECT
                              rgsm.Site_Id
                             ,rg.RM_GROUP_ID
                             ,rg.Group_Name
                        FROM
                              dbo.RM_GROUP_SITE_MAP rgsm
                              INNER JOIN dbo.RM_GROUP rg
                                    ON rgsm.RM_GROUP_ID = rg.RM_GROUP_ID ) gn
                  ON ch.Site_Id = gn.SITE_ID
      WHERE
            ch.Site_Closed = 0
            AND enht.ENTITY_NAME != 'Does not hedge'
            AND ch.Client_Id = @ClientId
            AND ch.Site_Id > 0
            AND ( enht.ENTITY_Id IS NOT NULL
                  OR rmb.MONTH_IDENTIFIER IS NOT NULL )
      GROUP BY
            ch.Client_Name
           ,ch.Country_Name
           ,ch.Site_name
           ,ch.Site_Id
           ,ch.Sitegroup_Id
           ,ch.Sitegroup_Name
           ,gn.GROUP_NAME
           ,gn.RM_GROUP_ID
           ,enht.ENTITY_NAME
           ,dd.MONTH_IDENTIFIER        
          --,rmb.Hedge_Type            
          --,rmb.MONTH_IDENTIFIER            
           ,fc.Forecasted_Usage    
            --,( h.HEDGE_PRICE * h.CONVERSION_FACTOR ) / ( VOLUME_CONVERSION_FACTOR )      
           ,hv.Hedge_Volume
           ,hv.Hedge_Price
           ,rmb.BudgetWACOGPRICE
           ,ct.ContractMeterVolume                   
           --HAVING COUNT(hv.Hedge_Volume)>=1                       
ORDER BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,gn.GROUP_NAME
           ,dd.MONTH_IDENTIFIER             
     --,rmb.MONTH_IDENTIFIER                           
END; 
;
;
GO


GRANT EXECUTE ON  [dbo].[Report_DE_RM_details] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_RM_details] TO [CBMSApplication]
GO
