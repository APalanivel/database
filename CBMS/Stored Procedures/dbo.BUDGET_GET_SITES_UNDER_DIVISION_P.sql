SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

















--exec BUDGET_GET_SITES_UNDER_DIVISION_P -1,-1,262
-- select * from vwSiteName where division_id = 262 order by site_name



CREATE              PROCEDURE dbo.BUDGET_GET_SITES_UNDER_DIVISION_P 
@userId varchar,
@sessionId varchar,
@divisionId int
as
select	viewSite.SITE_ID,
	viewSite.SITE_NAME

from	
	VWSITENAME viewSite,
	site site	

where 	
	viewSite.DIVISION_ID=@divisionId
	and site.SITE_ID = viewSite.SITE_ID
	

order by viewSite.SITE_NAME



















GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SITES_UNDER_DIVISION_P] TO [CBMSApplication]
GO
