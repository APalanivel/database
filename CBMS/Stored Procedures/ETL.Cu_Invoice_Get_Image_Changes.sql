SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
NAME:   etl.Cu_Invoice_Get_Image_Changes                     
              
                  
DESCRIPTION:                
 Gets CU_Invoice Image data for CU_Invoice records that have changed              
               
 assumes CBMS_Image_Id and Service_Month are populated                  
                                   
INPUT PARAMETERS:                        
Name   DataType Default  Description                        
------------------------------------------------------------                        
              
                              
OUTPUT PARAMETERS:                        
Name   DataType Default  Description                        
------------------------------------------------------------               
              
                       
USAGE EXAMPLES:                        
------------------------------------------------------------               
 EXEC etl.Cu_Invoice_Get_Image_Changes 0              
                     
                   
AUTHOR INITIALS:                        
Initials Name                        
------------------------------------------------------------                        
CMH   Chad Hattabaugh                
AKR   Ashok Kumar Raju            
DMR   Deana Ritter          
NM  Nagaraju MUppa                   
                   
MODIFICATIONS                         
Initials Date Modification                        
------------------------------------------------------------                        
CMH   12/29/2010 Created              
AKR   2012-08-29 Modified to include Is_Reported Flag            
DMR  12/8/2014 Removed Transaction Isolation Level statements.  Causing transaction logs to fill up.          
NM 28-04-2020 MAINT-10003 Added CU_INVOICE_CHARGE,CU_INVOICE_CHARGE_ACCOUNT and CU_INVOICE_DETERMINANT,CU_INVOICE_DETERMINANT_ACCOUNT tables to get commodity Id from determinant & charge table instead of CHA table.
NM 20-05-2020 MAINT-10332 added case statement to eliminate the invoice has ACCOUNT_ID ,Service_Month and Commodity_Id NUll if is reported is 1 
*****/
CREATE PROCEDURE [ETL].[Cu_Invoice_Get_Image_Changes](@in_Last_Processed BIGINT = 0)
AS
BEGIN
	SET NOCOUNT ON;

	IF 1 = 0
	BEGIN
		SELECT
			CAST(NULL AS INT) AS Client_Hier_Id
			, CAST(NULL AS INT) AS Commodity_Id
			, CAST(NULL AS INT) AS ACCOUNT_ID
			, CAST(NULL AS INT) AS Cu_Invoice_id
			, CAST(NULL AS DATE) AS Service_Month
			, CAST(NULL AS INT) AS CBMS_IMAGE_ID
			, CAST(NULL AS BIT) AS Is_Reported
			, CAST(NULL AS CHAR(1)) AS sys_Change_Operation
			, CAST(NULL AS VARCHAR(29)) AS DATA_Source;

	END;

	CREATE TABLE #Changes_Invoices
	(
		Client_Hier_Id		   INT
		, ACCOUNT_ID		   INT
		, Cu_Invoice_id		   INT
		, Service_Month		   DATE
		, CBMS_IMAGE_ID		   INT
		, Is_Reported		   BIT
		, sys_Change_Operation CHAR(1)
		, DATA_Source		   VARCHAR(100)
	);


	CREATE TABLE #Cu_Invoice_Image_Changes
	(
		Cu_Invoice_id  INT
		, ACCOUNT_ID   INT
		, Commodity_Id INT
	);

	INSERT INTO #Changes_Invoices
		(
			Client_Hier_Id
			, ACCOUNT_ID
			, Cu_Invoice_id
			, Service_Month
			, CBMS_IMAGE_ID
			, Is_Reported
			, sys_Change_Operation
			, DATA_Source
		)
	SELECT
		cha.Client_Hier_Id
		, ism.Account_ID
		, cng.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH) AS Service_Month
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED
		, CONVERT(CHAR(1), cng.SYS_CHANGE_OPERATION) AS sys_Change_Operation
		, 'CU_Invoice_Get_Images_Changes' AS DATA_Source
	FROM
		CHANGETABLE(CHANGES CU_INVOICE, @in_Last_Processed)cng
		LEFT JOIN CU_INVOICE_SERVICE_MONTH ism
			ON cng.CU_INVOICE_ID = ism.CU_INVOICE_ID AND ism.SERVICE_MONTH IS NOT NULL
		LEFT JOIN Core.Client_Hier_Account cha
			ON ism.Account_ID = cha.Account_Id
		LEFT JOIN dbo.CU_INVOICE ci
			ON ci.CU_INVOICE_ID = cng.CU_INVOICE_ID
	GROUP BY
		cha.Client_Hier_Id
		, ism.Account_ID
		, cng.CU_INVOICE_ID
		, CONVERT(DATE, ism.SERVICE_MONTH)
		, ci.CBMS_IMAGE_ID
		, ci.IS_REPORTED
		, cng.SYS_CHANGE_OPERATION;


	-- Data is continious: get changes            

	INSERT INTO #Cu_Invoice_Image_Changes
		(
			Cu_Invoice_id
			, ACCOUNT_ID
			, Commodity_Id
		)
	SELECT
		CIC.CU_INVOICE_ID
		, CICA.ACCOUNT_ID
		, CIC.COMMODITY_TYPE_ID AS Commodity_Id
	FROM
		dbo.CU_INVOICE_CHARGE CIC
		INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT CICA
			ON CICA.CU_INVOICE_CHARGE_ID = CICA.CU_INVOICE_CHARGE_ID
	WHERE
		CIC.COMMODITY_TYPE_ID IS NOT NULL
		AND CIC.COMMODITY_TYPE_ID <> -1
		AND EXISTS (
					   SELECT 1 FROM #Changes_Invoices ci WHERE CIC.CU_INVOICE_ID = ci.Cu_Invoice_id
																AND CICA.ACCOUNT_ID = ci.ACCOUNT_ID
				   )
		AND EXISTS (
					   SELECT
						   1
					   FROM
						   Core.Client_Hier_Account cha
					   WHERE
						   cha.Account_Id = CICA.ACCOUNT_ID AND cha.Commodity_Id = CIC.COMMODITY_TYPE_ID
				   )
	GROUP BY
		CIC.CU_INVOICE_ID
		, CICA.ACCOUNT_ID
		, CIC.COMMODITY_TYPE_ID;

	INSERT INTO #Cu_Invoice_Image_Changes
		(
			Cu_Invoice_id
			, ACCOUNT_ID
			, Commodity_Id
		)
	SELECT
		CID.CU_INVOICE_ID
		, CIDA.ACCOUNT_ID
		, CID.COMMODITY_TYPE_ID AS Commodity_Id
	FROM
		dbo.CU_INVOICE_DETERMINANT CID
		INNER JOIN dbo.CU_INVOICE_DETERMINANT_ACCOUNT CIDA
			ON CIDA.CU_INVOICE_DETERMINANT_ID = CID.CU_INVOICE_DETERMINANT_ID
	WHERE
		CID.COMMODITY_TYPE_ID IS NOT NULL
		AND CID.COMMODITY_TYPE_ID <> -1
		AND NOT EXISTS (
						   SELECT
							   1
						   FROM
							   #Cu_Invoice_Image_Changes ciic
						   WHERE
							   ciic.ACCOUNT_ID = CIDA.ACCOUNT_ID
							   AND ciic.Commodity_Id = CID.COMMODITY_TYPE_ID
							   AND ciic.Cu_Invoice_id = CID.CU_INVOICE_ID
					   )
		AND EXISTS (
					   SELECT 1 FROM #Changes_Invoices ci WHERE CID.CU_INVOICE_ID = ci.Cu_Invoice_id
																AND CIDA.ACCOUNT_ID = ci.ACCOUNT_ID
				   )
		AND EXISTS (
					   SELECT
						   1
					   FROM
						   Core.Client_Hier_Account cha
					   WHERE
						   cha.Account_Id = CIDA.ACCOUNT_ID AND cha.Commodity_Id = CID.COMMODITY_TYPE_ID
				   )
	GROUP BY
		CID.CU_INVOICE_ID
		, CIDA.ACCOUNT_ID
		, CID.COMMODITY_TYPE_ID;


	SELECT
		ciic.Client_Hier_Id
		, ci.Commodity_Id
		, ciic.ACCOUNT_ID
		, ciic.Cu_Invoice_id
		, ciic.Service_Month
		, ciic.CBMS_IMAGE_ID
		, CASE WHEN ciic.ACCOUNT_ID IS NULL OR ciic.Service_Month IS NULL OR ci.Commodity_Id IS NULL THEN
				   0
			  ELSE
				  ciic.Is_Reported
		  END AS Is_Reported
		, ciic.sys_Change_Operation
		, ciic.DATA_Source
	FROM
		#Changes_Invoices ciic
		LEFT JOIN #Cu_Invoice_Image_Changes ci
			ON ci.Cu_Invoice_id = ciic.Cu_Invoice_id AND ci.ACCOUNT_ID = ciic.ACCOUNT_ID
	GROUP BY
		ciic.Client_Hier_Id
		, ci.Commodity_Id
		, ciic.ACCOUNT_ID
		, ciic.Cu_Invoice_id
		, ciic.Service_Month
		, ciic.CBMS_IMAGE_ID
		, ciic.Is_Reported
		, ciic.sys_Change_Operation
		, ciic.DATA_Source;

	DROP TABLE #Cu_Invoice_Image_Changes;
	DROP TABLE #Changes_Invoices;
END;

GO


GRANT EXECUTE ON  [ETL].[Cu_Invoice_Get_Image_Changes] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Cu_Invoice_Get_Image_Changes] TO [ETL_Execute]
GO
