SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Cbms_Image_Upd_For_Image_Migration

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Invoice_Dt				Date

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @tvp_Invoice_Migration tvp_Invoice_Migration

	Exec Cbms_Image_Upd_For_Image_Migration tvp_Invoice_Migration


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AL			Ajeesh L
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	AL       	2018-05-21	Created for Cass Image loading
	KVK			10/29/2018	Modified to update all duplicate CBMS_Doc_Ids if one success. 
******/
CREATE PROCEDURE [dbo].[Cbms_Image_Upd_For_Image_Migration]
(
    @tvp_Invoice_Migration tvp_Invoice_Migration READONLY,
    @Image_Location_Id INT,
    @User_Info_Id INT,
    @CBMS_Image_Directory VARCHAR(255)
)
AS
BEGIN

    DECLARE @Completed_Cd INT,
            @RowCount INT;

    SELECT @Completed_Cd = cd.Code_Id
    FROM dbo.Code cd
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = cd.Codeset_Id
    WHERE cs.Codeset_Name = 'ICQ Batch Status'
          AND cd.Code_Value = 'Completed';

    DECLARE @Cass_Image_Location_Id INT;

    SELECT @Cass_Image_Location_Id = CBMS_Image_Location_Id
    FROM dbo.CBMS_Image_Location
    WHERE Image_Drive =
    (
        SELECT Parm_Value
        FROM UBM_Cass.dbo.Global_Parm
        WHERE Parm_Name = 'Image_Drive'
              AND UBM_ID =
              (
                  SELECT UBM_ID FROM dbo.UBM WHERE UBM_NAME = 'Cass'
              )
    );


    BEGIN TRY
        BEGIN TRANSACTION;

        UPDATE cimage
        SET cimage.CBMS_Image_FileName = tim.CBMS_Image_FileName,
            cimage.CBMS_Image_Directory = tim.CBMS_Image_Directory,
            cimage.CBMS_Image_Location_Id = @Image_Location_Id,
            cimage.CONTENT_TYPE = tim.CBMS_IMAGE_CONTENT_TYPE,
            cimage.CBMS_IMAGE_SIZE = tim.CBMS_IMAGE_SIZE,
            cimage.Last_Change_Ts = GETDATE()
        FROM dbo.cbms_image ci
            INNER JOIN @tvp_Invoice_Migration tim
                ON tim.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
            INNER JOIN dbo.cbms_image cimage
                ON cimage.CBMS_DOC_ID = ci.CBMS_DOC_ID
                   AND cimage.CBMS_Image_Location_Id = @Cass_Image_Location_Id
        WHERE tim.Status = 'Completed';

        SELECT @RowCount = @@ROWCOUNT;

        UPDATE dbo.CBMS_Image_Location
        SET Active_File_Cnt = CASE
                                  WHEN Active_Directory = @CBMS_Image_Directory THEN
                                      ISNULL(Active_File_Cnt, 0) + @RowCount
                                  ELSE
                                      @RowCount
                              END,
            Active_Directory = @CBMS_Image_Directory
        WHERE CBMS_Image_Location_Id = @Image_Location_Id;


        UPDATE iimb
        SET iimb.Status_Cd = @Completed_Cd,
            iimb.Last_Change_Ts = GETDATE(),
            iimb.Updated_User_Id = @User_Info_Id
        FROM dbo.Invoice_Image_Migration_Batch iimb
            INNER JOIN @tvp_Invoice_Migration tim
                ON tim.UBM_BATCH_MASTER_LOG_ID = iimb.UBM_Batch_Master_Log_Id;


        UPDATE iiml
        SET iiml.Status_Cd = cd.Code_Id,
            iiml.Last_Change_Ts = GETDATE(),
            iiml.Updated_User_Id = @User_Info_Id
        FROM dbo.Invoice_Image_Migration_Log iiml
            INNER JOIN @tvp_Invoice_Migration tim
                ON tim.UBM_BATCH_MASTER_LOG_ID = iiml.UBM_Batch_Master_Log_Id
                   AND tim.CBMS_IMAGE_ID = iiml.CBMS_Image_Id
            INNER JOIN dbo.Code cd
                ON cd.Code_Value = tim.Status
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = cd.Codeset_Id
        WHERE cs.Codeset_Name = 'ICQ Batch Status'
              AND tim.Status <> 'Completed';

        DELETE iiml
        FROM dbo.Invoice_Image_Migration_Log iiml
            INNER JOIN @tvp_Invoice_Migration tim
                ON tim.UBM_BATCH_MASTER_LOG_ID = iiml.UBM_Batch_Master_Log_Id
                   AND tim.CBMS_IMAGE_ID = iiml.CBMS_Image_Id
        WHERE tim.Status = 'Completed';
        COMMIT;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
            ROLLBACK;

        EXECUTE dbo.usp_RethrowError;
    END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[Cbms_Image_Upd_For_Image_Migration] TO [CBMSApplication]
GO
