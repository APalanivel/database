SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Client_Commodity_Variance_Test_Config_Upd

DESCRIPTION:

			To update table Client_Commodity_Variance_Test_Config
			
INPUT PARAMETERS:
	Name										DataType		Default	Description
---------------------------------------------------------------
	@Client_Commodity_Variance_Test_Config_Id	INT
    @Start_Dt									DATE
    @End_Dt										DATE
    @Variance_Test_Type_Cd						INT
    @User_Info_Id								INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------
   
	BEGIN TRANSACTION
	SELECT * FROM Client_Commodity_Variance_Test_Config WHERE Client_Commodity_Id=1528
	  EXEC dbo.Client_Commodity_Variance_Test_Config_Upd 
      @Client_Commodity_Variance_Test_Config_Id =2202
     ,@Client_Commodity_Id = 1528
     ,@Start_Dt = '2005-01-01'
     ,@End_Dt= NULL
     ,@Variance_Test_Type_Cd = 102246
     ,@User_Info_Id = 49
	SELECT * FROM Client_Commodity_Variance_Test_Config WHERE Client_Commodity_Id=1528		
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SP			Sandeep Pigilam
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SP       	2016-11-01	Created for Variance Test Enhancements Phase II.

******/

CREATE PROCEDURE [dbo].[Client_Commodity_Variance_Test_Config_Upd]
      ( 
       @Client_Commodity_Variance_Test_Config_Id INT
      ,@Client_Commodity_Id INT
      ,@Start_Dt DATE
      ,@End_Dt DATE
      ,@Variance_Test_Type_Cd INT
      ,@User_Info_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;

      
      UPDATE
            acirt
      SET   
            Client_Commodity_Id = @Client_Commodity_Id
           ,Start_Dt = @Start_Dt
           ,End_Dt = @End_Dt
           ,Variance_Test_Type_Cd = @Variance_Test_Type_Cd
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = GETDATE()
      FROM
            dbo.Client_Commodity_Variance_Test_Config acirt
      WHERE
            acirt.Client_Commodity_Variance_Test_Config_Id = @Client_Commodity_Variance_Test_Config_Id
      


END;
;

;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Variance_Test_Config_Upd] TO [CBMSApplication]
GO
