SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
  
NAME: [dbo].[Cost_Usage_Site_Dtl_Del_By_Client_Hier_Id]    
  
DESCRIPTION: To delete the details Cost_Usage_Site_Dtl_Id for a month , for deletion purpose  
  
        
INPUT PARAMETERS:            
 NAME    DATATYPE DEFAULT  DESCRIPTION           
--------------------------------------------------------------------  
 @Data_Source_Cd varchar  
 @Client_Hier_Id varchar  
 @Bucket_Master_Id varchar  
 @Service_Month varchar  
      
OUTPUT PARAMETERS:  
 NAME   DATATYPE DEFAULT  DESCRIPTION  
  
------------------------------------------------------------  
table data  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  Begin Tran  
    EXEC Cost_Usage_Site_Dtl_Del_By_Client_Hier_Id 100350,10918,101697,'2013-10-01'  
  Rollback Tran  
  
AUTHOR INITIALS:            
 INITIALS NAME  
------------------------------------------------------------  
 AS   Arun Skaria  
 RK   Raghu Kalvapudi
  
MODIFICATIONS:  
 
 INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
 AS   12-NOV-13  CREATED  
 RK   11/25/2013 Simplified Logic and removed IF- Condition. 
*****/  
  
CREATE PROCEDURE [dbo].[COST_USAGE_SITE_DTL_Del_By_Client_Hier_Id]
      (
       @Data_Source_Cd INT
      ,@Client_Hier_Id INT
      ,@Bucket_Master_Id INT
      ,@Service_Month DATE )
AS
BEGIN  
   
      SET NOCOUNT ON;
	
      DECLARE
            @Commodity_Id AS INT
           ,@BucketName VARCHAR(50)
           ,@BucketValue DECIMAL(28, 10)
           

      SELECT
            @BucketName = bm.Bucket_Name
           ,@Commodity_Id = bm.Commodity_Id
           ,@BucketValue = cusd.Bucket_Value
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
            INNER JOIN dbo.Bucket_Master bm
                  ON cusd.Bucket_Master_Id = bm.Bucket_Master_Id
      WHERE
            cusd.Client_Hier_Id = @Client_Hier_Id
            AND cusd.Bucket_Master_Id = @Bucket_Master_Id
            AND cusd.Service_Month = @Service_Month
            AND @BucketName IN ( 'Marketer Cost', 'Utility Cost' ) 


      UPDATE
            cusd
      SET
            Bucket_Value = Bucket_Value - isnull(@BucketValue, 0)
      FROM
            dbo.Cost_Usage_Site_Dtl cusd
            INNER JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
      WHERE
            cusd.Client_Hier_Id = @Client_Hier_Id
            AND cusd.Service_Month = @Service_Month
            AND bm.Bucket_Name = 'Total Cost'
            AND bm.Commodity_Id = @Commodity_Id
            AND @BucketName IN ( 'Marketer Cost', 'Utility Cost' )

                        			 
      DELETE FROM
            dbo.Cost_Usage_Site_Dtl
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            AND Bucket_Master_Id = @Bucket_Master_Id
            AND Service_Month = @Service_Month 
END  
;
GO
GRANT EXECUTE ON  [dbo].[COST_USAGE_SITE_DTL_Del_By_Client_Hier_Id] TO [CBMSApplication]
GO
