SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.SAVE_MARKET_FORECAST_DETAILS_P


 
@marketId int,
@QuaterYear int,
@LowPrice decimal(32,16),
@highPrice decimal(32,16),
@risk_tolerance varchar(50),
@year int,
@asOfDate Datetime


AS
	set nocount on
declare @risk_tolerance_id int
select @risk_tolerance_id = (select entity_id from entity where entity_name = @risk_tolerance and entity_type=1063)


insert into rm_market_forecast ( rm_market_id,
                                 low_range_price,
                                  hi_range_price,
                                 risk_tolerance_type_id,
                                 forecast_quarter,
                                 forecast_year,
                                 AsofDate) 
                         values (@marketId,
                                 @LowPrice,
                                 @highPrice, 
                                 @risk_tolerance_id,
                                 @QuaterYear,
                                 @year,
                                 @asOfDate)
GO
GRANT EXECUTE ON  [dbo].[SAVE_MARKET_FORECAST_DETAILS_P] TO [CBMSApplication]
GO
