SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Received_Not_Processed_Query_CBMS  
   
DESCRIPTION:    
     Received_Not_Processed_Query_CBMS  
    
INPUT PARAMETERS:    
 Name				  DataType	Default Description    
------------------------------------------------------------    
 @Client_Id           NVARCHAR(MAX)  
  
  
OUTPUT PARAMETERS:    
Name    DataType  Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  
  
 EXEC dbo.Received_Not_Processed_Query_CBMS '13082 ,11254'  
  
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
SP       Srinivasarao patchava  
  
   
MODIFICATIONS     
Initials Date			Modification    
------------------------------------------------------------    
SP		 2018/10/04      created  
SP       2019/08/16      Modified the script Input Paramter Data type VARCHAR(200) to NVARCHAR(MAX)
  
******/
CREATE PROCEDURE [dbo].[Received_Not_Processed_Query_CBMS]
    (
        @Client_Id NVARCHAR(MAX)
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  DISTINCT
                cch.Client_Name
                , cha.Display_Account_Number [Account_Number]
                , cch.Site_name
                , cha.Meter_State_Name
                , cc.Code_Dsc AS 'Collection Queue Type'
                , [Collection_Start_Dt]
                , [Collection_End_Dt]
                , [Is_Manual]
                , [Received_Status_Updated_Dt]
                , [Invoice_File_Name]
                , UI.USERNAME AS 'Updated By'
        FROM
            [CBMS].[dbo].[Invoice_Collection_Queue] icq
            JOIN dbo.Invoice_Collection_Account_Config CAC
                ON CAC.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = CAC.Account_Id
            JOIN Core.Client_Hier cch
                ON cch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN dbo.Commodity c
                ON c.Commodity_Id = icq.Commodity_Id
            JOIN dbo.USER_INFO UI
                ON icq.Updated_User_Id = UI.USER_INFO_ID
            INNER JOIN dbo.Code cc
                ON icq.Invoice_Collection_Queue_Type_Cd = cc.Code_Id
        WHERE
            icq.Received_Status_Updated_Dt IS NOT NULL
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@Client_Id, ',') ufn
                           WHERE
                                ufn.Segments = cch.Client_Id);

    END;

GO
GRANT EXECUTE ON  [dbo].[Received_Not_Processed_Query_CBMS] TO [CBMSApplication]
GO
