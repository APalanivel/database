
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec dbo.SR_RFP_LP_GET_ACCOUNT_TERM_P 358,5049

CREATE      PROCEDURE [dbo].[SR_RFP_LP_GET_ACCOUNT_TERM_P]
	@rfp_id int,
	@sr_rfp_account_id int
	AS	
	set nocount on
	--//replication 
	declare @is_replication varchar(5)
	select  @is_replication = entity_name from entity(nolock) where entity_type = 1040

	IF @sr_rfp_account_id > 0
	begin
		select	account_term.sr_account_group_id as sr_rfp_account_id,
		min(account_term.from_month)as from_month,
		max(account_term.to_month)as to_month,
		--(DATEDIFF(month, min(account_term.from_month), max(account_term.to_month))+ 1) as no_of_months
		max(account_term.NO_OF_MONTHS) AS no_of_months

		from 	sr_rfp_account_term account_term(nolock),
			sr_rfp_term term(nolock),
			sr_rfp_account rfp_account(nolock) 
		
		where	term.sr_rfp_id = @rfp_id
			and account_term.sr_rfp_term_id = term.sr_rfp_term_id
			and rfp_account.sr_rfp_account_id = account_term.sr_account_group_id
			and rfp_account.sr_rfp_account_id = @sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and account_term.is_bid_group = 0			
			and account_term.is_sdp = 0
	
		group by account_term.sr_account_group_id
	
		union

		select	rfp_account.sr_rfp_account_id as sr_rfp_account_id,
		min(account_term.from_month)as from_month,
		max(account_term.to_month)as to_month,
		--(DATEDIFF(month, min(account_term.from_month),max(account_term.to_month))+ 1) as no_of_months
       max(account_term.NO_OF_MONTHS) AS no_of_months
		from 	sr_rfp_account_term account_term(nolock),
			sr_rfp_term term(nolock),
			sr_rfp_account rfp_account(nolock) 
		
		where	term.sr_rfp_id = @rfp_id
			and account_term.sr_rfp_term_id = term.sr_rfp_term_id
			and rfp_account.sr_rfp_bid_group_id = account_term.sr_account_group_id
			and rfp_account.is_deleted = 0
			and rfp_account.sr_rfp_account_id = @sr_rfp_account_id
			and account_term.is_bid_group = 1			
			and account_term.is_sdp = 0
	
		group by rfp_account.sr_rfp_account_id
	end
	else
	begin

		select	account_term.sr_account_group_id as sr_rfp_account_id,
		min(account_term.from_month)as from_month,
		max(account_term.to_month)as to_month,
		--(DATEDIFF(month, min(account_term.from_month), max(account_term.to_month))+ 1) as no_of_months
		max(account_term.NO_OF_MONTHS) AS no_of_months
		from 	sr_rfp_account_term account_term(nolock),
			sr_rfp_term term(nolock),
			sr_rfp_account rfp_account(nolock) 
		
		where	term.sr_rfp_id = @rfp_id
			and account_term.sr_rfp_term_id = term.sr_rfp_term_id
			and rfp_account.sr_rfp_account_id = account_term.sr_account_group_id
			and rfp_account.is_deleted = 0
			and account_term.is_bid_group = 0
			and account_term.is_sdp = 0
	
		group by account_term.sr_account_group_id
	
		union

		select	rfp_account.sr_rfp_account_id as sr_rfp_account_id,
		min(account_term.from_month)as from_month,
		max(account_term.to_month)as to_month,
		--(DATEDIFF(month, min(account_term.from_month),max(account_term.to_month))+ 1) as no_of_months
        max(account_term.NO_OF_MONTHS) AS no_of_months
		from 	sr_rfp_account_term account_term(nolock),
			sr_rfp_term term(nolock),
			sr_rfp_account rfp_account(nolock) 
		
		where	term.sr_rfp_id = @rfp_id
			and account_term.sr_rfp_term_id = term.sr_rfp_term_id
			and rfp_account.sr_rfp_bid_group_id = account_term.sr_account_group_id
			and rfp_account.is_deleted = 0
			and account_term.is_bid_group = 1
			and account_term.is_sdp = 0
	
		group by rfp_account.sr_rfp_account_id

	end
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_ACCOUNT_TERM_P] TO [CBMSApplication]
GO
