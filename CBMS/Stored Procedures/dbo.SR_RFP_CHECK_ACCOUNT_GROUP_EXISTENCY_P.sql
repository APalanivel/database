SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_CHECK_ACCOUNT_GROUP_EXISTENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@isBidGroup    	int       	          	
	@supplierContactVendorMapId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE DBO.SR_RFP_CHECK_ACCOUNT_GROUP_EXISTENCY_P

@accountGroupId int,
@isBidGroup int,
@supplierContactVendorMapId int

AS
set nocount on
	select count(1) 
	
	from	SR_RFP_REASON_NOT_WINNING
	
	where 	SR_ACCOUNT_GROUP_ID = @accountGroupId
		and IS_BID_GROUP = @isBidGroup
		and SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CHECK_ACCOUNT_GROUP_EXISTENCY_P] TO [CBMSApplication]
GO
