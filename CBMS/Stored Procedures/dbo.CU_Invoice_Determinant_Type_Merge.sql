
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.CU_Invoice_Determinant_Type_Merge

DESCRIPTION:


INPUT PARAMETERS:
	Name					 DataType			Default				Description
------------------------------------------------------------------------------------
	@Invoice_Determinant	 CU_Invoice_Determinant_Type (its a type as table to accept the input from user as table type)

OUTPUT PARAMETERS:
	Name					 DataType			Default				Description
------------------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------------------
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	HG			Harihara Suthan G
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------------------------------
	SKA        	06/15/2010	Created
	SKA        	06/21/2010	Added the out put clause so that for child proc we can eliminate join with base tables
	SKA        	06/24/2010	Removed two coulumns from update statement (UBM_SERVICE_TYPE_ID,UBM_SERVICE_CODE)
	HG			09/01/2010	Changed the logic to populate the ubm_meter_number column , added LEFT JOIN of Cu_Invoice and Ubm table join and changed the meter_number selection using CASE statement.
							(For AS400 Ubm_Meter_Number should be populated as Ubm_Meter_Number for others determinant_name should be considered as Meter_Number)
	SKA			09/07/2010	Changed the case condition for Ubm_Meter_Number
    DMR			09/04/2012	Modified Output to Table Variable then output Table Variable This is to allow Triggers to base tables.	
    NR			2015-06-04	For AS400 Added new input parameter EC_Invoice_Sub_Bucket_Master_Id to TVP CU_Invoice_Determinant_Type.
******/
 
CREATE PROCEDURE [dbo].[CU_Invoice_Determinant_Type_Merge]
      ( 
       @Invoice_Determinant AS CU_Invoice_Determinant_Type READONLY )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE @OutTable TABLE
            ( 
             CU_INVOICE_DETERMINANT_ID INT NULL
            ,CU_DETERMINANT_CODE VARCHAR(55) NULL )      
      --DELETEING THE RECORD FROM CHILD TABLE AS WE HAVE TO DELETE THE RECORD FROM PARENT TABLE
      DELETE
            cida
      FROM
            @Invoice_Determinant cid
            INNER JOIN dbo.CU_Invoice_Determinant_Account cida
                  ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
      WHERE
            cid.Is_Delete = 1
 
      MERGE INTO dbo.CU_Invoice_Determinant AS tgt
            USING 
                  ( SELECT
                        id.CU_INVOICE_DETERMINANT_ID
                       ,id.CU_INVOICE_ID
                       ,id.COMMODITY_TYPE_ID
                       ,CASE WHEN u.Ubm_Name = 'AS400' THEN id.Determinant_Name
                             ELSE id.UBM_METER_NUMBER
                        END Ubm_Meter_Number
                       ,id.UBM_SERVICE_TYPE_ID
                       ,id.UBM_SERVICE_CODE
                       ,id.UBM_BUCKET_CODE
                       ,id.UNIT_OF_MEASURE_TYPE_ID
                       ,id.UBM_UNIT_OF_MEASURE_CODE
                       ,id.DETERMINANT_NAME
                       ,id.DETERMINANT_VALUE
                       ,id.UBM_INVOICE_DETAILS_ID
                       ,id.CU_DETERMINANT_NO
                       ,id.CU_DETERMINANT_CODE
                       ,id.Bucket_Master_Id
                       ,id.Is_Delete
                       ,id.EC_Invoice_Sub_Bucket_Master_Id
                    FROM
                        @Invoice_Determinant id
                        LEFT JOIN dbo.Cu_Invoice ci
                              ON ci.Cu_Invoice_Id = id.Cu_Invoice_Id
                        LEFT JOIN dbo.Ubm u
                              ON u.Ubm_Id = ci.Ubm_Id ) AS src
            ON tgt.CU_INVOICE_DETERMINANT_ID = src.CU_Invoice_Determinant_Id
            WHEN MATCHED AND Is_Delete = 0
                  THEN UPDATE
                    SET 
                        tgt.CU_INVOICE_ID = src.CU_INVOICE_ID
                       ,tgt.COMMODITY_TYPE_ID = src.COMMODITY_TYPE_ID
                       ,tgt.UBM_METER_NUMBER = src.UBM_METER_NUMBER
                       ,tgt.UBM_BUCKET_CODE = src.UBM_BUCKET_CODE
                       ,tgt.UNIT_OF_MEASURE_TYPE_ID = src.UNIT_OF_MEASURE_TYPE_ID
                       ,tgt.UBM_UNIT_OF_MEASURE_CODE = src.UBM_UNIT_OF_MEASURE_CODE
                       ,tgt.DETERMINANT_NAME = src.DETERMINANT_NAME
                       ,tgt.DETERMINANT_VALUE = src.DETERMINANT_VALUE
                       ,tgt.UBM_INVOICE_DETAILS_ID = src.UBM_INVOICE_DETAILS_ID
                       ,tgt.CU_DETERMINANT_NO = src.CU_DETERMINANT_NO
                       ,tgt.CU_DETERMINANT_CODE = src.CU_DETERMINANT_CODE
                       ,tgt.Bucket_Master_Id = src.Bucket_Master_Id
                       ,tgt.EC_Invoice_Sub_Bucket_Master_Id = src.EC_Invoice_Sub_Bucket_Master_Id
            WHEN MATCHED AND Is_Delete = 1
                  THEN DELETE
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         CU_INVOICE_ID
                        ,COMMODITY_TYPE_ID
                        ,UBM_METER_NUMBER
                        ,UBM_SERVICE_TYPE_ID
                        ,UBM_SERVICE_CODE
                        ,UBM_BUCKET_CODE
                        ,UNIT_OF_MEASURE_TYPE_ID
                        ,UBM_UNIT_OF_MEASURE_CODE
                        ,DETERMINANT_NAME
                        ,DETERMINANT_VALUE
                        ,UBM_INVOICE_DETAILS_ID
                        ,CU_DETERMINANT_NO
                        ,CU_DETERMINANT_CODE
                        ,Bucket_Master_Id
                        ,EC_Invoice_Sub_Bucket_Master_Id )
                    VALUES
                        ( 
                         src.CU_INVOICE_ID
                        ,src.COMMODITY_TYPE_ID
                        ,src.UBM_METER_NUMBER
                        ,src.UBM_SERVICE_TYPE_ID
                        ,src.UBM_SERVICE_CODE
                        ,src.UBM_BUCKET_CODE
                        ,src.UNIT_OF_MEASURE_TYPE_ID
                        ,src.UBM_UNIT_OF_MEASURE_CODE
                        ,src.DETERMINANT_NAME
                        ,src.DETERMINANT_VALUE
                        ,src.UBM_INVOICE_DETAILS_ID
                        ,src.CU_DETERMINANT_NO
                        ,src.CU_DETERMINANT_CODE
                        ,src.Bucket_Master_Id
                        ,src.EC_Invoice_Sub_Bucket_Master_Id )
            OUTPUT
                  INSERTED.CU_INVOICE_DETERMINANT_ID
                 ,INSERTED.CU_DETERMINANT_CODE
                  INTO @OutTable
                        ( CU_INVOICE_DETERMINANT_ID, CU_DETERMINANT_CODE );
      SELECT
            CU_INVOICE_DETERMINANT_ID
           ,CU_DETERMINANT_CODE
      FROM
            @OutTable
            
END;


;
GO


GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Type_Merge] TO [CBMSApplication]
GO
