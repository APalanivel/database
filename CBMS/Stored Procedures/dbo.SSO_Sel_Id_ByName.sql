
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                      
NAME:                      
 dbo.SSO_Sel_Id_ByName                   
                      
DESCRIPTION:                      
 This procedure checks whether roll up for security role id completed and email has been sent or not.                  
                                 
INPUT PARAMETERS:                      
 Name              DataType          Default     Description                      
----------------------------------------------------------------                      
  @client_name      VarChar
 ,@currency         VarChar
 ,@epUOM		    VarChar	
 ,@ngUOM			VarChar
 ,@role			    VarChar
                    
                           
OUTPUT PARAMETERS:                      
 Name              DataType          Default     Description                      
----------------------------------------------------------------                      
                      
USAGE EXAMPLES:                      
------------------------------------------------------------                      
 EXEC dbo.SSO_Sel_Id_ByName  'Starwood Hotels And Resorts','USD','kWh','MMBtu','Site Level User'     
                               
AUTHOR INITIALS:                      
      Initials    Name           Date                         
------------------------------------------------------------                      
      V N        Varada Nair     13-March-2014                      
      AS		 Arun Skaria	 30-Jun-2014  
	  MJ		 Meera Joy		 17-July-2017                            
MODIFICATIONS:                      
      Initials    Date           Modification                      
------------------------------------------------------------                      
      V N        27-Mar-2014     Included providername as input param to get its code value from code table
	  AS		 30-Jun-2014	 Philips66 SSO change- added default value (NULL) for parameters
	  MJ		 17-July-2017    MAINT-5464 - removed federationId column
******/        
          
CREATE PROCEDURE [dbo].[SSO_Sel_Id_ByName]
      ( 
       @client_name VARCHAR(100)
      ,@currency VARCHAR(100) = NULL
      ,@epUOM VARCHAR(100) = NULL
      ,@ngUOM VARCHAR(100) = NULL
      ,@role VARCHAR(100) = NULL
     )
AS 
BEGIN                      
                      
      SET NOCOUNT ON                      
      
      DECLARE
            @EP_UOM_Id INT
           ,@NG_UOM_Id INT
           ,@currencyId INT
           ,@clientId INT
           ,@roleId INT
	 
      SELECT
            @EP_UOM_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = @epUOM
            AND e.ENTITY_TYPE = 101      
			
	
      SELECT
            @NG_UOM_Id = e.ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_NAME = @ngUOM
            AND e.ENTITY_TYPE = 102      
 
      SELECT
            @currencyId = Currency_unit_id
      FROM
            dbo.CURRENCY_UNIT
      WHERE
            Currency_unit_name = @currency   


      SELECT
            @clientId = CLIENT_ID
      FROM
            dbo.CLIENT
      WHERE
            CLIENT_NAME = @client_name


      SELECT
            @roleId = Client_App_Access_Role_Id
      FROM
            dbo.Client_App_Access_Role
      WHERE
            Client_Id = @clientId
            AND App_Access_Role_Name = @role
      SELECT
            @EP_UOM_Id AS epUOM
           ,@NG_UOM_Id AS ngUOM
           ,@currencyId AS currencyId
           ,@clientId AS clientId
           ,@roleId AS roleId
END;

;

;
GO


GRANT EXECUTE ON  [dbo].[SSO_Sel_Id_ByName] TO [CBMSApplication]
GO
