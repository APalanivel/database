SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_SAD_RPT_GET_SITES_P
(@clientId int )
AS
	set nocount on
if(@clientId > 0)
	SELECT site_id, site_name from vwSiteName
	where client_id = @clientId
else
	SELECT site_id, site_name from vwSiteName
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_SITES_P] TO [CBMSApplication]
GO
