SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE    PROCEDURE dbo.BUDGET_UPDATE_CONTRACT_BUDGET_DETAIL_P
	@budget_contract_budget_detail_id int,
	@volume decimal(32,16),
	@market_id int,
	@fuel decimal(32,16),
	@multiplier decimal(32,16),
	@adder decimal(32,16),
	@volume_unit_type_id int,
	@tax decimal(32,16),
	@currency_unit_id int,
	@isNymex bit

	AS

	update budget_contract_budget_detail set volume = @volume, 
						 market_id = @market_id, 
						 fuel = @fuel, 
						 multiplier = @multiplier, 
						 adder = @adder, 
						 volume_unit_type_id = @volume_unit_type_id, 
						 tax = @tax, 
						 currency_unit_id = @currency_unit_id,
						 is_nymex_forecast = @isNymex

	where budget_contract_budget_detail_id = @budget_contract_budget_detail_id










GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_CONTRACT_BUDGET_DETAIL_P] TO [CBMSApplication]
GO
