SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsUbmVendorMap_Get]
	( @MyAccountId int
	, @ubm_vendor_map_id int
	)
AS
BEGIN

	   select ubm_vendor_map_id
		, ubm_id
		, ubm_vendor_code
		, vendor_id
	     from ubm_vendor_map
	    where ubm_vendor_map_id = @ubm_vendor_map_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmVendorMap_Get] TO [CBMSApplication]
GO
