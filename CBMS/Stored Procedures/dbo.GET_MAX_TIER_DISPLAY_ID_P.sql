SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_MAX_TIER_DISPLAY_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rateId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_MAX_TIER_DISPLAY_ID_P
	@rateId	INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT MAX(CONVERT(INT,(SUBSTRING(cm.charge_display_id,2,LEN(cm.charge_display_id)))))
	FROM dbo.charge_master cm INNER JOIN dbo.entity e ON e.entity_id = cm.charge_type_id
	WHERE charge_parent_id= @rateId 
		AND charge_parent_type_id = 96
		AND e.entity_name ='Tier' 
		AND e.entity_type =113

END
GO
GRANT EXECUTE ON  [dbo].[GET_MAX_TIER_DISPLAY_ID_P] TO [CBMSApplication]
GO
