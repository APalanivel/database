SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_PM

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P '6584','',0,'05/04/2006','04/04/2006',0,0,642,0  
  
exec SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P '','',0,'05/30/2005','04/30/2006',0,0,648,0  

exec dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P 5114, '', 0, '8/6/2007', '7/6/2008', 0, 420, 648,0  

exec dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P -1,-1, 0, '8/6/2007', '7/6/2008', 0, 420, 648,0  

exec dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P '','',0,'','10/31/2007',0,443,0,0  

exec dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P '','',0,'05/30/2005','04/30/2006',0,0,648,0    
    


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	09/01/2009	Added contract_id as join between contract and supplier_account_meter_map in place of account_id


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P 
 @userId VARCHAR(10),  
 @sessionId VARCHAR(50),  
 @regionId INT,  
 @fromDate VARCHAR(12),  
 @asOfDate VARCHAR(12),  
 @stateId INT,  
 @utilityId INT,  
 @supplierId INT,  
 @analystId INT  
AS  

BEGIN  
  
 DECLARE @selectClause VARCHAR(2000)  
  , @fromClause VARCHAR(3000)  
  , @whereClause VARCHAR(1000)   
  , @groupClause VARCHAR(1000)  
  , @SQLStatement VARCHAR(8000)  
  , @iscoprate INT  
   
 DECLARE @el_Commodity_type_id INT  
  , @sup_contract_type_id INT  
  
 SELECT @el_Commodity_type_id = entity_id FROM dbo.entity (NOLOCK) WHERE entity_name='Electric Power' AND entity_type = 157  
 SELECT @sup_contract_type_id = entity_id FROM dbo.entity (NOLOCK) WHERE entity_name='Supplier' AND entity_type = 129  
  
 SELECT @selectClause = ' DISTINCT ' +
 '''' +  'Electric Power' + '''' + ' AS COMMODITY,  
   utility.Vendor_name AS UTILITY_NAME,  
   STATE_NAME,  
   REGION_NAME,  
   user_info.first_name + SPACE(1) + user_info.last_name AS ANALYST,  
   supplier.Vendor_name AS SUPPLIER_NAME,  
   dbo.SR_RFP_FN_GET_AVAILABLE_VOLUMES(utilacc.account_id,'+ '''' + @fromDate + '''' + ',' + ''''+ @asOfDate+ ''''+') AS ACCOUNTS_SERVED ,  
   dbo.SR_RFP_FN_GET_VOLUMES_SERVED(utilacc.account_id,' + '''' + @fromDate + '''' + ',' + '''' + @asOfDate + '''' + ') AS VOLUME_SERVED '  
  
 SELECT @fromClause = ' dbo.account utilacc(NOLOCK)  
   JOIN dbo.meter met(NOLOCK) ON met.account_id = utilacc.account_id  
   JOIN dbo.supplier_account_meter_map map(NOLOCK) ON map.meter_id = met.meter_id  
   JOIN dbo.account suppacc(NOLOCK) ON suppacc.account_id = map.account_id  
   JOIN dbo.contract con(NOLOCK) ON con.contract_id = map.contract_id
   JOIN dbo.Vendor supplier(NOLOCK) ON supplier.Vendor_id = suppacc.Vendor_id  
   JOIN dbo.Vendor utility(NOLOCK) ON utility.Vendor_id = utilacc.Vendor_id  
   JOIN dbo.Vendor_state_map Vendor_map(NOLOCK) ON Vendor_map.Vendor_id = utility.Vendor_id  
   JOIN dbo.state (NOLOCK) ON state.state_id = Vendor_map.state_id  
   JOIN dbo.region (NOLOCK) ON region.region_id = state.region_id  
   JOIN dbo.UTILITY_DETAIL det ON det.Vendor_ID = utility.Vendor_id  
   JOIN dbo.UTILITY_ANALYST_MAP anaMap ON anaMap.UTILITY_DETAIL_ID = det.UTILITY_DETAIL_ID  
   JOIN dbo.user_info ON user_info.user_info_id = anaMap.power_analyst_id '  
  
 SELECT @whereClause = ' con.commodity_type_id = ' + STR(@el_Commodity_type_id) + '  
   AND con.contract_type_id = ' + STR(@sup_contract_type_id) + '   
   AND ' + '''' + @asOfDate + '''' + ' BETWEEN con.contract_start_date AND con.contract_end_date   
   AND utilacc.not_managed = 0 '  
  
 IF (@regionId<>0)  
  BEGIN  
  SELECT @whereClause=@whereClause+ ' AND region.region_id = ' + STR(@regionId)  
  END  
  
 IF (@stateId<>0)  
  BEGIN  
  SELECT @whereClause=@whereClause+' AND state.state_id = ' + STR(@stateId)  
  END  
       
 IF (@utilityId <> 0)  
 BEGIN  
  SELECT @whereClause=@whereClause + ' AND utility.Vendor_id = ' + STR(@utilityId)  
 END  
  
 IF (@supplierId <> 0)  
 BEGIN  
  SELECT @whereClause =@whereClause + ' AND supplier.Vendor_id = ' + STR(@supplierId)  
 END  
  
 SELECT @groupClause ='GROUP BY utility.Vendor_name,  
    STATE_NAME,  
    REGION_NAME,  
    user_info.first_name + SPACE(1) + user_info.last_name,  
    supplier.Vendor_name ,  
    utilacc.account_id  
    ORDER BY utility.Vendor_name,  
     supplier.Vendor_name '  
  
 SELECT @SQLStatement = ' SELECT ' +  
    @selectClause +   
    ' FROM ' +   
    @fromClause +  
     ' WHERE ' +  
    @whereClause + @groupClause  
  
 --PRINT @SQLStatement  
 
 EXEC(@SQLStatement)  
  
END
GO
GRANT EXECUTE ON  [dbo].[SR_SW_REP_GET_EP_VOLUMES_MARKET_SHARE_P] TO [CBMSApplication]
GO
