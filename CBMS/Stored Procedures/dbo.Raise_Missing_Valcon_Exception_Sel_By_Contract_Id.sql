SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: dbo.Raise_Missing_Valcon_Exception_Sel_By_Contract_Id
     
DESCRIPTION: 
			Raised the missing valcon exception when  contract image is uploaded 
			and check if any invoice in un associated list and configuration is in complete.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------



	EXEC dbo.Raise_Missing_Valcon_Exception_Sel_By_Contract_Id  118269

	select * from Valcon_Account_Config where contract_Id =
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	NR			Narayana Reddy

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	NR			2020-02-20		MAINT-9782 - Added IS_PROCESSED flag in Temp table.                       

*/


CREATE PROCEDURE [dbo].[Raise_Missing_Valcon_Exception_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Raise_Missing_Valcon_Exception BIT = 0;


        CREATE TABLE #Invoice_List
             (
                 Cu_Invoice_Id INT
                 , Account_Id INT
                 , CBMS_IMAGE_ID INT
                 , Begin_Dt DATE
                 , End_Dt DATE
                 , IS_PROCESSED BIT
             );


        -- > To save the contract  un associated list for Supplier & consoldidated.

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 0
            , @Is_Dis_Associated_Invoice = 1;

        INSERT INTO #Invoice_List
             (
                 Cu_Invoice_Id
                 , Account_Id
                 , CBMS_IMAGE_ID
                 , Begin_Dt
                 , End_Dt
                 , IS_PROCESSED
             )
        EXEC dbo.Associate_Dis_Associate_Invoice_Sel_By_Contract_Id_Account_Id_For_Consolidated
            @Contract_Id = @Contract_Id
            , @Account_Id = NULL
            , @Is_Associated_Invoice = 0
            , @Is_Dis_Associated_Invoice = 1;





        SELECT
            @Raise_Missing_Valcon_Exception = 1
        FROM
        (   SELECT
                ISNULL(vac.Is_Config_Complete, 0) AS Is_Config_Complete
            FROM
                #Invoice_List il
                LEFT JOIN dbo.Valcon_Account_Config vac
                    ON vac.Account_Id = il.Account_Id
                       AND  vac.Contract_Id = @Contract_Id) X
        WHERE
            X.Is_Config_Complete = 0;

        SELECT  @Raise_Missing_Valcon_Exception AS Raise_Missing_Valcon_Exception;


    END;

GO
GRANT EXECUTE ON  [dbo].[Raise_Missing_Valcon_Exception_Sel_By_Contract_Id] TO [CBMSApplication]
GO
