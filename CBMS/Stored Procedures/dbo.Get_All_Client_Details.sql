SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************    
NAME :	dbo.Get_All_Client_Details 
   
DESCRIPTION: 
Get all client details from client,vwClientFiscalYearStartMonth and Security_Role table.
If client has any role as corporate 1 then the indicator will return 'Yes' else 'No'
   
INPUT PARAMETERS:    
Name             DataType               Default        Description    
@ClientID			INT						NULL		Filter the output based on this value
--------------------------------------------------------------------      

OUTPUT PARAMETERS:    
Name   DataType  Default Description    
--------------------------------------------------------------------    
CLIENT_ID		INT
CLIENT_NAME		VARCHAR
start_month		DATE
Is_Corporate	INT

USAGE EXAMPLES:    
--------------------------------------------------------------------    

[dbo].[Get_All_Client_Details]--4030
[dbo].[Get_All_Client_Details] @ClientID=14525

AUTHOR INITIALS:    
Initials	Name    
-------------------------------------------------------------------    
KVK			K VINAY KUMAR
BM			BIJU MOHAN
   
MODIFICATIONS     
Initials	Date		Modification    
--------------------------------------------------------------------    
BM			08/31/2010  Include input parameter @ClientID
						Applied coding standards
KVK			10/08/2010	Updated the code to use insted of INNER JOIN used LEFT JOIN
KVK			12/01/2010	Revert back the changes done on 10/08/2010.	
RKV			2018-03-22	SE2017-273,Added New column security_Role_Id				
******************************************************************************************************/
CREATE  PROCEDURE [dbo].[Get_All_Client_Details]
      ( 
       @ClientID AS INT = NULL
      ,@Is_Invoice_Collection_Setup_Exists BIT = 0 )
AS 
BEGIN
      SET NOCOUNT ON;
      BEGIN TRY
            IF ( @ClientID IS NULL )
			-- get all client details
                  SELECT
                        C.CLIENT_ID
                       ,C.CLIENT_NAME
                       ,SM.start_month
                       ,CASE WHEN COALESCE(Is_Corporate, 0) = 1 THEN 'Yes'
                             ELSE 'No'
                        END AS Is_Corporate_Ind
                       ,sr.Security_Role_Id 
                  FROM
                        CLIENT C
                        INNER JOIN vwClientFiscalYearStartMonth SM
                              ON SM.client_id = C.CLIENT_ID
                        LEFT OUTER JOIN Security_Role SR
                              ON ( SR.Client_Id = C.CLIENT_ID
                                   AND SR.Is_Corporate = 1 )
                  WHERE
                        ( @Is_Invoice_Collection_Setup_Exists = 0
                          OR ( @Is_Invoice_Collection_Setup_Exists = 1
                               AND EXISTS ( SELECT
                                                1
                                            FROM
                                                dbo.Invoice_Collection_Client_Config ic
                                            WHERE
                                                ic.Client_Id = C.CLIENT_ID ) ) )
                  ORDER BY
                        C.CLIENT_NAME
            ELSE
			--get client details of the given ClientID
                  SELECT
                        C.CLIENT_ID
                       ,C.CLIENT_NAME
                       ,SM.start_month
                       ,CASE WHEN COALESCE(Is_Corporate, 0) = 1 THEN 'Yes'
                             ELSE 'No'
                        END AS Is_Corporate_Ind
                        ,sr.Security_Role_Id
                  FROM
                        CLIENT C
                        INNER JOIN vwClientFiscalYearStartMonth SM
                              ON SM.client_id = C.CLIENT_ID
                        LEFT OUTER JOIN Security_Role SR
                              ON ( SR.Client_Id = C.CLIENT_ID
                                   AND SR.Is_Corporate = 1 )
                  WHERE
                        C.CLIENT_ID = @ClientID
                        AND ( @Is_Invoice_Collection_Setup_Exists = 0
                              OR ( @Is_Invoice_Collection_Setup_Exists = 1
                                   AND EXISTS ( SELECT
                                                      1
                                                FROM
                                                      dbo.Invoice_Collection_Client_Config ic
                                                WHERE
                                                      ic.Client_Id = C.CLIENT_ID ) ) )
                  ORDER BY
                        C.CLIENT_NAME
      END TRY
	
      BEGIN CATCH
            EXEC usp_RethrowError
      END CATCH
END


;
GO
GRANT EXECUTE ON  [dbo].[Get_All_Client_Details] TO [CBMSApplication]
GO
