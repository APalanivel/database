SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--exec BUDGET_GET_MOST_RECENT_BUDGET_ACCOUNT_P 6833, 1, 2008
CREATE  PROCEDURE dbo.BUDGET_GET_MOST_RECENT_BUDGET_ACCOUNT_P
	@budget_account_id int,
	@start_month int,
	@start_year int
	AS
	
	begin
		set nocount on

		declare @account_id int, @budget_id int, @commodity_type_id int
		
		select	@account_id= budget_account.account_id, 
			@budget_id = budget.budget_id,
			@commodity_type_id = budget.commodity_type_id
		
		from budget, budget_account 
		
		where 	budget_account.budget_account_id = @budget_account_id
			and budget.budget_id = budget_account.budget_id
			and budget.budget_start_month = @start_month
			and budget.budget_start_year = @start_year
		
		select 	budget_account.budget_id, 
			budget_account.budget_account_id,
			budget.budget_start_month,
			budget.budget_start_year,
			entity.entity_name,
			case  when (budget.posted_by > 0)
			then 1
			else 0
			end is_posted
		 
		from budget, budget_account, entity
		where 	budget_account.account_id = @account_id
			and budget_account.budget_id < @budget_id
			and budget.budget_id = budget_account.budget_id
			and budget.commodity_type_id = @commodity_type_id
			and entity.entity_id = budget.commodity_type_id
			and budget.date_initiated = (	select max(b.date_initiated)
							from budget b, budget_account a
							where 	a.account_id = @account_id
								and a.budget_id < @budget_id
								and b.budget_id = a.budget_id
								and b.commodity_type_id = @commodity_type_id
						    )
	end












GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_MOST_RECENT_BUDGET_ACCOUNT_P] TO [CBMSApplication]
GO
