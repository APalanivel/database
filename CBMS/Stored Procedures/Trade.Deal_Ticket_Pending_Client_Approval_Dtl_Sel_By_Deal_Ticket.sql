SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Pending_Client_Approval_Dtl_Sel_By_Deal_Ticket
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Internal_Approval_Pending_Sel  
	Exec Trade.Deal_Ticket_Pending_Client_Approval_Dtl_Sel_By_Deal_Ticket '333,561' 
	
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy

    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-05-22	GRM Proejct.


     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Pending_Client_Approval_Dtl_Sel_By_Deal_Ticket]
    (
        @Deal_Ticket_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            dt.Deal_Ticket_Id
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END AS CM_Name
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END AS CM_Email
            , ch.Client_Name
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
              END AS Site_Name
            , REPLACE(CONVERT(VARCHAR(20), MAX(chws.Created_Ts), 106), ' ', '-') AS Status_Date
            , LTRIM(RIGHT(CONVERT(VARCHAR(20), MAX(chws.Created_Ts), 100), 7)) AS Status_Time
            , ui2.EMAIL_ADDRESS AS Initiator_Email
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.ENTITY dttyp
                ON dt.Hedge_Type_Cd = dttyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            CROSS APPLY
        (   SELECT
                ui.EMAIL_ADDRESS + ','
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
                INNER JOIN Trade.Deal_Ticket dt1
                    ON ccm.CLIENT_ID = dt.Client_Id
            WHERE
                dt1.Deal_Ticket_Id = dt.Deal_Ticket_Id
            GROUP BY
                ui.EMAIL_ADDRESS
            FOR XML PATH('')) CM(CM_Email)
            CROSS APPLY
        (   SELECT
                ui.FIRST_NAME + ' ' + ui.LAST_NAME + ', '
            FROM
                dbo.USER_INFO ui
                INNER JOIN dbo.CLIENT_CEM_MAP ccm
                    ON ui.USER_INFO_ID = ccm.USER_INFO_ID
                INNER JOIN Trade.Deal_Ticket dt1
                    ON ccm.CLIENT_ID = dt.Client_Id
            WHERE
                dt1.Deal_Ticket_Id = dt.Deal_Ticket_Id
            GROUP BY
                ui.FIRST_NAME + ' ' + ui.LAST_NAME
            FOR XML PATH('')) CMName(CM_Name)
            LEFT JOIN dbo.USER_INFO ui2
                ON dt.Created_User_Id = ui2.USER_INFO_ID
        WHERE
            chws.Is_Active = 1
            AND ws.Workflow_Status_Name IN ( 'Pending Client Approval', 'Requested Client Approval' )
            AND (   @Deal_Ticket_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Deal_Ticket_Id, ',') us
                                   WHERE
                                        us.Segments = dt.Deal_Ticket_Id))
        GROUP BY
            dt.Deal_Ticket_Id
            , CASE WHEN LEN(CM.CM_Email) > 0 THEN LEFT(CM.CM_Email, LEN(CM.CM_Email) - 1)
              END
            , ch.Client_Name
            , CASE WHEN LEN(CMName.CM_Name) > 0 THEN LEFT(CMName.CM_Name, LEN(CMName.CM_Name) - 1)
              END
            , ui2.EMAIL_ADDRESS;


    END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Pending_Client_Approval_Dtl_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
