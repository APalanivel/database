SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:   [dbo].[Invoice_Collection_Batch_Dtls_Ins]                 
                        
             
        
 Input Parameters:                        
    Name     DataType   Default     Description                          
--------------------------------------------------------------------------------------         
                         
 Output Parameters:                              
    Name     DataType   Default     Description                          
--------------------------------------------------------------------------------------                          
        
 Usage Examples:                            
--------------------------------------------------------------------------------------             
  BEGIN TRAN        
 exec dbo.Invoice_Collection_Batch_Dtls_Ins_Received_Not_Processed_Records        
  ROLLBACK        
        
Author Initials:                        
    Initials Name                        
--------------------------------------------------------------------------------------                          
 RKV   Ravi Kumar Vegesna        
 HG    Harihara suthan Ganesan        
        
 Modifications:                        
    Initials        Date  Modification                        
--------------------------------------------------------------------------------------                          
    RKV    2020-04-23  Created to load batch for the types 'Received Not Processed', 'Freq Next Config Run Dt', 'Chase Priority Date'   
         
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtls_Ins_Received_Not_Processed_Records]
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Record_Cnt INT = 1
            , @ICBatchType_Code_Cnt INT = 1
            , @Status_Cd INT
            , @Upload_Batch_Id INT = NULL
            , @Start_Dt DATETIME
            , @ICR_Status_Cd INT
            , @ICR_Batch_Type_Cd INT
            , @Batch_Size INT
            , @In_Progress_Status_Cd INT;

        DECLARE @ICBatchType_Codes TABLE
              (
                  Id INT IDENTITY(1, 1)
                  , Code_Id INT
                  , Code_Value VARCHAR(25)
              );
        
		 DECLARE @Custom_Frequency_Cd INT;



        SELECT
            @Custom_Frequency_Cd = c.Code_Id
        FROM
            Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            Code_Value = 'Custom'
            AND cs.Codeset_Name = 'InvoiceFrequency';
		
		CREATE TABLE #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
             );
        CREATE TABLE #IC_Account_Config_Ids_Chase_Priority_Date
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
             );


        INSERT INTO @ICBatchType_Codes
             (
                 Code_Id
                 , Code_Value
             )
        SELECT
            Code_Id
            , Code_Value
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value IN ( 'Received Not Processed', 'Freq Next Config Run Dt', 'Chase Priority Date' )
        ORDER BY
            c.Display_Seq;


        INSERT INTO #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
        WHERE
            icac.Is_Chase_Activated = 1
            AND icac.is_Config_Complete = 1
            AND (   EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_collection_New_Batch_Clients icnbc
                               WHERE
                                    icnbc.Client_Id = ch.Client_Id)
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Account_Invoice_Collection_Frequency aicf
                                   WHERE
                                        aicf.Invoice_Frequency_Cd = @Custom_Frequency_Cd
                                        AND aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id))
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id;

        INSERT INTO #Active_Icac
             (
                 Invoice_Collection_Account_Config_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Is_Chase_Activated = 1
            AND icac.is_Config_Complete = 1
            AND NOT EXISTS (SELECT  TOP 1   1 FROM  #Active_Icac)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id;


        SELECT
            @Batch_Size = CAST(App_Config_Value AS INT)
        FROM
            dbo.App_Config ac
        WHERE
            App_Config_Cd = 'IC_Batch_Size';



        SELECT
            @ICR_Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Status_Cd'
            AND c.Code_Value = 'Received';





        SELECT
            @Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'Pending';

        SELECT
            @In_Progress_Status_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Queue_Batch_Status_Cd'
            AND c.Code_Value = 'In Progress';



        CREATE TABLE #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
                 , Collection_Start_Dt DATE NOT NULL
                 , Collection_End_Dt DATE NOT NULL
                 , Invoice_Collection_Batch_Type_Cd INT
                 , Row_Num INT IDENTITY(1, 1)
             );



        -- Invoice Received but not resolved        

        SELECT
            @ICR_Batch_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value = 'Received Not Processed';


        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @ICR_Batch_Type_Cd
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            icq.Status_Cd = @ICR_Status_Cd
            AND DATEDIFF(dd, icq.Received_Status_Updated_Dt, GETDATE()) >= 5
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl icbd
                               WHERE
                                    icbd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;

        -- -- Chase_Priority_Date          

        SELECT
            @ICR_Batch_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value = 'Chase Priority Date';



        INSERT INTO #IC_Account_Config_Ids_Chase_Priority_Date
             (
                 Invoice_Collection_Account_Config_Id
             )
        EXEC dbo.Invoice_Collection_Account_Config_Sel_By_Chase_Priority_Date;

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @ICR_Batch_Type_Cd
        FROM
            dbo.#IC_Account_Config_Ids_Chase_Priority_Date icaccpd
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icaccpd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Is_Chase_Activated = 1
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Invoice_Collection_Batch_Dtl bd
                               WHERE
                                    icac.Invoice_Collection_Account_Config_Id = bd.Invoice_Collection_Account_Config_Id
                                    AND bd.Invoice_Collection_Batch_Type_Cd = @ICR_Batch_Type_Cd)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;


        --Next_Config_Run_Date  
        SELECT
            @ICR_Batch_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICBatchType'
            AND c.Code_Value = 'Freq Next Config Run Dt';

        INSERT INTO #Invoice_Collection_Batch_Dtl
             (
                 Invoice_Collection_Account_Config_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Batch_Type_Cd
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt
            , @ICR_Batch_Type_Cd
        FROM
            dbo.Account_Invoice_Collection_Frequency aicf
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON aicf.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            CAST(aicf.Next_Config_Run_Date AS DATE) = CAST(GETDATE() AS DATE)
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Active_Icac ai
                           WHERE
                                ai.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , icac.Invoice_Collection_Service_Start_Dt
            , icac.Invoice_Collection_Service_End_Dt;


        DELETE
        icbd
        FROM
            #Invoice_Collection_Batch_Dtl icbd
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = icbd.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
        WHERE
            cha.Account_Not_Managed = 1;



        SELECT
            icbd.Invoice_Collection_Account_Config_Id
            , icbd.Collection_Start_Dt
            , icbd.Collection_End_Dt
            , icbd.Invoice_Collection_Batch_Type_Cd
            , ROW_NUMBER() OVER (PARTITION BY
                                     icbd.Invoice_Collection_Batch_Type_Cd
                                 ORDER BY
                                     icbd.Invoice_Collection_Account_Config_Id) Sno
            , icbd.Row_Num
        INTO
            #Invoice_Collection_Batch_Dtl_loop
        FROM
            #Invoice_Collection_Batch_Dtl icbd;
        DROP TABLE #Invoice_Collection_Batch_Dtl;
        DROP TABLE #IC_Account_Config_Ids_Chase_Priority_Date;

        BEGIN TRY
            BEGIN TRAN;
            WHILE (@ICBatchType_Code_Cnt <= (SELECT MAX(occ.Id)FROM @ICBatchType_Codes occ))
                BEGIN
                    WHILE (@Record_Cnt <= (   SELECT
                                                    MAX(icbt.Sno)
                                              FROM
                                                    #Invoice_Collection_Batch_Dtl_loop icbt
                                                    INNER JOIN @ICBatchType_Codes occ
                                                        ON occ.Code_Id = icbt.Invoice_Collection_Batch_Type_Cd
                                              WHERE
                                                    occ.Id = @ICBatchType_Code_Cnt))
                        BEGIN

                            INSERT INTO dbo.Invoice_Collection_Batch
                                 (
                                     Invoice_Collection_Batch_Type_Cd
                                     , Status_Cd
                                 )
                            SELECT
                                occ.Code_Id
                                , @Status_Cd
                            FROM
                                @ICBatchType_Codes occ
                            WHERE
                                occ.Id = @ICBatchType_Code_Cnt;

                            SELECT  @Upload_Batch_Id = SCOPE_IDENTITY();

                            INSERT INTO dbo.Invoice_Collection_Batch_Dtl
                                 (
                                     Invoice_Collection_Account_Config_Id
                                     , Invoice_Collection_Batch_Id
                                     , Collection_Start_Dt
                                     , Collection_End_Dt
                                     , Status_Cd
                                 )
                            SELECT
                                Invoice_Collection_Account_Config_Id
                                , @Upload_Batch_Id
                                , MAX(Collection_Start_Dt)
                                , MAX(Collection_End_Dt)
                                , @Status_Cd
                            FROM
                                #Invoice_Collection_Batch_Dtl_loop icbd
                                INNER JOIN @ICBatchType_Codes occ
                                    ON occ.Code_Id = icbd.Invoice_Collection_Batch_Type_Cd
                            WHERE
                                icbd.Sno BETWEEN @Record_Cnt
                                         AND     @Record_Cnt + (CAST(@Batch_Size AS INT) - 1)
                                AND occ.Id = @ICBatchType_Code_Cnt
                            GROUP BY
                                Invoice_Collection_Account_Config_Id;

                            SET @Record_Cnt = @Record_Cnt + CAST(@Batch_Size AS INT);
                        END;
                    SET @Record_Cnt = 1;
                    SET @ICBatchType_Code_Cnt = @ICBatchType_Code_Cnt + 1;
                END;





            DROP TABLE #Invoice_Collection_Batch_Dtl_loop;


            COMMIT TRAN;
        END TRY
        BEGIN CATCH

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRAN;
                END;

            EXEC dbo.usp_RethrowError;

        END CATCH;

    END;




    ;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtls_Ins_Received_Not_Processed_Records] TO [CBMSApplication]
GO
