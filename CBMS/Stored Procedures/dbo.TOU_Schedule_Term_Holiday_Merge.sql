SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Term_Holiday_Merge

DESCRIPTION:  
	Merges the holidays of a particular term

INPUT PARAMETERS:
Name							DataType        Default     Description
-------------------------------------------------------------------
@Term_Holiday_Details			Term_Holiday_Details
@Time_Of_Use_Schedule_Term_Id	INT
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

DECLARE @Term_Holiday_Details XML = N'
<Term_Holiday_Details>
      <Details Holiday_Master_Id = "1" Is_Rule_One_Active = "1" Is_Rule_Two_Active = "1" Is_Manual_Override = "1" Holiday_Dt = "02/28/2011"/>
</Term_Holiday_Details>'


Exec dbo.TOU_Schedule_Term_Holiday_Merge @Term_Holiday_Details, 1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar


	Initials Date		Modification
------------------------------------------------------------
	CPE		2012-07-11  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Term_Holiday_Merge
    (
     @Term_Holiday_Details XML
    ,@Time_Of_Use_Schedule_Term_Id INT 
    )
AS 
    BEGIN
        SET NOCOUNT ON

        MERGE INTO dbo.Time_Of_Use_Schedule_Term_Holiday tgt
            USING 
                ( SELECT
					   T.C.value('@Holiday_Master_Id', 'INT') AS Holiday_Master_Id
					  ,T.C.value('@Is_Rule_One_Active', 'BIT') AS Is_Rule_One_Active
					  ,T.C.value('@Is_Rule_Two_Active', 'BIT') AS Is_Rule_Two_Active
					  ,T.C.value('@Is_Manual_Override', 'BIT') AS Is_Manual_Override
					  ,T.C.value('@Holiday_Dt', 'DATE') AS Input_Holiday_Dt
					  ,HM.Holiday_Dt
				   FROM
					   @Term_Holiday_Details.nodes('/Term_Holiday_Details/Details') T ( c )
					   JOIN dbo.Holiday_Master HM
 						    ON T.C.value('@Holiday_Master_Id', 'INT') = HM.Holiday_Master_Id
                ) src
            ON tgt.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                AND tgt.Holiday_Master_Id = src.Holiday_Master_Id
            WHEN NOT MATCHED 
                THEN 
	  INSERT  (
                 Time_Of_Use_Schedule_Term_Id
                ,Holiday_Master_Id
                ,Is_Rule_One_Active
                ,Is_Rule_Two_Active
                ,Is_Manual_Override
                ,Holiday_Dt 
                )   VALUES
                ( 
                 @Time_Of_Use_Schedule_Term_Id
                ,Holiday_Master_Id
                ,Is_Rule_One_Active
                ,Is_Rule_Two_Active
                ,Is_Manual_Override
			    ,CASE src.Is_Manual_Override
				    WHEN 1 THEN CASE WHEN DATENAME(dw, src.Input_Holiday_Dt) = 'Saturday'
				 						  AND src.Is_Rule_One_Active = 1 THEN DATEADD(dd, -1, src.Input_Holiday_Dt)
									 WHEN DATENAME(dw, src.Input_Holiday_Dt) = 'Sunday'
										  AND src.Is_Rule_Two_Active = 1 THEN DATEADD(dd, 1, src.Input_Holiday_Dt)
									 ELSE src.Input_Holiday_Dt
							    END
				    WHEN 0 THEN CASE WHEN DATENAME(dw, src.Holiday_Dt) = 'Saturday'
										  AND src.Is_Rule_One_Active = 1 THEN DATEADD(dd, -1, src.Holiday_Dt)
									 WHEN DATENAME(dw, src.Holiday_Dt) = 'Sunday'
										  AND src.Is_Rule_Two_Active = 1 THEN DATEADD(dd, 1, src.Holiday_Dt)
									 ELSE src.Holiday_Dt
							    END
				  END 
                )
            WHEN MATCHED 
                THEN
	  UPDATE      SET
                Is_Rule_One_Active = src.Is_Rule_One_Active
               ,Is_Rule_Two_Active = src.Is_Rule_Two_Active
               ,Is_Manual_Override = src.Is_Manual_Override
			   ,Holiday_Dt = CASE src.Is_Manual_Override
							   WHEN 1 THEN CASE WHEN DATENAME(dw, src.Input_Holiday_Dt) = 'Saturday'
													 AND src.Is_Rule_One_Active = 1 THEN DATEADD(dd, -1, src.Input_Holiday_Dt)
												WHEN DATENAME(dw, src.Input_Holiday_Dt) = 'Sunday'
													 AND src.Is_Rule_Two_Active = 1 THEN DATEADD(dd, 1, src.Input_Holiday_Dt)
												ELSE src.Input_Holiday_Dt
										   END
							   WHEN 0 THEN CASE WHEN DATENAME(dw, src.Holiday_Dt) = 'Saturday'
													 AND src.Is_Rule_One_Active = 1 THEN DATEADD(dd, -1, src.Holiday_Dt)
												WHEN DATENAME(dw, src.Holiday_Dt) = 'Sunday'
													 AND src.Is_Rule_Two_Active = 1 THEN DATEADD(dd, 1, src.Holiday_Dt)
												ELSE src.Holiday_Dt
										   END
							 END 
            WHEN NOT MATCHED BY SOURCE AND tgt.Time_Of_Use_Schedule_Term_Id = @Time_Of_Use_Schedule_Term_Id
                THEN 
	  DELETE ;
    END 

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Holiday_Merge] TO [CBMSApplication]
GO
