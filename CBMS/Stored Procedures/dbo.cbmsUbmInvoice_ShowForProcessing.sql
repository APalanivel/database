SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUbmInvoice_ShowForProcessing]
	( @MyAccountId int
	, @ubm_invoice_id int 
	)
AS
BEGIN

	EXEC cbmsUbmInvoice_GetForProcessing @MyAccountId, @ubm_invoice_id
	EXEC cbmsUbmInvoiceDeterminant_GetForUbmInvoice @MyAccountId, @ubm_invoice_id
	EXEC cbmsUbmInvoiceCharge_GetForUbmInvoice @MyAccountId, @ubm_invoice_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_ShowForProcessing] TO [CBMSApplication]
GO
