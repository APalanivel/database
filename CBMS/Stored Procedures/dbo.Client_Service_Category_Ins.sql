SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Client_Service_Category_Ins


DESCRIPTION: 

Insert data in to Client_Service_Category table.

INPUT PARAMETERS:    
      Name					DataType          Default     Description    
------------------------------------------------------------------    

      @Service_Category_Name NVARCHAR(200)
      @Client_Id			INT
      @Uom_Id				INT
      @Service_Level_Cd		INT
      

    
OUTPUT PARAMETERS:    
      Name                DataType          Default     Description    
------------------------------------------------------------    

	@Client_Service_Category_Id INT	OUTPUT


USAGE EXAMPLES:
------------------------------------------------------------
begin tran
declare @client_service_category_id int
Exec Client_Service_Category_Ins  'ng test 2',10069,12,1106,@client_service_category_id out
select @client_service_category_id

rollback tran

select top 3 * from client_service_category order by 1 desc

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	BCH			Balaraju

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	BCH			2011-10-19	Created 

******/

CREATE PROCEDURE dbo.Client_Service_Category_Ins
      ( 
       @Service_Category_Name NVARCHAR(200)
      ,@Client_Id INT
      ,@Uom_Id INT
      ,@Service_Level_Cd INT
      ,@Client_Service_Category_Id INT OUTPUT )
AS 
BEGIN
      SET NOCOUNT ON

      INSERT      INTO dbo.Client_Service_Category
                  ( 
                   csc.Service_Category_Name
                  ,csc.Client_Id
                  ,csc.Uom_Id
                  ,csc.Service_Level_Cd )
      VALUES
                  ( 
                   @Service_Category_Name
                  ,@Client_Id
                  ,@Uom_Id
                  ,@Service_Level_Cd )

      SET @Client_Service_Category_Id = scope_identity()

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Ins] TO [CBMSApplication]
GO
