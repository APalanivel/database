SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rm_Batch_Configuration_Details_Del
     
DESCRIPTION: 
	It Deletes Rm_Batch_Configuration_Details for Selected Rm_Batch_Configuration_Details_Id.
      
INPUT PARAMETERS:          
	NAME								DATATYPE	DEFAULT		DESCRIPTION         
---------------------------------------------------------------------------
	@Rm_Batch_Configuration_Details_Id	INT			
    
OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Rm_Batch_Configuration_Details_Del 6546
	ROLLBACK TRAN
	
	SELECT * FROM dbo.Rm_Batch_Configuration_Details 
	WHERE Rm_Batch_Configuration_Details_Id = 6546

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			21-JULY-10	CREATED

*/

CREATE PROCEDURE dbo.Rm_Batch_Configuration_Details_Del
    (
       @Rm_Batch_Configuration_Details_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rm_Batch_Configuration_Details
	WHERE
		Rm_Batch_Configuration_Details_Id = @Rm_Batch_Configuration_Details_Id
END
GO
GRANT EXECUTE ON  [dbo].[Rm_Batch_Configuration_Details_Del] TO [CBMSApplication]
GO
