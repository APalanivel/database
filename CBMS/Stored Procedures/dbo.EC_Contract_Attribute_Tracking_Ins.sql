SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Contract_Attribute_Tracking_Ins           
              
Description:              
        To insert Data to EC_Contract_Attribute_Tracking table.              
              
 Input Parameters:              
     Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
     @Contract_Id							INT
     @EC_Contract_Attribute_Id				INT
     @Start_Dt							DATE
     @End_Dt							DATE
     @EC_Contract_Attribute_Value			NVARCHAR(255)
     @User_Info_Id						INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	BEGIN TRAN  
	SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Value='test' and Contract_id=10866
	EXEC dbo.EC_Contract_Attribute_Tracking_Ins 
      @Contract_Id =10866
      ,@EC_Contract_Attribute_Id =2
      ,@Start_Dt ='2015-05-06'
      ,@End_Dt ='2015-06-06'
      ,@EC_Contract_Attribute_Value ='test'
      ,@User_Info_Id =100
	SELECT * FROM dbo.EC_Contract_Attribute_Tracking WHERE EC_Contract_Attribute_Value='test' and Contract_id=10866
	ROLLBACK TRAN     
	
	
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-09-16		Created For AS400-II.         
             
******/ 
CREATE PROCEDURE [dbo].[EC_Contract_Attribute_Tracking_Ins]
      ( 
       @Contract_Id INT
      ,@EC_Contract_Attribute_Id INT
      ,@EC_Contract_Attribute_Value NVARCHAR(255)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
      
      
      INSERT      INTO dbo.EC_Contract_Attribute_Tracking
                  ( 
                   Contract_Id
                  ,EC_Contract_Attribute_Id
                  ,EC_Contract_Attribute_Value
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
      VALUES
                  ( 
                   @Contract_Id
                  ,@EC_Contract_Attribute_Id
                  ,@EC_Contract_Attribute_Value
                  ,@User_Info_Id
                  ,getdate()
                  ,@User_Info_Id
                  ,getdate() )
                  
      
                  
            
END
   


;
;
GO
GRANT EXECUTE ON  [dbo].[EC_Contract_Attribute_Tracking_Ins] TO [CBMSApplication]
GO
