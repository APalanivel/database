
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Site_Commodity_Image_Ins

DESCRIPTION:
		
	For Site level cost usage tracking user can upload the invoice_image in Edit Cost_usage page , this procedure 
	used to save the cbms image id for the given site , commodity and service month.

INPUT PARAMETERS:
	Name			    DataType		Default	Description
------------------------------------------------------------
	@Client_Hier_Id    Int
	@Commodity_id	    Int
	@Service_Month	    Date
	@Cbms_Image_id	    Int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN
	EXEC dbo.Site_Commodity_Image_Ins 5351,65,'2012-10-01',12530180
ROLLBACK TRAN

	SELECT * FROM Site_Commodity_Image

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	AP		Athmaram Pabbathi
	
MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	HG        03/16/2010   Created
	AP		03/19/2012   Replaced @Site_Id parameter with @Client_Hier_Id (Addl Data Changes)

******/

CREATE PROCEDURE dbo.Site_Commodity_Image_Ins
      @Client_Hier_Id INT
     ,@Commodity_Id INT
     ,@Service_Month DATE
     ,@Cbms_Image_Id INT
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE @Site_Id INT
      
      SELECT
            @Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      INSERT      INTO dbo.Site_Commodity_Image
                  ( 
                   Site_Id
                  ,Commodity_Id
                  ,Service_Month
                  ,Cbms_Image_Id )
      VALUES
                  ( 
                   @Site_Id
                  ,@Commodity_Id
                  ,@Service_Month
                  ,@Cbms_Image_Id )

END
;
GO

GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Ins] TO [CBMSApplication]
GO
