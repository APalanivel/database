SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_IM_MAX_MIN_IMAGEID_P  
AS  
BEGIN  
  
	SET NOCOUNT ON;

	SELECT MIN(cbms_image_id) AS MinImageId
		, MAX(cbms_image_id) AS MaxImageId
	FROM dbo.CBMS_image

END
GO
GRANT EXECUTE ON  [dbo].[GET_IM_MAX_MIN_IMAGEID_P] TO [CBMSApplication]
GO
