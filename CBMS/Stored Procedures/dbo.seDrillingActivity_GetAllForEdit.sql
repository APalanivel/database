SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seDrillingActivity_GetAllForEdit
	( @FilterYear char(4) = null )
As
BEGIN
	set nocount on
	 select DrillingActivityId
				, ReportDate
				, Volume
		 from seDrillingActivity
	  where year(ReportDate) = isNull(@FilterYear, year(ReportDate))
 order by ReportDate desc

END
GO
GRANT EXECUTE ON  [dbo].[seDrillingActivity_GetAllForEdit] TO [CBMSApplication]
GO
