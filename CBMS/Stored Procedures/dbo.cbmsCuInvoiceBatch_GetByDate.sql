SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuInvoiceBatch_GetByDate

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@begin_date    	datetime  	          	
	@end_date      	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsCuInvoiceBatch_GetByDate]
	( @MyAccountId int
	, @begin_date datetime
	, @end_date datetime
	)
AS
BEGIN

	   select distinct cb.cu_invoice_batch_id
		, cb.batch_date
		, ubm.ubm_name
		, l.ubm_batch_master_log_id
		, isNull(uic.invoice_count, 0) 			ubm_invoice_count
		, isNull(uipc.processed_invoice_count, 0) 	ubm_invoice_count_processed
		, isNull(uinpc.not_processed_invoice_count, 0)	ubm_invoice_count_not_processed
		, isNull(cuic.cu_invoice_count, 0)		cu_invoice_count

	     from cu_invoice_batch cb
	     join ubm_invoice ui on ui.cu_invoice_batch_id = cb.cu_invoice_batch_id
	     join ubm_batch_master_log l on l.ubm_batch_master_log_id = ui.ubm_Batch_master_log_id
	     join ubm on ubm.ubm_id = l.ubm_id

  left outer join (select ubm_batch_master_log_id
			, count(*) invoice_count
		     from ubm_invoice
		 group by ubm_batch_master_log_id
		  ) uic on uic.ubm_batch_master_log_id = l.ubm_batch_master_log_id

  left outer join (select ubm_batch_master_log_id
			, count(*) processed_invoice_count
		     from ubm_invoice
		    where is_processed = 1
		 group by ubm_batch_master_log_id

		  ) uipc on uipc.ubm_batch_master_log_id = l.ubm_batch_master_log_id

  left outer join (select ubm_batch_master_log_id
			, count(*) not_processed_invoice_count
		     from ubm_invoice
		    where is_processed = 0
		 group by ubm_batch_master_log_id

		  ) uinpc on uinpc.ubm_batch_master_log_id = l.ubm_batch_master_log_id

  left outer join (select ui.ubm_batch_master_log_id
			, count(distinct cu.cu_invoice_id) cu_invoice_count
		     from ubm_invoice ui
		     join cu_invoice cu on cu.ubm_invoice_id = ui.ubm_invoice_id
		 group by ui.ubm_batch_master_log_id

		  ) cuic on cuic.ubm_batch_master_log_id = l.ubm_batch_master_log_id

	    where cb.batch_date between @begin_date and @end_date

	 order by cb.cu_invoice_batch_id
		, cb.batch_date
		, ubm.ubm_name
		, l.ubm_batch_master_log_id



END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceBatch_GetByDate] TO [CBMSApplication]
GO
