
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Get_EL_Cost_Usage_Site_Summary]  
     
DESCRIPTION:
	To get usage and cost of all divisions of given client and year

INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Report_Year				INT
@Currency_Unit_Id			INT				
@EL_Unit_of_Measure_Type_Id	INT				
@Client_Id				INT
@Division_Id				INT		NULL
@Client_Hier_Id			INT		NULL

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Get_EL_Cost_Usage_Site_Summary 2008,3,12,218,353
	EXEC dbo.Get_EL_Cost_Usage_Site_Summary 2009,3,12,170,409
	EXEC dbo.Get_EL_Cost_Usage_Site_Summary 2009,3,12,10069,570,12580
	EXEC dbo.Get_EL_Cost_Usage_Site_Summary 2011,3,12,218,352,2794
     
AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
RR			Raghu Reddy
AP			Athmaram Pabbathi

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
RR			07/25/2011  Created to fix MAINT-759
RR			07/27/2011  Required sites,cost,usage and other details are collected into local Temporary table two avoid referring the
						respective tables twice
RR			08/08/2011  Invoice_Participation_Site table is replaced with Invoice_Participation
RR			08/19/2011  Removed columns month_complete (x 12 months), month_published  (x 12 months), month_under_review  (x 12 months), a total of 36
						as they are not being used in the application
AP			09/07/2011	Did the following changes as a part of Addnl Data changes
						- Replaced Cost_Usage_Site table with Cost_Usage_Site_Dtl
						- Removed unused variables & select statement used for month variables
AP			04/04/2012	Replaced @Site_Id parameter with @Client_Hier_Id; added Client_Hier_Id in the result set and used Client_Hier table for finding fiscal @Begin_Dt & @End_Dt calculation
RR			2012-07-12	Renamed output column name "Client_Hier_Id" to "Site_Client_Hier_Id"
*/
CREATE PROCEDURE dbo.Get_EL_Cost_Usage_Site_Summary
      ( 
       @Report_Year INT
      ,@Currency_Unit_Id INT
      ,@EL_Unit_of_Measure_Type_Id INT
      ,@Client_Id INT
      ,@Division_Id INT = NULL
      ,@Client_Hier_Id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON ;

      DECLARE
            @Begin_Dt DATE
           ,@End_Dt DATE
           ,@EL_Commodity_Id INT
  
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )
            
      CREATE TABLE #Service_Month
            ( 
             Service_Month DATE PRIMARY KEY CLUSTERED
            ,Month_Num SMALLINT )  

      CREATE TABLE #Sites_Temp
            ( 
             Client_Hier_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(500)
            ,Client_Currency_Group_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) ) 

      CREATE TABLE #CUSummary_Temp
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,EL_Usage DECIMAL(32, 16)
            ,EL_Cost DECIMAL(32, 16)
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Service_Month ) )
            
      SELECT
            @EL_Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'


      SELECT
            @Begin_Dt = fy.Start_Month
           ,@End_Dt = dateadd(MONTH, -1, ( dateadd(YEAR, 1, fy.Start_Month) ))
      FROM
            ( SELECT
                  dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE)) Start_Month
              FROM
                  Core.Client_Hier ch
              WHERE
                  ch.Client_ID = @Client_Id
                  AND ch.Sitegroup_Id = 0
                  AND ch.Site_id = 0
              UNION ALL
              SELECT
                  cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS VARCHAR(10))
              WHERE
                  @client_id IS NULL ) fy

  
      INSERT      INTO #Service_Month
                  ( 
                   Service_Month
                  ,Month_Num )
                  SELECT
                        dd.Date_D AS Service_Month
                       ,Month_Num = row_number() OVER ( ORDER BY dd.Date_D )
                  FROM
                        meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN @Begin_Dt AND @End_Dt  

      INSERT      INTO #Sites_Temp
                  ( 
                   Client_Hier_Id
                  ,Site_Id
                  ,Site_Name
                  ,Client_Currency_Group_Id )
                  SELECT
                        ch.Client_Hier_Id
                       ,CH.Site_Id
                       ,rtrim(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_Name + ')' Site_Name
                       ,ch.Client_Currency_Group_Id
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        CH.Client_Id = @Client_Id
                        AND CH.Site_Closed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_Id > 0
                        AND ( @Division_Id IS NULL
                              OR CH.Sitegroup_Id = @Division_Id )
                        AND ( @Client_Hier_Id IS NULL
                              OR CH.Client_Hier_Id = @Client_Hier_Id )
                            
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @EL_Commodity_Id       

      INSERT      INTO #CUSummary_Temp
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,EL_Usage
                  ,EL_Cost )
                  SELECT
                        st.Client_Hier_Id
                       ,CUSD.Service_Month
                       ,sum(case WHEN bkt.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * CUC.Conversion_Factor
                            END) AS EL_Usage
                       ,sum(case WHEN bkt.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                            END) AS EL_Cost
                  FROM
                        #Sites_Temp st
                        INNER JOIN dbo.Cost_Usage_Site_Dtl CUSD
                              ON CUSD.Client_Hier_Id = st.Client_Hier_Id
                        INNER JOIN #Service_Month sm
                              ON cusd.Service_Month = sm.Service_Month
                        INNER JOIN @Cost_Usage_Bucket_Id bkt
                              ON bkt.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion CUC
                              ON CUC.Base_Unit_Id = CUSD.UOM_Type_Id
                                 AND CUC.converted_unit_id = @EL_Unit_of_Measure_Type_Id
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                              ON CurConv.Base_Unit_Id = CUSD.Currency_Unit_Id
                                 AND CurConv.Conversion_Date = CUSD.Service_Month
                                 AND CurConv.Currency_Group_Id = ST.Client_Currency_Group_Id
                                 AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                  GROUP BY
                        ST.Client_Hier_Id
                       ,CUSD.Service_Month ;


      WITH  Cte_Results
              AS ( SELECT
                        sg.Client_Hier_Id
                       ,sg.Site_Id
                       ,sg.site_name
                       ,sm.SERVICE_MONTH
                       ,Month1 = ( case WHEN sm.Month_Num = 1 THEN cu.EL_Cost
                                   END )
                       ,Month2 = ( case WHEN sm.Month_Num = 2 THEN cu.EL_Cost
                                   END )
                       ,Month3 = ( case WHEN sm.Month_Num = 3 THEN cu.EL_Cost
                                   END )
                       ,Month4 = ( case WHEN sm.Month_Num = 4 THEN cu.EL_Cost
                                   END )
                       ,Month5 = ( case WHEN sm.Month_Num = 5 THEN cu.EL_Cost
                                   END )
                       ,Month6 = ( case WHEN sm.Month_Num = 6 THEN cu.EL_Cost
                                   END )
                       ,Month7 = ( case WHEN sm.Month_Num = 7 THEN cu.EL_Cost
                                   END )
                       ,Month8 = ( case WHEN sm.Month_Num = 8 THEN cu.EL_Cost
                                   END )
                       ,Month9 = ( case WHEN sm.Month_Num = 9 THEN cu.EL_Cost
                                   END )
                       ,Month10 = ( case WHEN sm.Month_Num = 10 THEN cu.EL_Cost
                                    END )
                       ,Month11 = ( case WHEN sm.Month_Num = 11 THEN cu.EL_Cost
                                    END )
                       ,Month12 = ( case WHEN sm.Month_Num = 12 THEN cu.EL_Cost
                                    END )
                       ,Total = EL_Cost
                       ,field_type = 'Total Cost'
                       ,field = 'Total Cost'
                   FROM
                        #Sites_Temp sg
                        CROSS APPLY #Service_Month sm
                        LEFT OUTER JOIN #CUSummary_Temp cu
                              ON cu.Client_Hier_Id = sg.Client_Hier_Id
                                 AND cu.SERVICE_MONTH = sm.Service_Month
                   UNION ALL
                   SELECT
                        sg.Client_Hier_Id
                       ,sg.Site_Id
                       ,sg.site_name
                       ,sm.SERVICE_MONTH
                       ,Month1 = ( case WHEN sm.Month_Num = 1 THEN cu.EL_Usage
                                   END )
                       ,Month2 = ( case WHEN sm.Month_Num = 2 THEN cu.EL_Usage
                                   END )
                       ,Month3 = ( case WHEN sm.Month_Num = 3 THEN cu.EL_Usage
                                   END )
                       ,Month4 = ( case WHEN sm.Month_Num = 4 THEN cu.EL_Usage
                                   END )
                       ,Month5 = ( case WHEN sm.Month_Num = 5 THEN cu.EL_Usage
                                   END )
                       ,Month6 = ( case WHEN sm.Month_Num = 6 THEN cu.EL_Usage
                                   END )
                       ,Month7 = ( case WHEN sm.Month_Num = 7 THEN cu.EL_Usage
                                   END )
                       ,Month8 = ( case WHEN sm.Month_Num = 8 THEN cu.EL_Usage
                                   END )
                       ,Month9 = ( case WHEN sm.Month_Num = 9 THEN cu.EL_Usage
                                   END )
                       ,Month10 = ( case WHEN sm.Month_Num = 10 THEN cu.EL_Usage
                                    END )
                       ,Month11 = ( case WHEN sm.Month_Num = 11 THEN cu.EL_Usage
                                    END )
                       ,Month12 = ( case WHEN sm.Month_Num = 12 THEN cu.EL_Usage
                                    END )
                       ,Total = EL_Usage
                       ,field_type = 'Usage'
                       ,field = 'Usage'
                   FROM
                        #Sites_Temp sg
                        CROSS APPLY #Service_Month sm
                        LEFT OUTER JOIN #CUSummary_Temp cu
                              ON cu.Client_Hier_Id = sg.Client_Hier_Id
                                 AND cu.SERVICE_MONTH = sm.Service_Month )
            SELECT
                  res.Client_Hier_Id Site_Client_Hier_Id
                 ,res.Site_Id
                 ,res.Site_Name
                 ,res.Field_Type
                 ,res.Field
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month1, 0))) Month1
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month2, 0))) Month2
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month3, 0))) Month3
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month4, 0))) Month4
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month5, 0))) Month5
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month6, 0))) Month6
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month7, 0))) Month7
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month8, 0))) Month8
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month9, 0))) Month9
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month10, 0))) Month10
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month11, 0))) Month11
                 ,convert(DECIMAL(22, 6), sum(isnull(res.month12, 0))) Month12
                 ,convert(DECIMAL(22, 6), sum(isnull(res.Total, 0))) Total
            FROM
                  cte_results res
            GROUP BY
                  res.Client_Hier_Id
                 ,res.Site_Name
                 ,res.Site_Id
                 ,res.Field_Type
                 ,res.Field
            ORDER BY
                  res.Site_Name
                 ,res.Field_Type DESC
                       
      DROP TABLE #CUSummary_Temp
      DROP TABLE #Sites_Temp
      DROP TABLE #Service_Month

END
;
GO


GRANT EXECUTE ON  [dbo].[Get_EL_Cost_Usage_Site_Summary] TO [CBMSApplication]
GO
