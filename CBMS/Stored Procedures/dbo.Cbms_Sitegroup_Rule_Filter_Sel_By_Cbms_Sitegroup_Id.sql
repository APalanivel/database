SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.Cbms_Sitegroup_Participant_Ins
 
 DESCRIPTION:   
 
	This procedure used to insert the Non metric(Division/State/Country/Supplier/Vendor..) rules of a smart group
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@SiteGroup_Id INT
	, @Rule_Filter_Id INT	
	, @Filter_Condition_Id INT
	, @Filter_Value VARCHAR(50)
	, @Is_Inclusive BIT
	
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

Exec dbo.Cbms_Sitegroup_Rule_Filter_Sel_By_Cbms_Sitegroup_Id
    @Cbms_Sitegroup_Id = 203
Exec dbo.Cbms_Sitegroup_Rule_Filter_Sel_By_Cbms_Sitegroup_Id
    @Cbms_Sitegroup_Id = 204




AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 NR			Narayana Reddy    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 NR			2018-09-27	Created for GRM.
 ******/

CREATE PROC [dbo].[Cbms_Sitegroup_Rule_Filter_Sel_By_Cbms_Sitegroup_Id]
      ( 
       @Cbms_Sitegroup_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      SELECT
            *
      FROM
            ( SELECT
                  csp.Cbms_Sitegroup_Id
                 ,csp.Group_Name
                 ,sgr.CBMS_Sitegroup_Rule_Id
                 ,rf.Cbms_Rule_Filter_Id
                 ,rf.Filter_Name
                 ,fp.Param_Name
                 ,rpv.Param_Value
                 ,MAX(CAST	(sgr.Is_Inclusive AS INT)) AS Is_Inclusive
              FROM
                  dbo.Cbms_Sitegroup csp
                  INNER JOIN dbo.CBMS_Sitegroup_Rule sgr
                        ON csp.Cbms_Sitegroup_Id = sgr.Cbms_Sitegroup_Id
                  INNER JOIN dbo.Cbms_Rule_Filter rf
                        ON sgr.Cbms_Rule_Filter_Id = rf.Cbms_Rule_Filter_Id
                  INNER JOIN dbo.Cbms_Filter_Param fp
                        ON rf.Cbms_Rule_Filter_Id = fp.Cbms_Rule_Filter_Id
                  LEFT JOIN dbo.Cbms_Sitegroup_Rule_Param_Value rpv
                        ON sgr.CBMS_Sitegroup_Rule_Id = rpv.CBMS_Sitegroup_Rule_Id
                           AND fp.Cbms_Filter_Param_Id = rpv.Cbms_Filter_Param_Id
              WHERE
                  csp.Cbms_Sitegroup_Id = @Cbms_Sitegroup_Id
                  AND fp.Param_Name <> 'SourceValue'
              GROUP BY
                  csp.Cbms_Sitegroup_Id
                 ,csp.Group_Name
                 ,sgr.CBMS_Sitegroup_Rule_Id
                 ,rf.Cbms_Rule_Filter_Id
                 ,rf.Filter_Name
                 ,fp.Param_Name
                 ,rpv.Param_Value ) t PIVOT ( MIN(Param_Value) FOR Param_Name IN ( Condition, TargetTest ) ) pvt;


END;


GO
GRANT EXECUTE ON  [dbo].[Cbms_Sitegroup_Rule_Filter_Sel_By_Cbms_Sitegroup_Id] TO [CBMSApplication]
GO
