SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   [Invoice_Collection_Queue_Insert_Batch_Tool]    
                  
Description:                  
   This sproc is used in batch process to fill Invoice Collection Queue    
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                      
                        
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec [Invoice_Collection_Queue_Insert_Batch_Tool]    
       
       
       
Author Initials:                  
    Initials	Name                  
----------------------------------------------------------------------------------------                    
 RKV             Ravi Kumar Vegesna    

               
 Modifications:                  
 Initials        Date            Modification                  
----------------------------------------------------------------------------------------                    
 RKV             2019-12-06      Created For Invoice_Collection.    
 
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Insert_Batch_Tool]
     (
         @Invoice_Collection_Account_Config_Id INT
         , @Commodity_Id INT = -1
         , @Invoice_Collection_Queue_Type_Cd INT
         , @Invoice_Collection_Exception_Type_Cd INT = NULL
         , @Collection_Start_Dt DATE
         , @Collection_End_Dt DATE
         , @Status_Cd INT
         , @Account_Invoice_Collection_Month_Id NVARCHAR(MAX)
         , @Event_Desc NVARCHAR(MAX)
         , @User_Info_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @tvp_Invoice_Collection_Queue_Month AS tvp_Invoice_Collection_Queue_Month;
        DECLARE @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel AS tvp_Invoice_Collection_Queue_Account_Config_Id_Sel;

        DECLARE
            @Close_Code_Id INT
            , @Invoice_Collection_Queue_Id INT;


        SELECT
            @Close_Code_Id = Code_Id
        FROM
            Code C1
            INNER JOIN Codeset cs
                ON cs.Codeset_Id = C1.Codeset_Id
        WHERE
            C1.Code_Value = 'Close'
            AND cs.Codeset_Name = 'IC Chase Status';






        BEGIN TRY
            BEGIN TRAN;


            INSERT INTO dbo.Invoice_Collection_Queue
                 (
                     Invoice_Collection_Account_Config_Id
                     , Commodity_Id
                     , Invoice_Collection_Queue_Type_Cd
                     , Invoice_Collection_Exception_Type_Cd
                     , Collection_Start_Dt
                     , Collection_End_Dt
                     , Status_Cd
                     , Created_User_Id
                     , Updated_User_Id
                 )
            SELECT
                @Invoice_Collection_Account_Config_Id Invoice_Collection_Account_Config_Id
                , ISNULL(@Commodity_Id, -1) AS Commodity_Id
                , @Invoice_Collection_Queue_Type_Cd AS Invoice_Collection_Queue_Type_Cd
                , @Invoice_Collection_Exception_Type_Cd AS Invoice_Collection_Exception_Type_Cd
                , @Collection_Start_Dt AS Collection_Start_Dt
                , @Collection_End_Dt AS Collection_End_Dt
                , @Status_Cd AS Status_Cd
                , @User_Info_Id AS Created_User_Id
                , @User_Info_Id AS Updated_User_Id;


            SELECT  @Invoice_Collection_Queue_Id = SCOPE_IDENTITY();

            INSERT INTO @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel
                 (
                     Invoice_Collection_Queue_Id
                     , Invoice_Collection_Account_Config_Id
                 )
            SELECT  @Invoice_Collection_Queue_Id, @Invoice_Collection_Account_Config_Id;


            EXEC dbo.Invoice_Collection_Issue_Link_Auto
                @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel
                , @User_Info_Id;

            INSERT INTO @tvp_Invoice_Collection_Queue_Month
                 (
                     Invoice_Collection_Queue_Id
                     , Account_Invoice_Collection_Month_Id
                 )
            SELECT
                @Invoice_Collection_Queue_Id
                , aicm.Segments
            FROM
                dbo.ufn_split(@Account_Invoice_Collection_Month_Id, ',') aicm;


            EXEC dbo.Invoice_Collection_Queue_Month_Map_Merge
                @tvp_Invoice_Collection_Queue_Month;


            INSERT INTO dbo.Invoice_Collection_Queue_Event
                 (
                     Invoice_Collection_Queue_Id
                     , Event_Desc
                     , Event_By_User_Id
                 )
            SELECT  @Invoice_Collection_Queue_Id, @Event_Desc, @User_Info_Id;

            ------------------------------------------------------------------    





            UPDATE
                icil
            SET
                icil.Issue_Status_Cd = @Close_Code_Id
                , icil.Updated_User_Id = @User_Info_Id
                , icil.Last_Change_Ts = GETDATE()
            FROM
                dbo.Invoice_Collection_Issue_Log icil
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
            WHERE
                EXISTS (   SELECT
                                1
                           FROM
                                Code c
                           WHERE
                                Code_Id = @Status_Cd
                                AND icq.Status_Cd = c.Code_Id
                                AND c.Code_Value IN ( 'Resolved', 'Received', 'Excluded' ))
                AND icq.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id;





            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK;
                END;
            EXEC dbo.usp_RethrowError;

        END CATCH;



    END;





    ;

    ;


    ;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Insert_Batch_Tool] TO [CBMSApplication]
GO
