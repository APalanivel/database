SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Bucket_Invoice_Sub_Bucket_Sel_by_Ec_Calc_Val_Id         
              
Description:              
                     
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
  @Ec_Calc_Val_Id						INT	
 
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
   
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	
	BEGIN TRAN  
	
	EXEC Bucket_Invoice_Sub_Bucket_Sel_by_Ec_Calc_Val_Id 13
	
	ROLLBACK TRAN             
             
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2015-10-19		Created For AS400-II.         
             
******/ 
CREATE  PROCEDURE [dbo].[Bucket_Invoice_Sub_Bucket_Sel_by_Ec_Calc_Val_Id] ( @Ec_Calc_Val_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
	
     
      
      SELECT
            ecvbm.Bucket_Master_Id
           ,BM.Bucket_Name
           ,ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id
           ,eisbm.Sub_Bucket_Name
      FROM
            dbo.Ec_Calc_Val_Bucket_Map ecvbm
            INNER JOIN dbo.Bucket_Master bm
                  ON ecvbm.Bucket_Master_Id = bm.Bucket_Master_Id
            LEFT OUTER JOIN ( dbo.Ec_Calc_Val_Bucket_Sub_Bucket_Map ecvbsbm
                              INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                                    ON ecvbsbm.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id )
                              ON ecvbm.Ec_Calc_Val_Bucket_Map_Id = ecvbsbm.Ec_Calc_Val_Bucket_Map_Id
      WHERE
            Ec_Calc_Val_Id = @Ec_Calc_Val_Id
    
            
      
            
END;

;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Invoice_Sub_Bucket_Sel_by_Ec_Calc_Val_Id] TO [CBMSApplication]
GO
