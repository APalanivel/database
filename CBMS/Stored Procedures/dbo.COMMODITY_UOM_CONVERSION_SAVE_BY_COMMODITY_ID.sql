SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********

--------------------------------------------------------------    
NAME:    
 dbo.COMMODITY_UOM_CONVERSION_SAVE_BY_COMMODITY_ID  
   
DESCRIPTION:    
 Used to insert/update into Enity table and Commodity_Uom_conversion table to associate UOM with Commodity  
    
INPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
@Commodity_Id			INT,  
@Commodity_Name			VARCHAR(50),  
@New_Uom_Cd_List		VARCHAR(1024)  = NULL,  
@Deleted_Uom_Cd_List	VARCHAR(1024) = NULL   
  
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  
	SELECT * FROM Commodity_Uom_Conversion where base_commodity_id = 65 and converted_commodity_id = 65 and base_uom_cd = converted_uom_cd

	SELECT 
		Code_id, Code_Value
	FROM 
		code  cd
		JOIN Codeset cs
			ON cs.Codeset_id = cd.Codeset_Id
	WHERE
		cs.Std_Column_Name = 'Uom_Cd'

BEGIN TRAN
 
	EXEC dbo.COMMODITY_UOM_CONVERSION_SAVE_BY_COMMODITY_ID 65, 'Number 6 Fuel Oil', '100068, 100074'
	SELECT * FROM Commodity_Uom_Conversion where base_commodity_id = 65 and converted_commodity_id = 65 and base_uom_cd = converted_uom_cd

ROLLBACK TRAN

AUTHOR INITIALS:
Initials Name 
------------------------------------------------------------    
GB		Geetansu Behera    
CMH		Chad Hattabaugh     
HG		Hari  
SKA		Shobhit Kr Agrawal  
SS		Subhash Subramanyam
   
MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
GB					Created  
SKA		07/08/09	Modified as per coding standard  
HG		07/13/2009	Entity Exists check removed by adding the NOT EXISTS clause in the INSERT statement
					Variable names modified as per the standards.
					Error statement added as per SQL 2000 format
SS		09/17/2009	All DMLs are converted into procedures, Block of code for @Deleted_Uom_Cd_List removed
HG		07/02/2010	IF/THEN ELSE loop used to insert the comma seperated UOM codes removed by saving the value in table variable and looping through the values.
					Added code to insert default conversion factor in to Consumption_Unit_Conversion table(Consumption_Unit_Conversion_Ins) for new uom inserted in to Entity.
					Out put param added in Entity_Ins procdure to get the inserted uom for
******/

CREATE PROCEDURE dbo.COMMODITY_UOM_CONVERSION_SAVE_BY_COMMODITY_ID
(
	@Commodity_Id		AS INT
	, @Commodity_Name	AS VARCHAR( 50 )
	, @New_Uom_Cd_List	AS VARCHAR( 1024 ) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    DECLARE @Uom_Cd						AS INT
		,@Uom_Name					AS VARCHAR(25)
		,@unitFor					AS VARCHAR( 256 )
		,@Entity_Type				AS INT
		,@Is_Updated				AS BIT = 0
		,@Entity_Uom_Id				AS INT

    DECLARE @Uom_Cd_List TABLE(Uom_Cd INT, Uom_Name VARCHAR(25))
	
    SELECT      @unitFor = CASE @Commodity_Name
                               WHEN 'Electric Power' THEN 'Unit for electricity'
                               WHEN 'Natural Gas' THEN 'Unit for Gas'
                               WHEN 'Alternative Natural Gas' THEN 'Unit for alternate fuel'
                               ELSE 'Units for ' + @Commodity_Name
                           END

	INSERT INTO @Uom_Cd_List
	(
		Uom_Cd
		,Uom_Name
	)
	SELECT
		cd.Code_Id
		,cd.Code_Value
	FROM
		dbo.ufn_split(@New_Uom_Cd_List,',') uom
		JOIN dbo.Code cd
			ON cd.Code_Id = CONVERT(INT, uom.segments)

    BEGIN TRY
        BEGIN TRANSACTION

			WHILE EXISTS(SELECT 1 FROM @Uom_Cd_List)
			BEGIN

				SELECT TOP 1
					@Uom_Cd = Uom_Cd
					,@Uom_Name = Uom_Name
				FROM
					@Uom_Cd_List


				EXEC dbo.COMMODITY_UOM_CONVERSION_INS  @Commodity_Id
													 , @UOM_Cd
													 , @Commodity_Id
													 , @UOM_Cd
													 , 1


				SELECT      @Entity_Type = ISNULL( ( SELECT      MAX( ENTITY_TYPE )
													 FROM        dbo.ENTITY AS E
													 WHERE       ENTITY_DESCRIPTION = @unitFor ), ( SELECT      MAX( ENTITY_TYPE ) + 1
																									FROM        dbo.ENTITY ) )

				EXEC dbo.ENTITY_INS  @Uom_Name
								   , @Entity_Type
								   , @unitFor
								   , NULL
								   , @Entity_Uom_Id OUT

				EXEC dbo.Consumption_Unit_Conversion_Ins @Entity_Uom_Id, @Entity_Uom_Id, 1
			
				DELETE FROM @Uom_Cd_List WHERE Uom_Cd = @Uom_Cd

			END

        
        COMMIT TRANSACTION
    END TRY
        BEGIN CATCH

 			IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRAN
				END
        
        EXEC dbo.usp_RethrowError
    END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[COMMODITY_UOM_CONVERSION_SAVE_BY_COMMODITY_ID] TO [CBMSApplication]
GO
