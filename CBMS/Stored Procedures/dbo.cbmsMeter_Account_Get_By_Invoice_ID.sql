SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

	dbo.cbmsMeter_Account_Get_By_Invoice_ID

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
	@Cu_invoice_ID	INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
	EXEC cbmsMeter_Account_Get_By_Invoice_ID	68510454 ,26270 ,290
	EXEC cbmsMeter_Account_Get_By_Invoice_ID    798
	EXEC cbmsMeter_Account_Get_By_Invoice_ID    30562408,107997 
    EXEC cbmsMeter_Account_Get_By_Invoice_ID    30562408
    EXEC dbo.cbmsMeter_Account_Get_By_Invoice_ID @Cu_invoice_ID = 35238218 

	EXEC cbmsMeter_Account_Get_By_Invoice_ID 81287315 ,null ,290  


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava
	RR			Raghu Reddy
	SP          Srinivas Patchava

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SSR			03/25/2010	Created   
	SSR			07/09/2010  Removed the logic of fetching data from SAMM table
							(( SAMM.meter_disassociation_date > Account.Supplier_Account_Begin_Dt  
								OR SAMM.meter_disassociation_date IS NULL ))
							(After contract enhancement application is populating both Meter_association_date and meter_disassociation_Date column supplier_Account_meter_map and removing the entries when ever is_history = 1)
	RKV			2015-10-28	Added optional parameter Account_Id,Commodity_Id as part of AS400-PII
	RR			2017-03-22	Contract Placeholder - Modified join on dbo.contract inner to left, DMO supplier accounts will be created without contracts
	RKV         2019-06-11  MAINT-8681, Added Account_Id Filter to the on clause of the left join account table 
	SP          2019-07-16  SE-734 Modified the code Added Is_Map_Act_All_Contract Column 
	NR			2019-10-01	Add Contract - Added Supplier dates filter for supplier account invoice.

*/
CREATE PROCEDURE [dbo].[cbmsMeter_Account_Get_By_Invoice_ID]
    (
        @Cu_invoice_ID INT
        , @Account_Id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            m.METER_ID
            , m.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name commodity_type
            , u.VENDOR_ID utility_id
            , u.VENDOR_NAME utility_name
            , m.PURCHASE_METHOD_TYPE_ID
            , pm.ENTITY_NAME purchase_method_type
            , acc.ACCOUNT_ID utility_account_id
            , (CASE WHEN acc_type.ENTITY_NAME = 'Supplier' THEN
                        acc.ACCOUNT_NUMBER + SPACE(0) + '('
                        + CONVERT(VARCHAR(10), cha_acc.Supplier_Account_Begin_Dt, 101) + '-'
                        + CONVERT(VARCHAR(10), cha_acc.Supplier_Account_End_Dt, 101) + ')'
                   ELSE acc.ACCOUNT_NUMBER
               END) utility_account_number
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , s.SITE_ID
            , s.SITE_NAME
            , s.NOT_MANAGED
            , acc_type.ENTITY_NAME
            , cha_acc.Supplier_Account_Begin_Dt
            , cha_acc.Supplier_Account_End_Dt
            , acc.ACCOUNT_TYPE_ID
            , x.ED_CONTRACT_NUMBER
            , ag.ACCOUNT_GROUP_ID
            , MAX(CASE WHEN acirt.Account_Commodity_Invoice_Recalc_Type_Id IS NOT NULL
                            AND sup_acc.Supplier_Contract_ID IS NOT NULL THEN 1
                      ELSE 0
                  END) Is_Map_Act_All_Contract
        FROM
            dbo.CU_INVOICE cui
            JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = cui.CU_INVOICE_ID
            LEFT JOIN dbo.ACCOUNT_GROUP ag
                ON ag.ACCOUNT_GROUP_ID = cui.ACCOUNT_GROUP_ID
            LEFT JOIN dbo.ACCOUNT aagr
                ON ag.ACCOUNT_GROUP_ID = aagr.ACCOUNT_GROUP_ID
                   AND  aagr.ACCOUNT_ID = cism.Account_ID
            JOIN
            (   SELECT
                    ACCOUNT_ID
                    , METER_ID
                    , NULL ED_CONTRACT_NUMBER
                    , NULL Contract_Id
                FROM
                    dbo.METER
                UNION
                SELECT
                    map.ACCOUNT_ID
                    , map.METER_ID
                    , con.ED_CONTRACT_NUMBER
                    , map.Contract_ID
                FROM
                    dbo.SUPPLIER_ACCOUNT_METER_MAP AS map
                    LEFT JOIN dbo.CONTRACT AS con
                        ON con.CONTRACT_ID = map.Contract_ID
                    JOIN dbo.ACCOUNT a
                        ON a.ACCOUNT_ID = map.ACCOUNT_ID) AS x
                ON x.ACCOUNT_ID = ISNULL(aagr.ACCOUNT_ID, cism.Account_ID)
            JOIN dbo.METER m
                ON m.METER_ID = x.METER_ID
            JOIN dbo.RATE r
                ON r.RATE_ID = m.RATE_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = r.COMMODITY_TYPE_ID
            JOIN dbo.VENDOR u
                ON u.VENDOR_ID = r.VENDOR_ID
            JOIN dbo.ENTITY pm
                ON pm.ENTITY_ID = m.PURCHASE_METHOD_TYPE_ID
            JOIN dbo.ADDRESS ad
                ON ad.ADDRESS_ID = m.ADDRESS_ID
            JOIN dbo.STATE st
                ON st.STATE_ID = ad.STATE_ID
            JOIN dbo.SITE s
                ON s.SITE_ID = ad.ADDRESS_PARENT_ID
            JOIN dbo.ACCOUNT acc
                ON acc.ACCOUNT_ID = x.ACCOUNT_ID
            JOIN dbo.ENTITY acc_type
                ON acc_type.ENTITY_ID = acc.ACCOUNT_TYPE_ID
            LEFT JOIN(dbo.Account_Commodity_Invoice_Recalc_Type acirt
                      INNER JOIN dbo.Code c
                          ON c.Code_Id = acirt.Invoice_Recalc_Type_Cd
                             AND c.Code_Value LIKE '%ACT%'
                             AND c.Code_Value LIKE '%ALL')
                ON acirt.Account_Id = acc.ACCOUNT_ID
                   AND  acirt.Commodity_ID = com.Commodity_Id
                   AND  cism.SERVICE_MONTH BETWEEN acirt.Start_Dt
                                           AND     ISNULL(acirt.End_Dt, '2099-12-01')
            LEFT JOIN(Core.Client_Hier_Account u_cha
                      INNER JOIN Core.Client_Hier_Account sup_acc
                          ON u_cha.Meter_Id = sup_acc.Meter_Id)
                ON u_cha.Account_Id = acc.ACCOUNT_ID
                   AND  u_cha.Meter_Id = m.METER_ID
                   AND  cism.SERVICE_MONTH BETWEEN sup_acc.Supplier_Account_begin_Dt
                                           AND     sup_acc.Supplier_Account_End_Dt
                   AND  u_cha.Account_Type = 'Utility'
                   AND  sup_acc.Account_Type = 'Supplier'
            LEFT JOIN dbo.Supplier_Account_Config cha_acc
                ON cha_acc.Account_Id = acc.ACCOUNT_ID
                   AND  cha_acc.Contract_Id = x.Contract_Id
        WHERE
            cui.CU_INVOICE_ID = @Cu_invoice_ID
            AND (   @Account_Id IS NULL
                    OR  acc.ACCOUNT_ID = @Account_Id)
            AND (   @Commodity_Id IS NULL
                    OR  com.Commodity_Id = @Commodity_Id)
            AND (   acc_type.ENTITY_NAME = 'Utility'
                    OR  (   acc_type.ENTITY_NAME = 'Supplier'
                            AND (   cism.Begin_Dt BETWEEN cha_acc.Supplier_Account_Begin_Dt
                                                  AND     ISNULL(cha_acc.Supplier_Account_End_Dt, '9999-12-31')
                                    OR  cism.End_Dt BETWEEN cha_acc.Supplier_Account_Begin_Dt
                                                    AND     ISNULL(cha_acc.Supplier_Account_End_Dt, '9999-12-31')
                                    OR  cha_acc.Supplier_Account_Begin_Dt BETWEEN cism.Begin_Dt
                                                                          AND     cism.End_Dt
                                    OR  ISNULL(cha_acc.Supplier_Account_End_Dt, '9999-12-31') BETWEEN cism.Begin_Dt
                                                                                              AND     cism.End_Dt)))
        GROUP BY
            m.METER_ID
            , m.RATE_ID
            , r.RATE_NAME
            , r.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , u.VENDOR_ID
            , u.VENDOR_NAME
            , m.PURCHASE_METHOD_TYPE_ID
            , pm.ENTITY_NAME
            , acc.ACCOUNT_ID
            , acc_type.ENTITY_NAME
            , acc.ACCOUNT_NUMBER
            , m.ADDRESS_ID
            , ad.ADDRESS_LINE1
            , ad.ADDRESS_LINE2
            , ad.CITY
            , st.STATE_ID
            , st.STATE_NAME
            , m.METER_NUMBER
            , m.TAX_EXEMPT_STATUS
            , m.IS_HISTORY
            , s.SITE_ID
            , s.SITE_NAME
            , s.NOT_MANAGED
            , acc_type.ENTITY_NAME
            , cha_acc.Supplier_Account_Begin_Dt
            , cha_acc.Supplier_Account_End_Dt
            , acc.ACCOUNT_TYPE_ID
            , x.ED_CONTRACT_NUMBER
            , ag.ACCOUNT_GROUP_ID;

    END;

    ;



GO


GRANT EXECUTE ON  [dbo].[cbmsMeter_Account_Get_By_Invoice_ID] TO [CBMSApplication]
GO
