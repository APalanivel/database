SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Exception_Queue_Merge  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Cu_Invoice_Id						INT
 @Account_Id						INT
 @Commodity_Id						INT
 @Status_Cd							INT
 @Date_In_Queue						DATETIME
 @User_Info_Id						INT
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 select * from Cu_Invoice_Recalc_Exception_Queue
 EXEC Cu_Invoice_Recalc_Exception_Queue_Ins 35238713,55236,290,100981,'2015-11-06',49
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 EXEC Cu_Invoice_Recalc_Exception_Queue_Ins 35238713,55236,290,100982,'2015-11-06',49
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2015-11-06		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Exception_Queue_Ins]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Status_Cd INT
      ,@Date_In_Queue DATETIME
      ,@User_Info_Id INT = NULL )
AS 
BEGIN  
      
      BEGIN TRY
            BEGIN TRANSACTION     
            UPDATE
                  Cu_Invoice_Recalc_Exception_Queue
            SET   
                  Status_Cd = 100982
            WHERE
                  Cu_Invoice_Id = @Cu_Invoice_Id
                  AND Commodity_Id = @Commodity_Id
                  AND Account_Id = @Account_Id
                  AND Status_Cd = 100981
     
          
            INSERT      INTO Cu_Invoice_Recalc_Exception_Queue
                        ( 
                         Cu_Invoice_Id
                        ,Account_Id
                        ,Commodity_Id
                        ,Status_Cd
                        ,Queue_Date
                        ,User_Info_Id )
            VALUES
                        ( 
                         @Cu_Invoice_Id
                        ,@Account_Id
                        ,@Commodity_Id
                        ,@Status_Cd
                        ,@Date_In_Queue
                        ,@User_Info_Id );
                        
                  
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            ROLLBACK TRAN
            EXEC dbo.usp_RethrowError
      END CATCH
        
      
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Exception_Queue_Ins] TO [CBMSApplication]
GO
