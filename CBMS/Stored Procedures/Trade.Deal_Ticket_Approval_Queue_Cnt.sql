SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Approval_Queue_Cnt
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Approval_Queue_Cnt 112
	Exec Trade.Deal_Ticket_Approval_Queue_Cnt 49
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	NR          Narayana Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	NR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Approval_Queue_Cnt]
      ( 
       @User_Info_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            COUNT(DISTINCT dt.Deal_Ticket_Id) AS Deal_Ticket_Approval_Queue_Cnt
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Id = dt.Client_Id
                     AND ch.Client_Hier_Id = dtch.Client_Hier_Id
      WHERE
            chws.Is_Active = 1
            AND ws.Workflow_Status_Name = 'Pending Internal Approval'
            AND ( @User_Info_Id IS NULL
                  OR EXISTS ( SELECT
                                    1
                              FROM
                                    dbo.Geographic_Region_Permission_Info_Map grp
                                    INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                                          ON grp.Permission_Info_Id = gipi.PERMISSION_INFO_ID
                                    INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                                          ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
                                    INNER JOIN dbo.Geographic_Region_Country_Map grc
                                          ON grp.Geographic_Region_Id = grc.Geographic_Region_Id
                              WHERE
                                    uigi.USER_INFO_ID = @User_Info_Id
                                    AND ch.Country_Id = grc.COUNTRY_ID ) )
      
END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Approval_Queue_Cnt] TO [CBMSApplication]
GO
