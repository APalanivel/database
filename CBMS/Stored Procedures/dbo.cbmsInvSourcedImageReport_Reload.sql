
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.cbmsInvSourcedImageReport_Reload

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
  @MyAccountId int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.cbmsInvSourcedImageReport_Reload

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	KC			Kailash Choudhary
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	KC          26-Aug-2009		Implementing incremental loading of report table instead of truncate/rebuild. This is
								done by introducing two new columns INV_PROKARMA_NOT_REPORTED_BATCH_ID and
								INV_SOURCED_IMAGE_BATCH_ID in the report table, INV_SOURCED_IMAGE_REPORT			
	KC          27-Aug-2009		Removal of updates to replace special characters from INV_SOURCE_EMAIL_ATTACHMENT
								and INV_SOURCED_IMAGE tables. This is passed on to dbo.cbmsInvSourcedImage_Save
								and dbo.cbmsInvSourceEmailAttachment_Save with the usage of a function dbo.ufn_Remove_String						
																
	HG			08/09/2010		Update statement which update the Inv_Sourced_Image column in Cbms_image table looking for the max invoice batch id but it should look for max NOT RECEIVED batch id in inv_sourced_image_report , corrected it now.

	HG			08/11/2010		Removed the filter condtion of >@sImageMaxBatchId which used to update the Inv_Sourced_Image column in Cbms_image table as we may not receive all the images of a batch together.
								Modified this update process as follows
									insert the results into a table variable and loop through each row to update in Cbms_Image table to improve the performance.

								Removed inv_prokarma_not_reported table join in the insert statement which inserts the "Not received status" as the "not received" status determined by the Is_Received flag Inv_Sourced_Image_Track table.

								insert statement always inserts the recieved status for the ubm batches which are not in Inv_Sourced_Image_Report, 
								At the beginning of this procedure entries gets deleted from Inv_Sourced_Image_report table if the status in Inv_Sourced_Image_Track is not received but actually we have received that images.
								If all images of one batch got deleted in that way previous insert statement will insert as it is always trying to insert the batches which are not in Inv_Sourced_Image_Report table
								, but if we have half of the images received one batch and remaining half in the other batch then previous statement will fail to insert those deleted records , added a new insert statement which inserts such missed out records as received.							




	
								Saving all the records which we want to delete in a table variable and using the table variable for this delete and insert purpose.
								
								Moved the select statement which picks the max batch id after the delete statement so that it can pick the correct max batch_id.

	HG			05/10/2011		MAINT-636 fixed the code to update the keyword column in INV_SOURCED_IMAGE_REPORT
									- Update statement which updates the keyword column for invoice images received through mail is based on @sImageMaxBatchId (maximum Inv_Sourced_Image_Batch_Id found in INV_SOURCED_IMAGE_REPORT) as this variable populated after this update stateme
nt this column was never got updated.
										Changed the script to execute the Update keyworkd statement after populating variable @sImageMaxBatchId
									- DISTINCT clause use in the INSERT query replaced by GROUP BY
									- hard coded entity_id filter which used to filter the image type replaced by entity_name by joining the ENTITY table.
	HG			05/25/2011		Removed the unused parameter @MyAccountId
	HG			2014-07-07		MAINT-2360, modified the update on Inv_Image_Sourced.Keyword after removing the special characters in Original_File_Name in Inv_Source_Email_Attachment
	HG			2014-07-10		MAINT-2359, Added the following update statement on the Script which updates the Cu_Invoice_Id in Inv_Sourced_Image_Track table. If the image has multiple pages/bills then Prokarma copies the same image for each invoice by appending with


											number , since the update Inv_Sourced_Image_Track is happending based on Inv_Sourced_Image_Id it is copying the same Invoice_id to all the images. Thus using Cbms_Doc_Id also to find the correct invoice id
											t.PROKARMA_FILENAME = REPLACE(ci.CBMS_DOC_ID, REVERSE(LEFT(REVERSE(ci.CBMS_DOC_ID), CHARINDEX('.', REVERSE(ci.CBMS_DOC_ID)))), SPACE(0))
											-- Removes the file extension from Cbms_doc_Id and matches it with the prokarma file name.
	HG			2014-07-22		Reverted the changes made for MAINT-2359 as the Prokarma_Filename column in Inv_Sourced_Image_Track TABLE is never got updated FOR the invoice images which never had prokarma exceptions.
	HG			2014-09-25		Modified for UBM Rename Prokarma to Schneider Electric and addition of new UBM Prokarma
									-- Replaced the Ubm name in the statement which loads the data in to table variable to update the image id.
	HG			2015-01-09		modified the code to REMOVE the condition ON image TYPE as the update is based ON UBM Batch Id
******/

CREATE PROCEDURE [dbo].[cbmsInvSourcedImageReport_Reload]
      WITH EXECUTE AS SELF
AS 
BEGIN

      SET NOCOUNT ON;

      DECLARE @Received_Invoice_Track_Id_List TABLE
            ( 
             Inv_Sourced_Image_Track_Id INT )

      DECLARE @Received_Image TABLE
            ( 
             Cbms_Image_Id INT
            ,Cbms_Doc_Id VARCHAR(200)
            ,Inv_Sourced_Image_Id INT )

      DECLARE
            @ubmMaxBatchId INT
           ,@notReportedMaxBatchId INT
           ,@sImageMaxBatchId INT
           ,@received_type_id INT
           ,@not_received_type_id INT
           ,@exception_type_id INT
           ,@Cbms_Image_Id INT
           ,@Inv_Sourced_Image_Id INT
           ,@invalid_string VARCHAR(100)

      SELECT
            @invalid_string = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'Invalid_Characters'

      INSERT      INTO @Received_Image
                  ( 
                   Cbms_Image_Id
                  ,Cbms_Doc_Id
                  ,Inv_Sourced_Image_Id )
    -- CMH: REPLACED STATEMENT DUE TO PERFORMANCE ISSUE 
                  SELECT
                        i.CBMS_IMAGE_ID
                       ,i.CBMS_DOC_ID
                       ,s.INV_SOURCED_IMAGE_ID
                  FROM
                        dbo.cbms_image i
                        JOIN dbo.inv_sourced_image s
                              ON i.cbms_doc_id LIKE LEFT(s.cbms_filename, CASE WHEN ( CHARINDEX('.', s.cbms_filename) - 1 ) < 0 THEN LEN(s.cbms_filename)
                                                                               ELSE CHARINDEX('.', s.cbms_filename) - 1
                                                                          END) + '%'
                        JOIN dbo.ubm_invoice ui
                              ON ui.cbms_image_id = i.cbms_image_id
                        JOIN dbo.ubm_batch_master_log l
                              ON l.ubm_batch_master_log_id = ui.ubm_batch_master_log_id
                        JOIN dbo.Ubm
                              ON ubm.UBM_ID = l.UBM_ID
                  WHERE
                        ubm.UBM_NAME = 'Schneider Electric'
                        AND i.inv_sourced_image_id IS NULL
	
      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Received_Image ) 
            BEGIN
	
                  SELECT TOP 1
                        @Cbms_Image_Id = Cbms_Image_Id
                       ,@Inv_Sourced_Image_Id = Inv_Sourced_Image_id
                  FROM
                        @Received_Image

                  UPDATE
                        dbo.cbms_image
                  SET   
                        INV_SOURCED_IMAGE_ID = @Inv_Sourced_Image_Id
                  WHERE
                        CBMS_IMAGE_ID = @Cbms_Image_Id

                  DELETE
                        @Received_Image
                  WHERE
                        Cbms_Image_Id = @Cbms_Image_Id

            END
	
      SELECT
            @received_type_id = entity_id
      FROM
            dbo.entity
      WHERE
            entity_type = 553
            AND entity_name = 'Received'

      SELECT
            @not_received_type_id = entity_id
      FROM
            dbo.entity
      WHERE
            entity_type = 553
            AND entity_name = 'Not Received'

      SELECT
            @exception_type_id = entity_id
      FROM
            dbo.entity
      WHERE
            entity_type = 553
            AND entity_name = 'Exception'
	
      INSERT      INTO @Received_Invoice_Track_Id_List
                  ( 
                   Inv_Sourced_Image_Track_Id )
                  SELECT
                        track.INV_SOURCED_IMAGE_TRACK_ID
                  FROM
                        dbo.inv_sourced_image_report rpt
                        JOIN dbo.inv_sourced_image_track track
                              ON track.Inv_Sourced_Image_track_id = rpt.Inv_Sourced_Image_track_id
                        JOIN dbo.cbms_image ci
                              ON ci.INV_SOURCED_IMAGE_ID = track.inv_sourced_image_id
                        JOIN dbo.CU_Invoice cu
                              ON cu.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
                  WHERE
                        track.Is_Received = 0
                  GROUP BY
                        track.INV_SOURCED_IMAGE_TRACK_ID

	  /* Deleting the records from report table for which is_received = 1 before updating inv_sourced_image_track */
      DELETE
            rpt
      FROM
            dbo.inv_sourced_image_report rpt
            JOIN @Received_Invoice_Track_Id_List track
                  ON track.Inv_Sourced_Image_track_id = rpt.Inv_Sourced_Image_track_id

      UPDATE
            t
      SET   
            is_received = 1
           ,cu_invoice_id = cu.cu_invoice_id
      FROM
            dbo.inv_sourced_image_track t
            JOIN dbo.cbms_image ci
                  ON ci.inv_sourced_image_id = t.inv_sourced_image_id
            JOIN dbo.CU_INVOICE cu
                  ON cu.cbms_image_id = ci.cbms_image_id
      WHERE
            t.is_received = 0

	  -- Saving the new batch id in to variabl to build the reporting table
      SELECT
            @ubmMaxBatchId = ISNULL(MAX(ubm_batch_master_log_id), 0)
           ,@notReportedMaxBatchId = ISNULL(MAX(INV_PROKARMA_NOT_REPORTED_BATCH_ID), 0)
           ,@sImageMaxBatchId = ISNULL(MAX(INV_SOURCED_IMAGE_BATCH_ID), 0)
      FROM
            dbo.inv_sourced_image_report


	  -- Updating the keyword as folder name(archive_path) for the images received manually
      UPDATE
            dbo.inv_sourced_image
      SET   
            keyword = archive_path
      WHERE
            inv_source_id = 6	-- Images received manual
            AND keyword IS NULL

	-- dbo.ufn_Remove_Special_Characters(@original_filename, @invalid_string)
	  -- Updating the keyword as email from address for the images received from emails
      UPDATE
            s
      SET   
            s.keyword = l.from_address
      FROM
            dbo.inv_sourced_image s
            JOIN dbo.inv_source_email_attachment ea
                  ON REPLACE(dbo.ufn_Remove_Special_Characters(ea.final_filename, @invalid_string), SPACE(1), SPACE(0)) = s.original_filename
            JOIN dbo.inv_source_email_log l
                  ON l.inv_source_email_log_id = ea.inv_source_email_log_id
                     AND l.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
      WHERE
            s.inv_source_id = 8		-- Images received through email
            AND s.keyword IS NULL
            AND s.inv_sourced_image_batch_id > @sImageMaxBatchId


	/* Build report table for 'Received' status. These are invoices got back from Prokarma and processed by the Upstream UBM Batch Process	*/

      INSERT      INTO dbo.inv_sourced_image_report
                  ( 
                   inv_sourced_image_track_id
                  ,inv_sourced_image_id
                  ,inv_source_id
                  ,inv_source_label
                  ,inv_source_keyword
                  ,original_filename
                  ,prokarma_filename
                  ,archive_path
                  ,sent_date
                  ,received_date
                  ,status_type_id
                  ,status_type
                  ,account_number
                  ,cbms_image_id
                  ,ubm_batch_master_log_id
                  ,inv_prokarma_not_reported_batch_id
                  ,inv_sourced_image_batch_id
                  ,exception_comments )
                  SELECT
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword inv_source_keyword
                       ,s.original_filename
                       ,ci.cbms_doc_id prokarma_filename
                       ,s.archive_path
                       ,bs.batch_date sent_date
                       ,ci.date_imaged received_date
                       ,@received_type_id status_type_id
                       ,'Received' status_type
                       ,ui.ubm_account_number account_number
                       ,ci.cbms_image_id
                       ,ui.ubm_batch_master_log_id
                       ,NULL inv_prokarma_not_reported_batch_id
                       ,NULL inv_sourced_image_batch_id
                       ,NULL exception_comments
                  FROM
                        dbo.inv_sourced_image_track t
                        JOIN dbo.inv_sourced_image s
                              ON s.inv_sourced_image_id = t.inv_sourced_image_id
                        JOIN dbo.inv_sourced_image_batch bs
                              ON bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
                        JOIN dbo.inv_source sl
                              ON sl.inv_source_id = s.inv_source_id
                        JOIN dbo.cu_invoice cu
                              ON cu.cu_invoice_id = t.cu_invoice_id
                        JOIN dbo.cbms_image ci
                              ON ci.cbms_image_id = cu.cbms_image_id
                        JOIN dbo.ubm_invoice ui
                              ON ui.ubm_invoice_id = cu.ubm_invoice_id
                        JOIN dbo.ubm_batch_master_log bl
                              ON bl.ubm_batch_master_log_id = ui.ubm_batch_master_log_id
                  WHERE
                        t.is_received = 1
                        AND bl.ubm_batch_master_log_id > @ubmMaxBatchId
                  GROUP BY
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword
                       ,s.original_filename
                       ,ci.cbms_doc_id
                       ,s.archive_path
                       ,bs.batch_date
                       ,ci.date_imaged
                       ,ui.ubm_account_number
                       ,ci.cbms_image_id
                       ,ui.ubm_batch_master_log_id


	/* Build report table for 'Received' status. These are invoices got back from Prokarma in previous batches and images received in today's batch	*/

      INSERT      INTO dbo.inv_sourced_image_report
                  ( 
                   inv_sourced_image_track_id
                  ,inv_sourced_image_id
                  ,inv_source_id
                  ,inv_source_label
                  ,inv_source_keyword
                  ,original_filename
                  ,prokarma_filename
                  ,archive_path
                  ,sent_date
                  ,received_date
                  ,status_type_id
                  ,status_type
                  ,account_number
                  ,cbms_image_id
                  ,ubm_batch_master_log_id
                  ,inv_prokarma_not_reported_batch_id
                  ,inv_sourced_image_batch_id
                  ,exception_comments )
                  SELECT
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword inv_source_keyword
                       ,s.original_filename
                       ,ci.cbms_doc_id prokarma_filename
                       ,s.archive_path
                       ,bs.batch_date sent_date
                       ,ci.date_imaged received_date
                       ,@received_type_id status_type_id
                       ,'Received' status_type
                       ,ui.ubm_account_number account_number
                       ,ci.cbms_image_id
                       ,ui.ubm_batch_master_log_id
                       ,NULL inv_prokarma_not_reported_batch_id
                       ,NULL inv_sourced_image_batch_id
                       ,NULL exception_comments
                  FROM
                        dbo.inv_sourced_image_track t
                        JOIN dbo.inv_sourced_image s
                              ON s.inv_sourced_image_id = t.inv_sourced_image_id
                        JOIN dbo.inv_sourced_image_batch bs
                              ON bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
                        JOIN dbo.inv_source sl
                              ON sl.inv_source_id = s.inv_source_id
                        JOIN dbo.cu_invoice cu
                              ON cu.cu_invoice_id = t.cu_invoice_id
                        JOIN dbo.cbms_image ci
                              ON ci.cbms_image_id = cu.cbms_image_id
                        JOIN dbo.ubm_invoice ui
                              ON ui.ubm_invoice_id = cu.ubm_invoice_id
                        JOIN @Received_Invoice_Track_Id_List tl
                              ON tl.Inv_Sourced_Image_Track_Id = t.INV_SOURCED_IMAGE_TRACK_ID
                        LEFT JOIN dbo.INV_SOURCED_IMAGE_REPORT isir
                              ON isir.INV_SOURCED_IMAGE_TRACK_ID = t.INV_SOURCED_IMAGE_TRACK_ID
                  WHERE
                        t.is_received = 1
                        AND isir.INV_SOURCED_IMAGE_TRACK_ID IS NULL
                  GROUP BY
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword
                       ,s.original_filename
                       ,ci.cbms_doc_id
                       ,s.archive_path
                       ,bs.batch_date
                       ,ci.date_imaged
                       ,ui.ubm_account_number
                       ,ci.cbms_image_id
                       ,ui.ubm_batch_master_log_id

	/* Build report table for 'Exception' status. These are the Not_Entered...txt files with Exceptions received from Prokarma	*/

      INSERT      INTO dbo.inv_sourced_image_report
                  ( 
                   inv_sourced_image_track_id
                  ,inv_sourced_image_id
                  ,inv_source_id
                  ,inv_source_label
                  ,inv_source_keyword
                  ,original_filename
                  ,prokarma_filename
                  ,archive_path
                  ,sent_date
                  ,received_date
                  ,status_type_id
                  ,status_type
                  ,account_number
                  ,cbms_image_id
                  ,ubm_batch_master_log_id
                  ,inv_prokarma_not_reported_batch_id
                  ,inv_sourced_image_batch_id
                  ,exception_comments )
                  SELECT
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword inv_source_keyword
                       ,s.original_filename
                       ,pr.barcode prokarma_filename
                       ,s.archive_path
                       ,bs.batch_date sent_date
                       ,pb.batch_date received_date
                       ,@exception_type_id status_type_id
                       ,'Exception' status_type
                       ,pr.account_number
                       ,NULL cbms_image_id
                       ,NULL ubm_batch_master_log_id
                       ,pb.inv_prokarma_not_reported_batch_id
                       ,NULL inv_sourced_image_batch_id
                       ,pr.remarks exception_comments
                  FROM
                        dbo.inv_sourced_image_track t
                        JOIN dbo.inv_prokarma_not_reported pr
                              ON pr.inv_sourced_image_id = t.inv_sourced_image_id
                                 AND pr.barcode = t.prokarma_filename
                        JOIN dbo.inv_sourced_image s
                              ON s.inv_sourced_image_id = t.inv_sourced_image_id
                        JOIN dbo.inv_sourced_image_batch bs
                              ON bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
                        JOIN dbo.inv_source sl
                              ON sl.inv_source_id = s.inv_source_id
                        JOIN dbo.inv_prokarma_not_reported_batch pb
                              ON pb.inv_prokarma_not_reported_batch_id = pr.inv_prokarma_not_reported_batch_id
                  WHERE
                        t.is_received = 0
                        AND pb.inv_prokarma_not_reported_batch_id > @notReportedMaxBatchId
                  GROUP BY
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword
                       ,s.original_filename
                       ,pr.barcode
                       ,s.archive_path
                       ,bs.batch_date
                       ,pb.batch_date
                       ,pr.account_number
                       ,pb.inv_prokarma_not_reported_batch_id
                       ,pr.remarks


	/* Build report table for 'Not Received' status. These are the invoices sent to Prokarma but not yet received back from them */

      INSERT      INTO dbo.inv_sourced_image_report
                  ( 
                   inv_sourced_image_track_id
                  ,inv_sourced_image_id
                  ,inv_source_id
                  ,inv_source_label
                  ,inv_source_keyword
                  ,original_filename
                  ,prokarma_filename
                  ,archive_path
                  ,sent_date
                  ,received_date
                  ,status_type_id
                  ,status_type
                  ,account_number
                  ,cbms_image_id
                  ,ubm_batch_master_log_id
                  ,inv_prokarma_not_reported_batch_id
                  ,inv_sourced_image_batch_id
                  ,exception_comments )
                  SELECT
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword inv_source_keyword
                       ,s.original_filename
                       ,NULL prokarma_filename
                       ,s.archive_path
                       ,bs.batch_date sent_date
                       ,NULL received_date
                       ,@not_received_type_id status_type_id
                       ,'Not Received' status_type
                       ,NULL account_number
                       ,NULL cbms_image_id
                       ,NULL ubm_batch_master_log_id
                       ,NULL inv_prokarma_not_reported_batch_id
                       ,bs.inv_sourced_image_batch_id
                       ,NULL exception_comments
                  FROM
                        dbo.inv_sourced_image_track t
                        JOIN dbo.inv_sourced_image s
                              ON s.inv_sourced_image_id = t.inv_sourced_image_id
                        JOIN dbo.inv_sourced_image_batch bs
                              ON bs.inv_sourced_image_batch_id = s.inv_sourced_image_batch_id
                        JOIN dbo.inv_source sl
                              ON sl.inv_source_id = s.inv_source_id
                  WHERE
                        t.is_received = 0
                        AND bs.inv_sourced_image_batch_id > @sImageMaxBatchId
                  GROUP BY
                        t.inv_sourced_image_track_id
                       ,s.inv_sourced_image_id
                       ,s.inv_source_id
                       ,sl.inv_source_label
                       ,s.keyword
                       ,s.original_filename
                       ,s.archive_path
                       ,bs.batch_date
                       ,bs.inv_sourced_image_batch_id

END;
;
GO





GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageReport_Reload] TO [CBMSApplication]
GO
