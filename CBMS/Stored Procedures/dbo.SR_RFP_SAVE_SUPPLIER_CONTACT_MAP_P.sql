SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SAVE_SUPPLIER_CONTACT_MAP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId int,
@contactId int,
@vendorId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test insert a new entry 
--exec dbo.SR_RFP_SAVE_SUPPLIER_CONTACT_MAP_P
--@rfpId = 5,
--@contactId =73,
--@vendorId = 805

---- Test for entry in SV
--select * from SV..SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = 5 AND SR_SUPPLIER_CONTACT_INFO_ID = 73 AND VENDOR_ID = 805 and SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = (select max(SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID) FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP)

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_SAVE_SUPPLIER_CONTACT_MAP_P
@rfpId int,
@contactId int,
@vendorId int

as
	
declare @sr_rfp_supplier_contact_vendor_map_id int 

	set nocount on
	INSERT INTO SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
	(
		 SR_RFP_ID,
		 SR_SUPPLIER_CONTACT_INFO_ID,
		 VENDOR_ID
	)

	VALUES 
	(
		@rfpId,
		@contactId,
		@vendorId
	)

	select @sr_rfp_supplier_contact_vendor_map_id = SCOPE_IDENTITY()
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_SUPPLIER_CONTACT_MAP_P] TO [CBMSApplication]
GO
