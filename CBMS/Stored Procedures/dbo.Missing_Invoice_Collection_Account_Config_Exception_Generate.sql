SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Missing_Invoice_Collection_Account_Config_Exception_Generate                
                            
 DESCRIPTION:                            
   To delete the details of invoice collection account level                 
                            
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
@Invoice_Collection_Account_Config_Id INT  NULL     
                                  
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
        
BEGIN TRAN    
  EXEC dbo.Missing_Invoice_Collection_Account_Config_Exception_Generate    
      @Invoice_Collection_Account_Config_Id = 151    
          
       EXEC dbo.Missing_Invoice_Collection_Account_Config_Exception_Generate    
      @Invoice_Collection_Account_Config_Id = 166    
ROLLBACK          
    
                           
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
 SP                    Sandeep Pigilam    
 RKV       Ravi Kumar Vegesna              
                             
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SP                    2016-12-08       Created   for Invoice tracking    
 RKV                   2017-11-30       Added the additional check is_Primary for 1)UBM type = ADS and ubm url = blank    
                      2)ubm type = ADS and ubm login = blank    
                      3)ubm type = ADS and ubm password = blank     
RKV                   2019-07-22  Added new exception Service_Type
RKV                   2020-O5-08  Added new Missing supplier dates exception.                                    
                           
******/
CREATE PROCEDURE [dbo].[Missing_Invoice_Collection_Account_Config_Exception_Generate]
    (
        @Invoice_Collection_Account_Config_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Additional_Invoices_Per_Period INT
            , @Chase_Priority VARCHAR(25)
            , @bimonthly INT
            , @quarterly INT
            , @biannual INT
            , @annual INT
            , @UBM_Source INT
            , @Vendor_Source INT
            , @Client_Source INT
            , @Online INT
            , @Vendor_Primary_Contact INT
            , @Client_Primary_Contact INT
            , @Account_Primary_Contact INT
            , @Mail_Redirect INT
            , @Is_Any_Primary_Source BIT = 0
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT
            , @Is_Client_Primary BIT = 0
            , @Is_Account_Primary BIT = 0
            , @Is_Vendor_Primary BIT = 0
            , @Custom INT
            , @Custom_frq_1_9_Cd INT
            , @Custom_frq_Custom_Days_Cd INT
            , @Max_Invoice_Collection_Account_Config_Id INT;

        DECLARE
            @In_start_dt DATE
            , @In_end_dt DATE
            , @Missing_Min_dt DATE
            , @Missing_Max_dt DATE
            , @Account_Id INT
            , @IS_Missing_Supplier_Contract_Dates BIT = 0;

        --DECLARE @IS_Missing_Supplier_Contract_Dates TABLE (IS_missing_supplier_Dates BIT)

        DECLARE
            @Exception_1 VARCHAR(MAX)
            , @Exception_2 VARCHAR(MAX)
            , @Exception_3 VARCHAR(MAX)
            , @Exception_4 VARCHAR(MAX)
            , @Exception_5 VARCHAR(MAX)
            , @Exception_6 VARCHAR(MAX)
            , @Exception_7 VARCHAR(MAX)
            , @Exception_8 VARCHAR(MAX)
            , @Exception_9 VARCHAR(MAX)
            , @Exception_10 VARCHAR(MAX)
            , @Exception_11 VARCHAR(MAX)
            , @Exception_12 VARCHAR(MAX)
            , @Exception_13 VARCHAR(MAX)
            , @Exception_14 VARCHAR(MAX)
            , @Exception_15 VARCHAR(MAX)
            , @Exception_16 VARCHAR(MAX)
            , @Exception_17 VARCHAR(MAX)
            , @Exception_18 VARCHAR(MAX)
            , @Exception_19 VARCHAR(MAX)
            , @Exception_20 VARCHAR(MAX)
            , @Exception_21 VARCHAR(MAX)
            , @Exception_22 VARCHAR(MAX)
            , @Exception_23 VARCHAR(MAX)
            , @Exception_24 VARCHAR(MAX)
            , @Exception_25 VARCHAR(MAX)
            , @Exception_26 VARCHAR(MAX)
            , @Exception_27 VARCHAR(MAX)
            , @Exception_28 VARCHAR(MAX)
            , @Exception_29 VARCHAR(MAX)
            , @Exception_30 VARCHAR(MAX)
            , @Exception_31 VARCHAR(MAX)
            , @Exception_32 VARCHAR(MAX)
            , @Exception_33 VARCHAR(MAX)
            , @Exception_34 VARCHAR(MAX)
            , @Exception_35 VARCHAR(MAX)
            , @Exception_36 VARCHAR(MAX)
            , @Exception_37 VARCHAR(MAX)
            , @Exception_38 VARCHAR(MAX)
            , @Exception_39 VARCHAR(MAX)
            , @Exception_40 VARCHAR(MAX)
            , @Exception_41 VARCHAR(MAX)
            , @Exception_42 VARCHAR(MAX)
            , @Exception_43 VARCHAR(MAX)
            , @Exception_44 VARCHAR(MAX)
            , @Exception_45 VARCHAR(MAX)
            , @Exception_46 VARCHAR(MAX)
            , @Exception_47 VARCHAR(MAX)
            , @Exception_48 VARCHAR(MAX)
            , @Exception_49 VARCHAR(MAX)
            , @Exception_50 VARCHAR(MAX)
            , @Exception_51 VARCHAR(MAX)
            , @Exception_52 VARCHAR(MAX)
            , @Exception_53 VARCHAR(MAX)
            , @Exception_54 VARCHAR(MAX)
            , @Exception_55 VARCHAR(MAX)
            , @Exception_56 VARCHAR(MAX)
            , @Exception_57 VARCHAR(MAX)
            , @Exception_58 VARCHAR(MAX)
            , @Exception_59 VARCHAR(MAX)
            , @Exception_60 VARCHAR(MAX)
            , @Exception_61 VARCHAR(MAX)
            , @Exception_62 VARCHAR(MAX)
            , @Exception_63 VARCHAR(MAX)
            , @Exception_64 VARCHAR(MAX)
            , @Exception_65 VARCHAR(MAX)
            , @Exception_66 VARCHAR(MAX)
            , @Exception_67 VARCHAR(MAX)
            , @Exception_68 VARCHAR(MAX);




        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';



        SELECT
            @Custom_frq_1_9_Cd = MAX(CASE WHEN c.Code_Value = '1-9 Days' THEN c.Code_Id
                                     END)
            , @Custom_frq_Custom_Days_Cd = MAX(CASE WHEN c.Code_Value = 'Custom Days' THEN c.Code_Id
                                               END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'FrequencyPattern'
            AND c.Code_Value IN ( '1-9 Days', 'Custom Days' );


        SELECT
            @bimonthly = MAX(CASE WHEN c.Code_Value = 'bi-monthly' THEN c.Code_Id
                             END)
            , @quarterly = MAX(CASE WHEN c.Code_Value = 'quarterly' THEN c.Code_Id
                               END)
            , @biannual = MAX(CASE WHEN c.Code_Value = 'bi-annual' THEN c.Code_Id
                              END)
            , @annual = MAX(CASE WHEN c.Code_Value = 'annual' THEN c.Code_Id
                            END)
            , @Custom = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                            END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceFrequency'
            AND c.Code_Value IN ( 'bi-monthly', 'quarterly', 'bi-annual', 'annual', 'Custom' );


        SELECT
            @UBM_Source = MAX(CASE WHEN c.Code_Value = 'UBM' THEN c.Code_Id
                              END)
            , @Vendor_Source = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                   END)
            , @Client_Source = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                   END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceCollectionSource'
            AND c.Code_Value IN ( 'UBM', 'Vendor', 'Client' );


        SELECT
            @Online = MAX(CASE WHEN c.Code_Value = 'Online' THEN c.Code_Id
                          END)
            , @Vendor_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                            END)
            , @Client_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                            END)
            , @Account_Primary_Contact = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                             END)
            , @Mail_Redirect = MAX(CASE WHEN c.Code_Value = 'Mail Redirect' THEN c.Code_Id
                                   END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Online', 'Vendor Primary Contact', 'Client Primary Contact'
                                  , 'Account Primary Contact', 'Mail Redirect' );

        SELECT
            @Additional_Invoices_Per_Period = Additional_Invoices_Per_Period
            , @Invoice_Collection_Account_Config_Id = Invoice_Collection_Account_Config_Id
            , @Chase_Priority = cp.Code_Value
        FROM
            dbo.Invoice_Collection_Account_Config ica
            LEFT JOIN dbo.Code cp
                ON Chase_Priority_Cd = cp.Code_Id
        WHERE
            Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;



        SELECT
            @Exception_1 = CASE WHEN Is_Chase_Activated = 0 THEN 'Active is blank'
                           END
            , @Exception_2 = CASE WHEN Invoice_Collection_Service_Start_Dt IS NULL THEN
                                      'Invoice collection start date is blank'
                             END
            , @Exception_3 = CASE WHEN Invoice_Collection_Service_End_Dt IS NULL THEN 'Invoice collection end date'
                             END
            , @Exception_4 = CASE WHEN Invoice_Collection_Officer_User_Id IS NULL THEN
                                      'Invoice collection officer is blank'
                             END
            , @Exception_5 = CASE WHEN Chase_Priority_Cd IS NULL THEN 'Chase Priority is blank'
                             END
            , @Exception_6 = CASE WHEN Invoice_Pattern_Cd IS NULL THEN 'Invoice Pattern is blank'
                             END
            , @Exception_65 = CASE WHEN ica.Service_Type_Cd IS NULL THEN 'Service Type is blank'
                              END
        FROM
            dbo.Invoice_Collection_Account_Config ica
        WHERE
            Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;


        SELECT
            @Exception_7 = MAX(CASE WHEN ISNULL(aif.Seq_No, 0) = 0
                                         AND (   Invoice_Frequency_Cd IS NULL
                                                 OR Invoice_Frequency_Cd = 0) THEN
                                        '(Primary) Invoice Frequency is Blank'
                               END)
            , @Exception_8 = MAX(CASE WHEN ISNULL(aif.Seq_No, 0) = 0
                                           AND  Invoice_Frequency_Pattern_Cd IS NULL THEN
                                          '(Primary) Frequency Pattern is Blank'
                                 END)
            , @Exception_9 = MAX(CASE WHEN ISNULL(aif.Seq_No, 0) = 1
                                           AND  (   Invoice_Frequency_Cd IS NULL
                                                    OR  Invoice_Frequency_Cd = 0) THEN
                                          'additional invoices per period =1 AND Additional Invoice Frequency 1 = blank'
                                 END)
            , @Exception_10 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 1)
                                            AND Invoice_Frequency_Pattern_Cd IS NULL THEN
                                           'additional invoices per period =1 AND Additional Frequency Pattern 1 = blank'
                                  END)
            , @Exception_11 = MAX(CASE WHEN aif.Seq_No = 2
                                            AND (   Invoice_Frequency_Cd IS NULL
                                                    OR  Invoice_Frequency_Cd = 0) THEN
                                           'additional invoices per period =2 AND Additional Invoice Frequency 2 = blank'
                                  END)
            , @Exception_12 = MAX(CASE WHEN aif.Seq_No = 2
                                            AND Invoice_Frequency_Pattern_Cd IS NULL THEN
                                           'additional invoices per period =2 AND Additional Frequency Pattern 2 = blank'
                                  END)
            , @Exception_13 = MAX(CASE WHEN aif.Seq_No = 3
                                            AND (   Invoice_Frequency_Cd IS NULL
                                                    OR  Invoice_Frequency_Cd = 0) THEN
                                           'additional invoices per period =3 AND Additional Invoice Frequency 3 = blank'
                                  END)
            , @Exception_14 = MAX(CASE WHEN aif.Seq_No = 3
                                            AND Invoice_Frequency_Pattern_Cd IS NULL THEN
                                           'additional invoices per period =3 AND Additional Frequency Pattern 3 = blank'
                                  END)
            , @Exception_15 = MAX(CASE WHEN aif.Seq_No = 4
                                            AND (   Invoice_Frequency_Cd IS NULL
                                                    OR  Invoice_Frequency_Cd = 0) THEN
                                           'additional invoices per period =4 AND Additional Invoice Frequency 4 = blank'
                                  END)
            , @Exception_16 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND Invoice_Frequency_Pattern_Cd IS NULL THEN
                                           'additional invoices per period =4 AND Additional Frequency Pattern 4 = blank'
                                  END)
            , @Exception_17 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 0)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Issued' THEN
                                           'Chase priority = issued AND Expected Vendor Invoiced issued day = blank'
                                  END)
            , @Exception_19 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 0)
                                            AND Expected_Invoice_Raised_Month IS NULL
                                            AND @Chase_Priority = 'Day issued'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'Chase priority = issued AND invoice frequency = bimonthly, quarterly, biannual, annual and Expected Vendor Invoiced issued Month = blank'
                                  END)
            , @Exception_20 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 0)
                                            AND (   Expected_Invoice_Received_Day IS NULL
                                                    OR  Expected_Invoice_Received_Day = 0)
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Expected' THEN
                                           'Chase priority = expected AND Expected Invoice Received day = blank'
                                  END)
            , @Exception_21 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 0)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                            AND (   aif.Custom_Days IS NULL
                                                    OR  aif.Custom_Days = 0)
                                            AND (   aif.Custom_Invoice_Received_Day IS NULL
                                                    OR  aif.Custom_Invoice_Received_Day = 0) THEN
                                           'Frequency_Pattern = Custom Days AND Custom_Days or Custom_Invoice_Received_Day = blank'
                                  END)
            , @Exception_22 = MAX(CASE WHEN (ISNULL(aif.Seq_No, 0) = 0)
                                            AND Expected_Invoice_Received_Month IS NULL
                                            AND @Chase_Priority = 'Day Expected'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'Chase priority = expected AND invoice frequency = bimonthly, quarterly, biannual, annual and Expected Invoice Received Month = blank    
'
                                  END)
            , @Exception_23 = MAX(CASE WHEN (   aif.Seq_No = 1
                                                OR  icac.Additional_Invoices_Per_Period >= 1)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day issued' THEN
                                           'additional invoices per period =1 and Chase priority = issued AND Expected Vendor Invoiced issued day 1 = blank'
                                  END)
            , @Exception_25 = MAX(CASE WHEN (aif.Seq_No = 1)
                                            AND Expected_Invoice_Raised_Month IS NULL
                                            AND @Chase_Priority = 'Day issued'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =1 and Chase priority = issued AND invoice frequency 1 = bimonthly, quarterly, biannual, annual and Expected Vendor Invoiced issued Month 1 = blank'
                                  END)
            , @Exception_26 = MAX(CASE WHEN (aif.Seq_No = 1)
                                            AND (   Expected_Invoice_Received_Day IS NULL
                                                    OR  Expected_Invoice_Received_Day = 0)
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Expected' THEN
                                           'additional invoices per period =1 and Chase priority = expected AND Expected Invoice Received day 1 = blank'
                                  END)
            , @Exception_27 = MAX(CASE WHEN (aif.Seq_No = 1)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                            AND (   aif.Custom_Days IS NULL
                                                    OR  aif.Custom_Days = 0)
                                            AND (   aif.Custom_Invoice_Received_Day IS NULL
                                                    OR  aif.Custom_Invoice_Received_Day = 0) THEN
                                           'additional invoices per period =1 and Frequency_Pattern = Custom Days AND Custom_Days or Custom_Invoice_Received_Day = blank'
                                  END)
            , @Exception_28 = MAX(CASE WHEN (aif.Seq_No = 1)
                                            AND Expected_Invoice_Received_Month IS NULL
                                            AND @Chase_Priority = 'Day Expected'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =1 and Chase priority = expected AND invoice frequency 1 = bimonthly, quarterly, biannual, annual and Expected Invoice Received Month 1 = blank'
                                  END)
            , @Exception_29 = MAX(CASE WHEN (aif.Seq_No = 2)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day issued' THEN
                                           'additional invoices per period =2 and Chase priority = issued AND Expected Vendor Invoiced issued day 2 = blank'
                                  END)
            , @Exception_31 = MAX(CASE WHEN (aif.Seq_No = 2)
                                            AND Expected_Invoice_Raised_Month IS NULL
                                            AND @Chase_Priority = 'Day issued'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =2 and Chase priority = issued AND invoice frequency 2 = bimonthly, quarterly, biannual, annual and Expected Vendor Invoiced issued Month 2 = blank'
                                  END)
            , @Exception_32 = MAX(CASE WHEN (aif.Seq_No = 2)
                                            AND (   Expected_Invoice_Received_Day IS NULL
                                                    OR  Expected_Invoice_Received_Day = 0)
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Expected' THEN
                                           'additional invoices per period =2 and Chase priority = expected AND Expected Invoice Received day 2 = blank'
                                  END)
            , @Exception_33 = MAX(CASE WHEN (aif.Seq_No = 2)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                            AND (   aif.Custom_Days IS NULL
                                                    OR  aif.Custom_Days = 0)
                                            AND (   aif.Custom_Invoice_Received_Day IS NULL
                                                    OR  aif.Custom_Invoice_Received_Day = 0) THEN
                                           'additional invoices per period =2 and Frequency_Pattern = Custom Days AND Custom_Days or Custom_Invoice_Received_Day = blank'
                                  END)
            , @Exception_34 = MAX(CASE WHEN (aif.Seq_No = 2)
                                            AND Expected_Invoice_Received_Month IS NULL
                                            AND @Chase_Priority = 'Day Expected'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =2 and Chase priority = expected AND invoice frequency 2 = bimonthly, quarterly, biannual, annual and Expected Invoice Received Month 2 = blank'
                                  END)
            , @Exception_35 = MAX(CASE WHEN (aif.Seq_No = 3)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day issued' THEN
                                           'additional invoices per period =3 and Chase priority = issued AND Expected Vendor Invoiced issued day 3 = blank'
                                  END)
            , @Exception_37 = MAX(CASE WHEN (aif.Seq_No = 3)
                                            AND Expected_Invoice_Raised_Month IS NULL
                                            AND @Chase_Priority = 'Day issued'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =3 and Chase priority = issued AND invoice frequency 3 = bimonthly, quarterly, biannual, annual and Expected Vendor Invoiced issued Month 3 = blank'
                                  END)
            , @Exception_38 = MAX(CASE WHEN (   aif.Seq_No = 3
                                                OR  icac.Additional_Invoices_Per_Period >= 3)
                                            AND (   Expected_Invoice_Received_Day IS NULL
                                                    OR  Expected_Invoice_Received_Day = 0)
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Expected' THEN
                                           'additional invoices per period =3 and Chase priority = expected AND Expected Invoice Received day 3 = blank'
                                  END)
            , @Exception_39 = MAX(CASE WHEN (aif.Seq_No = 3)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                            AND (   aif.Custom_Days IS NULL
                                                    OR  aif.Custom_Days = 0)
                                            AND (   aif.Custom_Invoice_Received_Day IS NULL
                                                    OR  aif.Custom_Invoice_Received_Day = 0) THEN
                                           'additional invoices per period =2 and Frequency_Pattern = Custom Days AND Custom_Days or Custom_Invoice_Received_Day = blank'
                                  END)
            , @Exception_40 = MAX(CASE WHEN (aif.Seq_No = 3)
                                            AND Expected_Invoice_Received_Month IS NULL
                                            AND @Chase_Priority = 'Day Expected'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =3 and Chase priority = expected AND invoice frequency 3 = bimonthly, quarterly, biannual, annual and Expected Invoice Received Month 3 = blank'
                                  END)
            , @Exception_41 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day issued' THEN
                                           'additional invoices per period =4 and Chase priority = issued AND Expected Vendor Invoiced issued day 4 = blank'
                                  END)
            , @Exception_43 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND Expected_Invoice_Raised_Month IS NULL
                                            AND @Chase_Priority = 'Day issued'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =4 and Chase priority = issued AND invoice frequency 4 = bimonthly, quarterly, biannual, annual and Expected Vendor Invoiced issued Month 4 = blank'
                                  END)
            , @Exception_44 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND (   Expected_Invoice_Received_Day IS NULL
                                                    OR  Expected_Invoice_Received_Day = 0)
                                            AND aif.Invoice_Frequency_Pattern_Cd <> @Custom_frq_Custom_Days_Cd
                                            AND @Chase_Priority = 'Day Expected' THEN
                                           'additional invoices per period =4 and Chase priority = expected AND Expected Invoice Received day 4 = blank'
                                  END)
            , @Exception_45 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND Expected_Invoice_Raised_Day IS NULL
                                            AND aif.Invoice_Frequency_Pattern_Cd = @Custom_frq_Custom_Days_Cd
                                            AND (   aif.Custom_Days IS NULL
                                                    OR  aif.Custom_Days = 0)
                                            AND (   aif.Custom_Invoice_Received_Day IS NULL
                                                    OR  aif.Custom_Invoice_Received_Day = 0) THEN
                                           'additional invoices per period =4 and Frequency_Pattern = Custom Days AND Custom_Days or Custom_Invoice_Received_Day = blank'
                                  END)
            , @Exception_46 = MAX(CASE WHEN (aif.Seq_No = 4)
                                            AND Expected_Invoice_Received_Month IS NULL
                                            AND @Chase_Priority = 'Day Expected'
                                            AND Invoice_Frequency_Cd IN ( @bimonthly, @quarterly, @biannual, @annual ) THEN
                                           'additional invoices per period =4 and Chase priority = expected AND invoice frequency 4 = bimonthly, quarterly, biannual, annual and Expected Invoice Received Month 4 = blank'
                                  END)
        FROM
            dbo.Invoice_Collection_Account_Config icac
            LEFT OUTER JOIN dbo.Account_Invoice_Collection_Frequency aif
                ON icac.Invoice_Collection_Account_Config_Id = aif.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;



        SELECT
            @Is_Any_Primary_Source = MAX(ISNULL(CAST(aic.Is_Primary AS INT), 0))
            , @Exception_47 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @UBM_Source
                                            AND ubm.UBM_Id IS NULL THEN
                                           'UBM is the primary source of invoice collection and ubm = Blank'
                                  END)
            , @Exception_48 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @UBM_Source
                                            AND ubm.UBM_Type_Cd IS NULL THEN
                                           'UBM is the primary source of invoice collection and ubmType = Blank'
                                  END)
            , @Exception_49 = MAX(CASE WHEN aic.Invoice_Collection_Source_Cd = @UBM_Source
                                            AND aic.Is_Primary = 1
                                            AND ubmtype.Code_Value = 'ADS'
                                            AND ISNULL(onl.URL, '') = '' THEN
                                           'UBM is the primary source of invoice collection and UBM type = ADS and ubm url = blank'
                                  END)
            , @Exception_51 = MAX(CASE WHEN aic.Invoice_Collection_Source_Cd = @UBM_Source
                                            AND aic.Is_Primary = 1
                                            AND ubmtype.Code_Value = 'ADS'
                                            AND ISNULL(onl.Login_Name, '') = '' THEN
                                           'UBM is the primary source of invoice collection and ubm type = ADS and if ubm login = blank'
                                  END)
            , @Exception_52 = MAX(CASE WHEN aic.Invoice_Collection_Source_Cd = @UBM_Source
                                            AND aic.Is_Primary = 1
                                            AND ubmtype.Code_Value = 'ADS'
                                            AND ISNULL(onl.Passcode, '') = '' THEN
                                           'UBM is the primary source of invoice collection and ubm type = ADS and if ubm password = blank'
                                  END)
            , @Exception_53 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Vendor_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.URL, '') = '' THEN
                                           'Primary source is vendor online and vendor url = blank'
                                  END)
            , @Exception_54 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Vendor_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.Login_Name, '') = '' THEN
                                           'Primary source is vendor online and and vendor login = blank'
                                  END)
            , @Exception_55 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Vendor_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.Passcode, '') = '' THEN
                                           'Primary source is vendor online and and vendor password = blank'
                                  END)
            , @Exception_56 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Vendor_Source
                                            AND aic.Invoice_Source_Type_Cd = @Vendor_Primary_Contact
                                            AND aic.Invoice_Source_Method_of_Contact_Cd IS NULL THEN
                                           'Primary source is Vendor Primary Contact and vendor method of contact = blank'
                                  END)
            , @Exception_57 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                            AND aic.Invoice_Source_Type_Cd = @Client_Primary_Contact
                                            AND aic.Invoice_Source_Method_of_Contact_Cd IS NULL THEN
                                           'Primary source is Client Primary Contact and client method of contact = blank'
                                  END)
            , @Exception_58 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                            AND aic.Invoice_Source_Type_Cd = @Account_Primary_Contact
                                            AND aic.Invoice_Source_Method_of_Contact_Cd IS NULL THEN
                                           'Primary source is Account Primary Contact and client method of contact = blank'
                                  END)
            , @Exception_59 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.URL, '') = '' THEN
                                           'Primary source is Client online and client url = blank'
                                  END)
            , @Exception_60 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.Login_Name, '') = '' THEN
                                           'Primary source is Client online and client login = blank'
                                  END)
            , @Exception_61 = MAX(CASE WHEN aic.Is_Primary = 1
                                            AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                            AND aic.Invoice_Source_Type_Cd = @Online
                                            AND ISNULL(onl.Passcode, '') = '' THEN
                                           ' Primary source is Client online and client password = blank'
                                  END)
            , @Is_Client_Primary = MAX(CASE WHEN aic.Is_Primary = 1
                                                 AND aic.Invoice_Collection_Source_Cd = @Client_Source
                                                 AND aic.Invoice_Source_Type_Cd = @Client_Primary_Contact THEN 1
                                       END)
            , @Is_Account_Primary = MAX(CASE WHEN aic.Is_Primary = 1
                                                  AND   aic.Invoice_Collection_Source_Cd = @Client_Source
                                                  AND   aic.Invoice_Source_Type_Cd = @Account_Primary_Contact THEN 1
                                        END)
            , @Is_Vendor_Primary = MAX(CASE WHEN aic.Is_Primary = 1
                                                 AND aic.Invoice_Collection_Source_Cd = @Vendor_Source
                                                 AND aic.Invoice_Source_Type_Cd = @Vendor_Primary_Contact THEN 1
                                       END)
        FROM
            dbo.Account_Invoice_Collection_Source aic
            LEFT JOIN dbo.Account_Invoice_Collection_UBM_Source_Dtl ubm
                ON aic.Account_Invoice_Collection_Source_Id = ubm.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.Account_Invoice_Collection_Online_Source_Dtl onl
                ON aic.Account_Invoice_Collection_Source_Id = onl.Account_Invoice_Collection_Source_Id
            LEFT JOIN dbo.Code ubmtype
                ON ubm.UBM_Type_Cd = ubmtype.Code_Id
        WHERE
            aic.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

        SELECT
            @Exception_46 = 'No source (out of the seven options) is checked as Primary for invoice collection'
        WHERE
            @Is_Any_Primary_Source = 0
            AND @Invoice_Collection_Account_Config_Id IS NOT NULL;

        SELECT
            @Exception_62 = 'Client Primary Contact is blank'
        WHERE
            @Is_Client_Primary = 1
            AND @Invoice_Collection_Account_Config_Id IS NOT NULL;

        SELECT
            @Exception_63 = 'Account Primary Contact is blank'
        WHERE
            @Is_Account_Primary = 1
            AND @Invoice_Collection_Account_Config_Id IS NOT NULL;

        SELECT
            @Exception_64 = 'Vendor Primary Contact is blank'
        WHERE
            @Is_Vendor_Primary = 1
            AND @Invoice_Collection_Account_Config_Id IS NOT NULL;

        SELECT
            @Exception_62 = NULL
        FROM
            dbo.Invoice_Collection_Account_Contact iac
            INNER JOIN dbo.Contact_Info ci
                ON iac.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cl
                ON ci.Contact_Level_Cd = cl.Code_Id
        WHERE
            iac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND cl.Code_Value = 'Client'
            AND iac.Is_Primary = 1;



        SELECT
            @Exception_63 = NULL
        FROM
            dbo.Invoice_Collection_Account_Contact iac
            INNER JOIN dbo.Contact_Info ci
                ON iac.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cl
                ON ci.Contact_Level_Cd = cl.Code_Id
        WHERE
            iac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND cl.Code_Value = 'Account'
            AND iac.Is_Primary = 1;



        SELECT
            @Exception_64 = NULL
        FROM
            dbo.Invoice_Collection_Account_Contact iac
            INNER JOIN dbo.Contact_Info ci
                ON iac.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.Code cl
                ON ci.Contact_Level_Cd = cl.Code_Id
        WHERE
            iac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND cl.Code_Value = 'Vendor'
            AND iac.Is_Primary = 1;

        /* supplier date exception*/




        SELECT
            @Account_Id = Account_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

        SELECT
            @In_start_dt = MIN(Supplier_Account_Begin_Dt)
            , @In_end_dt = MAX(CASE WHEN Supplier_Account_End_Dt IS NULL THEN '2099-12-31'
                                   ELSE Supplier_Account_End_Dt
                               END)
        FROM
            Supplier_Account_Config
        WHERE
            Account_Id = @Account_Id
        GROUP BY
            Account_Id;



        SELECT
            Date_D
        INTO
            #Missing_Dates
        FROM
            meta.Date_Dim_Expd dd1
        WHERE
            dd1.Date_D BETWEEN @In_start_dt
                       AND     @In_end_dt
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Supplier_Account_Config r
                                    CROSS JOIN meta.Date_Dim_Expd dd
                               WHERE
                                    dd.Date_D BETWEEN r.Supplier_Account_Begin_Dt
                                              AND     ISNULL(r.Supplier_Account_End_Dt, '2099-12-31')
                                    AND dd1.Date_D = dd.Date_D
                                    AND r.Account_Id = @Account_Id);




        SELECT
            @Max_Invoice_Collection_Account_Config_Id = MAX(Invoice_Collection_Account_Config_Id)
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Account_Id = @Account_Id
            AND EXISTS (SELECT  1 FROM  #Missing_Dates md)
        GROUP BY
            icac.Account_Id;


        SELECT
            @IS_Missing_Supplier_Contract_Dates = 1
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Account_Id = @Account_Id
            AND EXISTS (SELECT  1 FROM  #Missing_Dates md)
        GROUP BY
            icac.Account_Id
        HAVING
            COUNT(1) = 1;



        SELECT
            @Missing_Min_dt = MIN(md.Date_D)
            , @Missing_Max_dt = MAX(md.Date_D)
        FROM
            #Missing_Dates md;


        SELECT
            @IS_Missing_Supplier_Contract_Dates = 1
        FROM
            dbo.Invoice_Collection_Account_Config icac
        WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @IS_Missing_Supplier_Contract_Dates = 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Missing_Dates md
                           WHERE
                                md.Date_D BETWEEN icac.Invoice_Collection_Service_Start_Dt
                                          AND     ISNULL(icac.Invoice_Collection_Service_End_Dt, '2099-12-01'));


        SELECT
            @IS_Missing_Supplier_Contract_Dates = 1
        WHERE
            @Max_Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
			AND EXISTS (SELECT  1 FROM  #Missing_Dates md)
			AND @IS_Missing_Supplier_Contract_Dates = 0






        SELECT
            @Exception_65 = 'There is a gap between the supplier contract timebands'
        WHERE
            @IS_Missing_Supplier_Contract_Dates = 1;

        DECLARE @Exceptions TABLE
              (
                  ID INT IDENTITY(1, 1)
                  , Exception_Type VARCHAR(MAX)
              );

        INSERT INTO @Exceptions
             (
                 Exception_Type
             )
        VALUES
            (@Exception_1)
            , (@Exception_2)
            , (@Exception_3)
            , (@Exception_4)
            , (@Exception_5)
            , (@Exception_6)
            , (@Exception_7)
            , (@Exception_8)
            , (@Exception_9)
            , (@Exception_10)
            , (@Exception_11)
            , (@Exception_12)
            , (@Exception_13)
            , (@Exception_14)
            , (@Exception_15)
            , (@Exception_16)
            , (@Exception_17)
            , (@Exception_18)
            , (@Exception_19)
            , (@Exception_20)
            , (@Exception_21)
            , (@Exception_22)
            , (@Exception_23)
            , (@Exception_24)
            , (@Exception_25)
            , (@Exception_26)
            , (@Exception_27)
            , (@Exception_28)
            , (@Exception_29)
            , (@Exception_30)
            , (@Exception_31)
            , (@Exception_32)
            , (@Exception_33)
            , (@Exception_34)
            , (@Exception_35)
            , (@Exception_36)
            , (@Exception_37)
            , (@Exception_38)
            , (@Exception_39)
            , (@Exception_40)
            , (@Exception_41)
            , (@Exception_42)
            , (@Exception_43)
            , (@Exception_44)
            , (@Exception_45)
            , (@Exception_46)
            , (@Exception_47)
            , (@Exception_48)
            , (@Exception_49)
            , (@Exception_50)
            , (@Exception_51)
            , (@Exception_52)
            , (@Exception_53)
            , (@Exception_54)
            , (@Exception_55)
            , (@Exception_56)
            , (@Exception_57)
            , (@Exception_58)
            , (@Exception_59)
            , (@Exception_60)
            , (@Exception_61)
            , (@Exception_62)
            , (@Exception_63)
            , (@Exception_64)
            , (@Exception_65);



        SELECT
            ID
            , Exception_Type
        FROM
            @Exceptions E
        WHERE
            E.Exception_Type IS NOT NULL;


        DROP TABLE #Missing_Dates;
    END;

    ;
GO

GRANT EXECUTE ON  [dbo].[Missing_Invoice_Collection_Account_Config_Exception_Generate] TO [CBMSApplication]
GO
