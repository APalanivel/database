SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
 Account_Commodity_Recalc_Type_Sel_By_Invoice_Id      
      
DESCRIPTION:      
      
      
INPUT PARAMETERS:      
 Name				DataType		Default				Description      
---------------------------------------------------------------------      
 @cu_invoice_id		int                         
      
OUTPUT PARAMETERS:      
 Name				DataType		Default				Description      
---------------------------------------------------------------------      
      
USAGE EXAMPLES:      
---------------------------------------------------------------------      
USE CBMS_TK3 
     
 EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id 30562408    
   
 EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id 35237378   
   
 EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id 35234563  
 
 EXEC Account_Commodity_Recalc_Type_Sel_By_Invoice_Id 75254082   
 

      
AUTHOR INITIALS:  
    
 Initials	Name      
---------------------------------------------------------------------      
 RKV		Ravi Kumar Vegesna  
 NR			Narayana Reddy
   
      
MODIFICATIONS      
      
Initials	Date			Modification      
---------------------------------------------------------------------      
RKV			2015-10-01		Created  
NR			2017-02-16		MAINT-4916 Showing contract_recalc_type column as code_desc insted of Code_Value.
NR			2019-08-08		Add Contract - Added Contract Level Recals.
NR			2020-02-10		PRSUPPORT-3494 - Handled Consolidated accounts if any recalc copied from Contract level.
******/
CREATE PROCEDURE [dbo].[Account_Commodity_Recalc_Type_Sel_By_Invoice_Id]
    (
        @cu_invoice_id INT
    )
AS
    BEGIN


        DECLARE @Group_Legacy_Name TABLE
              (
                  GROUP_INFO_ID INT
                  , GROUP_NAME VARCHAR(200)
                  , Legacy_Group_Name VARCHAR(200)
              );

        INSERT INTO @Group_Legacy_Name
             (
                 GROUP_INFO_ID
                 , GROUP_NAME
                 , Legacy_Group_Name
             )
        EXEC dbo.Group_Info_Sel_By_Group_Legacy
            @Group_Legacy_Name_Cd_Value = 'Standing Data Team';




        DECLARE @Account_Service_Month_Recacl_type_Latest_Service_Month TABLE
              (
                  Account_id INT
                  , service_Month DATETIME
                  , Commodity_Id INT
                  , Invoice_Recalc_Type_Cd INT
                  , Determinant_Source_Cd INT
              );



        DECLARE @Account_Service_Month_Recacl_type TABLE
              (
                  Account_id INT
                  , service_Month DATETIME
                  , Commodity_Id INT
                  , Invoice_Recalc_Type_Cd INT
                  , Determinant_Source_Cd INT
              );


        -- to consider the latest recalc_type after removing the recalc_type 'No_Recalc'
        INSERT INTO @Account_Service_Month_Recacl_type
             (
                 Account_id
                 , service_Month
                 , Commodity_Id
                 , Invoice_Recalc_Type_Cd
                 , Determinant_Source_Cd
             )
        SELECT
            cism.Account_ID
            , cism.SERVICE_MONTH
            , acirt.Commodity_ID
            , acirt.Invoice_Recalc_Type_Cd
            , acirt.Determinant_Source_Cd
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON acirt.Account_Id = cism.Account_ID
            INNER JOIN Code c
                ON c.Code_Id = acirt.Invoice_Recalc_Type_Cd
        WHERE
            CU_INVOICE_ID = @cu_invoice_id
            AND SERVICE_MONTH BETWEEN acirt.Start_Dt
                              AND     acirt.End_Dt
            AND Code_Value <> 'No Recalc'
            AND Invoice_Recalc_Type_Cd IS NOT NULL
        GROUP BY
            cism.Account_ID
            , cism.SERVICE_MONTH
            , acirt.Commodity_ID
            , acirt.Invoice_Recalc_Type_Cd
            , acirt.Determinant_Source_Cd;



        INSERT INTO @Account_Service_Month_Recacl_type
             (
                 Account_id
                 , service_Month
                 , Commodity_Id
                 , Invoice_Recalc_Type_Cd
                 , Determinant_Source_Cd
             )
        SELECT
            cism.Account_ID
            , cism.SERVICE_MONTH
            , acirt.Commodity_ID
            , acirt.Invoice_Recalc_Type_Cd
            , acirt.Determinant_Source_Cd
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON acirt.Account_Id = cism.Account_ID
            INNER JOIN Code c
                ON c.Code_Id = acirt.Invoice_Recalc_Type_Cd
        WHERE
            CU_INVOICE_ID = @cu_invoice_id
            AND SERVICE_MONTH BETWEEN acirt.Start_Dt
                              AND     ISNULL(acirt.End_Dt, '2099-12-01')
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    @Account_Service_Month_Recacl_type asmrt
                               WHERE
                                    asmrt.Account_id = cism.Account_ID)
        GROUP BY
            cism.Account_ID
            , cism.SERVICE_MONTH
            , acirt.Commodity_ID
            , acirt.Invoice_Recalc_Type_Cd
            , acirt.Determinant_Source_Cd;



        INSERT INTO @Account_Service_Month_Recacl_type_Latest_Service_Month
             (
                 Account_id
                 , service_Month
                 , Commodity_Id
                 , Invoice_Recalc_Type_Cd
                 , Determinant_Source_Cd
             )
        SELECT
            asmrt.Account_id
            , asmrt.service_Month
            , asmrt.Commodity_Id
            , asmrt.Invoice_Recalc_Type_Cd
            , asmrt.Determinant_Source_Cd
        FROM
            @Account_Service_Month_Recacl_type asmrt
            INNER JOIN @Account_Service_Month_Recacl_type asmrt2
                ON asmrt.Account_id = asmrt2.Account_id
        GROUP BY
            asmrt.Account_id
            , asmrt.service_Month
            , asmrt.Invoice_Recalc_Type_Cd
            , asmrt.Commodity_Id
            , asmrt.Determinant_Source_Cd
        HAVING
            asmrt.service_Month = MAX(asmrt2.service_Month);



        SELECT
            cism.Account_ID
            , cha.Account_Number
            , com.Commodity_Id
            , com.Commodity_Name
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN ISNULL(c.Code_Value, con_crt.Code_Value)
                  ELSE NULL
              END AS Recalc_Type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN ISNULL(acirt.Invoice_Recalc_Type_Cd, con_crt.Code_Id)
                  ELSE NULL
              END AS Invoice_Recalc_Type_Cd
            , cha.Account_Group_ID
            , e.ENTITY_ID AS Account_Type_Id
            , e.ENTITY_NAME AS Account_Type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE ISNULL(c.Code_Dsc, crt.Code_Dsc)
              END AS contract_recalc_type
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE ISNULL(c.Code_Id, crt.Code_Id)
              END AS contract_recalc_Cd
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS analyst
            , ch.Client_Id
            , ch.Client_Name
            , con.ED_CONTRACT_NUMBER
            , CASE WHEN ciacrl.Is_Locked IS NULL THEN 0
                  ELSE ciacrl.Is_Locked
              END Is_Locked
            , acirt.Determinant_Source_Cd
            , Source_Cd.Code_Value AS Determinant_Source
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = cism.Account_ID
            LEFT OUTER JOIN @Account_Service_Month_Recacl_type_Latest_Service_Month acirt
                ON acirt.Account_id = cism.Account_ID
                   AND  cha.Commodity_Id = acirt.Commodity_Id
            LEFT OUTER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.ENTITY e
                ON e.ENTITY_NAME = cha.Account_Type
            LEFT OUTER JOIN(dbo.Contract_Cu_Invoice_Map ccim
                            INNER JOIN dbo.Contract_Recalc_Config crc
                                ON crc.Contract_Id = ccim.Contract_Id
                                   AND  crc.Is_Consolidated_Contract_Config = 0 --> For Supplier
                                   AND  ccim.Cu_Invoice_Id = @cu_invoice_id
                            INNER JOIN dbo.Code crt
                                ON crt.Code_Id = crc.Supplier_Recalc_Type_Cd)
                ON ccim.Account_Id = cism.Account_ID
                   AND  crc.Commodity_Id = cha.Commodity_Id
                   AND  cism.SERVICE_MONTH BETWEEN crc.Start_Dt
                                           AND     ISNULL(crc.End_Dt, '2099-12-01')
            LEFT OUTER JOIN(dbo.Contract_Cu_Invoice_Map Con_ccim
                            INNER JOIN dbo.Contract_Recalc_Config Con_crc
                                ON Con_crc.Contract_Id = Con_ccim.Contract_Id
                                   AND  Con_crc.Is_Consolidated_Contract_Config = 1 --> Consolidated
                                   AND  Con_ccim.Cu_Invoice_Id = @cu_invoice_id
                            INNER JOIN dbo.Code con_crt
                                ON con_crt.Code_Id = Con_crc.Supplier_Recalc_Type_Cd)
                ON Con_ccim.Account_Id = cism.Account_ID
                   AND  Con_crc.Commodity_Id = cha.Commodity_Id
                   AND  cism.SERVICE_MONTH BETWEEN Con_crc.Start_Dt
                                           AND     ISNULL(Con_crc.End_Dt, '2099-12-01')
            LEFT JOIN(dbo.UTILITY_DETAIL ud
                      JOIN Utility_Detail_Analyst_Map udmap
                          ON udmap.Utility_Detail_ID = ud.UTILITY_DETAIL_ID
                      JOIN GROUP_INFO gi
                          ON gi.GROUP_INFO_ID = udmap.Group_Info_ID
                      INNER JOIN @Group_Legacy_Name gil
                          ON gi.GROUP_INFO_ID = gil.GROUP_INFO_ID)
                ON ud.VENDOR_ID = cha.Account_Vendor_Id
            LEFT OUTER JOIN USER_INFO ui
                ON ui.USER_INFO_ID = udmap.Analyst_ID
            LEFT OUTER JOIN dbo.CONTRACT AS con
                ON con.CONTRACT_ID = cha.Supplier_Contract_ID
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity ciac
                ON ciac.Account_Id = cha.Account_Id
                   AND  ciac.Commodity_Id = cha.Commodity_Id
                   AND  ciac.Cu_Invoice_Id = cism.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Lock ciacrl
                ON ciacrl.Cu_Invoice_Account_Commodity_Id = ciac.Cu_Invoice_Account_Commodity_Id
            LEFT OUTER JOIN dbo.Code Source_Cd
                ON Source_Cd.Code_Id = acirt.Determinant_Source_Cd
        WHERE
            cism.CU_INVOICE_ID = @cu_invoice_id
            AND e.ENTITY_TYPE = 111
            AND (   e.ENTITY_NAME = 'Utility'
                    OR  (   e.ENTITY_NAME = 'Supplier'
                            AND (   cism.Begin_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                  AND     cha.Supplier_Account_End_Dt
                                    OR  cism.End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                                    AND     cha.Supplier_Account_End_Dt
                                    OR  cha.Supplier_Account_begin_Dt BETWEEN cism.Begin_Dt
                                                                      AND     cism.End_Dt
                                    OR  cha.Supplier_Account_End_Dt BETWEEN cism.Begin_Dt
                                                                    AND     cism.End_Dt)))
        GROUP BY
            cism.Account_ID
            , cha.Account_Number
            , com.Commodity_Id
            , com.Commodity_Name
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN ISNULL(c.Code_Value, con_crt.Code_Value)
                  ELSE NULL
              END
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN ISNULL(acirt.Invoice_Recalc_Type_Cd, con_crt.Code_Id)
                  ELSE NULL
              END
            , cha.Account_Group_ID
            , e.ENTITY_ID
            , e.ENTITY_NAME
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE ISNULL(c.Code_Dsc, crt.Code_Dsc)
              END
            , CASE WHEN e.ENTITY_NAME = 'Utility' THEN NULL
                  ELSE ISNULL(c.Code_Id, crt.Code_Id)
              END
            , ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
            , ch.Client_Id
            , ch.Client_Name
            , con.ED_CONTRACT_NUMBER
            , CASE WHEN ciacrl.Is_Locked IS NULL THEN 0
                  ELSE ciacrl.Is_Locked
              END
            , acirt.Determinant_Source_Cd
            , Source_Cd.Code_Value;
    END;









GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Recalc_Type_Sel_By_Invoice_Id] TO [CBMSApplication]
GO
