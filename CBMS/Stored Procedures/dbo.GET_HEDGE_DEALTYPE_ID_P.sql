SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_HEDGE_DEALTYPE_ID_P
	@entity_name VARCHAR(200),
	@entity_type INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT entity_Id
	FROM dbo.entity
	WHERE entity_name =@entity_name
		AND entity_type=@entity_type

END
GO
GRANT EXECUTE ON  [dbo].[GET_HEDGE_DEALTYPE_ID_P] TO [CBMSApplication]
GO
