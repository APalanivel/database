SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
------------------------------------------------------------    

/******    
NAME:    
 Su.Client_Emission_Factor_Sources_Sel 

DESCRIPTION:    
 Used to get emission factor sources for a commodity   

INPUT PARAMETERS:    
Name      DataType Default Description    
------------------------------------------------------------    
@Client_id INT  
          
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  
	EXEC Su.Client_Emission_Factor_Sources_Sel 11528  
	EXEC Su.Client_Emission_Factor_Sources_Sel 170

	
	SELECT * FROM CORE.CLIENT_COMMODITY WHERE CLIENT_ID = 11528
	

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials	Date  Modification    
------------------------------------------------------------    
GB			created  
SKA			02-JUL-09	Modified  
SS						Replaced UNION with UNION ALL, Refactored the query to use Ranking
SS			20-JUL-09	ROW_NUMBER() Replaced by COUNT() Function
SS			24-AUG-09	Modified as per the coding standards (ANSI JOINS)
SS			06-SEP-09	Sp Renamed from Su.ClientEmissionFactorSources_Sel to Su.ClientEmissionFactorSources_Sel,
						Written as a single statement using a left join instead of not exists and a case statement to set the commodity name as suggested
SKA			01/19/2010  Referred Commodity_Service_Cd from Core.Client_commodity for sustainability
SKA			03/18/2010  Used CTE concept to avoid multiple selects

HG			03/31/2010	All the columns qualified with the owner.
						(Cte_Commodity used to find if any of the commodity added for the same client for more than one Scope
						as the rownumber gets calculated based on the partition of commodity and scope Rownumber = 2 will give all the commodities added for more than scope.)
******/  
CREATE PROCEDURE Su.Client_Emission_Factor_Sources_Sel 
(
@Client_id INT  
)   
AS  
BEGIN  

	SET NOCOUNT ON;

	WITH CTE_Client_Emission AS
	(
		SELECT
			cc.Client_Commodity_Id,
			cc.Commodity_Id,
			cm.Commodity_Name,
			cc.Scope_Cd,
			c1.Code_Value AS Scope_Name,
			cc.Hier_Level_Cd ,
			c2.Code_Value AS Level_Name,
			cc.Frequency_Cd,
			c3.Code_Value AS Frequency_Name,
			ROW_NUMBER() OVER (PARTITION BY cc.Commodity_Id ORDER BY cc.Commodity_Id ) AS ROWNUMBER
		FROM
			Core.Client_Commodity cc
			JOIN dbo.COMMODITY cm 
				ON cm.COMMODITY_ID = cc.Commodity_Id 
			JOIN dbo.CODE c1 
				ON cc.Scope_Cd = c1.CODE_ID 
			JOIN dbo.CODE c2 
				ON cc.Hier_Level_Cd = c2.CODE_ID 
			JOIN dbo.CODE c3  
				ON cc.Frequency_Cd = c3.CODE_ID 
			JOIN dbo.Code c4
				ON cc.Commodity_Service_Cd = c4.Code_Id 
		WHERE
			cc.Client_Id = @Client_id
			AND c4.Code_Value = 'GHG'
	)
	, Cte_Commodity
	AS
	(	SELECT 
			cte2.Commodity_Id
		FROM 
			CTE_Client_Emission CTE2
		WHERE
			ROWNUMBER = 2
	)
	SELECT
		CTE1.Client_Commodity_Id,
		CTE1.Commodity_Id,
		Commodity_Name = CASE 
							WHEN x.Commodity_Id IS NULL THEN CTE1.Commodity_Name 
							ELSE CTE1.Commodity_Name + ' (' + CTE1.Scope_Name + ')' 
						 END,
		CTE1.Scope_Cd,
		CTE1.Scope_Name,
		CTE1.Hier_Level_Cd ,
		CTE1.Level_Name,
		CTE1.Frequency_Cd,
		CTE1.Frequency_Name,
		ROWNUMBER
	FROM
		CTE_Client_Emission CTE1
		LEFT JOIN Cte_Commodity x
			ON x.Commodity_Id = CTE1.COMMODITY_ID
	ORDER BY
		CTE1.Hier_Level_Cd

END
GO
GRANT EXECUTE ON  [su].[Client_Emission_Factor_Sources_Sel] TO [CBMSApplication]
GO
