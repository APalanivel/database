
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 CBMS.dbo.Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity    
    
DESCRIPTION:    
    
 Used to Select the data from Cost_Usage_Account_Dtl table for the given Site_id , period and commodity    
     
INPUT PARAMETERS:    
 Name				DataType  Default Description    
------------------------------------------------------------    
 @Client_Hier_Id    int    
 @Account_Id		int       
 @Commodity_id		int    
 @Begin_Dt			Date    
 @End_Dt			Date    
 @Uom_Id			INT    
 @Currency_Unit_Id  INT

OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

  EXEC dbo.Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity 12542,null,67,'1/1/2010','12/1/2010',1409,3
  EXEC dbo.Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity 270433,null,66,'1/1/2004','12/1/2004',1405,3

AUTHOR INITIALS:
 Initials   Name
------------------------------------------------------------
 HG			Hari    
 BCH		Balaraju    
 AP			Athmaram Pabbathi

MODIFICATIONS
 Initials   Date		Modification
------------------------------------------------------------
 HG         1/28/2010   Created    
 SKA		02/16/2010  Added the logic to calculate the Marketer cost and utility cost    
 SSR		02/23/2010  Added table Alias    
 SKA		03/05/2010  Added the LEFT OUTER JOIN with meta.Date_Dim to get the constant no of rows.    
 HG			03/11/2010  Changed the usage column name in to Volume.    
 HG			03/12/2010  Converted_Uom_name column added in the select clause    
 HG			04/05/2010  In second part of the union query with in Cte_Cost_Usage_Account_Dtl the site_id selection and filter should be based on utility account site but it was wrongly filtered based on supplier account corrected it.    
 BCH		10/10/2011  Added Data_Source_Code column in the select and used code table with left outer join    
			13/10/2011  Used to finding the  utility and supplier account type id's from variables    
						Added data_source_cd column to select list    
 AP			03/19/2012  Addnl Data Changes  
						-- Replaced @Site_Id, @Client_Id parameter with @Client_Hier_Id  
						-- Removed Client table reference and used Client_Hier table  
						-- used dbo.Cost_usage_Bucket_Sel_By_Commodity SP to get the cost & usage buckets  
 AP			04/26/2012  Replaced Display_Account_Number with Account_Number  
 AKR		06/08/2012  Modified the Order By Clause
 HG			2012-06-08	Modified the query to return the actual values saved instead of returning 0 if the bucket value is NULL as we want to differentiate between NULL and value(0 or any)
 HG			2012-06-13	Site_Id join added on Invoice_Participation table as supplier accounts spans across multiple sites will have multiple entries in this table
******/
CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity
      ( 
       @Client_Hier_Id INT
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Uom_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN    
      SET NOCOUNT ON    
    
      DECLARE
            @Currency_Group_Id INT
           ,@Converted_Uom_Name VARCHAR(200)  
    
      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )  
  
  
      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id  
              
      SELECT
            @Converted_Uom_Name = uom.Entity_Name
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Id = @Uom_Type_Id  
  
--Inserting Cost & Usage Buckets to table variable  
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id ;  
  
      WITH  Cte_Cost_Usage_Account_Dtl
              AS ( SELECT
                        cua.ACCOUNT_ID
                       ,cha.Account_Number
                       ,ltrim(rtrim(cha.Account_Type)) AS Account_Type
                       ,cha.Account_Vendor_Id VENDOR_ID
                       ,cha.Account_Vendor_Name VENDOR_NAME
                       ,cha.Site_Id
                       ,cua.Service_Month
                       ,dts_code.Code_Value Data_Source_Code
                       ,sum(case WHEN cub.Bucket_Type = 'Determinant' THEN ( Bucket_Value * uc.Conversion_factor )
                            END) Volume
                       ,sum(case WHEN cub.Bucket_Type = 'Charge'
                                      AND cha.Account_Type = 'Utility' THEN ( Bucket_Value * cc.Conversion_Factor )
                            END) utility_cost
                       ,sum(case WHEN cub.Bucket_Type = 'Charge'
                                      AND cha.Account_Type = 'Supplier' THEN ( Bucket_Value * cc.Conversion_Factor )
                            END) marketer_cost
                       ,sum(case WHEN cub.Bucket_Type = 'Charge' THEN ( Bucket_Value * cc.Conversion_Factor )
                            END) Total_cost
                   FROM
                        ( SELECT
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Type
                             ,cha.Account_Number
                             ,cha.Account_Vendor_Id
                             ,cha.Account_Vendor_Name
                             ,ch.Site_Id
                          FROM
                              core.Client_Hier_Account cha
                              JOIN core.Client_Hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                          WHERE
                              ( @Account_Id IS NULL
                                OR cha.Account_id = @Account_Id )
                              AND cha.Client_Hier_Id = @Client_Hier_Id
                              AND cha.Commodity_Id = @Commodity_Id
                          GROUP BY
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,cha.Account_Type
                             ,cha.Account_Number
                             ,cha.Account_Vendor_Id
                             ,cha.Account_Vendor_Name
                             ,ch.Site_Id ) cha
                        JOIN dbo.Cost_Usage_Account_Dtl cua
                              ON cua.ACCOUNT_ID = cha.ACCOUNT_ID
                                 AND cua.Client_Hier_Id = cha.Client_Hier_Id
                        JOIN @Cost_Usage_Bucket_Id cub
                              ON cub.Bucket_Master_Id = cua.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.Code dts_code
                              ON dts_code.code_id = cua.data_source_cd
                        LEFT OUTER JOIN dbo.consumption_unit_conversion uc
                              ON uc.BASE_UNIT_ID = cua.UOM_Type_Id
                                 AND uc.CONVERTED_UNIT_ID = @Uom_Type_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cc
                              ON cc.currency_group_id = @Currency_Group_id
                                 AND cc.base_unit_id = cua.CURRENCY_UNIT_ID
                                 AND cc.converted_unit_id = @Currency_unit_id
                                 AND cc.conversion_date = cua.Service_month
                   GROUP BY
                        cua.ACCOUNT_ID
                       ,cha.Account_Number
                       ,cha.Account_Type
                       ,cha.Account_Vendor_Id
                       ,cha.Account_Vendor_Name
                       ,cua.Service_Month
                       ,dts_code.Code_Value
                       ,cha.SIte_Id)
            SELECT
                  cua.Account_Id
                 ,cua.Account_Number
                 ,cua.Account_Type
                 ,cua.Vendor_Id
                 ,cua.Vendor_Name
                 ,dd.Date_D Service_Month
                 ,sum(cua.Volume) AS Volume
                 ,sum(cua.utility_cost) AS Utility_Cost
                 ,sum(cua.marketer_cost) AS Marketer_Cost
                 ,sum(cua.Total_cost) AS Total_Cost
                 ,sum(cua.Total_cost) / nullif(sum(cua.Volume), 0) AS Unit_Cost
                 ,@Converted_Uom_Name AS Converted_Uom_Name
                 ,ip.Cbms_Image_Id
                 ,cua.Data_Source_Code
            FROM
                  meta.Date_Dim dd
                  LEFT OUTER JOIN Cte_Cost_Usage_Account_Dtl cua
                        ON cua.Service_Month = dd.Date_D
                  LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION ip
                        ON ip.Site_Id = cua.Site_Id
                           AND ip.ACCOUNT_ID = cua.ACCOUNT_ID
                           AND ip.SERVICE_MONTH = cua.Service_Month
            WHERE
                  dd.Date_D BETWEEN @Begin_Dt AND @End_Dt
            GROUP BY
                  cua.Account_Id
                 ,cua.Account_Number
                 ,cua.Account_Type
                 ,cua.Vendor_Id
                 ,cua.Vendor_Name
                 ,dd.Date_D
                 ,ip.Cbms_Image_Id
                 ,cua.Data_Source_Code
            ORDER BY
                  cua.Account_Type DESC
                 ,cua.Account_Number ASC
                 ,cua.Vendor_Name
                   
END 


;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Sel_By_Client_Account_Commodity] TO [CBMSApplication]
GO
