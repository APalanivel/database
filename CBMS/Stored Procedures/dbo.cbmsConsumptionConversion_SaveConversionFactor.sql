SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsConsumptionConversion_SaveConversionFactor]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId               int
	@CountryId                 int               null
	@CountryName               varchar(200)
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec cbmsConsumptionConversion_SaveConversionFactorX
	@MyAccountId = 1
	,@BaseUnitId = 4
	,@ConvertedUnitId =5
	,@ConversionFactor = 1.0 --Original value = 0.0000

exec cbmsConsumptionConversion_SaveConversionFactor
	@MyAccountId = 1
	,@BaseUnitId = 4
	,@ConvertedUnitId =5
	,@ConversionFactor = 0.000000 --Original value = 0.0000


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE PROCEDURE [dbo].[cbmsConsumptionConversion_SaveConversionFactor]
(
	@MyAccountId int
	,@BaseUnitId int
	,@ConvertedUnitId int
	,@ConversionFactor decimal(32,16)
)
AS



SET NOCOUNT ON
--set ansi_warnings off



declare @exists bit

if exists(select * from consumption_unit_conversion 
	where base_unit_id = @BaseUnitId
	and converted_unit_id = @ConvertedUnitId)
	set @exists = 1
else
	set @exists = 0

if @exists = 1
	begin
	update consumption_unit_conversion
	set conversion_factor = @ConversionFactor
	where base_unit_id = @BaseUnitId
	and converted_unit_id = @ConvertedUnitId
	end
else
	begin
	insert into consumption_unit_conversion
	(
		base_unit_id
		,converted_unit_id
		,conversion_factor
	)
	values	
	(
		@BaseUnitId
		,@ConvertedUnitId
		,@ConversionFactor
	)
	end

/*	Apply conversion factor for same units in opposite conversion	*/

declare @ReverseConversionFactor decimal(32,16)

--check for reverse conversion factor
if exists(select * from consumption_unit_conversion 
	where base_unit_id = @ConvertedUnitId
	and converted_unit_id = @BaseUnitId)
	set @exists = 1
else
	set @exists = 0

if @ConversionFactor = 0
	set @ReverseConversionFactor = 0
else
	set @ReverseConversionFactor = 1/@ConversionFactor

if @exists = 1
	begin
	update consumption_unit_conversion
	set conversion_factor = @ReverseConversionFactor
	where base_unit_id = @ConvertedUnitId
	and converted_unit_id = @BaseUnitId
	end
else
	begin
--found longstanding bug with this code that only occurs because of data mismatch
--2-26-08
--version in prod did not match what was in test major and got overwritten 
--some time in the fall of 2007
--the above checks to see if the reverse conversion factor exists
--if so then it updates
--otherwise it inserts.  The following insert was going into the sv_prod
--database instead of the local copy without checking it first which occurs below
	insert into dbo.consumption_unit_conversion
	(
		base_unit_id
		,converted_unit_id
		,conversion_factor
	)
	values	
	(
		@ConvertedUnitId
		,@BaseUnitId
		,@ReverseConversionFactor
	 )
	end
SET NOCOUNT OFF
SET ANSI_WARNINGS ON
GO
GRANT EXECUTE ON  [dbo].[cbmsConsumptionConversion_SaveConversionFactor] TO [CBMSApplication]
GO
