SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [dbo].[Charge_History_Del_For_Contract]  
     
DESCRIPTION: 

	To Delete Charge History associated with the given Contract Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Contract_Id	INT						

OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	BEGIN TRAN

		EXEC Charge_History_Del_For_Contract  13349
	
	ROLLBACK TRAN
	
	BEGIN TRAN
		EXEC Charge_History_Del_For_Contract  12435
	ROLLBACK TRAN

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			06/21/2010	Created
HG			05/25/2011	MAINT-488 fixes the code to remove unused table reference
							-- Removed the reference of obsolete tables ROLLUP_CHARGE, TEMPLAGE_CHARGE,VARIABLE_UNKNOWN_CHARGE and CHARGE_VALUE as these tables are dropped.

*/

CREATE PROCEDURE dbo.Charge_History_Del_For_Contract
	 @Contract_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	 @Additional_Charge_Id			INT
			,@Charge_Id						INT
			,@Charge_Master_Id				INT
			,@Charge_Season_Map_Id			INT
			,@Charge_Parent_Type_Id			INT
			,@Base_load_Id					INT
			,@Baseload_Details_Id			INT
			,@Billing_Determinant_Id		INT
			,@Billing_Determinant_Master_Id	INT
			,@Load_Profile_Specification_Id	INT
			,@Recalc_Header_Id				INT

	DECLARE @Additional_Charge_List TABLE(Additional_Charge_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Charge_List TABLE(Charge_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Charge_Master_List TABLE(Charge_Master_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Charge_Master_Sign_Map_List TABLE(Charge_Master_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Charge_Season_Map_List TABLE(Charge_Season_Map_Id INT PRIMARY KEY CLUSTERED)
	
	DECLARE @Base_Load_List TABLE (Base_load_Id	INT PRIMARY KEY CLUSTERED)		
	DECLARE @BaseLoad_Detail_List TABLE(Baseload_Details_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Billing_Determinant_List TABLE (Billing_Determinant_Id	INT PRIMARY KEY CLUSTERED)
	DECLARE @Billing_Determinant_Master_List TABLE (Billing_Determinant_Master_Id INT PRIMARY KEY CLUSTERED)
	DECLARE @Load_Profile_Specification_List TABLE (Load_Profile_Specification_Id INT)
	DECLARE @Recalc_Header_List TABLE (Recalc_Header_Id INT PRIMARY KEY CLUSTERED)
		
	SELECT 
		 @Charge_Parent_Type_Id = cm.Charge_Parent_Type_Id
	FROM 
		dbo.CHARGE_MASTER cm 
		JOIN dbo.ENTITY ent
			 ON cm.CHARGE_PARENT_TYPE_ID = ent.ENTITY_ID 
	WHERE 
		cm.Charge_Parent_Id = @Contract_Id
		AND ent.ENTITY_NAME = 'Contract'
		
	INSERT INTO @Additional_Charge_List(Additional_Charge_Id)
	SELECT
		ac.ADDITIONAL_CHARGE_ID
	FROM
		dbo.Charge_Master cm
		JOIN dbo.Additional_Charge ac
			 ON ac.charge_master_id = cm.charge_master_id
	WHERE
		 cm.Charge_Parent_Id = @Contract_Id
		 AND cm.CHARGE_PARENT_TYPE_ID = @Charge_Parent_Type_Id

    --- Charges Season Map
	INSERT INTO @Charge_Season_Map_List(Charge_Season_Map_Id)
	SELECT
         cs.Charge_Season_Map_Id
    FROM
        dbo.Charge_Master cm
        JOIN dbo.Charge c
             ON c.Charge_Master_Id = cm.Charge_Master_Id
        JOIN dbo.Charge_Season_Map cs
             ON cs.charge_id = c.charge_id
    WHERE
        cm.Charge_Parent_Id = @Contract_Id
		AND cm.CHARGE_PARENT_TYPE_ID = @Charge_Parent_Type_Id

	---Charges Master Sign Map
	INSERT INTO @Charge_Master_Sign_Map_List(Charge_Master_Id)
	SELECT
		sm.CHARGE_MASTER_ID
    FROM
       dbo.Charge_Master cm
       JOIN dbo.Charge_Master_Sign_Map sm
            ON sm.Charge_Master_Id = cm.Charge_Master_Id
    WHERE
		cm.Charge_Parent_Id = @Contract_Id
		AND cm.CHARGE_PARENT_TYPE_ID = @Charge_Parent_Type_Id
	GROUP BY
		sm.CHARGE_MASTER_ID

	---Charge
	INSERT INTO @Charge_List(Charge_Id)
	SELECT 
		c.CHARGE_ID
	FROM
		dbo.Charge_Master cm
		JOIN dbo.Charge c
		     ON c.Charge_Master_Id = cm.Charge_Master_Id
	WHERE
	   cm.Charge_Parent_Id = @Contract_Id
	   AND cm.Charge_Parent_Type_Id = @Charge_Parent_Type_Id
       
	---Charge Master
	INSERT INTO @Charge_Master_List(Charge_Master_Id)
	SELECT
	   Charge_Master_Id	  
	FROM
		dbo.Charge_Master 
	WHERE
		Charge_Parent_Id = @Contract_Id
		AND Charge_Parent_Type_Id = @Charge_Parent_Type_Id

	INSERT INTO @Base_Load_List(Base_load_Id)
	SELECT
		bd.Baseload_Id
	FROM
		dbo.Load_Profile_Specification lps
        JOIN dbo.Baseload_Details bd
			 ON bd.Load_Profile_Specification_Id = lps.Load_Profile_Specification_Id
    WHERE
		lps.Contract_id = @Contract_Id
	GROUP BY
		bd.Baseload_Id

	INSERT INTO @BaseLoad_Detail_List(Baseload_Details_Id)
	SELECT 
		bd.Baseload_Details_Id
    FROM
		dbo.Baseload_Details bd
		JOIN @Base_Load_List bdl
			ON bd.Baseload_Id = bdl.Base_load_Id
		
	INSERT INTO @Billing_Determinant_List(Billing_Determinant_Id)
	SELECT
		bd.Billing_Determinant_Id
	FROM	
		dbo.Billing_Determinant_Master dm
		JOIN dbo.Billing_Determinant bd
			 ON bd.Billing_Determinant_Master_Id = dm.Billing_Determinant_Master_Id
    WHERE
		dm.Bd_Parent_Id = @Contract_Id
		AND dm.Bd_Parent_Type_Id = @Charge_Parent_Type_Id

	INSERT INTO @Billing_Determinant_Master_List(Billing_Determinant_Master_Id)
	SELECT
		Billing_Determinant_Master_Id
    FROM
		dbo.Billing_Determinant_Master
    WHERE
        Bd_Parent_Id = @Contract_Id
        AND Bd_Parent_Type_Id = @Charge_Parent_Type_Id
           
    INSERT INTO @Load_Profile_Specification_List(Load_Profile_Specification_Id)
	SELECT
        Load_Profile_Specification_Id
    FROM
        dbo.Load_Profile_Specification 
    WHERE
        Contract_Id = @Contract_Id
	
	INSERT INTO @Recalc_Header_List(Recalc_Header_Id)
	SELECT 
		Recalc_Header_Id
	FROM 
		dbo.Recalc_Header  
	WHERE 
		Contract_Id = @Contract_Id
        
	BEGIN TRY
		BEGIN TRAN 

			WHILE EXISTS(SELECT 1 FROM @Additional_Charge_List)
				BEGIN

					SET @Additional_Charge_Id = (SELECT TOP 1 Additional_Charge_Id FROM @Additional_Charge_List)

					EXEC dbo.Additional_Charge_Del @Additional_Charge_Id

					DELETE
						@Additional_Charge_List
					WHERE
						Additional_Charge_Id = @Additional_Charge_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Charge_Season_Map_List)
				BEGIN

					SET @Charge_Season_Map_Id = (SELECT TOP 1 Charge_Season_Map_Id FROM @Charge_Season_Map_List)

					EXEC dbo.Charge_Season_Map_Del @Charge_Season_Map_Id

					DELETE
						@Charge_Season_Map_List
					WHERE
						Charge_Season_Map_Id = @Charge_Season_Map_Id
				END

			WHILE EXISTS(SELECT 1 FROM @Charge_Master_Sign_Map_List)
				BEGIN

					SET @Charge_Master_Id = (SELECT TOP 1 Charge_Master_Id FROM @Charge_Master_Sign_Map_List)

					EXEC dbo.Charge_Master_Sign_Map_Del @Charge_Master_Id

					DELETE
						@Charge_Master_Sign_Map_List
					WHERE
						Charge_Master_Id = @Charge_Master_Id

				END


			WHILE EXISTS(SELECT 1 FROM @Charge_List)
				BEGIN

					SET @Charge_Id =(SELECT TOP 1 Charge_Id FROM @Charge_List)

					EXEC dbo.Charge_Del @Charge_Id

					DELETE
						@Charge_List
					WHERE
						Charge_Id = @Charge_Id

				END


			WHILE EXISTS(SELECT 1 FROM @Charge_Master_List)
				BEGIN

					SET @Charge_Master_Id =(SELECT TOP 1 Charge_Master_Id FROM @Charge_Master_List)

					EXEC dbo.Charge_Master_Del @Charge_Master_Id

					DELETE
						@Charge_Master_List
					WHERE
						Charge_Master_Id = @Charge_Master_Id

				END
				
			WHILE EXISTS(SELECT 1 FROM @BaseLoad_Detail_List)
				BEGIN

					SET @Baseload_Details_Id =(SELECT TOP 1 Baseload_Details_Id FROM @BaseLoad_Detail_List)

					EXEC dbo.Baseload_Details_Del @Baseload_Details_Id

					DELETE
						@BaseLoad_Detail_List
					WHERE
						Baseload_Details_Id = @Baseload_Details_Id

				END				
			
			WHILE EXISTS(SELECT 1 FROM @Base_Load_List)
				BEGIN

					SET @Base_load_Id =(SELECT TOP 1 Base_load_Id FROM @Base_Load_List)

					EXEC dbo.Baseload_Del @Base_load_Id

					DELETE
						@Base_Load_List
					WHERE
						Base_load_Id = @Base_load_Id

				END
				

			WHILE EXISTS(SELECT 1 FROM @Billing_Determinant_List)
				BEGIN

					SET @Billing_Determinant_Id =(SELECT TOP 1 Billing_Determinant_Id FROM @Billing_Determinant_List)

					EXEC dbo.Billing_Determinant_Del @Billing_Determinant_Id

					DELETE
						@Billing_Determinant_List
					WHERE
						Billing_Determinant_Id = @Billing_Determinant_Id

				END

			
			WHILE EXISTS(SELECT 1 FROM @Billing_Determinant_Master_List)
				BEGIN

					SET @Billing_Determinant_Master_Id =(SELECT TOP 1 Billing_Determinant_Master_Id FROM @Billing_Determinant_Master_List)

					EXEC dbo.Billing_Determinant_Master_Del @Billing_Determinant_Master_Id

					DELETE
						@Billing_Determinant_Master_List
					WHERE
						Billing_Determinant_Master_Id = @Billing_Determinant_Master_Id

				END

			WHILE EXISTS(SELECT 1 FROM @Load_Profile_Specification_List)
				BEGIN

					SET @Load_Profile_Specification_Id =(SELECT TOP 1 Load_Profile_Specification_Id FROM @Load_Profile_Specification_List)

					EXEC dbo.Load_Profile_Specification_Del @Load_Profile_Specification_Id

					DELETE
						@Load_Profile_Specification_List
					WHERE
						Load_Profile_Specification_Id = @Load_Profile_Specification_Id

				END
			
			WHILE EXISTS(SELECT 1 FROM @Recalc_Header_List)
				BEGIN

					SET @Recalc_Header_Id =(SELECT TOP 1 Recalc_Header_Id FROM @Recalc_Header_List)

					EXEC dbo.Recalc_Header_Del @Recalc_Header_Id

					DELETE
						@Recalc_Header_List
					WHERE
						Recalc_Header_Id = @Recalc_Header_Id

				END
				
			
		COMMIT TRAN
	END TRY
	BEGIN CATCH
	
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END
			
		EXEC dbo.usp_RethrowError

	END CATCH
END
GO
GRANT EXECUTE ON  [dbo].[Charge_History_Del_For_Contract] TO [CBMSApplication]
GO
