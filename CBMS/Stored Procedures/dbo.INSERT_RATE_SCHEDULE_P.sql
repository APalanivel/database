SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.INSERT_RATE_SCHEDULE_P
	@cbmsImageId INT,
	@rateId INT,
	@rsStartDate DATETIME,
	@rsEndDate DATETIME
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.rate_schedule (
		cbms_image_id,
		rate_id,
		rs_start_date,
		rs_end_date) 
	VALUES (@cbmsImageId,
		@rateId,
		@rsStartDate,
		@rsEndDate) 

END
GO
GRANT EXECUTE ON  [dbo].[INSERT_RATE_SCHEDULE_P] TO [CBMSApplication]
GO
