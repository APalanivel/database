SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********                      
NAME: Budget.Meter_Attributes_Account_Id_Meter_Id                        
                    
DESCRIPTION:                         
                    
 To get the meter_attributes based on the Account_id,Meter_Id For calculation tester.                    
                     
INPUT PARAMETERS:                        
Name     DataType  Default Description                        
------------------------------------------------------------                         
@Start_Dt  DATE                    
@End_Dt   DATE                    
@Account_Id     INT                          
@Meter_Id       INT    NULL         
@Is_Recalc  BIT    1 -- 1  for Recalc (Actual meter attributes) & 0 for Budget (Based on precedence rules the actual /forecasted attributes must be selected)                     
                    
OUTPUT PARAMETERS:                        
Name   DataType  Default Description                        
------------------------------------------------------------                        
                    
USAGE EXAMPLES:                        
------------------------------------------------------------                        
EXEC Budget.Meter_Attributes_Account_Id_Meter_Id @Start_Dt = '2001-01-01', @End_Dt = '2017-01-01',@Account_Id = 1024636,@Meter_ID = 6030                    
                    
                
EXEC Budget.Meter_Attributes_Account_Id_Meter_Id                
    @Start_Dt = '2020-01-01'                
    , @End_Dt = '2020-12-01'                
    , @Account_Id = 1024636                
    , @Meter_ID = 6030    
    , @Is_Recalc = 1    
                
AUTHOR INITIALS:                        
Initials Name                        
------------------------------------------------------------                        
PRG   Prasanna R Gachinmani             
SLP   Sri Lakshmi Pallikonda                  
                     
MODIFICATIONS                         
Initials Date  Modification                        
------------------------------------------------------------                        
 PRG     2019-04-09    Created                    
 SLP     2020-04-15    Updated logic to display the actual attributes for  @Is_Recalc=1          
           & display the forecasted and actual attributes for  @Is_Recalc=0        
 SLP	 2020-06-08  Included MA with Attribute_us='SYS'                    
******/

CREATE PROC [Budget].[Meter_Attributes_Account_Id_Meter_Id]
    (
        @Start_Dt DATE
        , @End_Dt DATE
        , @Account_Id INT
        , @Meter_ID INT = NULL
        , @Is_Recalc BIT = 1
    )
AS
    BEGIN

        DECLARE @dtFirstDayOfMonth DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, @Start_Dt), 0);

        DECLARE @Attribute_Use_ForSys VARCHAR(25);
        SELECT
            @Attribute_Use_ForSys = c.Code_Id
        FROM
            Code c
            JOIN dbo.Codeset AS c2
                ON c2.Codeset_Id = c.Codeset_Id
        WHERE
            c2.Codeset_Name = 'Attribute Use'
            AND c.Code_Value = 'For SYS';

        -- Get all Meter Attributes in Time Period                    
        CREATE TABLE #tmp_MeterAttributes
             (
                 EC_Meter_Attribute_Id INT
                 , EC_Meter_Attribute_Name NVARCHAR(400)
                 , EC_Meter_Attribute_Value NVARCHAR(510)
                 , Service_Month DATE
                 , Start_Dt DATE
                 , End_Dt DATE
                 , Meter_Attribute_Type_Cd INT
                 , Meter_Attribute_Type_Value VARCHAR(25)
                 , Attribute_Use_Cd VARCHAR(25)
             );

        --Get all months which to be compared                    
        CREATE TABLE #tbl_TimeFrame_Months
             (
                 MeterAttributeMonth DATE
             );

        --Insert all the months into table for evaluation                    
        INSERT INTO #tbl_TimeFrame_Months
             (
                 MeterAttributeMonth
             )
        SELECT
            dm.FIRST_DAY_OF_MONTH_D
        FROM
            meta.DATE_DIM dm
        WHERE
            dm.FIRST_DAY_OF_MONTH_D BETWEEN @dtFirstDayOfMonth
                                    AND     @End_Dt
        GROUP BY
            dm.FIRST_DAY_OF_MONTH_D;

        --Insert the values,into table by quering meter attributes in time frame period                    
        INSERT INTO #tmp_MeterAttributes
             (
                 EC_Meter_Attribute_Id
                 , EC_Meter_Attribute_Name
                 , EC_Meter_Attribute_Value
                 , Service_Month
                 , Start_Dt
                 , End_Dt
                 , Meter_Attribute_Type_Cd
                 , Meter_Attribute_Type_Value
                 , Attribute_Use_Cd
             )
        SELECT
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , MIN(ISNULL(emat.EC_Meter_Attribute_Value, emav.EC_Meter_Attribute_Value)) EC_Meter_Attribute_Value
            , CAST(dm.MeterAttributeMonth AS DATE) AS Service_Month
            , emat.Start_Dt
            , emat.End_Dt
            , emat.Meter_Attribute_Type_Cd
            , c.Code_Value
            , ema.Attribute_Use_Cd
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.EC_Meter_Attribute ema
                ON cha.Commodity_Id = ema.Commodity_Id
                   AND  ema.State_Id = cha.Meter_State_Id
            LEFT OUTER JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON ema.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                   AND  cha.Meter_Id = emat.Meter_Id
            LEFT OUTER JOIN dbo.EC_Meter_Attribute_Value emav
                ON ema.EC_Meter_Attribute_Id = emav.EC_Meter_Attribute_Id
            LEFT JOIN Code c
                ON c.Code_Id = emat.Meter_Attribute_Type_Cd
            INNER JOIN #tbl_TimeFrame_Months dm
                ON dm.MeterAttributeMonth BETWEEN emat.Start_Dt
                                          AND     ISNULL(emat.End_Dt, '2099-12-31')
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Meter_Id = @Meter_ID
        GROUP BY
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , CAST(dm.MeterAttributeMonth AS DATE)
            , emat.Start_Dt
            , emat.End_Dt
            , emat.Meter_Attribute_Type_Cd
            , c.Code_Value
            , ema.Attribute_Use_Cd;


        --SELECT * FROM #tmp_MeterAttributes AS tma        


        --@Is_Recalc = 0 for budget         
        -- Actual with close dated must be given precendence over forecasted with open ended        
        -- Forecasted with open ended over Actual with open ended        
        -- Actual with open ended if no forecasted exists        

        CREATE TABLE #Final_Output
             (
                 [EC_Meter_Attribute_Id] INT
                 , [EC_Meter_Attribute_Name] NVARCHAR(400)
                 , [EC_Meter_Attribute_Value] NVARCHAR(510)
                 , [Service_Month] DATE
                 , [Start_Dt] DATE
                 , [End_Dt] DATE
                 , [Meter_Attribute_Type_Value] VARCHAR(25)
             );
        INSERT INTO #Final_Output
             (
                 EC_Meter_Attribute_Id
                 , EC_Meter_Attribute_Name
                 , EC_Meter_Attribute_Value
                 , [Service_Month]
                 , Start_Dt
                 , End_Dt
                 , Meter_Attribute_Type_Value
             )
        SELECT
            EC_Meter_Attribute_Id
            , EC_Meter_Attribute_Name
            , EC_Meter_Attribute_Value
            , [Service_Month]
            , Start_Dt
            , End_Dt
            , Meter_Attribute_Type_Value
        FROM
            #tmp_MeterAttributes
        WHERE
            Meter_Attribute_Type_Value = 'Actual'
            AND Attribute_Use_Cd = @Attribute_Use_ForSys
            AND End_Dt IS NOT NULL -- Actual with closed ended         
            AND @Is_Recalc = 0;




        INSERT INTO #Final_Output
        SELECT
            EC_Meter_Attribute_Id
            , EC_Meter_Attribute_Name
            , EC_Meter_Attribute_Value
            , [Service_Month]
            , Start_Dt
            , End_Dt
            , Meter_Attribute_Type_Value
        FROM
            #tmp_MeterAttributes tmp
        WHERE
            Meter_Attribute_Type_Value = 'Forecasted'
            AND Attribute_Use_Cd = @Attribute_Use_ForSys
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Final_Output AS fo
                               WHERE
                                    fo.EC_Meter_Attribute_Id = tmp.EC_Meter_Attribute_Id
                                    AND fo.Service_Month = tmp.Service_Month)
            AND @Is_Recalc = 0;



        INSERT INTO #Final_Output
        SELECT
            EC_Meter_Attribute_Id
            , EC_Meter_Attribute_Name
            , EC_Meter_Attribute_Value
            , [Service_Month]
            , Start_Dt
            , End_Dt
            , Meter_Attribute_Type_Value
        FROM
            #tmp_MeterAttributes tmp
        WHERE
            Meter_Attribute_Type_Value = 'Actual'
            AND Attribute_Use_Cd = @Attribute_Use_ForSys
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Final_Output AS fo
                               WHERE
                                    fo.EC_Meter_Attribute_Id = tmp.EC_Meter_Attribute_Id
                                    AND fo.Service_Month = tmp.Service_Month)
            AND @Is_Recalc = 0;

        INSERT INTO #Final_Output
        SELECT
            EC_Meter_Attribute_Id
            , EC_Meter_Attribute_Name
            , EC_Meter_Attribute_Value
            , [Service_Month]
            , Start_Dt
            , End_Dt
            , Meter_Attribute_Type_Value
        FROM
            #tmp_MeterAttributes tmp
        WHERE
            Meter_Attribute_Type_Value = 'Actual'
            AND @Is_Recalc = 1;


        SELECT
            tmp.EC_Meter_Attribute_Id
            , dm.EC_Meter_Attribute_Name
            , tmp.EC_Meter_Attribute_Value
            , dm.FIRST_DAY_OF_MONTH_D AS SERVICE_MONTH
            , tmp.Start_Dt
            , tmp.End_Dt
            , tmp.Meter_Attribute_Type_Value
        FROM    (   SELECT
                        md.FIRST_DAY_OF_MONTH_D
                        , tmp.EC_Meter_Attribute_Name
                    FROM
                        meta.DATE_DIM md
                        CROSS JOIN #Final_Output tmp
                    WHERE
                        FIRST_DAY_OF_MONTH_D BETWEEN @dtFirstDayOfMonth
                                             AND     @End_Dt
                    GROUP BY
                        md.FIRST_DAY_OF_MONTH_D
                        , tmp.EC_Meter_Attribute_Name) dm
                LEFT OUTER JOIN #Final_Output AS tmp
                    ON tmp.Service_Month = dm.FIRST_DAY_OF_MONTH_D
                       AND tmp.EC_Meter_Attribute_Name = dm.EC_Meter_Attribute_Name
        ORDER BY
            dm.EC_Meter_Attribute_Name
            , SERVICE_MONTH;



        DROP TABLE
            #tmp_MeterAttributes
            , #tbl_TimeFrame_Months
            , #Final_Output;

    END;
GO
GRANT EXECUTE ON  [Budget].[Meter_Attributes_Account_Id_Meter_Id] TO [CBMSApplication]
GO
