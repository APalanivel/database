SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
		dbo.Site_Sel_By_Client_Country_Country  
  
DESCRIPTION:
		Selects the site name(combination city, state, address, actual sitename and country) for the given client.
 
INPUT PARAMETERS:
	Name				DataType		Default			Description    
---------------------------------------------------------------------   
	@Client_id			INT
	@division_id		INT				NULL
	@Keyword			VARCHAR(100)	NULL
	@StartRecordNumber	INT				1
	@EndRecordNumber	INT				2147483647
	@Country_Name		INT				NULL

OUTPUT PARAMETERS:    
	Name				DataType		Default		Description    
---------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------

	EXEC dbo.Site_Sel_By_Client_Country 11552
	EXEC dbo.Site_Sel_By_Client_Country 11554,null,'O2 World'
	EXEC dbo.Site_Sel_By_Client_Country 11554,null,'Berlin'
	
	EXEC dbo.Site_Ref_Number_Search_By_Client 235
	EXEC dbo.Site_Sel_By_Client_Country 1011,null,null,1,2147483647,'USA'
	
 
AUTHOR INITIALS:
	Initials	Name 
---------------------------------------------------------------------
	RR			Raghu Reddy
  
MODIFICATIONS  
   
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2014-11-27  Created
	
******/

CREATE PROCEDURE [dbo].[Site_Sel_By_Client_Country]
      ( 
       @Client_Id INT
      ,@division_id INT = NULL
      ,@Keyword VARCHAR(100) = NULL
      ,@StartRecordNumber INT = 1
      ,@EndRecordNumber INT = 2147483647
      ,@Country_Name VARCHAR(200) = NULL )
AS 
BEGIN    
       
      SET NOCOUNT ON;    

      WITH  CTE_Site
              AS ( SELECT TOP ( @EndRecordNumber )
                        ch.Site_Id
                       ,ch.Client_Hier_Id
                       ,rtrim(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_Name + ')' AS Site_name
                       ,ch.Sitegroup_Id AS Division_Id
                       ,row_number() OVER ( ORDER BY Site_Name ASC ) AS Record_Number
                       ,ch.Site_Reference_Number
                   FROM
                        Core.Client_Hier ch
                   WHERE
                        ch.Client_ID = @Client_Id
                        AND ch.Site_Id > 0
                        AND ( @division_id IS NULL
                              OR ch.Sitegroup_Id = @division_id )
                        AND ( @Keyword IS NULL
                              OR ch.Site_Name LIKE '%' + @Keyword + '%'
                              OR ch.State_Name LIKE '%' + @Keyword + '%'
                              OR ch.City LIKE '%' + @Keyword + '%' )
                        AND ( @Country_Name IS NULL
                              OR ch.Country_Name = @Country_Name ))
            SELECT
                  cs.Client_hier_id
                 ,cs.Site_id
                 ,cs.Site_name
                 ,cs.Division_id
                 ,cs.Site_Reference_Number
            FROM
                  CTE_Site AS cs
            WHERE
                  cs.Record_Number BETWEEN @StartRecordNumber
                                   AND     @EndRecordNumber
            ORDER BY
                  cs.Site_name   
    
END;
;
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Client_Country] TO [CBMSApplication]
GO
