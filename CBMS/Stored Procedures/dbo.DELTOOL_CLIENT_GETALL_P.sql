SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_CLIENT_GETALL_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@region_id     	int       	null      	
	@state_id      	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE           PROCEDURE DBO.DELTOOL_CLIENT_GETALL_P
	( @MyAccountId int 
	, @region_id int = null
	, @state_id int = null
	)
AS
BEGIN

	   select distinct c.client_id
		, c.client_name
		, sm.start_month
	     from client c
	     join vwClientFiscalYearStartMonth sm on sm.client_id = c.client_id
	 order by c.client_name

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_CLIENT_GETALL_P] TO [CBMSApplication]
GO
