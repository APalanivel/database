SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:  ACT_Commodity_Sel_commodity_Id  
  
DESCRIPTION:    
  
    
INPUT PARAMETERS:    
 Name				DataType		 Default   Description    
------------------------------------------------------------------------------    
@Commodity_Id			 INT       

  
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
 SET STATISTICS IO ON    
  EXEC ACT_Commodity_Sel_commodity_Id 290 
   
  
AUTHOR INITIALS:  
  
 Initials Name    
------------------------------------------------------------    
    RKV   Ravi Kumar vegesna  
    
MODIFICATIONS:    
 Initials	Date		Modification    
------------------------------------------------------------    
RKV			2019-07-24	MAINT-8913  Created  

******/
CREATE PROC [dbo].[ACT_Commodity_Sel_commodity_Id]
    (
        @Commodity_Id INT
    )
AS
    BEGIN


        SELECT
            ac.ACT_Commodity_XName
            , ac.ACT_Commodity_Id
        FROM
            dbo.Commodity_ACT_Commodity_Map cacm
            INNER JOIN dbo.ACT_Commodity ac
                ON ac.ACT_Commodity_Id = cacm.ACT_Commodity_Id
        WHERE
            cacm.Commodity_Id = @Commodity_Id;

    END;

    
GO
GRANT EXECUTE ON  [dbo].[ACT_Commodity_Sel_commodity_Id] TO [CBMSApplication]
GO
