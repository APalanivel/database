SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
      
CREATE PROCEDURE [dbo].[Bulk_Process_BatchId_Ins]      
 @Process_Id int,      
 @User_Id int      
AS      
BEGIN      
      
 declare @statusPending int      
 select  @statusPending = code_id           
  FROM              
  dbo.CODE cd              
   INNER JOIN dbo.Codeset cs               
   ON cs.Codeset_Id = cd.CodeSet_Id              
  WHERE cs.CodeSet_Name = 'Batch Status' and Code_Value = 'Pending'            
  AND Is_Active = 1         
      
      
 insert into logdb.dbo.XL_Bulk_Invoice_Process_Batch      
  ([XL_Bulk_Data_Process_Id]      
           ,[Status_Cd]      
           ,[Requested_User_Id]      
           ,[Requested_Ts]      
           ,[Last_Change_Ts]
		   ,[Processing_Step])      
     VALUES      
           (@Process_Id      
           ,@statusPending      
           ,@User_Id      
           ,getdate()      
           ,getdate()
		   ,'Charge update'    
     )      
      
      
 select @@IDENTITY as Batch_Id      
      
END      
GO
GRANT EXECUTE ON  [dbo].[Bulk_Process_BatchId_Ins] TO [CBMSApplication]
GO
