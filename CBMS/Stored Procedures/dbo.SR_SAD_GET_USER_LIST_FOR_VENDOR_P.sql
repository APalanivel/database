SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@username      	varchar(20)	          	
	@lastname      	varchar(20)	          	
	@firstname     	varchar(20)	          	
	@vendorid      	int       	          	
	@contactid     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',11,0,NULL,NULL,NULL
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P 'l','L','',0,0,4,NULL,291
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P 'l','L','',0,0,1,NULL,NULL
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P 'l','L','',0,0,NULL,NULL,NULL
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P 'l','L','',0,0,4,44,291
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P 'Anationalaccounts','','',0,0,NULL,NULL,NULL

	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3809,NULL,NULL,NULL,null 





EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P
      'Anationalaccounts'
     ,''
     ,''
     ,0
     ,0
     ,4
     ,NULL
     ,291;

-- As expected not returning results for EP
EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P
      'Anationalaccounts'
     ,''
     ,''
     ,0
     ,0
     ,4
     ,NULL
     ,290;
     
     
    EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3837,NULL,NULL,NULL,NULL
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3837,NULL,NULL,NULL,'102548,102549'
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3837,NULL,NULL,NULL,'102549'
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3837,NULL,NULL,NULL,'102548'
      
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3832,NULL,NULL,NULL,NULL 
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3832,NULL,NULL,NULL,'102548,102549'
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3832,NULL,NULL,NULL,'102549'
	EXEC dbo.SR_SAD_GET_USER_LIST_FOR_VENDOR_P '','','',0,3832,NULL,NULL,NULL,'102548'
	
	SELECT * FROM dbo.Code WHERE Code_Id IN (102548,102549)


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
	RR			2015-07-08	Global sourcing - Added new input parameters @Country_Id, @State_Id, @Commodity_Id
							-- Function used to get the comma separated state name replaced with OUTER APPLY query
	RR			2018-06-26	Global Risk Management - Added new input parameter @Contact_Type_Cd and added field Contact_Type_Cd to resultset
******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_USER_LIST_FOR_VENDOR_P]
      ( 
       @username VARCHAR(20)
      ,@lastname VARCHAR(20)
      ,@firstname VARCHAR(20)
      ,@vendorid INT
      ,@contactid INT
      ,@Country_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Commodity_Id INT = NULL
      ,@Contact_Type_Cd VARCHAR(MAX) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      DECLARE @selectClause VARCHAR(7000);
      DECLARE @fromClause VARCHAR(7000);
      DECLARE @whereClause VARCHAR(7000);
      DECLARE @sqlStatement VARCHAR(8000);
      DECLARE @GroupByStatement VARCHAR(8000);
      DECLARE @Contact_Type_Input_Cnt INT
      
      SELECT
            @Contact_Type_Input_Cnt = COUNT(DISTINCT us.Segments)
      FROM
            dbo.ufn_split(@Contact_Type_Cd, ',') us
      WHERE
            us.Segments > 0

        --basic selectClause
      SET @selectClause = ' SELECT 
								map.SR_Supplier_Contact_Info_Id
								,v.Vendor_Name
								,u.Username
								,u.Last_Name
								,u.First_Name
								,u.Email_Address
								,LEFT(st.State_Name, LEN(st.State_Name)-1) AS State
								,NULL AS Country_Id
								,LEFT(cy.Country_Name, LEN(cy.Country_Name)-1) AS Country_Name
								,NULL AS Commodity_Type_Id
								,LEFT(com.Commodity_Name, LEN(com.Commodity_Name)-1) AS Commodity_Name
								,LEFT(ct.Contact_Type, LEN(ct.Contact_Type)-1) AS Contact_Type
								,COUNT(DISTINCT ssctm.Contact_Type_Cd) as Contact_Type_Cnt
								,' + CAST(@Contact_Type_Input_Cnt AS VARCHAR(10)) + ' as Contact_Type_Input_Cnt
								';

      SET @fromClause = ' FROM
								dbo.Vendor v 
								INNER JOIN dbo.SR_Supplier_Contact_Vendor_Map map 
									ON map.Vendor_id = v.Vendor_id
								INNER JOIN SR_Supplier_Contact_Info supcont 
									ON map.SR_Supplier_Contact_Info_Id = supcont.SR_Supplier_Contact_Info_Id
								INNER JOIN dbo.User_Info u
									ON supcont.User_Info_Id = u.User_Info_Id 																	 
								LEFT JOIN dbo.Sr_Supplier_Contact_Type_Map ssctm
								    ON supcont.SR_Supplier_Contact_Info_Id = ssctm.SR_Supplier_Contact_Info_Id	
								LEFT JOIN dbo.Sr_Supplier_Contact_Type_Map ssctm1
									ON supcont.SR_Supplier_Contact_Info_Id = ssctm1.SR_Supplier_Contact_Info_Id
								OUTER APPLY(
												SELECT
													s.State_Name + '', ''
												FROM 	
													dbo.SR_Supplier_Contact_Vendor_Map svm
													INNER JOIN dbo.State s
														ON s.State_Id = svm.State_Id
												WHERE 	
													svm.SR_Supplier_Contact_Info_Id = map.SR_Supplier_Contact_Info_Id 
													' + CASE WHEN @State_Id IS NOT NULL THEN ' AND s.State_Id = ' + STR(@State_Id)
                                                             ELSE ''
                                                        END + '
													' + CASE WHEN @Commodity_Id IS NOT NULL THEN ' AND svm.Commodity_Type_Id = ' + STR(@Commodity_Id)
                                                             ELSE ''
                                                        END + '                                                        
													' + CASE WHEN @Country_Id IS NOT NULL THEN ' AND svm.Country_Id = ' + STR(@Country_Id)
                                                             ELSE ''
                                                        END + '
												GROUP BY
													s.State_Name
												FOR
													XML PATH('''')
											) st (State_Name)
								OUTER APPLY(	
												SELECT
													cy.Country_Name + '', ''
												FROM 	
													dbo.SR_Supplier_Contact_Vendor_Map cvm
													INNER JOIN dbo.Country cy
														ON cy.Country_Id = cvm.Country_Id
												WHERE 	
													cvm.SR_Supplier_Contact_Info_Id = map.SR_Supplier_Contact_Info_Id
													' + CASE WHEN @Country_Id IS NOT NULL THEN ' AND cy.Country_Id = ' + STR(@Country_Id)
                                                             ELSE ''
                                                        END + '
													' + CASE WHEN @Commodity_Id IS NOT NULL THEN ' AND cvm.Commodity_Type_Id = ' + STR(@Commodity_Id)
                                                             ELSE ''
                                                        END + '
													' + CASE WHEN @State_Id IS NOT NULL THEN ' AND cvm.State_Id = ' + STR(@State_Id)
                                                             ELSE ''
                                                        END + '                                                                                                             
												GROUP BY
													cy.Country_Name
												FOR
													XML PATH('''')
											) cy ( Country_Name)
								OUTER APPLY(	
												SELECT
													c.Commodity_Name + '', ''
												FROM 	
													dbo.SR_Supplier_Contact_Vendor_Map vm
													INNER JOIN dbo.Commodity c
														ON c.Commodity_Id = vm.Commodity_Type_Id
												WHERE 	
													vm.SR_Supplier_Contact_Info_Id = map.SR_Supplier_Contact_Info_Id
													' + CASE WHEN @Commodity_Id IS NOT NULL THEN ' AND c.Commodity_Id = ' + STR(@Commodity_Id)
                                                             ELSE ''
                                                        END + '
													' + CASE WHEN @State_Id IS NOT NULL THEN ' AND vm.State_Id = ' + STR(@State_Id)
                                                             ELSE ''
                                                        END + '
													' + CASE WHEN @Country_Id IS NOT NULL THEN ' AND vm.Country_Id = ' + STR(@Country_Id)
                                                             ELSE ''
                                                        END + '                                                                                                              											
												GROUP BY
													c.Commodity_Name
												FOR
													XML PATH('''')
											) com ( Commodity_Name)

							OUTER APPLY(	
												SELECT
													cd.Code_Value + '', ''
												FROM 	
													dbo.Sr_Supplier_Contact_Type_Map sup_map
													INNER JOIN dbo.Code cd
														ON cd.Code_Id = sup_map.Contact_Type_Cd
												WHERE 	
													sup_map.SR_Supplier_Contact_Info_Id = supcont.SR_Supplier_Contact_Info_Id
												GROUP BY
													cd.Code_Value
												FOR
													XML PATH('''')
											) ct ( Contact_Type)

										';

        --basic whereClause
      SELECT
            @whereClause = ' WHERE 
								u.is_History <> 1 AND map.Is_Primary = 1 ';

      IF ( @vendorid <> 0
           AND @vendorid <> -1 ) 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + ' AND map.Vendor_id = ' + STR(@vendorid);
            END;

      IF ( @contactid > 0 ) 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + 'AND supcont.SR_Supplier_Contact_Info_Id = ' + STR(@contactid);
            END;
      IF ( @Contact_Type_Cd IS NOT NULL ) 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + 'AND EXISTS ( SELECT 									 
																		1 
																	FROM 
																		dbo.ufn_split(''' + @Contact_Type_Cd + ''', '','') us
																	WHERE us.Segments = ssctm1.Contact_Type_Cd)';



            END;

      IF ( @username IS NOT NULL
           AND @username <> '' ) 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + 'AND u.username like ''%' + @username + '%''';
            END;

      IF ( @lastname IS NOT NULL
           AND @lastname <> '' ) 
            BEGIN
                  SELECT
                        @whereClause = @whereClause + 'AND u.last_name like ''%' + @lastname + '%''';

            END;

      IF ( @firstname IS NOT NULL
           AND @firstname <> '' ) 
            BEGIN

                  SELECT
                        @whereClause = @whereClause + 'AND u.first_name like ''%' + @firstname + '%''';
            END;

      IF ( @Country_Id IS NOT NULL
           OR @State_Id IS NOT NULL
           OR @Commodity_Id IS NOT NULL ) 
            BEGIN

                  SET @whereClause = @whereClause + ' AND EXISTS( SELECT 
																		1 
																	FROM 
																		dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP f 
																	WHERE 
																		f.SR_SUPPLIER_CONTACT_INFO_ID = map.sr_supplier_contact_info_id ' + CASE WHEN @Country_Id IS NOT NULL THEN ' AND f.COUNTRY_ID = ' + STR(@Country_Id)
                                                                                                                                                 ELSE ''
                                                                                                                                            END;

                  SET @whereClause = @whereClause + CASE WHEN @State_Id IS NOT NULL THEN ' AND f.STATE_ID = ' + STR(@State_Id)
                                                         ELSE ''
                                                    END;

                  SET @whereClause = @whereClause + CASE WHEN @Commodity_Id IS NOT NULL THEN ' AND f.COMMODITY_TYPE_ID = ' + STR(@Commodity_Id)
                                                         ELSE ''
                                                    END;

                  SET @whereClause = @whereClause + ')';

            END;




      SET @GroupByStatement = '   GROUP BY  
								map.SR_Supplier_Contact_Info_Id
								,v.Vendor_Name
								,u.Username
								,u.Last_Name
								,u.First_Name
								,u.Email_Address
								,LEFT(st.State_Name, LEN(st.State_Name)-1) 								
								,LEFT(cy.Country_Name, LEN(cy.Country_Name)-1)							
								,LEFT(com.Commodity_Name, LEN(com.Commodity_Name)-1) 								
								,LEFT(ct.Contact_Type, LEN(ct.Contact_Type)-1) '
								
      IF ( @Contact_Type_Cd IS NOT NULL ) 
            SET @GroupByStatement = @GroupByStatement + '								
								HAVING COUNT(DISTINCT ssctm.Contact_Type_Cd) = ' + CAST(@Contact_Type_Input_Cnt AS VARCHAR(10));

      SELECT
            @sqlStatement = @selectClause + @fromClause + @whereClause + @GroupByStatement;

        --PRINT @sqlStatement;

      EXEC (@sqlStatement);

END;





GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_USER_LIST_FOR_VENDOR_P] TO [CBMSApplication]
GO
