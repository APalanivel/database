SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Get_Contract_By_SecurityRole

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Security_Role_ID   int       	          	
	@contract_id   		int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Get_Contract_By_SecurityRole 99, 46190
	EXEC dbo.dv2Contract_Get 112, 11231, 46190

SELECT TOP 10
      samm.*
FROM
      supplier_account_meter_map samm
      INNER JOIN account a ON a.account_id = samm.account_id
      INNER JOIN site s ON s.site_id = a.site_id
WHERE
      s.client_id = 11231

	  USE CBMS

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SKA		Shobhit Kumar Agrawal
 AKR        Ashok Kumar Raju
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
	        	4/28/2009	Autogenerated script
	SS        	9/30/2009	Replaced Contract's account_id with supplier_account_meter_map's account_id reference.
	HG			10/30/2009	Contract_Start_date and Contract_End_Date column renamed to Supplier_Account_begin_dt 
							and Supplier_account_End_Dt in DV2_Account_Site table.
							
	HG			11/12/2009	Procedure should get the contract specific start date and end date
							Code modified to get the values from contract table.
	SSR			04/08/2010	Replaced Entity(Entity_id) with Commodity(Commodity_id)								
							Replaced dv2_account_site,Site,Account,Vendor table  
							with dv2.client_account_sum,Added Group by Clause	
	SKA			09/02/2010	Created new sp to Replace dv2Contract_Get		
    HG			07/11/2011	Added Execute statement to grant the permission to application user
    AKR         2012-10-19  Added Heat_Rate, Retail_Adder and Wires_Cost to select list
	NR			2020-03-12  MAINT-10043 - Moved Sproc From DV to CBMS
******/

CREATE PROCEDURE [dbo].[Get_Contract_By_SecurityRole]
     (
         @Security_Role_ID INT
         , @Contract_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , com.Commodity_Name Commodity_Type
            , cha.Account_Vendor_Name Vendor_Name
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , ch.Site_name
            , c.contract_pricing_summary
            , c.Heat_Rate
            , c.Retail_Adder
            , c.Wires_Cost
        FROM
            Core.Client_Hier_Account cha
            JOIN dbo.Security_Role_Client_Hier srch
                ON srch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
            JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
        WHERE
            srch.Security_Role_Id = @Security_Role_ID
            AND c.CONTRACT_ID = @Contract_Id
        GROUP BY
            c.CONTRACT_ID
            , c.ED_CONTRACT_NUMBER
            , com.Commodity_Name
            , cha.Account_Vendor_Name
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
            , ch.Site_name
            , c.contract_pricing_summary
            , c.Heat_Rate
            , c.Retail_Adder
            , c.Wires_Cost
        ORDER BY
            ch.Site_name;
    END;
GO
GRANT EXECUTE ON  [dbo].[Get_Contract_By_SecurityRole] TO [CBMSApplication]
GO
