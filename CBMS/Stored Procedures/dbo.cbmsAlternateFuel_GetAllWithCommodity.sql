SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsAlternateFuel_GetAllWithCommodity

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE [dbo].[cbmsAlternateFuel_GetAllWithCommodity]
	( @MyAccountId int
	
	)
AS
BEGIN

	   select entity_id
		, entity_name = case entity_type
			when 157 then entity_description + ' ' + entity_name
			else entity_name
			end
		, entity_type
		, entity_description
	     from entity 
	    where entity_type in (116, 157)
	 order by case entity_type
			when 157 then entity_description + ' ' + entity_name
			else entity_name
			end

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAlternateFuel_GetAllWithCommodity] TO [CBMSApplication]
GO
