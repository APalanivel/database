
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsSsoDocument_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_document_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.cbmsSsoDocument_Get 49  , 1155


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DRG			Dhilu Raichal George


MODIFICATIONS

	Initials		Date				Modification
------------------------------------------------------------
	        		9/21/2010			Modify Quoted Identifier
	DRG				10-SEP-2014			modified to get category from new table 
*******************************************************************************/

CREATE PROCEDURE [dbo].[cbmsSsoDocument_Get]
      ( 
       @MyAccountId INT
      ,@sso_document_id INT )
AS 
BEGIN
  

      SELECT
            d.sso_document_id
           ,d.document_title
           ,d.document_description
           ,left(cat1.category, len(cat1.category) - 1) category_type_id
           ,left(cat.category, len(cat.category) - 1) category_type
           ,d.document_reference_date
           ,d.is_special
           ,d.cbms_image_id
           ,d.cem_homepage
           ,d.is_customreport
           ,img.cbms_doc_id
           ,img.cbms_image_size
           ,img.content_type
      FROM
            dbo.SSO_DOCUMENT d
            CROSS APPLY ( SELECT
                              cdc.Category_Name + ','
                          FROM
                              dbo.Client_Document_Category cdc
                              INNER  JOIN dbo.Client_Document_Category_map cdcm
                                    ON cdcm.Client_Document_Category_Id = cdc.Client_Document_Category_Id
                          WHERE
                              d.SSO_Document_ID = cdcm.SSO_Document_ID
            FOR
                          XML PATH('') ) cat ( category )
            CROSS APPLY ( SELECT
                              cast(cdcm.Client_Document_Category_Id AS VARCHAR(10)) + ','
                          FROM
                              dbo.Client_Document_Category_map cdcm
                          WHERE
                              d.SSO_Document_ID = cdcm.SSO_Document_ID
            FOR
                          XML PATH('') ) cat1 ( category )
            INNER JOIN dbo.cbms_image img
                  ON img.cbms_image_id = d.cbms_image_id
      WHERE
            d.sso_document_id = @sso_document_id



END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_Get] TO [CBMSApplication]
GO
