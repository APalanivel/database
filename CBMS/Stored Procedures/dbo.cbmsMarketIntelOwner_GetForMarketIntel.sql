SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      procedure [dbo].[cbmsMarketIntelOwner_GetForMarketIntel]
	( @MyAccountId int
	, @MarketIntelId int
	)
AS
BEGIN

	set nocount on
select mi.market_intel_id
	, mism.state_id
	, s.state_name
	, s.country_id
	, c.country_name
	from market_intel mi
	join market_intel_state_map mism on mism.market_intel_id = mi.market_intel_id
	join state s on s.state_id = mism.state_id
	join country c on c.country_id = s.country_id
	where mi.market_intel_id = @MarketIntelId
	order by c.country_name desc
		, s.state_name
	  

END
GO
GRANT EXECUTE ON  [dbo].[cbmsMarketIntelOwner_GetForMarketIntel] TO [CBMSApplication]
GO
