SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                  
/******    
                  
NAME: [dbo].[Group_Info_Exists]
                    
DESCRIPTION:                    
			 To Check weather Group_Info_Id exists or not                   
                    
INPUT PARAMETERS:    
                   
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
Group_Info_Id_List				VARCHAR(MAX) 
                    
OUTPUT PARAMETERS:      
                      
Name                              DataType            Default        Description    
---------------------------------------------------------------------------------------------------------------  
                    
USAGE EXAMPLES:                        
---------------------------------------------------------------------------------------------------------------                          
            
	 EXEC [dbo].[Group_Info_Exists] 123
	  
                   
AUTHOR INITIALS:    
                  
Initials                Name    
---------------------------------------------------------------------------------------------------------------  
SP                      Sandeep Pigilam      
                     
MODIFICATIONS:   
                    
Initials                Date            Modification  
---------------------------------------------------------------------------------------------------------------  
 SP                     2014-01-10      Created for RA Admin user management                  
                   
******/ 

               
CREATE PROCEDURE dbo.Group_Info_Exists
      ( 
       @Group_Info_Id_List VARCHAR(MAX) )
AS 
BEGIN                
      SET NOCOUNT ON;     
                
      DECLARE @Group_Info_Ids TABLE ( Group_Info_Id INT )              
              
              
      INSERT      INTO @Group_Info_Ids
                  ( 
                   Group_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id_List, ',') ufn  
                        

      SELECT
            MAX(CASE WHEN gi.Group_Info_Id IS NULL THEN 1
                     ELSE 0
                END) Is_Group_Info_Missing
      FROM
            @Group_Info_Ids te
            LEFT OUTER JOIN dbo.GROUP_INFO gi
                  ON gi.Group_Info_Id = te.Group_Info_Id
      GROUP BY
            Te.Group_Info_Id
END 


;
GO
GRANT EXECUTE ON  [dbo].[Group_Info_Exists] TO [CBMSApplication]
GO
