SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                            
NAME: dbo.invoice_collection_Account_Get_Config_id                             
                          
DESCRIPTION:                               
                          
 To get the Account config details                         
                           
INPUT PARAMETERS:                              
Name     DataType Default   Description                              
-------------------------------------------------------------------------                              
@Invoice_collection_Account_Get_Config_id      INT                               
                            
                          
OUTPUT PARAMETERS:                              
Name     DataType Default   Description                              
-------------------------------------------------------------------------                              
                          
USAGE EXAMPLES:                              
------------------------------------------------------------                              
         
 exec invoice_collection_Account_Get_Config_id  234                
                            
                          
AUTHOR INITIALS:                              
Initials Name                              
------------------------------------------------------------                              
PR   Pradip Rajuput                          
                         
                           
MODIFICATIONS                               
Initials Date  Modification                              
------------------------------------------------------------                              
PR   2020-03-31 Created                          
                           
******/
CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Frequency_Next_Config_Run_Date_upd]
    @invoice_Collection_Account_Config_Id INT
    , @Seq_No INT
    , @Next_Config_Run_Date DATE
    , @updated_by_user_id INT
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE
            dbo.Account_Invoice_Collection_Frequency
        SET
            Next_Config_Run_Date = @Next_Config_Run_Date
            , Last_Change_Ts = GETDATE()
            , Updated_User_Id = @updated_by_user_id
        WHERE
            Invoice_Collection_Account_Config_Id = @invoice_Collection_Account_Config_Id
            AND Seq_No = @Seq_No;

    END;

GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Frequency_Next_Config_Run_Date_upd] TO [CBMSApplication]
GO
