SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
            
 NAME: dbo.IC_Bots_Process_Ins			
            
 DESCRIPTION:            
    To Save the Bots Process names    
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
@IC_Bots_Process_Name				VARCHAR(200)
@IC_Bots_Process_Desc				NVARCHAR(MAX)		NULL
@User_Info_Id						INT  
     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          
BEGIN TRAN;

select
    *
from
    dbo.IC_Bots_Process
where
    IC_Bots_Process_Name = 'Hellow Welcome Bots'

EXEC IC_Bots_Process_Ins
    @IC_Bots_Process_Name = 'Hellow Welcome Bots'
    , @IC_Bots_Process_Desc = 'Small enahancement changes - Bots Process for IC'
	, @User_Info_Id = 49

select
    *
from
    dbo.IC_Bots_Process
where
    IC_Bots_Process_Name = 'Hellow Welcome Bots'


ROLLBACK TRAN;

 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2020-05-27      Created for SE2017-963          
           
******/


CREATE PROC [dbo].[IC_Bots_Process_Ins]
    (
        @IC_Bots_Process_Name VARCHAR(200)
        , @IC_Bots_Process_Desc NVARCHAR(MAX) = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        INSERT INTO dbo.IC_Bots_Process
             (
                 IC_Bots_Process_Name
                 , IC_Bots_Process_Desc
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @IC_Bots_Process_Name
            , @IC_Bots_Process_Desc
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.IC_Bots_Process ibp
                           WHERE
                                ibp.IC_Bots_Process_Name = @IC_Bots_Process_Name);




    END;

GO
GRANT EXECUTE ON  [dbo].[IC_Bots_Process_Ins] TO [CBMSApplication]
GO
