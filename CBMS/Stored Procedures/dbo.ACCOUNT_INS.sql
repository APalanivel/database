SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[DBO].[ACCOUNT_INS]

DESCRIPTION:
	INSERTS SUPPLIER ACCOUNT DETAILS INTO ACCOUNT TABLE - WHILE CREATING CONTRACT

INPUT PARAMETERS:
NAME				DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@CONTRACTID			INT
@ACCOUNT_NUMBER		VARCHAR(50)
@ACCOUNT_TYPE_ID	INT
@VENDORID			INT



OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@ACCOUNTID		INT OUT 

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN

declare Acc_Out int

[DBO].[ACCOUNT_INS] 
  @CONTRACTID =1
    , @ACCOUNT_NUMBER ='a'
    , @ACCOUNT_TYPE_ID =37
    , @VENDORID =1
    , @SERVICE_LEVEL_TYPE_ID = 1
    , @ACCOUNTID  = @Acc_Out out


	ROLLBACK TRAN
	

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------


MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
SS			10/05/2009	Reviewed
SS			08-OCT-09	Replaced contract's account_id with supplier_account_meter_map's account_id. 
SM			31-OCT-09	In select statement added one more parameter SUPPLIER_ACCOUNT_RECALC_TYPE_ID in else block  
HG			11-NOV-09	Removed the IF/THEN statement.
HG			11/05/2009	Contract.Contract_Recalc_Type_id and Account.Supplier_Account_Recalc_Type_id column relation moved to Code/Codeset from Entity
						Account.Supplier_Account_Recalc_Type_id renamed to Supplier_Account_Recalc_Type_Cd
SM			12/11/2009  Included service level type id as parameter
RR			09/14/2011	MAINT - 568 Changed the logic to populate Supplier_Account_Recalc_Type_Cd column
							-- Supplier_Account_Recalc_Type_Cd will be pulled from the default account of the contract if no account else contract Reacalc_Type_id value will be saved.
RKV         2018-08-14  MAINT-7669 Added Supplier_Account_Determinant_Source_Cd while inserting the data.
NR			2020-04-27	MAINT-10253 - Saved Service Level type as Blank entity for Suppler accounts.		

*/

CREATE PROCEDURE [dbo].[ACCOUNT_INS]
    @CONTRACTID INT
    , @ACCOUNT_NUMBER VARCHAR(50)
    , @ACCOUNT_TYPE_ID INT
    , @VENDORID INT
    , @SERVICE_LEVEL_TYPE_ID INT
    , @ACCOUNTID INT OUT
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Blank_Entity_Id INT
            , @Supplier_Account_Type_Id INT;

        SELECT
            @Blank_Entity_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = ' '
            AND e.ENTITY_TYPE = 708;

        SELECT
            @Supplier_Account_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_DESCRIPTION = 'Account Type';

        INSERT INTO dbo.ACCOUNT
             (
                 ACCOUNT_NUMBER
                 , VENDOR_ID
                 , ACCOUNT_TYPE_ID
                 , Supplier_Account_Begin_Dt
                 , Supplier_Account_End_Dt
                 , SERVICE_LEVEL_TYPE_ID
                 , Supplier_Account_Recalc_Type_Cd
                 , Supplier_Account_Determinant_Source_Cd
             )
        SELECT
            @ACCOUNT_NUMBER
            , @VENDORID
            , @ACCOUNT_TYPE_ID
            , CON.CONTRACT_START_DATE
            , CON.CONTRACT_END_DATE
            , CASE WHEN @ACCOUNT_TYPE_ID = @Supplier_Account_Type_Id THEN @Blank_Entity_Id
                  ELSE ISNULL(ACC.SERVICE_LEVEL_TYPE_ID, @SERVICE_LEVEL_TYPE_ID)
              END
            , ISNULL(ACC.Supplier_Account_Recalc_Type_Cd, CON.CONTRACT_RECALC_TYPE_ID)
            , ACC.Supplier_Account_Determinant_Source_Cd
        FROM
            dbo.CONTRACT CON
            LEFT JOIN(dbo.SUPPLIER_ACCOUNT_METER_MAP MAP
                      JOIN dbo.ACCOUNT ACC
                          ON ACC.ACCOUNT_ID = MAP.ACCOUNT_ID)
                ON MAP.Contract_ID = CON.CONTRACT_ID
                   AND  ACC.Supplier_Account_Is_Default_Setting = 1
        WHERE
            CON.CONTRACT_ID = @CONTRACTID
        GROUP BY
            CON.CONTRACT_START_DATE
            , CON.CONTRACT_END_DATE
            , CASE WHEN @ACCOUNT_TYPE_ID = @Supplier_Account_Type_Id THEN @Blank_Entity_Id
                  ELSE ISNULL(ACC.SERVICE_LEVEL_TYPE_ID, @SERVICE_LEVEL_TYPE_ID)
              END
            , ACC.Supplier_Account_Recalc_Type_Cd
            , CON.CONTRACT_RECALC_TYPE_ID
            , ACC.Supplier_Account_Determinant_Source_Cd;

        SET @ACCOUNTID = SCOPE_IDENTITY();

    END;


GO
GRANT EXECUTE ON  [dbo].[ACCOUNT_INS] TO [CBMSApplication]
GO
