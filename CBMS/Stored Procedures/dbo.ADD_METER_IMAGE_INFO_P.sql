SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE dbo.ADD_METER_IMAGE_INFO_P
	@cbmsImageID int,
	@desc varchar(500),
	@documentTypeID int,
	@meterID int
	as
	begin
		set nocount on

			INSERT INTO METER_CBMS_IMAGE_MAP (
							CBMS_IMAGE_ID,
							DESCRIPTION, 
							DOCUMENT_TYPE_ID, 
							METER_ID) 
			VALUES				(
							@cbmsImageID, 
							@desc, 
							@documentTypeID, 
							@meterID)	

	end





GO
GRANT EXECUTE ON  [dbo].[ADD_METER_IMAGE_INFO_P] TO [CBMSApplication]
GO
