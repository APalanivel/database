SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      PROCEDURE [dbo].[cbmsSsoProjectActivity_Get]
	( @MyAccountId int
	, @project_activity_id int
	
	)
AS
BEGIN

	  
	   select pa.sso_project_activity_id
		, pa.sso_project_id
		, pa.created_by_id
		, pa.activity_date
		, pa.activity_description
		, ui.first_name
		, ui.last_name
		, ui.first_name + ' ' + ui.last_name as full_name
	     from sso_project_activity pa
	     join user_info ui on pa.created_by_id = ui.user_info_id
	    where pa.sso_project_activity_id = @project_activity_id 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectActivity_Get] TO [CBMSApplication]
GO
