SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_DIVISION_SUPPLIER_MAP_P
	@division_id int
	AS
	begin
		set nocount on

		select	vendor_id 
		from	PREFER_NOT_TODO_BUSINESS 
		where	division_id = @division_id

	end
GO
GRANT EXECUTE ON  [dbo].[GET_DIVISION_SUPPLIER_MAP_P] TO [CBMSApplication]
GO
