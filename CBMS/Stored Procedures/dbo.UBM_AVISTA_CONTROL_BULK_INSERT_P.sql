SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.UBM_AVISTA_CONTROL_BULK_INSERT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dirName       	varchar(100)	          	
	@fileName      	varchar(100)	          	
	@server        	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.UBM_AVISTA_CONTROL_BULK_INSERT_P 'test','control.txt','nakula'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.	        	

******/

CREATE PROCEDURE dbo.UBM_AVISTA_CONTROL_BULK_INSERT_P
@dirName VARCHAR(100),
@fileName VARCHAR(100),
@server VARCHAR(100)
AS
BEGIN

	SET NOCOUNT ON ;
	
	DECLARE @bulkInsert VARCHAR(8000)
	
	SET @bulkInsert = 
		' BULK INSERT UBM_AVISTA_CONTROL ' +
		' FROM '+'''\\'+ @server +'\AvistaExtract\' + @dirName + '\' + @fileName+''' '+ 
		' WITH (MAXERRORS=0,tablock, FORMATFILE ='+'''C:\UBMFormatFiles\avista_data_control.fmt'') '

	EXEC(@bulkInsert)
	
END	
GO
GRANT EXECUTE ON  [dbo].[UBM_AVISTA_CONTROL_BULK_INSERT_P] TO [CBMSApplication]
GO
