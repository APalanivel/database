
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SITES_UNDER_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.SR_RFP_GET_SITES_UNDER_CLIENT_P 0,0,218
EXEC dbo.SR_RFP_GET_SITES_UNDER_CLIENT_P 0,0,11231
EXEC dbo.SR_RFP_GET_SITES_UNDER_CLIENT_P 0,0,12032

 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DMR			Deana Ritter
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010	Modified for Quoted_Identifier
 DMR		  01/21/2015	MAINT-3358 Modified to use Client_Hier table instead of legacy tables.


******/


CREATE PROCEDURE [dbo].[SR_RFP_GET_SITES_UNDER_CLIENT_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@clientId INT
AS
BEGIN
	
      SET NOCOUNT ON

      SELECT
            Ch.Site_Id
           ,Ch.Site_name
      FROM
            Core.Client_Hier Ch
      WHERE
            Ch.Client_Id = @clientId
            AND Ch.Site_Id > 0
            
      Order BY
            Ch.Site_name
           ,Ch.Site_Id
           
           
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SITES_UNDER_CLIENT_P] TO [CBMSApplication]
GO
