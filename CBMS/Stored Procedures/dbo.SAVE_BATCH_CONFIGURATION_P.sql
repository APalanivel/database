SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from report_list
CREATE  PROCEDURE dbo.SAVE_BATCH_CONFIGURATION_P

@userId varchar(10),
@sessionId varchar(20),
@masterId int,
@clientId int,
@reportName varchar(200),
@corporateReport int


as
	set nocount on
declare @reportId int

select @reportId=report_list_id from report_list where 
	report_name =@reportName

insert into RM_BATCH_CONFIGURATION 
	(
		RM_BATCH_CONFIGURATION_MASTER_ID,
	 	CLIENT_ID, REPORT_LIST_ID,
		CORPORATE_REPORT
	)
values(@masterId,@clientId,@reportId,@corporateReport)
GO
GRANT EXECUTE ON  [dbo].[SAVE_BATCH_CONFIGURATION_P] TO [CBMSApplication]
GO
