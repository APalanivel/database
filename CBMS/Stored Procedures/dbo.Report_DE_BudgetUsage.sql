SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
     dbo.Report_DE_BudgetUsage
           
DESCRIPTION:              
       
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------     
@budget_start_year			INT    
              
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
              
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                
     
 EXEC [dbo].[Report_DE_BudgetUsage] '2017'    
     
      
 AUTHOR INITIALS:            
           
 Initials              Name            
---------------------------------------------------------------------------------------------------------------                          
LEC					Lynn Cox        
    
      
 MODIFICATIONS:          
              
 Initials              Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 LEC                   2016-08-10      Created.    
*********/    
CREATE PROCEDURE [dbo].[Report_DE_BudgetUsage]
      (
       @budget_start_year INT )
AS
BEGIN    
      SET NOCOUNT ON;    
      
      DECLARE
            @Bucket_Master_Id INT
           ,@commodity_id INT
           ,@converted_unit_id INT;
      
      CREATE TABLE #Accounts
            (
             Client_Hier_Id INT --NOT NULL
            ,Account_Id INT --NOT NULL
            ,Account_Number VARCHAR(50) --NOT NULL
            ,Account_Vendor_Name VARCHAR(200) --NOT NULL PRIMARY KEY ( Client_Hier_Id, Account_Id, budget_start_year,budget_id,budget_month )
            ,budget_start_year INT
            ,budget_id INT
            ,budget_name VARCHAR(200)
            ,budget_month DATE
            ,commodity_name VARCHAR(200)
            ,TransportForBudgetMonth VARCHAR(5) );
                  
                  
      CREATE TABLE #Transport
            (
             account_id INT
            ,association_date DATE
            ,DISASSOCIATION_DATE DATE
            ,COMMODITY_TYPE_ID INT );


      CREATE TABLE #Cost_Usage_Account_Dtl
            (
             Client_Hier_Id INT
            ,Account_Id INT
            ,Service_Month DATE
            ,UOM_Type_Id INT
            ,Bucket_Value DECIMAL(28, 10) PRIMARY KEY ( Service_Month, Client_Hier_Id, Account_Id ) );
       
      
      SELECT
            @Bucket_Master_Id = bm.Bucket_Master_Id
           ,@commodity_id = com.Commodity_Id
           ,@converted_unit_id = bm.Default_Uom_Type_Id --12 kWh
            --SELECT *
      FROM
            dbo.Bucket_Master AS bm
            INNER JOIN Commodity com
                  ON com.Commodity_Id = bm.Commodity_Id
      WHERE
            com.Commodity_Name = 'electric power'
            AND bm.Bucket_Name = 'Total Usage';

      INSERT      #Transport
                  ( account_id
                  ,association_date
                  ,DISASSOCIATION_DATE
                  ,COMMODITY_TYPE_ID )
                  SELECT DISTINCT
                        m.ACCOUNT_ID
                       ,samm.METER_ASSOCIATION_DATE
                       ,samm.METER_DISASSOCIATION_DATE
                       ,c.COMMODITY_TYPE_ID
                  FROM
                        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                        JOIN METER m
                              ON m.METER_ID = samm.METER_ID
                        JOIN CONTRACT c
                              ON c.CONTRACT_ID = samm.Contract_ID
                        JOIN ACCOUNT a
                              ON a.ACCOUNT_ID = m.ACCOUNT_ID
                        JOIN ENTITY e
                              ON e.ENTITY_ID = a.SERVICE_LEVEL_TYPE_ID
                  WHERE
                        c.CONTRACT_TYPE_ID = 153
                        AND e.ENTITY_NAME IN ( 'a', 'b', 'c' ) -- User requirement to only pull A, B, C accounts
                        AND c.COMMODITY_TYPE_ID = @commodity_id;
      
      
      INSERT      #Accounts
                  ( Client_Hier_Id
                  ,Account_Id
                  ,Account_Number
                  ,Account_Vendor_Name
                  ,budget_start_year
                  ,budget_id
                  ,budget_name
                  ,budget_month
                  ,commodity_name
                  --,TransportForBudgetMonth
                   )
                  SELECT DISTINCT
                        cha.Client_Hier_Id
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Account_Vendor_Name
                       ,b.BUDGET_ID
                       ,b.BUDGET_START_YEAR
                       ,b.BUDGET_NAME
                       ,bd.MONTH_IDENTIFIER [budget_month]
                       ,com.Commodity_Name
                  FROM
                        Core.Client_Hier AS ch
                        INNER JOIN Core.Client_Hier_Account AS cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON ba.ACCOUNT_ID = cha.Account_Id
                        INNER JOIN BUDGET b
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN Commodity com
                              ON com.Commodity_Id = b.COMMODITY_TYPE_ID
                                 AND com.Commodity_Id = cha.Commodity_Id
                        INNER JOIN BUDGET_DETAILS bd
                              ON bd.BUDGET_ACCOUNT_ID = ba.BUDGET_ACCOUNT_ID
                  WHERE
                        cha.Commodity_Id = @commodity_id
                        AND cha.Account_Type = 'Utility'
                        AND b.BUDGET_START_YEAR = @budget_start_year
                        AND ba.IS_DELETED = 0;

            
      INSERT      #Cost_Usage_Account_Dtl
                  ( Client_Hier_Id
                  ,Account_Id
                  ,Service_Month
                  ,UOM_Type_Id
                  ,Bucket_Value )
                  SELECT DISTINCT
                        a.Client_Hier_Id
                       ,a.Account_Id
                       ,cuad.Service_Month
                       ,cuad.UOM_Type_Id
                       ,cuad.Bucket_Value * cnuc.CONVERSION_FACTOR
                  FROM
                        #Accounts AS a
                        INNER JOIN dbo.Cost_Usage_Account_Dtl AS cuad
                              ON a.Client_Hier_Id = cuad.Client_Hier_Id
                                 AND a.Account_Id = cuad.ACCOUNT_ID
                                 AND cuad.Bucket_Master_Id = 100991--@Bucket_Master_Id
                                 AND cuad.Service_Month BETWEEN DATEADD(YY, -3, a.budget_month)
                                                        AND     a.budget_month
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cnuc
                              ON cnuc.BASE_UNIT_ID = cuad.UOM_Type_Id
                                 AND cnuc.CONVERTED_UNIT_ID = @converted_unit_id;
                                 
                               
            
      SELECT DISTINCT
            ch.Client_Name [Client]
           ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' [Site]
           ,cha.Account_Number [Account Number]
           ,cha.Account_Id AS [Account ID]
           ,cha.Account_Vendor_Name [Utility]
           ,cha.commodity_name [Commodity]
           ,cha.budget_month AS Month
           ,CASE WHEN bd.BUDGET_USAGE IS NOT NULL THEN bd.BUDGET_USAGE
                 WHEN ( bu.VOLUME * conv.CONVERSION_FACTOR ) IS NOT NULL THEN CAST(( bu.VOLUME * conv.CONVERSION_FACTOR ) AS DECIMAL(32, 16))
                 WHEN cuad1.Bucket_Value IS NOT NULL THEN cuad1.Bucket_Value
                 WHEN cuad2.Bucket_Value IS NOT NULL THEN cuad2.Bucket_Value
                 WHEN cuad3.Bucket_Value IS NOT NULL THEN cuad3.Bucket_Value
            END AS Volume
           ,CASE WHEN bd.BUDGET_USAGE IS NOT NULL THEN 'Budget Details'
                 WHEN ( bu.VOLUME * conv.CONVERSION_FACTOR ) IS NOT NULL THEN entityVolume.ENTITY_NAME
                 WHEN cuad1.Bucket_Value IS NOT NULL THEN 'Last Year'
                 WHEN cuad2.Bucket_Value IS NOT NULL THEN '2 Years Ago'
                 WHEN cuad3.Bucket_Value IS NOT NULL THEN '3 Years Ago'
            END AS [Volume Type]
					 						--,[TransportForBudgetMonth] = CASE WHEN tc1.ACCOUNT_ID IS NOT NULL THEN 'Yes' ELSE 'No' end
           ,b.BUDGET_ID [Budget ID]
           ,b.BUDGET_START_YEAR [Budget Year]
           ,b.BUDGET_NAME [Budget Name]
           ,[Posted to RA] = CASE WHEN b.IS_POSTED_TO_DV = 1 THEN 'Yes'
                                  ELSE 'No'
                             END
      FROM
            #Accounts AS cha --CROSS JOIN meta.Date_Dim AS dd
            INNER JOIN Core.Client_Hier AS ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN ( dbo.BUDGET_ACCOUNT AS ba
                         INNER JOIN dbo.BUDGET_DETAILS bd
                              ON bd.BUDGET_ACCOUNT_ID = ba.BUDGET_ACCOUNT_ID
                         INNER JOIN BUDGET b
                              ON b.BUDGET_ID = ba.BUDGET_ID )
                  ON ba.ACCOUNT_ID = cha.Account_Id
                     AND bd.MONTH_IDENTIFIER = cha.budget_month
                     AND b.COMMODITY_TYPE_ID = @commodity_id
							--AND b.BUDGET_ID = cha.budget_id
            LEFT JOIN ( dbo.BUDGET_USAGE AS bu
                        INNER JOIN dbo.ENTITY entityVolume
                              ON entityVolume.ENTITY_ID = bu.BUDGET_USAGE_TYPE_ID
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION conv
                              ON conv.BASE_UNIT_ID = bu.VOLUME_UNIT_TYPE_ID
                                 AND conv.CONVERTED_UNIT_ID = @converted_unit_id )
                  --@unitTypeId )
                  ON bu.ACCOUNT_ID = cha.Account_Id
                     AND bu.MONTH_IDENTIFIER = cha.budget_month
                           --AND bu.MONTH_IDENTIFIER = dd.Date_D
                     AND bu.COMMODITY_TYPE_ID = @commodity_id
            LEFT JOIN #Cost_Usage_Account_Dtl AS cuad1
                  ON cuad1.Client_Hier_Id = cha.Client_Hier_Id
                     AND cuad1.Account_Id = cha.Account_Id
                     AND cuad1.Service_Month = DATEADD(yy, -1, cha.budget_month)
            LEFT JOIN #Cost_Usage_Account_Dtl AS cuad2
                  ON cuad2.Client_Hier_Id = cha.Client_Hier_Id
                     AND cuad2.Account_Id = cha.Account_Id
                     AND cuad2.Service_Month = DATEADD(yy, -2, cha.budget_month)
            LEFT JOIN #Cost_Usage_Account_Dtl AS cuad3
                  ON cuad3.Client_Hier_Id = cha.Client_Hier_Id
                     AND cuad3.Account_Id = cha.Account_Id
                     AND cuad3.Service_Month = DATEADD(yy, -3, cha.budget_month)
      WHERE
            b.BUDGET_NAME IS NOT NULL
            AND EXISTS ( SELECT
                              *
                         FROM
                              #Transport tc1
                              JOIN #Accounts a1
                                    ON a1.Account_Id = tc1.account_id
                         WHERE
                              a1.budget_month BETWEEN tc1.association_date
                                              AND     tc1.DISASSOCIATION_DATE
                              AND tc1.account_id = cha.Account_Id )
      ORDER BY
            [Budget ID]
           ,Client
           ,Site
           ,[Account Number];

      IF OBJECT_ID('tempdb..#Accounts') IS NOT NULL
            DROP TABLE #Accounts;
      IF OBJECT_ID('tempdb..#Cost_Usage_Account_Dtl') IS NOT NULL
            DROP TABLE #Cost_Usage_Account_Dtl;
			
      IF OBJECT_ID('tempdb..#transport') IS NOT NULL
            DROP TABLE #Transport;

END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_BudgetUsage] TO [CBMSApplication]
GO
