SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_LP_Interval_Data_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Load Profile Interval Data for Selected 
						SR RFP Load Profile Interval Data Id.
      
INPUT PARAMETERS:          
NAME					  DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_LP_Comment_Id	  INT	

   
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_LP_Interval_Data_Del  210
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR		    28-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_RFP_LP_Interval_Data_Del
   (
    @SR_RFP_LP_Interval_Data_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_RFP_LP_INTERVAL_DATA
	WHERE 
		SR_RFP_LP_INTERVAL_DATA_ID = @SR_RFP_LP_Interval_Data_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_Interval_Data_Del] TO [CBMSApplication]
GO
