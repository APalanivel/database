SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                       
/******        
                      
NAME: dbo.User_Info_Client_App_Access_Role_Map_Merge                      
                        
DESCRIPTION:                        
		To update the User Info Client App Access Role Map table.                        
                        
INPUT PARAMETERS:        
                       
Name                              DataType            Default        Description        
---------------------------------------------------------------------------------------------------------------      
@User_Info_Id                     INT              
@Client_App_Access_Role_Id        VARCHAR(MAX)        NULL      
@Assigned_User_Id                 INT                      
                        
OUTPUT PARAMETERS:          
                          
Name                              DataType            Default        Description        
---------------------------------------------------------------------------------------------------------------      
                        
USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                              

				BEGIN TRAN             
				SELECT          
					  *          
				FROM          
					  dbo.User_Info_Client_App_Access_Role_Map          
				WHERE          
					  User_Info_Id = 39116           
				                
				EXEC dbo.User_Info_Client_App_Access_Role_Map_Merge           
					  @User_Info_Id = 39116          
					 ,@Client_App_Access_Role_Id = '4'          
					 ,@Assigned_User_Id=39116         
				               
				SELECT          
					  *          
				FROM          
					  dbo.User_Info_Client_App_Access_Role_Map          
				WHERE          
					  User_Info_Id = 39116              
				ROLLBACK TRAN       
      
                       
AUTHOR INITIALS:        
                      
Initials                Name        
---------------------------------------------------------------------------------------------------------------      
SP                      Sandeep Pigilam          
                         
MODIFICATIONS:       
                        
Initials                Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2013-11-25      Created for RA Admin user management.                     
                       
******/                        
                      
CREATE PROCEDURE dbo.User_Info_Client_App_Access_Role_Map_Merge
      ( 
       @User_Info_Id INT
      ,@Client_App_Access_Role_Id VARCHAR(MAX) = NULL
      ,@Assigned_User_Id INT )
AS 
BEGIN 
                     
      SET NOCOUNT ON          
                      
      DECLARE @User_Info_Client_App_Access_Role_Map_List TABLE
            ( 
             User_Info_Id INT
            ,Client_App_Access_Role_Id INT )                    
                    
                    
      INSERT      INTO @User_Info_Client_App_Access_Role_Map_List
                  ( 
                   User_Info_Id
                  ,Client_App_Access_Role_Id )
                  SELECT
                        @User_Info_Id
                       ,ufn.Segments
                  FROM
                        dbo.ufn_split(@Client_App_Access_Role_Id, ',') ufn                 
             
      BEGIN TRY                          
            BEGIN TRANSACTION                           
                   
            MERGE INTO [dbo].[User_Info_Client_App_Access_Role_Map] AS tgt
                  USING 
                        ( SELECT
                              urr.User_Info_Id
                             ,urr.Client_App_Access_Role_Id
                          FROM
                              @User_Info_Client_App_Access_Role_Map_List urr ) AS src
                  ON tgt.User_Info_Id = src.User_Info_Id
                        AND tgt.Client_App_Access_Role_Id = src.Client_App_Access_Role_Id
                  WHEN NOT MATCHED BY SOURCE AND  tgt.User_Info_Id = @User_Info_Id
                        THEN                  
    DELETE
                  WHEN NOT MATCHED BY TARGET 
                        THEN                  
    INSERT
                              ( 
                               User_Info_Id
                              ,Client_App_Access_Role_Id
                              ,Assigned_User_Id
                              ,Assigned_Ts )
                         VALUES
                              ( 
                               src.User_Info_Id
                              ,src.Client_App_Access_Role_Id
                              ,@Assigned_User_Id
                              ,GETDATE() );      
                                   
            COMMIT TRANSACTION       
                                     
      END TRY                      
      BEGIN CATCH                      
            IF @@TRANCOUNT > 0 
                  BEGIN      
                        ROLLBACK TRANSACTION      
                  END                     
            EXEC dbo.usp_RethrowError                      
      END CATCH                                    
                             
END 



;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Client_App_Access_Role_Map_Merge] TO [CBMSApplication]
GO
