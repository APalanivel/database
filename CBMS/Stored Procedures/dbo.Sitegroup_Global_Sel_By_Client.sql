SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.Sitegroup_Global_Sel_By_Client                    
                      
Description:                      
        To get global site groups
                      
Input Parameters:                      
    Name            DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Client_Id      INT     
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
                      
Usage Examples:                          
--------------------------------------------------------------------------------
	
	EXEC dbo.Sitegroup_Global_Sel_By_Client 235    
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-20-07     Global Risk Management - Created                    
                     
******/       
CREATE PROCEDURE [dbo].[Sitegroup_Global_Sel_By_Client] ( @Client_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON;    
      
      
      SELECT
            ch.Sitegroup_Name
           ,ch.Sitegroup_Id
           ,cd.Code_Value AS Sitegroup_Type
      FROM
            core.Client_Hier ch
            INNER JOIN dbo.Code cd
                  ON ch.Sitegroup_Type_cd = cd.Code_Id
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.Site_Id = 0
            AND Sitegroup_Type_Name = 'Global'
      GROUP BY
            ch.Sitegroup_Name
           ,ch.Sitegroup_Id
           ,cd.Code_Value
           
           
END;
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_Global_Sel_By_Client] TO [CBMSApplication]
GO
