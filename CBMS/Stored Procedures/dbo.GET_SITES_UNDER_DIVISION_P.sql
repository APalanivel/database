
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.GET_SITES_UNDER_DIVISION_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------         	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES
------------------------------------------------------------

exec GET_SITES_UNDER_DIVISION_P -1,-1,62
exec GET_SITES_UNDER_DIVISION_P -1,-1,262
select * from site where division_id=357  order by site_name
select * from VWSITENAME where division_id=357  order by site_name
select * from division where division_id=357
select * from RM_ONBOARD_HEDGE_SETUP where division_id=357

	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaan Reddy
	
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2014-10-29	Added header.
							Replaced base tables and view(VWSITENAME) with client hier tables
								
******/

CREATE  PROCEDURE [dbo].[GET_SITES_UNDER_DIVISION_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@divisionId INT
AS 
BEGIN 
      SET NOCOUNT ON
      SELECT
            ch.SITE_ID
           ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
      FROM
            core.Client_Hier ch
            INNER JOIN RM_ONBOARD_HEDGE_SETUP hedge
                  ON ch.Site_Id = hedge.SITE_ID
      WHERE
            ch.Sitegroup_Id = @divisionId
            AND ch.Site_Closed = 0
            AND ch.Site_Not_Managed = 0
            AND hedge.INCLUDE_IN_REPORTS = 1
      ORDER BY
            SITE_NAME
END




;
GO

GRANT EXECUTE ON  [dbo].[GET_SITES_UNDER_DIVISION_P] TO [CBMSApplication]
GO
