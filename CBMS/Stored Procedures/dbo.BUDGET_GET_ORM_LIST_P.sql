
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.BUDGET_GET_ORM_LIST_P                     
                          
 DESCRIPTION:      
		                    
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @userId					        VARCHAR(10)  
 @sessionId							VARCHAR(20)					       
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
---------------------------------------------------------------------------------------------------------------      
   
  EXEC BUDGET_GET_ORM_LIST_P 0,0             
	          
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
---------------------------------------------------------------------------------------------------------------    
 NR                     2013-12-30      MAINT-5543 Added Header and include the Distribution Budgets in group.                     
                         
******/   
     
     
CREATE  PROCEDURE [dbo].[BUDGET_GET_ORM_LIST_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20) )
AS 
BEGIN  

      SET NOCOUNT ON      
      
      SELECT
            ui.USER_INFO_ID AS user_info_id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS USER_INFO_NAME
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                  ON ui.USER_INFO_ID = uigim.USER_INFO_ID
            INNER JOIN dbo.GROUP_INFO gi
                  ON uigim.GROUP_INFO_ID = gi.GROUP_INFO_ID
      WHERE
            gi.GROUP_NAME IN ( 'operations', 'Distribution Budgets' )
            AND ui.IS_HISTORY = 0
      ORDER BY
            ui.FIRST_NAME
      
            
END  
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_ORM_LIST_P] TO [CBMSApplication]
GO
