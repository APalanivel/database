SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Account_Comment_Sel_By_Account_Id

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Account_Id				INT
	@Comment_Type			VARCHAR(25)
	@Comment_User_Info_Id	INT
	@Comment_Dt				DATE
	@Comment_Text			VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM dbo.Account_Comment

	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'CommentType', @Code_Value = 'AccountComment'
	SELECT TOP 10 * FROM dbo.ACCOUNT

	BEGIN TRANSACTION
		EXEC dbo.Account_Comment_Ins 1, 'AccountComment', 16, 'Test_Account_Comment_Sel_By_Account_Id_Test'
		EXEC dbo.Account_Comment_Sel_By_Account_Id 1
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR       	2015-05-19	Created for AS400

******/
CREATE PROCEDURE [dbo].[Account_Comment_Sel_By_Account_Id] ( @Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            comm.Comment_Text
           ,comm.Comment_Dt
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Commented_By
      FROM
            dbo.Account_Comment acc
            INNER JOIN dbo.Comment comm
                  ON acc.Comment_Id = comm.Comment_ID
            INNER JOIN dbo.USER_INFO ui
                  ON comm.Comment_User_Info_Id = ui.USER_INFO_ID
      WHERE
            acc.Account_Id = @Account_Id
      
                  
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Comment_Sel_By_Account_Id] TO [CBMSApplication]
GO
