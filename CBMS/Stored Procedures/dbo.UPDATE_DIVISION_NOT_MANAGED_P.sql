SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--update_division_not_managed_p 1,'1',10

create    PROCEDURE dbo.UPDATE_DIVISION_NOT_MANAGED_P

@userId varchar(10),
@sessionId varchar(20),
@divisionId int


as

	
update site 
set 	not_managed=1,
	closed=1,
	closed_date=getdate()  
where division_id = @divisionId
	
	
update account 
set 	not_managed=1,
	not_expected=1,
	not_expected_date=getdate() 
where site_id in (select site_id from site where division_id = @divisionId)

update account 
set 	not_managed=1,
	not_expected=1,
	not_expected_date=getdate() 
where account_id in ( select distinct suppacc.ACCOUNT_ID  
	from 	meter met
	join supplier_account_meter_map map on map.meter_id = met.meter_id
	join account utilacc on met.account_id = utilacc.account_id 
	and  utilacc.site_id in(select site_id from site where division_id = @divisionId)
	join account suppacc on map.account_id = suppacc.account_id )




GO
GRANT EXECUTE ON  [dbo].[UPDATE_DIVISION_NOT_MANAGED_P] TO [CBMSApplication]
GO
