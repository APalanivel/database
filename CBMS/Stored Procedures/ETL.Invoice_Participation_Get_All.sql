SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Invoice_Participation_Get_All       

    
DESCRIPTION: 
	Gets All IP data From CBMS. This is uses by the IP ETL process to do a full laod          
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			09/28/2010	Created
*****/ 
CREATE PROCEDURE etl.Invoice_Participation_Get_All
AS 
BEGIN 

	SELECT 
		cha.Client_Hier_Id
		,ip.ACCOUNT_ID
		,cha.Commodity_Id
		,ip.SERVICE_MONTH
		,convert(int, ip.IS_EXPECTED) as Expected_Cnt
		,convert(int, ip.IS_RECEIVED) as Received_Cnt
	FROM
		INVOICE_PARTICIPATION ip 
		INNER JOIN Core.Client_Hier ch 
			ON ch.Site_Id = ip.SITE_ID
		INNER JOIN Core.Client_Hier_Account cha  
			ON ip.ACCOUNT_ID = cha.Account_Id
				AND cha.Client_Hier_Id = ch.Client_Hier_Id
	GROUP BY 
		cha.Client_Hier_Id
		,ip.ACCOUNT_ID
		,cha.Commodity_Id
		,ip.SERVICE_MONTH
		,ip.IS_EXPECTED
		,ip.IS_RECEIVED	
END

GO
GRANT EXECUTE ON  [ETL].[Invoice_Participation_Get_All] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Invoice_Participation_Get_All] TO [ETL_Execute]
GO
