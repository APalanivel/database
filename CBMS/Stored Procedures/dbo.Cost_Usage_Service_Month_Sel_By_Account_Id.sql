
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Cost_Usage_Service_Month_Sel_By_Account_Id]  
     
DESCRIPTION:

	To Get Service Month of Cost / Usage for Selected Account Id from both Cost_Usage and
	Cost_Usage_Account_Dtl

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@Account_Id		INT
@StartIndex		INT			1			
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Cost_Usage_Service_Month_Sel_By_Account_Id  440819
	EXEC Cost_Usage_Service_Month_Sel_By_Account_Id  116867 ,1,25

AUTHOR INITIALS:
INITIALS	  NAME
------------------------------------------------------------
PNR		  PANDARINATH
AP		  Athmaram Pabbathi

MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------     
PNR		25-MAY-10	    CREATED
AP		09/14/2011    Removed Cost_Usage table reference as a part of Addl Data changes
AP		10/03/2011    Removed duplicate CTE for row_num & total_rows calc and included in the main CTE and added group by in main CTE
*/

CREATE PROCEDURE dbo.Cost_Usage_Service_Month_Sel_By_Account_Id
      ( 
       @Account_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN
      SET NOCOUNT ON ;

      WITH  Cte_Service_Month_List
              AS ( SELECT
                        CUAD.Service_Month
                       ,row_number() OVER ( ORDER BY Service_Month ) Row_Num
                       ,count(1) OVER ( ) Total_Rows
                   FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                   WHERE
                        CUAD.Account_Id = @Account_Id
                   GROUP BY
                        CUAD.Service_Month)
            SELECT
                  CSML.Service_Month
                 ,CSML.Total_Rows
            FROM
                  Cte_Service_Month_List CSML
            WHERE
                  CSML.Row_Num BETWEEN @StartIndex AND @EndIndex
                  
END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Service_Month_Sel_By_Account_Id] TO [CBMSApplication]
GO
