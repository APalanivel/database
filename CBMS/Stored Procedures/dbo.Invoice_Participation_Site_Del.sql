SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Invoice_Participation_Site_Del]  
     
DESCRIPTION: 
	It Deletes Invoice Participation Site for Selected Invoice Participation Site Id.
      
INPUT PARAMETERS:          
	NAME							DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
	@Invoice_Participation_Site_Id	INT
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

    BEGIN TRAN
		EXEC Invoice_Participation_Site_Del  711636
	ROLLBACK TRAN
	
    
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
          
MODIFICATIONS:
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			27-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.Invoice_Participation_Site_Del
   (
    @Invoice_Participation_Site_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON ;
   
    DELETE 
   	FROM	
		dbo.INVOICE_PARTICIPATION_SITE
	WHERE 
		INVOICE_PARTICIPATION_SITE_ID = @Invoice_Participation_Site_Id
		
END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Site_Del] TO [CBMSApplication]
GO
