SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_UPDATE_ACCOUNT_BID_GROUP_STATUS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId                 varchar,
@sessionId              varchar,
@dueDate                datetime
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--select * from entity where entity_type=1029
--EXEC SR_RFP_GET_MIN_DUE_DATE_P 1,1

--SELECT * FROM SR_RFP_SEND_SUPPLIER

--select * from SR_RFP_SEND_SUPPLIER where  BID_STATUS_TYPE_ID IS NOT NULL



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_ACCOUNT_BID_GROUP_STATUS_P] 

@userId varchar,
@sessionId varchar,
@dueDate datetime

AS

	
set nocount on
DECLARE @rfpSendSupplierId int
DECLARE @accountGroupId int
DECLARE @isBidGroupId int
declare @expEntityId int
declare @entityName varchar(50)
declare @processEntityId varchar(50)

select @expEntityId = (Select ENTITY_ID FROM ENTITY WHERE ENTITY_NAME='Expired' AND entity_type=1029)
select @processEntityId = (Select ENTITY_ID FROM ENTITY WHERE ENTITY_NAME='Processing' AND entity_type=1029)

DECLARE UPDATE_ACC_BID_GRP_STATUS CURSOR FAST_FORWARD
	FOR 
		
		SELECT 
			SR_RFP_SEND_SUPPLIER_ID,
			SR_ACCOUNT_GROUP_ID,
			IS_BID_GROUP
		FROM
			SR_RFP_SEND_SUPPLIER
		WHERE
			SR_RFP_SEND_SUPPLIER.DUE_DATE<=@dueDate


	OPEN UPDATE_ACC_BID_GRP_STATUS
	FETCH NEXT FROM UPDATE_ACC_BID_GRP_STATUS INTO @rfpSendSupplierId,@accountGroupId,@isBidGroupId

	

	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
		
			select @entityName = 
			(
				Select ENTITY_NAME FROM ENTITY WHERE ENTITY_ID=
				(
				  SELECT BID_STATUS_TYPE_ID FROM SR_RFP_SEND_SUPPLIER 
				  WHERE SR_RFP_SEND_SUPPLIER_ID=@rfpSendSupplierId
				)
			)

			IF @entityName IS NOT NULL OR @entityName!=''

			BEGIN

				IF @entityName='Refresh' OR @entityName = 'Open'
	
				BEGIN
					--//
					if (select count(sr_rfp_sop_shortlist_id) from sr_rfp_sop_shortlist 
						where 	sr_account_group_id = @accountGroupId and is_bid_group = @isBidGroupId ) > 0
					begin	

						if (select count(sr_rfp_sop_shortlist_id) from sr_rfp_sop_shortlist 
							where 	sr_account_group_id = @accountGroupId and is_bid_group = @isBidGroupId  and revised_due_date <= getdate() ) > 0
						begin
					 
							update SR_RFP_SEND_SUPPLIER set BID_STATUS_TYPE_ID=@expEntityId 
							WHERE SR_RFP_SEND_SUPPLIER_ID=@rfpSendSupplierId
		
							if @isBidGroupId=0
							BEGIN
								update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@expEntityId 
								WHERE SR_RFP_ACCOUNT_ID=@accountGroupId
		
							END
							else if @isBidGroupId=1
							BEGIN
								update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@expEntityId 
								WHERE SR_RFP_BID_GROUP_ID=@accountGroupId
							
							END
						end
					end else
					begin --sec case

						update SR_RFP_SEND_SUPPLIER set BID_STATUS_TYPE_ID=@expEntityId 
						WHERE SR_RFP_SEND_SUPPLIER_ID=@rfpSendSupplierId
	
						if @isBidGroupId=0
						BEGIN
							update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@expEntityId 
							WHERE SR_RFP_ACCOUNT_ID=@accountGroupId
	
						END
						else if @isBidGroupId=1
						BEGIN
							update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@expEntityId 
							WHERE SR_RFP_BID_GROUP_ID=@accountGroupId
	
						END

					end -- sec case
					--//
	
				END

				ELSE IF @entityName = 'Bid Placed'
				BEGIN

					update SR_RFP_SEND_SUPPLIER set BID_STATUS_TYPE_ID=@processEntityId 
					WHERE SR_RFP_SEND_SUPPLIER_ID=@rfpSendSupplierId

					if @isBidGroupId=0
					BEGIN
						update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@processEntityId 
						WHERE SR_RFP_ACCOUNT_ID=@accountGroupId

					END
					else if @isBidGroupId=1
					BEGIN
						update SR_RFP_ACCOUNT set BID_STATUS_TYPE_ID=@processEntityId 
						WHERE SR_RFP_BID_GROUP_ID=@accountGroupId
						
					END

				END
	

			END

		END

	FETCH NEXT FROM UPDATE_ACC_BID_GRP_STATUS INTO @rfpSendSupplierId,@accountGroupId,@isBidGroupId
	END

	
	
	CLOSE UPDATE_ACC_BID_GRP_STATUS
	DEALLOCATE UPDATE_ACC_BID_GRP_STATUS
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_ACCOUNT_BID_GROUP_STATUS_P] TO [CBMSApplication]
GO
