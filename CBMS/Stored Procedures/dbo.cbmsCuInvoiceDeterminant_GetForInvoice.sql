
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.cbmsCuInvoiceDeterminant_GetForInvoice    
    
DESCRIPTION:    
    
Based on SortColumn have to do sorting, for cu_determinant_code have to use Right & Len as sorting on the basis of integer values not string    
    
INPUT PARAMETERS:    
 Name    DataType  Default      Description    
---------------------------------------------------------------------------                  
 @cu_invoice_id  int      
 @Sort_Column  VARCHAR(30)  'Cu_Invoice_Determinant_Id'     
 @account_id  INT    NULL       
     
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
 EXEC dbo.cbmsCuInvoiceDeterminant_GetForInvoice   
      123412  
     ,NULL  
     ,'cu_invoice_determinant_id'    
 EXEC dbo.cbmsCuInvoiceDeterminant_GetForInvoice   
      123412  
     ,1783  
     ,'Cu_Determinant_code'    
     
AUTHOR INITIALS:    
 Initials  Name    
------------------------------------------------------------    
 NK   Nageswara Rao Kosuri    
 SSR  Sharad Srivastava    
 NR   Narayana Reddy  
 RKV  Ravi Kumar Vegesna  
   
MODIFICATIONS    
    
 Initials Date  Modification    
------------------------------------------------------------    
   7/20/2009 Autogenerated script    
  NK  12/24/2009 Modified to incorporate account related determinants.     
      And also removed @MyAccountId parameter         
  NK  01/13/2010  Removed hardcoded UBM Id             
  SSR       01/29/2010 Replace bucket_type_id with bucket_master_id    
      Removed reference entity.entity_id with bucket_master.bucket_master_id    
  SSR  02/24/2010  Removed NOLOCK HINT     
  SKA  03/22/2010 Replaced Left join with Inner join for Commodity table    
  HG  03/29/2010 unused @account_id parameter removed    
  HG  03/30/2010 Result set sorted by COmmodity and bucket name as per the applicatino requirement.    
  HG  05/10/2010 New parameter @Sort_Column added to the procedure to sort the result sets based on page requirement.    
      Stage-3 application will pass 'Commodity_id' as @Sort_Column    
      Other pages (Edit manual invoices/Resolve to month/Resolve to Account) uses the default value set in the procedure.    
  HG  03/30/2010 Result set sorted by Commodity and bucket name as per the applicatin requirement.    
  SKA  04/23/2010 Given Alias Name in select Clause for map.Determinant_Value as DetValue    
      Added parameter @Account_ID,Added column name CU_INVOICE_DETERMINANT_ACCOUNT_ID in select Clause    
   04/28/2010 changed the order  by clause to cd.CU_Invoice_Determinant_Id          
 SKA  05/31/2010 Added one more case when condition in order by clause        
 SKA  06/03/2010 Added is null condition for DetValue columm, Converted map.Determinant_Value to varchar as cd.determinant_value is varchar column    
 DMR  09/10/2010 Modified for Quoted_Identifier    
 NR   2015-06-19 FOr AS400-Added cd.EC_Invoice_Sub_Bucket_Master_Id column in output list.  
 RKV  2015-11-02 AS400-PII Added Commodity_Id as Optional Parameter, If sub bucket is available for that item will show it under Bucket_type and   
      If sub bucket is not there then will show bucket name.  
      Added Ubm_Sub_Bucket_Code in select list  
           
    
******/    
CREATE PROCEDURE [dbo].[cbmsCuInvoiceDeterminant_GetForInvoice]  
      (   
       @cu_invoice_id INT  
      ,@account_id INT = NULL  
      ,@Sort_Column VARCHAR(30) = 'CU_Invoice_Determinant_Id'  
      ,@Commodity_Id INT = NULL )  
AS   
BEGIN    
    
      SET NOCOUNT ON    
     
      DECLARE @Ubm_Name VARCHAR(200)      
    
      SELECT  
            @Ubm_Name = u.ubm_name  
      FROM  
            dbo.CU_Invoice inv  
            JOIN dbo.UBM u  
                  ON u.ubm_id = inv.ubm_id  
      WHERE  
            inv.CU_Invoice_id = @CU_Invoice_Id    
    
      SELECT  
            cd.cu_invoice_determinant_id  
           ,cd.cu_invoice_id  
           ,cd.commodity_type_id  
           ,com.commodity_name commodity_type  
           ,cd.ubm_meter_number  
           ,cd.ubm_service_type_id  
           ,st.entity_name service_type  
           ,cd.ubm_service_code  
           ,cd.Bucket_Master_ID bucket_type_id  
           ,CASE WHEN cd.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name  
                 ELSE eisbm.Sub_Bucket_Name  
            END AS bucket_type  
           ,cd.ubm_bucket_code  
           ,cd.unit_of_measure_type_id  
           ,u.entity_name unit_of_measure_type  
           ,cd.ubm_unit_of_measure_code  
           ,CASE WHEN @Ubm_Name = 'AS400' THEN cd.ubm_meter_number  
                 ELSE cd.determinant_name  
            END AS determinant_name  
           ,cd.determinant_value  
           ,cd.ubm_invoice_details_id  
           ,cd.cu_determinant_code  
           ,map.ACCOUNT_ID  
           ,map.Determinant_Expression  
           ,ISNULL(CONVERT(VARCHAR(50), map.Determinant_Value), cd.determinant_value) DetValue  
           ,MAP.CU_INVOICE_DETERMINANT_ACCOUNT_ID  
           ,cd.EC_Invoice_Sub_Bucket_Master_Id  
           ,cd.Ubm_Sub_Bucket_Code  
           ,eisbm.Sub_Bucket_Name
           ,bm.Bucket_Name
      FROM  
            dbo.CU_Invoice_Determinant cd  
            JOIN dbo.CU_Invoice_Determinant_Account MAP  
                  ON map.cu_invoice_determinant_id = cd.cu_invoice_determinant_id  
            JOIN dbo.Commodity com  
                  ON com.Commodity_Id = cd.commodity_type_id  
            LEFT OUTER JOIN dbo.Entity st  
                  ON st.Entity_Id = cd.ubm_service_type_id  
            LEFT OUTER JOIN dbo.Bucket_Master bm  
                  ON bm.Bucket_Master_Id = cd.Bucket_Master_ID  
            LEFT OUTER JOIN dbo.Entity u  
                  ON u.Entity_Id = cd.unit_of_measure_type_id  
            LEFT OUTER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm  
                  ON cd.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id  
      WHERE  
            cd.CU_Invoice_Id = @cu_invoice_id  
            AND ( @account_id IS NULL  
                  OR map.Account_Id = @account_id )  
            AND ( @Commodity_Id IS NULL  
                  OR com.Commodity_Id = @Commodity_Id )  
      ORDER BY  
            CASE WHEN @Sort_Column = 'CU_Invoice_Determinant_Id' THEN cd.cu_invoice_determinant_id  
                 WHEN @Sort_Column = 'Commodity_Id' THEN cd.commodity_type_id  
                 WHEN @Sort_Column = 'Cu_Determinant_code' THEN CONVERT(INT, RIGHT(cd.cu_determinant_code, ( LEN(cd.cu_determinant_code) - 1 )))  
            END    
    
END;  
  
;
GO


GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceDeterminant_GetForInvoice] TO [CBMSApplication]
GO
