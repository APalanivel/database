SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO




CREATE   PROCEDURE [dbo].[cbmsNYISO_Detail_GetAllForZoneAndYear]
	( @zone_id int
	, @year int
	, @pricing_type varchar(50)
	)
AS
BEGIN

	   select zd.zone_detail_id
		, zd.zone_id
		, zd.date
		, zd.ptid
		, zd.lbmp
		, zd.marginal_cost_losses
		, zd.marginal_cost_congestion
		, zd.pricing_type
		, zd.scarcity	
	     from ny_iso_detail zd
	    where zone_id = @zone_id
	      and datepart(yyyy,zd.date) = @year
	      and pricing_type = @pricing_type
	 order by date
	

END

GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_GetAllForZoneAndYear] TO [CBMSApplication]
GO
