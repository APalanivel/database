SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Report_DE_Invoices_Unprocessed]  
     
DESCRIPTION: 
	
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Client_ID			INT				Client Name
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Report_DE_Invoices_Unprocessed  '13225,180' --2823


     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
LEC			Lynn Cox

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
LEC		2016-08-11		CREATED
*/

CREATE PROCEDURE [dbo].[Report_DE_Invoices_Unprocessed]
      (
       @CLIENT_LIST VARCHAR(MAX) )
AS
BEGIN

      SET NOCOUNT ON;
   
    
      SELECT
            i.CU_INVOICE_ID [Cu Invoice ID]
           ,i.UBM_ACCOUNT_NUMBER [UBM Account Number]
           ,[Reported] = CASE WHEN i.IS_REPORTED = '1' THEN 'Yes'
                              ELSE 'No'
                         END
           ,[DNT] = CASE WHEN i.IS_DNT = '1' THEN 'Yes'
                         ELSE 'No'
                    END
           ,c.CLIENT_NAME [Client]
      FROM
            CBMS.dbo.CU_INVOICE i
            LEFT JOIN CLIENT c
                  ON c.CLIENT_ID = i.CLIENT_ID
            JOIN dbo.ufn_split(@CLIENT_LIST, ',') ufn_User_tmp
                  ON CAST(ufn_User_tmp.Segments AS INT) = c.CLIENT_ID
      WHERE
            i.IS_PROCESSED = 0;

END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Invoices_Unprocessed] TO [CBMSApplication]
GO
