
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: dbo.[Account_Details_Sel_By_Account_Id]      
         
DESCRIPTION:    
    
 To select all the account details information for the given account id while moving an account from one site to another    
 Instead of calling individual procedures application will call this procedure to get all the informations required for the page at once.    
    
INPUT PARAMETERS:    
NAME    DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
@Account_Id   INT          
@New_Client_Hier_Id INT    
@Start_Index  INT   1    
@End_Index   INT   2147483647    
    
OUTPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------      
    
 EXEC dbo.Account_Details_Sel_By_Account_Id  9    
     
AUTHOR INITIALS:    
INITIALS NAME              
------------------------------------------------------------              
CPE   Chaitanya Panduga Eswara    
    
MODIFICATIONS    
INITIALS DATE  MODIFICATION    
------------------------------------------------------------              
CPE   2014-02-01 Created    
AKR   2014-08-20 added sproc dbo.Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id    
*/    
CREATE PROC dbo.Account_Details_Sel_By_Account_Id  
      (   
       @Account_Id INT  
      ,@Start_Index INT = 1  
      ,@End_Index INT = 2147483647 )  
AS   
BEGIN    
      SET NOCOUNT ON    
    
   -- Utility Account Information    
      EXEC dbo.Account_Information_Sel_By_Account_Id   
            @Account_Id    
       
   -- Meter Information    
      EXEC dbo.Meter_Information_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Supplier Accounts belonging to multiple sites    
      EXEC dbo.Supplier_Accounts_Belonging_To_Multiple_Sites   
            @Account_Id    
    
   -- Invoices    
      EXEC dbo.CU_Invoice_Detail_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Cost and Usage    
      EXEC dbo.Cost_Usage_Dtl_Sel_By_Account_Id_For_Utility_And_Dependant_Supplier_Accounts   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Rate Comparision    
      EXEC dbo.Rate_Comparison_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Budgets    
      EXEC dbo.Budget_Id_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Contracts      
      EXEC dbo.Contract_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- RM Deal Tickets    
      EXEC dbo.RM_Deal_Ticket_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
     
   -- RFP    
      EXEC dbo.Sr_Rfp_Id_Sel_By_Account_Id   
            @Account_Id  
           ,@Start_Index  
           ,@End_Index    
    
   -- Group Accounts    
      EXEC dbo.Account_Group_Sel_By_Account_Id   
            @Account_Id    
     
   --To get shared Supplier Account that has data.  
     
      EXEC dbo.Shared_Supplier_Accounts_CU_Exists_Sel_By_Account_Id   
            @Account_Id  
    
END;
;
GO

GRANT EXECUTE ON  [dbo].[Account_Details_Sel_By_Account_Id] TO [CBMSApplication]
GO
