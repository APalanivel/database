SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Sel_By_Watch_List_Group_Info_Id                    
                  
Description:                  
			Showing Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @watch_List_Group_Info_Id  								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


EXEC dbo.Account_Invoice_Processing_Config_Sel_By_Watch_List_Group_Info_Id
    @Account_Id = 325423
    , @Watch_List_Group_Info_Id = 443


                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Sel_By_Watch_List_Group_Info_Id]
    (
        @Account_Id INT
        , @Watch_List_Group_Info_Id INT = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            aipc.Account_Invoice_Processing_Config_Id
            , aipc.Account_Id
            , aipc.Config_Start_Dt
            , aipc.Config_End_Dt
            , aipc.Watch_List_Group_Info_Id
            , aipc.Processing_Instruction_Category_Cd
            , Instruction_Category.Code_Value AS Instruction_Category
            , aipc.Processing_Instruction
        FROM
            dbo.Account_Invoice_Processing_Config aipc
            INNER JOIN dbo.Code Instruction_Category
                ON Instruction_Category.Code_Id = aipc.Processing_Instruction_Category_Cd
        WHERE
            (   (   @Watch_List_Group_Info_Id IS NULL
                    AND aipc.Watch_List_Group_Info_Id IS NOT NULL)
                OR  (   @Watch_List_Group_Info_Id IS NOT NULL
                        AND aipc.Watch_List_Group_Info_Id = @Watch_List_Group_Info_Id))
            AND aipc.Account_Id = @Account_Id
            AND aipc.Is_Active = 1
            AND (   (   @Start_Dt IS NULL
                        AND @End_Dt IS NULL)
                    OR  (   aipc.Config_Start_Dt BETWEEN @Start_Dt
                                                 AND     ISNULL(@End_Dt, '9999-12-31')
                            OR  ISNULL(aipc.Config_End_Dt, '9999-12-31') BETWEEN @Start_Dt
                                                                         AND     ISNULL(@End_Dt, '9999-12-31')
                            OR  @Start_Dt BETWEEN aipc.Config_Start_Dt
                                          AND     ISNULL(aipc.Config_End_Dt, '9999-12-31')
                            OR  ISNULL(@End_Dt, '9999-12-31') BETWEEN aipc.Config_Start_Dt
                                                              AND     ISNULL(aipc.Config_End_Dt, '9999-12-31')));


    END;




GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Sel_By_Watch_List_Group_Info_Id] TO [CBMSApplication]
GO
