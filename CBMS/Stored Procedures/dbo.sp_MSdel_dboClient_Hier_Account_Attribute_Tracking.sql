SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[sp_MSdel_dboClient_Hier_Account_Attribute_Tracking]
		@pkc1 int
as
begin  
	delete [dbo].[Client_Hier_Account_Attribute_Tracking]
where [Client_Hier_Account_Attribute_Tracking_Id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end  
GO
