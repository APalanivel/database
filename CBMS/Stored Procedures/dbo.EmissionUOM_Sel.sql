SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
dbo.EmissionUOM_Sel    

DESCRIPTION:    
Used to get UOM CD,code value from code and  Emmision_UOM_Display table  

INPUT PARAMETERS:    
Name		DataType	Default	Description    
------------------------------------------------------------    
@Code_Value VARCHAR(25) 

OUTPUT PARAMETERS:    
Name		DataType	Default	Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC dbo.EmissionUOM_Sel 'Default'
EXEC dbo.EmissionUOM_Sel 'Sustainability'

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date		Modification    
------------------------------------------------------------    
GB					Created  
SKA		28/07/2009	Modified the object as per the change in Schema 'EMMISION_UOM_DISPLAY'
SKA		17/08/2009	Modified as per coding standard
SKA		06/09/2009	Used Codeset_Name instead of std_column_name in code
CMH		09/15/2009	Repalced @Code_Value parameter with @Emmsion_Display_Source_name
					Removed lnk to Codeset
SKA		28-OCT-09	Added 'kg','metric tons' as a part of new enhancement					


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[EmissionUOM_Sel]    
  
AS     

BEGIN        
    
      SET NOCOUNT ON ;          
           
      SELECT    
            c.CODE_ID    
           ,c.CODE_VALUE    
      FROM    
            dbo.CODE c INNER JOIN CODESET cs ON c.CODESET_ID = cs.CODESET_ID   
      WHERE    
            c.CODE_VALUE in ('lbs','grams','kg','metric tons')   
            AND cs.STD_COLUMN_NAME='UOM_Cd'    
    
END
GO
GRANT EXECUTE ON  [dbo].[EmissionUOM_Sel] TO [CBMSApplication]
GO
