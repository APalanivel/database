SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_IMAGE_FROM_CBMS_IMAGE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@cbmsImageID   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE dbo.DELETE_IMAGE_FROM_CBMS_IMAGE_P
	@cbmsImageID INT
AS
BEGIN

	SET NOCOUNT ON
	
	DELETE FROM dbo.CBMS_IMAGE WHERE CBMS_IMAGE_ID = @cbmsImageID

END
GO
GRANT EXECUTE ON  [dbo].[DELETE_IMAGE_FROM_CBMS_IMAGE_P] TO [CBMSApplication]
GO
