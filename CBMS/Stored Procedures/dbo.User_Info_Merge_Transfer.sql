SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:  User_Info_Merge_Transfer      
          
DESCRIPTION:          
 Merges changes from User_Info Service to the User_Info table          
          
INPUT PARAMETERS:          
 Name     DataType   Default  Description          
-------------------------------------------------------------------------------------------------------    
 @@Message    XML        XML string of changes to the client heir table          
 ,@Conversation_Handle  UNIQUEIDENTIFER     Conversation Handle that sent the message           
          
OUTPUT PARAMETERS:          
 Name      DataType  Default Description          
------------------------------------------------------------------------------------------------------          
@Message_Batch_Count  INT    Returns Count of source Records         
          
          
USAGE EXAMPLES:          
------------------------------------------------------------------------------------------------------    
    
  This sp should be executed from SQL Server Service Broker   
          
AUTHOR INITIALS:          
 Initials Name          
------------------------------------------------------------------------------------------------------    
 DSC  Kaushik
 KVK  Vinay K
 RKV  Ravi Kumar Vegesna         
  
  
MODIFICATIONS          
          
 Initials Date   Modification          
------------------------------------------------------------------------------------------------------          
 DSC  02/03/2014  Created   
 KVK	02/17/2014	Removed Insert event becuase CBMS no insert happen in DV or SV       
 DSC	03/05/2014	Removed DELETE event becuase CBMS no DELETE happen in DV or SV  
 DSC	04/26/2014	Added Isnull condition on Last_Change_Ts column
 RKV    2018-04-17  SE2017-520 Added new column Has_Accepted_Data_Privacy_Agreement 
 KVK	02/19/2019	added new column Last_Login_Email_Reminder_Ts        
******/
CREATE PROCEDURE [dbo].[User_Info_Merge_Transfer]
(
    @Message XML,
    @Conversation_Handle UNIQUEIDENTIFIER
)
AS
BEGIN

    SET NOCOUNT ON;

    DECLARE @Idoc INT;

    EXEC sys.sp_xml_preparedocument @Idoc OUTPUT, @Message;


    DECLARE @User_Info_Changes TABLE
    (
        USER_INFO_ID INT NOT NULL,
        USERNAME VARCHAR(30) NULL,
        QUEUE_ID INT NULL,
        FIRST_NAME VARCHAR(40) NULL,
        MIDDLE_NAME VARCHAR(40) NULL,
        LAST_NAME VARCHAR(40) NULL,
        EMAIL_ADDRESS VARCHAR(150) NULL,
        IS_HISTORY BIT NULL,
        ACCESS_LEVEL INT NULL,
        CLIENT_ID INT NULL,
        DIVISION_ID INT NULL,
        SITE_ID INT NULL,
        ALLOW_AUTO BIT NULL,
        PHONE_NUMBER VARCHAR(30) NULL,
        IS_DEMO_USER BIT NULL,
        EXPIRATION_DATE DATETIME NULL,
        PROSPECT_NAME VARCHAR(300) NULL,
        CEM_HOMEPAGE BIT NULL,
        CELL_NUMBER VARCHAR(30) NULL,
        FAX_NUMBER VARCHAR(30) NULL,
        PHONE_NUMBER_EXT VARCHAR(10) NULL,
        address_1 VARCHAR(300) NULL,
        address_2 VARCHAR(300) NULL,
        state_id INT NULL,
        zipcode VARCHAR(10) NULL,
        country_id INT NULL,
        currency_unit_id INT NULL,
        locale_code VARCHAR(10) NULL,
        city VARCHAR(50) NULL,
        el_uom_id INT NULL,
        ng_uom_id INT NULL,
        Default_View INT NULL,
        sitesearch_viewedpromo BIT NULL,
        Failed_Login_Attempts SMALLINT NULL,
        New_User_EMail_Ts DATETIME NULL,
        Job_Title VARCHAR(255),
        Created_User_Id INT,
        Created_Ts DATETIME,
        Updated_User_Id INT,
        Op_Code CHAR(1) NULL,
        Last_Change_Ts DATETIME NULL,
        Has_Accepted_Data_Privacy_Agreement SMALLINT NULL,
		Last_Login_Email_Reminder_Ts DATETIME
    );

    INSERT INTO @User_Info_Changes
    (
        USER_INFO_ID,
        USERNAME,
        QUEUE_ID,
        FIRST_NAME,
        MIDDLE_NAME,
        LAST_NAME,
        EMAIL_ADDRESS,
        IS_HISTORY,
        ACCESS_LEVEL,
        CLIENT_ID,
        DIVISION_ID,
        SITE_ID,
        ALLOW_AUTO,
        PHONE_NUMBER,
        IS_DEMO_USER,
        EXPIRATION_DATE,
        PROSPECT_NAME,
        CEM_HOMEPAGE,
        CELL_NUMBER,
        FAX_NUMBER,
        PHONE_NUMBER_EXT,
        address_1,
        address_2,
        state_id,
        zipcode,
        country_id,
        currency_unit_id,
        locale_code,
        city,
        el_uom_id,
        ng_uom_id,
        Default_View,
        sitesearch_viewedpromo,
        Failed_Login_Attempts,
        New_User_EMail_Ts,
        Job_Title,
        Created_User_Id,
        Created_Ts,
        Updated_User_Id,
        Op_Code,
        Last_Change_Ts,
        Has_Accepted_Data_Privacy_Agreement,
		Last_Login_Email_Reminder_Ts
    )
    SELECT USER_INFO_ID,
           USERNAME,
           QUEUE_ID,
           FIRST_NAME,
           MIDDLE_NAME,
           LAST_NAME,
           EMAIL_ADDRESS,
           IS_HISTORY,
           ACCESS_LEVEL,
           CLIENT_ID,
           DIVISION_ID,
           SITE_ID,
           ALLOW_AUTO,
           PHONE_NUMBER,
           IS_DEMO_USER,
           EXPIRATION_DATE,
           PROSPECT_NAME,
           CEM_HOMEPAGE,
           CELL_NUMBER,
           FAX_NUMBER,
           PHONE_NUMBER_EXT,
           address_1,
           address_2,
           state_id,
           zipcode,
           country_id,
           currency_unit_id,
           locale_code,
           city,
           el_uom_id,
           ng_uom_id,
           Default_View,
           sitesearch_viewedpromo,
           Failed_Login_Attempts,
           New_User_EMail_Ts,
           Job_Title,
           Created_User_Id,
           Created_Ts,
           Updated_User_Id,
           Op_Code,
           Last_Change_Ts,
           Has_Accepted_Data_Privacy_Agreement,
		   Last_Login_Email_Reminder_Ts
    FROM
        OPENXML(@Idoc, '/User_Info_Changes/User_Info_Change', 2)
        WITH
        (
            USER_INFO_ID INT,
            USERNAME VARCHAR(30),
            QUEUE_ID INT,
            FIRST_NAME VARCHAR(40),
            MIDDLE_NAME VARCHAR(40),
            LAST_NAME VARCHAR(40),
            EMAIL_ADDRESS VARCHAR(150),
            IS_HISTORY BIT,
            ACCESS_LEVEL INT,
            CLIENT_ID INT,
            DIVISION_ID INT,
            SITE_ID INT,
            ALLOW_AUTO BIT,
            PHONE_NUMBER VARCHAR(30),
            IS_DEMO_USER BIT,
            EXPIRATION_DATE DATETIME,
            PROSPECT_NAME VARCHAR(300),
            CEM_HOMEPAGE BIT,
            CELL_NUMBER VARCHAR(30),
            FAX_NUMBER VARCHAR(30),
            PHONE_NUMBER_EXT VARCHAR(10),
            address_1 VARCHAR(300),
            address_2 VARCHAR(300),
            state_id INT,
            zipcode VARCHAR(10),
            country_id INT,
            currency_unit_id INT,
            locale_code VARCHAR(10),
            city VARCHAR(50),
            el_uom_id INT,
            ng_uom_id INT,
            Default_View INT,
            sitesearch_viewedpromo BIT,
            Failed_Login_Attempts SMALLINT,
            New_User_EMail_Ts DATETIME,
            Job_Title VARCHAR(255),
            Created_User_Id INT,
            Created_Ts DATETIME,
            Updated_User_Id INT,
            Op_Code CHAR(1),
            Last_Change_Ts DATETIME,
            Has_Accepted_Data_Privacy_Agreement SMALLINT,
			Last_Login_Email_Reminder_Ts DATETIME
        );



    EXEC sys.sp_xml_removedocument @Idoc;



    UPDATE ui
    SET ui.USERNAME = uic.USERNAME,
        ui.QUEUE_ID = uic.QUEUE_ID,
        ui.FIRST_NAME = uic.FIRST_NAME,
        ui.MIDDLE_NAME = uic.MIDDLE_NAME,
        ui.LAST_NAME = uic.LAST_NAME,
        ui.EMAIL_ADDRESS = uic.EMAIL_ADDRESS,
        ui.IS_HISTORY = uic.IS_HISTORY,
        ui.ACCESS_LEVEL = uic.ACCESS_LEVEL,
        ui.CLIENT_ID = uic.CLIENT_ID,
        ui.DIVISION_ID = uic.DIVISION_ID,
        ui.SITE_ID = uic.SITE_ID,
        ui.ALLOW_AUTO = uic.ALLOW_AUTO,
        ui.PHONE_NUMBER = uic.PHONE_NUMBER,
        ui.IS_DEMO_USER = uic.IS_DEMO_USER,
        ui.EXPIRATION_DATE = uic.EXPIRATION_DATE,
        ui.PROSPECT_NAME = uic.PROSPECT_NAME,
        ui.CEM_HOMEPAGE = uic.CEM_HOMEPAGE,
        ui.CELL_NUMBER = uic.CELL_NUMBER,
        ui.FAX_NUMBER = uic.FAX_NUMBER,
        ui.PHONE_NUMBER_EXT = uic.PHONE_NUMBER_EXT,
        ui.address_1 = uic.address_1,
        ui.address_2 = uic.address_2,
        ui.state_id = uic.state_id,
        ui.zipcode = uic.zipcode,
        ui.country_id = uic.country_id,
        ui.currency_unit_id = uic.currency_unit_id,
        ui.locale_code = uic.locale_code,
        ui.city = uic.city,
        ui.el_uom_id = uic.el_uom_id,
        ui.ng_uom_id = uic.ng_uom_id,
        ui.Default_View = uic.Default_View,
        ui.sitesearch_viewedpromo = uic.sitesearch_viewedpromo,
        ui.Failed_Login_Attempts = uic.Failed_Login_Attempts,
        ui.New_User_EMail_Ts = uic.New_User_EMail_Ts,
        ui.Job_Title = uic.Job_Title,
        ui.Created_User_Id = uic.Created_User_Id,
        ui.Created_Ts = uic.Created_Ts,
        ui.Updated_User_Id = uic.Updated_User_Id,
        ui.Last_Change_Ts = uic.Last_Change_Ts,
        ui.Has_Accepted_Data_Privacy_Agreement = uic.Has_Accepted_Data_Privacy_Agreement,
		ui.Last_Login_Email_Reminder_Ts = uic.Last_Login_Email_Reminder_Ts
    FROM dbo.USER_INFO ui
        INNER JOIN @User_Info_Changes uic
            ON ui.USER_INFO_ID = uic.USER_INFO_ID
    WHERE uic.Op_Code = 'U'
          AND ISNULL(uic.Last_Change_Ts, '1900-01-01 00:00:00.000') >= ISNULL(
                                                                                 ui.Last_Change_Ts,
                                                                                 '1900-01-01 00:00:00.000'
                                                                             );


END;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Merge_Transfer] TO [sb_Execute]
GO
