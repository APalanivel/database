SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsUbmInvoice_GetProcessedForDuplicateTest2

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@ubm_id        	int       	          	
	@ubm_invoice_id	int       	          	
	@ubm_account_code	varchar(50)	null      	
	@cbms_doc_id   	varchar(255)	          	
	@begin_date    	datetime  	          	
	@end_date      	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE      procedure [dbo].[cbmsUbmInvoice_GetProcessedForDuplicateTest2]
	( @MyAccountId int 
	, @ubm_id int
	, @ubm_invoice_id int
	, @ubm_account_code varchar(50) = null
	, @cbms_doc_id varchar(255)
	, @begin_date datetime
	, @end_date datetime
	)
AS
BEGIN

	   select ml.ubm_id
		, i.ubm_invoice_id
		, i.ubm_account_id
		, i.currency ubm_currency_code
		, cu.cu_invoice_id
		, min(det.service_start_date) service_start_date
		, max(det.service_end_date) service_end_date
	     from ubm_invoice i  with (nolock)
--	     join cbms_image ci with (nolock) on ci.cbms_image_id = i.cbms_image_id
	     join ubm_batch_master_log ml with (nolock) on ml.ubm_batch_master_log_id = i.ubm_batch_master_log_id
		and i.ubm_invoice_id=@ubm_invoice_id
  left outer join ubm_invoice_details det with (nolock) on det.ubm_invoice_id = i.ubm_invoice_id
	     join cu_invoice cu with (nolock) on cu.ubm_invoice_id = i.ubm_invoice_id
	    where i.ubm_account_id = @ubm_account_code
--	      and ci.cbms_doc_id = @cbms_doc_id
	      and ml.ubm_id = @ubm_id
	      and i.is_processed = 1
	      and cu.is_duplicate = 0
	      and cu.is_dnt = 0
	      and i.ubm_invoice_id != @ubm_invoice_id
	      and i.ubm_account_id is not null
	 group by ml.ubm_id
		, i.ubm_invoice_id
		, i.ubm_account_id
		, i.currency
		, cu.cu_invoice_id
	   having min(det.service_start_date) = @begin_date
	      and max(det.service_end_date) = @end_date

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetProcessedForDuplicateTest2] TO [CBMSApplication]
GO
