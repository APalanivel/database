SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- exec BUDGET_GET_TRANSPORTATION_FOR_MULTIPLE_CONTRACTS_P '','', 49724, '04/01/2007', '03/01/2008'
CREATE          PROCEDURE dbo.BUDGET_GET_TRANSPORTATION_FOR_MULTIPLE_CONTRACTS_P
	@user_id varchar(10),
	@session_id varchar(10),
	--@contract_id int,
	@budgetaccountId int,
	@from_date datetime,
	@to_date datetime
	AS
	begin
		set nocount on

		select	 months.month_identifier,
			isnull(detail.volume,100) as volume,
			index_detail.index_detail_value,
			case when conv.conversion_factor > 0
			     then ((isnull(detail.adder,0) * curr_conv.conversion_factor)/conv.conversion_factor)
			     else 0
			end adder,
			detail.fuel,
			-1 as multiplier,
			detail.tax,
			0 as is_nymex,
			detail.budget_contract_budget_detail_id,
			vw.contract_id
		
		from	budget_account ba
			join budget_contract_vw vw on vw.account_id = ba.account_id
			and ba.budget_account_id = @budgetaccountId
			join budget_contract_budget contract_budget
			on contract_budget.contract_id = vw.contract_id
			join budget_contract_budget_months months 
			on months.budget_contract_budget_id = contract_budget.budget_contract_budget_id
			and months.month_identifier between @from_date and @to_date
			join budget_contract_budget_detail detail 
			on detail.budget_contract_budget_month_id = months.budget_contract_budget_month_id
			left join clearport_index_months index_months on index_months.clearport_index_id = detail.market_id
			and index_months.clearport_index_month = months.month_identifier
			left join clearport_index_detail index_detail 
			on index_detail.clearport_index_month_id = index_months.clearport_index_month_id
			and index_detail.index_detail_date = (select max(index_detail_date) from clearport_index_detail where clearport_index_month_id = index_detail.clearport_index_month_id),
			consumption_unit_conversion conv,
			currency_unit_conversion curr_conv

		where 	conv.base_unit_id = detail.volume_unit_type_id
			and conv.converted_unit_id = 25 --// MMBtu
			and curr_conv.base_unit_id = detail.currency_unit_id
			and curr_conv.converted_unit_id = 3 --// USD 
			and curr_conv.conversion_date = (select max(conversion_date) from currency_unit_conversion where base_unit_id = detail.currency_unit_id and converted_unit_id = 3 and currency_group_id = 3)
			and curr_conv.currency_group_id = 3
		group by months.month_identifier,
			detail.volume,
			index_detail.index_detail_value,
			conv.conversion_factor,
			detail.adder,
			curr_conv.conversion_factor,
			detail.fuel,
			detail.tax,
			detail.budget_contract_budget_detail_id,
			vw.contract_id
		
	end























GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_TRANSPORTATION_FOR_MULTIPLE_CONTRACTS_P] TO [CBMSApplication]
GO
