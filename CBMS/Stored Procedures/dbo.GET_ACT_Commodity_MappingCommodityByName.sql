SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [dbo].[GET_ACT_Commodity_MappingCommodityByName]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType  Default      Description    
---------------------------------------------------------------------------                  
@Commodity_Name		 VARCHAR(MAX)

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
  



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		    Naga Jyothi
   
MODIFICATIONS    
    
    
 Initials	Date		Modification    
---------------------------------------------------------------------------------------    
 NJ		   2019-07-06	 Created for SE2017-733 ACT Commodity Mapping within CBMS
******/
CREATE PROCEDURE [dbo].[GET_ACT_Commodity_MappingCommodityByName]
    (
        @Commodity_Name VARCHAR(MAX)
    )
AS
    BEGIN

        SET @Commodity_Name = ISNULL(@Commodity_Name, '');


        SET NOCOUNT ON;

        SELECT  DISTINCT
                C.Commodity_Id
                , C.Commodity_Name
        FROM
            Commodity C
        WHERE
            1 = (CASE WHEN @Commodity_Name = '' THEN 1
                     ELSE CASE WHEN CHARINDEX(@Commodity_Name, C.Commodity_Name) > 0 THEN 1
                              ELSE 0
                          END
                 END)
        ORDER BY
            C.Commodity_Name;

    END;

GO
GRANT EXECUTE ON  [dbo].[GET_ACT_Commodity_MappingCommodityByName] TO [CBMSApplication]
GO
