SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME:  
  dbo.cbmsSsoDocumentOwner_GetForProjectFlat
  
DESCRIPTION:
  
INPUT PARAMETERS:  
Name				DataType	Default Description  
------------------------------------------------------------
@sso_document_int	int			null
  
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
	
	EXEC dbo.cbmsSsoDocumentOwner_GetForProjectFlat

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
 CPE		Chaitanya Panduga Eshwar
  
MODIFICATIONS  
 Initials Date		 Modification  
------------------------------------------------------------  
 CPE	  03/28/2011 Replaced vwCbmsSsoDocumentOwnerFlat view with SSO_DOCUMENT_OWNER_MAP tables.
 
******/

CREATE PROCEDURE dbo.cbmsSsoDocumentOwner_GetForProjectFlat
( 
 @sso_document_id int )
AS 
BEGIN

      SELECT
            CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS owner_id
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS owner_name
           ,Ent.ENTITY_ID AS sso_owner_type_id
           ,Ent.ENTITY_NAME AS owner_type
           ,CH.Client_Id
           ,NULLIF(CH.Sitegroup_Id,0) AS division_id
           ,NULLIF(CH.Site_Id, 0) AS Site_Id
           ,CD.Display_Seq AS owner_sort
      from
            dbo.SSO_DOCUMENT_OWNER_MAP SDOM
            JOIN Core.Client_Hier CH
                  ON SDOM.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Code CD
                  ON CH.Hier_level_Cd = CD.Code_Id
            JOIN dbo.ENTITY Ent
                  ON CD.Code_Dsc = Ent.ENTITY_NAME
            JOIN dbo.Codeset CS
                  ON CD.Codeset_Id = CS.Codeset_Id
                     AND Ent.ENTITY_DESCRIPTION = 'SSO Owner Type'
                     AND CS.Codeset_Name = 'HierLevel'
      WHERE
            SDOM.SSO_DOCUMENT_ID = @sso_document_id
      ORDER BY
            owner_sort
           ,owner_name

END


GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocumentOwner_GetForProjectFlat] TO [CBMSApplication]
GO
