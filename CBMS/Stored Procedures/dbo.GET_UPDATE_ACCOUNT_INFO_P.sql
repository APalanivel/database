SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.GET_UPDATE_ACCOUNT_INFO_P    
DESCRIPTION: Updates information for a divsion    
    
INPUT PARAMETERS:        
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------        
@verdor_id                          integer,    
@site_id                            integer,    
@invoice_src_type_id                integer,    
@account_number                     varchar(50),    
@not_expected                       bit,    
@not_managed                        bit,    
@not_expected_date                  datetime,    
@account_id                         integer,    
@not_expected_by_id                 integer,    
@user_info_id                       integer,    
  
@serviceLevelId                     int,    
@eligibilityDate                    datetime,    
@classificationType                 int,    
@consolidatedBillingPostedToUtility bit    
@Is_Broker_Account                  BIT  
@Analyst_Mapping_Cd                 INT  
@Is_Level2_Recalc_Validation_Required	BIT
@Alternate_Account_Number			NVARCHAR(200)
                              
                               
OUTPUT PARAMETERS:             
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------        
    
    
USAGE EXAMPLES:    
---------------------------------------------------------------------------------    

Use cbms    
BEGIN TRAN
Select * from account where ACCOUNT_ID=108798
EXEC dbo.GET_UPDATE_ACCOUNT_INFO_P 
      @verdor_id = 11206
     ,@site_id = 549361
     ,@invoice_src_type_id = 35
     ,@account_number = '26920'
     ,@not_expected = 1
     ,@not_managed = 0
     ,@not_expected_date = '2008-01-01 00:00:00.000'
     ,@account_id = 1342210
     ,@not_expected_by_id = 15718
     ,@user_info_id = 15718
    
     ,@serviceLevelId = 861
     ,@eligibilityDate = NULL
    
     ,@watchListGroupInfoId = 0
     ,@isDataEntryOnly = 0
     ,@Is_Broker_Account = 1
     ,@Analyst_Mapping_Cd = 100530
     ,@Is_Level2_Recalc_Validation_Required = 0
     ,@Alternate_Account_Number = 'Alternate_Account_Number-123456123456789'
     Select * from account where ACCOUNT_ID=108798

ROLLBACK

BEGIN TRAN
EXEC dbo.GET_UPDATE_ACCOUNT_INFO_P 
      @verdor_id = 11206
     ,@site_id = 549361
     ,@invoice_src_type_id = 35
     ,@account_number = 'Tyson utility account 1'
     ,@not_expected = 0
     ,@not_managed = 0
     ,@not_expected_date = NULL
     ,@account_id = 1342210
     ,@not_expected_by_id = NULL
     ,@user_info_id = NULL
     
     ,@serviceLevelId = 860
     ,@eligibilityDate = NULL
    
     ,@watchListGroupInfoId = 0
     ,@isDataEntryOnly = 0
     ,@Is_Broker_Account = 0
     ,@Analyst_Mapping_Cd = 100496
     ,@Alternate_Account_Number = ''
     ,@Invoice_Collection_Is_Chased=1
     ,@Invoice_Collection_Is_Chased_Changed=1
	 , @ACCOUNT_TYPE_ID =37
	 ,@Is_Invoice_Posting_Blocked = 1
      
ROLLBACK     				

 AUTHOR INITIALS:    
 Initials		Name    
---------------------------------------------------------------------------------        
  DR			Deana Ritter    
  HG			Hari Hara Suthan  
  NK			Nageswara Rao Kosuri   
  AKR			Ashok Kumar Raju  
  SP			Sandeep Pigilam
  NR			Narayana Reddy
  RKV			Ravi Kumar Vegesna
  TRK			Ramakrishna Thummala
    
MODIFICATIONS    
    
 Initials	Date			Modification    
---------------------------------------------------------------------------------        
   DR		08/04/2009		Removed Linked Server Updates    
   HG		11/07/2009		Classification_type_id column moved to Contract table from account as it is specific to contract    
							Removed @classificationType input param and the column update in account table   
   NK		01/21/2010			Removed commented code and added Execute statement Account_Variance_Data_Entry_Only_Rules_Merge   
   NK		01/22/2010		execute statement changed From Account_Variance_Data_Entry_Only_Rules_Merge to Account_Variance_Data_Entry_Only_Rules_INS_UPD  
   NK		02/02/2010		Removed exec Account_Variance_Data_Entry_Only_Rules_INS_UPD  
   AKR		2012-09-27		Added Analyst_Mapping_Cd,Is_Broker_Account as a part of POCO  
   AKR		2012-10-09		Modified the Usage Example
   SP		2014-07-18		Data Operations Enhancements Phase III added Is_Level2_Recalc_Validation_Required Column in Update statement.
   RR		2015-07-20      For Global CBMS Sourcing - Added @Alternate_Account_Number as input parameter.				
   RKV      2015-09-21      Removed the parameter @Is_Level2_Recalc_Validation_Required as part of AS400-II 		
   SP		2016-11-24		Invoice tracking,Added  Invoice_Collection_Is_Chased,Invoice_Collection_Is_Chased_Changed  AS INPUT PARAM.  	
   NR		2017-03-07		Contract Place holder  - Removed  @consolidatedBillingPostedToUtility input parameter.
   NR		2019-04-03		Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.
   RKV		2019-10-21	    Included logic to make remove the acct config group when Account is not manged
   RKV		2019-12-30		D20-1762  REmoved ubm details from Parameters
   TRK		2020-02-03		Added @ACCOUNT_TYPE_ID & @Is_Invoice_Posting_Blocked for Invoice Blocking Process.
   RKV      2020-02-24      Modified the case statment while getting the data in to the variable @Is_Audit 
   NR		2020-04-27		MAINT-10253 - Saved Service Level type as Blank entity for Suppler accounts.		
 
******/

CREATE PROCEDURE [dbo].[GET_UPDATE_ACCOUNT_INFO_P]
    (
        @verdor_id INTEGER
        , @site_id INTEGER
        , @invoice_src_type_id INTEGER
        , @account_number VARCHAR(50)
        , @not_expected BIT
        , @not_managed BIT
        , @not_expected_date DATETIME
        , @account_id INTEGER
        , @not_expected_by_id INTEGER
        , @user_info_id INTEGER
        , @serviceLevelId INT
        , @eligibilityDate DATETIME
        , @isDataEntryOnly INT
        , @Is_Broker_Account BIT
        , @Analyst_Mapping_Cd INT
        , @Alternate_Account_Number NVARCHAR(200) = NULL
        , @Invoice_Collection_Is_Chased VARCHAR = NULL
        , @Invoice_Collection_Is_Chased_Changed BIT = NULL
        , @ACCOUNT_TYPE_ID INT
        , @Is_Invoice_Posting_Blocked BIT
    )
AS
    BEGIN



        SET NOCOUNT ON;

        DECLARE
            @Blank_Entity_Id INT
            , @Supplier_Account_Type_Id INT;

        SELECT
            @Blank_Entity_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = ' '
            AND e.ENTITY_TYPE = 708;

        SELECT
            @Supplier_Account_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_NAME = 'Supplier'
            AND e.ENTITY_DESCRIPTION = 'Account Type';



        DECLARE
            @Invoice_Collection_Is_Chased_Bit BIT
            , @Is_Audit BIT;

        EXEC cbmsAccount_UpdateParticipation
            93
            , @account_id
            , @not_expected
            , @not_expected_date
            , @not_managed;

        SELECT
            @Is_Audit = CASE WHEN A.Is_Invoice_Posting_Blocked <> @Is_Invoice_Posting_Blocked THEN 1
                            ELSE 0
                        END
        FROM
            dbo.ACCOUNT AS A
        WHERE
            A.ACCOUNT_ID = @account_id;

        UPDATE
            dbo.ACCOUNT
        SET
            VENDOR_ID = @verdor_id
            , SITE_ID = @site_id
            , INVOICE_SOURCE_TYPE_ID = @invoice_src_type_id
            , ACCOUNT_NUMBER = @account_number
            , NOT_EXPECTED = @not_expected
            , NOT_MANAGED = @not_managed
            , NOT_EXPECTED_DATE = @not_expected_date
            , NOT_EXPECTED_BY_ID = @not_expected_by_id
            , USER_INFO_ID = @user_info_id
            , SERVICE_LEVEL_TYPE_ID = CASE WHEN @Supplier_Account_Type_Id = @ACCOUNT_TYPE_ID
                                                AND @serviceLevelId IS NULL THEN @Blank_Entity_Id
                                          ELSE @serviceLevelId
                                      END
            , ELIGIBILITY_DATE = @eligibilityDate
            , Is_Data_Entry_Only = @isDataEntryOnly
            , Is_Broker_Account = @Is_Broker_Account
            , Analyst_Mapping_Cd = @Analyst_Mapping_Cd
            , Alternate_Account_Number = @Alternate_Account_Number
            , Is_Invoice_Posting_Blocked = @Is_Invoice_Posting_Blocked
        WHERE
            ACCOUNT_ID = @account_id
            AND ACCOUNT_TYPE_ID = @ACCOUNT_TYPE_ID;



        UPDATE
            icacgm
        SET
            icacgm.Invoice_Collection_Account_Config_Group_Id = NULL
            , Updated_User_Id = @user_info_id
            , Last_Change_Ts = GETDATE()
        FROM
            dbo.Invoice_Collection_Account_Config_Group_Map icacgm
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Invoice_Collection_Account_Config_Id = icacgm.Invoice_Collection_Account_Config_Id
        WHERE
            icac.Account_Id = @account_id
            AND @not_managed = 1;



        IF @Is_Audit = 1
            BEGIN

                IF NOT EXISTS (   SELECT
                                        1
                                  FROM
                                        dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                                  WHERE
                                        ACCOUNT_ID = @account_id)
                    BEGIN
                        INSERT INTO dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                             (
                                 ACCOUNT_ID
                                 , Created_User_Id
                                 , Created_Ts
                                 , Updated_User_Id
                                 , Last_Change_Ts
                             )
                        VALUES
                            (@account_id        -- ACCOUNT_ID - int
                             , @user_info_id    -- Created_User_Id - int
                             , GETDATE()        -- Created_Ts - datetime
                             , @user_info_id    -- Updated_User_Id - int
                             , GETDATE()        -- Last_Change_Ts - datetime
                            );
                    END;
            END;

        IF @Is_Audit = 1
            BEGIN
                IF EXISTS (   SELECT
                                    1
                              FROM
                                    dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                              WHERE
                                    ACCOUNT_ID = @account_id)
                    BEGIN
                        UPDATE
                            dbo.ACCOUNT_POSTING_BLOCKED_AUDIT_TRACKING
                        SET
                            Updated_User_Id = @user_info_id
                            , Last_Change_Ts = GETDATE()
                        WHERE
                            ACCOUNT_ID = @account_id;
                    END;
            END;
    END;




GO





GRANT EXECUTE ON  [dbo].[GET_UPDATE_ACCOUNT_INFO_P] TO [CBMSApplication]
GO
