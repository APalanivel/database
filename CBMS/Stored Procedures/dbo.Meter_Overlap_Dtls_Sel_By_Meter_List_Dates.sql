SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
                             
 NAME: dbo.Meter_Overlap_Dtls_Sel_By_Meter_List_Dates                         
                              
 DESCRIPTION:          
 Meter overlap check for missing contracts                    
                              
 INPUT PARAMETERS:          
                             
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------      
 @Contract_Id      INT        
     
     
                            
 OUTPUT PARAMETERS:          
                                   
 Name                               DataType          Default       Description          
-------------------------------------------------------------------------------------                                
 USAGE EXAMPLES:                                  
-------------------------------------------------------------------------------------                     
  use cbms  
EXEC dbo.Meter_Overlap_Dtls_Sel_By_Meter_List_Dates
    @Meter_List = '1231176,1215176'
    , @Supplier_Meter_Association_Date = '2020-01-01 00:00:00.000'
    , @Supplier_Meter_Disassociation_Date = '2020-05-05 00:00:00.000';

	EXEC [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_List_Dates] '994029', '2020-03-01','2021-02-28'

	exec Meter_Overlap_Dtls_Sel_By_Meter_List_Dates '1459666,1459667', '2019-01-01',null 
              
 AUTHOR INITIALS:        
           
 Initials                   Name          
-------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                                
                               
 MODIFICATIONS:        
                               
 Initials               Date            Modification        
-------------------------------------------------------------------------------------      
 NR                     2020-05-05      MAINT- Extended Contract. 
 RR						2020-05-07		GRMUER-107 -Added Contract type "utility" meter overlpap.
                           
                             
******/

CREATE PROC [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_List_Dates]
    (
        @Meter_List NVARCHAR(MAX)
        , @Supplier_Meter_Association_Date DATETIME
        , @Supplier_Meter_Disassociation_Date DATETIME = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            cha.Account_Id
            , cha.Account_Number
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.ufn_split(@Meter_List, ',') us
                ON cha.Meter_Id = us.Segments
        WHERE
            cha.Account_Type = 'Supplier'
            AND (   @Supplier_Meter_Association_Date BETWEEN cha.Supplier_Meter_Association_Date
                                                     AND     ISNULL(
                                                                 cha.Supplier_Meter_Disassociation_Date, '9999-12-31')
                    OR  ISNULL(@Supplier_Meter_Disassociation_Date, '9999-12-31') BETWEEN cha.Supplier_Meter_Association_Date
                                                                                  AND     ISNULL(
                                                                                              cha.Supplier_Meter_Disassociation_Date
                                                                                              , '9999-12-31')
                    OR  cha.Supplier_Meter_Association_Date BETWEEN @Supplier_Meter_Association_Date
                                                            AND     ISNULL(
                                                                        @Supplier_Meter_Disassociation_Date
                                                                        , '9999-12-31')
                    OR  ISNULL(cha.Supplier_Meter_Disassociation_Date, '9999-12-31') BETWEEN @Supplier_Meter_Association_Date
                                                                                     AND     ISNULL(
                                                                                                 @Supplier_Meter_Disassociation_Date
                                                                                                 , '9999-12-31'))
            AND (   cha.Supplier_Contract_ID = -1
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.CONTRACT c
                                        INNER JOIN dbo.ENTITY e
                                            ON e.ENTITY_ID = c.CONTRACT_TYPE_ID
                                   WHERE
                                        e.ENTITY_DESCRIPTION = 'Contract Type'
                                        AND e.ENTITY_NAME = 'Supplier'
                                        AND cha.Supplier_Contract_ID = c.CONTRACT_ID))
        GROUP BY
            cha.Account_Id
            , cha.Account_Number
            , cha.Account_Vendor_Id
            , cha.Account_Vendor_Name
            , cha.Supplier_Contract_ID
            , cha.Supplier_Account_Config_Id
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , cha.Meter_Id
            , cha.Meter_Number
            , cha.Supplier_Meter_Association_Date
            , cha.Supplier_Meter_Disassociation_Date;





    END;




GO
GRANT EXECUTE ON  [dbo].[Meter_Overlap_Dtls_Sel_By_Meter_List_Dates] TO [CBMSApplication]
GO
