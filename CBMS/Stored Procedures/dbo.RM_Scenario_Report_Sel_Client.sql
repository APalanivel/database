SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.RM_Scenario_Report_Sel_Client            
                        
 DESCRIPTION:                        
			To get the RM_Scenario_Report for client             
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Client_Id						INT                  
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [dbo].[RM_Scenario_Report_Sel_Client] 
      @Client_Id = 11278
     ,@Scenario_Name = 'scenario sample'     
          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-06       Created                
                       
******/   
       
                
CREATE PROCEDURE [dbo].[RM_Scenario_Report_Sel_Client] ( @Client_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      SELECT
            rsr.RM_Scenario_Report_Id
           ,rsr.Client_Id
           ,rsr.Scenario_Name
      FROM
            dbo.RM_Scenario_Report rsr
      WHERE
            Client_Id = @Client_Id                       
                                 
                       
END 
;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Report_Sel_Client] TO [CBMSApplication]
GO
