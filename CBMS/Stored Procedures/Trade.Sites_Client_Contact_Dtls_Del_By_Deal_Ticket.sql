SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Sites_Client_Contact_Dtls_Del_By_Deal_Ticket                      
                        
Description:                        
        
                        
Input Parameters:                        
    Name				DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Deal_Ticket_Id		INT
    @Client_Hier_Id		VARCHAR(MAX)	NULL  
                        
Output Parameters:                              
	Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Sites_Client_Contact_Dtls_Del_By_Deal_Ticket  299,1     
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
Modifications:                        
	Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          18-02-2019  Created   GRM             
                       
******/
CREATE PROCEDURE [Trade].[Sites_Client_Contact_Dtls_Del_By_Deal_Ticket]
    (
        @Deal_Ticket_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        ccch
        FROM
            Trade.Deal_Ticket_Client_Contact_Client_Hier ccch
            INNER JOIN Trade.Deal_Ticket_Client_Contact dtcc
                ON ccch.Deal_Ticket_Client_Contact_Id = dtcc.Deal_Ticket_Client_Contact_Id
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (EXISTS (   SELECT
                                1
                            FROM
                                dbo.ufn_split(@Client_Hier_Id, ',') ch
                            WHERE
                                CAST(ch.Segments AS INT) = ccch.Client_Hier_Id));

        DELETE
        dtcc
        FROM
            Trade.Deal_Ticket_Client_Contact dtcc
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Contact_Client_Hier ccch
                               WHERE
                                    ccch.Deal_Ticket_Client_Contact_Id = dtcc.Deal_Ticket_Client_Contact_Id);




    END;


GO
GRANT EXECUTE ON  [Trade].[Sites_Client_Contact_Dtls_Del_By_Deal_Ticket] TO [CBMSApplication]
GO
