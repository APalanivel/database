SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE  PROCEDURE dbo.SR_SAD_SAVE_DEAL_TICKET_RESPONSIBLE_FOR_NORMS_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@srDealTicketId int,
@utilityTypeId int,
@pipelineTypeId int


AS
	set nocount on
IF (select count(1) from SR_DT_RESPONSIBLE_FOR_NORMS where SR_DEAL_TICKET_ID = @srDealTicketId) = 0

BEGIN

	insert into SR_DT_RESPONSIBLE_FOR_NORMS
		(SR_DEAL_TICKET_ID, UTILITY_TYPE_ID, PIPELINE_TYPE_ID)
	
	values	(@srDealTicketId, @utilityTypeId, @pipelineTypeId)

END

ELSE

BEGIN

	update SR_DT_RESPONSIBLE_FOR_NORMS set UTILITY_TYPE_ID=@utilityTypeId, 
		PIPELINE_TYPE_ID=@pipelineTypeId

	where
		SR_DEAL_TICKET_ID=@srDealTicketId
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_DEAL_TICKET_RESPONSIBLE_FOR_NORMS_DETAILS_P] TO [CBMSApplication]
GO
