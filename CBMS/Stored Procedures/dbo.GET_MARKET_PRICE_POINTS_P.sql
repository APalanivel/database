SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  PROCEDURE dbo.GET_MARKET_PRICE_POINTS_P
@indexId int
AS
BEGIN
SELECT 	price_point.market_price_point_id,
	price_point.market_price_point 
FROM    market_price_point price_point 
WHERE   price_point.market_price_index_id = @indexId
END




GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_PRICE_POINTS_P] TO [CBMSApplication]
GO
