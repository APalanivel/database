
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Budget_Account_Queue_Ins_Upd

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@Budget_Id				INT
	@Budget_Account_Id		INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 8552
	SELECT * FROM dbo.Code WHERE Codeset_Id = 164
	BEGIN TRAN
		SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
		UPDATE dbo.BUDGET SET Analyst_Queue_Display_Cd = 100579 WHERE BUDGET_ID = 8552
		EXEC Budget_Account_Queue_Ins_Upd 8552
		SELECT * FROM dbo.BUDGET_ACCOUNT AS ba INNER JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 8552
	ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-05-26	SE2017-26 - Created
	RR			2017-06-20	MAINT-5491 - Modified the script to exclude rates reviewed & sourcing completed accounts
	
******/

CREATE PROCEDURE [dbo].[Budget_Account_Queue_Ins_Upd]
      ( 
       @Budget_Id INT
      ,@Budget_Account_Id INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;

      CREATE TABLE #Budget_Account_Queue
            ( 
             Budget_Account_Id INT
            ,Analyst_Type_Cd INT
            ,Queue_Id INT )
            
      CREATE TABLE #Budget_Account_Queue_To_Be_Deleted
            ( 
             Budget_Account_Id INT
            ,Analyst_Type_Cd INT )

      DECLARE
            @Rates_Queue_Cd INT
           ,@Both_Queue_Cd INT
           ,@Sourcing_Queue_Cd INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT
           ,@No_Queue_Cd INT
          

      DECLARE @Group_Legacy_Name TABLE
            ( 
             GROUP_INFO_ID INT
            ,GROUP_NAME VARCHAR(200)
            ,Legacy_Group_Name VARCHAR(200) )  
            
      INSERT      INTO @Group_Legacy_Name
                  ( 
                   GROUP_INFO_ID
                  ,GROUP_NAME
                  ,Legacy_Group_Name )
                  EXEC dbo.Group_Info_Sel_By_Group_Legacy 
                        @Group_Legacy_Name_Cd_Value = 'Regulated_Markets_Budgets'
                        
      SELECT
            @Rates_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates only'
  
      SELECT
            @Both_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Rates and Sourcing'
      
      SELECT
            @Sourcing_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'Yes - Sourcing only'
            
      SELECT
            @No_Queue_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND c.Code_Value = 'No'
            
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'

      DELETE
            baq
      FROM
            dbo.BUDGET b
            INNER JOIN dbo.BUDGET_ACCOUNT ba
                  ON b.BUDGET_ID = ba.BUDGET_ID
            INNER JOIN dbo.Budget_Account_Queue baq
                  ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
      WHERE
            ba.BUDGET_ID = @Budget_Id
            AND ( @Budget_Account_Id IS NULL
                  OR ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id )
            AND ( ba.IS_DELETED = 1
                  OR b.Analyst_Queue_Display_Cd = @No_Queue_Cd )
                                                
      INSERT      INTO #Budget_Account_Queue
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd
                  ,Queue_Id )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,@Rates_Queue_Cd
                       ,ISNULL(ra.QUEUE_ID, rm.QUEUE_ID) AS QUEUE_ID
                  FROM
                        dbo.BUDGET b
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                                    ---RM Analyst
                        LEFT JOIN ( dbo.UTILITY_DETAIL AS raud
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map raudam
                                          ON raud.UTILITY_DETAIL_ID = raudam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO ragi
                                          ON raudam.Group_Info_ID = ragi.GROUP_INFO_ID
                                    INNER JOIN @Group_Legacy_Name gil
                                          ON ragi.GROUP_INFO_ID = gil.GROUP_INFO_ID
                                    INNER JOIN dbo.USER_INFO ra
                                          ON raudam.Analyst_ID = ra.USER_INFO_ID )
                                    ON raud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
                        LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                                    INNER JOIN dbo.STATE AS s
                                          ON rmm.REGION_ID = s.REGION_ID
                                    INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                                          ON s.STATE_ID = vsm.STATE_ID
                                    INNER JOIN dbo.USER_INFO rm
                                          ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                                    ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                  WHERE
                        ba.BUDGET_ID = @Budget_Id
                        AND ( @Budget_Account_Id IS NULL
                              OR ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id )
                        AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Rates_Queue_Cd )
                        AND ba.IS_DELETED = 0
                        AND ba.RATES_COMPLETED_BY IS NULL
                        AND ba.RATES_REVIEWED_BY IS NULL
                        AND ba.CANNOT_PERFORMED_BY IS NULL
                  GROUP BY
                        ba.BUDGET_ACCOUNT_ID
                       ,ISNULL(ra.QUEUE_ID, rm.QUEUE_ID)
                       
      INSERT      INTO #Budget_Account_Queue
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd
                  ,Queue_Id )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,@Rates_Queue_Cd
                       ,ISNULL(rev.QUEUE_ID, rm.QUEUE_ID) AS QUEUE_ID
                  FROM
                        dbo.BUDGET b
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                        ---RM Reveiwer
                        LEFT JOIN ( dbo.UTILITY_DETAIL AS revud
                                    INNER JOIN dbo.Utility_Detail_Analyst_Map revudam
                                          ON revud.UTILITY_DETAIL_ID = revudam.Utility_Detail_ID
                                    INNER JOIN dbo.GROUP_INFO revgi
                                          ON revudam.Group_Info_ID = revgi.GROUP_INFO_ID
                                             AND revgi.GROUP_NAME = 'Budget Reviewer Queue'
                                    INNER JOIN dbo.USER_INFO rev
                                          ON revudam.Analyst_ID = rev.USER_INFO_ID )
                                    ON revud.VENDOR_ID = cha.Account_Vendor_Id
                                    ---Regional Manager
                        LEFT JOIN ( dbo.REGION_MANAGER_MAP AS rmm
                                    INNER JOIN dbo.STATE AS s
                                          ON rmm.REGION_ID = s.REGION_ID
                                    INNER JOIN dbo.VENDOR_STATE_MAP AS vsm
                                          ON s.STATE_ID = vsm.STATE_ID
                                    INNER JOIN dbo.USER_INFO rm
                                          ON rmm.USER_INFO_ID = rm.USER_INFO_ID )
                                    ON vsm.VENDOR_ID = cha.Account_Vendor_Id
                  WHERE
                        ba.BUDGET_ID = @Budget_Id
                        AND ( @Budget_Account_Id IS NULL
                              OR ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id )
                        AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Rates_Queue_Cd )
                        AND ba.IS_DELETED = 0
                        AND ba.RATES_COMPLETED_BY IS NOT NULL
                        AND ba.RATES_REVIEWED_BY IS NULL
                        AND ba.CANNOT_PERFORMED_BY IS NULL
                  GROUP BY
                        ba.BUDGET_ACCOUNT_ID
                       ,ISNULL(rev.QUEUE_ID, rm.QUEUE_ID)
                             
      INSERT      INTO #Budget_Account_Queue
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd
                  ,Queue_Id )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,@Sourcing_Queue_Cd
                       ,ui.QUEUE_ID
                  FROM
                        dbo.BUDGET b
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ba.ACCOUNT_ID = cha.Account_Id
                                 AND cha.Commodity_Id = b.COMMODITY_TYPE_ID
                        INNER JOIN Core.Client_Hier CH
                              ON cha.Client_Hier_Id = CH.Client_Hier_Id
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP AS vcm
                              ON vcm.VENDOR_ID = cha.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                        INNER JOIN core.Client_Commodity ccc
                              ON ccc.Client_Id = ch.Client_Id
                                 AND ccc.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cha.Account_Id
                                 AND aca.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.SITE_Commodity_Analyst sca
                              ON sca.Site_Id = ch.Site_Id
                                 AND sca.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = CASE WHEN COALESCE(cha.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                                                        ELSE vcam.Analyst_ID
                                                   END
                  WHERE
                        ba.BUDGET_ID = @Budget_Id
                        AND ( @Budget_Account_Id IS NULL
                              OR ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id )
                        AND b.Analyst_Queue_Display_Cd IN ( @Both_Queue_Cd, @Sourcing_Queue_Cd )
                        AND ba.IS_DELETED = 0
                        AND ba.SOURCING_COMPLETED_BY IS NULL
                        AND ba.CANNOT_PERFORMED_BY IS NULL
                  GROUP BY
                        ba.BUDGET_ACCOUNT_ID
                       ,ui.QUEUE_ID
                       
                       
      INSERT      INTO #Budget_Account_Queue_To_Be_Deleted
                  ( 
                   Budget_Account_Id
                  ,Analyst_Type_Cd )
                  SELECT
                        ba.BUDGET_ACCOUNT_ID
                       ,baq.Analyst_Type_Cd
                  FROM
                        dbo.BUDGET b
                        INNER JOIN dbo.BUDGET_ACCOUNT ba
                              ON b.BUDGET_ID = ba.BUDGET_ID
                        INNER JOIN dbo.Budget_Account_Queue baq
                              ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
                  WHERE
                        ba.BUDGET_ID = @Budget_Id
                        AND ( @Budget_Account_Id IS NULL
                              OR ba.BUDGET_ACCOUNT_ID = @Budget_Account_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          #Budget_Account_Queue new
                                         WHERE
                                          baq.Budget_Account_Id = new.Budget_Account_Id
                                          AND baq.Analyst_Type_Cd = new.Analyst_Type_Cd )
            
            
      
      DELETE
            baq
      FROM
            dbo.Budget_Account_Queue baq
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        #Budget_Account_Queue_To_Be_Deleted del
                     WHERE
                        baq.Budget_Account_Id = del.Budget_Account_Id
                        AND baq.Analyst_Type_Cd = del.Analyst_Type_Cd )
                             
      MERGE INTO dbo.Budget_Account_Queue AS tgt
            USING 
                  ( SELECT
                        Budget_Account_Id
                       ,Analyst_Type_Cd
                       ,Queue_Id
                       ,GETDATE() AS Created_Ts
                       ,GETDATE() AS Last_Change_Ts
                    FROM
                        #Budget_Account_Queue ) src
            ON ( tgt.Budget_Account_Id = src.Budget_Account_Id
                 AND tgt.Analyst_Type_Cd = src.Analyst_Type_Cd )
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Budget_Account_Id
                        ,Analyst_Type_Cd
                        ,Queue_Id
                        ,Created_Ts
                        ,Last_Change_Ts )
                    VALUES
                        ( 
                         src.Budget_Account_Id
                        ,src.Analyst_Type_Cd
                        ,src.Queue_Id
                        ,src.Created_Ts
                        ,src.Last_Change_Ts )
            WHEN MATCHED 
                  THEN UPDATE
                    SET 
                        tgt.Last_Change_Ts = src.Last_Change_Ts;
                             
      
                             
            
END;

;
GO

GRANT EXECUTE ON  [dbo].[Budget_Account_Queue_Ins_Upd] TO [CBMSApplication]
GO
