SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE       PROCEDURE [dbo].[cbmsState_GetByStateName]
( 
	@MyAccountId int 
	, @state_name varchar (200)
)
AS
BEGIN

	   select state_id
		, state_name
		from state
	        where state_name = @state_name

END

GO
GRANT EXECUTE ON  [dbo].[cbmsState_GetByStateName] TO [CBMSApplication]
GO
