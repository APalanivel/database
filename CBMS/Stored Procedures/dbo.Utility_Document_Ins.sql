SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Document_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@VENDOR_ID	INT
	@CBMS_IMAGE_ID INT
	@Utility_Document_Type_ID	INT
	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC Utility_Document_Ins 4256,5789,12
	ROLLBACK TRAN
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/
    
CREATE PROC dbo.Utility_Document_Ins
      (
       @VENDOR_ID INT
      ,@CBMS_IMAGE_ID INT
      ,@Utility_Document_Type_ID INT )
AS 
BEGIN    
     
      SET nocount ON ;    
    
      INSERT INTO
            Utility_Document
            (
             VENDOR_ID
            ,CBMS_IMAGE_ID
            ,Utility_Document_Type_ID )
      VALUES
            (
             @VENDOR_ID
            ,@CBMS_IMAGE_ID
            ,@Utility_Document_Type_ID )
END 

GO
GRANT EXECUTE ON  [dbo].[Utility_Document_Ins] TO [CBMSApplication]
GO
