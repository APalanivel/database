SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.Cu_Invoice_Recalc_Exception_Queue_Open_Exception_Sel   
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name        DataType    Default  Description    
-------------------------------------------------------------------------------------    
   
  
OUTPUT PARAMETERS:    
 Name        DataType    Default  Description    
-------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------    
  
   
 EXEC Cu_Invoice_Recalc_Exception_Queue_Open_Exception_Sel  
   
    
    
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------------------------    
 RKV  Ravi Kumar Vegesna    
     
MODIFICATIONS    
    
 Initials  Date   Modification    
-------------------------------------------------------------------------------------    
 RKV   2015-11-06  Created for AS400-PII   
              
                
******/    
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Exception_Queue_Open_Exception_Sel]
AS 
BEGIN    
        
        
       
      SELECT
            cireq.Cu_Invoice_Recalc_Exception_Queue_Id
           ,cireq.Cu_Invoice_Id
           ,cireq.Account_Id
           ,cireq.Commodity_Id
           ,cireq.Status_Cd
           ,c.Code_Value [Status]
           ,cireq.Queue_Date
           ,cireq.User_Info_Id
           ,cireq.Last_Change_Ts
      FROM
            dbo.Cu_Invoice_Recalc_Exception_Queue cireq
            INNER JOIN dbo.Code c
                  ON c.Code_Id = cireq.Status_Cd
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'IDMExceptionStatus'
            AND c.Code_Value = 'Open'
      ORDER BY
            cireq.Cu_Invoice_Id 
          
        
         
END       
     

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Exception_Queue_Open_Exception_Sel] TO [CBMSApplication]
GO
