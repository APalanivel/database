SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Collection_Chase_History_By_Invoice_Collection_Queue         
                
Description:                
   This sproc to get the attribute details for a Given id's.        
                             
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
  @Invoice_Collection_Queue_Id   INT  
     @Collection_Start_Date     DATE     NULL  
     @Collection_End_Date     DATE     NULL  
     @User_Info_Id       INT                      
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.Invoice_Collection_Issue_History_By_Invoice_Collection_Queue_1 '615,616',16  
     
    
     
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
----------------------------------------------------------------------------------------                  
    RKV    2016-12-29  Created For Invoice_Collection.  
 RKV             2018-04-12      Maint 7095 ,Added Pagination            
               
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_History_By_Invoice_Collection_Queue]
     (
         @Invoice_Collection_Queue_Id VARCHAR(MAX)
         , @User_Info_Id INT
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
     )
AS
    BEGIN
        SET NOCOUNT ON;

        WITH Cte_Invoice_Collection_Issue_History_By_Invoice_Collection_Queue_dummy1
        AS (
               SELECT   TOP (@End_Index)
                        CAST(icq.Collection_Start_Dt AS VARCHAR(12)) + '#' + CAST(icq.Collection_End_Dt AS VARCHAR(12)) AS Period_Chased
                        , ici.Created_Ts Issue_Date
                        , ici.Last_Change_Ts Last_Updated_Date
                        , icic.Code_Value Issue_Status
                        , icitc.Code_Value Type_Of_Issue
                        , ici.Issue_Entity_Owner_Cd Entry_Owner_Cd
                        , ui.FIRST_NAME + ' ' + ui.LAST_NAME Issue_Owner
                        , icoui.FIRST_NAME + ' ' + icoui.LAST_NAME Invoice_Collection_Officer
                        , '' Comment_Desc
                        , ici.Invoice_Collection_Issue_Log_Id AS Invoice_Collection_Issue_Id
                        , icq.Invoice_Collection_Queue_Id
                        , iewc.Code_Value Entry_Owner
                        , LEFT(icqi.Invoice_Collection_Issue_Grouped_Queue_Ids, LEN(icqi.Invoice_Collection_Issue_Grouped_Queue_Ids)
                                                                                - 1) Invoice_Collection_Issue_Grouped_Queue_Ids
                        , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                           ici.Created_Ts DESC)
                        , COUNT(1) OVER () AS TotalCount
						,ici.Is_Blocker
						,ici.Invoice_Collection_Activity_Id
               FROM
                    dbo.Invoice_Collection_Issue_Log ici
                    INNER JOIN dbo.Invoice_Collection_Queue icq
                        ON icq.Invoice_Collection_Queue_Id = ici.Invoice_Collection_Queue_Id
                    INNER JOIN dbo.Invoice_Collection_Account_Config icac
                        ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                    INNER JOIN dbo.Code icic
                        ON icic.Code_Id = ici.Issue_Status_Cd
                    INNER JOIN dbo.Code icitc
                        ON icitc.Code_Id = ici.Invoice_Collection_Issue_Type_Cd
                    INNER JOIN dbo.Code iewc
                        ON iewc.Code_Id = ici.Issue_Entity_Owner_Cd
                    INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                        ON ufn.Segments = icq.Invoice_Collection_Queue_Id
                    LEFT JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = ici.Issue_Owner_User_Id
                    INNER JOIN dbo.USER_INFO icoui
                        ON icoui.USER_INFO_ID = icac.Invoice_Collection_Officer_User_Id
                    CROSS APPLY
               (   SELECT
                        CAST(icil1.Invoice_Collection_Queue_Id AS VARCHAR(25)) + ','
                   FROM
                        dbo.Invoice_Collection_Issue_Log icil1
						 INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
                        ON ufn.Segments = icil1.Invoice_Collection_Queue_Id
                   WHERE
                        icil1.Invoice_Collection_Activity_Id = ici.Invoice_Collection_Activity_Id
                   FOR XML PATH('')) icqi(Invoice_Collection_Issue_Grouped_Queue_Ids)
           )
        SELECT
            c.Period_Chased
            , c.Issue_Date
            , c.Last_Updated_Date
            , c.Issue_Status
            , c.Type_Of_Issue
            , c.Entry_Owner_Cd
            , c.Issue_Owner
            , c.Invoice_Collection_Officer
            , c.Comment_Desc
            , c.Invoice_Collection_Issue_Id
            , c.Invoice_Collection_Queue_Id
            , c.Entry_Owner
            , c.Invoice_Collection_Issue_Grouped_Queue_Ids
            , c.Row_Num
            , c.TotalCount
			, c.Is_Blocker
			,c.Invoice_Collection_Activity_Id
        FROM
            Cte_Invoice_Collection_Issue_History_By_Invoice_Collection_Queue_dummy1 c
        WHERE
            c.Row_Num BETWEEN @Start_Index
                      AND     @End_Index;


    END;







GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_History_By_Invoice_Collection_Queue] TO [CBMSApplication]
GO
