SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Vendor_State_Map_Del  
     
DESCRIPTION: 
	It Deletes	Vendor State Map detail for Selected Vendor and State Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION          
----------------------------------------------------------------         
	@Vendor_Id			INT
    @State_Id			INT	
                
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.Vendor_State_Map_Del 3451,114
		--EXEC dbo.Vendor_State_Map_Del 4352,197
		
		--SELECT * FROM dbo.Vendor_State_Map where vendor_id = 3451
		--										and state_id = 114
		
		--SELECT * FROM dbo.Vendor_State_Map where vendor_id = 4352
		--										and state_id = 197					

	ROLLBACK TRAN

AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			23-August-10	CREATED

*/
CREATE PROCEDURE dbo.Vendor_State_Map_Del
    (
       @Vendor_Id	INT
      ,@State_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON ;

	DELETE
		dbo.Vendor_State_Map
	WHERE
		VENDOR_ID = @Vendor_Id
		AND STATE_ID = @State_Id

END
GO
GRANT EXECUTE ON  [dbo].[Vendor_State_Map_Del] TO [CBMSApplication]
GO
