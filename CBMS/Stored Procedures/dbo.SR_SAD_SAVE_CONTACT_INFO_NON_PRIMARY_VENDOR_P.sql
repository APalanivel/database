SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_SAD_SAVE_CONTACT_INFO_NON_PRIMARY_VENDOR_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@userId        varchar,
@sessionId     varchar,
@vendorId      int,
@contactinfoId int,
@isprimary     bit,
@stateId       int,
@countryId     int,
@commodityId   int,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@sr_supplier_contact_vendor_map_id int OUTPUT


USAGE EXAMPLES:
------------------------------------------------------------
---- exec dbo.SR_SAD_SAVE_CONTACT_INFO_NON_PRIMARY_VENDOR_P 1,1

---- Test Insert an entry
--exec dbo.SR_SAD_SAVE_CONTACT_INFO_NON_PRIMARY_VENDOR_P 
--@userId = 1,
--@sessionId = -1,
--@vendorId = 822 ,
--@contactinfoId = 1,
--@isprimary = 0,
--@stateId = NULL,
--@countryId = NULL,
--@commodityId = 291,
--@sr_supplier_contact_vendor_map_id = NULL

---- use sv
---- select * from sv..SR_SUPPLIER_CONTACT_VENDOR_MAP where SR_SUPPLIER_CONTACT_VENDOR_MAP_ID = (select max(SR_SUPPLIER_CONTACT_VENDOR_MAP_ID) from sv..SR_SUPPLIER_CONTACT_VENDOR_MAP)

---- Delete the entry
--delete from SR_SUPPLIER_CONTACT_VENDOR_MAP where SR_SUPPLIER_CONTACT_VENDOR_MAP_ID = (select max(SR_SUPPLIER_CONTACT_VENDOR_MAP_ID) from SR_SUPPLIER_CONTACT_VENDOR_MAP)



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_SAD_SAVE_CONTACT_INFO_NON_PRIMARY_VENDOR_P 

@userId varchar,
@sessionId varchar,
@vendorId int,
@contactinfoId int,
@isprimary bit,
@stateId int,
@countryId int,
@commodityId int,
@sr_supplier_contact_vendor_map_id int OUTPUT

AS
	
SET NOCOUNT ON

	INSERT INTO SR_SUPPLIER_CONTACT_VENDOR_MAP 
	(
 		SR_SUPPLIER_CONTACT_INFO_ID,
		VENDOR_ID,
		IS_PRIMARY,
		COUNTRY_ID,
		STATE_ID,
		COMMODITY_TYPE_ID
	)
	VALUES	
	(	
		@contactinfoId,
		@vendorId,
		@isprimary,
		@countryId,
		@stateId,
		@commodityId
	)

	select @sr_supplier_contact_vendor_map_id = SCOPE_IDENTITY()
	

RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_CONTACT_INFO_NON_PRIMARY_VENDOR_P] TO [CBMSApplication]
GO
