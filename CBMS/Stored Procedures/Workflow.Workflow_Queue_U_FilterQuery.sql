SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:    [Workflow].[Workflow_Queue_U_FilterQuery]    
DESCRIPTION:   The Stored Procedure will Update  the Saved Filter information   
------------------------------------------------------------       
 INPUT PARAMETERS:        
 Name   DataType  Default Description       
     
 @vc_FilterName AS NVARCHAR(MAX),    
 @int_Queue_Id     AS INT,          
 @vc_New_Filter_Name AS NVARCHAR(MAX),          
 @datetime_Expires_On   AS DATETIME =NULL,          
 @bit_No_Expiration_Date  AS BIT,          
 @int_Created_User_Id   AS INT,        
 @int_Last_Modified_User_Id  AS INT,          
 @bit_Is_Public_Filter   AS BIT = 0      
------------------------------------------------------------        
 OUTPUT PARAMETERS:        
 Name   DataType  Default Description        
------------------------------------------------------------        
 USAGE EXAMPLES:        
------------------------------------------------------------        
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
TRK Ramakrishna Thummala Summit Energy     
     
 MODIFICATIONS         
 Initials Date   Modification        
------------------------------------------------------------        
 TRK    02-08-2019 Created    
    
******/    
CREATE PROCEDURE [Workflow].[Workflow_Queue_U_FilterQuery]            
(            
 @FilterId AS INT,      
 @int_Module_Id     AS INT,            
 @vc_New_Filter_Name AS NVARCHAR(MAX),            
 @datetime_Expires_On   AS DATETIME =NULL,            
 @bit_No_Expiration_Date  AS BIT,            
 @int_Created_User_Id   AS INT,          
 @int_Last_Modified_User_Id  AS INT,            
 @bit_Is_Public_Filter   AS BIT = 0          
)            
AS            
BEGIN            
            
        
 UPDATE   fq             
  SET           
   Saved_Filter_Name   = @vc_New_Filter_Name       
   ,Owner_User_Id    = @int_Created_User_Id        
   ,Expiration_Dt   =  CASE WHEN @bit_No_Expiration_Date = CAST(0 AS BIT) THEN @datetime_Expires_On  ELSE '12/31/9999' END        
   ,Updated_User_Id   = @int_Created_User_Id        
   ,Last_Change_Ts       = GETDATE()       
 FROM Workflow.Workflow_queue_saved_filter_query fq       
 INNER JOIN   Workflow.Workflow_sub_queue sq       
 ON fq.Workflow_Queue_Saved_Filter_Query_Id = @FilterId      
 AND  fq.Workflow_Sub_Queue_Id = sq.Workflow_Sub_Queue_Id           
 AND sq.Workflow_Queue_Id = @int_Module_Id      
         
            
END                 
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_U_FilterQuery] TO [CBMSApplication]
GO
