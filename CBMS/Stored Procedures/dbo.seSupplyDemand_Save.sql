SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seSupplyDemand_Save
	(
		@SupplyDemandId int = null
	, @DataTypeId int
	,	@ReportYear char(4)
	, @ReportMonth char(2)
	, @UnaccountedFor int
	, @Consumption int
	, @NetImports int
	, @StorageWithdrawal int
	, @DryProduction int
	, @SupplementalProduction int
	, @PlantFuel int
	, @Vehicles int
	, @Industrial int
	, @Commercial int
	, @Residential int
	, @Transportation int
	, @PowerGeneration int
	)
As
BEGIN
	set nocount on
	
	declare @ThisId int
	
	set @ThisId = @SupplyDemandId
	
	if @ThisId is null
	begin
		select @ThisId = SupplyDemandId
		  from seSupplyDemand
		 where ReportYear = @ReportYear
		   and ReportMonth = @ReportMonth
		   and DataTypeId = @DataTypeId
	
	end
	if @ThisId is null
	begin
	
		insert into seSupplyDemand
			( DataTypeId
			, ReportYear
			, ReportMonth
			, UnaccountedFor 
			, Consumption
			, NetImports 
			, StorageWithdrawal 
			, DryProduction 
			, SupplementalProduction 
			, PlantFuel 
			, Vehicles 
			, Industrial 
			, Commercial 
			, Residential 
			, Transportation 
			, PowerGeneration 
			)
			values
			( @DataTypeId
			, @ReportYear
			, @ReportMonth
			, @UnaccountedFor 
			, @Consumption
			, @NetImports 
			, @StorageWithdrawal 
			, @DryProduction 
			, @SupplementalProduction 
			, @PlantFuel 
			, @Vehicles 
			, @Industrial 
			, @Commercial 
			, @Residential 
			, @Transportation 
			, @PowerGeneration 
			)
	
			set @ThisId = @@IDENTITY
	
	end
	else
	begin
	
		 update seSupplyDemand
		    set DataTypeId = @DataTypeId
					, ReportYear = @ReportYear
					, ReportMonth = @ReportMonth 
					, UnaccountedFor = @UnaccountedFor 
					, Consumption = @Consumption 
					, NetImports = @NetImports 
					, StorageWithdrawal = @StorageWithdrawal 
					, DryProduction = @DryProduction 
					, SupplementalProduction = @SupplementalProduction 
					, PlantFuel = @PlantFuel 
					, Vehicles = @Vehicles 
					, Industrial = @Industrial 
					, Commercial = @Commercial 
					, Residential = @Residential 
					, Transportation = @Transportation 
					, PowerGeneration = @PowerGeneration 
		  where SupplyDemandId = @ThisId
	
	end

	exec seSupplyDemand_Get @ThisId
END
GO
GRANT EXECUTE ON  [dbo].[seSupplyDemand_Save] TO [CBMSApplication]
GO
