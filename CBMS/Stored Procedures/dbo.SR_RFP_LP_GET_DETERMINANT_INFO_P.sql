
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_RFP_LP_GET_DETERMINANT_INFO_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@rfp_id int,
	@commodity varchar(50),
	@rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
EXEC DBO.SR_RFP_LP_GET_DETERMINANT_INFO_P 
      270
     ,1479
EXEC SR_RFP_LP_GET_DETERMINANT_INFO_P 
      @rfp_id = 270
     ,@commodity = 'Natural Gas'
     ,@rfp_account_id = 1479


exec SR_RFP_LP_GET_DETERMINANT_INFO_P 13730, 'Natural Gas', 12107812 



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DR			08/04/2009	    Removed Linked Server Updates
	DMR			09/10/2010		Modified for Quoted_Identifier
	NR			2017-02-22		MAINT-4841 Added  Demand determinant name for Natural Gas.	


******/

CREATE PROCEDURE [dbo].[SR_RFP_LP_GET_DETERMINANT_INFO_P]
      @rfp_id INT
     ,@commodity VARCHAR(50)
     ,@rfp_account_id INT
AS 
SET nocount ON

	
DECLARE
      @account_id INT
     ,
		--/** variables to hold LP determinant setup info */
      @month_selector VARCHAR(20)
     ,@determinant_id INT
     ,@unit_type_id INT
     ,
		--/** variables to hold default lp setup paramter which will be moved to RFP lp */
      @month_selector_type_id INT
     ,@additional_rows INT
     ,@default_lp_setup_id INT
     ,@rfp_lp_setup_id INT
     ,@determinant_type_id INT
     ,@account_determinant_type_id INT
     ,@lp_status_id INT
     ,@temp_utility_id INT
     ,@temp_rfp_account_id INT
		
	--//getting entity ids required to update while creating load profile
SELECT
      @determinant_type_id = entity_id
FROM
      entity(NOLOCK)
WHERE
      entity_type = 1030
      AND entity_name = 'RFP Level'
SELECT
      @account_determinant_type_id = entity_id
FROM
      entity(NOLOCK)
WHERE
      entity_type = 1030
      AND entity_name = 'Account Level'
SELECT
      @lp_status_id = entity_id
FROM
      entity(NOLOCK)
WHERE
      entity_type = 1006
      AND entity_name = 'Not Sent'


	
IF ( SELECT
      COUNT(sr_rfp_lp_account_summary_id)
     FROM
      sr_rfp_lp_account_summary(NOLOCK)
     WHERE
      sr_account_group_id = @rfp_account_id ) > 0 
      BEGIN
            UPDATE
                  sr_rfp_lp_account_summary
            SET   
                  date_created = GETDATE()
            WHERE
                  sr_account_group_id = @rfp_account_id
	
      END
ELSE 
      BEGIN
            INSERT      INTO sr_rfp_lp_account_summary
                        ( 
                         sr_account_group_id
                        ,is_bid_group
                        ,status_type_id
                        ,date_created )
            VALUES
                        ( 
                         @rfp_account_id
                        ,0
                        ,@lp_status_id
                        ,GETDATE() )
	
	
      END
	
	--/** Moving default lp setup to rfp lp setup, happens only once before load profile is created */
	
SET @rfp_lp_setup_id = NULL
IF ( SELECT
      COUNT(lp_setup.sr_rfp_load_profile_setup_id)
     FROM
      sr_rfp_load_profile_setup lp_setup ( NOLOCK )
     ,sr_rfp_account rfp_account ( NOLOCK )
     WHERE
      lp_setup.sr_rfp_account_id = @rfp_account_id
      AND rfp_account.sr_rfp_account_id = lp_setup.sr_rfp_account_id
      AND rfp_account.is_deleted = 0 ) = 0 
      BEGIN
            SELECT 	--@rfp_account_id = sr_rfp_account_id,
                  @month_selector_type_id = setup.month_selector_type_id
                 ,@additional_rows = setup.additional_rows
                 ,@default_lp_setup_id = setup.sr_load_profile_default_setup_id
            FROM
                  sr_rfp rfp ( NOLOCK )
                 ,sr_rfp_account rfp_acc ( NOLOCK )
                 ,account acc ( NOLOCK )
                 ,sr_load_profile_default_setup setup ( NOLOCK )
            WHERE
                  rfp.sr_rfp_id = @rfp_id
                  AND rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                  AND rfp_acc.sr_rfp_account_id = @rfp_account_id
                  AND rfp_acc.is_deleted = 0
                  AND acc.account_id = rfp_acc.account_id
                  AND setup.commodity_type_id = rfp.commodity_type_id
                  AND setup.vendor_id = acc.vendor_id
	
	
            INSERT      INTO sr_rfp_load_profile_setup
                        ( 
                         sr_rfp_account_id
                        ,sr_rfp_id
                        ,additional_rows
                        ,month_selector_type_id
                        ,updated_month_selector_type_id )
            VALUES
                        ( 
                         @rfp_account_id
                        ,@rfp_id
                        ,@additional_rows
                        ,@month_selector_type_id
                        ,@month_selector_type_id )
	
            SELECT
                  @rfp_lp_setup_id = @@identity
	
	
            INSERT      INTO sr_rfp_load_profile_determinant
                        ( 
                         sr_rfp_load_profile_setup_id
                        ,determinant_name
                        ,determinant_unit_type_id
                        ,updated_determinant_unit_type_id
                        ,is_checked
                        ,updated_is_checked
                        ,determinant_type_id )
                        SELECT
                              @rfp_lp_setup_id
                             ,determinant.determinant_name
                             ,determinant.determinant_unit_type_id
                             ,determinant.determinant_unit_type_id
                             ,determinant.is_checked
                             ,determinant.is_checked
                             ,@determinant_type_id
                        FROM
                              sr_load_profile_determinant determinant ( NOLOCK )
                        WHERE
                              determinant.sr_load_profile_default_setup_id = @default_lp_setup_id
	
		--//Additional rows
            INSERT      INTO sr_rfp_lp_account_additional_row
                        ( 
                         sr_rfp_account_id
                        ,row_name
                        ,row_value
                        ,updated_row_name
                        ,updated_row_value
                        ,row_type_id )
                        SELECT
                              @rfp_account_id
                             ,additional_row.row_name
                             ,additional_row.row_value
                             ,additional_row.row_name
                             ,additional_row.row_value
                             ,@determinant_type_id
                        FROM
                              sr_load_profile_additional_row additional_row
                        WHERE
                              additional_row.sr_load_profile_default_setup_id = @default_lp_setup_id

      END
ELSE 
      BEGIN
		--//Additional Rows
            IF ( SELECT
                  COUNT(row.sr_rfp_lp_account_additional_row_id)
                 FROM
                  sr_rfp_lp_account_additional_row row ( NOLOCK )
                 ,sr_rfp_account rfp_account ( NOLOCK )
                 WHERE
                  row.sr_rfp_account_id = @rfp_account_id
                  AND rfp_account.sr_rfp_account_id = row.sr_rfp_account_id
                  AND rfp_account.is_deleted = 0 ) > 0 
                  BEGIN
                        UPDATE
                              sr_rfp_lp_account_additional_row
                        SET   
                              row_name = updated_row_name
                             ,row_value = updated_row_value
                             ,row_type_id = @determinant_type_id
                        WHERE
                              sr_rfp_account_id = @rfp_account_id
                              AND row_type_id != @account_determinant_type_id
			
		
                  END
            ELSE 
                  BEGIN
                        SELECT
                              @temp_utility_id = acc.vendor_id
                        FROM
                              sr_rfp_account rfp_acc ( NOLOCK )
                             ,account acc ( NOLOCK )
                        WHERE
                              rfp_acc.sr_rfp_account_id = @rfp_account_id
                              AND acc.account_id = rfp_acc.account_id
                              AND rfp_acc.is_deleted = 0
			
                        SELECT
                              @temp_rfp_account_id = additional_row.sr_rfp_account_id
                        FROM
                              sr_rfp_lp_account_additional_row additional_row ( NOLOCK )
                             ,sr_rfp_account rfp_acc ( NOLOCK )
                             ,account acc ( NOLOCK )
                        WHERE
                              acc.vendor_id = @temp_utility_id
                              AND rfp_acc.account_id = acc.account_id
                              AND additional_row.sr_rfp_account_id = rfp_acc.sr_rfp_account_id
                              AND additional_row.row_type_id = @determinant_type_id
                              AND rfp_acc.sr_rfp_id = @rfp_id
                              AND rfp_acc.is_deleted = 0

                        IF @temp_rfp_account_id > 0 
                              BEGIN
                                    INSERT      INTO sr_rfp_lp_account_additional_row
                                                ( 
                                                 sr_rfp_account_id
                                                ,row_name
                                                ,row_value
                                                ,updated_row_name
                                                ,updated_row_value
                                                ,row_type_id )
                                                SELECT
                                                      @rfp_account_id
                                                     ,additional_row.row_name
                                                     ,additional_row.row_value
                                                     ,additional_row.row_name
                                                     ,additional_row.row_value
                                                     ,@determinant_type_id
                                                FROM
                                                      sr_rfp_lp_account_additional_row additional_row
                                                WHERE
                                                      additional_row.sr_rfp_account_id = @temp_rfp_account_id


                              END
                        ELSE 
                              BEGIN
                                    SELECT
                                          @default_lp_setup_id = setup.sr_load_profile_default_setup_id
                                    FROM
                                          sr_rfp rfp ( NOLOCK )
                                         ,sr_rfp_account rfp_acc ( NOLOCK )
                                         ,account acc ( NOLOCK )
                                         ,sr_load_profile_default_setup setup ( NOLOCK )
                                    WHERE
                                          rfp.sr_rfp_id = @rfp_id
                                          AND rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                                          AND rfp_acc.sr_rfp_account_id = @rfp_account_id
                                          AND rfp_acc.is_deleted = 0
                                          AND acc.account_id = rfp_acc.account_id
                                          AND setup.commodity_type_id = rfp.commodity_type_id
                                          AND setup.vendor_id = acc.vendor_id

                                    INSERT      INTO sr_rfp_lp_account_additional_row
                                                ( 
                                                 sr_rfp_account_id
                                                ,row_name
                                                ,row_value
                                                ,updated_row_name
                                                ,updated_row_value
                                                ,row_type_id )
                                                SELECT
                                                      @rfp_account_id
                                                     ,additional_row.row_name
                                                     ,additional_row.row_value
                                                     ,additional_row.row_name
                                                     ,additional_row.row_value
                                                     ,@determinant_type_id
                                                FROM
                                                      sr_load_profile_additional_row additional_row
                                                WHERE
                                                      additional_row.sr_load_profile_default_setup_id = @default_lp_setup_id


                              END
			
                  END		
		--//Additional Rows END

            SELECT
                  @rfp_lp_setup_id = sr_rfp_load_profile_setup_id
            FROM
                  sr_rfp_load_profile_setup setup ( NOLOCK )
                 ,sr_rfp_account rfp_account ( NOLOCK )
            WHERE
                  setup.sr_rfp_account_id = @rfp_account_id
                  AND rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
                  AND rfp_account.is_deleted = 0
	
            UPDATE
                  sr_rfp_load_profile_setup
            SET   
                  month_selector_type_id = updated_month_selector_type_id
            WHERE
                  sr_rfp_account_id = @rfp_account_id
	
            IF ( SELECT
                  COUNT(det.sr_rfp_load_profile_determinant_id)
                 FROM
                  sr_rfp_load_profile_determinant det ( NOLOCK )
                 WHERE
                  det.sr_rfp_load_profile_setup_id = @rfp_lp_setup_id ) > 0 
                  BEGIN
                        UPDATE
                              sr_rfp_load_profile_determinant
                        SET   
                              determinant_type_id = @determinant_type_id
                             ,determinant_unit_type_id = updated_determinant_unit_type_id
                             ,is_checked = updated_is_checked
                        WHERE
                              sr_rfp_load_profile_setup_id = @rfp_lp_setup_id
                              AND updated_determinant_unit_type_id IS NOT NULL
                              AND updated_is_checked IS NOT NULL
			
                  END
            ELSE 
                  BEGIN ---//
                        DECLARE
                              @max_rfp_lp_setup_id INT
                             ,@utility_id INT
                        SELECT
                              @utility_id = acc.vendor_id
                        FROM
                              sr_rfp_account rfp_acc ( NOLOCK )
                             ,account acc ( NOLOCK )
                        WHERE
                              rfp_acc.sr_rfp_account_id = @rfp_account_id
                              AND acc.account_id = rfp_acc.account_id
	
                        SELECT
                              @max_rfp_lp_setup_id = MAX(setup.sr_rfp_load_profile_setup_id)
                        FROM
                              sr_rfp_account rfp_acc ( NOLOCK )
                             ,sr_rfp_load_profile_setup setup ( NOLOCK )
                             ,sr_rfp_load_profile_determinant det ( NOLOCK )
                             ,account acc ( NOLOCK )
                        WHERE
                              rfp_acc.sr_rfp_id = @rfp_id
                              AND acc.account_id = rfp_acc.account_id
                              AND rfp_acc.is_deleted = 0
                              AND acc.vendor_id = @utility_id
                              AND setup.sr_rfp_account_id = rfp_acc.sr_rfp_account_id
                              AND det.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
	
                        IF @max_rfp_lp_setup_id > 0 
                              BEGIN	
                                    INSERT      INTO sr_rfp_load_profile_determinant
                                                ( 
                                                 sr_rfp_load_profile_setup_id
                                                ,determinant_name
                                                ,determinant_unit_type_id
                                                ,updated_determinant_unit_type_id
                                                ,is_checked
                                                ,updated_is_checked
                                                ,determinant_type_id )
                                                SELECT
                                                      @rfp_lp_setup_id
                                                     ,determinant.determinant_name
                                                     ,determinant.determinant_unit_type_id
                                                     ,determinant.determinant_unit_type_id
                                                     ,determinant.is_checked
                                                     ,determinant.is_checked
                                                     ,@determinant_type_id
                                                FROM
                                                      sr_rfp_load_profile_determinant determinant ( NOLOCK )
                                                WHERE
                                                      determinant.sr_rfp_load_profile_setup_id = @max_rfp_lp_setup_id
                              END
                        ELSE 
                              BEGIN
	
                                    SELECT
                                          @default_lp_setup_id = setup.sr_load_profile_default_setup_id
                                    FROM
                                          sr_rfp rfp ( NOLOCK )
                                         ,sr_rfp_account rfp_acc ( NOLOCK )
                                         ,account acc ( NOLOCK )
                                         ,sr_load_profile_default_setup setup ( NOLOCK )
                                    WHERE
                                          rfp.sr_rfp_id = @rfp_id
                                          AND rfp_acc.sr_rfp_id = rfp.sr_rfp_id
                                          AND rfp_acc.sr_rfp_account_id = @rfp_account_id
                                          AND rfp_acc.is_deleted = 0
                                          AND acc.account_id = rfp_acc.account_id
                                          AND setup.commodity_type_id = rfp.commodity_type_id
                                          AND setup.vendor_id = acc.vendor_id
	
                                    INSERT      INTO sr_rfp_load_profile_determinant
                                                ( 
                                                 sr_rfp_load_profile_setup_id
                                                ,determinant_name
                                                ,determinant_unit_type_id
                                                ,updated_determinant_unit_type_id
                                                ,is_checked
                                                ,updated_is_checked
                                                ,determinant_type_id )
                                                SELECT
                                                      @rfp_lp_setup_id
                                                     ,determinant.determinant_name
                                                     ,determinant.determinant_unit_type_id
                                                     ,determinant.determinant_unit_type_id
                                                     ,determinant.is_checked
                                                     ,determinant.is_checked
                                                     ,@determinant_type_id
                                                FROM
                                                      sr_load_profile_determinant determinant ( NOLOCK )
                                                WHERE
                                                      determinant.sr_load_profile_default_setup_id = @default_lp_setup_id
	
                              END
		
                  END --@end
      END
IF @commodity = 'Natural Gas' 
      BEGIN
		--/** selecting the fixed determinant 'Total Usage' to which the load profile is created */
            SELECT
                  monthSelector.entity_name AS month_selector
                 ,setup.additional_rows AS additional_rows
                 ,determinant.sr_rfp_load_profile_determinant_id AS determinant_id
                 ,determinant.determinant_name AS determinant_name
                 ,determinant.determinant_unit_type_id AS unit_type_id
            FROM
                  sr_rfp_load_profile_setup setup ( NOLOCK )
                  JOIN entity monthSelector ( NOLOCK )
                                            ON monthSelector.entity_id = setup.month_selector_type_id
                                               AND setup.sr_rfp_account_id = @rfp_account_id
                  JOIN sr_rfp_account rfp_account ( NOLOCK )
                                                  ON setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
                                                     AND rfp_account.is_deleted = 0
                  JOIN sr_rfp_load_profile_determinant determinant ( NOLOCK )
                                                                   ON determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                                                                      AND determinant.is_checked = 1
                                                                      AND determinant.determinant_name IN ( 'Total Usage', 'Demand' )
      END
ELSE 
      BEGIN
            SELECT
                  monthSelector.entity_name AS month_selector
                 ,setup.additional_rows AS additional_rows
                 ,determinant.sr_rfp_load_profile_determinant_id AS determinant_id
                 ,determinant.determinant_name AS determinant_name
                 ,determinant.determinant_unit_type_id AS unit_type_id
            FROM
                  sr_rfp_load_profile_setup setup ( NOLOCK )
                  JOIN entity monthSelector ( NOLOCK )
                                            ON monthSelector.entity_id = setup.month_selector_type_id
                                               AND setup.sr_rfp_account_id = @rfp_account_id
                  JOIN sr_rfp_account rfp_account ( NOLOCK )
                                                  ON setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
                                                     AND rfp_account.is_deleted = 0
                  JOIN sr_rfp_load_profile_determinant determinant ( NOLOCK )
                                                                   ON determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
                                                                      AND determinant.is_checked = 1
                                                                      AND determinant.determinant_name IN ( 'Total Usage', 'On Peak kWh', 'Off Peak kWh', 'Intermediate Peak kWh', 'Actual/On Peak kW', 'Contract kW', 'Billing kW' )

      END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_DETERMINANT_INFO_P] TO [CBMSApplication]
GO
