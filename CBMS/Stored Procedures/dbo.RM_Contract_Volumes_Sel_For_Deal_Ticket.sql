SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
 @Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 11236  
  
 EXEC dbo.Contract_Sel_By_Commodity_Hedge_Type_Period @Client_Id = 11236,@Commodity_Id = 291  
   ,@Start_Dt='2015-01-01',@End_Dt='2017-12-30',@Hedge_Type=586  
    
 SELECT SUM(VOLUME),MONTH_IDENTIFIER FROM dbo.CONTRACT_METER_VOLUME   
 WHERE CONTRACT_ID = 162277 AND MONTH_IDENTIFIER BETWEEN '2017-01-01' AND '2017-06-01' GROUP BY MONTH_IDENTIFIER  
 EXEC dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162277'  
 EXEC dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162278'  
 EXEC dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket  @Client_Id = 11236,@Commodity_Id = 291  
  ,@Start_Dt='2017-01-01',@End_Dt='2017-06-01',@Hedge_Type=586,@Contract_Id = '162277,162278'  
    
 EXEC dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket  @Client_Id = 10003,@Commodity_Id = 291  
  ,@Start_Dt='2019-07-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Uom_Id=25,@Contract_Id='164932,151196,150660,121968',@Deal_Ticket_Frequency_Cd = 102749
 EXEC dbo.RM_Contract_Volumes_Sel_For_Deal_Ticket  @Client_Id = 10003,@Commodity_Id = 291  
  ,@Start_Dt='2019-07-01',@End_Dt='2019-07-01',@Hedge_Type=586,@Uom_Id=25,@Contract_Id='164932,151196,150660,121968',@Deal_Ticket_Frequency_Cd = 102756
  
   
   
  
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials Date           Modification                        
--------------------------------------------------------------------------------  
 PR          02-11-2018    Created GRM  
                       
******/
CREATE PROCEDURE [dbo].[RM_Contract_Volumes_Sel_For_Deal_Ticket]
      ( 
       @Client_Id INT
      ,@Commodity_Id INT
      ,@Hedge_Type INT
      ,@Start_Dt DATE
      ,@End_Dt DATE
      ,@Contract_Id VARCHAR(MAX) = NULL
      ,@Uom_Id INT = NULL
      ,@Deal_Ticket_Frequency_Cd INT = NULL
      ,@Trade_tvp_Hedge_Contract_Sites AS Trade.tvp_Hedge_Contract_Sites READONLY )
AS 
BEGIN
      SET NOCOUNT ON;

      CREATE TABLE #Volumes
            ( 
             Client_Hier_Id INT
            ,METER_ID INT
            ,Service_Month DATE
            ,Contract_Volume DECIMAL(28, 0)
            ,Uom_Id INT );

      DECLARE
            @Common_Uom_Id INT
           ,@Deal_Ticket_Frequency VARCHAR(25)
           ,@Hedge_Contract_Sites_Cnt INT;


      DECLARE
            @Hedge_Type_Input VARCHAR(200)
           ,@Contract_Min_Start_Dt DATE
           ,@Contract_Max_End_Dt DATE
           ,@Contract_Not_Entire_Hedge_Period BIT = 0;

      SELECT
            @Hedge_Type_Input = ENTITY_NAME
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_ID = @Hedge_Type
            
      SELECT
            @Deal_Ticket_Frequency = Code_Value
      FROM
            dbo.Code
      WHERE
            Code_Id = @Deal_Ticket_Frequency_Cd
            
      SELECT
            @Hedge_Contract_Sites_Cnt = COUNT(1)
      FROM
            @Trade_tvp_Hedge_Contract_Sites;
            
            
      SELECT
            @Contract_Min_Start_Dt = MAX(con.CONTRACT_START_DATE)
           ,@Contract_Max_End_Dt = MIN(CONTRACT_END_DATE)
      FROM
            @Trade_tvp_Hedge_Contract_Sites hcs
            INNER JOIN dbo.CONTRACT con
                  ON con.CONTRACT_ID = hcs.Contract_Id
                  
      SELECT
            @Contract_Not_Entire_Hedge_Period = 1
      WHERE
            @Contract_Min_Start_Dt > @Start_Dt
            OR @Contract_Max_End_Dt < @End_Dt


    ----------------------To get site own config  
      INSERT      INTO #Volumes
                  ( 
                   Client_Hier_Id
                  ,METER_ID
                  ,Service_Month
                  ,Contract_Volume
                  ,Uom_Id )
                  SELECT
                        cha.Client_Hier_Id
                       ,cmv.METER_ID
                       ,CAST(cmv.MONTH_IDENTIFIER AS DATE) AS Service_Month
                       ,CASE WHEN frq.ENTITY_NAME = 'Daily' THEN cmv.VOLUME * cuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM
                             ELSE cmv.VOLUME * cuc.CONVERSION_FACTOR
                        END AS Contract_Volume
                       ,ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID) AS Uom_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Trade.RM_Client_Hier_Onboard chob
                              ON ch.Client_Hier_Id = chob.Client_Hier_Id
                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                              ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                        INNER JOIN dbo.ENTITY et
                              ON et.ENTITY_ID = chhc.Hedge_Type_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                              ON cha.Meter_Id = cmv.METER_ID
                                 AND cha.Supplier_Contract_ID = cmv.CONTRACT_ID
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID)
                        INNER JOIN dbo.ENTITY frq
                              ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
                        INNER JOIN meta.DATE_DIM dd
                              ON dd.DATE_D = cmv.MONTH_IDENTIFIER
                  WHERE
                        ch.Site_Id > 0
                        AND ch.Client_Id = @Client_Id
                        AND chob.Commodity_Id = @Commodity_Id
                        AND ( @Start_Dt BETWEEN chhc.Config_Start_Dt
                                        AND     chhc.Config_End_Dt
                              OR @End_Dt BETWEEN chhc.Config_Start_Dt
                                         AND     chhc.Config_End_Dt
                              OR chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                              OR chhc.Config_End_Dt BETWEEN @Start_Dt AND @End_Dt )
                        AND ( ( @Hedge_Type_Input = 'Physical'
                                AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ) )
                              OR ( @Hedge_Type_Input = 'Financial'
                                   AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' ) ) )
                        --AND EXISTS ( SELECT
                        --                  1
                        --             FROM
                        --                  dbo.ufn_split(@Contract_Id, ',') con
                        --             WHERE
                        --                  cha.Supplier_Contract_ID = CAST(Segments AS INT) )
                        AND ( @Hedge_Contract_Sites_Cnt = 0
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                @Trade_tvp_Hedge_Contract_Sites hcs
                                          WHERE
                                                cha.Supplier_Contract_ID = hcs.Contract_Id
                                                AND ch.Site_Id = hcs.Site_Id ) )
                        AND CAST(cmv.MONTH_IDENTIFIER AS DATE) BETWEEN @Start_Dt
                                                               AND     @End_Dt
                        AND cmv.MONTH_IDENTIFIER BETWEEN chhc.Config_Start_Dt
                                                 AND     chhc.Config_End_Dt
                  GROUP BY
                        cha.Client_Hier_Id
                       ,cmv.METER_ID
                       ,CAST(cmv.MONTH_IDENTIFIER AS DATE)
                       ,cmv.VOLUME * cuc.CONVERSION_FACTOR
                       ,ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID)
                       ,dd.DAYS_IN_MONTH_NUM
                       ,frq.ENTITY_NAME;



    ----------------------To get default config                         
      INSERT      INTO #Volumes
                  ( 
                   Client_Hier_Id
                  ,METER_ID
                  ,Service_Month
                  ,Contract_Volume
                  ,Uom_Id )
                  SELECT
                        cha.Client_Hier_Id
                       ,cmv.METER_ID
                       ,CAST(cmv.MONTH_IDENTIFIER AS DATE) AS Service_Month
                       ,CASE WHEN frq.ENTITY_NAME = 'Daily' THEN cmv.VOLUME * cuc.CONVERSION_FACTOR * dd.DAYS_IN_MONTH_NUM
                             ELSE cmv.VOLUME * cuc.CONVERSION_FACTOR
                        END AS Contract_Volume
                       ,ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID) AS Uom_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier chclient
                              ON ch.Client_Id = chclient.Client_Id
                        INNER JOIN Trade.RM_Client_Hier_Onboard chob
                              ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                                 AND ch.Country_Id = chob.Country_Id
                        INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                              ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                        INNER JOIN dbo.ENTITY et
                              ON et.ENTITY_ID = chhc.Hedge_Type_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                              ON cha.Meter_Id = cmv.METER_ID
                                 AND cha.Supplier_Contract_ID = cmv.CONTRACT_ID
                        INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                              ON cuc.BASE_UNIT_ID = cmv.UNIT_TYPE_ID
                                 AND cuc.CONVERTED_UNIT_ID = ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID)
                        INNER JOIN dbo.ENTITY frq
                              ON cmv.FREQUENCY_TYPE_ID = frq.ENTITY_ID
                        INNER JOIN meta.DATE_DIM dd
                              ON dd.DATE_D = cmv.MONTH_IDENTIFIER
                  WHERE
                        ch.Site_Id > 0
                        AND ch.Client_Id = @Client_Id
                        AND chclient.Sitegroup_Id = 0
                        AND chob.Commodity_Id = @Commodity_Id
                        AND ( @Start_Dt BETWEEN chhc.Config_Start_Dt
                                        AND     chhc.Config_End_Dt
                              OR @End_Dt BETWEEN chhc.Config_Start_Dt
                                         AND     chhc.Config_End_Dt
                              OR chhc.Config_Start_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                              OR chhc.Config_End_Dt BETWEEN @Start_Dt AND @End_Dt )
                        AND ( ( @Hedge_Type_Input = 'Physical'
                                AND et.ENTITY_NAME IN ( 'Physical', 'Physical & Financial' ) )
                              OR ( @Hedge_Type_Input = 'Financial'
                                   AND et.ENTITY_NAME IN ( 'Financial', 'Physical & Financial' ) ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Trade.RM_Client_Hier_Onboard siteob1
                                         WHERE
                                          siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                          AND siteob1.Commodity_Id = chob.Commodity_Id )
                        --AND EXISTS ( SELECT
                        --                  1
                        --             FROM
                        --                  dbo.ufn_split(@Contract_Id, ',') con
                        --             WHERE
                        --                  cha.Supplier_Contract_ID = CAST(Segments AS INT) )
                        AND ( @Hedge_Contract_Sites_Cnt = 0
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                @Trade_tvp_Hedge_Contract_Sites hcs
                                          WHERE
                                                cha.Supplier_Contract_ID = hcs.Contract_Id
                                                AND ch.Site_Id = hcs.Site_Id ) )
                        AND CAST(cmv.MONTH_IDENTIFIER AS DATE) BETWEEN @Start_Dt
                                                               AND     @End_Dt
                        AND cmv.MONTH_IDENTIFIER BETWEEN chhc.Config_Start_Dt
                                                 AND     chhc.Config_End_Dt
                  GROUP BY
                        cha.Client_Hier_Id
                       ,cmv.METER_ID
                       ,CAST(cmv.MONTH_IDENTIFIER AS DATE)
                       ,cmv.VOLUME * cuc.CONVERSION_FACTOR
                       ,ISNULL(chob.RM_Forecast_UOM_Type_Id, cmv.UNIT_TYPE_ID)
                       ,dd.DAYS_IN_MONTH_NUM
                       ,frq.ENTITY_NAME;

    --SELECT  
    --      @Common_Uom_Id = MAX(Uom_Id)  
    --FROM  
    --      #Volumes      
      
         
      SELECT
            dd.DATE_D AS Service_Month
           ,CASE WHEN @Deal_Ticket_Frequency = 'Monthly' THEN ISNULL(CAST(SUM(vol.Contract_Volume * cuc.CONVERSION_FACTOR) AS DECIMAL(28, 0)), 0)
                 WHEN @Deal_Ticket_Frequency = 'Daily' THEN ISNULL(CAST(SUM(vol.Contract_Volume * cuc.CONVERSION_FACTOR) / dd.DAYS_IN_MONTH_NUM AS DECIMAL(28, 0)), 0)
            END AS Contract_Volume
           ,@Contract_Not_Entire_Hedge_Period AS Contract_Not_Entire_Hedge_Period
      FROM
            meta.DATE_DIM dd
            LEFT JOIN #Volumes vol
                  ON dd.DATE_D = vol.Service_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                  ON cuc.BASE_UNIT_ID = vol.Uom_Id
                     AND cuc.CONVERTED_UNIT_ID = ISNULL(@Uom_Id, @Common_Uom_Id)
      WHERE
            dd.DATE_D BETWEEN @Start_Dt AND @End_Dt
      GROUP BY
            dd.DATE_D
           ,dd.DAYS_IN_MONTH_NUM;
           
      DROP TABLE #Volumes;

END;




GO
GRANT EXECUTE ON  [dbo].[RM_Contract_Volumes_Sel_For_Deal_Ticket] TO [CBMSApplication]
GO
