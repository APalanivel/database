SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.Make_Account_Managed_Expected_For_Site

DESCRIPTION:

	Used to update all the accounts of a site and make the account managed and managed.

	Making an accoung managed and expected has to do following activities as well
		- Delete the account from Do not track list
		- Update account table  not managed = 0 and not expected = 0
		- Update the invoice participation as Managed	(Procedure cbmsinvoiceparticipationqueue_save with the Event_type 17/19 will do this part)
		- Update invoice participation as Expected		(Procedure cbmsinvoiceparticipationqueue_save with the Event_type 18/20 will do this part)
  
INPUT PARAMETERS:
	Name	             DataType          Default     Description
---------------------------------------------------------------------------------
	@Site_id             INT
	@User_Info_Id	     INT


OUTPUT PARAMETERS:
	Name              DataType          Default     Description
---------------------------------------------------------------   

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Make_Account_Managed_Expected_For_Site  45,49
	ROLLBACK TRAN
  
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
  PNR		PANDARINATH

MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
   PNR		06/15/2010  Created

******/

CREATE PROCEDURE dbo.Make_Account_Managed_Expected_For_Site
	 @Site_Id		INT
	,@User_Info_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Site_Account_List TABLE(Account_Id INT PRIMARY KEY CLUSTERED,Account_Type VARCHAR(30))

	DECLARE  @Account_Id			INT
			,@Account_Type			VARCHAR(30)
			,@Managed_Event_Type	INT
			,@Expected_Event_Type	INT
	
	INSERT INTO @Site_Account_List(Account_Id
		, Account_Type)
	SELECT
		uacc.ACCOUNT_ID
		,ent.ENTITY_NAME
    FROM
        dbo.Account uacc
        JOIN dbo.ENTITY ent
             ON ent.ENTITY_ID = uacc.ACCOUNT_TYPE_ID
    WHERE
        uacc.SITE_ID = @Site_Id
        AND ent.ENTITY_NAME = 'UTILITY'

    INSERT INTO @Site_Account_List(Account_Id
		, Account_Type)
    SELECT
		samm.ACCOUNT_ID
		,at.ENTITY_NAME
    FROM
        dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        JOIN dbo.Meter m
             ON m.METER_ID = samm.METER_ID
        JOIN dbo.ACCOUNT utacc
             ON utacc.ACCOUNT_ID = m.ACCOUNT_ID
        JOIN dbo.Account sacc
             ON sacc.ACCOUNT_ID = samm.ACCOUNT_ID
        JOIN dbo.ENTITY at
             ON at.ENTITY_ID = sacc.ACCOUNT_TYPE_ID
    WHERE
        utacc.SITE_ID = @Site_Id
	GROUP BY
		 samm.ACCOUNT_ID
		,at.ENTITY_NAME
		
	BEGIN TRY
		BEGIN TRAN
	
			DELETE
				dnt
			FROM
				dbo.DO_NOT_TRACK dnt
				JOIN @Site_Account_List sal
					ON sal.Account_Id = dnt.Account_Id

			UPDATE dbo.ACCOUNT
				   SET NOT_MANAGED = 0
					   ,NOT_EXPECTED = 0
					   ,NOT_EXPECTED_BY_ID = NULL
					   ,NOT_EXPECTED_DATE = NULL
			FROM
				dbo.ACCOUNT acc
				JOIN @Site_Account_List sacc
					ON sacc.Account_Id = acc.ACCOUNT_ID

			WHILE EXISTS(SELECT 1 FROM @Site_Account_List)
			BEGIN

				SELECT TOP 1 @Account_Id = Account_Id,@Account_Type = Account_Type FROM @Site_Account_List

				SELECT
					@Managed_Event_Type = CASE
											WHEN @Account_Type = 'Utility' THEN 17
										  ELSE 19
										  END
					,@Expected_Event_Type = CASE
												WHEN @Account_Type = 'Utility' THEN 18
											ELSE 20
											END

				EXEC dbo.cbmsinvoiceparticipationqueue_save
					  @user_info_id
					, @Managed_Event_Type  
					, null
					, null
					, null
					, @account_id
					, null
					, 1
					
				EXEC dbo.cbmsinvoiceparticipationqueue_save   
					  @user_info_id
					, @Expected_Event_Type 
					, null
					, null
					, null
					, @account_id
					, null
					, 1
					
				DELETE 
					@Site_Account_List
				WHERE 
					Account_Id = @Account_Id
					AND Account_Type = @Account_Type

			END
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END

		EXEC dbo.usp_RethrowError
	END CATCH
END
GO
GRANT EXECUTE ON  [dbo].[Make_Account_Managed_Expected_For_Site] TO [CBMSApplication]
GO
