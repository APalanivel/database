SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:      
      dbo.[Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id]    
       
 DESCRIPTION:       
      Gets the Test details based on commodity.    
          
 INPUT PARAMETERS:      
 Name    DataType   Default    Description      
----------------------------------------------------------------------------     
 @Commodity_Id  INT    
    
 OUTPUT PARAMETERS:    
 Name    DataType   Default    Description      
----------------------------------------------------------------------------     
 USAGE EXAMPLES:      
----------------------------------------------------------------------------     
    
Cu Invoice    
    
75251689    
75251688    
75251687    
75251686    
    
    
EXEC dbo.Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id    
  @Cu_Invoice_Id = 75251686    
        
    
EXEC dbo.Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id    
    @Cu_Invoice_Id = 75251764    
    
EXEC dbo.Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id    
    @Cu_Invoice_Id = 75251765     
    
EXEC dbo.Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id    
    @Cu_Invoice_Id = 75251735     
  
 exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251854   
  
 exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251880   
 exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251883  
 exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251884  
 exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251854   
  
   
exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251880     
exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251890  


exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251918  --is_fail only 0

exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251782 --is fail 1
exec Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id 75251737

  
    
     
AUTHOR INITIALS:    
Initials Name    
----------------------------------------------------------------------------     
 NR      Narayana Reddy   
 SP      Srinivas Patchava 
 
     
    
 MODIFICATIONS       
 Initials  Date     Modification    
----------------------------------------------------------------------------    
 NR        2019-03-28  Created for Data Quality Exception.   
 SP        2019-08-06  MAINT-9105 modify the invoice_Service_Month and Begin date and End date column
 SP        2019-08-29  MAINT-9149 Added to the Is_Data_Quality_Test_Failed and Is_Data_Quality_Test Columns
 HG			2019-09-13	MAINT-9302, Invoice_Id filter was applied on wrong column, corrected that
******/
CREATE PROCEDURE [dbo].[Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id]
     (
         @Cu_Invoice_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Cu_Invoice_Service_Month
             (
                 Account_Id INT
                 , Service_Month DATE
             );


        CREATE TABLE #CISM_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID INT
                 , CBMS_IMAGE_ID INT
                 , IS_REPORTED BIT
                 , SERVICE_MONTH DATETIME
                 , Begin_Dt DATETIME
                 , End_Dt DATETIME
                 , Account_Group_Id INT
             );

        CREATE TABLE #IP_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID INT
                 , CBMS_IMAGE_ID INT
                 , IS_REPORTED BIT
                 , SERVICE_MONTH DATETIME
                 , Begin_Dt DATETIME
                 , End_Dt DATETIME
                 , Account_Group_Id INT
                 , Is_Previously_Reported BIT
             );



        CREATE TABLE #Final_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID INT
                 , CBMS_IMAGE_ID INT
                 , IS_REPORTED BIT
                 , SERVICE_MONTH DATETIME
                 , Begin_Dt DATETIME
                 , End_Dt DATETIME
                 , Account_Group_Id INT
                 , Is_Previously_Reported BIT
             );


        INSERT INTO #Cu_Invoice_Service_Month
             (
                 Account_Id
                 , Service_Month
             )
        SELECT
            cism.Account_ID
            , DATEADD(MM, -1, MIN(cism.SERVICE_MONTH)) AS Service_Month
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cism.Account_ID
        UNION
        SELECT
            cism.Account_ID
            , MAX(cism.SERVICE_MONTH) AS Service_Month
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cism.Account_ID
        UNION
        SELECT
            cism.Account_ID
            , DATEADD(MM, 1, MAX(cism.SERVICE_MONTH)) AS Service_Month
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
        WHERE
            cism.CU_INVOICE_ID = @Cu_Invoice_Id
        GROUP BY
            cism.Account_ID;


        INSERT INTO #CISM_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID
                 , CBMS_IMAGE_ID
                 , IS_REPORTED
                 , SERVICE_MONTH
                 , Begin_Dt
                 , End_Dt
                 , Account_Group_Id
             )
        SELECT
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ci.IS_REPORTED
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID
        FROM
            dbo.CU_INVOICE ci
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            #Cu_Invoice_Service_Month cu
                       WHERE
                            cu.Account_Id = cism.Account_ID
                            AND cism.SERVICE_MONTH = cu.Service_Month)
        GROUP BY
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ci.IS_REPORTED
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID;


        INSERT INTO #CISM_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID
                 , CBMS_IMAGE_ID
                 , IS_REPORTED
                 , SERVICE_MONTH
                 , Begin_Dt
                 , End_Dt
                 , Account_Group_Id
             )
        SELECT
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ci.IS_REPORTED
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID
        FROM
            dbo.CU_INVOICE ci
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
        WHERE
            ci.CU_INVOICE_ID = @Cu_Invoice_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #CISM_Adjacent_Cu_Invoices cm
                               WHERE
                                    cm.CU_INVOICE_ID = cism.CU_INVOICE_ID
                                    AND cm.SERVICE_MONTH = cism.SERVICE_MONTH)
        GROUP BY
            ci.CU_INVOICE_ID
            , ci.CBMS_IMAGE_ID
            , ci.IS_REPORTED
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID;


        INSERT INTO #IP_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID
                 , CBMS_IMAGE_ID
                 , IS_REPORTED
                 , SERVICE_MONTH
                 , Begin_Dt
                 , End_Dt
                 , Account_Group_Id
                 , Is_Previously_Reported
             )
        SELECT
            ipi.Cu_Invoice_Id
            , ci.CBMS_IMAGE_ID
            , ipi.Is_Reported
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID
            , ipi.Is_Previously_Reported
        FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN dbo.IP_Processing_Invoice ipi
                ON ipi.Cu_Invoice_Id = cism.CU_INVOICE_ID
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = ipi.Cu_Invoice_Id
        WHERE
            ipi.Processing_Cu_Invoice_Id = @Cu_Invoice_Id
        GROUP BY
            ipi.Cu_Invoice_Id
            , ci.CBMS_IMAGE_ID
            , ipi.Is_Reported
            , cism.SERVICE_MONTH
            , cism.Begin_Dt
            , cism.End_Dt
            , ci.ACCOUNT_GROUP_ID
            , ipi.Is_Previously_Reported;

        INSERT INTO #Final_Adjacent_Cu_Invoices
             (
                 CU_INVOICE_ID
                 , CBMS_IMAGE_ID
                 , IS_REPORTED
                 , SERVICE_MONTH
                 , Begin_Dt
                 , End_Dt
                 , Account_Group_Id
                 , Is_Previously_Reported
             )
        SELECT
            iaci.CU_INVOICE_ID
            , iaci.CBMS_IMAGE_ID
            , iaci.IS_REPORTED
            , iaci.SERVICE_MONTH
            , iaci.Begin_Dt
            , iaci.End_Dt
            , iaci.Account_Group_Id
            , iaci.Is_Previously_Reported
        FROM
            #IP_Adjacent_Cu_Invoices iaci
        UNION
        SELECT
            cism.CU_INVOICE_ID
            , cism.CBMS_IMAGE_ID
            , cism.IS_REPORTED
            , cism2.SERVICE_MONTH
            , cism2.Begin_Dt
            , cism2.End_Dt
            , cism.Account_Group_Id
            , cism.IS_REPORTED AS Is_Previously_Reported
        FROM
            #CISM_Adjacent_Cu_Invoices cism
            INNER JOIN dbo.CU_INVOICE_PROCESS cip
                ON cip.CU_INVOICE_ID = cism.CU_INVOICE_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism2
                ON cism2.CU_INVOICE_ID = cism.CU_INVOICE_ID
        WHERE
            NOT EXISTS (SELECT  1 FROM  #IP_Adjacent_Cu_Invoices iaci);






        SELECT
            faci.CU_INVOICE_ID
            , faci.CBMS_IMAGE_ID
            , faci.IS_REPORTED
            , CASE WHEN LEN(ci.SERVICE_MONTH) > 1 THEN LEFT(ci.SERVICE_MONTH, LEN(ci.SERVICE_MONTH) - 1)
                  ELSE ci.SERVICE_MONTH
              END AS Invoice_Service_Month
            , MIN(faci.Begin_Dt) AS Begin_Dt
            , MAX(faci.End_Dt) AS End_Dt
            , faci.Account_Group_Id
            , faci.Is_Previously_Reported
            , MAX(CASE WHEN idql.Is_Failure = 1
                            AND idql.Is_Closed = 0 THEN 1
                      ELSE 0
                  END) AS Is_Data_Quality_Test_Failed
            , MAX(CAST(Is_Data_Quality_Test AS INT)) AS Is_Data_Quality_Test
        FROM
            #Final_Adjacent_Cu_Invoices faci
            LEFT JOIN LOGDB.dbo.Invoice_Data_Quality_Log idql
                ON idql.Cu_Invoice_Id = faci.CU_INVOICE_ID
            LEFT JOIN dbo.CU_INVOICE_PROCESS cip
                ON cip.CU_INVOICE_ID = faci.CU_INVOICE_ID
            CROSS APPLY
        (   SELECT
                CAST(CAST(fac.SERVICE_MONTH AS DATE) AS VARCHAR(100)) + ','
            FROM
                #Final_Adjacent_Cu_Invoices fac
            WHERE
                fac.CU_INVOICE_ID = faci.CU_INVOICE_ID
            GROUP BY
                fac.SERVICE_MONTH
            FOR XML PATH('')) ci(SERVICE_MONTH)
        GROUP BY
            faci.CU_INVOICE_ID
            , faci.CBMS_IMAGE_ID
            , faci.IS_REPORTED
            , CASE WHEN LEN(ci.SERVICE_MONTH) > 1 THEN LEFT(ci.SERVICE_MONTH, LEN(ci.SERVICE_MONTH) - 1)
                  ELSE ci.SERVICE_MONTH
              END
            , faci.Account_Group_Id
            , faci.Is_Previously_Reported
        ORDER BY
            CASE WHEN LEN(ci.SERVICE_MONTH) > 1 THEN LEFT(ci.SERVICE_MONTH, LEN(ci.SERVICE_MONTH) - 1)
                ELSE ci.SERVICE_MONTH
            END;


    END;
GO
GRANT EXECUTE ON  [dbo].[Data_Quality_Adjacent_Cu_Invoices_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
