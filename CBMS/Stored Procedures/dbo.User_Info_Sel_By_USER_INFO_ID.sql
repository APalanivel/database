SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                          
NAME:                          
 dbo.User_info_sel_by_USER_INFO_ID                       
                          
DESCRIPTION:                          
 To Check Mail is sent or not  
                                     
INPUT PARAMETERS:                          
 Name              DataType          Default     Description                          
----------------------------------------------------------------                          
 @User_Info_Id  INT                         
                               
OUTPUT PARAMETERS:                          
 Name              DataType          Default     Description                          
----------------------------------------------------------------                          
                          
USAGE EXAMPLES:                          
------------------------------------------------------------                          
 EXEC dbo.User_info_sel_by_USER_INFO_ID   11960                 
                                   
AUTHOR INITIALS:                          
      Initials    Name           Date                             
------------------------------------------------------------                          
      AS        Arun Skaria      11-April-2014                          
                                           
MODIFICATIONS:                          
 Initials      Date           Modification                          
------------------------------------------------------------                          
  
******/                          
CREATE PROCEDURE dbo.User_Info_Sel_By_USER_INFO_ID ( @User_Info_Id INT )
AS 
BEGIN                          
                          
      SET NOCOUNT ON                          
                   
      SELECT
            New_User_EMail_Ts
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id  
       
                                           
END 
;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Sel_By_USER_INFO_ID] TO [CBMSApplication]
GO
