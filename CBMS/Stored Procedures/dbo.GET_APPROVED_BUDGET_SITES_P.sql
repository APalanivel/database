SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec GET_APPROVED_BUDGET_SITES_P '1','1',1,'01/01/2005','03/01/2005'



CREATE    PROCEDURE dbo.GET_APPROVED_BUDGET_SITES_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@startDate datetime,
@endDate datetime

as
declare @clientCount int

	select @clientCount=COUNT(*) from RM_BUDGET where IS_BUDGET_APPROVED=1
	and (
		(BUDGET_START_MONTH>=@startDate and BUDGET_START_MONTH <=@endDate)
		or 			
		(BUDGET_END_MONTH >= @startDate and BUDGET_END_MONTH <=@endDate)
	    )

	and client_id=@clientId



IF @clientCount<>0
BEGIN

	SELECT 
		div.DIVISION_NAME DIVISION_NAME , sit.SITE_NAME SITE_NAME, 
		CONVERT (Varchar(12), budget.BUDGET_START_MONTH, 101)  START_DATE,
		CONVERT (Varchar(12), budget.BUDGET_END_MONTH, 101)  END_DATE
		  

	FROM 
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join VWSITENAME sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_END_MONTH >= @startDate
			and  budget.BUDGET_END_MONTH <=@endDate)
		)
		AND budget.IS_BUDGET_APPROVED=1
		

END
else
begin
-- this else block is not being used need to be removed--Suresh Reddy
	SELECT 
		div.DIVISION_NAME DIVISION_NAME , sit.SITE_NAME SITE_NAMEs, 
		budget.BUDGET_START_MONTH  START_DATE,
		budget.BUDGET_END_MONTH  END_DATE

	FROM 
		client cli,	
		RM_BUDGET budget 
		left join division div on budget.division_id = div.division_id 
		left join site sit on budget.site_id = sit.site_id 
	WHERE   
		cli.CLIENT_ID=budget.CLIENT_ID
		AND budget.CLIENT_ID=@clientId
		AND 
		(
			(	budget.BUDGET_START_MONTH >= @startDate 
				AND  budget.BUDGET_START_MONTH <=@endDate) 
			or 

			(budget.BUDGET_START_MONTH >= @startDate
			and  budget.BUDGET_START_MONTH <=@endDate)
		)
		AND budget.IS_BUDGET_APPROVED=2

end
GO
GRANT EXECUTE ON  [dbo].[GET_APPROVED_BUDGET_SITES_P] TO [CBMSApplication]
GO
