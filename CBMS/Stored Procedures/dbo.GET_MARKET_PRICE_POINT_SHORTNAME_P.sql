SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      PROCEDURE dbo.GET_MARKET_PRICE_POINT_SHORTNAME_P
@marketPricePoint int

AS
begin

select  market_price_point_short_name as short_name	
 	from market_price_point 	
	where market_price_point_id <> @marketPricePoint
end
GO
GRANT EXECUTE ON  [dbo].[GET_MARKET_PRICE_POINT_SHORTNAME_P] TO [CBMSApplication]
GO
