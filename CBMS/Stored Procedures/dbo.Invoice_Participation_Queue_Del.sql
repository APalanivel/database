SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Invoice_Participation_Queue_Del]  
     
DESCRIPTION: 
	It Deletes Invoice Participation Queue for Selected Invoice Participation Queue Id Which are associated by account.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
---------------------------------------------------------------          
@Invoice_Participation_Queue_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------ 
	BEGIN TRAN
		EXEC Invoice_Participation_Queue_Del  10
	ROLLBACK TRAN

	SELECT TOP 10 * FROM Invoice_Participation_Queue

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------
PNR			27-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Invoice_Participation_Queue_Del
   (
      @Invoice_Participation_Queue_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.INVOICE_PARTICIPATION_QUEUE
	WHERE
		Invoice_Participation_Queue_Id = @Invoice_Participation_Queue_Id

END
GO
GRANT EXECUTE ON  [dbo].[Invoice_Participation_Queue_Del] TO [CBMSApplication]
GO
