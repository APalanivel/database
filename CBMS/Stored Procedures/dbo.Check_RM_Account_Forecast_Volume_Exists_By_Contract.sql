SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:    
   [dbo].[Check_RM_Account_Forecast_Volume_Exists_By_Contract]    

DESCRIPTION:    


INPUT PARAMETERS:    
Name							DataType		Default		Description    
-------------------------------------------------------------------------
 @Contract_Id									INT

OUTPUT PARAMETERS:    
	Name				DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------

SELECT MIN(Service_Month),MAX(Service_Month),Client_Hier_Id FROM dbo.RM_Client_Hier_Forecast_Volume WHERE Client_Hier_Id in(18551,18552,18553)
GROUP BY Client_Hier_Id

SELECT b.CONTRACT_START_DATE,b.CONTRACT_END_DATE,b.CONTRACT_ID FROM Core.Client_Hier_Account a 
	INNER JOIN dbo.CONTRACT b ON a.Supplier_Contract_ID = b.CONTRACT_ID 
	WHERE a.Client_Hier_Id in(18551,18552,18553)

EXEC [dbo].[Check_RM_Account_Forecast_Volume_Exists_By_Contract]   162279


AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy


MODIFICATIONS     
	Initials	Date			Modification    
------------------------------------------------------------    
	RR			2018-09-30		SP Created

******/

CREATE PROCEDURE [dbo].[Check_RM_Account_Forecast_Volume_Exists_By_Contract]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @RM_Account_Forecast_Volume_Exists BIT = 1;

        SELECT
            @RM_Account_Forecast_Volume_Exists = 1
        FROM
            Trade.RM_Client_Hier_Forecast_Volume chfv
            INNER JOIN Core.Client_Hier_Account cha
                ON chfv.Client_Hier_Id = cha.Client_Hier_Id
                   AND  chfv.Commodity_Id = cha.Commodity_Id
            INNER JOIN dbo.CONTRACT con
                ON con.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            cha.Supplier_Contract_ID = @Contract_Id
            AND (   chfv.Service_Month >= con.CONTRACT_START_DATE
                    OR  chfv.Service_Month >= con.CONTRACT_END_DATE);

        SELECT
            @RM_Account_Forecast_Volume_Exists AS RM_Account_Forecast_Volume_Exists;
    END;


GO
GRANT EXECUTE ON  [dbo].[Check_RM_Account_Forecast_Volume_Exists_By_Contract] TO [CBMSApplication]
GO
