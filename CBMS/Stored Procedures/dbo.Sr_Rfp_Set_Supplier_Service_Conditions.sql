SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.Sr_Rfp_Set_Supplier_Service_Conditions

DESCRIPTION: 


INPUT PARAMETERS:    
    Name		DataType          Default     Description    
---------------------------------------------------------------------------------    
	@Sr_Rfp_Id	INT
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	SELECT  count(1) FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Map WHERE SR_RFP_ACCOUNT_TERM_ID = 109481
		AND SR_RFP_SELECTED_PRODUCTS_ID = 1211360
	SELECT * FROM dbo.SR_RFP_TERM_PRODUCT_MAP WHERE SR_RFP_ACCOUNT_TERM_ID = 109481
		AND SR_RFP_SELECTED_PRODUCTS_ID = 1211360
	SELECT * FROM dbo.SR_RFP_ACCOUNT_TERM WHERE SR_ACCOUNT_GROUP_ID = 10011926
	SELECT * FROM dbo.SR_RFP_ACCOUNT WHERE SR_RFP_BID_GROUP_ID = 10011926
	SELECT * FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = 237971
	
	
	SELECT
      c.SR_RFP_ACCOUNT_TERM_ID,c.SR_RFP_SELECTED_PRODUCTS_ID
	FROM
		  dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map a
		  JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response b
				ON a.Sr_Rfp_Bid_Id = b.SR_RFP_BID_ID
		  JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Map c
				ON b.Sr_Rfp_Service_Condition_Template_Question_Map_Id = c.Sr_Rfp_Service_Condition_Template_Question_Map_Id
	WHERE c.Is_Post_To_Supplier = 1

	BEGIN TRANSACTION
	UPDATE dbo.Sr_Rfp_Service_Condition_Template_Question_Map SET Is_Post_To_Supplier = 0 
	WHERE SR_RFP_ACCOUNT_TERM_ID = 109481 AND SR_RFP_SELECTED_PRODUCTS_ID = 1211360
	UPDATE dbo.Sr_Rfp_Service_Condition_Template_Question_Map SET Is_Post_To_Supplier = 0 
	WHERE SR_RFP_ACCOUNT_TERM_ID = 109481 AND SR_RFP_SELECTED_PRODUCTS_ID = 1211361 
	SELECT count(1) FROM dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map
	SELECT count(1) FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	EXEC [dbo].[Sr_Rfp_Set_Supplier_Service_Conditions] 13155,10011926,1,743,532,'Post',16
	SELECT count(1) FROM dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map
	SELECT count(1) FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	ROLLBACK TRANSACTION
	
	SELECT DISTINCT
	c.SR_RFP_ACCOUNT_TERM_ID
	,c.SR_RFP_SELECTED_PRODUCTS_ID
	,f.SR_RFP_ID
	,f.SR_RFP_ACCOUNT_ID
	,g.VENDOR_ID
	,g.SR_SUPPLIER_CONTACT_INFO_ID
	FROM
	dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map a
	JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response b
		ON a.Sr_Rfp_Bid_Id = b.SR_RFP_BID_ID
	JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Map c
		ON b.Sr_Rfp_Service_Condition_Template_Question_Map_Id = c.Sr_Rfp_Service_Condition_Template_Question_Map_Id
	JOIN dbo.SR_RFP_TERM_PRODUCT_MAP d
		ON a.Sr_Rfp_Bid_Id = d.SR_RFP_BID_ID
	JOIN dbo.SR_RFP_ACCOUNT_TERM e
		ON c.SR_RFP_ACCOUNT_TERM_ID = e.SR_RFP_ACCOUNT_TERM_ID
	JOIN dbo.SR_RFP_ACCOUNT f
		ON e.SR_ACCOUNT_GROUP_ID = f.SR_RFP_ACCOUNT_ID
	JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP g
		ON d.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = g.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
	WHERE
	e.IS_BID_GROUP = 0
	AND c.SR_RFP_ACCOUNT_TERM_ID = 110025 AND c.SR_RFP_SELECTED_PRODUCTS_ID = 1211541

	BEGIN TRANSACTION
	UPDATE dbo.Sr_Rfp_Service_Condition_Template_Question_Map SET Is_Post_To_Supplier = 1 
	WHERE SR_RFP_ACCOUNT_TERM_ID = 110025 AND SR_RFP_SELECTED_PRODUCTS_ID = 1211541 
	SELECT count(1) FROM dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map
	SELECT count(1) FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	EXEC [dbo].[Sr_Rfp_Set_Supplier_Service_Conditions] 13326,12104824,0,1294,413,'Post',16
	SELECT count(1) FROM dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map
	SELECT count(1) FROM dbo.Sr_Rfp_Service_Condition_Template_Question_Response
	ROLLBACK TRANSACTION
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-18	Global Sourcing - Phase3 - Manage bid details - Created

******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Set_Supplier_Service_Conditions]
      ( 
       @Sr_Rfp_Id INT
      ,@Account_Group_Id INT
      ,@Is_Bid_Group BIT
      ,@Supplier_Id INT
      ,@Contact_Id INT
      ,@Status VARCHAR(50)
      ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;

      BEGIN 
            DECLARE @Supp_Status INT
            
                 
            SELECT
                  @Supp_Status = ENTITY_ID
            FROM
                  dbo.ENTITY
            WHERE
                  ENTITY_TYPE = 1013
                  AND ENTITY_NAME = 'Post'
                  
            DELETE
                  tqr
            FROM
                  dbo.Sr_Rfp_Service_Condition_Template_Question_Response tqr
                  INNER JOIN dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map suppsc
                        ON tqr.SR_RFP_BID_ID = suppsc.Sr_Rfp_Bid_Id
                           AND tqr.Sr_Rfp_Service_Condition_Template_Question_Map_Id = suppsc.Sr_Rfp_Service_Condition_Template_Question_Map_Id
            WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                              INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                    ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                              INNER JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                    ON term.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                       AND term.IS_BID_GROUP = srss.IS_BID_GROUP
                                       AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
                                    ON srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = scvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                           WHERE
                              scvm.SR_SUPPLIER_CONTACT_INFO_ID = @Contact_Id
                              AND scvm.VENDOR_ID = @Supplier_Id
                              AND scvm.SR_RFP_ID = @Sr_Rfp_Id
                              AND term.SR_ACCOUNT_GROUP_ID = @Account_Group_Id
                              AND term.IS_BID_GROUP = @Is_Bid_Group
                              --AND srss.STATUS_TYPE_ID = @Supp_Status
                              AND suppsc.Sr_Rfp_Bid_Id = tpm.SR_RFP_BID_ID )
                  
            DELETE
                  suppsc
            FROM
                  dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map suppsc
            WHERE
                  EXISTS ( SELECT
                              1
                           FROM
                              dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                              INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                    ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                              INNER JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                    ON term.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                       AND term.IS_BID_GROUP = srss.IS_BID_GROUP
                                       AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
                                    ON srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = scvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                           WHERE
                              scvm.SR_SUPPLIER_CONTACT_INFO_ID = @Contact_Id
                              AND scvm.VENDOR_ID = @Supplier_Id
                              AND scvm.SR_RFP_ID = @Sr_Rfp_Id
                              AND term.SR_ACCOUNT_GROUP_ID = @Account_Group_Id
                              AND term.IS_BID_GROUP = @Is_Bid_Group
                              --AND srss.STATUS_TYPE_ID = @Supp_Status
                              AND suppsc.Sr_Rfp_Bid_Id = tpm.SR_RFP_BID_ID )
                  
            INSERT      INTO dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map
                        ( 
                         Sr_Rfp_Service_Condition_Template_Question_Map_Id
                        ,Sr_Service_Condition_Category_Id
                        ,Category_Display_Seq
                        ,Sr_Service_Condition_Question_Id
                        ,Question_Display_Seq
                        ,Posted_User_Id
                        ,Posted_Ts
                        ,Sr_Rfp_Bid_Id )
                        SELECT
                              tqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                             ,tqm.Sr_Service_Condition_Category_Id
                             ,tqm.Category_Display_Seq
                             ,tqm.Sr_Service_Condition_Question_Id
                             ,tqm.Question_Display_Seq
                             ,@User_Info_Id
                             ,getdate()
                             ,tpm.SR_RFP_BID_ID
                        FROM
                              dbo.Sr_Rfp_Service_Condition_Template_Question_Map tqm
                              INNER  JOIN dbo.SR_RFP_TERM_PRODUCT_MAP tpm
                                    ON tqm.SR_RFP_ACCOUNT_TERM_ID = tpm.SR_RFP_ACCOUNT_TERM_ID
                                       AND tqm.SR_RFP_SELECTED_PRODUCTS_ID = tpm.SR_RFP_SELECTED_PRODUCTS_ID
                              INNER JOIN dbo.SR_RFP_ACCOUNT_TERM term
                                    ON tpm.SR_RFP_ACCOUNT_TERM_ID = term.SR_RFP_ACCOUNT_TERM_ID
                              INNER JOIN dbo.SR_RFP_SEND_SUPPLIER srss
                                    ON term.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                                       AND term.IS_BID_GROUP = srss.IS_BID_GROUP
                                       AND srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = tpm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
                                    ON srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = scvm.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                        WHERE
                              scvm.SR_SUPPLIER_CONTACT_INFO_ID = @Contact_Id
                              AND scvm.VENDOR_ID = @Supplier_Id
                              AND scvm.SR_RFP_ID = @Sr_Rfp_Id
                              AND term.SR_ACCOUNT_GROUP_ID = @Account_Group_Id
                              AND term.IS_BID_GROUP = @Is_Bid_Group
                              AND tqm.Is_Post_To_Supplier = 1
                              AND srss.STATUS_TYPE_ID = @Supp_Status
                              AND @Status = 'Post'
                        GROUP BY
                              tqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                             ,tqm.Sr_Service_Condition_Category_Id
                             ,tqm.Category_Display_Seq
                             ,tqm.Sr_Service_Condition_Question_Id
                             ,tqm.Question_Display_Seq
                             ,tpm.SR_RFP_BID_ID
                              
      END
      
END 
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Set_Supplier_Service_Conditions] TO [CBMSApplication]
GO
