SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[cbmsInvSourcedImageReport_GetForInvSourcedImageTrack]
	( @MyAccountId int
	, @inv_sourced_image_track_id int
	)
AS
BEGIN

	   select inv_sourced_image_report_id
		, inv_sourced_image_track_id
		, inv_sourced_image_id
		, inv_source_id
		, inv_source_label
		, inv_source_keyword
		, original_filename
		, prokarma_filename
		, archive_path
		, sent_date
		, received_date
		, status_type_id
		, status_type
		, account_number
		, cbms_image_id
		, ubm_batch_master_log_id
		, exception_comments
	     from inv_sourced_image_report
	    where inv_sourced_image_track_id = @inv_sourced_image_track_id

END


GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageReport_GetForInvSourcedImageTrack] TO [CBMSApplication]
GO
