SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.ADD_HEDGE_DETAIL_P
	@hedge_id INT,
	@month_identifier DATETIME,
	@previous_hedge_volume DECIMAL(32,16),
	@hedge_volume DECIMAL(32,16),
	@hedge_price DECIMAL(32,16)
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.hedge_detail(hedge_id,
		month_identifier,
		previous_hedge_volume,
		hedge_volume,hedge_price)
	VALUES(@hedge_id
		, @month_identifier
		, @previous_hedge_volume
		, @hedge_volume
		, @hedge_price)

END
GO
GRANT EXECUTE ON  [dbo].[ADD_HEDGE_DETAIL_P] TO [CBMSApplication]
GO
