SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[P2P_u_dboSR_RFP_PRICING_SCOPE01793653639]
		@c1 int = NULL,
		@c2 bit = NULL,
		@c3 varchar(1000) = NULL,
		@c4 bit = NULL,
		@c5 varchar(1000) = NULL,
		@c6 bit = NULL,
		@c7 varchar(1000) = NULL,
		@c8 bit = NULL,
		@c9 varchar(1000) = NULL,
		@c10 bit = NULL,
		@c11 varchar(1000) = NULL,
		@c12 bit = NULL,
		@c13 varchar(1000) = NULL,
		@c14 varchar(1000) = NULL,
		@c15 bit = NULL,
		@c16 varchar(1000) = NULL,
		@c17 int = NULL,
		@c18 int = NULL,
		@c19 int = NULL,
		@c20 int = NULL,
		@c21 int = NULL,
		@c22 int = NULL,
		@c23 int = NULL,
		@c24 uniqueidentifier = NULL,
		@pkc1 int = NULL,
		@bitmap binary(3),
		@MSp2pPreVersion varbinary(32) , @MSp2pPostVersion varbinary(32) 

as
begin  
update [dbo].[SR_RFP_PRICING_SCOPE] set
		[IS_PRICE_INCLUDE_LOSES] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [IS_PRICE_INCLUDE_LOSES] end,
		[LOSS_PERCENT] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [LOSS_PERCENT] end,
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [IS_PRICE_INCLUDE_ANCILIARY_SERVICE] end,
		[ANCILIARY_SERVICE_ESTIMATED_COST] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [ANCILIARY_SERVICE_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [IS_PRICE_INCLUDE_CAPACITY_CHARGES] end,
		[CAPACITY_CHARGES_ESTIMATED_COST] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [CAPACITY_CHARGES_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_ISO_CHARGES] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [IS_PRICE_INCLUDE_ISO_CHARGES] end,
		[ISO_CHARGES_ESTIMATED_COST] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [ISO_CHARGES_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_TRANSMISSION] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [IS_PRICE_INCLUDE_TRANSMISSION] end,
		[TRANSMISSION_ESTIMATED_COST] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [TRANSMISSION_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_TAXES] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [IS_PRICE_INCLUDE_TAXES] end,
		[TAXES_ESTIMATED_COST] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [TAXES_ESTIMATED_COST] end,
		[OTHER_CHARGES] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [OTHER_CHARGES] end,
		[IS_DEMAND_CAP_APPLICABLE] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [IS_DEMAND_CAP_APPLICABLE] end,
		[CAP_FLOOR_PRICE] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [CAP_FLOOR_PRICE] end,
		[PRICE_INCLUDE_LOSES_TYPE_ID] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [PRICE_INCLUDE_LOSES_TYPE_ID] end,
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID] end,
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID] end,
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [PRICE_INCLUDE_ISO_CHARGES_TYPE_ID] end,
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [PRICE_INCLUDE_TRANSMISSION_TYPE_ID] end,
		[PRICE_INCLUDE_TAXES_TYPE_ID] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [PRICE_INCLUDE_TAXES_TYPE_ID] end,
		[DEMAND_CAP_APPLICABLE_TYPE_ID] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [DEMAND_CAP_APPLICABLE_TYPE_ID] end,
		[msrepl_tran_version] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_PRICING_SCOPE_ID] = @pkc1
		and ($sys_p2p_cd_id = @MSp2pPreVersion or $sys_p2p_cd_id is null)
if @@rowcount = 0
begin  
	declare @cur_version varbinary(32) 
		,@conflict_type int = 1
		,@conflict_type_txt nvarchar(20) = N'Update-Update'
		,@reason_code int = 1
		,@reason_text nvarchar(720) = NULL
		,@is_on_disk_winner bit = 0
		,@is_incoming_winner bit = 0
		,@peer_id_current_node int = NULL
		,@peer_id_incoming int
		,@tranid_incoming nvarchar(40)
		,@peer_id_on_disk int
		,@tranid_on_disk nvarchar(40)
	select @peer_id_incoming = sys.fn_replvarbintoint(@MSp2pPostVersion)
		,@tranid_incoming = sys.fn_replp2pversiontotranid(@MSp2pPostVersion)
	select @cur_version = $sys_p2p_cd_id from [dbo].[SR_RFP_PRICING_SCOPE] 
where [SR_RFP_PRICING_SCOPE_ID] = @pkc1
	if @@rowcount = 0  
			select @conflict_type = 3, @conflict_type_txt = N'Update-Delete', @is_on_disk_winner = 1, @reason_text = formatmessage(22823), @reason_code = 0
	else 
	begin  
		select @peer_id_on_disk = sys.fn_replvarbintoint(@cur_version)
			,@tranid_on_disk = sys.fn_replp2pversiontotranid(@cur_version)
		if(@peer_id_incoming > @peer_id_on_disk)
			set @is_incoming_winner = 1
		else
			set @is_on_disk_winner = 1
	end  
		select @peer_id_current_node = p.originator_id from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_PRICING_SCOPE]') and p.options & 0x1 = 0x1
	if (@peer_id_current_node is not null) 
	begin 
		if (@reason_text is NULL)
			if (@peer_id_incoming > @peer_id_on_disk)
			begin  
				select @reason_text = formatmessage(22822,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
			else  
			begin  
				select @reason_text = formatmessage(22821,@peer_id_incoming,@peer_id_on_disk,@peer_id_current_node)
			end  
		create table #change_id (change_id varbinary(8))
		insert [dbo].[conflict_dbo_SR_RFP_PRICING_SCOPE] (
		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]
			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$update_bitmap
			,__$pre_version)
		output inserted.__$row_id into #change_id
		select 
    @pkc1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6,
    @c7,
    @c8,
    @c9,
    @c10,
    @c11,
    @c12,
    @c13,
    @c14,
    @c15,
    @c16,
    @c17,
    @c18,
    @c19,
    @c20,
    @c21,
    @c22,
    @c23,
    @c24			,@peer_id_current_node
			,@peer_id_incoming
			,@tranid_incoming
			,@conflict_type
			,@is_incoming_winner
			,@reason_code
			,@reason_text
			,@bitmap
			,@MSp2pPreVersion
		insert [dbo].[conflict_dbo_SR_RFP_PRICING_SCOPE] (

		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]			,__$originator_id
			,__$origin_datasource
			,__$tranid
			,__$conflict_type
			,__$is_winner
			,__$reason_code
			,__$reason_text
			,__$pre_version
			,__$change_id)
		select 

		[SR_RFP_PRICING_SCOPE_ID],
		[IS_PRICE_INCLUDE_LOSES],
		[LOSS_PERCENT],
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE],
		[ANCILIARY_SERVICE_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES],
		[CAPACITY_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_ISO_CHARGES],
		[ISO_CHARGES_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TRANSMISSION],
		[TRANSMISSION_ESTIMATED_COST],
		[IS_PRICE_INCLUDE_TAXES],
		[TAXES_ESTIMATED_COST],
		[OTHER_CHARGES],
		[IS_DEMAND_CAP_APPLICABLE],
		[CAP_FLOOR_PRICE],
		[PRICE_INCLUDE_LOSES_TYPE_ID],
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID],
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID],
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID],
		[PRICE_INCLUDE_TAXES_TYPE_ID],
		[DEMAND_CAP_APPLICABLE_TYPE_ID],
		[msrepl_tran_version]			,@peer_id_current_node
			,@peer_id_on_disk
			,@tranid_on_disk
			,@conflict_type
			,@is_on_disk_winner
			,@reason_code
			,@reason_text
			,NULL
			,(select change_id from #change_id)
		from [dbo].[SR_RFP_PRICING_SCOPE] 

where [SR_RFP_PRICING_SCOPE_ID] = @pkc1
	end 
	if(@peer_id_incoming > @peer_id_on_disk)
	begin  
update [dbo].[SR_RFP_PRICING_SCOPE] set
		[IS_PRICE_INCLUDE_LOSES] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [IS_PRICE_INCLUDE_LOSES] end,
		[LOSS_PERCENT] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [LOSS_PERCENT] end,
		[IS_PRICE_INCLUDE_ANCILIARY_SERVICE] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [IS_PRICE_INCLUDE_ANCILIARY_SERVICE] end,
		[ANCILIARY_SERVICE_ESTIMATED_COST] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [ANCILIARY_SERVICE_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_CAPACITY_CHARGES] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [IS_PRICE_INCLUDE_CAPACITY_CHARGES] end,
		[CAPACITY_CHARGES_ESTIMATED_COST] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [CAPACITY_CHARGES_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_ISO_CHARGES] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [IS_PRICE_INCLUDE_ISO_CHARGES] end,
		[ISO_CHARGES_ESTIMATED_COST] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [ISO_CHARGES_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_TRANSMISSION] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [IS_PRICE_INCLUDE_TRANSMISSION] end,
		[TRANSMISSION_ESTIMATED_COST] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [TRANSMISSION_ESTIMATED_COST] end,
		[IS_PRICE_INCLUDE_TAXES] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [IS_PRICE_INCLUDE_TAXES] end,
		[TAXES_ESTIMATED_COST] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [TAXES_ESTIMATED_COST] end,
		[OTHER_CHARGES] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [OTHER_CHARGES] end,
		[IS_DEMAND_CAP_APPLICABLE] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [IS_DEMAND_CAP_APPLICABLE] end,
		[CAP_FLOOR_PRICE] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [CAP_FLOOR_PRICE] end,
		[PRICE_INCLUDE_LOSES_TYPE_ID] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [PRICE_INCLUDE_LOSES_TYPE_ID] end,
		[PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [PRICE_INCLUDE_ANCILIARY_SERVICE_TYPE_ID] end,
		[PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [PRICE_INCLUDE_CAPACITY_CHARGES_TYPE_ID] end,
		[PRICE_INCLUDE_ISO_CHARGES_TYPE_ID] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [PRICE_INCLUDE_ISO_CHARGES_TYPE_ID] end,
		[PRICE_INCLUDE_TRANSMISSION_TYPE_ID] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [PRICE_INCLUDE_TRANSMISSION_TYPE_ID] end,
		[PRICE_INCLUDE_TAXES_TYPE_ID] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [PRICE_INCLUDE_TAXES_TYPE_ID] end,
		[DEMAND_CAP_APPLICABLE_TYPE_ID] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [DEMAND_CAP_APPLICABLE_TYPE_ID] end,
		[msrepl_tran_version] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [msrepl_tran_version] end		,$sys_p2p_cd_id = @MSp2pPostVersion

where [SR_RFP_PRICING_SCOPE_ID] = @pkc1
	end  
		if exists(select * from syspublications p join sysarticles a on a.pubid = p.pubid where a.objid = object_id(N'[dbo].[SR_RFP_PRICING_SCOPE]') and p.options & 0x10 = 0x10)
			raiserror(22815, 10, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
		else
			raiserror(22815, 16, -1, @conflict_type_txt, @peer_id_current_node, @peer_id_incoming, @tranid_incoming, @peer_id_on_disk, @tranid_on_disk) with log
end 
end 
GO
