SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Site_Commodity_Analyst_Del      
     
DESCRIPTION: Deletes Analyst for a given site and commodity from Site_Commodity_Analyst table.
    
INPUT PARAMETERS:        
      Name					DataType          Default     Description        
------------------------------------------------------------        
	  @Site_Id          	INT
      @Commodity_Id         INT
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   

BEGIN TRAN
EXEC DBO.Site_Commodity_Analyst_Del 100,290
ROLLBACK TRAN

------------------------------------------------------------  
    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKR		 Ashok Kumar Raju
    
    
Initials Date		Modification      
------------------------------------------------------------      
AKR		2012-09-28  Created new sp (for POCO Project)
******/
CREATE PROCEDURE dbo.Site_Commodity_Analyst_Del
      ( 
       @Site_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
      
      DELETE
            sca
      FROM
            dbo.Site_Commodity_Analyst sca
      WHERE
            sca.Site_Id = @Site_Id
            AND sca.Commodity_Id = @Commodity_Id
                  
END
;
GO
GRANT EXECUTE ON  [dbo].[Site_Commodity_Analyst_Del] TO [CBMSApplication]
GO
