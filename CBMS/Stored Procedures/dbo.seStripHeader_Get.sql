SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Procedure dbo.seStripHeader_Get
	(
		@StripHeaderId int
	)
AS
BEGIN
	set nocount on
	 select StripHeaderId
				, AccountId
				, StripName
				, ExpirationDate
				, UniformVolume
		 from seStripHeader
		where StripHeaderId = @StripHeaderId

END
GO
GRANT EXECUTE ON  [dbo].[seStripHeader_Get] TO [CBMSApplication]
GO
