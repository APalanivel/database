SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.BaseAccountMeterData_Get_By_Client             
                        
 DESCRIPTION:                        
			this procedure is to get all active clients Account meter information except AT&T
                        
 INPUT PARAMETERS:                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

                        
 OUTPUT PARAMETERS:                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      

                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 EXEC [BaseAccountMeterData_Get] 
                       
 AUTHOR INITIALS:       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
   KVK					VInay K  
                         
 MODIFICATIONS:          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
    KVK					03/09/2017		Created        
    KVK					11/01/2017		modified to add site type                
	KVK					09/26/2019		modified to add Site_ID and Geo Lat and Geo Long
******/

CREATE PROCEDURE [dbo].[BaseAccountMeterData_Get]
AS
BEGIN

      SET NOCOUNT ON;

      ;
      WITH Transport_Meter_CTE
      AS ( SELECT DISTINCT METER_ID
           FROM
                  dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                  JOIN CONTRACT c
                        ON c.CONTRACT_ID = samm.Contract_ID
           WHERE
                  getdate()
                  BETWEEN c.CONTRACT_START_DATE AND c.CONTRACT_END_DATE
                  AND c.CONTRACT_TYPE_ID = 153 -- supply
      )
      SELECT DISTINCT Client_Name [Client Name]
           , Region_Name [Region]
           , State_Name [State]
           , City [City]
           , Site_name [Site Name]
           , com.Commodity_Name [Commodity]
           , cha.Account_Vendor_Name [Vendor]
           , cha.Rate_Name [Rate]
           , cha.Display_Account_Number [Account Number]
           , cha.Meter_Number [Meter Number]
           , ch.Client_Id [Client ID]
           , cha.Account_Vendor_Id [Vendor ID]
           , cha.Rate_Id [Rate ID]
           , cha.Account_Id [Account ID]
           , e1.ENTITY_NAME [Service Level]
           , vcl.Consumption_Level_Desc [Consumption Level]
           , [Currently Transport Meter] = CASE WHEN tc.METER_ID IS NOT NULL THEN 'Transport'
                                                ELSE  'Tariff'
                                           END
           , [Site Not Managed] = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Not Managed'
                                       ELSE  'Managed'
                                  END
           , [Account Not Managed] = CASE WHEN cha.Account_Not_Managed = 1 THEN 'Not Managed'
                                          ELSE  'Managed'
                                     END
           , ch.Site_Type_Name [Site Type]
		   , ch.Site_Id
		   , ch.Geo_Lat
		   , ch.Geo_Long
      FROM
             Core.Client_Hier_Account cha
             JOIN Core.Client_Hier ch
                   ON ch.Client_Hier_Id = cha.Client_Hier_Id
             JOIN ENTITY e
                   ON e.ENTITY_ID = ch.Client_Type_Id
             JOIN dbo.Commodity com
                   ON com.Commodity_Id = cha.Commodity_Id
             JOIN ENTITY e1
                   ON e1.ENTITY_ID = cha.Account_Service_level_Cd
             JOIN dbo.Account_Variance_Consumption_Level avcl
                   ON avcl.ACCOUNT_ID = cha.Account_Id
             JOIN dbo.Variance_Consumption_Level vcl
                   ON vcl.Commodity_Id = com.Commodity_Id
                      AND vcl.Variance_Consumption_Level_Id = avcl.Variance_Consumption_Level_Id
             LEFT JOIN Transport_Meter_CTE tc
                   ON tc.METER_ID = cha.Meter_Id
      WHERE
             e.ENTITY_NAME != 'demo'
             AND ch.Client_Name != 'blackstone'
             AND ch.Client_Not_Managed != 1
             AND cha.Account_Type = 'utility'
             AND ch.Country_Name IN ( 'usa', 'canada' )
             AND ch.Client_Id != 11231 --att
             AND com.Commodity_Id IN ( 290, 291 );

END;

;
GO

GRANT EXECUTE ON  [dbo].[BaseAccountMeterData_Get] TO [CBMSApplication]
GO
