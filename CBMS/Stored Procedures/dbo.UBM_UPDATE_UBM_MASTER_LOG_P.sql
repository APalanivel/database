SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_UPDATE_UBM_MASTER_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@status        	varchar(200)	          	
	@expectedData  	int       	          	
	@actualData    	int       	          	
	@expectedImages	int       	          	
	@actualImages  	int       	          	
	@actualImageBytes	int       	          	
	@expectedAmount	decimal(18,0)	          	
	@actualAmount  	decimal(18,0)	          	
	@masterLogId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE [dbo].[UBM_UPDATE_UBM_MASTER_LOG_P]
   @status varchar(200)
 , @expectedData int
 , @actualData int
 , @expectedImages int
 , @actualImages int
 , @actualImageBytes int
 , @expectedAmount decimal
 , @actualAmount decimal
 , @masterLogId int
AS
set nocount on
   DECLARE @statusTypeId int

   select   @statusTypeId = ( select   entity_id
                              from     entity
                              where    entity_name = @status
                                       and entity_type = 656
                            )

   update   ubm_batch_master_log 
   set      status_type_id = @statusTypeId
          , end_date = getdate()
          , expected_data_records = @expectedData
          , actual_data_records = @actualData
          , expected_image_records = @expectedImages
          , actual_image_records = @actualImages
          , actual_image_bytes = @actualImageBytes
          , expected_dollar_amount = @expectedAmount
          , actual_dollar_amount = @actualAmount
   where    ubm_batch_master_log_id = @masterLogId
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_UBM_MASTER_LOG_P] TO [CBMSApplication]
GO
