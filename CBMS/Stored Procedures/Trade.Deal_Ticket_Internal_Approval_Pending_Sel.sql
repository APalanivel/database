SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Internal_Approval_Pending_Sel
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Internal_Approval_Pending_Sel  
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-03-28	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Internal_Approval_Pending_Sel]
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            dt.Deal_Ticket_Id
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN dbo.USER_INFO cui
                  ON cui.QUEUE_ID = dt.QUEUE_ID
      WHERE
            chws.Is_Active = 1
            AND ws.Workflow_Status_Name = 'Pending Internal Approval'
      GROUP BY
            dt.Deal_Ticket_Id
           
END;
GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Internal_Approval_Pending_Sel] TO [CBMSApplication]
GO
