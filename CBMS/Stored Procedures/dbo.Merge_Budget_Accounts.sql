
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Merge_Budget_Accounts

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Budget_Id		INT
	@Account_Ids	VARCHAR(MAX)
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
	SELECT * FROM dbo.BUDGET_ACCOUNT AS ba WHERE BUDGET_ID = 8552
		EXEC Merge_Budget_Accounts 8552, '1361,1365'
		SELECT * FROM dbo.Budget_Account WHERE Budget_Id = 8552
	ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	CPE			2014-03-11	Created
	RR			2017-05-26	SE2017-26 - Modified to call script dbo.Budget_Account_Queue_Ins_Upd to add/update budget accounts to queue							
******/

CREATE PROCEDURE [dbo].[Merge_Budget_Accounts]
      ( 
       @Budget_Id INT
      ,@Account_Ids VARCHAR(MAX) )
AS 
BEGIN
      SET NOCOUNT ON;

      CREATE TABLE #Account_Id
            ( 
             Account_Id INT PRIMARY KEY ( Account_ID ) )
             
      INSERT      #Account_Id
                  ( 
                   Account_Id )
                  SELECT
                        CONVERT(INT, Segments)
                  FROM
                        dbo.ufn_split(@Account_Ids, ',') AS us
                        

      BEGIN TRY
            BEGIN TRAN
	  -- Set the IS_DELETED flag
            UPDATE
                  ba
            SET   
                  IS_DELETED = 1
            FROM
                  dbo.BUDGET_ACCOUNT AS ba
            WHERE
                  ba.BUDGET_ID = @Budget_Id
                  AND ba.IS_DELETED = 0
                  AND NOT EXISTS ( SELECT
                                    1
                                   FROM
                                    #Account_Id AS ai2
                                   WHERE
                                    Account_Id = ba.ACCOUNT_ID )

            UPDATE
                  ba
            SET   
                  IS_DELETED = 0
            FROM
                  dbo.BUDGET_ACCOUNT AS ba
                  INNER JOIN #Account_Id AS ai
                        ON ba.ACCOUNT_ID = ai.Account_Id
                           AND ba.BUDGET_ID = @Budget_Id
            WHERE
                  IS_DELETED = 1
	  
	  -- Insert missing accounts
            INSERT      dbo.BUDGET_ACCOUNT
                        ( 
                         BUDGET_ID
                        ,ACCOUNT_ID )
                        SELECT
                              @Budget_Id
                             ,Account_Id
                        FROM
                              #Account_Id AS ai
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                dbo.BUDGET_ACCOUNT AS ba
                                           WHERE
                                                BUDGET_ID = @Budget_Id
                                                AND ACCOUNT_ID = ai.Account_Id )
                                                
            EXEC dbo.Budget_Account_Queue_Ins_Upd 
                  @Budget_Id = @Budget_Id
                                                
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
		
            EXEC dbo.usp_RethrowError 
		
      END CATCH	
END;

;
GO

GRANT EXECUTE ON  [dbo].[Merge_Budget_Accounts] TO [CBMSApplication]
GO
