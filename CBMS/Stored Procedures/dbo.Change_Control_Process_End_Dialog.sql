SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:		Change_Control_Process_End_Dialog

DESCRIPTION:
	Activated by Change_Control_Queue, it processes messages
	of http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog
	
	Procedure Ends conversation 


INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
	@Message					XML						-- Exists for Consistancy. message type validates to null 
	,@Conversation_Handle		UNIQUEIDENTIFIER		-- Conversation that sent the message

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	

USAGE EXAMPLES:
------------------------------------------------------------
 -- Procedure is only executed by Change_Control_Receive_Table_Changes
 when a message type is Recieved

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH		Chad Hattabaugh
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			11/04/2011	Created
	CMH			12/16/2011  Removed the Persistant_Message functionality
******/
CREATE PROCEDURE [dbo].[Change_Control_Process_End_Dialog]
(	@Message				XML				
	,@Conversation_Handle	UNIQUEIDENTIFIER	)
AS
BEGIN
	SET NOCOUNT ON;
	
	END CONVERSATION @Conversation_Handle

END


GO
GRANT EXECUTE ON  [dbo].[Change_Control_Process_End_Dialog] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Change_Control_Process_End_Dialog] TO [sb_Execute]
GO
