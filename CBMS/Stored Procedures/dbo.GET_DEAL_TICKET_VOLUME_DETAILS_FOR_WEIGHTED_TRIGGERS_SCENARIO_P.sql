SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec GET_DEAL_TICKET_VOLUME_DETAILS_FOR_WEIGHTED_TRIGGERS_P 1,1,109,100009,0,0,571


CREATE  PROCEDURE DBO.GET_DEAL_TICKET_VOLUME_DETAILS_FOR_WEIGHTED_TRIGGERS_SCENARIO_P

@userId varchar(10),
@sessionId varchar(20),
@clientId integer,
@dealTicketId integer,
@currencyUnit integer,
@consumptionUnit integer,
@hedgeTypeId integer,
@hedgeLevelTypeId integer,
@divisionId integer,
@siteId integer,
@contractId integer

AS


	set nocount on	

IF @currencyUnit>0 and @consumptionUnit>0
	
--WHEN NO CONVERSION OR WHEN THERE IS NO ENTERY FOR CLIENT TAKE CONVERSION FACOTOR 1

BEGIN--1ST

	if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)=@currencyUnit OR (select count(*) from RM_CURRENCY_UNIT_CONVERSION where client_id=@clientId)=0)
	
	BEGIN--2ND
				if @clientId>0 and @divisionId=0 and @siteId=0
					
					BEGIN--3RD

					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						1.00 conversion_factor,
						rmdtd.HEDGE_PRICE hedge_price,
						(rmdtd.hedge_price*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						onboard.site_id=rmdtvd.site_id 
					END --3RD CLOSED

				else if @clientId>0 and @divisionId>0 and @siteId=0
					
					BEGIN--4TH

					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						1.00 conversion_factor,
						(rmdtd.hedge_price*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						onboard.site_id=rmdtvd.site_id 
					END--4TH CLOSED

				else if @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0

					BEGIN--5TH
					
					if @contractId>0

					begin

						SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						1.00 conversion_factor,
						(rmdtd.hedge_price*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor


					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET rmdt,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdt.CONTRACT_ID=@contractId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						rmdtvd.site_id=onboard.site_id and
						rmdtvd.site_id = @siteId

					end

					else 

					begin

						
					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						1.00 conversion_factor,
						(rmdtd.hedge_price*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						CONSUMPTION_UNIT_CONVERSION consumption,


						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND

						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit and
						rmdtvd.site_id=onboard.site_id and
						rmdtvd.site_id = @siteId
						end
					END--5TH CLOSED
		END --2ND CLOSED

	else if((select currency_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)!=@currencyUnit)

		BEGIN--6TH

		if((select CURRENCY_UNIT_NAME from CURRENCY_UNIT where CURRENCY_UNIT_ID=@currencyUnit)!='CAN')
		BEGIN--7TH
		
		--PRINT 'usd-TO caN '
		 if @clientId>0 and @divisionId=0 and @siteId=0
	
			BEGIN--8TH
				SELECT 
					rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
					(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
					DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
					DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
					rmdtvd.site_id,
					rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
					(1/currency.CONVERSION_FACTOR),
					(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion
					consumption.CONVERSION_FACTOR Volume_Conversion_Factor

				FROM
					RM_DEAL_TICKET_DETAILS rmdtd,
					RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
					RM_CURRENCY_UNIT_CONVERSION currency,
					CONSUMPTION_UNIT_CONVERSION consumption,
					RM_ONBOARD_HEDGE_SETUP onboard

				WHERE
					
					rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
					rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
					
					consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
					consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
					RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
					where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
					onboard.site_id=rmdtvd.site_id 
			END--8TH CLOSED
			



		else if @clientId>0 and @divisionId>0 and @siteId=0
			
				BEGIN--9TH

					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						(1/currency.CONVERSION_FACTOR),
						(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_CURRENCY_UNIT_CONVERSION currency,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
						RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
						onboard.site_id=rmdtvd.site_id 

				END--9TH CLOSED
				
		

	    else if @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0
			BEGIN--10TTH
					
					if @contractId>0
					begin

						SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						(1/currency.CONVERSION_FACTOR),
						(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET rmdt,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_CURRENCY_UNIT_CONVERSION currency,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdt.CONTRACT_ID=@contractId AND
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
						RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
						rmdtvd.site_id=onboard.site_id and
						rmdtvd.site_id = @siteId

					end

					else

					begin



					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion,
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						(1/currency.CONVERSION_FACTOR),
						(rmdtd.hedge_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_CURRENCY_UNIT_CONVERSION currency,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND

						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
						RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
						rmdtvd.site_id=onboard.site_id and
						rmdtvd.site_id = @siteId
						end
			END--10TH CLOSED
		END--7TH CLOSED

		ELSE 
			--PRINT 'can-TO USD '
				BEGIN--11TH

				if @clientId>0 and @divisionId=0 and @siteId=0

				BEGIN--12TH


					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion,
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						currency.CONVERSION_FACTOR,
						(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_CURRENCY_UNIT_CONVERSION currency,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
						RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
						onboard.site_id=rmdtvd.site_id 
				END--12TH CLOSED
			

		else if @clientId>0 and @divisionId>0 and @siteId=0
			
				BEGIN--13TH
					SELECT 
						rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
						(rmdtd.trigger_price*(1/currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion,
						DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
						DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
						rmdtvd.site_id,
						rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
						currency.CONVERSION_FACTOR,
						(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
						consumption.CONVERSION_FACTOR Volume_Conversion_Factor

					FROM
						RM_DEAL_TICKET_DETAILS rmdtd,
						RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
						RM_CURRENCY_UNIT_CONVERSION currency,
						CONSUMPTION_UNIT_CONVERSION consumption,
						RM_ONBOARD_HEDGE_SETUP onboard

					WHERE
						rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
						rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
						
						consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
						consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
						RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
						where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
						onboard.site_id=rmdtvd.site_id 
				END--13TH CLOSED
				
		

	        else if @clientId>0 and @divisionId=0 and @siteId>0 or @clientId>0 and @divisionId>0 and @siteId>0

				BEGIN--14TH
					if @contractId>0

					BEGIN
						
						SELECT 
							rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
							(rmdtd.trigger_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion,
							DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
							DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
							rmdtvd.site_id,
							rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
							currency.CONVERSION_FACTOR,
							(rmdtd.hedge_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) hedgePrice,--For Rate Conversion,
							consumption.CONVERSION_FACTOR Volume_Conversion_Factor

						FROM
							RM_DEAL_TICKET_DETAILS rmdtd,
							RM_DEAL_TICKET rmdt,
							RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
							RM_CURRENCY_UNIT_CONVERSION currency,
							CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard

						WHERE
							rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
							rmdt.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdt.contract_id=@contractId AND
							consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
							consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
							RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
							where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
							rmdtvd.site_id=onboard.site_id and
							rmdtvd.site_id = @siteId
					END

					else

					begin

						SELECT 
							rmdtvd.hedge_volume*consumption.CONVERSION_FACTOR hedged_volume,
							(rmdtd.trigger_price*(currency.CONVERSION_FACTOR)*(1/consumption.CONVERSION_FACTOR)) trigger_price,--For Rate Conversion,
							DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
							DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
							rmdtvd.site_id,
							rmdtvd.RM_DEAL_TICKET_VOLUME_DETAILS_ID,
							currency.CONVERSION_FACTOR,
							rmdtd.hedge_price*currency.CONVERSION_FACTOR,
							consumption.CONVERSION_FACTOR Volume_Conversion_Factor

						FROM
							RM_DEAL_TICKET_DETAILS rmdtd,
							RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
							RM_CURRENCY_UNIT_CONVERSION currency,
							CONSUMPTION_UNIT_CONVERSION consumption,
							RM_ONBOARD_HEDGE_SETUP onboard

						WHERE
							rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
							rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID AND
							
							consumption.BASE_UNIT_ID=(select unit_type_id from RM_DEAL_TICKET where RM_DEAL_TICKET_id=@dealTicketId)AND
							consumption.CONVERTED_UNIT_ID=@consumptionUnit AND
							RM_CURRENCY_UNIT_CONVERSION_ID =(select RM_CURRENCY_UNIT_CONVERSION_ID from RM_CURRENCY_UNIT_CONVERSION
							where client_id=@clientId and conversion_year=DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER)) and
							rmdtvd.site_id=onboard.site_id and
							rmdtvd.site_id = @siteId
						end
					END	--14TH CLOSED
				END--11TH
		    	END--7TH
		END--END OF 1ST BEGIN

else if @currencyUnit=0 and @consumptionUnit=0

	BEGIN
			select 
				rmdtvd.hedge_volume,
				rmdtd.trigger_price trigger_price,
				DATEPART(MONTH,rmdtd.MONTH_IDENTIFIER) dealTicketMonth,
				DATEPART(YEAR,rmdtd.MONTH_IDENTIFIER) dealTicketYear,
				rmdtvd.site_id,
				rmdtd.HEDGE_PRICE hedge_price
			
			from
				RM_DEAL_TICKET_DETAILS rmdtd,
				RM_DEAL_TICKET_VOLUME_DETAILS rmdtvd,
				RM_ONBOARD_HEDGE_SETUP onboard
				
				
			where
				rmdtd.RM_DEAL_TICKET_ID=@dealTicketId AND
				rmdtd.RM_DEAL_TICKET_DETAILS_ID=rmdtvd.RM_DEAL_TICKET_DETAILS_ID and
				
				rmdtvd.site_id=onboard.site_id and
				onboard.hedge_type_id=@hedgeTypeId
	END
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_VOLUME_DETAILS_FOR_WEIGHTED_TRIGGERS_SCENARIO_P] TO [CBMSApplication]
GO
