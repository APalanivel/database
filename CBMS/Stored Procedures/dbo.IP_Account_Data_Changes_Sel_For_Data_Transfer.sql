
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                        
                              
NAME: [DBO].[IP_Account_Data_Changes_Sel_For_Data_Transfer]                              
                              
DESCRIPTION:                               
      Select the changed Account Level data from cost_usage and cost_usage_Account_Dtl tables(using change tracking) to transfer                  
      it to Hub                              
                                    
INPUT PARAMETERS:                                        
NAME			DATATYPE	DEFAULT  DESCRIPTION                                        
------------------------------------------------------------                                        
 @Min_CT_Ver	BIGINT                      
 @Max_CT_Ver	BIGINT                           
                                              
OUTPUT PARAMETERS:                                        
NAME			DATATYPE	DEFAULT  DESCRIPTION                                 
------------------------------------------------------------                                        

USAGE EXAMPLES:                                        
------------------------------------------------------------                              
                  
	DECLARE @Current_Version BIGINT                  
	SELECT @Current_Version = CHANGE_TRACKING_CURRENT_VERSION()                  
	EXEC dbo.IP_Account_Data_Changes_Sel_For_Data_Transfer 0,@Current_Version                  
                  
AUTHOR INITIALS:                                        
INITIALS	NAME                  
------------------------------------------------------------                                        
RR			Raghu Reddy
HG			Hariharasuthan Ganesan

MODIFICATIONS                              
INITIALS	DATE		MODIFICATION                              
------------------------------------------------------------                              
RR			2014-03-26  Created
HG			2014-06-12	Modified the join between INVOICE_PARTICIPATION and Core.Client_Hier_Account to INNER JOIN to ignore the orphan Supplier account sites left over in IP.
HG			2014-06-18	When the last meter of supplier account is removed IP is not getting deleted, to handle this split the delte and ins/upd to a seperate query and using INNER join between IP and CHA to ignore the orphan records
*/
CREATE PROCEDURE [dbo].[IP_Account_Data_Changes_Sel_For_Data_Transfer]
      (
       @Min_CT_Ver BIGINT
      ,@Max_CT_Ver BIGINT )
AS
BEGIN

      SET NOCOUNT ON

      SELECT
            ct.Client_Hier_Id
           ,ct.Account_Id
           ,ct.Service_Month
           ,CAST(0 AS INT) AS Is_Expected
           ,CAST(0 AS INT) AS Is_Received
           ,CAST(ct.SYS_CHANGE_OPERATION AS CHAR(1)) AS Sys_Change_Operation
           ,NULL AS Commodity_Id
      FROM
            CHANGETABLE(CHANGES dbo.INVOICE_PARTICIPATION, @Min_CT_Ver) ct
      WHERE
            ct.Sys_Change_Version <= @Max_CT_Ver
            AND ct.Sys_Change_Operation = 'D'
      UNION ALL
      SELECT
            ct.Client_Hier_Id
           ,ct.Account_Id
           ,ct.Service_Month
           ,CAST(ISNULL(ip.IS_EXPECTED, 0) AS INT) AS Is_Expected
           ,CAST(ISNULL(ip.IS_RECEIVED, 0) AS INT) AS Is_Received
           ,CAST(ct.SYS_CHANGE_OPERATION AS CHAR(1)) AS Sys_Change_Operation
           ,cha.Commodity_Id
      FROM
            CHANGETABLE(CHANGES dbo.INVOICE_PARTICIPATION, @Min_CT_Ver) ct
            INNER JOIN dbo.INVOICE_PARTICIPATION ip
                  ON ip.Account_Id = ct.Account_Id
                     AND ip.Client_Hier_Id = ct.Client_Hier_Id
                     AND ip.Service_Month = ct.Service_Month
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Client_Hier_Id = ip.Client_Hier_Id
                     AND cha.ACCOUNT_ID = ip.Account_Id
      WHERE
            ct.Sys_Change_Version <= @Max_CT_Ver
            AND ct.Sys_Change_Operation IN ('I','U')
      GROUP BY
            ct.Client_Hier_Id
           ,ct.Account_Id
           ,ct.Service_Month
           ,ip.IS_EXPECTED
           ,ip.IS_RECEIVED
           ,ct.SYS_CHANGE_OPERATION
           ,cha.Commodity_Id

END;
;
GO

GRANT EXECUTE ON  [dbo].[IP_Account_Data_Changes_Sel_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[IP_Account_Data_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
