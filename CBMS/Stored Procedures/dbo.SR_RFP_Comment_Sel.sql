SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.SR_RFP_Comment_Sel

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@SR_RFP_Id				INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM dbo.SR_RFP_Comment

	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'CommentType', @Code_Value = 'SR_RFPComment'
	SELECT TOP 10 * FROM dbo.SR_RFP

	BEGIN TRANSACTION
		EXEC dbo.SR_RFP_Comment_Ins 5, 16, 'Test_SR_RFP_Comment_Sel_Test'
		EXEC dbo.SR_RFP_Comment_Sel 5
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2015-10-15	Global Sourcing - Phase2 -Created

******/
CREATE PROCEDURE [dbo].[SR_RFP_Comment_Sel] ( @SR_RFP_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            comm.Comment_Text
           ,comm.Comment_Dt
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Commented_By
           ,rfpcom.Comment_Id
      FROM
            dbo.SR_RFP_Comment rfpcom
            INNER JOIN dbo.Comment comm
                  ON rfpcom.Comment_Id = comm.Comment_ID
            INNER JOIN dbo.USER_INFO ui
                  ON comm.Comment_User_Info_Id = ui.USER_INFO_ID
      WHERE
            rfpcom.SR_RFP_Id = @SR_RFP_Id
      
                  
END;
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Comment_Sel] TO [CBMSApplication]
GO
