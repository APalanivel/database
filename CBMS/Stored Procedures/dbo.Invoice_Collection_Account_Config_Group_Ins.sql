SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******             
                         
 NAME: [dbo].[Invoice_Collection_Account_Config_Group_Ins]                 
                            
 DESCRIPTION:                            
			To insert or update or delete data in [Invoice_Collection_Account_Config_Group]
                            
 INPUT PARAMETERS:            
                           
 Name                            DataType           Default       Description            
---------------------------------------------------------------------------------------------------------------          
@User_Report_Id						INT
@tvp_Account_Invoice_Collection_Source							tvp_Account_Invoice_Collection_Source		 READONLY                
                            
 OUTPUT PARAMETERS:                 
                            
 Name                            DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
                            
 USAGE EXAMPLES:            
---------------------------------------------------------------------------------------------------------------          
--EXAMPLE 1

select * from  Invoice_Collection_Account_Config_Group where Invoice_Collection_Account_Config_Group_name ='test1'
declare @Invoice_Collection_Account_Config_Group_Id_Out int
EXEC [Invoice_Collection_Account_Config_Group_Ins] 'test1' ,@Invoice_Collection_Account_Config_Group_Id_Out OUTPUT



                           
 AUTHOR INITIALS:            
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 SP                     Sandeep Pigilam  
 SLP					Sri Lakshmi Pallikonda                
                             
 MODIFICATIONS:          
           
 Initials               Date             Modification          
---------------------------------------------------------------------------------------------------------------          
 SLP                    2019-10-14     Created for adding group name   
 SLP					2019-11-13	   Included  user_info_id details while saving the Group Name
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Config_Group_Ins]
(
    @Invoice_Collection_Account_Config_Group_Name NVARCHAR(236),
    @User_Info_Id INT,
    @Invoice_Collection_Account_Config_Group_Id_Out INT OUTPUT
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @ID TABLE
    (
        Invoice_Collection_Account_Config_Group_Id_Out INT
    );


    SELECT @Invoice_Collection_Account_Config_Group_Id_Out = Invoice_Collection_Account_Config_Group_ID
    FROM Invoice_Collection_Account_Config_Group
    WHERE Invoice_Collection_Account_Config_Group_name = @Invoice_Collection_Account_Config_Group_Name;

    INSERT INTO @ID
    SELECT @Invoice_Collection_Account_Config_Group_Id_Out;



    IF @Invoice_Collection_Account_Config_Group_Id_Out IS NULL
    BEGIN
        INSERT INTO Invoice_Collection_Account_Config_Group
        (
            Invoice_Collection_Account_Config_Group_name,
            Created_User_Id
        )
        OUTPUT inserted.Invoice_Collection_Account_Config_Group_ID
        INTO @ID
        VALUES
        (@Invoice_Collection_Account_Config_Group_Name, @User_Info_Id);
    END;

    SELECT @Invoice_Collection_Account_Config_Group_Id_Out = Invoice_Collection_Account_Config_Group_Id_Out
    FROM @ID
    WHERE Invoice_Collection_Account_Config_Group_Id_Out IS NOT NULL;

END;





GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Config_Group_Ins] TO [CBMSApplication]
GO
