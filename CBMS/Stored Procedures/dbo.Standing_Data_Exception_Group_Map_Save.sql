SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
 CBMS.dbo.Standing_Data_Exception_Group_Map_Save  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
 Name       DataType  Default     Description  
---------------------------------------------------------------------------------------------  
 @cu_exception_type_id   INT  
    @exception_type     VARCHAR(200)  
    @exception_group_type_id  INT  
    @managed_by_group_info_id  INT  
    @stop_processing    BIT  
    @is_routed_reason    BIT  
    @sort_order      INT = 1  
    @Individual_User_Info_Id  INT  
  
OUTPUT PARAMETERS:  
 Name       DataType  Default     Description  
---------------------------------------------------------------------------------------------  
   
USAGE EXAMPLES:  
---------------------------------------------------------------------------------------------  
  
  
  
BEGIN TRANSACTION  
EXEC dbo.Standing_Data_Exception_Group_Map_Save  
    @cu_exception_type_id = ''  
    , @exception_type = ''  
    , @exception_group_type_id = ''  
    , @managed_by_group_info_id = ''  
    , @stop_processing = ''  
    , @is_routed_reason = ''  
    , @sort_order = 1  
    , @Individual_User_Info_Id = 49  
  
ROLLBACK TRANSACTION  
  
  
AUTHOR INITIALS:    
 Initials  			Name    
-----------  		-------------------------------------------------    
 HKT   				Harish Kumar Tirumandyam  

MODIFICATIONS    
    
 Initials   			Date    			Modification    
-------------  			------------  		-----------------------------------    
HKT     				2020-04-24 		    created for saving Standing_Data_Exception changes for Data Purple Project   
  
******/  
  
  
CREATE PROCEDURE [dbo].[Standing_Data_Exception_Group_Map_Save]  
    (  
         @Managed_By_Group_Info_Id INT = NULL
        , @Managed_By_User_Info_Id INT = NULL
        , @Updated_User_Id INT
        , @Standing_Data_Exception_Group_Map_Id INT
		, @Exception_Type_Id INT =NULL 
		, @Exception_Desc NVARCHAR(MAX)=  NULL
            
    )  
AS  
    BEGIN  
  
        SET NOCOUNT ON;  
  
  
        DECLARE @Is_Individual_User_Group_Info_Id BIT = 0;  
  
        SELECT  
            @Is_Individual_User_Group_Info_Id = 1  
        FROM  
            dbo.GROUP_INFO gi  
        WHERE  
            gi.GROUP_INFO_ID = @managed_by_group_info_id  
            AND gi.GROUP_NAME = 'Individual User';  
  
  
  
  
        DECLARE @This_Id INT;  
        SET @This_Id = @Exception_Type_Id;  
  
        IF @This_Id IS NULL  
            BEGIN  
  
                INSERT INTO dbo.Standing_Data_Exception_Group_Map  
                     (  
                         Exception_Type_Cd
						 ,Exception_Desc
						 ,Managed_By_Group_Info_Id
						 ,Managed_By_User_Info_Id
						 ,Created_User_Id
						 ,Created_Ts
						 ,Updated_User_Id
						 ,Last_Change_Ts
                     )  
                VALUES  
                    (
					 @Exception_Type_Id
						 ,@Exception_Desc
						 ,@Managed_By_Group_Info_Id
						 ,@Managed_By_User_Info_Id
						 ,@Updated_User_Id
						 ,getdate()
						 ,@Updated_User_Id
						 ,getdate()
					
				   );  
            END;  
  
        ELSE  
            BEGIN  
  
                UPDATE  
                    dbo.Standing_Data_Exception_Group_Map  
                SET  
				Exception_Type_Cd = @Exception_Type_Id
						 ,Exception_Desc = @Exception_Desc
						 ,Managed_By_Group_Info_Id =@Managed_By_Group_Info_Id
						 , Managed_By_User_Info_Id = CASE WHEN @Is_Individual_User_Group_Info_Id = 1 THEN  
                                                         @Managed_By_User_Info_Id  
                                                    ELSE NULL  
                                                END  
						 ,Updated_User_Id = @Updated_User_Id
						 ,Last_Change_Ts = getdate()                    
                WHERE  
                    ( 
					Exception_Type_Cd = @this_id OR 
					Standing_Data_Exception_Group_Map_Id = @Standing_Data_Exception_Group_Map_Id
					)
  
      END;  
    END;  
  
  
  
GO
GRANT EXECUTE ON  [dbo].[Standing_Data_Exception_Group_Map_Save] TO [CBMSApplication]
GO
