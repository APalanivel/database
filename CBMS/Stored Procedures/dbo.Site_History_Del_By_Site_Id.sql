
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: dbo.Site_History_Del_By_Site_Id

DESCRIPTION:

 To Delete Site History associated with the given Site Client Hier Id.

INPUT PARAMETERS:
 NAME			DATATYPE DEFAULT  DESCRIPTION
------------------------------------------------------------
 @Site_Id		INT
 @User_Info_id  INT      User who deletes this Site.

OUTPUT PARAMETERS:
 NAME   DATATYPE DEFAULT  DESCRIPTION
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5352
		EXEC dbo.Site_History_Del_By_Site_Id   554,49
		SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5352
	ROLLBACK TRAN

	BEGIN TRAN
		SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5605
		EXEC dbo.Site_History_Del_By_Site_Id   10292,49
		SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5605
	ROLLBACK TRAN

	BEGIN TRAN
			SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5352
			EXEC dbo.Site_History_Del_By_Site_Id   554,49
			SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 5352
	ROLLBACK TRAN

	BEGIN TRAN
			SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 17430
			EXEC dbo.Site_History_Del_By_Site_Id   25313,49
			SELECT count(1) FROM Bucket_Site_Interval_Dtl WHERE Client_Hier_Id = 17430
	ROLLBACK TRAN


AUTHOR INITIALS:
 INITIALS	NAME
------------------------------------------------------------
 PNR		PANDARINATH
 HG			Harihara Suthan G
 BCH		Balaraju
 CPE		Chaitanya Panduga Eshwar      
 RR			Raghu Reddy      
 AKR		Ashok Kumar Raju
 RKV		Ravi Kumar Vegesna

MODIFICATIONS:
 INITIALS	DATE  MODIFICATION
--------------------------------------------------------------
 PNR		06/28/2010 Created      
 HG			07/21/2010 Invoice_Participation_Site table added to the list to remove all the entries belongs to the given Site.      
 HG			07/27/2010 Added Sr_Rfp_Site_History_Del_By_Site_Id procedure to remove the Rfp site level history.      
 HG			08/03/2010 Added code to remove the inactive users associated with the site from User_Site_Map table.      
 PNR		23/11/2010 Removed Transaction part as part of Project : Security_Roles_Administration.      
 CPE		2012-04-06 Removed the call to dbo.Site_GHG_History_Del_By_Site_Id  as core.Client_Commodity_Detail is moved to Hub      
 BCH		2012-04-30  Removed part of the script for all Cost_Usage_Site references.      
 RR			2012-06-18  Replaced while exists(select and deleting processed row) for looping through each record in table variables with       
							row number column, generated & total rows saved into local variable, iterating the loop till count reaches variable value      
 AKR        2012-10-01  Added code to Delete data from dbo.Site_Analyst_Mapping table, as a part of POCO
 RKV		2013-04-22  Added Code to Delete Bucket_Site_Interval History associated with the given Site Client Hier Id.
 RR			2013-10-09	MAINT-2269 To improve performance removed the script part that deletes records from [User Site Map] table, triggers
						on this table deletes and reloads the records of [user sal] blocking the process. Delete/reload of records will
						be a scheduled process
 HG			2017-09-19	MAINT-5952, Modifed the cost/usage delete sproc to use the PK in CUSD to improve the performance						
******/
CREATE PROCEDURE [dbo].[Site_History_Del_By_Site_Id]
      (
       @Site_Id INT
      ,@User_Info_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @Client_Hier_Id INT
           ,@Bucket_Master_Id INT
           ,@Cost_Usage_Site_Dtl_Id INT
           ,@Commodity_Id INT
           ,@Service_Month DATE
           ,@Sitegroup_id INT
           ,@Address_Id INT
           ,@Alternate_Fuel_Id INT
           ,@Rm_OnBoard_Hedge_Setup_Id INT
           ,@Invoice_Participation_Site_Id INT
           ,@User_Id INT
           ,@Inactive_User_Info_Id INT
           ,@Client_Name VARCHAR(200)
           ,@User_Name VARCHAR(81)
           ,@Application_Name VARCHAR(30) = 'Delete Site'
           ,@Audit_Function SMALLINT = -1
           ,@Current_Ts DATETIME
           ,@Lookup_Value XML
           ,@Total_Row_Count INT
           ,@Row_Counter INT
           ,@Service_Start_Dt DATE
           ,@Service_End_Dt DATE
           ,@Data_Source_Cd INT

      DECLARE @Cost_Usage_Site_Dtl_List TABLE
            (
             Bucket_Master_Id INT
            ,Service_Month DATE
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Bucket_Site_Interval_Dtl_List TABLE
            (
             Client_Hier_Id INT
            ,Bucket_Master_Id INT
            ,Service_Start_Dt DATE
            ,Service_End_Dt DATE
            ,Data_Source_Cd INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Bucket_Master_Id, Service_Start_Dt, Service_End_Dt, Data_Source_Cd )
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Site_Commodity_Image_List TABLE
            (
             Commodity_Id INT
            ,Service_Month DATE
            ,PRIMARY KEY CLUSTERED ( Commodity_Id, Service_Month )
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Sitegroup_Site_List TABLE
            (
             Sitegroup_id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @User_Info_List TABLE
            (
             User_Info_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      
      DECLARE @Address_List TABLE
            (
             Address_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Alternate_Fuel_List TABLE
            (
             Alternate_Fuel_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Rm_Onboard_Hedge_Setup_List TABLE
            (
             Rm_On_Board_Hedge_Setup_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Invoice_Participation_Site_List TABLE
            (
             Invoice_Participation_Site_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      DECLARE @Site_Commodity_Analyst_List TABLE
            (
             Commodity_Id INT PRIMARY KEY CLUSTERED
            ,Row_Num INT IDENTITY(1, 1) )

      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            Core.Client_Hier
      WHERE
            Site_Id = @Site_Id      
      
      SELECT
            @Client_Name = cl.CLIENT_NAME
      FROM
            dbo.SITE s
            INNER JOIN dbo.CLIENT cl
                  ON cl.CLIENT_ID = s.Client_ID
      WHERE
            s.SITE_ID = @Site_Id      
      
      SELECT
            @User_Name = FIRST_NAME + SPACE(1) + LAST_NAME
      FROM
            dbo.USER_INFO
      WHERE
            USER_INFO_ID = @User_Info_Id      
      
      SET @Lookup_Value = ( SELECT
                              Sitegroup_Name
                             ,SITE_NAME
                             ,SITE_ID
                            FROM
                              dbo.Sitegroup Division
                              INNER JOIN dbo.SITE
                                    ON Division.Sitegroup_Id = SITE.DIVISION_ID
                            WHERE
                              SITE.SITE_ID = @Site_Id
            FOR
                            XML AUTO )      
      
      INSERT      INTO @Alternate_Fuel_List
                  ( Alternate_Fuel_Id )
                  SELECT
                        ALTERNATE_FUEL_ID
                  FROM
                        dbo.ALTERNATE_FUEL
                  WHERE
                        SITE_ID = @Site_Id      
      
      INSERT      INTO @Cost_Usage_Site_Dtl_List
                  ( Bucket_Master_Id
                  ,Service_Month )
                  SELECT
                        Bucket_Master_Id
                       ,Service_Month
                  FROM
                        dbo.Cost_Usage_Site_Dtl
                  WHERE
                        Client_Hier_Id = @Client_Hier_Id      
      
      INSERT      INTO @Bucket_Site_Interval_Dtl_List
                  ( Client_Hier_Id
                  ,Bucket_Master_Id
                  ,Service_Start_Dt
                  ,Service_End_Dt
                  ,Data_Source_Cd )
                  SELECT
                        Client_Hier_Id
                       ,Bucket_Master_Id
                       ,Service_Start_Dt
                       ,Service_End_Dt
                       ,Data_Source_Cd
                  FROM
                        dbo.Bucket_Site_Interval_Dtl
                  WHERE
                        Client_Hier_Id = @Client_Hier_Id 
          
      INSERT      INTO @Site_Commodity_Image_List
                  ( Commodity_Id
                  ,Service_Month )
                  SELECT
                        Commodity_Id
                       ,Service_Month
                  FROM
                        dbo.Site_Commodity_Image
                  WHERE
                        SITE_ID = @Site_Id      
      
      INSERT      INTO @Sitegroup_Site_List
                  ( Sitegroup_id )
                  SELECT
                        Sitegroup_id
                  FROM
                        dbo.Sitegroup_Site
                  WHERE
                        Site_id = @Site_Id      
      
      INSERT      INTO @User_Info_List
                  ( User_Info_Id )
                  SELECT
                        USER_INFO_ID
                  FROM
                        dbo.USER_INFO
                  WHERE
                        SITE_ID = @Site_Id      
      
      
      INSERT      INTO @Address_List
                  ( Address_Id )
                  SELECT
                        PRIMARY_ADDRESS_ID
                  FROM
                        dbo.SITE
                  WHERE
                        SITE_ID = @Site_Id
                        AND PRIMARY_ADDRESS_ID IS NOT NULL      
      
      INSERT      INTO @Rm_Onboard_Hedge_Setup_List
                  ( Rm_On_Board_Hedge_Setup_Id )
                  SELECT
                        rohs.RM_ONBOARD_HEDGE_SETUP_ID
                  FROM
                        dbo.RM_ONBOARD_HEDGE_SETUP rohs
                        INNER JOIN dbo.ENTITY ht
                              ON ht.ENTITY_ID = rohs.HEDGE_TYPE_ID
                  WHERE
                        rohs.SITE_ID = @Site_Id
                        AND ht.ENTITY_NAME = 'Does not hedge'      
      
      INSERT      INTO @Invoice_Participation_Site_List
                  ( Invoice_Participation_Site_Id )
                  SELECT
                        INVOICE_PARTICIPATION_SITE_ID
                  FROM
                        dbo.INVOICE_PARTICIPATION_SITE
                  WHERE
                        SITE_ID = @Site_Id      
             
      INSERT      INTO @Site_Commodity_Analyst_List
                  ( Commodity_Id )
                  SELECT
                        sca.Commodity_Id
                  FROM
                        dbo.Site_Commodity_Analyst sca
                  WHERE
                        Site_Id = @Site_Id      
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Alternate_Fuel_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Alternate_Fuel_Id = Alternate_Fuel_Id
                  FROM
                        @Alternate_Fuel_List
                  WHERE
                        Row_Num = @Row_Counter                    
                 
                  EXEC dbo.Alternate_Fuel_Del
                        @Alternate_Fuel_Id      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END      
            
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Site_Commodity_Image_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Commodity_Id = Commodity_Id
                       ,@Service_Month = Service_Month
                  FROM
                        @Site_Commodity_Image_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.Site_Commodity_Image_Del
                        @Site_Id
                       ,@Commodity_Id
                       ,@Service_Month      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END      
      
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Bucket_Site_Interval_Dtl_List      
      SET @Row_Counter = 1       
                  
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
      
                  SELECT
                        @Client_Hier_Id = Client_Hier_Id
                       ,@Bucket_Master_Id = Bucket_Master_Id
                       ,@Service_Start_Dt = Service_Start_Dt
                       ,@Service_End_Dt = Service_End_Dt
                       ,@Data_Source_Cd = Data_Source_Cd
                  FROM
                        @Bucket_Site_Interval_Dtl_List
                  WHERE
                        Row_Num = @Row_Counter       
                                  
                  EXECUTE dbo.Bucket_Site_Interval_Dtl_Del
                        @Client_Hier_Id = @Client_Hier_Id
                       ,@Bucket_Master_Id = @Bucket_Master_Id
                       ,@Service_Start_Dt = @Service_Start_Dt
                       ,@Service_End_Dt = @Service_End_Dt
                       ,@Data_Source_Cd = @Data_Source_Cd    
                                   
                  SET @Row_Counter = @Row_Counter + 1      
            END      
            
            
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Cost_Usage_Site_Dtl_List    
            
              
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Bucket_Master_Id = Bucket_Master_Id
                       ,@Service_Month = Service_Month
                  FROM
                        @Cost_Usage_Site_Dtl_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.Cost_Usage_Site_Dtl_Del_By_Site_Bucket_Service_Month
                        @Client_Hier_Id = @Client_Hier_Id
                       , -- int
                        @Bucket_Master_id = @Bucket_Master_Id
                       , -- int
                        @Service_Month = @Service_Month -- date

                  SET @Row_Counter = @Row_Counter + 1      
            END      

      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Sitegroup_Site_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Sitegroup_id = Sitegroup_id
                  FROM
                        @Sitegroup_Site_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.Sitegroup_Site_Del
                        @Sitegroup_id
                       ,@Site_Id      
      
                  SET @Row_Counter = @Row_Counter + 1 
            END      
      
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @User_Info_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @User_Id = User_Info_Id
                  FROM
                        @User_Info_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.User_Info_Upd_Site_Id_Null_By_User_Info_Id
                        @User_Id      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END      
      
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Rm_Onboard_Hedge_Setup_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Rm_OnBoard_Hedge_Setup_Id = Rm_On_Board_Hedge_Setup_Id
                  FROM
                        @Rm_Onboard_Hedge_Setup_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.Rm_OnBoard_Hedge_Setup_Del
                        @Rm_OnBoard_Hedge_Setup_Id      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END      
      
           
      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Invoice_Participation_Site_List      
      SET @Row_Counter = 1       
      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN      
                  SELECT
                        @Invoice_Participation_Site_Id = Invoice_Participation_Site_Id
                  FROM
                        @Invoice_Participation_Site_List
                  WHERE
                        Row_Num = @Row_Counter      
      
                  EXEC dbo.Invoice_Participation_Site_Del
                        @Invoice_Participation_Site_Id      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END

      SELECT
            @Total_Row_Count = MAX(sca.Row_Num)
      FROM
            @Site_Commodity_Analyst_List sca   
      SET @Row_Counter = 1

      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN

                  SELECT
                        @Commodity_Id = sca.Commodity_Id
                  FROM
                        @Site_Commodity_Analyst_List sca
                  WHERE
                        Row_Num = @Row_Counter      

                  EXEC dbo.Site_Commodity_Analyst_Del
                        @Site_Id
                       ,@Commodity_Id      

                  SET @Row_Counter = @Row_Counter + 1      

            END      

      EXEC dbo.Sr_Rfp_Site_History_Del_By_Site_Id
            @Site_Id

      EXEC dbo.Site_Del
            @Site_Id

      SELECT
            @Total_Row_Count = MAX(Row_Num)
      FROM
            @Address_List

      SET @Row_Counter = 1

      WHILE ( @Row_Counter <= @Total_Row_Count )
            BEGIN

                  SELECT
                        @Address_Id = Address_Id
                  FROM
                        @Address_List
                  WHERE
                        Row_Num = @Row_Counter    
      
                  EXEC dbo.Address_Del
                        @Address_Id      
      
                  SET @Row_Counter = @Row_Counter + 1      
            END

      SET @Current_Ts = GETDATE()

      EXEC dbo.Application_Audit_Log_Ins
            @Client_Name
           ,@Application_Name
           ,@Audit_Function
           ,'SITE'
           ,@Lookup_Value
           ,@User_Name
           ,@Current_Ts

END;
;
GO






GRANT EXECUTE ON  [dbo].[Site_History_Del_By_Site_Id] TO [CBMSApplication]
GO
