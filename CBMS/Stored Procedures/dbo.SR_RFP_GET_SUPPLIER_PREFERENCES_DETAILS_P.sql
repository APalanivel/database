SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SUPPLIER_PREFERENCES_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@vendor_id     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE DBO.SR_RFP_GET_SUPPLIER_PREFERENCES_DETAILS_P
	@vendor_id int
	AS
set nocount on
declare @sr_supplier_contact_info_id int 

--select @sr_supplier_contact_info_id = (select DISTINCT SR_SUPPLIER_CONTACT_INFO_ID from SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where VENDOR_ID = @vendor_id)

select 	info.first_name,
	info.email_address
	
	from 	sr_supplier_contact_info contact_info(nolock),
		user_info info(nolock)
	--where	contact_info.sr_supplier_contact_info_id = @sr_supplier_contact_info_id
	where	contact_info.sr_supplier_contact_info_id in  (select DISTINCT SR_SUPPLIER_CONTACT_INFO_ID from SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where VENDOR_ID = @vendor_id)
		and info.user_info_id = contact_info.user_info_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_PREFERENCES_DETAILS_P] TO [CBMSApplication]
GO
