SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
DELETE_UTILITY_BUDGET_COMMENTS_P(?,?,?,?,?)
SAVE_UTILITY_BUDGET_COMMENTS_P(?,?,?,?,?,?)
GET_UTILITY_BUDGET_COMMENTS_P(?,?,?)
*/


CREATE   PROCEDURE dbo.SAVE_UTILITY_BUDGET_COMMENTS_P

@userId varchar(10),
@sessionId varchar(20),
@utilityId int,
@entityId int,
@commentdate datetime,
@budgetComments varchar(4000)
as
	set nocount on

	insert into UTILITY_BUDGET_COMMENTS ( VENDOR_ID, COMMODITY_TYPE_ID, COMMENTS, USER_INFO_ID, COMMENT_DATE)
	values(@utilityId,@entityId,@budgetComments,@userId,@commentdate)
GO
GRANT EXECUTE ON  [dbo].[SAVE_UTILITY_BUDGET_COMMENTS_P] TO [CBMSApplication]
GO
