SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE  procedure [dbo].[cbmsCostUsageSiteAltFuel_Delete]
	( @MyAccountId int
	, @cost_usage_site_alt_fuel_id int 
	)
AS
BEGIN

	set nocount on

	   delete cost_usage_site_alt_fuel
	    where cost_usage_site_alt_fuel_id = @cost_usage_site_alt_fuel_id

	exec cbmsCostUsageSiteAltFuel_Get @MyAccountId, @cost_usage_site_alt_fuel_id

END





GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSiteAltFuel_Delete] TO [CBMSApplication]
GO
