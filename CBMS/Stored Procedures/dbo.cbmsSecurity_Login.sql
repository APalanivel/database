SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

 NAME: cbms.dbo.cbmsSecurity_Login  
 
 DESCRIPTION:   

 INPUT PARAMETERS:  
 Name			 DataType		Default Description  
------------------------------------------------------------  
 @username       VARCHAR(30)              
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------  
  
	 EXEC dbo.cbmsSecurity_Login 'Scott_cem'
	 EXEC dbo.cbmsSecurity_Login 'dblock'
	 EXEC dbo.cbmsSecurity_Login 'Jscott'
 
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 PNR		Pandarinath	
 HG			Harihara Suthan G
  
 MODIFICATIONS   
 Initials	Date			Modification  
------------------------------------------------------------  
 PNR		10/14/2010		Pulling the passcode value from new USER_PASSCODE table as this column moved there as a part of DV_SV Password Expiration enhancement.
 HG			11/17/2010		Modified the script to return only the active passcode by sorting last_Change_ts descendiong this way later if we want to implement passcode history for internal users this procedure would work.
******/

CREATE PROCEDURE dbo.cbmsSecurity_Login
(
      @username VARCHAR(30)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @PassCode NVARCHAR(128)

	SELECT TOP 1
		@PassCode = up.PassCode
	FROM
		dbo.User_PassCode up
		INNER JOIN dbo.USER_INFO ui
			ON ui.USER_INFO_ID = up.User_Info_Id
	WHERE
		ui.USERNAME = @username
		AND ui.ACCESS_LEVEL = 0
		AND ui.is_history = 0
	ORDER BY
		up.Last_Change_Ts DESC

	SELECT
		 ui.user_info_id
		,ui.username
		,ui.queue_id
		,@PassCode			AS	PassCode
		,ui.first_name
		,ui.middle_name
		,ui.last_name
		,ui.email_address
		,ui.is_history
		,ui.IS_DEMO_USER
		,ui.EXPIRATION_DATE
	FROM
		dbo.USER_INFO ui
	WHERE
		ui.username = @username
		AND ui.ACCESS_LEVEL = 0
		AND ui.is_history = 0

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSecurity_Login] TO [CBMSApplication]
GO
