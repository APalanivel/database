SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsNYISO_Detail_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@zone_detail_id	int       	          	
	@zone_id       	int       	          	
	@date          	datetime  	          	
	@ptid          	int       	          	
	@lbmp          	decimal(18,2)	          	
	@marginal_cost_losses	decimal(18,2)	          	
	@marginal_cost_congestion	decimal(18,2)	          	
	@pricing_type  	varchar(50)	          	
	@scarcity      	varchar(2)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE           PROCEDURE [dbo].[cbmsNYISO_Detail_Save]

	( @zone_detail_id int
	, @zone_id int	
	, @date datetime
	, @ptid int
	, @lbmp decimal(18,2)
	, @marginal_cost_losses decimal(18,2)
	, @marginal_cost_congestion decimal(18,2)
	, @pricing_type varchar(50)
	, @scarcity varchar(2) = null
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @zone_detail_id

	if @this_id is null
	begin

		insert into ny_iso_detail
			( 
			  zone_id
			, date
			, ptid
			, lbmp
			, marginal_cost_losses
			, marginal_cost_congestion
			, pricing_type
			, scarcity
			 )
		 values
			(  
			  @zone_id
			, @date
			, @ptid
			, @lbmp
			, @marginal_cost_losses
			, @marginal_cost_congestion
			, @pricing_type
			, @scarcity
			)

		set @this_id = @@IDENTITY

	end
	else
	begin

		   update ny_iso_detail
		      set zone_id = @zone_id
			, date = @date
			, ptid = @ptid
			, lbmp = @lbmp
			, marginal_cost_losses = @marginal_cost_losses
			, marginal_cost_congestion = @marginal_cost_congestion
			, pricing_type = @pricing_type
			, scarcity = @scarcity
		    where zone_detail_id = @this_id

	end

--	set nocount off

--	exec cbmsSsoDocument_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsNYISO_Detail_Save] TO [CBMSApplication]
GO
