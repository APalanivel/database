SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:  
	dbo.Report_DE_Demo_UserActivity        
         
DESCRIPTION:           
	This procedure is to fetch all Demo users from Session Info and Sesssion Activity          
        
INPUT PARAMETERS:          
Name				DataType		Default		Description          
-----------------------------------------------------------          
@Access_Begin_date  DATETIME        
@Access_End_Date	DATETIME        
@Prospect_Name		VARCHAR(300)	NULL      
@User_Name			VARCHAR(30)		NULL      
@First_name			VARCHAR(40)		NULL      
@Last_Name			VARCHAR(40)		NULL      
        
OUTPUT PARAMETERS:          
 Name   DataType  Default Description          
------------------------------------------------------------          
        
USAGE EXAMPLES:          
------------------------------------------------------------          
Usage example may not return any rows as it will filter email domains
summitenergy, ems.schneider-electric.com 
   
Exec dbo.[Report_DE_Demo_UserActivity] '01/01/2012','12/01/2012'        
      
        
AUTHOR INITIALS:          
 Initials	Name          
----------------------------------------------------------          
 AP		Ashok Pothuri         
         
 MODIFICATIONS:          
 Initials	Date		Modification          
----------------------------------------------------------          
 AP			2013-08-26	Created      
        
******/        
CREATE PROCEDURE [dbo].[Report_DE_Demo_UserActivity]
      ( 
       @Access_Begin_date DATETIME
      ,@Access_End_Date DATETIME
      ,@Prospect_Name VARCHAR(300) = ''
      ,@User_Name VARCHAR(30) = ''
      ,@First_name VARCHAR(40) = ''
      ,@Last_Name VARCHAR(40) = '' )
AS 
BEGIN          
          
      SET NOCOUNT ON;        
            
      DECLARE @User_Entity INT      
            
      SELECT
            @User_Entity = en.ENTITY_ID
      FROM
            dbo.ENTITY en
      WHERE
            en.ENTITY_NAME = 'USER_INFO'
            AND en.ENTITY_DESCRIPTION = 'Table_Type';      
      WITH  CTE_User_info
              AS ( SELECT
                        ui.user_info_id
                       ,ui.username
                       ,ui.PROSPECT_NAME
                       ,ui.first_name
                       ,ui.last_name
                       ,ui.email_address
                       ,cl.client_id
                       ,cl.client_name
                       ,uie.FIRST_NAME + ' ' + uie.LAST_NAME User_Added_By
                       ,uie.EMAIL_ADDRESS User_Added_By_Email_Address
                   FROM
                        dbo.user_info ui
                        INNER JOIN dbo.CLIENT cl
                              ON cl.client_id = ui.client_id
                        INNER JOIN dbo.ENTITY_AUDIT ea
                              ON ea.ENTITY_ID = @User_Entity
                                 AND ea.ENTITY_IDENTIFIER = ui.USER_INFO_ID
                                 AND ea.AUDIT_TYPE = 1
                        INNER JOIN dbo.USER_INFO uie
                              ON uie.USER_INFO_ID = ea.USER_INFO_ID
                   WHERE
                        ui.IS_DEMO_USER = 1
                        AND ui.access_level = 1
                        AND ( @Prospect_Name = ''
                              OR ui.PROSPECT_NAME LIKE '%' + @Prospect_Name + '%' )
                        AND ( @User_Name = ''
                              OR ui.USERNAME LIKE '%' + @User_Name + '%' )
                        AND ( @First_name = ''
                              OR ui.FIRST_NAME LIKE '%' + @First_name + '%' )
                        AND ( @Last_Name = ''
                              OR ui.LAST_NAME LIKE '%' + @Last_Name + '%' )
                        AND ui.email_address NOT LIKE '%summitenergy%'
                        AND ui.email_address NOT LIKE '%ems.schneider-electric.com%')
            SELECT
                  user_inf.username
                 ,user_inf.first_name
                 ,user_inf.last_name
                 ,user_inf.email_address
                 ,user_inf.client_name
                 ,user_inf.User_Added_By
                 ,user_inf.User_Added_By_Email_Address
                 ,user_inf.PROSPECT_NAME
                 ,user_inf.user_info_id
                 ,x.Cnt [Count of Logins]
                 ,x.Maxdate [Last Login]
            FROM
                  CTE_User_info user_inf
                  LEFT JOIN ( SELECT
                                    count(DISTINCT si.session_info_id) Cnt
                                   ,max(sa.access_date) Maxdate
                                   ,si.USER_INFO_ID
                              FROM
                                    dbo.DV2_Session_Info si
                                    JOIN dbo.DV2_session_activity sa
                                          ON sa.SESSION_INFO_ID = si.SESSION_INFO_ID
                              WHERE
                                    sa.ACCESS_DATE BETWEEN @Access_Begin_date
                                                   AND     @Access_End_Date
                                    AND si.session_info_id NOT IN ( SELECT
                                                                        session_info_id
                                                                    FROM
                                                                        dv2_session_activity
                                                                    WHERE
                                                                        page_url LIKE '%/rider.aspx%' )
                                    AND si.ip_address NOT LIKE '10.%'
                                    AND si.ip_address NOT LIKE '172.16%'
                                    AND si.ip_address NOT LIKE '172.17%'
                                    AND si.ip_address NOT LIKE '172.18%'
                                    AND si.ip_address NOT LIKE '172.19%'
                                    AND si.ip_address NOT LIKE '172.20%'
                                    AND si.ip_address NOT LIKE '172.21%'
                                    AND si.ip_address NOT LIKE '172.22%'
                                    AND si.ip_address NOT LIKE '172.23%'
                                    AND si.ip_address NOT LIKE '172.24%'
                                    AND si.ip_address NOT LIKE '172.25%'
                                    AND si.ip_address NOT LIKE '172.26%'
                                    AND si.ip_address NOT LIKE '172.27%'
                                    AND si.ip_address NOT LIKE '172.28%'
                                    AND si.ip_address NOT LIKE '172.29%'
                                    AND si.ip_address NOT LIKE '172.30%'
                                    AND si.ip_address NOT LIKE '172.31%'
                                    AND si.ip_address NOT LIKE '192.0.8%'
                                    AND si.ip_address NOT LIKE '192.168.%'
                              GROUP BY
                                    si.USER_INFO_ID ) x
                        ON x.USER_INFO_ID = user_inf.USER_INFO_ID
            GROUP BY
                  user_inf.username
                 ,user_inf.first_name
                 ,user_inf.last_name
                 ,user_inf.email_address
                 ,user_inf.client_name
                 ,user_inf.User_Added_By
                 ,user_inf.PROSPECT_NAME
                 ,user_inf.User_Added_By_Email_Address
                 ,user_inf.user_info_id
                 ,x.Cnt
                 ,x.Maxdate       
END 
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Demo_UserActivity] TO [CBMS_SSRS_Reports]
GO
