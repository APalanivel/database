
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
            
NAME: [dbo].[Merge_DE_Cost_Usage_Site_Dtl_Changes]              
                 
DESCRIPTION:            
            
 used to Merge the data received from dvdehub cost_usage_Site_Dtl table(change_tracking data)  to Cost_Usage_Site_Dtl table                   
                    
 Strictly used by ETL                    
                     
INPUT PARAMETERS:               
NAME            DATATYPE         DEFAULT     DESCRIPTION                      
------------------------------------------------------------                      
@Batch_Size     INT    
        
            
OUTPUT PARAMETERS:                      
NAME   DATATYPE DEFAULT  DESCRIPTION               
                   
------------------------------------------------------------                      
USAGE EXAMPLES:                      
------------------------------------------------------------  

BEGIN TRAN
	EXEC dbo.Merge_DE_Cost_Usage_Site_Dtl_Changes   5000
ROLLBACK TRAN

AUTHOR INITIALS:                      
INITIALS	NAME                      
------------------------------------------------------------                      
AKR			Ashok Kumar Raju  
RR			Raghu Reddy      
        
MODIFICATIONS            
INITIALS DATE  MODIFICATION            
------------------------------------------------------------            
AKR     2011-11-20  Created                
AKR     2012-03-28  Modified the sp to incorporate the Additional Data Changes  
RR		2013-05-03	Modified the code to use batch processing instead of row by row and from stage table Stage.Cost_Usage_Site_Dtl_Transfer
					as the data source.
RKV  2017-06-22		Added new column Data_Type_Cd,Actual_Bucket_Value,Estimated_Bucket_Value as part of Data enhancement.						
  
*/       
      
CREATE PROCEDURE [dbo].[Merge_DE_Cost_Usage_Site_Dtl_Changes] ( @Batch_Size INT )
AS 
BEGIN        
      SET NOCOUNT ON      
      DECLARE
            @Data_Source_Cd_CBMS INT
           ,@Data_Source_Cd_DE INT   
           
      DECLARE
            @Min_Cost_Usage_Site_Dtl_Transfer_Id INT = 1
           ,@Max_Cost_Usage_Site_Dtl_Transfer_Id INT      
           
      SELECT
            @Max_Cost_Usage_Site_Dtl_Transfer_Id = MAX(cusdt.Cost_Usage_Site_Dtl_Transfer_Id)
      FROM
            Stage.Cost_Usage_Site_Dtl_Transfer cusdt      
                     
      SELECT
            @Data_Source_Cd_DE = MAX(CASE WHEN cd.Code_Value = 'DE' THEN cd.CODE_ID
                                     END)
           ,@Data_Source_Cd_CBMS = MAX(CASE WHEN cd.Code_Value = 'CBMS' THEN cd.CODE_ID
                                       END)
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value IN ( 'DE', 'CBMS' );     
            
      WHILE @Min_Cost_Usage_Site_Dtl_Transfer_Id <= @Max_Cost_Usage_Site_Dtl_Transfer_Id 
            BEGIN 
                                       
                  MERGE INTO dbo.Cost_Usage_Site_Dtl tgt_cuad
                        USING 
                              ( SELECT
                                    cusdt.Bucket_Master_Id
                                   ,cusdt.Bucket_Value
                                   ,cusdt.UOM_Type_Id
                                   ,cusdt.Currency_Unit_Id
                                   ,cusdt.Created_By_Id
                                   ,cusdt.Created_Ts
                                   ,cusdt.Updated_By_Id
                                   ,cusdt.Updated_Ts
                                   ,cusdt.Client_Hier_Id
                                   ,cusdt.Service_Month
                                   ,cusdt.Data_Source_Cd
                                   ,cusdt.Sys_Change_Operation
                                   ,cusdt.Data_Type_Cd
                                   ,cusdt.Actual_Bucket_Value
                                   ,cusdt.Estimated_Bucket_Value
                                FROM
                                    Stage.Cost_Usage_Site_Dtl_Transfer cusdt
                                WHERE
                                    cusdt.Cost_Usage_Site_Dtl_Transfer_Id BETWEEN @Min_Cost_Usage_Site_Dtl_Transfer_Id
                                                                          AND     ( @Min_Cost_Usage_Site_Dtl_Transfer_Id + @Batch_Size - 1 ) ) AS src_cuad
                        ON ( tgt_cuad.Bucket_Master_Id = src_cuad.Bucket_Master_Id
                             AND tgt_cuad.Client_Hier_Id = src_cuad.Client_Hier_Id
                             AND tgt_cuad.Service_Month = src_cuad.Service_Month )
                        WHEN MATCHED AND ( src_cuad.Sys_Change_Operation = 'D'
                                           AND src_cuad.Bucket_Value IS NULL
                                           AND ( tgt_cuad.Data_Source_Cd = @Data_Source_Cd_CBMS
                                                 OR tgt_cuad.Data_Source_Cd = @Data_Source_Cd_DE ) )
                              THEN      
								DELETE
                        WHEN MATCHED AND ( tgt_cuad.Data_Source_Cd = @Data_Source_Cd_CBMS
                                           OR tgt_cuad.Data_Source_Cd = @Data_Source_Cd_DE )
                              THEN            
								UPDATE
                                   SET
                                    tgt_cuad.Bucket_Value = src_cuad.Bucket_Value
                                   ,tgt_cuad.UOM_Type_Id = src_cuad.UOM_Type_Id
                                   ,tgt_cuad.Currency_Unit_Id = src_cuad.Currency_Unit_Id
                                   ,tgt_cuad.Updated_By_Id = src_cuad.Created_By_Id
                                   ,tgt_cuad.Created_Ts = src_cuad.Created_Ts
                                   ,tgt_cuad.Updated_Ts = ISNULL(src_cuad.Updated_Ts, GETDATE())
                                   ,tgt_cuad.Data_Source_Cd = src_cuad.Data_Source_Cd
                                   ,tgt_cuad.Data_Type_Cd = src_cuad.Data_Type_Cd
                                   ,tgt_cuad.Actual_Bucket_Value = src_cuad.Actual_Bucket_Value
                                   ,tgt_cuad.Estimated_Bucket_Value = src_cuad.Estimated_Bucket_Value
                        WHEN NOT MATCHED AND ( src_cuad.Sys_Change_Operation IN ( 'I', 'U' ) )
                              THEN            
								INSERT
                                    ( 
                                     Bucket_Master_Id
                                    ,Bucket_Value
                                    ,UOM_Type_Id
                                    ,Currency_Unit_Id
                                    ,Created_By_Id
                                    ,Created_Ts
                                    ,Updated_By_Id
                                    ,Updated_Ts
                                    ,Client_Hier_Id
                                    ,Service_Month
                                    ,Data_Source_Cd
                                    ,Data_Type_Cd
                                    ,Actual_Bucket_Value
                                    ,Estimated_Bucket_Value )
                                   VALUES
                                    ( 
                                     src_cuad.Bucket_Master_Id
                                    ,src_cuad.Bucket_Value
                                    ,src_cuad.UOM_Type_Id
                                    ,src_cuad.Currency_Unit_Id
                                    ,src_cuad.Created_By_Id
                                    ,src_cuad.Created_Ts
                                    ,src_cuad.Updated_By_Id
                                    ,src_cuad.Updated_Ts
                                    ,src_cuad.Client_Hier_Id
                                    ,src_cuad.Service_Month
                                    ,src_cuad.Data_Source_Cd
                                    ,src_cuad.Data_Type_Cd
                                    ,src_cuad.Actual_Bucket_Value
                                    ,src_cuad.Estimated_Bucket_Value );    
                                     
                  SET @Min_Cost_Usage_Site_Dtl_Transfer_Id = @Min_Cost_Usage_Site_Dtl_Transfer_Id + @Batch_Size     
            END 
END;
;

;

;
GO


GRANT EXECUTE ON  [dbo].[Merge_DE_Cost_Usage_Site_Dtl_Changes] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Merge_DE_Cost_Usage_Site_Dtl_Changes] TO [ETL_Execute]
GO
