SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:  [Workflow].[Workflow_Queue_Search_DataSources]   
DESCRIPTION: it'll Return the Datasource information 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @Key_Word varchar(200) = NULL,    
 @Start_Index INT = 1,    
 @End_Index INT = 2147483647  
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Aug-2019 Created
 TRK		  Sep-2019 Migrated to Workflow Schema.

******/     


CREATE PROC [Workflow].[Workflow_Queue_Search_DataSources]    
(      
 @Key_Word varchar(200) = NULL,      
 @Start_Index INT = 1,      
 @End_Index INT = 2147483647      
)      
AS        
BEGIN        
      
     
 ;WITH CTE AS      
 (      
 SELECT UBM_ID, UBM_NAME,      
 ROW_NUMBER() OVER (ORDER BY UBM_NAME)AS Row_Num 
 FROM
 (
 SELECT   ubm_id, ubm_name   
 FROM dbo.UBM WHERE UBM_NAME Like '%' + isNull(@Key_Word, UBM_NAME) + '%'    
 UNION
 SELECT 0 AS ubm_id,'Manual'  AS ubm_name 
 ) A)
 
 SELECT UBM_ID, UBM_NAME FROM CTE WHERE Row_Num BETWEEN @Start_Index  AND     @End_Index      
        ORDER BY UBM_NAME      
      
      
END;   
   

GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_DataSources] TO [CBMSApplication]
GO
