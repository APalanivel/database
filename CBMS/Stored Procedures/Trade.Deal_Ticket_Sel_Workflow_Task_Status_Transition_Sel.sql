SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  291
	Exec Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel  291,24
	Exec Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel  291,17
	Exec Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel  291,16
	
	Exec Trade.Deal_Ticket_Current_Task_Sel_By_Deal_Ticket_Id  288
	Exec Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel  288,26
	Exec Trade.Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel  288,16
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava    
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
     
    
******/  

CREATE PROCEDURE [Trade].[Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel]
      ( 
       @Deal_Ticket_Id INT
      ,@Workflow_Task_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON;	
            
      SELECT
            dtch.Deal_Ticket_Id
           ,wt.Task_Name
           ,wftn.Transition_Name
           ,wftn.Workflow_Transition_Id
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN -1
                 WHEN wtl.Level_Name = 'Participant' THEN dtch.Client_Hier_Id
            END AS Client_Hier_Id
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN NULL
                 WHEN wtl.Level_Name = 'Participant' THEN ch.Site_name
            END AS Site_Name
           ,pinfo.PERMISSION_NAME
           ,wtpi.Permission_Info_Id
      FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                  ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                  ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                  ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            INNER JOIN Trade.Workflow_Task_Status_Map tsm
                  ON tsm.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Task wt
                  ON tsm.Workflow_Task_Id = wt.Workflow_Task_Id
            INNER JOIN Trade.Workflow_Task_Status_Transition_Map wtsm
                  ON wtsm.Workflow_Task_Status_Map_Id = tsm.Workflow_Task_Status_Map_Id
            INNER JOIN Trade.Workflow_Transition wftn
                  ON wtsm.Workflow_Transition_Id = wftn.Workflow_Transition_Id
            INNER JOIN Trade.Workflow_Transition_Level wtl
                  ON wtsm.Workflow_Transition_Level_Id = wtl.Workflow_Transition_Level_Id
            INNER JOIN Core.Client_Hier ch
                  ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            LEFT JOIN dbo.Workflow_Task_Permission_Info_Map wtpi
                  ON wtpi.Workflow_Task_Id = tsm.Workflow_Task_Id
            LEFT JOIN dbo.PERMISSION_INFO pinfo
                  ON wtpi.Permission_Info_Id = pinfo.PERMISSION_INFO_ID
      WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            --AND chws.Is_Active = 1
            --AND chws.Is_Active = CASE WHEN EXISTS ( SELECT
            --                                          1
            --                                        FROM
            --                                          Trade.Deal_Ticket_Client_Hier_Workflow_Status dtc
            --                                        WHERE
            --                                          dtc.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
            --                                          AND Is_Active = 1 ) THEN 1
            --                          ELSE 0
            --                     END
            AND tsm.Workflow_Task_Id = @Workflow_Task_Id
      GROUP BY
            dtch.Deal_Ticket_Id
           ,wt.Task_Name
           ,wftn.Transition_Name
           ,wftn.Workflow_Transition_Id
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN -1
                 WHEN wtl.Level_Name = 'Participant' THEN dtch.Client_Hier_Id
            END
           ,CASE WHEN wtl.Level_Name = 'Deal Ticket' THEN NULL
                 WHEN wtl.Level_Name = 'Participant' THEN ch.Site_name
            END
           ,pinfo.PERMISSION_NAME
           ,wtpi.Permission_Info_Id
     
                  
      
            
      
                   
  
END;




GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Sel_Workflow_Task_Status_Transition_Sel] TO [CBMSApplication]
GO
