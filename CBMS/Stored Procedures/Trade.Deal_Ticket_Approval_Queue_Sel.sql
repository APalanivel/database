SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Approval_Queue_Sel
   
    
DESCRIPTION:   
   
  This procedure to get summary page details Deal Ticket Approval Queue.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	 @Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Approval_Queue_Sel  @Geographic_Region_Id = 1
	Exec Trade.Deal_Ticket_Approval_Queue_Sel 112
	Exec Trade.Deal_Ticket_Approval_Queue_Sel 49
	
	Exec Trade.Deal_Ticket_Approval_Queue_Sel @User_Info_Id = 112, @Start_Index = 1, @End_Index = 5
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	NR          Narayana Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	NR          2019-01-10	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Deal_Ticket_Approval_Queue_Sel]
    (
        @User_Info_Id INT = NULL
        , @Deal_Ticket_Number NVARCHAR(255) = NULL
        , @Client_Id INT = NULL
        , @Site_Client_Hier_Id VARCHAR(MAX) = NULL
        , @Hedge_Start_Dt DATE = NULL
        , @Hedge_End_Dt DATE = NULL
        , @Currently_With_User_Info_Id VARCHAR(MAX) = NULL
        , @Initiated_By_User_Info_Id VARCHAR(MAX) = NULL
        , @Geographic_Region_Id INT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Tbl_Dt_Dtls AS TABLE
              (
                  Deal_Ticket_Id INT
                  , Deal_Ticket_Number NVARCHAR(255)
                  , Client_Name VARCHAR(200)
                  , Sites VARCHAR(600)
                  , Date_Initiated VARCHAR(50)
                  , Deal_Type VARCHAR(50)
                  , Hedge_Type VARCHAR(200)
                  , Commodity_Name VARCHAR(50)
                  , Deal_Status NVARCHAR(255)
                  , Initiated_By VARCHAR(50)
                  , Currently_With VARCHAR(50)
                  , Workflow_Name NVARCHAR(255)
                  , Workflow_Description NVARCHAR(MAX)
                    --,Row_Num INT IDENTITY(1, 1) )
                  , Row_Num INT
                  , Client_Id INT
                  , Sitegroup_Id INT
                  , Site_Id INT
              );

        DECLARE @Total INT;

        INSERT INTO @Tbl_Dt_Dtls
             (
                 Deal_Ticket_Id
                 , Deal_Ticket_Number
                 , Client_Name
                 , Sites
                 , Date_Initiated
                 , Deal_Type
                 , Hedge_Type
                 , Commodity_Name
                 , Deal_Status
                 , Initiated_By
                 , Currently_With
                 , Workflow_Name
                 , Workflow_Description
                 , Row_Num
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Id
             )
        SELECT
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                  ELSE MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
              END AS Sites
            , REPLACE(CONVERT(VARCHAR(15), dt.Created_Ts, 106), ' ', '-') AS Date_Initiated
            , atype.Code_Value + ' ' + ttype.Code_Value AS Deal_Type
            , ht.ENTITY_NAME AS Hedge_Type
            , c.Commodity_Name
            , CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN 'Multiple'
                  ELSE MAX(ws.Workflow_Status_Name)
              END AS Deal_Status
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Initiated_By
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME AS Currently_With
            , w.Workflow_Name
            , w.Workflow_Description
            , ROW_NUMBER() OVER (ORDER BY
                                     dt.Deal_Ticket_Id DESC)
            , ch.Client_Id
            , CASE WHEN COUNT(DISTINCT ch.Sitegroup_Id) > 1 THEN -1
                  ELSE MAX(ch.Sitegroup_Id)
              END AS Sitegroup_Id
            , CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                  ELSE MAX(ch.Site_Id)
              END AS Site_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = dt.Client_Id
                   AND  ch.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.Workflow w
                ON w.Workflow_Id = dt.Workflow_Id
            LEFT JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = dt.Created_User_Id
            INNER JOIN dbo.Code ttype
                ON dt.Deal_Ticket_Type_Cd = ttype.Code_Id
            INNER JOIN dbo.Code atype
                ON dt.Trade_Action_Type_Cd = atype.Code_Id
            INNER JOIN dbo.ENTITY ht
                ON dt.Hedge_Type_Cd = ht.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN dbo.USER_INFO cui
                ON cui.QUEUE_ID = dt.Queue_Id
        WHERE
            chws.Is_Active = 1
            AND ws.Workflow_Status_Name = 'Pending Internal Approval'
            AND (   @Deal_Ticket_Number IS NULL
                    OR  dt.Deal_Ticket_Number = @Deal_Ticket_Number)
            AND (   @Client_Id IS NULL
                    OR  ch.Client_Id = @Client_Id)
            AND (   @Site_Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Client_Hier_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = dtch.Client_Hier_Id))
            AND (   @Hedge_Start_Dt IS NULL
                    OR  dt.Hedge_Start_Dt = @Hedge_Start_Dt)
            AND (   @Hedge_End_Dt IS NULL
                    OR  dt.Hedge_End_Dt = @Hedge_End_Dt)
            AND (   @Initiated_By_User_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Initiated_By_User_Info_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = dt.Created_User_Id))
            AND (   @Geographic_Region_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Geographic_Region_Country_Map grc
                                   WHERE
                                        grc.Geographic_Region_Id = @Geographic_Region_Id
                                        AND ch.Country_Id = grc.COUNTRY_ID))
            AND (   @Currently_With_User_Info_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Currently_With_User_Info_Id, ',') usid
                                        INNER JOIN dbo.USER_INFO cui
                                            ON CAST(usid.Segments AS INT) = cui.USER_INFO_ID
                                   WHERE
                                        cui.QUEUE_ID = dt.Queue_Id))
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.Geographic_Region_Permission_Info_Map grp
                                INNER JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gipi
                                    ON grp.Permission_Info_Id = gipi.PERMISSION_INFO_ID
                                INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigi
                                    ON gipi.GROUP_INFO_ID = uigi.GROUP_INFO_ID
                                INNER JOIN dbo.Geographic_Region_Country_Map grc
                                    ON grp.Geographic_Region_Id = grc.Geographic_Region_Id
                           WHERE
                                uigi.USER_INFO_ID = @User_Info_Id
                                AND ch.Country_Id = grc.COUNTRY_ID)
        GROUP BY
            dt.Deal_Ticket_Id
            , dt.Deal_Ticket_Number
            , ch.Client_Name
            , dt.Created_Ts
            , atype.Code_Value + ' ' + ttype.Code_Value
            , ht.ENTITY_NAME
            , c.Commodity_Name
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , w.Workflow_Name
            , w.Workflow_Description
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME
            , ch.Client_Id;




        --ORDER BY
        --      dt.Deal_Ticket_Id DESC

        SELECT  @Total = MAX(Row_Num)FROM   @Tbl_Dt_Dtls;

        SELECT
            Deal_Ticket_Id
            , Deal_Ticket_Number
            , Client_Name
            , Sites
            , Date_Initiated
            , Deal_Type
            , Hedge_Type
            , Commodity_Name
            , Deal_Status
            , Initiated_By
            , Currently_With
            , Workflow_Name
            , Workflow_Description
            , Row_Num
            , @Total AS Total
            , Client_Id
            , Sitegroup_Id
            , Site_Id
        FROM
            @Tbl_Dt_Dtls
        WHERE
            Row_Num BETWEEN @Start_Index
                    AND     @End_Index;
    END;





GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Approval_Queue_Sel] TO [CBMSApplication]
GO
