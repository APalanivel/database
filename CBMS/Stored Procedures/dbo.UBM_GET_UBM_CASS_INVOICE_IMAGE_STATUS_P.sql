SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE dbo.UBM_GET_UBM_CASS_INVOICE_IMAGE_STATUS_P
	@entityName VARCHAR(200),    
	@cbmsDocId VARCHAR(200)         
AS
BEGIN    
    
	SET NOCOUNT ON

	DECLARE @cbms_image_id INT

	SELECT @cbms_image_id = Cbms_Image_Id FROM dbo.Cbms_Image WHERE Cbms_Doc_Id = @cbmsDocId

	SELECT @cbms_image_id AS ImageID
		, Entity_Id AS EntityId
	FROM dbo.Entity (NOLOCK)
	WHERE Entity_Name = @entityname
		AND Entity_Type = 100

END
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_CASS_INVOICE_IMAGE_STATUS_P] TO [CBMSApplication]
GO
