SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Search                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Search  @Client_Id = 1005,@Start_Dt='2019-01-01'
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-12-23	RM-Budgets Enahancement - Created
	RR			2019-12-23	GRM-1765 Added input parameter @RM_Group_Id
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Search]
    (
        @Client_Id INT
        , @Division_Id INT = NULL
        , @Site_Id VARCHAR(MAX) = NULL
        , @Start_Dt DATE = NULL
        , @End_Dt DATE = NULL
        , @Index_Id INT = NULL
        , @Is_Budget_Approved BIT = NULL
        , @Rm_Budget_Id INT = NULL
        , @RM_Group_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #RM_Group_Sites
             (
                 [Site_Id] INT
                 , Client_Id INT
                 , Sitegroup_Id INT
                 , Site_name VARCHAR(1000)
             );

        INSERT INTO #RM_Group_Sites
             (
                 Site_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_name
             )
        EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id
            @Cbms_Sitegroup_Id = @RM_Group_Id;

        WITH Cte_RMBudgets
        AS (
               SELECT
                    rb.Rm_Budget_Id
                    , rb.Budget_NAME
                    , rb.Client_Id
                    , c.CLIENT_NAME
                    , bdgttyp.Code_Value AS Budget_Type
                    , bdglvl.Code_Value AS Budget_Level
                    , indx.ENTITY_NAME AS Index_Name
                    , cu.CURRENCY_UNIT_NAME
                    , rb.Budget_Start_Dt
                    , rb.Budget_End_Dt
                    , CASE rb.Is_Budget_Approved WHEN 1 THEN 'Yes'
                          ELSE 'No'
                      END AS Is_Budget_Approved
                    , rb.Is_Client_Generated
                    , c2.Comment_Text
                    , ROW_NUMBER() OVER (ORDER BY
                                             rb.Rm_Budget_Id DESC) AS Row_Num
                    , rb.Index_Id
                    , rb.Budget_Type_Cd
                    , rb.Budget_Level_Cd
                    , rb.Uom_Type_Id
                    , cu.CURRENCY_UNIT_ID
                    , uom.ENTITY_NAME AS Uom_Name
                    , c3.Commodity_Name
                    , rb.Commodity_Id
                    , rb.Rm_Forecast_Id
                    , rf.FORECAST_AS_OF_DATE AS AS_OF_DATE
                    , rf.FORECAST_FROM_DATE
                    , rf.FORECAST_TO_DATE
                    , rb.RM_Budget_Participant_Sites_Type_Cd
                    , c4.Code_Value
                    , REPLACE(CONVERT(VARCHAR(20), rb.Approved_Ts, 106), ' ', '-') AS Approved_Date
               FROM
                    Trade.Rm_Budget rb
                    INNER JOIN dbo.CLIENT c
                        ON c.CLIENT_ID = rb.Client_Id
                    INNER JOIN dbo.ENTITY indx
                        ON indx.ENTITY_ID = rb.Index_Id
                    INNER JOIN dbo.Code bdgttyp
                        ON bdgttyp.Code_Id = rb.Budget_Type_Cd
                    INNER JOIN dbo.Code bdglvl
                        ON rb.Budget_Level_Cd = bdglvl.Code_Id
                    INNER JOIN dbo.CURRENCY_UNIT cu
                        ON rb.Currency_Unit_Id = cu.CURRENCY_UNIT_ID
                    LEFT JOIN Trade.Rm_Budget_Comment rbc
                        ON rbc.Rm_Budget_Id = rb.Rm_Budget_Id
                    LEFT JOIN dbo.Comment c2
                        ON c2.Comment_ID = rbc.Comment_Id
                    LEFT JOIN dbo.Commodity c3
                        ON c3.Commodity_Id = rb.Commodity_Id
                    LEFT JOIN dbo.ENTITY uom
                        ON uom.ENTITY_ID = rb.Uom_Type_Id
                    LEFT JOIN dbo.RM_FORECAST rf
                        ON rf.RM_FORECAST_ID = rb.Rm_Forecast_Id
                    LEFT JOIN dbo.Code c4
                        ON c4.Code_Id = rb.RM_Budget_Participant_Sites_Type_Cd
               WHERE
                    (   @Client_Id IS NULL
                        OR  rb.Client_Id = @Client_Id)
                    AND (   @Rm_Budget_Id IS NULL
                            OR  rb.Rm_Budget_Id = @Rm_Budget_Id)
                    AND (   (   @Start_Dt IS NULL
                                AND @End_Dt IS NULL)
                            OR  (   @Start_Dt IS NOT NULL
                                    AND @End_Dt IS NULL
                                    AND @Start_Dt = rb.Budget_Start_Dt)
                            OR  (   @Start_Dt IS NULL
                                    AND @End_Dt IS NOT NULL
                                    AND @End_Dt = rb.Budget_End_Dt)
                            OR  (   (   @Start_Dt IS NOT NULL
                                        AND @End_Dt IS NOT NULL)
                                    AND (   @Start_Dt BETWEEN rb.Budget_Start_Dt
                                                      AND     rb.Budget_End_Dt
                                            OR  @End_Dt BETWEEN rb.Budget_Start_Dt
                                                        AND     rb.Budget_End_Dt)))
                    AND (   @Index_Id IS NULL
                            OR  rb.Index_Id = @Index_Id)
                    AND (   @Is_Budget_Approved IS NULL
                            OR  rb.Is_Budget_Approved = @Is_Budget_Approved)
                    AND EXISTS (   SELECT
                                        1
                                   FROM
                                        Trade.Rm_Budget_Participant rbp
                                        INNER JOIN Core.Client_Hier ch
                                            ON ch.Client_Hier_Id = rbp.Client_Hier_Id
                                   WHERE
                                        rbp.Rm_Budget_Id = rb.Rm_Budget_Id
                                        AND ch.Client_Id = @Client_Id
                                        AND (   @Division_Id IS NULL
                                                OR  ch.Sitegroup_Id = @Division_Id)
                                        AND (   @Site_Id IS NULL
                                                OR  EXISTS (   SELECT
                                                                    1
                                                               FROM
                                                                    dbo.ufn_split(@Site_Id, ',') us
                                                               WHERE
                                                                    CAST(us.Segments AS INT) = ch.Site_Id))
                                        AND (   @RM_Group_Id IS NULL
                                                OR  EXISTS (SELECT  1 FROM  #RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id)))
           )
        SELECT
            Cte_RMBudgets.Rm_Budget_Id
            , Budget_NAME
            , Client_Id
            , CLIENT_NAME
            , Budget_Type
            , Budget_Level
            , Index_Name
            , CURRENCY_UNIT_NAME
            , Budget_Start_Dt
            , Budget_End_Dt
            , Is_Budget_Approved
            , Is_Client_Generated
            , Comment_Text
            , Row_Num
            , cnt.Total_Cnt
            , Index_Id
            , Budget_Type_Cd
            , Budget_Level_Cd
            , Uom_Type_Id
            , CURRENCY_UNIT_ID
            , Uom_Name
            , Commodity_Name
            , Commodity_Id
            , Entt_Cnt.Entity_Cnt
            , Rm_Forecast_Id
            , AS_OF_DATE
            , FORECAST_FROM_DATE
            , FORECAST_TO_DATE
            , RM_Budget_Participant_Sites_Type_Cd
            , Code_Value AS RM_Budget_Participant_Sites_Type
            , Approved_Date
        FROM
            Cte_RMBudgets
            CROSS JOIN
            (   SELECT
                    MAX(Row_Num) AS Total_Cnt
                FROM
                    Cte_RMBudgets) cnt
            LEFT JOIN
            (   SELECT
                    rbvpd.Rm_Budget_Id
                    , COUNT(DISTINCT rbvpd.Client_Hier_Id) AS Entity_Cnt
                FROM
                    Trade.Rm_Budget_Volume_Price_Dtl rbvpd
                GROUP BY
                    rbvpd.Rm_Budget_Id) Entt_Cnt
                ON Entt_Cnt.Rm_Budget_Id = Cte_RMBudgets.Rm_Budget_Id
        ORDER BY
            Cte_RMBudgets.Row_Num;
    END;


GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Search] TO [CBMSApplication]
GO
