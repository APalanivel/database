
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:  dbo.BUDGET_GET_HEADER_P

DESCRIPTION: 
				To get the budget details for the given  the budget_account_id/

INPUT PARAMETERS: 
Name				DataType			Default			Description 
------------------------------------------------------------------------------- 
@userId				VARCHAR(10)
@sessionId			VARCHAR(20)		
@budget_account_id	INT

OUTPUT PARAMETERS:   
Name				DataType			Default			Description 
------------------------------------------------------------------------------- 

USAGE EXAMPLES:   
-------------------------------------------------------------------------------  
 
 exec BUDGET_GET_HEADER_P '','',42124
 

AUTHOR INITIALS:   
Initials	Name   
-------------------------------------------------------------------------------   
NR			Narayana Reddy


MODIFICATIONS:  
Initials	Date			Modification 
-------------------------------------------------------------------------------   
NR			2016-05-30		MAINT-4092 Added Header and Added ACCOUNT_ID in Output.
	 	 		 
******/
CREATE PROCEDURE [dbo].[BUDGET_GET_HEADER_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@budget_account_id INT )
AS 
BEGIN
      SET NOCOUNT ON
      SELECT
            budget.budget_id
           ,cli.client_name
           ,site.site_name
           ,site.state_id
           ,utilacc.account_number
           ,commodity_type.entity_id AS commodityid
           ,commodity_type.entity_name AS commodity
           ,utility.vendor_id
           ,utility.vendor_name AS utility_name
           ,budget.budget_name
           ,budget.budget_start_year
           ,budget.due_date
           ,dbo.BUDGET_FN_GET_RATES(utilacc.account_id, budget.commodity_type_id) rates
           ,vw.tariff_transport
           ,service_level.entity_name AS service_level_char
           ,budget_xml.budget_detail_snap_shot_id AS budget_xml_id
           ,ra_notes.cbms_image_id AS ra_notes_id
           ,ra_notes.cbms_doc_id AS ra_notes_name
           ,sa_notes.cbms_image_id AS sa_notes_id
           ,sa_notes.cbms_doc_id AS sa_notes_name
           ,sa.user_info_id AS sa_user_id
           ,sa.username AS sa_username
           ,ra.user_info_id AS ra_user_id
           ,ra.username AS ra_username
           ,ra_reviewed.user_info_id AS ra_reviewed_user_id
           ,ra_reviewed.username AS ra_reviewed_username
           ,cp.user_info_id AS cp_user_id
           ,cp.username AS cp_username
           ,budget_account.sourcing_completed_date
           ,budget_account.rates_completed_date
           ,budget_account.rates_reviewed_date
           ,budget_account.cannot_performed_date
           ,budget_account.is_reforecast
           ,budget_status_type.entity_name AS budget_status
           ,utilacc.not_managed AS not_managed
           ,utilacc.ACCOUNT_ID
      FROM
            budget_account budget_account
            INNER JOIN budget budget
                  ON budget.budget_id = budget_account.budget_id
                     AND budget_account.budget_account_id = @budget_account_id
            INNER JOIN entity budget_status_type
                  ON budget_status_type.entity_id = budget.status_type_id
            INNER JOIN account utilacc
                  ON utilacc.account_id = budget_account.account_id
            INNER JOIN BUDGET_TARIFF_TRANSPORT_VW vw
                  ON vw.account_id = utilacc.account_id
                     AND vw.commodity_type_id = budget.commodity_type_id
            INNER JOIN vwSiteName site
                  ON site.site_id = utilacc.site_id
            INNER  JOIN client cli
                  ON cli.client_id = site.client_id
            INNER JOIN entity commodity_type
                  ON commodity_type.entity_id = budget.commodity_type_id
            INNER JOIN vendor utility
                  ON utility.vendor_id = utilacc.vendor_id
            INNER JOIN entity service_level
                  ON service_level.entity_id = utilacc.service_level_type_id
            LEFT JOIN user_info sa
                  ON sa.user_info_id = budget_account.sourcing_completed_by
            LEFT JOIN user_info ra
                  ON ra.user_info_id = budget_account.rates_completed_by
            LEFT JOIN user_info ra_reviewed
                  ON ra_reviewed.user_info_id = budget_account.rates_reviewed_by
            LEFT JOIN user_info cp
                  ON cp.user_info_id = budget_account.cannot_performed_by
            LEFT JOIN budget_detail_snap_shot budget_xml
                  ON budget_xml.budget_account_id = budget_account.budget_account_id
            LEFT JOIN cbms_image ra_notes
                  ON ra_notes.cbms_image_id = budget_account.rate_notes_id
            LEFT JOIN cbms_image sa_notes
                  ON sa_notes.cbms_image_id = budget_account.sourcing_notes_id

END























;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_GET_HEADER_P] TO [CBMSApplication]
GO
