SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec GET_SITE_ACCOUNTS_QUERY_P 27, 38

CREATE     PROCEDURE dbo.GET_SITE_ACCOUNTS_QUERY_P
	@site_id int,
	@account_type_id int
	AS
	begin
		set nocount on

		select a.ACCOUNT_ID,
		       a.ACCOUNT_NUMBER,
		       a.VENDOR_ID,
		       v.vendor_name,
		       e.entity_name 
		from   account a
		       join vendor v on v.vendor_id= a.vendor_id
		       and a.site_id=@site_id
		       and a.account_type_id=@account_type_id 
		       join entity e on e.entity_id=a.service_level_type_id

	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_ACCOUNTS_QUERY_P] TO [CBMSApplication]
GO
