SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Make_Site_Managed_By_Site_Id

DESCRIPTION:

	Used to update site details and make account Managed.
	
	Making a site managed will cover the following activities

		- Delete the accounts of that site from Do not track list
		- Update all the accounts of that site with not managed = 0
		- Update the invoice participation as Managed for that site
		- Update invoice participation as Expected	for that site

INPUT PARAMETERS:    
	 Name                       DataType          Default     Description    
---------------------------------------------------------------------------------    
	 @Site_Type_Id				INTEGER
	 @Site_Name					VARCHAR(200)
	 @Division_Id				INTEGER
	 @UbmSite_Id				VARCHAR(30)
	 @Site_Reference_Number		VARCHAR(30)
	 @Site_Product_Service		VARCHAR(2000)
	 @Production_Schedule		VARCHAR(2000)
	 @Shutdown_Schedule			VARCHAR(2000)
	 @Is_Alternate_Power		BIT
	 @Is_Alternate_Gas			BIT
	 @Doing_Business_As			VARCHAR(200)
	 @Naics_Code				VARCHAR(30)
	 @Tax_Number				VARCHAR(30)
	 @Duns_Number				VARCHAR(30)
	 @User_Info_id				INTEGER
	 @Contracting_Entity		VARCHAR(200)
	 @Client_Legal_Structure	VARCHAR(4000)
	 @Lp_Contact_First_Name		VARCHAR(50)
	 @Lp_Contact_Last_Name		VARCHAR(50)
	 @Lp_Contact_Email_Address	VARCHAR(50)
	 @Lp_Contact_CC				VARCHAR(400)
	 @Is_Preference_By_Email	BIT
	 @Is_Preference_By_Dv		BIT
	 @Site_Id					INTEGER                           
                    
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN	
		EXEC DBO.Make_Site_Managed_By_Site_Id 
				@Site_Type_Id = 250,
				@Site_Name = 'Rockford, IL--CLOSED',
				@Division_Id =210 ,
				@UbmSite_Id ='' ,
				@Site_Reference_Number ='' ,
				@Site_Product_Service ='',
				@Production_Schedule ='',
				@Shutdown_Schedule ='', 
				@Is_Alternate_Power =0,
				@Is_Alternate_Gas =0,
				@Doing_Business_As ='',
				@Naics_Code ='',
				@Tax_Number ='363514169', 
				@Duns_Number ='161403-852',
				@User_Info_Id =24,
				@Contracting_Entity ='Newell Rubbermaid Inc.',
				@Client_Legal_Structure ='Newell Rubbermaid Inc. ?Newell? (NWL listed: NYSE CHX) is a global marketer of consumer products with 2002 sales of over $7 billion and a powerful brand family including Sharpie(R), Paper Mate(R), Parker(R), Waterman(R), Colorific(R), Rubbermaid(R), 
										Stain Shield., Blue Ice(R), TakeAlongs(R), Roughneck(R), Brute(R), Calphalon(R), Little Tikes(R), Graco(R), Levolor(R), Kirsch(R), Shur-Line(R), BernzOmatic(R), Goody(R), Vise-Grip(R), Quick-Grip(R), IRWIN(R), Lenox(R), and Marathon(R). The company is headquartered in Atlanta,Georgia and employs approximately 44,000 people worldwide. Additional financial information about Newell (including consolidated financial statements) is available under the Investor Relations section of the company,s website at www.newellrubbermaid.com.  Additionally, a credit summary sheet is available upon request.',
				@Lp_Contact_First_Name ='Jon',
				@Lp_Contact_Last_Name ='Melhorn',
				@Lp_Contact_Email_Address ='jscott@summitenergy.com',
				@Lp_Contact_CC ='jscott@summitenergy.com',
				@Is_Preference_By_Email =1,
				@Is_Preference_By_Dv =0,
				@Site_Id = 45
	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			06/15/2010	Created

******/ 
  
CREATE PROCEDURE dbo.Make_Site_Managed_By_Site_Id  
	  @Site_Type_Id				INTEGER
	, @Site_Name				VARCHAR(200)
	, @Division_Id				INTEGER
	, @UbmSite_Id				VARCHAR(30)
	, @Site_Reference_Number	VARCHAR(30)
	, @Site_Product_Service		VARCHAR(2000)
	, @Production_Schedule		VARCHAR(2000)
	, @Shutdown_Schedule		VARCHAR(2000)
	, @Is_Alternate_Power		BIT
	, @Is_Alternate_Gas			BIT
	, @Doing_Business_As		VARCHAR(200)
	, @Naics_Code				VARCHAR(30)
	, @Tax_Number				VARCHAR(30)
	, @Duns_Number				VARCHAR(30)
	, @User_Info_id				INTEGER
	, @Contracting_Entity		VARCHAR(200)
	, @Client_Legal_Structure	VARCHAR(4000)
	, @Lp_Contact_First_Name	VARCHAR(50)
	, @Lp_Contact_Last_Name		VARCHAR(50)
	, @Lp_Contact_Email_Address	VARCHAR(50)
	, @Lp_Contact_CC			VARCHAR(400)
	, @Is_Preference_By_Email	BIT
	, @Is_Preference_By_Dv		BIT
	, @Site_Id					INTEGER
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRY
		BEGIN TRAN

			EXEC dbo.Make_Account_Managed_Expected_For_Site @site_id, @user_info_id

			UPDATE
				dbo.SITE
				SET
					SITE_TYPE_ID = @Site_Type_Id
					, SITE_NAME = @Site_Name
					, DIVISION_ID = @Division_Id
					, UBMSITE_ID = @UbmSite_Id
					, SITE_REFERENCE_NUMBER = @Site_Reference_Number
					, SITE_PRODUCT_SERVICE = @Site_Product_Service
					, PRODUCTION_SCHEDULE = @Production_Schedule
					, SHUTDOWN_SCHEDULE = @Shutdown_Schedule
					, IS_ALTERNATE_POWER = @Is_Alternate_Power
					, IS_ALTERNATE_GAS = @Is_Alternate_Gas
					, DOING_BUSINESS_AS = @Doing_Business_As
					, NAICS_CODE = @Naics_Code
					, TAX_NUMBER = @Tax_Number
					, DUNS_NUMBER = @Duns_Number
					, NOT_MANAGED = 0
					, USER_INFO_ID = @User_Info_id
					, CONTRACTING_ENTITY = @Contracting_Entity
					, CLIENT_LEGAL_STRUCTURE = @Client_Legal_Structure
					, LP_CONTACT_FIRST_NAME = @Lp_Contact_First_Name
					, LP_CONTACT_LAST_NAME = @Lp_Contact_Last_Name
					, LP_CONTACT_EMAIL_ADDRESS = @Lp_Contact_Email_Address
					, LP_CONTACT_CC = @Lp_Contact_CC
					, IS_PREFERENCE_BY_EMAIL = @Is_Preference_By_Email
					, IS_PREFERENCE_BY_DV = @Is_Preference_By_Dv
			WHERE
				site_id = @Site_Id

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END
		
		EXEC dbo.usp_RethrowError
	END CATCH
END
GO
GRANT EXECUTE ON  [dbo].[Make_Site_Managed_By_Site_Id] TO [CBMSApplication]
GO
