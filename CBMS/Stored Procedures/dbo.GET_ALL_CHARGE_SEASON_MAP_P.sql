
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
  dbo.GET_ALL_CHARGE_SEASON_MAP_P
  
DESCRIPTION:   
	To get the mapped season details for the given charge id
	 
  
INPUT PARAMETERS:      
	Name            DataType          Default     Description      
-------------------------------------------------------------      
	@chargeId		INT	


OUTPUT PARAMETERS:
      Name          DataType          Default     Description
------------------------------------------------------------- 

USAGE EXAMPLES:
-------------------------------------------------------------
	Exec dbo.GET_ALL_CHARGE_SEASON_MAP_P 547818
	Exec dbo.GET_ALL_CHARGE_SEASON_MAP_P 547776

	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

	Initials	Date		Modification
------------------------------------------------------------    
	RR			2012-06-08	Added description header.
							Added two more columns SEASON_FROM_DATE, SEASON_TO_DATE to the result set.
							CHARGE_SEASON_MAP_ID is selecting twice from two tables dbo.CHARGE_SEASON_MAP,dbo.TIER_DETAIL, both the tables 
							joined by the same column and have the same column name, so removed from column list selecting from secondary 
							table dbo.TIER_DETAIL


*/ 
CREATE PROCEDURE dbo.GET_ALL_CHARGE_SEASON_MAP_P ( @chargeId INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            CSM.CHARGE_SEASON_MAP_ID
           ,CSM.SEASON_ID
           ,SES.SEASON_NAME
           ,CSM.RATE
           ,TD.TIER_DETAIL_ID
           ,TD.LOWER_BOUND
           ,TD.UPPER_BOUND
           ,SES.SEASON_FROM_DATE
           ,SES.SEASON_TO_DATE
      FROM
            dbo.CHARGE_SEASON_MAP CSM
            INNER JOIN dbo.SEASON SES
                  ON SES.SEASON_ID = CSM.SEASON_ID
            LEFT JOIN dbo.TIER_DETAIL TD
                  ON CSM.CHARGE_SEASON_MAP_ID = TD.CHARGE_SEASON_MAP_ID
      WHERE
            CSM.CHARGE_ID = @chargeId


END
;
GO

GRANT EXECUTE ON  [dbo].[GET_ALL_CHARGE_SEASON_MAP_P] TO [CBMSApplication]
GO
