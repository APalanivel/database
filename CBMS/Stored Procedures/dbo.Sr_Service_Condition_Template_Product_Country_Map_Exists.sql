SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Product_Country_Map_Exists]
           
DESCRIPTION:             
			To get any templates referring unmapped product-country combination
			
INPUT PARAMETERS:            
	Name					DataType	Default		Description  
---------------------------------------------------------------------------------  
	@SR_PRICING_PRODUCT_ID	INT
    @Countrty_Id_List		VARCHAR(MAX)


OUTPUT PARAMETERS:
	Name			DataType		Default		Description  
---------------------------------------------------------------------------------  


 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT spp.SR_PRICING_PRODUCT_ID,cspp.Country_Id,cspp.SR_Pricing_Product_Country_Map_Id,tpm.Sr_Service_Condition_Template_Id
	FROM dbo.SR_PRICING_PRODUCT spp INNER JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
	INNER JOIN dbo.Sr_Service_Condition_Template_Product_Map tpm ON cspp.SR_Pricing_Product_Country_Map_Id = tpm.SR_Pricing_Product_Country_Map_Id
	WHERE spp.SR_PRICING_PRODUCT_ID = 179
	
	EXEC dbo.Sr_Service_Condition_Template_Product_Country_Map_Exists 179,'123,14'
	
	
	SELECT spp.SR_PRICING_PRODUCT_ID,cspp.Country_Id,cspp.SR_Pricing_Product_Country_Map_Id,tpm.Sr_Service_Condition_Template_Id
	FROM dbo.SR_PRICING_PRODUCT spp INNER JOIN dbo.SR_Pricing_Product_Country_Map cspp ON spp.SR_PRICING_PRODUCT_ID = cspp.SR_PRICING_PRODUCT_ID
	INNER JOIN dbo.Sr_Service_Condition_Template_Product_Map tpm ON cspp.SR_Pricing_Product_Country_Map_Id = tpm.SR_Pricing_Product_Country_Map_Id
	WHERE spp.SR_PRICING_PRODUCT_ID = 194
	
	EXEC dbo.Sr_Service_Condition_Template_Product_Country_Map_Exists 194

 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-13	Global Sourcing - Phase3 - GCS-618 Created
								
******/

CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Product_Country_Map_Exists]
      ( 
       @SR_PRICING_PRODUCT_ID INT
      ,@Countrty_Id_List VARCHAR(MAX) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            tpm.Sr_Service_Condition_Template_Id
           ,sppcm.SR_Pricing_Product_Country_Map_Id
           ,sppcm.Country_Id
      FROM
            dbo.Sr_Service_Condition_Template_Product_Map tpm
            INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                  ON tpm.SR_Pricing_Product_Country_Map_Id = sppcm.SR_Pricing_Product_Country_Map_Id
      WHERE
            sppcm.SR_Pricing_Product_Id = @SR_PRICING_PRODUCT_ID
            AND ( @Countrty_Id_List IS NULL
                  OR NOT EXISTS ( SELECT
                                    1
                                  FROM
                                    dbo.ufn_split(@Countrty_Id_List, ',')
                                  WHERE
                                    cast(Segments AS INT) = sppcm.Country_Id ) )

     
                  

END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Product_Country_Map_Exists] TO [CBMSApplication]
GO
