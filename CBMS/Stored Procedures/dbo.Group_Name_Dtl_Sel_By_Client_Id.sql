SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Group_Name_Dtl_Sel_By_Client_Id        
                
Description:                
		This sproc to get the group detailsbased on client       
                             
 Input Parameters:                
    Name						DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@Client_Id					INT
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     

   Exec dbo.Group_Name_Dtl_Sel_By_Client_Id   170
   
   Exec dbo.Group_Name_Dtl_Sel_By_Client_Id   10687
       
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2016-11-15		Created For MAINT-4563.           
               
******/   
CREATE PROCEDURE [dbo].[Group_Name_Dtl_Sel_By_Client_Id] ( @Client_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 

      SELECT
            ecag.Ec_Client_Account_Group_Id
           ,ecag.Account_Group_Name
           ,eagt.Group_Type_Name
           ,LEFT(acc_Id.Acc_Dtl_Id, LEN(acc_Id.Acc_Dtl_Id) - 1) AS Account_Ids
           ,LEFT(acc.Acc_Dtl, LEN(acc.Acc_Dtl) - 1) AS Accounts
           ,ecag.Start_Dt
           ,ecag.End_Dt
           ,ch.Country_Name
           ,ch.State_Name
           ,c.Commodity_Name
           ,eagt.State_Id
           ,eagt.Ec_Account_Group_Type_Id
           ,eagt.Commodity_Id
      FROM
            dbo.Ec_Account_Group_Type eagt
            INNER JOIN dbo.Ec_Client_Account_Group ecag
                  ON eagt.Ec_Account_Group_Type_Id = ecag.Ec_Account_Group_Type_Id
            INNER JOIN dbo.Ec_Client_Account_Group_Account ecaga
                  ON ecag.Ec_Client_Account_Group_Id = ecaga.Ec_Client_Account_Group_Id
            INNER JOIN Core.Client_Hier_Account cha
                  ON ecaga.Account_Id = cha.Account_Id
                     AND cha.Commodity_Id = eagt.Commodity_Id
            INNER JOIN core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
                     AND ch.Client_Id = ecag.Client_Id
                     AND ch.State_Id = eagt.State_Id
            INNER JOIN dbo.Commodity c
                  ON cha.Commodity_Id = c.Commodity_Id
            CROSS APPLY ( SELECT
                              chat.Account_Number + ', '
                          FROM
                              Core.Client_Hier_Account chat
                              INNER JOIN dbo.Ec_Client_Account_Group_Account ecagaa
                                    ON chat.Account_Id = ecagaa.Account_Id
                          WHERE
                              ecagaa.Ec_Client_Account_Group_Id = ecag.Ec_Client_Account_Group_Id
                          GROUP BY
                              chat.Account_Number
            FOR
                          XML PATH('') ) acc ( Acc_Dtl )
            CROSS APPLY ( SELECT
                              CAST(chat.Account_Id AS VARCHAR(100)) + ', '
                          FROM
                              Core.Client_Hier_Account chat
                              INNER JOIN dbo.Ec_Client_Account_Group_Account ecagaa
                                    ON chat.Account_Id = ecagaa.Account_Id
                          WHERE
                              ecagaa.Ec_Client_Account_Group_Id = ecag.Ec_Client_Account_Group_Id
                          GROUP BY
                              chat.Account_Id
            FOR
                          XML PATH('') ) acc_Id ( Acc_Dtl_Id )
      WHERE
            ch.Client_Id = @Client_Id
      GROUP BY
            ecag.Ec_Client_Account_Group_Id
           ,eagt.Group_Type_Name
           ,ecag.Account_Group_Name
           ,LEFT(acc_Id.Acc_Dtl_Id, LEN(acc_Id.Acc_Dtl_Id) - 1)
           ,LEFT(acc.Acc_Dtl, LEN(acc.Acc_Dtl) - 1)
           ,ecag.Start_Dt
           ,ecag.End_Dt
           ,ch.Country_Name
           ,ch.State_Name
           ,c.Commodity_Name
           ,eagt.State_Id
           ,eagt.Ec_Account_Group_Type_Id
           ,eagt.Commodity_Id
      ORDER BY
            ecag.Account_Group_Name
           ,eagt.Group_Type_Name
           
       
        
            
      
END



;
GO
GRANT EXECUTE ON  [dbo].[Group_Name_Dtl_Sel_By_Client_Id] TO [CBMSApplication]
GO
