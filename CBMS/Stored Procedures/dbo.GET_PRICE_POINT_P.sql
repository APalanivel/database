SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_PRICE_POINT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@indexId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.GET_PRICE_POINT_P
@indexId integer
AS
set nocount on
	select price_index_id, index_id, entity_name, pricing_point,
	index_description from price_index, entity
	where index_id = @indexId and index_id = entity_id
GO
GRANT EXECUTE ON  [dbo].[GET_PRICE_POINT_P] TO [CBMSApplication]
GO
