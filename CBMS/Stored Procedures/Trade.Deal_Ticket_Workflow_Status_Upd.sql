SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Deal_Ticket_Workflow_Status_Upd
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Workflow_Status_Upd  195
	Exec Trade.Deal_Ticket_Workflow_Status_Upd  198
	Exec Trade.Deal_Ticket_Workflow_Status_Upd  212
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	SP          SrinivasaRao Patchava
	RR			Raghu Reddy    
    
MODIFICATIONS     
	Initials    Date        Modification      
-------------------------------------------------------------------------------------------------------------------       
	SP          2018-11-21  Global Risk Management - Create sp to get summary page details for deal ticket summery
	RR			2019-09-04	GRM-1487 - Added new input @Trade_Id and updating respective order executed site/month/contract for
							sent confirmation to client task    
    RR			2019-09-04	GRM-1556 - Applied site filter to update the only selected sites status in "Send Confirmation to Client" task
	RR			2019-09-04	GRM-1566 Added trade number check in cancel task, volumes of a site cannot be updated to zero if trade number
							assigned(Once moved to trade)
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Workflow_Status_Upd]
     (
         @Deal_Ticket_Id INT
        , @Workflow_Transition_Id INT
        , @Workflow_Task_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
        , @Workflow_Status_Comment NVARCHAR(MAX) = NULL
        , @CBMS_Image_Id INT = NULL
        , @Notification_Msg_Dtl_Id INT = NULL
        , @Deal_Month DATE = NULL
        , @User_Info_Id INT = NULL
        , @Trade_Id INT = NULL
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Workflow_Status_Map_Id INT
            , @Workflow_Status_Name NVARCHAR(255)
            , @Task_Name NVARCHAR(255);

        DECLARE @New_Client_Hier_Workflow_Status_Ids AS TABLE
              (
                  Deal_Ticket_Client_Hier_Workflow_Status_Id INT
              );

        DECLARE @Trade_Dtl AS TABLE
              (
                  [Deal_Ticket_Client_Hier_Id] [INT] NULL
                  , [Trade_Month] [DATE] NULL
                  , [Contract_Id] [INT] NULL
              );

        SELECT
            @Task_Name = wt.Task_Name
        FROM
            Trade.Workflow_Task wt
        WHERE
            wt.Workflow_Task_Id = @Workflow_Task_Id;

        INSERT INTO @Trade_Dtl
             (
                 Deal_Ticket_Client_Hier_Id
                 , Trade_Month
                 , Contract_Id
             )
        SELECT
            dtchws.Deal_Ticket_Client_Hier_Id
            , dtchws.Trade_Month
            , dtchws.Contract_Id
        FROM
            Trade.Deal_Ticket_Client_Hier_Workflow_Status dtchws
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Client_Hier_Id = dtchws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                ON dtchvd.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                   AND  dtchvd.Client_Hier_Id = dtch.Client_Hier_Id
                   AND  dtchvd.Deal_Month = dtchws.Trade_Month
                   AND  dtchvd.Contract_Id = dtchws.Contract_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON wsm.Workflow_Status_Map_Id = dtchws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
        WHERE
            dtchvd.Trade_Number = @Trade_Id
            AND dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND dtchws.Is_Active = 1
            AND ws.Workflow_Status_Name = 'Order Executed'
            AND @Task_Name = 'Send Confirmation to Client'
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') chids
                                   WHERE
                                        CAST(chids.Segments AS INT) = dtch.Client_Hier_Id))
        GROUP BY
            dtchws.Deal_Ticket_Client_Hier_Id
            , dtchws.Trade_Month
            , dtchws.Contract_Id;


        UPDATE
            chws
        SET
            chws.Is_Active = 0
            , chws.Last_Change_Ts = ISNULL(chws.Last_Change_Ts, GETDATE())
            , chws.Updated_User_Id = ISNULL(chws.Updated_User_Id, @User_Info_Id)
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') chids
                                   WHERE
                                        CAST(chids.Segments AS INT) = dtch.Client_Hier_Id))
            AND (   @Deal_Month IS NULL
                    OR  chws.Trade_Month = @Deal_Month)
            AND (   @Trade_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        @Trade_Dtl td
                                   WHERE
                                        td.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                                        AND td.Trade_Month = chws.Trade_Month
                                        AND td.Contract_Id = chws.Contract_Id
                                        AND @Task_Name = 'Send Confirmation to Client'));

        SELECT
            @Workflow_Status_Map_Id = wtsm.Descendant_Workflow_Status_Map_Id
            , @Workflow_Status_Name = desw.Workflow_Status_Name
        FROM
            Trade.Workflow_Task_Status_Transition_Map wtsm
            INNER JOIN Trade.Workflow_Status_Map desws
                ON wtsm.Descendant_Workflow_Status_Map_Id = desws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status desw
                ON desw.Workflow_Status_Id = desws.Workflow_Status_Id
            INNER JOIN Trade.Workflow_Task_Status_Map tsm
                ON wtsm.Workflow_Task_Status_Map_Id = tsm.Workflow_Task_Status_Map_Id
            INNER JOIN Trade.Workflow_Status_Map ws
                ON tsm.Workflow_Status_Map_Id = ws.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status w
                ON ws.Workflow_Status_Id = w.Workflow_Status_Id
            INNER JOIN Trade.Deal_Ticket dt
                ON ws.Workflow_Id = dt.Workflow_Id
        WHERE
            wtsm.Workflow_Transition_Id = @Workflow_Transition_Id
            AND tsm.Workflow_Task_Id = @Workflow_Task_Id
            AND dt.Deal_Ticket_Id = @Deal_Ticket_Id;

        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , CBMS_Image_Id
                 , Trade_Month
                 , Workflow_Task_Id
             )
        OUTPUT
            INSERTED.Deal_Ticket_Client_Hier_Workflow_Status_Id
        INTO @New_Client_Hier_Workflow_Status_Ids (Deal_Ticket_Client_Hier_Workflow_Status_Id)
        SELECT
            dtch.Deal_Ticket_Client_Hier_Id
            , @Workflow_Status_Map_Id
            , @Workflow_Status_Comment
            , 1
            , GETDATE()
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
            , @CBMS_Image_Id
            , @Deal_Month
            , @Workflow_Task_Id
        FROM
            Trade.Deal_Ticket_Client_Hier dtch
            INNER JOIN Trade.Deal_Ticket dt
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND @Trade_Id IS NULL
            AND @Task_Name <> 'Send Confirmation to Client'
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') chids
                                   WHERE
                                        CAST(chids.Segments AS INT) = dtch.Client_Hier_Id));

        INSERT INTO Trade.Deal_Ticket_Client_Hier_Workflow_Status
             (
                 Deal_Ticket_Client_Hier_Id
                 , Workflow_Status_Map_Id
                 , Workflow_Status_Comment
                 , Is_Active
                 , Last_Notification_Sent_Ts
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
                 , CBMS_Image_Id
                 , Trade_Month
                 , Workflow_Task_Id
                 , Contract_Id
             )
        OUTPUT
            INSERTED.Deal_Ticket_Client_Hier_Workflow_Status_Id
        INTO @New_Client_Hier_Workflow_Status_Ids (Deal_Ticket_Client_Hier_Workflow_Status_Id)
        SELECT
            td.Deal_Ticket_Client_Hier_Id
            , @Workflow_Status_Map_Id
            , @Workflow_Status_Comment
            , 1
            , GETDATE()
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
            , @CBMS_Image_Id
            , td.Trade_Month
            , @Workflow_Task_Id
            , td.Contract_Id
        FROM
            @Trade_Dtl td
        WHERE
            @Trade_Id IS NOT NULL
            AND @Task_Name = 'Send Confirmation to Client';

        INSERT INTO Trade.Deal_Ticket_Client_Hier_Notification_Log
             (
                 Deal_Ticket_Client_Hier_Workflow_Status_Id
                 , Notification_Msg_Dtl_Id
                 , Workflow_Task_Id
             )
        SELECT
            Deal_Ticket_Client_Hier_Workflow_Status_Id
            , @Notification_Msg_Dtl_Id
            , @Workflow_Task_Id
        FROM
            @New_Client_Hier_Workflow_Status_Ids
        WHERE
            @Notification_Msg_Dtl_Id IS NOT NULL;

        UPDATE
            voldtl
        SET
            voldtl.Total_Volume = 0
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl voldtl
        WHERE
            voldtl.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') chids
                                   WHERE
                                        CAST(chids.Segments AS INT) = voldtl.Client_Hier_Id))
            AND (   @Deal_Month IS NULL
                    OR  voldtl.Deal_Month = @Deal_Month)
            AND @Workflow_Status_Name IN ( 'Canceled' )
            AND voldtl.Trade_Number IS NULL;


        UPDATE
            totvol
        SET
            totvol.Total_Volume = voldtl.Total_Volume
        FROM
        (   SELECT
                SUM(Total_Volume) AS Total_Volume
                , Deal_Month
                , Deal_Ticket_Id
            FROM
                Trade.Deal_Ticket_Client_Hier_Volume_Dtl
            WHERE
                Deal_Ticket_Id = @Deal_Ticket_Id
            GROUP BY
                Deal_Month
                , Deal_Ticket_Id) voldtl
        INNER JOIN Trade.Deal_Total_Hedge_Volume totvol
            ON voldtl.Deal_Ticket_Id = totvol.Deal_Ticket_Id
               AND  voldtl.Deal_Month = totvol.Deal_Month
        WHERE
            totvol.Deal_Ticket_Id = @Deal_Ticket_Id;

        UPDATE -- Deal ticket should go to initiator queue once director approved/rejected
            dt
        SET
            Dt.Queue_Id = ui.QUEUE_ID
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.USER_INFO ui
                ON dt.Created_User_Id = ui.USER_INFO_ID
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND EXISTS (   SELECT
                                1
                           FROM
                                Trade.Workflow_Task
                           WHERE
                                Workflow_Task_Id = @Workflow_Task_Id
                                AND Task_Name = 'Internal Approval');

        EXEC Trade.Deal_Ticket_Last_Updated_Ins @Deal_Ticket_Id;


    END;






GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Workflow_Status_Upd] TO [CBMSApplication]
GO
