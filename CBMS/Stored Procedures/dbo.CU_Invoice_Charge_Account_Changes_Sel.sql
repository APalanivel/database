
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	 dbo.CU_Invoice_Charge_Account_Changes_Sel

DESCRIPTION:
			This procedure is used to select changed records between two versions from CU_INVOICE_CHARGE_ACCOUNT.

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
@Last_Version_Id				BIGINT			Beginnng Change tracking Version Id (From Persistent vaiables) 			
@Current_Version_Id				BIGINT			Current Change tracking Version Id

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
It will execute only through the replication replacement SSIS package.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RK			Raghu Kalvapudi
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	RK			08/01/2013		Created
	KVk			09/17/2014		Modified to have select correct result
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Charge_Account_Changes_Sel]
      (
       @Last_Version_Id BIGINT
      ,@Current_Version_Id BIGINT
      ,@CU_I_Last_Version_Id BIGINT )
AS
BEGIN
      SET NOCOUNT ON;

      SELECT
            convert(CHAR(1), cuica_ct.SYS_CHANGE_OPERATION) Op_Code
           ,cuica_ct.CU_INVOICE_CHARGE_ACCOUNT_ID
           ,NULL ACCOUNT_ID
           ,NULL Charge_Expression
           ,NULL Charge_Value
           ,NULL CU_INVOICE_CHARGE_ID
      FROM
            changetable(CHANGES dbo.CU_INVOICE_CHARGE_ACCOUNT, @Last_Version_Id) cuica_ct
      WHERE
            cuica_ct.SYS_Change_Version <= @Current_Version_Id
            AND convert(CHAR(1), cuica_ct.SYS_CHANGE_OPERATION) = 'D'
      UNION ALL
      SELECT
            'U' Op_Code --Insert or update
           ,cica.CU_INVOICE_CHARGE_ACCOUNT_ID
           ,cica.ACCOUNT_ID
           ,cica.Charge_Expression
           ,cica.Charge_Value
           ,cica.CU_INVOICE_CHARGE_ID
      FROM
            dbo.CU_INVOICE_CHARGE_ACCOUNT AS cica
            JOIN ( SELECT
                        cuica_ct.CU_INVOICE_CHARGE_ACCOUNT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE_CHARGE_ACCOUNT, @Last_Version_Id) cuica_ct
                        INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cuica
                              ON cuica.CU_INVOICE_CHARGE_ACCOUNT_ID = cuica_ct.CU_INVOICE_CHARGE_ACCOUNT_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE cuic
                              ON cuica.CU_INVOICE_CHARGE_ID = cuic.CU_INVOICE_CHARGE_ID
                        INNER JOIN dbo.CU_INVOICE ci
                              ON cuic.CU_INVOICE_ID = ci.CU_INVOICE_ID
                   WHERE
                        cuica_ct.SYS_Change_Version <= @Current_Version_Id
                        AND ci.IS_PROCESSED = 1
                        AND ci.IS_REPORTED = 1
                   UNION
                   SELECT
                        cuica.CU_INVOICE_CHARGE_ACCOUNT_ID
                   FROM
                        changetable(CHANGES dbo.CU_INVOICE, @CU_I_Last_Version_Id) cui_ct
                        INNER JOIN dbo.CU_INVOICE cui
                              ON cui.CU_INVOICE_ID = cui_ct.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE cuic
                              ON cuic.CU_INVOICE_ID = cui.CU_INVOICE_ID
                        INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT cuica
                              ON cuica.CU_INVOICE_CHARGE_ID = cuic.CU_INVOICE_CHARGE_ID
                   WHERE
                        cui_ct.SYS_Change_Version <= @Current_Version_Id
                        AND cui.IS_PROCESSED = 1
                        AND cui.IS_REPORTED = 1 ) AS change
                  ON change.CU_INVOICE_CHARGE_ACCOUNT_ID = cica.CU_INVOICE_CHARGE_ACCOUNT_ID

END
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Account_Changes_Sel] TO [ETL_Execute]
GO
