SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*********       

NAME:
     dbo.Account_Conversion_Variance_Rule_Dtl_Account_Override_Ins 
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
          
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
          
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 BEGIN TRAN
 EXEC dbo.Account_Conversion_Variance_Rule_Dtl_Account_Override_Ins
 ROLLBACK TRAN	  
	 
      
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 AKR                   Ashok Kumar Raju  
 SP						Sandeep pigilam  
 AP						Arunkumar Palanivel    

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 AKR                   2014-06-01      Created.
 HG						2017-03-30		CPH, modified to use the temp table created with in SSIS
 SP						2018-02-13		MAINT-6838,Removed the LEFT JOIN on Variance_Rule_Dtl_Account_Override 
											as it is already used in the not exists clause to filter out the existing records


Ap					Feb 24,2020			Modified Procedure fo AVT project
AP					May 19, 2020		Modified procedure for performance fixes. Ticket ID https://summit.jira.com/browse/MAINT-10329
AP					June 11,2020        https://summit.jira.com/browse/SE2017-996 Tax exclusion
*********/
CREATE PROCEDURE [dbo].[Account_Conversion_Variance_Rule_Dtl_Account_Override_Ins]
AS
      BEGIN
            SET NOCOUNT ON;

            DECLARE @Variance_Process_Engine_Id INT;

			declare  @account_country_id TABLE
			(account_id INT null,
			country_id INT NOT null)
			 

			INSERT #account_country_id (
			                                      account_id
			                                    , country_id
			                                )
			SELECT DISTINCT acc.ACCOUNT_ID, cha.Meter_Country_Id FROM core.Client_Hier_Account cha
			JOIN dbo.account acc
			ON acc.ACCOUNT_ID = cha.Account_Id
			JOIN  #Data_Conversion_Accounts_Stage  conv_a
			on acc.SITE_ID = conv_a.[Site id]
                                             AND acc.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                             AND acc.VENDOR_ID = conv_a.[Vendor ID]
			

            SELECT
                  @Variance_Process_Engine_Id = VPE.Variance_Processing_Engine_Id
            FROM  dbo.Variance_Processing_Engine VPE
                  JOIN
                  dbo.Code C
                        ON VPE.Variance_Engine_Cd = C.Code_Id
            WHERE C.Code_Dsc = 'Advanced Variance Test';


            INSERT INTO CBMS.dbo.Variance_Rule_Dtl_Account_Override (
                                                                          Variance_Rule_Dtl_Id
                                                                        , ACCOUNT_ID
                                                                    )
                        SELECT      DISTINCT
                                    vrd.Variance_Rule_Dtl_Id
                                  , acc.ACCOUNT_ID
                        FROM        CBMS.dbo.Variance_Rule_Dtl vrd
                                    JOIN
                                    CBMS.dbo.Variance_Consumption_Level vcl
                                          ON vcl.Variance_Consumption_Level_Id = vrd.Variance_Consumption_Level_Id
                                    JOIN
                                    CBMS.dbo.Account_Variance_Consumption_Level avcl
                                          ON avcl.Variance_Consumption_Level_Id = vrd.Variance_Consumption_Level_Id
                                    JOIN
                                    CBMS.dbo.ACCOUNT acc
                                          ON avcl.ACCOUNT_ID = acc.ACCOUNT_ID
                                             AND isnull(acc.Is_Data_Entry_Only, 0) = vrd.Is_Data_Entry_Only
                                    --JOIN
                                    --#Data_Conversion_Accounts_Stage conv_a
                                    --      ON acc.SITE_ID = conv_a.[Site id]
                                    --         AND acc.ACCOUNT_NUMBER = conv_a.[ACCOUNT NUMBER]
                                    --         AND acc.VENDOR_ID = conv_a.[Vendor ID]
											JOIN @account_country_id acc1
											ON acc1.account_id = acc.ACCOUNT_ID
                        WHERE       vrd.IS_Active = 1
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  CBMS.dbo.Variance_Rule_Dtl_Account_Override vrda
                                    WHERE vrda.Variance_Rule_Dtl_Id = vrd.Variance_Rule_Dtl_Id
                                          AND   acc.ACCOUNT_ID = vrda.ACCOUNT_ID )
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Variance_Rule vr
                                          INNER JOIN
                                          dbo.Variance_Parameter_Baseline_Map vpbmap
                                                ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                          INNER JOIN
                                          dbo.Variance_Parameter vp
                                                ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                          INNER JOIN
                                          dbo.Code cd
                                                ON cd.Code_Id = vp.Variance_Category_Cd
                                          INNER JOIN
                                          dbo.Code cdb
                                                ON cdb.Code_Id = vpbmap.Baseline_Cd
                                          JOIN
                                          dbo.Variance_Rules_Exclusion vre
                                                ON acc1.Country_id = vre.Country_id
                                                   AND vre.Baseline_Cd = cdb.Code_Id
                                                   AND vre.Category_id = cd.Code_Id
                                    WHERE vrd.Variance_Rule_Id = vr.Variance_Rule_Id )
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.Variance_Parameter vp
                                          JOIN
                                          dbo.Variance_Parameter_Baseline_Map vpbm
                                                ON vpbm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                          JOIN
                                          dbo.Variance_Rule vr
                                                ON vr.Variance_Baseline_Id = vpbm.Variance_Baseline_Id
                                          JOIN
                                          dbo.Variance_Rule_Dtl vrd1
                                                ON vrd1.Variance_Rule_Id = vr.Variance_Rule_Id
                                          JOIN
                                          dbo.Variance_Parameter_Commodity_Map vpcm
                                                ON vpcm.Variance_Parameter_Id = vp.Variance_Parameter_Id
                                          JOIN
                                          dbo.Variance_Processing_Engine vpe
                                                ON vpe.Variance_Processing_Engine_Id = vpcm.variance_process_Engine_id
                                    WHERE vpe.Variance_Processing_Engine_Id = @Variance_Process_Engine_Id
                                          AND   vrd1.Variance_Rule_Dtl_Id = vrd.Variance_Rule_Dtl_Id );

      END;

GO

GRANT EXECUTE ON  [dbo].[Account_Conversion_Variance_Rule_Dtl_Account_Override_Ins] TO [ETL_Execute]
GO
