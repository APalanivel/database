SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Rm_Budget_Site_Default_Budget_Config_Upd                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Rm_Budget_Site_Default_Budget_Config_Upd 584,1005,291,NULL,'2020-04-01','2020-05-01','All Sites'

    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	                
******/
CREATE PROCEDURE [Trade].[Rm_Budget_Site_Default_Budget_Config_Upd]
    (
        @Rm_Budget_Site_Default_Budget_Config_Id INT
        , @Client_Hier_Id INT
        , @Rm_Budget_Id INT
        , @Start_Dt DATE
        , @End_Dt DATE
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Start_Dt_Old DATE
            , @End_Dt_Old DATE
            , @Rm_Budget_Id_Old INT;

        SELECT
            @Rm_Budget_Id_Old = Rm_Budget_Id
            , @Start_Dt_Old = Start_Dt
            , @End_Dt_Old = End_Dt
        FROM
            Trade.Rm_Budget_Site_Default_Budget_Config
        WHERE
            Rm_Budget_Site_Default_Budget_Config_Id = @Rm_Budget_Site_Default_Budget_Config_Id
            AND Client_Hier_Id = @Client_Hier_Id;

        UPDATE
            Trade.Rm_Budget_Site_Default_Budget_Config
        SET
            Rm_Budget_Id = @Rm_Budget_Id
            , Start_Dt = @Start_Dt
            , End_Dt = @End_Dt
        WHERE
            Rm_Budget_Site_Default_Budget_Config_Id = @Rm_Budget_Site_Default_Budget_Config_Id
            AND Client_Hier_Id = @Client_Hier_Id;

        UPDATE
            Trade.Rm_Budget_Site_Default_Budget_Config
        SET
            Last_Updated_By = @User_Info_Id
            , Last_Change_Ts = GETDATE()
        WHERE
            Rm_Budget_Site_Default_Budget_Config_Id = @Rm_Budget_Site_Default_Budget_Config_Id
            AND Client_Hier_Id = @Client_Hier_Id
            AND (   @Start_Dt <> @Start_Dt_Old
                    OR  @End_Dt <> @End_Dt_Old
                    OR  @Rm_Budget_Id <> @Rm_Budget_Id_Old);


    END;
GO
GRANT EXECUTE ON  [Trade].[Rm_Budget_Site_Default_Budget_Config_Upd] TO [CBMSApplication]
GO
