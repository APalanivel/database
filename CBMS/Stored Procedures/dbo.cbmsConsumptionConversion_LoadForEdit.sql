SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsConsumptionConversion_LoadForEdit

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@base_unit_id  	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsConsumptionConversion_LoadForEdit]
	( @MyAccountId int
	, @base_unit_id int = null
	)
AS
BEGIN
	   select e.entity_id 		'base_unit_id'
		, e.entity_name 	'base_unit'
		, e.entity_description 	'base_unit_type'
		, e2.entity_id		'converted_unit_id'
		, e2.entity_name	'converted_unit'
		, e2.entity_description 'converted_unit_type'
		, isNull(cuc.conversion_factor, 0) conversion_factor

	       from entity e
	 cross join entity e2
    left outer join consumption_unit_conversion cuc on cuc.base_unit_id = e.entity_id and cuc.converted_unit_id = e2.entity_id

	     where e.entity_id = @base_unit_id
	       and e2.entity_type in (101, 102, 726, 727, 717, 718, 719, 720, 721, 722, 723, 724, 729, 728, 725,730,731, 732, 733, 734,735)


	  order by e2.entity_description
		, e2.entity_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsConsumptionConversion_LoadForEdit] TO [CBMSApplication]
GO
