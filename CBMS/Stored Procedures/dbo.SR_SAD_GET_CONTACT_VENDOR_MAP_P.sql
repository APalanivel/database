SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- exec dbo.SR_SAD_GET_CONTACT_VENDOR_MAP_P '','',31
CREATE  PROCEDURE dbo.SR_SAD_GET_CONTACT_VENDOR_MAP_P 
@contactinfoId int
 
 

AS
	set nocount on
	SELECT  map.SR_SUPPLIER_CONTACT_VENDOR_MAP_id,
		map.VENDOR_ID,
		map.IS_PRIMARY,
		map.COUNTRY_ID,
		map.STATE_ID,
		map.COMMODITY_TYPE_ID,
		v.vendor_name,
		s.state_name,
		e.entity_name,
		c.country_name
	FROM	SR_SUPPLIER_CONTACT_VENDOR_MAP map
		left join country c on map.country_id = c.country_id
		left join entity e on map.commodity_type_id=e.entity_id
		left join vendor v on map.vendor_id=v.vendor_id
		left join state s on map.state_id = s.state_id
	WHERE	map.SR_SUPPLIER_CONTACT_INFO_ID = @contactinfoId 
		and map.is_primary = 0
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_CONTACT_VENDOR_MAP_P] TO [CBMSApplication]
GO
