SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seStripHeader_Remove
	(
		@StripHeaderId int
	,	@AccountId int
	)
AS
BEGIN

	set nocount on

		declare @ThisId int
		
		select @ThisId = StripHeaderId
		  from seStripHeader
		 where StripHeaderId = @StripHeaderId
		   and AccountId = @AccountId
		   
	if @ThisId is not null
	begin

		delete seStripDetail
		 where StripHeaderId = @StripHeaderId
		 
		delete seStripHeader
		 where StripHeaderId = @StripHeaderId

	end
	exec seStripHeader_Get @StripHeaderId

END
GO
GRANT EXECUTE ON  [dbo].[seStripHeader_Remove] TO [CBMSApplication]
GO
