SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 
DESCRIPTION: 
  This procedure is called monthly from a SQL Agent job to create the Invoice Participation for the upcoming Fiscal year.  
    
 INPUT PARAMETERS:    
 Name				DataType	Default		Description    
------------------------------------------------------------------------------    
 @start_month		INT			NULL		Month to create the IP for. If null defaults to the next month
 @report_year		INT			NULL		Year to create the IP for.  If null defaults to the next year

 OUTPUT PARAMETERS:    
 Name				DataType	Default		Description    
------------------------------------------------------------------------------    

 
 USAGE EXAMPLES:    
------------------------------------------------------------------------------    
Exec cbmsInvoiceParticipation_EstablishReportYear Null, Null
Exec cbmsInvoiceParticipation_EstablishReportYear 6, 2011
    
    
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------------------------    
DMR			Deana Ritter	
RR			Raghu Reddy
 
 MODIFICATIONS     
 
 Initials Date   Modification    
------------------------------------------------------------    
 DMR		  4/03/2012		Maint-818
							Complete re-write.  Left call to cbmsInvoiceParticipation_EstablishClientForReportYear
							hard coded reference to user_id 93, which is Scott Drake.  Not sure of the impact
							if modifying to 16 (system conversion).   Removed reference to view ClientFiscalYearStartMonth
							(intentionally miss-named so that it won't come up on SysComment/Definition search.) and 
							replaced with Core.Client_Hier and Client_Fiscal_Offset.   					
 HG			2017-01-13		Modified the sproc to consider the month number as 1 if the current month is December.
 RR			2017-04-04		Contract Placeholder -  Script modfied to extend IP for 12 for open end date DMO supplier acccount if
							current participation ending in currrent month
 HG			2019-12-18		MAINT-9700	, Excluding client's based on the client id saved in App_Config table ( Made this change specifically to exclude AT&T)
******/

CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_EstablishReportYear]
      (
      @start_month INT = NULL
    , @report_year INT = NULL )
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @current_date DATETIME
                , @client_id    INT
                , @client_name  VARCHAR(200);

            DECLARE @Ip_Excluded_Client_List TABLE
                  ( Client_Id INT );

            DECLARE @Tbl_Open_Supp AS TABLE
                  ( Id                        INT IDENTITY(1, 1)
                  , Account_Id                INT
                  , Site_Id                   INT
                  , Supplier_Account_Begin_Dt DATETIME
                  , Supplier_Account_End_Dt   DATETIME
                  , Current_Last_Month        DATETIME
                  , Start_Month               DATETIME
                  , End_Month                 DATETIME );

            DECLARE
                  @Total_Row_Count    INT
                , @Row_Counter        INT
                , @Account_Id         INT
                , @IP_Start_Month     DATETIME
                , @IP_End_Month       DATETIME
                , @IP_Service_Month   DATETIME
                , @Site_Id            INT
                , @IP_Excluded_Client VARCHAR(MAX);

            IF @report_year IS NULL
                  BEGIN
                        SET @report_year = ( year(getdate()) + 1 );
                  END;

            SELECT
                  @IP_Excluded_Client = ac.App_Config_Value
            FROM  dbo.App_Config ac
            WHERE ac.App_Config_Cd = 'Invoice_Participation_Excluded_Clients';

            INSERT INTO @Ip_Excluded_Client_List (
                                                       Client_Id
                                                 )
                        SELECT
                              Segments
                        FROM  dbo.ufn_split(@IP_Excluded_Client, ',');

            DECLARE curClients CURSOR READ_ONLY FOR
            SELECT
                  ch.Client_Id
                , ch.Client_Name
            FROM  Core.Client_Hier ch
            WHERE month(dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@report_year AS VARCHAR(4)) AS DATE))) = CASE WHEN month(getdate()) = 12
                                                                                                                                   THEN 1
                                                                                                                             ELSE  isnull(@start_month, month(getdate()) + 1)
                                                                                                                        END
                  AND   ch.Sitegroup_Id = 0
                  AND   ch.Site_Id = 0
                  AND   ch.Client_Not_Managed = 0
                  AND   NOT EXISTS
                  (     SELECT
                              1
                        FROM  @Ip_Excluded_Client_List ecl
                        WHERE ecl.Client_Id = ch.Client_Id );

            OPEN curClients;

            FETCH NEXT FROM curClients
            INTO
                  @client_id
                , @client_name;
            WHILE ( @@fetch_status <> -1 )
                  BEGIN

                        EXEC cbmsInvoiceParticipation_EstablishClientForReportYear
                              93    --scott drake not system conversion
                            , @client_id
                            , @report_year;

                        FETCH NEXT FROM curClients
                        INTO
                              @client_id
                            , @client_name;
                  END;

            CLOSE curClients;
            DEALLOCATE curClients;

            INSERT INTO @Tbl_Open_Supp (
                                             Account_Id
                                           , Site_Id
                                           , Supplier_Account_Begin_Dt
                                           , Supplier_Account_End_Dt
                                           , Current_Last_Month
                                           , Start_Month
                                           , End_Month
                                       )
                        SELECT
                                    samm.ACCOUNT_ID
                                  , ch.Site_Id
                                  , acc.Supplier_Account_Begin_Dt
                                  , acc.Supplier_Account_End_Dt
                                  , max(ip.SERVICE_MONTH) AS Current_Last_Month
                                  , cast(dateadd(mm, 1, max(ip.SERVICE_MONTH)) AS DATETIME) AS Start_Month
                                  , dateadd(mm, datediff(mm, max(ip.SERVICE_MONTH), getdate()) + 12, max(ip.SERVICE_MONTH)) AS End_Month
                        FROM        dbo.ACCOUNT acc
                                    INNER JOIN
                                    dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                          ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
                                    INNER JOIN
                                    dbo.INVOICE_PARTICIPATION ip
                                          ON samm.ACCOUNT_ID = ip.ACCOUNT_ID
                                    INNER JOIN
                                    Core.Client_Hier_Account cha
                                          ON samm.ACCOUNT_ID = cha.Account_Id
                                    INNER JOIN
                                    Core.Client_Hier ch
                                          ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        WHERE       acc.Supplier_Account_End_Dt IS NULL
                                    AND   samm.Contract_ID = -1
                        GROUP BY    samm.ACCOUNT_ID
                                  , ch.Site_Id
                                  , acc.Supplier_Account_Begin_Dt
                                  , acc.Supplier_Account_End_Dt
                        HAVING      cast(max(ip.SERVICE_MONTH) AS DATE) <= cast(getdate() AS DATE);


            SELECT
                  @Total_Row_Count = max(Id)
            FROM  @Tbl_Open_Supp;

            SET @Row_Counter = 1;
            WHILE ( @Row_Counter <= @Total_Row_Count )
                  BEGIN
                        SELECT
                              @Account_Id = Account_Id
                            , @Site_Id = Site_Id
                            , @IP_Start_Month = Start_Month
                            , @IP_End_Month = End_Month
                        FROM  @Tbl_Open_Supp
                        WHERE Id = @Row_Counter;
                        SET @IP_Service_Month = @IP_Start_Month;

                        WHILE ( @IP_Service_Month <= @IP_End_Month )
                              BEGIN
                                    EXEC dbo.cbmsInvoiceParticipationQueue_Save
                                          16
                                        , 9     -- Make Expected
                                        , NULL
                                        , NULL
                                        , @Site_Id
                                        , @Account_Id
                                        , @IP_Service_Month
                                        , 1;


                                    SET @IP_Service_Month = dateadd(mm, 1, @IP_Service_Month);
                              END;

                        SET @Row_Counter = @Row_Counter + 1;
                  END;



      END;


      ;

GO



GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_EstablishReportYear] TO [CBMSApplication]
GO
