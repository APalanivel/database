SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:	dbo.[cbmsUtilityAnalystMap_SaveSourcingAnalyst]


DESCRIPTION: 

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId           int
	@vendor_id             int
	@user_info_id          int             null
	@gas_analyst_id        int
	@power_analyst_id      int
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec cbmsUtilityAnalystMap_SaveSourcingAnalyst
--	 @MyAccountId = 1
--	, @vendor_id = 3048
--	, @user_info_id  = 1
--	, @gas_analyst_id =  1
--	, @power_analyst_id = 1  -- 2263


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

******/

	

CREATE PROCEDURE [dbo].[cbmsUtilityAnalystMap_SaveSourcingAnalyst]
      (
       @MyAccountId int
      ,@vendor_id int
      ,@user_info_id int = null
      ,@gas_analyst_id int
      ,@power_analyst_id int )
AS 
BEGIN

      set nocount on
      Declare
            @GasGroup int
           ,@PowerGroup int
           ,@utility_detail_id int
           ,@group_info_id int
           ,@analyst_id int
   
      
      select
            @utility_detail_id = utility_detail_id
      from
            utility_detail
      where
            vendor_id = @vendor_id

      Select
            @GasGroup = Group_Info_ID
      From
            Group_Info
      Where
            Group_Name = 'Gas Analyst'

      Select
            @PowerGroup = Group_Info_ID
      From
            Group_Info
      Where
            Group_Name = 'Power Analyst'
           
      Merge Into utility_detail_analyst_map as UDAM Using ( select
                                                                  @utility_detail_id Utility_Detail_ID
                                                                 ,@GasGroup Group_Info_ID
                                                                 ,@Gas_Analyst_ID Analyst_ID
                                                            Union
                                                            select
                                                                  @utility_detail_id
                                                                 ,@PowerGroup
                                                                 ,@Power_Analyst_ID ) as SRC
on ( UDAM.Utility_Detail_ID = SRC.Utility_Detail_ID
     And UDAM.Group_Info_ID = SRC.Group_Info_ID ) When Matched Then Update
      Set   
            UDAM.Analyst_ID = SRC.Analyst_ID When Not Matched Then Insert ( utility_detail_id, group_info_id, analyst_id )
      values
            (
             src.Utility_Detail_ID
            ,src.Group_info_id
            ,src.Analyst_Id ) ;                            
 
      exec cbmsUtilityAnalystMap_Get @MyAccountId, @utility_detail_id

END


GO
GRANT EXECUTE ON  [dbo].[cbmsUtilityAnalystMap_SaveSourcingAnalyst] TO [CBMSApplication]
GO
