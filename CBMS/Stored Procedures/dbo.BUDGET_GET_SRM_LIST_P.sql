SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE              PROCEDURE dbo.BUDGET_GET_SRM_LIST_P
@userId varchar(10),
@sessionId varchar(20)


AS
begin
set nocount on

select 	userInfo.user_info_id ,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from 	user_info userInfo

where 	userInfo.user_info_id = @userId

union 

select 	userInfo.user_info_id ,	
	userInfo.FIRST_NAME+' '+userInfo.LAST_NAME USER_INFO_NAME

from 	user_info userInfo,sr_sa_sm_map map

where 	map.sourcing_analyst_id = userInfo.user_info_id
	and map.sourcing_manager_id = @userId
	and userInfo.is_history = 0

order by USER_INFO_NAME

end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_SRM_LIST_P] TO [CBMSApplication]
GO
