SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: dbo.SSO_SAVINGS_Del

DESCRIPTION:

	Used to delete SSO_SAVINGS.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@SSO_Savings_Id		INT

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.SSO_SAVINGS_Del 2786

	ROLLBACK TRAN

	 SELECT TOP 100 * FROM SSO_SAVINGS s WHERE NOT EXISTS(SELECT 1 FROM SSO_SAVINGS_DETAIL d WHERE d.SSo_Savings_Id = s.SSo_Savings_Id)
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created

*/
CREATE PROCEDURE dbo.SSO_SAVINGS_Del
    (
       @SSO_Savings_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.SSO_SAVINGS
	WHERE
		SSO_SAVINGS_ID = @Sso_Savings_Id


END
GO
GRANT EXECUTE ON  [dbo].[SSO_SAVINGS_Del] TO [CBMSApplication]
GO
