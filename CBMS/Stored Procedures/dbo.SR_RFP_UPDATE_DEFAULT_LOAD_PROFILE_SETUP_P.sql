SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_UPDATE_DEFAULT_LOAD_PROFILE_SETUP_P
	@user_id varchar(10),
	@session_id varchar(20),
	@vendor_id int,
	@commodity_type_id int,
	@additional_rows int,
	@month_selector_type_id int
	
	AS
	set nocount on
update	sr_load_profile_default_setup 
set 	additional_rows = @additional_rows,
	month_selector_type_id = @month_selector_type_id
where 	commodity_type_id = @commodity_type_id
	and vendor_id = @vendor_id

/*select	
	setup.additional_rows,
       	setup.month_selector_type_id,
	determinant.sr_load_profile_determinant_id,
       	determinant.determinant_name,
	determinant.determinant_unit_type_id,
	determinant.is_checked
from
	sr_load_profile_default_setup setup,
	sr_load_profile_determinant determinant
where 
	determinant.sr_load_profile_default_setup_id = setup.sr_load_profile_default_setup_id	
	and setup.commodity_type_id = @commodity_type_id
	and setup.vendor_id = @vendor_id
*/
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_DEFAULT_LOAD_PROFILE_SETUP_P] TO [CBMSApplication]
GO
