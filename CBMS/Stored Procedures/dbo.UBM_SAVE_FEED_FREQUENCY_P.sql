SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_SAVE_FEED_FREQUENCY_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@ubmName       	varchar(25)	          	
	@cbmsClient    	varchar(100)	          	
	@ubmClientId   	int       	          	
	@frequencyId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec UBM_SAVE_FEED_FREGUENCY_P '1','1','Cass','AFG - AGC',763,763



CREATE    PROCEDURE dbo.UBM_SAVE_FEED_FREQUENCY_P
@userId varchar(10),
@sessionId varchar(20),
@ubmName varchar(25),
@cbmsClient varchar(100),
@ubmClientId int,
@frequencyId int


AS
set nocount on

	declare @ubmType int
	
	select @ubmType=UBM_ID from ubm where ubm_name like @ubmName

	INSERT INTO UBM_FEED_FREQUENCY (FREQUENCY_type_ID,UBM_ID,CLIENT_NAME,UBM_CLIENT_ID) 
	VALUES	(@frequencyId,@ubmType,@cbmsClient,@ubmClientId)
GO
GRANT EXECUTE ON  [dbo].[UBM_SAVE_FEED_FREQUENCY_P] TO [CBMSApplication]
GO
