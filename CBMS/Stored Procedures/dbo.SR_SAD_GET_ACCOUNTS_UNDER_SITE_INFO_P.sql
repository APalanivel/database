
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@siteId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P 101
	EXEC dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P 327396
	EXEC dbo.SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P 1827

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-10-05	Global Sourcing - Phase2 - added Alternate Account Numbers to select list

******/

CREATE PROCEDURE [dbo].[SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P] ( @siteId INT )
AS 
BEGIN
	

      SET NOCOUNT ON;

      SELECT
            cha.Account_Id
           ,cha.Display_Account_Number AS account_number
           ,cha.Alternate_Account_Number
      FROM
            Core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            ch.site_id = @siteId
            AND cha.Account_Type = 'Utility'
      GROUP BY
            cha.Account_Id
           ,cha.Display_Account_Number
           ,cha.Alternate_Account_Number
END

GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_ACCOUNTS_UNDER_SITE_INFO_P] TO [CBMSApplication]
GO
