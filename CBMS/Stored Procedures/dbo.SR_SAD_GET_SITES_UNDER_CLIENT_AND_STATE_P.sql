SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_SITES_UNDER_CLIENT_AND_STATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@stateId       	int       	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_GET_SITES_UNDER_CLIENT_AND_STATE_P 
@userId varchar,
@stateId int,
@clientId int
as
set nocount on
if @clientId > 0  and @stateId = 0
begin
	select	site.SITE_ID,
		viewSite.SITE_NAME
	
	from	SITE site,	
		DIVISION division ,
		VWSITENAME viewSite,
		client c
	
	where 	c.CLIENT_ID= @clientId 
		AND site.DIVISION_ID=division.DIVISION_ID 	
		and site.SITE_ID = viewSite.SITE_ID
		and site.closed=0 and site.not_managed=0
		and division.client_id = c.client_id
	
	order by viewSite.SITE_NAME
end
else if @clientId = 0  and @stateId > 0
begin

	select	site.SITE_ID,
		viewSite.SITE_NAME
	
	from	SITE site,	
		DIVISION division ,
		VWSITENAME viewSite,
		client c,
		state st,
		address ad
	
	where 	st.state_id = @stateId
		and site.DIVISION_ID=division.DIVISION_ID 	
		and site.SITE_ID = viewSite.SITE_ID
		and site.closed=0 and site.not_managed=0
		and division.client_id = c.client_id
		and ad.address_id = site.primary_address_id
		and st.state_id = ad.state_id
		
	order by viewSite.SITE_NAME
end

else if @clientId > 0  and @stateId > 0
begin

	select	site.SITE_ID,
		viewSite.SITE_NAME
	
	from	SITE site,	
		DIVISION division ,
		VWSITENAME viewSite,
		client c,
		state st,
		address ad
	
	where 	c.CLIENT_ID= @clientId 
		and st.state_id = @stateId
		and site.DIVISION_ID=division.DIVISION_ID 	
		and site.SITE_ID = viewSite.SITE_ID
		and site.closed=0 and site.not_managed=0
		and division.client_id = c.client_id
		and ad.address_id = site.primary_address_id
		and st.state_id = ad.state_id
		
	order by viewSite.SITE_NAME

end
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SITES_UNDER_CLIENT_AND_STATE_P] TO [CBMSApplication]
GO
