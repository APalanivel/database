SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account                    
                  
Description:                  
			Showing Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Id								INT

                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------

Note : Is_Show_Only_Watch_List_Configs = -1 --To get the All PI and WL 
       Is_Show_Only_Watch_List_Configs =  0 --To get the Only PI
	   Is_Show_Only_Watch_List_Configs =  1 --To get the Only WL

EXEC dbo.Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account
    @Account_Id = 1655252
    , @Is_Show_Only_Watch_List_Configs = -1

EXEC dbo.Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account
    @Account_Id = 1655252
    , @Is_Active = 0

EXEC dbo.Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account
    @Account_Id = 1655252
    , @Year = '2018'

EXEC dbo.Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account
    @Account_Id = 970513
    , @Year = '2015'

		   select * from Account_Invoice_Processing_Config
                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account]
    (
        @Account_Id INT
        , @Is_Show_Only_Watch_List_Configs INT = -1
        , @Is_Showing_PI_Past_3yrs BIT = 0
        , @Year INT = NULL
        , @Is_Active BIT = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Sort_Col VARCHAR(50) = NULL
        , @Service_Month DATE = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE
            @Default_Start_Dt DATE
            , @Default_End_Dt DATE
            , @Service_Start_Dt DATE
            , @Service_End_Dt DATE;

        SET @Default_Start_Dt = CAST(DATEADD(YY, -3, GETDATE()) AS DATE);
        SET @Default_End_Dt = CAST(GETDATE() AS DATE);

        SET @Service_Start_Dt = DATEADD(YY, -3, @Service_Month);
        SET @Service_End_Dt = @Service_Month;

        SET @Sort_Col = ISNULL(@Sort_Col, 'Config_Start_Dt DESC');
        WITH Cte_Processing_Config
        AS (
               SELECT
                    aipc.Account_Invoice_Processing_Config_Id
                    , aipc.Account_Id
                    , aipc.Config_Start_Dt
                    , aipc.Config_End_Dt
                    , aipc.Watch_List_Group_Info_Id
                    , gi.GROUP_NAME
                    , aipc.Processing_Instruction_Category_Cd
                    , Instruction_Category.Code_Value AS Instruction_Category
                    , aipc.Processing_Instruction
                    , aipc.Updated_User_Id
                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User
                    , aipc.Last_Change_Ts
                    , aipc.Is_Active
                    , COUNT(1) OVER () Total_Count
                    , ROW_NUMBER() OVER (ORDER BY (CASE WHEN @Sort_Col = 'GROUP_NAME ASC' THEN gi.GROUP_NAME
                                                       WHEN @Sort_Col = 'Instruction_Category ASC' THEN
                                                           Instruction_Category.Code_Value
                                                       WHEN @Sort_Col = 'Updated_User ASC' THEN
                                                           ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                       WHEN @Sort_Col = 'Processing_Instruction ASC' THEN
                                                           aipc.Processing_Instruction
                                                   END) ASC
                                                  , (CASE WHEN @Sort_Col = 'GROUP_NAME DESC' THEN gi.GROUP_NAME
                                                         WHEN @Sort_Col = 'Instruction_Category DESC' THEN
                                                             Instruction_Category.Code_Value
                                                         WHEN @Sort_Col = 'Updated_User DESC' THEN
                                                             ui.FIRST_NAME + ' ' + ui.LAST_NAME
                                                         WHEN @Sort_Col = 'Processing_Instruction DESC' THEN
                                                             aipc.Processing_Instruction
                                                     END) DESC
                                                  , (CASE WHEN @Sort_Col = 'Config_Start_Dt ASC' THEN
                                                              aipc.Config_Start_Dt
                                                         WHEN @Sort_Col = 'Config_End_Dt ASC' THEN aipc.Config_End_Dt
                                                         WHEN @Sort_Col = 'Last_Change_Ts ASC' THEN aipc.Last_Change_Ts
                                                     END) ASC
                                                  , (CASE WHEN @Sort_Col = 'Config_Start_Dt DESC' THEN
                                                              aipc.Config_Start_Dt
                                                         WHEN @Sort_Col = 'Config_End_Dt DESC' THEN aipc.Config_End_Dt
                                                         WHEN @Sort_Col = 'Last_Change_Ts DESC' THEN
                                                             aipc.Last_Change_Ts
                                                     END) DESC) AS Row_Num
               FROM
                    dbo.Account_Invoice_Processing_Config aipc
                    INNER JOIN dbo.Code Instruction_Category
                        ON Instruction_Category.Code_Id = aipc.Processing_Instruction_Category_Cd
                    LEFT JOIN dbo.GROUP_INFO gi
                        ON gi.GROUP_INFO_ID = aipc.Watch_List_Group_Info_Id
                    LEFT JOIN dbo.USER_INFO ui
                        ON ui.USER_INFO_ID = aipc.Updated_User_Id
               WHERE
                    aipc.Account_Id = @Account_Id
                    AND (   @Is_Show_Only_Watch_List_Configs = -1
                            OR  (   @Is_Show_Only_Watch_List_Configs = 1
                                    AND aipc.Watch_List_Group_Info_Id IS NOT NULL)
                            OR  (   @Is_Show_Only_Watch_List_Configs = 0
                                    AND aipc.Watch_List_Group_Info_Id IS NULL))
                    AND (   @Is_Showing_PI_Past_3yrs = 0
                            OR  (   @Is_Showing_PI_Past_3yrs = 1
                                    AND @Service_Month IS NULL
                                    AND (   @Default_Start_Dt BETWEEN aipc.Config_Start_Dt
                                                              AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                                            OR  @Default_End_Dt BETWEEN aipc.Config_Start_Dt
                                                                AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                                            OR  aipc.Config_Start_Dt BETWEEN @Default_Start_Dt
                                                                     AND     @Default_End_Dt
                                            OR  ISNULL(aipc.Config_End_Dt, '2099-12-31') BETWEEN @Default_Start_Dt
                                                                                         AND     @Default_End_Dt))
                            OR  (   @Is_Showing_PI_Past_3yrs = 1
                                    AND @Service_Month IS NOT NULL
                                    AND (   @Service_Start_Dt BETWEEN aipc.Config_Start_Dt
                                                              AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                                            OR  @Service_End_Dt BETWEEN aipc.Config_Start_Dt
                                                                AND     ISNULL(aipc.Config_End_Dt, '2099-12-31')
                                            OR  aipc.Config_Start_Dt BETWEEN @Service_Start_Dt
                                                                     AND     @Service_End_Dt
                                            OR  ISNULL(aipc.Config_End_Dt, '2099-12-31') BETWEEN @Service_Start_Dt
                                                                                         AND     @Service_End_Dt)))
                    AND (   @Is_Active IS NULL
                            OR  aipc.Is_Active = @Is_Active)
                    AND (   @Year IS NULL
                            OR  (@Year BETWEEN DATEPART(YYYY, aipc.Config_Start_Dt)
                                       AND     DATEPART(YYYY, ISNULL(aipc.Config_End_Dt, '9999-12-31'))))
           )
        SELECT
            cp.Account_Invoice_Processing_Config_Id
            , cp.Account_Id
            , cp.Config_Start_Dt
            , cp.Config_End_Dt
            , cp.Watch_List_Group_Info_Id
            , cp.GROUP_NAME
            , cp.Processing_Instruction_Category_Cd
            , cp.Instruction_Category
            , cp.Processing_Instruction
            , cp.Is_Active
            , cp.Updated_User_Id
            , cp.Updated_User
            , cp.Last_Change_Ts
            , cp.Row_Num
            , cp.Total_Count
        FROM
            Cte_Processing_Config cp
        WHERE
            cp.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cp.Row_Num;




    END;








GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_And_Instruction_Sel_By_Account] TO [CBMSApplication]
GO
