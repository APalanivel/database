SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******   
 NAME:    
 dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name
   
 DESCRIPTION:    
 
	Get open exception detail report values based on Queue Names
   
 INPUT PARAMETERS:    
	Name					     DataType	  Default Description    
-------------------------------------------------------------------------    
	@Queue_Name_List			 VARCHAR(MAX) 
        
 OUTPUT PARAMETERS:    
  
 Name							DataType	 Default Description    
------------------------------------------------------------ 
   
 USAGE EXAMPLES:  
------------------------------------------------------------

 exec dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name @Queue_Name_List = 'Hema Mandhapati,Mayukh Ghatak,Sujitha Ummiti,Krishna Kumari Padiloju'
 exec dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name  'naga lakshmi'
 exec dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name 'siva kanth'
 exec dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name  'Hema Mandhapati'
 
AUTHOR INITIALS:
 Initials		Name
------------------------------------------------------------  
 BCH			Balaraju

 MODIFICATIONS:
 Initials   Date		 Modification
------------------------------------------------------------
 BCH		09-05-2013   (Main -  1830), Created stored porcedure Instead of  BPO tool open expception detail Detail report Script

******/
CREATE PROCEDURE dbo.Get_Open_Exception_Dtl_Report_By_Queue_Name
      ( 
       @Queue_Name_List VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Temp_Quename
            ( 
             Queue_Id INT
            ,Queue_Type_Id INT
            ,Queue_Type VARCHAR(200)
            ,Queue_Name VARCHAR(100) )

      CREATE TABLE #Queue_table
            ( 
             Queue_Name VARCHAR(80)
            ,QueueType VARCHAR(50)
            ,Exception_Type VARCHAR(200)
            ,Cu_Invoice_Id INT )

      DECLARE @Queue_Name TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,Queue_Name VARCHAR(200) )

      DECLARE
            @StrBuild VARCHAR(MAX)
           ,@ResultName VARCHAR(200)
           ,@SQL_Select VARCHAR(500)
           ,@SQL_From VARCHAR(8000)
           ,@SQL_Query VARCHAR(MAX)
           ,@i INT

      SET @StrBuild = ''
      SET @i = 1

      INSERT      INTO @Queue_Name
                  ( 
                   Queue_Name )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Queue_Name_List, ',')

      WHILE ( @i <= ( SELECT
                        COUNT(1)
                      FROM
                        @Queue_Name ) ) 
            BEGIN
                  SELECT
                        @ResultName = Queue_Name
                  FROM
                        @Queue_Name
                  WHERE
                        ID = @i 
                  SET @StrBuild = @StrBuild + 'ISNULL(SUM(CASE WHEN queue_name =' + '''' + @ResultName + '''' + ' THEN InvoiceCount ELSE 0 END), 0) AS  ' + '[' + @ResultName + ']' + ','

                  SET @i = @i + 1
            END
      SET @StrBuild = SUBSTRING(@StrBuild, 1, LEN(@StrBuild) - 1)

      INSERT      INTO #Temp_Quename
                  ( 
                   Queue_Id
                  ,Queue_Type_Id
                  ,Queue_Type
                  ,Queue_Name )
                  SELECT
                        q.Queue_Id
                       ,q.Queue_Type_Id
                       ,ent.ENTITY_NAME
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.QUEUE q
                              ON q.Queue_Id = ui.Queue_Id
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = q.Queue_Type_Id
                        INNER JOIN @Queue_Name qn
                              ON qn.Queue_Name = ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME
                  WHERE
                        ent.ENTITY_NAME = 'Private'


      INSERT      INTO #Temp_Quename
                  ( 
                   Queue_Id
                  ,Queue_Type_Id
                  ,Queue_Type
                  ,Queue_Name )
                  SELECT
                        q.Queue_Id
                       ,q.Queue_Type_Id
                       ,ent.ENTITY_NAME AS queue_type
                       ,gi.GROUP_NAME
                  FROM
                        dbo.GROUP_INFO gi
                        INNER JOIN dbo.QUEUE q
                              ON q.Queue_Id = gi.Queue_Id
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = q.Queue_Type_Id
                  WHERE
                        ent.ENTITY_NAME = 'Public' 

      INSERT      INTO #Queue_table
                  ( 
                   Queue_Name
                  ,QueueType
                  ,Exception_Type
                  ,Cu_Invoice_Id )
                  SELECT
                        tq.Queue_Name
                       ,CASE WHEN t.IS_MANUAL = 1
                                  AND ui.IS_HISTORY != 1 THEN 'Incoming'
                             WHEN t.IS_MANUAL = 0
                                  AND ui.IS_HISTORY != 1 THEN 'Exception'
                        END AS QueueType
                       ,t.Exception_Type
                       ,t.Cu_Invoice_Id
                  FROM
                        dbo.CU_EXCEPTION_DENORM AS t
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.Queue_Id = t.Queue_Id
                        LEFT OUTER JOIN ( #Temp_Quename tq
                                          INNER JOIN @Queue_Name qname
                                                ON tq.Queue_Name = qname.Queue_Name )
                                          ON tq.Queue_Id = t.Queue_Id
                  WHERE
                        ( t.IS_MANUAL = 1
                          OR t.IS_MANUAL = 0 )
                        AND ui.IS_HISTORY != 1
                  GROUP BY
                        tq.Queue_Name
                       ,t.IS_MANUAL
                       ,ui.IS_HISTORY
                       ,t.Exception_Type
                       ,t.Cu_Invoice_Id
                       

      INSERT      INTO #Queue_table
                  ( 
                   Queue_Name
                  ,QueueType
                  ,Exception_Type
                  ,Cu_Invoice_Id )
                  SELECT
                        tq.queue_name
                       ,'Variance' AS queuetype
                       ,CASE WHEN COUNT(DISTINCT l.Variance_Status_Cd) > 1 THEN 'Multiple'
                             ELSE MAX(l.Variance_Status_Desc)
                        END AS Exception_Type
                       ,MIN(l.Variance_Log_Id) AS Cu_Invoice_Id
                  FROM
                        dbo.VARIANCE_LOG AS l
                        INNER JOIN dbo.USER_INFO ui
                              ON l.Queue_Id = ui.Queue_Id
                        INNER JOIN #Temp_Quename AS tq
                              ON tq.Queue_Id = l.Queue_Id
                        INNER JOIN @Queue_Name qn
                              ON l.Queue_Name = qn.Queue_Name
                  WHERE
                        ( l.Closed_Dt IS NULL )
                        AND l.Is_Failure = 1
                        AND ui.IS_HISTORY != 1
                  GROUP BY
                        l.account_id
                       ,l.Service_Month
                       ,tq.queue_name
      SET @SQL_Select = 'SELECT 
							x.QueueType, x.Exception_Type, '

      SET @SQL_From = ' FROM
      ( SELECT
            Queue_Name
           ,QueueType
           ,Exception_Type
           ,COUNT(Cu_Invoice_Id) InvoiceCount
        FROM
            #Queue_table
        GROUP BY
            Queue_Name
           ,QueueType
           ,Exception_Type ) x
 GROUP BY
      x.QueueType
     ,x.Exception_Type
 ORDER BY
      x.QueueType '

      SELECT
            @SQL_Query = @SQL_Select + @StrBuild + @SQL_From

      EXECUTE (@SQL_Query)

      DROP TABLE #Temp_Quename
      DROP TABLE #Queue_table

END

;
GO
GRANT EXECUTE ON  [dbo].[Get_Open_Exception_Dtl_Report_By_Queue_Name] TO [BPOReportRole]
GRANT EXECUTE ON  [dbo].[Get_Open_Exception_Dtl_Report_By_Queue_Name] TO [CBMSApplication]
GO
