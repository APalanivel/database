SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

NAME: dbo.Rfp_Id_String_Sel_By_Site_Id  
  
  
DESCRIPTION: 

	It Return Rfp Ids in Comma Seperated format for given Site Id where bid status type for the accounts associated with site is in
	'New','Open','Bid Placed','Expired','Refresh','Processing'	
  
INPUT PARAMETERS:      
	Name	            DataType       Default     Description      
--------------------------------------------------------------------
	@Site_Id			INTEGER					   

OUTPUT PARAMETERS:           
	Name              DataType          Default     Description      
------------------------------------------------------------      
    
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Rfp_Id_String_Sel_By_Site_Id 2218
	EXEC dbo.Rfp_Id_String_Sel_By_Site_Id 43053

  
AUTHOR INITIALS:  
 Initials	Name
------------------------------------------------------------  
  PNR		PANDARINATH

MODIFICATIONS
  
 Initials	Date		Modification  
------------------------------------------------------------  
   PNR		06/15/2010  Created
   PNR		06/30/2010	Modified script to get RFP Ids in comma seperated format as string for Given Site Id and RFP Type.

******/
  
CREATE PROCEDURE dbo.Rfp_Id_String_Sel_By_Site_Id
	 @Site_Id	INTEGER
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @Rfp_Id_List VARCHAR(1000)
		, @Delimiter CHAR = ','

	SELECT
		@Rfp_Id_List = COALESCE(@Rfp_Id_List + @Delimiter, '') + SPACE(1) + CONVERT(VARCHAR(30),RfpAcc.SR_RFP_ID)
	FROM
		dbo.ACCOUNT acct
		JOIN dbo.Sr_Rfp_Account RfpAcc
			ON RfpAcc.ACCOUNT_ID = acct.ACCOUNT_ID
		JOIN dbo.ENTITY ent
			ON RfpAcc.BID_STATUS_TYPE_ID = ent.ENTITY_ID
	WHERE
		acct.SITE_ID = @Site_Id
		AND RfpAcc.IS_DELETED = 0
		AND ent.ENTITY_NAME IN ('New'
								,'Open'
								,'Bid Placed'
								,'Expired'
								,'Refresh'
								,'Processing'
								)
	GROUP BY
		RfpAcc.SR_RFP_ID

	SELECT @Rfp_Id_List AS Rfp_Id_List

END
GO
GRANT EXECUTE ON  [dbo].[Rfp_Id_String_Sel_By_Site_Id] TO [CBMSApplication]
GO
