SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Sr_Rc_Contract_Document_Accounts_Map_Del]  
     
DESCRIPTION: 
	It Deletes Sr Rc Contract Document Accounts Map  for Selected Sr Rc Contract Document Accounts Map Id.
      
INPUT PARAMETERS:          
NAME										DATATYPE	DEFAULT		DESCRIPTION          
--------------------------------------------------------------------          
@Sr_Rc_Contract_Document_Accounts_Map_Id	INT				
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	Begin Tran
	  EXEC Sr_Rc_Contract_Document_Accounts_Map_Del 10148
	Rollback Tran

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			09-JUNE-10	CREATED     

*/  

CREATE PROCEDURE dbo.Sr_Rc_Contract_Document_Accounts_Map_Del
    (
      @Sr_Rc_Contract_Document_Accounts_Map_Id INT
    )
AS 
BEGIN

    SET NOCOUNT ON ;
   
	DELETE
	FROM 
		dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP
	WHERE
		SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP_ID = @Sr_Rc_Contract_Document_Accounts_Map_Id

END
GO
GRANT EXECUTE ON  [dbo].[Sr_Rc_Contract_Document_Accounts_Map_Del] TO [CBMSApplication]
GO
