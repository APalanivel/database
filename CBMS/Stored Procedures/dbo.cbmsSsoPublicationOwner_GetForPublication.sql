SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     procedure [dbo].[cbmsSsoPublicationOwner_GetForPublication]
	( @MyAccountId int
	, @sso_publication_id int
	)
AS
BEGIN

	   select p.sso_publication_id
		, ps.state_id
		, s.state_name
		, s.country_id
		, c.country_name
	     from sso_publications p 
	     join sso_publications_state_map ps on ps.sso_publication_id = p.sso_publication_id
             join state s on s.state_id = ps.state_id
	     join country c on c.country_id = s.country_id
	    where p.sso_publication_id = @sso_publication_id
	 order by s.state_name

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoPublicationOwner_GetForPublication] TO [CBMSApplication]
GO
