SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_MASTERID_P
	@charge_parent_id INT
AS
BEGIN

	SET NOCOUNT ON

	
--	SELECT DISTINCT cm.charge_master_id
--	FROM dbo.charge_master cm LEFT OUTER JOIN dbo.charge c on c.charge_master_id=cm.charge_master_id
--	WHERE (c.charge_master_id IS NULL)
--		AND EXISTS
--		( SELECT entity_id FROM dbo.entity e1
--			WHERE 
--				(( entity_name IN ('Baseload', 'Contract', 'Load Profile Specification')
--				AND entity_type = 120
--				)
--				OR 
--				( entity_name ='Other' AND entity_type = 113) )
--				AND e1.entity_id = cm.charge_parent_type_id
--		) 
--		AND cm.CHARGE_DISPLAY_ID NOT LIKE 'E%'
--		AND cm.charge_parent_id = @charge_parent_id ;

	SELECT DISTINCT cm.charge_master_id
	FROM dbo.charge_master cm LEFT OUTER JOIN dbo.charge c on c.charge_master_id = cm.charge_master_id
		INNER JOIN dbo.entity e1 ON e1.entity_id = cm.charge_parent_type_id
	WHERE (c.charge_master_id IS NULL)
		AND ((entity_name IN ('Baseload', 'Contract', 'Load Profile Specification') 
				AND entity_type = 120
			) 
			OR ( entity_name ='Other' AND entity_type = 113)
			)
		AND cm.CHARGE_DISPLAY_ID NOT LIKE 'E%'
		AND cm.charge_parent_id = @charge_parent_id ;

END
GO
GRANT EXECUTE ON  [dbo].[GET_MASTERID_P] TO [CBMSApplication]
GO
