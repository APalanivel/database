SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  [Contract_Is_Published_To_EC_Upd_By_Contract]       
        
DESCRIPTION:        
 To Update the flag Is_Publiushed_To_Ec  based on the given Contract_Id     
        
INPUT PARAMETERS:        
 Name				DataType   Default  Description        
-------------------------------------------------------------------------------------------------------  
 @Contract_Id		Int			
 
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
 
 EXEC Contract_Is_Published_To_EC_Upd_By_Contract 10866,1 

        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------------------------------------------------  
 RKV      Ravi Kumar Vegesna      


MODIFICATIONS        
        
 Initials	Date			Modification        
------------------------------------------------------------        
 RKV		2015-12-30		Created        
 
******/        
CREATE PROCEDURE [dbo].[Contract_Is_Published_To_EC_Upd_By_Contract]
      ( 
       @Contract_ID INT
      ,@Is_Published_To_EC BIT
      ,@Published_To_EC_By_User_Id INT = NULL )
AS 
BEGIN
      UPDATE
            CONTRACT
      SET   
            Is_Published_To_EC = @Is_Published_To_EC
           ,Published_To_EC_By_User_Id = @Published_To_EC_By_User_Id
      WHERE
            CONTRACT_ID = @Contract_ID

END


;
GO
GRANT EXECUTE ON  [dbo].[Contract_Is_Published_To_EC_Upd_By_Contract] TO [CBMSApplication]
GO
