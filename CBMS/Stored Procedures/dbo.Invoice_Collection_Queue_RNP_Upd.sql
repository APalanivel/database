SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME: dbo.Invoice_Collection_Queue_RNP_Upd    
    
    
DESCRIPTION: 
    
INPUT PARAMETERS:        
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------  
	@Invoice_collection_Queue_Id		int
	@User_Info_Id						int

OUTPUT PARAMETERS:             
      Name                             DataType          Default     Description        
---------------------------------------------------------------------------------        
    
    
USAGE EXAMPLES:    
---------------------------------------------------------------------------------  
	exec dbo.Invoice_Collection_Queue_RNP_Upd @Invoice_Collection_Queue_Id='2872982',@User_Info_Id=71920


AUTHOR INITIALS:    
 Initials		Name    
---------------------------------------------------------------------------------        
  SLP			Sri Lakshmi Pallikonda    
MODIFICATIONS    
    
 Initials	Date			Modification    
---------------------------------------------------------------------------------      
 SLP		2019-11-05      When moved from ICE to ICR, Received_Status_Updated_Dt,
							& Invoice_File_Name & Invoice_Collection_Exception_Type_Cd must be made NULL
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_RNP_Upd]
(
    @Invoice_collection_Queue_Id VARCHAR(MAX),
    @User_Info_Id INT
)
AS
BEGIN


    DECLARE @Received_Not_Processed_Cd INT;



    SELECT @Received_Not_Processed_Cd = Code_Id
    FROM Code c
        JOIN Codeset cs
            ON c.Codeset_Id = cs.Codeset_Id
    WHERE cs.Codeset_Name = 'IC Exception Type'
          AND c.Code_Value = 'Received Not Processed';


    CREATE TABLE #InputICQ_RNP
    (
        Invoice_Collection_Queue_Id INT,
    );
	

    INSERT #InputICQ_RNP
    (
        Invoice_Collection_Queue_Id
    )
    SELECT Segments Invoice_Collection_Queue_Id
    FROM dbo.ufn_split(@Invoice_collection_Queue_Id, ',') us
        JOIN Invoice_Collection_Queue icq
            ON us.Segments = icq.Invoice_Collection_Queue_Id
    WHERE Invoice_Collection_Exception_Type_Cd = @Received_Not_Processed_Cd;


	UPDATE icq
    SET Received_Status_Updated_Dt = NULL,
        Invoice_File_Name = NULL,
        Invoice_Collection_Exception_Type_Cd = NULL,
        Last_Change_Ts = GETDATE(),
        Updated_User_Id = @User_Info_Id
    FROM #InputICQ_RNP irnp
        JOIN Invoice_Collection_Queue icq
            ON irnp.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id;


    DROP TABLE #InputICQ_RNP;
    
END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_RNP_Upd] TO [CBMSApplication]
GO
