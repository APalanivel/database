SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Missing_Valcon_Exception_Raise_Exist_Sel_By_Contract_Id                     
                          
 DESCRIPTION: To raise the Valcal Exception
       
		              
                          
 INPUT PARAMETERS:      
                         
 Name                  DataType          Default       Description      
--------------------------------------------------------------------
 @Contract_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                  DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                            


   EXEC dbo.Missing_Valcon_Exception_Raise_Exist_Sel_By_Contract_Id  @Contract_Id  = 163963      
    
	                   
 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Missing_Valcon_Exception_Raise_Exist_Sel_By_Contract_Id]
    (
        @Contract_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Raise_Valcon_Exception BIT = 0;

        SELECT
            @Is_Raise_Valcon_Exception = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Supplier_Contract_ID = @Contract_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Valcon_Account_Config vac
                               WHERE
                                    vac.Account_Id = cha.Account_Id
                                    AND vac.Contract_Id = @Contract_Id
                                    AND vac.Is_Config_Complete = 1);


        SELECT  @Is_Raise_Valcon_Exception AS Is_Raise_Valcon_Exception;



    END;



GO
GRANT EXECUTE ON  [dbo].[Missing_Valcon_Exception_Raise_Exist_Sel_By_Contract_Id] TO [CBMSApplication]
GO
