SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********   
NAME:  dbo.Account_Level_Variance_Test_Dtl_Merge  
 
DESCRIPTION:  Used to save all the Account level rules data  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@Variance_Rule_Dtl_Id	Int	 
	@is_Active				bit    
	@Tolerance				decimal(10,0)    
	@Test_Description		varchar(255)  
	@Account_Id				Int 
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	exec Account_Level_Variance_Test_UPD 1425,1,1,321,'nag',141759
	select * from variance_rule_dtl
	
	select * from variance_rule_dtl_account_override where account_id = 141759
	0 , 0,1423	25018
	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------
NK	10/13/2009  Created
NK	12/03/2009  Removed IF ELSE statements and Incorporated MERGE INTO
NK	12/16/2009	Procedure renamed from Account_Level_Variance_Test_Upd
NK  01/20/2010  Added vrdac.tolerance is NULL condition in Merge DELETE section
NK  01/22/2010  Added @is_Manual_Override check in Merge DELETE section
AP	June 11,2020	Modified procedure for AVT phase 3 https://summit.jira.com/browse/SE2017-996
******/

CREATE PROCEDURE [dbo].[Account_Level_Variance_Test_Dtl_Merge]
      @Variance_Rule_Dtl_Id INT
    , @is_Active            BIT
    , @is_Manual_Override   BIT
    , @Tolerance            DECIMAL(16, 4)
    , @Test_Description     VARCHAR(255)
    , @Account_Id           INT
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @Is_Rule_Dtl_Active BIT
                , @Site_Country_Id    INT;



            SELECT      TOP (1)
                        @Site_Country_Id = ch.Country_Id
            FROM        Core.Client_Hier_Account cha
                        INNER JOIN
                        Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
            WHERE       cha.Account_Id = @Account_Id
            ORDER BY    ch.Country_Name;

            SELECT
                  @Is_Rule_Dtl_Active = vrd.IS_Active
            FROM  dbo.Variance_Rule_Dtl vrd
            WHERE vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id;

			IF NOT EXISTS  
                             (     SELECT
                                          1
                                   FROM   dbo.Variance_Rule_Dtl vrd
                                          INNER JOIN
                                          dbo.Variance_Rule vr
                                                ON vrd.Variance_Rule_Id = vr.Variance_Rule_Id
                                          INNER JOIN
                                          dbo.Variance_Parameter_Baseline_Map vpbmap
                                                ON vr.Variance_Baseline_Id = vpbmap.Variance_Baseline_Id
                                          INNER JOIN
                                          dbo.Variance_Parameter vp
                                                ON vp.Variance_Parameter_Id = vpbmap.Variance_Parameter_Id
                                          INNER JOIN
                                          dbo.Code cd
                                                ON cd.Code_Id = vp.Variance_Category_Cd
                                          INNER JOIN
                                          dbo.Code cdb
                                                ON cdb.Code_Id = vpbmap.Baseline_Cd
                                          JOIN
                                          dbo.Variance_Rules_Exclusion vre
                                                ON vre.COuntry_id = @Site_Country_Id
                                                   AND vre.Baseline_Cd = cdb.Code_Id
                                                   AND vre.Category_id = cd.Code_Id
                                   WHERE  vrd.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id )

								   BEGIN
            MERGE INTO dbo.Variance_Rule_Dtl_Account_Override vrdac
            USING
                  (     SELECT
                              @Variance_Rule_Dtl_Id Variance_Rule_Dtl_Id
                            , @Account_Id Account_Id ) src
            ON vrdac.Variance_Rule_Dtl_Id = src.Variance_Rule_Dtl_Id
               AND vrdac.ACCOUNT_ID = src.Account_Id
               AND src.Variance_Rule_Dtl_Id = @Variance_Rule_Dtl_Id
                 
            WHEN MATCHED AND (     (     @Is_Rule_Dtl_Active = 0
                                         AND    vrdac.Tolerance IS NULL )
                                   OR
                                          (     @is_Active = 0
                                                AND   @Is_Rule_Dtl_Active = 1 ))
                  THEN DELETE
            WHEN MATCHED AND (     @is_Active = 1
                                   AND    @is_Manual_Override = 1 )
                             OR
                                    (     @is_Active = 1
                                          AND   @is_Manual_Override = 0 )
                  THEN UPDATE SET
                             vrdac.Tolerance = CASE @is_Manual_Override
                                                    WHEN 0
                                                          THEN NULL
                                                    ELSE  @Tolerance
                                               END
                           , vrdac.Test_Description = CASE @is_Manual_Override
                                                           WHEN 0
                                                                 THEN NULL
                                                           ELSE  @Test_Description
                                                      END
            WHEN NOT MATCHED BY TARGET AND (     (     @is_Active = 1
                                                       AND  @is_Manual_Override = 0 )
                                                 AND  ( @Is_Rule_Dtl_Active = 1 )
                                                 OR
                                                       (     (     @is_Manual_Override = 1
                                                                   AND  @is_Active = 1 )
                                                             AND
                                                                   (     @Is_Rule_Dtl_Active = 0
                                                                         OR   @Is_Rule_Dtl_Active = 1 ))
                                                 OR
                                                       (     (     @is_Manual_Override = 0
                                                                   AND  @is_Active = 0 )
                                                             AND  ( @Is_Rule_Dtl_Active = 1 )))
                  THEN INSERT
                             ( Variance_Rule_Dtl_Id
                             , ACCOUNT_ID
                             , Tolerance
                             , Test_Description )
                       VALUES
                            ( @Variance_Rule_Dtl_Id, @Account_Id, CASE WHEN @is_Manual_Override = 0
                                                                             THEN NULL
                                                                       ELSE  @Tolerance
                                                                  END, CASE WHEN @is_Manual_Override = 0
                                                                                  THEN NULL
                                                                            ELSE  @Test_Description
                                                                       END );
      END;
	  END;
GO
GRANT EXECUTE ON  [dbo].[Account_Level_Variance_Test_Dtl_Merge] TO [CBMSApplication]
GO
