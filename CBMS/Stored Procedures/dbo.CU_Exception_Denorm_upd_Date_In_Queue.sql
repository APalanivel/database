SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
CREATE PROCEDURE [dbo].[CU_Exception_Denorm_upd_Date_In_Queue]  
      (     
       @cu_invoice_id INT    
      ,@Account_Id INT = NULL    
      ,@Commodity_Id INT = NULL  
   ,@date_in_queue datetime )    
AS     
BEGIN      
      
      SET nocount ON      
      
      update    
            cu_exception_denorm    
 set   
  DATE_IN_QUEUE = @date_in_queue  
      WHERE    
            cu_invoice_id = @cu_invoice_id    
            AND ( @Account_Id IS NULL    
                  OR Account_ID = @Account_Id )    
            AND ( @Commodity_Id IS NULL    
                  OR Commodity_Id = @Commodity_Id )     
END      
;  
  
GO
GRANT EXECUTE ON  [dbo].[CU_Exception_Denorm_upd_Date_In_Queue] TO [CBMSApplication]
GO
