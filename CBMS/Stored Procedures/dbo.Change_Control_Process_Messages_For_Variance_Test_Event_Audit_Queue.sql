SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:		[dbo].[Change_Control_Process_Messages_For_Variance_Test_Event_Audit_Queue]

DESCRIPTION:
	automaitcially activates when a record is recieved on the Variance_Test_Event_Audit_Queue
	and manages the changes based on the Message Type


INPUT PARAMETERS:
	Name						DataType		Default	Description
-----------------------------------------------------------------------------


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------------------


USAGE EXAMPLES:
-----------------------------------------------------------------------------
 -- Procedure is only executed on Variance_Test_Event_Audit_Queue

AUTHOR INITIALS:
	Initials	Name
-----------------------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
-----------------------------------------------------------------------------
	RR			2014-09-17	Created MAINT-3040 Moving non-process dependent auditing to queued update to help performance

******/


CREATE PROCEDURE [dbo].[Change_Control_Process_Messages_For_Variance_Test_Event_Audit_Queue]
AS 
BEGIN 
      SET NOCOUNT ON;

      DECLARE
            @Param_Definition NVARCHAR(100) = N'@Message XML, @Conversation_Handle UNIQUEIDENTIFIER, @Message_Batch_Count INT OUTPUT' -- footprint of message procedures
           ,@CG_Iterator BIT = 1 -- Flag used to iterate through conversation Groups
			
      DECLARE @Message TABLE -- Table used to store failed messages for a conversation_Group
            ( 
             Message_Sequence_Number INT
            ,Conversation_Handle UNIQUEIDENTIFIER
            ,SERVICE_NAME VARCHAR(255)
            ,CONTRACT_NAME VARCHAR(255)
            ,Message_Type VARCHAR(255)
            ,Message_Body VARBINARY(MAX) )

      WHILE ( @Cg_Iterator = 1 ) 
            BEGIN
                  DECLARE @Conversation_Group_Id UNIQUEIDENTIFIER = NULL; 

                  GET CONVERSATION GROUP @Conversation_Group_Id	-- get a conversation group to process
		FROM Variance_Test_Event_Audit_Queue


		
		
                  IF @Conversation_Group_Id IS NOT NULL 
                        BEGIN
                              BEGIN TRY 
                                    BEGIN TRANSACTION
                                    DECLARE @M_Iterator INT = 1
					 
                                    WHILE ( @M_Iterator = 1 )		-- Cycle through all messages in CG
                                          BEGIN
                                                DECLARE
                                                      @I_Conversation_Handle UNIQUEIDENTIFIER = NULL		-- Incmming message handler 
                                                     ,@I_Message_Type SYSNAME = NULL						-- Incomming Message Type
                                                     ,@I_Message VARCHAR(MAX) = NULL 					-- Incomming Message
                                                     ,@Stmt NVARCHAR(2000) = NULL						-- Statement to execute
                                                     ,@Message_Type_Exists BIT = 0 						-- Flag to validate tha tthe message type exists
                                                     ,@O_Message_Batch_Count INT							-- Output Variable to get count of total records changed.
						
                                                SAVE TRANSACTION savepoint					-- Set individual save points for each message
						
						;
                                                RECEIVE TOP (1)
							@I_Conversation_Handle = Conversation_Handle
							,@I_Message_Type = Message_Type_Name
							,@I_Message = Message_Body	
						FROM 
							Variance_Test_Event_Audit_Queue
						WHERE 
							Conversation_Group_Id = @Conversation_Group_Id

                                                IF @I_Conversation_Handle IS NOT NULL -- Process message 
                                                      BEGIN
							-- Get the statement to run for this message type 
                                                            SELECT
                                                                  @Message_Type_Exists = 1
                                                                 ,@Stmt = N'EXEC ' + PROCEDURE_NAME + ' @Message, @Conversation_Handle'
                                                            FROM
                                                                  Service_Broker_Message_Procedure
                                                            WHERE
                                                                  Queue_Name = 'Variance_Test_Event_Audit_Queue'
                                                                  AND Message_Type = @I_Message_Type

                                                            IF @Message_Type_Exists = 1  -- Execute procedure for known the message type
                                                                  BEGIN 
                                                                        EXEC sp_EXECUTESQL 
                                                                              @Statement = @Stmt
                                                                             ,@params = @Param_Definition
                                                                             ,@Message = @I_message
                                                                             ,@Conversation_Handle = @I_Conversation_Handle
                                                                             ,@Message_Batch_Count = @O_Message_Batch_Count OUTPUT
									
                                                                  END
                                                            ELSE 
                                                                  BEGIN 
                                                                        RAISERROR ('Unknown message Type: %s', 16, 1, @I_Message_Type)	-- if the message type is not known then thow an error
                                                                  END
                                                      END
                                                ELSE 
                                                      BEGIN															-- Quit processes when no message was recieved
                                                            SET @M_Iterator = 0 
                                                      END
                                          END
                                    COMMIT TRANSACTION		-- Commit the successfull messages in the Conversation Group
				
			
				
                              END TRY	
                              BEGIN CATCH
                                    DECLARE @ErrorMessage NVARCHAR(4000) = error_message()

                                    IF ( xact_state() <> -1 ) 
                                          BEGIN
                                                ROLLBACK TRANSACTION savePoint -- Rollback the active transaction to the last save 
                                                COMMIT TRANSACTION	-- Commit any messages that executed successfully
                                          END 
                                    ELSE 
                                          BEGIN
                                                ROLLBACK TRANSACTION -- Rollback complete trnasaction if it is not possible to roll back to the save point
                                          END
				
				-- Receive all messages and insert them into the Poison message table	
				;
                                    RECEIVE 
					Message_Sequence_Number
					,Conversation_Handle
					,SERVICE_NAME
					,Service_Contract_Name
					,Message_Type_Name
					,Message_Body
				FROM 
					Variance_Test_Event_Audit_Queue
				INTO @message
				WHERE 
					Conversation_Group_Id = @Conversation_Group_Id

                                    INSERT      Service_Broker_Poison_Message
                                                ( 
                                                 Queue_Name
                                                ,Message_Received_Ts
                                                ,Conversation_Group_Id
                                                ,Message_Sequence_Number
                                                ,Conversation_Handle
                                                ,SERVICE_NAME
                                                ,CONTRACT_NAME
                                                ,Message_Type
                                                ,Error_text
                                                ,MESSAGE_Body )
                                                SELECT
                                                      'Variance_Test_Event_Audit_Queue'
                                                     ,getdate()
                                                     ,@Conversation_Group_Id
                                                     ,Message_Sequence_Number
                                                     ,Conversation_Handle
                                                     ,SERVICE_NAME
                                                     ,CONTRACT_NAME
                                                     ,Message_Type
                                                     ,@ErrorMessage
                                                     ,message_Body
                                                FROM
                                                      @Message 
				
                                    RAISERROR(60001, 16, 1, 'Variance_Test_Event_Audit_Queue', @errormessage) -- Raise Standard Error 
                              END CATCH
                        END
                  ELSE 
                        BEGIN
                              SET @Cg_Iterator = 0 
                        END
            END
END
IF @@ERROR <> 0
      AND @@TRANCOUNT > 0 
      ROLLBACK TRANSACTION;

GO
