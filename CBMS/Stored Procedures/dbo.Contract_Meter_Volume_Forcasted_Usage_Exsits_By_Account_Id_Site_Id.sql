SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
 [dbo].[Contract_Meter_Volume_Forcasted_Usage_Exsits_By_Account_Id_Site_Id]
 
 Description:  
 			
 
 Input Parameters:  
  Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
 @userId				VARCHAR
 @sessionId				VARCHAR

 
 Output Parameters:  
  Name					DataType			Default					Description  
-------------------------------------------------------------------------------------   
 
 Usage Examples:
------------------------------------------------------------------------------------- 

EXEC dbo.Contract_Meter_Volume_Forcasted_Usage_Exsits_By_Account_Id_Site_Id 
      1
     ,262357
     ,262357


Author Initials:  
	Initials	Name
-------------------------------------------------------------------------------------   
	RKV			Ravi Kumar Vegesna
	RR			Raghu Reddy
 
 Modifications :  
	Initials	Date		Modification  
-------------------------------------------------------------------------------------   
	RKV			2017-11-14	SE2017-319 Created
	RR			2019-09-27	GRM-1507 & PRSUPPORT-2990 - Updated script references to new risk management schema  
      
******/
CREATE PROCEDURE [dbo].[Contract_Meter_Volume_Forcasted_Usage_Exsits_By_Account_Id_Site_Id]
    (
        @Account_Id INT
        , @Old_Site_Id INT
        , @New_Site_Id INT = NULL
    )
AS
    BEGIN

        DECLARE @Meter_Id TABLE
              (
                  Meter_Id INT
              );

        DECLARE
            @Contract_Meter_Volume_Exists BIT
            , @Old_Site_forecasted_Usage_Exists BIT = 0
            , @New_Site_forecasted_Usage_Exists BIT = 0;

        INSERT INTO @Meter_Id
             (
                 Meter_Id
             )
        SELECT
            Meter_Id
        FROM
            Core.Client_Hier_Account cha
        WHERE
            Account_Id = @Account_Id;


        SELECT
            @Contract_Meter_Volume_Exists = CASE WHEN CAST(ISNULL(SUM(VOLUME), 0) AS INTEGER) = 0 THEN 0
                                                ELSE 1
                                            END
        FROM
            dbo.CONTRACT_METER_VOLUME cmv
            INNER JOIN @Meter_Id mi
                ON cmv.METER_ID = mi.Meter_Id;

        SELECT
            @Old_Site_forecasted_Usage_Exists = 1
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Trade.RM_Client_Hier_Onboard siteob
                            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                            INNER JOIN dbo.ENTITY et
                                ON et.ENTITY_ID = chhc.Hedge_Type_Id
                            INNER JOIN Core.Client_Hier ch
                                ON ch.Client_Hier_Id = siteob.Client_Hier_Id
                       WHERE
                            ch.Site_Id = @Old_Site_Id
                            AND et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
            OR  EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Client_Hier_Onboard clntob
                                INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                    ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                INNER JOIN dbo.ENTITY et
                                    ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                INNER JOIN Core.Client_Hier clch
                                    ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                INNER JOIN Core.Client_Hier ch
                                    ON clch.Client_Id = ch.Client_Id
                                       AND  clntob.Country_Id = ch.Country_Id
                           WHERE
                                et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' )
                                AND clch.Sitegroup_Id = 0
                                AND ch.Site_Id > 0
                                AND ch.Site_Id = @Old_Site_Id
                                AND NOT EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        Trade.RM_Client_Hier_Onboard siteob1
                                                   WHERE
                                                        siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                        AND clntob.Commodity_Id = siteob1.Commodity_Id))
            OR  EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Account_Forecast_Volume rafv
                           WHERE
                                rafv.Account_Id = @Account_Id
                           HAVING
                                CAST(ISNULL(SUM(rafv.Forecast_Volume), -1) AS INTEGER) > 0);

        SELECT
            @New_Site_forecasted_Usage_Exists = 1
        WHERE
            EXISTS (   SELECT
                            1
                       FROM
                            Trade.RM_Client_Hier_Onboard siteob
                            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                ON siteob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                            INNER JOIN dbo.ENTITY et
                                ON et.ENTITY_ID = chhc.Hedge_Type_Id
                            INNER JOIN Core.Client_Hier ch
                                ON ch.Client_Hier_Id = siteob.Client_Hier_Id
                       WHERE
                            ch.Site_Id = @New_Site_Id
                            AND @New_Site_Id IS NOT NULL
                            AND et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' ))
            OR  EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Client_Hier_Onboard clntob
                                INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                                    ON clntob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                                INNER JOIN dbo.ENTITY et
                                    ON et.ENTITY_ID = chhc.Hedge_Type_Id
                                INNER JOIN Core.Client_Hier clch
                                    ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                                INNER JOIN Core.Client_Hier ch
                                    ON clch.Client_Id = ch.Client_Id
                                       AND  clntob.Country_Id = ch.Country_Id
                           WHERE
                                et.ENTITY_NAME IN ( 'Physical', 'Financial', 'Physical & Financial' )
                                AND clch.Sitegroup_Id = 0
                                AND ch.Site_Id > 0
                                AND ch.Site_Id = @New_Site_Id
                                AND @New_Site_Id IS NOT NULL
                                AND NOT EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        Trade.RM_Client_Hier_Onboard siteob1
                                                   WHERE
                                                        siteob1.Client_Hier_Id = ch.Client_Hier_Id
                                                        AND clntob.Commodity_Id = siteob1.Commodity_Id))
            OR  EXISTS (   SELECT
                                1
                           FROM
                                Trade.RM_Account_Forecast_Volume rafv
                           WHERE
                                rafv.Account_Id = @Account_Id
                           HAVING
                                CAST(ISNULL(SUM(rafv.Forecast_Volume), -1) AS INTEGER) > 0);



        SELECT
            @Contract_Meter_Volume_Exists Contract_Meter_Volume_Exists
            , @Old_Site_forecasted_Usage_Exists Old_Site_forecasted_Usage_Exists
            , @New_Site_forecasted_Usage_Exists New_Site_forecasted_Usage_Exists;
    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Meter_Volume_Forcasted_Usage_Exsits_By_Account_Id_Site_Id] TO [CBMSApplication]
GO
