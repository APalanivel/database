SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_INVOICE_P
	@invoice_for_month DATETIME,
	@account_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT inv.invoice_id
	FROM dbo.invoice inv
	WHERE inv.account_id = @account_id
		AND inv.invoice_for_month = @invoice_for_month
		AND inv.is_invoice_effective = 1
		

END
GO
GRANT EXECUTE ON  [dbo].[GET_INVOICE_P] TO [CBMSApplication]
GO
