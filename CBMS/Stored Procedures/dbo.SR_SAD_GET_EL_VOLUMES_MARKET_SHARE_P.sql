
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.SR_SAD_GET_EL_VOLUMES_MARKET_SHARE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@regionId      	int       	          	
	@fromDate      	VARCHAR(12)	          	
	@asOfDate      	VARCHAR(12)	          	
	@stateId       	int       	          	
	@utilityId     	int       	          	
	@supplierId    	int       	          	
	@analystId     	int    
	@StartIndex		INT = 1
    @EndIndex		INT = 2147483647   	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.SR_SAD_GET_EL_VOLUMES_MARKET_SHARE_P  NULL ,'09/09/2009','08/09/2010',NULL,NULL,NULL,NULL,1,25
	
	EXEC dbo.SR_SAD_GET_EL_VOLUMES_MARKET_SHARE_P NULL,'5/31/2005','5/31/2006',NULL,NULL,NULL,NULL,1
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR		Pandarinath
	AP		Athmaram Pabbathi

MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	        	07/20/2009   Autogenerated script
	SS		10/08/2009   Replaced contract's account_id reference with supplier_account_meter_map's account_id
	HG		10/23/2009   unused parameters @Userid @Sessionid removed.
	SKA       06/30/2010   Added pagination logic
							Removed nolock hints & Hard coded values
	SKA		08/04/2010   Added the sorting logic.							
	PNR		08/09/2010   Modified Sql from dynamic to static for the purpose of sorting only on Utility Name 
							and also added cross join part to display total count of Unique Utility names in Total Rows column.
     DMR		09/10/2010   Modified for Quoted_Identifier
     AP		09/19/2011   Changes made for Addnl Data prj
						  - Removed UDF SR_RFP_FN_GET_AVAILABLE_VOLUMES and used Sub Query instead.
     AP		2012-04-10   Repaced Account, Meter, SAMM, Vendor, Utility_Analyst_Map, User_Info base tables with CHA
******/
CREATE PROCEDURE [dbo].[SR_SAD_GET_EL_VOLUMES_MARKET_SHARE_P]
      (
       @RegionId INT
      ,@FromDate VARCHAR(12)
      ,@AsOfDate VARCHAR(12)
      ,@StateId INT
      ,@UtilityId INT
      ,@SupplierId INT
      ,@AnalystId INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS
BEGIN
      SET NOCOUNT ON;

      DECLARE
            @Commodity_type_id INT
           ,@Contract_Type_id INT
           ,@EL_Unit_of_Measure_Type_ID INT;

      CREATE TABLE #Utility_Account_Dtl
            (
             Commodity VARCHAR(200)
            ,Utility_Name VARCHAR(200)
            ,State_Name VARCHAR(200)
            ,Region_Name VARCHAR(200)
            ,Analyst VARCHAR(100)
            ,Supplier_Name VARCHAR(200)
            ,Account_Id INT
            ,Total_Rows INT );		

      CREATE CLUSTERED INDEX IDX_Account_Dtl_Account_Id ON #Utility_Account_Dtl(Account_Id);
      
      CREATE TABLE #Cost_Usage_Data
            (
             Account_Id INT PRIMARY KEY CLUSTERED
            ,Available_Volume DECIMAL(32, 16)
            ,Volume_Served DECIMAL(32, 16) );

      SELECT
            @Commodity_type_id = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power';
            

      SELECT
            @Contract_Type_id = e.entity_id
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_DESCRIPTION = 'Contract Type'
            AND e.ENTITY_NAME = 'Supplier'; 
            
      SELECT
            @EL_Unit_of_Measure_Type_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'kW'
            AND uom.Entity_Description = 'Unit for electricity';
            

      WITH  CTE_Utility_Account_Dtl
              AS ( SELECT
                        'Electric Power' AS Commodity
                       ,ucha.Account_Vendor_Name AS Utility_Name
                       ,st.State_Name
                       ,r.Region_Name
                       ,ui.First_Name + ' ' + ui.Last_Name AS Analyst
                       ,scha.Account_Vendor_Name AS Supplier_Name
                       ,ucha.Account_Id
                       ,DENSE_RANK() OVER ( ORDER BY ucha.Account_Vendor_Name ) AS Row_Num
                   FROM
                        Core.Client_Hier_Account ucha
                        INNER JOIN Core.Client_Hier_Account scha
                              ON scha.Meter_Id = ucha.Meter_Id
                        INNER JOIN dbo.[Contract] con
                              ON con.Contract_Id = scha.Supplier_Contract_Id
                        INNER JOIN dbo.Vendor_State_Map vsm
                              ON vsm.Vendor_Id = ucha.Account_Vendor_Id
                        INNER JOIN dbo.[State] st
                              ON st.State_Id = vsm.State_Id
                        INNER JOIN dbo.Region r
                              ON r.Region_Id = st.Region_Id
                        INNER JOIN dbo.Utility_Detail ud
                              ON ud.Vendor_Id = ucha.Account_Vendor_Id
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.Vendor_Id = ud.VENDOR_ID
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
                        INNER JOIN dbo.User_Info ui
                              ON ui.User_Info_id = vcam.Analyst_Id
                   WHERE
                        @AsOfDate BETWEEN con.Contract_Start_Date
                                  AND     con.Contract_End_Date
                        AND vcm.Commodity_Type_Id = @Commodity_type_id
                        AND con.Commodity_Type_Id = @Commodity_type_id
                        AND con.Contract_Type_Id = @Contract_Type_id
                        AND ucha.Account_Not_Managed = 0
                        AND ( @RegionId IS NULL
                              OR r.Region_id = @RegionId )
                        AND ( @StateId IS NULL
                              OR st.State_Id = @StateId )
                        AND ( @UtilityId IS NULL
                              OR ucha.Account_Vendor_Id = @UtilityId )
                        AND ( @SupplierId IS NULL
                              OR scha.Account_Vendor_Id = @SupplierId )
                        AND ( @AnalystId IS NULL
                              OR ui.User_Info_Id = @AnalystId )
                   GROUP BY
                        ucha.Account_Vendor_Name
                       ,st.State_Name
                       ,r.Region_Name
                       ,ui.First_Name + ' ' + ui.Last_Name
                       ,scha.Account_Vendor_Name
                       ,ucha.Account_Id)
            INSERT      INTO #Utility_Account_Dtl
                        ( Commodity
                        ,Utility_Name
                        ,State_Name
                        ,Region_Name
                        ,Analyst
                        ,Supplier_Name
                        ,Account_Id )
                        SELECT
                              uad.Commodity
                             ,uad.Utility_Name
                             ,uad.State_Name
                             ,uad.Region_Name
                             ,uad.Analyst
                             ,uad.Supplier_Name
                             ,uad.Account_Id
                        FROM
                              CTE_Utility_Account_Dtl uad
                              CROSS JOIN ( SELECT
                                                COUNT(DISTINCT final.Utility_Name) AS Total
                                           FROM
                                                CTE_Utility_Account_Dtl final ) Utility_Total_Rows
                        WHERE
                              uad.Row_Num BETWEEN @StartIndex AND @EndIndex;

      INSERT      INTO #Cost_Usage_Data
                  ( Account_Id
                  ,Available_Volume
                  ,Volume_Served )
                  SELECT
                        AD.Account_Id
                       ,ISNULL(X.Available_Volume, -1) AS Available_Volume
                       ,dbo.SR_RFP_FN_GET_VOLUMES_SERVED(AD.Account_Id, @FromDate, @AsOfDate) AS Volume_Served
                  FROM
                        #Utility_Account_Dtl AD
                        LEFT OUTER JOIN ( SELECT
                                                CUAD.Account_Id
                                               ,MAX(CUAD.Bucket_Value) AS Available_Volume
                                          FROM
                                                #Utility_Account_Dtl act
                                                INNER JOIN dbo.Cost_Usage_Account_Dtl CUAD
                                                      ON CUAD.Account_Id = act.Account_Id
                                                INNER JOIN dbo.Bucket_Master BM
                                                      ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                                                INNER JOIN dbo.Code CD
                                                      ON CD.Code_Id = BM.Bucket_Type_Cd
                                          WHERE
                                                BM.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' )
                                                AND BM.Commodity_Id = @Commodity_type_id
                                                AND CD.Code_Value = 'Determinant'
                                                AND CUAD.UOM_Type_Id = @EL_Unit_of_Measure_Type_ID
                                                AND CUAD.Service_Month BETWEEN CONVERT(DATETIME, @FromDate)
                                                                       AND     CONVERT(DATETIME, @AsOfDate)
                                          GROUP BY
                                                CUAD.Account_Id ) X
                              ON X.Account_Id = AD.Account_Id;


      SELECT
            ad.Commodity
           ,ad.Utility_Name
           ,ad.State_Name
           ,ad.Region_Name
           ,ad.Analyst
           ,ad.Supplier_Name
           ,SUM(cud.Available_Volume) AS Available_Volume
           ,SUM(cud.Volume_Served) AS Volume_Served
           ,ad.Total_Rows AS Total
      FROM
            #Utility_Account_Dtl ad
            LEFT OUTER JOIN #Cost_Usage_Data cud
                  ON ad.Account_Id = cud.Account_Id
      GROUP BY
            ad.Commodity
           ,ad.Utility_Name
           ,ad.State_Name
           ,ad.Region_Name
           ,ad.Analyst
           ,ad.Supplier_Name
           ,ad.Total_Rows; 


      DROP TABLE #Utility_Account_Dtl;
      DROP TABLE #Cost_Usage_Data;

END;
;
GO


GRANT EXECUTE ON  [dbo].[SR_SAD_GET_EL_VOLUMES_MARKET_SHARE_P] TO [CBMSApplication]
GO
