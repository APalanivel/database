SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_GET_DEAL_VOLUME_TERM_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.SR_SAD_GET_DEAL_VOLUME_TERM_TYPE_P
@userId varchar(10),
@sessionId varchar(20)

AS
set nocount on

select ENTITY_ID, ENTITY_NAME from ENTITY 
	where ENTITY_DESCRIPTION='Deal Volume Type'
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_DEAL_VOLUME_TERM_TYPE_P] TO [CBMSApplication]
GO
