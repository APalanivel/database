SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.UPDATE_RATE_SCHEDULE_DATES_IMAGE_P
	@cbmsImageId INT,
	@rsStartDate DATETIME,
	@rsEndDate DATETIME,
	@rateScheduleId INT
AS
BEGIN

	SET NOCOUNT ON

	UPDATE dbo.rate_schedule 
	SET cbms_image_id = @cbmsImageId, 
		rs_start_date = @rsStartDate, 
		rs_end_date = @rsEndDate 
	WHERE rate_schedule_id = @rateScheduleId

END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_RATE_SCHEDULE_DATES_IMAGE_P] TO [CBMSApplication]
GO
