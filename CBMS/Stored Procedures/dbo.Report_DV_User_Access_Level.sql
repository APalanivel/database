
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:

CBMS.dbo.Report_DV_User_Access_Level

DESCRIPTION:

A list of all DV users for a client, and those users? access levels (Site, Division, Corporate)

INPUT PARAMETERS:
Name				DataType		Default	Description
------------------------------------------------------------
@Client_ID_List		Varchar(MAX)
@User_ID_LIST		VARCHAR(MAX)

OUTPUT PARAMETERS:
Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	-- Corporate & Division
		Exec dbo.Report_DV_User_Access_Level '1005','324,1924,2578,244'
	-- Site
		Exec dbo.Report_DV_User_Access_Level '10005','248,244'

	-- Corporate
		Exec dbo.Report_DV_User_Access_Level '1032','6604'

	-- Corporate & Division
		Exec dbo.Report_DV_User_Access_Level '1005','324,1924,2578,244'
		Exec dbo.Report_DV_User_Access_Level_New '1005','324,1924,2578,244'
	-- Site
		Exec dbo.Report_DV_User_Access_Level '10005','248,244'
		Exec dbo.Report_DV_User_Access_Level_New '10005','248,244'

	-- Corporate
		Exec dbo.Report_DV_User_Access_Level '1032','6604'

AUTHOR INITIALS:
Initials	Name
-----------------------------------------------------------
SSR			Sharad Srivastava
HG			Harihara Suthan Ganesan

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
SSR        	06/14/2010	Created
HG			2014-02-07	MAINT-2554, User_Sal, User_Site_Map and User_Division_Map tables replaced with Security_Role & Client_Hier tables.
******/
CREATE PROC dbo.Report_DV_User_Access_Level
      @Client_ID_List VARCHAR(MAX)
     ,@User_ID_LIST VARCHAR(MAX)
AS 
BEGIN 

      SET NOCOUNT ON;

      DECLARE @User_List TABLE
            ( 
             User_Info_Id INT PRIMARY KEY )

      DECLARE @Client_List TABLE
            ( 
             Client_Id INT PRIMARY KEY )

      CREATE TABLE #User_Access_Level
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED
            ,Hier_Level_Cd INT
            ,Access_Level VARCHAR(25)
            ,Security_Role_Id INT )

      CREATE INDEX #UAL_Security_Role_Id ON #User_Access_Level(Security_Role_Id)
      CREATE INDEX #UAL_Hier_Level ON #User_Access_Level(Hier_Level_Cd)

      INSERT      INTO @User_List
                  ( 
                   User_Info_Id )
                  SELECT
                        CAST(ufn_tmp.Segments AS INT) Segments
                  FROM
                        dbo.ufn_split(@User_ID_LIST, ',') ufn_tmp;

      INSERT      INTO @Client_List
                  ( 
                   Client_Id )
                  SELECT
                        CAST(ufn_tmp.Segments AS INT) Segments
                  FROM
                        dbo.ufn_split(@Client_ID_List, ',') ufn_tmp;

      INSERT      INTO #User_Access_Level
                  ( 
                   User_Info_Id
                  ,Hier_Level_Cd
                  ,Access_Level
                  ,Security_Role_Id )
                  SELECT
                        usr.User_Info_Id
                       ,COALESCE(MAX(CASE WHEN hl.Code_Value = 'Corporate' THEN hl.Code_Id
                                     END), MAX(CASE WHEN hl.Code_Value = 'Division' THEN hl.Code_Id
                                               END), MAX(CASE WHEN hl.Code_Value = 'Site' THEN hl.Code_Id
                                                         END)) AS Hier_Level_Cd
                       ,COALESCE(MAX(CASE WHEN hl.Code_Value = 'Corporate' THEN hl.Code_Value
                                     END), MAX(CASE WHEN hl.Code_Value = 'Division' THEN hl.Code_Value
                                               END), MAX(CASE WHEN hl.Code_Value = 'Site' THEN hl.Code_Value
                                                         END)) AS Access_Level
                       ,usr.Security_Role_Id
                  FROM
                        dbo.User_Security_Role usr
                        INNER JOIN dbo.Security_Role sr
                              ON usr.Security_Role_Id = sr.Security_Role_Id
                        INNER JOIN dbo.Security_Role_Client_Hier csrch
                              ON csrch.Security_Role_Id = usr.Security_Role_Id
                        INNER JOIN core.Client_Hier ch
                              ON ch.Client_Hier_Id = csrch.Client_Hier_Id
                        INNER JOIN dbo.Code hl
                              ON hl.Code_Id = ch.Hier_level_Cd
                        INNER JOIN @Client_List cl
                              ON sr.Client_Id = cl.Client_Id
                        INNER JOIN @User_List ul
                              ON ul.User_Info_Id = usr.User_Info_Id
                        INNER JOIN dbo.User_Info ui
                              ON usr.User_Info_Id = ui.USER_INFO_ID
                  WHERE
                        ui.ACCESS_LEVEL = 1
                  GROUP BY
                        usr.User_Info_Id
                       ,usr.Security_Role_Id;

      SELECT
            ui.First_Name [First Name]
           ,ui.Last_Name [Last Name]
           ,ui.UserName [User Name]
           ,ui.Email_Address [Email Address]
           ,ual.Access_Level AS AccessLevel
           ,ch.CLIENT_NAME [Client]
           ,Division = CASE WHEN ual.Access_Level = 'Corporate' THEN 'All'
                            ELSE ch.Sitegroup_Name
                       END
           ,Site = CASE WHEN ual.Access_Level IN ( 'Corporate', 'Division' ) THEN 'All'
                        ELSE ch.Site_Name
                   END
      FROM
            dbo.User_Info ui
            INNER JOIN #User_Access_Level ual
                  ON ual.User_Info_Id = ui.USER_INFO_ID
            INNER JOIN dbo.Security_Role_Client_Hier srch
                  ON srch.Security_Role_Id = ual.Security_Role_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = srch.Client_Hier_Id
                     AND ch.Hier_level_Cd = ual.Hier_Level_Cd
      WHERE
            ui.ACCESS_LEVEL != 0
            AND ui.IS_HISTORY = 0
            
      DROP TABLE #User_Access_Level

END

;
GO

GRANT EXECUTE ON  [dbo].[Report_DV_User_Access_Level] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DV_User_Access_Level] TO [CBMSApplication]
GO
