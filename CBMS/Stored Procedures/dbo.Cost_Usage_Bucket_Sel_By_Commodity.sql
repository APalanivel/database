SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME: [DBO].[Cost_Usage_Bucket_Sel_By_Commodity]  
     
DESCRIPTION:
	To get total usage and cost Bucket id for the given commodity id

INPUT PARAMETERS:          
NAME						DATATYPE	DEFAULT	DESCRIPTION          
------------------------------------------------------------          
@Commodity_Id				INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	EXEC dbo.Cost_Usage_Bucket_Sel_By_Commodity 290
	EXEC dbo.Cost_Usage_Bucket_Sel_By_Commodity 67

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
HG		Harihara Suthan G
AP	     Athmaram Pabbathi

MODIFICATIONS
INITIALS	DATE		    MODIFICATION
------------------------------------------------------------
HG		08/20/2011    Created as a part of additional data enhancement
AP		03/28/2012    Added bm.Commodity_Id = @Commodity_Id filter in the final select
*/

CREATE PROCEDURE dbo.Cost_Usage_Bucket_Sel_By_Commodity 
( @Commodity_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE @Total_Usage_Bucket_Name VARCHAR(200)

      SELECT
            @Total_Usage_Bucket_Name = case WHEN com.Commodity_Name = 'Electric Power' THEN 'Total Usage'
                                            WHEN com.Commodity_Name = 'Natural Gas' THEN 'Total Usage'
                                            ELSE 'Volume'	-- For other services
                                       END
      FROM
            dbo.Commodity com
      WHERE
            Com.Commodity_Id = @Commodity_id

      SELECT
            bm.Bucket_Master_id
           ,bm.Bucket_Name
           ,bt.Code_Value AS Bucket_Type
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
      WHERE
            bm.Bucket_Name IN ( @Total_Usage_Bucket_Name, 'Total Cost' )
            AND bm.Commodity_Id = @Commodity_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Bucket_Sel_By_Commodity] TO [CBMSApplication]
GO
