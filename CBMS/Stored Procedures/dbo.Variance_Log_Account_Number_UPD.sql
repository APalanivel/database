SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Variance_Log_Account_Number_UPD   
 
DESCRIPTION:  Used to update Account Number when Edit supplier account

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_Id				Int
@Account_Number			Varchar(50)
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	dbo.Variance_Log_Account_Number_UPD 1649224, 'New Account'
	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	12/02/2009  Created
HG	1/20/2010	Unnecessary Subselect on Variance_Log table was removed.

******/  


CREATE PROCEDURE dbo.Variance_Log_Account_Number_UPD	    
	@Account_Id		INT,
	@Account_Number VARCHAR(50)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	UPDATE Variance_Log
		SET Account_Number = @Account_Number
	WHERE
		Account_Id = @Account_Id
		AND Closed_Dt IS NULL
		AND Is_Failure = 1
			
END
GO
GRANT EXECUTE ON  [dbo].[Variance_Log_Account_Number_UPD] TO [CBMSApplication]
GO
