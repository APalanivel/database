SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                    
      dbo.Site_Ref_Number_Search_By_Client                 
                  
Description:                  
                  
            smart search for Brand Name.                  
                  
 Input Parameters:                  
    Name                    DataType                Default Description                  
-------------------------------------------------------------------------------------------                  
 @Client_Id     INT  
 @Sitegroup_Id    INT                          
 @Keyword     VARCHAR(200)                            
 @Start_Index    INT                       
 @End_Index     INT              
                 
 Output Parameters:                  
    Name                                        DataType                Default Description                  
-------------------------------------------------------------------------------------------                  
                  
                      
 Usage Examples:                  
-------------------------------------------------------------------------------------------                  
                 
	EXEC dbo.Site_Ref_Number_Search_By_Client 235  
	EXEC dbo.Site_Ref_Number_Search_By_Client 235,null,null,'00'
	EXEC dbo.Site_Ref_Number_Search_By_Client 235,null,1892,'00'
	EXEC dbo.Site_Ref_Number_Search_By_Client 235,null,1892,null

	EXEC dbo.Site_Sel_By_Client 235,null,null,1,100,null,'0001'
                
                  
Author Initials:                      
 Initials        Name                      
-------------------------------------------------------------------------------------------                  
 RKV             Ravi Kumar Vegesna                
   
  
                  
 Modifications:                  
	Initials	Date		Modification                  
-------------------------------------------------------------------------------------------                  
	RKV			2014-10-29  Created        
   
                         
******/           
  
CREATE PROCEDURE [dbo].[Site_Ref_Number_Search_By_Client]
      ( 
       @Client_Id INT
      ,@Sitegroup_Id INT = NULL
      ,@Site_Id INT = NULL
      ,@Keyword VARCHAR(200) = NULL
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN    
   
   
      SET NOCOUNT ON;    
      
      WITH  cte_SiteList
              AS ( SELECT TOP ( @End_Index )
                        ch.Site_Reference_Number
                       ,row_number() OVER ( ORDER BY ch.Site_Reference_Number ASC ) AS Record_Number
                   FROM
                        Core.Client_Hier ch
                        INNER JOIN dbo.Security_Role_Client_Hier srch
                              ON srch.Client_Hier_Id = ch.Client_Hier_Id
                   WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.site_id > 0
                        AND ( @Keyword IS NULL
                              OR ch.Site_Reference_Number LIKE '%' + @Keyword + '%' )
                        AND ( @Sitegroup_Id IS NULL
                              OR EXISTS ( SELECT
                                                1
                                          FROM
                                                dbo.Sitegroup_Site ss
                                          WHERE
                                                ss.Site_id = ch.Site_Id
                                                AND ss.Sitegroup_id = @Sitegroup_Id ) )
                        AND ( ch.Site_Reference_Number IS NOT NULL
                              AND ch.Site_Reference_Number <> '' )
                        AND ( @Site_Id IS NULL
                              OR ch.Site_Id = @Site_Id )
                   GROUP BY
                        ch.Site_Reference_Number)
            SELECT
                  sl.Site_Reference_Number
                 ,sl.Record_Number
            FROM
                  cte_SiteList sl
            WHERE
                  sl.Record_Number BETWEEN @Start_Index
                                   AND     @End_Index
            ORDER BY
                  sl.Site_Reference_Number    
  
                
END  


;
GO
GRANT EXECUTE ON  [dbo].[Site_Ref_Number_Search_By_Client] TO [CBMSApplication]
GO
