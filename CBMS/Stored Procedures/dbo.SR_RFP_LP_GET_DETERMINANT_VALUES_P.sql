
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.SR_RFP_LP_GET_DETERMINANT_VALUES_P

DESCRIPTION:

Used to select the all determinant values

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@determinant_id		 int
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
Exec SR_RFP_LP_GET_DETERMINANT_VALUES_P 191440

--TK5 example
Exec SR_RFP_LP_GET_DETERMINANT_VALUES_P 177100

select top 10 * from sr_rfp_lp_determinant_values ORDER BY sr_rfp_lp_determinant_values_Id DESC

AUTHOR	INITIALS:
	BCH			Balaraju
------------------------------------------------------------

MODIFICATIONS

 Initials	Date		Modification
------------------------------------------------------------
 BCH		2011-10-21  Added Data_Source_Cd column to select list
						Removed nolock hints
						Unused @user_id VARCHAR(10) and @session_id VARCHAR(20) parameters removed

******/
CREATE PROCEDURE dbo.SR_RFP_LP_GET_DETERMINANT_VALUES_P @determinant_id INT
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            value.sr_rfp_lp_determinant_values_id
           ,value.month_identifier
           ,value.lp_value
           ,value.meter_reading_from_date
           ,value.meter_reading_to_date
           ,rt.entity_name AS reading_type
           ,value.Data_Source_Cd
      FROM
            dbo.sr_rfp_lp_determinant_values value
            INNER JOIN dbo.entity rt
                  ON rt.ENTITY_ID = value.READING_TYPE_ID
      WHERE
            value.sr_rfp_load_profile_determinant_id = @determinant_id
      ORDER BY
            value.month_identifier

END
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_DETERMINANT_VALUES_P] TO [CBMSApplication]
GO
