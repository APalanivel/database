SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CONTRACT_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contract_number	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_RFP_GET_CONTRACT_TYPE_P
@contract_number varchar(20)
AS
set nocount on
	select e.entity_name 
	from contract con, entity e
	where  e.entity_id  = con.contract_type_id
	and con.ed_contract_number = @contract_number
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CONTRACT_TYPE_P] TO [CBMSApplication]
GO
