SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Sr_Pricing_Product_Exists       
              
Description:              
			This sproc checks if the PRODUCT_NAME is already exists      
              
 Input Parameters:              
    Name						DataType		Default		Description                
----------------------------------------------------------------------------------------                
	@Product_Name				VARCHAR(100)
    @Sr_Pricing_Product_Id		INT				NULL
    
 Output Parameters:                    
    Name						DataType		Default		Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   SELECT * FROM dbo.SR_PRICING_PRODUCT
   
   Exec dbo.Sr_Pricing_Product_Exists 'Test_Global_Sourcing_Phase2',Null,290
   
   Exec dbo.Sr_Pricing_Product_Exists 'Test_Global_Sourcing_Phase2',Null,291


   Exec dbo.Sr_Pricing_Product_Exists 'Block Price - Vendor Collected',Null,290
   
   Exec dbo.Sr_Pricing_Product_Exists 'Block Price - Vendor Collected',Null,291


   Exec dbo.Sr_Pricing_Product_Exists 'Percentage of Usage Block',32,290
   
   Exec dbo.Sr_Pricing_Product_Exists 'Percentage of Usage Block',32,291

	
Author Initials:              
	Initials		Name              
----------------------------------------------------------------------------------------                
	RR				Raghu Reddy               
	
Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-09-29		Global Sourcing - Phase2 - Created  
             
******/ 

CREATE PROCEDURE [dbo].[Sr_Pricing_Product_Exists]
      ( 
       @Product_Name VARCHAR(100)
      ,@Sr_Pricing_Product_Id INT = NULL
      ,@Commodity_Type_Id INT )
AS 
BEGIN
      SET NOCOUNT ON;
            
      SELECT
            spp.SR_PRICING_PRODUCT_ID
      FROM
            dbo.SR_PRICING_PRODUCT spp
      WHERE
            ( ( @Sr_Pricing_Product_Id IS NULL
                AND spp.PRODUCT_NAME = @Product_Name
                AND spp.COMMODITY_TYPE_ID = @Commodity_Type_Id )
              OR ( spp.SR_PRICING_PRODUCT_ID != @Sr_Pricing_Product_Id
                   AND spp.PRODUCT_NAME = @Product_Name
                   AND spp.COMMODITY_TYPE_ID = @Commodity_Type_Id ) )
                 
END;
;

GO
GRANT EXECUTE ON  [dbo].[Sr_Pricing_Product_Exists] TO [CBMSApplication]
GO
