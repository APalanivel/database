SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*********   
NAME:  dbo.Get_Minimum_Analagous_Account_Count  
 
DESCRIPTION:  Used to load all the baselines by parameter

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Parameter_Id			int     
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   
	Get_Minimum_Analagous_Account_Count 4
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
AP	Arunkumar Palanivel       


Initials Date  Modification  
------------------------------------------------------------  
AP		Ar 17,2020 new procedure created to get minimum analagous count


******/
CREATE PROCEDURE [dbo].[Get_Minimum_Analagous_Account_Count]
      (
      @Account_id   INT
    , @commodity_id INT )
AS
      BEGIN

            SET NOCOUNT ON;

            SELECT      TOP ( 1 )
                        vespv.Param_Value AS Min_Count
            FROM        dbo.Variance_Consumption_Extraction_Service_Param_Value vespv
                        JOIN
                        dbo.Variance_Extraction_Service_Param vesp
                              ON vesp.Variance_Extraction_Service_Param_Id = vespv.Variance_Extraction_Service_Param_Id
                        JOIN
                        dbo.Variance_Consumption_Level vcl
                              ON vcl.Variance_Consumption_Level_Id = vespv.Variance_Consumption_Level_Id
                        JOIN
                        dbo.Account_Variance_Consumption_Level avcl
                              ON avcl.Variance_Consumption_Level_Id = vcl.Variance_Consumption_Level_Id
                                 AND vesp.Param_Name = 'Analogous_Account_Min_Account_Required'
                                 AND vcl.Commodity_Id = @commodity_id
                                 AND avcl.ACCOUNT_ID = @Account_id;

      END;
GO
GRANT EXECUTE ON  [dbo].[Get_Minimum_Analagous_Account_Count] TO [CBMSApplication]
GO
