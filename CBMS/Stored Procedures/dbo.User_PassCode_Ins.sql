SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  

 NAME: DV2.dbo.User_PassCode_Ins  
 
 DESCRIPTION: 
	To Insert the Encrypted Passcode value into User_PassCode table for the given user.

 INPUT PARAMETERS:  
 Name				DataType		Default Description
------------------------------------------------------------
 @User_Info_Id		INT
 @PassCode          NVARCHAR(128)
 @Active_From_Dt    DATETIME		NULL
 @Created_By_Id     INT
 
 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

 USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
	DECLARE @Dt DATETIME = GETDATE()
		EXEC dbo.User_PassCode_Ins 49,'jscott',@Dt,16
	ROLLBACK TRAN

 AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 PNR		Pandarinath
 HG			Harihara Suthan G
 
 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 PNR		10/12/2010		Created as a part of DV/SV Passowrd expiration.
 HG			11/01/2010		Script added to delete/readjust the passcode entries which crossed the reuse limit set.
							Transactions added to the script.
******/

CREATE PROCEDURE dbo.User_PassCode_Ins
(
	 @User_Info_Id		INT
	,@PassCode          NVARCHAR(128)
	,@Active_From_Dt    DATETIME		= NULL
	,@Created_By_Id     INT
)
AS
BEGIN

    SET NOCOUNT ON ;

    SET @Active_From_Dt = ISNULL(@Active_From_Dt, GETDATE())

	BEGIN TRY
		BEGIN TRAN
		
			INSERT INTO dbo.User_PassCode
			(
				 User_Info_Id
				,PassCode
				,Active_From_Dt
				,Created_By_Id
			)
			VALUES
		   (
   				 @User_Info_Id
				,@PassCode
				,@Active_From_Dt
				,@Created_By_Id
			)

			EXEC dbo.User_Info_Upd_Failed_Login_Attempts @User_Info_Id, 0

			EXEC dbo.User_PassCode_Del_Expired_Entries @User_Info_Id

		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN
		END

		EXEC dbo.usp_RethrowError

	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[User_PassCode_Ins] TO [CBMSApplication]
GO
