SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE       PROCEDURE [dbo].[cbmsSsoUBMDDSM_GetForClient]
	( @MyAccountId int
	, @client_id int
	
	)
AS
BEGIN

  

         select c.client_id 
		, ub.ubm_id
		, ub.ubm_name
	     from client c
	     join ubm_service us on us.ubm_service_id = c.ubm_service_id
	     join ubm ub on ub.ubm_id = us.ubm_id
 	    where c.client_id = @client_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoUBMDDSM_GetForClient] TO [CBMSApplication]
GO
