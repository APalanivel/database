SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[cbmsAccount_MakeNotManagedForSite]
	( @MyAccountId int
	, @site_id int
	)
AS
BEGIN

	insert into do_not_track
		( user_info_id, account_id, reason_type_id, account_number
		, client_name, client_city, state_id, vendor_name, date_added
		)
	   select distinct @MyAccountId user_info_id
		, a.account_id
		, 314
		, isNull(a.account_number, 'Blank for ' + v.vendor_name)
		, cl.client_name
		, ad.city
		, ad.state_id
		, v.vendor_name
		, getdate()
	     from site s
	     join vwAccountMeter vam on vam.site_id = s.site_id
	     join account a on a.account_id = vam.account_id
	     join division d on d.division_id = s.division_id
	     join client cl on cl.client_id = d.client_id
	     join address ad on ad.address_id = s.primary_address_id
	     join vendor v on v.vendor_id = a.vendor_id
  left outer join do_not_track dnt on dnt.account_id = a.account_id
	    where s.site_id = @site_id
	      and dnt.do_not_track_id is null


END






















GO
GRANT EXECUTE ON  [dbo].[cbmsAccount_MakeNotManagedForSite] TO [CBMSApplication]
GO
