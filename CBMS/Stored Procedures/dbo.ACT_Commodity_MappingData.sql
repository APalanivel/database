SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.[ACT_Commodity_MappingData]
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				 DataType  Default      Description    
---------------------------------------------------------------------------                  
@ACTCommmodityID	VARCHAR(255)
@CBMSCommodityID    VARCHAR(255)
OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
  ACT_Commodity_MappingData 0, 0   
 



AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 NJ		   Naga Jyothi
   
MODIFICATIONS    
    
 Initials	Date		Modification    
-------------------------------------------------------------------------------------    
 NJ		   2019-07-06	Created	for SE2017-733 ACT Commodity Mapping within CBMS
******/
CREATE PROCEDURE [dbo].[ACT_Commodity_MappingData]
    (
        @ACTCommmodityID VARCHAR(255)
        , @CBMSCommodityID VARCHAR(255)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            ac.ACT_Commodity_XName
            , c.Commodity_Name
            , ac.ACT_Commodity_Id
            , c.Commodity_Id
            , cacm.Commodity_ACT_Commodity_Map_Id
        FROM
            dbo.Commodity_ACT_Commodity_Map cacm
            INNER JOIN dbo.Commodity c
                ON cacm.Commodity_Id = c.Commodity_Id
            INNER JOIN dbo.ACT_Commodity ac
                ON cacm.ACT_Commodity_Id = ac.ACT_Commodity_Id
        WHERE
            1 = (CASE WHEN @ACTCommmodityID = 0 THEN 1
                     ELSE CASE WHEN cacm.ACT_Commodity_Id = @ACTCommmodityID THEN 1
                              ELSE 0
                          END
                 END)
            AND 1 = (CASE WHEN @CBMSCommodityID = 0 THEN 1
                         ELSE CASE WHEN cacm.Commodity_Id = @CBMSCommodityID THEN 1
                                  ELSE 0
                              END
                     END)
        ORDER BY
            ac.ACT_Commodity_XName;

    END;
GO
GRANT EXECUTE ON  [dbo].[ACT_Commodity_MappingData] TO [CBMSApplication]
GO
