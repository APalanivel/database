SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Hedge_Detail_Del]  

DESCRIPTION: It Deletes Hedge Detail for Selected Hedge Detail Id.     
      
INPUT PARAMETERS:          
	NAME				DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Hedge_Detail_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Hedge_Detail_Del 2867
  Rollback Tran
SELECT
AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Hedge_Detail_Del
    (
      @Hedge_Detail_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.HEDGE_DETAIL 
	WHERE
		HEDGE_DETAIL_ID = @Hedge_Detail_Id

END
GO
GRANT EXECUTE ON  [dbo].[Hedge_Detail_Del] TO [CBMSApplication]
GO
