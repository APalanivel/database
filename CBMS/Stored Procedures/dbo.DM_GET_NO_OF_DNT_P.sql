SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.DM_GET_NO_OF_DNT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(20)	          	
	@sessionId     	varchar(20)	          	
	@fromDate      	varchar(20)	          	
	@toDate        	varchar(20)	          	
	@ubmId         	int       	          	
	@statusId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.DM_GET_NO_OF_DNT_P '1','1','','',1,1
	EXEC dbo.DM_GET_NO_OF_DNT_P '1','1','1/1/2010','9/1/2010',1,1

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/23/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.
	
******/

CREATE PROCEDURE dbo.DM_GET_NO_OF_DNT_P
    @userId		VARCHAR(20),
    @sessionId	VARCHAR(20),
    @fromDate	VARCHAR(20),
    @toDate		VARCHAR(20),
    @ubmId		INT,
    @statusId	INT
AS
BEGIN

	SET NOCOUNT ON

    DECLARE @selectClause VARCHAR(8000)
    DECLARE @fromClause VARCHAR(8000)
    DECLARE @whereClause VARCHAR(8000)
    DECLARE @groupByClause VARCHAR(8000)
    DECLARE @orderByClause VARCHAR(8000)
    DECLARE @selectClauseDetailed VARCHAR(8000)
    DECLARE @groupByClauseDetailed VARCHAR(8000)
    DECLARE @orderByClauseDetailed VARCHAR(8000)
    DECLARE @SQLStatement VARCHAR(8000)


    SELECT  @orderByClause = 'ubm.UBM_NAME,SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+ ''-'' + SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3)'
	select @orderByClauseDetailed = 'ubm.UBM_NAME,CONVERT (Varchar(12), masterLog.END_DATE, 101)'


    SELECT  @selectClause = 'ubm.UBM_NAME ,
				SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3) MONTH_IDENTIFIER,				
				COUNT(distinct invoice.UBM_INVOICE_ID)	NO_OF_INVOICES
			       '	
    SELECT  @selectClauseDetailed = 'ubm.UBM_NAME ,
					CONVERT (Varchar(12), masterLog.END_DATE, 101)  MONTH_IDENTIFIER,
					COUNT(distinct invoice.UBM_INVOICE_ID)	NO_OF_INVOICES
				       '	


    SELECT  @fromClause = 'UBM_INVOICE invoice,
				UBM_BATCH_MASTER_LOG masterLog,
				UBM ubm,
				CU_INVOICE cuInvoice
			     '

    SELECT  @groupByClause = ' SUBSTRING(DATENAME(MM, masterLog.END_DATE), 0, 4)+''-''+SUBSTRING(CONVERT(VARCHAR(4), DATEPART(YYYY, masterLog.END_DATE)), 3, 3),
				  ubm.UBM_NAME
			        '
    SELECT  @groupByClauseDetailed = ' CONVERT (Varchar(12), masterLog.END_DATE, 101),ubm.UBM_NAME' 

    SELECT  @whereClause = ' invoice.UBM_BATCH_MASTER_LOG_ID = masterLog.UBM_BATCH_MASTER_LOG_ID AND
		                masterLog.UBM_ID = ubm.UBM_ID AND
				invoice.IS_QUARTERLY = 0 AND
				cuInvoice.UBM_INVOICE_ID = invoice.UBM_INVOICE_ID AND
				cuInvoice.CBMS_IMAGE_ID = invoice.CBMS_IMAGE_ID AND
				cuInvoice.IS_DNT=1		      
			      '

    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN
            IF ( @fromDate = @toDate ) 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND CONVERT(Varchar(12), masterLog.END_DATE,101) = '
                            + '''' + CONVERT(VARCHAR(12), @fromDate, 101) + ''''
                END
            ELSE 
                BEGIN
                    SELECT  @whereClause = @whereClause
                            + ' AND masterLog.END_DATE BETWEEN ' + ''''
                            + CONVERT(VARCHAR(12), @fromDate, 101) + + '''' + ' AND ' + ''''
                            + CONVERT(VARCHAR(12), @toDate, 101) + ''''
                END
        END 


    IF ( @fromDate IS NOT NULL
         AND @fromDate <> ''
       )
        AND ( @toDate IS NULL
              OR @toDate = ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE >= '+ ''''
                    + CONVERT(VARCHAR(12), @fromDate, 101) + ''''
        END 


    IF ( @fromDate IS NULL
         OR @fromDate = ''
       )
        AND ( @toDate IS NOT NULL
              AND @toDate <> ''
            ) 
        BEGIN

            SELECT  @whereClause = @whereClause
                    + ' AND masterLog.END_DATE <= '+ ''''
                    + CONVERT(VARCHAR(12), @toDate, 101) + ''''
        END 

    IF @ubmId > 0 
        BEGIN
            SELECT  @whereClause = @whereClause + ' AND ubm.UBM_ID = '
                    + STR(@ubmId) 
        END 




    IF @statusId = 1 
        BEGIN
            SELECT  @SQLStatement = 'SELECT ' + @selectClause + ' FROM '
                    + @fromClause + ' WHERE ' + @whereClause + ' GROUP BY '
                    + @groupByClause + 'order by ' + @orderByClause
        END

    IF @statusId = 2 
        BEGIN
            SELECT  @SQLStatement = 'SELECT ' + @selectClauseDetailed
                    + ' FROM ' + @fromClause + ' WHERE ' + @whereClause
                    + ' GROUP BY ' + @groupByClauseDetailed + 'order by '
                    + @orderByClauseDetailed 
        END

    EXEC ( @SQLStatement )

END
GO
GRANT EXECUTE ON  [dbo].[DM_GET_NO_OF_DNT_P] TO [CBMSApplication]
GO
