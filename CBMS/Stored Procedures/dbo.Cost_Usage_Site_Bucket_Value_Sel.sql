
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 CBMS.dbo.Cost_Usage_Site_Bucket_Value_Sel  
   
 Description:    
   
 Input Parameters:    
    Name				DataType   Default   Description
------------------------------------------------------------------------
    @Bucket_List		AS dbo.Cost_Usage_Bucket    READONLY
    @Report_Year		INT
    @Client_Id			INT    NULL
    @Division_Id		INT    NULL
    @Site_Client_Hier_Id INT    NULL
    @Region_Id			INT    NULL
    @State_Id			INT    NULL
    @Site_Type_Id		INT    NULL
    @StartIndex			INT    1
    @EndIndex			INT    2147483647

 Output Parameters:    
 Name					Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------   
   
DECLARE @Bucket_List1 AS Cost_Usage_Bucket  
INSERT INTO @Bucket_List1  
VALUES (290, 'Total Usage', 'Determinant', 12)  
    ,(290, 'Total Cost', 'Charge', 3)  
EXECUTE dbo.Cost_Usage_Site_Bucket_Value_Sel @Bucket_List1, 2011, 10069, NULL, NULL, NULL, NULL, NULL,1,100  


DECLARE @Bucket_List1 AS Cost_Usage_Bucket  
INSERT INTO @Bucket_List1  
VALUES (290, 'Total Usage', 'Determinant', 12)  
    ,(290, 'Total Cost', 'Charge', 3)  
EXECUTE dbo.Cost_Usage_Site_Bucket_Value_Sel @Bucket_List1, 2011, 235, 258, NULL, NULL, NULL, NULL,1,50  


DECLARE @Bucket_List1 AS Cost_Usage_Bucket  
INSERT INTO @Bucket_List1  
VALUES (290, 'Total Usage', 'Determinant', 12)  
    ,(290, 'Total Cost', 'Charge', 3)  
EXECUTE dbo.Cost_Usage_Site_Bucket_Value_Sel @Bucket_List1, 2011,NULL,NULL,NULL, NULL, NULL, NULL,1,20  

DECLARE @Bucket_List1 AS Cost_Usage_Bucket
INSERT INTO @Bucket_List1
VALUES (291, 'Total Cost', 'Charge', 3)
    ,(290, 'Total Cost', 'Charge', 3)
    ,(291, 'Taxes', 'Charge', 3)
    ,(290, 'Taxes', 'Charge', 3)
EXECUTE dbo.Cost_Usage_Site_Bucket_Value_Sel @Bucket_List1, 2012, NULL, NULL, 1134, NULL, NULL, 250,1,100

SELECT  * FROM Core.Client_Hier WHERE client_Id = 10069   
SELECT * FROM dbo.ENTITY WHERE ENTITY_NAME IN ('Industrial','Commercial')  
   
Author Initials:    
 Initials	Name  
------------------------------------------------------------  
 AP			Athmaram Pabbathi  
 RR			Raghu Reddy  
 BCH		Balaraju Chalumuri  
 AKR		Ashok Kumar Raju
 HG			Harihara Suthan G

 Modifications :    
 Initials	Date   Modification    
------------------------------------------------------------    
 AP			10/10/2011  Created and replaced flowlling SPs as a part of Addtl Data changes  
							- dbo.cbmsCostUsageSite_LoadForSite  
 AP			2012-03-15  Added Client_Hier_Id & Data_Source_Cd in the result set  
 AP			2012-04-15  Replaced @Site_Id with @Site_Client_Hier_Id  
 RR			2012-06-07  Modified the usage example  
 BCH		2012-08-06  (For MAIN -1295) Added StartIndex and EndIndex input parameters for Pagination and Pivoted rows for Performance issue.  
						-- Site table join removed and used Site_Type_Name column to filter the Site_Type_id  
 AKR		2012-08-15  Modified the logic to fill @Service_Month
 HG			2012-08-16	Code_Value column removed from group by clause in the final select statement.
						Removed the Commodity_Name column from the output as app doesn't need it and changed the Case statement which used to return the data source cd as it should be at bucket level instead of commodity level
 
******/
CREATE PROCEDURE [dbo].Cost_Usage_Site_Bucket_Value_Sel
      ( 
       @Bucket_List AS dbo.Cost_Usage_Bucket READONLY
      ,@Report_Year INT
      ,@Client_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Site_Client_Hier_Id INT = NULL
      ,@Region_Id INT = NULL
      ,@State_Id INT = NULL
      ,@Site_Type_Id INT = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN      
      
      SET NOCOUNT ON      
      
      DECLARE
            @Start_Month DATE
           ,@End_Month DATE
           ,@Usd_Currency_Unit_Id INT
           ,@Site_Type_Name VARCHAR(200)     
                
      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT )      
    
      CREATE TABLE #Client_Dtl
            ( 
             Client_Name VARCHAR(200)
            ,Division_Name VARCHAR(200)
            ,City VARCHAR(200)
            ,Address_Line1 VARCHAR(200)
            ,State_Name VARCHAR(20)
            ,Region_Name VARCHAR(200)
            ,Site_Id INT
            ,Client_Hier_Id INT
            ,Client_Currency_Group_Id INT
            ,Total_Rows INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id ) )      
      
           
      SELECT
            @Usd_Currency_Unit_Id = CU.Currency_Unit_Id
      FROM
            dbo.Currency_Unit CU
      WHERE
            CU.Symbol = 'USD'      
      
      SELECT
            @Site_Type_Name = ENTITY_NAME
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_ID = @Site_Type_Id    
      
      INSERT      INTO @Service_Month
                  ( 
                   Service_Month
                  ,Month_Number )
                  SELECT
                        dd.Date_D
                       ,row_number() OVER ( ORDER BY dd.Date_D )
                  FROM
                        ( SELECT
                              dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE)) Start_month
                          FROM
                              Core.Client_Hier ch
                          WHERE
                              ch.Client_ID = @Client_Id
                              AND ch.Sitegroup_Id = 0
                              AND ch.Site_Id = 0
                          UNION ALL
                          SELECT
                              cast('1/1/' + cast(@report_year AS VARCHAR(4)) AS VARCHAR(10))
                          WHERE
                              @client_id IS NULL ) X
                        CROSS JOIN meta.Date_Dim dd
                  WHERE
                        dd.Date_D BETWEEN x.Start_month
                                  AND     dateadd(MONTH, -1, ( dateadd(YEAR, 1, X.Start_Month) ))  
  
      SELECT
            @Start_Month = min(sm.Service_Month)
           ,@End_Month = max(sm.Service_Month)
      FROM
            @Service_Month sm ;    
                    
      WITH  CTE_Client_Detail
              AS ( SELECT
                        ch.Client_Name
                       ,ch.Sitegroup_Name AS division_name
                       ,ch.city
                       ,ch.Site_Address_Line1 AS address_line1
                       ,ch.State_Name
                       ,ch.Region_Name
                       ,ch.Site_Id
                       ,ch.Client_Hier_Id
                       ,ch.Client_Currency_Group_Id
                       ,row_number() OVER ( ORDER BY ch.Client_Name , ch.Sitegroup_Name , ch.City ) Row_Num
                       ,count(1) OVER ( ) Total_Rows
                   FROM
                        Core.Client_Hier ch
                   WHERE
                        ( @Client_Id IS NULL
                          OR CH.Client_Id = @Client_Id )
                        AND ( @Division_Id IS NULL
                              OR CH.Sitegroup_Id = @Division_Id )
                        AND ( @Site_Client_Hier_Id IS NULL
                              OR CH.Client_Hier_Id = @Site_Client_Hier_Id )
                        AND ( @Site_Type_Id IS NULL
                              OR ch.Site_Type_Name = @Site_Type_Name )
                        AND ( @State_Id IS NULL
                              OR CH.State_Id = @State_Id )
                        AND ( @Region_Id IS NULL
                              OR CH.Region_ID = @Region_Id )
                        AND CH.Client_Not_Managed = 0
                        AND CH.Site_Not_Managed = 0
                        AND CH.Site_Closed = 0
                        AND CH.Division_Not_Managed = 0
                        AND CH.Site_Id > 0
                   GROUP BY
                        ch.Client_Name
                       ,ch.Sitegroup_Name
                       ,ch.city
                       ,ch.Site_Address_Line1
                       ,ch.State_Name
                       ,ch.Region_Name
                       ,ch.City
                       ,ch.Site_Id
                       ,ch.Client_Hier_Id
                       ,ch.Client_Currency_Group_Id)
            INSERT      INTO #Client_Dtl
                        ( 
                         Client_Name
                        ,Division_Name
                        ,City
                        ,Address_Line1
                        ,State_Name
                        ,Region_Name
                        ,Site_Id
                        ,Client_Hier_Id
                        ,Client_Currency_Group_Id
                        ,Total_Rows )
                        SELECT
                              Client_Name
                             ,Division_Name
                             ,City
                             ,Address_Line1
                             ,State_Name
                             ,Region_Name
                             ,Site_Id
                             ,Client_Hier_Id
                             ,Client_Currency_Group_Id
                             ,Total_Rows
                        FROM
                              CTE_Client_Detail
                        WHERE
                              Row_Num BETWEEN @StartIndex AND @EndIndex    
                      
      SELECT
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City
           ,CDtl.Address_Line1
           ,CDtl.State_Name
           ,CDtl.Region_Name
           ,CDtl.City AS Site_Name
           ,CDtl.Site_Id
           ,CDtl.Client_Hier_Id
           ,@Start_Month AS Fiscal_Start
           ,@End_Month AS Fiscal_End
           ,sum(case WHEN CUSD.Month_Number = 1
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 1
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month1
           ,sum(case WHEN CUSD.Month_Number = 2
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 2
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month2
           ,sum(case WHEN CUSD.Month_Number = 3
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 3
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month3
           ,sum(case WHEN CUSD.Month_Number = 4
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 4
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month4
           ,sum(case WHEN CUSD.Month_Number = 5
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 5
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month5
           ,sum(case WHEN CUSD.Month_Number = 6
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 6
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month6
           ,sum(case WHEN CUSD.Month_Number = 7
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 7
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month7
           ,sum(case WHEN CUSD.Month_Number = 8
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 8
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month8
           ,sum(case WHEN CUSD.Month_Number = 9
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 9
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month9
           ,sum(case WHEN CUSD.Month_Number = 10
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 10
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month10
           ,sum(case WHEN CUSD.Month_Number = 11
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 11
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month11
           ,sum(case WHEN CUSD.Month_Number = 12
                          AND CUSD.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                     WHEN CUSD.Month_Number = 12
                          AND CUSD.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                END) AS Month12
           ,CUSD.Bucket_Name
           ,CUSD.Bucket_Type
           ,case max(case WHEN cusd.Month_Number = 1
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 1
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month1_Data_Scource_Code

           ,case max(case WHEN cusd.Month_Number = 2
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 2
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month2_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 3
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 3
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month3_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 4
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 4
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month4_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 5
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 5
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month5_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 6
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 6
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month6_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 7
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 7
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month7_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 8
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 8
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month8_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 9
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 9
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month9_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 10
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 10
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month10_Data_Scource_Code
           ,case max(case WHEN cusd.Month_Number = 11
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 11
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month11_Data_Scource_Code            
           ,case max(case WHEN cusd.Month_Number = 12
                               AND dsc.Code_Value IN ( 'CBMS', 'DE' ) THEN 1
                          WHEN cusd.Month_Number = 12
                               AND dsc.Code_Value = 'Invoice' THEN 0
                     END)
              WHEN 1 THEN 'CBMS'	-- Has Manual Data
              WHEN 0 THEN 'Invoice' -- Invoice Data
            END AS Month12_Data_Scource_Code
           ,cdtl.Total_Rows
      FROM
            #Client_Dtl CDtl
            LEFT OUTER JOIN ( SELECT
                                    CUSD.Client_Hier_Id
                                   ,CUSD.Service_Month
                                   ,BM.Bucket_Name
                                   ,bkt_type.Code_Value AS Bucket_Type
                                   ,CUSD.UOM_Type_Id
                                   ,CUSD.Currency_Unit_Id
                                   ,CUSD.Bucket_Value
                                   ,BL.Bucket_UOM_Id
                                   ,BM.Default_Uom_Type_Id
                                   ,cusd.Data_Source_Cd
                                   ,sm.Month_Number
                              FROM
                                    #Client_Dtl cd
                                    INNER JOIN dbo.Cost_Usage_Site_Dtl CUSD
                                          ON cusd.Client_Hier_Id = cd.Client_Hier_Id
                                    INNER JOIN @Service_Month Sm
                                          ON Sm.Service_Month = CUSD.Service_Month
                                    INNER JOIN dbo.Bucket_Master BM
                                          ON BM.Bucket_Master_Id = CUSD.Bucket_Master_Id
                                    INNER JOIN @Bucket_List BL
                                          ON BL.Bucket_Name = BM.Bucket_Name
                                             AND BL.Commodity_Id = BM.Commodity_Id
                                    INNER JOIN dbo.Code bkt_type
                                          ON bkt_type.Code_Id = BM.Bucket_Type_Cd
                              GROUP BY
                                    CUSD.Client_Hier_Id
                                   ,CUSD.Service_Month
                                   ,BM.Bucket_Name
                                   ,bkt_type.Code_Value
                                   ,CUSD.UOM_Type_Id
                                   ,CUSD.Currency_Unit_Id
                                   ,CUSD.Bucket_Value
                                   ,BL.Bucket_UOM_Id
                                   ,BM.Default_Uom_Type_Id
                                   ,cusd.Data_Source_Cd
                                   ,sm.Month_Number ) AS CUSD
                  ON CUSD.Client_Hier_Id = CDtl.Client_Hier_Id
            LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UOMConv
                  ON UOMConv.Base_Unit_Id = CUSD.UOM_Type_Id
                     AND UOMConv.Converted_Unit_Id = isnull(CUSD.Bucket_UOM_Id, CUSD.Default_Uom_Type_Id)
                     AND CUSD.Bucket_Type = 'Determinant'
            LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                  ON CurConv.Base_Unit_Id = CUSD.Currency_Unit_Id
                     AND CurConv.Currency_Group_Id = CDtl.Client_Currency_Group_Id
                     AND CurConv.Conversion_Date = CUSD.Service_Month
                     AND CurConv.Converted_Unit_Id = isnull(CUSD.Bucket_UOM_Id, @Usd_Currency_Unit_Id)
                     AND CUSD.Bucket_Type = 'Charge'
            LEFT OUTER JOIN dbo.Code dsc
                  ON dsc.Code_Id = cusd.Data_Source_Cd
      GROUP BY
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City
           ,CDtl.Address_Line1
           ,CDtl.State_Name
           ,CDtl.Region_Name
           ,CDtl.Site_Id
           ,CDtl.Client_Hier_Id
           ,CUSD.Bucket_Name
           ,CUSD.Bucket_Type
           ,cdtl.Total_Rows
      ORDER BY
            CDtl.Client_Name
           ,CDtl.Division_Name
           ,CDtl.City      
      
      DROP TABLE #Client_Dtl     
 
END  

;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Bucket_Value_Sel] TO [CBMSApplication]
GO
