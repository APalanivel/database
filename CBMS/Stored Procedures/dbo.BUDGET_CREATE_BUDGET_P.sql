
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:
	dbo.BUDGET_CREATE_BUDGET_P

DESCRIPTION:


INPUT PARAMETERS:
Name					DataType		Default	Description
------------------------------------------------------------
@user_id				varchar(10)
@clientId				int
@commodityId			int
@budgetStartYear		int
@budgetStartMonth		int
@budgetName				varchar(200)
@dueDate				datetime
@Analyst_Queue_Display_Cd int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
@budgetId			int				out

USAGE EXAMPLES:
------------------------------------------------------------
BEGIN TRAN
DECLARE @Budget_Id INT
	EXEC BUDGET_CREATE_BUDGET_P 49, 11231, 290, 2010, 01, 'Testing', '2012-01-01', 49, @Budget_Id OUT
	SELECT * FROM dbo.Budget WHERE Budget_Id = @Budget_Id
ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 CPE			2014-03-11	Made Account_Ids as CSV
							
******/

CREATE PROCEDURE dbo.BUDGET_CREATE_BUDGET_P
      @user_id VARCHAR(10)
     ,@clientId INT
     ,@commodityId INT
     ,@budgetStartYear INT
     ,@budgetStartMonth INT
     ,@budgetName VARCHAR(200)
     ,@dueDate DATETIME
     ,@Analyst_Queue_Display_Cd INT
     ,@budgetId INT OUT
AS 
BEGIN
      SET NOCOUNT ON

      DECLARE @budgetStatusId INT
      SELECT
            @budgetStatusId = ENTITY_ID
      FROM
            dbo.ENTITY e
      WHERE
            e.ENTITY_DESCRIPTION = 'BUDGET_STATUS'
            AND e.ENTITY_NAME = 'Original'

      SELECT
            @Analyst_Queue_Display_Cd = c.Code_Id
      FROM
            dbo.Code AS c
            JOIN dbo.Codeset AS cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            c.Code_Value = 'Yes - Rates and Sourcing'
            AND cs.Codeset_Name = 'IncludeInAnalystQueue'
            AND @Analyst_Queue_Display_Cd IS NULL


      BEGIN TRY
            BEGIN TRAN

            INSERT      INTO dbo.BUDGET
                        ( 
                         CLIENT_ID
                        ,COMMODITY_TYPE_ID
                        ,BUDGET_START_YEAR
                        ,BUDGET_START_MONTH
                        ,BUDGET_NAME
                        ,DUE_DATE
                        ,DATE_INITIATED
                        ,INITIATED_BY
                        ,STATUS_TYPE_ID
                        ,Analyst_Queue_Display_Cd )
            VALUES
                        ( 
                         @clientId
                        ,@commodityId
                        ,@budgetStartYear
                        ,@budgetStartMonth
                        ,@budgetName
                        ,@dueDate
                        ,GETDATE()
                        ,@user_id
                        ,@budgetStatusId
                        ,@Analyst_Queue_Display_Cd )

            SELECT
                  @budgetId = SCOPE_IDENTITY()

            INSERT      dbo.Budget_Column_Audit
                        ( 
                         Budget_Id
                        ,Audit_Column_Name
                        ,Previous_Value
                        ,Current_Value
                        ,Audit_By_User_Id
                        ,Audit_Ts )
                        SELECT
                              @budgetId
                             ,'Analyst_Queue_Display_Cd'
                             ,NULL
                             ,@Analyst_Queue_Display_Cd
                             ,@user_id
                             ,GETDATE()
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC dbo.usp_RethrowError 
      END CATCH
END
;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_CREATE_BUDGET_P] TO [CBMSApplication]
GO
