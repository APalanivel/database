SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuExceptionType_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@cu_exception_type_id	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsCuExceptionType_Get]
	( @MyAccountId int
	, @cu_exception_type_id int
	)
AS
BEGIN


	   select cu_exception_type_id
		, exception_type
		, exception_group_type_id
		, stop_processing
		, is_routed_reason
		, sort_order
		, MANAGED_BY_GROUP_INFO_ID
	from cu_exception_type
	    where cu_exception_type_id = @cu_exception_type_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuExceptionType_Get] TO [CBMSApplication]
GO
