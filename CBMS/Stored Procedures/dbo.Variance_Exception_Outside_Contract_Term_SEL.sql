
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Variance_Exception_Outside_Contract_Term_SEL  
 
DESCRIPTION:  Used to check supplier data for Out side contract term  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	 @Account_Id		INT
	 @InvBeginDate		DATETIME
	 @InvEndDate		DATETIME   
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  
	
	EXEC Variance_Exception_Outside_Contract_Term_SEL 35071,'2008-11-10','2009-12-01'
	EXEC Variance_Exception_Outside_Contract_Term_SEL 105559,'2008-10-28','2010-10-31'
	EXEC Variance_Exception_Outside_Contract_Term_SEL 135109,'2009-05-13','2011-04-30'
	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
NK			Nageswara Rao Kosuri         


Initials	Date		Modification  
------------------------------------------------------------  
NK			10/13/2009  Created
HG			2017-04-05	MAINT-5049, Modified to exclude the not yet assigned supplier accounts
HG			2017-06-12	MAINT-5418 Modified the query to handle the DMO supplier account as it wont have real contracts associated to that Supplier account
******/
CREATE PROCEDURE [dbo].[Variance_Exception_Outside_Contract_Term_SEL]
      (
       @Account_Id INT
      ,@InvBeginDate DATETIME
      ,@InvEndDate DATETIME )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            suppacc2.ACCOUNT_ID
      FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm1
            JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm2
                  ON samm1.METER_ID = samm2.METER_ID
            LEFT OUTER JOIN dbo.CONTRACT con1
                  ON samm1.Contract_ID = con1.CONTRACT_ID
            LEFT OUTER JOIN dbo.CONTRACT con2
                  ON samm2.Contract_ID = con2.CONTRACT_ID
            JOIN dbo.ACCOUNT suppacc1
                  ON samm1.ACCOUNT_ID = suppacc1.ACCOUNT_ID
            JOIN dbo.ACCOUNT suppacc2
                  ON samm2.ACCOUNT_ID = suppacc2.ACCOUNT_ID
      WHERE
            samm1.ACCOUNT_ID = @Account_Id
            AND ( samm1.Contract_ID != samm2.Contract_ID
                  OR ( samm1.Contract_ID = -1
                       AND samm2.Contract_ID = -1 ) )
            AND samm1.ACCOUNT_ID != samm2.ACCOUNT_ID
            AND ISNULL(con1.CONTRACT_TYPE_ID, -1) = ISNULL(con2.CONTRACT_TYPE_ID, -1)
            AND suppacc1.VENDOR_ID = suppacc2.VENDOR_ID
            AND suppacc2.Supplier_Account_Begin_Dt <= @InvBeginDate
            AND suppacc2.Supplier_Account_End_Dt >= @InvEndDate
            AND ISNULL(NULLIF(suppacc2.ACCOUNT_NUMBER, ''), 'Not Yet Assigned') <> 'Not Yet Assigned'

END
;
GO


GRANT EXECUTE ON  [dbo].[Variance_Exception_Outside_Contract_Term_SEL] TO [CBMSApplication]
GO
