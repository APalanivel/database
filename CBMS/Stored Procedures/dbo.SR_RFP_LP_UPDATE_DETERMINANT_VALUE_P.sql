SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_UPDATE_DETERMINANT_VALUE_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_value_id int,
	@lp_value numeric(32, 16)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test an update
--exec dbo.SR_RFP_LP_UPDATE_DETERMINANT_VALUE_P
--	@user_id =1,
--	@session_id = -1,
--	@determinant_value_id = 3069251,
--	@lp_value = 640.6000000000000000           -- original value = 640.6000000000000000 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_UPDATE_DETERMINANT_VALUE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_value_id int,
	@lp_value numeric(32, 16)
	AS
	
set nocount on
	
		update  sr_rfp_lp_determinant_values
		set lp_value = @lp_value
		where sr_rfp_lp_determinant_values_id = @determinant_value_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_UPDATE_DETERMINANT_VALUE_P] TO [CBMSApplication]
GO
