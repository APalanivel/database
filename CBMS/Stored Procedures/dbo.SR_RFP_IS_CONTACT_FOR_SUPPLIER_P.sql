SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_IS_CONTACT_FOR_SUPPLIER_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@contactId     	int       	          	
	@vendorId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--exec SR_RFP_IS_CONTACT_FOR_SUPPLIER_P 31,1091

CREATE  PROCEDURE dbo.SR_RFP_IS_CONTACT_FOR_SUPPLIER_P 
@contactId int,
@vendorId int

as
set nocount on

	

DECLARE @cnt int
SELECT @cnt = (
SELECT count(*)

from	USER_INFO userInfo,	
	SR_SUPPLIER_CONTACT_INFO scInfo , 
	SR_SUPPLIER_CONTACT_VENDOR_MAP  cvMap

where	
	cvMap.vendor_id = @vendorId
	and scInfo.SR_SUPPLIER_CONTACT_INFO_ID = cvMap.SR_SUPPLIER_CONTACT_INFO_ID
	and scInfo.USER_INFO_ID = userInfo.USER_INFO_ID
	and scInfo.SR_SUPPLIER_CONTACT_INFO_ID = @contactId
)

RETURN  @cnt
--print @cnt
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_IS_CONTACT_FOR_SUPPLIER_P] TO [CBMSApplication]
GO
