SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
   
 dbo.Variance_Consumption_Level_Desc_Sel_By_Commodity  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType Default  Description  
-------------------------------------------------------------------------  
 @Commodity_id   Int              Indicates commodity type(water , kerosene etc ..,)  
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
 EXEC dbo.Variance_Consumption_Level_Desc_Sel_By_Commodity 290  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 GM  GANGADHAR MAMILLAPALLI  
  
MODIFICATIONS   
 Initials Date  Modification  
------------------------------------------------------------  
 GM         1/28/2010 Created  
  
 */
CREATE PROCEDURE dbo.Variance_Consumption_Level_Desc_Sel_By_Commodity  
 @Commodity_id INT  
AS  

BEGIN  
  
 SET NOCOUNT ON;  
    
	SELECT  
		CONSUMPTION_LEVEL_DESC,    
		Variance_Consumption_Level_ID  
	FROM  
		dbo.Variance_Consumption_Level VCL  
	WHERE  
		VCL.commodity_id=@commodity_id  
  
END
GO
GRANT EXECUTE ON  [dbo].[Variance_Consumption_Level_Desc_Sel_By_Commodity] TO [CBMSApplication]
GO
