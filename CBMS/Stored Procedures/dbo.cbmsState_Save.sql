SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsState_Save]


DESCRIPTION: 

INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	 @MyAccountId              int
	, @state_id                int             null
	, @state_name              varchar(200)
	, @region_id               int             null
	, @country_id              int             null
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--cbmsState_Save
--	 @MyAccountId =1
--	, @state_id =1
--	, @state_name ='AKA' -- ORIGINAL VALUE: 'AK'
--	, @region_id  = 5
--	, @country_id  = 4



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier

*******/

CREATE PROCEDURE [dbo].[cbmsState_Save]
(
	 @MyAccountId int
	, @state_id int = null
	, @state_name varchar(200)
	, @region_id int = null
	, @country_id int = null
	)
AS

BEGIN

	SET NOCOUNT ON


	declare @this_id int
	set @this_id = @State_Id


	if @this_id is null
	begin

	  select @this_id = state_id
	    from state
	   where state_name = @state_name
	     and country_id = @country_id

	end



	if @this_id is null
	begin

		insert into state(
			state_name
			,region_id
			,country_id
			
			)
		values(
			@State_Name
			,@Region_Id
			,@Country_Id
			)


		set @this_id = scope_identity()

	end
	else
	begin
		update state
		set state_name = @State_Name
		,region_id = @Region_Id
		,country_id = @Country_Id
		where state_id = @State_Id
	end
	
	exec cbmsState_Get @MyAccountId, @this_id
	
	end
GO
GRANT EXECUTE ON  [dbo].[cbmsState_Save] TO [CBMSApplication]
GO
