SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_REPORT_CATEGORY_P
@userId varchar,
@sessionId varchar
as
	set nocount on
	select report_category_id,report_category_name,report_category_description
	from report_category
	where depth=0
GO
GRANT EXECUTE ON  [dbo].[GET_REPORT_CATEGORY_P] TO [CBMSApplication]
GO
