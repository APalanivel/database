SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Corporate_RM_Budget_Volume_Price_Dtls_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Corporate_RM_Budget_Volume_Price_Dtls_Sel  584,1005,291,NULL,'2020-01-01','2020-01-01',25,3,'All Sites'
	EXEC Trade.Corporate_RM_Budget_Volume_Price_Dtls_Sel  584,1005,291,4,'2020-06-01','2020-06-01',25,NULL,'All Sites',NULL,NULL,334
	EXEC Trade.Corporate_RM_Budget_Volume_Price_Dtls_Sel @Start_Dt='2020-07-01', @End_Dt='2021-06-01', @Index_Id=584, 
		@Client_Id=10017, @Commodity_Id=291, @Country_Id=NULL, @Uom_Id=25, @Currency_Unit_Id=3, @Participant_Sites='Reported Sites'
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	RR			2020-02-24	GRM-1725 Applied number conversion on hedged and un-hedged to remove decimal points
	RR			2020-02-24	GRM-1769 Added budget id input @Rm_Budget_Id
	RR			2020-04-06	GRM-1782 Added local varaible parameters to control the parameter sniffing with in the procedure
	               
******/
CREATE PROCEDURE [Trade].[Corporate_RM_Budget_Volume_Price_Dtls_Sel]
    (
        @Index_Id INT = NULL
        , @Client_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Country_Id NVARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Uom_Id INT = NULL
        , @Currency_Unit_Id INT = NULL
        , @Participant_Sites VARCHAR(20) = 'All Sites'
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Rm_Budget_Id INT = NULL
    )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Index VARCHAR(200)
            , @Trade_Index_Id INT;

        DECLARE
            @lcl_Index_Id INT = @Index_Id
            , @lcl_Client_Id INT = @Client_Id
            , @lcl_Commodity_Id INT = @Commodity_Id
            , @lcl_Country_Id NVARCHAR(MAX) = @Country_Id
            , @lcl_Start_Dt DATE = @Start_Dt
            , @lcl_End_Dt DATE = @End_Dt
            , @lcl_Uom_Id INT = @Uom_Id
            , @lcl_Currency_Unit_Id INT = @Currency_Unit_Id
            , @lcl_Participant_Sites VARCHAR(20) = @Participant_Sites
            , @lcl_Start_Index INT = @Start_Index
            , @lcl_End_Index INT = @End_Index
            , @lcl_Rm_Budget_Id INT = @Rm_Budget_Id;

        CREATE TABLE #Participant_Sites
             (
                 Client_Hier_Id INT
             );


        CREATE TABLE #Tbl_Client_Forecast
             (
                 Client_Id INT
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 6)
             );

        CREATE TABLE #Tbl_Site_Forecast
             (
                 Client_Id INT
                 , Service_Month DATE
                 , Client_Hier_Id INT
                 , Forecast_Volume DECIMAL(28, 6)
             );

        CREATE TABLE #Tbl_Site_Hedge
             (
                 Client_Id INT
                 , Client_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Index_Id INT
                 , Index_Name VARCHAR(200)
                 , Price_Index_Id INT
                 , Price_Point_Name VARCHAR(200)
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge DECIMAL(24, 3)
                 , Hedge_Mode VARCHAR(200)
                 , Hedge_Mode_Type_Id INT
             );

        CREATE TABLE #Tbl_Client_Hedge
             (
                 Client_Id INT
                 , Client_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge_Price DECIMAL(24, 3)
             );

        SELECT
            @Index = e.ENTITY_NAME
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_ID = @lcl_Index_Id;

        SELECT
            @Trade_Index_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_TYPE = 165
            AND (   (   e.ENTITY_NAME = 'Nymex'
                        AND @Index = 'NYMEX')
                    OR  (   e.ENTITY_NAME = 'CGPR'
                            AND @Index = 'AECO'));

        IF @lcl_Rm_Budget_Id IS NULL
            BEGIN
                INSERT INTO #Participant_Sites
                     (
                         Client_Hier_Id
                     )
                EXEC Trade.RM_Budget_Participant_Sites_Sel
                    @Client_Id = @lcl_Client_Id
                    , @Commodity_Id = @lcl_Commodity_Id
                    , @Country_Id = @lcl_Country_Id
                    , @Start_Dt = @lcl_Start_Dt
                    , @End_Dt = @lcl_End_Dt
                    , @Participant_Sites = @lcl_Participant_Sites
                    , @Index_Id = @lcl_Index_Id;
            END;
        ELSE
            BEGIN

                DELETE  FROM #Participant_Sites WHERE   @lcl_Rm_Budget_Id IS NOT NULL;
                INSERT INTO #Participant_Sites
                     (
                         Client_Hier_Id
                     )
                SELECT
                    rbp.Client_Hier_Id
                FROM
                    Trade.Rm_Budget_Participant rbp
                WHERE
                    rbp.Rm_Budget_Id = @lcl_Rm_Budget_Id
                    AND @lcl_Rm_Budget_Id IS NOT NULL;
            END;

        INSERT INTO #Tbl_Site_Hedge
             (
                 Client_Id
                 , Client_Name
                 , Client_Hier_Id
                 , Index_Id
                 , Index_Name
                 , Price_Index_Id
                 , Price_Point_Name
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge
                 , Hedge_Mode
                 , Hedge_Mode_Type_Id
             )
        SELECT
            dt.Client_Id
            , ch.Client_Name
            , vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , SUM((CASE WHEN frq.Code_Value = 'Monthly' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END)
                       WHEN frq.Code_Value = 'Daily' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END) * dd.DAYS_IN_MONTH_NUM
                   END))
            , SUM((CASE WHEN frq.Code_Value = 'Monthly' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END)
                       WHEN frq.Code_Value = 'Daily' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END) * dd.DAYS_IN_MONTH_NUM
                   END) * tp.Trade_Price * cuc.CONVERSION_FACTOR) AS WA_Hedge
            , e.ENTITY_NAME AS Hedge_Mode
            , dt.Hedge_Mode_Type_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt.Deal_Ticket_Frequency_Cd
            INNER JOIN dbo.ENTITY hdgtyp
                ON dt.Hedge_Type_Cd = hdgtyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dt.Deal_Ticket_Id = vol.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = vol.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc2
                ON cuc2.BASE_UNIT_ID = vol.Uom_Type_Id
                   AND  cuc2.CONVERTED_UNIT_ID = @lcl_Uom_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Deal_Month
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = vol.Trade_Price_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON dt.Price_Index_Id = pridx.PRICE_INDEX_ID
                   AND  pridx.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dt.Currency_Unit_Id
                   AND  cuc.CONVERTED_UNIT_ID = @lcl_Currency_Unit_Id
                   AND  cuc.CONVERSION_DATE = vol.Deal_Month
                   AND  cuc.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.ENTITY e
                ON dt.Hedge_Mode_Type_Id = e.ENTITY_ID
        WHERE
            idx.ENTITY_ID = @Trade_Index_Id
            AND e.ENTITY_NAME = 'Index'
            AND vol.Deal_Month BETWEEN @lcl_Start_Dt
                               AND     @lcl_End_Dt
            AND dt.Commodity_Id = @lcl_Commodity_Id
            AND tp.Trade_Price IS NOT NULL
            AND ch.Client_Id = @lcl_Client_Id
            AND (   @lcl_Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@lcl_Country_Id, ',') c
                                   WHERE
                                        CAST(c.Segments AS INT) = ch.Country_Id))
            AND ch.Site_Id > 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Participant_Sites ps
                           WHERE
                                ps.Client_Hier_Id = ch.Client_Hier_Id)
        GROUP BY
            dt.Client_Id
            , ch.Client_Name
            , vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , vol.Total_Volume
            , tp.Trade_Price
            , cuc.CONVERSION_FACTOR
            , trdact.Code_Value
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , e.ENTITY_NAME
            , dt.Hedge_Mode_Type_Id;

        INSERT INTO #Tbl_Site_Forecast
             (
                 Client_Id
                 , Service_Month
                 , Client_Hier_Id
                 , Forecast_Volume
             )
        SELECT
            ch.Client_Id
            , chf.Service_Month
            , ch.Client_Hier_Id
            , MAX(chf.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chf
                ON chf.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chf.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @lcl_Uom_Id
        WHERE
            ch.Client_Id = @lcl_Client_Id
            AND chf.Commodity_Id = @lcl_Commodity_Id
            AND (   @lcl_Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@lcl_Country_Id, ',') c
                                   WHERE
                                        CAST(c.Segments AS INT) = ch.Country_Id))
            AND chf.Service_Month BETWEEN @lcl_Start_Dt
                                  AND     @lcl_End_Dt
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Participant_Sites ps
                           WHERE
                                ps.Client_Hier_Id = ch.Client_Hier_Id)
        GROUP BY
            ch.Client_Id
            , chf.Service_Month
            , ch.Client_Hier_Id;

        INSERT INTO #Tbl_Client_Forecast
             (
                 Client_Id
                 , Client_Hier_Id
                 , Service_Month
                 , Forecast_Volume
             )
        SELECT
            tcf.Client_Id
            , ch.Client_Hier_Id
            , tcf.Service_Month
            , SUM(tcf.Forecast_Volume)
        FROM
            #Tbl_Site_Forecast tcf
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = tcf.Client_Id
        WHERE
            ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
        GROUP BY
            tcf.Client_Id
            , ch.Client_Hier_Id
            , tcf.Service_Month;

        INSERT INTO #Tbl_Client_Hedge
             (
                 Client_Id
                 , Client_Name
                 , Client_Hier_Id
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge_Price
             )
        SELECT
            tch.Client_Id
            , tch.Client_Name
            , ch.Client_Hier_Id
            , tch.Service_Month
            , SUM(tch.Hedged_Volume) AS Hedged_Volume
            , SUM(tch.WA_Hedge) / NULLIF(SUM(tch.Hedged_Volume), 0) AS WA_Hedge_Price
        FROM
            #Tbl_Site_Hedge tch
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Id = tch.Client_Id
        WHERE
            ch.Sitegroup_Id = 0
            AND ch.Site_Id = 0
        GROUP BY
            tch.Client_Id
            , tch.Client_Name
            , ch.Client_Hier_Id
            , tch.Service_Month;


        SELECT
            ISNULL(tch.Client_Hier_Id, tcf.Client_Hier_Id) AS Client_Hier_Id
            , tch.Client_Name AS Client_Name
            , dd.DATE_D AS Budget_Month
            , CAST(tcf.Forecast_Volume AS DECIMAL(22, 0)) AS Forecasted_Volume
            , CAST(tch.Hedged_Volume AS DECIMAL(22, 0)) AS Total_Volume_Hedged
            , tch.WA_Hedge_Price AS WACOG_Of_Hedges
            , CAST((tcf.Forecast_Volume - ISNULL(tch.Hedged_Volume, 0)) AS DECIMAL(22, 0)) AS Total_Volume_Unhedged
            , CAST(0 AS DECIMAL(22, 3)) AS Budget_Target_Price
        FROM
            meta.DATE_DIM dd
            LEFT JOIN #Tbl_Client_Forecast tcf
                ON dd.DATE_D = tcf.Service_Month
            LEFT JOIN #Tbl_Client_Hedge tch
                ON dd.DATE_D = tch.Service_Month
        WHERE
            dd.DATE_D BETWEEN @lcl_Start_Dt
                      AND     @lcl_End_Dt;

        DROP TABLE
            #Participant_Sites
            , #Tbl_Client_Forecast
            , #Tbl_Client_Hedge
            , #Tbl_Site_Forecast
            , #Tbl_Site_Hedge;


    END;


GO
GRANT EXECUTE ON  [Trade].[Corporate_RM_Budget_Volume_Price_Dtls_Sel] TO [CBMSApplication]
GO
