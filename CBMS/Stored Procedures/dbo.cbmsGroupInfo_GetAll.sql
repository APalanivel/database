SET NUMERIC_ROUNDABORT OFF 
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME: dbo.cbmsGroupInfo_GetAll
  
DESCRIPTION:   
  
INPUT PARAMETERS:      
 Name			DataType			Default     Description      
----------------------------------------------------------------
 @Start_Index						INT   1          
 @End_Index							INT   2147483647  
 @Total_Row_Count					INT	  0

OUTPUT PARAMETERS:
Name            DataType          Default     Description      
----------------------------------------------------------      
  
USAGE EXAMPLES:  
----------------------------------------------------------  

EXEC dbo.cbmsGroupInfo_GetAll

EXEC dbo.cbmsGroupInfo_GetAll
      @Start_Index = 1
     ,@End_Index = 25
     ,@Total_Row_Count = 0                   

  
AUTHOR INITIALS:  
Initials Name  
----------------------------------------------------------  
DR		 Deana Ritter
PNR		 Pandarinath
RR		 Raghu Reddy
SP       Sandeep Pigilam
NR		 Narayana Reddy

  
MODIFICATIONS :
Initials	Date		    Modification  
----------------------------------------------------------
PNR			03/31/2011		Comments header added
							Added Group_Info_Category_Id and app_menu profile  columns to the select statement as per the DV group revamp requirement.
							Removed unused parameter @MyAccountId
RR			2013-06-06	    ENHANCE-60 Added Is_Fee_Module column in select list
SP			2014-01-30		for RA Admin user management added App_Access_Role_Type_Cd,Is_Required_For_RA_Login in select list
NR			2016-10-24		MAINT-4391(4519) Added Group_Type column.

******/

CREATE PROCEDURE [dbo].[cbmsGroupInfo_GetAll]
      ( 
       @Start_Index INT = 1
      ,@End_Index INT = 2147483647
      ,@Total_Row_Count INT = 0 )
AS 
BEGIN
      SET NOCOUNT ON;
      
      
      IF ( @Total_Row_Count = 0 ) 
            BEGIN
            
                  SELECT
                        @Total_Row_Count = COUNT(1)
                  FROM
                        dbo.Group_Info_Category gic
                        INNER JOIN dbo.APP_MENU_PROFILE amp
                              ON gic.APP_MENU_PROFILE_ID = amp.APP_MENU_PROFILE_ID
                        INNER JOIN dbo.APP_PROFILE ap
                              ON amp.APP_MENU_PROFILE_ID = ap.APP_MENU_PROFILE_ID
                        LEFT OUTER JOIN dbo.GROUP_INFO gi
                              ON gi.Group_Info_Category_Id = gic.Group_Info_Category_Id
                        LEFT OUTER JOIN ( dbo.Group_Info_App_Access_Role_Type_Cd gcd
                                          INNER JOIN dbo.Code c
                                                ON gcd.App_Access_Role_Type_Cd = c.Code_Id )
                                          ON gi.GROUP_INFO_ID = gcd.Group_Info_Id
            END;
      WITH  cte_cbmsGroupInfo_GetAll
              AS ( SELECT TOP ( @End_Index )
                        gi.group_info_id
                       ,gi.queue_id
                       ,gi.group_name
                       ,gi.group_description
                       ,gic.Group_Info_Category_Id
                       ,gic.Category_Name
                       ,gic.Sort_Order
                       ,gic.APP_MENU_PROFILE_ID
                       ,amp.APP_MENU_PROFILE_NAME
                       ,gi.Is_Fee_Module
                       ,gcd.App_Access_Role_Type_Cd
                       ,c.Code_Value AS App_Access_Role_Type
                       ,gi.Is_Required_For_RA_Login
                       ,ROW_NUMBER() OVER ( ORDER BY gic.sort_Order
					   , CASE WHEN gi.Is_Required_For_RA_Login = 1 THEN 1
                              ELSE 2
                         END
					   , gic.Category_Name
					   , gi.group_name ) Row_Num
                       ,CASE WHEN ap.APP_PROFILE_NAME = 'CBMS' THEN 'CBMS Group'
                             WHEN ap.APP_PROFILE_NAME = 'DV' THEN 'RA Function'
                             ELSE ap.APP_PROFILE_NAME
                        END AS Group_Type
                   FROM
                        dbo.Group_Info_Category gic
                        INNER JOIN dbo.APP_MENU_PROFILE amp
                              ON gic.APP_MENU_PROFILE_ID = amp.APP_MENU_PROFILE_ID
                        INNER JOIN dbo.APP_PROFILE ap
                              ON amp.APP_MENU_PROFILE_ID = ap.APP_MENU_PROFILE_ID
                        LEFT OUTER JOIN dbo.GROUP_INFO gi
                              ON gi.Group_Info_Category_Id = gic.Group_Info_Category_Id
                        LEFT OUTER JOIN ( dbo.Group_Info_App_Access_Role_Type_Cd gcd
                                          INNER JOIN dbo.Code c
                                                ON gcd.App_Access_Role_Type_Cd = c.Code_Id )
                                          ON gi.GROUP_INFO_ID = gcd.Group_Info_Id)
            SELECT
                  group_info_id
                 ,queue_id
                 ,group_name
                 ,group_description
                 ,Group_Info_Category_Id
                 ,Category_Name
                 ,Sort_Order
                 ,APP_MENU_PROFILE_ID
                 ,APP_MENU_PROFILE_NAME
                 ,Is_Fee_Module
                 ,App_Access_Role_Type_Cd
                 ,App_Access_Role_Type
                 ,Is_Required_For_RA_Login
                 ,@Total_Row_Count AS Total_Row_Count
                 ,Group_Type
            FROM
                  cte_cbmsGroupInfo_GetAll csa
            WHERE
                  csa.Row_Num BETWEEN @Start_Index AND @End_Index
            ORDER BY
                  csa.Row_Num 

END;

;
GO




GRANT EXECUTE ON  [dbo].[cbmsGroupInfo_GetAll] TO [CBMSApplication]
GO
GO