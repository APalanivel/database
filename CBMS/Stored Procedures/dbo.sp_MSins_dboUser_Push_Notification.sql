SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dbo].[sp_MSins_dboUser_Push_Notification]
    @c1 int,
    @c2 int,
    @c3 int,
    @c4 int,
    @c5 int,
    @c6 datetime
as
begin  
	insert into [dbo].[User_Push_Notification](
		[User_Push_Notification_Id],
		[User_Info_Id],
		[Notification_Type_Cd],
		[Notification_Mode_Cd],
		[Created_User_Id],
		[Created_Ts]
	) values (
    @c1,
    @c2,
    @c3,
    @c4,
    @c5,
    @c6	) 
end  
GO
