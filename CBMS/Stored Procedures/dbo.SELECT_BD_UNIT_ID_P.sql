SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.SELECT_BD_UNIT_ID_P
	@rs_date DATETIME,
	@billing_determinant_master_id INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT bd.bd_unit_type_id
	FROM dbo.billing_determinant bd INNER JOIN dbo.rate_schedule rs ON rs.rate_schedule_id = bd.bd_parent_id
		INNER JOIN dbo.entity ent ON ent.entity_id = bd.bd_parent_type_id
	WHERE ent.entity_name = 'Rate schedule'
		AND ent.entity_type = 120
		AND (@rs_date BETWEEN rs.rs_start_date AND ISNULL(rs.rs_end_date,'12/31/2030'))
		AND bd.billing_determinant_master_id=@billing_determinant_master_id

END
GO
GRANT EXECUTE ON  [dbo].[SELECT_BD_UNIT_ID_P] TO [CBMSApplication]
GO
