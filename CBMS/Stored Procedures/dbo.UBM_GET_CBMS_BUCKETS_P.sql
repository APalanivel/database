SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC dbo.UBM_GET_BATCHLOG_DETAILS_P '1','1',73,'cASS'


CREATE   PROCEDURE dbo.UBM_GET_CBMS_BUCKETS_P 
@userId varchar(20),
@sessionId varchar(20)


as
	set nocount on
select ENTITY_ID, ENTITY_NAME  from entity
where entity_type in (123,201) 
	/*and entity_id not in 
	(
		select cbms_charge_bucket_type_Id from  ubm_charge_bucket_map
	)

	and 
	entity_id not in 
	(
		select cbms_usage_bucket_type_Id from  ubm_usage_bucket_map
	)*/
GO
GRANT EXECUTE ON  [dbo].[UBM_GET_CBMS_BUCKETS_P] TO [CBMSApplication]
GO
