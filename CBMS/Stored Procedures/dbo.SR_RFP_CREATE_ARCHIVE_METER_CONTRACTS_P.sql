SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_ARCHIVE_METER_CONTRACTS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@RFP_ID int,
@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test archiving 
--exec [DBO].SR_RFP_CREATE_ARCHIVE_METER_CONTRACTS_P 5, 164

---- check if the records have been  entered newly
--SELECT MAX(SR_RFP_ARCHIVE_METER_CONTRACTS_ID) FROM sv..SR_RFP_ARCHIVE_METER_CONTRACTS 
---- 6 records should be added after test archive 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [DBO].[SR_RFP_CREATE_ARCHIVE_METER_CONTRACTS_P] 

@RFP_ID int,
@sr_rfp_account_id int

AS
	
SET NOCOUNT ON

	declare @current_date datetime
	select @current_date =  convert(datetime, convert( varchar(10), getdate(), 101 ))

declare @SR_RFP_ARCHIVE_METER_CONTRACTS_ID int, @SR_RFP_ACCOUNT_METER_MAP_ID int, @CONTRACT_NUMBER varchar(50), @CONTRACT_TYPE varchar(50), @CURRENT_SUPPLIER varchar(200)

DECLARE C_ARC_MET_CON CURSOR FAST_FORWARD

FOR select 
	map.sr_rfp_account_meter_map_id,
	con.ed_contract_number ,
	CASE e.ENTITY_NAME 
	WHEN 'Client' THEN 'Customer'
	ELSE ''
	END ENTITY_NAME,
	v.vendor_name
from 
	sr_rfp_account_meter_map map,		
	sr_rfp_account rfp_account,
	sr_rfp_closure closure,
	contract con join dbo.SR_RFP_FN_GET_CONTRACT_DETAILS_FOR_ARCHIEVING(@sr_rfp_account_id,@current_date , @RFP_ID ) archieving on archieving.contract_id = con.contract_id,
	entity e,
	vendor v,
	account a,
	sr_rfp rfp

where	 	
	rfp.sr_rfp_id = @RFP_ID
	and rfp_account.sr_rfp_id = rfp.sr_rfp_id
	and closure.sr_account_group_id = rfp_account.sr_rfp_account_id
	and rfp_account.sr_rfp_account_id = @sr_rfp_account_id	
	and rfp_account.is_deleted = 0
	and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id	
	and con.contract_type_id= e.entity_id
	and rfp_account.account_id = a.account_id
	and a.vendor_id = v.vendor_id


OPEN C_ARC_MET_CON
	
FETCH NEXT FROM C_ARC_MET_CON INTO @SR_RFP_ACCOUNT_METER_MAP_ID, @CONTRACT_NUMBER, @CONTRACT_TYPE, @CURRENT_SUPPLIER
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN

		delete from SR_RFP_ARCHIVE_METER_CONTRACTS where sr_rfp_account_meter_map_id = @SR_RFP_ACCOUNT_METER_MAP_ID
		and contract_number = @CONTRACT_NUMBER and contract_type = @CONTRACT_TYPE and current_supplier =@CURRENT_SUPPLIER
		

		INSERT INTO SR_RFP_ARCHIVE_METER_CONTRACTS (
														SR_RFP_ACCOUNT_METER_MAP_ID, 
														CONTRACT_NUMBER, 
														CONTRACT_TYPE, 
														CURRENT_SUPPLIER
													)
											VALUES (
														@SR_RFP_ACCOUNT_METER_MAP_ID, 
														@CONTRACT_NUMBER, 
														@CONTRACT_TYPE, 
														@CURRENT_SUPPLIER
													)

		

	END -- IF
		FETCH NEXT FROM C_ARC_MET_CON INTO @SR_RFP_ACCOUNT_METER_MAP_ID, @CONTRACT_NUMBER, @CONTRACT_TYPE, @CURRENT_SUPPLIER
END
CLOSE C_ARC_MET_CON
DEALLOCATE C_ARC_MET_CON
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_ARCHIVE_METER_CONTRACTS_P] TO [CBMSApplication]
GO
