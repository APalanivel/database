SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Variance_Log_Queue_Upd_By_Account_Commodity_Service_Month

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id		Int
	@Commodity_id	Int
	@Service_Month	Date 
	@Queue_Id		Int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Variance_Log_Queue_Upd_By_Account_Commodity_Service_Month 158043,67,'2009-10-01',15160

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/17/2010	Created

******/

CREATE PROCEDURE dbo.Variance_Log_Queue_Upd_By_Account_Commodity_Service_Month
	@Account_id		Int
	,@Commodity_id	Int
	,@Service_Month	Date
	,@Queue_Id		Int
AS
BEGIN

    SET NOCOUNT ON;

	DECLARE @Queue_name VARCHAR(255)
   
	SELECT
		@Queue_Name = FIRST_NAME + SPACE(1) + LAST_NAME
	FROM
		dbo.User_Info
	WHERE
		QUEUE_ID = @Queue_Id

	BEGIN TRY
	
		BEGIN TRAN
		
			UPDATE
				vlog
			SET
				Queue_Id = @Queue_Id
				,Queue_Name = @Queue_Name
				,Queue_Dt = GETDATE()
			FROM
				dbo.Variance_Log vlog
			WHERE
				vlog.Account_ID = @Account_Id
				AND vlog.Commodity_ID = @Commodity_id
				AND vlog.Service_Month = @Service_Month

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		ROLLBACK TRAN
		EXEC dbo.usp_RethrowError
		
	END CATCH	
END
GO
GRANT EXECUTE ON  [dbo].[Variance_Log_Queue_Upd_By_Account_Commodity_Service_Month] TO [CBMSApplication]
GO
