
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 dbo.cbmsSite_GetByAccount

DESCRIPTION:
  It is used to get site informaion by given account.

INPUT PARAMETERS:
 Name			 DataType  Default	Description
------------------------------------------------------------
 @Account_Id     INT

OUTPUT PARAMETERS:
 Name			DataType  Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

Exec dbo.cbmsSite_GetByAccount 526435


AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 BCH		Balaraju
 
MODIFICATIONS

 Initials   Date			Modification
------------------------------------------------------------

 BCH	    2012-03-28		Removed @MyAccountId parameter(not using in bellow code),
							Removed vwAccountMeter and vwSiteName  views and division table,
							used core.client_hier_account and core.client_hier tables instead.

******/
CREATE  PROCEDURE dbo.cbmsSite_GetByAccount ( @Account_Id INT )
AS 
BEGIN

      SELECT
            cha.Client_Hier_Id
           ,s.site_id
           ,s.site_type_id
           ,ch.Site_Type_Name AS site_type
           ,ch.site_name
           ,s.division_id
           ,ch.client_id
           ,s.ubmsite_id
           ,s.primary_address_id
           ,s.site_reference_number
           ,s.site_product_service
           ,s.production_schedule
           ,s.shutdown_schedule
           ,s.is_alternate_power
           ,s.is_alternate_gas
           ,s.doing_business_as
           ,s.naics_code
           ,s.tax_number
           ,s.duns_number
           ,s.is_history
           ,s.history_reason_type_id
           ,hr.entity_name history_reason_type
           ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' site_label
      FROM
            Core.Client_Hier_Account cha
            JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN dbo.[Site] s
                  ON s.Site_Id = ch.Site_Id
            LEFT JOIN dbo.Entity hr
                  ON hr.Entity_Id = s.HISTORY_REASON_TYPE_ID
      WHERE
            cha.Account_Id = @Account_Id
      GROUP BY
            cha.Client_Hier_Id
           ,s.site_id
           ,s.site_type_id
           ,ch.Site_Type_Name
           ,ch.site_name
           ,s.division_id
           ,ch.client_id
           ,s.ubmsite_id
           ,s.primary_address_id
           ,s.site_reference_number
           ,s.site_product_service
           ,s.production_schedule
           ,s.shutdown_schedule
           ,s.is_alternate_power
           ,s.is_alternate_gas
           ,s.doing_business_as
           ,s.naics_code
           ,s.tax_number
           ,s.duns_number
           ,s.is_history
           ,s.history_reason_type_id
           ,hr.entity_name
           ,ch.city
           ,ch.state_name
END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsSite_GetByAccount] TO [CBMSApplication]
GO
