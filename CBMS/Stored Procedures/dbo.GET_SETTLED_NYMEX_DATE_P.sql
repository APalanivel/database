SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE     PROCEDURE dbo.GET_SETTLED_NYMEX_DATE_P
@userId varchar,
@sessionId varchar,
@monthIdentifier varchar (20)


as
	set nocount on
	select SETTLEMENT_DATE from RM_NYMEX_SETTLEMENT 
	where month_identifier=CONVERT(Varchar(12), @monthIdentifier, 101)
GO
GRANT EXECUTE ON  [dbo].[GET_SETTLED_NYMEX_DATE_P] TO [CBMSApplication]
GO
