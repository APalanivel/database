SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                      
/******                                                  
NAME:   [BulkProcess_Invoice_Excel]                                            
DESCRIPTION:                                               
------------------------------------------------------------                                                 
 INPUT PARAMETERS: @BATCH_ID INT ,                                            
 -----------------------------------------------------------            
 unit test case                         
            
--[dbo].[BulkProcess_GetHeaders]  73            
--[BulkProcess_Invoice_Excel] 183            
--[BulkProcess_Invoice_Excel] 64            
--[BulkProcess_Invoice_Excel] 66                                   
------------------------------------------------------------                                                  
AUTHOR INITIALS:                                                  
Initials Name                                                  
------------------------------------------------------------                                                  
PK   Prasan kumar                            
                                               
 MODIFICATIONS                                                   
 Initials  Date  Modification                                                  
------------------------------------------------------------                                                  
 PK   10-06-2020  added auto sub bucket code to email                         
******/              
CREATE PROCEDURE [dbo].[BulkProcess_Invoice_Excel]                     
    (                      
        @Batch_ID INT                      
    )                      
AS                      
BEGIN                      
        SET NOCOUNT ON;               
              
  declare @process_name varchar(255)      
   , @statuspassauto INT;                  
                      
  UPDATE [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch]                
  SET Processing_Step = 'Sending mail'                
  WHERE [XL_Bulk_Invoice_Process_Batch_Id] = @Batch_ID              
              
              
              
  SELECT              
    @process_name = c.Code_Value               
  FROM              
   dbo.Xl_Bulk_Data_Process dp             
   INNER JOIN Code c              
       ON c.Code_Id = dp.Xl_Process_Name_Cd              
  WHERE              
   Xl_Bulk_Data_Process_Id = (SELECT  Xl_Bulk_Data_Process_Id              
          FROM              
           LOGDB.dbo.[XL_Bulk_Invoice_Process_Batch]              
          WHERE              
           Xl_Bulk_Invoice_Process_Batch_Id = @Batch_ID);              
            
  SELECT @statuspassauto =  MAX(CASE WHEN [cd].[Code_Value] = 'Completed Auto Sub bucket' THEN [cd].[Code_Id]      END)                       
        FROM                        
               [dbo].[Code] AS [cd]                        
  INNER JOIN [dbo].[Codeset] AS [cs]                        
                   ON [cs].[Codeset_Id] = [cd].[Codeset_Id]                        
  WHERE [cd].[Is_Active] = 1                        
               AND [cs].[Codeset_Name] = 'Batch Status';          
                     
                    
  IF( @process_name ='Update Charges')                         
  BEGIN            
   SELECT                      
                 
       [XBIPB].[Cu_Invoice_Id]                      
       , [XBIPB].[Bucket_Code]  as Charges_Bucket_Code                    
       , [XBIPB].[Current_Bucket_Name]  as Current_Bucket                    
       , [XBIPB].[Current_Sub_Bucket_Name]  as Current_Sub_Bucket                                
       , [XBIPB].[New_Bucket_Name]  as Correct_Bucket                    
       , [XBIPB].[New_Sub_Bucket_Name]  as Correct_Sub_Bucket                                
       , [XBIPB].[Run_Data_Quality_Test]  as Run_Data_Quality                    
       , [XBIPB].[Run_Recalc]  as Run_Recalac                    
       , [XBIPB].[Run_Variance_Test]  as Run_Variance_Test                        
       , case when [XBIPB].[Error_Msg] is not null then [XBIPB].[Error_Msg]         
   else 'Success'         
   end  as  [Status]                 
   FROM                      
       [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [XBIPB]                      
		left join CODE c on [XBIPB].Status_Cd = c.Code_Id                    
   WHERE                      
       [XBIPB].[XL_Bulk_Invoice_Process_Batch_Id] = @Batch_ID                 
   ORDER BY Created_Ts
       
  END             
  ELSE IF( @process_name ='Update Determinants')            
  BEGIN            
               
    SELECT                      
                  
        [XBIPB].[Cu_Invoice_Id]                      
        , [XBIPB].[Bucket_Code]  as Determinant_Bucket_Code                    
        , [XBIPB].[Current_Bucket_Name]  as Current_Bucket                    
        , [XBIPB].[Current_Sub_Bucket_Name]  as Current_Sub_Bucket             
     , [XBIPB].[Current_Uom_Name] as Current_UOM                               
        , [XBIPB].[New_Bucket_Name]  as Correct_Bucket                    
        , [XBIPB].[New_Sub_Bucket_Name]  as Correct_Sub_Bucket              
     , [XBIPB].[New_Uom_Name] as Correct_UOM                        
        , [XBIPB].[Run_Data_Quality_Test]  as Run_Data_Quality                    
        , [XBIPB].[Run_Recalc]  as Run_Recalac                    
        , [XBIPB].[Run_Variance_Test]  as Run_Variance_Test                             
        , case when [XBIPB].[Error_Msg] is not null then [XBIPB].[Error_Msg]                   
    when [XBIPB].[Status_Cd] =@statuspassauto then 'Success- Automapping selected (Bucket Name) for sub bucket'     
    ELSE 'Success'    
   end  as  [Status]                 
    FROM                      
        [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [XBIPB]                      
		left join CODE c on [XBIPB].Status_Cd = c.Code_Id                    
    WHERE                      
        [XBIPB].[XL_Bulk_Invoice_Process_Batch_Id] = @Batch_ID                 
    ORDER BY Created_Ts           
            
            
  END             
  ELSE IF( @process_name ='Save and Submit Invoices')            
  BEGIN            
               
   SELECT                      
                 
       [XBIPB].[Cu_Invoice_Id]              
       , [XBIPB].[Run_Data_Quality_Test]  as Run_Data_Quality                    
       , [XBIPB].[Run_Recalc]  as Run_Recalac                    
       , [XBIPB].[Run_Variance_Test]  as Run_Variance_Test                   
       , case when [XBIPB].[Error_Msg] is not null then [XBIPB].[Error_Msg]         
   else 'Success'         
   end  as  [Status]                
   FROM                      
       [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [XBIPB]                      
		left join CODE c on [XBIPB].Status_Cd = c.Code_Id                    
   WHERE                      
       [XBIPB].[XL_Bulk_Invoice_Process_Batch_Id] = @Batch_ID                 
   ORDER BY Created_Ts           
            
            
  END             
                    
END; 
GO
GRANT EXECUTE ON  [dbo].[BulkProcess_Invoice_Excel] TO [CBMSApplication]
GO
