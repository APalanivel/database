
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
Name:  
 CBMS.dbo.Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year
 
 Description:  
 
 Input Parameters:  
    Name			    DataType		    Default Description  
------------------------------------------------------------------------  
    @Client_Id		    INT
    @Report_Year	    INT
    @Bucket_List	    Cost_Usage_Bucket		  READONLY - Table Value Parameter
    @Division_Id	    INT			    NULL
    @Site_Id		    INT			    NULL
    @Country_Id	    INT			    NULL
    @Site_Not_Managed   INT			    -1
 
 Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
 Usage Examples:
------------------------------------------------------------  
DECLARE @Bucket_List1 AS Cost_Usage_Bucket

INSERT INTO @Bucket_List1
VALUES (290, 'Total Usage', 'Determinant', 12)
    ,(290, 'Total Cost', 'Charge', 3)
--SELECT * FROM @Bucket_List1

EXECUTE dbo.Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year 10069, 2011, @Bucket_List1, NULL, NULL, NULL, -1
EXECUTE dbo.Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year 10069, 2011, @Bucket_List1, 567, NULL, NULL, -1
EXECUTE dbo.Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year 10069, 2011, @Bucket_List1, NULL, 17654, NULL, -1

 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 
 Modifications :  
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP			09/14/2011  Created and replaced flowlling SPs as a part of Addtl Data changes
					   - cbmsCostUsageSite_GetDetailElCostForClient
					   - cbmsCostUsageSite_GetDetailElUnitCostForClient
					   - cbmsCostUsageSite_GetDetailElUsageForClient
					   - cbmsCostUsageSite_GetDetailNgCostForClient
					   - cbmsCostUsageSite_GetDetailNgUnitCostForClient
					   - cbmsCostUsageSite_GetDetailNgUsageForClient
					   - cbmsCostUsageSite_GetDetailTotalCostForClient
AP			10/03/2011  Removed @Currency_Unit_Id parameter and used bucket_uom_id from TVP
AP			03/28/2012  Replaced @Site_Id parameter with @Site_Client_Hier_Id and added Client_Hier_Id in the result set
HG			2012-07-09	MAINT-1356, modified the code to return site name as rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' instead of actual site name
******/

CREATE PROCEDURE [dbo].[Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year]
      ( 
       @Client_Id INT
      ,@Report_Year INT
      ,@Bucket_List AS dbo.Cost_Usage_Bucket READONLY
      ,@Division_Id INT = NULL
      ,@Site_Client_Hier_Id INT = NULL
      ,@Country_Id INT = NULL
      ,@Site_Not_Managed INT = -1 )
AS 
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Begin_Date DATETIME
           ,@End_Date DATETIME
                
      CREATE TABLE #Client_Dtl
            ( 
             Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Division_Id INT
            ,Division_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(1000)
            ,Site_Status VARCHAR(20)
            ,Country_Id INT
            ,Country_Name VARCHAR(200)
            ,State_Id INT
            ,State_Name VARCHAR(20)
            ,Currency_Group_Id INT
            ,PRIMARY KEY CLUSTERED ( Client_Hier_Id, Site_Id ) )


      CREATE TABLE #Cost_Usage_Site_Dtl
            ( 
             Service_Month DATE
            ,Client_Hier_Id INT
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Value DECIMAL(28, 10)
            ,Commodity_Id INT
            ,Data_Source_Cd INT
            ,Data_Source_Code VARCHAR(25) )
            
      CREATE INDEX IDX_Cost_Usage_Site_Dtl ON #Cost_Usage_Site_Dtl(Client_Hier_Id, Service_Month)

      CREATE TABLE #Invoice_Participation_Site
            ( 
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Is_Complete SMALLINT
            ,Is_Published SMALLINT
            ,Under_Review SMALLINT
            ,Commodity_Id INT )

      CREATE INDEX IDX_Invoice_Participation_Site ON #Invoice_Participation_Site(Client_Hier_Id, Service_Month)

                                    
   	 -- Fiscal service months calculation based on Client_fiscal_offset value
      SELECT
            @Begin_Date = dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))
           ,@End_Date = dateadd(MONTH, -1, ( dateadd(YEAR, 1, dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))) ))
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Id = @Client_Id
            AND ch.SiteGroup_ID = 0
            

      INSERT      INTO #Client_Dtl
                  ( 
                   Client_Id
                  ,Client_Name
                  ,Division_Id
                  ,Division_Name
                  ,Client_Hier_Id
                  ,Site_Id
                  ,Site_Name
                  ,Site_Status
                  ,Country_Id
                  ,Country_Name
                  ,State_Id
                  ,State_Name
                  ,Currency_Group_Id )
                  SELECT
                        CH.Client_ID
                       ,CH.Client_Name
                       ,CH.Sitegroup_Id AS Division_ID
                       ,CH.Sitegroup_Name AS Division_Name
                       ,ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')'
                       ,( case WHEN CH.Site_Not_Managed = 1 THEN convert(VARCHAR(20), 'Inactive')
                               ELSE convert(VARCHAR(20), 'Active')
                          END ) AS Active
                       ,CH.Country_ID
                       ,CH.Country_Name
                       ,CH.State_ID
                       ,CH.State_Name
                       ,CH.Client_Currency_Group_ID AS Currency_Group_ID
                  FROM
                        Core.Client_Hier CH
                  WHERE
                        CH.Client_Id = @Client_ID
                        AND ( @Division_ID IS NULL
                              OR CH.Sitegroup_Id = @Division_ID )
                        AND ( @Site_Client_Hier_Id IS NULL
                              OR CH.Client_Hier_Id = @Site_Client_Hier_Id )
                        AND ( @Country_ID IS NULL
                              OR CH.Country_ID = @Country_ID )
                        AND ( @Site_Not_Managed = -1
                              OR CH.Site_Not_Managed = @Site_Not_Managed )
                        AND CH.Site_ID > 0
                  GROUP BY
                        CH.Client_ID
                       ,CH.Client_Name
                       ,CH.Sitegroup_Id
                       ,CH.Sitegroup_Name
                       ,ch.Client_Hier_Id
                       ,CH.Site_ID
                       ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')'
                       ,( case WHEN CH.Site_Not_Managed = 1 THEN convert(VARCHAR(20), 'Inactive')
                               ELSE convert(VARCHAR(20), 'Active')
                          END )
                       ,CH.Country_ID
                       ,CH.Country_Name
                       ,CH.State_ID
                       ,CH.State_Name
                       ,CH.Client_Currency_Group_ID

      INSERT      INTO #Cost_Usage_Site_Dtl
                  ( 
                   Service_Month
                  ,Client_Hier_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Bucket_Value
                  ,Commodity_Id
                  ,Data_Source_Cd
                  ,Data_Source_Code )
                  SELECT
                        CUSD.Service_Month
                       ,CUSD.Client_Hier_Id
                       ,BM.Bucket_Name
                       ,BL.Bucket_Type
                       ,sum(case WHEN BL.Bucket_Type = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                                 WHEN BL.Bucket_Type = 'Determinant' THEN CUSD.Bucket_Value * UC.Conversion_Factor
                                 ELSE 0
                            END)
                       ,BM.Commodity_Id
                       ,cusd.Data_Source_Cd
                       ,dsc.Code_Value
                  FROM
                        dbo.Cost_Usage_Site_Dtl CUSD
                        INNER JOIN #Client_Dtl CDtl
                              ON CDtl.Client_Hier_Id = CUSD.Client_Hier_Id
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_type
                              ON bkt_type.Code_Id = bm.Bucket_Type_Cd
                        INNER JOIN @Bucket_List BL
                              ON BL.Bucket_Name = BM.Bucket_Name
                                 AND BL.Commodity_Id = BM.Commodity_Id
                                 AND bl.Bucket_Type = bkt_type.Code_Value
                        LEFT OUTER JOIN dbo.Consumption_Unit_Conversion UC
                              ON UC.Base_Unit_Id = CUSD.UOM_Type_Id
                                 AND UC.Converted_Unit_Id = BL.Bucket_Uom_Id
                                 AND BL.Bucket_Type = 'Determinant'
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                              ON CurConv.Base_Unit_ID = CUSD.Currency_Unit_ID
                                 AND CurConv.Currency_Group_ID = CDtl.Currency_Group_ID
                                 AND CurConv.Conversion_Date = CUSD.Service_Month
                                 AND CurConv.Converted_Unit_ID = BL.Bucket_Uom_Id
                                 AND BL.Bucket_Type = 'Charge'
                        LEFT OUTER JOIN dbo.Code dsc
                              ON dsc.Code_Id = cusd.Data_Source_Cd
                  WHERE
                        CUSD.Service_Month BETWEEN @Begin_Date
                                           AND     @End_Date
                  GROUP BY
                        CUSD.Service_Month
                       ,CUSD.Client_Hier_Id
                       ,BM.Bucket_Name
                       ,BL.Bucket_Type
                       ,BM.Commodity_Id
                       ,cusd.Data_Source_Cd
                       ,dsc.Code_Value
                       
      INSERT      INTO #Invoice_Participation_Site
                  ( 
                   Client_Hier_Id
                  ,Service_Month
                  ,Is_Complete
                  ,Is_Published
                  ,Under_Review
                  ,Commodity_Id )
                  SELECT
                        cd.Client_Hier_Id
                       ,IP.Service_Month
                       ,( case WHEN ( sum(case WHEN IP.Is_Expected = 1 THEN 1
                                               ELSE 0
                                          END) = sum(case WHEN IP.Is_Received = 1 THEN 1
                                                          ELSE 0
                                                     END) ) THEN 1
                               ELSE 0
                          END ) AS Is_Complete
                       ,( case WHEN sum(case WHEN IP.Is_Received = 1 THEN 1
                                             ELSE 0
                                        END) > 0 THEN 1
                               ELSE 0
                          END ) AS Is_Published
                       ,isnull(max(case WHEN IP.Recalc_Under_Review = 1 THEN 1
                                        WHEN IP.Variance_Under_Review = 1 THEN 1
                                   END), 0) Under_Review
                       ,CHA.Commodity_Id
                  FROM
                        dbo.Invoice_Participation IP
                        INNER JOIN #Client_Dtl CD
                              ON CD.Site_ID = IP.Site_ID
                        INNER JOIN ( SELECT
                                          Account_Id
                                         ,Commodity_Id
                                     FROM
                                          Core.Client_Hier_Account
                                     GROUP BY
                                          Account_Id
                                         ,Commodity_Id ) CHA
                              ON CHA.Account_ID = IP.Account_ID
                  WHERE
                        IP.Service_Month BETWEEN @Begin_Date
                                         AND     @End_Date
                        AND EXISTS ( SELECT
                                          bl.Commodity_Id
                                     FROM
                                          @Bucket_List bl
                                     WHERE
                                          bl.Commodity_Id = CHA.Commodity_Id )
                  GROUP BY
                        cd.Client_Hier_Id
                       ,IP.Service_Month
                       ,CHA.Commodity_Id

                       
      SELECT
            CD.Division_ID
           ,CD.Division_Name
           ,CD.Client_Hier_Id
           ,cd.Site_Id
           ,CD.Site_Name
           ,CD.Site_Status
           ,CD.Country_ID
           ,CD.Country_Name
           ,CD.State_ID
           ,CD.State_Name
           ,COM.Commodity_Name
           ,dd.Date_D AS Service_Month
           ,bl.Bucket_Name
           ,bl.Bucket_Type
           ,cusd.Bucket_Value
           ,isnull(IP.Is_Complete, 0) AS Is_Complete
           ,isnull(IP.Is_Published, 0) AS Is_Published
           ,isnull(IP.Under_Review, 0) AS Under_Review
           ,cusd.Data_Source_Code
      FROM
            #Client_Dtl CD
            CROSS JOIN @Bucket_List bl
            CROSS JOIN meta.Date_Dim dd
            INNER JOIN dbo.Commodity COM
                  ON COM.Commodity_Id = bl.Commodity_Id
            LEFT OUTER JOIN #Cost_Usage_Site_Dtl CUSD
                  ON CUSD.Client_Hier_Id = CD.Client_Hier_Id
                     AND CUSD.Service_Month = dd.Date_D
                     AND bl.Bucket_Name = cusd.Bucket_Name
                     AND bl.Bucket_Type = cusd.Bucket_Type
                     AND bl.Commodity_Id = cusd.Commodity_Id
            LEFT OUTER JOIN #Invoice_Participation_Site IP
                  ON IP.Client_Hier_Id = CD.Client_Hier_Id
                     AND IP.Service_Month = CUSD.Service_Month
                     AND IP.Commodity_Id = CUSD.Commodity_Id
      WHERE
            dd.Date_D BETWEEN @Begin_Date AND @End_Date
            AND ( IP.Is_Published = 1
                  OR IP.Is_Published IS NULL )
      GROUP BY
            CD.Division_ID
           ,CD.Division_Name
           ,CD.Client_Hier_Id
           ,cd.Site_Id
           ,CD.Site_Name
           ,CD.Site_Status
           ,CD.Country_ID
           ,CD.Country_Name
           ,CD.State_ID
           ,CD.State_Name
           ,COM.Commodity_Name
           ,dd.Date_D
           ,bl.Bucket_Name
           ,bl.Bucket_Type
           ,CUSD.Bucket_Value
           ,IP.Is_Complete
           ,IP.Is_Published
           ,IP.Under_Review
           ,cusd.Data_Source_Code
      ORDER BY
            CD.Division_Name
           ,CD.Site_Name
           ,dd.Date_D
           ,COM.Commodity_Name
           
      DROP TABLE #Client_Dtl
      DROP TABLE #Invoice_Participation_Site
      DROP TABLE #Cost_Usage_Site_Dtl

END

;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Bucket_Value_Sel_For_Client_By_Report_Year] TO [CBMSApplication]
GO
