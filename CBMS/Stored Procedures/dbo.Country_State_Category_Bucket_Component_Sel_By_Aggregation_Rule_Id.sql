SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******      
                         
 NAME: dbo.Country_State_Category_Bucket_Component_Sel_By_Aggregation_Rule_Id                     
                          
 DESCRIPTION:      
		  To get the details bucket companent details for the given rule.                          
                          
 INPUT PARAMETERS:      
                         
 Name							DataType          Default       Description      
-----------------------------------------------------------------------------------    
 @Country_State_Category_Bucket_Aggregation_Rule_Id         INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-----------------------------------------------------------------------------------    
                          
 USAGE EXAMPLES:                              
-----------------------------------------------------------------------------------    
               
 EXEC dbo.Country_State_Category_Bucket_Component_Sel_By_Aggregation_Rule_Id 1   
	
			                
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
-----------------------------------------------------------------------------------    
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-----------------------------------------------------------------------------------    
 NR                     2016-10-04      Created for MAINT-4352.                        
                         
******/ 

CREATE PROCEDURE [dbo].[Country_State_Category_Bucket_Component_Sel_By_Aggregation_Rule_Id]
      ( 
       @Country_State_Category_Bucket_Aggregation_Rule_Id INT )
AS 
BEGIN 
      SET NOCOUNT ON 


      SELECT
            cscbc.Country_State_Category_Bucket_Component_Id
           ,cscbc.Country_State_Category_Bucket_Aggregation_Rule_Id
           ,cscbc.Calculation_Param_Code
           ,cscbc.Source_Id
           ,cscbc.Source_Type_Cd
           ,c.Code_Value AS Source_Type
           ,cscbc.Uom_Type_Id
           ,e.ENTITY_NAME
           ,cscbc.Default_Value
           ,cscbc.Is_Use_Default_Value_When_Null
      FROM
            dbo.Country_State_Category_Bucket_Aggregation_Rule cscbar
            INNER JOIN dbo.Country_State_Category_Bucket_Component cscbc
                  ON cscbar.Country_State_Category_Bucket_Aggregation_Rule_Id = cscbc.Country_State_Category_Bucket_Aggregation_Rule_Id
            INNER JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = cscbc.Uom_Type_Id
            INNER JOIN dbo.Code c
                  ON c.Code_Id = cscbc.Source_Type_Cd
      WHERE
            cscbc.Country_State_Category_Bucket_Aggregation_Rule_Id = @Country_State_Category_Bucket_Aggregation_Rule_Id
      
END



;
GO
GRANT EXECUTE ON  [dbo].[Country_State_Category_Bucket_Component_Sel_By_Aggregation_Rule_Id] TO [CBMSApplication]
GO
