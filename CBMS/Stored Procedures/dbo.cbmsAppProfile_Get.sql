SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[cbmsAppProfile_Get]
	( @app_profile_id int
	)
AS
BEGIN

	   select app_profile_id
		, app_profile_name
		, app_menu_profile_id
		, app_env
		, app_logon_url
		, app_overview_url
		, app_cbms_server
		, app_allow_manage
	     from app_profile
	    where app_profile_id = @app_profile_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppProfile_Get] TO [CBMSApplication]
GO
