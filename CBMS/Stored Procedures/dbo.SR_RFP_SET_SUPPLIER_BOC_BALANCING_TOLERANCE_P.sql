SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_BALANCING_TOLERANCE_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@toleranceId int,
@isPriceVarying VARCHAR(10),
@volumeTolerance VARCHAR(10),
@aboveToleranceComments VARCHAR(4000),
@below_toleranceComments VARCHAR(4000),
@balancingFrequencyTypeId VARCHAR(10),
@toleranceComments VARCHAR(4000),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@toleranceIdentityId int outPUT


USAGE EXAMPLES:
------------------------------------------------------------
-- TEST 1
--EXEC  SR_RFP_GET_SUPPLIER_BOC_BALANCING_TOLERANCE_P 1,1,931

---- TEST 2
---- Check before and after update of Record at SV
--SELECT * FROM SV..SR_RFP_BALANCING_TOLERANCE WHERE SR_RFP_BALANCING_TOLERANCE_ID = (SELECT MAX(SR_RFP_BALANCING_TOLERANCE_ID) FROM SV..SR_RFP_BALANCING_TOLERANCE)

---- Update an entry
--exec [dbo].[SR_RFP_SET_SUPPLIER_BOC_BALANCING_TOLERANCE_P] 
--@toleranceId = 197763,
--@isPriceVarying = NULL,
--@volumeTolerance = NULL,
--@aboveToleranceComments = NULL,  
--@below_toleranceComments = NULL,
--@balancingFrequencyTypeId = NULL,
--@toleranceComments = 'Test Data', -- Original value = NULL
--@toleranceIdentityId = NULL



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_BALANCING_TOLERANCE_P] 

@toleranceId int,
@isPriceVarying VARCHAR(10),
@volumeTolerance VARCHAR(10),
@aboveToleranceComments VARCHAR(4000),
@below_toleranceComments VARCHAR(4000),
@balancingFrequencyTypeId VARCHAR(10),
@toleranceComments VARCHAR(4000),
@toleranceIdentityId int outPUT


as
	
set nocount on

if @toleranceId=0
begin	

		INSERT INTO SR_RFP_BALANCING_TOLERANCE
		(
			IS_PRICE_VARYING,
			VOLUME_TOLERANCE,
			ABOVE_TOLERANCE_COMMENTS,
			BELOW_TOLERANCE_COMMENTS,
			BALANCING_FREQUENCY_TYPE_ID,
			TOLERANCE_COMMENTS
		)
		VALUES
		(
			@isPriceVarying,
			@volumeTolerance,
			@aboveToleranceComments,
			@below_toleranceComments,
			@balancingFrequencyTypeId,
			@toleranceComments

		)
   SELECT @toleranceIdentityId=SCOPE_IDENTITY()

end
else if @toleranceId>0
begin


	UPDATE SR_RFP_BALANCING_TOLERANCE SET

	IS_PRICE_VARYING=@isPriceVarying,
	VOLUME_TOLERANCE=@volumeTolerance,
	ABOVE_TOLERANCE_COMMENTS=@aboveToleranceComments,
	BELOW_TOLERANCE_COMMENTS=@below_toleranceComments,
	BALANCING_FREQUENCY_TYPE_ID=@balancingFrequencyTypeId,
	TOLERANCE_COMMENTS=@toleranceComments
	
	WHERE SR_RFP_BALANCING_TOLERANCE_ID=@toleranceId		
   
   SELECT @toleranceIdentityId=@toleranceId

return

end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_BALANCING_TOLERANCE_P] TO [CBMSApplication]
GO
