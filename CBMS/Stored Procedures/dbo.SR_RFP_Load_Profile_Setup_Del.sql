SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Load_Profile_Setup_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Load Profile Setup for Selected Sr RFP Load Profile Setup Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION
-------------------------------------------------------------------
@@SR_RFP_Load_Profile_Setup_Id  INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Load_Profile_Setup_Del  154
	Rollback Tran

	SELECT *
	FROM
		dbo.SR_RFP_LOAD_PROFILE_SETUP
	WHERE
		SR_RFP_LOAD_PROFILE_SETUP_ID =154

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			28-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Load_Profile_Setup_Del
   (
    @SR_RFP_Load_Profile_Setup_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_LOAD_PROFILE_SETUP
	WHERE
		SR_RFP_LOAD_PROFILE_SETUP_ID = @SR_RFP_Load_Profile_Setup_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Load_Profile_Setup_Del] TO [CBMSApplication]
GO
