SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsRateSchedule_Delete]


DESCRIPTION: 

INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId             int
	,@Rate_Schedule_Id       int 
	,@is_sv_rpl              bit                 0
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DMR		08/04/2009	Removed Linked Server Updates
	  DMR		09/10/2010	Modified for Quoted_Identifier
	  HG		05/24/2011	MAINT-488 fixes the code to remove all the history of given rate schedule id
							
							-- BILLING_DETERMIANANT_VALUE, CHARGE_VALUE and VARIABLE_UNKNOWN_CHARGE obselete table reference removed.
							-- CBMS Image delete part moved to application to delete the record from cbms_image and file location.
							-- Hardcoded parent_type_id value replaced by prepopulated variable.
							-- TRY/CATCH block added for error handling.
							-- Deleting the records from table based on PK of the table
								 this is done by saving the PK values saved in a table variable then deleting the record by looping through values saved in table variable.
							-- Unused @MyAccountId and @is_sv_rpl parameters removed

******/
CREATE PROCEDURE [dbo].[cbmsRateSchedule_Delete]
(
@Rate_Schedule_Id INT
)
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Charge_List TABLE ( Charge_Id INT )
      DECLARE @Tier_Detail_Id_List TABLE ( Tier_Detail_Id INT )
      DECLARE @Charge_Season_Map_Id_List TABLE ( Charge_Season_Map_Id INT )
      DECLARE @Season_Id_List TABLE ( SEASON_ID INT )
      DECLARE @Billing_Determinant_Id_List TABLE ( Billing_Determinant_Id INT )
      DECLARE @charge_master_list TABLE ( Charge_Master_Id INT )
      DECLARE @Additional_Charge_Id_List TABLE ( Additional_Charge_Id INT )

      DECLARE
            @Tier_Detail_Id INT
           ,@Charge_Season_Map_Id INT
           ,@Season_Id INT
           ,@Rate_Schedule_Table_Entity_Id INT
           ,@Charge_Id INT
           ,@Billing_Determinant_Id INT
           ,@Additional_Charge_Id INT
           ,@Charge_Master_Id INT
           ,@Parent_Type_Id INT;

		SELECT
			@Parent_Type_id = Entity_Id
		FROM
			dbo.Entity
		WHERE
			Entity_Name = 'Rate Schedule'
			AND Entity_Description = 'Billing Determinant Charge Parent'

		SELECT
			@Rate_Schedule_Table_Entity_Id = Entity_Id
		FROM
			dbo.Entity
		WHERE
			Entity_Name = 'RATE_SCHEDULE_TABLE'
			AND Entity_Description = 'Table_Type'

      INSERT INTO
            @Charge_List ( Charge_Id )
            SELECT
                  charge_id
            FROM
                  dbo.Charge
            WHERE
                  charge_parent_id = @Rate_Schedule_Id
                  AND charge_parent_type_id = @Parent_Type_id
            GROUP BY
                  charge_id

      INSERT INTO
            @Tier_Detail_Id_List ( Tier_Detail_Id )
            SELECT
                  td.TIER_DETAIL_ID
            FROM
                  dbo.TIER_DETAIL td
                  INNER JOIN dbo.charge_season_map csm
                        ON csm.charge_season_map_id = td.CHARGE_SEASON_MAP_ID
                  INNER JOIN @Charge_List cl
                        ON cl.Charge_Id = csm.Charge_Id

      INSERT INTO
            @Charge_Season_Map_Id_List
            ( Charge_Season_Map_Id )
            SELECT
                  csm.CHARGE_SEASON_MAP_ID
            FROM
                  dbo.charge_season_map csm
                  INNER JOIN @Charge_List cl
                        ON cl.Charge_Id = csm.Charge_Id	
		

      INSERT INTO
            @Season_Id_List ( SEASON_ID )
            SELECT
                  SEASON_ID
            FROM
                  dbo.Season
            WHERE
                  season_parent_id = @Rate_Schedule_Id
                  AND season_parent_type_id = @Parent_Type_id

      INSERT INTO
            @Billing_Determinant_Id_List
            ( Billing_Determinant_Id )
            SELECT
                  Billing_Determinant_Id
            FROM
				  dbo.billing_determinant
            WHERE
                  bd_parent_id = @Rate_Schedule_Id
                  AND bd_parent_type_id = @Parent_Type_id


      INSERT INTO
            @Additional_Charge_Id_List
            ( Additional_Charge_Id )
            SELECT
                  ac.ADDITIONAL_CHARGE_ID
            FROM
                  dbo.ADDITIONAL_CHARGE ac
                  INNER JOIN @Charge_Master_List cml
                        ON cml.Charge_Master_Id = ac.CHARGE_MASTER_ID

	-- Delete the records from table by looping through the PK values saved in the table variable
	BEGIN TRY


      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Tier_Detail_Id_List )
            BEGIN

                  SET @Tier_Detail_Id = ( SELECT TOP 1
                                                Tier_Detail_Id
                                          FROM
                                                @Tier_Detail_Id_List )  
                  EXEC dbo.Tier_Detail_Del @Tier_Detail_Id
       
                  DELETE FROM
                        @Tier_Detail_Id_List
                  WHERE
                        Tier_Detail_Id = @Tier_Detail_Id  
            END  
    
      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Charge_Season_Map_Id_List )  
            BEGIN  
                  SET @Charge_Season_Map_Id = ( SELECT TOP 1
                                                      Charge_Season_Map_Id
                                                FROM
                                                      @Charge_Season_Map_Id_List )  
                  EXEC dbo.Charge_Season_Map_Del @Charge_Season_Map_Id
       
                  DELETE FROM
                        @Charge_Season_Map_Id_List
                                                                  WHERE
                        Charge_Season_Map_Id = @Charge_Season_Map_Id  
            END


      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Charge_List )  
            BEGIN  
                  SET @Charge_Id = ( SELECT TOP 1 Charge_Id FROM @Charge_List )  
                  EXEC dbo.Charge_Del @Charge_Id
       
                  DELETE FROM
                        @Charge_List
                  WHERE
                        Charge_Id = @Charge_Id  
            END  


     WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Season_Id_List )
            BEGIN

                  SET @Season_Id = ( SELECT TOP 1 SEASON_ID FROM @Season_Id_List )

                  EXEC dbo.Delete_Season_p @Season_Id

                  DELETE FROM
                        @Season_Id_List
                  WHERE
                        Season_Id = @Season_Id
            END

      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @Billing_Determinant_Id_List )  
            BEGIN  
                  SET @Billing_Determinant_Id = ( SELECT TOP 1
                                                      Billing_Determinant_Id
                                                  FROM
                                                      @Billing_Determinant_Id_List )  
                  EXEC dbo.Billing_Determinant_Del @Billing_Determinant_Id
       
                  DELETE FROM
                        @Billing_Determinant_Id_List
                  WHERE
                        Billing_Determinant_Id = @Billing_Determinant_Id  
            END


          WHILE EXISTS ( SELECT
                            1
                         FROM
                            @Additional_Charge_Id_List )
                BEGIN

                      SET @Additional_Charge_Id = ( SELECT TOP 1
                                                          Additional_Charge_Id
                                                    FROM
                                                          @Additional_Charge_Id_List )

                      EXEC Additional_Charge_Del @Additional_Charge_Id
		
                      DELETE FROM
                            @Additional_Charge_Id_List
                      WHERE
                            Additional_Charge_Id = @Additional_Charge_Id

                END

      WHILE EXISTS ( SELECT
                        1
                     FROM
                        @charge_master_list )
            BEGIN
	
                  SET @Charge_Master_Id = ( SELECT TOP 1
                                                Charge_Master_Id
                                            FROM
                                                @charge_master_list )

                  EXEC dbo.Charge_Master_Sign_Map_Del @Charge_Master_Id
				
                  DELETE FROM
                        @charge_master_list
                  WHERE
                        Charge_Master_Id = @Charge_Master_Id

            END

		EXEC dbo.Entity_Audit_Del_By_Entity_Id_Entity_Identifier
			 @Entity_Id = @Rate_Schedule_Table_Entity_Id
			 ,@Entity_Identifier = @Rate_Schedule_Id

		EXEC dbo.DELETE_RATE_SCHEDULE_P @rateScheduleId = @Rate_Schedule_Id

	END TRY
	BEGIN CATCH
		EXEC dbo.usp_RethrowError @CustomMessage = 'Error in rate schedule delete'

	END CATCH

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRateSchedule_Delete] TO [CBMSApplication]
GO