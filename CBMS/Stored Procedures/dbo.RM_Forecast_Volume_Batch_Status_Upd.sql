SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Forecast_Volume_Batch_Status_Upd                    
                      
Description:                      
        To load forecast volumes for on-boarded sites
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	exec [dbo].[RM_Forecast_Volume_Batch_Status_Upd]
     
         @RM_Forecast_Volume_Batch_Id =10000
         , @Batch_Status_Cd =2
		
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-27     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Forecast_Volume_Batch_Status_Upd]
    (
        @RM_Forecast_Volume_Batch_Id INT
        , @Batch_Status_Cd INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        --EXEC dbo.CODE_SEL_BY_CodeSet_Name 
        --      @CodeSet_Name = 'Batch Status'--, @Code_Value =''

        UPDATE
            Trade.RM_Forecast_Volume_Batch
        SET
            Batch_Status_Cd = @Batch_Status_Cd
        WHERE
            RM_Forecast_Volume_Batch_Id = @RM_Forecast_Volume_Batch_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Forecast_Volume_Batch_Status_Upd] TO [CBMSApplication]
GO
