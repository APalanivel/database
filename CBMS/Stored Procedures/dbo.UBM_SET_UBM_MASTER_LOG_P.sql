SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_SET_UBM_MASTER_LOG_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@ubmName       	varchar(20)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   PROCEDURE dbo.UBM_SET_UBM_MASTER_LOG_P

	@ubmName varchar(20)
	--@startDate datetime

AS
set nocount on
	DECLARE @ubmId int
	DECLARE @statusTypeId int

	select @ubmId=(select ubm_id from ubm where ubm_name=@ubmName)

	select @statusTypeId=(select entity_id from entity where entity_name='In process' and entity_type=656)

	insert into UBM_BATCH_MASTER_LOG (ubm_id, status_type_id, start_date) values (@ubmId, @statusTypeId, getdate())
GO
GRANT EXECUTE ON  [dbo].[UBM_SET_UBM_MASTER_LOG_P] TO [CBMSApplication]
GO
