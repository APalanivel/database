SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.GET_FISCAL_MONTH_P 
@userId varchar(10),
@sessionId varchar(20),
@clientId int

as
	 set nocount on
select	entity.ENTITY_NAME

from	CLIENT client,
	RM_ONBOARD_CLIENT onboard,
	ENTITY entity

where	client.CLIENT_ID=onboard.CLIENT_ID AND
	client.FISCALYEAR_STARTMONTH_TYPE_ID=entity.ENTITY_ID and
	client.CLIENT_ID=@clientId
GO
GRANT EXECUTE ON  [dbo].[GET_FISCAL_MONTH_P] TO [CBMSApplication]
GO
