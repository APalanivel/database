SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name: [dbo].[Rate_Default_Meter_Attribute_Del]           
        
Description:                      
        To Delete Data to Rate_Default_Meter_Attribute table.                      
        
 Input Parameters:                      
     Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
@Rate_Id     INT    
@EC_Meter_Attribute_Id  INT    
@Meter_Attribute_Type_Cd INT    
        
 Output Parameters:                            
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
        
 Usage Examples:                          
----------------------------------------------------------------------------------------                        
 BEGIN TRAN          
       
 EXEC [dbo].[Rate_Default_Meter_Attribute_Del]      
     @Rate_Id =1            
  ,@EC_Meter_Attribute_Id=123               
  ,@Meter_Attribute_Type_Cd ='1123'        
              
 SELECT * FROM Budget.Rate_Default_Meter_Attribute       
 ROLLBACK TRAN             
     
Author Initials:                      
    Initials  Name                      
----------------------------------------------------------------------------------------                        
SC  Sreenivasulu Cheerala    
          
 Modifications:                      
    Initials        Date  Modification                      
----------------------------------------------------------------------------------------                        
 SC    2020-01-09    Initially Creation on budget 2.o  
******/
CREATE PROCEDURE [dbo].[Rate_Default_Meter_Attribute_Del]
    (
        @Rate_Default_Meter_Attribute_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DELETE
        RDMA
        FROM
            Budget.Rate_Default_Meter_Attribute RDMA
        WHERE
            RDMA.Rate_Default_Meter_Attribute_Id = @Rate_Default_Meter_Attribute_Id;
    END;

GO
GRANT EXECUTE ON  [dbo].[Rate_Default_Meter_Attribute_Del] TO [CBMSApplication]
GO
