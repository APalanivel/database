SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Queue_Merge           
                  
Description:                  
   This sproc is used in batch process to fill Invoice Collection Queue    
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                      
                        
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------       
    
   Exec dbo.[Invoice_Collection_Queue_Merge]     17
       
       
       
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna          
 SLP    Sri Lakshmi Pallikonda             
 Modifications:                  
    Initials        Date		Modification                  
----------------------------------------------------------------------------------------                    
    RKV				2016-12-27  Created For Invoice_Collection.    
	SLP				2019-09-26  Added the parameter @Is_Not_Default_Vendor , @Manual_ICR_Vendor_Id   
    
    
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Merge]
(
    @tvp_Invoice_Collection_Queue_Ins_Upd tvp_Invoice_Collection_Queue_Ins_Upd READONLY,
    @User_Info_Id INT,
    @Is_Not_Default_Vendor BIT = NULL,
	@Manual_ICR_Vendor_Id   INT=null
)
AS
BEGIN

    SET NOCOUNT ON;
    DECLARE @tvp_Invoice_Collection_Queue_Month AS tvp_Invoice_Collection_Queue_Month;

    CREATE TABLE #Inserted_Invoice_Collection_Queue
    (
        Invoice_Collection_Queue_Id INT,
        Invoice_Collection_Account_Config_Id INT,
        Commodity_Id INT,
        Invoice_Collection_Queue_Type_Cd INT,
        Invoice_Collection_Exception_Type_Cd INT,
        Collection_Start_Dt DATE,
        Collection_End_Dt DATE,
        Is_Manual BIT,
        Invoice_Request_Type_Cd INT,
        Status_Cd INT,
        Is_Locked INT,
        Received_Status_Updated_Dt DATE,
        Is_Not_Default_Vendor BIT
    );
    BEGIN TRY
        BEGIN TRAN;

        MERGE INTO dbo.Invoice_Collection_Queue tgt
        USING
        (
            SELECT tvp.Invoice_Collection_Queue_Id,
                   tvp.Invoice_Collection_Account_Config_Id,
                   ISNULL(tvp.Commodity_Id, -1) AS Commodity_Id,
                   tvp.Invoice_Collection_Queue_Type_Cd,
                   tvp.Invoice_Collection_Exception_Type_Cd,
                   tvp.Collection_Start_Dt,
                   tvp.Collection_End_Dt,
                   tvp.Is_Manual,
                   tvp.Invoice_Request_Type_Cd,
                   tvp.Status_Cd,
                   ISNULL(tvp.Is_Locked, 0) Is_Locked,
                   tvp.Received_Status_Updated_Dt,
                   tvp.Invoice_File_Name,
                   @User_Info_Id AS Created_User_Id,
                   @User_Info_Id AS Updated_User_Id
            FROM @tvp_Invoice_Collection_Queue_Ins_Upd tvp
        ) src
        ON src.Invoice_Collection_Queue_Id = tgt.Invoice_Collection_Queue_Id
        WHEN NOT MATCHED THEN
            INSERT
            (
                Invoice_Collection_Account_Config_Id,
                Commodity_Id,
                Invoice_Collection_Queue_Type_Cd,
                Invoice_Collection_Exception_Type_Cd,
                Collection_Start_Dt,
                Collection_End_Dt,
                Is_Manual,
                Invoice_Request_Type_Cd,
                Status_Cd,
                Is_Locked,
                Received_Status_Updated_Dt,
                Invoice_File_Name,
                Created_User_Id,
                Updated_User_Id,
                Is_Not_Default_Vendor,
				Manual_ICR_Vendor_Id
            )
            VALUES
            (   src.Invoice_Collection_Account_Config_Id, src.Commodity_Id, src.Invoice_Collection_Queue_Type_Cd,
                src.Invoice_Collection_Exception_Type_Cd, src.Collection_Start_Dt, src.Collection_End_Dt,
                src.Is_Manual, src.Invoice_Request_Type_Cd, src.Status_Cd, src.Is_Locked,
                src.Received_Status_Updated_Dt, Invoice_File_Name, src.Created_User_Id, src.Updated_User_Id,
                @Is_Not_Default_Vendor,@Manual_ICR_Vendor_Id)
        WHEN MATCHED THEN
            UPDATE SET tgt.Invoice_Collection_Queue_Type_Cd = src.Invoice_Collection_Queue_Type_Cd,
                       tgt.Invoice_Collection_Exception_Type_Cd = src.Invoice_Collection_Exception_Type_Cd,
                       tgt.Collection_Start_Dt = src.Collection_Start_Dt,
                       tgt.Collection_End_Dt = src.Collection_End_Dt,
                       tgt.Is_Manual = src.Is_Manual,
                       tgt.Invoice_Request_Type_Cd = src.Invoice_Request_Type_Cd,
                       tgt.Status_Cd = src.Status_Cd,
                       tgt.Is_Locked = src.Is_Locked,
                       tgt.Received_Status_Updated_Dt = src.Received_Status_Updated_Dt,
                       tgt.Invoice_File_Name = src.Invoice_File_Name,
                       tgt.Is_Not_Default_Vendor = @Is_Not_Default_Vendor,
					   tgt.Manual_ICR_Vendor_Id=@Manual_ICR_Vendor_Id
        OUTPUT Inserted.Invoice_Collection_Queue_Id,
               Inserted.Invoice_Collection_Account_Config_Id,
               Inserted.Commodity_Id,
               Inserted.Invoice_Collection_Queue_Type_Cd,
               Inserted.Invoice_Collection_Exception_Type_Cd,
               Inserted.Collection_Start_Dt,
               Inserted.Collection_End_Dt,
               Inserted.Is_Manual,
               Inserted.Invoice_Request_Type_Cd,
               Inserted.Status_Cd,
               Inserted.Is_Locked,
               Inserted.Received_Status_Updated_Dt,
               Inserted.Is_Not_Default_Vendor
        INTO #Inserted_Invoice_Collection_Queue;

        INSERT INTO @tvp_Invoice_Collection_Queue_Month
        (
            Invoice_Collection_Queue_Id,
            Account_Invoice_Collection_Month_Id
        )
        SELECT icq.Invoice_Collection_Queue_Id,
               aicm.Account_Invoice_Collection_Month_Id
        FROM #Inserted_Invoice_Collection_Queue icq
            INNER JOIN @tvp_Invoice_Collection_Queue_Ins_Upd tvp
                ON tvp.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                   AND tvp.Invoice_Collection_Queue_Type_Cd = icq.Invoice_Collection_Queue_Type_Cd
                   AND ISNULL(tvp.Invoice_Collection_Exception_Type_Cd, 0) = ISNULL(
                                                                                       icq.Invoice_Collection_Exception_Type_Cd,
                                                                                       0
                                                                                   )
                   AND ISNULL(tvp.Commodity_Id, -1) = ISNULL(icq.Commodity_Id, -1)
                   AND tvp.Collection_Start_Dt = icq.Collection_Start_Dt
                   AND tvp.Collection_End_Dt = icq.Collection_End_Dt
                   AND ISNULL(tvp.Invoice_Request_Type_Cd, 0) = ISNULL(icq.Invoice_Request_Type_Cd, 0)
                   AND ISNULL(tvp.Is_Locked, 0) = ISNULL(icq.Is_Locked, 0)
                   AND ISNULL(tvp.Received_Status_Updated_Dt, '1900-01-01') = ISNULL(
                                                                                        icq.Received_Status_Updated_Dt,
                                                                    '1900-01-01'
                                                                                    )
                   AND tvp.Status_Cd = icq.Status_Cd
                   AND tvp.Is_Manual = icq.Is_Manual
            OUTER APPLY
        (
            SELECT Segments
            FROM dbo.ufn_split(tvp.Account_Invoice_Collection_Month_Id, ',')
        ) aicm(Account_Invoice_Collection_Month_Id);

        EXEC dbo.Invoice_Collection_Queue_Month_Map_Merge @tvp_Invoice_Collection_Queue_Month;


        INSERT INTO dbo.Invoice_Collection_Queue_Event
        (
            Invoice_Collection_Queue_Id,
            Event_Desc,
            Event_By_User_Id
        )
        SELECT icq.Invoice_Collection_Queue_Id,
               tvp.Event_Desc,
               @User_Info_Id
        FROM #Inserted_Invoice_Collection_Queue icq
            INNER JOIN @tvp_Invoice_Collection_Queue_Ins_Upd tvp
                ON tvp.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
                   AND tvp.Invoice_Collection_Queue_Type_Cd = icq.Invoice_Collection_Queue_Type_Cd
                   AND ISNULL(tvp.Invoice_Collection_Exception_Type_Cd, 0) = ISNULL(
                                                                                       icq.Invoice_Collection_Exception_Type_Cd,
                                                                                       0
                                                                                   )
                   AND ISNULL(tvp.Commodity_Id, -1) = ISNULL(icq.Commodity_Id, -1)
                   AND tvp.Collection_Start_Dt = icq.Collection_Start_Dt
                   AND tvp.Collection_End_Dt = icq.Collection_End_Dt
                   AND ISNULL(tvp.Invoice_Request_Type_Cd, 0) = ISNULL(icq.Invoice_Request_Type_Cd, 0)
                   AND ISNULL(tvp.Is_Locked, 0) = ISNULL(icq.Is_Locked, 0)
                   AND ISNULL(tvp.Received_Status_Updated_Dt, '1900-01-01') = ISNULL(
                                                                                        icq.Received_Status_Updated_Dt,
                                                                                        '1900-01-01'
                                                                                    )
                   AND tvp.Status_Cd = icq.Status_Cd
                   AND tvp.Is_Manual = icq.Is_Manual;



        COMMIT TRAN;
    END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK;
        END;
        EXEC dbo.usp_RethrowError;

    END CATCH;

    DROP TABLE #Inserted_Invoice_Collection_Queue;

END;





;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Merge] TO [CBMSApplication]
GO
