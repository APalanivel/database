SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsBudgetDetailComments_GetForBudgetAndOwner]
	( @budget_id int = null
	, @owner_type_id int = null
	, @owner_id int = null
	)
AS
BEGIN

	   select bm.budget_detail_comments_owner_map_id 
		, bm.budget_id
		, bm.budget_detail_id
		, bm.owner_id
		, bm.owner_type_id
		, bm.budget_detail_comments
		, own.entity_name
	     from budget_detail_comments_owner_map bm
	     join entity own on bm.owner_type_id = own.entity_id  
	    where bm.budget_id = @budget_id
	      and bm.owner_id = @owner_id
	      and bm.owner_type_id = @owner_type_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsBudgetDetailComments_GetForBudgetAndOwner] TO [CBMSApplication]
GO
