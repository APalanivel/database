
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Cost_Usage_Site_Dtl_Del_By_Site_Commodity_Service_Month

DESCRIPTION:

INPUT PARAMETERS:
    Name			DataType		Default	Description
------------------------------------------------------------
    @Client_Hier_Id INT
    @Commodity_Id	INT
    @Service_Month	DATE

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
     EXEC dbo.Cost_Usage_Site_Dtl_Del_By_Site_Commodity_Service_Month 
      281
     ,65
     ,'2008-01-01'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	AP		Athmaram Pabbathi
	
MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
	HG        03/16/2010   Created
	AP		03/17/2012   Replaced @Site_Id parameter with @Client_Hier_Id
******/

CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Del_By_Site_Commodity_Service_Month
      @Client_Hier_Id INT
     ,@Commodity_id INT
     ,@Service_Month DATE
AS 
BEGIN

      SET NOCOUNT ON

      DELETE
            csd
      FROM
            dbo.Cost_Usage_Site_Dtl csd
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = csd.Bucket_Master_Id
      WHERE
            csd.Client_Hier_Id = @Client_Hier_Id
            AND csd.Service_Month = @Service_Month
            AND bm.Commodity_Id = @Commodity_id

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Del_By_Site_Commodity_Service_Month] TO [CBMSApplication]
GO
