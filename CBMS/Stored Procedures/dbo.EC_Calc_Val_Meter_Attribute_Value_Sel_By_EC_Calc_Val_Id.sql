SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.EC_Calc_Val_Meter_Attribute_Value_Sel_By_EC_Calc_Val_Id        
                
Description:                
		This sproc to get the calc value attribute details for a Given id.        
                             
 Input Parameters:                
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
	@EC_Calc_Val_Id			 INT  
      
 Output Parameters:                      
    Name					  DataType			Default				Description                  
----------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
----------------------------------------------------------------------------------------     
  
   Exec dbo.EC_Calc_Val_Meter_Attribute_Value_Sel_By_EC_Calc_Val_Id  27  
     
   Exec dbo.EC_Calc_Val_Meter_Attribute_Value_Sel_By_EC_Calc_Val_Id   28         
   
Author Initials:                
    Initials		Name                
----------------------------------------------------------------------------------------                  
	NR				Narayana Reddy                 
 Modifications:                
    Initials        Date			Modification                
----------------------------------------------------------------------------------------                  
    NR				2015-04-22		Created For AS400.           
               
******/   
  
CREATE PROCEDURE [dbo].[EC_Calc_Val_Meter_Attribute_Value_Sel_By_EC_Calc_Val_Id] ( @EC_Calc_Val_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON   
              
      SELECT
            ecvma.EC_Meter_Attribute_Id
           ,ecvma.EC_Meter_Attribute_Value
           ,ema.EC_Meter_Attribute_Name
      FROM
            dbo.EC_Calc_Val_Meter_Attribute ecvma
            INNER JOIN dbo.EC_Meter_Attribute ema
                  ON ecvma.EC_Meter_Attribute_Id = ema.EC_Meter_Attribute_Id
      WHERE
            ecvma.EC_Calc_Val_Id = @EC_Calc_Val_Id
                  
END  

;
GO
GRANT EXECUTE ON  [dbo].[EC_Calc_Val_Meter_Attribute_Value_Sel_By_EC_Calc_Val_Id] TO [CBMSApplication]
GO
