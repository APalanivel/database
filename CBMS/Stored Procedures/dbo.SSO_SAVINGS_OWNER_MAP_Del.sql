SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******

NAME: dbo.SSO_SAVINGS_OWNER_MAP_Del

DESCRIPTION:

	Used to delete SSo_Savings_Owner_Map.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION     
----------------------------------------------------------------
	@SSO_Savings_Id		INT
	@Client_Hier_Id		INT

OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------         
	USAGE EXAMPLES:
------------------------------------------------------------ 

	BEGIN TRAN

		EXEC dbo.SSO_SAVINGS_OWNER_MAP_Del 8, 1515

	ROLLBACK TRAN

	SELECT TOP 100 * FROM SSO_SAVINGS_OWNER_MAP
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	HG			9/24/2010		Created
	CPE			04/13/2011		Replaced @Owner_Id parameter with @Client_Hier_Id

*/
CREATE PROCEDURE dbo.SSO_SAVINGS_OWNER_MAP_Del
    (
       @SSO_Savings_Id	INT
      ,@Client_Hier_Id	INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE
		dbo.SSO_SAVINGS_OWNER_MAP
	WHERE
		SSO_SAVINGS_ID = @SSO_Savings_Id
		AND Client_Hier_Id = @Client_Hier_Id

END
GO
GRANT EXECUTE ON  [dbo].[SSO_SAVINGS_OWNER_MAP_Del] TO [CBMSApplication]
GO
