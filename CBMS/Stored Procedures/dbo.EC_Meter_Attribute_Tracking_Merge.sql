SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.EC_Meter_Attribute_Tracking_Merge                                        
                                         
                                        
DESCRIPTION:                                        
       This sproc is to get the Meter and attribute tracking details for the given search criteria                                  
           
                                        
INPUT PARAMETERS:                                        
 Name					        		DataType			Default   Description                                        
------------------------------------------------------------------------------------                                        
 @EC_Meter_Attribute_Tracking_Id         INT
 @EC_Meter_Attribute_Value               NVARCHAR(255)
 @User_Info_Id                           INT
 
                                        
OUTPUT PARAMETERS:                                        
 Name							DataType		Default   Description                                        
------------------------------------------------------------------------------------                                        
                                      
                                        
USAGE EXAMPLES:                                        
------------------------------------------------------------------------------------                                        
select * from EC_Meter_Attribute_Tracking    where EC_Meter_Attribute_Tracking_Id = 56                           
BEGIN TRANSACTION
EXEC dbo.EC_Meter_Attribute_Tracking_Merge 
      @EC_Meter_Attribute_Tracking_Id = 56
      ,@EC_Meter_Attribute_Value  = '15'
      ,@User_Info_Id = 49
     
select * from EC_Meter_Attribute_Tracking    where EC_Meter_Attribute_Tracking_Id = 56
ROLLBACK TRANSACTION                  
                                        
AUTHOR INITIALS:                                        
	Initials		Name                                        
------------------------------------------------------------------------------------                                        
	RKV             Ravi Kumar Vegesna
                                         
MODIFICATIONS                                        
                                        
	Initials	Date			Modification                                        
------------------------------------------------------------------------------------                                        
	RKV			2015-05-19		Created For As400.

******/    
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Tracking_Merge]
      ( 
       @EC_Meter_Attribute_Tracking_Id INT
      ,@EC_Meter_Attribute_Value NVARCHAR(255)
      ,@User_Info_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON          
  
      MERGE INTO dbo.EC_Meter_Attribute_Tracking tgt
            USING 
                  ( SELECT
                        @EC_Meter_Attribute_Tracking_Id AS EC_Meter_Attribute_Tracking_Id
                       ,@EC_Meter_Attribute_Value AS EC_Meter_Attribute_Value ) src
            ON ( tgt.EC_Meter_Attribute_Tracking_Id = src.EC_Meter_Attribute_Tracking_Id )
            WHEN MATCHED AND  src.EC_Meter_Attribute_Value IS NOT NULL
                  AND src.EC_Meter_Attribute_Value <> ''
                  THEN UPDATE
                    SET 
                        tgt.EC_Meter_Attribute_Value = src.EC_Meter_Attribute_Value
                       ,tgt.Updated_User_Id = @User_Info_Id
                       ,tgt.Last_Change_Ts = GETDATE()
            WHEN MATCHED AND ( src.EC_Meter_Attribute_Value IS  NULL
                               OR src.EC_Meter_Attribute_Value = '' )
                  THEN  DELETE;
     
END  


;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Tracking_Merge] TO [CBMSApplication]
GO
