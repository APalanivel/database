SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_BATCH_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.GET_DEAL_TICKET_BATCH_INFO_P

@dealTicketId int

AS
set nocount on
	SELECT	MAX(BATCH_GENERATION_DATE) AS BATCH_GENERATION_DATE
	FROM	RM_DEAL_TICKET_BATCH_MAP 
	WHERE 	RM_DEAL_TICKET_ID = @dealTicketId
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_BATCH_INFO_P] TO [CBMSApplication]
GO
