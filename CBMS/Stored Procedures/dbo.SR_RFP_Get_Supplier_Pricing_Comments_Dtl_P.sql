SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.SR_RFP_Get_Supplier_Pricing_Comments_Dtl_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
@vendor_name		VARCHAR(100)
@Commodity_Name		VARCHAR(50)
@Summit_Begin_date	DATE
@Summit_End_date	DATE
@RFPID				INT				 NULL     	          	


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
EXEC SR_RFP_Get_Supplier_Pricing_Comments_Dtl_P 'Constellation New Energy', 'Natural Gas', '04/01/2009', '06/30/2010', 2031
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SSR			Sharad Srivastava

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	 SSR       	06/30/2010	Created
	 LEC		10/18/2010  Added the filter to exclude any accounts that were marked as is_deleted = 1
							Added    and sr_sendsupp.IS_BID_GROUP = 1 and 0 filters
	
******/
CREATE PROC [dbo].[SR_RFP_Get_Supplier_Pricing_Comments_Dtl_P]
       @vendor_name VARCHAR(100)
     , @Commodity_Name VARCHAR(50)
     , @Summit_Begin_date DATE
     , @Summit_End_date DATE
     , @RFPID INT = NULL
AS 
       BEGIN     
    
             SET NOCOUNT ON ;    
    
             WITH CTE_RFP_ID
                    AS (
                         SELECT
                           scvm.sr_rfp_id
                         , scvm.VENDOR_ID
                         FROM
                           sr_rfp_supplier_contact_vendor_map scvm
                           JOIN sr_rfp_send_supplier ss
                              ON scvm.sr_rfp_supplier_contact_vendor_map_id = ss.sr_rfp_supplier_contact_vendor_map_id
                           JOIN SR_SUPPLIER_CONTACT_INFO suppcont
                              ON scvm.sr_supplier_contact_info_id = suppcont.SR_SUPPLIER_CONTACT_INFO_ID
                           JOIN SR_RFP_SEND_SUPPLIER_LOG srsl
                              ON scvm.SR_RFP_ID = srsl.SR_RFP_ID
                         WHERE
                           srsl.GENERATION_DATE BETWEEN @Summit_Begin_date
                                                AND     @Summit_End_date
                           AND suppcont.USER_INFO_ID IN (
                           SELECT
                              contact_info.user_info_id
                           FROM
                              SR_SUPPLIER_CONTACT_INFO contact_info
                              JOIN sr_supplier_contact_vendor_map vendor_map
                                 ON contact_info.SR_SUPPLIER_CONTACT_INFO_ID = vendor_map.SR_SUPPLIER_CONTACT_INFO_ID
                              JOIN VENDOR v
                                 ON v.VENDOR_ID = vendor_map.VENDOR_ID
                           WHERE
                              v.VENDOR_NAME = @vendor_name
                              AND vendor_map.IS_PRIMARY = 1 )
                         GROUP BY
                           scvm.sr_rfp_id
                         , scvm.VENDOR_ID
                         , ss.DUE_DATE
                       ) ,
                  CTE_Bid_Grp_Acct_All
                    AS (
                         SELECT
                           sr_acct.SR_RFP_ID
                         , sr_acct.ACCOUNT_ID
                         , sr_bid_grp.GROUP_NAME
                         , sr_acctterm.NO_OF_MONTHS
                         , sr_acctterm.SR_RFP_ACCOUNT_TERM_ID
                         , sr_acct.SR_RFP_ACCOUNT_ID
                         , rfp.COMMODITY_TYPE_ID
                         , sr_sendsupp.DUE_DATE
                         , sr_acctterm.SR_ACCOUNT_GROUP_ID
                         FROM
                           CTE_RFP_ID Rfp_tbl
                           JOIN SR_RFP_ACCOUNT sr_acct
                              ON Rfp_tbl.SR_RFP_ID = sr_acct.SR_RFP_ID
                           JOIN SR_RFP rfp
                              ON rfp.SR_RFP_ID = Rfp_tbl.SR_RFP_ID
                           JOIN SR_RFP_ACCOUNT_TERM sr_acctterm
                              ON sr_acctterm.SR_ACCOUNT_GROUP_ID = sr_acct.SR_RFP_BID_GROUP_ID
                           JOIN SR_RFP_BID_GROUP sr_bid_grp
                              ON sr_bid_grp.SR_RFP_BID_GROUP_ID = sr_acct.SR_RFP_BID_GROUP_ID
                           JOIN SR_RFP_SEND_SUPPLIER sr_sendsupp
                              ON sr_sendsupp.SR_ACCOUNT_GROUP_ID = sr_acctterm.SR_ACCOUNT_GROUP_ID
                         WHERE
                           sr_acctterm.IS_BID_GROUP = 1
                               
                             and sr_sendsupp.IS_BID_GROUP = 1
                           and IS_DELETED != 1
                         UNION
                         SELECT
                           sr_acct.SR_RFP_ID
                         , sr_acct.ACCOUNT_ID
                         , sr_bid_grp.GROUP_NAME
                         , sr_acctterm.NO_OF_MONTHS
                         , sr_acctterm.SR_RFP_ACCOUNT_TERM_ID
                         , sr_acct.SR_RFP_ACCOUNT_ID
                         , rfp.COMMODITY_TYPE_ID
                         , sr_sendsupp.DUE_DATE
                         , sr_acctterm.SR_ACCOUNT_GROUP_ID
                         FROM
                           CTE_RFP_ID Rfp_tbl
                           JOIN SR_RFP_ACCOUNT sr_acct
                              ON Rfp_tbl.SR_RFP_ID = sr_acct.SR_RFP_ID
                           JOIN SR_RFP rfp
                              ON rfp.SR_RFP_ID = Rfp_tbl.SR_RFP_ID
                           JOIN SR_RFP_ACCOUNT_TERM sr_acctterm
                              ON sr_acctterm.SR_ACCOUNT_GROUP_ID = sr_acct.SR_RFP_ACCOUNT_ID
                           JOIN SR_RFP_SEND_SUPPLIER sr_sendsupp
                              ON sr_sendsupp.SR_ACCOUNT_GROUP_ID = sr_acctterm.SR_ACCOUNT_GROUP_ID
                           LEFT JOIN SR_RFP_BID_GROUP sr_bid_grp
                              ON sr_bid_grp.SR_RFP_BID_GROUP_ID = sr_acct.SR_RFP_BID_GROUP_ID
                         WHERE
                           sr_acctterm.IS_BID_GROUP = 0
                              and sr_sendsupp.IS_BID_GROUP = 0
                           and IS_DELETED != 1
                       ) ,
                  CTE_final_Rpd_Dtl
                    AS (
                         SELECT
                           cte_bid.SR_RFP_ID
                         , cl.CLIENT_NAME
                         , ad.CITY
                         , st.STATE_NAME
                         , a.ACCOUNT_NUMBER
                         , cte_bid.GROUP_NAME
                         , ven.VENDOR_NAME Utility
                         , com.Commodity_Name
                         , cte_bid.NO_OF_MONTHS
                         , sr_pprd.PRODUCT_NAME
                         , dbo.SR_RFP_FN_GET_SUPPLIER_COMMENTS(sr_bid.SR_RFP_BID_ID) PRICE_RESPONSE_COMMENTS
                         , sr_delFuel.FUEL_IN_BID_VALUE
                         , v.VENDOR_NAME
                         , srcheck.NEW_CONTRACT_ID
                         , supp.VENDOR_NAME NewSupplierVendor
                         , srcheck.CONTRACT_START_DATE
                         , srcheck.CONTRACT_END_DATE
                         , con.CONTRACT_PRICING_SUMMARY
                         , con.ED_CONTRACT_NUMBER
                         , cte_bid.DUE_DATE [BidDueDate]
                         , ent_res.ENTITY_NAME Reason
                         , sr_nowin.COMMENTS Comments
                         FROM
                           CTE_RFP_ID Rfp_tbl
                           JOIN CTE_Bid_Grp_Acct_All cte_bid
                              ON cte_bid.SR_RFP_ID = Rfp_tbl.SR_RFP_ID
                           JOIN SR_RFP_TERM_PRODUCT_MAP sr_prdmap
                              ON sr_prdmap.SR_RFP_ACCOUNT_TERM_ID = cte_bid.SR_RFP_ACCOUNT_TERM_ID
                           JOIN SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP svmap
                              ON svmap.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = sr_prdmap.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                           JOIN SR_SUPPLIER_CONTACT_INFO sr_supcont
                              ON sr_supcont.SR_SUPPLIER_CONTACT_INFO_ID = svmap.SR_SUPPLIER_CONTACT_INFO_ID
                           JOIN ACCOUNT a
                              ON A.ACCOUNT_ID = cte_bid.ACCOUNT_ID
                           JOIN SITE s
                              ON s.SITE_ID = a.SITE_ID
                           JOIN Client cl
                              ON cl.CLIENT_ID = s.Client_ID
                           JOIN ADDRESS ad
                              ON ad.address_ID = s.PRIMARY_ADDRESS_ID
                           JOIN STATE st
                              ON st.STATE_ID = ad.STATE_ID
                           JOIN commodity com
                              ON com.Commodity_Id = cte_bid.COMMODITY_TYPE_ID
                           JOIN Vendor ven
                              ON ven.vendor_id = a.VENDOR_ID
                           LEFT JOIN SR_RFP_REASON_NOT_WINNING sr_nowin
                              ON sr_nowin.SR_ACCOUNT_GROUP_ID = cte_bid.SR_ACCOUNT_GROUP_ID
                                 and sr_nowin.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = svmap.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                           LEFT JOIN SR_RFP_REASON_NOT_WINNING_MAP sr_notwinMap
                              ON sr_notwinMap.SR_RFP_REASON_NOT_WINNING_ID = sr_nowin.SR_RFP_REASON_NOT_WINNING_ID
                           LEFT JOIN ENTITY ent_res
                              ON ent_res.ENTITY_ID = sr_notwinMap.NOT_WINNING_REASON_TYPE_ID
                           LEFT JOIN SR_RFP_SELECTED_PRODUCTS sr_selprd
                              ON sr_selprd.SR_RFP_SELECTED_PRODUCTS_ID = sr_prdmap.SR_RFP_SELECTED_PRODUCTS_ID
                           LEFT JOIN SR_RFP_PRICING_PRODUCT sr_pprd
                              ON sr_pprd.SR_RFP_PRICING_PRODUCT_ID = sr_selprd.PRICING_PRODUCT_ID
                           LEFT JOIN SR_RFP_BID sr_bid
                              ON sr_bid.SR_RFP_BID_ID = sr_prdmap.SR_RFP_BID_ID
                           LEFT JOIN SR_RFP_SUPPLIER_SERVICE sr_serSup
                              ON sr_serSup.SR_RFP_SUPPLIER_SERVICE_ID = sr_bid.SR_RFP_SUPPLIER_SERVICE_ID
                           LEFT JOIN SR_RFP_DELIVERY_FUEL sr_delFuel
                              ON sr_delFuel.SR_RFP_DELIVERY_FUEL_ID = sr_serSup.SR_RFP_DELIVERY_FUEL_ID
                           LEFT JOIN sr_rfp_checklist srcheck
                              ON srcheck.SR_RFP_ACCOUNT_ID = cte_bid.SR_RFP_ACCOUNT_ID
                           LEFT JOIN CONTRACT con
                              ON con.contract_id = srcheck.NEW_CONTRACT_ID
                           LEFT JOIN VENDOR v
                              ON v.VENDOR_ID = svmap.VENDOR_ID
                           LEFT JOIN VENDOR supp
                              ON supp.VENDOR_ID = srcheck.NEW_SUPPLIER_ID
                         WHERE
                           com.Commodity_Name = @Commodity_Name
                           AND ( @RFPID IS NULL
                                 OR Rfp_tbl.SR_RFP_ID = @RFPID
                               )
                         GROUP BY
                           cte_bid.SR_RFP_ID
                         , cl.CLIENT_NAME
                         , ad.CITY
                         , st.STATE_NAME
                         , a.ACCOUNT_NUMBER
                         , cte_bid.GROUP_NAME
                         , ven.VENDOR_NAME
                         , com.Commodity_Name
                         , cte_bid.NO_OF_MONTHS
                         , sr_pprd.PRODUCT_NAME
                         , v.VENDOR_NAME
                         , sr_bid.SR_RFP_BID_ID
                         , sr_delFuel.FUEL_IN_BID_VALUE
                         , srcheck.NEW_CONTRACT_ID
                         , supp.VENDOR_NAME
                         , srcheck.CONTRACT_START_DATE
                         , srcheck.CONTRACT_END_DATE
                         , con.CONTRACT_PRICING_SUMMARY
                         , con.ED_CONTRACT_NUMBER
                         , cte_bid.DUE_DATE
                         , ent_res.ENTITY_NAME
                         , sr_nowin.COMMENTS
                       )
                  SELECT
                     SR_RFP_ID [RFP ID]
                   , CLIENT_NAME [Client]
                   , CITY [City]
                   , STATE_NAME [State]
                   , ACCOUNT_NUMBER
                   , GROUP_NAME [BID Group Name]
                   , Utility
                   , Commodity_Name [Commodity]
                   , NO_OF_MONTHS [ NumberofMonths]
                   , PRODUCT_NAME [Product]
                   , PRICE_RESPONSE_COMMENTS [Price Response]
                   , VENDOR_NAME [Supplier]
                   , BidDueDate [Bid Due Date]
                   , ED_CONTRACT_NUMBER [NEW_CONTRACT_ID]
                   , NewSupplierVendor [New Supplier]
                   , CONTRACT_START_DATE [Contract Start Date]
                   , CONTRACT_END_DATE [Contract End Date]
                   , CONTRACT_PRICING_SUMMARY [Contract Pricing Summary]
                   , Reason
                   , Comments
                  FROM
                     CTE_final_Rpd_Dtl cte_rpt_dt
                  GROUP BY
                     SR_RFP_ID
                   , CLIENT_NAME
                   , CITY
                   , STATE_NAME
                   , ACCOUNT_NUMBER
                   , GROUP_NAME
                   , Commodity_Name
                   , Utility
                   , NO_OF_MONTHS
                   , PRODUCT_NAME
                   , PRICE_RESPONSE_COMMENTS
                   , VENDOR_NAME
                   , BidDueDate
                   , ED_CONTRACT_NUMBER
                   , NewSupplierVendor
                   , CONTRACT_START_DATE
                   , CONTRACT_END_DATE
                   , CONTRACT_PRICING_SUMMARY
                   , Reason
                   , Comments  
      
       END     
    
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Get_Supplier_Pricing_Comments_Dtl_P] TO [CBMSApplication]
GO
