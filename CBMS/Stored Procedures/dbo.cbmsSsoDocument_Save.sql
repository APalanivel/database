SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoDocument_Save

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@sso_document_id	int       	          	
	@document_title	varchar(600)	          	
	@document_description	varchar(4000)	null      	
	@category_type_id	int       	          	
	@document_reference_date	datetime  	          	
	@is_special    	bit       	          	
	@cbms_image_id 	int       	          	
	@cem_homepage  	bit       	null      	
	@is_custom_report	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE         PROCEDURE [dbo].[cbmsSsoDocument_Save]
	( @MyAccountId int
	, @sso_document_id int
	, @document_title varchar(600)
	, @document_description varchar(4000)=null
	, @category_type_id int
	, @document_reference_date datetime
	, @is_special bit
	, @cbms_image_id int
	, @cem_homepage bit = null
	, @is_custom_report bit
	)
AS
BEGIN

	set nocount on

	  declare @this_id int

	      set @this_id = @sso_document_id

	if @this_id is null
	begin

		insert into sso_document
			( 
			document_title
			, document_description
			, category_type_id
			, document_reference_date
			, is_special	
			, cbms_image_id
			, cem_homepage
			, is_CustomReport
			 )
		 values
			(  
			@document_title
			, @document_description
			, @category_type_id
			, @document_reference_date
			, @is_special
			, @cbms_image_id
			, @cem_homepage
			, @is_custom_report
			)

		set @this_id = @@IDENTITY

		exec cbmsEntityAudit_Log @MyAccountId, 'sso_document', @This_Id, 'Insert'

	end
	else
	begin

		   update sso_document
		      set document_title = @document_title
			, document_description = @document_description
			, category_type_id = @category_type_id
			, document_reference_date = @document_reference_date
			, is_special = @is_special
			, cbms_image_id = @cbms_image_id
			, cem_homepage = @cem_homepage
			, is_CustomReport = @is_custom_report
		    where sso_document_id = @this_id

		exec cbmsEntityAudit_Log @MyAccountId, 'sso_document', @This_Id, 'Edit'

	end

	exec cbmsSsoDocument_Get @MyAccountId, @this_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_Save] TO [CBMSApplication]
GO
