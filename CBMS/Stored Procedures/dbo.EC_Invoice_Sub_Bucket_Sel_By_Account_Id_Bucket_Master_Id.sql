SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.EC_Invoice_Sub_Bucket_Sel_By_Account_Id_Bucket_Master_Id

DESCRIPTION:
		Script to get available sub buckets for that account's state and commodity for which invoice is posted

INPUT PARAMETERS:
	Name					DataType	Default		Description
----------------------------------------------------------------
	@Account_Id			INT
    @Bucket_Master_Id	INT
	

OUTPUT PARAMETERS:
	Name					DataType	Default		Description
----------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.EC_Invoice_Sub_Bucket_Sel_By_Account_Id_Bucket_Master_Id 1, 70	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR       	2015-05-29	Created for AS400

******/
CREATE PROCEDURE [dbo].[EC_Invoice_Sub_Bucket_Sel_By_Account_Id_Bucket_Master_Id]
      ( 
       @Account_Id INT
      ,@Bucket_Master_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
           ,eisbm.Sub_Bucket_Name
      FROM
            dbo.EC_Invoice_Sub_Bucket_Master eisbm
            INNER JOIN core.Client_Hier_Account cha
                  ON cha.Meter_State_Id = eisbm.State_Id
      WHERE
            eisbm.Bucket_Master_Id = @Bucket_Master_Id
            AND cha.Account_Id = @Account_Id
      GROUP BY
            eisbm.EC_Invoice_Sub_Bucket_Master_Id
           ,eisbm.Sub_Bucket_Name
      
                  
END;
;
GO
GRANT EXECUTE ON  [dbo].[EC_Invoice_Sub_Bucket_Sel_By_Account_Id_Bucket_Master_Id] TO [CBMSApplication]
GO
