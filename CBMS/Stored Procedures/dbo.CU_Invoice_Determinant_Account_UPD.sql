SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.CU_Invoice_Determinant_Account_UPD 
     
     
DESCRIPTION:  
	This object is used update the existing record during stage-3 process for CU_INVOICE_Determinant_ACCOUNT
    
INPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
	 @Account_Id		INT
     @CU_Invoice_Id		INT
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
     
	EXEC dbo.CU_Invoice_Determinant_Account_UPD 446513, 6953750
    EXEC dbo.CU_Invoice_Determinant_Account_UPD 446513, 6957572
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------

SKA	Shobhit Kumar Agrawal

Initials	Date		Modification
------------------------------------------------------------
SKA			06/07/2010	Created
HG			06/15/2011	MAINT-658 fixed the code to update the account_id only for the determinant commodity mapped with the Account.
******/

CREATE PROCEDURE dbo.CU_Invoice_Determinant_Account_UPD
      @Account_Id AS INT
     ,@CU_Invoice_Id AS INT
AS 
BEGIN

      SET NOCOUNT ON

      UPDATE
            cida
      SET   
            cida.ACCOUNT_ID = @Account_Id
      FROM
            dbo.CU_Invoice_Determinant cid
            INNER JOIN dbo.CU_Invoice_Determinant_Account cida
                  ON cida.CU_INVOICE_DETERMINANT_ID = cid.CU_INVOICE_DETERMINANT_ID
      WHERE
            cid.CU_INVOICE_ID = @CU_Invoice_Id
            AND EXISTS ( SELECT
                              1
                         FROM
                              core.Client_Hier_Account cha
                         WHERE
                              cha.Account_id = @Account_Id
                              AND cha.Commodity_Id = cid.Commodity_Type_id ) 

END
GO

GRANT EXECUTE ON  [dbo].[CU_Invoice_Determinant_Account_UPD] TO [CBMSApplication]
GO