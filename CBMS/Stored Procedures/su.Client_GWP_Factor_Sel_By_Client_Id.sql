SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
	Su.Client_GWP_Factor_Sel_By_Client_Id  

DESCRIPTION:    
	Used to get UOM Conversion from COMMODITY_UOM_CONVERSION table  

INPUT PARAMETERS:    
Name      DataType Default Description    
------------------------------------------------------------    
 @CLIENT_ID INT  
          
OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    
USAGE EXAMPLES:    
------------------------------------------------------------  

EXEC Su.Client_GWP_Factor_Sel_By_Client_Id  

AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
GB   Geetansu Behera    
CMH  Chad Hattabaugh     
HG   Hari   
SKA  Shobhit Kr Agrawal  

MODIFICATIONS     
Initials Date  Modification    
------------------------------------------------------------    
GB				Created  
SKA   17-JUL-09 Modified  as per coding standard
SKA	  20 Aug 09 Added order by clause to display seq

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [Su].[Client_GWP_Factor_Sel_By_Client_Id]  
	@CLIENT_ID INT  
AS   

BEGIN  
  
SET NOCOUNT ON  
  
  
 SELECT	@CLIENT_ID AS CLIENT_ID,  
		c.CODE_ID AS Emmision_CD,  
		gwpfactor.CO2E_FACTOR,  
		C.CODE_VALUE AS Pollutant  
 FROM dbo.CODE c
 inner join Codeset cs ON c.Codeset_Id=cs.Codeset_Id  
  LEFT OUTER JOIN Su.CLIENT_GWP_FACTOR gwpfactor  
	ON gwpfactor.Emission_Cd = c.CODE_ID  
		AND gwpfactor.CLIENT_ID = @CLIENT_ID
 WHERE cs.Std_Column_Name = 'POLLUTANT_CD' 
 ORDER BY  c.display_seq  

END
GO
GRANT EXECUTE ON  [su].[Client_GWP_Factor_Sel_By_Client_Id] TO [CBMSApplication]
GO
