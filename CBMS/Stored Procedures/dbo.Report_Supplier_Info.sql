
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:  Report_Supplier_Info

        
DESCRIPTION:        
	To get the supplier vendor information
		Sproc would be running from prod copy ( excel exporter job)

INPUT PARAMETERS:        
 Name					DataType		Default		Description        
-------------------------------------------------------------------------------------------------------  

        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
     
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
  
  	EXEC dbo.Report_Supplier_Info 

  User_Info
 
        
AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------------------------------------------------       
 HG			Hariharasuthan Ganesan

MODIFICATIONS
 Initials	Date			Modification        
------------------------------------------------------------------------------------------------------       
 HG			2015-07-17		Created for prod excel export job requirement
 LEC		2017-07-31		Modified to allow for changes that were made to embedded sproc SR_SAD_GET_USER_LIST_FOR_VENDOR_P 
******/
CREATE PROCEDURE [dbo].[Report_Supplier_Info]
AS
BEGIN

      SET NOCOUNT ON;

      CREATE TABLE #Supplier_Vendor
            (
             SR_Supplier_Contact_Info_Id INT
            ,Vendor_Name NVARCHAR(255)
            ,UserName VARCHAR(30)
            ,Last_Name VARCHAR(40)
            ,First_Name VARCHAR(40)
            ,Email_Address VARCHAR(150)
            ,State NVARCHAR(MAX)
            ,Country_Id INT
            ,Country_Name VARCHAR(30)
            ,Commodity_Type_Id INT
            ,Commodity_Name VARCHAR(40)
            ,Row_Num INT IDENTITY(1, 1) );

      INSERT      INTO #Supplier_Vendor
                  (SR_Supplier_Contact_Info_Id
                  ,Vendor_Name
                  ,UserName
                  ,Last_Name
                  ,First_Name
                  ,Email_Address
                  ,State
                  ,COUNTRY_ID
                  ,Country_Name
                  ,Commodity_Type_Id
                  ,Commodity_Name )
                  EXEC SR_SAD_GET_USER_LIST_FOR_VENDOR_P
                        @username = N''
                       ,@lastname = N''
                       ,@firstname = N''
                       ,@vendorid = -1
                       ,@contactid = 0;

      SELECT
            Vendor_Name AS Vendor
           ,UserName AS [User Name]
           ,Last_Name AS [Last Name]
           ,First_Name AS [First Name]
           ,Email_Address AS Email
           ,State AS [State]
      FROM
            #Supplier_Vendor

END;

;
GO

GRANT EXECUTE ON  [dbo].[Report_Supplier_Info] TO [CBMS_SSRS_Reports]
GO
