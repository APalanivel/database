SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
      dbo.CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id
   
 DESCRIPTION:   
      Gets the  flag invoice has data in cu_invoice_charge or cu_invoice_determinant table or not..
      
 INPUT PARAMETERS:  
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 @Cu_Invoice_Id		INT

 OUTPUT PARAMETERS:
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 USAGE EXAMPLES:  
---------------------------------------------------------------------------- 

EXEC dbo.CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id
  @Cu_Invoice_Id = 2234567
    

EXEC dbo.CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id
    @Cu_Invoice_Id = 75251764

EXEC dbo.CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id
    @Cu_Invoice_Id = 75251765 

EXEC dbo.CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id
    @Cu_Invoice_Id = 1025 

	
AUTHOR INITIALS:
Initials	Name
---------------------------------------------------------------------------- 
 NR			Narayana Reddy
 

 MODIFICATIONS   
 Initials	Date			Modification
----------------------------------------------------------------------------
 NR        2019-06-14		Created for SE2017-722.

******/


CREATE PROCEDURE [dbo].[CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id]
    (
        @Cu_Invoice_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Is_Charge_And_Determinant_Exists BIT = 0;



        SELECT
            @Is_Charge_And_Determinant_Exists = 1
        FROM
            dbo.CU_INVOICE_DETERMINANT cid
        WHERE
            cid.CU_INVOICE_ID = @Cu_Invoice_Id;


        SELECT
            @Is_Charge_And_Determinant_Exists = 1
        FROM
            dbo.CU_INVOICE_CHARGE cic
        WHERE
            cic.CU_INVOICE_ID = @Cu_Invoice_Id;


        SELECT  @Is_Charge_And_Determinant_Exists AS Is_Charge_And_Determinant_Exists;


    END;

GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_And_Determinant_Exists_Check_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
