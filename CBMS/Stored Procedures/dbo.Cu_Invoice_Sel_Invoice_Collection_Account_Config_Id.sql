SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
 NAME: dbo.Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id                    
                                
 DESCRIPTION:                                
   To get the details of invoice for the given account_config                 
                                
 INPUT PARAMETERS:                  
                             
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
@Invoice_Collection_Account_Config_Id INT  NULL         
                                      
 OUTPUT PARAMETERS:                  
                                   
 Name                        DataType         Default       Description                
---------------------------------------------------------------------------------------------------------------              
                                
 USAGE EXAMPLES:                                    
---------------------------------------------------------------------------------------------------------------                                    
            
        
  EXEC dbo.Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id         
      4813, '1/1/2012', '1/1/2017'  
              
        
                               
 AUTHOR INITIALS:                
               
 Initials              Name                
---------------------------------------------------------------------------------------------------------------                              
 RKV                  Ravi Kumar Vegesna        
                                 
 MODIFICATIONS:              
                  
 Initials              Date             Modification              
---------------------------------------------------------------------------------------------------------------              
    RKV    2017-01-25  Created For Invoice_Collection.                 
                               
******/                         
                        
CREATE PROCEDURE [dbo].[Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id]
      (
       @Invoice_Collection_Account_Config_Id INT
      ,@Invoice_Start_dt date
      ,@Invoice_End_dt date )
AS
BEGIN                        
      SET NOCOUNT ON;            
             
             
      SELECT
            cism.CU_INVOICE_ID
           ,cism.SERVICE_MONTH
           ,cism.Begin_Dt
           ,cism.End_Dt
           ,ci.IS_PROCESSED
      FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON icac.Account_Id = cism.Account_ID
            INNER JOIN dbo.CU_INVOICE ci
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND ISNULL(cism.SERVICE_MONTH, '') <> ''
            AND ( ( cism.Begin_Dt BETWEEN @Invoice_Start_dt
                                  AND     @Invoice_End_dt )
                  OR ( cism.End_Dt BETWEEN @Invoice_Start_dt
                                   AND     @Invoice_End_dt ) )
      ORDER BY
            cism.Begin_Dt
           ,cism.SERVICE_MONTH

END;    
;

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Sel_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
