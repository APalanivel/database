SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SAVE_EMAIL_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@from_address varchar(100),
	@sr_rfp_id int,
	@to_address varchar(100),
	@cc_address varchar(1000),
	@subject text,
	@content text,
	@email_type varchar(200),
	@cbms_image_id int,
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@sr_rfp_email_log_id int out


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_SAVE_EMAIL_P
--	@user_id =1,
--	@session_id =-1,
--	@from_address ='TestData@TestData.com',
--	@sr_rfp_id = 5,
--	@to_address ='TestData@TestData.com',
--	@cc_address ='TestData@TestData.com',
--	@subject ='Test Data',
--	@content = 'Test Data',
--	@email_type = 'Email-001-DoubleBuyWarning_System_SA',
--	@cbms_image_id = 1,
--	@sr_rfp_email_log_id = null

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_SAVE_EMAIL_P
	@user_id varchar(10),
	@session_id varchar(20),
	@from_address varchar(100),
	@sr_rfp_id int,
	@to_address varchar(100),
	@cc_address varchar(1000),
	@subject text,
	@content text,
	@email_type varchar(200),
	@cbms_image_id int,
	@sr_rfp_email_log_id int out
	AS
	
SET NOCOUNT ON
	
	
	declare @email_type_id int
	select @email_type_id = entity_id from entity(nolock) where entity_type = 1033 and entity_name = @email_type

	insert into sr_rfp_email_log values(	@from_address, 
						@sr_rfp_id,
						@to_address,
						@cc_address,
						@subject,
						@content,
						@email_type_id,
						getdate()
					)

	select @sr_rfp_email_log_id = SCOPE_IDENTITY()

	if @cbms_image_id > 0
	begin
		insert into SR_RFP_EMAIL_ATTACHMENT values(@sr_rfp_email_log_id, @cbms_image_id)
	end


	return
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_EMAIL_P] TO [CBMSApplication]
GO
