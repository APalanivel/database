SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Supplier_Account_Dtls_Sel_By_Account_Exception_Id                  
                            
 DESCRIPTION:        
  Get the Suppler account associated invoices count based on Exception Id.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Account_Exception_Id   INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  
 
 EXEC dbo.Supplier_Account_Dtls_Sel_By_Account_Exception_Id  376706    
  
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2020-06-16      Created for SE2017- 981                         
                           
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Dtls_Sel_By_Account_Exception_Id]
    (
        @Account_Exception_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE
            @New_Exception_Status_Cd INT
            , @In_Progress_Exception_Status_Cd INT;




        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @In_Progress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                                     END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd'
            AND c.Code_Value IN ( 'New', 'In Progress' );


        SELECT
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id AS Utility_Account_Id
            , Uti_cha.Account_Number AS Utility_Account_Number
            , Sup_Cha.Account_Id AS Supplier_Account_Id
            , Sup_Cha.Account_Number AS Supplier_Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31') AS Supplier_Account_End_Dt
            , Sup_Cha.Meter_Number
            , @Account_Exception_Id AS Account_Exception_Id
            , ISNULL(COUNT(DISTINCT aecim.Cu_Invoice_Id), 0) AS Invoices
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account Uti_cha
                ON Uti_cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account Sup_Cha
                ON Uti_cha.Meter_Id = Sup_Cha.Meter_Id
                   AND  Uti_cha.Client_Hier_Id = Sup_Cha.Client_Hier_Id
            INNER JOIN dbo.Account_Exception ae
                ON ae.Account_Id = Sup_Cha.Account_Id
            LEFT OUTER JOIN dbo.Account_Exception_Cu_Invoice_Map aecim
                ON aecim.Account_Exception_Id = ae.Account_Exception_Id
                   AND  ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
        WHERE
            Uti_cha.Account_Type = 'Utility'
            AND Sup_Cha.Account_Type = 'Supplier'
            AND ae.Account_Exception_Id = @Account_Exception_Id
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
        GROUP BY
            ch.Client_Hier_Id
            , ch.Client_Id
            , ch.Site_Id
            , ch.Sitegroup_Id
            , Uti_cha.Account_Id
            , Uti_cha.Account_Number
            , Sup_Cha.Account_Id
            , Sup_Cha.Account_Number
            , Sup_Cha.Supplier_Account_begin_Dt
            , NULLIF(Sup_Cha.Supplier_Account_End_Dt, '9999-12-31')
            , Sup_Cha.Meter_Number;


    END;


GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Dtls_Sel_By_Account_Exception_Id] TO [CBMSApplication]
GO
