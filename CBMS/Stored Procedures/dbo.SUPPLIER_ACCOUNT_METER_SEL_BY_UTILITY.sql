SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:   DBO.SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY      
      
DESCRIPTION:      
 TO GET THE SUPPLIER_ACCOUNT_METER DETAILS BASED ON GIVEN CONTRACT ID WITH PAGINATION AND SORTING.      
      
INPUT PARAMETERS:      
NAME			DATATYPE	  DEFAULT  DESCRIPTION      
--------------------------------------------------------------------      
@CONTRACT_ID	INT      
@SITE_ID		INT			  NULL      
@CITY			VARCHAR(200)  NULL      
@STATE_ID		INT			  NULL      
@VENDOR_ID		INT			  NULL      
@ACCOUNT_NUMBER VARCHAR(50)   NULL      
@METER_NUMBER	VARCHAR(50)   NULL      
      
      
OUTPUT PARAMETERS:      
NAME			DATATYPE	  DEFAULT  DESCRIPTION      
--------------------------------------------------------------------      
USAGE EXAMPLES:      
--------------------------------------------------------------------      
	USE [CBMS_TK2]

	EXEC SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY 26046,null,null,null,null,null,null,'ACCOUNT_ID','ASC',1,25  
	EXEC SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY 26046,null,null,null,null,null,null,'CONTRACT_ID','DESC',1,25  
	EXEC SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY 76199,null,null,null,null,null,null,'CONTRACT_ID','DESC',1,25
    
AUTHOR INITIALS:      
INITIALS	NAME      
------------------------------------------------------------      
SKA			SHOBHIT KR AGRAWAL        
MGB			BHASKARAN GOPALAKRISHNAN      
HG			Hari      
NK			Nageswara Rao Kosuri  
PNR			Pandarinath
      
MODIFICATIONS      
INITIALS DATE		MODIFICATION      
------------------------------------------------------------      
SKA		21-Aug-09	Created         
MGB		21-AUG-09	MODIFIED (IN WHERE CONDITION OTHER PARAMETERS ARE NOT MANDATORY EXCEPT CONTRACT ID.       
HG		23-Oct-09	Fetching Supplier Account begin_dt and end_dt in place of contract begin_dt and end_dt.      
HG		27-Oct-09	MAP.IS_HISTORY=0 filter clause added.    
NK		11/06/2009  Get Supplier Processing Instructions    
NK		11/11/2009  Modified to incorporate watchlist group and Is Data Entry Only  
PNR		19-May-2010 Added 4 new Parameters for pagination and sorting
					Converted Static to dynamic as user has ability to choose Sorting on Columns
PNR		06/08/2010	For City,Account_Number & Meter_Number added single quate	as these are varchar column			
SKA		06/14/2010	Filter the records on the basis of utility account, instead of supplier account in where clause
 NR		2019-04-03	Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.		

*/


CREATE PROCEDURE [dbo].[SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY]
    (
        @CONTRACT_ID INT
        , @SITE_ID INT = NULL
        , @CITY VARCHAR(200) = NULL
        , @STATE_ID INT = NULL
        , @VENDOR_ID INT = NULL
        , @ACCOUNT_NUMBER VARCHAR(50) = NULL
        , @METER_NUMBER VARCHAR(50) = NULL
        , @SORTCOLUMN VARCHAR(200) = 'CONTRACT_ID'
        , @SORTINDEX VARCHAR(20) = 'ASC'
        , @STARTINDEX INT = 1
        , @ENDINDEX INT = 2147483647
    )
AS
    BEGIN

        SET NOCOUNT ON;
        DECLARE @Sql VARCHAR(8000);


        SET @Sql = '; WITH Client_Attribute_Tracking_Sel_By_Client_Site_Id 
		AS  
		(            
		  SELECT
				CON.CONTRACT_ID      
				,MAP.ACCOUNT_ID ACCOUNT_ID      
				,A.ACCOUNT_NUMBER SUPP_ACC_NUMBER      
				,M.METER_NUMBER METER_NUMBER      
				,M.METER_ID METER_ID      
				,a.Supplier_Account_Begin_Dt      
				,a.Supplier_Account_End_Dt      
				,UTILACC.ACCOUNT_NUMBER UTIL_ACCOUNT_NUMBER      
				,A.RA_WATCH_LIST RA_WATCH_LIST      
				,A.NOT_MANAGED NOT_MANAGED      
				,A.NOT_EXPECTED NOT_EXPECTED      
				,A.SERVICE_LEVEL_TYPE_ID SERVICE_LEVEL_TYPE_ID      
				,RATE.RATE_NAME RATENAME      
				,V.VENDOR_ID UTILITY_ID      
				,V.VENDOR_NAME UTILITY      
				,S.SITE_ID SITE_ID      
				,S.SITE_NAME SITENAME      
				,ADR.CITY CITY      
				,ST.STATE_ID STATE_ID      
				,ST.STATE_NAME STATE    
				,A.Is_Data_Entry_Only    
				
				     
		 FROM      
			dbo.CONTRACT CON      
				JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP MAP 
						ON CON.CONTRACT_ID = MAP.CONTRACT_ID      
				JOIN dbo.ACCOUNT A 
						ON A.ACCOUNT_ID = MAP.ACCOUNT_ID      
				JOIN dbo.METER M 
						ON MAP.METER_ID = M.METER_ID      
				JOIN dbo.ACCOUNT UTILACC 
						ON M.ACCOUNT_ID = UTILACC.ACCOUNT_ID      
				JOIN dbo.RATE RATE 
						ON RATE.RATE_ID = M.RATE_ID      
				JOIN dbo.VENDOR V 
						ON UTILACC.VENDOR_ID = V.VENDOR_ID      
				JOIN dbo.SITE S 
						ON UTILACC.SITE_ID = S.SITE_ID      
				JOIN dbo.ADDRESS ADR 
						ON ADR.ADDRESS_ID = S.PRIMARY_ADDRESS_ID      
				JOIN dbo.STATE ST 
						ON ST.STATE_ID = ADR.STATE_ID    
	  
		WHERE      
			CON.CONTRACT_ID = ' + CAST(@CONTRACT_ID AS VARCHAR);

        IF @SITE_ID IS NOT NULL
            SET @Sql = @Sql + ' AND (S.SITE_ID=' + CAST(@SITE_ID AS VARCHAR) + ')';
        IF @CITY IS NOT NULL
            SET @Sql = @Sql + ' AND (ADR.CITY= ''' + @CITY + ''')';
        IF @STATE_ID IS NOT NULL
            SET @Sql = @Sql + ' AND (ST.STATE_ID = ' + CAST(@STATE_ID AS VARCHAR) + ')';
        IF @VENDOR_ID IS NOT NULL
            SET @Sql = @Sql + ' AND (V.VENDOR_ID= ' + CAST(@VENDOR_ID AS VARCHAR) + ')';
        IF @ACCOUNT_NUMBER IS NOT NULL
            SET @Sql = @Sql + ' AND (UTILACC.ACCOUNT_NUMBER = ''' + @ACCOUNT_NUMBER + ''')';
        IF @METER_NUMBER IS NOT NULL
            SET @Sql = @Sql + ' AND (M.METER_NUMBER = ''' + @METER_NUMBER + ''')';

        SET @Sql = @Sql
                   + ' AND MAP.IS_HISTORY = 0      
  		),
  		
  		Cte_Fin
  		AS
  		(
  			SELECT  
				 ACCOUNT_ID       
				,SUPP_ACC_NUMBER       
				,METER_NUMBER        
				,METER_ID       
				,SUPPLIER_ACCOUNT_BEGIN_DT       
				,SUPPLIER_ACCOUNT_END_DT       
				,UTIL_ACCOUNT_NUMBER       
				,RA_WATCH_LIST       
				,NOT_MANAGED       
				,NOT_EXPECTED       
				,SERVICE_LEVEL_TYPE_ID       
				,RATENAME  
				,UTILITY_ID       
				,UTILITY       
				,SITE_ID       
				,SITENAME       
				,CITY       
				,STATE_ID       
				,STATE
				,IS_DATA_ENTRY_ONLY      
				
				,Row_Num=ROW_NUMBER() OVER ( ORDER BY ' + @SORTCOLUMN + SPACE(1) + @SORTINDEX
                   + ')
				,Total=COUNT(1) OVER() 
			FROM  
				Client_Attribute_Tracking_Sel_By_Client_Site_Id
		)';


        SET @Sql = @Sql
                   + ' 
			SELECT  
				 ACCOUNT_ID       
				,SUPP_ACC_NUMBER       
				,METER_NUMBER        
				,METER_ID       
				,SUPPLIER_ACCOUNT_BEGIN_DT       
				,SUPPLIER_ACCOUNT_END_DT       
				,UTIL_ACCOUNT_NUMBER       
				,RA_WATCH_LIST       
				,NOT_MANAGED       
				,NOT_EXPECTED       
				,SERVICE_LEVEL_TYPE_ID       
				,RATENAME  
				,UTILITY_ID       
				,UTILITY       
				,SITE_ID       
				,SITENAME       
				,CITY       
				,STATE_ID       
				,STATE
				,IS_DATA_ENTRY_ONLY      
				
				,Total   
			FROM  
				Cte_Fin
			WHERE  
				Row_Num BETWEEN ' + CAST(@STARTINDEX AS VARCHAR) + ' AND ' + CAST(@ENDINDEX AS VARCHAR);


        EXEC (@Sql);

    END;

GO
GRANT EXECUTE ON  [dbo].[SUPPLIER_ACCOUNT_METER_SEL_BY_UTILITY] TO [CBMSApplication]
GO
