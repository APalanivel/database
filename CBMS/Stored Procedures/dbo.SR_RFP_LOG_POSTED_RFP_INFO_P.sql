SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LOG_POSTED_RFP_INFO_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                       DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id                      varchar(10),
	@session_id                   varchar(20),
	@rfp_id                       int,
	@rfp_send_supplier_id         int	
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test insert
--exec dbo.SR_RFP_LOG_POSTED_RFP_INFO_P
--	@user_id = 17756 ,
--	@session_id = -1,
--	@rfp_id = 2046,
--	@rfp_send_supplier_id =33887


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LOG_POSTED_RFP_INFO_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int,
	@rfp_send_supplier_id int	
	AS
	
set nocount on
	
	
	INSERT INTO sr_rfp_send_supplier_log(
											sr_rfp_id, 
											sr_rfp_send_supplier_id, 
											generation_date, 
											posted_by
										)
								VALUES (
											@rfp_id, 
											@rfp_send_supplier_id, 
											getdate(), 
											@user_id
										)
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LOG_POSTED_RFP_INFO_P] TO [CBMSApplication]
GO
