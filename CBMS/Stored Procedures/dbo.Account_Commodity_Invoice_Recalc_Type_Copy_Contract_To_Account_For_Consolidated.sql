SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account_For_Consolidated  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
Name						DataType			Default				Description  
-------------------------------------------------------------------------------  
 @Account_Id				INT       
   
   
OUTPUT PARAMETERS:  
Name						DataType			Default				Description  
-------------------------------------------------------------------------------  
   
USAGE EXAMPLES:  
-------------------------------------------------------------------------------  
USE CBMS  
  
select Max(Account_Commodity_Invoice_Recalc_Type_Id)  
 from dbo.Account_Commodity_Invoice_Recalc_Type  --> 1692975  
 select top 1 *  
 from dbo.Account_Commodity_Invoice_Recalc_Type  order by Account_Commodity_Invoice_Recalc_Type_Id desc   
  
  
 
BEGIN TRAN   

SELECT * FROM  dbo.Account_Commodity_Invoice_Recalc_Type WHERE Account_Id =1384526
SELECT * FROM  dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =166968 AND crc.Is_Consolidated_Contract_Config = 1
SELECT * FROM   dbo.Valcon_Contract_Config vcc WHERE vcc.Contract_Id=166968 AND  vcc.Is_Config_Complete =1

EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account_For_Consolidated  
    @Account_Id = 1384526  
 ,@Contract_Id = 166968  
 , @User_Info_Id =49  

SELECT * FROM  dbo.Account_Commodity_Invoice_Recalc_Type WHERE Account_Id =1384526
SELECT * FROM  dbo.Contract_Recalc_Config crc WHERE crc.Contract_Id =166968 AND crc.Is_Consolidated_Contract_Config = 1
SELECT * FROM   dbo.Valcon_Contract_Config vcc WHERE vcc.Contract_Id=166968 AND  vcc.Is_Config_Complete =1
  
ROLLBACK TRAN  
  
AUTHOR INITIALS:  
 Initials		Name  
-------------------------------------------------------------------------------  
 NR				Narayana Reddy   
   
MODIFICATIONS  
 Initials		Date			Modification  
-------------------------------------------------------------------------------  
 NR				2020-03-24		Created For  MAINT - - To copy the recacl for consolidated Account.
******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account_For_Consolidated]
     (
         @Account_Id INT
         , @Contract_Id INT
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Valcon_Account_Config_Type_cd INT
            , @Contract_Source_Cd INT
            , @Valcon_Source_Type_Cd INT
            , @Valcon_Contract_Config_Id_Exists BIT = 0
            , @Account_Recalc_Exist BIT = 0
            , @Contract_Recalc_Exist BIT = 0
            , @Commodity_Id INT
            , @Contract_Start_Dt DATE
            , @Contract_End_Dt DATE;

        SELECT
            @Valcon_Account_Config_Type_cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Consolidated Account'
            AND cs.Codeset_Name = 'Valcon Config Type';

        SELECT
            @Valcon_Source_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Contract'
            AND cs.Codeset_Name = 'Valcon Source Type';


        SELECT
            @Commodity_Id = c.COMMODITY_TYPE_ID
            , @Contract_Start_Dt = c.CONTRACT_START_DATE
            , @Contract_End_Dt = c.CONTRACT_END_DATE
        FROM
            dbo.CONTRACT c
        WHERE
            c.CONTRACT_ID = @Contract_Id;



        SELECT  TOP 1
                @Valcon_Contract_Config_Id_Exists = 1
        FROM
            dbo.Valcon_Contract_Config vcc
        WHERE
            vcc.Contract_Id = @Contract_Id
            AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_cd
            AND vcc.Is_Config_Complete = 1;


        SELECT  TOP 1
                @Contract_Recalc_Exist = 1
        FROM
            dbo.Contract_Recalc_Config crc
        WHERE
            crc.Contract_Id = @Contract_Id
            AND crc.Is_Consolidated_Contract_Config = 1;




        SELECT  TOP 1
                @Account_Recalc_Exist = 1
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Commodity_ID = @Commodity_Id
            AND (   @Contract_Start_Dt BETWEEN acirt.Start_Dt
                                       AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  @Contract_End_Dt BETWEEN acirt.Start_Dt
                                         AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  acirt.Start_Dt BETWEEN @Contract_Start_Dt
                                       AND     @Contract_End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Contract_Start_Dt
                                                           AND     @Contract_End_Dt);




        DELETE
        acirt
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
            INNER JOIN dbo.Code c
                ON acirt.Invoice_Recalc_Type_Cd = c.Code_Id
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Commodity_ID = @Commodity_Id
            AND acirt.Start_Dt = '2005-01-01'
            AND acirt.End_Dt IS NULL
            AND c.Code_Value IN ( 'No Recalc', 'No Recalc Service' )
            AND @Valcon_Contract_Config_Id_Exists = 1
            AND @Contract_Recalc_Exist = 1
            AND @Account_Recalc_Exist = 1;



        SET @Account_Recalc_Exist = 0;



        SELECT  TOP 1
                @Account_Recalc_Exist = 1
        FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
        WHERE
            acirt.Account_Id = @Account_Id
            AND acirt.Commodity_ID = @Commodity_Id
            AND (   @Contract_Start_Dt BETWEEN acirt.Start_Dt
                                       AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  @Contract_End_Dt BETWEEN acirt.Start_Dt
                                         AND     ISNULL(acirt.End_Dt, '9999-12-31')
                    OR  acirt.Start_Dt BETWEEN @Contract_Start_Dt
                                       AND     @Contract_End_Dt
                    OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN @Contract_Start_Dt
                                                           AND     @Contract_End_Dt);







        IF (   @Account_Recalc_Exist = 0
               AND  @Contract_Recalc_Exist = 1
               AND  @Valcon_Contract_Config_Id_Exists = 1)
            BEGIN

                EXEC dbo.Valcon_Account_Config_Ins_Upd
                    @Account_Id = @Account_Id
                    , @Contract_Id = @Contract_Id
                    , @Source_Cd = @Valcon_Source_Type_Cd
                    , @Is_Config_Complete = 1
                    , @Comment_Id = NULL
                    , @User_Info_Id = @User_Info_Id;


                INSERT INTO dbo.Account_Commodity_Invoice_Recalc_Type
                     (
                         Account_Id
                         , Commodity_ID
                         , Start_Dt
                         , End_Dt
                         , Invoice_Recalc_Type_Cd
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                         , Determinant_Source_Cd
                         , Source_Cd
                     )
                SELECT
                    @Account_Id
                    , crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , NULL
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                    , crc.Determinant_Source_Cd
                    , @Valcon_Source_Type_Cd
                FROM
                    dbo.Contract_Recalc_Config crc
                WHERE
                    crc.Is_Consolidated_Contract_Config = 1
                    AND crc.Contract_Id = @Contract_Id
                    AND crc.Commodity_Id = @Commodity_Id
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Commodity_Invoice_Recalc_Type acr
                                       WHERE
                                            acr.Account_Id = @Account_Id
                                            AND acr.Commodity_ID = crc.Commodity_Id
                                            AND acr.Start_Dt = crc.Start_Dt
                                            AND ISNULL(acr.End_Dt, '9999-12-31') = ISNULL(crc.End_Dt, '9999-12-31'))
                GROUP BY
                    crc.Commodity_Id
                    , crc.Start_Dt
                    , crc.End_Dt
                    , crc.Determinant_Source_Cd;



                INSERT INTO dbo.Account_Outside_Contract_Term_Config
                     (
                         Account_Id
                         , Contract_Id
                         , OCT_Parameter_Cd
                         , OCT_Tolerance_Date_Cd
                         , OCT_Dt_Range_Cd
                         , Config_Start_Dt
                         , Config_End_Dt
                         , Created_Ts
                         , Created_User_Id
                         , Updated_User_Id
                         , Last_Change_Ts
                     )
                SELECT
                    @Account_Id
                    , @Contract_Id
                    , coctc.OCT_Parameter_Cd
                    , coctc.OCT_Tolerance_Date_Cd
                    , coctc.OCT_Dt_Range_Cd
                    , coctc.Config_Start_Dt
                    , coctc.Config_End_Dt
                    , GETDATE()
                    , @User_Info_Id
                    , @User_Info_Id
                    , GETDATE()
                FROM
                    dbo.Contract_Outside_Contract_Term_Config coctc
                    INNER JOIN dbo.Valcon_Contract_Config vcc
                        ON vcc.Valcon_Contract_Config_Id = coctc.Valcon_Contract_Config_Id
                WHERE
                    vcc.Contract_Id = @Contract_Id
                    AND vcc.Valcon_Account_Config_Type_Cd = @Valcon_Account_Config_Type_cd
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Outside_Contract_Term_Config aoc
                                       WHERE
                                            aoc.Account_Id = @Account_Id
                                            AND aoc.Contract_Id = @Contract_Id
                                            AND aoc.OCT_Parameter_Cd = coctc.OCT_Parameter_Cd);




            END;





    END;


GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Copy_Contract_To_Account_For_Consolidated] TO [CBMSApplication]
GO
