SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default		Description
-------------------------------------------------------------------
	@Rfp_Id INT
    @Sr_Rfp_Account_Id	VARCHAR(MAX)	NULL   	          	

OUTPUT PARAMETERS:
	Name				DataType		Default		Description
-------------------------------------------------------------------

USAGE EXAMPLES:
--------------------------------------------------------------------
 
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 1876
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 1069
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 3374
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 13258,'12105018'
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 13258,'12105019'
	EXEC dbo.Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account 13258,'12105018,12105019'

	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-19	Global Sourcing - Phase3 - GCS-531 Created

******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account]
      ( 
       @Rfp_Id INT
      ,@Sr_Rfp_Account_Id VARCHAR(MAX) = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
	
      WITH  Cte_Accs
              AS ( SELECT
                        isnull(rfpacc.SR_RFP_BID_GROUP_ID, rfpacc.SR_RFP_ACCOUNT_ID) AS SR_ACCOUNT_GROUP_ID
                       ,case WHEN rfpacc.SR_RFP_BID_GROUP_ID IS NOT NULL THEN 1
                             ELSE 0
                        END AS Is_Bid_Group
                   FROM
                        dbo.SR_RFP_ACCOUNT rfpacc
                   WHERE
                        rfpacc.SR_RFP_ID = @Rfp_Id
                        AND ( @Sr_Rfp_Account_Id IS NULL
                              OR EXISTS ( SELECT
                                                cast(Segments AS INT)
                                          FROM
                                                dbo.ufn_split(@Sr_Rfp_Account_Id, ',')
                                          WHERE
                                                cast(Segments AS INT) = rfpacc.SR_RFP_ACCOUNT_ID ) ))
            SELECT
                  vndr.VENDOR_NAME
                 ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Contact_Name
                 ,convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            FROM
                  dbo.SR_RFP_SEND_SUPPLIER srss
                  INNER JOIN dbo.ENTITY en
                        ON en.ENTITY_ID = srss.STATUS_TYPE_ID
                  INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP convndr
                        ON convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = srss.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                  INNER JOIN dbo.VENDOR vndr
                        ON convndr.VENDOR_ID = vndr.VENDOR_ID
                  INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                        ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = convndr.SR_SUPPLIER_CONTACT_INFO_ID
                  INNER JOIN dbo.USER_INFO ui
                        ON ssci.USER_INFO_ID = ui.USER_INFO_ID
                  INNER JOIN Cte_Accs accs
                        ON accs.SR_ACCOUNT_GROUP_ID = srss.SR_ACCOUNT_GROUP_ID
                           AND srss.IS_BID_GROUP = accs.Is_Bid_Group
            WHERE
                  en.ENTITY_NAME = 'Post'
                  AND ENTITY_TYPE = 1013
            GROUP BY
                  vndr.VENDOR_NAME
                 ,ui.FIRST_NAME + ' ' + ui.LAST_NAME
                 ,convndr.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
            
				
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Post_Supplier_Sel_By_Rfp_Account] TO [CBMSApplication]
GO
