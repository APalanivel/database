SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
Name:   dbo.[Invoice_Collection_Batch_Decider_By_Invoice_Collection_Account_Config_Id]             
                    
Description:                    
   Based on account_config_id it will return old logic or new one,    
    which will be used from app side and decide to use old logic or new one.            
                                 
 Input Parameters:                    
    Name         DataType   Default   Description                      
----------------------------------------------------------------------------------------                  
    @Invoice_Collection_Account_Config_Id       INT           
          
                                
       
 Output Parameters:                          
    Name        DataType   Default   Description                      
----------------------------------------------------------------------------------------                      
                    
 Usage Examples:                        
----------------------------------------------------------------------------------------         
       
 EXEC dbo.Invoice_Collection_Batch_Decider_By_Invoice_Collection_Account_Config_Id 1686      
         
Author Initials:                    
    Initials  Name                    
----------------------------------------------------------------------------------------                      
 RKV    Ravi Kumar vegesna          
                  
 Modifications:                    
    Initials        Date   Modification                    
----------------------------------------------------------------------------------------                      
    RKV    2020-05-11  Created For IC Tool revamp.               
******/  
  
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Decider_By_Invoice_Collection_Account_Config_Id]  
    (  
        @Invoice_Collection_Account_Config_Id INT = NULL  
    )
	WITH RECOMPILE  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        DECLARE @Invoice_Collection_Batch VARCHAR(50) = 'Old Batch';  
        DECLARE @Custom_Frequency_Cd INT;  
  
  
  
        SELECT  
            @Custom_Frequency_Cd = c.Code_Id  
        FROM  
            Code c  
            INNER JOIN dbo.Codeset cs  
                ON cs.Codeset_Id = c.Codeset_Id  
        WHERE  
            Code_Value = 'Custom'  
            AND cs.Codeset_Name = 'InvoiceFrequency';  
  
  
        SELECT  
            @Invoice_Collection_Batch = 'New Batch'  
		WHERE NOT EXISTS (   SELECT  
                                    1  
                               FROM  
                                    dbo.Invoice_collection_New_Batch_Clients icnbc )
		
		SELECT  
            @Invoice_Collection_Batch = 'New Batch'  
        FROM  
            dbo.Invoice_Collection_Account_Config icac  
            INNER JOIN Core.Client_Hier_Account cha  
                ON cha.Account_Id = icac.Account_Id  
            INNER JOIN Core.Client_Hier ch  
                ON ch.Client_Hier_Id = cha.Client_Hier_Id  
        WHERE  
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id  
            AND @Invoice_Collection_Batch = 'Old Batch'
			AND (   EXISTS (   SELECT  
                                    1  
                               FROM  
                                    dbo.Invoice_collection_New_Batch_Clients icnbc  
                               WHERE  
                                    icnbc.Client_Id = ch.Client_Id)  
                    OR  EXISTS (   SELECT  
                                        1  
                                   FROM  
                                        dbo.Account_Invoice_Collection_Frequency aicf  
                                   WHERE  
                                        aicf.Invoice_Frequency_Cd = @Custom_Frequency_Cd  
                                        AND aicf.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id));  
  
  
        SELECT  @Invoice_Collection_Batch Invoice_Collection_Batch;  
  
    END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Decider_By_Invoice_Collection_Account_Config_Id] TO [CBMSApplication]
GO
