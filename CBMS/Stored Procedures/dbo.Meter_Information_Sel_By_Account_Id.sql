SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Meter_Information_Sel_By_Account_Id]  
     
DESCRIPTION: 
	To Get Meter Information for Selected Account Id.
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						Utility Account
@StartIndex		INT			1			
@EndIndex		INT			2147483647
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  

	EXEC Meter_Information_Sel_By_Account_Id  30008 , 1,25
	
	EXEC Meter_Information_Sel_By_Account_Id  34970
	
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			25-MAY-10	CREATED  
*/

CREATE PROCEDURE [dbo].[Meter_Information_Sel_By_Account_Id]
     (
         @Account_Id INT
         , @StartIndex INT = 1
         , @EndIndex INT = 2147483647
     )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH Cte_Meter_Number_List
        AS (
               SELECT
                    METER_NUMBER
                    , ADDRESS_ID
                    , RATE_ID
                    , ROW_NUMBER() OVER (ORDER BY
                                             METER_NUMBER) Row_Num
                    , COUNT(1) OVER () Total_Rows
               FROM
                    dbo.METER
               WHERE
                    ACCOUNT_ID = @Account_Id
           )
        SELECT
            mtr.METER_NUMBER
            , com.Commodity_Name
            , com.Commodity_Id
            , r.RATE_NAME
            , adr.ADDRESS_LINE1
            , mtr.Total_Rows
        FROM
            Cte_Meter_Number_List mtr
            JOIN dbo.RATE r
                ON mtr.RATE_ID = r.RATE_ID
            JOIN dbo.Commodity com
                ON r.COMMODITY_TYPE_ID = com.Commodity_Id
            JOIN dbo.ADDRESS adr
                ON mtr.ADDRESS_ID = adr.ADDRESS_ID
        WHERE
            Row_Num BETWEEN @StartIndex
                    AND     @EndIndex;

    END;

GO
GRANT EXECUTE ON  [dbo].[Meter_Information_Sel_By_Account_Id] TO [CBMSApplication]
GO
