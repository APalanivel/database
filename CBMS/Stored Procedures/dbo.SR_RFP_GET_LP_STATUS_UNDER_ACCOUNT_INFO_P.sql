SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_LP_STATUS_UNDER_ACCOUNT_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	
	@accountId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.SR_RFP_GET_LP_STATUS_UNDER_ACCOUNT_INFO_P
@rfpId int,
@accountId int

AS
set nocount on


select 	en.entity_id,
	en.entity_name as status
	

from 
	sr_rfp_account rfp_account(nolock),
	sr_rfp_lp_account_summary summary(nolock),
	client c(nolock), 
	division d(nolock),
	site s(nolock), 
	account a(nolock),	
	entity en(nolock)
	
	 
where 
	rfp_account.sr_rfp_id = @rfpId
	and rfp_account.sr_rfp_bid_group_id is null
	and summary.sr_account_group_id = rfp_account.sr_rfp_account_id
	and rfp_account.is_deleted = 0
	and en.entity_id = summary.status_type_id
	and a.account_id = rfp_account.account_id
	and rfp_account.account_id= @accountId 		
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id

union
	
select 	
	en.entity_id,
	en.entity_name as status
	

from 
	--sr_rfp rfp,
	sr_rfp_account rfp_account,
	sr_rfp_bid_group bid_group,
	sr_rfp_lp_account_summary summary(nolock),
	client c, 
	division d,
	site s, 
	account a,
	entity en(nolock)

where 
	rfp_account.sr_rfp_id = @rfpId 
	and rfp_account.sr_rfp_bid_group_id is not null
	and bid_group.sr_rfp_bid_group_id = rfp_account.sr_rfp_bid_group_id
	and rfp_account.is_deleted = 0
	and summary.sr_account_group_id = rfp_account.sr_rfp_bid_group_id
	and a.account_id = rfp_account.account_id
	and en.entity_id = summary.status_type_id
	and bid_group.sr_rfp_bid_group_id= @accountId 	
	and s.site_id = a.site_id
	and d.division_id = s.division_id
	and c.client_id = d.client_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_LP_STATUS_UNDER_ACCOUNT_INFO_P] TO [CBMSApplication]
GO
