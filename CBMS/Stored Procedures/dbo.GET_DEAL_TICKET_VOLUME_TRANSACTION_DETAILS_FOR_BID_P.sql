SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_FOR_BID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@isSelectedCounterParty	bit       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE	PROCEDURE dbo.GET_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_FOR_BID_P

@userId varchar(10), 
@sessionId varchar(20), 
@dealTicketId int,
@isSelectedCounterParty bit

AS
set nocount on
	
	SELECT	MONTH_IDENTIFIER,
			TOTAL_VOLUME,
			HEDGE_PRICE
	FROM 
			RM_DEAL_TICKET_COUNTER_PARTY_PRICE_DETAILS
	WHERE 
			RM_DEAL_TICKET_ID = @dealTicketId AND 
			IS_SELECTED_COUNTERPARTY = @isSelectedCounterParty
			order by MONTH_IDENTIFIER
GO
GRANT EXECUTE ON  [dbo].[GET_DEAL_TICKET_VOLUME_TRANSACTION_DETAILS_FOR_BID_P] TO [CBMSApplication]
GO
