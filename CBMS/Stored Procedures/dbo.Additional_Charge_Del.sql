SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Additional_Charge_Del]  

DESCRIPTION: It Deletes Additional Charge for Selected Additional Charge Id.     
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Additional_Charge_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Additional_Charge_Del 4368
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED

*/

CREATE PROCEDURE dbo.Additional_Charge_Del
    (
      @Additional_Charge_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.ADDITIONAL_CHARGE 
	WHERE
		Additional_Charge_Id = @Additional_Charge_Id

END
GO
GRANT EXECUTE ON  [dbo].[Additional_Charge_Del] TO [CBMSApplication]
GO
