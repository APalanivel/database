SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
/******            
NAME:   dbo.Report_DE_Client_Site_Account_Details         
  
      
DESCRIPTION:   
 Gets all Client, Site and Account Data, excludes AT&T non USA & Canada sites.  
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
EXEC dbo.Report_DE_Client_Site_Account_Details  
         
       
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
AKR   Ashok Kumar Raju       
       
MODIFICATIONS             
Initials Date Modification            
------------------------------------------------------------            
AKR   2012-09-06 Created  
*****/ 

CREATE PROCEDURE dbo.Report_DE_Client_Site_Account_Details
AS 
BEGIN 

DECLARE @Client_Id INT

SELECT  @Client_Id = c.CLIENT_ID
FROM    dbo.CLIENT c
WHERE   CLIENT_NAME = 'AT&T Services, Inc.'

SELECT  ch.Client_Name Client ,
        ISNULL(s.SITE_REFERENCE_NUMBER, '') [Site Ref No] ,
        ch.Site_name SITE ,
        ch.Site_Address_Line1 [Address 1] ,
        ISNULL(ch.Site_Address_Line2, '') [Address 2] ,
        '' AS [Address 3] ,
        ch.City City ,
        ch.State_Name STATE ,
        ch.ZipCode [Zip Code] ,
        ch.Country_Name Country ,
        cha.Display_Account_Number Account ,
        cha.Account_Id [Account id] ,
        cha.Account_Vendor_Name Vendor ,
        e.ENTITY_NAME [Account Type] ,
        CASE WHEN cha.Account_Group_ID IS NULL THEN 'No'
             ELSE 'Yes'
        END AS [Group Bill]
FROM    core.Client_Hier ch
        INNER JOIN core.Client_Hier_Account cha ON ch.Client_Hier_Id = cha.Client_Hier_Id
        INNER JOIN dbo.SITE s ON ch.site_id = s.SITE_ID
        INNER JOIN dbo.ENTITY e ON e.ENTITY_ID = cha.Account_Vendor_Type_ID
WHERE   ( ch.CLIENT_ID != @Client_Id
          OR ch.CLIENT_ID = @Client_Id
          AND ch.Country_Name NOT IN ( 'USA', 'Canada' )
        )
        AND cha.Account_Not_Managed = 0
        AND ch.Site_Not_Managed = 0
        AND ch.Division_Not_Managed = 0
        AND ch.Client_Not_Managed = 0
GROUP BY ch.Client_Name ,
        SITE_REFERENCE_NUMBER ,
        ch.Site_name ,
        ch.Site_Address_Line1 ,
        ch.Site_Address_Line2 ,
        ch.City ,
        ch.State_Name ,
        ch.ZipCode ,
        ch.Country_Name ,
        cha.Display_Account_Number ,
        cha.Account_Id ,
        cha.Account_Vendor_Name ,
        e.ENTITY_NAME ,
        cha.Account_Group_ID 

END  

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Client_Site_Account_Details] TO [CBMSApplication]
GO
