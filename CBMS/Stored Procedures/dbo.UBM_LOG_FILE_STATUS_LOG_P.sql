SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from entity where entity_name='Success'
--select * from ubm_batch_master_log where ubm_batch_master_log_id=380

--select * from UBM_FEED_FILE_MAP
--select * from entity where entity_id=757

--delete from UBM_FEED_FILE_MAP 

CREATE  PROCEDURE dbo.UBM_LOG_FILE_STATUS_LOG_P

@masterLogId int,
@fileName varchar(100),
@fileStatus varchar(200)

AS

	set nocount on
	declare @fileStatusTypeId int

	select @fileStatusTypeId=entity_id from entity where entity_name=@fileStatus and entity_type=656

	INSERT INTO UBM_FEED_FILE_MAP (
		UBM_BATCH_MASTER_LOG_ID,
		FEED_FILE_NAME,
		STATUS_TYPE_ID
		) 
	VALUES	(
		@masterLogId,
		@fileName,
		@fileStatusTypeId
		)
GO
GRANT EXECUTE ON  [dbo].[UBM_LOG_FILE_STATUS_LOG_P] TO [CBMSApplication]
GO
