SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Supplier_Account_Config_Upd_By_Account_Contract_Id_For_OCT
           
DESCRIPTION:             
			To update DMO supplier account end date
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Account_Id					INT
    @Supplier_Account_End_Dt	DATETIME
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		SELECT TOP 10 * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP WHERE Contract_ID = -1
	BEGIN TRANSACTION
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149234
		EXEC dbo.Supplier_Account_Config_Upd_By_Account_Contract_Id_For_OCT 1149234,'2017-03-01 00:00:00.000',NULL
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149234
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149235
		EXEC dbo.Supplier_Account_Config_Upd_By_Account_Contract_Id_For_OCT 1149235,NULL,'2017-12-01 00:00:00.000',NULL
		SELECT acc.Supplier_Account_Begin_Dt,acc.Supplier_Account_End_Dt,samm.* 
			FROM dbo.ACCOUNT acc INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm ON acc.ACCOUNT_ID = samm.ACCOUNT_ID
			WHERE acc.ACCOUNT_ID = 1149235
	ROLLBACK TRANSACTION
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2019-07-23	MAINT-9791 - Created to update the only Supplier account configuration.
	NR			2020-02-26	Added Audit log for Last_Change_Ts. 
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Config_Upd_By_Account_Contract_Id_For_OCT]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @Supplier_Account_Begin_Dt DATETIME = NULL
        , @Supplier_Account_End_Dt DATETIME = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @System_User_Info_Id INT;

        SELECT
            @System_User_Info_Id = ui.USER_INFO_ID
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USERNAME = 'conversion'
            AND ui.FIRST_NAME = 'System';

        DECLARE
            @Min_Supplier_Account_Begin_Dt DATE
            , @Max_Supplier_Account_End_Dt DATE;


        SET @Supplier_Account_End_Dt = NULLIF(@Supplier_Account_End_Dt, '9999-12-31');

        BEGIN TRY
            BEGIN TRAN;

            UPDATE
                sac
            SET
                Supplier_Account_Begin_Dt = ISNULL(@Supplier_Account_Begin_Dt, sac.Supplier_Account_Begin_Dt)
                , Supplier_Account_End_Dt = ISNULL(@Supplier_Account_End_Dt, sac.Supplier_Account_End_Dt)
                , sac.Last_Change_Ts = GETDATE()
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Account_Id = @Account_Id
                AND sac.Contract_Id = @Contract_Id
                AND sac.Contract_Id <> -1;



            SELECT
                @Min_Supplier_Account_Begin_Dt = MIN(sac.Supplier_Account_Begin_Dt)
                , @Max_Supplier_Account_End_Dt = MAX(sac.Supplier_Account_End_Dt)
            FROM
                dbo.Supplier_Account_Config sac
            WHERE
                sac.Account_Id = @Account_Id;


            UPDATE
                a
            SET
                a.Supplier_Account_Begin_Dt = @Min_Supplier_Account_Begin_Dt
                , a.Supplier_Account_End_Dt = @Max_Supplier_Account_End_Dt
            FROM
                dbo.ACCOUNT a
            WHERE
                a.ACCOUNT_ID = @Account_Id;




            EXEC dbo.Supplier_Account_IP_Upd
                @User_Info_Id = @System_User_Info_Id
                , @Account_Id = @Account_Id;


            COMMIT TRAN;

        END TRY
        BEGIN CATCH

            ROLLBACK TRAN;
            EXEC usp_RethrowError 'Error In Update';

        END CATCH;


    END;



GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Upd_By_Account_Contract_Id_For_OCT] TO [CBMSApplication]
GO
