SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Sourcing_Dashboard_Missing_Contract_Image_Sel_By_User_Info_Id            
                        
 DESCRIPTION:                        
			To get Supplier Account Details                       
                        
 INPUT PARAMETERS:          
                     
 Name							DataType         Default       Description        
------------------------------------------------------------------------------     
@User_Info_Id					INT
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
------------------------------------------------------------------------------     
                        
 USAGE EXAMPLES:                            
------------------------------------------------------------------------------     
 
EXEC dbo.Sourcing_Dashboard_Missing_Contract_Image_Sel_By_User_Info_Id
     @User_Info_Id = 8757
    

                       
 AUTHOR INITIALS:        
       
 Initials              Name        
------------------------------------------------------------------------------     
 NR						Narayana Reddy
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
------------------------------------------------------------------------------     
 NR                    2019-05-29       Created for - Add Contract.
                       
******/
CREATE PROCEDURE [dbo].[Sourcing_Dashboard_Missing_Contract_Image_Sel_By_User_Info_Id]
    (
        @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Missing_Contract_Image_Type_Cd INT
            , @New_Exception_Status_Cd INT;


        SELECT
            @Missing_Contract_Image_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND c.Code_Value = 'Missing Contract Image';



        SELECT
            @New_Exception_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND c.Code_Value = 'New';


        SELECT
            ui.QUEUE_ID
            , ae.Exception_Type_Cd
            , COUNT(1) AS Cnt_Missing_Contract_Image
        FROM
            dbo.Contract_Exception ae
            INNER JOIN dbo.USER_INFO ui
                ON ui.QUEUE_ID = ae.Queue_Id
        WHERE
            ui.USER_INFO_ID = @User_Info_Id
            AND ae.Exception_Type_Cd = @Missing_Contract_Image_Type_Cd
            AND ae.Exception_Status_Cd = @New_Exception_Status_Cd
        GROUP BY
            ui.QUEUE_ID
            , ae.Exception_Type_Cd;



    END;


GO
GRANT EXECUTE ON  [dbo].[Sourcing_Dashboard_Missing_Contract_Image_Sel_By_User_Info_Id] TO [CBMSApplication]
GO
