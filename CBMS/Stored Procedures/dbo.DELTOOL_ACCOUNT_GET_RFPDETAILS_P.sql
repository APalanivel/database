SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELTOOL_ACCOUNT_GET_RFPDETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	int       	          	
	@account_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_account
--select * from budget


--exec DELTOOL_ACCOUNT_GET_BUDGETDETAILS_P 1,127

CREATE      procedure dbo.DELTOOL_ACCOUNT_GET_RFPDETAILS_P
( 
	@userId int
	,@account_id int
)
AS
BEGIN

	select 	rfp.sr_rfp_id ,
		acc.sr_rfp_account_id ,  
		e.entity_name   
	from 	sr_rfp rfp , 
		sr_rfp_account acc , 
		entity e
	where 	acc.account_id = @account_id
		and rfp.sr_rfp_id = acc.sr_rfp_id
		and acc.BID_STATUS_TYPE_ID = e.entity_id


END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_ACCOUNT_GET_RFPDETAILS_P] TO [CBMSApplication]
GO
