SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/****** 

NAME:
		dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id 

DESCRIPTION: 
				To get full details of notification deliveried

INPUT PARAMETERS: 
Name						DataType		Default		Description 
----------------------------------------------------------------------
@Notification_Msg_Queue_Id	INT

OUTPUT PARAMETERS:   
Name						DataType	Default		Description 
---------------------------------------------------------------------- 

USAGE EXAMPLES:   
----------------------------------------------------------------------   
SELECT * FROM dbo.USER_INFO WHERE USER_INFO_ID = 16

EXEC dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id 10558
EXEC dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id 8311
EXEC dbo.Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id 8319


AUTHOR INITIALS:   
	Initials	Name   
----------------------------------------------------------------------   
	NR			Narayana Reddy


MODIFICATIONS:  
	Initials	Date		Modification 
-------------------------------------------------------------------------------   
	NR			2016-06-03  Created  GCS-988 GCS-Phase-5
	 	 		 
******/
CREATE PROCEDURE [dbo].[Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id]
      ( 
       @Notification_Msg_Queue_Id INT )
AS 
BEGIN 

      SET NOCOUNT ON;
      
      DECLARE @Tbl_System_Account_Email AS TABLE
            ( 
             Email_Address VARCHAR(200) );
      
      DECLARE @System_Account_Email VARCHAR(200);
      
      INSERT      INTO @Tbl_System_Account_Email
                  ( Email_Address )
                  EXEC dbo.SR_SAD_GET_SYSTEM_ACCOUNT_P;
                  
      SELECT
            @System_Account_Email = Email_Address
      FROM
            @Tbl_System_Account_Email;
                  
      
      SELECT
            nmq.Requested_User_Id AS From_User_Id
           ,From_User.FIRST_NAME + SPACE(1) + From_User.LAST_NAME AS From_User
           ,CASE WHEN From_User.USERNAME = 'conversion' THEN @System_Account_Email
                 ELSE From_User.EMAIL_ADDRESS
            END AS From_User_Email
           ,LEFT(recpmail.recpmails, LEN(recpmail.recpmails) - 1) AS To_User_Email
           ,nmq.Email_Subject
           ,nmq.Email_Body
           ,nma.Cbms_Image_Id
           ,ci.CBMS_DOC_ID AS Attachment
           ,fvl.Last_Change_Ts
      FROM
            dbo.Contract_Notification_Log fvl
            INNER JOIN dbo.Notification_Msg_Queue nmq
                  ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
            LEFT JOIN dbo.Notification_Msg_Attachment nma
                  ON nmq.Notification_Msg_Queue_Id = nma.Notification_Msg_Queue_Id
            LEFT JOIN dbo.USER_INFO From_User
                  ON From_User.USER_INFO_ID = nmq.Requested_User_Id
            LEFT JOIN dbo.cbms_image ci
                  ON nma.Cbms_Image_Id = ci.CBMS_IMAGE_ID
            CROSS APPLY ( SELECT
                              nmd.Recipient_Email_Address + ','
                          FROM
                              dbo.Notification_Msg_Dtl nmd
                          WHERE
                              nmq.Notification_Msg_Queue_Id = nmd.Notification_Msg_Queue_Id
                          GROUP BY
                              nmd.Recipient_Email_Address
            FOR
                          XML PATH('') ) recpmail ( recpmails )
      WHERE
            nmq.Notification_Msg_Queue_Id = @Notification_Msg_Queue_Id;
      
END;
;
GO
GRANT EXECUTE ON  [dbo].[Notification_Msg_Dtl_Sel_By_Notification_Msg_Queue_Id] TO [CBMSApplication]
GO
