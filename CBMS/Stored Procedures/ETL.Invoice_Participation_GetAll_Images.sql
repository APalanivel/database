SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Invoice_Participation_GetAll_Images       

    
DESCRIPTION:    
	Gets all imagea data from Invoice Participation
	Used by teh IP ETl process to load the Client_Hier_Commodity-Iamge table      
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			09/30/2010	Created
*****/ 
CREATE PROCEDURE etl.Invoice_Participation_GetAll_Images
AS 
BEGIN 
	SELECT
		ch.Client_Hier_Id
		,ip.ACCOUNT_ID
		,cha.Commodity_Id
		,Convert(date, ip.SERVICE_MONTH) as Service_Month
		,ip.cbms_image_id
	FROM 
		cbms.dbo.INVOICE_PARTICIPATION ip 
		INNER JOIN Core.Client_Hier ch 
			ON ip.SITE_ID = ch.Site_Id
		INNER JOIN Core.Client_Hier_Account cha 
			ON cha.Client_Hier_Id = ch.Client_Hier_Id
				AND cha.Account_Id = ip.ACCOUNT_ID
	WHERE 
		ip.cbms_image_id IS NOT NULL
END



GO
GRANT EXECUTE ON  [ETL].[Invoice_Participation_GetAll_Images] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Invoice_Participation_GetAll_Images] TO [ETL_Execute]
GO
