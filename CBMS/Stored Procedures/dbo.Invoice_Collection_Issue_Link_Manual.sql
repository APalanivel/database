SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                
Name:   dbo.Invoice_Collection_Issue_Link_Manual         
                
Description:                
          
                             
 Input Parameters:                
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
----------------------------------------------------------------------------------------   
  
 Usage Examples:                    
----------------------------------------------------------------------------------------    
DECLARE @RC int  
DECLARE @IC_Activity_Id varchar(max) ='43049'  
DECLARE @IC_Queue_Id varchar(max)='2764850,2763815'  
  
 Exec dbo.Invoice_Collection_Issue_Link_Manual    
        @IC_Activity_Id,  
  @IC_Queue_Id ,17  
  
  
  --select * from Invoice_Collection_Issue_Log where Invoice_Collection_Activity_Id in (43049,43053)  
  
  
  
Author Initials:                
    Initials  Name                
----------------------------------------------------------------------------------------                  
 SLP    Sri Lakshmi Pallikonda  
  
 Modifications:                
    Initials        Date		Modification                
----------------------------------------------------------------------------------------                  
    SLP				2019-09-19  Link the issues manually  
	SLP				2019-11-11  Included the logic to use System user info id when user name is 0 / null
  
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Link_Manual]
(
    @IC_Activity_Id VARCHAR(MAX),
    @IC_Queue_Id VARCHAR(MAX),
    @User_Info_Id INT
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @Internal_Issue_event_Type_Cd INT;
    DECLARE @Invoice_Collection_Issue_Log_Ids TABLE
    (
        Invoice_Collection_Issue_Log_Id INT
    );
    DECLARE @User_Name VARCHAR(100);


    CREATE TABLE #Issues_to_Link_By_Activity_Ids
    (
        Invoice_Collection_Activity_Id INT,
        Invoice_Collection_Issue_Log_Id INT
    );
    CREATE TABLE #Invoice_Collection_Queue_Ids
    (
        Invoice_Collection_Queue_Id INT,
        Collection_Start_Dt DATE,
        Collection_End_Dt DATE,
        Invoice_Collection_Activity_Id INT
    );

    IF (@User_Info_Id = 0 OR @User_Info_Id IS NULL)
        SELECT @User_Info_Id = USER_INFO_ID
        FROM dbo.USER_INFO ui
        WHERE ui.FIRST_NAME = 'System'
              AND ui.LAST_NAME = 'Conversion';


    INSERT INTO #Invoice_Collection_Queue_Ids
    (
        Invoice_Collection_Queue_Id,
        Collection_Start_Dt,
        Collection_End_Dt,
        Invoice_Collection_Activity_Id
    )
    SELECT icq.Invoice_Collection_Queue_Id,
           icq.Collection_Start_Dt,
           icq.Collection_End_Dt,
           itlbai.Segments
    FROM dbo.Invoice_Collection_Queue icq
        INNER JOIN dbo.ufn_split(@IC_Queue_Id, ',') us
            ON us.Segments = icq.Invoice_Collection_Queue_Id
        CROSS APPLY dbo.ufn_split(@IC_Activity_Id, ',') itlbai;

    SELECT @Internal_Issue_event_Type_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
          AND c.Code_Value = 'IC Internal Comments';

    SELECT Segments AS Invoice_Collection_Activity_Id
    INTO #IC_Activity_Id
    FROM dbo.ufn_split(@IC_Activity_Id, ',') us;

    SELECT @User_Name = ui.USERNAME
    FROM dbo.USER_INFO ui
    WHERE ui.USER_INFO_ID = @User_Info_Id;

    /*Get the Original Issue If exists or the Latest Issue to be linked for the Activitiy_ID */
    INSERT INTO #Issues_to_Link_By_Activity_Ids
    (
        Invoice_Collection_Activity_Id,
        Invoice_Collection_Issue_Log_Id
    )
    SELECT ica.Invoice_Collection_Activity_Id,
           MAX(icil.Invoice_Collection_Issue_Log_Id)
    FROM dbo.Invoice_Collection_Issue_Log icil
        INNER JOIN #IC_Activity_Id ica
            ON ica.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id
    WHERE icil.Created_Ts = icil.Last_Change_Ts
    GROUP BY ica.Invoice_Collection_Activity_Id;

    INSERT INTO #Issues_to_Link_By_Activity_Ids
    (
        Invoice_Collection_Activity_Id,
        Invoice_Collection_Issue_Log_Id
    )
    SELECT icil1.Invoice_Collection_Activity_Id,
           MAX(icil1.Invoice_Collection_Issue_Log_Id)
    FROM dbo.Invoice_Collection_Issue_Log icil1
        INNER JOIN
        (
            SELECT MAX(icil.Last_Change_Ts) Last_Change_Ts,
                   icil.Invoice_Collection_Activity_Id
            FROM dbo.Invoice_Collection_Issue_Log icil
                INNER JOIN #IC_Activity_Id icai
                    ON icai.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id
            GROUP BY icil.Invoice_Collection_Activity_Id
        ) icil2
            ON icil1.Invoice_Collection_Activity_Id = icil2.Invoice_Collection_Activity_Id
               AND icil1.Last_Change_Ts = icil2.Last_Change_Ts
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM #Issues_to_Link_By_Activity_Ids itlbai
        WHERE itlbai.Invoice_Collection_Activity_Id = icil1.Invoice_Collection_Activity_Id
    )
    GROUP BY icil1.Invoice_Collection_Activity_Id;

    INSERT INTO dbo.Invoice_Collection_Issue_Log
    (
        Invoice_Collection_Activity_Id,
        Invoice_Collection_Queue_Id,
        Collection_Start_Dt,
        Collection_End_Dt,
        Invoice_Collection_Issue_Type_Cd,
        Issue_Status_Cd,
        Issue_Entity_Owner_Cd,
        Issue_Owner_User_Id,
        Created_User_Id,
        Updated_User_Id,
        Type_Of_Issue_Owner_Cd,
        Is_Blocker,
        Blocker_Action_Date
    )
    OUTPUT Inserted.Invoice_Collection_Issue_Log_Id
    INTO @Invoice_Collection_Issue_Log_Ids
    SELECT icil.Invoice_Collection_Activity_Id,
           icq1.Invoice_Collection_Queue_Id,
           icq1.Collection_Start_Dt,
           icq1.Collection_End_Dt,
           MAX(Invoice_Collection_Issue_Type_Cd) Invoice_Collection_Issue_Type_Cd,
           MAX(Issue_Status_Cd) Issue_Status_Cd,
           MAX(Issue_Entity_Owner_Cd) Issue_Entity_Owner_Cd,
           MAX(Issue_Owner_User_Id) Issue_Owner_User_Id,
           @User_Info_Id Created_User_Id,
           @User_Info_Id Updated_User_Id,
           MAX(Type_Of_Issue_Owner_Cd) Type_Of_Issue_Owner_Cd,
           MAX(   CASE
                      WHEN Is_Blocker = 1 THEN
                          1
                      ELSE
                          0
                  END
              ) Is_Blocker,
           MAX(Blocker_Action_Date) Blocker_Action_Date
    FROM dbo.Invoice_Collection_Issue_Log icil
        --INNER JOIN dbo.Invoice_Collection_Activity ica  
        --    ON ica.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id  
        INNER JOIN #Issues_to_Link_By_Activity_Ids ica
            ON ica.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id
               AND ica.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
        CROSS JOIN
        (
            SELECT icq.Invoice_Collection_Queue_Id,
                   icq.Collection_Start_Dt,
                   icq.Collection_End_Dt
            FROM dbo.ufn_split(@IC_Queue_Id, ',') us1
                INNER JOIN dbo.Invoice_Collection_Queue icq
                    ON us1.Segments = icq.Invoice_Collection_Queue_Id
        ) icq1
    WHERE EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@IC_Activity_Id, ',') us
        WHERE us.Segments = icil.Invoice_Collection_Activity_Id
    )
          AND NOT EXISTS
    (
        SELECT 1
        FROM dbo.Invoice_Collection_Issue_Log icil2
        WHERE icq1.Invoice_Collection_Queue_Id = icil2.Invoice_Collection_Queue_Id
              AND icil.Invoice_Collection_Activity_Id = icil2.Invoice_Collection_Activity_Id
    )
    GROUP BY icil.Invoice_Collection_Activity_Id,
             icq1.Invoice_Collection_Queue_Id,
             icq1.Collection_Start_Dt,
             icq1.Collection_End_Dt;


    INSERT INTO dbo.Invoice_Collection_Issue_Event
    (
        Invoice_Collection_Issue_Log_Id,
        Issue_Event_Type_Cd,
        Event_Desc,
        Created_User_Id,
        Updated_User_Id
    )
    SELECT icili.Invoice_Collection_Issue_Log_Id,
           @Internal_Issue_event_Type_Cd,
           'Added the Issue on ' + CONVERT(VARCHAR(8), GETDATE(), 1) + ' by ' + @User_Name,
           @User_Info_Id,
           @User_Info_Id
    FROM @Invoice_Collection_Issue_Log_Ids icili;




    INSERT INTO dbo.Invoice_Collection_Issue_Event
    (
        Invoice_Collection_Issue_Log_Id,
        Issue_Event_Type_Cd,
        Event_Desc,
        Created_User_Id,
        Updated_User_Id
    )
    SELECT icili.Invoice_Collection_Issue_Log_Id,
           icie.Issue_Event_Type_Cd,
           icie.Event_Desc,
           @User_Info_Id,
           @User_Info_Id
    FROM dbo.Invoice_Collection_Issue_Event icie
        INNER JOIN #Issues_to_Link_By_Activity_Ids itlbai
            ON itlbai.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
        CROSS JOIN @Invoice_Collection_Issue_Log_Ids icili;


    INSERT INTO dbo.Invoice_Collection_Issue_Attachment
    (
        Invoice_Collection_Issue_Log_Id,
        Cbms_Image_Id,
        Created_User_Id
    )
    SELECT icili.Invoice_Collection_Issue_Log_Id,
           icia.Cbms_Image_Id,
           @User_Info_Id
    FROM dbo.Invoice_Collection_Issue_Attachment icia
        INNER JOIN #Issues_to_Link_By_Activity_Ids itlbai
            ON itlbai.Invoice_Collection_Issue_Log_Id = icia.Invoice_Collection_Issue_Log_Id
        CROSS JOIN @Invoice_Collection_Issue_Log_Ids icili;

    /*Next Action Date copy*/
    INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
    (
        Invoice_Collection_Activity_Log_Id,
        Invoice_Collection_Queue_Id,
        Collection_Start_Dt,
        Collection_End_Dt
    )
    SELECT ical.Invoice_Collection_Activity_Log_Id,
           icqi.Invoice_Collection_Queue_Id,
           icqi.Collection_Start_Dt,
           icqi.Collection_End_Dt
    FROM dbo.Invoice_Collection_Activity_Log ical
        INNER JOIN #Invoice_Collection_Queue_Ids icqi
            ON icqi.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
    WHERE NOT EXISTS
    (
        SELECT 1
        FROM dbo.Invoice_Collection_Activity_Log_Queue_Map icalq
        WHERE icalq.Invoice_Collection_Queue_Id = icqi.Invoice_Collection_Queue_Id
              AND icalq.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id
    );


    UPDATE icq
    SET icq.Next_Action_Dt = ical2.Next_Action_Dt
    FROM dbo.Invoice_Collection_Queue icq
        INNER JOIN #Invoice_Collection_Queue_Ids icqi
            ON icqi.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
        INNER JOIN dbo.Invoice_Collection_Activity_Log ical2
            ON ical2.Invoice_Collection_Activity_Id = icqi.Invoice_Collection_Activity_Id
        INNER JOIN
        (
            SELECT icqi.Invoice_Collection_Activity_Id,
                   MAX(ical.Created_Ts) Created_Ts
            FROM dbo.Invoice_Collection_Activity_Log ical
                INNER JOIN #Invoice_Collection_Queue_Ids icqi
                    ON icqi.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
            GROUP BY icqi.Invoice_Collection_Activity_Id
        ) Nad_Lts
            ON Nad_Lts.Created_Ts = ical2.Created_Ts;


    UPDATE icq
    SET Last_Change_Ts = GETDATE()
    FROM dbo.Invoice_Collection_Queue icq
    WHERE EXISTS
    (
        SELECT 1
        FROM dbo.ufn_split(@IC_Queue_Id, ',') us1
        WHERE us1.Segments = icq.Invoice_Collection_Queue_Id
    );

    DROP TABLE #Issues_to_Link_By_Activity_Ids;
END;







GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Link_Manual] TO [CBMSApplication]
GO
