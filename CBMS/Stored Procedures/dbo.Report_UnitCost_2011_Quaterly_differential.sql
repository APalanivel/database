
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UnitCost_2011_Quaterly_differential

DESCRIPTION:
	This report is to get unit Cost (Budget Variances)

INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
@Client_id		INT    
@Metric_Id		INT    
@Commodity_Id	INT    
@Budget_id		INT    
@Compare_Month_Id INT    
@Site_Status	INT   
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UnitCost_2011_Quaterly_differential 235,171,291,5519,40297,0    
Report_UnitCost_2011_Quaterly_differential 235,0,291,5139,40297,0    

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava
AKR         Ashok Kumar Raju


MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   01/17/2011	Created
 LEC       03/19/2012  Added Core.Client_Hier to account for changes in Core.Client_Attribute_Tracking which eliminated Client_ID
 AKR       2014-06-11  Modified Attribute tables to refer DV2 on the same instance.(Changes for CBMS RO)

*/
CREATE PROC [dbo].[Report_UnitCost_2011_Quaterly_differential]
      @Client_id INT
     ,@Metric_Id INT
     ,@Commodity_Id INT
     ,@Budget_id INT
     ,@Compare_Month_Id INT
     ,@Site_Status INT
AS 
BEGIN    
    
      SET NOCOUNT ON    
      
      DECLARE
            @ng_unit_of_measure_type_id INT
           ,@Compare_date DATE
           ,@Year_Num INT
           ,@month_num INT
           ,@Month_Name VARCHAR(20)
           ,@Entity_Name VARCHAR(50)
           ,@active_table VARCHAR(50)
           ,@SQL VARCHAR(MAX)
           ,@Account_type_id INT
           ,@currency_factor DECIMAL(18, 5) = 1
           ,@Base_Unit_id INT = 25
		  
		  
      CREATE TABLE #Compare_Month_UnitCost
            ( 
             Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Usage DECIMAL(38, 6)
            ,TotalCost DECIMAL(38, 6) )    
    
      SELECT
            @Compare_date = dd.Date_D
           ,@Year_Num = dd.Year_Num
           ,@month_num = dd.Month_Num
      FROM
            meta.Date_Dim dd
      WHERE
            dd.date_id = @Compare_Month_Id    
    
      SELECT
            @Entity_Name = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_ID = @Commodity_Id     
        
      SET @Month_Name = 'Month' + CAST(@Month_num AS CHAR)
    
      SELECT
            @ng_unit_of_measure_type_id = ent.Entity_id
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'MMBtu'
            AND ent.ENTITY_DESCRIPTION = 'Unit for Gas'

      SELECT
            @Account_type_id = ent.Entity_id
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'Utility'
            AND ent.ENTITY_DESCRIPTION = 'Account Type'
        
        
        
      SELECT
            @active_table = active_table
      FROM
            RPT_VARIANCE_COST_USAGE_SITE_FLAG      
    
      SET @SQL = ' INSERT INTO #Compare_Month_UnitCost(Site_Name,Site_id,Usage,TotalCost)    
						SELECT    
							fin.site_name    
						  , fin.Site_id    
						  , fin.[Usage]    
						  , fin.[Total Cost]    
						FROM    
							( SELECT    
								a.site_name    
							  , a.site_id    
							  , a.field_type    
							  , a.' + @Month_Name + '    
							  FROM'    
      IF @active_table = 'rpt_variancE_cost_usage_site_1' 
            BEGIN    
                  SET @SQL = @SQL + ' rpt_variancE_cost_usage_site_1 a'    
            END    
      ELSE 
            BEGIN    
                  SET @SQL = @SQL + ' rpt_variancE_cost_usage_site_2 a'    
            END    
      SET @SQL = @SQL + ' WHERE    
							a.client_id = ' + CAST(@Client_id AS VARCHAR) + '    
						   AND a.report_year =' + CAST(@Year_Num AS VARCHAR) + '    
						   AND a.commodity_type = ''' + @Entity_Name + '''    
						   AND a.field_type IN ( ''Usage'', ''Total Cost'' )    
						) pvt PIVOT ( SUM(' + @Month_Name + ') FOR  field_type IN ( [Usage], [Total Cost] ) ) fin'    
					    
      EXEC ( @SQL )    
    
      SELECT
            Final_Avg_Out.Site_Id
           ,Final_Avg_Out.SITE_NAME
           ,Final_Avg_Out.Attribute_Value
           ,Final_Avg_Out.[2011/01]
           ,Final_Avg_Out.[2011/02]
           ,Final_Avg_Out.[2011/03]
           ,Final_Avg_Out.[2011/04]
           ,Final_Avg_Out.[2011/05]
           ,Final_Avg_Out.[2011/06]
           ,Final_Avg_Out.[2011/07]
           ,Final_Avg_Out.[2011/08]
           ,Final_Avg_Out.[2011/09]
           ,Final_Avg_Out.[2011/10]
           ,Final_Avg_Out.[2011/11]
           ,Final_Avg_Out.[2011/12]
           ,Final_Avg_Out.FY_Avg
           ,Final_Avg_Out.[Compare_Month_unit_cost]
           ,CASE WHEN Final_Avg_Out.Sum_Qtr1 = 0 THEN 0
                 ELSE Final_Avg_Out.Sum_Qtr1 / 3
            END Qtr1
           ,CASE WHEN Final_Avg_Out.Sum_Qtr2 = 0 THEN 0
                 ELSE Final_Avg_Out.Sum_Qtr2 / 3
            END Qtr2
           ,CASE WHEN Final_Avg_Out.Sum_Qtr3 = 0 THEN 0
                 ELSE Final_Avg_Out.Sum_Qtr3 / 3
            END Qtr3
           ,CASE WHEN Final_Avg_Out.Sum_Qtr4 = 0 THEN 0
                 ELSE Final_Avg_Out.Sum_Qtr4 / 3
            END Qtr4
           ,CASE WHEN ( ( CASE WHEN Final_Avg_Out.Sum_Qtr1 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr1 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) = 0 THEN 0
                 ELSE ( ( CASE WHEN Final_Avg_Out.Sum_Qtr1 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr1 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) / CASE WHEN Final_Avg_Out.[Compare_Month_unit_cost] = 0 THEN 1
                                                                                   ELSE Final_Avg_Out.[Compare_Month_unit_cost]
                                                                              END
            END '%Change Qtr1'
           ,CASE WHEN ( ( CASE WHEN Final_Avg_Out.Sum_Qtr2 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr2 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) = 0 THEN 0
                 ELSE ( ( CASE WHEN Final_Avg_Out.Sum_Qtr2 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr2 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) / CASE WHEN Final_Avg_Out.[Compare_Month_unit_cost] = 0 THEN 1
                                                                                   ELSE Final_Avg_Out.[Compare_Month_unit_cost]
                                                                              END
            END '%Change Qtr2'
           ,CASE WHEN ( ( CASE WHEN Final_Avg_Out.Sum_Qtr3 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr3 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) = 0 THEN 0
                 ELSE ( ( CASE WHEN Final_Avg_Out.Sum_Qtr3 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr3 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) / CASE WHEN Final_Avg_Out.[Compare_Month_unit_cost] = 0 THEN 1
                                                                                   ELSE Final_Avg_Out.[Compare_Month_unit_cost]
                                                                              END
            END '%Change Qtr3'
           ,CASE WHEN ( ( CASE WHEN Final_Avg_Out.Sum_Qtr4 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr4 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) = 0 THEN 0
                 ELSE ( ( CASE WHEN Final_Avg_Out.Sum_Qtr4 = 0 THEN 0
                               ELSE Final_Avg_Out.Sum_Qtr4 / 3
                          END ) - Final_Avg_Out.[Compare_Month_unit_cost] ) / CASE WHEN Final_Avg_Out.[Compare_Month_unit_cost] = 0 THEN 1
                                                                                   ELSE Final_Avg_Out.[Compare_Month_unit_cost]
                                                                              END
            END '%Change Qtr4'
      FROM
            ( SELECT
                  Final_Sum_Qtr.Site_Id
                 ,Final_Sum_Qtr.SITE_NAME
                 ,Final_Sum_Qtr.Attribute_Value
                 ,Final_Sum_Qtr.[2011/01]
                 ,Final_Sum_Qtr.[2011/02]
                 ,Final_Sum_Qtr.[2011/03]
                 ,Final_Sum_Qtr.[2011/04]
                 ,Final_Sum_Qtr.[2011/05]
                 ,Final_Sum_Qtr.[2011/06]
                 ,Final_Sum_Qtr.[2011/07]
                 ,Final_Sum_Qtr.[2011/08]
                 ,Final_Sum_Qtr.[2011/09]
                 ,Final_Sum_Qtr.[2011/10]
                 ,Final_Sum_Qtr.[2011/11]
                 ,Final_Sum_Qtr.[2011/12]
                 ,Final_Sum_Qtr.FY_Avg
                 ,Final_Sum_Qtr.[Compare_Month_unit_cost]
                 ,SUM(Final_Sum_Qtr.[2011/01] + Final_Sum_Qtr.[2011/02] + Final_Sum_Qtr.[2011/03]) Sum_Qtr1
                 ,SUM(Final_Sum_Qtr.[2011/04] + Final_Sum_Qtr.[2011/05] + Final_Sum_Qtr.[2011/06]) Sum_Qtr2
                 ,SUM(Final_Sum_Qtr.[2011/07] + Final_Sum_Qtr.[2011/08] + Final_Sum_Qtr.[2011/09]) Sum_Qtr3
                 ,SUM(Final_Sum_Qtr.[2011/10] + Final_Sum_Qtr.[2011/11] + Final_Sum_Qtr.[2011/12]) Sum_Qtr4
              FROM
                  ( SELECT
                        pvt_final.SITE_ID
                       ,pvt_final.SITE_NAME
                       ,pvt_final.Attribute_Value
                       ,pvt_final.[2011/01]
                       ,pvt_final.[2011/02]
                       ,pvt_final.[2011/03]
                       ,pvt_final.[2011/04]
                       ,pvt_final.[2011/05]
                       ,pvt_final.[2011/06]
                       ,pvt_final.[2011/07]
                       ,pvt_final.[2011/08]
                       ,pvt_final.[2011/09]
                       ,pvt_final.[2011/10]
                       ,pvt_final.[2011/11]
                       ,pvt_final.[2011/12]
                       ,pvt_final.FY_Avg
                       ,pvt_final.[Compare_Month_unit_cost]
                    FROM
                        ( SELECT
                              Final.SITE_ID
                             ,Final.SITE_NAME
                             ,Final.month_Name
                             ,Final.Attribute_Value
                             ,Final.unit_cost
                             ,AVG(final.unit_cost) OVER ( PARTITION BY final.Site_id ) FY_Avg
                             ,( CASE SUM(cmuc.usage)
                                  WHEN 0 THEN 0
                                  ELSE SUM(cmuc.TotalCost) / ( SUM(cmuc.usage) * cconng_prv_month.conversion_factor )
                                END ) / @currency_factor AS [Compare_Month_unit_cost]
                          FROM
                              ( SELECT
                                    x.SITE_ID
                                   ,x.SITE_NAME
                                   ,cat.Attribute_Value
                                   ,x.month_Name
                                   ,x.budget_id
                                   ,( CASE SUM(x.usage)
                                        WHEN 0 THEN 0
                                        ELSE SUM(x.total_cost) / ( SUM(x.usage) * cconng.conversion_factor )
                                      END ) / @currency_factor AS [unit_cost]
                                   ,x.Qtr_distribution
                                FROM
                                    ( SELECT
                                          c.CLIENT_ID
                                         ,s.SITE_ID
                                         ,s.SITE_NAME
                                         ,CONVERT(VARCHAR(7), bd.month_identifier, 111) AS [month_Name]
                                         ,CASE WHEN MONTH(bd.month_identifier) IN ( 1, 2, 3 )
                                                    AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR1'
                                               WHEN MONTH(bd.month_identifier) IN ( 4, 5, 6 )
                                                    AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR2'
                                               WHEN MONTH(bd.month_identifier) IN ( 7, 8, 9 )
                                                    AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR3'
                                               WHEN MONTH(bd.month_identifier) IN ( 10, 11, 12 )
                                                    AND ( YEAR(bd.month_identifier) = YEAR(GETDATE()) ) THEN 'QTR4'
                                          END Qtr_distribution
                                         ,acct.ACCOUNT_ID
                                         ,( ( ( ISNULL(bd.variable_value, 0) ) + ISNULL(bd.rates_tax_value, 0) + ISNULL(bd.sourcing_tax_value, 0) + ISNULL(bd.transportation_value, 0) + ISNULL(bd.transmission_value, 0) + ISNULL(bd.distribution_value, 0) + ISNULL(bd.other_bundled_value, 0) ) * ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) ) + ISNULL(bd.other_fixed_costs_value, 0) AS [total_cost]
                                         ,ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) usage
                                         ,b.budget_id
                                      FROM
                                          dbo.client c
                                          JOIN dbo.site s
                                                ON s.Client_ID = c.CLIENT_ID
                                                   AND s.closed = 0
                                                   AND ( @Site_Status = -1
                                                         OR s.NOT_MANAGED = @Site_Status )
                                          JOIN dbo.address a
                                                ON a.address_parent_id = s.site_id
                                          JOIN ( SELECT
                                                      acct.account_id
                                                     ,m.address_id
                                                 FROM
                                                      dbo.account acct
                                                      JOIN dbo.meter m
                                                            ON m.account_id = acct.account_id
                                                 WHERE
                                                      acct.not_managed = 0
                                                      AND acct.account_type_id = @Account_type_id
                                                 UNION
                                                 SELECT
                                                      acct.account_id
                                                     ,m.address_id
                                                 FROM
                                                      dbo.meter m
                                                      JOIN dbo.supplier_account_meter_map samm
                                                            ON samm.meter_id = m.meter_id
                                                      JOIN dbo.account acct
                                                            ON acct.account_id = samm.account_id
                                                 WHERE
                                                      acct.not_managed = 0 ) AS acct
                                                ON acct.address_id = a.address_id
                                          JOIN dbo.budget_account ba
                                                ON ba.account_id = acct.account_id
                                                   AND ba.is_deleted = 0
                                          JOIN dbo.budget b
                                                ON b.budget_id = ba.budget_id
                                          LEFT OUTER JOIN dbo.budget_details bd
                                                ON bd.budget_account_id = ba.budget_account_id
                                          LEFT OUTER JOIN dbo.budget_usage bu
                                                ON bu.account_id = ba.account_id
                                                   AND bd.month_identifier = bu.month_identifier
                                                   AND bd.month_identifier = bu.month_identifier
                                                   AND b.commodity_type_id = bu.commodity_type_id
                                      WHERE
                                          b.budget_id = @budget_id
                                          AND b.COMMODITY_TYPE_ID = @Commodity_Id
                                      GROUP BY
                                          c.CLIENT_ID
                                         ,s.SITE_ID
                                         ,s.SITE_NAME
                                         ,bd.month_identifier
                                         ,CONVERT(VARCHAR(7), bd.month_identifier, 111)
                                         ,( ( ( ISNULL(bd.variable_value, 0) ) + ISNULL(bd.rates_tax_value, 0) + ISNULL(bd.sourcing_tax_value, 0) + ISNULL(bd.transportation_value, 0) + ISNULL(bd.transmission_value, 0) + ISNULL(bd.distribution_value, 0) + ISNULL(bd.other_bundled_value, 0) ) * ISNULL(bd.budget_usage, ISNULL(bu.volume, 0)) ) + ISNULL(bd.other_fixed_costs_value, 0)
                                         ,ISNULL(bd.budget_usage, ISNULL(bu.volume, 0))
                                         ,b.budget_id
                                         ,acct.ACCOUNT_ID ) x
                                    JOIN dbo.consumption_unit_conversion cconng
                                          ON ( cconng.base_unit_id = @Base_Unit_id
                                               AND cconng.converted_unit_id = @ng_unit_of_measure_type_id )
                                     --new
                                    LEFT JOIN ( SELECT
                                                      cat1.attribute_value
                                                     ,cat1.client_attribute_id
                                                     ,ca.client_id
                                                     ,cat1.client_hier_id
                                                     ,site_id
                                                FROM
                                                      dv2.core.client_attribute ca
                                                      JOIN core.client_hier ch
                                                            ON ch.client_id = ca.client_id
                                                      JOIN dv2.core.client_attribute_tracking cat1
                                                            ON cat1.client_attribute_id = ca.client_attribute_id
                                                               AND cat1.client_hier_id = ch.client_hier_id ) cat
                                          ON cat.site_id = x.site_id
                                             AND cat.client_id = x.client_id
                                             AND cat.client_attribute_id = @Metric_Id
                                GROUP BY
                                    x.SITE_ID
                                   ,x.SITE_NAME
                                   ,cat.Attribute_Value
                                   ,x.month_Name
                                   ,x.budget_id
                                   ,cconng.conversion_factor
                                   ,x.Qtr_distribution ) Final
                              JOIN #Compare_Month_UnitCost cmuc
                                    ON cmuc.Site_Id = Final.SITE_ID
                              JOIN dbo.consumption_unit_conversion cconng_prv_month
                                    ON ( cconng_prv_month.base_unit_id = @Base_Unit_id
                                         AND cconng_prv_month.converted_unit_id = @ng_unit_of_measure_type_id )
                          GROUP BY
                              Final.SITE_ID
                             ,Final.SITE_NAME
                             ,Final.month_Name
                             ,Final.Attribute_Value
                             ,Final.unit_cost
                             ,cmuc.usage
                             ,cmuc.TotalCost
                             ,cconng_prv_month.conversion_factor ) pvt PIVOT ( SUM(unit_cost) FOR month_Name IN ( [2011/01], [2011/02], [2011/03], [2011/04], [2011/05], [2011/06], [2011/07], [2011/08], [2011/09], [2011/10], [2011/11], [2011/12] ) ) pvt_final ) Final_Sum_Qtr
              GROUP BY
                  Final_Sum_Qtr.Site_Id
                 ,Final_Sum_Qtr.SITE_NAME
                 ,Final_Sum_Qtr.Attribute_Value
                 ,Final_Sum_Qtr.[2011/01]
                 ,Final_Sum_Qtr.[2011/02]
                 ,Final_Sum_Qtr.[2011/03]
                 ,Final_Sum_Qtr.[2011/04]
                 ,Final_Sum_Qtr.[2011/05]
                 ,Final_Sum_Qtr.[2011/06]
                 ,Final_Sum_Qtr.[2011/07]
                 ,Final_Sum_Qtr.[2011/08]
                 ,Final_Sum_Qtr.[2011/09]
                 ,Final_Sum_Qtr.[2011/10]
                 ,Final_Sum_Qtr.[2011/11]
                 ,Final_Sum_Qtr.[2011/12]
                 ,Final_Sum_Qtr.FY_Avg
                 ,Final_Sum_Qtr.[Compare_Month_unit_cost] ) Final_Avg_Out
      ORDER BY
            Final_Avg_Out.SITE_NAME       
END


;
GO

GRANT EXECUTE ON  [dbo].[Report_UnitCost_2011_Quaterly_differential] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_UnitCost_2011_Quaterly_differential] TO [CBMSApplication]
GO
