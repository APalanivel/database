
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Sr_Rfp_History_Ins]  
     
DESCRIPTION: 
	To get assigned RFPs count based on User Id
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Account_Id		INT						Utility Account
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
	SELECT rfp.* FROM dbo.SR_RFP rfp INNER JOIN dbo.ENTITY ent ON rfp.rfp_status_type_id = ent.ENTITY_ID
	WHERE ENTITY_NAME != 'Closed' AND ENTITY_TYPE = 1004
	
	EXEC dbo.SR_SAD_GET_RFP_COUNT_P 25681,1
	EXEC dbo.SR_SAD_GET_RFP_COUNT_P 49,1
	
	 
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	RR			Raghu Reddy


MODIFICATIONS           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	RR			2015-07-28	Global Sourcing - Modified to get count based on Queue id

*/
CREATE   PROCEDURE [dbo].[SR_SAD_GET_RFP_COUNT_P]
      @user_id VARCHAR(10)
     ,@session_id VARCHAR(20)
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE
            @Queue_Id INT
           ,@RFP_Status_Closed INT
      
      SELECT
            @Queue_Id = ui.QUEUE_ID
      FROM
            dbo.USER_INFO ui
      WHERE
            ui.USER_INFO_ID = @user_id
            
      SELECT
            @RFP_Status_Closed = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Closed'
            AND ENTITY_TYPE = 1004
            
      SELECT
            count(DISTINCT rfp.SR_RFP_ID) AS no_of_rfps
      FROM
            dbo.SR_RFP rfp
            INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account
                  ON rfp_account.sr_rfp_id = rfp.sr_rfp_id
                     AND rfp_account.is_deleted = 0
                     AND rfp.rfp_status_type_id != @RFP_Status_Closed
                     AND rfp.Queue_Id = @Queue_Id
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_GET_RFP_COUNT_P] TO [CBMSApplication]
GO
