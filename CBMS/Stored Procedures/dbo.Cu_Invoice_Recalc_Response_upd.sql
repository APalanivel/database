
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.Cu_Invoice_Recalc_Response_upd  
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name								DataType    Default  Description    
-------------------------------------------------------------------------------------    
 @Recalc_Header_Id					INT  
 @Cu_Invoice_Recalc_Response_Id		INT  
 @Charge_Name						NVARCHAR(200)  
 @Determinant_Unit					NVARCHAR(200)  
 @Determinant_Value					DECIMAL(28, 10)  
 @Rate_Unit							NVARCHAR(200)  
 @Rate_Currency						NVARCHAR(200)  
 @Rate_Amount						DECIMAL(28, 10)  
 @Net_Amount						DECIMAL(28, 10)  
 @Calculator_Working				NVARCHAR  
 @Created_Ts						DATETIME  
 @Determinant_Type					NVARCHAR(200)  
 @Charge_Type						NVARCHAR(200)  
 @Bucket_Name						VARCHAR(255)    

OUTPUT PARAMETERS:    
 Name        DataType    Default  Description    
-------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------  
  BEGIN TRAN
	SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr WHERE Cu_Invoice_Recalc_Response_Id = 5
	
        EXEC [dbo].[Cu_Invoice_Recalc_Response_upd] 
            @Recalc_Header_Id = 1456487
           ,@Cu_Invoice_Recalc_Response_Id = 5
           ,@Charge_Name = 'sfdf'
           ,@Determinant_Unit = 'Days'
           ,@Determinant_Value = '32.0000000000'
           ,@Rate_Unit = 'Ccf'
           ,@Rate_Currency = 'AED'
           ,@Rate_Amount = '32.0000000000'
           ,@Net_Amount = '312.0000000000'
           ,@Bucket_Name  ='Commodity'
           ,@Net_Amount_Currency = 53
           ,@Updated_User_Id = NULL
           ,@Comment_Text = 'Welcome SE2017-263' 
            
	SELECT * FROM dbo.Cu_Invoice_Recalc_Response cirr WHERE Cu_Invoice_Recalc_Response_Id = 5
  ROLLBACK TRAN

  

    
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------------------------    
 RKV		Ravi Kumar Vegesna 
 NR			Narayana Reddy   
     
MODIFICATIONS    
    
 Initials	Date		Modification    
-------------------------------------------------------------------------------------    
 RKV		2015-10-14  Created for AS400-PII   
 HG		    2016-10-06	Modified to consider the bucket type on the query which gets the Bucket id 
 NR			2017-10-17	SE2017-263 - Update the Charge_GUID,Is_Locked,Is_Send_To_Sys new columns.

******/    
CREATE   PROCEDURE [dbo].[Cu_Invoice_Recalc_Response_upd]
      ( 
       @Recalc_Header_Id INT
      ,@Cu_Invoice_Recalc_Response_Id INT
      ,@Charge_Name NVARCHAR(200)
      ,@Determinant_Unit NVARCHAR(200) = NULL
      ,@Determinant_Value DECIMAL(28, 10) = NULL
      ,@Rate_Unit NVARCHAR(200) = NULL
      ,@Rate_Currency NVARCHAR(200) = NULL
      ,@Rate_Amount DECIMAL(28, 10) = NULL
      ,@Net_Amount DECIMAL(28, 10)
      ,@Bucket_Name VARCHAR(255)
      ,@Net_Amount_Currency NVARCHAR(200) = NULL
      ,@Updated_User_Id INT
      ,@Charge_GUID NVARCHAR(60) = NULL
      ,@Is_Locked BIT = 0
      ,@Is_Send_To_Sys BIT = 0
      ,@Comment_Text VARCHAR(MAX) = NULL )
AS 
BEGIN    
      SET NOCOUNT ON;    
       
      DECLARE
            @Bucket_Master_Id INT
           ,@Rate_Uom_Type_Id INT = NULL
           ,@Rate_Currency_Unit_Id INT = NULL
           ,@Net_Amt_Currency_Unit_Id INT = NULL
           ,@Commodity_Id INT
           ,@Comment_Id INT = NULL
           ,@Comment_Dt DATETIME = GETDATE()
           ,@Comment_User_Id INT;  
       
      SELECT
            @Bucket_Master_Id = bm.Bucket_Master_Id
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.RECALC_HEADER rh
                  ON bm.Commodity_Id = rh.Commodity_Id
            INNER JOIN dbo.Code bt
                  ON bt.Code_Id = bm.Bucket_Type_Cd
      WHERE
            bm.Bucket_Name = @Bucket_Name
            AND bt.Code_Value = 'Charge'
            AND rh.RECALC_HEADER_ID = @Recalc_Header_Id;

      SELECT
            @Rate_Currency_Unit_Id = CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT
      WHERE
            CURRENCY_UNIT_NAME = @Rate_Currency;  

      SELECT
            @Net_Amt_Currency_Unit_Id = CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT
      WHERE
            CURRENCY_UNIT_NAME = @Net_Amount_Currency;  

      SELECT
            @Rate_Uom_Type_Id = e.ENTITY_TYPE
      FROM
            dbo.RECALC_HEADER rh
            INNER JOIN dbo.Commodity c
                  ON rh.Commodity_Id = c.Commodity_Id
            INNER JOIN dbo.ENTITY e
                  ON e.ENTITY_TYPE = c.UOM_Entity_Type
      WHERE
            RECALC_HEADER_ID = @Recalc_Header_Id
            AND e.ENTITY_NAME = @Rate_Unit;  
       
  
  
--Update The Existing comment, if not entry the new comment for that responce.


      SELECT
            @Comment_User_Id = ISNULL(@Updated_User_Id, USER_INFO_ID)
      FROM
            dbo.USER_INFO
      WHERE
            USERNAME = 'Conversion'
 
      SELECT
            @Comment_Id = cirr.Charge_Comment_Id
      FROM
            dbo.Cu_Invoice_Recalc_Response cirr
      WHERE
            Cu_Invoice_Recalc_Response_Id = @Cu_Invoice_Recalc_Response_Id
  
  
  
      IF @Comment_Id IS NOT NULL 
            BEGIN  
                  EXECUTE dbo.Comment_Upd 
                        @Comment_Id = @Comment_Id
                       ,@Comment_Text = @Comment_Text  
            END 
      ELSE 
            IF @Comment_Id IS NULL
                  AND @Comment_Text IS NOT NULL 
                  BEGIN  
                  
                        EXECUTE dbo.Comment_Ins 
                              @Comment_Text = @Comment_Text
                             ,@Comment_User_Info_Id = @Comment_User_Id
                             ,@Comment_Type_Cd = NULL
                             ,@Comment_Dt = @Comment_Dt
                             ,@Comment_Id = @Comment_Id OUTPUT  
                  END  
  
  
  
  
      UPDATE
            cirr
      SET   
            Charge_Name = @Charge_Name
           ,Bucket_Master_Id = @Bucket_Master_Id
           ,Determinant_Unit = @Determinant_Unit
           ,Determinant_Value = @Determinant_Value
           ,Rate_Unit = @Rate_Unit
           ,Rate_Currency = @Rate_Currency
           ,Rate_Amount = @Rate_Amount
           ,Net_Amount = @Net_Amount
           ,Net_Amt_Currency_Unit_Id = @Net_Amt_Currency_Unit_Id
           ,Rate_Currency_Unit_Id = @Rate_Currency_Unit_Id
           ,Rate_Uom_Type_Id = @Rate_Uom_Type_Id
           ,Updated_User_Id = @Updated_User_Id
           ,Last_Change_Ts = GETDATE()
           ,Charge_GUID = @Charge_GUID
           ,Is_Locked = @Is_Locked
           ,Is_Send_To_Sys = @Is_Send_To_Sys
           ,Charge_Comment_Id = @Comment_Id
      FROM
            dbo.Cu_Invoice_Recalc_Response cirr
      WHERE
            Cu_Invoice_Recalc_Response_Id = @Cu_Invoice_Recalc_Response_Id;              
        
END;


;
GO



GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Response_upd] TO [CBMSApplication]
GO
