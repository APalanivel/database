
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:			dbo.User_SingleSignOn_Merge
DESCRIPTION:	Inserts or Updates the Single Signon User Map table

INPUT PARAMETERS:
	Name				DataType			Default	Description
-----------------------------------------------------------------
	@user_info_id		INT
	@sso_provider_cd	INT
	@sso_user_id		INT
	@client_id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.User_SingleSignOn_Merge 69936,12955,100675,"ew33"


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	KH			Kevin Horton
	SPT			Sanjai P T
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	KH			01-Nov-2012		Created
	SPT			10-May-2017		Modified to include update of Signon_Provider_Cd
	TP			28-Jun-2017		MAINT-5464 - removed Signon_Provider_Cd column				
******/
CREATE PROCEDURE [dbo].[User_SingleSignOn_Merge]
      @user_info_id INT
     ,@client_id INT
     ,@sso_user_id VARCHAR(200)
AS 
BEGIN

      SET NOCOUNT ON;
	
      MERGE dbo.Single_Signon_User_Map AS tgt
            USING 
                  ( SELECT
                        @user_info_id AS Internal_User_ID
                       ,@client_id AS Client_ID
                       ,@sso_user_id AS Provider_XUser_Name ) AS src
            ON	tgt.Client_ID = src.Client_ID
                  AND tgt.Internal_User_ID = src.Internal_User_ID
            WHEN MATCHED 
                  THEN
			UPDATE  SET 
                        tgt.Provider_XUser_Name = src.Provider_XUser_Name
            WHEN NOT MATCHED 
                  THEN
			INSERT
                        ( 
                         Internal_User_ID
                        ,Client_ID
                        ,Provider_XUser_Name )
                    VALUES
                        ( 
                         src.Internal_User_ID
                        ,src.Client_ID
                        ,src.Provider_XUser_Name );
		
      SELECT
            @user_info_id AS user_info_id
           ,@client_id AS client_id
           ,@sso_user_id AS Provider_XUser_Name
END;

;



;
GO


GRANT EXECUTE ON  [dbo].[User_SingleSignOn_Merge] TO [CBMSApplication]
GO
