SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Issue_Exist_By_Invoice_Collection_Queue            
                        
 DESCRIPTION:                        
			To get the details of Invoice_Collection_Issue               
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Issue_Id INT

                             
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------------------------------------------- 
                           
            DECLARE @Is_Issue_Exists BIT= 0                    
            EXEC [Invoice_Collection_Issue_Exist_By_Invoice_Collection_Queue] 
                  '1,2'
                 ,@Is_Issue_Exists = @Is_Issue_Exists OUT
            SELECT
                  @Is_Issue_Exists as Is_Issue_Exists
                  
     
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-21       Created for Invoice Tracking               
                       
******/    
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Exist_By_Invoice_Collection_Queue]
      ( 
       @Invoice_Collection_Queue_Ids VARCHAR(MAX)
      ,@Is_Issue_Exists BIT OUT )
AS 
BEGIN                
      SET NOCOUNT ON;  
      
      SELECT
            @Is_Issue_Exists = 1
      FROM
            Invoice_Collection_Issue_Log i
            INNER JOIN dbo.Code isc
                  ON isc.Code_Id = i.Issue_Status_Cd
            INNER JOIN dbo.ufn_split(@Invoice_Collection_Queue_Ids, ',') uf
                  ON i.Invoice_Collection_Queue_Id = uf.Segments
      WHERE
            isc.Code_Value = 'Open'       
            
                 

END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Exist_By_Invoice_Collection_Queue] TO [CBMSApplication]
GO
