SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seStripDetail_RemoveForHeader
	(
		@StripHeaderId int
	)
AS
BEGIN

	set nocount on
	
		delete seStripDetail
		 where StripHeaderId = @StripHeaderId	
	
	exec seStripDetail_GetForHeader @StripHeaderId

END
GO
GRANT EXECUTE ON  [dbo].[seStripDetail_RemoveForHeader] TO [CBMSApplication]
GO
