SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE         PROCEDURE 
[dbo].[cbmsInvoiceParticipation_GetMyTwelveMonthsStatistics2]



	( @user_info_id int
	, @client_id int
	, @division_id int = null
	, @site_id int = null
	)
AS
BEGIN

	set nocount on

	  declare @month1 datetime
		, @month2 datetime
		, @month3 datetime
		, @month4 datetime
		, @month5 datetime
		, @month6 datetime
		, @month7 datetime
		, @month8 datetime
		, @month9 datetime
		, @month10 datetime
		, @month11 datetime		, @month12 datetime

	set @month12 = convert(datetime, convert(varchar, month(getdate())) + '/1/' + convert(varchar, year(getdate())))
	set @month12 = dateadd(m, -2, @month12)
	set @month11 = dateadd(m, -1, @month12)
	set @month10 = dateadd(m, -1, @month11)
	set @month9 = dateadd(m, -1, @month10)
	set @month8 = dateadd(m, -1, @month9)
	set @month7 = dateadd(m, -1, @month8)
	set @month6 = dateadd(m, -1, @month7)
	set @month5 = dateadd(m, -1, @month6)
	set @month4 = dateadd(m, -1, @month5)
	set @month3 = dateadd(m, -1, @month4)
	set @month2 = dateadd(m, -1, @month3)
	set @month1 = dateadd(m, -1, @month2)

	print @month1--Oct  1 2005 12:00AM
	print @month12--Sep  1 2006 12:00AM





--	set nocount off


	   select 'Expected' invoice_type
		, 'Month1' = isNull(sum(case when x.service_month = @month1 then x.account_count else 0 end), 0)
		, 'Month2' = isNull(sum(case when x.service_month = @month2 then x.account_count else 0 end), 0)
		, 'Month3' = isNull(sum(case when x.service_month = @month3 then x.account_count else 0 end), 0)
		, 'Month4' = isNull(sum(case when x.service_month = @month4 then x.account_count else 0 end), 0)
		, 'Month5' = isNull(sum(case when x.service_month = @month5 then x.account_count else 0 end), 0)
		, 'Month6' = isNull(sum(case when x.service_month = @month6 then x.account_count else 0 end), 0)
		, 'Month7' = isNull(sum(case when x.service_month = @month7 then x.account_count else 0 end), 0)
		, 'Month8' = isNull(sum(case when x.service_month = @month8 then x.account_count else 0 end), 0)
		, 'Month9' = isNull(sum(case when x.service_month = @month9 then x.account_count else 0 end), 0)
		, 'Month10' = isNull(sum(case when x.service_month = @month10 then x.account_count else 0 end), 0)
		, 'Month11' = isNull(sum(case when x.service_month = @month11 then x.account_count else 0 end), 0)
		, 'Month12' = isNull(sum(case when x.service_month = @month12 then x.account_count else 0 end), 0)
	     from (
		   select ip.service_month
			, count(distinct ip.account_id) account_count
		     from division d  WITH (NOLOCK)
		     join site s WITH (NOLOCK) on s.division_id = d.division_id
			and s.not_managed=0

		     --join vwAccountMeter vam WITH (NOLOCK) on vam.site_id = s.site_id
		     join invoice_participation ip WITH (NOLOCK) on ip.site_id = s.site_id
		    where d.client_id = @client_id
		      and d.division_id = isNull(@division_id, d.division_id)
		      and s.site_id = isNull(@site_id, s.site_id)
		      and ip.service_month between @month1 and @month12
		      and ip.is_expected = 1
		 group by ip.service_month
		  ) x
	union all
	   select 'Received' invoice_type
		, 'Month1' = isNull(sum(case when x.service_month = @month1 then x.account_count else 0 end), 0)
		, 'Month2' = isNull(sum(case when x.service_month = @month2 then x.account_count else 0 end), 0)
		, 'Month3' = isNull(sum(case when x.service_month = @month3 then x.account_count else 0 end), 0)
		, 'Month4' = isNull(sum(case when x.service_month = @month4 then x.account_count else 0 end), 0)
		, 'Month5' = isNull(sum(case when x.service_month = @month5 then x.account_count else 0 end), 0)
		, 'Month6' = isNull(sum(case when x.service_month = @month6 then x.account_count else 0 end), 0)
		, 'Month7' = isNull(sum(case when x.service_month = @month7 then x.account_count else 0 end), 0)
		, 'Month8' = isNull(sum(case when x.service_month = @month8 then x.account_count else 0 end), 0)
		, 'Month9' = isNull(sum(case when x.service_month = @month9 then x.account_count else 0 end), 0)
		, 'Month10' = isNull(sum(case when x.service_month = @month10 then x.account_count else 0 end), 0)
		, 'Month11' = isNull(sum(case when x.service_month = @month11 then x.account_count else 0 end), 0)
		, 'Month12' = isNull(sum(case when x.service_month = @month12 then x.account_count else 0 end), 0)
	     from (
		   select ip.service_month
			, count(distinct ip.account_id) account_count
		     from division d WITH (NOLOCK) 
		     join site s WITH (NOLOCK) on s.division_id = d.division_id
			and s.not_managed=0
		     --join vwAccountMeter vam WITH (NOLOCK) on vam.site_id = s.site_id
		     join invoice_participation ip WITH (NOLOCK) on ip.site_id = s.site_id
		    where d.client_id = @client_id
		      and d.division_id = isNull(@division_id, d.division_id)
		      and s.site_id = isNull(@site_id, s.site_id)
		      and ip.service_month between @month1 and @month12
		      and ip.is_received = 1
		 group by ip.service_month
		  ) x

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_GetMyTwelveMonthsStatistics2] TO [CBMSApplication]
GO
