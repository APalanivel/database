SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.Site_dtl_Sel_by_Client_id

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@client_id     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec dbo.Site_dtl_Sel_by_Client_id  201


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	05/06/2010	Created
		SKA		05/31/2010	Removed Site from order by clause	        	
******/

CREATE PROCEDURE dbo.Site_dtl_Sel_by_Client_id @client_id INT
AS 
BEGIN
	
      SET NOCOUNT ON ;

      SELECT
            s.site_id
           ,RTRIM(a.city) + ', ' + st.state_name + ' (' + s.site_name + ')' Site_Name
           ,a.address_line1
           ,a.address_line2
           ,a.city
           ,st.state_name
           ,st.state_id
      FROM
            dbo.site s
            JOIN dbo.address a ON a.address_id = s.primary_address_id
            JOIN dbo.STATE st ON st.state_id = a.state_id
      WHERE
            s.Client_id = @client_id
      ORDER BY
            a.city
           ,st.state_name
 
END

GO
GRANT EXECUTE ON  [dbo].[Site_dtl_Sel_by_Client_id] TO [CBMSApplication]
GO
