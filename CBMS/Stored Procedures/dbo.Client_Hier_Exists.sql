SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                
/******                  
 Name:                  
        [dbo].[Client_Hier_Exists]                
                  
 Description:                  
			  To Check weather Client_Hier_Id exists or not .                  
                  
 Input Parameters:                  
 Name                                 DataType          Default     Description                  
 -------------------------------------------------------------------------------------------------------  
 @Client_Hier_Id_List				  VARCHAR(MAX) 
                  
 Output Parameters:                        
 Name                                 DataType          Default     Description                  
 --------------------------------------------------------------------------------------------------------                  
                  
 Usage Examples:                      
 -------------------------------------------------------------------------------------------------------  

	 EXEC dbo.Client_Hier_Exists 123
                 
 Author Initials:                  
      Initials       Name                  
 -------------------------------------------------------------------------------------------------------  
       SP            Sandeep Pigilam    
                   
 Modifications:                  
      Initials       Date           Modification                  
 -------------------------------------------------------------------------------------------------------  
       SP            2014-01-21		Created for RA Admin user management Bulk Upload                
                 
******/                  
                
CREATE PROCEDURE [dbo].[Client_Hier_Exists]
      ( 
       @Client_Hier_Id_List VARCHAR(MAX) )
AS 
BEGIN                
      SET NOCOUNT ON;     
                
      DECLARE @Client_Hier_Ids TABLE ( Client_Hier_Id INT )              
               
              
      INSERT      INTO @Client_Hier_Ids
                  ( 
                   Client_Hier_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Client_Hier_Id_List, ',') ufn  
                        

      SELECT
            MAX(CASE WHEN ch.Client_Hier_Id IS NULL THEN 1
                     ELSE 0
                END) Is_Client_Hier_Missing
      FROM
            @Client_Hier_Ids te
            LEFT OUTER JOIN core.Client_Hier ch
                  ON ch.Client_Hier_Id = te.Client_HIer_Id
      GROUP BY
            Te.Client_Hier_Id
                
                                                 
                       
END 



;
GO
GRANT EXECUTE ON  [dbo].[Client_Hier_Exists] TO [CBMSApplication]
GO
