SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.GET_EXPECTED_VOLUME_FOR_INTERNAL_COUNTERPARTY_POSITION_REPORT_P 
@userId varchar,
@sessionId varchar,
@year Varchar(12),
@fromDate Varchar(12),
@toDate Varchar(12),
@unitId int


as
set nocount on
select 	CAST( volumedetails.VOLUME * consumption.CONVERSION_FACTOR  as decimal(20,3)) EXPECTED_VOLUME,
	CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER,
	volumedetails.SITE_ID

from	RM_FORECAST_VOLUME_DETAILS volumedetails,
	RM_FORECAST_VOLUME volume,
	RM_ONBOARD_HEDGE_SETUP onboard ,
	CONSUMPTION_UNIT_CONVERSION consumption					 					

where	volumedetails.SITE_ID IN( select DISTINCT site.SITE_ID

				  from	SITE site,
					DIVISION division,
					RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	division.CLIENT_ID IN(	select distinct CLIENT_ID
								from	RM_DEAL_TICKET 
								where	RM_COUNTERPARTY_ID is not null AND
									HEDGE_LEVEL_TYPE_ID=( select  ENTITY_ID
											      from    ENTITY
											      where   ENTITY_TYPE=262 AND
											  	      ENTITY_NAME like 'corporate'
								                            ) 
					                      )AND
					division.DIVISION_ID=site.DIVISION_ID AND
					site.SITE_ID=hedge.SITE_ID AND
					hedge.INCLUDE_IN_REPORTS=1 AND
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
				)AND

	volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
	volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
					    from  RM_FORECAST_VOLUME
					    where FORECAST_YEAR =@year
					GROUP BY CLIENT_ID
				    )	AND
	volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromdate, 101) AND CONVERT(Varchar(12), @todate,101) AND

	volumedetails.SITE_ID=onboard.SITE_ID AND
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId


UNION		


select 	CAST( volumedetails.VOLUME * consumption.CONVERSION_FACTOR  as decimal(20,3)) EXPECTED_VOLUME,
	CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER,
	volumedetails.SITE_ID

from	RM_FORECAST_VOLUME_DETAILS volumedetails,
	RM_FORECAST_VOLUME volume,
	RM_ONBOARD_HEDGE_SETUP onboard ,
	CONSUMPTION_UNIT_CONVERSION consumption					 					

where	volumedetails.SITE_ID IN( select DISTINCT site.SITE_ID

				  from	SITE site,
					DIVISION division,
					RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	division.DIVISION_ID IN(select distinct DIVISION_ID
								from	RM_DEAL_TICKET 
								where	RM_COUNTERPARTY_ID is not null AND
									HEDGE_LEVEL_TYPE_ID=( select  ENTITY_ID
											      from    ENTITY
											      where   ENTITY_TYPE=262 AND
											  	      ENTITY_NAME like 'division'
								                            ) 
					                      )AND
					division.DIVISION_ID=site.DIVISION_ID AND
					site.SITE_ID=hedge.SITE_ID AND
					hedge.INCLUDE_IN_REPORTS=1 AND
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
 	
				) AND
	volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
	volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
					    from  RM_FORECAST_VOLUME
					    where FORECAST_YEAR =@year
					GROUP BY CLIENT_ID
				    )	AND
	volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromdate, 101) AND CONVERT(Varchar(12), @todate,101) AND

	volumedetails.SITE_ID=onboard.SITE_ID AND
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId


UNION		


select 	CAST( volumedetails.VOLUME * consumption.CONVERSION_FACTOR  as decimal(20,3)) EXPECTED_VOLUME,
	CONVERT (Varchar(12), volumedetails.MONTH_IDENTIFIER, 101) MONTH_IDENTIFIER,
	volumedetails.SITE_ID

from	RM_FORECAST_VOLUME_DETAILS volumedetails,

	RM_FORECAST_VOLUME volume,
	RM_ONBOARD_HEDGE_SETUP onboard ,
	CONSUMPTION_UNIT_CONVERSION consumption					 					

where	volumedetails.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID IN(select distinct SITE_ID
							 from	RM_DEAL_TICKET 
						 	 where	RM_COUNTERPARTY_ID is not null AND
								HEDGE_LEVEL_TYPE_ID=( select  ENTITY_ID
										      from    ENTITY
										      where   ENTITY_TYPE=262 AND
										  	      ENTITY_NAME like 'site'
							                            ) 
					                )AND				

					hedge.INCLUDE_IN_REPORTS=1 AND
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
 	
				) AND

	volumedetails.RM_FORECAST_VOLUME_ID=volume.RM_FORECAST_VOLUME_ID AND		
	volume.FORECAST_AS_OF_DATE IN(select MAX(FORECAST_AS_OF_DATE) 
					    from  RM_FORECAST_VOLUME
					    where FORECAST_YEAR =@year
					GROUP BY CLIENT_ID
				    )	AND
	volumedetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromdate, 101) AND CONVERT(Varchar(12), @todate,101) AND

	volumedetails.SITE_ID=onboard.SITE_ID AND
	onboard.VOLUME_UNITS_TYPE_ID=consumption.BASE_UNIT_ID AND
	consumption.CONVERTED_UNIT_ID=@unitId
GO
GRANT EXECUTE ON  [dbo].[GET_EXPECTED_VOLUME_FOR_INTERNAL_COUNTERPARTY_POSITION_REPORT_P] TO [CBMSApplication]
GO
