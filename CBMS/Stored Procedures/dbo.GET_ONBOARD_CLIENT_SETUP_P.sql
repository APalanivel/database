SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ONBOARD_CLIENT_SETUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_ONBOARD_CLIENT_SETUP_P 
@userId varchar(10),
@sessionId varchar(20),
@clientId integer
as
set nocount on
select 	RM_onboard_client_id,
client_id,
max_hedge_percent,
contact_name,
contact_office_phone,
contact_cell_phone,
contact_fax_number,
contact_email,
CONVERT(VARCHAR(12),onboard_date, 101)onboard_date,

comments

from	RM_ONBOARD_CLIENT  onboard
where	onboard.client_id=@clientId
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_CLIENT_SETUP_P] TO [CBMSApplication]
GO
