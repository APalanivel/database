SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME:        
		dbo.Client_Commodity_INS_By_CSM_Site

DESCRIPTION:      
	Used to insert the clinet commodity for those sites which added later it site table.        		         
	
INPUT PARAMETERS:      
Name			      DataType		Default Description      
------------------------------------------------------------      
@Client_Id				INT,               
@Client_Commodity_Id	INT,              
            
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      

USAGE EXAMPLES:          
------------------------------------------------------------      

   
   
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA  Shobhit Kr Agrawal  
   
MODIFICATIONS         
Initials Date		Modification        
------------------------------------------------------------        
GB					Created      
GB  12-OCT-09	Modified the value for for UOM_CD from 1 to 0,Based on Bug 12107
			
  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Client_Commodity_INS_By_CSM_Site
  @Client_Id INT,               
  @Client_Commodity_Id INT
    
AS              

BEGIN              
    
 SET NOCOUNT ON;  
					
DECLARE @UOM_Cd INT
SELECT @UOM_Cd = UOM_Cd FROM Core.Client_Commodity WHERE Client_Commodity_Id = @Client_Commodity_Id

			
		INSERT INTO Core.Client_Commodity_Detail (Client_Commodity_Id,
													CLIENT_ID,
													Sitegroup_id,
													SITE_ID,
													ACCOUNT_ID,
													Is_GHG_Reported,
													UOM_Cd
													)
			
		SELECT @Client_Commodity_Id,@CLient_ID,s.DIVISION_ID,s.SITE_ID,NULL,0,@UOM_Cd
		FROM
			dbo.SITE s
		LEFT JOIN	
			Core.Client_Commodity_Detail ccd  WITH(NOLOCK) 
			ON ccd.client_id = s.client_id
				AND ccd.SITE_ID = s.SITE_ID
				AND ccd.Client_commodity_id = @Client_Commodity_Id
		WHERE S.Client_ID = @Client_Id
			AND ccd.SITE_ID IS NULL
END
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_INS_By_CSM_Site] TO [CBMSApplication]
GO
