SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/******
NAME:
	CBMS.dbo.Variance_Comment_Ins_For_Account_Commodity

DESCRIPTION:

	Used to save variance Comments added for the other service cost_usages.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Account_Id		Int
	@Commodity_Id	Int
	@Service_Month	Date
	@User_Info_Id	Int
	@Comment_Text	Varchar(max)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Variance_Comment_Ins_For_Account_Commodity 157988,67,'2009-01-01',49,'Usage is correct'
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	AP			Arunkumar Palanivel

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	03/17/2010	Created
	HG			03/26/2010	Codeset name corrrected to "CommentType" which used to get Commenttype code
	AP			Feb 21,2020	Modified procedure to accept internal and external comments in 
******/

CREATE PROCEDURE [dbo].[Variance_Comment_Ins_For_Account_Commodity]
      @Account_Id      INT
    , @Commodity_Id    INT
    , @Service_Month   DATE
    , @User_Info_Id    INT
	,@Internal_Comment_Text NVARCHAR(MAX)
	,@external_comment_text NVARCHAR(MAX)
    , @TVP_Comments AS TVP_Category_Ids READONLY
AS
      BEGIN

            SET NOCOUNT ON;

            DECLARE
                  @internal_Comment_Type_Cd INT
                , @external_Comment_Type_Cd INT
                , @internal_Comment_id      INT
                , @external_comment_id      INT 
				,@inserted_value int;				           

		   SELECT @internal_Comment_Type_Cd= code_id FROM code 
		   WHERE Code_Value ='varianceinternalcomment'

		   	   SELECT  @external_Comment_Type_Cd = code_id FROM code 
		   WHERE Code_Value ='varianceexternalcomment'

            BEGIN TRY
                  BEGIN TRAN;

				  --internal comment insert

				  IF (@Internal_Comment_Text IS NOT NULL)
				  BEGIN
                  
                  EXEC dbo.Comment_Ins
                        @internal_Comment_Type_Cd
                      , @User_Info_Id
                      , NULL
                      , @Internal_Comment_Text
                      , @internal_Comment_id OUT; 


					     EXEC dbo.Cost_Usage_Account_Dtl_Comment_Map_Ins
					           @Account_Id = @Account_Id                           -- int
					         , @Commodity_Id = -1                        -- int
					         , @Service_Month = @Service_Month             -- date
					         , @Comment_ID = @internal_Comment_id
							                            -- int
					         , @inserted_value = @inserted_value OUTPUT  -- int 

							 IF EXISTS (SELECT 1 FROM @TVP_Comments)
							 begin
					     
					  INSERT INTO dbo.Variance_Comment_Category_Map (
					                                                      Cost_Usage_Account_Dtl_Comment_Map_Id
					                                                    , Variance_Category_Cd
					                                                )
				

								    SELECT DISTINCT  @inserted_value,c.Code_Id FROM @TVP_Comments TVP
								   JOIN  dbo.code c 
						ON tvp.category_Name = c.Code_Value
						JOIN dbo.Codeset cs
						ON cs.Codeset_Id = c.Codeset_Id
						WHERE cs.Codeset_name ='VarianceCategory'
						END

                        ELSE
                        
						BEGIN 

						 INSERT INTO dbo.Variance_Comment_Category_Map (
					                                                      Cost_Usage_Account_Dtl_Comment_Map_Id
					                                                    , Variance_Category_Cd
					                                                )

						SELECT DISTINCT  @inserted_value,NULL FROM @TVP_Comments TVP
							
						END
					


					  END

					  --external comment insert

					  IF (@external_comment_text IS NOT NULL) 
					  BEGIN

					    EXEC dbo.Comment_Ins
                        @external_Comment_Type_Cd
                      , @User_Info_Id
                      , NULL
                      , @external_comment_text
                      , @external_comment_id OUT;

					  SET @inserted_value = NULL

                    EXEC dbo.Cost_Usage_Account_Dtl_Comment_Map_Ins
					           @Account_Id = @Account_Id                           -- int
					         , @Commodity_Id = -1                        -- int
					         , @Service_Month = @Service_Month             -- date
					         , @Comment_ID = @external_comment_id                           -- int
					         , @inserted_value = @inserted_value OUTPUT  -- int 

							  INSERT INTO dbo.Variance_Comment_Category_Map (
					                                                      Cost_Usage_Account_Dtl_Comment_Map_Id
					                                                    , Variance_Category_Cd
					                                                )
				

								    SELECT DISTINCT  @inserted_value,c.Code_Id FROM @TVP_Comments TVP
								   JOIN  dbo.code c 
						ON tvp.category_Name = c.Code_Value
						JOIN dbo.Codeset cs
						ON cs.Codeset_Id = c.Codeset_Id
						WHERE cs.Codeset_name ='VarianceCategory'

					  END

                  COMMIT TRAN;
            END TRY
            BEGIN CATCH
                  ROLLBACK TRAN;

                  EXEC dbo.usp_RethrowError
                        'Error While saving Variance Comments';

            END CATCH;

      END;
GO
GRANT EXECUTE ON  [dbo].[Variance_Comment_Ins_For_Account_Commodity] TO [CBMSApplication]
GO
