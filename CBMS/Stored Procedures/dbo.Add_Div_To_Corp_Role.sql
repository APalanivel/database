SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
  
NAME: dbo.Add_Div_To_Corp_Role  
  
DESCRIPTION: To add a division and its sites to the corporate role of the client. 
  
INPUT PARAMETERS:      
 Name			   DataType          Default     Description      
------------------------------------------------------------------      
 @DivisionId       INT  
      
OUTPUT PARAMETERS:
 Name              DataType          Default     Description      
------------------------------------------------------------      

  
USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.Add_Div_To_Corp_Role 601
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 PNR	  Pandarinath  
  
MODIFICATIONS  
  
 Initials	Date		Modification  
------------------------------------------------------------  
 PNR		11/23/2010	created as part proj : Security Roles Administration.
 
******/  

CREATE PROCEDURE dbo.Add_Div_To_Corp_Role
    (
		@Division_Id	INT
    )
AS 
BEGIN  
	
	SET NOCOUNT ON ;
	
	DECLARE
		 @Corp_Role_Id	INT
		,@Client_Id		INT
	
	SELECT 
		 @Corp_Role_Id = sr.Security_Role_Id
		,@Client_Id = dd.CLIENT_ID
	FROM 
		dbo.Security_Role sr
		JOIN dbo.Division_Dtl dd
			 ON dd.CLIENT_ID = sr.Client_Id
	WHERE
		dd.SiteGroup_Id = @Division_Id
		AND is_corporate = 1
	
	INSERT INTO dbo.Security_Role_Client_Hier
	(
		 Security_Role_Id
		,Client_Hier_Id
    )
	SELECT 
		  @Corp_Role_Id
		 ,ch.Client_Hier_Id 
	FROM
		Core.Client_Hier ch
		JOIN dbo.Code c 
			 ON ch.Hier_level_Cd = c.Code_Id
		JOIN dbo.Codeset cs 
			 ON cs.Codeset_Id = c.Codeset_Id
	WHERE
		ch.Client_Id = @Client_Id
		AND ch.Sitegroup_Id = @Division_Id
		AND cs.Codeset_Name = 'HierLevel'
		AND c.Code_Value in ('Division','Site')
		
END
GO
GRANT EXECUTE ON  [dbo].[Add_Div_To_Corp_Role] TO [CBMSApplication]
GO
