SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********  
NAME:  dbo.TOU_Schedule_Del  
  
DESCRIPTION:  
  
Used to Delete TOU Schedule information for selected TOU Schedule Id  
  
INPUT PARAMETERS:  
      Name       DataType          Default     Description  
------------------------------------------------------------  
   @Time_Of_Use_Schedule_Id  INT  
   @User_Info_Id     INT  
  
OUTPUT PARAMETERS:  
      Name        DataType          Default     Description  
------------------------------------------------------------  
  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
  
 select * FROM dbo.Time_Of_Use_Schedule where Time_Of_Use_Schedule_Id=132  
 select * FROM dbo.Time_Of_Use_Schedule_Term where Time_Of_Use_Schedule_Id=132  
 BEGIN TRAN  
  Exec dbo.TOU_Schedule_Del 132,49  
 select * FROM dbo.Time_Of_Use_Schedule where Time_Of_Use_Schedule_Id=132  
 select * FROM dbo.Time_Of_Use_Schedule_Term where Time_Of_Use_Schedule_Id=132  
 ROLLBACK TRAN  

  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 BCH  Balaraju  
  
 Initials Date  Modification  
------------------------------------------------------------  
 BCH  2012-07-09  Created  
******/  

CREATE PROCEDURE dbo.TOU_Schedule_Del
( 
 @Time_Of_Use_Schedule_Id INT
,@User_Info_Id INT )
AS 
BEGIN  
      SET NOCOUNT ON  
        
      DECLARE
            @LookUp_Value XML
           ,@User_Name VARCHAR(100)
           ,@Client_Name VARCHAR(200)
           ,@Current_Ts DATETIME
           ,@Counter INT
           ,@Term_Count INT
           ,@Time_Of_Use_Schedule_Term_Id INT  
             
             
      DECLARE @TOU_Schedule_Term_List TABLE
		( 
		 Id INT IDENTITY(1, 1)
		,Time_Of_Use_Schedule_Term_Id INT )  
              
      INSERT INTO
            @TOU_Schedule_Term_List
            ( 
             Time_Of_Use_Schedule_Term_Id )
            SELECT
                  Time_Of_Use_Schedule_Term_Id
            FROM
                  dbo.Time_Of_Use_Schedule_Term
            WHERE
                  Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id  
              
      SET @Term_Count = @@ROWCOUNT
        
      SELECT
            @User_Name = FIRST_NAME + space(1) + LAST_NAME
      FROM
            dbo.USER_INFO UI
      WHERE
            USER_INFO_ID = @User_Info_Id  
  
      BEGIN TRY  
  
            BEGIN TRAN  
  
            SET @Counter = 1    

            WHILE ( @Counter <= @Term_Count ) 
                  BEGIN    
    
                        SELECT
                              @Time_Of_Use_Schedule_Term_Id = Time_Of_Use_Schedule_Term_Id
                        FROM
                              @TOU_Schedule_Term_List
                        WHERE
                              Id = @Counter    
    
                        EXEC dbo.TOU_Schedule_Term_Del @Time_Of_Use_Schedule_Term_Id, @User_Info_Id  
       
                        SET @Counter = @Counter + 1

                  END    

            SELECT
                  @LookUp_Value = ( SELECT
                                          Time_Of_Use_Schedule_Id
                                         ,Schedule_Name
                                         ,Commodity_Id
                                         ,VENDOR_ID
                                    FROM
                                          dbo.Time_Of_Use_Schedule
                                    WHERE
                                          Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
                  FOR
                                    XML AUTO )     
            

            DELETE FROM
                  dbo.Time_Of_Use_Schedule
            WHERE
                  Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id  
              
            SET @Current_Ts = GETDATE()  
      
            EXEC dbo.Application_Audit_Log_Ins NULL, 'Delete Time Of Use Schedule', -1, 'Time_Of_Use_Schedule', @Lookup_Value, @User_Name, @Current_Ts    
  
            COMMIT TRAN  
      END TRY  
      BEGIN CATCH  
            IF @@TRANCOUNT > 0 
                  ROLLBACK  
            EXECUTE dbo.usp_RethrowError  
      END CATCH  
  
END

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Del] TO [CBMSApplication]
GO
