SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.GET_UTILITY_BUDGET_COMMENTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	VARCHAR(10)	          	
	@sessionId     	VARCHAR(20)	          	
	@utilityId     	INT       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	exec GET_UTILITY_BUDGET_COMMENTS_P '','',

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SKA			4-Aug-2009	Modified as per coding standards	        	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.GET_UTILITY_BUDGET_COMMENTS_P
	@userId VARCHAR(10),
	@sessionId VARCHAR(20),
	@utilityId INT
AS

SET NOCOUNT ON

	SELECT 	comments.UTILITY_BUDGET_COMMENTS_ID, 
		comments.COMMENTS,
		comments.COMMENT_DATE,
		ent1.ENTITY_ID ENTITYID,
		ent1.ENTITY_NAME as ENTITYNAME,
		info.USERNAME
	FROM UTILITY_BUDGET_COMMENTS comments
	INNER JOIN
		ENTITY ent1
	ON comments.COMMODITY_TYPE_ID = ent1.entity_id
	INNER JOIN
		USER_INFO info
	ON comments.USER_INFO_ID =info.USER_INFO_ID
	WHERE comments.VENDOR_ID = @utilityId
	ORDER BY ENTITYID,
		comments.COMMENT_DATE
GO
GRANT EXECUTE ON  [dbo].[GET_UTILITY_BUDGET_COMMENTS_P] TO [CBMSApplication]
GO
