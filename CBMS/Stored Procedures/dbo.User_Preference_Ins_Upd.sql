SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 	dbo.User_Preference_Ins_Upd  
  
DESCRIPTION:  
  
INPUT PARAMETERS:  
	Name					DataType	Default		Description  
----------------------------------------------------------------  
	@User_Info_Id			INT
    @User_Preference_Cd		INT   
	@User_Preference_Value	NVARCHAR(MAX)  
   
OUTPUT PARAMETERS:  
	 Name		DataType	Default		Description  
----------------------------------------------------------------  
  
USAGE EXAMPLES:  
----------------------------------------------------------------  
	
	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'User Preference keys', @Code_Value = 'RFP Checklist Columns'
	SELECT TOP 10 * FROM dbo.User_Preference
	SELECT TOP 10 * FROM dbo.USER_INFO ui
      
	BEGIN TRANSACTION
		SELECT * FROM dbo.User_Preference up WHERE User_Info_Id = 2 AND User_Preference_Cd = 102226
		EXEC dbo.User_Preference_Ins_Upd 2,'RFP Checklist Columns','Client|Site|Account Numer'
		SELECT * FROM dbo.User_Preference up WHERE User_Info_Id = 2 AND User_Preference_Cd = 102226
	ROLLBACK TRANSACTION
    
   
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	RR			Raghu Reddy
   
MODIFICATIONS  
  
	Initials	Date		Modification  
------------------------------------------------------------  
	RR			2016-09-21	MAINT-4236 Created
	
******/
  
CREATE PROCEDURE [dbo].[User_Preference_Ins_Upd]
      ( 
       @User_Info_Id INT
      ,@User_Preference_Code_Value VARCHAR(25)
      ,@User_Preference_Value NVARCHAR(MAX) )
AS 
BEGIN  
  
      SET NOCOUNT ON; 
      
      DECLARE @User_Preference_Cd INT
      
      SELECT
            @User_Preference_Cd = c.Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'User Preference keys'
            AND c.Code_Value = @User_Preference_Code_Value
      
      DELETE FROM
            dbo.User_Preference
      WHERE
            User_Info_Id = @User_Info_Id
            AND User_Preference_Cd = @User_Preference_Cd
  
      INSERT      INTO dbo.User_Preference
                  ( 
                   User_Info_Id
                  ,User_Preference_Cd
                  ,User_Preference_Value )
      VALUES
                  ( 
                   @User_Info_Id
                  ,@User_Preference_Cd
                  ,@User_Preference_Value )
      
                 
END;
;
GO
GRANT EXECUTE ON  [dbo].[User_Preference_Ins_Upd] TO [CBMSApplication]
GO
