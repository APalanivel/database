SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   PROCEDURE [dbo].[cbmsInvoiceParticipation_Get]
	( @MyAccountId int
	, @invoice_participation_id int
	)
AS
BEGIN

	   select invoice_participation_id
		, account_id
		, site_id
		, service_month
		, is_expected
		, is_received
		, recalc_under_review
		, variance_under_review
	     from invoice_participation  WITH (NOLOCK) 
	    where invoice_participation_id = @invoice_participation_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_Get] TO [CBMSApplication]
GO
