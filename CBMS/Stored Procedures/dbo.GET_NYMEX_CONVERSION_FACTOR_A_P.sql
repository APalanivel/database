SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






























-- executed in 03_30
--exec dbo.GET_NYMEX_CONVERSION_FACTOR_A_P -1,-1,16,25

CREATE   PROCEDURE dbo.GET_NYMEX_CONVERSION_FACTOR_A_P 
@userId varchar,
@sessionId varchar,
@unitId int,
@baseUnitId int

as

select	isnull(CAST(consumption.CONVERSION_FACTOR as decimal(15,3)),'1.000' ) VOLUME_CONVERSION_FACTOR	
		
from	CONSUMPTION_UNIT_CONVERSION consumption
	
where	consumption.BASE_UNIT_ID=@baseUnitId AND
	consumption.CONVERTED_UNIT_ID=@unitId 

















































GO
GRANT EXECUTE ON  [dbo].[GET_NYMEX_CONVERSION_FACTOR_A_P] TO [CBMSApplication]
GO
