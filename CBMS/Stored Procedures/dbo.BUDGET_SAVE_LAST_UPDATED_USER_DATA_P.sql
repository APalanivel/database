SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.BUDGET_SAVE_LAST_UPDATED_USER_DATA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@updateTypeName	varchar(20)	          	
	@userInfoId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

--select * from budget_nymex_forecast 



CREATE     PROC dbo.BUDGET_SAVE_LAST_UPDATED_USER_DATA_P

@clientId int ,	
@updateTypeName  varchar(20) ,
@userInfoId int



	AS
declare 
@updateTypeId int
	begin
		set nocount on

		select @updateTypeId =  entity_id from entity where entity_name = @updateTypeName
		if((select count(*) from BUDGET_CLIENT_UPDATE_INFO
		where CLIENT_ID=@clientId 
		and UPDATE_TYPE_ID = @updateTypeId)=1)
		begin
			update	BUDGET_CLIENT_UPDATE_INFO 
			set	UPDATED_DATE = getDate() ,
 				USER_INFO_ID =@userInfoId  
			where	CLIENT_ID=@clientId 
			and	UPDATE_TYPE_ID = @updateTypeId

		end

		else
		begin
			insert into BUDGET_CLIENT_UPDATE_INFO 
			(
			CLIENT_ID , 
			UPDATE_TYPE_ID ,
			UPDATED_DATE ,
			USER_INFO_ID 
			) 
			values 
			(
			@clientId ,
			@updateTypeId ,
			getDate() ,
			@userInfoId 
			)
		end

	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_SAVE_LAST_UPDATED_USER_DATA_P] TO [CBMSApplication]
GO
