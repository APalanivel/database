
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
	dbo.Report_UserInfo_Permission
	
DESCRIPTION:
	This report is to get All the User Information of Client \Clients

INPUT PARAMETERS:
Name			DataType		Default	Description
-------------------------------------------------------------------------------------------
@CLIENT_LIST	VARCHAR(MAX)
@User_LIST		VARCHAR(MAX)
@IS_history		INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
Report_UserInfo_Permission 1016,'3137,14322',1,'2','2,3,4,7,13,14,15,18,19,20,21'
	
AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
SSR			Sharad srivastava
AKR         Ashok Kumar Raju
SP		    Sandeep Pigilam

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
 SSR	   07/28/2010	Created
 AKR       2012-10-04   Added parameters @Group_List and @Permission_List
 SP		   2014-02-26   Modified for RA-Admin used Client_App_Access_Role_Group_Info_Map also to get user groups. 	  	
*/

CREATE PROC [dbo].[Report_UserInfo_Permission]
      ( 
       @CLIENT_List VARCHAR(MAX)
      ,@User_List VARCHAR(MAX)
      ,@IS_history INT
      ,@Group_List VARCHAR(MAX)
      ,@Permission_List VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON
       
      DECLARE @ent_id INT  
          
      DECLARE @Client_List_Temp TABLE
            ( 
             Client_Id INT PRIMARY KEY )  
          
      DECLARE @Group_List_Temp TABLE
            ( 
             Group_Id INT PRIMARY KEY )  
      DECLARE @Permission_List_Temp TABLE
            ( 
             Permiussion_Id INT PRIMARY KEY )  
          
      DECLARE @User_List_Temp TABLE
            ( 
             User_Info_Id INT PRIMARY KEY )  
          
          
      INSERT      INTO @Client_List_Temp
                  ( 
                   Client_Id )
                  SELECT
                        ufn_Client.segments
                  FROM
                        dbo.ufn_split(@CLIENT_LIST, ',') ufn_Client  
                          
                          
                                  
      INSERT      INTO @Group_List_Temp
                  ( 
                   Group_Id )
                  SELECT
                        ufn_Group.segments
                  FROM
                        dbo.ufn_split(@Group_LIST, ',') ufn_Group  
                          
                                  
      INSERT      INTO @Permission_List_Temp
                  ( 
                   Permiussion_Id )
                  SELECT
                        ufn_Per.segments
                  FROM
                        dbo.ufn_split(@Permission_List, ',') ufn_Per  
                          
                                  
      INSERT      INTO @User_List_Temp
                  ( 
                   USER_INFO_ID )
                  SELECT
                        ufn_User.segments
                  FROM
                        dbo.ufn_split(@User_LIST, ',') ufn_User  
         
  
      SELECT
            @ent_id = ent.ENTITY_ID
      FROM
            dbo.ENTITY ent
      WHERE
            ent.ENTITY_NAME = 'USER_INFO'
            AND ent.ENTITY_DESCRIPTION = 'Table_Type'  
  
      SELECT
            uif.First_name
           ,uif.LAST_NAME
           ,uif.USERNAME
           ,CASE WHEN uif.IS_HISTORY = 1 THEN 'Yes'
                 ELSE 'No'
            END [Moved to History]
           ,c.CLIENT_NAME
           ,uif.locale_code
           ,ent_aud.MODIFIED_DATE DateAdded
           ,gif.GROUP_NAME
           ,per_inf.PERMISSION_NAME
           ,uif.EMAIL_ADDRESS
           ,uif.PHONE_NUMBER
           ,user_cr.FIRST_NAME + SPACE(1) + user_cr.LAST_NAME [Created By]
      FROM
            dbo.User_info uif
            JOIN @User_List_Temp ult
                  ON ult.User_Info_Id = uif.USER_INFO_ID
            JOIN dbo.CLIENT c
                  ON c.CLIENT_ID = uif.CLIENT_ID
            JOIN @Client_List_Temp clt
                  ON clt.Client_Id = c.CLIENT_ID
            JOIN ( SELECT
                        uif.USER_INFO_ID
                       ,uif.GROUP_INFO_ID
                   FROM
                        dbo.USER_INFO_GROUP_INFO_MAP uif
                   UNION
                   SELECT
                        uicar.User_Info_Id
                       ,cargm.GROUP_INFO_ID
                   FROM
                        dbo.User_Info_Client_App_Access_Role_Map uicar
                        INNER JOIN dbo.Client_App_Access_Role_Group_Info_Map cargm
                              ON uicar.Client_App_Access_Role_Id = cargm.Client_App_Access_Role_Id
                   GROUP BY
                        uicar.User_Info_Id
                       ,cargm.GROUP_INFO_ID ) uif_map
                  ON uif_map.USER_INFO_ID = uif.USER_INFO_ID
            JOIN dbo.GROUP_INFO gif
                  ON gif.GROUP_INFO_ID = uif_map.GROUP_INFO_ID
            JOIN @Group_List_Temp glt
                  ON glt.Group_Id = gif.GROUP_INFO_ID
            JOIN dbo.GROUP_INFO_PERMISSION_INFO_MAP gr_per
                  ON gr_per.GROUP_INFO_ID = gif.GROUP_INFO_ID
            JOIN dbo.PERMISSION_INFO per_inf
                  ON per_inf.PERMISSION_INFO_ID = gr_per.PERMISSION_INFO_ID
            JOIN @Permission_List_Temp plt
                  ON plt.Permiussion_Id = per_inf.PERMISSION_INFO_ID
            JOIN dbo.ENTITY_AUDIT ent_aud
                  ON ent_aud.ENTITY_IDENTIFIER = uif.USER_INFO_ID
            JOIN dbo.USER_INFO user_cr
                  ON user_cr.USER_INFO_ID = ent_aud.USER_INFO_ID
      WHERE
            ent_aud.AUDIT_TYPE = 1
            AND ent_aud.ENTITY_ID = @ent_id
            AND ( @IS_history = -1
                  OR uif.IS_HISTORY = @IS_history )  
       
	
END;

;
GO


GRANT EXECUTE ON  [dbo].[Report_UserInfo_Permission] TO [CBMS_SSRS_Reports]
GO
