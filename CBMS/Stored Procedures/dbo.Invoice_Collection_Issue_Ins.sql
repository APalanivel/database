SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:   dbo.Invoice_Collection_Issue_Ins               
                      
Description:                      
   This sproc to get the attribute details for a Given id's.              
                                   
 Input Parameters:                      
    Name            DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
    @Invoice_Collection_Queue_Id      VARCHAR(MAX)      
    @Invoice_Collection_Issue_Type_Cd     INT      
    @Invoice_Collection_Issue_Entity_Owner_Cd   INT      
    @Issue_Owner_User_Info_Id       INT      
    @Invoice_Internal_Comment       NVARCHAR(MAX) = NULL      
    @Invoice_External_Comment       NVARCHAR(MAX) = NULL      
    @User_Info_Id          INT                           
         
 Output Parameters:                            
    Name        DataType   Default   Description                        
----------------------------------------------------------------------------------------                        
                      
 Usage Examples:                          
----------------------------------------------------------------------------------------           
        
  EXEC Invoice_Collection_Issue_Ins '141,142',102362,102257,49,null,null,49      
            
Author Initials:                      
    Initials  Name                      
----------------------------------------------------------------------------------------                        
 RKV    Ravi Kumar Vegesna        
 Modifications:                      
    Initials        Date   Modification                      
----------------------------------------------------------------------------------------                        
    RKV    2016-12-29  Created For Invoice_Collection.         
 RKV    2018-05-31  Create new temp table for collecting the distinct Invoice_Collection_Queue_Id's                 
 RKV    2019-06-28  Added two new parameters as part of IC Project.      
 RKV    2019-08-14  Added @Next_Action_Date save when creating issue.      
 RKV    2019-09-18  Added two new columns Is_Automatic_Link,Issue_Link_Start_Date  
 RKV	2019-10-03  Added logic for issue autolinking     
                     
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Ins]
(
    @Invoice_Collection_Queue_Id VARCHAR(MAX),
    @Invoice_Collection_Issue_Type_Cd INT,
    @Invoice_Collection_Issue_Entity_Owner_Cd INT,
    @Issue_Owner_User_Info_Id INT = NULL,
    @Invoice_Internal_Comment NVARCHAR(MAX) = NULL,
    @Invoice_External_Comment NVARCHAR(MAX) = NULL,
    @User_Info_Id INT,
    @CBMS_Image_Ids VARCHAR(MAX) = NULL,
    @Contact_Level_Cd INT = -1,
    @Is_Blocker BIT = 0,
    @Blocker_Action_Date DATE = NULL,
    @Next_Action_Date DATE = NULL,
    @Is_Automatic_Link BIT = 0,
    @Issue_Link_Start_Date DATE = NULL
)
AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @IC_Activity_Cd INT,
            @Invoice_Collection_Activity_Id INT,
            @IC_Issue_Open_Status_Cd INT,
            @Internal_Issue_event_Type_Cd INT,
            @External_Issue_event_Type_Cd INT,
            @Invoice_Collection_Activity_Log_Id INT;
    DECLARE @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel tvp_Invoice_Collection_Queue_Account_Config_Id_Sel;

    CREATE TABLE #Invoice_Collection_Queue
    (
        Invoice_Collection_Queue_Id INT
    );


    INSERT INTO #Invoice_Collection_Queue
    (
        Invoice_Collection_Queue_Id
    )
    SELECT ufn.Segments
    FROM dbo.ufn_split(@Invoice_Collection_Queue_Id, ',') ufn
    GROUP BY ufn.Segments;

    SELECT @IC_Activity_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Activity_Type_Cd'
          AND c.Code_Value = 'Create Issue';

    SELECT @IC_Issue_Open_Status_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Chase_Status_Cd'
          AND c.Code_Value = 'Open';

    SELECT @Internal_Issue_event_Type_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
          AND c.Code_Value = 'IC Internal Comments';

    SELECT @External_Issue_event_Type_Cd = c.Code_Id
    FROM dbo.Code c
        INNER JOIN dbo.Codeset cs
            ON cs.Codeset_Id = c.Codeset_Id
    WHERE cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
          AND c.Code_Value = 'IC External Comments';


    BEGIN TRY
        BEGIN TRAN;

        DECLARE @Invoice_Collection_Issue_Id INT;


        INSERT INTO dbo.Invoice_Collection_Activity
        (
            Invoice_Collection_Activity_Type_Cd,
            Created_User_Id,
            Is_Automatic_Link,
            Issue_Link_Start_Date
        )
        SELECT @IC_Activity_Cd,
               @User_Info_Id,
               @Is_Automatic_Link,
               @Issue_Link_Start_Date;

        SELECT @Invoice_Collection_Activity_Id = IDENT_CURRENT('Invoice_Collection_Activity');



        INSERT INTO dbo.Invoice_Collection_Activity_Log
        (
            Invoice_Collection_Activity_Id,
            Next_Action_Dt,
            Created_User_Id
        )
        SELECT @Invoice_Collection_Activity_Id,
               @Next_Action_Date,
               @User_Info_Id
        WHERE @Next_Action_Date IS NOT NULL;

        SELECT @Invoice_Collection_Activity_Log_Id = MAX(Invoice_Collection_Activity_Log_Id)
        FROM dbo.Invoice_Collection_Activity_Log ical
        WHERE ical.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
              AND @Next_Action_Date IS NOT NULL;

        INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
        (
            Invoice_Collection_Activity_Log_Id,
            Invoice_Collection_Queue_Id,
            Collection_Start_Dt,
            Collection_End_Dt
        )
        SELECT @Invoice_Collection_Activity_Log_Id,
               ticq.Invoice_Collection_Queue_Id,
               icq.Collection_Start_Dt,
               icq.Collection_End_Dt
        FROM #Invoice_Collection_Queue ticq
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = ticq.Invoice_Collection_Queue_Id
        WHERE @Next_Action_Date IS NOT NULL;





        INSERT INTO dbo.Invoice_Collection_Issue_Log
        (
            Invoice_Collection_Activity_Id,
            Invoice_Collection_Queue_Id,
            Invoice_Collection_Issue_Type_Cd,
            Issue_Status_Cd,
            Issue_Entity_Owner_Cd,
            Issue_Owner_User_Id,
            Created_User_Id,
            Updated_User_Id,
            Type_Of_Issue_Owner_Cd,
            Collection_Start_Dt,
            Collection_End_Dt,
            Is_Blocker,
            Blocker_Action_Date
        )
        SELECT @Invoice_Collection_Activity_Id,
               icq.Invoice_Collection_Queue_Id,
               @Invoice_Collection_Issue_Type_Cd,
               @IC_Issue_Open_Status_Cd,
               @Invoice_Collection_Issue_Entity_Owner_Cd,
               @Issue_Owner_User_Info_Id,
               @User_Info_Id,
               @User_Info_Id,
               @Contact_Level_Cd,
               icq.Collection_Start_Dt,
               icq.Collection_End_Dt,
               @Is_Blocker,
               @Blocker_Action_Date
        FROM dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN #Invoice_Collection_Queue us
                ON us.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id;


        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Updated_User_Id
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @Internal_Issue_event_Type_Cd,
               @Invoice_Internal_Comment,
               @User_Info_Id,
               @User_Info_Id
        FROM dbo.Invoice_Collection_Issue_Log ici
        WHERE @Invoice_Internal_Comment IS NOT NULL
              AND ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;


        INSERT INTO dbo.Invoice_Collection_Issue_Event
        (
            Invoice_Collection_Issue_Log_Id,
            Issue_Event_Type_Cd,
            Event_Desc,
            Created_User_Id,
            Updated_User_Id
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               @External_Issue_event_Type_Cd,
               @Invoice_External_Comment,
               @User_Info_Id,
               @User_Info_Id
        FROM dbo.Invoice_Collection_Issue_Log ici
        WHERE @Invoice_External_Comment IS NOT NULL
              AND ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;


        INSERT INTO dbo.Invoice_Collection_Issue_Attachment
        (
            Invoice_Collection_Issue_Log_Id,
            Cbms_Image_Id,
            Created_User_Id
        )
        SELECT ici.Invoice_Collection_Issue_Log_Id,
               f.Segments,
               @User_Info_Id
        FROM dbo.ufn_split(@CBMS_Image_Ids, ',') f
            CROSS APPLY dbo.Invoice_Collection_Issue_Log ici
        WHERE ici.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;

        UPDATE icq
        SET icq.Is_Locked = 1,
            icq.Next_Action_Dt = CASE
                                     WHEN @Next_Action_Date IS NOT NULL THEN
                                         @Next_Action_Date
                                     ELSE
                                         icq.Next_Action_Dt
                                 END,
            icq.Last_Change_Ts = GETDATE()
        FROM dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Queue us
                ON us.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id;

  --      INSERT INTO @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel
  --      (
  --          Invoice_Collection_Queue_Id,
  --          Invoice_Collection_Account_Config_Id,
  --          Invoice_Collection_Issue_Log_Id
  --      )
  --      SELECT icq2.Invoice_Collection_Queue_Id,
  --             icq2.Invoice_Collection_Account_Config_Id,
  --             @Invoice_Collection_Issue_Id
  --      FROM dbo.Invoice_Collection_Queue icq
  --          INNER JOIN dbo.Invoice_Collection_Queue icq2
  --              ON icq2.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
  --          INNER JOIN #Invoice_Collection_Queue icq3
  --              ON icq.Invoice_Collection_Queue_Id = icq3.Invoice_Collection_Queue_Id;

        
		--SELECT Invoice_Collection_Queue_Id,Invoice_Collection_Account_Config_Id,Invoice_Collection_Issue_Log_Id 
		--FROM @tvp_Invoice_Collection_Queue_Account_Config_Id_Sel

		EXEC dbo.Invoice_Collection_Issue_Link_Auto_By_ActivityId @Invoice_Collection_Activity_Id,
			                                              @User_Info_Id   

        COMMIT TRAN;
    END TRY
    BEGIN CATCH

        IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRAN;
        END;

        EXEC dbo.usp_RethrowError;

    END CATCH;


END;







GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Ins] TO [CBMSApplication]
GO
