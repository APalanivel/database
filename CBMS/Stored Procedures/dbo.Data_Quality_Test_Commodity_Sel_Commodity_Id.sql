SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:      
	dbo.Data_Quality_Test_Commodity_Sel_Commodity_Id

DESCRIPTION:
		Created For Data Quality Tests - Global Level Config Page.
		
 INPUT PARAMETERS:      
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------      
	
 OUTPUT PARAMETERS:     
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------   
   
 USAGE EXAMPLES:      
-------------------------------------------------------------------------------------------------      

	EXEC dbo.Data_Quality_Test_Commodity_Sel_Commodity_Id 290
	EXEC dbo.Data_Quality_Test_Commodity_Sel_Commodity_Id 67
	
 AUTHOR INITIALS:
 Initials	Name
-------------------------------------------------------------------------------------------------      
 NR			Narayana Reddy
 
 MODIFICATIONS
 Initials	     Date		     Modification
-------------------------------------------------------------------------------------------------      
 NR				2019-04-23		Created Data2.0 -  Data Quality Tests - Global Level Config Page.

******/

CREATE PROCEDURE [dbo].[Data_Quality_Test_Commodity_Sel_Commodity_Id]
    (
        @Commodity_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            dq.Data_Quality_Test_Id AS Data_Quality_Test_Commodity_Id
            , c.Commodity_Name
            , dq.Test_Description
            , dq.Is_Active
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Last_Updated_By
            , ui.USERNAME
            , dq.Last_Change_Ts
        FROM
             dbo.Data_Quality_Test dq
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = dq.Commodity_Id
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = dq.Updated_User_Id
        WHERE
            c.Commodity_Id = @Commodity_Id;


    END;




GO
GRANT EXECUTE ON  [dbo].[Data_Quality_Test_Commodity_Sel_Commodity_Id] TO [CBMSApplication]
GO
