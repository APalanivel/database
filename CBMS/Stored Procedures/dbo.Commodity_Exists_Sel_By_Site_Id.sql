SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.Commodity_Exists_Sel_By_Site_Id  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 @Site_Id   INT    
 @Commodity_Id    INT                 
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
exec Commodity_Exists_Sel_By_Site_Id 128,291  
  
AUTHOR INITIALS:  
 Initials      Name  
------------------------------------------------------------  
 SP           Srinivas Patchava  
  
MODIFICATIONS  
 Initials       Date       Modification  
------------------------------------------------------------  
 SP            2019-05-15  Created for Maint-8506  
         
  
******/

CREATE PROCEDURE [dbo].[Commodity_Exists_Sel_By_Site_Id]
     (
         @Site_Id INT
         , @Commodity_Id INT

     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Is_Commodity_Exist BIT = 0;

        IF EXISTS (   SELECT
                            1
                      FROM
                            Core.Client_Hier ch
                            INNER JOIN Core.Client_Hier_Account cha
                                ON cha.Client_Hier_Id = ch.Client_Hier_Id
                      WHERE
                            ch.Site_Id = @Site_Id
                            AND cha.Commodity_Id = @Commodity_Id)
            BEGIN

                SET @Is_Commodity_Exist = 1;

            END;

        SELECT  @Is_Commodity_Exist Is_Commodity_Exist;

    END;
GO
GRANT EXECUTE ON  [dbo].[Commodity_Exists_Sel_By_Site_Id] TO [CBMSApplication]
GO
