SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[cbmsEntityAudit_Log]
	( @MyAccountId int
	, @table_name varchar(50)
	, @entity_identifier int
	, @audit_type varchar(10)
	)
AS
BEGIN

	/*
	Audit Types, should be lookups
	1 - Create
	2 - Edit
	3 - Remove/Move to History
	*/

	set nocount on

	  declare @audit_type_id int
		, @table_id int


	   select @table_id = entity_id
	     from entity
	    where entity_type = 500
	      and entity_name = @table_name

	if @audit_type = 'Insert'
	begin
		set @audit_type_id = 1
	end
	else if @audit_type = 'Delete'
	begin
		set @audit_type_id = 3
	end
	else
	begin
		set @audit_type_id = 2
	end

	if @table_id is not null and @audit_type is not null
	begin

		insert into entity_audit
			( entity_id, entity_identifier, user_info_id, audit_type, modified_date )
			values
			( @table_id
			, @entity_identifier
			, @MyAccountId
			, @audit_type_id
			, getdate()
			)

	end

END
GO
GRANT EXECUTE ON  [dbo].[cbmsEntityAudit_Log] TO [CBMSApplication]
GO
