SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_COUNTERPARTY_UNDER_DIVISION_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@divisionId    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.GET_COUNTERPARTY_UNDER_DIVISION_P 
@userId varchar,
@sessionId varchar,
@divisionId int
as
set nocount on
select distinct rm_counterparty.rm_counterparty_id,counterparty_name,1 as is_counter_party

from	rm_deal_ticket,	rm_counterparty , division , RM_ONBOARD_HEDGE_SETUP

where 	rm_deal_ticket.client_id = division.client_id AND
	division.division_id = @divisionId AND
	RM_ONBOARD_HEDGE_SETUP.division_id=division.division_id AND
	RM_ONBOARD_HEDGE_SETUP.INCLUDE_IN_REPORTS=1 AND
	rm_deal_ticket.rm_counterparty_id = rm_counterparty.rm_counterparty_id

UNION

select	distinct vendor.vendor_id as counterparrty_id,
	vendor_name as counterparty_name, 0 as is_counter_party

from	rm_deal_ticket,division,RM_ONBOARD_HEDGE_SETUP,
	vendor

where 	rm_deal_ticket.client_id = division.client_id AND
	division.division_id = @divisionId AND
	RM_ONBOARD_HEDGE_SETUP.division_id=division.division_id AND
	RM_ONBOARD_HEDGE_SETUP.INCLUDE_IN_REPORTS=1 AND
	rm_deal_ticket.vendor_id = vendor.vendor_id

order by counterparty_name
GO
GRANT EXECUTE ON  [dbo].[GET_COUNTERPARTY_UNDER_DIVISION_P] TO [CBMSApplication]
GO
