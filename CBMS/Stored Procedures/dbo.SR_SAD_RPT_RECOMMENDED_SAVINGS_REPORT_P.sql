SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@regionId      	int       	          	
	@stateId       	int       	          	
	@analystId     	int       	          	
	@commodityId   	int       	          	
	@clientId      	int       	          	
	@siteId        	int       	          	
	@supplierId    	int       	          	
	@utilityId     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 3,0,0,291,0,0,0,0
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 3,0,0,0,0,0,0,0
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,5,0,0,0,0,0,0
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,9,0,0,0,0,0  -- analyst
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,0,290,0,0,0,0 --commodity
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,0,0,1005,0,0,0 -- client
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,0,0,0,2498,0,0 -- site
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,0,0,0,0,1091,0 -- supplier
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,0,0,0,0,0,0,258 -- util
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 5,5,0,0,0,0,0,0
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 1,0,9,0,0,0,0,0 -- region and analyst
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 1,0,0,0,1005,0,0,0 -- region and client
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 1,0,0,0,0,2498,0,0 -- region ans site
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 1,0,0,0,0,0,1091,0 -- region and supplier 
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 1,0,0,0,0,0,0,258  -- regopm amd utility
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,5,9,0,0,0,0,0    -- state and analyst
 EXEC dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P 0,5,291,0,0,0,0,0    -- state and commodity
 
 select * from entity where entity_type >1000
 select * from contract
 select * from sr_rfp_closure where SAVINGS != ''		is not null
 select * from sr_rfp_closure
 select * from contract
 select * from vwSiteName 
 select * from site where site_name ='Arlington, TX'
 select vendor_id from vendor where vendor_name = 'Enogex Inc. (Pipeline)'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/28/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on.	        	
******/

CREATE PROCEDURE dbo.SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P
    @regionId		INT,
    @stateId		INT,
    @analystId		INT,
    @commodityId	INT,
    @clientId		INT,
    @siteId			INT,
    @supplierId		INT,
    @utilityId		INT
AS 
    BEGIN
    
        SET NOCOUNT ON ;
        
        DECLARE @sqlSelect VARCHAR(1000)
        DECLARE @sqlFrom VARCHAR(2000)
        DECLARE @sqlWhere VARCHAR(2000)
        DECLARE @sql VARCHAR(5000)


        SELECT  @sqlSelect = ' select 	
					reg.region_name as REGION_NAME,
					vwSite.state_name as STATE_NAME,
					userInfo.first_name+SPACE(1)+userInfo.last_name as ANALYST,
					commodity_type.entity_name as COMMODITY,
					cli.client_name as CLIENT_NAME,
					vwSite.site_name as SITE_NAME,
					isnull(supp.vendor_name, '''') as VENDOR_NAME,
					utilacc.account_number as ACCOUNT_NUMBER,
					util.vendor_name as UTILITY,
					isnull(con.ed_contract_number, '''') AS CONTRACT_NUMBER,
				 	rfp_closure.SAVINGS,
					rfp_closure.SAVINGS_COMMENTS '

        SELECT  @sqlFrom = ' from 	dbo.sr_rfp_closure rfp_closure left join dbo.contract con on con.contract_id = rfp_closure.contract_id
					left join dbo.vendor supp on  rfp_closure.vendor_id = supp.vendor_id,
					dbo.sr_rfp_account rfp_account ,
				    dbo.sr_rfp rfp,
					dbo.account utilacc,
					dbo.vendor util,
					--vendor supp,
					dbo.vwSiteName vwSite,
					dbo.client cli,
					dbo.region reg,
					dbo.state state,
					dbo.user_info userInfo,
					dbo.entity commodity_type '

        SELECT  @sqlWhere = ' where rfp_closure.is_bid_group = 0
					and rfp_closure.sr_account_group_id  = rfp_account.sr_rfp_account_id
					and rfp_account.account_id = utilacc.account_id
					and rfp_account.is_deleted = 0
					and utilacc.vendor_id = util.vendor_id
					and rfp_account.sr_rfp_id = rfp.sr_rfp_id
					and rfp.initiated_by = userInfo.user_info_id
					--and rfp_closure.vendor_id = supp.vendor_id
					and vwSite.site_id = utilacc.site_id
					and cli.client_id = vwSite.client_id
					and vwSite.state_id = state.state_id
					and state.region_id = reg.region_id
					and rfp.COMMODITY_TYPE_ID = commodity_type.ENTITY_ID					and rfp_closure.SAVINGS <> '''' and rfp_closure.SAVINGS is not null '
					
        IF ( @regionId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and reg.region_id ='
                        + STR(@regionId) 
	
            END


        IF ( @stateId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and state.state_id ='
                        + STR(@stateId) 
	
            END


        IF ( @analystId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere
                        + ' and  userInfo.user_info_id =' + STR(@analystId) 
	
            END


        IF ( @commodityId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and rfp.COMMODITY_TYPE_ID ='
                        + STR(@commodityId) 
	
            END


        IF ( @clientId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and cli.client_id ='
                        + STR(@clientId) 
	
            END

        IF ( @siteId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and vwSite.site_id ='
                        + STR(@siteId) 
	
            END


        IF ( @supplierId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and rfp_closure.vendor_id ='
                        + STR(@supplierId) 
	
            END

        IF ( @utilityId > 0 ) 
            BEGIN
                SELECT  @sqlWhere = @sqlWhere + ' and utilacc.vendor_id  ='
                        + STR(@utilityId) 
	
            END
            
        SELECT  @sql = @sqlSelect + @sqlFrom + @sqlWhere
				
        EXEC ( @sql )
        

END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_RECOMMENDED_SAVINGS_REPORT_P] TO [CBMSApplication]
GO
