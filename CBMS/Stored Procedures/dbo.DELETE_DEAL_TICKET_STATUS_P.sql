SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DELETE_DEAL_TICKET_STATUS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	
	@dealTicketStatusType	int       	          	
	@dealTicketStatus	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE      PROCEDURE dbo.DELETE_DEAL_TICKET_STATUS_P  

@userId varchar,
@sessionId varchar,
@dealTicketId int, 
@dealTicketStatusType int, 
@dealTicketStatus varchar(200)

AS
DECLARE @statusId int 

	SELECT	@statusId = ENTITY_ID FROM ENTITY 
	WHERE 	ENTITY_TYPE=@dealTicketStatusType AND 
		ENTITY_NAME=@dealTicketStatus

	BEGIN
		DELETE FROM RM_DEAL_TICKET_TRANSACTION_STATUS 
		WHERE 	RM_DEAL_TICKET_ID = @dealTicketId AND 
			DEAL_TRANSACTION_STATUS_TYPE_ID = @statusId
	END
GO
GRANT EXECUTE ON  [dbo].[DELETE_DEAL_TICKET_STATUS_P] TO [CBMSApplication]
GO
