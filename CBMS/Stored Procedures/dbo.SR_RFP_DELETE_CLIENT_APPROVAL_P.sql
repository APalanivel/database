
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_CLIENT_APPROVAL_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@accountId           int,
	@rfp_id              int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
EXEC SR_RFP_DELETE_CLIENT_APPROVAL_P 2000560, 17985, 0, 1577

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR		Deana Ritter
    AKR     Ashok Kumar Raju
    
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 AKR      2012-01-21 Changes Summit to Schnider Electric as a part of Name Change Project

******/

CREATE PROCEDURE [dbo].[SR_RFP_DELETE_CLIENT_APPROVAL_P]
      ( 
       @cbmsImageId INT
      ,@srAccountGroupId INT
      ,@isBidGroup INT
      ,@rfp_Id INT )
AS 
BEGIN

      SET NOCOUNT ON
	
	--added by prasad start  
	
      DECLARE @temp TABLE ( [site_id] [INT] )
	--DECLARE @allsitetemp TABLE ([site_id] [INT])
      DECLARE @allsitetemp TABLE
            ( 
             Site_Index INT IDENTITY(1, 1)
            ,site_id INT )
	
      DECLARE
            @siteId INT
           ,@projectId INT
           ,@clientapprovaluploadedaccountcount INT
           ,@SiteCnt INT
           ,@SiteIndex INT
           ,@account INT
           ,@project_status_type_id INT
           ,@tempSiteCnt INT		
		   
      INSERT      INTO @allsitetemp
                  ( 
                   site_id )
                  SELECT
                        Acct.Site_id
                  FROM
                        dbo.Sr_Rfp_Sop_Client_Approval clientapp
                        JOIN dbo.Sr_Rfp_Account rfpacct
                              ON rfpacct.Sr_Rfp_Account_id = clientapp.Sr_Account_Group_id
                        JOIN dbo.Account Acct
                              ON Acct.account_id = rfpacct.account_id
                  WHERE
                        clientapp.SR_ACCOUNT_GROUP_ID = @srAccountGroupId
                        AND clientapp.IS_BID_GROUP = @isBidGroup
                  UNION
                  SELECT
                        Acct.site_id
                  FROM
                        dbo.Sr_Rfp_Sop_Client_Approval clientapp
                        JOIN dbo.sr_rfp_account rfpacct
                              ON clientapp.sr_account_group_id = rfpacct.sr_rfp_bid_group_id
                        JOIN dbo.account Acct
                              ON rfpacct.account_id = Acct.account_id
                  WHERE
                        clientapp.SR_ACCOUNT_GROUP_ID = @srAccountGroupId
                        AND clientapp.IS_BID_GROUP = @isBidGroup

      SET @SiteCnt = @@ROWCOUNT -- Gathering the no of records inserted in to table variable @allsitetemp

	--added by prasad end

      DELETE FROM
            dbo.SR_RFP_SOP_CLIENT_APPROVAL
      WHERE
            SR_ACCOUNT_GROUP_ID = @srAccountGroupId
            AND cbms_image_id = @cbmsImageId
            AND IS_BID_GROUP = @isBidGroup

	--DELETE FROM dbo.Cbms_Image
	--WHERE cbms_image_id = @cbmsImageId

      SET @SiteCnt = isnull(@SiteCnt, 0)
      SET @SiteIndex = 1

	--WHILE (( SELECT COUNT(site_id) FROM @allsitetemp) > 0)
      WHILE ( @SiteIndex < = @SiteCnt ) 
            BEGIN --//WHILE

                  SELECT
                        @siteId = Site_ID
                  FROM
                        @allsitetemp
                  WHERE
                        Site_Index = @SiteIndex

		--IF (select count(*) from RFP_SITE_PROJECT where RFP_ID = @rfp_id AND SITE_ID = @siteId ) > 0

                  SELECT
                        @projectId = SSO_PROJECT_ID
                  FROM
                        dbo.RFP_SITE_PROJECT
                  WHERE
                        RFP_ID = @rfp_id
                        AND SITE_ID = @siteId
                  IF @@ROWCOUNT > 0 -- If any records fetched by the above query
                        BEGIN --if

			--added by saritha 10.12.07
			--getting client uploaded accountcount for that site

                              SET @account = 0

                              SELECT
                                    @account = count(rfpAcct.sr_rfp_account_id)
                              FROM
                                    dbo.sr_rfp_sop_client_approval clientapp
                                    INNER JOIN dbo.sr_rfp_account rfpAcct
                                          ON rfpAcct.sr_rfp_account_id = clientapp.sr_account_group_id
                                    INNER JOIN dbo.account Acct
                                          ON Acct.account_id = rfpAcct.account_id
				--INNER JOIN dbo.cbms_image img ON unnecessar join
                              WHERE
                                    rfpAcct.sr_rfp_id = @rfp_id
                                    AND rfpAcct.is_deleted = 0
                                    AND clientapp.is_bid_group = 0
                                    AND Acct.site_id = @siteId

                              SELECT
                                    @account = isnull(@account, 0) + count(rfpAcct.sr_rfp_bid_group_id)
                              FROM
                                    dbo.sr_rfp_sop_client_approval clientapp
                                    INNER JOIN dbo.sr_rfp_account rfpAcct
                                          ON rfpAcct.sr_rfp_bid_group_id = clientapp.sr_account_group_id
                                    INNER JOIN dbo.account Acct
                                          ON Acct.account_id = rfpAcct.account_id
				--INNER JOIN dbo.cbms_image img
                              WHERE
                                    rfpAcct.sr_rfp_id = @rfp_id
                                    AND rfpAcct.is_deleted = 0
                                    AND clientapp.is_bid_group = 1
                                    AND Acct.site_id = @siteId

                              SET @clientapprovaluploadedaccountcount = @account  

                              IF ( @clientapprovaluploadedaccountcount ) = 0 
                                    BEGIN--if1   

				--finding smr upload is done for that site  
  
				--INSERT INTO @temp (site_id)
				--SELECT DISTINCT Acct.site_id
				
                                          SET @tempSiteCnt = 0
				
                                          SELECT
                                                @tempSiteCnt = count(Acct.site_id)
                                          FROM
                                                dbo.sr_rfp_sop_smr smr
                                                JOIN dbo.sr_rfp_account rfpacct
                                                      ON rfpacct.sr_rfp_account_id = smr.sr_account_group_id
                                                JOIN dbo.cbms_image img
                                                      ON img.cbms_image_id = smr.cbms_image_id
                                                JOIN dbo.account Acct
                                                      ON rfpacct.account_id = Acct.account_id
                                          WHERE
                                                smr.is_bid_group = 0
                                                AND rfpacct.sr_rfp_id = @rfp_id
                                                AND rfpacct.is_deleted = 0
                                                AND img.cbms_image_type_id = 1200 --//for smr upload
                                                AND Acct.Site_ID = @siteid -- added by hari to remove the @temp table usage
				--INSERT INTO @temp (site_id)
				--SELECT DISTINCT Acct.site_id

                                          SELECT
                                                @tempSiteCnt = @tempSiteCnt + count(Acct.site_id)
                                          FROM
                                                dbo.sr_rfp_sop_smr smr
                                                JOIN dbo.sr_rfp_account rfpacct
                                                      ON rfpacct.sr_rfp_bid_group_id = smr.sr_account_group_id
                                                JOIN dbo.cbms_image img
                                                      ON img.cbms_image_id = smr.cbms_image_id
                                                JOIN dbo.account Acct
                                                      ON Acct.account_id = rfpacct.account_id
                                          WHERE
                                                smr.is_bid_group = 1
                                                AND rfpacct.sr_rfp_id = @rfp_id
                                                AND rfpacct.is_deleted = 0
                                                AND img.cbms_image_type_id = 1200 --//for smr upload
                                                AND Acct.Site_ID = @siteid -- added by hari to remove the @temp table usage

				--IF (select count (site_id) from @temp where site_id = @siteId)=0 --sendtosuplier is not done
				
				--IF NOT EXISTS (SELECT 1 FROM @temp WHERE site_id = @siteId) --sendtosuplier is not done
                                          IF @tempSiteCnt = 0 
                                                BEGIN--if2
  
                                                      DELETE
                                                            dbo.sso_project_step
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND step_no = 4
					 
                                                      DELETE
                                                            dbo.sso_project_activity
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND activity_description = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.'
  
  
                                                END --if2  
                                          ELSE 
                                                BEGIN --if smr uploaded
				 
                                                      UPDATE
                                                            dbo.SSO_PROJECT_STEP
                                                      SET   
                                                            IS_ACTIVE = 1
                                                           ,IS_COMPLETE = 0
                                                      WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND STEP_NO = 4
  
                                                END --sent to rfp
				 --finding clientapproval is done for that site  
				 
				--DELETE FROM @temp
  
                                          SET @tempSiteCnt = 0
				--INSERT INTO @temp(site_id)
                                          SELECT
                                                @tempSiteCnt = count(Acct.site_id)
                                          FROM
                                                dbo.sr_rfp_closure rfpClosure
                                                JOIN dbo.sr_rfp_account rfpacct
                                                      ON rfpacct.sr_rfp_account_id = rfpClosure.sr_account_group_id
                                                JOIN dbo.Account Acct
                                                      ON Acct.account_id = rfpacct.account_id
                                          WHERE
                                                rfpacct.sr_rfp_id = @rfp_id
                                                AND rfpacct.is_deleted = 0
                                                AND Acct.Site_ID = @siteid -- added by hari to remove the @temp table usage
					
				--IF (select count (site_id) from @temp where site_id = @siteId)=0 --close is not done  				
                                          IF @tempSiteCnt = 0 
                                                BEGIN--if3
  
                                                      DELETE
                                                            dbo.sso_project_step
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND step_no = 5
					
                                                      DELETE
                                                            dbo.sso_project_activity
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND activity_description = 'Schneider Electric is taking action to implement the approved path forward. Resource Advisor will reflect the actions taken and will categorize any new supporting information, contracts or other documents.'
						
					--added for 18499
                                                      SELECT
                                                            @project_status_type_id = Entity_id
                                                      FROM
                                                            dbo.Entity (NOLOCK)
                                                      WHERE
                                                            Entity_Name = 'Open'
                                                            AND Entity_Type = 601
					
                                                      UPDATE
                                                            dbo.Sso_Project
                                                      SET   
                                                            project_status_type_id = @project_status_type_id
                                                      WHERE
                                                            sso_project_id = @projectid
  
                                                END --if3
                                    END --end if1
			--ended by saritha 10.12.07  
                              ELSE 
                                    BEGIN --else
  
                                          UPDATE
                                                dbo.SSO_PROJECT_STEP
                                          SET   
                                                IS_ACTIVE = 1
                                               ,IS_COMPLETE = 0
                                          WHERE
                                                SSO_PROJECT_ID = @projectId
                                                AND STEP_NO = 3

                                    END
  
                        END --if
  		
		--DELETE FROM @allsitetemp where site_id = @siteId
                  SET @SiteIndex = @SiteIndex + 1
	
            END --//while  
  
--end by prasad
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_CLIENT_APPROVAL_P] TO [CBMSApplication]
GO
