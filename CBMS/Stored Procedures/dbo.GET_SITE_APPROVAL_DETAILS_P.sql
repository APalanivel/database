SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_SITE_APPROVAL_DETAILS_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@userId			VARCHAR
@sessionId		VARCHAR
@clientId		INT

OUTPUT PARAMETERS:     
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.GET_SITE_APPROVAL_DETAILS_P 1,1,235

EXEC dbo.GET_SITE_APPROVAL_DETAILS_P 1,1,10069

AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------ 
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						Client, Division, Site tables replaced by Client_Hier table

*/

CREATE PROCEDURE dbo.GET_SITE_APPROVAL_DETAILS_P
	@userId		VARCHAR(10)
	,@sessionId	VARCHAR(20)
	,@clientId	INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		approval.rm_manage_forecast_approval_id
		,approval.site_id
		,approval.cbms_image_id
		,cimage.cbms_doc_id
		,approval.description
	FROM
		dbo.RM_MANAGE_FORECAST_APPROVAL approval
		INNER JOIN dbo.CBMS_IMAGE cimage
			ON approval.cbms_image_id = cimage.cbms_image_id
		INNER JOIN Core.Client_Hier ch
			ON ch.Site_Id = approval.Site_Id
	WHERE
		ch.Client_id =  @clientId

END
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_APPROVAL_DETAILS_P] TO [CBMSApplication]
GO
