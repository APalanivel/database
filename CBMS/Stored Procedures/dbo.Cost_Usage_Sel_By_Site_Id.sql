
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME: [DBO].[Cost_Usage_Sel_By_Site_Id]

DESCRIPTION:
	
	To Get Information from Cost_Usage_Site_dtl table by Selected site client_hier_id with pagination.

INPUT PARAMETERS:
	NAME				DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Clinet_Hier_Id		INT	
	@Start_Index		INT			1
	@End_Index			INT			2147483647

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec Cost_Usage_Sel_By_Site_Id 3096 , 1,50

	select top 10 * from Cost_Usage_Site_Dtl
	select top 10 * from  core.client_hier where client_hier_id=1204
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	RR			Raghu Reddy
	BCH			Balaraju

MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			25-JUN-10	CREATED   
	RR			2012-02-01	Removed the following for ep and ng applied to to get cost and usage serivce months 
							EP ((EL_USAGE!=0 OR EL_COST!=0)	AND (EL_ON_PEAK_USAGE!=0 OR EL_OFF_PEAK_USAGE!=0 OR EL_INT_PEAK_USAGE!=0 OR EL_DEMAND!=0 OR EL_TAX!=0))
							NG ((NG_USAGE!=0 OR NG_COST!=0) AND (NG_TAX!=0))
	RR			2012-02-13	Added the condition to check for any one bucket, cost or usage sholud not be zero. From CBMS data entry page deleting data
							for a service month is nothing but making all the parameters as zeros of the respective commodity
    BCH			2012-03-30  Removed part of the script of cost_usage_site references.
*/
CREATE PROCEDURE dbo.Cost_Usage_Sel_By_Site_Id
      ( 
       @Site_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @Client_Hier_Id INT
      SELECT
            @Client_Hier_Id = Client_Hier_Id
      FROM
            core.Client_Hier
      WHERE
            Site_Id = @Site_Id ;
            
      WITH  Cte_Service_month_List
              AS ( SELECT
                        Service_Month
                       ,Row_Num = ROW_NUMBER() OVER ( ORDER BY SERVICE_MONTH )
                       ,Total_Rows = count(1) OVER ( )
                   FROM
                        dbo.Cost_Usage_Site_Dtl
                   WHERE
                        Client_Hier_Id = @Client_Hier_Id
                   GROUP BY
                        Service_Month)
            SELECT
                  Service_Month
                 ,Total_Rows
            FROM
                  Cte_Service_month_List
            WHERE
                  Row_Num BETWEEN @Start_Index AND @End_Index
END
;
GO


GRANT EXECUTE ON  [dbo].[Cost_Usage_Sel_By_Site_Id] TO [CBMSApplication]
GO
