
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:
 dbo.[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Textron]

 DESCRIPTION:

	This procedure selects the comments associated to the report master id.

 INPUT PARAMETERS:
	Name			DataType	Default		Description
------------------------------------------------------------
   @site_id			INT
   @begin_date		DATETIME
   @end_date		DATETIME

 OUTPUT PARAMETERS:
 Name   DataType  Default Description
------------------------------------------------------------

  USAGE EXAMPLES:
------------------------------------------------------------
set Statistics Profile on 

	Exec [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Textron] 1758, '2010-01-01', '2010-12-31'


AUTHOR INITIALS:
 Initials	Name
------------------------------------------------------------
 JR			Jon Ruel
 MM			Moses Morales
 DR			Deana Ritter
 SKA		Shobhit Kr Agrawal
 BCH		Balaraju
 NR			Narayana Reddy
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
MM		1/22/2010	Created from script of cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite and changed the
					Natural Gas units to CCF
BCH     2012-04-02  Used core.client_hier and cost_usage_site_dtl tables, Removed client,site and cost_usage_site tables ,
					And Removed the part of the script and that tables references
							
							LEFT  JOIN currency_unit_conversion cuc
											  ON cuc.base_unit_id = cu.currency_unit_id
												 AND cuc.converted_unit_id = cu.currency_unit_id
												 AND cuc.conversion_date = cu.service_month
												 AND cuc.currency_group_id = ch.Client_Currency_Group_Id
							LEFT  JOIN consumption_unit_conversion elcon
								  ON elcon.base_unit_id = cu.UOM_Type_Id
									 AND elcon.converted_unit_id = cu.UOM_Type_Id
					
 NR      2016-05-31  MAINT-3789 Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity.              

******/
CREATE PROCEDURE [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Textron]
      ( 
       @site_id INT
      ,@begin_date DATETIME
      ,@end_date DATETIME )
AS 
BEGIN
      SET NOCOUNT ON;
      DECLARE
            @ccfunitID INT
           ,@ccfunitname VARCHAR(25)
           ,@ccmunitID INT
           ,@ccmunitname VARCHAR(25)
           ,@EL_COMMODITY_TYPE_ID INT
           ,@kvarhunitID INT
           ,@kvarhunitname VARCHAR(25)

      SELECT
            @ccfunitID = ENTITY_ID
           ,@ccfunitname = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'Ccf'
            AND comtype.Commodity_Name = 'Natural Gas'

      SELECT
            @kvarhunitID = ENTITY_ID
           ,@kvarhunitname = ent.ENTITY_NAME
      FROM
            dbo.ENTITY ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.ENTITY_TYPE
      WHERE
            ent.ENTITY_NAME = 'kvarh'
            AND comtype.Commodity_Name = 'Electric Power'

      SELECT
            @ccmunitID = entity_id
           ,@ccmunitname = ent.entity_name
      FROM
            dbo.ENTITY ent
            JOIN dbo.Commodity comtype
                  ON comtype.UOM_Entity_Type = ent.entity_type
      WHERE
            ent.entity_name = 'Ccm'
            AND comtype.Commodity_Name = 'Natural Gas'
      SELECT
            @EL_COMMODITY_TYPE_ID = Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            Commodity_Name = 'Electric Power'

      SELECT
            ch.site_id
           ,ch.site_name
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) AS el_cost
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                   END, 0) AS el_usage
           ,CASE WHEN ( CASE WHEN bm.bucket_name = 'Total Usage'
                                  AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                        END ) > 0 THEN CONVERT(DECIMAL(32, 16), ( CASE WHEN bm.bucket_name = 'Total Cost'
                                                                            AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                                                                  END ) / ( CASE WHEN bm.bucket_name = 'Total Usage'
                                                                                      AND com.commodity_name = 'Electric Power' THEN cu.bucket_value
                                                                            END ))
                 ELSE CONVERT(DECIMAL(32, 16), 0)
            END AS el_unit_cost
           ,'Electric Power' AS el_commodity_type
           ,elunit.UBM_UNIT_OF_MEASURE_CODE AS el_unit_of_measure_type
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) AS ng_cost
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) * ISNULL(ngcon.conversion_factor, 0) AS ng_usage
           ,@ccfunitname AS ng_unit_of_measure_type
           ,ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                             AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                   END, 0) * ISNULL(ngcon2.conversion_factor, 0) AS ng_usage2
           ,@ccmunitname AS ng_unit_of_measure_type2
           ,CASE WHEN ( ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                                         AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                               END, 0) * ISNULL(ngcon.conversion_factor, 0) ) > 0 THEN CONVERT(DECIMAL(32, 16), ( ISNULL(CASE WHEN bm.bucket_name = 'Total Cost'
                                                                                                                                   AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                                                                                                                         END, 0) ) / ( ISNULL(CASE WHEN bm.bucket_name = 'Total Usage'
                                                                                                                                                        AND com.commodity_name = 'Natural Gas' THEN cu.bucket_value
                                                                                                                                              END, 0) * ISNULL(ngcon.conversion_factor, 0) ))
                 ELSE CONVERT(DECIMAL(32, 16), 0)
            END AS ng_unit_cost
           ,'Natural Gas' AS ng_commodity_type
           ,cu.service_month
      FROM
            core.Client_Hier ch
            JOIN dbo.Cost_Usage_Site_Dtl cu
                  ON cu.Client_Hier_Id = ch.Client_Hier_Id
            JOIN dbo.Bucket_Master bm
                  ON cu.Bucket_Master_Id = bm.Bucket_Master_Id
            JOIN dbo.Commodity com
                  ON bm.Commodity_Id = com.Commodity_Id
            JOIN dbo.UBM_UNIT_OF_MEASURE_MAP elunit
                  ON elunit.COMMODITY_TYPE_ID = @EL_COMMODITY_TYPE_ID
                     AND elunit.UNIT_OF_MEASURE_TYPE_ID = cu.UOM_Type_Id
                     AND elunit.UBM_UNIT_OF_MEASURE_MAP_ID = @kvarhunitID
            LEFT JOIN dbo.consumption_unit_conversion ngcon
                  ON ngcon.base_unit_id = cu.UOM_Type_Id
                     AND ngcon.converted_unit_id = @ccfunitID
            LEFT JOIN dbo.consumption_unit_conversion ngcon2
                  ON ngcon2.base_unit_id = cu.UOM_Type_Id
                     AND ngcon2.converted_unit_id = @ccmunitID
      WHERE
            ch.SITE_ID = @site_id
            AND cu.service_month BETWEEN @begin_date
                                 AND     @end_date
            AND ch.Site_NOT_MANAGED <> 1
      ORDER BY
            ch.site_name
END;

;
GO


GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetSummaryDivisionDetailForExportAndSite_Textron] TO [CBMSApplication]
GO
