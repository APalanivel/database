SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[cbmsUser_GetAllInternal]
	( @MyAccountId int )
AS
BEGIN

	   select distinct ui.user_info_id
		, ui.username
		, ui.queue_id
		, null passcode
		, ui.first_name
		, ui.middle_name
		, ui.last_name
		, ui.first_name + ' ' + ui.last_name full_name
		, ui.email_address
		, ui.is_history
		, ui.access_level
		, ui.client_id
		, ui.division_id
		, ui.site_id
	     from user_info ui
  left outer join sr_supplier_contact_info sup on sup.user_info_id = ui.user_info_id
	    where ui.client_id is null
	      and ui.is_history = 0
	      and sup.sr_supplier_contact_info_id is null
	 order by ui.first_name
		, ui.last_name


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUser_GetAllInternal] TO [CBMSApplication]
GO
