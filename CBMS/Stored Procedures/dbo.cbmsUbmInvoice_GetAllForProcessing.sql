SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.cbmsUbmInvoice_GetAllForProcessing

DESCRIPTION:
	Used by Stage-3 IP2 exe to process the UBM Invoices
	
INPUT PARAMETERS:
    Name						DataType			 Default	Description
-------------------------------------------------------------------------
	@MyAccountId				INT
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC cbmsUbmInvoice_GetAllForProcessing 16

AUTHOR INITIALS:
    Initials	Name
------------------------------------------------------------
    HG			Harihara Suthan G

MODIFICATIONS:
    Initials	Date		Modification
------------------------------------------------------------
    HG			2014-06-27	Comments header added
							MAINT-2769 , Created a seperate job in tidal for UBM_CASS_INSERT_MISSING_IMAGES_P2 as it is not necessary to run this sproc every time this sproc gets executed.
								Modified the process to create CU_INVOICE_BATCH only if one record present in UBM_INVOICE to process.
	HG			2016-08-09	Modified the sproc to consider only the batches for which data loading is completed from Stage1 to Stage-2 tables. Using Status_Type in Ubm_Batch_Master_Log table to identify the completed status.
	HG			2018-01-10	MAINT-6704,Modified the script to update the CU_INVOICE_BATCH_Id on UBM_INVOICE to NULL if the batch is failed in between so that those dead invoices can be re processed.
******/
CREATE PROCEDURE [dbo].[cbmsUbmInvoice_GetAllForProcessing] ( @MyAccountId INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE
            @Cu_Invoice_Batch_Id INT
           ,@Invoice_Count INT
           ,@Min_Batch_Master_Log INT
           ,@Ubm_Batch_Job_Success_Status VARCHAR(MAX)
           ,@Current_Ts DATETIME = GETDATE()

      DECLARE @Ubm_Batch_Job_Success_Status_Id TABLE ( Status_Type_Id INT );
      DECLARE @Dead_Invoice_Ubm_Batch_Master_Log_Id TABLE
            (
             Ubm_Batch_Master_Log_Id INT )

      SELECT
            @Ubm_Batch_Job_Success_Status = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'Ubm_Batch_Job_Success_Status'
            AND App_Id = 1;

      INSERT      INTO @Ubm_Batch_Job_Success_Status_Id
                  (Status_Type_Id )
                  SELECT
                        en.ENTITY_ID
                  FROM
                        dbo.ufn_split(@Ubm_Batch_Job_Success_Status, '|') sg
                        INNER JOIN dbo.ENTITY en
                              ON en.ENTITY_NAME = sg.Segments
                  WHERE
                        en.ENTITY_DESCRIPTION = 'UBM_BATCH_PROCESS_STATUS_TYPE';

		---------------------- Dead invoice reprocess ------------------------------
      INSERT      INTO @Dead_Invoice_Ubm_Batch_Master_Log_Id
                  (Ubm_Batch_Master_Log_Id )
                  SELECT
                        bml.UBM_BATCH_MASTER_LOG_ID
                  FROM
                        dbo.UBM_BATCH_MASTER_LOG bml
                        INNER JOIN @Ubm_Batch_Job_Success_Status_Id ss
                              ON ss.Status_Type_Id = bml.Status_Type_Id
                  WHERE
                        bml.START_DATE BETWEEN DATEADD(DAY, -180, @Current_Ts)
                                       AND     @Current_Ts
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.UBM_INVOICE ui
                                     WHERE
                                          ui.CU_INVOICE_BATCH_ID IS NOT NULL
                                          AND ui.IS_PROCESSED = 0
                                          AND ui.UBM_BATCH_MASTER_LOG_ID = bml.UBM_BATCH_MASTER_LOG_ID
                                          AND NOT EXISTS ( SELECT
                                                            1
                                                           FROM
                                                            dbo.CU_INVOICE inv
                                                           WHERE
                                                            inv.UBM_INVOICE_ID = ui.UBM_INVOICE_ID ) )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.UBM_INVOICE ui
                                     WHERE
                                          ui.UBM_BATCH_MASTER_LOG_ID = bml.UBM_BATCH_MASTER_LOG_ID
                                          AND ui.CU_INVOICE_BATCH_ID IS NOT NULL
                                     GROUP BY
                                          ui.UBM_BATCH_MASTER_LOG_ID
                                     HAVING
                                          MAX(ui.PROCESSED_DATE) < DATEADD(MINUTE, -240, @Current_Ts) -- Last processed ts is less than 6 hrs from now.
                                          )     

      UPDATE
            ui
      SET
            ui.CU_INVOICE_BATCH_ID = NULL
      FROM
            dbo.UBM_INVOICE ui
            INNER JOIN @Dead_Invoice_Ubm_Batch_Master_Log_Id dbl
                  ON dbl.Ubm_Batch_Master_Log_Id = ui.UBM_BATCH_MASTER_LOG_ID
      WHERE
            ui.CU_INVOICE_BATCH_ID IS NOT NULL
            AND ui.IS_PROCESSED = 0
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.CU_INVOICE inv
                             WHERE
                              inv.UBM_INVOICE_ID = ui.UBM_INVOICE_ID )

		---------------------- Dead invoice reprocess ------------------------------
 
	-- Create CU Invoice batch if atleast one record available to process in Ubm_Invoice table and the data load has been success for that batch
      IF EXISTS ( SELECT
                        1
                  FROM
                        dbo.UBM_BATCH_MASTER_LOG bml
                        INNER JOIN @Ubm_Batch_Job_Success_Status_Id ss
                              ON ss.Status_Type_Id = bml.Status_Type_Id
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.UBM_INVOICE ui
                                 WHERE
                                    bml.UBM_BATCH_MASTER_LOG_ID = ui.UBM_BATCH_MASTER_LOG_ID
                                    AND ui.CU_INVOICE_BATCH_ID IS NULL
                                    AND ui.IS_PROCESSED = 0
                                    AND ui.IS_QUARTERLY = 0
                                    AND ui.UBM_CLIENT_ID IS NOT NULL
                                    AND ui.CBMS_IMAGE_ID IS NOT NULL ) )
            BEGIN

                  INSERT      INTO dbo.CU_INVOICE_BATCH
                              ( BATCH_DATE )
                  VALUES
                              ( GETDATE() );

                  SET @Cu_Invoice_Batch_Id = SCOPE_IDENTITY();

                  SELECT
                        @Min_Batch_Master_Log = MIN(bml.UBM_BATCH_MASTER_LOG_ID)
                  FROM
                        dbo.UBM_BATCH_MASTER_LOG bml
                        INNER JOIN @Ubm_Batch_Job_Success_Status_Id ss
                              ON ss.Status_Type_Id = bml.Status_Type_Id
                  WHERE
                        EXISTS ( SELECT
                                    1
                                 FROM
                                    dbo.UBM_INVOICE ui
                                 WHERE
                                    bml.UBM_BATCH_MASTER_LOG_ID = ui.UBM_BATCH_MASTER_LOG_ID
                                    AND ui.CU_INVOICE_BATCH_ID IS NULL
                                    AND ui.IS_PROCESSED = 0
                                    AND ui.IS_QUARTERLY = 0
                                    AND ui.UBM_CLIENT_ID IS NOT NULL
                                    AND ui.CBMS_IMAGE_ID IS NOT NULL );
	       
			--set transaction isolation level serializable
                  UPDATE
                        UBM_INVOICE
                  SET
                        CU_INVOICE_BATCH_ID = @Cu_Invoice_Batch_Id
                  WHERE
                        CU_INVOICE_BATCH_ID IS NULL
                        AND IS_PROCESSED = 0
                        AND IS_QUARTERLY = 0
                        AND UBM_CLIENT_ID IS NOT NULL
                        AND CBMS_IMAGE_ID IS NOT NULL
                        AND UBM_BATCH_MASTER_LOG_ID = @Min_Batch_Master_Log;
	                                              

                  SET @Invoice_Count = @@ROWCOUNT;

                  UPDATE
                        CU_INVOICE_BATCH
                  SET
                        INVOICE_COUNT = ISNULL(@Invoice_Count, 0)
                  WHERE
                        CU_INVOICE_BATCH_ID = @Cu_Invoice_Batch_Id;

                  SELECT
                        i.UBM_INVOICE_ID
                       ,i.CBMS_IMAGE_ID
                       ,i.UBM_BATCH_MASTER_LOG_ID
                       ,MIN(det.SERVICE_START_DATE) Begin_Date
                  FROM
                        dbo.UBM_INVOICE i
                        LEFT OUTER JOIN dbo.UBM_INVOICE_DETAILS det
                              ON det.UBM_INVOICE_ID = i.UBM_INVOICE_ID
                  WHERE
                        i.CU_INVOICE_BATCH_ID = @Cu_Invoice_Batch_Id
                  GROUP BY
                        i.UBM_INVOICE_ID
                       ,i.CBMS_IMAGE_ID
                       ,i.UBM_BATCH_MASTER_LOG_ID
                  ORDER BY
                        Begin_Date ASC
                       ,i.UBM_BATCH_MASTER_LOG_ID DESC
                       ,i.CBMS_IMAGE_ID ASC;

            END;

END;
;

;
GO


GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_GetAllForProcessing] TO [CBMSApplication]
GO
