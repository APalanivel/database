SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_ONBOARD_DIVISION_SITES_NAME_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@userId			VARCHAR
@sessionId		VARCHAR
@clientId		INT

OUTPUT PARAMETERS:     
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

EXEC dbo.GET_ONBOARD_DIVISION_SITES_NAME_P 1,1,235

EXEC dbo.GET_ONBOARD_DIVISION_SITES_NAME_P 1,1,10069

AUTHOR INITIALS:
INITIALS	NAME          
------------------------------------------------------------ 
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						Client, Division, Site, VwSitename tables replaced by Client_Hier table

*/
CREATE PROCEDURE dbo.GET_ONBOARD_DIVISION_SITES_NAME_P
	@userId		VARCHAR
	,@sessionId	VARCHAR
	,@clientId	INTEGER
AS
BEGIN

	SET NOCOUNT ON

	SELECT
		ch.site_Id
		,RTRIM(ch.city) + ', ' + ch.State_name + ' (' + ch.site_name + ')'	AS	site_name
		,ch.Sitegroup_Id													AS	division_id
		,ch.Sitegroup_Name													AS	division_name
	FROM
		core.Client_Hier ch
	WHERE
		ch.Client_Id  =  @clientId
		AND ch.Site_Id > 0
	ORDER BY
		division_name
		,Site_Name

END
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_DIVISION_SITES_NAME_P] TO [CBMSApplication]
GO
