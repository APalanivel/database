SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.CU_Invoice_CU_Data_Correction_Batch_Upd
           
DESCRIPTION:             
			To update consolidated billing configurations
			
INPUT PARAMETERS:            
	Name									DataType	Default		Description  
-----------------------------------------------------------------------------------  
	@CU_Invoice_CU_Data_Correction_Batch_Id INT
    @Billing_Start_Dt						DATE
    @Billing_End_Dt							DATE
    @Invoice_Vendor_Type_Id					INT
    @Supplier_Vendor_Id						INT			NULL
    @User_Info_Id							INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  

    EXEC dbo.CODE_SEL_BY_CodeSet_Name 
      @CodeSet_Name = 'Request Status'
	
	SELECT TOP 10 * FROM dbo.CU_Invoice_CU_Data_Correction_Batch
            
	BEGIN TRANSACTION
		Declare @CU_Invoice_CU_Data_Correction_Batch_Id INT
		SELECT * FROM  dbo.CU_Invoice_CU_Data_Correction_Batch WHERE Contract_Id = 2
		EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Ins 2,1,100429,16
		SELECT @CU_Invoice_CU_Data_Correction_Batch_Id = CU_Invoice_CU_Data_Correction_Batch_Id FROM  dbo.CU_Invoice_CU_Data_Correction_Batch 
			WHERE Contract_Id = 2 
		SELECT * FROM  dbo.CU_Invoice_CU_Data_Correction_Batch 
			WHERE CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
		EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Upd @CU_Invoice_CU_Data_Correction_Batch_Id,100431
		SELECT * FROM  dbo.CU_Invoice_CU_Data_Correction_Batch 
			WHERE CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
	ROLLBACK TRANSACTION
	
	
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-09	Contract placeholder - CP-77 Created.
******/

CREATE PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Upd]
      ( 
       @CU_Invoice_CU_Data_Correction_Batch_Id INT
      ,@Status_Cd INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.CU_Invoice_CU_Data_Correction_Batch
      SET   
            Status_Cd = @Status_Cd
           ,Last_Change_Ts = GETDATE()
      WHERE
            CU_Invoice_CU_Data_Correction_Batch_Id = @CU_Invoice_CU_Data_Correction_Batch_Id
                      
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Upd] TO [CBMSApplication]
GO
