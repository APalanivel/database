
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
           
/******            
 NAME: dbo.Client_Group_Info_Map_Sel          
          
 DESCRIPTION:            
      To select the columns for cilent group information map table given the client id.          
            
 INPUT PARAMETERS:            
           
 Name                               DataType        Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @Client_Id                         INT
 @APP_MENU_PROFILE_NAME			VARCHAR(200)		 'DV2'          
            
 OUTPUT PARAMETERS:                  
 Name                               DataType        Default       Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------           
           
 EXEC dbo.Client_Group_Info_Map_Sel 235          
           
 AUTHOR INITIALS:           
           
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
 SP						Sandeep Pigilam            
 MODIFICATIONS:           
            
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2013-11-25      Created for RA Admin user management   
 SP						2014-05-09		RA Admin Bulk Update Order by is changed to GROUP_NAME 
           
******/            
          
CREATE PROCEDURE dbo.Client_Group_Info_Map_Sel
      ( 
       @Client_Id INT
      ,@APP_MENU_PROFILE_NAME VARCHAR(200) = 'DV2' )
AS 
BEGIN  
        
      SET NOCOUNT ON;          
             
      SELECT
            cgf.Group_Info_Id
           ,gf.Group_Info_Category_Id
           ,gic.Category_Name
           ,gf.GROUP_NAME
           ,gf.GROUP_DESCRIPTION
      FROM
            dbo.Client_Group_Info_Map cgf
            INNER JOIN dbo.group_info gf
                  ON cgf.group_info_id = gf.group_info_id
            INNER JOIN dbo.Group_Info_Category gic
                  ON gic.Group_Info_Category_Id = gf.Group_Info_Category_Id
            INNER JOIN dbo.APP_MENU_PROFILE amp
                  ON gic.APP_MENU_PROFILE_ID = amp.APP_MENU_PROFILE_ID
      WHERE
            cgf.Client_Id = @Client_Id
            AND amp.APP_MENU_PROFILE_NAME = @APP_MENU_PROFILE_NAME
      ORDER BY
            gf.GROUP_NAME
                   
                
END;

;
GO

GRANT EXECUTE ON  [dbo].[Client_Group_Info_Map_Sel] TO [CBMSApplication]
GO
