SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:  dbo.Variance_Tests_Billing_Days_SEL    
   
DESCRIPTION:  This will retrieve billing days   
  
INPUT PARAMETERS:      
      Name         DataType		Default     Description      
------------------------------------------------------------      
@account_id			int    
@service_month		datetime    
      
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:    
   
 EXEC Variance_Tests_Billing_Days_SEL 34, 45, '2006-05-01'  
 EXEC Variance_Tests_Billing_Days_SEL 131273, null, '2009-11-01'    
 EXEC Variance_Tests_Billing_Days_SEL 39198, null, '2010-04-01' 
   
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
NK   Nageswara Rao Kosuri           
  
  
Initials	 Date		Modification    
------------------------------------------------------------    
NK			11/04/2009  Created  
SSR			03/12/2010	Added table owner Name and removed with NOLOCK Hints
SSR			03/17/2010	Replaced Cu_invoice_account_map(Account_id) with Cu_invoice_service_month(Account_id)
SKA			04/15/2010	Removed the reference of account & supplier_account_meter_map as we are not selecting any record
						Removed unused input parameter (@site_id)
******/    

CREATE PROCEDURE dbo.Variance_Tests_Billing_Days_SEL    
	  @account_id INT
	 ,@service_month DATETIME        
AS    
BEGIN    
 
 SET NOCOUNT ON
   
	SELECT 
		DATEDIFF(D, MIN(i.Begin_Date), MAX(i.End_Date)) AS Billing_Days  
	FROM    
		dbo.cu_invoice i 
		JOIN dbo.cu_invoice_service_month im 
			ON im.cu_invoice_id = i.cu_invoice_id
	WHERE 
		im.Account_ID = @account_id    
		AND im.service_month = @service_month  
		AND i.is_default = 1
END  
  
GO
GRANT EXECUTE ON  [dbo].[Variance_Tests_Billing_Days_SEL] TO [CBMSApplication]
GO
