SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.CU_Invoice_Charge_Account_Type_Merge

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Invoice_Charge_Account AS CU_Invoice_Charge_Account_Type (its a type as table to accept the input from user as table type)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	06/15/2010	Created
	        	06/21/2010	Removed the inner join clause
												
******/

CREATE PROCEDURE [dbo].[CU_Invoice_Charge_Account_Type_Merge]
      (
       @Invoice_Charge_Account AS CU_Invoice_Charge_Account_Type READONLY )
AS 
BEGIN
      SET NOCOUNT ON ;
 
      MERGE INTO CU_Invoice_Charge_Account AS tgt USING ( SELECT
                                                                  ds1.CU_INVOICE_CHARGE_ACCOUNT_ID
                                                                 ,ds1.CU_INVOICE_CHARGE_ID
                                                                 ,ds1.CU_INVOICE_ID
                                                                 ,ds1.CU_DETERMINANT_CODE
                                                                 ,ds1.ACCOUNT_ID
                                                                 ,ds1.Charge_Expression
                                                                 ,ds1.Charge_Value
                                                                 ,ds1.Is_Delete
                                                               FROM
                                                                  @Invoice_Charge_Account ds1
                                                               ) AS src
	ON tgt.CU_INVOICE_CHARGE_ACCOUNT_ID = src.CU_INVOICE_CHARGE_ACCOUNT_ID 
	WHEN MATCHED AND Is_Delete = 0 THEN UPDATE
      SET   
            tgt.CU_INVOICE_CHARGE_ID = src.CU_INVOICE_CHARGE_ID
           ,tgt.ACCOUNT_ID = src.ACCOUNT_ID
           ,tgt.Charge_Expression = src.Charge_Expression
           ,tgt.Charge_Value = src.Charge_Value 
    WHEN MATCHED AND Is_Delete = 1 THEN DELETE 
    WHEN NOT MATCHED THEN INSERT 
		( CU_INVOICE_CHARGE_ID, ACCOUNT_ID, Charge_Expression, Charge_Value )
      VALUES
            (
             src.CU_INVOICE_CHARGE_ID
            ,src.ACCOUNT_ID
            ,src.Charge_Expression
            ,src.Charge_Value ) ;
END			
	
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Charge_Account_Type_Merge] TO [CBMSApplication]
GO
