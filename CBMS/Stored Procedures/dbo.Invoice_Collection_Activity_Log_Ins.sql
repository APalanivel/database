SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[dbo].[Invoice_Collection_Activity_Log_Ins]

DESCRIPTION:


INPUT PARAMETERS:
	      Name			     				DataType		Default	Description
---------------------------------------------------------------------------------------
       
	@tvp_Invoice_Collection_Queue  tvp_Invoice_Collection_Queue READONLY
    @Next_Action_Date			   Date
	@User_Info_Id				   INT
        	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
DECLARE @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue;

INSERT INTO @tvp_Invoice_Collection_Queue
     (
         Invoice_Collection_Queue_Id
     )
VALUES
    (1 )
    , (2);

EXEC Invoice_Collection_Activity_Log_Ins @tvp_Invoice_Collection_Queue,'2019-08-20',49


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV 		RAVI KUMAR VEGESNA


MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RKV			2019-08-02	 Created
	
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Activity_Log_Ins]
     (
         @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue READONLY
         , @IC_Activity_Type_Cd INT
         , @Next_Action_Date DATE = NULL
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Invoice_Collection_Activity_Id INT
            , @Invoice_Collection_Activity_Log_Id INT
           


       






                BEGIN TRY
                    BEGIN TRAN;


                    INSERT INTO dbo.Invoice_Collection_Activity
                         (
                             Invoice_Collection_Activity_Type_Cd
                             , Created_User_Id
                         )
                    SELECT  @IC_Activity_Type_Cd, @User_Info_Id;




                    SELECT
                        @Invoice_Collection_Activity_Id = IDENT_CURRENT('Invoice_Collection_Activity');


                    INSERT INTO dbo.Invoice_Collection_Activity_Log
                         (
                             Invoice_Collection_Activity_Id
                             , Next_Action_Dt
                             , Created_User_Id
                         )
                    SELECT  @Invoice_Collection_Activity_Id, @Next_Action_Date, @User_Info_Id;




                    SELECT
                        @Invoice_Collection_Activity_Log_Id = IDENT_CURRENT('Invoice_Collection_Activity_Log');

                    INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
                         (
                             Invoice_Collection_Activity_Log_Id
                             , Invoice_Collection_Queue_Id
                             , Collection_Start_Dt
                             , Collection_End_Dt
                         )
                    SELECT
                        @Invoice_Collection_Activity_Log_Id
                        , ticq.Invoice_Collection_Queue_Id
                        , icq.Collection_Start_Dt
                        , icq.Collection_End_Dt
                    FROM
                        @tvp_Invoice_Collection_Queue ticq
                        INNER JOIN dbo.Invoice_Collection_Queue icq
                            ON icq.Invoice_Collection_Queue_Id = ticq.Invoice_Collection_Queue_Id;

                    UPDATE
                        icq
                    SET
                        icq.Download_Attempt_Cnt = icq.Download_Attempt_Cnt + 1
                        , icq.Last_Change_Ts = GETDATE()
                        , icq.Is_Locked = 1
                    FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN @tvp_Invoice_Collection_Queue ticq
                            ON ticq.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                    WHERE
                        EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.Code
                                   WHERE
                                        Code_Id = @IC_Activity_Type_Cd
                                        AND Code_Value = 'Attempt Download');

                    UPDATE
                        icq
                    SET
                        icq.Next_Action_Dt = @Next_Action_Date
                        , icq.Last_Change_Ts = GETDATE()
                    FROM
                        dbo.Invoice_Collection_Queue icq
                        INNER JOIN @tvp_Invoice_Collection_Queue ticq
                            ON ticq.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
                    WHERE
							@Next_Action_Date IS NOT NULL;


                    COMMIT TRAN;
                END TRY
                BEGIN CATCH

                    IF @@TRANCOUNT > 0
                        BEGIN
                            ROLLBACK TRAN;
                        END;

                    EXEC dbo.usp_RethrowError;

                END CATCH;

            END;
    








GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Activity_Log_Ins] TO [CBMSApplication]
GO
