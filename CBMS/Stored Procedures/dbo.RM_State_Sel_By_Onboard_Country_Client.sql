SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.RM_State_Sel_By_Onboard_Country_Client
   
 DESCRIPTION:     
   
	This procedure is to get client onboarded details
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id			INT
    @Commodity_Id		INT				NULL
    @Country_Id			INT				NULL
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_State_Sel_By_Onboard_Country_Client 235
	EXEC RM_State_Sel_By_Onboard_Country_Client 1005,291
	   
	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2018-09-04	Created For Global Risk Managemnet		


******/

CREATE PROCEDURE [dbo].[RM_State_Sel_By_Onboard_Country_Client]
    (
        @Client_Id INT
        , @Commodity_Id INT
        , @Country_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Client_Client_Hier_Id INT;

        SELECT
            @Client_Client_Hier_Id = Client_Hier_Id
        FROM
            Core.Client_Hier
        WHERE
            Client_Id = @Client_Id
            AND Sitegroup_Id = 0;

        SELECT
            st.STATE_ID
            , st.STATE_NAME
        FROM
            Trade.RM_Client_Hier_Onboard rco
            INNER JOIN dbo.COUNTRY con
                ON rco.COUNTRY_ID = con.COUNTRY_ID
            INNER JOIN dbo.STATE st
                ON rco.COUNTRY_ID = st.COUNTRY_ID
        WHERE
            rco.Client_Hier_Id = @Client_Client_Hier_Id
            AND rco.Commodity_Id = @Commodity_Id
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') con
                                   WHERE
                                        rco.Country_Id = con.Segments))
        ORDER BY
            con.COUNTRY_NAME
            , st.STATE_NAME;

    END;



GO
GRANT EXECUTE ON  [dbo].[RM_State_Sel_By_Onboard_Country_Client] TO [CBMSApplication]
GO
