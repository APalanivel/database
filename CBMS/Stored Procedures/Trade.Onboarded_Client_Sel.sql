SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                
NAME:                                
    Trade.Onboarded_Client_Sel                                
                                
DESCRIPTION:                                
   
                                
INPUT PARAMETERS:                                
 Name						DataType		Default Description                                
---------------------------------------------------------------
 @Keyword					VARCHAR(200)	NULL
 @StartIndex				INT				1
 @EndIndex					INT				2147483647
                          
                       
OUTPUT PARAMETERS:                                
 Name   DataType  Default Description                                
---------------------------------------------------------------    

	EXEC Trade.Onboarded_Client_Sel 
	EXEC Trade.Onboarded_Client_Sel 'dar'
	               
 
USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy
                   
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	NR			2018-07-25  Created For Risk Managemnet.

******/
CREATE PROCEDURE [Trade].[Onboarded_Client_Sel]
      ( 
       @Keyword VARCHAR(200) = NULL
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Search_Keyword VARCHAR(200) = NULL
      
      DECLARE @Total_Cnt AS INT
      
      DECLARE @Tbl_Clients AS TABLE
            ( 
             Client_Id INT
            ,Client_Name VARCHAR(200)
            ,Row_Num INT )

      SELECT
            @Search_Keyword = '%' + @Keyword + '%'

      INSERT      INTO @Tbl_Clients
                  ( 
                   Client_Id
                  ,Client_Name
                  ,Row_Num )
                  SELECT
                        clch.Client_Id
                       ,clch.Client_Name
                       ,ROW_NUMBER() OVER ( ORDER BY clch.Client_Name )
                  FROM
                        Trade.RM_Client_Hier_Onboard clntob
                        INNER JOIN Core.Client_Hier clch
                              ON clntob.Client_Hier_Id = clch.Client_Hier_Id
                  WHERE
                        clch.Sitegroup_Id = 0
                        AND ( @Keyword IS NULL
                              OR clch.Client_Name LIKE @Search_Keyword )
                  GROUP BY
                        clch.Client_Id
                       ,clch.Client_Name
           
      SELECT
            @Total_Cnt = MAX(Row_Num)
      FROM
            @Tbl_Clients            
                        
      SELECT
            Client_Id
           ,Client_Name
           ,Row_Num
           ,@Total_Cnt AS Total_Cnt
      FROM
            @Tbl_Clients
      WHERE
            Row_Num BETWEEN @StartIndex AND @EndIndex

END;
GO
GRANT EXECUTE ON  [Trade].[Onboarded_Client_Sel] TO [CBMSApplication]
GO
