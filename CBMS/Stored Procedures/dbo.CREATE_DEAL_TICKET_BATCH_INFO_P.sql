SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CREATE_DEAL_TICKET_BATCH_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@dealTicketId  	int       	          	
	@batchName     	varchar(200)	          	
	@batchType     	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE   PROCEDURE DBO.CREATE_DEAL_TICKET_BATCH_INFO_P 

@dealTicketId int, 
@batchName varchar(200), 
@batchType int

AS
begin
	set nocount on
	dECLARE @batchProcessTypeId int

	SELECT @batchProcessTypeId = ENTITY_ID FROM ENTITY 
	WHERE ENTITY_NAME = @batchName AND ENTITY_TYPE = @batchType
	

	INSERT INTO RM_DEAL_TICKET_BATCH_MAP 
		(RM_DEAL_TICKET_ID, BATCH_PROCESS_TYPE_ID, BATCH_GENERATION_DATE)
	VALUES	(@dealTicketId, @batchProcessTypeId, getDate())

end
GO
GRANT EXECUTE ON  [dbo].[CREATE_DEAL_TICKET_BATCH_INFO_P] TO [CBMSApplication]
GO
