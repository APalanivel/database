SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.Budget_Header_Changes_Sel_By_Audit_Ts


DESCRIPTION:

	Gets all changes made on Budget table between the given Audit_Start_Ts and the Audit_End_Ts

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Audit_Start_Ts		DateTime		Minimum Audit Start Time stamp
	@Audit_End_Ts		DateTime        Maximum Audit End Time stamp

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
	
	DECLARE @Cur_Ts DateTime = GETDATE()

	EXEC dbo.Budget_Header_Changes_Sel_By_Audit_Ts '6/1/2010', @Cur_Ts

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	PNR			06/10/2010	Created       
	 	
******/
CREATE PROCEDURE dbo.Budget_Header_Changes_Sel_By_Audit_Ts
(    
	 @Audit_Start_Ts  DATETIME
	,@Audit_End_Ts	  DATETIME

)    
AS     
BEGIN     
    SET NOCOUNT ON

	SELECT
		BdgAdt.BUDGET_ID
		,B.CLIENT_ID
		,B.BUDGET_NAME
		,CONVERT(DATETIME, CONVERT(CHAR(4), B.BUDGET_START_YEAR) + '-01-01') AS BUDGET_START_YEAR
		,ISNULL(B.IS_POSTED_TO_DV, 0) IS_POSTED_TO_DV
		,com.Default_UOM_Entity_Type_Id AS BASE_UOM_ID
		,BdgAdt.Audit_Function
	FROM
		dbo.Budget_Audit BdgAdt
		JOIN 
			(
			SELECT
				 Budget_Id
				,MAX(Audit_Ts) Max_Audit_Ts
			FROM
				dbo.Budget_Audit
			WHERE
				Audit_Ts BETWEEN @Audit_Start_Ts
								AND @Audit_End_Ts
			GROUP BY
				Budget_Id
			) maxadt
			ON maxadt.Budget_id = BdgAdt.Budget_id
				AND maxadt.Max_Audit_Ts = BdgAdt.Audit_Ts

		LEFT JOIN 
			(dbo.Budget b
				JOIN dbo.Commodity com
					ON b.COMMODITY_TYPE_ID = com.Commodity_Id
			)
			ON b.BUDGET_ID = BdgAdt.Budget_Id

END
GO
GRANT EXECUTE ON  [dbo].[Budget_Header_Changes_Sel_By_Audit_Ts] TO [CBMSApplication]
GO
