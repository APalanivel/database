
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
    
NAME: [dbo].[Site_Sel_Changes_For_IDM]    
         
DESCRIPTION:     
 procedure to select site changes for IDM    
          
INPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
------------------------------------------------------------              
@ClientId   INT    
                    
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION       
           
------------------------------------------------------------              
USAGE EXAMPLES:              
------------------------------------------------------------            
    
    exec [dbo].[Site_Sel_Changes_For_IDM]    
 exec [dbo].[Site_Sel_Changes_For_IDM] 11278    
 
Site_Sel_Changes_For_IDM  1609303632
    
AUTHOR INITIALS:              
INITIALS NAME              
------------------------------------------------------------    
 KVK  Vinay K    
 AC         Ajay chejarla   
 SS			Sani
MODIFICATIONS    
    
INITIALS DATE  MODIFICATION    
------------------------------------------------------------              
 KVK  12/14/2014 created    
 AC         2015-11-13      Added weather station code to select list     
 SS         2017-03-16      Added Time zone to the select list   
*/    
    
CREATE PROCEDURE [dbo].[Site_Sel_Changes_For_IDM]    
      (     
       @Current_CT_Version_Id BIGINT    
      ,@ClientId AS INT = NULL )    
AS     
BEGIN    
    
      SET NOCOUNT ON;    
    
      DECLARE @Last_CT_Version_Id BIGINT               
    
      SELECT    
            @Last_CT_Version_Id = ac.App_Config_Value    
      FROM    
            dbo.App_Config AS ac    
      WHERE    
            ac.App_Config_Cd = 'SITE_CT_Version'    
    
    
    
      SELECT    
            s.Client_ID AS [Client_Id]    
           ,ch.Client_Hier_Id    
           ,s.SITE_NAME AS [Site_Name]    
           ,s.Is_IDM_Enabled    
           ,s.IDM_Node_XId IDM_Node_Id    
           ,ch.Weather_Station_Code  
		   ,ch.Time_Zone
      FROM    
            changetable(CHANGES dbo.SITE, @Last_CT_Version_Id) chg    
            JOIN dbo.SITE AS s    
                  ON s.SITE_ID = chg.SITE_ID    
            JOIN core.Client_Hier AS ch    
                  ON ch.Site_Id = s.SITE_ID    
      WHERE    
            chg.sys_change_version <= @Current_CT_Version_Id    
            AND SYS_CHANGE_OPERATION = 'U'    
            AND s.Is_IDM_Enabled = 1    
            AND s.IDM_Node_XId > 0    
            AND ( s.Client_ID = @ClientId    
                  OR @ClientId IS NULL )    
    
END 

;
GO


GRANT EXECUTE ON  [dbo].[Site_Sel_Changes_For_IDM] TO [CBMSApplication]
GO
