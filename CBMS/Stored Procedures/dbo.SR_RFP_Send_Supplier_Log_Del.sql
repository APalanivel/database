SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[SR_RFP_Send_Supplier_Log_Del]  
     
DESCRIPTION: 
	It Deletes SR RFP Send Supplier Log for Selected 
						SR RFP Send Supplier Log Id.
      
INPUT PARAMETERS:          
NAME							DATATYPE	DEFAULT		DESCRIPTION          
-------------------------------------------------------------------          
@SR_RFP_Send_Supplier_Log_Id	INT	

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  
	Begin Tran
		EXEC SR_RFP_Send_Supplier_Log_Del  9103
	Rollback Tran
    
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR		    28-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.SR_RFP_Send_Supplier_Log_Del
   (
    @SR_RFP_Send_Supplier_Log_Id INT
   )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
   	FROM
		dbo.SR_RFP_SEND_SUPPLIER_LOG
	WHERE
		SR_RFP_SEND_SUPPLIER_LOG_ID = @SR_RFP_Send_Supplier_Log_Id

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_Send_Supplier_Log_Del] TO [CBMSApplication]
GO
