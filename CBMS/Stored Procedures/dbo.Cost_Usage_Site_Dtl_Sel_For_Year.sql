
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	 dbo.Cost_Usage_Site_Dtl_Sel_For_Year
	 
DESCRIPTION:  
	Used to select site level cost usage data for the given year 
	 
INPUT PARAMETERS:  
	 Name				DataType	Default Description  
------------------------------------------------------------  
	 @Client_Hier_Id	INT  
	 @report_year		INT
	 @Commodity_id		INT
	 @Currency_unit_id  INT
	 @UOM_Type_Id		INT
	 @Country_Id		int			null  
	 @Site_Not_Managed INT			-1       


 OUTPUT PARAMETERS:  
 Name					DataType	Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------ 
 
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 43502,2009,67,3,1408,4
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 43506,2009,67,3,1408,NULL
	
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 256267,2012,290,3,12,NULL
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 5534,2010,290,3,12,NULL
	
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 256267,2012,290,3,12,NULL,1
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 5534,2010,290,3,12,NULL,0
	
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 256267,2012,290,3,12,NULL,0
	EXEC dbo.Cost_Usage_Site_Dtl_Sel_For_Year 5534,2010,290,3,12,NULL,1

	

 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SSR		Sharad Srivastava
 HG			Harihara Suthan G
 BCH		Balaraju CH
 AP			Athmaram Pabbathi
 RR			Raghu Reddy
 
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 SSR        02/18/2010	Created
 HG			03/12/2010	Converted_Uom_name column added in the select clause
 HG			03/24/2010	Is_Complete, Is_Published and is_Under_Review columns added in select clause.
 HG			04/01/2010	Script modified to fix the issue with the fiscal year calculation.
						If fiscal start month is January then fiscal start date is 01-01-<Report Year> and End Date is 12-31-<Report Year>
							otherwise previous will be considered for the fiscal start and end date calculation.
							
						Changed the condition in Cte_fy 
									dd.Year_Num = @Report_Year	to
																	(CASE
																			WHEN dd.Month_Num = 1 THEN @report_year
																			ELSE @Report_Year - 1
																	   END
																	  )
 HG			04/07/2010	Utility_Cost and marketer_Cost column which used to return 0 always removed from final select statement and required logic moved to application.
 BCH		10/10/2011  Added data_source_code column in the select list and used code table with left outer join
 AP			2011-12-22  Removed & modified logic for CTE_fy and replaced with @Service_Month
 AP			2012-03-13  Following changes made during Addl Data 
						  --@Site_Id Parameter is replaced with @Client_Hier_Id
						  --Removed @Client_Id parameter since it no longer used
						  --Removed @Service_Month TVP and used meta.Date_Dim & modified the logic accordingly
						  --Utility_Cost and marketer_Cost column which used to return 0 always removed from final select statement and required logic moved to application.
BCH			2012-08-22	(MAINT 1496 ). Added @Country_Id parameter as optional.
RR			2013-02-26	Maint-1378/MAINT-1751(sub task for db) Added new input parameter @Site_Not_Managed

******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Dtl_Sel_For_Year
      ( 
       @Client_Hier_Id INT
      ,@report_year INT
      ,@Commodity_id INT
      ,@Currency_unit_id INT
      ,@UOM_Type_Id INT
      ,@Country_Id INT = NULL
      ,@Site_Not_Managed INT = -1 )
AS 
BEGIN
      SET NOCOUNT ON
      DECLARE
            @Currency_Group_Id INT
           ,@Begin_Dt DATE
           ,@End_Dt DATE
           ,@Site_Id INT

      DECLARE @Cost_Usage_Bucket_Id TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(25) )

--Inserting Cost & Usage Buckets to table variable
      INSERT      INTO @Cost_Usage_Bucket_Id
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type )
                  EXEC dbo.Cost_usage_Bucket_Sel_By_Commodity 
                        @Commodity_id = @Commodity_Id
            
      SELECT
            @Currency_Group_id = ch.Client_Currency_Group_Id
           ,@Site_Id = ch.Site_Id
           ,@Begin_Dt = dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))
           ,@End_Dt = dateadd(MONTH, -1, ( dateadd(YEAR, 1, dateadd(m, ch.Client_Fiscal_Offset, cast('1/1/' + cast(@Report_Year AS VARCHAR(4)) AS DATE))) ))
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id
            AND ( @Site_Not_Managed = -1
                  OR ch.Site_Not_Managed = @Site_Not_Managed )
            AND ( @Country_Id IS NULL
                  OR ch.Country_Id = @Country_Id ) ;    
 
    
      WITH  Cte_SiteResult
              AS ( SELECT
                        cusd.Client_Hier_Id
                       ,cusd.Service_Month
                       ,dts_code.Code_Value data_source_code
                       ,sum(case WHEN bkt.Bucket_Type = 'Charge' THEN cusd.Bucket_Value * cc.CONVERSION_FACTOR
                                 ELSE 0
                            END) AS Total_Cost
                       ,sum(case WHEN bkt.Bucket_Type = 'Determinant' THEN cusd.Bucket_Value * uc.CONVERSION_FACTOR
                                 ELSE 0
                            END) AS Volume
                   FROM
                        dbo.Cost_Usage_Site_Dtl cusd
                        JOIN @Cost_Usage_Bucket_Id bkt
                              ON bkt.Bucket_Master_Id = cusd.Bucket_Master_Id
                        LEFT OUTER JOIN dbo.Code dts_code
                              ON dts_code.Code_Id = cusd.Data_Source_Cd
                        LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION uc
                              ON uc.BASE_UNIT_ID = cusd.UOM_Type_Id
                                 AND uc.CONVERTED_UNIT_ID = @UOM_Type_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cc
                              ON cc.currency_group_id = @Currency_Group_id
                                 AND cc.base_unit_id = cusd.CURRENCY_UNIT_ID
                                 AND cc.converted_unit_id = @Currency_unit_id
                                 AND cc.conversion_date = cusd.Service_month
                   WHERE
                        cusd.Client_Hier_Id = @Client_Hier_Id
                        AND cusd.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                        AND @Site_Id IS NOT NULL
                   GROUP BY
                        cusd.Client_Hier_Id
                       ,cusd.Service_Month
                       ,dts_code.Code_Value)
            SELECT
                  @Client_Hier_Id AS Client_Hier_Id
                 ,fy.Date_D AS Service_Month
                 ,x.Volume Volume
                 ,x.Total_Cost Total_Cost
                 ,x.Total_Cost / nullif(x.Volume, 0) Unit_cost
                 ,un.ENTITY_NAME AS Converted_Uom_Name
                 ,isnull(max(case WHEN ip.Is_Expected = 1
                                       AND ip.Is_Received = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Complete
                 ,isnull(max(convert(INT, ip.Is_Received)), 0) AS Is_published
                 ,isnull(max(case WHEN ip.Recalc_Under_Review = 1
                                       OR ip.Variance_Under_Review = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Under_Review
                 ,x.Data_Source_Code
            FROM
                  meta.Date_Dim fy
                  LEFT OUTER JOIN Cte_SiteResult x
                        ON x.service_month = fy.Date_D
                  LEFT OUTER JOIN dbo.INVOICE_PARTICIPATION ip
                        ON ip.Site_id = @Site_Id
                           AND ip.SERVICE_MONTH = x.Service_Month
                  JOIN dbo.ENTITY un
                        ON un.ENTITY_ID = @UOM_Type_Id
            WHERE
                  fy.Date_D BETWEEN @Begin_Dt AND @End_Dt
                  AND @Site_Id IS NOT NULL
            GROUP BY
                  fy.Date_D
                 ,un.ENTITY_NAME
                 ,x.Volume
                 ,x.total_cost
                 ,x.data_source_code
            ORDER BY
                  fy.Date_D    
    
END ;

;
GO




GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Sel_For_Year] TO [CBMSApplication]
GO
