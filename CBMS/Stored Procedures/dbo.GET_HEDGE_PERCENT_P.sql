SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_HEDGE_PERCENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@clientId      	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.GET_HEDGE_PERCENT_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int

AS
set nocount on
	SELECT max_hedge_percent from RM_ONBOARD_CLIENT
	WHERE CLIENT_ID=@clientId;
GO
GRANT EXECUTE ON  [dbo].[GET_HEDGE_PERCENT_P] TO [CBMSApplication]
GO
