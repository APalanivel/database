SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Invoice_Collection_Exception_Type_User_Info_Map_Upd

DESCRIPTION:

INPUT PARAMETERS:
	Name							   DataType		Default					Description
---------------------------------------------------------------------------------------------
 @Invoice_Collection_Exception_Type_Cd	INT
 @Assigned_User_Info_Id					INT
 @User_Info_Id INT

OUTPUT PARAMETERS:
	Name							DataType		Default					Description
---------------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
---------------------------------------------------------------------------------------------

EXEC dbo.CODE_SEL_BY_CodeSet_Name
    @CodeSet_Name = 'IC Exception Type'  -- varchar(25)

--	select * from user_info where username='jjackson'


EXEC dbo.Invoice_Collection_Exception_Type_User_Info_Map_Upd
    @Invoice_Collection_Exception_Type_Cd = 1
    , @Assigned_User_Info_Id = 49
    , @User_Info_Id = 2875



AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------------
	SP          Srinivas Patchava
	
MODIFICATIONS
	Initials	Date			Modification
---------------------------------------------------------------------------------------------
	SP       	2019-07-05		Created for SE2017-729

******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Exception_Type_User_Info_Map_Upd]
    (
        @Invoice_Collection_Exception_Type_Cd INT
        , @Assigned_User_Info_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        UPDATE
            uimap
        SET
            uimap.User_Info_Id = @Assigned_User_Info_Id
            , uimap.Updated_User_Id = @User_Info_Id
            , uimap.Last_Change_Ts = GETDATE()
        FROM
            Invoice_Collection_Exception_Type_User_Info_Map uimap
        WHERE
            uimap.Invoice_Collection_Exception_Type_Cd = @Invoice_Collection_Exception_Type_Cd;



    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Exception_Type_User_Info_Map_Upd] TO [CBMSApplication]
GO
