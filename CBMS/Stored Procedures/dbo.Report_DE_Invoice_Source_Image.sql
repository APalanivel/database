SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******      
                         
 NAME: dbo.Report_DE_Invoice_Source_Image                     

 DESCRIPTION:      
		  To get the invocie details.                       

 INPUT PARAMETERS:      

 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    
 @DE_Recipient						VARCHAR(25)						descripes the recipient name CTE/ Prokarma
 @Report_Days						SMALLINT						Descripes how many days of images has to listed in the report

 OUTPUT PARAMETERS:      

 Name                               DataType          Default       Description      
---------------------------------------------------------------------------------------------------------------    

 USAGE EXAMPLES:                              
--------------------------------------------------------------------------------------------------------------- 

	BEGIN TRAN
	SELECT * FROM REPTMGR.dbo.Client_Report_Config
    EXEC Report_DE_Invoice_Source_Image
      @Client_Name = 'Philips'
	SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      
    
	BEGIN TRAN
	SELECT * FROM REPTMGR.dbo.Client_Report_Config
	EXEC Report_DE_Invoice_Source_Image  @Client_Name = 'Philips','2014-03-01','2014-09-01'
	SELECT * FROM REPTMGR.dbo.Client_Report_Config 
	ROLLBACK TRAN      
	  
	  
	  SELECT * FROM Client WHERE Client_id = 12344
                         
 AUTHOR INITIALS:    
       
 Initials                   Name      
---------------------------------------------------------------------------------------------------------------
 HG							Harihara Suthan

 MODIFICATIONS:

 Initials               Date            Modification
--------------------------------------------------------------------------------------------------------------
 HG                     2016-01-08      Created for calling this sproc from some other .net tool to export
										the output in to FTP/SFTP location.
******/
CREATE PROCEDURE [dbo].[Report_DE_Invoice_Source_Image]
      (
       @DE_Recipient VARCHAR(25) = 'CTE'
      ,@Report_Days SMALLINT = 1 )
AS
BEGIN

      SET NOCOUNT ON;

      SET @Report_Days = @Report_Days * -1;

      SELECT
            isi.INV_SOURCED_IMAGE_ID AS Inv_Sourced_Image_Id
           ,isi.CBMS_FILENAME AS Invoice_FileName
           ,Recipient_Transfer_Ts AS Transfer_Ts
      FROM
            dbo.INV_SOURCED_IMAGE isi
            INNER JOIN dbo.Code rc
                  ON rc.Code_Id = isi.DE_Recipient_Cd
            INNER JOIN dbo.INV_SOURCED_IMAGE_BATCH b
                  ON b.INV_SOURCED_IMAGE_BATCH_ID = isi.INV_SOURCED_IMAGE_BATCH_ID
      WHERE
            CAST(b.BATCH_DATE AS DATE) = CAST(DATEADD(dd, @Report_Days, GETDATE()) AS DATE)
            AND ( @DE_Recipient IS NULL
                  OR rc.Code_Value = @DE_Recipient );

END;
GRANT EXECUTE ON dbo.Report_DE_Invoice_Source_Image TO CBMSApplication;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Invoice_Source_Image] TO [CBMSReports]
GO
