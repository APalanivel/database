SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_CREATE_ARCHIVE_SUPPLIER_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@RFP_ID int,
@sr_rfp_account_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- check the archive 
--exec [DBO].[SR_RFP_CREATE_ARCHIVE_SUPPLIER_P] 
--@RFP_ID =1937,
--@sr_rfp_account_id = 21221

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [DBO].[SR_RFP_CREATE_ARCHIVE_SUPPLIER_P] 

@RFP_ID int,
@sr_rfp_account_id int

AS
	
set nocount on


declare @sr_rfp_archive_supplier_id int, @sr_rfp_supplier_contact_vendor_map_id int, @supplier_name varchar(200), @contact_name varchar(100), @is_no_bid bit

DECLARE C_ARC_SUPP CURSOR FAST_FORWARD
FOR select  vendor_map.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,      -- SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
	    v.VENDOR_NAME,                               	   -- SUPPLIER_NAME,
	    user_info.USERNAME,  			           -- CONTACT_NAME	   
	    CASE rfp_account.sr_rfp_bid_group_id 	    
	    WHEN 1
	    THEN 0
	    ELSE 1
	    END sr_rfp_bid_group_id  
FROM     
	SR_SUPPLIER_CONTACT_INFO 		contact_info, 
	SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP 	vendor_map,
	sr_rfp_account     			rfp_account,
	VENDOR 					v 
	left join  SR_RFP_CLOSURE closure on closure.VENDOR_ID = v.VENDOR_ID,
	USER_INFO 				user_info

WHERE
     rfp_account.SR_RFP_ID = @RFP_ID	
     and closure.sr_account_group_id = rfp_account.sr_rfp_account_id	
     AND rfp_account.sr_rfp_account_id = @sr_rfp_account_id
     and rfp_account.is_deleted = 0
     AND vendor_map.SR_SUPPLIER_CONTACT_INFO_ID = contact_info.SR_SUPPLIER_CONTACT_INFO_ID     
     AND contact_info.USER_INFO_ID = user_info.USER_INFO_ID
     and vendor_map.SR_RFP_ID = rfp_account.SR_RFP_ID
    
OPEN C_ARC_SUPP

FETCH NEXT FROM C_ARC_SUPP INTO @sr_rfp_supplier_contact_vendor_map_id, @supplier_name, @contact_name, @is_no_bid
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN

		delete from SR_RFP_ARCHIVE_SUPPLIER where sr_rfp_supplier_contact_vendor_map_id = @sr_rfp_supplier_contact_vendor_map_id
		and supplier_name = @supplier_name and contact_name= @contact_name and is_no_bid= @is_no_bid

		insert into  SR_RFP_ARCHIVE_SUPPLIER ( SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID,SUPPLIER_NAME,CONTACT_NAME, IS_NO_BID )
		values 	( @sr_rfp_supplier_contact_vendor_map_id, @supplier_name, @contact_name, @is_no_bid )
	
		select @sr_rfp_archive_supplier_id = SCOPE_IDENTITY()		
	
	END -- IF
		FETCH NEXT FROM C_ARC_SUPP INTO @sr_rfp_supplier_contact_vendor_map_id, @supplier_name, @contact_name, @is_no_bid
END 
CLOSE C_ARC_SUPP
DEALLOCATE C_ARC_SUPP
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_CREATE_ARCHIVE_SUPPLIER_P] TO [CBMSApplication]
GO
