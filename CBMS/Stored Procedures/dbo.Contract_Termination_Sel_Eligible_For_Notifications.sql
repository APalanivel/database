
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.Contract_Termination_Sel_Eligible_For_Notifications    
 
DESCRIPTION:  

INPUT PARAMETERS:    
Name					DataType	Default			Description    
----------------------------------------------------------------    
@batch_execution_date	datetime                  
@Commodity_Id			INT					

OUTPUT PARAMETERS:    
Name					DataType	Default			Description    
----------------------------------------------------------------    
 
USAGE EXAMPLES:    
----------------------------------------------------------------    
SELECT DISTINCT TOP 10 cha.Supplier_Contract_ID,c.NOTIFICATION_DAYS,c.Advance_Termination_Notofication_Days,cha.Supplier_Meter_Disassociation_Date
,c.CONTRACT_END_DATE
,DATEADD(DAY, -( ISNULL(c.NOTIFICATION_DAYS, 0) + ISNULL(c.Advance_Termination_Notofication_Days, 0) ), cha.Supplier_Meter_Disassociation_Date) AS Notice_Date
FROM
      core.Client_Hier_Account cha
      JOIN dbo.CONTRACT c
            ON cha.Supplier_Contract_ID = c.CONTRACT_ID
WHERE
      cha.Supplier_Meter_Disassociation_Date > CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      AND c.CONTRACT_END_DATE > CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      AND DATEADD(DAY, -( ISNULL(c.NOTIFICATION_DAYS, 0) + ISNULL(c.Advance_Termination_Notofication_Days, 0) ), cha.Supplier_Meter_Disassociation_Date)> CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))
      AND c.Is_Notification_Required_On_Termination = 1
      ORDER BY DATEADD(DAY, -( ISNULL(c.NOTIFICATION_DAYS, 0) + ISNULL(c.Advance_Termination_Notofication_Days, 0) ), cha.Supplier_Meter_Disassociation_Date)


EXEC dbo.Contract_Termination_Sel_Eligible_For_Notifications '2016-06-27' 
EXEC dbo.Contract_Termination_Sel_Eligible_For_Notifications '2016-06-30' 
EXEC dbo.Contract_Termination_Sel_Eligible_For_Notifications '2016-07-01' 
EXEC dbo.Contract_Termination_Sel_Eligible_For_Notifications '2016-07-15' 

AUTHOR INITIALS:    
Initials	Name    
----------------------------------------------------------------    
RR			Raghu Reddy

MODIFICATIONS:
Initials	Date		Modification    
----------------------------------------------------------------    
RR			2016-05-26	Global Sourcing - Phase5 - GCS-985 Created	
RR			2016-07-13	GCS - 5b - Added locale code					
RR			2016-07-22	GCS-1179 - Modified to get supplier contact mapped to commodity, state and country  of the expiring contract 

******/
CREATE PROCEDURE [dbo].[Contract_Termination_Sel_Eligible_For_Notifications]
      ( 
       @Execution_Date DATETIME = NULL )
AS 
BEGIN    
    
      SET NOCOUNT ON;    
        
      DECLARE
            @Default_Analyst INT
           ,@Custom_Analyst INT
           ,@Termination INT;  
        
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'; 
            
      SELECT
            @Termination = cd.Code_Id
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Flow Verification'
            AND cd.Code_Value = 'Termination';       
      
      
      SELECT
            @Execution_Date = ISNULL(@Execution_Date, CONVERT(DATETIME, CONVERT(VARCHAR(10), GETDATE(), 101))); 

      DECLARE @Client_Con_Info TABLE
            ( 
             Client_Name VARCHAR(200)
            ,Client_Id INT
            ,Site_Name VARCHAR(800)
            ,Site_Id INT
            ,Account_Number VARCHAR(50)
            ,Account_Id INT
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Contract_Id INT
            ,Ed_Contract_Number VARCHAR(150)
            ,Vendor_Id INT
            ,Vendor_Name VARCHAR(200)
            ,Expiry_Date DATETIME
            ,Analyst_Id INT
            ,Analyst_Name VARCHAR(200)
            ,Analyst_Email_Address VARCHAR(150)
            ,Commodity_Type_id INT
            ,Analyst_Mapping_Cd INT
            ,Phone_Number VARCHAR(50)
            ,Due_Date_Crossed INT
            ,Commodity VARCHAR(50)
            ,Country VARCHAR(200) );         
                  
---> Eligible for todays notification
      INSERT      INTO @Client_Con_Info
                  ( 
                   Client_Name
                  ,Client_Id
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,Vendor_Id
                  ,Vendor_Name
                  ,Expiry_Date
                  ,Analyst_Id
                  ,Analyst_Name
                  ,Analyst_Email_Address
                  ,Commodity_Type_id
                  ,Analyst_Mapping_Cd
                  ,Phone_Number
                  ,Due_Date_Crossed
                  ,Commodity
                  ,Country )
                  SELECT
                        ch.Client_Name
                       ,ch.Client_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id vendor_id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date AS Expiry_Date
                       ,vcam.Analyst_Id AS Analyst_ID
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
                       ,ui.EMAIL_ADDRESS AS Analyst_Email_Address
                       ,chautil.Commodity_Id AS COMMODITY_TYPE_ID
                       ,COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) AS Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,0 AS Due_Date_Crossed
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON chasupp.Supplier_Contract_ID = con.CONTRACT_ID
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.VENDOR_ID = chautil.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = chautil.Commodity_Id
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        LEFT JOIN dbo.USER_INFO AS ui
                              ON ui.USER_INFO_ID = vcam.Analyst_Id
                        INNER JOIN dbo.Commodity com
                              ON con.COMMODITY_TYPE_ID = com.Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        --AND chasupp.Supplier_Meter_Disassociation_Date > @Execution_Date
                        --AND con.CONTRACT_END_DATE > @Execution_Date
                        AND DATEADD(DAY, -( ISNULL(con.NOTIFICATION_DAYS, 0) + ISNULL(con.Advance_Termination_Notofication_Days, 0) ), chasupp.Supplier_Meter_Disassociation_Date) = @Execution_Date
                        AND con.Is_Notification_Required_On_Termination = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Contract_Notification_Log fvl
                                          INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                                                ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                                          INNER JOIN dbo.Notification_Msg_Queue nmq
                                                ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                                          INNER JOIN dbo.Notification_Template nt
                                                ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                                          INNER JOIN dbo.Code cd
                                                ON nt.Notification_Type_Cd = cd.Code_Id
                                          INNER JOIN dbo.Codeset cs
                                                ON cd.Codeset_Id = cs.Codeset_Id
                                         WHERE
                                          cs.Codeset_Name = 'Notification Type'
                                          AND cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' )
                                          AND chasupp.Supplier_Contract_ID = fvl.Contract_Id
                                          AND chasupp.Meter_Id = fvmd.Meter_Id
                                          AND chasupp.Supplier_Meter_Disassociation_Date = fvl.Meter_Term_Dt
                                          AND CAST(ISNULL(con.NOTIFICATION_DAYS, 0) + ISNULL(con.Advance_Termination_Notofication_Days, 0) AS INT) = fvl.Advance_Notofication_Days )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Client_Id
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date
                       ,vcam.Analyst_Id
                       ,ui.FIRST_NAME
                       ,ui.LAST_NAME
                       ,ui.EMAIL_ADDRESS
                       ,chautil.Commodity_Id
                       ,chautil.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name;
 
 ---> Eligible but due date crossed, this may happens when contract/meter dates changed                      
      INSERT      INTO @Client_Con_Info
                  ( 
                   Client_Name
                  ,Client_Id
                  ,Site_Name
                  ,Site_Id
                  ,Account_Number
                  ,Account_Id
                  ,Meter_Id
                  ,Meter_Number
                  ,Contract_Id
                  ,Ed_Contract_Number
                  ,Vendor_Id
                  ,Vendor_Name
                  ,Expiry_Date
                  ,Analyst_Id
                  ,Analyst_Name
                  ,Analyst_Email_Address
                  ,Commodity_Type_id
                  ,Analyst_Mapping_Cd
                  ,Phone_Number
                  ,Due_Date_Crossed
                  ,Commodity
                  ,Country )
                  SELECT
                        ch.Client_Name
                       ,ch.Client_Id
                       ,RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' Site_Name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id vendor_id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date AS Expiry_Date
                       ,vcam.Analyst_Id AS Analyst_ID
                       ,ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS Analyst_Name
                       ,ui.EMAIL_ADDRESS AS Analyst_Email_Address
                       ,chautil.Commodity_Id AS COMMODITY_TYPE_ID
                       ,COALESCE(chautil.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) AS Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,1 AS Due_Date_Crossed
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier_Account chautil
                              ON ch.Client_Hier_Id = chautil.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account chasupp
                              ON chautil.Meter_Id = chasupp.Meter_Id
                        INNER JOIN dbo.CONTRACT con
                              ON chasupp.Supplier_Contract_ID = con.CONTRACT_ID
                        LEFT JOIN dbo.VENDOR_COMMODITY_MAP vcm
                              ON vcm.VENDOR_ID = chautil.Account_Vendor_Id
                                 AND vcm.COMMODITY_TYPE_ID = chautil.Commodity_Id
                        LEFT JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id
                        LEFT JOIN dbo.USER_INFO AS ui
                              ON ui.USER_INFO_ID = vcam.Analyst_Id
                        INNER JOIN dbo.Commodity com
                              ON con.COMMODITY_TYPE_ID = com.Commodity_Id
                  WHERE
                        chautil.Account_Type = 'Utility'
                        AND chasupp.Account_Type = 'Supplier'
                        --AND chasupp.Supplier_Meter_Disassociation_Date > @Execution_Date
                        --AND con.CONTRACT_END_DATE > @Execution_Date
                        AND DATEADD(DAY, -( ISNULL(con.NOTIFICATION_DAYS, 0) + ISNULL(con.Advance_Termination_Notofication_Days, 0) ), chasupp.Supplier_Meter_Disassociation_Date) < @Execution_Date
                        AND con.Is_Notification_Required_On_Termination = 1
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Contract_Notification_Log fvl
                                          INNER JOIN dbo.Contract_Notification_Log_Meter_Dtl fvmd
                                                ON fvl.Contract_Notification_Log_Id = fvmd.Contract_Notification_Log_Id
                                          INNER JOIN dbo.Notification_Msg_Queue nmq
                                                ON fvl.Notification_Msg_Queue_Id = nmq.Notification_Msg_Queue_Id
                                          INNER JOIN dbo.Notification_Template nt
                                                ON nmq.Notification_Template_Id = nt.Notification_Template_Id
                                          INNER JOIN dbo.Code cd
                                                ON nt.Notification_Type_Cd = cd.Code_Id
                                          INNER JOIN dbo.Codeset cs
                                                ON cd.Codeset_Id = cs.Codeset_Id
                                         WHERE
                                          cs.Codeset_Name = 'Notification Type'
                                          AND cd.Code_Value IN ( 'Termination Analyst', 'Termination Vendor' )
                                          AND chasupp.Supplier_Contract_ID = fvl.Contract_Id
                                          AND chasupp.Meter_Id = fvmd.Meter_Id
                                          AND chasupp.Supplier_Meter_Disassociation_Date = fvl.Meter_Term_Dt )
                  GROUP BY
                        ch.Client_Name
                       ,ch.Client_Id
                       ,ch.City
                       ,ch.State_Name
                       ,ch.Site_name
                       ,ch.Site_Id
                       ,chautil.Account_Number
                       ,chautil.Account_Id
                       ,chasupp.Meter_Id
                       ,chasupp.Meter_Number
                       ,con.CONTRACT_ID
                       ,con.ED_CONTRACT_NUMBER
                       ,chasupp.Account_Vendor_Id
                       ,chasupp.Account_Vendor_Name
                       ,chasupp.Supplier_Meter_Disassociation_Date
                       ,vcam.Analyst_Id
                       ,ui.FIRST_NAME
                       ,ui.LAST_NAME
                       ,ui.EMAIL_ADDRESS
                       ,chautil.Commodity_Id
                       ,chautil.Account_Analyst_Mapping_Cd
                       ,ch.Site_Analyst_Mapping_Cd
                       ,ch.Client_Analyst_Mapping_Cd
                       ,ui.PHONE_NUMBER
                       ,com.Commodity_Name
                       ,chautil.Meter_Country_Name;
                       
      WITH  Cte_Terminations
              AS ( SELECT
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Expiry_Date
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Id
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END AS Analyst_Id
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Name
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.FIRST_NAME + SPACE(1) + uia.LAST_NAME, uis.FIRST_NAME + SPACE(1) + uis.LAST_NAME, uic.FIRST_NAME + SPACE(1) + uic.LAST_NAME)
                        END AS Analyst_Name
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Email_Address
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.EMAIL_ADDRESS, uis.EMAIL_ADDRESS, uic.EMAIL_ADDRESS)
                        END AS Analyst_Email_Address
                       ,cn.Commodity_Type_id
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Phone_Number
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.PHONE_NUMBER, uis.PHONE_NUMBER, uic.PHONE_NUMBER)
                        END AS Phone_Number
                       ,cn.Due_Date_Crossed
                       ,cn.Commodity
                       ,cn.Country
                   FROM
                        @Client_Con_Info cn
                        JOIN Core.Client_Commodity ccc
                              ON ccc.Client_Id = cn.Client_Id
                                 AND ccc.Commodity_Id = cn.Commodity_Type_id
                        LEFT JOIN dbo.Account_Commodity_Analyst aca
                              ON aca.Account_Id = cn.Account_Id
                                 AND aca.Commodity_Id = cn.Commodity_Type_id
                        LEFT JOIN dbo.USER_INFO uia
                              ON aca.Analyst_User_Info_Id = uia.USER_INFO_ID
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON sca.Site_Id = cn.Site_Id
                                 AND sca.Commodity_Id = cn.Commodity_Type_id
                        LEFT JOIN dbo.USER_INFO uis
                              ON sca.Analyst_User_Info_Id = uis.USER_INFO_ID
                        LEFT JOIN dbo.Client_Commodity_Analyst cca
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id
                        LEFT JOIN dbo.USER_INFO uic
                              ON cca.Analyst_User_Info_Id = uic.USER_INFO_ID
                   WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          Core.Client_Hier_Account samesupp
                                     WHERE
                                          samesupp.Meter_Id = cn.Meter_Id
                                          AND samesupp.Account_Vendor_Id = cn.Vendor_Id
                                          AND samesupp.Supplier_Contract_ID <> cn.Contract_Id
                                          AND samesupp.Supplier_Meter_Association_Date = DATEADD(DAY, 1, cn.Expiry_Date) )
                   GROUP BY
                        cn.Client_Name
                       ,cn.Site_Name
                       ,cn.Account_Number
                       ,cn.Account_Id
                       ,cn.Meter_Id
                       ,cn.Meter_Number
                       ,cn.Contract_Id
                       ,cn.Ed_Contract_Number
                       ,cn.Vendor_Id
                       ,cn.Vendor_Name
                       ,cn.Analyst_Mapping_Cd
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Id
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)
                        END
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Name
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.FIRST_NAME + SPACE(1) + uia.LAST_NAME, uis.FIRST_NAME + SPACE(1) + uis.LAST_NAME, uic.FIRST_NAME + SPACE(1) + uic.LAST_NAME)
                        END
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Analyst_Email_Address
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.EMAIL_ADDRESS, uis.EMAIL_ADDRESS, uic.EMAIL_ADDRESS)
                        END
                       ,cn.Commodity_Type_id
                       ,cn.Expiry_Date
                       ,CASE WHEN cn.Analyst_Mapping_Cd = @Default_Analyst THEN cn.Phone_Number
                             WHEN cn.Analyst_Mapping_Cd = @Custom_Analyst THEN COALESCE(uia.PHONE_NUMBER, uis.PHONE_NUMBER, uic.PHONE_NUMBER)
                        END
                       ,cn.Due_Date_Crossed
                       ,cn.Commodity
                       ,cn.Country)
            SELECT
                  ctet.Contract_Id
                 ,ctet.Ed_Contract_Number
                 ,ctet.Expiry_Date
                 ,ctet.Client_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1) AS Meter_Id
                 ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1) AS Meter_Number
                 ,ctet.Vendor_Name
                 ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1) AS Vendor_Contact_User_Info_Id
                 ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1) AS Termination_Contact_Email_Address
                 ,LEFT(auid.auids, LEN(auid.auids) - 1) Analyst_Id
                 ,LEFT(aname.anames, LEN(aname.anames) - 1) Analyst_Name
                 ,LEFT(aea.aeas, LEN(aea.aeas) - 1) Analyst_Email_Address
                 ,LEFT(pn.pns, LEN(pn.pns) - 1) Phone_Number
                 ,ctet.Due_Date_Crossed
                 ,ctet.Vendor_Id
                 ,LEFT(reqmail.reqmails, LEN(reqmail.reqmails) - 1) AS Requested_User_Info_Id_Email_Address
                 ,ctet.Commodity
                 ,LEFT(con.cons, LEN(con.cons) - 1) AS Country
                 ,LEFT(locale.code, LEN(locale.code) - 1) AS Locale_Cd
            FROM
                  Cte_Terminations ctet
                  CROSS APPLY ( SELECT
                                    CAST(ct.Meter_Id AS VARCHAR(20)) + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ctet.Expiry_Date = ct.Expiry_Date
                                    AND ct.Meter_Id IS NOT NULL
                                GROUP BY
                                    ct.Meter_Id
                  FOR
                                XML PATH('') ) mtr ( mtrs )
                  CROSS APPLY ( SELECT
                                    ct.Meter_Number + ', '
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ctet.Expiry_Date = ct.Expiry_Date
                                    AND ct.Meter_Number IS NOT NULL
                                GROUP BY
                                    ct.Meter_Number
                                   ,ct.Meter_Id
                  FOR
                                XML PATH('') ) mtrnum ( mtrnums )
                  CROSS APPLY ( SELECT
                                    CAST(ssci.USER_INFO_ID AS VARCHAR(20)) + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER  JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Due_Date_Crossed = 0
                                    AND ctet.Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                                GROUP BY
                                    ssci.USER_INFO_ID
                  FOR
                                XML PATH('') ) vcui ( vcuis )
                  CROSS APPLY ( SELECT
                                    ssci.Termination_Contact_Email_Address + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER  JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Due_Date_Crossed = 0
                                    AND ctet.Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                                GROUP BY
                                    ssci.Termination_Contact_Email_Address
                  FOR
                                XML PATH('') ) tcmail ( tcmails )
                  CROSS APPLY ( SELECT
                                    CAST(ct.Analyst_Id AS VARCHAR(20)) + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Analyst_Id IS NOT NULL
                                GROUP BY
                                    ct.Analyst_Id
                  FOR
                                XML PATH('') ) auid ( auids )
                  CROSS APPLY ( SELECT
                                    ct.Analyst_Name + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Analyst_Name IS NOT NULL
                                GROUP BY
                                    ct.Analyst_Name
                  FOR
                                XML PATH('') ) aname ( anames )
                  CROSS APPLY ( SELECT
                                    ct.Analyst_Email_Address + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Analyst_Email_Address IS NOT NULL
                                GROUP BY
                                    ct.Analyst_Email_Address
                  FOR
                                XML PATH('') ) aea ( aeas )
                  CROSS APPLY ( SELECT
                                    ct.Phone_Number + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Phone_Number IS NOT NULL
                                GROUP BY
                                    ct.Phone_Number
                  FOR
                                XML PATH('') ) pn ( pns )
                  CROSS APPLY ( SELECT
                                    CAST(ct.Analyst_Id AS VARCHAR(20)) + '~' + ct.Analyst_Email_Address + '^'
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Analyst_Email_Address IS NOT NULL
                                GROUP BY
                                    ct.Analyst_Id
                                   ,ct.Analyst_Email_Address
                  FOR
                                XML PATH('') ) reqmail ( reqmails )
                  CROSS APPLY ( SELECT
                                    ct.Country + ','
                                FROM
                                    Cte_Terminations ct
                                WHERE
                                    ctet.Contract_Id = ct.Contract_Id
                                    AND ct.Country IS NOT NULL
                                GROUP BY
                                    ct.Country
                  FOR
                                XML PATH('') ) con ( cons )
                  CROSS APPLY ( SELECT TOP 1
                                    CAST(cd.Code_Id AS VARCHAR(10)) + '~' + cd.Code_Value + ','
                                FROM
                                    dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                                    INNER  JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                                          ON sscvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN dbo.USER_INFO AS vci
                                          ON vci.USER_INFO_ID = ssci.USER_INFO_ID
                                    INNER JOIN dbo.Code cd
                                          ON cd.Code_Value = vci.locale_code
                                    INNER JOIN dbo.Codeset cs
                                          ON cd.Codeset_Id = cs.Codeset_Id
                                    INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP metermap
                                          ON metermap.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
                                    INNER JOIN Core.Client_Hier_Account cha
                                          ON metermap.COUNTRY_ID = cha.Meter_Country_Id
                                             AND metermap.STATE_ID = cha.Meter_State_Id
                                             AND metermap.COMMODITY_TYPE_ID = cha.Commodity_Id
                                    INNER JOIN Cte_Terminations mtr
                                          ON mtr.Meter_Id = cha.Meter_Id
                                WHERE
                                    sscvm.IS_PRIMARY = 1
                                    AND ctet.Due_Date_Crossed = 0
                                    AND ctet.Vendor_Id = sscvm.VENDOR_ID
                                    AND ssci.Termination_Contact_Email_Address IS NOT NULL
                                    AND ssci.Termination_Contact_Email_Address <> ''
                                    AND vci.IS_HISTORY = 0
                                    AND cs.Codeset_Name = 'LocalizationLanguage'
                                    AND metermap.IS_PRIMARY = 0
                                    AND cha.Supplier_Contract_ID = ctet.Contract_Id
                  FOR
                                XML PATH('') ) locale ( code )
            GROUP BY
                  ctet.Contract_Id
                 ,ctet.Ed_Contract_Number
                 ,ctet.Expiry_Date
                 ,ctet.Client_Name
                 ,LEFT(mtr.mtrs, LEN(mtr.mtrs) - 1)
                 ,LEFT(mtrnum.mtrnums, LEN(mtrnum.mtrnums) - 1)
                 ,ctet.Vendor_Name
                 ,LEFT(vcui.vcuis, LEN(vcui.vcuis) - 1)
                 ,LEFT(tcmail.tcmails, LEN(tcmail.tcmails) - 1)
                 ,LEFT(auid.auids, LEN(auid.auids) - 1)
                 ,LEFT(aname.anames, LEN(aname.anames) - 1)
                 ,LEFT(aea.aeas, LEN(aea.aeas) - 1)
                 ,LEFT(pn.pns, LEN(pn.pns) - 1)
                 ,ctet.Due_Date_Crossed
                 ,ctet.Vendor_Id
                 ,LEFT(reqmail.reqmails, LEN(reqmail.reqmails) - 1)
                 ,ctet.Commodity
                 ,LEFT(con.cons, LEN(con.cons) - 1)
                 ,LEFT(locale.code, LEN(locale.code) - 1);
        
END;




;
GO

GRANT EXECUTE ON  [dbo].[Contract_Termination_Sel_Eligible_For_Notifications] TO [CBMSApplication]
GO
