SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	Commodity_Current_Recalc_Type_Sel_By_Account

DESCRIPTION:

	Return the details of current Recalc Type for the commodity Based on Account_Id 
	
INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
	@Account_Id			int						(Utility)

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Recalc_Type_Sel_By_Invoice_Commodity_Account 35234563,732549,290

	
	EXEC Recalc_Type_Sel_By_Invoice_Commodity_Account 35237318,1148737,290


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV			RAVI KUMAR VEGESNA
	NR			Narayana Reddy				

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV        	2015-11-05 	Created as Part of AS400-PII
	NR			2018-03-23	PAm interval data - Added Determinant Source.
								Modified to use the maximum service month of invoice to get the Recalc type/determinant source from Account_Commodity_Invoice_Recalc_Type
	HG			2018-05-14	MAINT-7360, Modified to return the recalc type and Determinant source from Account table for the Supplier accounts.

******/
CREATE PROCEDURE [dbo].[Recalc_Type_Sel_By_Invoice_Commodity_Account]
      (
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_ID INT )
AS
BEGIN

      SET NOCOUNT ON

      DECLARE @Account_Commodity_Invoice_Recalc_Type TABLE
            (
             Account_Id INT
            ,Commodity_ID INT
            ,commodity_Name VARCHAR(50)
            ,Recalc_Type_Cd INT
            ,Recalc_Type VARCHAR(25) )

      DECLARE
            @Invoice_Max_Service_Month DATE
           ,@Supplier_Account_Type_Id INT
           ,@Supplier_Account_Recalc_Type_Cd INT
           ,@Supplier_Account_Recalc_Type VARCHAR(25)
           ,@Supplier_Account_Determinant_Source VARCHAR(25)
           ,@Commodity_Name VARCHAR(25)
           ,@Account_Type VARCHAR(200)= ''

      SELECT
            @Supplier_Account_Type_Id = ENTITY_ID
      FROM
            dbo.ENTITY
      WHERE
            ENTITY_NAME = 'Supplier'
            AND ENTITY_DESCRIPTION = 'Account Type'

      INSERT      INTO @Account_Commodity_Invoice_Recalc_Type
                  ( Account_Id
                  ,Commodity_ID
                  ,commodity_Name
                  ,Recalc_Type_Cd
                  ,Recalc_Type )
                  SELECT
                        ced.Account_ID
                       ,ced.Commodity_Id
                       ,com.Commodity_Name
                       ,ced.Recalc_Type_cd
                       ,cd.Code_Value
                  FROM
                        dbo.CU_EXCEPTION_DENORM ced
                        INNER JOIN dbo.Commodity com
                              ON ced.Commodity_Id = com.Commodity_Id
                        INNER JOIN dbo.Code cd
                              ON ced.Recalc_Type_cd = cd.Code_Id
                  WHERE
                        ced.CU_INVOICE_ID = @Cu_Invoice_Id
                        AND ced.Account_ID = @Account_Id
                        AND ced.Commodity_Id = @Commodity_ID
     
      SELECT
            @Invoice_Max_Service_Month = MAX(SERVICE_MONTH)
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH
      WHERE
            CU_INVOICE_ID = @Cu_Invoice_Id
            AND Account_ID = @Account_Id

      SELECT
            @Commodity_Name = Commodity_Name
      FROM
            dbo.Commodity
      WHERE
            Commodity_Id = @Commodity_ID

      SELECT
            @Supplier_Account_Recalc_Type_Cd = ac.Supplier_Account_Recalc_Type_Cd
           ,@Supplier_Account_Recalc_Type = rt.Code_Value
           ,@Supplier_Account_Determinant_Source = c.Code_Value
           ,@Account_Type = 'Supplier'
      FROM
            dbo.ACCOUNT ac
            LEFT OUTER JOIN dbo.Code c
                  ON c.Code_Id = ac.Supplier_Account_Determinant_Source_Cd
            LEFT JOIN dbo.Code rt
                  ON rt.Code_Id = ac.Supplier_Account_Recalc_Type_Cd
      WHERE
            ac.ACCOUNT_TYPE_ID = @Supplier_Account_Type_Id
            AND ac.ACCOUNT_ID = @Account_Id

      INSERT      INTO @Account_Commodity_Invoice_Recalc_Type
                  (Account_Id
                  ,Commodity_ID
                  ,commodity_Name
                  ,Recalc_Type_Cd
                  ,Recalc_Type )
                  SELECT
                        acirt.Account_Id
                       ,acirt.Commodity_ID
                       ,@Commodity_Name
                       ,acirt.Invoice_Recalc_Type_Cd
                       ,cd.Code_Value AS Recalc_Type
                  FROM
                        dbo.Account_Commodity_Invoice_Recalc_Type acirt
                        INNER JOIN dbo.Code cd
                              ON acirt.Invoice_Recalc_Type_Cd = cd.Code_Id
                  WHERE
                        acirt.Account_Id = @Account_Id
                        AND acirt.Commodity_ID = @Commodity_ID
                        AND @Invoice_Max_Service_Month BETWEEN Start_Dt
                                                       AND     ISNULL(End_Dt, DATEADD(dd, 1, GETDATE()))
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          @Account_Commodity_Invoice_Recalc_Type acirt2
                                         WHERE
                                          acirt.Account_Id = acirt2.Account_Id
                                          AND acirt.Commodity_ID = acirt2.Commodity_ID )

      INSERT      INTO @Account_Commodity_Invoice_Recalc_Type
                  (Account_Id
                  ,Commodity_ID
                  ,commodity_Name
                  ,Recalc_Type_Cd
                  ,Recalc_Type )
                  SELECT
                        @Account_Id
                       ,@Commodity_ID
                       ,@Commodity_Name
                       ,@Supplier_Account_Recalc_Type_Cd
                       ,@Supplier_Account_Recalc_Type
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Account_Commodity_Invoice_Recalc_Type acirt2
                                     WHERE
                                          acirt2.Account_Id = @Account_Id
                                          AND acirt2.Commodity_ID = @Commodity_ID )

      SELECT
            acirt.Account_Id
           ,acirt.Commodity_ID
           ,acirt.commodity_Name
           ,acirt.Recalc_Type_Cd
           ,acirt.Recalc_Type
           ,CASE WHEN @Account_Type = 'Supplier' THEN @Supplier_Account_Determinant_Source
                 ELSE c.Code_Value
            END AS Determinant_Source
      FROM
            @Account_Commodity_Invoice_Recalc_Type acirt
            LEFT OUTER JOIN ( dbo.Account_Commodity_Invoice_Recalc_Type acit
                              INNER JOIN dbo.Code c
                                    ON acit.Determinant_Source_Cd = c.Code_Id )
                  ON acirt.Account_Id = acit.Account_Id
                     AND acirt.Commodity_ID = acit.Commodity_ID
                     AND @Invoice_Max_Service_Month BETWEEN Start_Dt
                                                    AND     ISNULL(End_Dt, DATEADD(dd, 1, GETDATE()))                                     



END;






GO
GRANT EXECUTE ON  [dbo].[Recalc_Type_Sel_By_Invoice_Commodity_Account] TO [CBMSApplication]
GO
