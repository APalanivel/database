
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Billing_Determinant_Del]  

DESCRIPTION: It Deletes Billing Determinant for Selected Billing Determinant Id.     
      
INPUT PARAMETERS:          
	NAME						DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Billing_Determinant_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
	EXEC Billing_Determinant_Del 1066
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH
	CPE			Chaitanya Panduga Eshwar

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			17-JUN-10	CREATED
	CPE			2012-08-22	Included the delete on Time_Of_Use_Schedule_Term_Peak_Billing_Determinant 

*/

CREATE PROCEDURE dbo.Billing_Determinant_Del
( 
 @Billing_Determinant_Id INT )
AS 
BEGIN

      SET NOCOUNT ON ;

      BEGIN TRY
            BEGIN TRAN
            DELETE FROM
                  dbo.Time_Of_Use_Schedule_Term_Peak_Billing_Determinant
            WHERE
                  BILLING_DETERMINANT_ID = @Billing_Determinant_Id
	
            DELETE FROM
                  dbo.BILLING_DETERMINANT
            WHERE
                  BILLING_DETERMINANT_ID = @Billing_Determinant_Id
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC dbo.usp_RethrowError
      END CATCH
END
;
GO

GRANT EXECUTE ON  [dbo].[Billing_Determinant_Del] TO [CBMSApplication]
GO
