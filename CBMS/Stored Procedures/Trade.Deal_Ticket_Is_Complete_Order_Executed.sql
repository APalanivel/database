SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	Trade.Deal_Ticket_Is_Complete_Order_Executed
   
    
DESCRIPTION:   
   
  This procedure to get summary page details for deal ticket summery.
    
INPUT PARAMETERS:    
      Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Deal_Ticket_Is_Complete_Order_Executed  131849
	Exec Trade.Deal_Ticket_Is_Complete_Order_Executed  131856
	Exec Trade.Deal_Ticket_Is_Complete_Order_Executed  132006
	Exec Trade.Deal_Ticket_Is_Complete_Order_Executed  132015
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy    
    
MODIFICATIONS     
	Initials    Date        Modification      
-------------------------------------------------------------------------------------------------------------------       
	RR          2018-11-21  GRM - Created
     
    
******/

CREATE PROCEDURE [Trade].[Deal_Ticket_Is_Complete_Order_Executed]
    (
        @Deal_Ticket_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Deal_Ticket_Is_Complete_Order_Executed BIT = 0
            , @Any_Trade INT = 1
            , @Any_Trade_Price INT = 1;



        SELECT
            @Any_Trade = MIN(CASE WHEN vol.Trade_Number IS NULL THEN 0
                                 ELSE 1
                             END)
            , @Any_Trade_Price = MIN(CASE WHEN tp.Trade_Price IS NULL THEN 0
                                         ELSE 1
                                     END)
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
            LEFT JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = vol.Trade_Price_Id
        WHERE
            vol.Deal_Ticket_Id = @Deal_Ticket_Id
            AND vol.Total_Volume > 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier dtch
                                    INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                                        ON chws.Deal_Ticket_Client_Hier_Id = dtch.Deal_Ticket_Client_Hier_Id
                                    INNER JOIN Trade.Workflow_Status_Map wsm
                                        ON wsm.Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
                                    INNER JOIN Trade.Workflow_Status ws
                                        ON ws.Workflow_Status_Id = wsm.Workflow_Status_Id
                               WHERE
                                    vol.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                                    AND vol.Client_Hier_Id = dtch.Client_Hier_Id
                                    AND vol.Deal_Month = chws.Trade_Month
                                    AND ws.Workflow_Status_Name = 'Canceled'
                                    AND chws.Is_Active = 1);

        SELECT
            @Deal_Ticket_Is_Complete_Order_Executed = 1
        WHERE
            @Any_Trade = 1
            AND @Any_Trade_Price = 1;
        SELECT
            @Deal_Ticket_Is_Complete_Order_Executed AS Deal_Ticket_Is_Complete_Order_Executed;



    END;


GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Is_Complete_Order_Executed] TO [CBMSApplication]
GO
