
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UBM_GET_UBM_AVISTA_EXPECTED_RECORDS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@masterLogId   	int       	          	
	@consolidation 	VARCHAR(50)       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

EXEC UBM_GET_UBM_AVISTA_EXPECTED_RECORDS_P '','',15196,'03052014'


SELECT TOP 10 * FROM UBM_AVISTA_CONTROL ORDER BY UBM_BATCH_MASTER_LOG_ID DESC

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			2014-04-17	Ecova enhancement- Changed the consolidation parameter to match with the table column type
******/


CREATE PROCEDURE dbo.UBM_GET_UBM_AVISTA_EXPECTED_RECORDS_P
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@masterLogId INT
      ,@consolidation VARCHAR(50) )
AS 
BEGIN

      SET NOCOUNT ON
	
      SELECT
            TOTAL_EXPECTED_RECORDS
           ,TOTAL_EXPECTED_DOLLARS
           ,TOTAL_EXPECTED_IMAGES
           ,TOTAL_EXPECTED_BYTES
      FROM
            UBM_AVISTA_CONTROL
      WHERE
            CONSOLIDATION_ID = @CONSOLIDATION
            AND UBM_BATCH_MASTER_LOG_ID = @MASTERLOGID

END

;
GO

GRANT EXECUTE ON  [dbo].[UBM_GET_UBM_AVISTA_EXPECTED_RECORDS_P] TO [CBMSApplication]
GO
