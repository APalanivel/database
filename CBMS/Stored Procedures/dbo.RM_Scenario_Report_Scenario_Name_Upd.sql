SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.RM_Scenario_Report_Scenario_Name_Upd            
                        
 DESCRIPTION:                        
			To Update Scenario Name of RM_Scenario_Report table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@RM_Scenario_Report_Id			INT
@Scenario_Name				NVARCHAR(200)   
@Updated_User_Id				INT       
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

BEGIN TRAN 
SELECT * FROM RM_Scenario_Report WHERE RM_Scenario_Report_Id=22

EXEC dbo.RM_Scenario_Report_Scenario_Name_Upd 
     @RM_Scenario_Report_Id=22
     ,@Scenario_Name='test update'
     ,@Updated_User_Id =49
      
SELECT * FROM RM_Scenario_Report WHERE RM_Scenario_Report_Id=22
ROLLBACK
  
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-11-14       Created                
                       
******/

          
CREATE PROCEDURE [dbo].[RM_Scenario_Report_Scenario_Name_Upd]
      ( 
       @RM_Scenario_Report_Id INT
      ,@Scenario_Name NVARCHAR(200)
      ,@Updated_User_Id INT )
AS 
BEGIN                
      SET NOCOUNT ON;     
      
      UPDATE
            dbo.RM_Scenario_Report
      SET   
            Scenario_Name = @Scenario_Name
           ,Updated_User_Id = @Updated_User_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            RM_Scenario_Report_Id = @RM_Scenario_Report_Id
          
                   
END 

;
GO
GRANT EXECUTE ON  [dbo].[RM_Scenario_Report_Scenario_Name_Upd] TO [CBMSApplication]
GO
