SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.[Get_Currency_Details] 
   
DESCRIPTION: 
To get the currency details

INPUT PARAMETERS:      
Name             DataType               Default        Description      
--------------------------------------------------------------------   


OUTPUT PARAMETERS:      
Name   DataType  Default Description      
-------------------------------------------------------------------- 
CURRENCY_UNIT_ID
CURRENCY_UNIT_NAME

USAGE EXAMPLES:    
--------------------------------------------------------------------    
EXEC Get_Currency_Details 

AUTHOR INITIALS:    
Initials	Name    
-------------------------------------------------------------------    
BM			BIJU MOHAN
   
MODIFICATIONS     
Initials	Date		Modification    
--------------------------------------------------------------------    

******************************************************************************************************/

CREATE PROCEDURE [dbo].[Get_Currency_Details]
AS
BEGIN

	SELECT 
		CURRENCY_UNIT_ID
	,	CURRENCY_UNIT_NAME 
	FROM 
		CURRENCY_UNIT;--Retrieve all the valid currency names
	
END
	
	



GO
GRANT EXECUTE ON  [dbo].[Get_Currency_Details] TO [CBMSApplication]
GO
