
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.UPDATE_UTILITY_DETAILS_P    

DESCRIPTION:  This procedure used to insert the records in UTILITY_DETAILS table

INPUT PARAMETERS:    
      Name                        DataType          Default     Description    
---------------------------------------------------------------------------    
	@UTILITY_DETAIL_ID	INT
    @VENDOR_ID			INT
    @WEBSITE_URL		VARCHAR(50)
    @PIPELINE			VARCHAR(100)
    @MAIN_CONTACT		VARCHAR(200)
    @RATE_CONTACT		VARCHAR(200)
    @BILLING_CONTACT	VARCHAR(40)
    @DATA_CONTACT		VARCHAR(200)
    @UTILITY_COMMENTS	VARCHAR(1000)
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    

    
USAGE EXAMPLES:    
------------------------------------------------------------    

Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
	SKA		01/21/2011	Modifed as we removed few columns from table
            
******/  

CREATE PROCEDURE dbo.UPDATE_UTILITY_DETAILS_P
      @UTILITY_DETAIL_ID INT
     ,@VENDOR_ID INT
     ,@WEBSITE_URL VARCHAR(50)
     ,@PIPELINE VARCHAR(100)
     ,@MAIN_CONTACT VARCHAR(200)
     ,@RATE_CONTACT VARCHAR(200)
     ,@BILLING_CONTACT VARCHAR(40)
     ,@DATA_CONTACT VARCHAR(200)
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      UPDATE
            dbo.utility_detail
      SET   
            VENDOR_ID = @VENDOR_ID
           ,WEBSITE_URL = @WEBSITE_URL
           ,MAIN_CONTACT = @MAIN_CONTACT
           ,RATE_CONTACT = @RATE_CONTACT
           ,BILLING_CONTACT = @BILLING_CONTACT
           ,PIPELINE = @PIPELINE
           ,Data_Contact = @DATA_CONTACT
      WHERE
            UTILITY_DETAIL_ID = @UTILITY_DETAIL_ID  
END  

GO

GRANT EXECUTE ON  [dbo].[UPDATE_UTILITY_DETAILS_P] TO [CBMSApplication]
GO
