SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Account_Exception_History_Dtl_Sel_By_Account_Exception_Id                  
                            
 DESCRIPTION:        
  Exception history details based on Exception Id.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Account_Exception_Id   INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  
 
 EXEC dbo.Account_Exception_History_Dtl_Sel_By_Account_Exception_Id  376706    
  
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2020-06-16      Created for SE2017- 981                         
                           
******/

CREATE PROCEDURE [dbo].[Account_Exception_History_Dtl_Sel_By_Account_Exception_Id]
    (
        @Account_Exception_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;


        SELECT
            ae.Exception_Type_Cd
            , c.Code_Dsc AS Exception_Type
            , ae.Exception_Status_Cd
            , cc.Code_Value AS Exception_Status
            , ae.Exception_Created_Ts
            , ae.Exception_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Exception_By_User
            , ae.Account_Exception_Id
        FROM
            dbo.Account_Exception ae
            INNER JOIN dbo.Code c
                ON c.Code_Id = ae.Exception_Type_Cd
            INNER JOIN Code cc
                ON cc.Code_Id = ae.Exception_Status_Cd
            INNER JOIN dbo.USER_INFO ui
                ON ui.USER_INFO_ID = ae.Exception_By_User_Id
        WHERE
            ae.Account_Exception_Id = @Account_Exception_Id
            AND cc.Code_Value IN ( 'New', 'In Progress' )
        GROUP BY
            ae.Exception_Type_Cd
            , c.Code_Dsc
            , ae.Exception_Status_Cd
            , cc.Code_Value
            , ae.Exception_Created_Ts
            , ae.Exception_By_User_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , ae.Account_Exception_Id
        ORDER BY
            ae.Exception_Created_Ts;
    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Account_Exception_History_Dtl_Sel_By_Account_Exception_Id] TO [CBMSApplication]
GO
