SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                                        
 NAME: dbo.Report_DE_IC_Chase                                                            
                                                                        
 DESCRIPTION:                                                                        
   This will allow the managers to gain a better understanding of the scope of Chase history that can be encountered in Invoice collection                                                
                                                                     
                                                                        
 INPUT PARAMETERS:                                                          
                                                                     
 Name                        DataType         Default       Description                                                        
---------------------------------------------------------------------------------------------------------------                                                      
                                                                       
 OUTPUT PARAMETERS:                                                          
                                                                           
 Name                        DataType         Default       Description                                                        
---------------------------------------------------------------------------------------------------------------                                                      
                                                                        
 USAGE EXAMPLES:                                                                            
---------------------------------------------------------------------------------------------------------------                                                                            
   EXEC [dbo].[Report_DE_IC_Chase]                                                 
       @Client_Id=-1                                                
       ,@Start_Dt='2017-02-01'                                                
       ,@End_Dt='2017-02-28'                                                
                                                       
    EXEC [dbo].[Report_DE_IC_Chase]                                                 
       @Client_Id=141                                                
       ,@Date_Type='Chase dates'                                                
       ,@Start_Dt='2017-02-17'                                                
       ,@End_Dt='2017-02-17'                                                
                                                       
    --Delfood                                                   
                                                  
EXEC [dbo].[Report_DE_IC_Chase]                                                 
       @Client_Id=12628                                                
       ,@Date_Type='CHASE dates'                                                
       ,@Start_Dt='2017-02-17'                                                
       ,@End_Dt='2017-02-17'                                                
                                                
 EXEC [dbo].[Report_DE_IC_Chase]                                                 
       @Client_Id=12628                                                
       ,@Date_Type='CHASE dates'                                                
       ,@Start_Dt='2017-02-17'                                                
       ,@End_Dt='2017-02-17'                                                
                                                        
   EXEC [dbo].[Report_DE_IC_Chase]                                                 
       @Client_Id=11230                                                
       ,@Date_Type='Invoice dates'                                                
       ,@Start_Dt='2018-06-01'                                                
       ,@End_Dt='2018-12-31'        
    ,@CM = 'Mark Williams'                    
                  
  EXEC [dbo].[Report_DE_IC_Chase]                                              
       @Client_Id=12945         
       ,@Date_Type='Chase dates'                                                
       ,@Start_Dt='2016-01-01'                                                
       ,@End_Dt='2019-12-31'               
  ,@CM = '<All>'  --'44129, 51344'              
 ,@CA = '<All>'  --'46338'              
 ,@CS = '<All>'  --'51344'                                   
                                                   
 SELECT * FROM core.Client_Hier ch                                
     WHERE ch.Client_Name LIKE 'arse%'                                                                    
                                       
 AUTHOR INITIALS:                                                        
                                                       
 Initials              Name                                                        
---------------------------------------------------------------------------------------------------------------                                                                      
 SP                    Sandeep Pigilam                                                
 RKV                   Ravi Kumar Vegesna                                               
 NR      Narayana Reddy                                                         
 ABK     Aditya Bharadwaj                                    
                                                                         
 MODIFICATIONS:                                                                                            
 Initials              Date             Modification                                                      
---------------------------------------------------------------------------------------------------------------                               
 SP                    2017-01-25       Created for Invoice Tracking.                                                
 RKV                   2017-12-05       SE2017-313,Added new column to the result set Invoice_Collection_Owner                                                   
 NR      2018-01-02       SE2017-388 - IC Reports - To excluded the  Demo Clients.                                                   
 RKV                   2018-01-09       SE2017-389  Added Filter to exclude the Archived Records.                                                
 ABK     2019-06-07  SE2017-613                                        
 ABK     2019-07-19  REPTMGR-117  Added filters to Invoice Collection - Issue Report and Chase History Report in Reporting Services              
 ABK  2019-08-21	SSRS hot fix:  Added <All> in drop down values   
 ABK  2019-08-28	REPTMGR-120      Added invoice source types                                                          
******/


CREATE PROCEDURE [dbo].[Report_DE_IC_Chase]
     (
         @Client_Id INT = -1
         , @Client_Hier_Id INT = -1
         , @Date_Type VARCHAR(25) = 'Chase dates'
         , @Start_Dt DATE
         , @End_Dt DATE
         , @Invoice_Collection_Officer_Id INT = -1
         , @Country_Id INT = -1
         , @Vendor_Id INT = -1
         , @Invoice_Collection_Coordinator_Id INT = -1
         , @Is_Historic INT = 0
         , @Invoice_Collection_Owner_Id INT = -1
         , @CM VARCHAR(MAX) = -1
         , @CA VARCHAR(MAX) = -1
         , @CS VARCHAR(MAX) = -1
     )
AS
    BEGIN
        SET NOCOUNT ON;

        IF (1 = 2)
            SELECT
                1 AS Client
                , 1 AS Site
                , 1 AS Country
                , 1 AS State_Name
                , 1 AS Vendor_Name
                , 1 AS Display_Account_Number
                , 1 AS Alternative_Account_Number
                , 1 AS Commodity
                , 1 AS Collection_Start_Dt
                , 1 AS Collection_End_Dt
                , 1 AS Invoice_Source
                , 1 AS Raised_date
                , 1 AS Invoice_status
                , 1 AS Issue_Status
                , 1 AS Invoice_Collection_Chase_Log_Id
                , 1 AS Chase_Type
                , 1 AS Chased_Date
                , 1 AS Chase_method
                , 1 AS Chased_By
                , 1 AS Person_chased
                , 1 AS Contact_Source
                , 1 AS Invoice_Chase_Comment
                , 1 AS Status_value
                , 1 AS Received_date
                , 1 AS Exception_date
                , 1 AS Exception_comments
                , 1 AS Invoice_Collection_Officer
                , 1 AS Invoice_Collection_Coordinator
                , 1 AS Invoice_Collection_Owner
                , 1 AS Invoice_Collection_Queue_Id
                , 1 AS Client_Id
                , 1 AS CEM
                , 1 AS CEA
                , 1 AS CSA;

        DECLARE
            @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Source_Type_ETL INT
            , @Source_Type_Partner INT
            , @Source_Type_Mail_Redirect INT
            , @source_type_online INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT
            , @ICR_Excluded INT
            , @ICR_Received INT
            , @ICE_Excluded INT
            , @ICE_Received INT
            , @Invoice_Collection_Type_Cd INT
            , @Exclude_Demo_Client_Type_Id INT;


        CREATE TABLE #UserInfo
             (
                 Client_Id INT
                 , User_List_CEM VARCHAR(MAX)
                 , User_List_CEA VARCHAR(MAX)
                 , User_List_CSA VARCHAR(MAX)
             );

        INSERT INTO #UserInfo
             (
                 Client_Id
                 , User_List_CEM
                 , User_List_CEA
                 , User_List_CSA
             )
        (SELECT
                u.CLIENT_ID
                , STUFF((   SELECT
                                ', ' + u1.Name
                            FROM
                            (   SELECT
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME Name
                                FROM
                                    dbo.USER_INFO ui
                                    JOIN dbo.CLIENT_CEM_MAP ccm2
                                        ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                                GROUP BY
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME) u1
                            WHERE
                                u1.CLIENT_ID = u.CLIENT_ID
                            GROUP BY
                                u1.Name
                            FOR XML PATH('')), 1, 2, '') AS cm
                , STUFF((   SELECT
                                ', ' + u1.Name
                            FROM
                            (   SELECT
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME Name
                                FROM
                                    dbo.USER_INFO ui
                                    JOIN dbo.CLIENT_CEA_MAP ccm2
                                        ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                                GROUP BY
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME) u1
                            WHERE
                                u1.CLIENT_ID = u.CLIENT_ID
                            GROUP BY
                                u1.Name
                            FOR XML PATH('')), 1, 2, '') AS ca
                , STUFF((   SELECT
                                ', ' + u1.Name
                            FROM
                            (   SELECT
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME Name
                                FROM
                                    dbo.USER_INFO ui
                                    JOIN dbo.CLIENT_CSA_MAP ccm2
                                        ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                                GROUP BY
                                    ccm2.CLIENT_ID
                                    , ui.FIRST_NAME + ' ' + ui.LAST_NAME) u1
                            WHERE
                                u1.CLIENT_ID = u.CLIENT_ID
                            GROUP BY
                                u1.Name
                            FOR XML PATH('')), 1, 2, '') AS cs
         FROM
            (   SELECT
                    c.CLIENT_ID
                    , cem.USER_INFO_ID CEM_ID
                    , uicem.FIRST_NAME + ' ' + uicem.LAST_NAME CEM_Name
                    , cea.USER_INFO_ID CEA_ID
                    , uicea.FIRST_NAME + ' ' + uicea.LAST_NAME CEA_Name
                    , csa.USER_INFO_ID CSA_ID
                    , uicsa.FIRST_NAME + ' ' + uicsa.LAST_NAME CSA_Name
                FROM
                    dbo.CLIENT c
                    LEFT JOIN dbo.CLIENT_CEM_MAP cem
                        ON cem.CLIENT_ID = c.CLIENT_ID
                    LEFT JOIN dbo.USER_INFO uicem
                        ON uicem.USER_INFO_ID = cem.USER_INFO_ID
                    LEFT JOIN dbo.CLIENT_CEA_MAP cea
                        ON cea.CLIENT_ID = c.CLIENT_ID
                    LEFT JOIN dbo.USER_INFO uicea
                        ON uicea.USER_INFO_ID = cea.USER_INFO_ID
                    LEFT JOIN dbo.CLIENT_CSA_MAP csa
                        ON csa.CLIENT_ID = c.CLIENT_ID
                    LEFT JOIN dbo.USER_INFO uicsa
                        ON uicsa.USER_INFO_ID = csa.USER_INFO_ID
                GROUP BY
                    c.CLIENT_ID
                    , cem.USER_INFO_ID
                    , uicem.FIRST_NAME + ' ' + uicem.LAST_NAME
                    , cea.USER_INFO_ID
                    , uicea.FIRST_NAME + ' ' + uicea.LAST_NAME
                    , csa.USER_INFO_ID
                    , uicsa.FIRST_NAME + ' ' + uicsa.LAST_NAME) AS u
         WHERE
             (   @Client_Id <> -1
                 AND u.CLIENT_ID = @Client_Id)
             OR (   @Client_Id = -1
                    AND @CM = '<All>'
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@CM, ',') us
                                   WHERE
                                        us.Segments = CAST(u.CEM_ID AS VARCHAR(MAX))))
             OR (   @Client_Id = -1
                    AND @CA = '<All>'
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@CA, ',') us
                                   WHERE
                                        us.Segments = CAST(u.CEA_ID AS VARCHAR(MAX))))
             OR (   @Client_Id = -1
                    AND @CS = '<All>'
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@CS, ',') us
                                   WHERE
                                        us.Segments = CAST(u.CSA_ID AS VARCHAR(MAX))))
             OR (   @Client_Id = -1
                    AND @CM = CAST(-1 AS VARCHAR(MAX))
                    AND @CA = CAST(-1 AS VARCHAR(MAX))
                    AND @CS = CAST(-1 AS VARCHAR(MAX)))
         GROUP BY
             u.CLIENT_ID);



        SELECT
            @Source_Type_Client = MAX(CASE WHEN c.Code_Value = 'Client Primary Contact' THEN c.Code_Id
                                      END)
            , @Source_Type_Account = MAX(CASE WHEN c.Code_Value = 'Account Primary Contact' THEN c.Code_Id
                                         END)
            , @Source_Type_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor Primary Contact' THEN c.Code_Id
                                        END)
            , @Source_Type_ETL = MAX(CASE WHEN c.Code_Value = 'ETL' THEN c.Code_Id
                                     END)
            , @Source_Type_Partner = MAX(CASE WHEN c.Code_Value = 'Partner' THEN c.Code_Id
                                         END)
            , @Source_Type_Mail_Redirect = MAX(CASE WHEN c.Code_Value = 'Mail Redirect' THEN c.Code_Id
                                               END)
            , @source_type_online = MAX(CASE WHEN c.Code_Value = 'Online' THEN c.Code_Id
                                        END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'InvoiceSourceType'
            AND c.Code_Value IN ( 'Account Primary Contact', 'Client Primary Contact', 'Vendor Primary Contact', 'ETL'
                                  , 'Partner', 'Mail Redirect', 'Online' );


        SELECT
            @Contact_Level_Client = MAX(CASE WHEN c.Code_Value = 'Client' THEN c.Code_Id
                                        END)
            , @Contact_Level_Account = MAX(CASE WHEN c.Code_Value = 'Account' THEN c.Code_Id
                                           END)
            , @Contact_Level_Vendor = MAX(CASE WHEN c.Code_Value = 'Vendor' THEN c.Code_Id
                                          END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ContactLevel';

        SELECT
            @ICR_Excluded = MAX(CASE WHEN c.Code_Value = 'Excluded' THEN c.Code_Id
                                END)
            , @ICR_Received = MAX(CASE WHEN c.Code_Value = 'Received' THEN c.Code_Id
                                  END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICR Status';

        SELECT
            @ICE_Excluded = MAX(CASE WHEN c.Code_Value = 'Excluded' THEN c.Code_Id
                                END)
            , @ICE_Received = MAX(CASE WHEN c.Code_Value = 'Received' THEN c.Code_Id
                                  END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'ICE Status';

        SELECT
            @Invoice_Collection_Type_Cd = Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Type_Cd'
            AND c.Code_Value = 'ICE';


        SELECT
            @Exclude_Demo_Client_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            ENTITY_NAME = 'Demo'
            AND ENTITY_TYPE = 125
            AND ENTITY_DESCRIPTION = 'Client Type';

        CREATE TABLE #Invoices_Not_Chased
             (
                 Client VARCHAR(MAX) NOT NULL
                 , Site VARCHAR(MAX) NOT NULL
                 , Country VARCHAR(MAX) NULL
                 , State_Name VARCHAR(MAX) NULL
                 , Vendor_Name VARCHAR(MAX) NULL
                 , Display_Account_Number VARCHAR(MAX) NULL
                 , Alternative_Account_Number NVARCHAR(MAX) NULL
                 , Commodity VARCHAR(MAX) NOT NULL
                 , Collection_Start_Dt DATE NOT NULL
                 , Collection_End_Dt DATE NOT NULL
                 , Invoice_Source VARCHAR(MAX) NULL
                 , Raised_date DATETIME NOT NULL
                 , Invoice_status VARCHAR(MAX) NULL
                 , Issue_Status VARCHAR(MAX) NULL
                 , Invoice_Collection_Chase_Log_Id INT NULL
                 , Chase_Type VARCHAR(MAX) NULL
                 , Chased_Date DATETIME NULL
                 , Chase_method VARCHAR(MAX) NULL
                 , Chased_By VARCHAR(MAX) NULL
                 , Person_chased VARCHAR(MAX) NULL
                 , Contact_Source VARCHAR(MAX) NULL
                 , Invoice_Chase_Comment NVARCHAR(MAX) NULL
                 , Status_value VARCHAR(MAX) NULL
                 , Received_date DATETIME NULL
                 , Exception_date DATETIME NULL
                 , Exception_comments NVARCHAR(MAX) NULL
                 , Invoice_Collection_Officer VARCHAR(MAX) NULL
                 , Invoice_Collection_Coordinator VARCHAR(MAX) NULL
                 , Invoice_Collection_Owner VARCHAR(MAX) NULL
                 , Invoice_Collection_Queue_Id INT NOT NULL
                 , Client_Id INT NOT NULL
             );



        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Vendor_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                WHEN icav.VENDOR_NAME IS NOT NULL
                     AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                ELSE ucha.Account_Vendor_Name
            END AS Account_Vendor_Name
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                  WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                  ELSE ucha.Account_Type
              END AS Account_Vendor_Type
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                  WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                  ELSE ucha.Account_Vendor_Id
              END AS Account_Vendor_Id
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                ON icccm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Code st
                ON iccc.Status_Cd = st.Code_Id
                   AND  st.Code_Value = 'Close'
            INNER JOIN dbo.Code cpc
                ON cpc.Code_Id = icac.Chase_Priority_Cd
            INNER JOIN Core.Client_Hier_Account ucha
                ON icac.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN Core.Client_Hier_Account scha
                ON ucha.Meter_Id = scha.Meter_Id
                   AND  scha.Account_Type = 'Supplier'
                   AND  icac.Invoice_Collection_Service_End_Dt BETWEEN scha.Supplier_Account_begin_Dt
                                                               AND     scha.Supplier_Account_End_Dt
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
                            INNER JOIN dbo.ENTITY e
                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                   AND  e.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v
                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
                ON asbv.Account_Id = ucha.Account_Id
                   AND  icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt
                                                               AND     asbv.Billing_End_Dt
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
                            INNER JOIN dbo.ENTITY e1
                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                   AND  e1.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v1
                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
                ON asbv1.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd)
                                            OR  (@Source_Type_ETL = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Partner = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Mail_Redirect = aics.Invoice_Source_Type_Cd)
                                            OR  (@source_type_online = aics.Invoice_Source_Type_Cd))
                            INNER JOIN dbo.Vendor_Contact_Map vcm
                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                            INNER JOIN dbo.VENDOR icav
                                ON icav.VENDOR_ID = vcm.VENDOR_ID)
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            ucha.Account_Type = 'Utility'
            AND (   (   @Date_Type = 'Invoice dates'
                        AND (   @Start_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                          AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  @End_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                            AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  icq.Collection_Start_Dt BETWEEN @Start_Dt
                                                            AND     @End_Dt
                                OR  icq.Collection_End_Dt BETWEEN @Start_Dt
                                                          AND     @End_Dt))
                    OR  (   @Date_Type = 'Chase dates'
                            AND (CAST(iccc.Created_Ts AS DATE) BETWEEN @Start_Dt
                                                               AND     @End_Dt))
                    OR  (   @Date_Type = 'Raise dates'
                            AND (CAST(icq.Created_Ts AS DATE) BETWEEN @Start_Dt
                                                              AND     @End_Dt)))
        GROUP BY
            CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                WHEN icav.VENDOR_NAME IS NOT NULL
                     AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                ELSE ucha.Account_Vendor_Name
            END
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                  WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                  ELSE ucha.Account_Type
              END
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                  WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                  ELSE ucha.Account_Vendor_Id
              END
            , icac.Invoice_Collection_Account_Config_Id;



        INSERT INTO #Invoices_Not_Chased
             (
                 Client
                 , Site
                 , Country
                 , State_Name
                 , Vendor_Name
                 , Display_Account_Number
                 , Alternative_Account_Number
                 , Commodity
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Source
                 , Raised_date
                 , Invoice_status
                 , Issue_Status
                 , Invoice_Collection_Chase_Log_Id
                 , Chase_Type
                 , Chased_Date
                 , Chase_method
                 , Chased_By
                 , Person_chased
                 , Contact_Source
                 , Invoice_Chase_Comment
                 , Status_value
                 , Received_date
                 , Exception_date
                 , Exception_comments
                 , Invoice_Collection_Officer
                 , Invoice_Collection_Coordinator
                 , Invoice_Collection_Owner
                 , Invoice_Collection_Queue_Id
                 , Client_Id
             )
        SELECT
            ch.Client_Name AS Client
            , ch.Site_name AS SITE
            , ch.Country_Name AS Country
            , ch.State_Name
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) AS Vendor_Name
            , cha.Display_Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number AS Alternative_Account_Number
            , CASE WHEN LEN(commo.Commodity_Names) > 0 THEN LEFT(commo.Commodity_Names, LEN(commo.Commodity_Names) - 1)
                  ELSE commo.Commodity_Names
              END AS Commodity
            , REPLACE(CONVERT(NVARCHAR, icccm.Collection_Start_Dt, 106), ' ', '-') AS Collection_Start_Dt
            , REPLACE(CONVERT(NVARCHAR, icccm.Collection_End_Dt, 106), ' ', '-') AS Collection_End_Dt
            , CASE WHEN (   cics.Code_Value IS NULL
                            AND cicst.Code_Value IS NULL
                            AND cicsm.Code_Value IS NULL) THEN NULL
                  ELSE
                      ISNULL(cics.Code_Value, '') + ' - ' + ISNULL(cicst.Code_Value, '')
                      + CASE WHEN ISNULL(cicst.Code_Value, '')NOT IN ( 'Partner', 'Online', 'ETL', 'Mail Redirect' ) THEN
                                 ' - ' + ISNULL(cicsm.Code_Value, '')
                            ELSE ''
                        END
              END AS Invoice_Source
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-') AS Raised_date
            , icqs.Code_Value AS Invoice_status
            , icims.Code_Value AS Issue_Status
            , iccc.Invoice_Collection_Chase_Log_Id
            , CASE WHEN MAX(icccmin.Invoice_Collection_Chase_Log_Id) = iccc.Invoice_Collection_Chase_Log_Id THEN
                       'Chase'
                  ELSE 'Rechase'
              END AS Chase_Type
            , REPLACE(CONVERT(NVARCHAR, iccc.Created_Ts, 106), ' ', '-') AS Chased_Date
            , mc.Code_Value AS Chase_method
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Chased_By
            , pc.First_Name + ' ' + pc.Last_Name AS Person_chased
            , ISNULL(cics.Code_Value, 'unknown') AS Contact_Source
            , iccc.Invoice_Chase_Comment
            , st.Code_Value AS status_value
            , CASE WHEN icqs.Code_Value = 'Received' THEN REPLACE(CONVERT(NVARCHAR, icq.Last_Change_Ts, 106), ' ', '-')
              END AS Received_date
            , CASE WHEN icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL THEN
                       REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
              END AS Exception_date
            , CASE WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                       LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 1)
                  ELSE Comments.Comment_desc_comma
              END AS Exception_comments
            , ico.FIRST_NAME + ' ' + ico.LAST_NAME AS Invoice_Collection_Officer
            , icco.FIRST_NAME + ' ' + icco.LAST_NAME AS Invoice_Collection_Coordinator
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME AS Invoice_Collection_Owner
            , icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Code icqs
                ON icq.Status_Cd = icqs.Code_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log_Queue_Map icccm
                ON icccm.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Chase_Log iccc
                ON icccm.Invoice_Collection_Chase_Log_Id = iccc.Invoice_Collection_Chase_Log_Id
            INNER JOIN dbo.Code st
                ON iccc.Status_Cd = st.Code_Id
                   AND  st.Code_Value = 'Close'
            CROSS APPLY
        (   SELECT
                com.Commodity_Name + ','
            FROM
                dbo.Invoice_Collection_Queue icq1
                INNER JOIN dbo.Invoice_Collection_Account_Config icac2
                    ON icq1.Invoice_Collection_Account_Config_Id = icac2.Invoice_Collection_Account_Config_Id
                INNER JOIN Core.Client_Hier_Account cha2
                    ON icac2.Account_Id = cha2.Account_Id
                INNER JOIN dbo.Commodity com
                    ON cha2.Commodity_Id = com.Commodity_Id
            WHERE
                icq.Invoice_Collection_Queue_Id = icq1.Invoice_Collection_Queue_Id
            GROUP BY
                com.Commodity_Name
            FOR XML PATH('')) commo(Commodity_Names)
            LEFT JOIN dbo.USER_INFO ico
                ON icac.Invoice_Collection_Officer_User_Id = ico.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO icowner
                ON icac.Invoice_Collection_Owner_User_Id = icowner.USER_INFO_ID
            INNER JOIN dbo.Invoice_Collection_Client_Config iccco
                ON ch.Client_Id = iccco.Client_Id
            LEFT JOIN dbo.USER_INFO icco
                ON iccco.Invoice_Collection_Coordinator_User_Id = icco.USER_INFO_ID
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                   AND  aics.Is_Primary = 1
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd)
                                            OR  (@Source_Type_ETL = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Partner = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Mail_Redirect = aics.Invoice_Source_Type_Cd)
                                            OR  (@source_type_online = aics.Invoice_Source_Type_Cd)))
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Code cics
                ON cics.Code_Id = aics.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code cicst
                ON cicst.Code_Id = aics.Invoice_Source_Type_Cd
            LEFT OUTER JOIN dbo.Code cicsm
                ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd
            LEFT OUTER JOIN dbo.Code cci
                ON cci.Code_Id = ci.Contact_Level_Cd
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN
            (   SELECT
                    Invoice_Collection_Queue_Id
                    , MIN(icclqmp.Invoice_Collection_Chase_Log_Id) AS Invoice_Collection_Chase_Log_Id
                    , MIN(Collection_Start_Dt) AS Collection_Start_Dt
                    , MIN(Collection_End_Dt) AS Collection_End_Dt
                FROM
                    dbo.Invoice_Collection_Chase_Log_Queue_Map icclqmp
                    INNER JOIN dbo.Invoice_Collection_Chase_Log iccl
                        ON icclqmp.Invoice_Collection_Chase_Log_Id = iccl.Invoice_Collection_Chase_Log_Id
                    INNER JOIN dbo.Code icst
                        ON iccl.Status_Cd = icst.Code_Id
                WHERE
                    icst.Code_Value = 'Close'
                GROUP BY
                    Invoice_Collection_Queue_Id) icccmin
                ON icccmin.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Code mc
                ON iccc.Invoice_Collection_Method_Of_Contact_Cd = mc.Code_Id
            LEFT JOIN dbo.USER_INFO ui
                ON iccc.Created_User_Id = ui.USER_INFO_ID
            LEFT JOIN dbo.Contact_Info pc
                ON iccc.Contact_Info_Id = pc.Contact_Info_Id
            LEFT OUTER JOIN
            (   SELECT
                    ici.Invoice_Collection_Queue_Id
                    , MAX(ici.Invoice_Collection_Issue_Log_Id) AS Invoice_Collection_Issue_Log_Id
                FROM
                    dbo.Invoice_Collection_Issue_Log ici
                GROUP BY
                    ici.Invoice_Collection_Queue_Id) ici
                ON ici.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            LEFT JOIN dbo.Invoice_Collection_Issue_Log icim
                ON ici.Invoice_Collection_Issue_Log_Id = icim.Invoice_Collection_Issue_Log_Id
            LEFT JOIN dbo.Code icims
                ON icim.Issue_Status_Cd = icims.Code_Id
            CROSS APPLY
        (   SELECT
                icecom.Comment_Desc + ' # ' + ccom.Code_Value + ','
            FROM
                dbo.Invoice_Collection_Exception_Comment icecom
                INNER JOIN dbo.Code ccom
                    ON icecom.Comment_Type_Cd = ccom.Code_Id
            WHERE
                icecom.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            GROUP BY
                icecom.Comment_Desc
                , ccom.Code_Value
            FOR XML PATH('')) Comments(Comment_desc_comma)
        WHERE
            (   @Client_Id = -1
                OR  ch.Client_Id = @Client_Id)
            AND (   @Client_Hier_Id = -1
                    OR  ch.Client_Hier_Id = @Client_Hier_Id)
            AND (   @Country_Id = -1
                    OR  ch.Country_Id = @Country_Id)
            AND (   @Invoice_Collection_Coordinator_Id = -1
                    OR  iccco.Invoice_Collection_Coordinator_User_Id = @Invoice_Collection_Coordinator_Id)
            AND (   @Invoice_Collection_Officer_Id = -1
                    OR  icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id)
            AND (   @Invoice_Collection_Owner_Id = -1
                    OR  icac.Invoice_Collection_Owner_User_Id = @Invoice_Collection_Owner_Id)
            AND (   @Vendor_Id = -1
                    OR  vd.Account_Vendor_Id = @Vendor_Id)
            AND (   @Is_Historic = 1
                    OR  icac.Is_Chase_Activated = 1)
            AND (   (   @Date_Type = 'Invoice dates'
                        AND (   iccc.Invoice_Collection_Chase_Log_Id IS NOT NULL
                                OR  icq.Is_Manual = 1
                                OR  icq.Is_Locked = 1
                                OR  ici.Invoice_Collection_Queue_Id IS NOT NULL)
                        AND (   @Start_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                          AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  @End_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                            AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  icq.Collection_Start_Dt BETWEEN @Start_Dt
                                                            AND     @End_Dt
                                OR  icq.Collection_End_Dt BETWEEN @Start_Dt
                                                          AND     @End_Dt))
                    OR  (   @Date_Type = 'Chase dates'
                            AND iccc.Invoice_Collection_Chase_Log_Id IS NOT NULL
                            AND (CAST(iccc.Created_Ts AS DATE) BETWEEN @Start_Dt
                                                               AND     @End_Dt))
                    OR  (   @Date_Type = 'Raise dates'
                            AND iccc.Invoice_Collection_Chase_Log_Id IS NOT NULL
                            AND (CAST(icq.Created_Ts AS DATE) BETWEEN @Start_Dt
                                                              AND     @End_Dt)))
            AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
            AND icqs.Code_Value <> 'Archived'
        GROUP BY
            ch.Client_Name
            , ch.Site_name
            , ch.Country_Name
            , ch.State_Name
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
            , cha.Display_Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number
            , CASE WHEN LEN(commo.Commodity_Names) > 0 THEN LEFT(commo.Commodity_Names, LEN(commo.Commodity_Names) - 1)
                  ELSE commo.Commodity_Names
              END
            , REPLACE(CONVERT(NVARCHAR, icccm.Collection_Start_Dt, 106), ' ', '-')
            , REPLACE(CONVERT(NVARCHAR, icccm.Collection_End_Dt, 106), ' ', '-')
            , CASE WHEN (   cics.Code_Value IS NULL
                            AND cicst.Code_Value IS NULL
                            AND cicsm.Code_Value IS NULL) THEN NULL
                  ELSE
                      ISNULL(cics.Code_Value, '') + ' - ' + ISNULL(cicst.Code_Value, '')
                      + CASE WHEN ISNULL(cicst.Code_Value, '')NOT IN ( 'Partner', 'Online', 'ETL', 'Mail Redirect' ) THEN
                                 ' - ' + ISNULL(cicsm.Code_Value, '')
                            ELSE ''
                        END
              END
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
            , icqs.Code_Value
            , icims.Code_Value
            , iccc.Invoice_Collection_Chase_Log_Id
            , REPLACE(CONVERT(NVARCHAR, iccc.Created_Ts, 106), ' ', '-')
            , mc.Code_Value
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME
            , pc.First_Name + ' ' + pc.Last_Name
            , ISNULL(cics.Code_Value, 'unknown')
            , iccc.Invoice_Chase_Comment
            , st.Code_Value
            , CASE WHEN icqs.Code_Value = 'Received' THEN REPLACE(CONVERT(NVARCHAR, icq.Last_Change_Ts, 106), ' ', '-')
              END
            , CASE WHEN icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL THEN
                       REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
              END
            , ico.FIRST_NAME + ' ' + ico.LAST_NAME
            , icco.FIRST_NAME + ' ' + icco.LAST_NAME
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME
            , CASE WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                       LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 1)
                  ELSE Comments.Comment_desc_comma
              END
            , icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
        ORDER BY
            ch.Client_Name
            , ch.Site_name
            , ch.Country_Name
            , cha.Display_Account_Number
            , REPLACE(CONVERT(NVARCHAR, icccm.Collection_Start_Dt, 106), ' ', '-')
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
            , REPLACE(CONVERT(NVARCHAR, iccc.Created_Ts, 106), ' ', '-');



        INSERT INTO #Invoices_Not_Chased
             (
                 Client
                 , Site
                 , Country
                 , State_Name
                 , Vendor_Name
                 , Display_Account_Number
                 , Alternative_Account_Number
                 , Commodity
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Source
                 , Raised_date
                 , Invoice_status
                 , Issue_Status
                 , Invoice_Collection_Chase_Log_Id
                 , Chase_Type
                 , Chased_Date
                 , Chase_method
                 , Chased_By
                 , Person_chased
                 , Contact_Source
                 , Invoice_Chase_Comment
                 , Status_value
                 , Received_date
                 , Exception_date
                 , Exception_comments
                 , Invoice_Collection_Officer
                 , Invoice_Collection_Coordinator
                 , Invoice_Collection_Owner
                 , Invoice_Collection_Queue_Id
                 , Client_Id
             )
        SELECT
            ch.Client_Name AS Client
            , ch.Site_name AS SITE
            , ch.Country_Name AS Country
            , ch.State_Name
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name) AS Vendor_Name
            , cha.Display_Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number AS Alternative_Account_Number
            , CASE WHEN LEN(commo.Commodity_Names) > 0 THEN LEFT(commo.Commodity_Names, LEN(commo.Commodity_Names) - 1)
                  ELSE commo.Commodity_Names
              END AS Commodity
            , REPLACE(CONVERT(NVARCHAR, icq.Collection_Start_Dt, 106), ' ', '-') AS Collection_Start_Dt
            , REPLACE(CONVERT(NVARCHAR, icq.Collection_End_Dt, 106), ' ', '-') AS Collection_End_Dt
            , CASE WHEN (   cics.Code_Value IS NULL
                            AND cicst.Code_Value IS NULL
                            AND cicsm.Code_Value IS NULL) THEN NULL
                  ELSE
                      ISNULL(cics.Code_Value, '') + ' - ' + ISNULL(cicst.Code_Value, '')
                      + CASE WHEN ISNULL(cicst.Code_Value, '')NOT IN ( 'Partner', 'Online', 'ETL', 'Mail Redirect' ) THEN
                                 ' - ' + ISNULL(cicsm.Code_Value, '')
                            ELSE ''
                        END
              END AS Invoice_Source
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-') AS Raised_date
            , icqs.Code_Value AS Invoice_status
            , t.Issue_Status AS Issue_Status
            , t.Invoice_Collection_Chase_Log_Id
            , t.Chase_Type
            , t.Chased_Date
            , t.Chase_method AS Chase_method
            , t.Chased_By AS Chased_By
            , t.Person_chased AS Person_chased
            , ISNULL(cics.Code_Value, 'unknown') AS Contact_Source
            , t.Invoice_Chase_Comment
            , t.Status_value AS Status_value
            , CASE WHEN icqs.Code_Value = 'Received' THEN REPLACE(CONVERT(NVARCHAR, icq.Last_Change_Ts, 106), ' ', '-')
              END AS Received_date
            , CASE WHEN icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL THEN
                       REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
              END AS Exception_date
            , CASE WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                       LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 1)
                  ELSE Comments.Comment_desc_comma
              END AS Exception_comments
            , ico.FIRST_NAME + ' ' + ico.LAST_NAME AS Invoice_Collection_Officer
            , icco.FIRST_NAME + ' ' + icco.LAST_NAME AS Invoice_Collection_Coordinator
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME AS Invoice_Collection_Owner
            , icq.Invoice_Collection_Queue_Id
            , ch.Client_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN dbo.Code icqs
                ON icq.Status_Cd = icqs.Code_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = icac.Account_Id
            INNER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN #Invoices_Not_Chased t
                ON t.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            CROSS APPLY
        (   SELECT
                com.Commodity_Name + ','
            FROM
                dbo.Invoice_Collection_Queue icq1
                INNER JOIN dbo.Invoice_Collection_Account_Config icac2
                    ON icq1.Invoice_Collection_Account_Config_Id = icac2.Invoice_Collection_Account_Config_Id
                INNER JOIN Core.Client_Hier_Account cha2
                    ON icac2.Account_Id = cha2.Account_Id
                INNER JOIN dbo.Commodity com
                    ON cha2.Commodity_Id = com.Commodity_Id
            WHERE
                icq.Invoice_Collection_Queue_Id = icq1.Invoice_Collection_Queue_Id
            GROUP BY
                com.Commodity_Name
            FOR XML PATH('')) commo(Commodity_Names)
            LEFT JOIN dbo.USER_INFO ico
                ON icac.Invoice_Collection_Officer_User_Id = ico.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO icowner
                ON icac.Invoice_Collection_Owner_User_Id = icowner.USER_INFO_ID
            INNER JOIN dbo.Invoice_Collection_Client_Config iccco
                ON ch.Client_Id = iccco.Client_Id
            LEFT JOIN dbo.USER_INFO icco
                ON iccco.Invoice_Collection_Coordinator_User_Id = icco.USER_INFO_ID
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                                   AND  aics.Is_Primary = 1
                            INNER JOIN dbo.Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd)
                                            OR  (@Source_Type_ETL = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Partner = aics.Invoice_Source_Type_Cd)
                                            OR  (@Source_Type_Mail_Redirect = aics.Invoice_Source_Type_Cd)
                                            OR  (@source_type_online = aics.Invoice_Source_Type_Cd)))
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT OUTER JOIN dbo.Code cics
                ON cics.Code_Id = aics.Invoice_Collection_Source_Cd
            LEFT OUTER JOIN dbo.Code cicst
                ON cicst.Code_Id = aics.Invoice_Source_Type_Cd
            LEFT OUTER JOIN dbo.Code cicsm
                ON cicsm.Code_Id = aics.Invoice_Source_Method_of_Contact_Cd
            LEFT OUTER JOIN dbo.Code cci
                ON cci.Code_Id = ci.Contact_Level_Cd
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            CROSS APPLY
        (   SELECT
                icecom.Comment_Desc + ' # ' + ccom.Code_Value + ','
            FROM
                dbo.Invoice_Collection_Exception_Comment icecom
                INNER JOIN dbo.Code ccom
                    ON icecom.Comment_Type_Cd = ccom.Code_Id
            WHERE
                icecom.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            GROUP BY
                icecom.Comment_Desc
                , ccom.Code_Value
            FOR XML PATH('')) Comments(Comment_desc_comma)
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Invoices_Not_Chased t
                           WHERE
                                t.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id)
            AND (   @Client_Id = -1
                    OR  ch.Client_Id = @Client_Id)
            AND (   @Client_Hier_Id = -1
                    OR  ch.Client_Hier_Id = @Client_Hier_Id)
            AND (   @Country_Id = -1
                    OR  ch.Country_Id = @Country_Id)
            AND (   @Invoice_Collection_Coordinator_Id = -1
                    OR  iccco.Invoice_Collection_Coordinator_User_Id = @Invoice_Collection_Coordinator_Id)
            AND (   @Invoice_Collection_Officer_Id = -1
                    OR  icac.Invoice_Collection_Officer_User_Id = @Invoice_Collection_Officer_Id)
            AND (   @Invoice_Collection_Owner_Id = -1
                    OR  icac.Invoice_Collection_Owner_User_Id = @Invoice_Collection_Owner_Id)
            AND (   @Vendor_Id = -1
                    OR  vd.Account_Vendor_Id = @Vendor_Id)
            AND (   @Is_Historic = 1
                    OR  icac.Is_Chase_Activated = 1)
            AND (   (   @Date_Type = 'Invoice dates'
                        AND (   @Start_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                          AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  @End_Dt BETWEEN CAST(icq.Collection_Start_Dt AS DATE)
                                            AND     CAST(icq.Collection_End_Dt AS DATE)
                                OR  icq.Collection_Start_Dt BETWEEN @Start_Dt
                                                            AND     @End_Dt
                                OR  icq.Collection_End_Dt BETWEEN @Start_Dt
                                                          AND     @End_Dt))
                    OR  (   @Date_Type = 'Raise dates'
                            AND (icq.Created_Ts BETWEEN @Start_Dt
                                                AND     @End_Dt)))
            AND ch.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
            AND icqs.Code_Value = 'Open'
        GROUP BY
            ch.Client_Name
            , ch.Site_name
            , ch.Country_Name
            , ch.State_Name
            , ISNULL(vd.Account_Vendor_Name, cha.Account_Vendor_Name)
            , cha.Display_Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number
            , CASE WHEN LEN(commo.Commodity_Names) > 0 THEN LEFT(commo.Commodity_Names, LEN(commo.Commodity_Names) - 1)
                  ELSE commo.Commodity_Names
              END
            , REPLACE(CONVERT(NVARCHAR, icq.Collection_Start_Dt, 106), ' ', '-')
            , REPLACE(CONVERT(NVARCHAR, icq.Collection_End_Dt, 106), ' ', '-')
            , CASE WHEN (   cics.Code_Value IS NULL
                            AND cicst.Code_Value IS NULL
                            AND cicsm.Code_Value IS NULL) THEN NULL
                  ELSE
                      ISNULL(cics.Code_Value, '') + ' - ' + ISNULL(cicst.Code_Value, '')
                      + CASE WHEN ISNULL(cicst.Code_Value, '')NOT IN ( 'Partner', 'Online', 'ETL', 'Mail Redirect' ) THEN
                                 ' - ' + ISNULL(cicsm.Code_Value, '')
                            ELSE ''
                        END
              END
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
            , icqs.Code_Value
            , ISNULL(cics.Code_Value, 'unknown')
            , CASE WHEN icqs.Code_Value = 'Received' THEN REPLACE(CONVERT(NVARCHAR, icq.Last_Change_Ts, 106), ' ', '-')
              END
            , CASE WHEN icq.Invoice_Collection_Exception_Type_Cd IS NOT NULL THEN
                       REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-')
              END
            , ico.FIRST_NAME + ' ' + ico.LAST_NAME
            , icco.FIRST_NAME + ' ' + icco.LAST_NAME
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME
            , CASE WHEN LEN(Comments.Comment_desc_comma) > 0 THEN
                       LEFT(Comments.Comment_desc_comma, LEN(Comments.Comment_desc_comma) - 1)
                  ELSE Comments.Comment_desc_comma
              END
            , icq.Invoice_Collection_Queue_Id
            , t.Invoice_Collection_Chase_Log_Id
            , t.Chase_Type
            , t.Chase_method
            , t.Chased_By
            , t.Chased_Date
            , t.Person_chased
            , t.Invoice_Chase_Comment
            , t.Issue_Status
            , t.Status_value
            , ch.Client_Id
        ORDER BY
            ch.Client_Name
            , ch.Site_name
            , ch.Country_Name
            , cha.Display_Account_Number
            , REPLACE(CONVERT(NVARCHAR, icq.Collection_Start_Dt, 106), ' ', '-')
            , REPLACE(CONVERT(NVARCHAR, icq.Created_Ts, 106), ' ', '-');

        SELECT
            Client
            , Site
            , Country
            , State_Name
            , Vendor_Name
            , Display_Account_Number
            , Alternative_Account_Number
            , Commodity
            , Collection_Start_Dt
            , Collection_End_Dt
            , Invoice_Source
            , Raised_date
            , Invoice_status
            , Issue_Status
            , Invoice_Collection_Chase_Log_Id
            , Chase_Type
            , Chased_Date
            , Chase_method
            , Chased_By
            , Person_chased
            , Contact_Source
            , Invoice_Chase_Comment
            , Status_value
            , Received_date
            , Exception_date
            , Exception_comments
            , Invoice_Collection_Officer
            , Invoice_Collection_Coordinator
            , Invoice_Collection_Owner
            , Invoice_Collection_Queue_Id
            , t.Client_Id
            , ui.User_List_CEM CEM
            , ui.User_List_CEA CEA
            , ui.User_List_CSA CSA
        FROM
            #Invoices_Not_Chased t
            JOIN #UserInfo ui
                ON ui.Client_Id = t.Client_Id
        GROUP BY
            Client
            , Site
            , Country
            , State_Name
            , Vendor_Name
            , Display_Account_Number
            , Alternative_Account_Number
            , Commodity
            , Collection_Start_Dt
            , Collection_End_Dt
            , Invoice_Source
            , Raised_date
            , Invoice_status
            , Issue_Status
            , Invoice_Collection_Chase_Log_Id
            , Chase_Type
            , Chased_Date
            , Chase_method
            , Chased_By
            , Person_chased
            , Contact_Source
            , Invoice_Chase_Comment
            , Status_value
            , Received_date
            , Exception_date
            , Exception_comments
            , Invoice_Collection_Officer
            , Invoice_Collection_Coordinator
            , Invoice_Collection_Owner
            , Invoice_Collection_Queue_Id
            , t.Client_Id
            , ui.User_List_CEM
            , ui.User_List_CEA
            , ui.User_List_CSA;




        DROP TABLE #Invoices_Not_Chased;
        DROP TABLE #Vendor_Dtls;
        DROP TABLE #UserInfo;








    END;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_IC_Chase] TO [CBMS_SSRS_Reports]
GO
