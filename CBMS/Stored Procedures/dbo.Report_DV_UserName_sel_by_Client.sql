SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_DV_UserName_sel_by_Client

DESCRIPTION:

 This procedure is to fetch all user\users of Client\Clients. 

INPUT PARAMETERS:
Name				DataType		Default	Description
----------------------------------------------------------
@Client_ID			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
----------------------------------------------------------

USAGE EXAMPLES:
----------------------------------------------------------
EXEC Report_DV_UserName_sel_by_Client 1043

AUTHOR INITIALS:
	Initials	Name
----------------------------------------------------------
	SSR			Sharad srivastava

MODIFICATIONS:
	Initials	Date		Modification
----------------------------------------------------------
	SSR        	06/21/2010	Created
	SSR			07/16/2010	Added Condition in where Clause to filter only Users which are not moved to history
******/  
CREATE PROC [dbo].[Report_DV_UserName_sel_by_Client] @Client_ID INT
AS 
    BEGIN 

        SET NOCOUNT ON 

        
        SELECT
            ufo.USER_INFO_ID
          , ufo.USERNAME
          , ufo.FIRST_NAME
          , ufo.LAST_NAME
          , ufo.FIRST_NAME + SPACE(1) + ufo.LAST_NAME + ' - ' + ' ( '
            + ufo.USERNAME + ' ) ' AS NAME
          , c.CLIENT_ID
          , c.CLIENT_NAME
        FROM
            USER_INFO ufo
            JOIN CLIENT c
                ON ufo.CLIENT_ID = c.CLIENT_ID
        WHERE
            c.client_id = @Client_ID
            AND ufo.is_history = 0
            AND ufo.access_level = 1
        GROUP BY
            ufo.USER_INFO_ID
          , ufo.USERNAME
          , ufo.FIRST_NAME
          , ufo.LAST_NAME
          , c.CLIENT_ID
          , c.CLIENT_NAME
        ORDER BY
            NAME 	
				                			                

    END


GO
GRANT EXECUTE ON  [dbo].[Report_DV_UserName_sel_by_Client] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DV_UserName_sel_by_Client] TO [CBMSApplication]
GO
