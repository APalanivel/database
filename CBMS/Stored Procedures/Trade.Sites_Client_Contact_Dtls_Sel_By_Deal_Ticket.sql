SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
Name:                        
        Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket                      
                        
Description:                        
        To get site's hedge configurations  
                        
Input Parameters:                        
    Name    DataType        Default     Description                          
--------------------------------------------------------------------------------  
	@Client_Id   INT  
    @Commodity_Id  INT  
    @Hedge_Type  INT  
    @Start_Dt DATE  
    @End_Dt  DATE  
    @Contract_Id  VARCHAR(MAX)  
    @Site_Id   INT    NULL  
                        
 Output Parameters:                              
 Name            Datatype        Default  Description                              
--------------------------------------------------------------------------------  
     
                      
Usage Examples:                            
--------------------------------------------------------------------------------  
 
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  291,1
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  291,1,'18508'
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  291,1,'18520'
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  288,1
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  132248,73
	EXEC Trade.Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket  132224,12
  
Author Initials:                        
    Initials    Name                        
--------------------------------------------------------------------------------  
    RR          Raghu Reddy     
                         
 Modifications:                        
    Initials	Date        Modification                        
--------------------------------------------------------------------------------  
	RR          24-01-2019  Created GRM
	RR			28-08-2019	GRM-1493 - Changed CBMS_Image_Id reference from Trade.Deal_Ticket_Client_Contact to 
							Trade.Deal_Ticket_Client_Hier_Workflow_Status table
                       
******/
CREATE PROCEDURE [Trade].[Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket]
    (
        @Deal_Ticket_Id INT
        , @Workflow_Task_Status_Map_Id INT
        , @Client_Hier_Id VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Task_Name NVARCHAR(255);

        CREATE TABLE #Configs
             (
                 Client_Hier_Id INT
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Last_Updated_Ts DATETIME
                 , Email_Address NVARCHAR(150)
                 , Approval_Cbms_Image_Id INT
             );

        CREATE TABLE #Sites
             (
                 Client_Id INT
                 , Client_Hier_Id INT
                 , Site_Id INT
                 , Site_name VARCHAR(200)
                 , Site_Not_Managed BIT
             );

        SELECT
            @Task_Name = wt.Task_Name
        FROM
            Trade.Workflow_Task wt
        WHERE
            wt.Workflow_Task_Id = @Workflow_Task_Status_Map_Id;

        ---------Already mapped
        INSERT INTO #Sites
             (
                 Client_Id
                 , Client_Hier_Id
                 , Site_Id
                 , Site_name
                 , Site_Not_Managed
             )
        SELECT
            ch.Client_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , ch.Site_Not_Managed
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
        GROUP BY
            ch.Client_Id
            , ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , ch.Site_Not_Managed;

        INSERT INTO #Configs
             (
                 Client_Hier_Id
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Last_Updated_Ts
                 , Email_Address
                 , Approval_Cbms_Image_Id
             )
        SELECT
            ch.Client_Hier_Id
            , dtcc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , dtcc.Last_Change_Ts
            , ci.Email_Address
            , dtcc.Approval_Cbms_Image_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Contact dtcc
                ON dt.Deal_Ticket_Id = dtcc.Deal_Ticket_Id
            INNER JOIN Trade.Deal_Ticket_Client_Contact_Client_Hier dtcch
                ON dtcc.Deal_Ticket_Client_Contact_Id = dtcch.Deal_Ticket_Client_Contact_Id
            INNER JOIN Core.Client_Hier ch
                ON dtcch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Contact_Info ci
                ON dtcc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id;


        ----------------------To get site own config  
        INSERT INTO #Configs
             (
                 Client_Hier_Id
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Last_Updated_Ts
                 , Email_Address
                 , Approval_Cbms_Image_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ci.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , chhc.Last_Updated_Ts
            , ci.Email_Address
            , NULL
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
                   AND  dt.Commodity_Id = chob.Commodity_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                   AND  (   dt.Hedge_Start_Dt BETWEEN chhc.Config_Start_Dt
                                              AND     chhc.Config_End_Dt
                            OR  dt.Hedge_End_Dt BETWEEN chhc.Config_Start_Dt
                                                AND     chhc.Config_End_Dt
                            OR  chhc.Config_Start_Dt BETWEEN chhc.Config_Start_Dt
                                                     AND     chhc.Config_End_Dt
                            OR  chhc.Config_End_Dt BETWEEN chhc.Config_Start_Dt
                                                   AND     chhc.Config_End_Dt)
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN(dbo.Contact_Info ci
                       INNER JOIN dbo.Code cd
                           ON ci.Contact_Type_Cd = cd.Code_Id
                       INNER JOIN dbo.Codeset cs
                           ON cd.Codeset_Id = cs.Codeset_Id
                              AND   cs.Codeset_Name = 'ContactType'
                              AND   cd.Code_Value = 'RM Client Contact')
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') sit
                                   WHERE
                                        CAST(sit.Segments AS INT) = ch.Client_Hier_Id))
            AND NOT EXISTS (SELECT  1 FROM  #Configs chcc WHERE ch.Client_Hier_Id = chcc.Client_Hier_Id);

        ----------------------To get default config                         
        INSERT INTO #Configs
             (
                 Client_Hier_Id
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Last_Updated_Ts
                 , Email_Address
                 , Approval_Cbms_Image_Id
             )
        SELECT
            ch.Client_Hier_Id
            , ci.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , chhc.Last_Updated_Ts
            , ci.Email_Address
            , NULL
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dt.Deal_Ticket_Id = dtch.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  dt.Commodity_Id = chob.Commodity_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
                   AND  (   dt.Hedge_Start_Dt BETWEEN chhc.Config_Start_Dt
                                              AND     chhc.Config_End_Dt
                            OR  dt.Hedge_End_Dt BETWEEN chhc.Config_Start_Dt
                                                AND     chhc.Config_End_Dt
                            OR  chhc.Config_Start_Dt BETWEEN chhc.Config_Start_Dt
                                                     AND     chhc.Config_End_Dt
                            OR  chhc.Config_End_Dt BETWEEN chhc.Config_Start_Dt
                                                   AND     chhc.Config_End_Dt)
            INNER JOIN dbo.ENTITY et
                ON et.ENTITY_ID = chhc.Hedge_Type_Id
            INNER JOIN(dbo.Contact_Info ci
                       INNER JOIN dbo.Code cd
                           ON ci.Contact_Type_Cd = cd.Code_Id
                       INNER JOIN dbo.Codeset cs
                           ON cd.Codeset_Id = cs.Codeset_Id
                              AND   cs.Codeset_Name = 'ContactType'
                              AND   cd.Code_Value = 'RM Client Contact')
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            dt.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chclient.Sitegroup_Id = 0
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                               WHERE
                                    siteob.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteob.Commodity_Id = chob.Commodity_Id)
            AND (   @Client_Hier_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Client_Hier_Id, ',') sit
                                   WHERE
                                        CAST(sit.Segments AS INT) = ch.Client_Hier_Id))
            AND NOT EXISTS (SELECT  1 FROM  #Configs chcc WHERE ch.Client_Hier_Id = chcc.Client_Hier_Id);

        SELECT
            ss.Client_Id
            , ss.Client_Hier_Id
            , ss.Site_Id
            , ss.Site_name
            , cnf.Client_Contact_Info_Id
            , cnf.Client_Contact
            , ws.Workflow_Status_Name
            , Email_Address
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN 'Canceled'
                  WHEN cawfts.Transition_Name IS NULL THEN 'Pending'
                  ELSE cawfts.Transition_Name
              END AS Task_Status
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.Last_Change_Ts
                  ELSE cachws.Last_Change_Ts
              END AS Task_Status_Ts
            , ISNULL(NULLIF(cnf.Approval_Cbms_Image_Id, 0), cachws.CBMS_Image_Id) AS CBMS_Image_Id
            , nl.Notification_Msg_Dtl_Id
            , cawfts.Transition_Name
        FROM
            #Sites ss
            LEFT JOIN #Configs cnf
                ON ss.Client_Hier_Id = cnf.Client_Hier_Id
            LEFT JOIN
            (   SELECT
                    c2.Client_Hier_Id
                    , MAX(c2.Last_Updated_Ts) AS Last_Updated_Ts
                FROM
                    #Configs c2
                GROUP BY
                    c2.Client_Hier_Id) c3
                ON ss.Client_Hier_Id = c3.Client_Hier_Id
                   AND  cnf.Last_Updated_Ts = c3.Last_Updated_Ts
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON ss.Client_Hier_Id = dtch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
            INNER JOIN Trade.Workflow_Status_Map wsm
                ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
            INNER JOIN Trade.Workflow_Status ws
                ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
            LEFT JOIN Trade.Workflow_Task_Status_Transition_Map catm
                ON catm.Descendant_Workflow_Status_Map_Id = chws.Workflow_Status_Map_Id
            LEFT JOIN Trade.Workflow_Task_Status_Map catsm
                ON chws.Workflow_Status_Map_Id = catsm.Workflow_Status_Map_Id
            LEFT JOIN Trade.Workflow_Transition cawt
                ON catm.Workflow_Transition_Id = cawt.Workflow_Transition_Id
            LEFT JOIN(Trade.Deal_Ticket_Client_Hier_Workflow_Status cachws
                      INNER JOIN Trade.Workflow_Task_Status_Transition_Map catsktm
                          ON catsktm.Descendant_Workflow_Status_Map_Id = cachws.Workflow_Status_Map_Id
                      INNER JOIN Trade.Workflow_Task_Status_Map catsksm
                          ON catsktm.Workflow_Task_Status_Map_Id = catsksm.Workflow_Task_Status_Map_Id
                             AND catsksm.Workflow_Task_Id = @Workflow_Task_Status_Map_Id
                             AND cachws.Workflow_Task_Id = catsksm.Workflow_Task_Id
                      INNER JOIN Trade.Workflow_Transition cawfts
                          ON catsktm.Workflow_Transition_Id = cawfts.Workflow_Transition_Id
                      LEFT JOIN Trade.Deal_Ticket_Client_Hier_Notification_Log nl
                          ON cachws.Deal_Ticket_Client_Hier_Workflow_Status_Id = nl.Deal_Ticket_Client_Hier_Workflow_Status_Id)
                ON cachws.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
        WHERE
            dtch.Deal_Ticket_Id = @Deal_Ticket_Id
            AND chws.Is_Active = 1
            AND (   @Task_Name = 'Request Client Approval'
                    OR  (   @Task_Name = 'Upload Client Approval'
                            AND ws.Workflow_Status_Name NOT IN ( 'Pending Internal Approval', 'Pending Client Approval' )))
        GROUP BY
            ss.Client_Id
            , ss.Client_Hier_Id
            , ss.Site_Id
            , ss.Site_name
            , cnf.Client_Contact_Info_Id
            , cnf.Client_Contact
            , ws.Workflow_Status_Name
            , cnf.Last_Updated_Ts
            , Email_Address
            , CASE WHEN ws.Workflow_Status_Name = 'Pending Client Approval' THEN NULL
                  ELSE chws.Last_Change_Ts
              END
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN 'Canceled'
                  WHEN cawfts.Transition_Name IS NULL THEN 'Pending'
                  ELSE cawfts.Transition_Name
              END
            , CASE WHEN ws.Workflow_Status_Name = 'Canceled' THEN chws.Last_Change_Ts
                  ELSE cachws.Last_Change_Ts
              END
            , ISNULL(NULLIF(cnf.Approval_Cbms_Image_Id, 0), cachws.CBMS_Image_Id)
            , nl.Notification_Msg_Dtl_Id
            , cawfts.Transition_Name;

        DROP TABLE #Sites;
        DROP TABLE #Configs;
    END;






GO
GRANT EXECUTE ON  [Trade].[Sites_Client_Contact_Dtls_Sel_By_Deal_Ticket] TO [CBMSApplication]
GO
