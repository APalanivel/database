SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.DELTOOL_SRRFPCLOSURE_GETFORCONTRACT_P 
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select distinct r.sr_rfp_closure_id
		, a.sr_rfp_id
	     from sr_rfp_closure r
	     join sr_rfp_account a on a.sr_rfp_account_id = r.sr_account_group_id 
	    where r.contract_id = @contract_id 
	      and r.is_bid_group = 0

	union all

	   select distinct r.sr_rfp_closure_id
		, a.sr_rfp_id
	     from sr_rfp_closure r
	     join sr_rfp_account a on a.sr_rfp_bid_group_id = r.sr_account_group_id 
	    where r.contract_id = @contract_id 
	      and r.is_bid_group = 0


END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_SRRFPCLOSURE_GETFORCONTRACT_P] TO [CBMSApplication]
GO
