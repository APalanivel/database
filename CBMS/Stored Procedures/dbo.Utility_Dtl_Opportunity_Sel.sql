SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Opportunity_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Vendor_ID				INT
	@Commodity_Id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.Utility_Dtl_Opportunity_Sel 30,290
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
	
******/

CREATE PROC [dbo].[Utility_Dtl_Opportunity_Sel]
      ( 
       @Vendor_ID INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
	
      SELECT
            utl_op.Utility_Dtl_Opportunity_Id
           ,qcm.Question_Commodity_Map_Id
           ,usq.Question_Label
           ,utl_op.Program_Dsc
           ,utl_op.Program_Req
           ,utl_op.Savings_Potential_Cd
           ,cd.Code_Value AS Savings_Potential_Value
           ,utl_op.Comment_ID
           ,cmt.Comment_Text
      FROM
            dbo.Question_Commodity_Map qcm
            INNER JOIN dbo.Utility_Summary_Question usq
                  ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
            INNER JOIN dbo.Utility_Summary_Section uss
                  ON uss.Utility_Summary_Section_Id = usq.Utility_Summary_Section_Id
            INNER JOIN VENDOR_COMMODITY_MAP vcm
                  ON vcm.COMMODITY_TYPE_ID = qcm.Commodity_Id
            LEFT JOIN dbo.Utility_Dtl_Opportunity utl_op
                  ON vcm.VENDOR_COMMODITY_MAP_ID = utl_op.VENDOR_COMMODITY_MAP_ID
                     AND qcm.Question_Commodity_Map_Id = utl_op.Question_Commodity_Map_Id
            LEFT JOIN dbo.Code cd
                  ON cd.Code_Id = utl_op.Savings_Potential_Cd
            LEFT JOIN dbo.Comment cmt
                  ON cmt.Comment_ID = utl_op.Comment_ID
      WHERE
            vcm.VENDOR_ID = @Vendor_ID
            AND vcm.COMMODITY_TYPE_ID = @Commodity_Id
            AND usq.Is_Active = 1
            AND uss.Section_Label = 'Other Rate Opportunities (not transport related)'
      ORDER BY
            usq.Display_Order
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Opportunity_Sel] TO [CBMSApplication]
GO
