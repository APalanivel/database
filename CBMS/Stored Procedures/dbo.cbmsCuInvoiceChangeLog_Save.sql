
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.cbmsCuInvoiceChangeLog_Save            
                        
 DESCRIPTION:                        
			   To save cu_invoice_change_log table                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@MyAccountId					INT
@cu_invoice_id					INT
@change_type_id					INT
@field_type_id					INT
@field_name						VARCHAR(200)
@previous_value					VARCHAR(200)	 NULL
@current_value					VARCHAR(200)	 NULL                     
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
    BEGIN TRAN EXEC cbmsCuInvoiceChangeLog_Save
       @MyAccountId =49
      ,@cu_invoice_id =124529
      ,@change_type_id =932
      ,@field_type_id =933
      ,@field_name ='Charge Value'
      ,@previous_value= '3399.5400000000000000'
      ,@current_value ='0'
      ,@Bucket_Number =NULL ROLLBACK      
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-07-22       Added Description Header
										Data Operations Enhancement Phase III,Added new columns Bucket_Number, Updated_User_Id in Insert statement.And
										 Removed Internal Sproc call and replaced with Select Statement.             
                       
******/  
CREATE   PROCEDURE [dbo].[cbmsCuInvoiceChangeLog_Save]
      ( 
       @MyAccountId INT
      ,@cu_invoice_id INT
      ,@change_type_id INT
      ,@field_type_id INT
      ,@field_name VARCHAR(200)
      ,@previous_value VARCHAR(200) = NULL
      ,@current_value VARCHAR(200) = NULL
      ,@Bucket_Number VARCHAR(50) = NULL )
AS 
BEGIN

      DECLARE
            @this_id INT
           ,@Change_Date DATETIME = GETDATE()

      SET NOCOUNT ON

      INSERT      INTO cu_invoice_change_log
                  ( 
                   cu_invoice_id
                  ,change_type_id
                  ,field_type_id
                  ,field_name
                  ,previous_value
                  ,current_value
                  ,change_date
                  ,Bucket_Number
                  ,Updated_User_Id )
      VALUES
                  ( 
                   @cu_invoice_id
                  ,@change_type_id
                  ,@field_type_id
                  ,@field_name
                  ,@previous_value
                  ,@current_value
                  ,@Change_Date
                  ,@Bucket_Number
                  ,@MyAccountId )

      SET @this_id = SCOPE_IDENTITY() 

      SELECT
            @this_id AS cu_invoice_change_log_id
           ,@cu_invoice_id AS cu_invoice_id
           ,@change_type_id AS change_type_id
           ,@field_type_id AS field_type_id
           ,@field_name AS field_name
           ,@previous_value AS previous_value
           ,@current_value AS current_value
           ,@Change_Date AS change_date
        

END



;
GO

GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceChangeLog_Save] TO [CBMSApplication]
GO
