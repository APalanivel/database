SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Meter_Cbms_Image_Map_Del]  
     
DESCRIPTION:

	It Deletes Meter CBMS Image Map for given Meter_Cbms_Image_Map_Id.
	This table contains the tax exemption document uploaded for the meter.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
@METER_CBMS_IMAGE_MAP_ID	INT

OUTPUT PARAMETERS :
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
	BEGIN TRAN
		EXEC dbo.Meter_Cbms_Image_Map_Del 11
	ROLLBACK TRAN
	
	SELECT TOP 10 * FROM meter_cbms_image_map

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			26-MAY-10	CREATED

*/

CREATE PROCEDURE dbo.Meter_Cbms_Image_Map_Del
    (
      @METER_CBMS_IMAGE_MAP_ID INT
    )
AS
BEGIN

    SET NOCOUNT ON;

    DELETE
		dbo.METER_CBMS_IMAGE_MAP
	WHERE
		METER_CBMS_IMAGE_MAP_ID = @METER_CBMS_IMAGE_MAP_ID

END
GO
GRANT EXECUTE ON  [dbo].[Meter_Cbms_Image_Map_Del] TO [CBMSApplication]
GO
