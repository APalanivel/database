SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                 
                 
NAME: Report_Get_Price_Index_Monthly_Avg_Dtl
    
DESCRIPTION:          
    
	This proc will return Monthly Averages of Daily Midpoints from PRICE_INDEX table

    
INPUT PARAMETERS:    
NAME   DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------     
    
OUTPUT PARAMETERS:    
NAME     DATATYPE DEFAULT  DESCRIPTION    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    exec dbo.Report_Get_Price_Index_Monthly_Avg_Dtl

AUTHOR INITIALS:    
INITIALS	NAME    
------------------------------------------------------------
 KVK		Vinay k

MODIFICATIONS    
INITIALS	DATE		MODIFICATION    
------------------------------------------------------------
	KVK		4/6/2016		created(requested by Ian Lawrence & Lynn Cox)
    
*/ 
CREATE PROCEDURE [dbo].[Report_Get_Price_Index_Monthly_Avg_Dtl]
AS
BEGIN
      SET NOCOUNT ON;
      SELECT
            T1.[PRICE_INDEX_ID]
           ,T1.[INDEX_ID]
           ,T1.[PRICING_POINT]
           ,T1.Index_Type
           ,T1.INDEX_VALUE
           ,CU.SYMBOL
           ,T1.INDEX_MONTH
      FROM
            ( SELECT
                  PI.[PRICE_INDEX_ID]
                 ,[INDEX_ID]
                 ,[PRICING_POINT]
                 ,[CURRENCY_UNIT_ID]
                 ,[VOLUME_UNIT_ID]
                 ,E.ENTITY_NAME AS Index_Type
                 ,PIV.INDEX_VALUE
                 ,PIV.INDEX_MONTH
              FROM
                  [CBMS].[dbo].[PRICE_INDEX] PI
                  INNER JOIN CBMS.dbo.ENTITY E
                        ON E.ENTITY_ID = PI.INDEX_ID
                  INNER JOIN CBMS.dbo.PRICE_INDEX_VALUE PIV
                        ON PIV.PRICE_INDEX_ID = PI.PRICE_INDEX_ID
              WHERE
                  E.ENTITY_NAME = 'Monthly Averages of Daily Midpoints'
                  AND year(PIV.INDEX_MONTH) > 2002 ) AS T1 ( PRICE_INDEX_ID, [INDEX_ID], [PRICING_POINT], [CURRENCY_UNIT_ID], [VOLUME_UNIT_ID], Index_Type, INDEX_VALUE, INDEX_MONTH )
            INNER JOIN CBMS.dbo.CURRENCY_UNIT CU
                  ON CU.CURRENCY_UNIT_ID = T1.CURRENCY_UNIT_ID
      ORDER BY
            T1.PRICING_POINT
           ,T1.INDEX_MONTH;
END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_Get_Price_Index_Monthly_Avg_Dtl] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Report_Get_Price_Index_Monthly_Avg_Dtl] TO [CBMSReports]
GO
