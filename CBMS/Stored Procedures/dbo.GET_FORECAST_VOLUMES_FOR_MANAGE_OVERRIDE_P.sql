SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

--exec GET_FORECAST_VOLUMES_FOR_MANAGE_OVERRIDE_P '-1','-1',10784,2005
CREATE  PROCEDURE dbo.GET_FORECAST_VOLUMES_FOR_MANAGE_OVERRIDE_P

@userId varchar(10),
@sessionId varchar(20),
@siteId int,
@year int

 AS
	set nocount on
	/*select SUM(VOLUME) VOLUME, MONTH_IDENTIFIER, ENTITY_NAME VOLUME_STATUS 
	from rm_forecast_volume_details, ENTITY entity where site_id=10784
	and datepart(year,month_identifier)=2005 and entity.ENTITY_ID=VOLUME_TYPE_ID
	
	GROUP BY month_identifier,site_id,ENTITY_NAME*/

select SUM(VOLUME) VOLUME, MONTH_IDENTIFIER, ENTITY_NAME VOLUME_STATUS 
from rm_forecast_volume_details, ENTITY entity where site_id=@siteId
and datepart(year,month_identifier)=@year and entity.ENTITY_ID=VOLUME_TYPE_ID

GROUP BY month_identifier,site_id,ENTITY_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_FORECAST_VOLUMES_FOR_MANAGE_OVERRIDE_P] TO [CBMSApplication]
GO
