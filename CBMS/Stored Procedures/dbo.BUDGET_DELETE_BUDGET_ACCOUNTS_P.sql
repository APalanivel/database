
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.BUDGET_DELETE_BUDGET_ACCOUNTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType		Default	Description
----------------------------------------------------------------
	@accountId				INT
	@budgetId				INT
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
-----------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	SELECT * FROM dbo.BUDGET WHERE BUDGET_ID = 8552
	SELECT * FROM dbo.Code WHERE Codeset_Id = 164
	BEGIN TRAN
		SELECT ba.IS_DELETED,ba.ACCOUNT_ID,baq.* FROM dbo.BUDGET_ACCOUNT AS ba LEFT JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 11129 and ba.ACCOUNT_ID = 58374
		UPDATE dbo.BUDGET_ACCOUNT SET IS_DELETED = 1 WHERE BUDGET_ID = 11129 and ACCOUNT_ID = 58374
		EXEC BUDGET_DELETE_BUDGET_ACCOUNTS_P 58374,11129
		SELECT ba.IS_DELETED,baq.* FROM dbo.BUDGET_ACCOUNT AS ba LEFT JOIN dbo.Budget_Account_Queue baq 
			ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id WHERE BUDGET_ID = 11129 and ba.ACCOUNT_ID = 58374
	ROLLBACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-06-01	Added header
							SE2017-26 - Modified to remove the deleted budget account from budget queue
	
******/
CREATE    PROCEDURE [dbo].[BUDGET_DELETE_BUDGET_ACCOUNTS_P]
      ( 
       @accountId INT
      ,@budgetId INT )
AS 
BEGIN
      SET NOCOUNT ON;

      UPDATE
            dbo.BUDGET_ACCOUNT
      SET   
            IS_DELETED = 1
      WHERE
            ACCOUNT_ID = @accountId
            AND BUDGET_ID = @budgetId
            
      DELETE
            baq
      FROM
            dbo.BUDGET_ACCOUNT ba
            INNER JOIN dbo.Budget_Account_Queue baq
                  ON ba.BUDGET_ACCOUNT_ID = baq.Budget_Account_Id
      WHERE
            ba.BUDGET_ID = @budgetId
            AND ba.ACCOUNT_ID = @accountId
            


END












;
GO

GRANT EXECUTE ON  [dbo].[BUDGET_DELETE_BUDGET_ACCOUNTS_P] TO [CBMSApplication]
GO
