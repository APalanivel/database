SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
 NAME: dbo.Contract_Exists_Sel_By_Account_Invoice_Begin_Dt_End_Dt              
                          
 DESCRIPTION:                          
   To get Supplier Account Details                         
                          
 INPUT PARAMETERS:            
                       
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
@Account_Id      INT  
@Start_Dt      Date  
@End_Dt       Date                           
                          
 OUTPUT PARAMETERS:            
                             
 Name                        DataType         Default       Description          
------------------------------------------------------------------------------       
                          
 USAGE EXAMPLES:                              
------------------------------------------------------------------------------       
   
EXEC dbo.Contract_Exists_Sel_By_Account_Invoice_Begin_Dt_End_Dt  
    @Account_Id = 1740963  
    , @Start_Dt = '2018-06-01'  
    , @End_Dt = '2018-06-30'  
  
  
 EXEC dbo.Contract_Exists_Sel_By_Account_Invoice_Begin_Dt_End_Dt  
    @Account_Id = 1740963  
    , @Start_Dt = '2019-10-01'  
    , @End_Dt = '2019-10-30'  
  
  
Select * from Contract where contract_Id=172899    
                       
 AUTHOR INITIALS:          
         
 Initials              Name          
------------------------------------------------------------------------------       
 NR      Narayana Reddy  
                           
 MODIFICATIONS:        
            
 Initials              Date             Modification        
------------------------------------------------------------------------------       
 NR                    2019-05-29       Created for - Add Contract.  
                         
******/
CREATE PROCEDURE [dbo].[Contract_Exists_Sel_By_Account_Invoice_Begin_Dt_End_Dt]
     (
         @Account_Id INT
         , @Start_Dt DATE
         , @End_Dt DATE
     )
AS
    BEGIN
        SET NOCOUNT ON;




        CREATE TABLE #Final_Account_List
             (
                 Account_Id INT
                 , Commodity_Id INT
                 , DMO_Start_Dt DATE
                 , DMO_End_Dt DATE
             );


        DECLARE
            @Is_Contract_Exists_With_In_Period BIT = 0
            , @Is_Raise_Missing_Contract_Exception BIT = 0
            , @Is_Overlap_And_Outside_Dates BIT = 0
            , @Is_Need_To_Add_New_Configuration BIT = 0
            , @Is_Dmo_Account BIT = 0
            , @Site_Id INT;


        SELECT
            @Site_Id = ch.Site_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @Account_Id;


        INSERT INTO #Final_Account_List
             (
                 Account_Id
                 , Commodity_Id
                 , DMO_Start_Dt
                 , DMO_End_Dt
             )
        EXEC dbo.DMO_Config_Sel_By_Site_Id @Site_Id = @Site_Id;






        SELECT
            @Is_Overlap_And_Outside_Dates = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID <> -1
            AND cha.Account_Type = 'Supplier'
            AND (   @Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                              AND     cha.Supplier_Account_End_Dt
                    OR  @End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                AND     cha.Supplier_Account_End_Dt
                    OR  cha.Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                    OR  cha.Supplier_Account_End_Dt BETWEEN @Start_Dt
                                                    AND     @End_Dt)
            AND (   cha.Supplier_Account_begin_Dt > @Start_Dt
                    OR  cha.Supplier_Account_End_Dt < @End_Dt);


        SELECT
            @Is_Contract_Exists_With_In_Period = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
            AND @Is_Overlap_And_Outside_Dates = 0
            AND cha.Supplier_Contract_ID <> -1
            AND (   cha.Supplier_Account_begin_Dt <= @Start_Dt
                    AND cha.Supplier_Account_End_Dt >= @End_Dt);


        SELECT
            @Is_Raise_Missing_Contract_Exception = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Type = 'Supplier'
            AND @Is_Contract_Exists_With_In_Period = 0
            AND cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID = -1
            AND (   @Start_Dt BETWEEN cha.Supplier_Account_begin_Dt
                              AND     cha.Supplier_Account_End_Dt
                    OR  @End_Dt BETWEEN cha.Supplier_Account_begin_Dt
                                AND     cha.Supplier_Account_End_Dt
                    OR  cha.Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                      AND     @End_Dt
                    OR  cha.Supplier_Account_End_Dt BETWEEN @Start_Dt
                                                    AND     @End_Dt)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    #Final_Account_List fal
                               WHERE
                                    fal.Account_Id = cha.Account_Id
                                    AND fal.Commodity_Id = cha.Commodity_Id
                                    AND (   @Start_Dt BETWEEN fal.DMO_Start_Dt
                                                      AND     fal.DMO_End_Dt
                                            AND @End_Dt BETWEEN fal.DMO_Start_Dt
                                                        AND     fal.DMO_End_Dt))
            AND @Is_Overlap_And_Outside_Dates = 0;











        SELECT
            @Is_Dmo_Account = 1
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN #Final_Account_List fal
                ON fal.Account_Id = cha.Account_Id
                   AND  fal.Commodity_Id = cha.Commodity_Id
        WHERE
            cha.Account_Type = 'Supplier'
            AND cha.Account_Id = @Account_Id
            AND cha.Supplier_Contract_ID = -1
            AND @Start_Dt BETWEEN fal.DMO_Start_Dt
                          AND     fal.DMO_End_Dt
            AND @End_Dt BETWEEN fal.DMO_Start_Dt
                        AND     fal.DMO_End_Dt;


        SELECT
            @Is_Need_To_Add_New_Configuration = 1
        FROM
            Core.Client_Hier_Account cha
        WHERE
            cha.Account_Id = @Account_Id
            AND @Is_Dmo_Account = 0
            AND cha.Account_Type = 'Supplier'
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Core.Client_Hier_Account chaa
                               WHERE
                                    chaa.Account_Id = cha.Account_Id
                                    AND chaa.Supplier_Contract_ID <> -1
                                    AND (   @Start_Dt BETWEEN chaa.Supplier_Account_begin_Dt
                                                      AND     chaa.Supplier_Account_End_Dt
                                            OR  @End_Dt BETWEEN chaa.Supplier_Account_begin_Dt
                                                        AND     chaa.Supplier_Account_End_Dt
                                            OR  chaa.Supplier_Account_begin_Dt BETWEEN @Start_Dt
                                                                               AND     @End_Dt
                                            OR  chaa.Supplier_Account_End_Dt BETWEEN @Start_Dt
                                                                             AND     @End_Dt))
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Core.Client_Hier_Account cha2
                               WHERE
                                    cha2.Account_Id = cha.Account_Id
                                    AND cha.Supplier_Contract_ID = -1);







        SELECT
            @Is_Raise_Missing_Contract_Exception AS Is_Raise_Missing_Contract_Exception
            , @Is_Contract_Exists_With_In_Period AS Is_Contract_Exists_With_In_Period
            , @Is_Overlap_And_Outside_Dates AS Is_Overlap_And_Outside_Dates
            , @Is_Need_To_Add_New_Configuration AS Is_Need_To_Add_New_Configuration
            , @Is_Dmo_Account AS Is_Dmo_Account;


    END;




GO
GRANT EXECUTE ON  [dbo].[Contract_Exists_Sel_By_Account_Invoice_Begin_Dt_End_Dt] TO [CBMSApplication]
GO
