SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******   
 NAME:    
 dbo.Get_Drt_Detail_Report_By_Queue_Name
   
 DESCRIPTION:    
 
	Get DRT detail report values based on Queue Name   
   
 INPUT PARAMETERS:    
	Name					     DataType	  Default Description    
-------------------------------------------------------------------------    
	@Queue_Name_List			 VARCHAR(MAX)

 OUTPUT PARAMETERS:

 Name							DataType	 Default Description    
------------------------------------------------------------ 
   
 USAGE EXAMPLES:  
------------------------------------------------------------    
 
 exec dbo.Get_Drt_Detail_Report_By_Queue_Name @Queue_Name_List = 'Hema Mandhapati,Mayukh Ghatak'
 exec dbo.Get_Drt_Detail_Report_By_Queue_Name  'Mayukh Ghatak'
 exec dbo.Get_Drt_Detail_Report_By_Queue_Name 'Hema Mandhapati'
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
BCH		  Balaraju  
  
 MODIFICATIONS:  
 Initials   Date		 Modification
------------------------------------------------------------
 BCH		09-05-2013   (Main -  1830), Created stored porcedure Instead of  BPO tool DRT Report Detail Script
******/
CREATE PROCEDURE dbo.Get_Drt_Detail_Report_By_Queue_Name
      (
       @Queue_Name_List VARCHAR(MAX) )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @Queue_Name TABLE
            ( 
             Queue_Name VARCHAR(200) )
             
      DECLARE @Analyst TABLE
            ( 
             Analyst_Name VARCHAR(100) )
              
      CREATE TABLE #Temp_Quename
            ( 
             QUEUE_ID INT
            ,QUEUE_TYPE_ID INT
            ,Queue_Type VARCHAR(200)
            ,Queue_Name VARCHAR(100) )
            
      CREATE TABLE #Queue_Cnt
            ( 
             Incoming_Private_Count_Blue INT
            ,Incoming_Private_Count_Red INT
            ,Incoming_Public_Count_Blue INT
            ,Incoming_Public_Count_Red INT
            ,Exception_Private_Count_Blue INT
            ,Exception_Private_Count_Red INT
            ,Exception_Public_Count_Blue INT
            ,Exception_Public_Count_Red INT
            ,Variance_Count_Blue INT
            ,Variance_Count_Red INT
            ,Queue_Name VARCHAR(200) )
            
      INSERT      INTO @Queue_Name
                  ( 
                   Queue_Name )
                  SELECT
                        Segments
                  FROM
                        dbo.ufn_split(@Queue_Name_List, ',')
                        
      INSERT      INTO @Analyst
                  ( 
                   Analyst_Name )
                  SELECT
                        ui.FIRST_NAME + space(1) + ui.LAST_NAME AS analyst
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.USER_INFO_GROUP_INFO_MAP uigim
                              ON uigim.user_info_id = ui.user_info_id
                        INNER JOIN @Queue_Name Qu
                              ON qu.Queue_Name = ui.first_name + ' ' + ui.last_name
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.GROUP_INFO_ID = uigim.GROUP_INFO_ID
                  WHERE
                        gi.GROUP_NAME = 'operations'
                        AND ui.is_history = '0'

      INSERT      INTO #Temp_Quename
                  ( 
                   QUEUE_ID
                  ,QUEUE_TYPE_ID
                  ,Queue_Type
                  ,Queue_Name )
                  SELECT
                        q.QUEUE_ID
                       ,q.QUEUE_TYPE_ID
                       ,ent.ENTITY_NAME AS Queue_Type
                       ,ui.FIRST_NAME + space(1) + ui.LAST_NAME AS Queue_Name
                  FROM
                        dbo.USER_INFO ui
                        INNER JOIN dbo.QUEUE q
                              ON q.QUEUE_ID = ui.QUEUE_ID
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = q.QUEUE_TYPE_ID
                        INNER JOIN @Queue_Name qn
                              ON qn.Queue_Name = ui.FIRST_NAME + space(1) + ui.LAST_NAME
                  WHERE
                        ent.ENTITY_NAME = 'Private'

      INSERT      INTO #Temp_Quename
                  ( 
                   QUEUE_ID
                  ,QUEUE_TYPE_ID
                  ,Queue_Type
                  ,Queue_Name )
                  SELECT
                        q.QUEUE_ID
                       ,q.QUEUE_TYPE_ID
                       ,ent.ENTITY_NAME AS queue_type
                       ,gi.GROUP_NAME
                  FROM
                        dbo.GROUP_INFO gi
                        INNER JOIN dbo.QUEUE q
                              ON q.QUEUE_ID = gi.QUEUE_ID
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = q.QUEUE_TYPE_ID
                  WHERE
                        ent.ENTITY_NAME = 'Public' 

            
            
      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        count(DISTINCT t.cu_invoice_id) AS Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.CU_EXCEPTION_DENORM t
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = qn.QUEUE_TYPE_ID
                  WHERE
                        t.is_manual = 1
                        AND ent.ENTITY_NAME = 'private'
                  GROUP BY
                        qn.Queue_Name

      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,count(DISTINCT t.cu_invoice_id) AS Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.CU_EXCEPTION_DENORM t
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = qn.QUEUE_TYPE_ID
                  WHERE
                        t.is_manual = 1
                        AND getdate() - t.date_in_queue >= 2
                        AND ent.ENTITY_NAME = 'private'
                  GROUP BY
                        qn.Queue_Name


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,count(DISTINCT t.cu_invoice_id) AS Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP ugm
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.group_info_id = ugm.group_info_id
                        INNER JOIN dbo.CU_EXCEPTION_DENORM t
                              ON t.queue_id = gi.queue_id
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 1
                  GROUP BY
                        qn.Queue_Name


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,count(DISTINCT t.cu_invoice_id) AS Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP ugm
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.group_info_id = ugm.group_info_id
                        INNER JOIN dbo.CU_EXCEPTION_DENORM t
                              ON t.queue_id = gi.queue_id
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 1
                        AND getdate() - t.date_in_queue >= 2
                  GROUP BY
                        qn.Queue_Name

      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,count(DISTINCT t.cu_invoice_id) AS Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.CU_EXCEPTION_DENORM t
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 0
                  GROUP BY
                        qn.Queue_Name


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,count(DISTINCT t.cu_invoice_id) AS Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.CU_EXCEPTION_DENORM t
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 0
                        AND getdate() - t.date_in_queue >= 2
                  GROUP BY
                        qn.Queue_Name


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,count(DISTINCT t.cu_invoice_id) AS Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP ugm
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.group_info_id = ugm.group_info_id
                        INNER JOIN dbo.CU_EXCEPTION_DENORM t
                              ON t.queue_id = gi.queue_id
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 1
                  GROUP BY
                        qn.Queue_Name


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,count(DISTINCT t.cu_invoice_id) AS Exception_Public_Count_Red
                       ,0 Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,qn.Queue_Name
                  FROM
                        dbo.USER_INFO_GROUP_INFO_MAP ugm
                        INNER JOIN dbo.GROUP_INFO gi
                              ON gi.group_info_id = ugm.group_info_id
                        INNER JOIN dbo.CU_EXCEPTION_DENORM t
                              ON t.queue_id = gi.queue_id
                        INNER JOIN #Temp_Quename qn
                              ON qn.queue_id = t.queue_id
                  WHERE
                        t.is_manual = 1
                        AND getdate() - t.date_in_queue >= 2
                  GROUP BY
                        qn.Queue_Name

      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
                  SELECT
                        0 Incoming_Private_Count_Blue
                       ,0 Incoming_Private_Count_Red
                       ,0 Incoming_Public_Count_Blue
                       ,0 Incoming_Public_Count_Red
                       ,0 Exception_Private_Count_Blue
                       ,0 Exception_Private_Count_Red
                       ,0 Exception_Public_Count_Blue
                       ,0 Exception_Public_Count_Red
                       ,min(l.Variance_Log_Id) AS Variance_Count_Blue
                       ,0 Variance_Count_Red
                       ,l.Queue_Name
                  FROM
                        dbo.Variance_Log AS l
                  WHERE
                        Partition_Key IS NULL
                        AND l.Is_Failure = 1
                        AND l.Closed_Dt IS NULL
                  GROUP BY
                        l.Queue_Name
                       ,l.account_id
                       ,l.service_month


      INSERT      INTO #Queue_Cnt
                  ( 
                   Incoming_Private_Count_Blue
                  ,Incoming_Private_Count_Red
                  ,Incoming_Public_Count_Blue
                  ,Incoming_Public_Count_Red
                  ,Exception_Private_Count_Blue
                  ,Exception_Private_Count_Red
                  ,Exception_Public_Count_Blue
                  ,Exception_Public_Count_Red
                  ,Variance_Count_Blue
                  ,Variance_Count_Red
                  ,Queue_Name )
      SELECT
            0 Incoming_Private_Count_Blue
           ,0 Incoming_Private_Count_Red
           ,0 Incoming_Public_Count_Blue
           ,0 Incoming_Public_Count_Red
           ,0 Exception_Private_Count_Blue
           ,0 Exception_Private_Count_Red
           ,0 Exception_Public_Count_Blue
           ,0 Exception_Public_Count_Red
           ,0 Variance_Count_Blue
           ,min(l.Variance_Log_Id) AS Variance_Count_Red
           ,l.Queue_Name
      FROM
            dbo.Variance_Log AS l
      WHERE
            Partition_Key IS NULL
            AND l.Is_Failure = 1
            AND l.Closed_Dt IS NULL
            AND datediff(dd, l.Execution_Dt, getdate()) >= 5
      GROUP BY
            l.Queue_Name
           ,l.account_id
           ,l.service_month 


      SELECT
            Queue_Name AS Analyst
           ,getdate() AS Date
           ,sum(x.Incoming_Private_Count_Blue) AS 'Incoming_Total'
           ,sum(x.Incoming_Private_Count_Red) AS 'Incoming_Red'
           ,sum(case WHEN x.Variance_Count_Blue > 0 THEN 1
                     ELSE 0
                END) AS 'Variance_Total'
           ,sum(case WHEN x.Variance_Count_Red > 0 THEN 1
                     ELSE 0
                END) AS 'Variance_Red'
           ,sum(x.Exception_Private_Count_Blue) AS 'Exceptions_Total'
           ,sum(x.Exception_Private_Count_Red) AS 'Exceptions_Red'
      FROM
            #Queue_Cnt x
            INNER JOIN @Analyst anl
                  ON anl.Analyst_Name = x.Queue_Name
      GROUP BY
            x.Queue_Name

      DROP TABLE #Temp_Quename
      DROP TABLE #Queue_Cnt

END

;
GO
GRANT EXECUTE ON  [dbo].[Get_Drt_Detail_Report_By_Queue_Name] TO [BPOReportRole]
GRANT EXECUTE ON  [dbo].[Get_Drt_Detail_Report_By_Queue_Name] TO [CBMSApplication]
GO
