SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE dbo.COPY_BATCH_CONFIGURATION_P

@userId varchar(10),
@sessionId varchar(20),
@masterId int


as
	set nocount on
	insert into RM_BATCH_CONFIGURATION 
		(
			RM_BATCH_CONFIGURATION_MASTER_ID,
			client_id,
			report_list_id,
			corporate_report
		)
	select 		@masterId,client_id,report_list_id,corporate_report 
	from 		RM_BATCH_CONFIGURATION 
	where 		RM_BATCH_CONFIGURATION_MASTER_ID = 
		(
			select max(RM_BATCH_CONFIGURATION_MASTER_ID) from RM_REPORT_BATCH 
		)


	/*insert into RM_BATCH_CONFIGURATION_DETAILS
	(
		RM_BATCH_CONFIGURATION_ID,
		SITE_ID,
		DIVISION_ID
	)
	select 	RM_BATCH_CONFIGURATION_ID,SITE_ID,
		DIVISION_ID
	from 	RM_BATCH_CONFIGURATION_DETAILS
	where 	RM_BATCH_CONFIGURATION_ID in 
		( 	
			select RM_BATCH_CONFIGURATION_id from RM_BATCH_CONFIGURATION where 
			RM_BATCH_CONFIGURATION_master_id=
			(
				select max(RM_BATCH_CONFIGURATION_MASTER_ID) from RM_REPORT_BATCH 
			)
		)*/
GO
GRANT EXECUTE ON  [dbo].[COPY_BATCH_CONFIGURATION_P] TO [CBMSApplication]
GO
