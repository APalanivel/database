SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                        
 NAME: dbo.Report_DE_IC_Issue                            
                                        
 DESCRIPTION:                                        
   This will allow the managers to gain a better understanding of the scope of issues that can be encountered in Invoice collection                
                                     
                                        
 INPUT PARAMETERS:                          
                                     
 Name                        DataType         Default       Description                        
---------------------------------------------------------------------------------------------------------------                      
                                       
 OUTPUT PARAMETERS:                          
                                           
 Name                        DataType         Default       Description                        
---------------------------------------------------------------------------------------------------------------                      
                                        
 USAGE EXAMPLES:                                            
---------------------------------------------------------------------------------------------------------------                                            
                
                
   EXEC [dbo].[Report_DE_IC_Issue] 
    @Client_Id=11309                
  ,@Issue_Status_Cd = 102304            
  ,@CM =  '<All>'         
  ,@CA = '<All>'                  
                    
   EXEC [dbo].[Report_DE_IC_Issue]                 
      -- @Client_Id=11309                
       @Issue_Status_Cd = 102304            
  ,@CM =  '119'         
  ,@CA = '27789'		          
                      
                  
                 
       SELECT * FROM core.Client_Hier ch WHERE ch.Client_Name LIKE 'Kraft Heinz Foods Company'                                    
 AUTHOR INITIALS:                        
                       
 Initials              Name                        
---------------------------------------------------------------------------------------------------------------                                      
 SP                    Sandeep Pigilam                 
 RKV                   Ravi Kumar Vegesna                  
 NR      Narayana Reddy                         
 ABK     Aditya Bharadwaj  K            
                                         
 MODIFICATIONS:                      
                          
 Initials              Date             Modification                      
---------------------------------------------------------------------------------------------------------------                      
 SP                    2017-01-25       Created for Invoice Tracking.                 
 RKV                   2017-12-05       SE2017-313,Added new column to the result set Invoice_Collection_Owner                
 NR        2018-01-02       SE2017-388 - IC Reports - To excluded the  Demo Clients.                                 
 RKV                   2018-01-09       SE2017-389  Added Filter to exclude the Archived Records.         
 ABK     2019-07-19  REPTMGR-117  Added filters to Invoice Collection - Issue Report and Chase History Report in Reporting Services              
 RKV	 2020-06-16	 Maint-10287  Perfromance tuning..	
                                      
******/


CREATE PROCEDURE [dbo].[Report_DE_IC_Issue]
    (
        @Client_Id INT = -1
        , @Country_Id INT = -1
        , @State_Id INT = -1
        , @Invoice_Collection_Coordinator_Id INT = -1
        , @Invoice_Collection_Officer_Id INT = -1
        , @Primary_Source INT = -1
        , @Issue_Status_Cd INT = 102304
        , @Vendor_Id INT = -1
        , @Invoice_Collection_Owner_Id INT = -1
        , @CM VARCHAR(MAX) = -1
        , @CA VARCHAR(MAX) = -1
        , @CS VARCHAR(MAX) = -1
    )
AS
    BEGIN
        SET NOCOUNT ON;

        IF (1 = 2)
            SELECT
                1 AS Client_Name
                , 1 AS Country_Name
                , 1 AS State_Name
                , 1 AS Site_name
                , 1 AS Account_Number
                , 1 AS Alternative_Account_Number
                , 1 AS Officer
                , 1 AS Coordinator
                , 1 AS [Invoice period at issue]
                , 1 AS Account_Vendor_Name
                , 1 AS Invoice_Collection_Primary_Source
                , 1 AS Issue_Dt
                , 1 AS [issue raised by]
                , 1 AS Day_Since_Raised
                , 1 AS Last_Change_Ts
                , 1 AS Day_Since_Updated
                , 1 AS Last_Updated_By
                , 1 AS Issue_Status
                , 1 AS Closed_Dt
                , 1 AS Issue_Type
                , 1 AS Issue_Entity_Owner
                , 1 AS Issue_Owner
                , 1 AS Internal_Comments
                , 1 AS External_Comments
                , 1 AS Invoice_Collection_Owner
                , 1 AS CEM
                , 1 AS CEA
                , 1 AS CSA;

        
        DECLARE
            @IC_Internal_Comment_Cd INT
            , @IC_External_Comment_Cd INT
            , @Today_Dt DATE = GETDATE()
            , @Source_Type_Client INT
            , @Source_Type_Account INT
            , @Source_Type_Vendor INT
            , @Contact_Level_Client INT
            , @Contact_Level_Account INT
            , @Contact_Level_Vendor INT
            , @Exclude_Demo_Client_Type_Id INT;

        CREATE TABLE #Account_dtls
             (
                 Invoice_Collection_Account_Config_Id INT NOT NULL
                 , Client_Name VARCHAR(200)
                 , Country_Name VARCHAR(200)
                 , State_Name VARCHAR(200)
                 , Site_name VARCHAR(200)
                 , Account_Number VARCHAR(50)
                 , Client_Type_Id INT
                 , Account_Vendor_Name VARCHAR(200)
                 , Client_Id INT
             );



        CREATE TABLE #Vendor_Dtls
             (
                 Account_Vendor_Name VARCHAR(200)
                 , Account_Vendor_Type CHAR(8)
                 , Account_Vendor_Id INT
                 , Invoice_Collection_Account_Config_Id INT
             );

        CREATE TABLE #Invoice_Collection_Issue_Comments
             (
                 Invoice_Collection_Issue_Log_Id INT
                 , IC_Internal_Comments_Event_Id INT
                 , IC_External_Comments_Event_Id INT
             );



        INSERT INTO #Account_dtls
             (
                 Invoice_Collection_Account_Config_Id
                 , Client_Name
                 , Country_Name
                 , State_Name
                 , Site_name
                 , Account_Number
                 , Client_Type_Id
                 , Account_Vendor_Name
                 , Client_Id
             )
        SELECT
            icac.Invoice_Collection_Account_Config_Id
            , ch.Client_Name
            , ch.Country_Name
            , ch.State_Name
            , ch.Site_name
            , cha.Account_Number
            , ch.Client_Type_Id
            , cha.Account_Vendor_Name
            , ch.Client_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icac.Account_Id = cha.Account_Id
        WHERE
            (   @Client_Id = -1
                OR  ch.Client_Id = @Client_Id)
            AND (   @Country_Id = -1
                    OR  ch.Country_Id = @Country_Id)
            AND (   @State_Id = -1
                    OR  ch.State_Id = @State_Id)
        GROUP BY
            icac.Invoice_Collection_Account_Config_Id
            , ch.Client_Name
            , ch.Country_Name
            , ch.State_Name
            , ch.Site_name
            , cha.Account_Number
            , ch.Client_Type_Id
            , cha.Account_Vendor_Name
            , ch.Client_Id;

        CREATE NONCLUSTERED INDEX [ix_Account_dtls]
            ON #Account_dtls
        (
            [Invoice_Collection_Account_Config_Id]
        )   ;



        CREATE TABLE #UserInfo
             (
                 Client_Id INT
                 , User_List_CEM VARCHAR(MAX)
                 , User_List_CEA VARCHAR(MAX)
                 , User_List_CSA VARCHAR(MAX)
             );

        CREATE TABLE #CEM_UserInfo
             (
                 Client_Id INT
                 , User_List_CEM VARCHAR(MAX)
             );
        CREATE TABLE #CEA_UserInfo
             (
                 Client_Id INT
                 , User_List_CEA VARCHAR(MAX)
             );
        CREATE TABLE #CSA_UserInfo
             (
                 Client_Id INT
                 , User_List_CSA VARCHAR(MAX)
             );


        INSERT INTO #CEM_UserInfo
             (
                 Client_Id
                 , User_List_CEM
             )
        SELECT
            c.CLIENT_ID
            , STUFF((   SELECT
                            ', ' + CAST(ui.FIRST_NAME + ' ' + ui.LAST_NAME AS VARCHAR(80))
                        FROM
                            dbo.USER_INFO ui
                            JOIN dbo.CLIENT_CEM_MAP ccm2
                                ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                        WHERE
                            ccm2.CLIENT_ID = c.CLIENT_ID
                        FOR XML PATH('')), 1, 1, '') AS cm
        FROM
            CLIENT c
        WHERE
            @CM = -1
            OR  @CM = '<All>'
            OR  EXISTS (   SELECT
                                1
                           FROM
                                dbo.USER_INFO ui
                                JOIN dbo.CLIENT_CEM_MAP ccm3
                                    ON ui.USER_INFO_ID = ccm3.USER_INFO_ID
                           WHERE
                                c.CLIENT_ID = ccm3.CLIENT_ID
                                AND EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.ufn_split(@CM, ',') us
                                               WHERE
                                                    us.Segments = CAST(ccm3.USER_INFO_ID AS VARCHAR(MAX))));




        INSERT INTO #CSA_UserInfo
             (
                 Client_Id
                 , User_List_CSA
             )
        SELECT
            c.CLIENT_ID
            , STUFF((   SELECT
                            ', ' + CAST(ui.FIRST_NAME + ' ' + ui.LAST_NAME AS VARCHAR(80))
                        FROM
                            dbo.USER_INFO ui
                            JOIN dbo.CLIENT_CSA_MAP ccm2
                                ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                        WHERE
                            ccm2.CLIENT_ID = c.CLIENT_ID
                        FOR XML PATH('')), 1, 1, '') AS cs
        FROM
            CLIENT c
        WHERE
            @CS = -1
            OR  @CS = '<All>'
            OR  EXISTS (   SELECT
                                1
                           FROM
                                dbo.USER_INFO ui
                                JOIN dbo.CLIENT_CSA_MAP ccm3
                                    ON ui.USER_INFO_ID = ccm3.USER_INFO_ID
                           WHERE
                                c.CLIENT_ID = ccm3.CLIENT_ID
                                AND EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.ufn_split(@CS, ',') us
                                               WHERE
                                                    us.Segments = CAST(ccm3.USER_INFO_ID AS VARCHAR(MAX))));

        INSERT INTO #CEA_UserInfo
             (
                 Client_Id
                 , User_List_CEA
             )
        SELECT
            C.CLIENT_ID
            , STUFF((   SELECT
                            ', ' + CAST(ui.FIRST_NAME + ' ' + ui.LAST_NAME AS VARCHAR(80))
                        FROM
                            dbo.USER_INFO ui
                            JOIN dbo.CLIENT_CEA_MAP ccm2
                                ON ui.USER_INFO_ID = ccm2.USER_INFO_ID
                        WHERE
                            ccm2.CLIENT_ID = C.CLIENT_ID
                            AND (   @CA = -1
                                    OR  @CA = '<All>'
                                    OR  EXISTS (   SELECT
                                                        1
                                                   FROM
                                                        dbo.ufn_split(@CA, ',') us
                                                   WHERE
                                                        us.Segments = CAST(ccm2.USER_INFO_ID AS VARCHAR(MAX))))
                        FOR XML PATH('')), 1, 1, '') AS ca
        FROM
            CLIENT C
        WHERE
            @CA = -1
            OR  @CA = '<All>'
            OR  EXISTS (   SELECT
                                1
                           FROM
                                dbo.USER_INFO ui
                                JOIN dbo.CLIENT_CEA_MAP ccm3
                                    ON ui.USER_INFO_ID = ccm3.USER_INFO_ID
                           WHERE
                                C.CLIENT_ID = ccm3.CLIENT_ID
                                AND EXISTS (   SELECT
                                                    1
                                               FROM
                                                    dbo.ufn_split(@CA, ',') us
                                               WHERE
                                                    us.Segments = CAST(ccm3.USER_INFO_ID AS VARCHAR(MAX))));






        INSERT INTO #UserInfo
             (
                 Client_Id
                 , User_List_CEM
                 , User_List_CEA
                 , User_List_CSA
             )
        SELECT
            CEA.Client_Id
            , CEM.User_List_CEM
            , CEA.User_List_CEA
            , csa.User_List_CSA
        FROM
            #CEA_UserInfo CEA
            INNER JOIN #CEM_UserInfo CEM
                ON CEM.Client_Id = CEA.Client_Id
            INNER JOIN #CSA_UserInfo csa
                ON csa.Client_Id = CEM.Client_Id
        WHERE
            (   @Client_Id = -1
                OR  CEA.Client_Id = @Client_Id);




        SELECT
            @IC_Internal_Comment_Cd = MAX(CASE WHEN c.Code_Value = 'IC Internal Comments' THEN c.Code_Id
                                          END)
            , @IC_External_Comment_Cd = MAX(CASE WHEN c.Code_Value = 'IC External Comments' THEN c.Code_Id
                                            END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON c.Codeset_Id = cs.Codeset_Id
        WHERE
            cs.Codeset_Name = 'IC Issue Event Type'
            AND c.Code_Value IN ( 'IC Internal Comments', 'IC External Comments' );


        SELECT
            @Exclude_Demo_Client_Type_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            ENTITY_NAME = 'Demo'
            AND ENTITY_TYPE = 125
            AND ENTITY_DESCRIPTION = 'Client Type';





        INSERT INTO #Vendor_Dtls
             (
                 Account_Vendor_Name
                 , Account_Vendor_Type
                 , Account_Vendor_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            Account_Vendor_Name = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                            AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                                      WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                                      WHEN icav.VENDOR_NAME IS NOT NULL
                                           AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                                      ELSE ucha.Account_Vendor_Name
                                  END
            , Account_Vendor_Type = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                              AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                                        WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                                        WHEN icav.VENDOR_NAME IS NOT NULL
                                             AND asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                                        ELSE ucha.Account_Type
                                    END
            , Account_Vendor_Id = CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                                            AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                                      WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                                      WHEN icav.VENDOR_NAME IS NOT NULL
                                           AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                                      ELSE ucha.Account_Vendor_Id
                                  END
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN Core.Client_Hier_Account ucha
                ON icac.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN Core.Client_Hier_Account scha
                ON ucha.Meter_Id = scha.Meter_Id
                   AND  scha.Account_Type = 'Supplier'
                   AND  icac.Invoice_Collection_Service_End_Dt BETWEEN scha.Supplier_Account_begin_Dt
                                                               AND     scha.Supplier_Account_End_Dt
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv
                            INNER JOIN dbo.ENTITY e
                                ON asbv.Invoice_Vendor_Type_Id = e.ENTITY_ID
                                   AND  e.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v
                                ON v.VENDOR_ID = asbv.Supplier_Vendor_Id)
                ON asbv.Account_Id = ucha.Account_Id
                   AND  icac.Invoice_Collection_Service_End_Dt BETWEEN asbv.Billing_Start_Dt
                                                               AND     asbv.Billing_End_Dt
            LEFT OUTER JOIN(dbo.Account_Consolidated_Billing_Vendor asbv1
                            INNER JOIN dbo.ENTITY e1
                                ON asbv1.Invoice_Vendor_Type_Id = e1.ENTITY_ID
                                   AND  e1.ENTITY_NAME = 'Supplier'
                            LEFT OUTER JOIN dbo.VENDOR v1
                                ON v1.VENDOR_ID = asbv1.Supplier_Vendor_Id)
                ON asbv1.Account_Id = ucha.Account_Id
            LEFT OUTER JOIN(dbo.Invoice_Collection_Account_Contact icc
                            INNER JOIN dbo.Account_Invoice_Collection_Source aics
                                ON icc.Invoice_Collection_Account_Config_Id = aics.Invoice_Collection_Account_Config_Id
                            INNER JOIN Contact_Info ci
                                ON ci.Contact_Info_Id = icc.Contact_Info_Id
                                   AND  icc.Is_Primary = 1
                                   AND  (   (   @Source_Type_Client = aics.Invoice_Source_Type_Cd
                                                AND @Contact_Level_Client = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Account = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Account = ci.Contact_Level_Cd)
                                            OR  (   @Source_Type_Vendor = aics.Invoice_Source_Type_Cd
                                                    AND @Contact_Level_Vendor = ci.Contact_Level_Cd))
                            INNER JOIN dbo.Vendor_Contact_Map vcm
                                ON ci.Contact_Info_Id = vcm.Contact_Info_Id
                            INNER JOIN dbo.VENDOR icav
                                ON icav.VENDOR_ID = vcm.VENDOR_ID)
                ON icc.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
        WHERE
            ucha.Account_Type = 'Utility'
        GROUP BY
            CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                      AND   asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Name
                WHEN v.VENDOR_NAME IS NOT NULL THEN v.VENDOR_NAME
                WHEN icav.VENDOR_NAME IS NOT NULL
                     AND asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_NAME
                ELSE ucha.Account_Vendor_Name
            END
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Type
                  WHEN e.ENTITY_NAME IS NOT NULL THEN e.ENTITY_NAME
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN 'Supplier'
                  ELSE ucha.Account_Type
              END
            , CASE WHEN scha.Account_Vendor_Name IS NOT NULL
                        AND asbv1.Account_Id IS NOT NULL THEN scha.Account_Vendor_Id
                  WHEN e.ENTITY_NAME IS NOT NULL THEN v.VENDOR_ID
                  WHEN icav.VENDOR_NAME IS NOT NULL
                       AND  asbv1.Account_Id IS NOT NULL THEN icav.VENDOR_ID
                  ELSE ucha.Account_Vendor_Id
              END
            , icac.Invoice_Collection_Account_Config_Id;

        CREATE NONCLUSTERED INDEX [IX_Invoice_Collection_Account_Config_Id_Vendor]
            ON #Vendor_Dtls
        (
            Invoice_Collection_Account_Config_Id
        )   ;



        INSERT INTO #Invoice_Collection_Issue_Comments
             (
                 Invoice_Collection_Issue_Log_Id
                 , IC_Internal_Comments_Event_Id
                 , IC_External_Comments_Event_Id
             )
        SELECT
            icie.Invoice_Collection_Issue_Log_Id
            , MAX(CASE WHEN icie.Issue_Event_Type_Cd = @IC_Internal_Comment_Cd THEN
                           icie.Invoice_Collection_Issue_Event_Id
                  END) int_comm
            , MAX(CASE WHEN icie.Issue_Event_Type_Cd = @IC_External_Comment_Cd THEN
                           icie.Invoice_Collection_Issue_Event_Id
                  END) ext_comm
        FROM
            Invoice_Collection_Issue_Event icie
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                ON icil.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Queue_Id = icil.Invoice_Collection_Queue_Id
        WHERE
            icq.Collection_Start_Dt > '2019-01-01'
            AND icie.Issue_Event_Type_Cd IN ( @IC_Internal_Comment_Cd, @IC_External_Comment_Cd )
        GROUP BY
            icie.Invoice_Collection_Issue_Log_Id;


        SELECT
            ad.Client_Name
            , ad.Country_Name
            , ad.State_Name
            , ad.Site_name
            , ad.Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number AS Alternative_Account_Number
            , oui.FIRST_NAME + ' ' + oui.LAST_NAME AS Officer
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME AS Coordinator
            , '' + REPLACE(CONVERT(NVARCHAR, icil.Collection_Start_Dt, 106), ' ', '') + ' to '
              + REPLACE(CONVERT(NVARCHAR, icil.Collection_End_Dt, 106), ' ', '') + '' AS [Invoice period at issue]
            , ISNULL(vd.Account_Vendor_Name, ad.Account_Vendor_Name) Account_Vendor_Name
            , Icsc.Code_Value AS Invoice_Collection_Primary_Source
            , REPLACE(CONVERT(NVARCHAR, icil.Created_Ts, 106), ' ', '-') AS Issue_Dt
            , icilr.FIRST_NAME + ' ' + icilr.LAST_NAME AS [issue raised by]
            , DATEDIFF(dd, icil.Created_Ts, @Today_Dt) AS Day_Since_Raised
            , REPLACE(CONVERT(NVARCHAR, icil.Last_Change_Ts, 106), ' ', '-') Last_Change_Ts
            , DATEDIFF(dd, icil.Last_Change_Ts, @Today_Dt) AS Day_Since_Updated
            , icilu.FIRST_NAME + ' ' + icilu.LAST_NAME AS Last_Updated_By
            , icilus.Code_Value AS Issue_Status
            , CASE WHEN icilus.Code_Value = 'Closed' THEN
                       REPLACE(CONVERT(NVARCHAR, icil.Last_Change_Ts, 106), ' ', '-')
                  ELSE ''
              END AS Closed_Dt
            , icilt.Code_Value AS Issue_Type
            , icile.Code_Value AS Issue_Entity_Owner
            , icileu.FIRST_NAME + ' ' + icileu.LAST_NAME AS Issue_Owner
            , iciei.Event_Desc AS Internal_Comments
            , iciee.Event_Desc AS External_Comments
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME AS Invoice_Collection_Owner
            , ui.User_List_CEM CEM
            , ui.User_List_CEA CEA
            , ui.User_List_CSA CSA
        FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Code icqs
                ON icq.Status_Cd = icqs.Code_Id
            INNER JOIN dbo.Invoice_Collection_Account_Config icac
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN #Account_dtls ad
                ON ad.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Account_Invoice_Collection_Source ics
                ON icac.Invoice_Collection_Account_Config_Id = ics.Invoice_Collection_Account_Config_Id
                   AND  ics.Is_Primary = 1
            LEFT JOIN dbo.Invoice_Collection_Client_Config iccc
                ON ad.Client_Id = iccc.Client_Id
            LEFT JOIN dbo.USER_INFO oui
                ON ISNULL(icac.Invoice_Collection_Officer_User_Id, iccc.Invoice_Collection_Officer_User_Id) = oui.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO icowner
                ON icac.Invoice_Collection_Owner_User_Id = icowner.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO cui
                ON iccc.Invoice_Collection_Coordinator_User_Id = cui.USER_INFO_ID
            LEFT JOIN dbo.Code Icsc
                ON ics.Invoice_Collection_Source_Cd = Icsc.Code_Id
            LEFT JOIN dbo.USER_INFO icilu
                ON icil.Updated_User_Id = icilu.USER_INFO_ID
            LEFT JOIN dbo.USER_INFO icilr
                ON icil.Created_User_Id = icilr.USER_INFO_ID
            LEFT JOIN dbo.Code icilus
                ON icil.Issue_Status_Cd = icilus.Code_Id
            LEFT JOIN dbo.Code icilt
                ON icil.Invoice_Collection_Issue_Type_Cd = icilt.Code_Id
            LEFT JOIN dbo.Code icile
                ON icil.Issue_Entity_Owner_Cd = icile.Code_Id
            LEFT JOIN dbo.USER_INFO icileu
                ON icil.Issue_Owner_User_Id = icileu.USER_INFO_ID
            LEFT OUTER JOIN #Vendor_Dtls vd
                ON vd.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            LEFT JOIN(dbo.Invoice_Collection_Issue_Event iciei
                      INNER JOIN #Invoice_Collection_Issue_Comments icic
                          ON icic.Invoice_Collection_Issue_Log_Id = iciei.Invoice_Collection_Issue_Log_Id
                             AND iciei.Invoice_Collection_Issue_Event_Id = icic.IC_Internal_Comments_Event_Id)
                ON icil.Invoice_Collection_Issue_Log_Id = iciei.Invoice_Collection_Issue_Log_Id
            LEFT JOIN(dbo.Invoice_Collection_Issue_Event iciee
                      INNER JOIN #Invoice_Collection_Issue_Comments icie
                          ON icie.Invoice_Collection_Issue_Log_Id = iciee.Invoice_Collection_Issue_Log_Id
                             AND iciee.Invoice_Collection_Issue_Event_Id = icie.IC_External_Comments_Event_Id)
                ON icil.Invoice_Collection_Issue_Log_Id = iciee.Invoice_Collection_Issue_Log_Id
            INNER JOIN #UserInfo ui
                ON ui.Client_Id = ad.Client_Id
        WHERE
            icq.Collection_Start_Dt > '2019-01-01'
            AND (   @Invoice_Collection_Coordinator_Id = -1
                    OR  iccc.Invoice_Collection_Coordinator_User_Id = @Invoice_Collection_Coordinator_Id)
            AND (   @Invoice_Collection_Coordinator_Id = -1
                    OR  iccc.Invoice_Collection_Coordinator_User_Id = @Invoice_Collection_Coordinator_Id)
            AND (   @Invoice_Collection_Owner_Id = -1
                    OR  icac.Invoice_Collection_Owner_User_Id = @Invoice_Collection_Owner_Id)
            AND (   @Primary_Source = -1
                    OR  (   ics.Is_Primary = 1
                            AND ics.Invoice_Collection_Source_Cd = @Primary_Source))
            AND (   @Issue_Status_Cd = -2
                    OR  icil.Issue_Status_Cd = @Issue_Status_Cd)
            AND (   @Vendor_Id = -1
                    OR  vd.Account_Vendor_Id = @Vendor_Id)
            AND ad.Client_Type_Id <> @Exclude_Demo_Client_Type_Id
            AND icqs.Code_Value <> 'Archived'
        GROUP BY
            ad.Client_Name
            , ad.Country_Name
            , ad.State_Name
            , ad.Site_name
            , ad.Account_Number
            , icac.Invoice_Collection_Alternative_Account_Number
            , oui.FIRST_NAME + ' ' + oui.LAST_NAME
            , cui.FIRST_NAME + ' ' + cui.LAST_NAME
            , '' + REPLACE(CONVERT(NVARCHAR, icil.Collection_Start_Dt, 106), ' ', '') + ' to '
              + REPLACE(CONVERT(NVARCHAR, icil.Collection_End_Dt, 106), ' ', '') + ''
            , ISNULL(vd.Account_Vendor_Name, ad.Account_Vendor_Name)
            , Icsc.Code_Value
            , REPLACE(CONVERT(NVARCHAR, icil.Created_Ts, 106), ' ', '-')
            , icilr.FIRST_NAME + ' ' + icilr.LAST_NAME
            , DATEDIFF(dd, icil.Created_Ts, @Today_Dt)
            , REPLACE(CONVERT(NVARCHAR, icil.Last_Change_Ts, 106), ' ', '-')
            , DATEDIFF(dd, icil.Last_Change_Ts, @Today_Dt)
            , icilu.FIRST_NAME + ' ' + icilu.LAST_NAME
            , icilus.Code_Value
            , CASE WHEN icilus.Code_Value = 'Closed' THEN
                       REPLACE(CONVERT(NVARCHAR, icil.Last_Change_Ts, 106), ' ', '-')
                  ELSE ''
              END
            , icilt.Code_Value
            , icile.Code_Value
            , icileu.FIRST_NAME + ' ' + icileu.LAST_NAME
            , iciei.Event_Desc
            , iciee.Event_Desc
            , icil.Collection_Start_Dt
            , icowner.FIRST_NAME + ' ' + icowner.LAST_NAME
            , ui.User_List_CEM
            , ui.User_List_CEA
            , ui.User_List_CSA
        ORDER BY
            ad.Client_Name
            , ad.Site_name
            , ad.Account_Number
            , icil.Collection_Start_Dt;


        DROP TABLE #Vendor_Dtls;
        DROP TABLE
            #UserInfo
            , #CSA_UserInfo
            , #CEM_UserInfo
            , #CEA_UserInfo
            , #Invoice_Collection_Issue_Comments;
    END;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_IC_Issue] TO [CBMS_SSRS_Reports]
GO
