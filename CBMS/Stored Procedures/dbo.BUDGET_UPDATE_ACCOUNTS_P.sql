SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	cbms_prod.dbo.BUDGET_UPDATE_ACCOUNTS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_id       	varchar(10)	          	
	@session_id    	varchar(20)	          	
	@budget_id     	int   
	@Startindex		int             0
	@EndIndex		int             25

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC BUDGET_UPDATE_ACCOUNTS_P 350,1,20
	EXEC BUDGET_UPDATE_ACCOUNTS_P 116,1,30
	EXEC BUDGET_UPDATE_ACCOUNTS_P 226,10,25
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	HG			07/21/2010	Pagination added.
							vwSitename view replaced by base tables
 DMR		  09/10/2010 Modified for Quoted_Identifier
							

******/
CREATE PROCEDURE [dbo].[BUDGET_UPDATE_ACCOUNTS_P]
      @budget_id INT
     ,@Startindex INT = 1
     ,@EndIndex INT = 2147483647
AS 

BEGIN

      SET NOCOUNT ON ;
	    
      WITH  BUDGET_UPDATE_ACCOUNTS_Pagination
              AS ( SELECT
                        client.client_id
                       ,client.client_name
                       ,d.sitegroup_id division_id
                       ,d.Sitegroup_Name
                       ,s.site_id
                       ,RTRIM(ad.city) + ', ' + st.state_name + ' (' + s.site_name + ')' site_name
                       ,account.account_id
                       ,account.account_number
                       ,vendor.vendor_name
                       ,budget_account.budget_account_id
                       ,entity.entity_name AS service_level_char
                       ,budget_tariff_transport_vw.Tariff_Transport AS tariff_transport
                       ,budget_account.is_deleted isDeleted
                       ,account.not_managed AS not_managed
                       ,CASE WHEN budget.posted_by > 0 THEN 1
                             ELSE 0
                        END is_posted_to_dv
                       ,commodity_type.Commodity_name AS commodity_name
                       ,COUNT(1) OVER ( ) AS total
                       ,ROW_NUMBER() OVER ( ORDER BY d.Sitegroup_Name, s.site_name ) AS Row_Num
                   FROM
                        dbo.BUDGET
                        JOIN dbo.CLIENT ON client.client_id = budget.client_id
                        JOIN dbo.SITE s ON s.Client_id = client.Client_id
                        JOIN dbo.ADDRESS ad ON ad.address_id = s.primary_address_id
                        JOIN dbo.STATE st ON st.state_id = ad.state_id
                        JOIN dbo.Sitegroup d ON d.Sitegroup_Id = s.DIVISION_ID
                        JOIN dbo.ACCOUNT ON account.site_id = s.site_id
                        JOIN dbo.VENDOR ON vendor.vendor_id = account.vendor_id
                        JOIN dbo.BUDGET_TARIFF_TRANSPORT_VW ON budget_tariff_transport_vw.account_id = account.account_id
                                                               AND budget_tariff_transport_vw.commodity_type_id = budget.commodity_type_id
                        JOIN dbo.BUDGET_ACCOUNT_COMMODITY_VW account_commodity_vw ON account_commodity_vw.account_id = account.account_id
                                                                                     AND account_commodity_vw.commodity_type_id = budget.commodity_type_id
                        JOIN dbo.Commodity commodity_type ON commodity_type.Commodity_id = budget.commodity_type_id
                        JOIN dbo.ENTITY ON entity.entity_id = account.service_level_type_id
                        JOIN dbo.BUDGET_ACCOUNT ON account.account_id = budget_account.account_id
                                                   AND budget_account.budget_id = budget.budget_id
                   WHERE
                        budget.budget_id = @budget_id
                        AND budget_account.is_deleted = 0)
            SELECT
                  client_id
                 ,client_name
                 ,division_id
                 ,Sitegroup_Name
                 ,site_id
                 ,site_name
                 ,account_id
                 ,account_number
                 ,vendor_name
                 ,budget_account_id
                 ,service_level_char
                 ,tariff_transport
                 ,isDeleted
                 ,not_managed
                 ,is_posted_to_dv
                 ,commodity_name
                 ,total
            FROM
                  BUDGET_UPDATE_ACCOUNTS_Pagination
            WHERE
                  Row_Num BETWEEN @Startindex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[BUDGET_UPDATE_ACCOUNTS_P] TO [CBMSApplication]
GO
