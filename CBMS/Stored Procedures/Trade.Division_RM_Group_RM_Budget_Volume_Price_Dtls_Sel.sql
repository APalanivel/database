SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
Name:                          
        Trade.Division_RM_Group_RM_Budget_Volume_Price_Dtls_Sel                        
                          
Description:                          
        To get market price and forecast pirce if a selected index   
                          
Input Parameters:                          
    Name    DataType        Default     Description                            
--------------------------------------------------------------------------------    
	@Index_Id   INT    
    @Start_Dt	Date    
	@End_Dt		Date
                          
 Output Parameters:                                
	Name            Datatype        Default  Description                                
--------------------------------------------------------------------------------    
       
Usage Examples:                              
--------------------------------------------------------------------------------    
	SELECT * FROM dbo.ENTITY e WHERE e.ENTITY_TYPE=272

	EXEC Trade.Division_RM_Group_RM_Budget_Volume_Price_Dtls_Sel  584,1005,291,NULL,'2019-12-01','2019-12-01',25,3,'All Sites',1,NULL,NULL
    
Author Initials:                          
    Initials    Name                          
--------------------------------------------------------------------------------    
    RR          Raghu Reddy       
                           
 Modifications:                          
    Initials	Date        Modification                          
--------------------------------------------------------------------------------    
	RR			2019-11-29	RM-Budgets Enahancement - Created
	RR			2020-02-24	GRM-1725 Applied number conversion on hedged and un-hedged to remove decimal points
	RR			2020-02-24	GRM-1769 Added budget id input @Rm_Budget_Id                
******/
CREATE PROCEDURE [Trade].[Division_RM_Group_RM_Budget_Volume_Price_Dtls_Sel]
    (
        @Index_Id INT = NULL
        , @Client_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Country_Id NVARCHAR(MAX) = NULL
        , @Start_Dt DATE
        , @End_Dt DATE
        , @Uom_Id INT = NULL
        , @Currency_Unit_Id INT = NULL
        , @Participant_Sites VARCHAR(20) = 'All Sites'
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647
        , @Division_Id INT = NULL
        , @RM_Group_Id INT = NULL
        , @Rm_Budget_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @Index VARCHAR(200)
            , @Trade_Index_Id INT;

        CREATE TABLE #Participant_Sites
             (
                 Client_Hier_Id INT
             );


        CREATE TABLE #Tbl_Division_Forecast
             (
                 Sitegroup_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Forecast_Volume DECIMAL(28, 6)
             );

        CREATE TABLE #Tbl_Site_Forecast
             (
                 Service_Month DATE
                 , Client_Hier_Id INT
                 , Forecast_Volume DECIMAL(28, 6)
             );

        CREATE TABLE #Tbl_Site_Hedge
             (
                 Client_Hier_Id INT
                 , Index_Id INT
                 , Index_Name VARCHAR(200)
                 , Price_Index_Id INT
                 , Price_Point_Name VARCHAR(200)
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge DECIMAL(24, 3)
                 , Market_Price DECIMAL(28, 6)
                 , Hedge_Mode VARCHAR(200)
                 , Hedge_Mode_Type_Id INT
             );

        CREATE TABLE #Tbl_Division_Hedge
             (
                 Sitegroup_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Service_Month DATE
                 , Hedged_Volume DECIMAL(28, 6)
                 , WA_Hedge_Price DECIMAL(24, 3)
             );


        SELECT  @Index = e.ENTITY_NAME FROM dbo.ENTITY e WHERE  e.ENTITY_ID = @Index_Id;

        SELECT
            @Trade_Index_Id = e.ENTITY_ID
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_TYPE = 165
            AND (   (   e.ENTITY_NAME = 'Nymex'
                        AND @Index = 'NYMEX')
                    OR  (   e.ENTITY_NAME = 'CGPR'
                            AND @Index = 'AECO'));

        IF @Rm_Budget_Id IS NULL
            BEGIN
                INSERT INTO #Participant_Sites
                     (
                         Client_Hier_Id
                     )
                EXEC Trade.RM_Budget_Participant_Sites_Sel
                    @Client_Id = @Client_Id
                    , @Commodity_Id = @Commodity_Id
                    , @Country_Id = @Country_Id
                    , @Start_Dt = @Start_Dt
                    , @End_Dt = @End_Dt
                    , @Participant_Sites = @Participant_Sites
                    , @Division_Id = @Division_Id
                    , @Index_Id = @Index_Id
                    , @RM_Group_Id = @RM_Group_Id;
            END;
        ELSE
            BEGIN
                DELETE  FROM #Participant_Sites WHERE   @Rm_Budget_Id IS NOT NULL;
                INSERT INTO #Participant_Sites
                     (
                         Client_Hier_Id
                     )
                SELECT
                    rbp.Client_Hier_Id
                FROM
                    Trade.Rm_Budget_Participant rbp
                WHERE
                    rbp.Rm_Budget_Id = @Rm_Budget_Id
                    AND @Rm_Budget_Id IS NOT NULL;
            END;


        INSERT INTO #Tbl_Site_Hedge
             (
                 Client_Hier_Id
                 , Index_Id
                 , Index_Name
                 , Price_Index_Id
                 , Price_Point_Name
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge
                 , Market_Price
                 , Hedge_Mode
                 , Hedge_Mode_Type_Id
             )
        SELECT
            vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , SUM((CASE WHEN frq.Code_Value = 'Monthly' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END)
                       WHEN frq.Code_Value = 'Daily' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END) * dd.DAYS_IN_MONTH_NUM
                   END))
            , SUM((CASE WHEN frq.Code_Value = 'Monthly' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END)
                       WHEN frq.Code_Value = 'Daily' THEN
                  (CASE WHEN trdact.Code_Value = 'Buy' THEN vol.Total_Volume * cuc2.CONVERSION_FACTOR
                       WHEN trdact.Code_Value = 'Sell' THEN -vol.Total_Volume * cuc2.CONVERSION_FACTOR
                   END) * dd.DAYS_IN_MONTH_NUM
                   END) * tp.Trade_Price * cuc.CONVERSION_FACTOR) AS WA_Hedge
            , MAX((piv.INDEX_VALUE * cucm.CONVERSION_FACTOR) / cuc3.CONVERSION_FACTOR) AS Market_Price
            , e.ENTITY_NAME AS Hedge_Mode
            , dt.Hedge_Mode_Type_Id
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code frq
                ON frq.Code_Id = dt.Deal_Ticket_Frequency_Cd
            INNER JOIN dbo.ENTITY hdgtyp
                ON dt.Hedge_Type_Cd = hdgtyp.ENTITY_ID
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                ON dtch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl vol
                ON dt.Deal_Ticket_Id = vol.Deal_Ticket_Id
                   AND  dtch.Client_Hier_Id = vol.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc2
                ON cuc2.BASE_UNIT_ID = vol.Uom_Type_Id
                   AND  cuc2.CONVERTED_UNIT_ID = @Uom_Id
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = vol.Deal_Month
            INNER JOIN Trade.Trade_Price tp
                ON tp.Trade_Price_Id = vol.Trade_Price_Id
            INNER JOIN dbo.PRICE_INDEX pridx
                ON dt.Price_Index_Id = pridx.PRICE_INDEX_ID
                   AND  pridx.Commodity_Id = dt.Commodity_Id
            INNER JOIN dbo.ENTITY idx
                ON pridx.INDEX_ID = idx.ENTITY_ID
            INNER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = dt.Currency_Unit_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
                   AND  cuc.CONVERSION_DATE = vol.Deal_Month
                   AND  cuc.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
            INNER JOIN dbo.Code trdact
                ON dt.Trade_Action_Type_Cd = trdact.Code_Id
            INNER JOIN dbo.ENTITY e
                ON dt.Hedge_Mode_Type_Id = e.ENTITY_ID
            LEFT JOIN dbo.PRICE_INDEX_VALUE piv
                ON piv.PRICE_INDEX_ID = pridx.PRICE_INDEX_ID
                   AND  piv.INDEX_MONTH = vol.Deal_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc3
                ON pridx.VOLUME_UNIT_ID = cuc3.BASE_UNIT_ID
                   AND  cuc3.CONVERTED_UNIT_ID = @Uom_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cucm
                ON cucm.BASE_UNIT_ID = pridx.CURRENCY_UNIT_ID
                   AND  cucm.CONVERTED_UNIT_ID = @Currency_Unit_Id
                   AND  cucm.CONVERSION_DATE = vol.Deal_Month
                   AND  cucm.CURRENCY_GROUP_ID = ch.Client_Currency_Group_Id
        WHERE
            idx.ENTITY_ID = @Trade_Index_Id
            AND e.ENTITY_NAME = 'Index'
            AND vol.Deal_Month BETWEEN @Start_Dt
                               AND     @End_Dt
            AND dt.Commodity_Id = @Commodity_Id
            AND tp.Trade_Price IS NOT NULL
            AND ch.Client_Id = @Client_Id
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') c
                                   WHERE
                                        CAST(c.Segments AS INT) = ch.Country_Id))
            AND ch.Site_Id > 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Participant_Sites ps
                           WHERE
                                ps.Client_Hier_Id = ch.Client_Hier_Id)
        GROUP BY
            vol.Client_Hier_Id
            , idx.ENTITY_ID
            , idx.ENTITY_NAME
            , pridx.PRICE_INDEX_ID
            , pridx.PRICING_POINT
            , vol.Deal_Month
            , vol.Total_Volume
            , tp.Trade_Price
            , tp.Market_Price
            , cuc.CONVERSION_FACTOR
            , trdact.Code_Value
            , frq.Code_Value
            , dd.DAYS_IN_MONTH_NUM
            , e.ENTITY_NAME
            , dt.Hedge_Mode_Type_Id;

        INSERT INTO #Tbl_Site_Forecast
             (
                 Service_Month
                 , Client_Hier_Id
                 , Forecast_Volume
             )
        SELECT
            chf.Service_Month
            , ch.Client_Hier_Id
            , MAX(chf.Forecast_Volume * cuc.CONVERSION_FACTOR) AS Forecast_Volume
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Forecast_Volume chf
                ON chf.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = chf.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = @Uom_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND chf.Commodity_Id = @Commodity_Id
            AND (   @Division_Id IS NULL
                    OR  ch.Sitegroup_Id = @Division_Id)
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') c
                                   WHERE
                                        CAST(c.Segments AS INT) = ch.Country_Id))
            AND chf.Service_Month BETWEEN @Start_Dt
                                  AND     @End_Dt
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Participant_Sites ps
                           WHERE
                                ps.Client_Hier_Id = ch.Client_Hier_Id)
        GROUP BY
            chf.Service_Month
            , ch.Client_Hier_Id;

        INSERT INTO #Tbl_Division_Forecast
             (
                 Sitegroup_Name
                 , Client_Hier_Id
                 , Service_Month
                 , Forecast_Volume
             )
        SELECT
            div.Sitegroup_Name
            , div.Client_Hier_Id
            , dd.DATE_D
            , SUM(tcf.Forecast_Volume)
        FROM
            #Participant_Sites ps
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN Core.Client_Hier pssite
                ON pssite.Client_Hier_Id = ps.Client_Hier_Id
            INNER JOIN Core.Client_Hier div
                ON div.Sitegroup_Id = pssite.Sitegroup_Id
            LEFT JOIN #Tbl_Site_Forecast tcf
                ON tcf.Client_Hier_Id = pssite.Client_Hier_Id
                   AND  dd.DATE_D = tcf.Service_Month
        WHERE
            div.Sitegroup_Id > 0
            AND div.Site_Id = 0
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
        GROUP BY
            div.Sitegroup_Name
            , div.Client_Hier_Id
            , dd.DATE_D;

        INSERT INTO #Tbl_Division_Hedge
             (
                 Sitegroup_Name
                 , Client_Hier_Id
                 , Service_Month
                 , Hedged_Volume
                 , WA_Hedge_Price
             )
        SELECT
            div.Sitegroup_Name
            , div.Client_Hier_Id
            , dd.DATE_D
            , SUM(tch.Hedged_Volume) AS Hedged_Volume
            , SUM(tch.WA_Hedge) / NULLIF(SUM(tch.Hedged_Volume), 0) AS WA_Hedge_Price
        FROM
            #Participant_Sites ps
            CROSS JOIN meta.DATE_DIM dd
            INNER JOIN Core.Client_Hier pssite
                ON pssite.Client_Hier_Id = ps.Client_Hier_Id
            INNER JOIN Core.Client_Hier div
                ON div.Sitegroup_Id = pssite.Sitegroup_Id
            LEFT JOIN #Tbl_Site_Hedge tch
                ON tch.Client_Hier_Id = pssite.Client_Hier_Id
                   AND  dd.DATE_D = tch.Service_Month
        WHERE
            div.Sitegroup_Id > 0
            AND div.Site_Id = 0
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
        GROUP BY
            div.Sitegroup_Name
            , div.Client_Hier_Id
            , dd.DATE_D;


        SELECT
            ISNULL(tch.Sitegroup_Name, tcf.Sitegroup_Name) AS Division_RM_Group_Name
            , ISNULL(tch.Client_Hier_Id, tcf.Client_Hier_Id) AS Client_Hier_Id
            , dd.DATE_D AS Budget_Month
            , CAST(tcf.Forecast_Volume AS DECIMAL(22, 0)) AS Forecasted_Volume
            , CAST(tch.Hedged_Volume AS DECIMAL(22, 0)) AS Total_Volume_Hedged
            , tch.WA_Hedge_Price AS WACOG_Of_Hedges
            , CAST((tcf.Forecast_Volume - ISNULL(tch.Hedged_Volume, 0)) AS DECIMAL(22, 0)) AS Total_Volume_Unhedged
            , CAST(0 AS DECIMAL(22, 3)) AS Budget_Target_Price
        FROM
            meta.DATE_DIM dd
            INNER JOIN #Tbl_Division_Forecast tcf
                ON dd.DATE_D = tcf.Service_Month
            INNER JOIN #Tbl_Division_Hedge tch
                ON dd.DATE_D = tch.Service_Month
                   AND  tch.Client_Hier_Id = tcf.Client_Hier_Id
        WHERE
            dd.DATE_D BETWEEN @Start_Dt
                      AND     @End_Dt;




    END;

GO
GRANT EXECUTE ON  [Trade].[Division_RM_Group_RM_Budget_Volume_Price_Dtls_Sel] TO [CBMSApplication]
GO
