SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--exec BUDGET_GET_CD_ACCOUNTS_FOR_MANAGE_BUDGET_P 1 6
CREATE    PROCEDURE dbo.BUDGET_GET_CD_ACCOUNTS_FOR_MANAGE_BUDGET_P
	@budgetId int
	AS
	begin
		set nocount on

		select 	bacc.budget_account_id  
		from 	budget_account bacc, account a
		where 	bacc.budget_id = @budgetId
			and bacc.account_id = a.account_id	
			and a.service_level_type_id in(859,860)
		
		union all
		
		select 	bacc.budget_account_id 
		from 	budget_account bacc, account a,budget_transport_account_vw transport		
		where 	bacc.budget_id = @budgetId
			and bacc.account_id = a.account_id
			and transport.account_id = bacc.account_id	
			and a.service_level_type_id in(861,1024)

			
		
		

	end



			


			
			

		
			











GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_CD_ACCOUNTS_FOR_MANAGE_BUDGET_P] TO [CBMSApplication]
GO
