SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE       PROCEDURE dbo.UBM_GET_AVISTA_FEED_FREQUENCY_P
@frequency varchar(10)
AS
	set nocount on
	select ubm_client_id,client_name from UBM_FEED_FREQUENCY where 
	(frequency_type_id=890
	or frequency_type_id in (
	select case when @frequency='monday' then 891
	when @frequency='tuesday' then 892
	when @frequency='wednesday' then 893
	when @frequency='thursday' then 894
	when @frequency='friday' then 895
	when @frequency='saturday' then 896
	when @frequency='sunday' then 897
	end)) and ubm_id=1
	

GO
GRANT EXECUTE ON  [dbo].[UBM_GET_AVISTA_FEED_FREQUENCY_P] TO [CBMSApplication]
GO
