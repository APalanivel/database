SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE dbo.UBM_UPDATE_EFFECTIVE_INVOICE_STATUS_P
	@status varchar(200),
	@masterLogId int
AS
	set nocount on
	DECLARE @statusTypeId int

	select @statusTypeId=(select entity_id from entity where entity_name=@status and entity_type=656)

	update ubm_batch_master_log set status_type_id = @statusTypeId
					where ubm_batch_master_log_id = @masterLogId

GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_EFFECTIVE_INVOICE_STATUS_P] TO [CBMSApplication]
GO
