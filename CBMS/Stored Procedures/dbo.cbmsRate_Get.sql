SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsRate_Get

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@RateId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE PROCEDURE [dbo].[cbmsRate_Get]
(
	@MyAccountId int
	,@RateId int
)
AS
select r.rate_id
	,r.rate_name
	,c.client_name
	,addr.city
	,st.state_name
	,acct.account_number
	,m.meter_number
	,case when count(rc.rc_rate_comparison_id) > 0 then 'Yes' else 'No' end as rate_comparison
	,v.vendor_name

from rate r
left join rc_rate_comparison rc on rc.rate_id = r.rate_id
left join meter m on m.rate_id = r.rate_id
left join account acct on acct.account_id = m.account_id
left join vendor v on v.vendor_id = r.vendor_id
left join address addr on addr.address_id = m.address_id
left join state st on st.state_id = addr.state_id
left join site s on s.site_id = addr.address_parent_id
left join division d on d.division_id = s.division_id
left join client c on c.client_id = d.client_id
where r.rate_id = @rateid
group by r.rate_id, r.rate_name, c.client_name, addr.city, st.state_name, acct.account_number, m.meter_number, v.vendor_name
order by c.client_name, st.state_name, addr.city, acct.account_number, m.meter_number, r.rate_name
GO
GRANT EXECUTE ON  [dbo].[cbmsRate_Get] TO [CBMSApplication]
GO
