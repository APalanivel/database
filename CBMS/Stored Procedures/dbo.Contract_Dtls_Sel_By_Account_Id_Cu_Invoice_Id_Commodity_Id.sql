SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:            
  dbo.[Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id]           
            
DESCRIPTION:            
  to get the contract_dtls for the given utility and supplier account          
            
INPUT PARAMETERS:            
 Name     DataType    Default    Description            
-------------------------------------------------------------------------------------          
 @Account_Id   INT          
 @Cu_Invoice_Id   INT          
 @commodity_Id   INT          
          
OUTPUT PARAMETERS:            
 Name     DataType    Default    Description            
-------------------------------------------------------------------------------------          
            
USAGE EXAMPLES:            
-------------------------------------------------------------------------------------            
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 206098,18604132,290          
          
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 194774,16517207,290           
          
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 67861,13505316,291          
          
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 258880,25773598,290          
        
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 55262,32227387,290        
        
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 76404,32412669,291       
        
  exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id]      
      @Account_Id = 1271032      
     ,@Cu_Invoice_Id = 54395711      
     ,@commodity_Id = 290      
    
  exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id]      
      @Account_Id =1655913    
     ,@Cu_Invoice_Id =75254288    
     ,@commodity_Id = 290      
           
exec [Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] 1655527,75251851,290 
         
AUTHOR INITIALS:            
 Initials Name            
-------------------------------------------------------------------------------------            
 RKV  Ravi Kumar Vegesna         
 SP   Srinivas Patchava
 NM   Nagaraju Muppa         
             
MODIFICATIONS            
            
 Initials Date  Modification            
-------------------------------------------------------------------------------------            
 RKV  2016-01-27  Created for Maint-6964,contract_dtls for the given utility or supplier account          
 RKV        2018-04-13  Maint-7002,Added filter to show only the supplier contract details       
 RKV        2018-07-04  MAINT-7570,Modified the code to display only the supplier contract details if the account has both supplier and utility contracts.if it has only one contract will display the respective contract.      
 SP         2019-08-05  MAINT-9040 Modified the code to display the ascending order of contract start and end dates     
 SP         2019-11-01  MAINT-9496 modifies the code to display the invoice in between contract dates
 NM			2020-19-02  MAINT-9040 Added Order by clause in final select Query to display the contract start and end dates in ascending order.     
******/    
CREATE PROCEDURE [dbo].[Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id]    
    (    
        @Account_Id INT    
        , @Cu_Invoice_Id INT    
        , @commodity_Id INT = NULL    
    )    
AS    
    BEGIN    
    
        SET NOCOUNT ON;    
    
        CREATE TABLE #Contract_Dtls    
             (    
                 CONTRACT_ID INT    
                 , ED_CONTRACT_NUMBER VARCHAR(150)    
                 , CONTRACT_START_DATE DATETIME    
                 , CONTRACT_END_DATE DATETIME    
                 , Meter_State_Id INT    
                 , Invoice_Begin_Dt DATE    
                 , Invoice_End_Dt DATE    
                 , Contract_Recalc_Begin_Dt DATE    
                 , Contract_Recalc_End_Dt DATE    
                 , Updated_User VARCHAR(80)    
                 , Last_Change_Ts DATETIME    
             );    

        DECLARE @Contract_Id TABLE    
              (    
                  Contract_Id INT    
              );    

        INSERT INTO @Contract_Id    
             (    
                 Contract_Id    
             )    
        SELECT    
            c.CONTRACT_ID    
        FROM    
            Core.Client_Hier_Account cha    
            INNER JOIN Core.Client_Hier_Account chat    
                ON chat.Meter_Id = cha.Meter_Id    
INNER JOIN dbo.CONTRACT c    
                ON chat.Supplier_Contract_ID = c.CONTRACT_ID    
            INNER JOIN dbo.ENTITY e    
                ON e.ENTITY_ID = c.CONTRACT_TYPE_ID    
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism    
                ON cism.Account_ID = cha.Account_Id    
        WHERE    
            (   cha.Account_Id = @Account_Id    
                OR  chat.Account_Id = @Account_Id)    
            AND (   (cism.Begin_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                   AND     chat.Supplier_Meter_Disassociation_Date)    
                    OR  (cism.End_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                     AND     chat.Supplier_Meter_Disassociation_Date))    
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id    
            AND e.ENTITY_NAME = 'Supplier'    
            AND (   @commodity_Id IS NULL    
                    OR  cha.Commodity_Id = @commodity_Id);    

        INSERT INTO @Contract_Id    
             (    
                 Contract_Id    
             )    
        SELECT    
            c.CONTRACT_ID    
        FROM    
            Core.Client_Hier_Account cha    
            INNER JOIN Core.Client_Hier_Account chat    
                ON chat.Meter_Id = cha.Meter_Id    
            INNER JOIN dbo.CONTRACT c    
                ON chat.Supplier_Contract_ID = c.CONTRACT_ID    
            INNER JOIN dbo.ENTITY e    
                ON e.ENTITY_ID = c.CONTRACT_TYPE_ID    
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism    
                ON cism.Account_ID = cha.Account_Id    
        WHERE    
            (   cha.Account_Id = @Account_Id    
                OR  chat.Account_Id = @Account_Id)    
            AND (   (cism.Begin_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                   AND     chat.Supplier_Meter_Disassociation_Date)    
                    OR  (cism.End_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                     AND     chat.Supplier_Meter_Disassociation_Date))    
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id    
            AND e.ENTITY_NAME = 'Utility'    
            AND (   @commodity_Id IS NULL    
                    OR  cha.Commodity_Id = @commodity_Id)    
            AND NOT EXISTS (SELECT  1 FROM  @Contract_Id);    

        INSERT INTO #Contract_Dtls    
             (    
                 CONTRACT_ID    
                 , ED_CONTRACT_NUMBER    
                 , CONTRACT_START_DATE    
                 , CONTRACT_END_DATE    
                 , Meter_State_Id    
                 , Invoice_Begin_Dt    
                 , Invoice_End_Dt    
                 , Contract_Recalc_Begin_Dt    
                 , Contract_Recalc_End_Dt    
                 , Updated_User    
                 , Last_Change_Ts    
             )    
        SELECT    
            c.CONTRACT_ID    
            , c.ED_CONTRACT_NUMBER    
            , c.CONTRACT_START_DATE    
            , c.CONTRACT_END_DATE    
            , cha.Meter_State_Id    
            , cism.Begin_Dt AS Invoice_Begin_Dt    
            , cism.End_Dt AS Invoice_End_Dt    
            , ciacrc.Contract_Recalc_Begin_Dt    
            , ciacrc.Contract_Recalc_End_Dt    
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Updated_User    
            , ciacrc.Last_Change_Ts    
        FROM    
            Core.Client_Hier_Account cha    
            INNER JOIN Core.Client_Hier_Account chat    
                ON chat.Meter_Id = cha.Meter_Id    
            INNER JOIN dbo.CONTRACT c    
                ON chat.Supplier_Contract_ID = c.CONTRACT_ID    
            INNER JOIN @Contract_Id e    
                ON e.Contract_Id = c.CONTRACT_ID    
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism    
               ON cism.Account_ID = cha.Account_Id    
            LEFT OUTER JOIN(dbo.Cu_Invoice_Account_Commodity ciac    
                            INNER JOIN dbo.Cu_Invoice_Account_Commodity_Recalc_Contract ciacrc    
                                ON ciac.Cu_Invoice_Account_Commodity_Id = ciacrc.Cu_Invoice_Account_Commodity_Id)    
                ON cism.CU_INVOICE_ID = ciac.Cu_Invoice_Id    
                   AND  cism.Account_ID = ciac.Account_Id    
                   AND  ciac.Commodity_Id = cha.Commodity_Id    
                   AND  c.CONTRACT_ID = ciacrc.Contract_Id    
            LEFT JOIN dbo.USER_INFO ui    
                ON ui.USER_INFO_ID = ciacrc.Updated_User_Id    
        WHERE    
            (   cha.Account_Id = @Account_Id    
                OR  chat.Account_Id = @Account_Id)    
            AND (   (cism.Begin_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                   AND     chat.Supplier_Meter_Disassociation_Date)    
                    OR  (cism.End_Dt BETWEEN chat.Supplier_Meter_Association_Date    
                                     AND     chat.Supplier_Meter_Disassociation_Date))    
            AND cism.CU_INVOICE_ID = @Cu_Invoice_Id    
            AND (   @commodity_Id IS NULL    
                    OR  cha.Commodity_Id = @commodity_Id)    
        GROUP BY    
            c.CONTRACT_ID    
            , c.ED_CONTRACT_NUMBER    
            , c.CONTRACT_START_DATE    
            , c.CONTRACT_END_DATE    
            , cha.Meter_State_Id    
            , cism.Begin_Dt    
            , cism.End_Dt    
            , ciacrc.Contract_Recalc_Begin_Dt    
            , ciacrc.Contract_Recalc_End_Dt    
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME    
            , ciacrc.Last_Change_Ts    
        ORDER BY    
            c.CONTRACT_START_DATE    
            , c.CONTRACT_END_DATE;    

        SELECT    
            CONTRACT_ID    
            , ED_CONTRACT_NUMBER    
            , CONTRACT_START_DATE    
            , CONTRACT_END_DATE    
            , Meter_State_Id    
            , MIN(Invoice_Begin_Dt) AS Invoice_Begin_Dt    
            , MAX(Invoice_End_Dt) AS Invoice_End_Dt    
            , Contract_Recalc_Begin_Dt    
            , Contract_Recalc_End_Dt    
            , Updated_User    
            , Last_Change_Ts    
        FROM    
            #Contract_Dtls    
        GROUP BY    
            CONTRACT_ID    
            , ED_CONTRACT_NUMBER    
            , CONTRACT_START_DATE    
            , CONTRACT_END_DATE    
            , Meter_State_Id    
            , Contract_Recalc_Begin_Dt    
            , Contract_Recalc_End_Dt    
            , Updated_User    
            , Last_Change_Ts
   --------MAINT-9040 added Order by clause to display the data in ascending order
		ORDER BY    
			CONTRACT_START_DATE    
            , CONTRACT_END_DATE;  
    
    
        DROP TABLE #Contract_Dtls;    
    
    
    END;    
    
    
GO
GRANT EXECUTE ON  [dbo].[Contract_Dtls_Sel_By_Account_Id_Cu_Invoice_Id_Commodity_Id] TO [CBMSApplication]
GO
