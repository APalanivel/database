SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******    
NAME:    
      dbo.Client_Commodity_Analyst_Sel_By_Client_Commodity  
     
 DESCRIPTION:     
      Gets the Analyst for a given client.  
        
 INPUT PARAMETERS:    
 Name   DataType Default Description    
------------------------------------------------------------    
 @Client_Id      int                       
 @Commodity_Id   int  
   
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:    
------------------------------------------------------------  
  
 EXEC dbo.Client_Commodity_Analyst_Sel_By_Client_Commodity 235,290  
 EXEC dbo.Client_Commodity_Analyst_Sel_By_Client_Commodity 11231,291  
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------   
 AKR  Ashok Kumar Raju  
   
  
 MODIFICATIONS     
 Initials Date  Modification  
------------------------------------------------------------  
 AKR        2012-09-26  Created for POCO  
  
******/  
CREATE  PROCEDURE dbo.Client_Commodity_Analyst_Sel_By_Client_Commodity  
      (   
       @Client_Id INT  
      ,@Commodity_Id INT )  
AS   
BEGIN  
      SET nocount ON  
  
      SELECT  
            c.Client_Name  
           ,cca.Analyst_User_Info_Id
           ,cc.Client_Commodity_Id  
      FROM  
            dbo.CLIENT c  
            INNER JOIN Core.Client_Commodity cc  
                  ON c.CLIENT_ID = cc.Client_Id  
            INNER JOIN dbo.Code csc  
                  ON csc.Code_Id = cc.Commodity_Service_Cd  
            LEFT JOIN dbo.Client_Commodity_Analyst cca  
                  ON cc.Client_Commodity_Id = cca.Client_Commodity_Id  
      WHERE  
            c.CLIENT_ID = @client_id  
            AND cc.Commodity_Id = @Commodity_Id  
            AND csc.Code_Value = 'Invoice'    
                 
   
END  
;
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_Analyst_Sel_By_Client_Commodity] TO [CBMSApplication]
GO
