SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoProjectOwner_RemoveForOwner

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@project_id    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  PROCEDURE [dbo].[cbmsSsoProjectOwner_RemoveForOwner]
	( @MyAccountId int
	, @project_id int
	)
AS
BEGIN

	set nocount on

	  delete sso_project_owner_map
	   where sso_project_id = @project_id

--	exec cbmsPermissionInfo_GetMy @MyAccountId, @group_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectOwner_RemoveForOwner] TO [CBMSApplication]
GO
