SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Exception_Sel_By_Account_Id_Exception_Type_Cd       
              
Description:              
			This sproc to get all exceptions of a given account id.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Account_Id							INT					
    @Queue_Id							INT					NULL
                          
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
	SELECT * FROM dbo.Contract_Exception
	select * from code where code_id =102949
	
	EXEC dbo.Contract_Exception_Sel_By_Account_Id_Exception_Type_Cd 166737,102909
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy
	           
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
	NR				2019-07-23		Add Contract -
******/

CREATE PROCEDURE [dbo].[Contract_Exception_Sel_By_Account_Id_Exception_Type_Cd]
     (
         @Contract_Id INT
         , @Exception_Type_Cd INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ae.Contract_Exception_Id
        FROM
            dbo.Contract_Exception ae
            INNER JOIN dbo.Code c
                ON c.Code_Id = ae.Exception_Status_Cd
        WHERE
            ae.Contract_Id = @Contract_Id
            AND ae.Exception_Type_Cd = @Exception_Type_Cd
            AND c.Code_Value IN ( 'New', 'In Progress' )
        GROUP BY
            ae.Contract_Exception_Id;


    END;

GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_Sel_By_Account_Id_Exception_Type_Cd] TO [CBMSApplication]
GO
