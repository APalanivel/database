
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel]
           
DESCRIPTION:             
			To get bid responses placed by suppliers 
			
INPUT PARAMETERS:            
	Name			DataType	Default		Description  
---------------------------------------------------------------------------------  
	@Supplier_Id	INT
    @Contact_Id		INT
    @Sr_Rfp_Id		INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT top 10 * FROM dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ssci.USER_INFO_ID = ui.USER_INFO_ID
            INNER JOIN dbo.SR_RFP_EMAIL_LOG srel
                  ON scvm.SR_RFP_ID = srel.SR_RFP_ID
                     AND srel.TO_EMAIL_ADDRESS = ui.EMAIL_ADDRESS
        WHERE srel.EMAIL_TYPE_ID = 1196
    
    SELECT * FROM dbo.ENTITY WHERE ENTITY_ID IN ( SELECT DISTINCT EMAIL_TYPE_ID FROM dbo.SR_RFP_EMAIL_LOG WHERE SR_RFP_ID IS NOT NULL)
            	
	EXEC dbo.Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel 13163,616,1644
	EXEC dbo.Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel 13154,1816,890
	EXEC dbo.Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel 13176,3152,1333
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy
	NR			Narayana Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-18	Global Sourcing - Phase3 - GCS-530 Created
	NR			2016-07-19	GCS-1176 - added User_Info_Id in output list.
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel]
      ( 
       @Sr_Rfp_Id INT
      ,@Supplier_Id INT
      ,@Contact_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      SELECT
            ui.FIRST_NAME
           ,ui.EMAIL_ADDRESS
           ,srel.GENERATION_DATE
           ,srel.SR_RFP_EMAIL_LOG_ID
           ,ui.USER_INFO_ID
      FROM
            dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP scvm
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON scvm.SR_SUPPLIER_CONTACT_INFO_ID = ssci.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.USER_INFO ui
                  ON ssci.USER_INFO_ID = ui.USER_INFO_ID
            INNER JOIN dbo.SR_RFP_EMAIL_LOG srel
                  ON scvm.SR_RFP_ID = srel.SR_RFP_ID
                     AND srel.TO_EMAIL_ADDRESS = ui.EMAIL_ADDRESS
            INNER JOIN dbo.ENTITY typ
                  ON srel.EMAIL_TYPE_ID = typ.ENTITY_ID
      WHERE
            scvm.SR_RFP_ID = @Sr_Rfp_Id
            AND scvm.VENDOR_ID = @Supplier_Id
            AND scvm.SR_SUPPLIER_CONTACT_INFO_ID = @Contact_Id
            AND typ.ENTITY_TYPE = '1033'
            AND typ.ENTITY_NAME IN ( 'Email-035-RFPPostedByAttachment_SA_SupplierContact', 'Email-011-RFPPosted_SA_SupplierContact' )
      GROUP BY
            ui.FIRST_NAME
           ,ui.EMAIL_ADDRESS
           ,srel.GENERATION_DATE
           ,srel.SR_RFP_EMAIL_LOG_ID
           ,ui.USER_INFO_ID
      ORDER BY
            srel.GENERATION_DATE
      
END;


;
GO

GRANT EXECUTE ON  [dbo].[Sr_Rfp_Send_Rfp_Email_Log_Dtls_Sel] TO [CBMSApplication]
GO
