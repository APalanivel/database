SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
           
/******          
            
 NAME:  [dbo].[Client_App_Access_Role_Group_Info_Map_Ins]          
            
 DESCRIPTION:            
	    To insert the values into Client_App_Access_Role_Group_Info_Map  table.            
            
 INPUT PARAMETERS:            
           
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------         
 @GROUP_INFO_ID                      INT  
 @Assigned_User_Id                   INT  
 @Client_App_Access_Role_Id          INT     
             
 OUTPUT PARAMETERS:           
                  
 Name                               DataType            Default      Description            
---------------------------------------------------------------------------------------------------------------          
            
 USAGE EXAMPLES:                
---------------------------------------------------------------------------------------------------------------          
		 BEGIN TRAN    
		 EXEC dbo.Client_App_Access_Role_Group_Info_Map_Ins    
				@Client_App_Access_Role_Id =4  
			   ,@GROUP_INFO_ID=223  
			   ,@Assigned_User_Id =39134   
		 select * from [dbo].[Client_App_Access_Role_Group_Info_Map] where  Client_App_Access_Role_Id =4       
		 ROLLBACK TRAN            
           
 AUTHOR INITIALS:          
            
 Initials               Name            
---------------------------------------------------------------------------------------------------------------          
 NR                     Narayana Reddy              
             
 MODIFICATIONS:           
           
 Initials               Date            Modification          
---------------------------------------------------------------------------------------------------------------          
 NR                     2014-01-01      Created for RA Admin user management          
           
******/            
          
CREATE PROCEDURE dbo.Client_App_Access_Role_Group_Info_Map_Ins
      ( 
       @Client_App_Access_Role_Id INT
      ,@GROUP_INFO_ID INT
      ,@Assigned_User_Id INT )
AS 
BEGIN          
      SET NOCOUNT ON          
        
        
           
      INSERT      INTO dbo.Client_App_Access_Role_Group_Info_Map
                  ( 
                   Client_App_Access_Role_Id
                  ,GROUP_INFO_ID
                  ,Assigned_User_Id
                  ,Assigned_Ts )
      VALUES
                  ( 
                   @Client_App_Access_Role_Id
                  ,@GROUP_INFO_ID
                  ,@Assigned_User_Id
                  ,GETDATE() )          
      
                     
END 




;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Group_Info_Map_Ins] TO [CBMSApplication]
GO
