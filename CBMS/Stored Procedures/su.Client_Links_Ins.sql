SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*      
NAME:      
	Su.Client_Links_Ins      
 
DESCRIPTION:     
  
	Used to and insert into  CLIENT_LINKS table    
  
INPUT PARAMETERS:      
Name				DataType  Default Description      
------------------------------------------------------------      
@CLIENT_ID		INT,     
@LINK_TYPE_CD	INT,       
@LINK_URL		VARCHAR(255),    
@LINK_NAME		VARCHAR(100)    
            
OUTPUT PARAMETERS:      
Name				DataType  Default Description      
------------------------------------------------------------      


USAGE EXAMPLES:      
------------------------------------------------------------    
 
EXEC Su.Client_Links_Ins 11231,9,'http://www.hotmail.com','hotmail'  


AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
GB   Geetansu Behera      
CMH  Chad Hattabaugh       
HG   Hari     
SKA  Shobhit Kr Agrawal   


MODIFICATIONS       
Initials Date		Modification      
------------------------------------------------------------      
GB      created    
SKA		17-JUL-09	Modified as per coding standard
HG		08/21/2009	Removed the update statement from the procedure as we dont have Update functionality
					- Added Error message as a input for the sp usp_RethrowError 
    
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [Su].[Client_Links_Ins]      
   @CLIENT_ID INT,     
   @LINK_TYPE_CD INT,       
   @LINK_URL VARCHAR(255),    
   @LINK_NAME VARCHAR(100)    
       
AS      

BEGIN      
      
      
SET NOCOUNT ON;  

BEGIN TRY  
	BEGIN TRANSACTION	

		INSERT INTO dbo.CLIENT_LINKS (
			CLIENT_ID, 
			LINK_TYPE_CD, 
			LINK_URL, LINK_NAME
			)     
		VALUES (@CLIENT_ID,   
			@LINK_TYPE_CD,   
			@LINK_URL,   
			@LINK_NAME 
			)      

	COMMIT TRANSACTION  
END TRY   

BEGIN CATCH  
		ROLLBACK TRANSACTION  
		EXEC dbo.usp_RethrowError 'A link with the same name exists, please enter a different name'

END CATCH  

END
GO
GRANT EXECUTE ON  [su].[Client_Links_Ins] TO [CBMSApplication]
GO
