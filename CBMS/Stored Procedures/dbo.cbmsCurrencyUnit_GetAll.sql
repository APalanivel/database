SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCurrencyUnit_GetAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@CurrencyId    	int       	null      	
	@CurrencyUnitName	varchar(200)	null      	
	@Symbol        	varchar(10)	null      	
	@CountryId     	int       	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE procedure [dbo].[cbmsCurrencyUnit_GetAll]
( 
	@MyAccountId int 
	,@CurrencyId int = null
	,@CurrencyUnitName varchar(200) = null
	,@Symbol varchar(10) = null
	,@CountryId int = null
)
AS
BEGIN

	   select cu.currency_unit_id
		, cu.currency_unit_name
		, cu.symbol
		, cmap.country_id
		, co.country_name
	     from currency_unit cu
	left join currency_unit_country_map cmap on (cu.currency_unit_id = cmap.currency_unit_id)
	left join country co on co.country_id = cmap.country_id
	where cu.currency_unit_name like '%' + coalesce(@CurrencyUnitName, cu.currency_unit_name) + '%'
	and (cu.symbol = coalesce(@Symbol, cu.symbol) or (cu.symbol is null and @Symbol is null))
	and cu.currency_unit_id = coalesce(@CurrencyId, cu.currency_unit_id)
	and (cmap.country_id = coalesce(@CountryId, cmap.country_id) or (@CountryId is null and cmap.country_id is null))
	 order by currency_unit_name asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCurrencyUnit_GetAll] TO [CBMSApplication]
GO
