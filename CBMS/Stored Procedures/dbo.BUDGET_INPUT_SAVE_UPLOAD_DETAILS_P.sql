SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE      PROCEDURE dbo.BUDGET_INPUT_SAVE_UPLOAD_DETAILS_P 
	@userId varchar(10),
	@sessionId varchar(20),
	@budget_account_id int,	
	@cbmsImageId int,
	@flag bit

	AS
	begin
	set nocount on
if(@flag = 1)
begin 
	update 	budget_account 
	set 	rate_notes_id=@cbmsImageId
	where	budget_account_id = @budget_account_id
	
	
end
else if(@flag = 0)
begin
	update 	budget_account 
	set 	sourcing_notes_id=@cbmsImageId
	where 	budget_account_id = @budget_account_id
	
	
	
end
end







GO
GRANT EXECUTE ON  [dbo].[BUDGET_INPUT_SAVE_UPLOAD_DETAILS_P] TO [CBMSApplication]
GO
