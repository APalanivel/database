SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Client_Contact_Ins                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		INT
    @Batch_Status_Cd				INT
    @User_Info_Id					INT
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------

	DECLARE @Tvp_Deal_Ticket_Client_Contact AS [Trade].[Tvp_Deal_Ticket_Client_Contact] 
	INSERT INTO @Tvp_Deal_Ticket_Client_Contact VALUES (5420,18508)
	SELECT * FROM @Tvp_Deal_Ticket_Client_Contact
	EXEC Trade.Deal_Ticket_Client_Contact_Ins 288,@Tvp_Deal_Ticket_Client_Contact,16
	SELECT
		  *
	FROM
      Trade.Deal_Ticket_Client_Contact a
      INNER JOIN Trade.Deal_Ticket_Client_Contact_Client_Hier b
            ON a.Deal_Ticket_Client_Contact_Id = b.Deal_Ticket_Client_Contact_Id
	WHERE a.Deal_Ticket_Id = 288
		                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-14     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Client_Contact_Ins]
    (
        @Deal_Ticket_Id INT
        , @Tvp_Deal_Ticket_Client_Contact AS [Trade].[Tvp_Deal_Ticket_Client_Contact] READONLY
        , @User_Info_Id INT
        , @Approval_Cbms_Image_Id INT = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  @Approval_Cbms_Image_Id = NULLIF(@Approval_Cbms_Image_Id, 0);

        INSERT INTO Trade.Deal_Ticket_Client_Contact
             (
                 Deal_Ticket_Id
                 , Contact_Info_Id
                 , Approval_Cbms_Image_Id
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Deal_Ticket_Id
            , tvp.Contact_Info_Id
            , @Approval_Cbms_Image_Id
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        FROM
            @Tvp_Deal_Ticket_Client_Contact tvp
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                Trade.Deal_Ticket_Client_Contact dtcc
                           WHERE
                                dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
                                AND tvp.Contact_Info_Id = dtcc.Contact_Info_Id)
        GROUP BY
            tvp.Contact_Info_Id;

        UPDATE
            dtcc
        SET
            dtcc.Approval_Cbms_Image_Id = ISNULL(@Approval_Cbms_Image_Id, dtcc.Approval_Cbms_Image_Id)
        FROM
            Trade.Deal_Ticket_Client_Contact dtcc
            INNER JOIN @Tvp_Deal_Ticket_Client_Contact tvp
                ON tvp.Contact_Info_Id = dtcc.Contact_Info_Id
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id;

        INSERT INTO Trade.Deal_Ticket_Client_Contact_Client_Hier
             (
                 Deal_Ticket_Client_Contact_Id
                 , Client_Hier_Id
                 , Created_User_Id
                 , Created_Ts
             )
        SELECT
            dtcc.Deal_Ticket_Client_Contact_Id
            , tvp.Client_Hier_Id
            , @User_Info_Id
            , GETDATE()
        FROM
            Trade.Deal_Ticket_Client_Contact dtcc
            INNER JOIN @Tvp_Deal_Ticket_Client_Contact tvp
                ON dtcc.Contact_Info_Id = tvp.Contact_Info_Id
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Contact_Client_Hier ccch1
                               WHERE
                                    ccch1.Client_Hier_Id = tvp.Client_Hier_Id
                                    AND ccch1.Deal_Ticket_Client_Contact_Id = dtcc.Deal_Ticket_Client_Contact_Id)
        GROUP BY
            dtcc.Deal_Ticket_Client_Contact_Id
            , tvp.Client_Hier_Id;


        DELETE
        ccch
        FROM
            Trade.Deal_Ticket_Client_Contact_Client_Hier ccch
            INNER JOIN Trade.Deal_Ticket_Client_Contact dtcc
                ON ccch.Deal_Ticket_Client_Contact_Id = dtcc.Deal_Ticket_Client_Contact_Id
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
            AND (EXISTS (   SELECT
                                1
                            FROM
                                @Tvp_Deal_Ticket_Client_Contact tvp
                            WHERE
                                tvp.Client_Hier_Id = ccch.Client_Hier_Id
                                AND tvp.Contact_Info_Id <> dtcc.Contact_Info_Id));

        DELETE
        dtcc
        FROM
            Trade.Deal_Ticket_Client_Contact dtcc
        WHERE
            dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Contact_Client_Hier ccch
                               WHERE
                                    ccch.Deal_Ticket_Client_Contact_Id = dtcc.Deal_Ticket_Client_Contact_Id);



    --MERGE INTO Trade.Deal_Ticket_Client_Contact_Client_Hier AS tgt
    --      USING 
    --            ( SELECT
    --                  tvp.Client_Hier_Id
    --                 ,dtcc.Deal_Ticket_Client_Contact_Id
    --                 ,dtcc
    --              FROM
    --                  @Tvp_Deal_Ticket_Client_Contact tvp
    --                  INNER JOIN Trade.Deal_Ticket_Client_Contact dtcc
    --                        ON dtch.Contact_Info_Id = dtcc.Contact_Info_Id
    --                  INNER JOIN Trade.Deal_Ticket_Client_Contact_Client_Hier dtccch
    --                        ON tvp.Client_Hier_Id = dtccch.Client_Hier_Id

    --              WHERE
    --                  dtcc.Deal_Ticket_Id = @Deal_Ticket_Id
    --              GROUP BY
    --                  tvp.Client_Hier_Id
    --                 ,dtcc.Deal_Ticket_Client_Contact_Id ) AS src
    --      ON tgt.Deal_Ticket_Id = src.Deal_Ticket_Id
    --      WHEN MATCHED AND tgt.Contact_Info_Id <> src.Contact_Info_Id
    --            AND tgt.Deal_Ticket_Client_Contact_Id = src.Deal_Ticket_Client_Contact_Id
    --            THEN UPDATE
    --              SET 
    --                  tgt.Contact_Info_Id = src.Contact_Info_Id
    --                 ,tgt.Updated_User_Id = @User_Info_Id
    --                 ,tgt.Last_Change_Ts = GETDATE()
    --      WHEN NOT MATCHED BY TARGET 
    --            THEN
    --    INSERT
    --                  ( 
    --                   Deal_Ticket_Id
    --                  ,Contact_Info_Id
    --                  ,Created_User_Id
    --                  ,Created_Ts
    --                  ,Updated_User_Id
    --                  ,Last_Change_Ts )
    --              VALUES
    --                  ( 
    --                   @Deal_Ticket_Id
    --                  ,src.Contact_Info_Id
    --                  ,@User_Info_Id
    --             ,GETDATE()
    --                  ,@User_Info_Id
    --                  ,GETDATE() );

    -------- Tried merge for Trade.Deal_Ticket_Client_Contact_Client_Hier but not able accomplish all scenarios
    -------- Case: Site1 & Site2 previously mapped to contact C1, now Site2 moved to C2, look for possibilities to use merge




    END;



GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Client_Contact_Ins] TO [CBMSApplication]
GO
