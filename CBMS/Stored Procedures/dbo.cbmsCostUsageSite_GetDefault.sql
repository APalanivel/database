SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE [dbo].[cbmsCostUsageSite_GetDefault]
	( @MyAccountId int
	, @site_id int
	, @service_month datetime
	)
AS
BEGIN

	   select cu.cost_usage_site_id
		, cu.site_id
		, cu.service_month
		, cu.is_default
		, cu.el_is_complete
		, cu.ng_is_complete
		, cu.created_by_id
		, ui.username
		, cu.created_date	
		, cu.currency_unit_id
		, cu.el_unit_of_measure_type_id
		, cu.ng_unit_of_measure_type_id
		, cu.el_usage
		, cu.el_on_peak_usage
		, cu.el_off_peak_usage
		, cu.el_int_peak_usage
		, cu.el_demand
		, cu.el_marketer_cost
		, cu.el_utility_cost
		, cu.el_cost
		, cu.el_tax
		, cu.ng_usage
		, cu.ng_marketer_cost
		, cu.ng_utility_cost
		, cu.ng_cost
		, cu.total_marketer_cost
		, cu.total_utility_cost
		, cu.total_cost
		, cu.total_tax
	     from cost_usage_site cu
	     join user_info ui on ui.user_info_id = cu.created_by_id
	    where cu.site_id = @site_id
	      and cu.service_month = @service_month
	      and cu.is_default = 1

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCostUsageSite_GetDefault] TO [CBMSApplication]
GO
