
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
   
 dbo.Report_Contract_Expiration_Details  
   
DESCRIPTION:  
 This Procedure is to get all Contract Expiration Details.  
   
INPUT PARAMETERS:  
Name     DataType  Default Description  
-------------------------------------------------------------------------------------------  
 @Client_Id_List  VARCHAR(MAX)  
 @Commodity_Id_List  VARCHAR(MAX)  
 @Region_Id_List  VARCHAR(MAX)  
 @State_id_List   VARCHAR(MAX)  
 @Begin_date   DATETIME  
 @End_date    DATETIME  
 @Contract_type_id  INT  
 @Sourcing_Buyer_id_List VARCHAR(MAX)  
 @Sourcing_Manager_id_List VARCHAR(MAX)  
 @Account_Service_lvl VARCHAR(24)
   
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------  
  
EXEC dbo.Report_Contract_Expiration_Details   
      11231  
     ,'290,291'  
     ,'2,5,4,3'  
     ,'6,7,8,9'  
     ,'01/01/2008'  
     ,'12/01/2010'  
     ,'Supplier,Utility'  
     ,'18807,1094,12077,29,75,9896,5837,5150,7537,20713,21005,21461,18899,38,19560,8870,15170,73,16850,7536,18900,18021,6133, 16561,145,16980,51'  
     ,'38,54,105,120'  
     ,'859'  
       
  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
SSR   Sharad srivastava  
AKR   Ashok Kumar  
HG   Harihara Suthan Ganesan  
  
MODIFICATIONS  
Initials Date  Modification  
------------------------------------------------------------  
 SSR     01/18/2011 Created  
 LEC        02/14/2011  Modified - Removed     AND ch.Country_Name IN ( ''usa'', ''mexico'', ''canada'' )  
 AKR        2011-01-28 Modified the script to include Custom Analysts  
 HG   2013-05-02 Added INNER JOIN with code table to filter only the Cost & usages services mapped to the client in Core.Client_Commodity  
 AKR	2014-06-05  Modified to remove CHA.Supplier_Meter_Disassociation_Date >= CHA.Supplier_Account_End_Dt  condition and
                    moved Account service level filter to Report level.
 */  
CREATE PROC [dbo].[Report_Contract_Expiration_Details]
      ( 
       @Client_Id_List VARCHAR(MAX)
      ,@Commodity_Id_List VARCHAR(MAX)
      ,@Region_Id_List VARCHAR(MAX)
      ,@State_id_List VARCHAR(MAX)
      ,@Begin_date DATETIME
      ,@End_date DATETIME
      ,@Contract_Name VARCHAR(200)
      ,@Sourcing_Buyer_id_List VARCHAR(MAX)
      ,@Sourcing_Manager_id_List VARCHAR(MAX)
      ,@Account_Service_lvl VARCHAR(24) )
AS 
BEGIN  
           
      SET NOCOUNT ON                 
  
      DECLARE
            @Contract_Classification_id INT
           ,@Contract_Supplier_id INT
           ,@Contract_Utility_id INT
           ,@Default_Analyst INT
           ,@Custom_Analyst INT       
  
      DECLARE @SQL VARCHAR(MAX)  
      DECLARE @SQL_ext VARCHAR(MAX)  
  
      SELECT
            @Contract_Supplier_id = a.ENTITY_ID
      FROM
            dbo.Entity a
      WHERE
            a.ENTITY_Name = 'Supplier'
            AND a.Entity_Description = 'Contract Type'              
                 
      SELECT
            @Contract_Utility_id = a.ENTITY_ID
      FROM
            dbo.Entity a
      WHERE
            a.ENTITY_Name = 'Utility'
            AND a.Entity_Description = 'Contract Type'          
  
      SELECT
            @Contract_Classification_id = a.Entity_id
      FROM
            dbo.Entity a
      WHERE
            a.Entity_Name = 'Pipeline - Transportation'
            AND a.Entity_Description = 'Contract Classification'  
         
      SELECT
            @Default_Analyst = MAX(CASE WHEN c.Code_Value = 'Default' THEN c.Code_Id
                                   END)
           ,@Custom_Analyst = MAX(CASE WHEN c.Code_Value = 'Custom' THEN c.Code_Id
                                  END)
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'Analyst Type'       
         
                      
                            
      SET @SQL = ';                
    WITH    maxcontract_cte              
    AS ( SELECT               
                        samm.meter_id [MeterID]              
                      , max(contract_end_date) [MaxContractExp]              
                      , c.CONTRACT_TYPE_ID [ContractType]              
                      , c.Contract_Classification_Cd [Class]              
                       FROM              
                        dbo.CONTRACT c              
                        JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm              
                            ON samm.Contract_ID = c.CONTRACT_ID              
                        JOIN dbo.ACCOUNT a              
                            ON a.ACCOUNT_ID = samm.ACCOUNT_ID '         
                                    
      IF @Contract_Name <> 'Supplier,Utility' 
            BEGIN                              
                  SET @SQL = @SQL + ' WHERE              
        (  c.CONTRACT_TYPE_ID = CASE WHEN ''' + @Contract_Name + '''= ''Utility'' THEN ' + CAST(@Contract_Utility_id AS VARCHAR) + ' ELSE ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' END ' + ' AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1 '              
                  IF @Contract_Name = 'Utility' 
                        SET @SQL = @SQL + ' AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)         
            END        
      ELSE 
            BEGIN        
           
                  SET @SQL = @SQL + ' WHERE              
        (  c.CONTRACT_TYPE_ID = ' + CAST(@Contract_Supplier_id AS VARCHAR) + ' AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1 )        
     OR        
     (  c.CONTRACT_TYPE_ID =' + CAST(@Contract_Utility_id AS VARCHAR) + '        
                     AND c.COMMODITY_TYPE_ID in (' + CAST(@Commodity_Id_List AS VARCHAR) + ')' + ' AND samm.IS_HISTORY != 1         
     AND c.Contract_Classification_Cd = ' + CAST(@Contract_Classification_id AS VARCHAR)         
           
           
            END        
      SET @SQL = @SQL + ' )               
    GROUP BY                
   samm.METER_ID     
    , c.CONTRACT_TYPE_ID                
    , c.Contract_Classification_Cd                
  )     
      
  SELECT * INTO #Temp_Account_Details    
  FROM    
  (    
   SELECT                 
    c.Ed_contract_number [Contract Number]                
     , ch.Client_Name [Client]          
     ,ch.Client_Id    
     ,ch.Site_Id    
     ,ca.Account_Id          
     , ch.Sitegroup_Name [Division]                
     , ch.Site_name [Site]                
     , ch.City                
     , ca.Account_Number [Account Number]                
     , ch.state_Name [State]                
     , ch.Region_Name [Region]                
     , com.Commodity_Name [Commodity]                
     , ctyp.ENTITY_NAME [ContractType]                
     , cd.Code_Value [Contract Classification Type]                
     , v.VENDOR_NAME [Vendor]        
     , ca.Account_Vendor_Name [Utility]           
     ,ca.Account_Vendor_Id    
     ,Com.Commodity_Id         
     , c.CONTRACT_START_DATE [Contract Start Date]                
     , c.CONTRACT_end_DATE [Contract End Date]                
     , c.CONTRACT_END_DATE - ( NOTIFICATION_DAYS ) [Contract Notification Date]        
     ,coalesce(ca.Account_Analyst_Mapping_Cd, ch.Site_Analyst_Mapping_Cd, ch.Client_Analyst_Mapping_Cd) Analyst_Mapping_Cd             
  FROM                
    dbo.CONTRACT c       
    JOIN maxcontract_cte mc                
     ON mc.Class = c.Contract_Classification_Cd                
        AND mc.ContractType = c.CONTRACT_TYPE_ID                
        AND mc.MaxContractExp = c.CONTRACT_END_DATE                
    JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm                
     ON samm.METER_ID = mc.MeterID                
        AND samm.Contract_ID = c.CONTRACT_ID                
  JOIN Core.Client_Hier_Account ca                
     ON ca.Meter_Id = mc.MeterID                
        AND ca.Account_type = ''Utility''                
    JOIN Core.Client_Hier ch                
     ON ch.Client_Hier_Id = ca.Client_Hier_Id                
    JOIN dbo.ENTITY ctyp                
     ON ctyp.ENTITY_ID = c.contract_type_id                
    JOIN dbo.Code cd                
     ON cd.Code_Id = c.Contract_Classification_Cd                
    JOIN dbo.Commodity com                
     ON com.Commodity_Id = ca.Commodity_Id                
        AND com.Commodity_Id = c.COMMODITY_TYPE_ID                
    JOIN dbo.ACCOUNT a                
     ON a.ACCOUNT_ID = samm.ACCOUNT_ID                
    JOIN dbo.VENDOR v                
     ON v.VENDOR_ID = a.VENDOR_ID                   
   WHERE                
     ch.Client_Id in (' + @Client_Id_List + ') AND ch.State_id in (' + @State_id_List + ')                
      AND ch.Region_id in (' + @Region_Id_List + ')                
      AND ca.Account_Service_level_Cd IN  (' + @Account_Service_lvl + ') AND mc.MaxContractExp BETWEEN ' + '''' + CONVERT(VARCHAR(10), @Begin_date, 101) + ''' AND ' + '''' + CONVERT(VARCHAR(10), @End_date, 101) + '''                
      AND ch.Site_Id != 0                
    AND ch.Site_Not_Managed != 1     
    GROUP BY    
    c.Ed_contract_number                 
     , ch.Client_Name                 
     , ch.Sitegroup_Name                 
     , ch.Site_name                 
     , ch.City                
     , ca.Account_Number                 
     , ch.state_Name                 
     , ch.Region_Name                 
     , com.Commodity_Name                 
     , ctyp.ENTITY_NAME                 
     , cd.Code_Value                 
     , v.VENDOR_NAME         
     , ca.Account_Vendor_Name            
     ,ca.Account_Vendor_Id    
     ,Com.Commodity_Id         
     , c.CONTRACT_START_DATE                 
     , c.CONTRACT_end_DATE                 
     ,  NOTIFICATION_DAYS     
     ,ca.Account_Analyst_Mapping_Cd,     
     ch.Site_Analyst_Mapping_Cd,    
      ch.Client_Analyst_Mapping_Cd    
      ,ch.Client_Id    
     ,ch.Site_Id    
     ,ca.Account_Id    
        
     )    k           
        
      ;      
      WITH  cte_Account_User      
              AS ( SELECT      
               acc.[Contract Number]                
     , acc.[Client]                
     , acc.[Division]                
     , acc.[Site]                
     , acc.City                
     , acc.[Account Number]                
     , acc.[State]                
     , acc.[Region]                
     , acc.[Commodity]                
     , acc.[ContractType]                
     , acc.[Contract Classification Type]                
     , acc.[Vendor]        
     , acc.[Utility]           
     ,acc.Account_Vendor_Id    
     ,acc.Commodity_Id         
     , acc.[Contract Start Date]                
     , acc.[Contract End Date]                
     , acc.[Contract Notification Date]        
                     ,case WHEN acc.Analyst_Mapping_Cd = ' + CAST(@Default_Analyst AS VARCHAR) + ' THEN vcam.Analyst_Id      
                             WHEN acc.Analyst_Mapping_Cd = ' + CAST(@Custom_Analyst AS VARCHAR) + '  THEN coalesce(aca.Analyst_User_Info_Id, sca.Analyst_User_Info_Id, cca.Analyst_User_Info_Id)      
                        END AS Analyst_Id      
                   FROM      
                        #Temp_Account_Details acc      
                        INNER JOIN dbo.VENDOR_COMMODITY_MAP vcm      
                              ON acc.Account_Vendor_Id = vcm.VENDOR_ID      
                                 AND acc.Commodity_Id = vcm.COMMODITY_TYPE_ID      
                        INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam      
                              ON vcm.VENDOR_COMMODITY_MAP_ID = vcam.Vendor_Commodity_Map_Id      
                        INNER JOIN core.Client_Commodity ccc   
                              ON ccc.Client_Id = acc.Client_Id      
               AND ccc.Commodity_Id = acc.COMMODITY_ID  
      INNER JOIN dbo.Code cd  
       ON cd.Code_Id = ccc.Commodity_Service_Cd  
        AND cd.Code_Value = ''Invoice''  
                        LEFT OUTER JOIN dbo.Account_Commodity_Analyst aca  
                              ON aca.Account_Id = acc.Account_Id   
                                 AND aca.Commodity_Id = acc.COMMODITY_ID  
                        LEFT OUTER JOIN dbo.SITE_Commodity_Analyst sca  
                              ON sca.Site_Id = acc.Site_Id  
                                 AND sca.Commodity_Id = acc.COMMODITY_ID  
                        LEFT OUTER JOIN dbo.Client_Commodity_Analyst cca  
                              ON ccc.Client_Commodity_Id = cca.Client_Commodity_Id)  
            SELECT  DISTINCT  
                  cau.[Contract Number]  
     , cau.[Client]                
     , cau.[Division]                
     , cau.[Site]                
     , cau.City                
     , cau.[Account Number]                
     , cau.[State]                
     , cau.[Region]                
     , cau.[Commodity]                
     , cau.[ContractType]                
     , cau.[Contract Classification Type]                
     , cau.[Vendor]        
     , cau.[Utility]           
     ,cau.Account_Vendor_Id    
     ,cau.Commodity_Id         
     , cau.[Contract Start Date]                
     , cau.[Contract End Date]                
     , cau.[Contract Notification Date]        
           ,uia.first_name + space(1) + uia.last_name [Sourcing Buyer]    
           ,uim.first_name + space(1) + uim.last_name [Sourcing Manager]    
      FROM    
            cte_Account_User cau    
            INNER JOIN dbo.USER_INFO uia    
                  ON cau.Analyst_Id = uia.USER_INFO_ID    
            LEFT OUTER JOIN dbo.SR_SA_SM_MAP sm    
                  ON cau.Analyst_Id = sm.SOURCING_ANALYST_ID    
            LEFT OUTER JOIN dbo.USER_INFO uim    
                  ON uim.USER_INFO_ID = sm.SOURCING_MANAGER_ID    
                  AND sm.SOURCING_MANAGER_ID in ( ' + @Sourcing_Manager_id_List + ')     
            WHERE  cau.Analyst_Id IN ( ' + @Sourcing_Buyer_id_List + ')        
               
  '                
                
      EXEC   ( @SQL )         
               
END;  
  
;  
  
  
  
;
GO



GRANT EXECUTE ON  [dbo].[Report_Contract_Expiration_Details] TO [CBMS_SSRS_Reports]
GO
