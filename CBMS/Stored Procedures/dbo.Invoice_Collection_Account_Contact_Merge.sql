SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******                        
 NAME: dbo.Invoice_Collection_Account_Contact_Merge            
                        
 DESCRIPTION:                        
			To Update and insert data for Invoice_Collection_Account_Contact table                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@tvp_Invoice_Collection_Account_Contact				 tvp_Invoice_Collection_Account_Contact READONLY
@User_Info_Id					 INT 
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 --INSERT
 
 BEGIN TRAN
 
DECLARE @tvp_Invoice_Collection_Account_Contact tvp_Invoice_Collection_Account_Contact;  
  
 INSERT     INTO @tvp_Invoice_Collection_Account_Contact
            ( [Invoice_Collection_Account_Config_Id],  [Contact_Info_Id],[Is_Primary],[Is_CC] )
 VALUES
            ( 1,136,1,0 )
            ,
            ( 1,137,0,0 );  


 EXEC [dbo].[Invoice_Collection_Account_Contact_Merge] 
      @tvp_Invoice_Collection_Account_Contact = @tvp_Invoice_Collection_Account_Contact
     ,@User_Info_Id = 49
 SELECT
      *
 FROM
      dbo.Invoice_Collection_Account_Contact  
      
 ROLLBACK         

 
                        
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-25       Created for Invoice Tracking Phase I.   
 RKV                   2018-04-13       Maint 7160 Added code to delete all the contacts if the tvp passes contact_info_id = -1            
                       
******/

CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Contact_Merge]
     (
         @tvp_Invoice_Collection_Account_Contact tvp_Invoice_Collection_Account_Contact READONLY
         , @User_Info_Id INT
         , @Preserve_Existing_Value_When_Null BIT = 0
         , @Is_Replace_Bulk BIT = 0
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Invoice_Collection_Account_Config_Id INT
            , @Contact_Level_Cds VARCHAR(MAX) = '';

        SELECT  TOP 1
                @Invoice_Collection_Account_Config_Id = tvp.Invoice_Collection_Account_Config_Id
        FROM
            @tvp_Invoice_Collection_Account_Contact tvp;

        SELECT
            @Contact_Level_Cds = @Contact_Level_Cds + ',' + CAST(ci.Contact_Level_Cd AS VARCHAR)
        FROM
            @tvp_Invoice_Collection_Account_Contact tvp
            INNER JOIN dbo.Contact_Info ci
                ON ci.Contact_Info_Id = tvp.Contact_Info_Id
        WHERE
            @Preserve_Existing_Value_When_Null = 1;

        SELECT
            @Contact_Level_Cds = RIGHT(@Contact_Level_Cds, LEN(@Contact_Level_Cds) - 1)
        WHERE
            @Preserve_Existing_Value_When_Null = 1;

        --User selected Replace in bulk update remove all existing contacts for that level(client,account,vendor)            
        DELETE
        tgt
        FROM
            dbo.Invoice_Collection_Account_Contact tgt
            INNER JOIN dbo.Contact_Info ci
                ON tgt.Contact_Info_Id = ci.Contact_Info_Id
            INNER JOIN dbo.ufn_split(@Contact_Level_Cds, ',') uf
                ON ci.Contact_Level_Cd = uf.Segments
        WHERE
            @Is_Replace_Bulk = 1
            AND @Preserve_Existing_Value_When_Null = 1
            AND tgt.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id;

        --Update existing primary if its from bulk upload and contacts a primary contact in it
        UPDATE
            icac
        SET
            icac.Is_Primary = 0
        FROM
            dbo.Invoice_Collection_Account_Contact icac
            INNER JOIN dbo.Contact_Info ci
                ON icac.Contact_Info_Id = ci.Contact_Info_Id
        WHERE
            @Preserve_Existing_Value_When_Null = 1
            AND icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND @Is_Replace_Bulk = 0
            AND icac.Is_Primary = 1
            AND EXISTS (   SELECT
                                1
                           FROM
                                @tvp_Invoice_Collection_Account_Contact tvp
                                INNER JOIN dbo.Contact_Info ci1
                                    ON tvp.Contact_Info_Id = ci1.Contact_Info_Id
                           WHERE
                                tvp.Is_Primary = 1
                                AND ci.Contact_Level_Cd = ci1.Contact_Level_Cd);


        MERGE INTO dbo.Invoice_Collection_Account_Contact tgt
        USING
        (   SELECT
                tvp.Invoice_Collection_Account_Config_Id
                , tvp.Contact_Info_Id
                , @User_Info_Id AS User_Info_Id
                , tvp.Is_Primary
                , tvp.Is_CC
            FROM
                @tvp_Invoice_Collection_Account_Contact tvp) src
        ON (   tgt.Invoice_Collection_Account_Config_Id = src.Invoice_Collection_Account_Config_Id
               AND  tgt.Contact_Info_Id = src.Contact_Info_Id)
        WHEN NOT MATCHED BY TARGET AND src.Contact_Info_Id <> -1 THEN INSERT (Invoice_Collection_Account_Config_Id
                                                                              , Contact_Info_Id
                                                                              , Is_Primary
                                                                              , Is_Include_In_CC
                                                                              , Created_User_Id
                                                                              , Created_Ts
                                                                              , Updated_User_Id
                                                                              , Last_Change_Ts)
                                                                      VALUES
                                                                          (src.Invoice_Collection_Account_Config_Id
                                                                           , src.Contact_Info_Id
                                                                           , src.Is_Primary
                                                                           , src.Is_CC
                                                                           , src.User_Info_Id
                                                                           , GETDATE()
                                                                           , src.User_Info_Id
                                                                           , GETDATE())
        WHEN MATCHED THEN UPDATE SET
                              tgt.Is_Primary = src.Is_Primary
                              , tgt.Is_Include_In_CC = src.Is_CC
                              , tgt.Updated_User_Id = src.User_Info_Id
                              , tgt.Last_Change_Ts = GETDATE()
        WHEN NOT MATCHED BY SOURCE AND tgt.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
                                       AND  @Preserve_Existing_Value_When_Null = 0 THEN DELETE;




    END;



GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Contact_Merge] TO [CBMSApplication]
GO
