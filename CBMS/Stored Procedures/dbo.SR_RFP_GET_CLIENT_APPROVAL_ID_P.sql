SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_CLIENT_APPROVAL_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfp_id        	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.SR_RFP_GET_CLIENT_APPROVAL_ID_P
	@rfp_id int,
	@site_id int
	AS
set nocount on
	select 	max(approval.cbms_image_id) as cbms_image_id
	from 	sr_rfp_account rfp_account(nolock),
		sr_rfp_lp_client_approval approval(nolock),
		site s(nolock), 
		account a(nolock)
		 
	where	rfp_account.sr_rfp_id = @rfp_id
		and approval.sr_account_group_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and a.account_id = rfp_account.account_id
		and s.site_id = a.site_id
		and s.site_id = @site_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_CLIENT_APPROVAL_ID_P] TO [CBMSApplication]
GO
