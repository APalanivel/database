SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    dbo.cbmsRmDealTicket_Get
   
    
DESCRIPTION:   
   
    
INPUT PARAMETERS:    
    Name			DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Deal_Ticket_Id   INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec dbo.cbmsRmDealTicket_Get 0,132087
	Exec dbo.cbmsRmDealTicket_Get 0,100685
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR			Raghu Reddy   
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-06-01  GRM - Modified to refer new schema
     
    
******/  
CREATE PROCEDURE [dbo].[cbmsRmDealTicket_Get]
      ( 
       @MyAccountId INT
      ,@DealTicketNumber VARCHAR(20) = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
            
      SELECT
            dt.Deal_Ticket_Id AS rm_deal_ticket_id
           ,ch.Client_Name AS client_name
           ,CASE WHEN COUNT(DISTINCT ch.City) > 1 THEN 'Multiple'
                 ELSE MAX(ch.City)
            END AS city
           ,CASE WHEN COUNT(DISTINCT ch.State_Name) > 1 THEN 'Multiple'
                 ELSE MAX(ch.State_Name)
            END AS state_name
           ,REPLACE(CONVERT(VARCHAR(20), ( dt.Hedge_Start_Dt ), 106), ' ', '-') AS hedge_start_month
           ,REPLACE(CONVERT(VARCHAR(20), ( dt.Hedge_End_Dt ), 106), ' ', '-') AS hedge_end_month
           ,CASE WHEN MAX(reclc.DEAL_TICKET_NUMBER) IS NOT NULL THEN 'Yes'
                 ELSE 'No'
            END AS recalc
      FROM
            Trade.Deal_Ticket dt
            INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                  ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
            INNER JOIN Core.Client_Hier ch
                  ON ch.Client_Id = dt.Client_Id
                     AND ch.Client_Hier_Id = dtch.Client_Hier_Id
            LEFT JOIN dbo.RECALC_PHYSICAL_HEDGE reclc
                  ON reclc.DEAL_TICKET_NUMBER = dt.Deal_Ticket_Id
      WHERE
            dt.Deal_Ticket_Number = @DealTicketNumber
      GROUP BY
            dt.Deal_Ticket_Id
           ,ch.Client_Name
           ,dt.Hedge_Start_Dt
           ,dt.Hedge_End_Dt

END
GO
GRANT EXECUTE ON  [dbo].[cbmsRmDealTicket_Get] TO [CBMSApplication]
GO
