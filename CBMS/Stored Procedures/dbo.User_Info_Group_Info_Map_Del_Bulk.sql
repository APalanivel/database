SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
                 
/******                        
 NAME: dbo.User_Info_Group_Info_Map_Del_Bulk            
                        
 DESCRIPTION:                        
			To Bulk update the User_Info_Group_Info_Map table.                        
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Group_Info_Id_List        VARCHAR(MAX)       
 @User_Info_Id_List         VARCHAR(MAX)       
                        
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 BEGIN TRAN
           SELECT
            *
           FROM
            dbo.USER_INFO_GROUP_INFO_MAP ui
            INNER JOIN dbo.USER_INFO u
                  ON ui.USER_INFO_ID = u.USER_INFO_ID
           WHERE
            u.CLIENT_ID = 235
            AND GROUP_INFO_ID IN ( 211, 212 )
            AND u.USER_INFO_ID IN ( 182, 9029 )
		      
           EXEC [dbo].[User_Info_Group_Info_Map_Del_Bulk] 
            @Group_Info_Id_List = '211,212'
           ,@User_Info_Id_List = '182,9029'

           SELECT
            *
           FROM
            dbo.USER_INFO_GROUP_INFO_MAP ui
            INNER JOIN dbo.USER_INFO u
                  ON ui.USER_INFO_ID = u.USER_INFO_ID
           WHERE
            u.CLIENT_ID = 235
            AND GROUP_INFO_ID IN ( 211, 212 )
            AND u.USER_INFO_ID IN ( 182, 9029 )
 ROLLBACK	      

           EXEC [dbo].[User_Info_Group_Info_Map_Ins_Bulk] 
            @Group_Info_Id_List = '211,212'
           ,@User_Info_Id_List = '182,9029'
                            
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2014-04-23       Created                
                       
******/                 
 

 
CREATE PROCEDURE [dbo].[User_Info_Group_Info_Map_Del_Bulk]
      ( 
       @Group_Info_Id_List VARCHAR(MAX)
      ,@User_Info_Id_List VARCHAR(MAX) )
AS 
BEGIN                
      SET NOCOUNT ON;         
                    
      DECLARE @Group_Info_Id_Tbl TABLE
            ( 
             Group_Info_Id INT PRIMARY KEY CLUSTERED ) 

      DECLARE @User_Info_Id_Tbl TABLE
            ( 
             User_Info_Id INT PRIMARY KEY CLUSTERED )                                 
                  
                  
      INSERT      INTO @Group_Info_Id_Tbl
                  ( 
                   Group_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@Group_Info_Id_List, ',') ufn      


      INSERT      INTO @User_Info_Id_Tbl
                  ( 
                   User_Info_Id )
                  SELECT
                        ufn.Segments
                  FROM
                        dbo.ufn_split(@User_Info_Id_List, ',') ufn                                     
  
      BEGIN TRY                        
            BEGIN TRANSACTION     
              
            DELETE
                  ugm
            FROM
                  @Group_Info_Id_Tbl urr
                  CROSS APPLY @User_Info_Id_Tbl ui
                  INNER JOIN dbo.User_Info_Group_Info_Map ugm
                        ON ugm.Group_Info_Id = urr.Group_Info_Id
                           AND ugm.User_Info_Id = ui.User_Info_Id 
                 
                       
            COMMIT TRANSACTION                                 
                                   
                                   
      END TRY                    
      BEGIN CATCH                    
            IF @@TRANCOUNT > 0 
                  BEGIN    
                        ROLLBACK TRANSACTION    
                  END                   
            EXEC dbo.usp_RethrowError                    
      END CATCH  
                       
END 


;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Group_Info_Map_Del_Bulk] TO [CBMSApplication]
GO
