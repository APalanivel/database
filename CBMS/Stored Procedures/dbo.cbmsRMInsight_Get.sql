SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    procedure [dbo].[cbmsRMInsight_Get]
	( @MyAccountId int
	, @RMInsightId int
	)
as
begin
SELECT rm_risk_insight_id 
	,insight_date 
	,insight_title
	,insight_text
	,author_id
	,ui.first_name + ' ' + ui.last_name [author]

FROM RM_RISK_INSIGHT rri
	join user_info ui on ui.user_info_id = rri.author_id
where rm_risk_insight_id = @RMInsightId


end
GO
GRANT EXECUTE ON  [dbo].[cbmsRMInsight_Get] TO [CBMSApplication]
GO
