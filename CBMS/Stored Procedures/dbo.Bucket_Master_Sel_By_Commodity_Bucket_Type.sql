
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	
	dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type

DESCRIPTION:


INPUT PARAMETERS:
	Name				DataType		Default	Description
-------------------------------------------------------------------------------------------
	@Commodity_Id		int  					indicates commodity type(water or kerosene)
	@bucket_Type_Code	VARCHAR(25) 			indicates which kind of bubket(chnarges or billing determinats)
	@Is_Shown_on_Site	BIT				NULL
	@Is_Shown_On_Account BIT			NULL
	@Is_Shown_On_Invoice BIT			NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
-------------------------------------------------------------
	EXEC dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type 67, 'Charge',NULL,1,NULL
	EXEC dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type 67, NULL,NULL,1,NULL

	EXEC dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type 290, 'Charge',@Is_Shown_On_Site = 1
	EXEC dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type 290, 'Determinant',@Is_Shown_On_Site = 1

	EXEC dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type 290

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/28/2010	Created
	HG			3/5/2010	Order By column modified to sort the bucket type in descending order.
	DMR			09/10/2010	Modified for Quoted_Identifier
	HG			07/27/2011	As a part of additional data enhancement added Is_Active  = 1 filter condition to filter only the active buckets from bucket master.
								-- Sort_Order , default_uom_type_id and Default_Uom_Type_id column included to the select list.
*/

CREATE PROCEDURE dbo.Bucket_Master_Sel_By_Commodity_Bucket_Type
      @CommodityId INT
     ,@bucket_Type_Cd_Value VARCHAR(25) = NULL
     ,@Is_Shown_on_Site BIT = NULL
     ,@Is_Shown_On_Account BIT = NULL
     ,@Is_Shown_On_Invoice BIT = NULL
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            bm.Bucket_Master_Id
           ,bm.Bucket_Name
           ,bkt_type.Code_Value AS Bucket_Type
           ,bm.Is_Shown_on_Site
           ,bm.Is_Shown_On_Account
           ,bm.Is_Shown_On_Invoice
           ,bm.Sort_Order
           ,bm.Default_Uom_Type_Id
      FROM
            dbo.Bucket_Master bm
            INNER JOIN dbo.Code bkt_type
                  ON bkt_type.Code_id = bm.Bucket_Type_Cd
      WHERE
            bm.Commodity_Id = @commodityId
            AND bm.Is_Active = 1
            AND ( @bucket_Type_Cd_Value IS NULL
                  OR bkt_type.Code_Value = @bucket_Type_Cd_Value )
            AND ( @Is_Shown_on_Site IS NULL
                  OR bm.Is_Shown_on_Site = @Is_Shown_on_Site )
            AND ( @Is_Shown_On_Account IS NULL
                  OR bm.is_Shown_On_Account = @Is_Shown_On_Account )
            AND ( @Is_Shown_On_Invoice IS NULL
                  OR bm.Is_Shown_On_Invoice = @Is_Shown_On_Invoice )
      ORDER BY
            bkt_type.Code_Value DESC
           ,bm.Bucket_Name
 
END
;
GO

GRANT EXECUTE ON  [dbo].[Bucket_Master_Sel_By_Commodity_Bucket_Type] TO [CBMSApplication]
GO
