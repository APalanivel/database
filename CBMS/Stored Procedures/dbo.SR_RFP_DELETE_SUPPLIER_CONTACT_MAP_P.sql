SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_DELETE_SUPPLIER_CONTACT_MAP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpId int,
@contactId int,
@vendorId int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test Delete an entry
--EXEC SR_RFP_DELETE_SUPPLIER_CONTACT_MAP_P
--@rfpId =5,
--@contactId =73,
--@vendorId = 805

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_DELETE_SUPPLIER_CONTACT_MAP_P
@rfpId int,
@contactId int,
@vendorId int

AS
	
SET NOCOUNT ON


DELETE	SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
WHERE	SR_RFP_ID = @rfpId and
		SR_SUPPLIER_CONTACT_INFO_ID = @contactId and
		VENDOR_ID = @vendorId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SUPPLIER_CONTACT_MAP_P] TO [CBMSApplication]
GO
