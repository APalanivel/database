SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.SR_RFP_GET_GROUP_LP_IMAGE_ID_P
	@site_id int,
	@rfp_id int
	AS
	set nocount on	
	select cbms_image_id from sr_rfp_lp_client_approval(nolock) where site_id = @site_id and is_group_lp = 1
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_GROUP_LP_IMAGE_ID_P] TO [CBMSApplication]
GO
