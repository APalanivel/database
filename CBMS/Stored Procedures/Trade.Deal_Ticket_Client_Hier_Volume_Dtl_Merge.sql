SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        Trade.Deal_Ticket_Client_Hier_Volume_Dtl_Merge                    
                      
Description:                      
        
                      
Input Parameters:                      
    Name				DataType        Default     Description                        
--------------------------------------------------------------------------------
	@Group_Name			VARCHAR(100)
    @Client_Id			INT
    @RM_Group_Type_Cd	INT
    @User_Info_Id		INT   
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
	    
                     
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy        
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-11-16     Global Risk Management - Created                    
                     
******/
CREATE PROCEDURE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl_Merge]
    (
        @Deal_Ticket_Id INT
        , @Trade_tvp_Deal_Ticket_Client_Hier_Volume AS Trade.tvp_Deal_Ticket_Client_Hier_Volume READONLY
        , @Uom_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Deal_Ticket_Frequency VARCHAR(25);

        SELECT
            @Deal_Ticket_Frequency = cd.Code_Value
        FROM
            Trade.Deal_Ticket dt
            INNER JOIN dbo.Code cd
                ON cd.Code_Id = dt.Deal_Ticket_Frequency_Cd
        WHERE
            Deal_Ticket_Id = @Deal_Ticket_Id;

        DECLARE @Tbl_Deal_Ticket_Client_Hier_Volume AS TABLE
              (
                  Client_Hier_Id INT NOT NULL
                  , Account_Id INT NOT NULL
                  , Deal_Month DATE NOT NULL
                  , Total_Volume DECIMAL(24, 2) NOT NULL
                  , Contract_Id INT NULL
              );

        INSERT INTO @Tbl_Deal_Ticket_Client_Hier_Volume
             (
                 Client_Hier_Id
                 , Account_Id
                 , Deal_Month
                 , Total_Volume
                 , Contract_Id
             )
        SELECT
            dtv.Client_Hier_Id
            , dtv.Account_Id
            , dtv.Deal_Month
            , dtv.Total_Volume AS Total_Volume
            , dtv.Contract_Id
        FROM
            @Trade_tvp_Deal_Ticket_Client_Hier_Volume dtv
            INNER JOIN meta.DATE_DIM dd
                ON dd.DATE_D = dtv.Deal_Month
        WHERE
            Total_Volume >= 0;

        DELETE
        dtchv
        FROM
            @Tbl_Deal_Ticket_Client_Hier_Volume dtchv
        WHERE
            EXISTS (   SELECT
                            tdtchv.Client_Hier_Id
                       FROM
                            @Tbl_Deal_Ticket_Client_Hier_Volume tdtchv
                       WHERE
                            dtchv.Client_Hier_Id = tdtchv.Client_Hier_Id
                       GROUP BY
                           tdtchv.Client_Hier_Id
                       HAVING
                            (SUM(tdtchv.Total_Volume)) <= 0)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtchvd
                               WHERE
                                    dtchvd.Deal_Ticket_Id = @Deal_Ticket_Id
                                    AND dtchvd.Client_Hier_Id = dtchv.Client_Hier_Id);

        MERGE INTO Trade.Deal_Ticket_Client_Hier_Volume_Dtl AS tgt
        USING
        (   SELECT
                @Deal_Ticket_Id AS Deal_Ticket_Id
                , Client_Hier_Id
                , Account_Id
                , Deal_Month
                , MIN(Total_Volume) AS Total_Volume
                , Contract_Id
                , @Uom_Id AS Uom_Type_Id
            FROM
                @Tbl_Deal_Ticket_Client_Hier_Volume
            GROUP BY
                Client_Hier_Id
                , Account_Id
                , Deal_Month
                , Contract_Id) AS src
        ON tgt.Deal_Ticket_Id = src.Deal_Ticket_Id
           AND  tgt.Client_Hier_Id = src.Client_Hier_Id
           AND  tgt.Account_Id = src.Account_Id
           AND  tgt.Deal_Month = src.Deal_Month
           AND  ISNULL(tgt.Contract_Id, -1) = ISNULL(src.Contract_Id, -1)
        WHEN MATCHED THEN UPDATE SET
                              tgt.Total_Volume = src.Total_Volume
                              , tgt.Uom_Type_Id = src.Uom_Type_Id
                              , tgt.Updated_User_Id = @User_Info_Id
                              , tgt.Last_Change_Ts = GETDATE()
        WHEN NOT MATCHED BY TARGET THEN INSERT (Deal_Ticket_Id
                                                , Client_Hier_Id
                                                , Account_Id
                                                , Deal_Month
                                                , Total_Volume
                                                , Uom_Type_Id
                                                , Created_User_Id
                                                , Created_Ts
                                                , Updated_User_Id
                                                , Last_Change_Ts
                                                , Contract_Id)
                                        VALUES
                                            (@Deal_Ticket_Id
                                             , src.Client_Hier_Id
                                             , src.Account_Id
                                             , src.Deal_Month
                                             , src.Total_Volume
                                             , src.Uom_Type_Id
                                             , @User_Info_Id
                                             , GETDATE()
                                             , @User_Info_Id
                                             , GETDATE()
                                             , src.Contract_Id);

        INSERT INTO Trade.Deal_Ticket_Client_Hier
             (
                 Deal_Ticket_Id
                 , Client_Hier_Id
                 , Created_Ts
             )
        SELECT
            dtl.Deal_Ticket_Id
            , dtl.Client_Hier_Id
            , GETDATE()
        FROM
            Trade.Deal_Ticket_Client_Hier_Volume_Dtl dtl
        WHERE
            dtl.Deal_Ticket_Id = @Deal_Ticket_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.Deal_Ticket_Client_Hier dtch
                               WHERE
                                    dtl.Deal_Ticket_Id = dtch.Deal_Ticket_Id
                                    AND dtl.Client_Hier_Id = dtch.Client_Hier_Id)
        GROUP BY
            dtl.Deal_Ticket_Id
            , dtl.Client_Hier_Id;


    END;




GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl_Merge] TO [CBMSApplication]
GO
