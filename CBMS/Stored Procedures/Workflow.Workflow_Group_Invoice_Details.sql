SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******              
NAME:    [workflow].[Workflow_Group_Invoice_Details]          
DESCRIPTION:  it'll Return the User & System group invoice details          
------------------------------------------------------------             
 INPUT PARAMETERS:              
  @Invoice_ID INT       
------------------------------------------------------------              
 OUTPUT PARAMETERS:              
 Name   DataType  Default Description              
------------------------------------------------------------              
 USAGE EXAMPLES:     

 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =5471016
 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =15336000
 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =71759652     
 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =74334450     
 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =74580349  
 EXEC    [workflow].[Workflow_Group_Invoice_Details]   @Invoice_ID  =74844019  
------------------------------------------------------------              
AUTHOR INITIALS:              
Initials Name              
------------------------------------------------------------              
TRK   Ramakrishna Thummala Summit Energy           
           
 MODIFICATIONS               
 Initials Date   Modification              
------------------------------------------------------------              
 TRK    09-OCT-2019  Created          
          
******/     
CREATE PROCEDURE [Workflow].[Workflow_Group_Invoice_Details]    
(    
@Invoice_ID INT     
)    
AS    
BEGIN    
    
	DECLARE	@CNT INT,
			@Systemgroupid INT     
    
  CREATE TABLE #DEFAULTEXCEPTIONS                    
        (                              
            Systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] [INT] NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                                      
			[DATE_IN_QUEUE] [DATETIME] NULL                  
                          
        );

CREATE TABLE #DEFAULTEXCEPTIONS_SYSTEM_GROUP                   
        (                              
            Systemgroupid INT NULL,                    
            SystemGroupCount INT NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] [INT] NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                                      
			[DATE_IN_QUEUE] [DATETIME] NULL                  
                          
        );

CREATE TABLE #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL                    
        (                              
            Systemgroupid INT NULL,                    
            SystemGroupName NVARCHAR(2000) NULL,                    
            ubmid INT NULL,                    
            ubmaccountcode VARCHAR(200) NULL,                    
            [RN] [BIGINT] NULL,                    
            [CU_INVOICE_ID] [INT] NULL,                    
            [ACCOUNT] [VARCHAR](MAX) NULL,                    
            [Account_Id] [INT] NULL,                    
            [EXCEPTION_TYPE] [VARCHAR](MAX) NULL,                    
            [EXCEPTION_STATUS_TYPE] [VARCHAR](MAX) NULL,                    
            [ASSIGNEE] [VARCHAR](MAX) NULL,                    
            [CLIENTID] INT NULL,                    
            [CLIENT] [VARCHAR](MAX) NULL,                                      
			[DATE_IN_QUEUE] [DATETIME] NULL                  
                          
        );
 
	 INSERT #DEFAULTEXCEPTIONS                      
		SELECT                      
	   NULL,                      
	   NULL,                    
		CI.UBM_ID,                    
		CI.UBM_ACCOUNT_CODE,                    
		 ROW_NUMBER ()OVER(PARTITION BY CUEX.CU_INVOICE_ID ORDER BY (SELECT NULL)    ) AS RN,                       
	   CUEX.CU_INVOICE_ID                      
		, ( CASE WHEN CUEX.EXCEPTION_TYPE = 'WRONG ACCOUNT' THEN NULL                      
				 WHEN AG.ACCOUNT_GROUP_ID IS NULL   THEN COALESCE(CHA.Account_Number, CUEX.UBM_Account_Number,INV_LBL.ACCOUNT_NUMBER)                      
				 ELSE  COALESCE(AG.GROUP_BILLING_NUMBER, CHA.ACCOUNT_NUMBER, CUEX.UBM_ACCOUNT_NUMBER,INV_LBL.account_number)                          
			END ) AS ACCOUNT                 
		,(CASE WHEN CUEX.EXCEPTION_TYPE = 'WRONG ACCOUNT' THEN NULL                        
			WHEN AG.ACCOUNT_GROUP_ID IS NULL   THEN COALESCE(CHA.Account_Id, CUEX.Account_Id)                      
		  ELSE  COALESCE(CHA.Account_Id, CUEX.Account_Id ,AG.ACCOUNT_GROUP_ID)                            
			END ) AS ACCOUNT_ID                     
		, CUEX.EXCEPTION_TYPE                      
		, CUEX.EXCEPTION_STATUS_TYPE                       
		, UI.FIRST_NAME + ' '+ UI.LAST_NAME ASSIGNEE                      
	   , CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL                      
					 THEN CH.Client_Id                      
			   ELSE  NULL                      
		  END CLIENT_ID                      
		, CASE WHEN CUEX.CLIENT_HIER_ID IS NOT NULL  THEN CH.CLIENT_NAME                      
			   ELSE  ISNULL(CUEX.UBM_CLIENT_NAME , INV_LBL.CLIENT_NAME)                    
		  END CLIENT                                             
		, CUEX.DATE_IN_QUEUE       
	                                 
	FROM  DBO.CU_EXCEPTION_DENORM CUEX  WITH (NOLOCK)                      
	INNER JOIN   DBO.CU_INVOICE CI WITH (NOLOCK)  ON CI.CU_INVOICE_ID = CUEX.CU_INVOICE_ID                      
	LEFT OUTER JOIN  DBO.USER_INFO UI WITH (NOLOCK)  ON UI.QUEUE_ID = CUEX.QUEUE_ID                    
	LEFT JOIN  DBO.ACCOUNT_GROUP AG WITH (NOLOCK)  ON AG.ACCOUNT_GROUP_ID = CI.ACCOUNT_GROUP_ID                       
	LEFT JOIN  CORE.CLIENT_HIER_ACCOUNT CHA WITH (NOLOCK)  ON CHA.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID    AND CHA.ACCOUNT_ID = CUEX.ACCOUNT_ID                      
	LEFT JOIN  CORE.CLIENT_HIER CH  ON CH.CLIENT_HIER_ID = CUEX.CLIENT_HIER_ID                      
	LEFT JOIN   DBO.CU_INVOICE_LABEL INV_LBL WITH (NOLOCK) ON CUEX.CU_INVOICE_ID = INV_LBL.CU_INVOICE_ID                      
	WHERE  CUEX.EXCEPTION_TYPE <> 'FAILED RECALC'                
                        
	DELETE FROM #DEFAULTEXCEPTIONS  WHERE RN <> 1;  

	INSERT INTO #DEFAULTEXCEPTIONS_SYSTEM_GROUP
	SELECT DF.* FROM #DEFAULTEXCEPTIONS DF 
	WHERE  DF.EXCEPTION_TYPE = 'UNKNOWN ACCOUNT'                    
	   AND DF.ubmid IS NOT NULL                    
       AND DF.ubmaccountcode IS NOT NULL  
		

	DECLARE @DefaultExceptionsystemgrouptemp TABLE                    
    (                    
        systemgroupid INT NULL,                
		ubmid INT NULL,                    
        ubmaccountcode VARCHAR(200) NULL,                    
        exception_type VARCHAR(200) NULL,                    
        systemgrouptotalcount INT NULL                    
    );
	INSERT INTO @DefaultExceptionsystemgrouptemp
	SELECT ROW_NUMBER() OVER (ORDER BY DF.ubmid ASC) AS systemgroupid,              
           DF.ubmid,                    
		   DF.ubmaccountcode,                    
           'UNKNOWN ACCOUNT' AS exception_type ,   
			COUNT(1) AS system_group_total_count                
        FROM #DEFAULTEXCEPTIONS_SYSTEM_GROUP DF                                      
        GROUP BY DF.ubmid,                    
                 DF.ubmaccountcode                
        HAVING COUNT(1) > 1
		

		INSERT INTO #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL
		(
		Systemgroupid,
		SystemGroupName,
		ubmid,
		ubmaccountcode,
		CU_INVOICE_ID,
		ACCOUNT,
		Account_Id,                    
        EXCEPTION_TYPE,                    
        EXCEPTION_STATUS_TYPE,                    
        ASSIGNEE,                    
        CLIENTID,                    
        CLIENT,                                      
		DATE_IN_QUEUE
		)
		SELECT 
		DESGT.Systemgroupid,
		'SystemGroup_' + cast(DESGT.Systemgroupid AS NVARCHAR) AS SystemGroupName,
		DESG.ubmid,
		DESG.ubmaccountcode,
		DESG.CU_INVOICE_ID,
		DESG.ACCOUNT,
		DESG.Account_Id,                    
        DESG.EXCEPTION_TYPE,                    
        DESG.EXCEPTION_STATUS_TYPE,                    
        DESG.ASSIGNEE,                    
        DESG.CLIENTID,                    
        DESG.CLIENT,                                      
		DESG.DATE_IN_QUEUE
		FROM #DEFAULTEXCEPTIONS_SYSTEM_GROUP DESG
		INNER JOIN @DefaultExceptionsystemgrouptemp DESGT ON DESG.ubmid=DESGT.ubmid AND DESG.ubmaccountcode=DESGT.ubmaccountcode
		ORDER BY DESG.ubmaccountcode,
		DESGT.ubmaccountcode

		

		SELECT @CNT=count(1) FROM #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL
		WHERE CU_INVOICE_ID= @Invoice_ID

		SELECT @Systemgroupid = Systemgroupid FROM #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL
		WHERE CU_INVOICE_ID= @Invoice_ID

		 IF @CNT = 0     
		  BEGIN    
			SELECT @CNT AS Invoice_Id        
		  END    
		ELSE    

		SELECT 
		DESGF.CU_INVOICE_ID AS Invoice_ID,
		DESGF.DATE_IN_QUEUE AS Date_In_Queue,
		DESGF.EXCEPTION_TYPE  AS [Type],
        DESGF.EXCEPTION_STATUS_TYPE AS [Status],
		DESGF.ASSIGNEE AS [Assigned_To],
		DESGF.CLIENT [Client],
		DESGF.ACCOUNT [Account_Number]                  
 		FROM #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL DESGF WHERE DESGF.Systemgroupid IN (@Systemgroupid)

		DROP TABLE #DEFAULTEXCEPTIONS
		DROP TABLE #DEFAULTEXCEPTIONS_SYSTEM_GROUP
		DROP TABLE #DEFAULTEXCEPTIONS_SYSTEM_GROUP_FINAL
 
    
END    
    
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Group_Invoice_Details] TO [CBMSApplication]
GO
