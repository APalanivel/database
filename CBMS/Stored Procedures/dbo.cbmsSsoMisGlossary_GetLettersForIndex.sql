SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[cbmsSsoMisGlossary_GetLettersForIndex]
	( @MyAccountId int )
AS
BEGIN

	   select distinct left(upper(term), 1) start_letter
	     from sso_mis_glossary
	 order by left(upper(term), 1)

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoMisGlossary_GetLettersForIndex] TO [CBMSApplication]
GO
