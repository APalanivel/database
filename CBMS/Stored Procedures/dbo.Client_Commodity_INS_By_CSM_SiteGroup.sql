SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******        
NAME:        
		dbo.Client_Commodity_INS_By_CSM_SiteGroup

DESCRIPTION:      
		Used to insert the clinet commodity for those sitegroup which added later it sitegroup table.           
	
INPUT PARAMETERS:      
Name			      DataType		Default Description      
------------------------------------------------------------      
@Client_Id				INT,               
@Client_Commodity_Id	INT,              
@sitegroup_type_name	VARCHAR(15) = 'Global'
            
OUTPUT PARAMETERS:      
Name   DataType  Default Description      
------------------------------------------------------------      

USAGE EXAMPLES:          
------------------------------------------------------------      

   
   
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
GB   Geetansu Behera        
CMH  Chad Hattabaugh         
HG   Hari       
SKA  Shobhit Kr Agrawal  
   
MODIFICATIONS         
Initials Date		Modification        
------------------------------------------------------------        
GB					Created      
GB  12-OCT-09	Modified the value for for UOM_CD from 1 to 0, Based on Bug 12107
			
  
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Client_Commodity_INS_By_CSM_SiteGroup
  @Client_Id INT,               
  @Client_Commodity_Id INT,              
  @sitegroup_type_name VARCHAR(15) = 'Global'
  
AS              

BEGIN              
    
 SET NOCOUNT ON;  
					
DECLARE @UOM_Cd INT
SELECT @UOM_Cd = UOM_Cd FROM Core.Client_Commodity WHERE Client_Commodity_Id = @Client_Commodity_Id

			
		INSERT INTO Core.Client_Commodity_Detail (Client_Commodity_Id,
													CLIENT_ID,
													Sitegroup_id,
													SITE_ID,
													ACCOUNT_ID,
													Is_GHG_Reported,
													UOM_Cd
													)
			
		SELECT @Client_Commodity_Id,@CLient_ID,sitedetails.sitegroup_id,NULL,NULL,0,@UOM_Cd
		FROM Core.Client_Commodity_Detail ccd      
		INNER JOIN Core.Client_Commodity cc        
			ON cc.Client_commodity_id = ccd.client_commodity_id       
				AND cc.Client_commodity_id = @Client_Commodity_Id
		RIGHT OUTER JOIN	
			(SELECT Sitegroup_Id, @CLient_ID AS CLient_ID
			FROM
				Sitegroup sg       
			inner JOIN CODE cd       
				ON cd.CODE_ID = sg.Sitegroup_Type_Cd      
			inner JOIN CODESET cs      
				ON cd.CODESET_ID = cs.CODESET_ID  
			where sg.Client_Id = @CLient_ID
				AND cs.STD_COLUMN_NAME = 'Sitegroup_type_cd'
				AND cd.Code_Value = @sitegroup_type_name  
			)sitedetails
		ON sitedetails.Sitegroup_Id = ccd.sitegroup_id
			AND sitedetails.CLient_ID = ccd.CLIENT_ID
		INNER JOIN
			dbo.CLIENT c
		ON c.CLIENT_ID = sitedetails.CLient_ID
		WHERE ccd.sitegroup_id IS NULL
END
GO
GRANT EXECUTE ON  [dbo].[Client_Commodity_INS_By_CSM_SiteGroup] TO [CBMSApplication]
GO
