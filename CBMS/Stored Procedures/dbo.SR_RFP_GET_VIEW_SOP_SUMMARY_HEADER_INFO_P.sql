
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	[dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P]

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------         	
	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES
------------------------------------------------------------

EXEC [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] 127,0,98,1
EXEC [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] 6872,0,19900,0
EXEC [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] 12208,0,21627,0

SELECT TOP 10 * FROM dbo.SR_RFP_SOP_SUMMARY 
WHERE START_DATE IS NOT NULL AND END_DATE IS NOT NULL

EXEC [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] 0,0,1064,0
EXEC [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] 0,0,395,1
EXEC dbo.SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P null,null,12105691,0



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayaan Reddy
	RR			Raghu Reddy	
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	NR			2014-10-29	Added header.
							Added the Site_Reference_Number in select list.
	RR			2016-05-16	GCS-977	Rolled back date format conversion to DD-MON-YYYY, as start and date fields are strings,
							maupltiple dates can be saved as comma seperated								
******/

CREATE PROCEDURE [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P]
      @userId VARCHAR
     ,@sessionId VARCHAR
     ,@accountGroupId INT
     ,@isBidGroupId INT
AS 
DECLARE
      @count INT
     ,@count1 INT
     ,@commodity VARCHAR(50)
     ,@bidGroupName VARCHAR(100)
     ,@Lpsize DECIMAL(32, 16)  


BEGIN  

      SET NOCOUNT ON;

      SELECT
            @Lpsize = dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(@accountGroupId, @isBidGroupId)

      IF @isBidGroupId = 0 
            BEGIN  

                  SELECT
                        @bidGroupName = NULL
	 
                  SELECT
                        @commodity = entity.entity_name
                  FROM
                        dbo.SR_RFP rfp
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacct
                              ON rfpacct.sr_rfp_id = rfp.sr_rfp_id
                                 AND rfpacct.sr_rfp_account_id = @accountGroupId
                                 AND rfpacct.is_deleted = 0
                        INNER JOIN dbo.ENTITY
                              ON entity.entity_ID = rfp.commodity_type_id

            END	  
      ELSE 
            IF @isBidGroupId = 1 
                  BEGIN

                        SELECT
                              @bidGroupName = ( SELECT
                                                      GROUP_NAME
                                                FROM
                                                      dbo.SR_RFP_BID_GROUP
                                                WHERE
                                                      SR_RFP_BID_GROUP_ID = @accountGroupId )  
		  
                        SELECT
                              @commodity = entity.entity_name
                        FROM
                              dbo.SR_RFP rfp
                              INNER JOIN dbo.SR_RFP_ACCOUNT rfpacct
                                    ON rfpacct.sr_rfp_id = rfp.sr_rfp_id
                                       AND rfpacct.sr_rfp_bid_group_id = @accountGroupId
                                       AND rfpacct.is_deleted = 0
                              INNER JOIN dbo.ENTITY
                                    ON entity.entity_ID = rfp.commodity_type_id

                  END
  
      SELECT
            @count = count(1)
      FROM
            sr_rfp_sop_summary summary
            INNER JOIN sr_rfp_sop sop
                  ON SOP.SR_RFP_SOP_SUMMARY_ID = summary.SR_RFP_SOP_SUMMARY_ID
                     AND summary.SR_ACCOUNT_GROUP_ID = @accountGroupId
                     AND summary.IS_BID_GROUP = @isBidGroupId
            INNER JOIN sr_rfp_sop_details details
                  ON details.SR_RFP_SOP_ID = sop.SR_RFP_SOP_ID
                     AND details.IS_RECOMMENDED = 1
	  
      SELECT
            @count1 = count(1)
      FROM
            sr_rfp_sop_summary summary
      WHERE
            summary.SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND summary.IS_BID_GROUP = @isBidGroupId
	  
      IF @isBidGroupId = 0 
            BEGIN	  
                  IF ( @count ) > 0 
                        BEGIN  
	  
                              IF @commodity = 'Natural Gas' 
                                    BEGIN  
	   	     
                                          SELECT
                                                ch.Client_Name
                                               ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                               ,cha.Account_Id
                                               ,cha.Account_Number
                                               ,cha.Account_Vendor_Name AS UTILITY_NAME
                                               ,SOP.VENDOR_NAME RECOMMENDED_VENDOR_NAME
                                               ,DETAILS.PRODUCT_NAME
                                               ,DETAILS.PRICE_RESPONSE_COMMENTS
                                               ,CURRENT_VENDOR_NAME
                                               ,PRICING_SUMMARY
                                               ,START_DATE
                                               ,END_DATE
                                               ,@Lpsize AS LP_SIZE
                                               ,ENT.ENTITY_NAME SERVICE_TYPE
                                               ,substring(datename(month, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 1, 3) + '-' + substring(datename(year, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 3, 4) + ' (' + convert(VARCHAR(2), SR_RFP_ACCOUNT_TERM.NO_OF_MONTHS) + ' months)' TERM
                                               ,@bidGroupName BID_GROUP_NAME
                                               ,SOP.SOP_CREATION_DATE
                                               ,ch.City
                                               ,ch.State_Name
                                               ,ch.Site_Reference_Number
                                          FROM
                                                Core.Client_Hier ch
                                                INNER JOIN ( SELECT
                                                                  Account_Id
                                                                 ,Account_Number
                                                                 ,Account_Vendor_Name
                                                                 ,Client_Hier_Id
                                                             FROM
                                                                  core.Client_Hier_Account
                                                             GROUP BY
                                                                  Account_Id
                                                                 ,Account_Number
                                                                 ,Account_Vendor_Name
                                                                 ,Client_Hier_Id ) cha
                                                      ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                INNER JOIN SR_RFP_ACCOUNT
                                                      ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                LEFT JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                      ON SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = SUMMARY.SR_ACCOUNT_GROUP_ID
                                                         AND SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = @accountGroupId
                                                         AND SR_RFP_ACCOUNT.is_deleted = 0
                                                         AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                         AND SUMMARY.IS_BID_GROUP = @isBidGroupId
                                                INNER JOIN SR_RFP_SOP SOP
                                                      ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                INNER JOIN SR_RFP_SOP_DETAILS DETAILS
                                                      ON DETAILS.SR_RFP_SOP_ID = SOP.SR_RFP_SOP_ID
                                                         AND DETAILS.IS_RECOMMENDED = 1
                                                INNER JOIN SR_RFP_BID
                                                      ON SR_RFP_BID.SR_RFP_BID_ID = DETAILS.SR_RFP_BID_ID
                                                INNER JOIN SR_RFP_ACCOUNT_TERM
                                                      ON SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = DETAILS.SR_RFP_ACCOUNT_TERM_ID
                                                INNER JOIN SR_RFP_BID_REQUIREMENTS
                                                      ON SR_RFP_BID_REQUIREMENTS.SR_RFP_BID_REQUIREMENTS_ID = SR_RFP_BID.SR_RFP_BID_REQUIREMENTS_ID
                                                LEFT JOIN ENTITY ENT
                                                      ON SR_RFP_BID_REQUIREMENTS.TRANSPORTATION_LEVEL_TYPE_ID = ENT.ENTITY_ID
				     
                                    END
                              ELSE 
                                    IF @commodity = 'Electric Power' 
                                          BEGIN

                                                SELECT
                                                      ch.Client_Name
                                                     ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                                     ,cha.Account_Id
                                                     ,cha.Account_Number
                                                     ,cha.Account_Vendor_Name AS UTILITY_NAME
                                                     ,SOP.VENDOR_NAME RECOMMENDED_VENDOR_NAME
                                                     ,DETAILS.PRODUCT_NAME
                                                     ,DETAILS.PRICE_RESPONSE_COMMENTS
                                                     ,CURRENT_VENDOR_NAME
                                                     ,PRICING_SUMMARY
                                                     ,START_DATE
                                                     ,END_DATE
                                                     ,@Lpsize AS LP_SIZE
                                                     ,ENT.ENTITY_NAME SERVICE_TYPE
                                                     ,substring(datename(month, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 1, 3) + '-' + substring(datename(year, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 3, 4) + ' Meter Read for ' + convert(VARCHAR(2), SR_RFP_ACCOUNT_TERM.NO_OF_MONTHS) + ' months' TERM
                                                     ,@bidGroupName BID_GROUP_NAME
                                                     ,SOP.SOP_CREATION_DATE
                                                     ,ch.City
                                                     ,ch.State_Name
                                                     ,ch.Site_Reference_Number
                                                FROM
                                                      Core.Client_Hier ch
                                                      INNER JOIN ( SELECT
                                                                        Account_Id
                                                                       ,Account_Number
                                                                       ,Account_Vendor_Name
                                                                       ,Client_Hier_Id
                                                                   FROM
                                                                        core.Client_Hier_Account
                                                                   GROUP BY
                                                                        Account_Id
                                                                       ,Account_Number
                                                                       ,Account_Vendor_Name
                                                                       ,Client_Hier_Id ) cha
                                                            ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                      INNER JOIN SR_RFP_ACCOUNT
                                                            ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                      LEFT JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                            ON SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = SUMMARY.SR_ACCOUNT_GROUP_ID
                                                               AND SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = @accountGroupId
                                                               AND SR_RFP_ACCOUNT.is_deleted = 0
                                                               AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                               AND SUMMARY.IS_BID_GROUP = @isBidGroupId
                                                      INNER JOIN SR_RFP_SOP SOP
                                                            ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                      INNER JOIN SR_RFP_SOP_DETAILS DETAILS
                                                            ON DETAILS.SR_RFP_SOP_ID = SOP.SR_RFP_SOP_ID
                                                               AND DETAILS.IS_RECOMMENDED = 1
                                                      INNER JOIN SR_RFP_BID
                                                            ON SR_RFP_BID.SR_RFP_BID_ID = DETAILS.SR_RFP_BID_ID
                                                      INNER JOIN SR_RFP_ACCOUNT_TERM
                                                            ON SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = DETAILS.SR_RFP_ACCOUNT_TERM_ID
                                                      INNER JOIN SR_RFP_BID_REQUIREMENTS
                                                            ON SR_RFP_BID_REQUIREMENTS.SR_RFP_BID_REQUIREMENTS_ID = SR_RFP_BID.SR_RFP_BID_REQUIREMENTS_ID
                                                      LEFT JOIN ENTITY ENT
                                                            ON SR_RFP_BID_REQUIREMENTS.TRANSPORTATION_LEVEL_TYPE_ID = ENT.ENTITY_ID
	     
	     
                                          END	    
                        END	   
                  ELSE 
                        IF ( @count1 > 0 ) 
                              BEGIN  

                                    PRINT ' @COUNT1 ' + str(@COUNT1)  
	  	    
                                    SELECT
                                          ch.Client_Name
                                         ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                         ,cha.Account_Id
                                         ,cha.Account_Number
                                         ,cha.Account_Vendor_Name AS UTILITY_NAME
                                         ,NULL RECOMMENDED_VENDOR_NAME
                                         ,NULL PRODUCT_NAME
                                         ,NULL PRICE_RESPONSE_COMMENTS
                                         ,SUMMARY.CURRENT_VENDOR_NAME
                                         ,SUMMARY.PRICING_SUMMARY
                                         ,SUMMARY.START_DATE
                                         ,SUMMARY.END_DATE
                                         ,@Lpsize AS LP_SIZE
                                         ,NULL SERVICE_TYPE
                                         ,NULL TERM
                                         ,@bidGroupName BID_GROUP_NAME
                                         ,sop.SOP_CREATION_DATE
                                         ,ch.City
                                         ,ch.State_Name
                                         ,ch.Site_Reference_Number
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN ( SELECT
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id
                                                       FROM
                                                            core.Client_Hier_Account
                                                       GROUP BY
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id ) cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                          INNER JOIN SR_RFP_ACCOUNT
                                                ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                   AND SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = @accountGroupId
                                                   AND SR_RFP_ACCOUNT.is_deleted = 0
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID IS NULL
                                          INNER JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                ON SUMMARY.SR_ACCOUNT_GROUP_ID = SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID
                                          INNER JOIN SR_RFP_SOP SOP
                                                ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                   AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                   AND SUMMARY.IS_BID_GROUP = @isBidGroupId

			  
                              END
                        ELSE 
                              BEGIN  	   

                                    SELECT
                                          ch.Client_Name
                                         ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                         ,cha.Account_Id
                                         ,cha.Account_Number
                                         ,cha.Account_Vendor_Name AS UTILITY_NAME
                                         ,NULL RECOMMENDED_VENDOR_NAME
                                         ,NULL PRODUCT_NAME
                                         ,NULL PRICE_RESPONSE_COMMENTS
                                         ,NULL CURRENT_VENDOR_NAME
                                         ,NULL PRICING_SUMMARY
                                         ,cast(NULL AS DATETIME) START_DATE
                                         ,cast(NULL AS DATETIME) END_DATE
                                         ,@Lpsize AS LP_SIZE
                                         ,NULL SERVICE_TYPE
                                         ,NULL TERM
                                         ,@bidGroupName BID_GROUP_NAME
                                         ,ch.City
                                         ,ch.State_Name
                                         ,ch.Site_Reference_Number
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN ( SELECT
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id
                                                       FROM
                                                            core.Client_Hier_Account
                                                       GROUP BY
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id ) cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                          INNER JOIN SR_RFP_ACCOUNT
                                                ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                   AND SR_RFP_ACCOUNT.SR_RFP_ACCOUNT_ID = @accountGroupId
                                                   AND SR_RFP_ACCOUNT.is_deleted = 0
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID IS NULL
    
		      
                              END	    
            END  
	  
      IF @isBidGroupId = 1 
            BEGIN
	  
                  IF ( @count ) > 0 
                        BEGIN  
		  
                              PRINT @commodity  
			  
                              IF @commodity = 'Natural Gas' 
                                    BEGIN
	  
                                          SELECT
                                                ch.Client_Name
                                               ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                               ,cha.Account_Id
                                               ,cha.Account_Number
                                               ,cha.Account_Vendor_Name AS UTILITY_NAME
                                               ,SOP.VENDOR_NAME RECOMMENDED_VENDOR_NAME
                                               ,DETAILS.PRODUCT_NAME
                                               ,DETAILS.PRICE_RESPONSE_COMMENTS
                                               ,CURRENT_VENDOR_NAME
                                               ,PRICING_SUMMARY
                                               ,START_DATE
                                               ,END_DATE
                                               ,@Lpsize AS LP_SIZE
                                               ,ENT.ENTITY_NAME SERVICE_TYPE
                                               ,substring(datename(month, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 1, 3) + '-' + substring(datename(year, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 3, 4) + --SUBSTRING(DATENAME(month, SR_RFP_ACCOUNT_TERM.TO_MONTH),1,3)+'-'+ 
                                                ' (' + convert(VARCHAR(2), SR_RFP_ACCOUNT_TERM.NO_OF_MONTHS) + ' months)' TERM
                                               ,@bidGroupName BID_GROUP_NAME
                                               ,SOP.SOP_CREATION_DATE
                                               ,ch.City
                                               ,ch.State_Name
                                               ,ch.Site_Reference_Number
                                          FROM
                                                Core.Client_Hier ch
                                                INNER JOIN ( SELECT
                                                                  Account_Id
                                                                 ,Account_Number
                                                                 ,Account_Vendor_Name
                                                                 ,Client_Hier_Id
                                                             FROM
                                                                  core.Client_Hier_Account
                                                             GROUP BY
                                                                  Account_Id
                                                                 ,Account_Number
                                                                 ,Account_Vendor_Name
                                                                 ,Client_Hier_Id ) cha
                                                      ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                INNER JOIN SR_RFP_ACCOUNT
                                                      ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                INNER JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                      ON SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = SUMMARY.SR_ACCOUNT_GROUP_ID
                                                         AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                         AND SR_RFP_ACCOUNT.is_deleted = 0
                                                         AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                         AND SUMMARY.IS_BID_GROUP = 1
                                                INNER JOIN SR_RFP_SOP SOP
                                                      ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                INNER JOIN SR_RFP_SOP_DETAILS DETAILS
                                                      ON DETAILS.SR_RFP_SOP_ID = SOP.SR_RFP_SOP_ID
                                                         AND DETAILS.IS_RECOMMENDED = 1
                                                INNER JOIN SR_RFP_BID
                                                      ON SR_RFP_BID.SR_RFP_BID_ID = DETAILS.SR_RFP_BID_ID
                                                INNER JOIN SR_RFP_ACCOUNT_TERM
                                                      ON SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = DETAILS.SR_RFP_ACCOUNT_TERM_ID
                                                INNER JOIN SR_RFP_BID_REQUIREMENTS
                                                      ON SR_RFP_BID_REQUIREMENTS.SR_RFP_BID_REQUIREMENTS_ID = SR_RFP_BID.SR_RFP_BID_REQUIREMENTS_ID
                                                LEFT OUTER JOIN ENTITY ENT
                                                      ON SR_RFP_BID_REQUIREMENTS.TRANSPORTATION_LEVEL_TYPE_ID = ENT.ENTITY_ID
   
				     				        
                                    END  
                              ELSE 
                                    IF @commodity = 'Electric Power' 
                                          BEGIN
	  
                                                SELECT
                                                      ch.Client_Name
                                                     ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                                     ,cha.Account_Id
                                                     ,cha.Account_Number
                                                     ,cha.Account_Vendor_Name AS UTILITY_NAME
                                                     ,SOP.VENDOR_NAME RECOMMENDED_VENDOR_NAME
                                                     ,DETAILS.PRODUCT_NAME
                                                     ,DETAILS.PRICE_RESPONSE_COMMENTS
                                                     ,CURRENT_VENDOR_NAME
                                                     ,PRICING_SUMMARY
                                                     ,START_DATE
                                                     ,END_DATE
                                                     ,@Lpsize AS LP_SIZE
                                                     ,ENT.ENTITY_NAME SERVICE_TYPE
                                                     ,substring(datename(month, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 1, 3) + '-' + substring(datename(year, SR_RFP_ACCOUNT_TERM.FROM_MONTH), 3, 4) + --SUBSTRING(DATENAME(month, SR_RFP_ACCOUNT_TERM.TO_MONTH),1,3)
                                                      +'-' + --SUBSTRING(DATENAME(year, SR_RFP_ACCOUNT_TERM.TO_MONTH),3,4) TERM,  
                                                      ' Meter Read for ' + convert(VARCHAR(2), SR_RFP_ACCOUNT_TERM.NO_OF_MONTHS) + ' months' TERM
                                                     ,@bidGroupName BID_GROUP_NAME
                                                     ,SOP.SOP_CREATION_DATE
                                                     ,ch.City
                                                     ,ch.State_Name
                                                     ,ch.Site_Reference_Number
                                                FROM
                                                      Core.Client_Hier ch
                                                      INNER JOIN ( SELECT
                                                                        Account_Id
                                                                       ,Account_Number
                                                                       ,Account_Vendor_Name
                                                                       ,Client_Hier_Id
                                                                   FROM
                                                                        core.Client_Hier_Account
                                                                   GROUP BY
                                                                        Account_Id
                                                                       ,Account_Number
                                                                       ,Account_Vendor_Name
                                                                       ,Client_Hier_Id ) cha
                                                            ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                      INNER JOIN SR_RFP_ACCOUNT
                                                            ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                      INNER JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                            ON SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = SUMMARY.SR_ACCOUNT_GROUP_ID
                                                               AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                               AND SR_RFP_ACCOUNT.is_deleted = 0
                                                               AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                               AND SUMMARY.IS_BID_GROUP = 1
                                                      INNER JOIN SR_RFP_SOP SOP
                                                            ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                      INNER JOIN SR_RFP_SOP_DETAILS DETAILS
                                                            ON DETAILS.SR_RFP_SOP_ID = SOP.SR_RFP_SOP_ID
                                                               AND DETAILS.IS_RECOMMENDED = 1
                                                      INNER JOIN SR_RFP_BID
                                                            ON SR_RFP_BID.SR_RFP_BID_ID = DETAILS.SR_RFP_BID_ID
                                                      INNER JOIN SR_RFP_ACCOUNT_TERM
                                                            ON SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = DETAILS.SR_RFP_ACCOUNT_TERM_ID
                                                      INNER JOIN SR_RFP_BID_REQUIREMENTS
                                                            ON SR_RFP_BID_REQUIREMENTS.SR_RFP_BID_REQUIREMENTS_ID = SR_RFP_BID.SR_RFP_BID_REQUIREMENTS_ID
                                                      LEFT OUTER JOIN ENTITY ENT
                                                            ON SR_RFP_BID_REQUIREMENTS.TRANSPORTATION_LEVEL_TYPE_ID = ENT.ENTITY_ID
                                                           

                                          END  
                        END	 	  	  
                  ELSE 
                        IF ( @count1 > 0 ) 
                              BEGIN

                                    PRINT ' @COUNT1 ' + str(@COUNT1)
	   		   
                                    SELECT
                                          ch.Client_Name
                                         ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                         ,cha.Account_Id
                                         ,cha.Account_Number
                                         ,cha.Account_Vendor_Name AS UTILITY_NAME
                                         ,NULL RECOMMENDED_VENDOR_NAME
                                         ,NULL PRODUCT_NAME
                                         ,NULL PRICE_RESPONSE_COMMENTS
                                         ,SUMMARY.CURRENT_VENDOR_NAME
                                         ,SUMMARY.PRICING_SUMMARY
                                         ,SUMMARY.START_DATE
                                         ,SUMMARY.END_DATE
                                         ,@Lpsize AS LP_SIZE
                                         ,NULL SERVICE_TYPE
                                         ,NULL TERM
                                         ,@bidGroupName BID_GROUP_NAME
                                         ,sop.SOP_CREATION_DATE
                                         ,ch.City
                                         ,ch.State_Name
                                         ,ch.Site_Reference_Number
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN ( SELECT
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id
                                                       FROM
                                                            core.Client_Hier_Account
                                                       GROUP BY
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id ) cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                          INNER  JOIN SR_RFP_ACCOUNT
                                                ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                   AND SR_RFP_ACCOUNT.is_deleted = 0
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID IS NOT NULL
                                          INNER JOIN SR_RFP_SOP_SUMMARY SUMMARY
                                                ON SUMMARY.SR_ACCOUNT_GROUP_ID = SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID
                                          INNER JOIN SR_RFP_SOP SOP
                                                ON SOP.SR_RFP_SOP_SUMMARY_ID = SUMMARY.SR_RFP_SOP_SUMMARY_ID
                                                   AND SUMMARY.SR_ACCOUNT_GROUP_ID = @accountGroupId
                                                   AND SUMMARY.IS_BID_GROUP = @isBidGroupId    
			  			    
                              END  
                        ELSE 
                              BEGIN  
	  	  
                                    SELECT
                                          ch.Client_Name
                                         ,rtrim(ch.city) + ', ' + ch.state_name + ' (' + ch.site_name + ')' AS SITE_NAME
                                         ,cha.Account_Id
                                         ,cha.Account_Number
                                         ,cha.Account_Vendor_Name AS UTILITY_NAME
                                         ,NULL RECOMMENDED_VENDOR_NAME
                                         ,NULL PRODUCT_NAME
                                         ,NULL PRICE_RESPONSE_COMMENTS
                                         ,NULL CURRENT_VENDOR_NAME
                                         ,NULL PRICING_SUMMARY
                                         ,cast(NULL AS DATETIME) START_DATE
                                         ,cast(NULL AS DATETIME) END_DATE
                                         ,@Lpsize AS LP_SIZE
                                         ,NULL SERVICE_TYPE
                                         ,NULL TERM
                                         ,@bidGroupName BID_GROUP_NAME
                                         ,ch.City
                                         ,ch.State_Name
                                         ,ch.Site_Reference_Number
                                    FROM
                                          Core.Client_Hier ch
                                          INNER JOIN ( SELECT
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id
                                                       FROM
                                                            core.Client_Hier_Account
                                                       GROUP BY
                                                            Account_Id
                                                           ,Account_Number
                                                           ,Account_Vendor_Name
                                                           ,Client_Hier_Id ) cha
                                                ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                          INNER JOIN SR_RFP_ACCOUNT
                                                ON SR_RFP_ACCOUNT.account_id = cha.account_id
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = @accountGroupId
                                                   AND SR_RFP_ACCOUNT.is_deleted = 0
                                                   AND SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID IS NOT NULL                                         	    	   
                              END
	  
            END
END;


;
GO



GRANT EXECUTE ON  [dbo].[SR_RFP_GET_VIEW_SOP_SUMMARY_HEADER_INFO_P] TO [CBMSApplication]
GO
