SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
Name:  
	[dbo].[Filtered_Code_SeL_By_Filtered_Code_Codeset_Name]
 
 Description:  
	Returns the codes falling under the  Filtered_Code_Id and Codeset.
 
 Input Parameters:  
    Name						DataType		Default				Description  
----------------------------------------------------------------------------------------  
	@Filtered_Code_Id			INT
	@CodeSet_Name				VARCHAR(25)
	@Filtered_Code_Codeset_Name VARCHAR(25)		Filtered Codeset

 
 Output Parameters:  
    Name						DataType		Default				Description  
----------------------------------------------------------------------------------------  
 
 Usage Examples:
----------------------------------------------------------------------------------------

--- For Utility

SELECT * FROM dbo.Code c INNER JOIN dbo.Codeset cs
ON c.Codeset_Id = cs.Codeset_Id
WHERE c.Code_Value IN ('System Recalc - DIST','System Recalc - All','Validate Enrollment') 
and cs.Codeset_Id=245
 

EXEC dbo.Filtered_Code_SeL_By_Filtered_Code_Codeset_Name 
      @Filtered_Code_Id = 102145
     ,@CodeSet_Name =  'Determinant Source Type'
     ,@Filtered_Code_Codeset_Name = 'Filtered Codeset'

--- For Supplier

SELECT * FROM dbo.Code c INNER JOIN dbo.Codeset cs
ON c.Codeset_Id = cs.Codeset_Id
WHERE c.Code_Value IN ('System Recalc - Supply','System Recalc - VC','System Recalc-Commercial')  
and cs.Codeset_Id=32


EXEC dbo.Filtered_Code_SeL_By_Filtered_Code_Codeset_Name 
      @Filtered_Code_Id = 100254
     ,@CodeSet_Name =  'Determinant Source Type'
     ,@Filtered_Code_Codeset_Name = 'Filtered Codeset'
     
     
    
Author Initials:  
 Initials	Name
----------------------------------------------------------------------------------------  
 NR			Narayana Reddy

 Modifications :  
 Initials		Date			Modification  
----------------------------------------------------------------------------------------  
 NR				2018-28-02		Created For Recalc Data Interval.
              
******/

CREATE  PROCEDURE [dbo].[Filtered_Code_SeL_By_Filtered_Code_Codeset_Name]
      ( 
       @Filtered_Code_Id INT
      ,@CodeSet_Name VARCHAR(25)
      ,@Filtered_Code_Codeset_Name VARCHAR(25) = 'Filtered Codeset' )
AS 
BEGIN

      SET NOCOUNT ON;
      

      DECLARE
            @Filtered_Code_Value VARCHAR(255)
           ,@Codeset_Filtered_Cd INT
  
      SELECT
            @Filtered_Code_Value = c.Code_Value
      FROM
            dbo.Code c
      WHERE
            Code_Id = @Filtered_Code_Id
       
       
      SELECT
            @Codeset_Filtered_Cd = Cd.Code_Id
      FROM
            dbo.Code Cd
            JOIN dbo.Codeset CS
                  ON Cd.Codeset_Id = CS.Codeset_Id
      WHERE
            Cd.Code_Value = @Filtered_Code_Value
            AND CS.Codeset_Name = @Filtered_Code_Codeset_Name;
       	
      SELECT
            FC.Filtered_Code_Id AS Code_Id
           ,Cd.Code_Value
           ,Cd.Code_Dsc
           ,FC.Display_Seq
           ,FC.Is_Default
      FROM
            dbo.Filtered_Code FC
            INNER JOIN dbo.Code Cd
                  ON FC.Filtered_Code_Id = Cd.Code_Id
            INNER JOIN dbo.CodeSet CS
                  ON Cd.Codeset_Id = CS.Codeset_Id
      WHERE
            CS.Codeset_Name = @CodeSet_Name
            AND FC.Codeset_Filtered_Cd = @Codeset_Filtered_Cd
      ORDER BY
            FC.Display_Seq

END;




GO
GRANT EXECUTE ON  [dbo].[Filtered_Code_SeL_By_Filtered_Code_Codeset_Name] TO [CBMSApplication]
GO
