SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
/******            
NAME: dbo.Client_Service_Category_Commodity_Merge            
            
DESCRIPTION:             
            
	Used to merge commodities added directly to a Client service category

INPUT PARAMETERS:                
      Name     DataType          Default     Description                
------------------------------------------------------------------                
      @Client_Service_Category_id   INT            
      @Commodity_Id       VARCHAR(MAX)          list of commodities with comma separator            

                
OUTPUT PARAMETERS:                
      Name              DataType          Default     Description                
------------------------------------------------------------                
            
            
USAGE EXAMPLES:            
------------------------------------------------------------            
            
BEGIN TRAN

	DECLARE @COMMODITY_ID VARCHAR(MAX)= '290,291'
	EXEC CLIENT_SERVICE_CATEGORY_COMMODITY_MERGE  87 , @COMMODITY_ID

	select TOP 10 * from Client_Service_Category_Commodity WHERE Client_Service_Category_Id = 87

ROLLBACK TRAN
	DECLARE @COMMODITY_ID VARCHAR(MAX)= '290'
	EXEC CLIENT_SERVICE_CATEGORY_COMMODITY_MERGE  29 , @COMMODITY_ID


select TOP 10 * from Client_Service_Category_Commodity WHERE Client_Service_Category_Id = 87
select TOP 10 * from Client_Service_Category
            
AUTHOR INITIALS:            
 Initials	Name            
------------------------------------------------------------            
 BCH		Balaraju            
            
MODIFICATIONS            
            
 Initials	Date		Modification            
------------------------------------------------------------            
 BCH		2011-10-19  Created

******/
CREATE PROCEDURE dbo.Client_Service_Category_Commodity_Merge
      ( 
       @Client_Service_Category_id INT
      ,@Commodity_Id VARCHAR(MAX) )
AS 
BEGIN
       
      SET NOCOUNT ON
        
      MERGE INTO dbo.Client_Service_Category_Commodity tgt
            USING 
                  ( SELECT
                        @Client_Service_Category_Id AS Client_Service_Category_Id
                       ,cast(cl.Segments AS INT) AS Commodity_Id
                    FROM
                        dbo.ufn_split(@Commodity_Id, ',') cl ) src
            ON tgt.client_service_category_id = src.Client_Service_Category_Id
                  AND tgt.Commodity_Id = src.Commodity_Id
                  AND tgt.Sub_Client_Service_Category_Id IS NULL
            WHEN  NOT MATCHED 
                  THEN INSERT
                        ( 
                         Client_Service_Category_Id
                        ,Commodity_Id )
                    VALUES
                        ( 
                         src.Client_Service_Category_id
                        ,src.Commodity_Id )
            WHEN  NOT MATCHED BY SOURCE AND tgt.Client_Service_Category_Id = @Client_Service_Category_id
                  AND tgt.Sub_Client_Service_Category_Id IS NULL
                  THEN        
        
  DELETE ;        
        
END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Commodity_Merge] TO [CBMSApplication]
GO
