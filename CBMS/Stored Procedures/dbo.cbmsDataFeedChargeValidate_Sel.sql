SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--exec [dbo].[cbmsDataFeedChargeValidate_Sel]  2,5,'add' 
CREATE PROCEDURE [dbo].[cbmsDataFeedChargeValidate_Sel]           
 (             
 @umb_id int   ,       
 @commodity_id int ,  
 @ubm_bucket_code varchar(255) 
 )              
AS              
BEGIN      

	Declare @result  int   
      
	select @result = 1 from UBM_BUCKET_CHARGE_MAP  
	where   
	 UBM_ID    = @umb_id  
	 AND COMMODITY_TYPE_ID   = @commodity_id  
	 AND UBM_BUCKET_CODE   = @ubm_bucket_code  
  
     
	select isnull(@result,0) as result
            
END 

GO
GRANT EXECUTE ON  [dbo].[cbmsDataFeedChargeValidate_Sel] TO [CBMSApplication]
GO
