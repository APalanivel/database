SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Utility_Detail_Del

DESCRIPTION: It Deletes Utility Detail for Selected Utility Detail Id.
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Utility_Detail_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
		EXEC dbo.Utility_Detail_Del  3608

  Rollback Tran


AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			25-August-10	CREATED
*/

CREATE PROCEDURE dbo.Utility_Detail_Del
    (
       @Utility_Detail_Id INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE 
	FROM
		dbo.Utility_Detail
	WHERE
		Utility_Detail_Id = @Utility_Detail_Id
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Del] TO [CBMSApplication]
GO
