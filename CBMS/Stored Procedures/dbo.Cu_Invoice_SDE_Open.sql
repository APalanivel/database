SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                            
 NAME: dbo.Cu_Invoice_SDE_Open
                            
 DESCRIPTION:                            
   This sproc is used to see if the Exception for Invoice is Open or Closed in the Invoice Exception Table 

   for the following exceptions 

   Sub Bucket Charge
   Sub Bucket Determinant
   Template
    
  
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
@Cu_Invoice_Id          INT                 
                            
 OUTPUT PARAMETERS:              
                               
 Name                        DataType         Default       Description            
------------------------------------------------------------------------------         
                            
 USAGE EXAMPLES:                                
------------------------------------------------------------------------------         
     
 EXEC dbo.Cu_Invoice_SDE_Open_OR_Closed  @Cu_Invoice_Id =93899252  
  
   
  
 AUTHOR INITIALS:            
           
 Initials               Name            
------------			------------------------------------------------------------------         
 HKT					Harish Kumar Tirumandyam
                             
 MODIFICATIONS:          
              
 Initials               Date				Modification          
------------			------------		------------------------------------------------------         
HKT						2020-05-19          to check the eligibility of the recalc for the given invoice for Standing Data Exceptions
                           
******/
CREATE   PROCEDURE [dbo].[Cu_Invoice_SDE_Open]
    (
        @Cu_Invoice_Id INT
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE
            @SDE_Charge_Exception_Type_Cd INT
            , @SDE_Determinant_Exception_Type_Cd INT
            , @SDE_Template_Exception_Type_Cd INT
            , @New_Exception_Status_Cd INT
            , @Inprogress_Exception_Status_Cd INT

        -------------------------------------------------------------------------------------------------  
        -------------------------------------------------------------------------------------------------  

        -- GET THE Standing Data Exception Type Codes 

        -- Standing Data Exception Type Codes .. Charge , Determinant , Template
        SELECT
            @SDE_Charge_Exception_Type_Cd = MAX(CASE C.Code_Value WHEN 'Sub-Bkt Charge Mapping' THEN C.Code_Id
                                                END)
            , @SDE_Determinant_Exception_Type_Cd = MAX(CASE C.Code_Value WHEN 'Sub-Bkt Determnt Mapping' THEN
                                                                             C.Code_Id
                                                       END)
            , @SDE_Template_Exception_Type_Cd = MAX(CASE C.Code_Value WHEN 'Sub-Bkt Template Mapping' THEN C.Code_Id
                                                    END)
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = C.Codeset_Id
        WHERE
            C.Code_Value IN ( 'Sub-Bkt Charge Mapping', 'Sub-Bkt Determnt Mapping', 'Sub-Bkt Template Mapping' )
            AND cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd';

        --  Status Codes ..  New , In Progress
        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN C.Code_Value = 'New' THEN C.Code_Id
                                           END)
            , @Inprogress_Exception_Status_Cd = MAX(CASE WHEN C.Code_Value = 'In Progress' THEN C.Code_Id
                                                    END)
        FROM
            dbo.Code C
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = C.Codeset_Id
        WHERE
            C.Code_Value IN ( 'New', 'In Progress' )
            AND cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd';


        --------------------------------------------------------------------------------------------------------------------------
        --------------------------------------------------------------------------------------------------------------------------

        -- Find out Eligible for ReCalc 

        SELECT
            CASE WHEN cisde.Exception_Type_Cd = @SDE_Charge_Exception_Type_Cd THEN 'Sub Bucket Charge Exception'
                 WHEN cisde.Exception_Type_Cd = @SDE_Determinant_Exception_Type_Cd THEN 'Sub Bucket Determimant Exception'
                 WHEN cisde.Exception_Type_Cd = @SDE_Template_Exception_Type_Cd THEN 'Template Exception'
            END AS Exception_Type
            , CISDED.Account_Id
            , CISDED.Commodity_Id
            , C.Commodity_Name
        FROM
            dbo.Cu_Invoice_Standing_Data_Exception cisde
            JOIN dbo.Cu_Invoice_Standing_Data_Exception_Dtl AS CISDED
                ON cisde.Cu_Invoice_Standing_Data_Exception_Id = CISDED.Cu_Invoice_Standing_Data_Exception_Id
            JOIN dbo.Commodity AS C
                ON C.Commodity_Id = CISDED.Commodity_Id
        WHERE
            cisde.Cu_Invoice_Id = @Cu_Invoice_Id
            AND cisde.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @Inprogress_Exception_Status_Cd );

    END;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_SDE_Open] TO [CBMSApplication]
GO
