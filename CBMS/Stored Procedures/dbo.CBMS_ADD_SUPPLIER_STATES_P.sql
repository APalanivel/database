SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_ADD_SUPPLIER_STATES_P    

DESCRIPTION:  This procedure used to insert the records in VENDOR_STATE_MAP table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_id                int,
@state_id                 int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
exec [CBMS_ADD_SUPPLIER_STATES_P]
   @vendor_id = 3051,
   @state_id = 14


Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
DR    8/4/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
******/
CREATE PROCEDURE [dbo].[CBMS_ADD_SUPPLIER_STATES_P] 
@vendor_id int,
@state_id int

AS


INSERT INTO VENDOR_STATE_MAP 
(
	VENDOR_ID, 
	STATE_ID, 
	IS_HISTORY
) 

VALUES 
(
	@vendor_id, 
	@state_id, 
	0
)
GO
GRANT EXECUTE ON  [dbo].[CBMS_ADD_SUPPLIER_STATES_P] TO [CBMSApplication]
GO
