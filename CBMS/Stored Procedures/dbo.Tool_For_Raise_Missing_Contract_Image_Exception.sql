SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Tool_For_Raise_Missing_Contract_Image_Exception    
    
DESCRIPTION:    
    
 Raise the Missing Contract Image   exception.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    
BEGIN TRAN

SELECT * FROM dbo.CONTRACT c WHERE c.CONTRACT_ID =166796
SELECT * FROM dbo.CONTRACT_CBMS_IMAGE_MAP c WHERE c.CONTRACT_ID =166796
SELECT * FROM dbo.Contract_Exception ae WHERE ae.Contract_Id = 166796 
SELECT * FROM dbo.App_Config ac WHERE ac.App_Config_Cd = 'Add_Contract_Missing_Contract_Image'

EXEC dbo.Tool_For_Raise_Missing_Contract_Image_Exception

SELECT * FROM dbo.CONTRACT c WHERE c.CONTRACT_ID =166796
SELECT * FROM dbo.CONTRACT_CBMS_IMAGE_MAP c WHERE c.CONTRACT_ID =166796
SELECT * FROM dbo.Contract_Exception ae WHERE ae.Contract_Id = 166796 
SELECT * FROM dbo.App_Config ac WHERE ac.App_Config_Cd = 'Add_Contract_Missing_Contract_Image'

ROLLBACK TRAN
     
    
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
  
******/

CREATE PROC [dbo].[Tool_For_Raise_Missing_Contract_Image_Exception]
AS
    SET NOCOUNT ON;

    BEGIN

        DECLARE
            @User_Info_Id INT
            , @Missing_Contract_Image_Type_Cd INT
            , @Commodity_Id INT
            , @Contract_Id INT
            , @New_Exception_Status_Cd INT
            , @Counter INT = 1
            , @Last_Running_Date DATE;

        SELECT
            @Last_Running_Date = CONVERT(DATE, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Missing_Contract_Image';


        SELECT
            @User_Info_Id = ui.USER_INFO_ID
        FROM
            dbo.USER_INFO ui
        WHERE
            ui.USERNAME = 'conversion'
            AND ui.FIRST_NAME = 'System';


        SELECT
            @Missing_Contract_Image_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Type'
            AND c.Code_Value = 'Missing Contract Image';

        SELECT
            @New_Exception_Status_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Exception Status'
            AND c.Code_Value = 'New';

        CREATE TABLE #Contract
             (
                 Id INT IDENTITY(1, 1)
                 , Contract_Id INT
                 , Commodity_Id INT
             );

        INSERT INTO #Contract
             (
                 Contract_Id
             )
        SELECT
            c.CONTRACT_ID
        FROM
            dbo.CONTRACT c
        WHERE
            DATEADD(DD, 30, CAST(c.Contract_Created_Ts AS DATE)) BETWEEN @Last_Running_Date
                                                                 AND     CAST(GETDATE() AS DATE)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.CONTRACT_CBMS_IMAGE_MAP ccim
                               WHERE
                                    ccim.CONTRACT_ID = c.CONTRACT_ID)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Contract_Exception ae
                               WHERE
                                    ae.Contract_Id = c.CONTRACT_ID
                                    AND ae.Exception_Type_Cd = @Missing_Contract_Image_Type_Cd);

        BEGIN TRY

            BEGIN TRANSACTION;

            WHILE (@Counter <= (SELECT  COUNT(1)FROM    #Contract))
                BEGIN


                    SELECT  @Contract_Id = c.Contract_Id FROM   #Contract c WHERE   Id = @Counter;


                    IF NOT EXISTS (   SELECT
                                            1
                                      FROM
                                            dbo.Contract_Exception ae
                                      WHERE
                                            ae.Contract_Id = @Contract_Id
                                            AND ae.Exception_Type_Cd = @Missing_Contract_Image_Type_Cd)
                        BEGIN

                            EXEC dbo.Contract_Exception_Ins
                                @Contract_Id = @Contract_Id
                                , @Exception_Type_Cd = @Missing_Contract_Image_Type_Cd
                                , @User_Info_Id = @User_Info_Id;




                        END;

                    SET @Counter = @Counter + 1;


                END;


            UPDATE
                ac
            SET
                ac.App_Config_Value = CONVERT(VARCHAR(100), GETDATE(), 23)
            FROM
                dbo.App_Config ac
            WHERE
                ac.App_Config_Cd = 'Add_Contract_Missing_Contract_Image';


            COMMIT TRANSACTION;




        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK;
                END;

            EXEC dbo.usp_RethrowError @CustomMessage = '';

        END CATCH;


    END;



GO
GRANT EXECUTE ON  [dbo].[Tool_For_Raise_Missing_Contract_Image_Exception] TO [CBMSApplication]
GO
