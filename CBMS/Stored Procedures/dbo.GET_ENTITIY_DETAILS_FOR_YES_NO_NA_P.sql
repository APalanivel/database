SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_ENTITIY_DETAILS_FOR_YES_NO_NA_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@entityType    	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE DBO.GET_ENTITIY_DETAILS_FOR_YES_NO_NA_P
@entityType int
AS
set nocount on
	SELECT 
		ENTITY_ID, ENTITY_NAME 
	FROM 
		ENTITY 
	WHERE 
		ENTITY_TYPE=@entityType
	
	ORDER BY ENTITY_ID
GO
GRANT EXECUTE ON  [dbo].[GET_ENTITIY_DETAILS_FOR_YES_NO_NA_P] TO [CBMSApplication]
GO
