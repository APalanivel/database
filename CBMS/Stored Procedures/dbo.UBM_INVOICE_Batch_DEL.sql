SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******  
NAME:  
 
 dbo.UBM_INVOICE_Batch_DEL
 
 DESCRIPTION:   
 
 Deletes the UBM_INVOICE tables for the passed in UBM_BATCH_MASTER_LOG_ID.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@Ubm_Batch_Master_Log_Id Integer

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.UBM_INVOICE_Batch_DEL 3256

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page    10/08/2009

 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    
******/


CREATE PROCEDURE [dbo].[UBM_INVOICE_Batch_DEL]
(
	@Ubm_Batch_Master_Log_Id Integer
)
AS
BEGIN

	SET NOCOUNT ON	
	
	DELETE UBM_INVOICE_METER_DETAILS
	  FROM UBM_INVOICE_METER_DETAILS uimd
	INNER JOIN UBM_INVOICE ui ON ui.UBM_INVOICE_ID = uimd.UBM_INVOICE_ID
	WHERE ui.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id
	
	DELETE UBM_INVOICE_DETAILS
	  FROM UBM_INVOICE_DETAILS ud
	INNER JOIN UBM_INVOICE ui ON ui.UBM_INVOICE_ID = ud.UBM_INVOICE_ID
	WHERE ui.UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id
	
	DELETE UBM_INVOICE WHERE UBM_BATCH_MASTER_LOG_ID = @Ubm_Batch_Master_Log_Id	
	
END

GO
GRANT EXECUTE ON  [dbo].[UBM_INVOICE_Batch_DEL] TO [CBMSApplication]
GO
