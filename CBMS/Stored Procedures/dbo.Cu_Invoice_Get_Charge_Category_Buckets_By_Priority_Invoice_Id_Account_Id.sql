SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
            
/******                        
              
NAME: [DBO].[dbo.Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id_Account_Id]                
                   
DESCRIPTION:              
              
 To Get determinant category buckets by invoice, priority  and account id
                    
INPUT PARAMETERS:                        
NAME					   DATATYPE DEFAULT  DESCRIPTION                        
------------------------------------------------------------                        
@Cu_Invoice_Id			   INT              
@Category_Bucket_Master_Id  INT              
@Priority_Order		   INT              
@Account_Id			   INT
              
OUTPUT PARAMETERS:              
NAME   DATATYPE DEFAULT  DESCRIPTION              
                     
------------------------------------------------------------                        
USAGE EXAMPLES:                        
------------------------------------------------------------                
              
 EXEC dbo.Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id_Account_Id  7530069,101025 ,1 ,136408          
 EXEC dbo.Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id_Account_Id  7530069,445 ,1 ,136405

 SELECT TOP 20 * FROM CU_INVOICE_DETERMINANT ORDER BY Cu_invoice_Id dESC 
 
 SELECT * FROM cu_INvoice_Service_Month WHERE Cu_invoice_id = 7530069
 
 SELECT * FROM Bucket_Category_Rule r WHERE r.Category_Bucket_Master_Id IN 
 (SELECT Bucket_Master_Id FROM Bucket_Master bm WHERE bm.Commodity_id = 290 AND bm.Bucket_Type_Cd = 100261)
               SELECT * FROM Code WHERE code_id = 100261
                   
AUTHOR INITIALS:                        
INITIALS NAME                        
------------------------------------------------------------                        
PKY   Pavan K Yadalam              

MODIFICATIONS
INITIALS DATE  MODIFICATION
------------------------------------------------------------
PKY   25-JUL-11 CREATED as a part of additional data New Cost/usage aggregation logic

*/
              
CREATE PROCEDURE dbo.Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id_Account_Id
      ( 
       @Cu_Invoice_Id INT
      ,@Category_Bucket_Master_Id INT
      ,@Priority_Order INT
      ,@Account_Id INT )
AS 
BEGIN
	
      SET NOCOUNT ON

      SELECT
            r.Category_Bucket_Master_Id
           ,cd.Code_Value Aggregation_Type_Cd
           ,r.Is_Aggregate_Category_Bucket
           ,r.Child_Bucket_Master_Id
           ,d.CU_INVOICE_CHARGE_ID
           ,d.COMMODITY_TYPE_ID
           ,d.CHARGE_NAME
           ,isnull(convert(VARCHAR(30), da.CHARGE_VALUE), d.CHARGE_VALUE) DETERMINANT_VALUE
           ,d.CU_DETERMINANT_CODE
           ,d.Bucket_Master_Id
           ,da.Charge_Expression
      FROM
            dbo.Bucket_Category_Rule r
            INNER JOIN dbo.Code cd
                  ON r.Aggregation_Type_CD = cd.Code_Id
            INNER JOIN dbo.Code aglvlCd
                  ON aglvlCd.Code_id = r.CU_Aggregation_Level_Cd
            LEFT OUTER JOIN ( dbo.CU_INVOICE_CHARGE d
                              INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT da
                                    ON d.CU_INVOICE_CHARGE_ID = da.CU_INVOICE_CHARGE_ID
                                       AND @Account_Id = da.ACCOUNT_ID )
                              ON r.Child_Bucket_Master_Id = d.Bucket_Master_Id
                                 AND d.CU_INVOICE_ID = @Cu_Invoice_Id
      WHERE
            aglvlCd.Code_Value = 'Invoice'
            AND r.Category_Bucket_Master_Id = @Category_Bucket_Master_Id
            AND Priority_Order = @Priority_Order
END
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_Charge_Category_Buckets_By_Priority_Invoice_Id_Account_Id] TO [CBMSApplication]
GO
