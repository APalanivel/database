SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
NAME:    [Workflow].[Get_Left_Navigation_Default_Exceptions]  
DESCRIPTION: The New Stored Procedure is created to get the let navigation of queue resulets  
  
->  When the user click Queue in CBMS, this procedure will be executed to get the left navigation values based on the user permissions  
->  This procedure will give total count of exception by default for My exceptions and Global.  
------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
  @userID  INT,   
  @moduleID  INT ,  
  @startindex int,  
  @Endindex  int =2147483647  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 EXEC [Workflow].[Get_Left_Navigation_Default_Exceptions]   
  -- Add the parameters for the stored procedure here   
  @userID   INT=49,   
  @moduleID INT =1,  
  @startindex int=1,  
  @Endindex int =2147483647  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKP   Arunkumar Summit Energy   
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AKP    Aug 7,2019 Created  
  
******/  
  
    
CREATE PROCEDURE [Workflow].[Get_Left_Navigation_Default_Exceptions]       
  -- Add the parameters for the stored procedure here       
  @userID   INT,       
  @moduleID INT ,      
  @startindex int,      
  @Endindex int =2147483647      
      
AS       
  BEGIN -- SET NOCOUNT ON added to prevent extra result sets from       
      -- interfering with SELECT statements.       
      DECLARE @Proc_name     VARCHAR(100) =       
              'GetLeftNavigationQueueandActionsPermissions',       
              @Input_Params  VARCHAR (1000),       
              @Error_Line    INT,       
              @Error_Message VARCHAR(3000)       
      
      SELECT @Input_Params = Cast ( @userID AS VARCHAR) + ','       
                             + Cast( @moduleID AS VARCHAR)       
      
      SET nocount ON;       
      
      DECLARE @ISUSERSAVEDFILTERACTIVE BIT       

      
      -- INSERT STATEMENTS FOR PROCEDURE HERE       
           
      
      BEGIN TRY        
      
    IF @USERID IS NULL       
        RAISERROR ('User ID is Null.', -- Message text.        
               16, -- Severity.        
               1 -- State.        
               );       
      
;WITH Left_Navigation_Permissions       
     AS (SELECT DISTINCT WQS.workflow_sub_queue_id        AS ID,       
                         WQS.parent_workflow_sub_queue_id AS PARENTID,       
                         WQS.workflow_sub_queue_name,       
                         WQS.permission_info_id,       
                         WQS.workflow_queue_id,       
                         P.permission_name ,      
       NULL AS TOTAL_COUNT       
         FROM   group_info GI       
                JOIN user_info_group_info_map UIG       
                  ON GI.group_info_id = UIG.group_info_id       
                JOIN user_info U       
                  ON U.user_info_id = UIG.user_info_id       
                JOIN group_info_permission_info_map GIPM       
                  ON GIPM.group_info_id = GI.group_info_id       
                JOIN permission_info P WITH (nolock)       
                  ON P.permission_info_id = GIPM.permission_info_id       
                JOIN workflow.workflow_sub_queue WQS       
    on WQS.permission_info_id = P.permission_info_id       
         WHERE  U.queue_id = @userID       
   AND WQS.Workflow_Queue_Id = @moduleID      
                AND parent_workflow_sub_queue_id IS NULL       
         UNION ALL       
         SELECT a.workflow_sub_queue_id        AS id,       
                a.parent_workflow_sub_queue_id AS parentid,       
                a.workflow_sub_queue_name,       
                a.permission_info_id,       
                a.workflow_queue_id,       
                NULL,      
    NULL AS TOTAL_COUNT        
         FROM   [WORKFLOW].[workflow_sub_queue] a       
                JOIN Left_Navigation_Permissions c       
                  ON a.parent_workflow_sub_queue_id = c.id)       
       
 SELECT * INTO #Left_Navigation_Permissions      
      FROM Left_Navigation_Permissions      
      
      
      
      
iF EXISTS  (SELECT 1 FROM #Left_Navigation_Permissions WHERE Workflow_Sub_Queue_NAME ='My Priority Queue' )      
      
BEGIN       
      
 SELECT        
  vw.[Inv Id] as Inv_Id,        
  vw.[Date in Queue] as [Date_in_Queue] ,        
  vw.[Date in CBMS] as Date_in_CBMS,        
  vw.Type as type,        
  vw.Status as status,        
  vw.Assignee as ASsignee,        
  vw.[Data Source] as Data_Source,        
  vw.[Client] as Client,        
  vw.[Site] as Site,        
  vw.[Account] as Account,        
  vw.[Vendor] as Vendor,        
  vw.[Commodity] as Commodity,         
  CONVERT(char(3), [Month], 0) + '/'+ CAST (YEAR([Month])AS VARCHAR) AS  [Month],        
  vw.[Filename] as Filename,        
  vw.[Comments] as Comments,        
  vw.[Utility] as Utility,        
  vw.[Utility Account] as Utility_Account ,        
  vw.[Supplier] as Supplier,        
  vw.[Supplier Account] as Supplier_Account ,        
  vw.[GROUP_NAME] as GROUP_NAME,      
  ROW_NUMBER() OVER (Partition by vw.[Inv Id] ORDER BY vw.[Inv Id]) AS Row_Num        
  INTO #My_Priority_Queue_Duplicate FROM   [Workflow].[VW_QUEUE_INVOICES_GetDetails_Left_Navigation] vw        
    JOIN  WORKFLOW.CU_INVOICE_ATTRIBUTE IA ON VW.[Inv Id] = IA.CU_INVOICE_ID      
  WHERE vw.QUEUE_ID=@userID        
      
  --removing duplicate records      
      
delete from #My_Priority_Queue_Duplicate       
where row_Num <> 1       
      
select * into #My_Priority_Queue from (      
select *, row_number() over (order by Inv_Id) as indexrange from #My_Priority_Queue_Duplicate )A        
      
--update the total count for my priority queue      
      
 UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = (SELECT COUNT (1) FROM #My_Priority_Queue )      
   WHERE Workflow_Sub_Queue_NAME ='My Priority Queue'      
      
   SELECT * FROM #My_Priority_Queue       
   where indexrange between @startindex and @Endindex       
END       
      
iF EXISTS  (SELECT 1 FROM #Left_Navigation_Permissions WHERE Workflow_Sub_Queue_NAME ='All My Exceptions' )      
      
BEGIN       
      
 SELECT           
  vw.[Inv Id] as Inv_Id,        
  vw.[Date in Queue] as [Date_in_Queue] ,        
  vw.[Date in CBMS] as Date_in_CBMS,        
  vw.Type as type,        
  vw.Status as status,        
  vw.Assignee as ASsignee,        
  vw.[Data Source] as Data_Source,        
  vw.[Client] as Client,        
  vw.[Site] as Site,        
  vw.[Account] as Account,        
  vw.[Vendor] as Vendor,        
  vw.[Commodity] as Commodity,         
  CONVERT(char(3), [Month], 0) + '/'+ CAST (YEAR([Month])AS VARCHAR) AS  [Month],        
  vw.[Filename] as Filename,        
  vw.[Comments] as Comments,        
  vw.[Utility] as Utility,        
  vw.[Utility Account] as Utility_Account ,        
  vw.[Supplier] as Supplier,        
  vw.[Supplier Account] as Supplier_Account ,        
  vw.[GROUP_NAME] as GROUP_NAME,      
  ROW_NUMBER() OVER (Partition by vw.[Inv Id] ORDER BY vw.[Inv Id]) AS Row_Num       
  INTO #All_My_Exceptions_Duplicates FROM   [Workflow].[VW_QUEUE_INVOICES_GetDetails_Left_Navigation] vw         
  WHERE vw.QUEUE_ID=@userID        
      
  delete from #All_My_Exceptions_Duplicates       
where row_Num <> 1       
      
select * into #All_My_Exceptions from (      
select *, row_number() over (order by Inv_Id) as indexrange from #All_My_Exceptions_Duplicates )A        
      
 UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = (SELECT COUNT (1) FROM #All_My_Exceptions )      
   WHERE Workflow_Sub_Queue_NAME ='All My Exceptions'      
      
   UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = (SELECT COUNT (1) FROM #All_My_Exceptions )      
   WHERE Workflow_Sub_Queue_NAME ='Assigned To Me'      
      
   SELECT * FROM #All_My_Exceptions       
    where indexrange between @startindex and @Endindex       
END       
      
      
      
iF EXISTS  (SELECT 1 FROM #Left_Navigation_Permissions WHERE Workflow_Sub_Queue_NAME ='UnAssigned' )      
      
BEGIN       
      
 SELECT         
  vw.[Inv Id] as Inv_Id,        
  vw.[Date in Queue] as [Date_in_Queue] ,        
  vw.[Date in CBMS] as Date_in_CBMS,        
  vw.Type as type,        
  vw.Status as status,        
  vw.Assignee as ASsignee,        
  vw.[Data Source] as Data_Source,        
  vw.[Client] as Client,        
  vw.[Site] as Site,        
  vw.[Account] as Account,        
  vw.[Vendor] as Vendor,        
  vw.[Commodity] as Commodity,         
  CONVERT(char(3), [Month], 0) + '/'+ CAST (YEAR([Month])AS VARCHAR) AS  [Month],        
  vw.[Filename] as Filename,        
  vw.[Comments] as Comments,        
  vw.[Utility] as Utility,        
  vw.[Utility Account] as Utility_Account ,        
  vw.[Supplier] as Supplier,        
  vw.[Supplier Account] as Supplier_Account ,        
  vw.[GROUP_NAME] as GROUP_NAME,      
  ROW_NUMBER() OVER (partition by vw.[inv id] ORDER BY vw.[Inv Id]) AS Row_Num        
  INTO #UnAssigned_Duplicates FROM   [Workflow].[VW_QUEUE_INVOICES_GetDetails_Left_Navigation] vw        
  left join user_info U       
  on vw.QUEUE_ID = u.QUEUE_ID       
  where u.QUEUE_ID is null         
      
      
  delete from #UnAssigned_Duplicates       
where row_Num <> 1       
      
select * into #UnAssigned from (      
select *, row_number() over (order by Inv_Id) as indexrange from #UnAssigned_Duplicates )A        
      
 UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = (SELECT COUNT (1) FROM #UnAssigned )      
   WHERE Workflow_Sub_Queue_NAME ='UnAssigned'       
      
   SELECT * FROM #UnAssigned       
    where indexrange between @startindex and @Endindex       
END       
      
iF EXISTS  (SELECT 1 FROM #Left_Navigation_Permissions WHERE Workflow_Sub_Queue_NAME ='Assigned' )      
      
BEGIN       
      
 SELECT            
  vw.[Inv Id] as Inv_Id,        
  vw.[Date in Queue] as [Date_in_Queue] ,        
  vw.[Date in CBMS] as Date_in_CBMS,        
  vw.Type as type,        
  vw.Status as status,        
  vw.Assignee as ASsignee,        
  vw.[Data Source] as Data_Source,        
  vw.[Client] as Client,        
  vw.[Site] as Site,        
  vw.[Account] as Account,        
  vw.[Vendor] as Vendor,        
  vw.[Commodity] as Commodity,         
  CONVERT(char(3), [Month], 0) + '/'+ CAST (YEAR([Month])AS VARCHAR) AS  [Month],        
  vw.[Filename] as Filename,        
  vw.[Comments] as Comments,        
  vw.[Utility] as Utility,        
  vw.[Utility Account] as Utility_Account ,        
  vw.[Supplier] as Supplier,        
  vw.[Supplier Account] as Supplier_Account ,        
  vw.[GROUP_NAME] as GROUP_NAME,      
  ROW_NUMBER() OVER (partition by  vw.[Inv Id] ORDER BY vw.[Inv Id]) AS Row_Num        
  INTO #Assigned_Duplicates FROM   [Workflow].[VW_QUEUE_INVOICES_GetDetails_Left_Navigation] vw        
  join user_info U       
  on vw.QUEUE_ID = u.QUEUE_ID       
  where u.QUEUE_ID <> @userID         
      
  delete from #Assigned_Duplicates       
where row_Num <> 1       
      
select * into #Assigned from (      
select *, row_number() over (order by Inv_Id) as indexrange from #Assigned_Duplicates )A        
      
 UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = (SELECT COUNT (1) FROM #Assigned )      
   WHERE Workflow_Sub_Queue_NAME ='Assigned'       
      
UPDATE #Left_Navigation_Permissions       
 SET TOTAL_COUNT = isnull((SELECT COUNT (1) FROM #Assigned ),0) + isnull((SELECT COUNT (1) FROM #UnAssigned ),0)      
   WHERE Workflow_Sub_Queue_NAME ='Global Items'       
      
   SELECT * FROM #Assigned       
    where indexrange between @startindex and @Endindex       
END       
      
      
 SELECT *       
FROM   #Left_Navigation_Permissions        
ORDER BY ID      
      
if exists (select 1 from tempdb.INFORMATION_SCHEMA.tables where table_name ='#My_Priority_Queue')      
drop table #My_Priority_Queue      
if exists (select 1 from tempdb.INFORMATION_SCHEMA.tables where table_name ='#All_My_Exceptions')      
drop table #All_My_Exceptions      
if exists (select 1 from tempdb.INFORMATION_SCHEMA.tables where table_name ='#UnAssigned')      
drop table #UnAssigned      
if exists (select 1 from tempdb.INFORMATION_SCHEMA.tables where table_name ='#Assigned')      
drop table #Assigned      
      
       
      END try       
      
      BEGIN catch       
      
      
   -- Entry made to the logging SP to capture the errors.      
          SELECT @ERROR_LINE = Error_line(),       
                 @ERROR_MESSAGE = Error_message()       
      
          INSERT INTO storedproc_error_log       
                      (storedproc_name,       
                       error_line,       
                       error_message,       
                       input_params)       
          VALUES      ( @PROC_NAME,       
                        @ERROR_LINE,       
                        @ERROR_MESSAGE,       
                        @INPUT_PARAMS )       
      END catch       
  END          
GO
GRANT EXECUTE ON  [Workflow].[Get_Left_Navigation_Default_Exceptions] TO [CBMSApplication]
GO
