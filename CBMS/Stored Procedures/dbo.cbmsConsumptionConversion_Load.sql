SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.cbmsConsumptionConversion_Load(    
	 @MyAccountId INT
	 ,@BaseUnitId INT = NULL)    
AS
BEGIN    

	SET NOCOUNT ON
	
	SELECT e2.Entity_Id AS 'base_unit_id'
		, e2.Entity_Name AS 'base_unit'
		, e.Entity_Id AS 'converted_unit_id'
		, e.Entity_Name AS 'converted_unit'
		, CASE e.Entity_Type
			WHEN 101 THEN 'Electric Power'  
			WHEN 102 THEN 'Natural Gas'
			WHEN 717 THEN 'Coal'
			WHEN 718 THEN 'Diesel'
			WHEN 719 THEN 'Number 1 Fuel Oil'
			WHEN 720 THEN 'Number 2 Fuel Oil'
			WHEN 721 THEN 'Number 4 Fuel Oil'
			WHEN 722 THEN 'Number 5 Fuel Oil'
			WHEN 723 THEN 'Number 6 Fuel Oil'
			WHEN 724 THEN 'Propane'
			WHEN 725 THEN 'Water'
			WHEN 726 THEN 'Bark/Wood Chips'
			WHEN 727 THEN 'Black Liquor'
			WHEN 728 THEN 'Waste Oil'
			WHEN 729 THEN 'Tires'
			WHEN 730 THEN 'Kerosene'
			WHEN 731 THEN 'Steam'
			WHEN 732 THEN 'Sludge'
			WHEN 733 THEN 'Chilled Water'
			WHEN 734 THEN 'Liquefied Petroleum Gas (LPG)'
			WHEN 735 THEN 'Gas oil low sulphur'
			WHEN 740 THEN 'Landfill Gas'
			WHEN 741 THEN 'Liquefied Natural Gas' --  modified spelling of Liquified to Liquefied
			WHEN 742 THEN 'Waste Water' -- added by lakshmi
			WHEN 743 then 'Propane Air'
		 END AS 'converted_unit_type'
		, cuc.Conversion_Factor

	FROM dbo.Entity e
	
		CROSS JOIN dbo.Entity e2
		LEFT JOIN dbo.Consumption_Unit_Conversion cuc ON (cuc.Base_Unit_Id = e2.Entity_Id
			AND cuc.Converted_Unit_Id = e.Entity_Id)

	WHERE e.Entity_Type IN (101, 102, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734,735,740,741, 742, 743)
		AND (e2.Entity_Type IN (101, 102, 717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734,735, 740, 741, 742, 743))
		AND e2.Entity_Id = COALESCE(@BaseUnitId, e2.Entity_Id)
		
	ORDER BY 
		CASE e.Entity_Type     
			WHEN 101 THEN 'Electric Power'    
			WHEN 102 THEN 'Natural Gas'    
			WHEN 717 THEN 'Coal'    
			WHEN 718 THEN 'Diesel'    
			WHEN 719 THEN 'Number 1 Fuel Oil'    
			WHEN 720 THEN 'Number 2 Fuel Oil'    
			WHEN 721 THEN 'Number 4 Fuel Oil'    
			WHEN 722 THEN 'Number 5 Fuel Oil'    
			WHEN 723 THEN 'Number 6 Fuel Oil'    
			WHEN 724 THEN 'Propane'    
			WHEN 725 THEN 'Water'    
			WHEN 726 THEN 'Bark/Wood Chips'    
			WHEN 727 THEN 'Black Liquor'    
			WHEN 728 THEN 'Waste Oil'    
			WHEN 729 THEN 'Tires'    
			WHEN 730 THEN 'Kerosene'    
			WHEN 731 THEN 'Steam'    
			WHEN 732 THEN 'Sludge'    
			WHEN 733 THEN 'Chilled Water'    
			WHEN 734 THEN 'Liquefied Petroleum Gas (LPG)'    
			WHEN 735 THEN 'Gas oil low sulphur'    
			WHEN 740 THEN 'Landfill Gas'    
			WHEN 741 THEN 'Liquefied Natural Gas'
			WHEN 742 THEN 'Waste Water'
			WHEN 743 then 'Propane Air'
		END
		, e.Entity_Name
    
END
GO
GRANT EXECUTE ON  [dbo].[cbmsConsumptionConversion_Load] TO [CBMSApplication]
GO
