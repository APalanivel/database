SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 Get_Clients_By_State_Vendor
   
DESCRIPTION:  
  Get Client names based on State / Vendor
  
INPUT PARAMETERS:  
 Name			DataType  Default Description  
------------------------------------------------------------  
@Country_Id		INT		NULL
@State_Id		INT		NULL
@Service_Id		INT		NULL
@Vendor_Id		INT		NULL
@Rate_Id		INT		NULL                   
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 SET STATISTICS IO ON  
 EXEC dbo.Get_Clients_By_State_Vendor 1 
 
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
    PRG   Prasanna Raghavendra G
  
MODIFICATIONS:  
 Initials Date		Modification  
------------------------------------------------------------  
PRG		08-APR-2019	Created For Budget 2.0  
******/
CREATE PROCEDURE [dbo].[Get_Clients_By_State_Vendor]
    (
        @Country_Id INT = NULL
        , @State_Id INT = NULL
        , @Service_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Rate_Id INT = NULL
        , @Keyword NVARCHAR(MAX) = NULL
        , @Start_Index INT = 1
        , @End_Index INT = 2147483647

    )
AS
    BEGIN

        SET NOCOUNT ON;

        WITH Cte_Vendors
        AS (
               SELECT
                    ch.Client_Id
                    , ch.Client_Name
                    , Row_Num = ROW_NUMBER() OVER (ORDER BY
                                                       ch.Client_Id)
                    , Total_Rows = COUNT(1) OVER ()
               FROM
                    Core.Client_Hier AS ch
                    INNER JOIN Core.Client_Hier_Account AS cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
               WHERE
                    cha.Account_Not_Managed = 0
                    AND ch.Client_Not_Managed = 0
                    AND (   @Country_Id IS NULL
                            OR  ch.Country_Id = @Country_Id)
                    AND (   @State_Id IS NULL
                            OR  ch.State_Id = @State_Id)
                    AND (   @Service_Id IS NULL
                            OR  cha.Commodity_Id = @Service_Id)
                    AND (   @Vendor_Id IS NULL
                            OR  cha.Account_Vendor_Id = @Vendor_Id)
                    AND (   @Rate_Id IS NULL
                            OR  cha.Rate_Id = @Rate_Id)
                    AND (   @Keyword IS NULL
                            OR  ch.Client_Name LIKE '%' + @Keyword + '%')
               GROUP BY
                   ch.Client_Id
                   , ch.Client_Name
           )
        SELECT
            ven.Client_Id
            , ven.Client_Name
        FROM
            Cte_Vendors ven
        WHERE
            ven.Row_Num BETWEEN @Start_Index
                        AND     @End_Index
        ORDER BY
            ven.Client_Name;
    END;


GO
GRANT EXECUTE ON  [dbo].[Get_Clients_By_State_Vendor] TO [CBMSApplication]
GO
