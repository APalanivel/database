SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Bucket_Master_Uom_Map_Sel_By_Bucket_Master_Id]

DESCRIPTION:

	To get the uom mapped with the given bucket 
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@Bucket_Master_Id	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC Bucket_Master_Uom_Map_Sel_By_Bucket_Master_Id 70
	EXEC Bucket_Master_Uom_Map_Sel_By_Bucket_Master_Id 72

	SELECT * FROM Bucket_Master_Uom_Map	
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
HG			Harihara Suthan G

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
HG			2011-07-19	Created for Additional data requirement

*/

CREATE PROCEDURE dbo.Bucket_Master_Uom_Map_Sel_By_Bucket_Master_Id
      @Bucket_Master_Id INT
AS 
BEGIN

      SET NOCOUNT ON ;

      SELECT
            m.Uom_Type_Id
           ,un.Entity_Name AS Uom_Name
      FROM
            dbo.Bucket_Master_Uom_Map m
            INNER JOIN dbo.Entity un
                  ON un.Entity_id = m.Uom_Type_Id
      WHERE
            m.Bucket_Master_Id = @Bucket_Master_Id

END
;
GO
GRANT EXECUTE ON  [dbo].[Bucket_Master_Uom_Map_Sel_By_Bucket_Master_Id] TO [CBMSApplication]
GO
