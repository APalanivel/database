SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- exec dbo.GET_LPS_DETAILS_P 20467, 142, 'Baseload'

CREATE  PROCEDURE dbo.GET_LPS_DETAILS_P
	@contractID INT,
	@entityType INT,
	@entityName VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON

	SELECT lps.load_profile_specification_id,
		lps.price_index_id,  
		deal.entity_id, 
		deal.ENTITY_NAME deal_type,  
		price.pricing_poINT, 
		lps.contract_id,  
		lps.lps_type_id, 
		typ.entity_name lps_type,
		lps.month_identifier,  
		lps.lps_fixed_price,  
		lps.lps_unit_type_id, 
		unit.ENTITY_NAME unit,  
		lps.lps_frequency_type_id,  
		FREQ.ENTITY_NAME FREQUENCY,  
		lps.lps_expression 
	FROM load_profile_specification lps INNER JOIN price_index price ON lps.price_index_id = price.price_index_id
		INNER JOIN entity deal ON deal.entity_id = price.index_id
		INNER JOIN ENTITY FREQ ON FREQ.ENTITY_ID = lps.LPS_FREQUENCY_TYPE_ID
		INNER JOIN ENTITY unit ON UNIT.ENTITY_ID =lps.LPS_UNIT_TYPE_ID
		INNER JOIN entity typ ON typ.entity_id = lps.lps_type_id
	WHERE lps.contract_id = @contractID
		AND typ.entity_name = @entityName
		AND typ.entity_type = @entityType
		

END
GO
GRANT EXECUTE ON  [dbo].[GET_LPS_DETAILS_P] TO [CBMSApplication]
GO
