SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	cmbs.dbo.Etl_Get_Budget_Dtl_Info


DESCRIPTION:
	Gets all changes made between the MinDBTS and the MaxDBTS. 
	This procedure is stricly used for ETL applications 


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MinDBTS		bigint					minimum row version 
	@MaxDBTS		bigint        	        max row version   	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
EXEC dbo.Etl_Get_Budget_Dtl_Info 1, 100

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			06/08/2009	Created   
	CMH			07/10/2009	Changed logic to transfer all data for a budget that hasn changed   	
******/
CREATE PROCEDURE [dbo].[Etl_Get_Budget_Dtl_Info]  
(    
	@MinDBTS        BIGINT    
	,@MaxDBTS       BIGINT    
)    
AS     

BEGIN     
	SET NOCOUNT ON    
	  
	SELECT    
		b.Budget_Id    
		,ba.Account_Id    
		,bd.Budget_Detail_Id    
		,bd.Month_Identifier    
		,b.COMMODITY_TYPE_ID    
		,b.Budget_Start_Year    
		,isnull(bd.TRANSMISSION_VALUE,0) as TRANSMISSION_VALUE    
		,isnull(bd.TRANSPORTATION_VALUE,0) as TRANSPORTATION_VALUE    
		,isnull(bd.VARIABLE_VALUE,0) as VARIABLE_VALUE    
		,isnull(bd.DISTRIBUTION_VALUE,0) as DISTRIBUTION_VALUE    
		,isnull(bd.OTHER_BUNDLED_VALUE,0) as OTHER_BUNDLED_VALUE    
		,isnull(bd.RATES_TAX_VALUE,0) as RATES_TAX_VALUE    
		,isnull(bd.SOURCING_TAX_VALUE,0) as SOURCING_TAX_VALUE    
		,isnull(bd.BUDGET_USAGE,0) as BUDGET_USAGE    
		,isnull(bd.OTHER_FIXED_COSTS_VALUE,0) as OTHER_FIXED_COSTS_VALUE    
	FROM 
		Budget_Details bd (NOLOCK)    
		INNER JOIN Budget_Account ba (NOLOCK) 
			ON ba.Budget_Account_Id = bd.Budget_Account_Id    
		INNER JOIN Budget b (NOLOCK) 
			ON b.Budget_id = ba.Budget_id    
	WHERE 
		( BA.ROW_VERSION between @MinDBTS and @MaxDBTS    
			OR BD.ROW_VERSION between @MinDBTS and @MaxDBTS
			OR b.Row_Version between @MinDBTS and @MaxDBTS )    
		AND b.IS_POSTED_TO_DV = 1     
END
GO
GRANT EXECUTE ON  [dbo].[Etl_Get_Budget_Dtl_Info] TO [CBMSApplication]
GO
