SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
      dbo.Data_Quality_Test_Exists_Sel_By_Commodity_Id
   
 DESCRIPTION:   
      Gets the Test details based on commodity.
      
 INPUT PARAMETERS:  
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 @Commodity_Id		INT

 OUTPUT PARAMETERS:
 Name				DataType			Default				Description  
---------------------------------------------------------------------------- 
 USAGE EXAMPLES:  
---------------------------------------------------------------------------- 

EXEC dbo.Data_Quality_Test_Exists_Sel_By_Commodity_Id
   @Commodity_Id = 290
    

EXEC dbo.Data_Quality_Test_Exists_Sel_By_Commodity_Id
    @Commodity_Id = 291

AUTHOR INITIALS:
Initials	Name
---------------------------------------------------------------------------- 
 NR			Narayana Reddy
 

 MODIFICATIONS   
 Initials	Date			Modification
----------------------------------------------------------------------------
 NR        2019-03-28		Created for Data Quality.

******/


CREATE PROCEDURE [dbo].[Data_Quality_Test_Exists_Sel_By_Commodity_Id]
     (
         @Commodity_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Data_Quality_Test_Exists BIT = 0;


        SELECT
            @Data_Quality_Test_Exists = 1
        FROM
            dbo.Data_Quality_Test dqt
        WHERE
            dqt.Commodity_Id = @Commodity_Id;

        SELECT  @Data_Quality_Test_Exists AS Data_Quality_Test_Exists;


    END;



GO
GRANT EXECUTE ON  [dbo].[Data_Quality_Test_Exists_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
