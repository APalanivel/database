SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  Budget.Calc_Tester_Ins  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								 DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Calculation_Request_Type_Cd			INT
 @Test_Type_Cd							INT
 @Verification_Status_Cd				BIT
 @Account_Id							INT
 @Meter_Id								INT
 @Term_Start_Dt						 DATETIME
 @Term_End_Dt						 DATETIME
 @User_Info_Id							INT
  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
  
  

 Exec Budget.Calculation_Tester_Audit_Ins 
  

  
AUTHOR INITIALS:  
 Initials	         Name  
-------------------------------------------------------------------------------------  
 SP		             Srinivas Patchava 
   
MODIFICATIONS  
  
 Initials		Date			     Modification  
-------------------------------------------------------------------------------------  
 SP			   2019-06-04		      Created
            
              
******/
CREATE PROCEDURE [Budget].[Calculation_Tester_Audit_Ins]
     (
         @Calculation_Request_Type_Cd INT
         , @Data_Type_Cd INT
         , @Verification_Status_Cd BIT
         , @Account_Id INT
         , @Meter_Id INT
         , @Term_Start_Dt DATETIME
         , @Term_End_Dt DATETIME
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        BEGIN TRY
            BEGIN TRANSACTION;
            INSERT INTO Budget.Calculation_Tester_Audit
                 (
                     Calculation_Request_Type_Cd
                     , Data_Type_Cd
                     , Verification_Status_Cd
                     , Account_Id
                     , Meter_Id
                     , Term_Start_Dt
                     , Term_End_Dt
                     , Verified_User_Id
                     , Verified_Ts
                 )
            VALUES
                (@Calculation_Request_Type_Cd
                 , @Data_Type_Cd
                 , @Verification_Status_Cd
                 , @Account_Id
                 , @Meter_Id
                 , @Term_Start_Dt
                 , @Term_End_Dt
                 , @User_Info_Id
                 , GETDATE());
            COMMIT TRANSACTION;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION;
                END;
            EXEC dbo.usp_RethrowError;
        END CATCH;

    END;

GO
GRANT EXECUTE ON  [Budget].[Calculation_Tester_Audit_Ins] TO [CBMSApplication]
GO
