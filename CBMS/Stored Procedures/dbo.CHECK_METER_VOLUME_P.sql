SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.CHECK_METER_VOLUME_P
	@contract_id INT,
	@meter_id INT,
	@month_identifier DATETIME
AS
BEGIN

	SET NOCOUNT ON

	SELECT volume
	FROM dbo.contract_meter_volume
	WHERE contract_id = @contract_id
		AND meter_id = @meter_id
		AND month_identifier = @month_identifier

END
GO
GRANT EXECUTE ON  [dbo].[CHECK_METER_VOLUME_P] TO [CBMSApplication]
GO
