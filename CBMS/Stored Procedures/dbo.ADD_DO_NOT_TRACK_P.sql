
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.ADD_DO_NOT_TRACK_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userInfoID		INT
    @accID			INT
    @reasonTypeID	INT
    @accNumber		VARCHAR(200)
    @clientName		VARCHAR(50)
    @clientCity		VARCHAR(200)
    @stateID		INT
    @vendorName		VARCHAR(200)
    @dateAdded		DATETIME      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN


EXEC dbo.ADD_DO_NOT_TRACK_P
   @userInfoID = 16
     ,@accID  = 7347
     ,@reasonTypeID = 314
     ,@accNumber ='7561'
     ,@clientName = 'Brooksville, KY (City of )'
     ,@clientCity ='Brooksville'
     ,@stateID =18
     ,@vendorName = 'Columbia Gas Transmission Corporation (Pipeline) KY'
     ,@dateAdded = '2013-12-09 14:22:14.967'

ROLLBACK TRAN

SELECT TOP 10 * FROM Core.Client_Hier_Account x WHERE NOT EXISTS(SELECT 1 FROM DO_NOT_TRACK t WHERE t.Account_Id = x.Account_Id)

SELECT * FROM CORE.Client_Hier where client_Hier_Id = 1130

SELECT TOP 10 * FROM DO_NOT_TRACK

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2013-12-09	Comments header added
							MAINT-2419, @Client_Name param width changed to 200 as Client_Name column width on DO_NOT_TRACK increased to VARCHAR(200) to match with Client.Client_Name
******/

CREATE PROCEDURE dbo.ADD_DO_NOT_TRACK_P
(      @userInfoID INT
     ,@accID INT
     ,@reasonTypeID INT
     ,@accNumber VARCHAR(200)
     ,@clientName VARCHAR(50)
     ,@clientCity VARCHAR(200)
     ,@stateID INT
     ,@vendorName VARCHAR(200)
     ,@dateAdded DATETIME)
AS
BEGIN

      SET NOCOUNT ON

      INSERT      INTO DO_NOT_TRACK
                  (USER_INFO_ID
                  ,ACCOUNT_ID
                  ,REASON_TYPE_ID
                  ,ACCOUNT_NUMBER
                  ,CLIENT_NAME
                  ,CLIENT_CITY
                  ,STATE_ID
                  ,VENDOR_NAME
                  ,DATE_ADDED )
      VALUES
                  (@userInfoID
                  ,@accID
                  ,@reasonTypeID
                  ,@accNumber
                  ,@clientName
                  ,@clientCity
                  ,@stateID
                  ,@vendorName
                  ,@dateAdded )

END
;
GO

GRANT EXECUTE ON  [dbo].[ADD_DO_NOT_TRACK_P] TO [CBMSApplication]
GO
