SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_INTERNAL_APP_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@hostAddress   	varchar(200)	          	
	@hostName      	varchar(200)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE dbo.GET_INTERNAL_APP_INFO_P

@hostAddress varchar(200), 
@hostName varchar(200)

AS
set nocount on
	SELECT	aprof.APP_INT_SERVER 
	FROM	APP_PROFILE aprof, APP_SERVER aserv 
	WHERE	aserv.APP_SERVER_IP = @hostAddress AND 
		aserv.APP_SERVER_NAME = @hostName AND 
		aprof.APP_PROFILE_ID = aserv.APP_PROFILE_ID
GO
GRANT EXECUTE ON  [dbo].[GET_INTERNAL_APP_INFO_P] TO [CBMSApplication]
GO
