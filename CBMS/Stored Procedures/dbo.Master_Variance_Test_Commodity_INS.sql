SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********   
NAME:  dbo.Master_Variance_Test_Commodity_INS  
 
DESCRIPTION:  Used to Insert data into Variance_Test_Commodity_Map  table  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
	@Variance_Test_Id		Int
	@Commodity_Id			Int
	
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:   

	
------------------------------------------------------------  
AUTHOR INITIALS:  
Initials Name  
------------------------------------------------------------  
NK   Nageswara Rao Kosuri         


Initials Date  Modification  
------------------------------------------------------------  
NK	10/19/2009  Created


 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.Master_Variance_Test_Commodity_INS
	@Variance_Test_Id int,
	@Commdoity_Id Int	
AS

BEGIN
	
	SET NOCOUNT ON;
	 
	 INSERT INTO Variance_Test_Commodity_Map (
		Commodity_Id,
		Variance_Test_Id
		)    
	values(@Commdoity_Id
		,@Variance_Test_Id)
	
END
GO
GRANT EXECUTE ON  [dbo].[Master_Variance_Test_Commodity_INS] TO [CBMSApplication]
GO
