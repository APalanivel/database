SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------------------------------------  
/******  
NAME:  
 [DBO].[METER_ASSOCIATE_DISASSOCIATE_DATE_SEL]  
  
DESCRIPTION:  
 GET METER_ASSOCIATION_DATE, METER_DISASSOCIATION_DATE FROM SUPP_ACCOUNT_METER_MAP TABLE  
   
INPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
@CONTRACTID  INT  
@ACCOUNTID  INT  
@METERID  INT  
  
OUTPUT PARAMETERS:  
NAME   DATATYPE DEFAULT  DESCRIPTION  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
 EXEC METER_ASSOCIATE_DISASSOCIATE_DATE_SEL 12038,4638,3928
 EXEC METER_ASSOCIATE_DISASSOCIATE_DATE_SEL 24376,14229,7814
 SELECT * FROM dbo.SUPPLIER_ACCOUNT_METER_MAP
  
AUTHOR INITIALS:  
INITIALS NAME  
------------------------------------------------------------  
MGB  BHASKARAN GOPALAKRISHNAN  
  
MODIFICATIONS  
INITIALS DATE  MODIFICATION  
------------------------------------------------------------  
MGB  06/12/2010  CREATED  
  
*/  
  
CREATE PROCEDURE [DBO].[METER_ASSOCIATE_DISASSOCIATE_DATE_SEL]
      @CONTRACTID INT
     ,@ACCOUNTID INT
     ,@METERID INT
AS 
BEGIN    
      SET NOCOUNT ON  
      SELECT
            METER_ASSOCIATION_DATE
           ,METER_DISASSOCIATION_DATE
      FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP
      WHERE
            CONTRACT_ID = @CONTRACTID
            AND ACCOUNT_ID = @ACCOUNTID
            AND METER_ID = @METERID     
END 
GO
GRANT EXECUTE ON  [dbo].[METER_ASSOCIATE_DISASSOCIATE_DATE_SEL] TO [CBMSApplication]
GO
