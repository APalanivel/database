SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec dbo.GET_SITE_ACCOUNT_GROUP_P 2853

CREATE   PROCEDURE dbo.GET_SITE_ACCOUNT_GROUP_P
	@site_id int
	AS
	begin
		set nocount on

		SELECT distinct	accountGroup.ACCOUNT_GROUP_ID, 
				accountGroup.GROUP_BILLING_NUMBER, 
				vendor.VENDOR_NAME, isnull(suppacc.ACCOUNT_NUMBER,'Not yet Assigned') as account_number  
		FROM 		ACCOUNT_GROUP accountGroup
				join account suppacc on suppacc.ACCOUNT_GROUP_ID = accountGroup.ACCOUNT_GROUP_ID 
				join account utilacc on utilacc.ACCOUNT_GROUP_ID = accountGroup.ACCOUNT_GROUP_ID
				and utilacc.SITE_ID = @site_id 
				join VENDOR vendor on vendor.VENDOR_ID = accountGroup.VENDOR_ID
				
		ORDER BY 	accountGroup.ACCOUNT_GROUP_ID 

	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_ACCOUNT_GROUP_P] TO [CBMSApplication]
GO
