SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE     PROCEDURE dbo.dmlLookup_Get
	( @Id int = null
	, @LookupTypeId int = null 
	)
AS
BEGIN
   select LookupId
	, Code
	, LookupTypeId
	, LookupName
	, SortOrder
     from dmlLookup
    where LookupId = isNull(@Id, LookupId)
      and LookupTypeId = isNull(@LookupTypeId, LookupTypeId)
 order by LookupName asc
END




GO
GRANT EXECUTE ON  [dbo].[dmlLookup_Get] TO [CBMSApplication]
GO
