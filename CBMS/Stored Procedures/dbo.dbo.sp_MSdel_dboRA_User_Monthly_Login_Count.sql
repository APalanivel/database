SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dbo].[dbo.sp_MSdel_dboRA_User_Monthly_Login_Count]
		@pkc1 int,
		@pkc2 date
as
begin  
	delete [dbo].[RA_User_Monthly_Login_Count]
where [USER_INFO_ID] = @pkc1
  and [Login_Month] = @pkc2
if @@rowcount = 0
    if @@microsoftversion>0x07320000
        exec sp_MSreplraiserror 20598
end  
GO
