SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name

DESCRIPTION:  

	Displays the Schedule details of the Vendor/Commodity/Schedule Name combination

INPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------
	  @Vendor_Id						INT
      @Commodity_Id						INT
      @Schedule_Name					NVARCHAR(100)
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	Exec dbo.TOU_Schedule_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name 454,290,'No'
	Exec dbo.TOU_Schedule_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name 12730

AUTHOR INITIALS:
	Initials Name
------------------------------------------------------------
	BCH		Balaraju


	Initials Date		Modification
------------------------------------------------------------
	BCH		2012-07-10  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name
( 
 @Vendor_Id INT
,@Commodity_Id INT = NULL
,@Schedule_Name NVARCHAR(100) = NULL )
AS 
BEGIN
    
      SET NOCOUNT ON ;
      SELECT
            TOUS.Time_Of_Use_Schedule_Id
           ,TOUS.Schedule_Name
           ,TOUS.Commodity_Id
           ,COM.Commodity_Name
           ,TOUS.Comment_ID
           ,CMT.Comment_Text
      FROM
            dbo.Time_Of_Use_Schedule TOUS
            JOIN dbo.VENDOR ven
                  ON TOUS.VENDOR_ID = ven.VENDOR_ID
            JOIN dbo.Commodity COM
                  ON TOUS.Commodity_Id = COM.Commodity_Id
            LEFT JOIN dbo.Comment CMT
                  ON TOUS.Comment_ID = CMT.Comment_ID
      WHERE
            Tous.VENDOR_ID = @Vendor_Id
            AND ( @Commodity_Id IS NULL
                  OR TOUS.Commodity_Id = @Commodity_Id )
            AND ( @Schedule_Name IS NULL
                  OR Tous.Schedule_Name LIKE '%' + @Schedule_Name + '%' )
      ORDER BY
            COM.Commodity_Name
           ,TOUS.Schedule_Name
 
END


;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Sel_By_Vendor_Id_Commodity_Id_Schedule_Name] TO [CBMSApplication]
GO
