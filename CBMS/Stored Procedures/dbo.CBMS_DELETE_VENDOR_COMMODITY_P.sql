SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[CBMS_DELETE_VENDOR_COMMODITY_P]


DESCRIPTION: Deeltes data from the VENDOR_COMMODITY_MAP table.

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@vendor_id              int      
    
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@rate_id                int 


USAGE EXAMPLES:
------------------------------------------------------------
exec [CBMS_DELETE_VENDOR_COMMODITY_P] vendor_id = 3051


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
create PROCEDURE [dbo].[CBMS_DELETE_VENDOR_COMMODITY_P]   
@vendor_id int  
  
AS  

 

DELETE FROM 
	VENDOR_COMMODITY_MAP 
WHERE 
	VENDOR_ID = @vendor_id AND IS_HISTORY=0
GO
GRANT EXECUTE ON  [dbo].[CBMS_DELETE_VENDOR_COMMODITY_P] TO [CBMSApplication]
GO
