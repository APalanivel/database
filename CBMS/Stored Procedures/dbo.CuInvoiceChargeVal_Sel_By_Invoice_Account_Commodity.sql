SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.CuInvoiceChargeVal_Sel_By_Invoice_Account_Commodity   
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name				DataType  Default      Description    
---------------------------------------------------------------------------                  
 @cu_invoice_id		INT      
 @account_id		INT       NULL       
 @Commodity_Id		INT       NULL

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
     
 
 EXEC CuInvoiceChargeVal_Sel_By_Invoice_Account_Commodity 
      59576661
     ,1366849
     ,290  
     
AUTHOR INITIALS:    
 Initials	Name    
------------------------------------------------------------    
   
 RKV		Ravi Kumar Vegesna 
 SP         Srinivas Patchava 
   
MODIFICATIONS    
    
 Initials	Date		Modification    
------------------------------------------------------------    
 RKV		2018-02-12	Created
 SP         2019-07-16  SE2017-733 ACT Commodity Mapping within CBMS

******/
CREATE PROCEDURE [dbo].[CuInvoiceChargeVal_Sel_By_Invoice_Account_Commodity]
    (
        @cu_invoice_id INT
        , @account_id INT = NULL
        , @Commodity_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Charge
             (
                 Bucket_Type NVARCHAR(255)
                 , Charge_Value DECIMAL(28, 10)
                 , Commodity_Type_Id INT
                 , Commodity_Type VARCHAR(200)
                 , Aggregation_Type VARCHAR(25)
                 , Row_Num INT
             );

        CREATE TABLE #Final_Charge_Values
             (
                 Bucket_Type NVARCHAR(255)
                 , ChargeValue DECIMAL(28, 10)
                 , Commodity_Type_Id INT
                 , Commodity_Type VARCHAR(200)
             );

        DECLARE @Aggregation_Type_Tbl TABLE
              (
                  Aggregation_Type VARCHAR(25)
                  , Row_Num INT
              );

        DECLARE
            @Ubm_Name VARCHAR(200)
            , @Priority_Order INT
            , @Total_Rows INT
            , @Current_Row_Num INT = 1
            , @Sql VARCHAR(MAX)
            , @Aggregation_Type VARCHAR(25)
            , @Currency_Unit_Name VARCHAR(200);

        DECLARE @Invoice_Bucket_Master_Ids TABLE
              (
                  Bucket_Master_Id INT
              );
        DECLARE @Total_Cost_Child_Buckets TABLE
              (
                  Bucket_Master_Id INT
                  , Priority_Order INT
              );

        SELECT
            @Currency_Unit_Name = cu.CURRENCY_UNIT_NAME
        FROM
            dbo.CU_INVOICE inv
            JOIN dbo.CURRENCY_UNIT cu
                ON cu.CURRENCY_UNIT_ID = inv.CURRENCY_UNIT_ID
        WHERE
            inv.CU_INVOICE_ID = @cu_invoice_id;

        CREATE TABLE #Bucket_Category_Rule
             (
                 Category_Bucket_Master_Id INT
                 , Category_Bucket_Name VARCHAR(200)
                 , Priority_Order INT
                 , Child_Bucket_Master_Id INT
                 , Aggregation_Type VARCHAR(25)
             );

        INSERT INTO #Bucket_Category_Rule
             (
                 Category_Bucket_Master_Id
                 , Category_Bucket_Name
                 , Priority_Order
                 , Child_Bucket_Master_Id
                 , Aggregation_Type
             )
        SELECT
            bcr.Category_Bucket_Master_Id
            , bm.Bucket_Name
            , bcr.Priority_Order
            , bcr.Child_Bucket_Master_Id
            , at.Code_Value
        FROM
            dbo.Bucket_Category_Rule bcr
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
            INNER JOIN dbo.Bucket_Master cbm
                ON cbm.Bucket_Master_Id = bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code al
                ON al.Code_Id = bcr.CU_Aggregation_Level_Cd
            INNER JOIN dbo.Code bt
                ON bt.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN dbo.Code at
                ON at.Code_Id = bcr.Aggregation_Type_CD
        WHERE
            bm.Commodity_Id = @Commodity_Id
            AND al.Code_Value = 'Invoice'
            AND bt.Code_Value = 'Charge';

        --/*
        INSERT INTO @Total_Cost_Child_Buckets
             (
                 Bucket_Master_Id
                 , Priority_Order
             )
        SELECT
            bcr.Child_Bucket_Master_Id
            , bcr.Priority_Order
        FROM
            dbo.Bucket_Category_Rule bcr
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = bcr.Category_Bucket_Master_Id
                   AND  Bucket_Name = 'Total Cost'
                   AND  Commodity_Id = @Commodity_Id
                   AND  bm.Bucket_Master_Id <> bcr.Child_Bucket_Master_Id
            INNER JOIN dbo.Code bt
                ON bt.Code_Id = bm.Bucket_Type_Cd
        WHERE
            bt.Code_Value = 'Charge';

        INSERT INTO @Invoice_Bucket_Master_Ids
             (
                 Bucket_Master_Id
             )
        SELECT
            bm.Bucket_Master_Id
        FROM
            dbo.CU_INVOICE_CHARGE cd
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT CICa
                ON CICa.CU_INVOICE_CHARGE_ID = cd.CU_INVOICE_CHARGE_ID
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = cd.COMMODITY_TYPE_ID
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cd.Bucket_Master_Id
        WHERE
            CICa.ACCOUNT_ID = @account_id
            AND bm.Commodity_Id = @Commodity_Id
            AND cd.CU_INVOICE_ID = @cu_invoice_id
        GROUP BY
            bm.Bucket_Master_Id;

        SELECT
            @Priority_Order = MAX(tucb.Priority_Order)
        FROM
            @Invoice_Bucket_Master_Ids ibmi
            INNER JOIN @Total_Cost_Child_Buckets tucb
                ON ibmi.Bucket_Master_Id = tucb.Bucket_Master_Id
        GROUP BY
            tucb.Priority_Order;

        -- Keep the breakup buckets and deletes the priority 1 buckets
        DELETE
        ibmi
        FROM
            @Invoice_Bucket_Master_Ids ibmi
            INNER JOIN @Total_Cost_Child_Buckets tucb
                ON ibmi.Bucket_Master_Id = tucb.Bucket_Master_Id
        WHERE
            tucb.Priority_Order < @Priority_Order;

        INSERT INTO #Charge
             (
                 Bucket_Type
                 , Charge_Value
                 , Commodity_Type_Id
                 , Commodity_Type
                 , Aggregation_Type
                 , Row_Num
             )
        SELECT
            CASE WHEN CIC.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name
                ELSE eisbm.Sub_Bucket_Name
            END AS bucket_type
            , (CASE WHEN NULLIF(CICA.Charge_Expression, '') IS NULL THEN
            (CONVERT(
                 DECIMAL(28, 10)
                 , REPLACE(
                       REPLACE(
                           REPLACE(
                               REPLACE(
                                   REPLACE(
                                       REPLACE(
                                           (CASE WHEN (PATINDEX('%[0-9]-', CIC.CHARGE_VALUE)) > 0 THEN
                                                     STUFF(
                                                         CIC.CHARGE_VALUE, PATINDEX('%[0-9]-', CIC.CHARGE_VALUE) + 1, 1
                                                         , '')
                                                ELSE CIC.CHARGE_VALUE
                                            END), ',', ''), '0-', 0), '+', ''), '$', ''), ')', ''), '(', '')))
                   ELSE CICA.Charge_Value
               END)
            , CIC.COMMODITY_TYPE_ID
            , com.Commodity_Name
            , ISNULL(x.Aggregation_Type, 'Sum') -- NG other doesn't get aggregated to any other bucket, for such cases defining it as other.
            , DENSE_RANK() OVER (PARTITION BY
                                     CASE WHEN CIC.EC_Invoice_Sub_Bucket_Master_Id IS NULL THEN bm.Bucket_Name
                                         ELSE eisbm.Sub_Bucket_Name
                                     END
                                 ORDER BY
                                     CIC.CU_INVOICE_CHARGE_ID) Row_Num
        FROM
            dbo.CU_INVOICE_CHARGE CIC
            INNER JOIN @Invoice_Bucket_Master_Ids ibm
                ON ibm.Bucket_Master_Id = CIC.Bucket_Master_Id
            LEFT OUTER JOIN
            (   SELECT
                    Child_Bucket_Master_Id
                    , Aggregation_Type
                FROM
                    #Bucket_Category_Rule bcr
                GROUP BY
                    Child_Bucket_Master_Id
                    , Aggregation_Type) x
                ON x.Child_Bucket_Master_Id = CIC.Bucket_Master_Id
            INNER JOIN dbo.CU_INVOICE_CHARGE_ACCOUNT CICA
                ON CICA.CU_INVOICE_CHARGE_ID = CIC.CU_INVOICE_CHARGE_ID
            INNER JOIN dbo.Commodity com
                ON com.Commodity_Id = CIC.COMMODITY_TYPE_ID
            LEFT OUTER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = CIC.Bucket_Master_Id
            INNER JOIN dbo.EC_Invoice_Sub_Bucket_Master eisbm
                ON CIC.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
        WHERE
            CIC.CU_INVOICE_ID = @cu_invoice_id
            AND (   @account_id IS NULL
                    OR  CICA.ACCOUNT_ID = @account_id)
            AND (   @Commodity_Id IS NULL
                    OR  com.Commodity_Id = @Commodity_Id);


        INSERT INTO @Aggregation_Type_Tbl
             (
                 Aggregation_Type
                 , Row_Num
             )
        SELECT
            Aggregation_Type
            , ROW_NUMBER() OVER (ORDER BY
                                     Aggregation_Type)
        FROM
            #Charge
        GROUP BY
            Aggregation_Type;


        SELECT  @Total_Rows = @@ROWCOUNT;

        WHILE @Current_Row_Num <= @Total_Rows
            BEGIN

                BEGIN TRY

                    SELECT
                        @Aggregation_Type = Aggregation_Type
                    FROM
                        @Aggregation_Type_Tbl
                    WHERE
                        Row_Num = @Current_Row_Num;

                    IF @Aggregation_Type <> 'Recent'
                        BEGIN

                            SELECT
                                @Sql = 'INSERT INTO #Final_Charge_Values
									( Bucket_Type
									,ChargeValue
									,Commodity_Type_Id
                                    ,Commodity_Type )
                                    SELECT
                                          dv.Bucket_Type
                                         ,' + @Aggregation_Type + '(dv.Charge_Value)'
                                       + '
                                         ,dv.Commodity_Type_Id
                                         ,dv.Commodity_Type
                                    FROM
                                          #Charge dv
                                    WHERE
                                          dv.Aggregation_Type = ' + '''' + @Aggregation_Type + ''''
                                       + '
                                    GROUP BY
										  dv.Bucket_Type
                                         ,dv.Commodity_Type_Id
                                         ,dv.Commodity_Type';

                            PRINT @Current_Row_Num;

                            EXEC (@Sql);
                        END;
                    ELSE
                        BEGIN
                            INSERT INTO #Final_Charge_Values
                                 (
                                     Bucket_Type
                                     , ChargeValue
                                     , Commodity_Type_Id
                                     , Commodity_Type
                                 )
                            SELECT
                                dv.Bucket_Type
                                , dv.Charge_Value
                                , dv.Commodity_Type_Id
                                , dv.Commodity_Type
                            FROM
                                #Charge dv
                            WHERE
                                dv.Aggregation_Type = @Aggregation_Type
                                AND dv.Row_Num = 1; -- Most recent will fetch First entry present in the Invoice for each Sub bucket/bucket
                        END;

                END TRY
                BEGIN CATCH

                    EXEC dbo.usp_RethrowError;
                    BREAK;

                END CATCH;
                SET @Current_Row_Num = @Current_Row_Num + 1;
            END;

        SELECT
            Bucket_Type
            , ChargeValue
            , '' Currency_Unit_Name
            , Commodity_Type_Id
            , Commodity_Type
            , ISNULL(ac.ACT_Commodity_XName, Commodity_Type) AS ACT_Commodity_XName
        FROM
            #Final_Charge_Values fv
            LEFT OUTER JOIN(dbo.ACT_Commodity ac
                            INNER JOIN dbo.Commodity_ACT_Commodity_Map cacm
                                ON cacm.ACT_Commodity_Id = ac.ACT_Commodity_Id)
                ON cacm.Commodity_Id = fv.Commodity_Type_Id;

        DROP TABLE
            #Bucket_Category_Rule
            , #Charge
            , #Final_Charge_Values;

    END;

    ;
    ;
    ;

GO
GRANT EXECUTE ON  [dbo].[CuInvoiceChargeVal_Sel_By_Invoice_Account_Commodity] TO [CBMSApplication]
GO
