SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Category_Map_Upd]
           
DESCRIPTION:             
			To update category display order within the template
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Sr_Service_Condition_Template_Id	INT
    @Sr_Service_Condition_Category_Id	INT
    @User_Info_Id						INT
    @Display_Seq INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRAN
		EXEC dbo.Sr_Service_Condition_Template_Category_Sel_By_Template_Language_CD 3 
		EXEC dbo.Sr_Service_Condition_Template_Category_Map_Upd 
			@Sr_Service_Condition_Template_Id = 3
			,@Sr_Service_Condition_Category_Id = 4
			,@Display_Seq = 2
			,@User_Info_Id = 49
		EXEC dbo.Sr_Service_Condition_Template_Category_Sel_By_Template_Language_CD 3 
	ROLLBACK
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-02-22	Global Sourcing - Phase3 - GCS-448 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Category_Map_Upd]
      ( 
       @Sr_Service_Condition_Template_Id INT
      ,@Sr_Service_Condition_Category_Id INT
      ,@User_Info_Id INT
      ,@Display_Seq INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      UPDATE
            dbo.Sr_Service_Condition_Template_Category_Map
      SET   
            Display_Seq = @Display_Seq
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = getdate()
      WHERE
            Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND Sr_Service_Condition_Category_Id = @Sr_Service_Condition_Category_Id
                  
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Category_Map_Upd] TO [CBMSApplication]
GO
