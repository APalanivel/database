SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE          PROCEDURE [dbo].[cbmsAppMenu_GetMy]
	( @user_info_id int
	, @app_menu_profile_id int
	)
AS
BEGIN

	set nocount on

	  declare @session_uid uniqueidentifier
		, @menu_level int

	      set @session_uid = newid()
	      set @menu_level = 1

	   create table #MenuOption
		( item_id int not null identity(1,1)
		, app_menu_id int not null
		, parent_menu_id int not null
		, display_text varchar(200) not null
		, module_number int not null
		, target_action varchar(500) not null
		, target_server varchar(500) not null
		, sort_order int not null
		)

	insert into #MenuOption
		( app_menu_id, parent_menu_id
		, display_text, module_number, target_action, target_server, sort_order )
	   select distinct m.app_menu_id
		, isNull(m.parent_menu_id, m.app_menu_id) parent_menu_id
		, m.display_text
		, x.module_number
		, m.target_action
		, m.target_server
		, m.display_order
	     from app_menu m
	     join app_module x on x.app_module_id = m.app_module_id
	     join group_info_permission_info_map pm on pm.permission_info_id = m.permission_info_id
	     join user_info_group_info_map gm on gm.group_info_id = pm.group_info_id
	    where m.app_menu_profile_id = @app_menu_profile_id
	      and m.menu_level = @menu_level
	      and gm.user_info_id = @user_info_id
	 order by m.display_order


	  declare @app_menu_id int
		, @parent_menu_id int
		, @display_text varchar(200)
		, @module_number int
		, @target_action varchar(500)
		, @target_server varchar(500)
		, @count int
		, @length int

	   select @length = isNull(max(item_id),0)
	     from #MenuOption

	      set @count = 1

	      set @menu_level = @menu_level + 1
	
	while (@count <= @length)
	begin

		   select @app_menu_id = app_menu_id
			, @parent_menu_id = parent_menu_id
			, @display_text = display_text
			, @module_number = module_number
			, @target_action = target_action
			, @target_server = target_server
		     from #MenuOption
		    where item_id = @count

		insert into app_menu_temp
			( session_uid, app_menu_id, parent_menu_id
			, menu_level, display_text, module_number, target_action, target_server
			)
		values
			( @session_uid, @app_menu_id, @parent_menu_id
			, @menu_level - 1, @display_text, @module_number, @target_action, @target_server
			)

		exec cbmsAppMenu_LoadChildrenToTemp
			@session_uid, @app_menu_profile_id, @user_info_id, @app_menu_id, @menu_level

		set @count = @count + 1
	end
	

	   drop table #MenuOption

	set nocount off

	   select app_menu_id
		, parent_menu_id
		, menu_level
		, display_text
		, display_image
		, active_color
		, module_number
		, target_action
		, target_server
	     from app_menu_temp
	    where session_uid = @session_uid

	set nocount on
	delete app_menu_temp where session_uid = @session_uid
	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_GetMy] TO [CBMSApplication]
GO
