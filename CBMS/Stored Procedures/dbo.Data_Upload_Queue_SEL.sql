
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******************************************************************************************************      
NAME : dbo.Data_Upload_Queue_SEL 
     
DESCRIPTION:   
Stored Procedure is used to get the data from Data Upload Audit Log.
 
 INPUT PARAMETERS:      
 Name             DataType               Default        Description      
--------------------------------------------------------------------   
@DataUploadQueueId			INT

 OUTPUT PARAMETERS:      
 Name			DataType				Default			Description      
--------------------------------------------------------------------      
RequestType		VARCHAR
RequestedBy		VARCHAR
DataPath		VARCHAR
Stauts			CHAR
  
  USAGE EXAMPLES:      
--------------------------------------------------------------------      
EXEC Data_Upload_Queue_SEL 1
    
AUTHOR INITIALS:      
 Initials			Name      
-------------------------------------------------------------------      
	RT				Romy Thomas
	RMG				Rani Mary George
     
 MODIFICATIONS       
 Initials Date  Modification      
--------------------------------------------------------------------
RT		 07/13/2011 Created
RMG      03/08/2013 Copied from DVDEHub
******************************************************************************************************/  

CREATE PROCEDURE [dbo].[Data_Upload_Queue_SEL]
      ( 
       @DataUploadQueueId INT )
AS 
BEGIN    
   
      SET NOCOUNT ON ;

      SELECT
            duq.DATA_UPLOAD_REQUEST_TYPE_ID
           ,duq.REQUESTED_USER_INFO_ID
           ,duq.CBMS_IMAGE_ID
           ,duq.STATUS_CD
           ,duq.SOURCE_DSC
           ,ui.EMAIL_ADDRESS
      FROM
            dbo.DATA_UPLOAD_QUEUE duq
            LEFT JOIN dbo.USER_INFO ui
                  ON duq.Requested_User_Info_Id = ui.USER_INFO_ID
      WHERE
            duq.DATA_UPLOAD_QUEUE_ID = @DataUploadQueueId

END

;
GO

GRANT EXECUTE ON  [dbo].[Data_Upload_Queue_SEL] TO [CBMSApplication]
GO
