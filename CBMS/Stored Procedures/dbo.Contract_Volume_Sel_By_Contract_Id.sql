SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Contract_Volume_Sel_By_Contract_Id]  
     
DESCRIPTION:

	To Get Sum of Contract Volume for the given Contract Id.

INPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
	@Contract_Id	INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	EXEC Contract_Volume_Sel_By_Contract_Id 12419
	
	EXEC Contract_Volume_Sel_By_Contract_Id 20041
	 
AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------          
	PNR			PANDARINATH
	HG			Harihara Suthan G
         
MODIFICATIONS:           
	INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
	PNR			16-JUNE-10	CREATED
	HG			7/28/2010	Removed the Contract.IS_VOLUMES_NOT_REQUIRED = 0 condition as the requirement got changed to show the actual volume irrespective the flag.
	
*/
					 
CREATE PROCEDURE dbo.Contract_Volume_Sel_By_Contract_Id
(
	 @Contract_Id	INT						
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT 
		SUM(cmv.VOLUME) Volume
	FROM 
		dbo.CONTRACT_METER_VOLUME cmv
		JOIN dbo.CONTRACT c
			 ON cmv.CONTRACT_ID = c.CONTRACT_ID
	WHERE
		c.CONTRACT_ID = @Contract_Id

END
GO
GRANT EXECUTE ON  [dbo].[Contract_Volume_Sel_By_Contract_Id] TO [CBMSApplication]
GO
