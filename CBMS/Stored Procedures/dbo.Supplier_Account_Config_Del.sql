SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
		dbo.Supplier_Account_Config_Del      
        
DESCRIPTION:        
        
        
INPUT PARAMETERS:        
 Name			DataType			Default				Description        
-------------------------------------------------------------------------        
@contract_id	INT
@Account_Id		VARCHAR(MAX) 
        
OUTPUT PARAMETERS:        
 Name			DataType			Default				Description        
-------------------------------------------------------------------------        
        
USAGE EXAMPLES:        
-------------------------------------------------------------------------  
      
EXEC dbo.Supplier_Account_Config_Del @contract_id = 11994,@Account_Id = '3994,3995,3996,3997,'

	
	Select * from supplier_account_meter_map where contract_Id = 166826

	EXEC CONTRACT_METERS_Sel_For_Invoice 166826,'1227304'

	SELECT DISTINCT TOP 100
      map.CONTRACT_ID
     ,map.meter_id
FROM
      dbo.supplier_account_meter_map AS map
      JOIN dbo.account a ON a.account_id = map.ACCOUNT_ID
      JOIN dbo.cu_invoice_service_month cism ON ci.ACCOUNT_ID = map.ACCOUNT_ID
      JOIN dbo.CU_INVOICE ci ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
ORDER BY
	CONTRACT_ID
        
AUTHOR INITIALS:        
	Initials	Name        
------------------------------------------------------------        
	NR			Narayana Reddy

MODIFICATIONS                
 Initials		Date		Modification        
------------------------------------------------------------        
  NR			2019-12-02	Add Contract - Delete last meter.					
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Config_Del]
     (
         @Contract_id INT
         , @Account_Id VARCHAR(MAX)
     )
AS
    BEGIN

        SET NOCOUNT ON;


        DELETE
        sac
        FROM
            dbo.Supplier_Account_Config sac
            INNER JOIN dbo.ufn_split(@Account_Id, ',') us
                ON CAST(us.Segments AS INT) = sac.Account_Id
        WHERE
            sac.Contract_Id = @Contract_id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                               WHERE
                                    samm.ACCOUNT_ID = sac.Account_Id
                                    AND samm.Contract_ID = sac.Contract_Id
                                    AND samm.Supplier_Account_Config_Id = sac.Supplier_Account_Config_Id);




    END;

GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Del] TO [CBMSApplication]
GO
