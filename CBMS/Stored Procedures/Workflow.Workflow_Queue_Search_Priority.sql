SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[Workflow_Queue_Search_Priority]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description 
 @Key_Word varchar(200) = null,    
 @Start_Index INT = 1,    
 @End_Index INT = 2147483647    
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  AUG-2019 Created
 TRK		  SEP-2019 Migrated to Workflow Schema.

******/
CREATE PROCEDURE [Workflow].[Workflow_Queue_Search_Priority]        
 (          
 @Key_Word varchar(200) = null,          
 @Start_Index INT = 1,          
 @End_Index INT = 2147483647          
 )          
AS          
BEGIN          
 -- SET NOCOUNT ON added to prevent extra result sets from          
 -- interfering with SELECT statements.          
 SET NOCOUNT ON;          
          
   SELECT *           
  FROM (          
  VALUES          
      (1 , 'Yes'),          
      (0, 'No')          
  ) AS C (id, name);          
          
END    
GO
GRANT EXECUTE ON  [Workflow].[Workflow_Queue_Search_Priority] TO [CBMSApplication]
GO
