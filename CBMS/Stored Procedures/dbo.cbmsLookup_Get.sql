SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  procedure [dbo].[cbmsLookup_Get]
	( @MyAccountId int
	, @entity_id int
	)
AS
BEGIN


	   select entity_id
		, entity_name
		, entity_type
		, entity_description
	     from entity
	    where entity_id = @entity_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsLookup_Get] TO [CBMSApplication]
GO
