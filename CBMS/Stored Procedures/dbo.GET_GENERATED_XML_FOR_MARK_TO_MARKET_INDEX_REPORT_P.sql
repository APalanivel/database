SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_GENERATED_XML_FOR_MARK_TO_MARKET_INDEX_REPORT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@reportDate    	varchar(12)	          	
	@clientId      	int       	          	
	@divisionId    	int       	          	
	@siteId        	varchar(1000)	          	
	@volumeId      	int       	          	
	@currencyId    	int       	          	
	@reportName    	varchar(50)	          	
	@displayStatus 	int       	          	
	@fiscalYear    	int       	          	
	@groupId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
	
	EXEC dbo.GET_GENERATED_XML_FOR_MARK_TO_MARKET_INDEX_REPORT_P -1,-1,'2005-12-29',10009,'','11096,11097',25,552,'Basis Only Marked-To-Market Report',3,2006,''

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	PNR			Pandarinath

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	PNR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on
******/

CREATE PROCEDURE dbo.GET_GENERATED_XML_FOR_MARK_TO_MARKET_INDEX_REPORT_P
	@userId			VARCHAR,
	@sessionId		VARCHAR,
	@reportDate		VARCHAR(12),
	@clientId		INT,
	@divisionId		INT,
	@siteId			VARCHAR(1000),
	@volumeId		INT,
	@currencyId		INT,
	@reportName		VARCHAR(50),
	@displayStatus	INT,
	@fiscalYear		INT,
	@groupId		INT
AS
BEGIN

	SET NOCOUNT ON ;

	DECLARE @selectClause VARCHAR(8000)
	DECLARE @fromClause	VARCHAR(8000)
	DECLARE @whereClause VARCHAR(8000)
	DECLARE @SQLStatement VARCHAR(8000)

IF @displayStatus=1
	SELECT 
		report.REPORT_XML
	FROM
		dbo.REPORT_LIST list,
		dbo.RM_REPORT report,
		dbo.RM_REPORT_BATCH_LOG batch	
	WHERE
		list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND
		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 628 AND
						INPUT_PARAMETER = @clientId AND
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 
									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND
												RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
														 FROM	RM_REPORT_FILTER_DATA filter
														 WHERE	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 


								)
						
				      )	


ELSE IF @displayStatus=2
	SELECT report.REPORT_XML
	FROM 
		dbo.REPORT_LIST list,
		dbo.RM_REPORT report,
		dbo.RM_REPORT_BATCH_LOG batch	
	WHERE	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND
		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 629 AND
						INPUT_PARAMETER = @divisionId AND
						
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
														 FROM	RM_REPORT_FILTER_DATA filter
														 WHERE	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 

								)
						
				      )	

ELSE IF @displayStatus=3
BEGIN
	SET @selectClause = ' Select report.REPORT_XML '
	SET @fromClause =  ' from REPORT_LIST list, RM_REPORT report, RM_REPORT_BATCH_LOG batch '	
	SET @whereClause =   ' where	list.REPORT_NAME = '+''''+@reportName+''''+' AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = ' + '''' + @reportDate + '''' + ' AND
		report.RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where	INPUT_PARAMETER_TYPE_ID = 630 AND INPUT_PARAMETER in ('+@siteId+') AND
		RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where INPUT_PARAMETER_TYPE_ID = 655 AND	INPUT_PARAMETER='+STR(@volumeId)+' AND 
		RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where INPUT_PARAMETER_TYPE_ID = 656 AND	INPUT_PARAMETER='+STR(@currencyId)+' AND
		RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where INPUT_PARAMETER_TYPE_ID = 814 AND	INPUT_PARAMETER='+STR(@fiscalYear)+')))) '	

	SELECT @SQLStatement = @selectClause + @fromClause + @whereClause
	PRINT @SQLSTATEMENT
	EXEC (@SQLStatement)
END

ELSE IF @displayStatus=4

	SELECT report.REPORT_XML
	
	FROM	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	WHERE	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 628 AND
						INPUT_PARAMETER = @clientId AND
						
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId 
											) 


								)
						
				      )	
				      
ELSE IF @displayStatus=5

	SELECT report.REPORT_XML
	
	FROM	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	WHERE	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 629 AND
						INPUT_PARAMETER = @divisionId AND
						
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId 
											) 



								)
						
				      )	
				      
ELSE IF @displayStatus=6
BEGIN
SET @selectClause = ' Select report.REPORT_XML '
SET @fromClause =  ' from REPORT_LIST list, RM_REPORT report, RM_REPORT_BATCH_LOG batch '	
SET @whereClause =   ' where	list.REPORT_NAME = '+''''+@reportName+''''+' AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(Varchar(12),batch.LOG_TIME, 101) = '+ '''' + @reportDate +  '''' +' AND
		report.RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where	INPUT_PARAMETER_TYPE_ID = 630 AND INPUT_PARAMETER in ('+@siteId+') AND
		RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where INPUT_PARAMETER_TYPE_ID = 655 AND	INPUT_PARAMETER='+STR(@volumeId)+' AND 
		RM_REPORT_ID IN( select	RM_REPORT_ID from RM_REPORT_FILTER_DATA filter
		where INPUT_PARAMETER_TYPE_ID = 656 AND	INPUT_PARAMETER='+STR(@currencyId)+'))) '	

SELECT @SQLStatement = @selectClause + @fromClause + @whereClause


EXEC (@SQLStatement)

END

ELSE IF @displayStatus=7

	SELECT report.REPORT_XML
	
	FROM	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	WHERE	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID = report.REPORT_LIST_ID AND
		report.RM_REPORT_ID = batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 841 AND
						INPUT_PARAMETER = @groupId AND
						
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER=@volumeId AND 

									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER = @currencyId 
											) 

								)
						
				      )	
ELSE IF @displayStatus=8

	SELECT report.REPORT_XML
	
	FROM	REPORT_LIST list,
		RM_REPORT report,
		RM_REPORT_BATCH_LOG batch	
	
	WHERE	list.REPORT_NAME = @reportName AND
		list.REPORT_LIST_ID=report.REPORT_LIST_ID AND
		report.RM_REPORT_ID=batch.RM_REPORT_ID AND
		CONVERT(VARCHAR(12),batch.LOG_TIME, 101) = @reportDate AND

		report.RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
					FROM	RM_REPORT_FILTER_DATA filter
					WHERE	INPUT_PARAMETER_TYPE_ID = 841 AND
						INPUT_PARAMETER = @groupId AND
						
						RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
								 FROM	RM_REPORT_FILTER_DATA filter
								 WHERE	INPUT_PARAMETER_TYPE_ID = 655 AND
									INPUT_PARAMETER = @volumeId AND 

									RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
											 FROM	RM_REPORT_FILTER_DATA filter
											 WHERE	INPUT_PARAMETER_TYPE_ID = 656 AND
												INPUT_PARAMETER=@currencyId AND

												RM_REPORT_ID IN( SELECT	RM_REPORT_ID 
														 FROM	RM_REPORT_FILTER_DATA filter
														 WHERE	INPUT_PARAMETER_TYPE_ID = 814 AND
															INPUT_PARAMETER=@fiscalYear
														)

											) 

								)
						
				      )
END				      
GO
GRANT EXECUTE ON  [dbo].[GET_GENERATED_XML_FOR_MARK_TO_MARKET_INDEX_REPORT_P] TO [CBMSApplication]
GO
