SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********
NAME:  dbo.TOU_Schedule_Ins

DESCRIPTION:  
	Insert a new schedule

INPUT PARAMETERS:
Name				DataType        Default     Description
-------------------------------------------------------------------
@Schedule_Name		NVARCHAR(100)
@Commodity_Id		INT
@Vendor_Id			INT
@Comment_Text		NVARCHAR(max)	NULL
@Comment_User_Id	INT				NULL
				

OUTPUT PARAMETERS:
      Name								DataType          Default     Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

	DECLARE @oTOU_Schedule_Id INT
	Exec dbo.TOU_Schedule_Ins 'Two Seasons - Three Peak', 290, 530, 'Applicable to single- and three-phase general service including lighting and power customers whose monthly Maximum Demand registers, or in the opinion of SCE is expected to register, above 
20 kW', 49, @oTOU_Schedule_Id = @oTOU_Schedule_Id OUT


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar


	Initials Date		Modification
------------------------------------------------------------
	CPE		2012-07-11  Created

******/
CREATE PROCEDURE dbo.TOU_Schedule_Ins
( 
 @Schedule_Name NVARCHAR(100)
,@Commodity_Id INT
,@Vendor_Id INT
,@Comment_Text NVARCHAR(max) = NULL
,@Comment_User_Id INT = NULL
,@oTOU_Schedule_Id INT OUTPUT )
AS 
BEGIN
      SET NOCOUNT ON
      
      DECLARE
            @Comment_Type_Cd INT
           ,@Comment_Id INT
           ,@Comment_Dt DATETIME
    
            
      BEGIN TRY
            BEGIN TRAN

            IF @Comment_Text IS NOT NULL 
                  BEGIN
                        SELECT
                              @Comment_Type_Cd = CD.Code_Id
                        FROM
                              dbo.Code CD
                              INNER JOIN dbo.Codeset CS
                                    ON CD.Codeset_Id = CS.Codeset_Id
                        WHERE
                              CD.Code_Value = 'TOUSComment'
                              AND CS.Codeset_Name = 'CommentType'

                        SELECT
                              @Comment_Dt = GETDATE()
                              
                        EXECUTE dbo.Comment_Ins @Comment_Text = @Comment_Text, @Comment_User_Info_Id = @Comment_User_Id, @Comment_Type_Cd = @Comment_Type_Cd, @Comment_Dt = @Comment_Dt, @Comment_Id = @Comment_Id OUTPUT
                  END
            INSERT
                  dbo.Time_Of_Use_Schedule
                  ( 
                   Schedule_Name
                  ,Commodity_Id
                  ,VENDOR_ID
                  ,Comment_ID )
                  SELECT
                        @Schedule_Name
                       ,@Commodity_Id
                       ,@Vendor_Id
                       ,@Comment_Id
			
            SELECT
                  @oTOU_Schedule_Id = SCOPE_IDENTITY()

            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN
            EXEC usp_RethrowError
      END CATCH

END 


;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Ins] TO [CBMSApplication]
GO
