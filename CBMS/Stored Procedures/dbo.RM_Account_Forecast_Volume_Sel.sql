SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**
NAME:    
   
	dbo.RM_Account_Forecast_Volume_Sel
   
 DESCRIPTION:     
   
	This procedure is to get client onboarded details
   
 INPUT PARAMETERS:    
	Name				DataType		 Default		 Description    
----------------------------------------------------------------------      
	@Client_Id			INT
    @Commodity_Id		INT
    @Country_Id			VARCHAR(MAX)	NULL
    @Start_Dt			DATE
    @End_Dt				DATE			NULL
    @Site_Not_Managed	INT				NULL
    @Sitegroup_Id		INT				NULL
    @Site_Id			VARCHAR(MAX)	NULL
   
 OUTPUT PARAMETERS:    
 Name				DataType		 Default		 Description    
----------------------------------------------------------------------

  
  USAGE EXAMPLES:    
------------------------------------------------------------  

  
	EXEC RM_Account_Forecast_Volume_Sel 235,291,NULL,'2016-01-01','2016-12-01',NULL,NULL,NULL,NULL,1,25
	EXEC RM_Account_Forecast_Volume_Sel 11236,291,NULL,'2018-01-01','2018-12-01',NULL,NULL,NULL  
	EXEC RM_Account_Forecast_Volume_Sel 11307,291,NULL,'2019-01-01','2019-12-01',NULL,NULL,NULL
	EXEC RM_Account_Forecast_Volume_Sel 10003,291,NULL,'2019-01-01','2019-12-01',NULL,NULL,NULL,241

	
  
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy
    
 MODIFICATIONS     
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2018-09-06	Created For Global Risk Managemnet		


******/

CREATE PROCEDURE [dbo].[RM_Account_Forecast_Volume_Sel]
     (
         @Client_Id INT
         , @Commodity_Id INT
         , @Country_Id VARCHAR(MAX) = NULL
         , @Start_Dt DATE
         , @End_Dt DATE = NULL
         , @Site_Not_Managed INT = NULL
         , @Sitegroup_Id INT = NULL
         , @Site_Id VARCHAR(MAX) = NULL
         , @RM_Group_Id INT = NULL
         , @Start_Index INT = 1
         , @End_Index INT = 2147483647
     )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Total_Cnt INT;

        CREATE TABLE #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id INT
                 , Forecast_Uom INT
                 , HedgeType VARCHAR(200)
                 , Service_Month DATE
             );

        DECLARE @Tbl_RM_Group_Sites AS TABLE
              (
                  Site_Id INT
                  , Client_Id INT
                  , Sitegroup_Id INT
                  , Site_Name VARCHAR(200)
              );

        INSERT INTO @Tbl_RM_Group_Sites
             (
                 Site_Id
                 , Client_Id
                 , Sitegroup_Id
                 , Site_Name
             )
        EXEC dbo.Cbms_Sitegroup_Sites_Sel_By_Cbms_Sitegroup_Id
            @Cbms_Sitegroup_Id = @RM_Group_Id;

        INSERT INTO #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id
                 , Forecast_Uom
                 , HedgeType
                 , Service_Month
             )
        SELECT
            ch.Client_Hier_Id
            , MAX(chob.RM_Forecast_UOM_Type_Id)
            , MIN(ht.ENTITY_NAME)
            , dd.DATE_D
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Client_Id = @Client_Id
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') con
                                   WHERE
                                        ch.Country_Id = con.Segments))
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @Sitegroup_Id IS NULL
                    OR  ch.Sitegroup_Id = @Sitegroup_Id)
            AND (   @Site_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  @Tbl_RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D;

        INSERT INTO #Tbl_Site_HedgeType_Uom
             (
                 Client_Hier_Id
                 , Forecast_Uom
                 , HedgeType
                 , Service_Month
             )
        SELECT
            ch.Client_Hier_Id
            , MAX(chob.RM_Forecast_UOM_Type_Id)
            , MIN(ht.ENTITY_NAME)
            , dd.DATE_D
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chcl
                ON ch.Client_Id = chcl.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chcl.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            INNER JOIN dbo.ENTITY ht
                ON chhc.Hedge_Type_Id = ht.ENTITY_ID
            CROSS JOIN meta.DATE_DIM dd
        WHERE
            ch.Client_Id = @Client_Id
            AND ht.ENTITY_NAME <> 'Does Not Hedge'
            AND chcl.Sitegroup_Id = 0
            AND chob.Commodity_Id = @Commodity_Id
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') con
                                   WHERE
                                        ch.Country_Id = con.Segments))
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @Sitegroup_Id IS NULL
                    OR  ch.Sitegroup_Id = @Sitegroup_Id)
            AND (   @Site_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
            AND dd.DATE_D BETWEEN chhc.Config_Start_Dt
                          AND     chhc.Config_End_Dt
            AND dd.DATE_D BETWEEN @Start_Dt
                          AND     @End_Dt
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteonb
                               WHERE
                                    siteonb.Client_Hier_Id = ch.Client_Hier_Id
                                    AND siteonb.Commodity_Id = chob.Commodity_Id)
            AND (   @RM_Group_Id IS NULL
                    OR  EXISTS (SELECT  1 FROM  @Tbl_RM_Group_Sites rgs WHERE   rgs.Site_Id = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id
            , dd.DATE_D;



        SELECT
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')' AS Site_name
            , cha.Account_Id
            , cha.Account_Number
            , shu.Service_Month AS Service_Month
            , CAST(cuc.CONVERSION_FACTOR * vol.Forecast_Volume AS DECIMAL(28, 0)) AS Forecast_Volume
            , ent.ENTITY_NAME AS UOM
            , cd.Code_Value AS Legend
            , shu.HedgeType
            , ISNULL(shu.Forecast_Uom, vol.Uom_Id) AS Uom_Id
            , DENSE_RANK() OVER (ORDER BY
                                     cha.Account_Number) AS Row_Num
            , ch.Sitegroup_Id
        INTO
            #Account_Dtls
        FROM
            Core.Client_Hier ch
            INNER JOIN #Tbl_Site_HedgeType_Uom shu
                ON ch.Client_Hier_Id = shu.Client_Hier_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON ch.Client_Hier_Id = cha.Client_Hier_Id
            LEFT JOIN Trade.RM_Account_Forecast_Volume vol
                ON vol.Client_Hier_Id = shu.Client_Hier_Id
                   AND  vol.Commodity_Id = cha.Commodity_Id
                   AND  vol.Account_Id = cha.Account_Id
                   AND  vol.Service_Month = shu.Service_Month
            LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = vol.Uom_Id
                   AND  cuc.CONVERTED_UNIT_ID = ISNULL(shu.Forecast_Uom, vol.Uom_Id)
            LEFT JOIN dbo.ENTITY ent
                ON ent.ENTITY_ID = ISNULL(shu.Forecast_Uom, vol.Uom_Id)
            LEFT JOIN dbo.Code cd
                ON vol.Volume_Source_Cd = cd.Code_Id
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Site_Id > 0
            AND cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Type = 'Utility'
            AND (   @Country_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Country_Id, ',') con
                                   WHERE
                                        ch.Country_Id = con.Segments))
            AND (   @Site_Not_Managed IS NULL
                    OR  ch.Site_Not_Managed = @Site_Not_Managed)
            AND (   @Sitegroup_Id IS NULL
                    OR  ch.Sitegroup_Id = @Sitegroup_Id)
            AND (   @Site_Id IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.ufn_split(@Site_Id, ',')
                                   WHERE
                                        CAST(Segments AS INT) = ch.Site_Id))
        GROUP BY
            ch.Client_Hier_Id
            , ch.Site_Id
            , RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')'
            , cha.Account_Id
            , cha.Account_Number
            , shu.Service_Month
            , CAST(cuc.CONVERSION_FACTOR * vol.Forecast_Volume AS DECIMAL(28, 0))
            , ent.ENTITY_NAME
            , cd.Code_Value
            , shu.HedgeType
            , ISNULL(shu.Forecast_Uom, vol.Uom_Id)
            , ch.Sitegroup_Id;



        SELECT  @Total_Cnt = MAX(sd.Row_Num)FROM    #Account_Dtls sd;



        SELECT
            cs.Client_Hier_Id
            , cs.Site_Id
            , cs.Site_name
            , cs.Account_Id
            , cs.Account_Number
            , cs.Service_Month
            , cs.Forecast_Volume
            , cs.UOM
            , cs.Legend
            , cs.HedgeType
            , cs.Uom_Id
            , @Total_Cnt AS Total_Cnt
            , cs.Row_Num
            , @Client_Id AS Client_Id
            , cs.Sitegroup_Id
        FROM
            #Account_Dtls cs
        WHERE
            cs.Row_Num BETWEEN @Start_Index
                       AND     @End_Index
        ORDER BY
            cs.Row_Num;

        DROP TABLE #Account_Dtls;
        DROP TABLE #Tbl_Site_HedgeType_Uom;

    END;
GO
GRANT EXECUTE ON  [dbo].[RM_Account_Forecast_Volume_Sel] TO [CBMSApplication]
GO
