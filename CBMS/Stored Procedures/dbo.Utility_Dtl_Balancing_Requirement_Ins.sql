SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Dtl_Balancing_Requirement_Ins

DESCRIPTION:


INPUT PARAMETERS:
	Name							DataType	Default	Description
------------------------------------------------------------
	 @Commodity_Id					INT
	 @Vendor_Id						INT
     @Question_Commodity_Map_Id		INT
     @Industrial_Flag_Cd			INT
     @Commercial_Flag_Cd			INT
     @Comment_ID					INT			NULL
     @Comment_ID					INT			NULL

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		EXEC Utility_Dtl_Balancing_Requirement_Ins 73,17855,100306,100307
	ROLLBACK TRAN
	
	SELECT * FROM Vendor_Commodity_Map WHERE Vendor_id = 370
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	HG			Harihara Suthan G

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
	HG			03/11/2011	Utility_Summary_Question_Id and Utility_Detail_Id column replaced by Question_Commodity_Map_Id and Vendor_Commodity_Map_Id column as this utility balancing requirement section becomes specific to commodity(Natural Gas)
******/

CREATE PROC dbo.Utility_Dtl_Balancing_Requirement_Ins
( 
@Commodity_Id			INT
,@Vendor_Id				INT
,@Question_Commodity_Map_Id INT
,@Industrial_Flag_Cd	INT
,@Commercial_Flag_Cd	INT
,@Comment_ID			INT = NULL )
AS
BEGIN
 
      SET NOCOUNT ON ;

      INSERT INTO
            dbo.Utility_Dtl_Balancing_Requirement
            ( 
             Question_Commodity_Map_Id
            ,Vendor_Commodity_Map_Id
            ,Industrial_Flag_Cd
            ,Commercial_Flag_Cd
            ,Comment_ID )
      SELECT
              @Question_Commodity_Map_Id
             ,vcm.Vendor_Commodity_Map_Id
             ,@Industrial_Flag_Cd
             ,@Commercial_Flag_Cd
             ,@Comment_ID
	 FROM
			dbo.Vendor_Commodity_Map vcm
	 WHERE
			Vendor_Id = @Vendor_Id
			AND Commodity_Type_Id = @Commodity_Id

END
GO
GRANT EXECUTE ON  [dbo].[Utility_Dtl_Balancing_Requirement_Ins] TO [CBMSApplication]
GO
