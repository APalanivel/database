SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
  dbo.Invoice_Data_Quality_Test_Comment_Sel_By_Cu_Invoice_Id  
    
DESCRIPTION:    
    To get the invoice data quality test comments based on Invoice.
    
INPUT PARAMETERS:    
  Name						DataType				Default		Description    
-------------------------------------------------------------------------------------    
  @Cu_Invoice_Id			INT


OUTPUT PARAMETERS:    
  Name						DataType				Default		Description    
-------------------------------------------------------------------------------------    
    
USAGE EXAMPLES:    
-------------------------------------------------------------------------------------  


EXEC dbo.Invoice_Data_Quality_Test_Comment_Sel_By_Cu_Invoice_Id
    @Cu_Invoice_Id = 10
  

    
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------------------------    
 NR			Narayana Reddy   
     
MODIFICATIONS    
    
 Initials	Date		Modification    
-------------------------------------------------------------------------------------    
 NR			2019-04-24  Data2.0 -  Created for Data Quality.   

******/
CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Test_Comment_Sel_By_Cu_Invoice_Id]
     (
         @Cu_Invoice_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @Comment_Type_Cd INT;

        SELECT
            @Comment_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'CommentType'
            AND c.Code_Value = 'Data Quality Test Comment';

        SELECT
            idqtc.Invoice_Data_Quality_Test_Comment_Id
            , idqtc.Cu_Invoice_Id
            , idqtc.Comment_Id
            , c.Comment_Text
			,c.Comment_Dt
			,c.Comment_User_Info_Id
			,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Commented_User_Name
        FROM
            dbo.Invoice_Data_Quality_Test_Comment idqtc
            INNER JOIN dbo.Comment c
                ON c.Comment_ID = idqtc.Comment_Id
				INNER JOIN dbo.USER_INFO ui
				ON ui.USER_INFO_ID = c.Comment_User_Info_Id
        WHERE
            idqtc.Cu_Invoice_Id = @Cu_Invoice_Id
            AND c.Comment_Type_CD = @Comment_Type_Cd;


    END;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Test_Comment_Sel_By_Cu_Invoice_Id] TO [CBMSApplication]
GO
