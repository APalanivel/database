SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Account_Commodity_Invoice_Recalc_Type_Upd

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default	Description
---------------------------------------------------------------
	@Account_Commodity_Invoice_Recalc_Type_Id	INT
    @Start_Dt									DATE
    @End_Dt										DATE
    @Invoice_Recalc_Type_Cd						INT
    @User_Info_Id								INT

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------
    DECLARE  @Account_Commodity_Invoice_Recalc_Type_Id INT 
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520
	BEGIN TRANSACTION
	  EXEC dbo.Account_Commodity_Invoice_Recalc_Type_Ins 
      @Account_Id =1148520
     ,@Commodity_Id = 290
     ,@Start_Dt = '2015-01-01'
     ,@End_Dt= NULL
     ,@Invoice_Recalc_Type_Cd = 102030
     ,@User_Info_Id = 49
     
    SET @Account_Commodity_Invoice_Recalc_Type_Id = @@identity 
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520	
	  EXEC Account_Commodity_Invoice_Recalc_Type_Del @Account_Commodity_Invoice_Recalc_Type_Id
	SELECT * FROM Account_Commodity_Invoice_Recalc_Type WHERE Account_Id=1148520  		
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV			Ravi Kumar Vegesna
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RKV       	2015-08-10	Created for AS400-II

******/

CREATE PROCEDURE [dbo].[Account_Commodity_Invoice_Recalc_Type_Del]
      ( 
       @Account_Commodity_Invoice_Recalc_Type_Id INT
       ,@User_Info_Id INT )
AS 
BEGIN
      SET NOCOUNT ON 
      
      DECLARE @Account_Id INT 
           
      
      SELECT
            @Account_Id = Account_Id
      FROM
            Account_Commodity_Invoice_Recalc_Type
      WHERE
            Account_Commodity_Invoice_Recalc_Type_Id = @Account_Commodity_Invoice_Recalc_Type_Id
      
      
       EXEC dbo.ADD_ENTITY_AUDIT_ITEM_P 
            @entity_identifier = @Account_Id
           ,@user_info_id = @User_Info_Id
           ,@audit_type = 3
           ,@entity_name = 'ACCOUNT_TABLE'
           ,@entity_type = 500 
     
    
      
      DELETE
            acirt
      FROM
            dbo.Account_Commodity_Invoice_Recalc_Type acirt
      WHERE
            acirt.Account_Commodity_Invoice_Recalc_Type_Id = @Account_Commodity_Invoice_Recalc_Type_Id
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Invoice_Recalc_Type_Del] TO [CBMSApplication]
GO
