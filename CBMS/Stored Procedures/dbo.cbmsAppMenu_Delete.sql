SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.[cbmsAppMenu_Delete]


DESCRIPTION: 


INPUT PARAMETERS:    
      Name                  DataType          Default     Description    
------------------------------------------------------------------    
	@MyAccountId int
	, @menu_id int
	
        
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier
	  

******/
CREATE procedure [dbo].[cbmsAppMenu_Delete]
	( 
	@MyAccountId int
	, @menu_id int
	)
AS

BEGIN

	SET NOCOUNT ON

DELETE 	FROM 
		APP_MENU 
WHERE 
		APP_MENU_ID = @menu_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsAppMenu_Delete] TO [CBMSApplication]
GO
