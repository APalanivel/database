SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
Name:    
 CBMS.dbo.Cost_Usage_Site_Summary_For_Data_Compare  
   
 Description:    
   
 Input Parameters:    
    Name       DataType      Default Description    
------------------------------------------------------------------------    
    @Commodity_Id     INT  
    @Report_Year     SMALLINT  
    @UOM_Type_Id     INT  
    @Currency_Unit_Id   INT  
    @Client_Hier_Id     INT  
   
 Output Parameters:    
 Name  Datatype  Default Description    
------------------------------------------------------------    
   
 Usage Examples:  
------------------------------------------------------------    
    EXEC dbo.Cost_Usage_Site_Summary_For_Data_Compare 17654, 290, 2011, 12, 3  
      
    EXEC dbo.Cost_Usage_Site_Summary_For_Data_Compare 12592, 290, 2011, 12, 3  
  
    EXEC dbo.Cost_Usage_Site_Summary_For_Data_Compare 5459, 290, 2010, 12, 3  
   
Author Initials:    
 Initials Name  
------------------------------------------------------------  
 AP   Athmaram Pabbathi  
 RR   Raghu Reddy  
   
 Modifications :    
 Initials	  Date	    Modification    
------------------------------------------------------------    
 AP		  09/29/2011  Created   
 AP		  10/11/2011  Added Start_Month column in the result set  
 AP		  11/16/2011  Added Bucket_Type_Cd in the result set  
 AP		  11/27/2011  Added UOM_Name in the result set  
 AP		  12/01/2011  If the Conversion Factor is not available for Determinants then passing the actual UOM Type Id's entered in the cost usage table.  
 AP		  2011-12-15  Modified the SP to use Calendar Year instead of Fiscal Year  
 RR		  2012-03-30  Replaced the input parameter @Site_Id with @Client_Hier_Id 
 ******/
CREATE PROCEDURE dbo.Cost_Usage_Site_Summary_For_Data_Compare
      ( 
       @Client_Hier_Id INT
      ,@Commodity_Id INT
      ,@Report_Year SMALLINT
      ,@UOM_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN  
  
      SET NOCOUNT ON  
  
      DECLARE
            @Begin_Date DATETIME
           ,@End_Date DATETIME
           ,@Calendar_Year_Start_Month DATE
           ,@Currency_Group_Id INT
           ,@KW_Uom_Type_Id INT
           ,@Site_Id INT
           ,@Commodity_Name VARCHAR(50)
  
      DECLARE @Bucket_List TABLE
            ( 
             Bucket_Master_Id INT PRIMARY KEY CLUSTERED
            ,Bucket_Name VARCHAR(255)
            ,Uom_Type_Id INT
            ,Bucket_Type_Cd INT )  
  
      DECLARE @CU_Reaggregated_Data TABLE
            ( 
             Bucket_Master_ID INT
            ,Bucket_Name VARCHAR(200)
            ,Service_Month DATE
            ,Month_Number INT
            ,Uom_Type_Id INT
            ,Bucket_Value DECIMAL(28, 10) )  
            
      DECLARE @CU_Old_Data TABLE
            ( 
             Bucket_Name VARCHAR(200)
            ,Service_Month DATE
            ,Month_Number INT
            ,Uom_Type_Id INT
            ,Bucket_Value DECIMAL(28, 10) );  
  
      SELECT
            @KW_Uom_Type_Id = uom.Entity_Id
      FROM
            dbo.ENTITY uom
      WHERE
            uom.Entity_Name = 'KW'
            AND uom.ENTITY_DESCRIPTION = 'Unit for electricity'  
              
  
      SELECT
            @Currency_Group_id = CH.Client_Currency_Group_Id
           ,@Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier CH
      WHERE
            CH.Client_Hier_Id = @Client_Hier_Id  
  
      SELECT
            @Commodity_Name = com.Commodity_Name
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Id = @Commodity_Id
              
  
      SET @Calendar_Year_Start_Month = convert(DATE, '1/1/' + convert(VARCHAR(4), @Report_Year))  
  
      INSERT      INTO @Bucket_List
                  ( 
                   Bucket_Master_Id
                  ,Bucket_Name
                  ,Uom_Type_Id
                  ,Bucket_Type_Cd )
                  SELECT
                        BM.Bucket_Master_Id
                       ,BM.Bucket_Name
                       ,case WHEN Bm.Bucket_Name IN ( 'Demand', 'Billed Demand', 'Contract Demand', 'Reactive Demand' ) THEN Bm.Default_UOM_Type_Id
                             ELSE @UOM_Type_Id
                        END
                       ,BM.Bucket_Type_Cd
                  FROM
                        CBMS.dbo.Bucket_Master BM
                  WHERE
                        BM.Commodity_Id = @Commodity_Id
                        AND BM.Is_Shown_on_Site = 1
                        AND BM.Is_Active = 1;  
  
      INSERT      INTO @CU_Reaggregated_Data
                  ( 
                   Bucket_Master_ID
                  ,Bucket_Name
                  ,Service_Month
                  ,Month_Number
                  ,Uom_Type_Id
                  ,Bucket_Value )
                  SELECT
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,SM.Date_D
                       ,SM.Month_Num
                       ,( case WHEN UOMConv.Conversion_Factor = 0
                                    AND CD.Code_Value = 'Determinant' THEN CUSD.UOM_Type_Id
                               WHEN UOMConv.Conversion_Factor != 0
                                    AND CD.Code_Value = 'Determinant' THEN BL.UOM_Type_Id
                          END ) AS UOM_Type_Id
                       ,sum(case WHEN CD.Code_Value = 'Charge' THEN CUSD.Bucket_Value * CurConv.Conversion_Factor
                                 WHEN CD.Code_Value = 'Determinant'
                                      AND UOMConv.Conversion_Factor != 0 THEN CUSD.Bucket_Value * UOMConv.Conversion_Factor
                                 WHEN CD.Code_Value = 'Determinant'
                                      AND UOMConv.Conversion_Factor = 0 THEN CUSD.Bucket_Value
                                 ELSE 0
                            END) Bucket_Value
                  FROM
                        dbo.Cost_Usage_Site_Dtl CUSD
                        INNER JOIN @Bucket_List BL
                              ON BL.Bucket_Master_Id = CUSD.Bucket_Master_Id
                        INNER JOIN dbo.Code CD
                              ON CD.Code_Id = BL.Bucket_Type_Cd
                        INNER JOIN meta.Date_Dim SM
                              ON SM.Date_D = CUSD.Service_Month
                        LEFT OUTER JOIN dbo.Currency_Unit_Conversion CurConv
                              ON CurConv.Base_Unit_Id = CUSD.Currency_Unit_Id
                                 AND CurConv.Conversion_Date = SM.DATE_D
                                 AND CurConv.Currency_Group_Id = @Currency_Group_Id
                                 AND CurConv.Converted_Unit_Id = @Currency_Unit_Id
                        LEFT OUTER JOIN CBMS.dbo.Consumption_Unit_Conversion UOMConv
                              ON UOMConv.Base_Unit_Id = CUSD.UOM_Type_Id
                                 AND UOMConv.Converted_Unit_Id = bl.Uom_Type_Id
                  WHERE
                        CUSD.Client_Hier_Id = @Client_Hier_Id
                        AND sm.Year_Num = @Report_Year
                  GROUP BY
                        BL.Bucket_Master_Id
                       ,BL.Bucket_Name
                       ,SM.Date_D
                       ,SM.Month_Num
                       ,( case WHEN UOMConv.Conversion_Factor = 0
                                    AND CD.Code_Value = 'Determinant' THEN CUSD.UOM_Type_Id
                               WHEN UOMConv.Conversion_Factor != 0
                                    AND CD.Code_Value = 'Determinant' THEN BL.UOM_Type_Id
                          END )  
  
  
  -- EP  
      INSERT      INTO @CU_Old_Data
                  ( 
                   Bucket_Name
                  ,Service_Month
                  ,Month_Number
                  ,Uom_Type_Id
                  ,Bucket_Value )
                  SELECT
                    unpvt.Bucket_Name
                       ,unpvt.Service_Month
                       ,datepart(mm, unpvt.Service_Month)
                       ,case WHEN unpvt.Bucket_Name = 'Demand' THEN @KW_Uom_Type_Id
                             WHEN unpvt.Bucket_Name = 'Total Cost'
                                  OR unpvt.Bucket_Name = 'Marketer Cost'
                                  OR unpvt.Bucket_Name = 'Utility Cost'
                                  OR unpvt.Bucket_Name = 'Taxes' THEN unpvt.CURRENCY_UNIT_ID
                             ELSE unpvt.EL_UNIT_OF_MEASURE_TYPE_ID
                        END AS Uom_Type_Id
                       ,unpvt.Bucket_Value
                  FROM
                        ( SELECT
                              cu.Service_Month
                             ,cu.EL_USAGE AS [Total Usage]
                             ,cu.EL_COST AS [Total Cost]
                             ,cu.EL_ON_PEAK_USAGE AS [On-Peak USAGE]
                             ,cu.EL_OFF_PEAK_USAGE AS [Off-Peak Usage]
                             ,cu.EL_INT_PEAK_USAGE AS [Other-Peak USAGE]
                             ,cu.EL_DEMAND AS [Demand]
                             ,cu.EL_MARKETER_COST AS [Marketer Cost]
                             ,cu.EL_UTILITY_COST AS [Utility Cost]
                             ,cu.EL_TAX AS [Taxes]
                             ,cu.EL_UNIT_OF_MEASURE_TYPE_ID
                             ,cu.CURRENCY_UNIT_ID
                          FROM
                              CBMS.dbo.Cost_Usage_Site cu
                              INNER JOIN meta.Date_Dim SM
                                    ON SM.Date_D = cu.Service_Month
                          WHERE
                              cu.Site_Id = @Site_Id
                              AND sm.Year_Num = @Report_Year
                              AND @Commodity_Name = 'Electric Power' ) p UNPIVOT ( Bucket_Value FOR Bucket_Name IN ( [Total Usage], [Total Cost], [On-Peak USAGE], [Off-Peak Usage], [Other-Peak USAGE], [Demand], [Marketer Cost], [Utility Cost], [Taxes] ) )
 as unpvt  
  
       
  
  -- NG  
      INSERT      INTO @CU_Old_Data
                  ( 
                   Bucket_Name
                  ,Service_Month
                  ,Month_Number
                  ,Uom_Type_Id
                  ,Bucket_Value )
                  SELECT
                        unpvt.Bucket_Name
                       ,unpvt.Service_Month
                       ,datepart(mm, unpvt.Service_Month)
                       ,case WHEN unpvt.Bucket_Name = 'Total Cost'
                                  OR unpvt.Bucket_Name = 'Marketer Cost'
                                  OR unpvt.Bucket_Name = 'Utility Cost'
                                  OR unpvt.Bucket_Name = 'Taxes' THEN unpvt.CURRENCY_UNIT_ID
                             ELSE unpvt.NG_UNIT_OF_MEASURE_TYPE_ID
                        END AS Uom_Type_Id
                       ,unpvt.Bucket_Value
                  FROM
                        ( SELECT
                              cu.Service_Month
                             ,cu.NG_USAGE AS [Total Usage]
                             ,cu.NG_COST AS [Total Cost]
                             ,cu.NG_MARKETER_COST AS [Marketer Cost]
                             ,cu.NG_UTILITY_COST AS [Utility Cost]
                             ,cu.NG_TAX AS [Taxes]
                             ,cu.NG_UNIT_OF_MEASURE_TYPE_ID
                             ,cu.CURRENCY_UNIT_ID
                          FROM
                              CBMS.dbo.Cost_Usage_Site cu
                              INNER JOIN meta.Date_Dim SM
                                    ON SM.Date_D = cu.Service_Month
                          WHERE
                              cu.Site_Id = @Site_Id
                              AND sm.Year_Num = @Report_Year
                              AND @Commodity_Name = 'Natural Gas' ) p UNPIVOT ( Bucket_Value FOR Bucket_Name IN ( [Total Usage], [Total Cost], [Marketer Cost], [Utility Cost], [Taxes] ) ) as unpvt  
  
  
      SELECT
            BL.Bucket_Name
           ,BL.Bucket_Master_Id
           ,BL.Bucket_Type_Cd
           ,( case WHEN CD.Code_Value = 'Charge' THEN CU.Currency_Unit_Name
                   WHEN CD.Code_Value = 'Determinant' THEN UOM.Entity_Name
              END ) AS UOM_Name
           ,max(case WHEN CUSD.Month_Number = 1 THEN CUSD.Bucket_Value
                END) AS Month1
           ,max(case WHEN CUSD.Month_Number = 2 THEN CUSD.Bucket_Value
                END) AS Month2
           ,max(case WHEN CUSD.Month_Number = 3 THEN CUSD.Bucket_Value
                END) AS Month3
           ,max(case WHEN CUSD.Month_Number = 4 THEN CUSD.Bucket_Value
                END) AS Month4
           ,max(case WHEN CUSD.Month_Number = 5 THEN CUSD.Bucket_Value
                END) AS Month5
           ,max(case WHEN CUSD.Month_Number = 6 THEN CUSD.Bucket_Value
                END) AS Month6
           ,max(case WHEN CUSD.Month_Number = 7 THEN CUSD.Bucket_Value
                END) AS Month7
           ,max(case WHEN CUSD.Month_Number = 8 THEN CUSD.Bucket_Value
                END) AS Month8
           ,max(case WHEN CUSD.Month_Number = 9 THEN CUSD.Bucket_Value
                END) AS Month9
           ,max(case WHEN CUSD.Month_Number = 10 THEN CUSD.Bucket_Value
                END) AS Month10
           ,max(case WHEN CUSD.Month_Number = 11 THEN CUSD.Bucket_Value
                END) AS Month11
           ,max(case WHEN CUSD.Month_Number = 12 THEN CUSD.Bucket_Value
                END) AS Month12
           ,sum(CUSD.Bucket_Value) AS Total
           ,@Calendar_Year_Start_Month AS Start_Month
           ,max(case WHEN CUSD.Month_Number = 1
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month1_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 2
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month2_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 3
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month3_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 4
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month4_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 5
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month5_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 6
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month6_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 7
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month7_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 8
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month8_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 9
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month9_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 10
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
        END) AS Month10_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 11
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month11_Is_Matched
           ,max(case WHEN CUSD.Month_Number = 12
                          AND abs(isnull(cusd.Bucket_Value, 0) - isnull(ob.Bucket_Value, 0)) <= 1 THEN 1
                     ELSE 0
                END) AS Month12_Is_Matched
      FROM
            @Bucket_List BL
            INNER JOIN CBMS.dbo.Code CD
                  ON CD.Code_Id = BL.Bucket_Type_Cd
            LEFT OUTER JOIN @CU_Reaggregated_Data CUSD
                  ON CUSD.Bucket_Master_Id = BL.Bucket_Master_Id
            LEFT OUTER JOIN CBMS.dbo.Currency_Unit CU
                  ON CU.Currency_Unit_Id = @Currency_Unit_Id
                     AND CD.Code_Value = 'Charge'
            LEFT OUTER JOIN CBMS.dbo.Entity UOM
                  ON UOM.Entity_Id = CUSD.UOM_Type_Id
                     AND CD.Code_Value = 'Determinant'
            OUTER APPLY ( SELECT
                              case WHEN cd.COde_Value = 'Determinant' THEN cu.Bucket_Value * cuc.CONVERSION_FACTOR
                                   WHEN cd.COde_Value = 'Charge' THEN cu.Bucket_Value * cc.Conversion_Factor
                              END
                          FROM
                              @Cu_Old_Data cu
                              LEFT JOIN CBMS.dbo.Consumption_Unit_Conversion cuc
                                    ON cuc.Base_Unit_Id = cu.Uom_Type_Id
                                       AND cuc.CONVERTED_UNIT_ID = cusd.Uom_Type_Id
                                       AND cd.COde_Value = 'Determinant'
                              LEFT JOIN CBMS.dbo.Currency_Unit_Conversion cc
                                    ON cc.CURRENCY_GROUP_ID = @Currency_Group_Id
                                       AND cc.BASE_UNIT_ID = cu.Uom_Type_Id
                                       AND cc.CONVERTED_UNIT_ID = @Currency_Unit_Id
                                       AND cc.CONVERSION_DATE = cusd.Service_Month
                                       AND cd.COde_Value = 'Charge'
                          WHERE
                              cu.Service_Month = cusd.Service_Month
                              AND cu.Bucket_Name = cusd.Bucket_Name ) ob ( Bucket_Value )
      GROUP BY
            BL.Bucket_Name
           ,BL.Bucket_Master_Id
           ,BL.Bucket_Type_Cd
           ,( case WHEN CD.Code_Value = 'Charge' THEN CU.Currency_Unit_Name
                   WHEN CD.Code_Value = 'Determinant' THEN UOM.Entity_Name
              END )
      ORDER BY
            BL.Bucket_Name  
  
END  

;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Summary_For_Data_Compare] TO [CBMSApplication]
GO
