SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	su.ghg_Load_Queue_UpdHas_Errors


DESCRIPTION:
	Updates the ghg_Load_Queue.Has_Error_Flag for the ghg_Load_Queue_Id

INPUT PARAMETERS:
	Name						DataType		Default	Description
------------------------------------------------------------
    @ghg_Load_Queue_Id 	    Varcahr(25)					-- Record to update
    ,@Has_Error				Bit			1				-- value to update. 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	 CMH		12/01/2009	Created  
	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
Create Procedure su.ghg_Load_Queue_Upd_Has_Errors
(	
	@ghg_Load_Queue_Id 	    Varchar(25)					-- Record to update
    ,@Has_Errors			bit							-- value to update. If it is null then the current timestamp is used
)
AS 

BEGIN 
	SET NOCOUNT ON 
	
	UPDATE 
		su.ghg_Load_Queue
	SET 
		Has_Errors = @Has_Errors
	WHERE 
		ghg_Load_Queue_Id = @ghg_Load_Queue_Id

END
GO
GRANT EXECUTE ON  [su].[ghg_Load_Queue_Upd_Has_Errors] TO [CBMSApplication]
GO
