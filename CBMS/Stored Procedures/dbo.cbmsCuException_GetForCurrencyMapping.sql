SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:  
	dbo.cbmsCuException_GetForCurrencyMapping

DESCRIPTION:
	Used to return all the unmapped services from either determinant or charges of the gives invoice id.

INPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------
@Ubm_id			INT     
@Ubm_Service_Code VARCHAR(200)

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCuException_GetForCurrencyMapping	49, 1, 'Electric'
	EXEC dbo.cbmsCuException_GetForCurrencyMapping	49, 1, 'Misc Charges'
	EXEC dbo.cbmsCuException_GetForCurrencyMapping	49, 1, 'GAS'

	

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
NR			Narayana Reddy

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------

NR			2018-10-01	 D20-164 -Replaced Currency Not Mapped to Mapping Required - Currency Not Mapped

******/
CREATE PROCEDURE [dbo].[cbmsCuException_GetForCurrencyMapping]
    (
        @MyAccountId INT
        , @ubm_id INT
        , @ubm_currency_code VARCHAR(200)
    )
AS
    BEGIN

        DECLARE @exception_type_id INT;

        SET NOCOUNT ON;

        SELECT
            @exception_type_id = CU_EXCEPTION_TYPE_ID
        FROM
            dbo.CU_EXCEPTION_TYPE WITH (NOLOCK)
        WHERE
            EXCEPTION_TYPE = 'Mapping Required - Currency Not Mapped';

        SET NOCOUNT OFF;

        SELECT
            e.CU_EXCEPTION_ID
            , e.CU_INVOICE_ID
        FROM
            dbo.CU_INVOICE i WITH (NOLOCK)
            JOIN dbo.CU_EXCEPTION e WITH (NOLOCK)
                ON e.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION_DETAIL d WITH (NOLOCK)
                ON d.CU_EXCEPTION_ID = e.CU_EXCEPTION_ID
        WHERE
            i.UBM_ID = @ubm_id
            AND i.UBM_CURRENCY_CODE = @ubm_currency_code
            AND d.EXCEPTION_TYPE_ID = @exception_type_id
            AND d.IS_CLOSED = 0;

    END;


GO
GRANT EXECUTE ON  [dbo].[cbmsCuException_GetForCurrencyMapping] TO [CBMSApplication]
GO
