SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE     PROCEDURE dbo.ADD_ACCOUNT_GROUP_P
		@group_billing_number varchar(50),
		@vendor_id int
	AS
	begin
		set nocount on

		insert into account_group(GROUP_BILLING_NUMBER, VENDOR_ID)
		values (@group_billing_number, @vendor_id)

	end





GO
GRANT EXECUTE ON  [dbo].[ADD_ACCOUNT_GROUP_P] TO [CBMSApplication]
GO
