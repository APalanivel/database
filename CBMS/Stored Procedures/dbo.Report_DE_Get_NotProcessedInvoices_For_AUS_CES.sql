SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:

 dbo.[Report_DE_Get_NotProcessedInvoices_For_AUS_CES]

 DESCRIPTION:
	this procedure is to get notprocessed invoices for CES Feed

 INPUT PARAMETERS:
	Name			DataType	Default		Description
------------------------------------------------------------


 OUTPUT PARAMETERS:
	Name			DataType  Default Description
------------------------------------------------------------

  USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------


******/
CREATE PROCEDURE [dbo].[Report_DE_Get_NotProcessedInvoices_For_AUS_CES]
AS
BEGIN
      SET NOCOUNT ON;

      WITH InvLastAction_CTE
      AS ( SELECT
                 max(cue.EVENT_DATE) [Last Action Date]
               , cue.CU_INVOICE_ID
           FROM
                 dbo.CU_INVOICE_EVENT cue
                 JOIN CU_INVOICE cui
                       ON cui.CU_INVOICE_ID = cue.CU_INVOICE_ID
                 JOIN dbo.UBM u
                       ON u.UBM_ID = cui.UBM_ID
           WHERE
                 u.UBM_NAME = 'ces'
                 AND cui.IS_PROCESSED != 1
           GROUP BY
                 cue.CU_INVOICE_ID )
         , Account_Data_CTE
      AS ( SELECT DISTINCT Client_Name [Client]
                , Client_Id [ClientID]
                , Site_name [Site]
                , Site_Id [SiteID]
                , [SiteNotManagedStatus] = CASE WHEN ch.Site_Not_Managed = 1 THEN 'Inactive'
                                                ELSE  'Active'
                                           END
                , [SiteClosedStatus] = CASE WHEN ch.Site_Closed = 1 THEN 'Closed'
                                            ELSE  'Not Closed'
                                       END
                , cha.Display_Account_Number [Account Number]
                , cha.Account_Id [AccountID]
                , [AccountInvoiceNotExpectedStatus] = CASE WHEN cha.Account_Not_Expected = 1 THEN 'AccountNotExpected'
                                                           ELSE  'Expected'
                                                      END
                , [AccountNotManagedStatus] = CASE WHEN cha.Account_Not_Managed = 1 THEN 'Inactive'
                                                   ELSE  'Active'
                                              END
                , com.Commodity_Name [Commodity Type]
                , [Data Entry Only Status] = CASE WHEN cha.Account_Is_Data_Entry_Only = 1 THEN 'DEO'
                                                  ELSE  'Non-DEO'
                                             END
                , ch.Client_Hier_Id [ClientHierID]
                , cha.Account_Vendor_Name [Vendor]
           FROM
                  Core.Client_Hier ch
                  JOIN Core.Client_Hier_Account cha
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  INNER JOIN dbo.Commodity com
                        ON com.Commodity_Id = cha.Commodity_Id )
      SELECT DISTINCT [CBMS Client] = c.CLIENT_NAME
           , ui.UBM_CLIENT_NAME [UBM Client]
           , cui.CLIENT_ID [ClientID]
           , [Site] = CASE WHEN Ac.[Site] IS NOT NULL THEN Ac.Site
                           ELSE  ui.CITY + ', ' + ui.STATE
                      END
           , Ac.SiteID
           , Ac.[SiteNotManagedStatus]
           , Ac.SiteClosedStatus
           , Ac.[Account Number]
           , Ac.AccountID
           , Ac.AccountInvoiceNotExpectedStatus
           , Ac.[AccountNotManagedStatus]
           , Ac.[Commodity Type]
           , Ac.[Data Entry Only Status]
           --,ch.Client_Hier_Id [ClientHierID]
           , cui.CU_INVOICE_ID [Invoice ID]
           , ui.UBM_BATCH_MASTER_LOG_ID [Batch ID]
           , ci.CBMS_DOC_ID [File Name]
           , [Vendor] = CASE WHEN Ac.[Vendor] IS NOT NULL THEN Ac.Vendor
                             ELSE  ui.UBM_VENDOR_NAME
                        END
           , vq.queue_name [Queue]
           , ui.PROCESSED_DATE [UBM Invoice Process Date]
           , cui.UPDATED_DATE [Invoice Updated]
           , [History Last Event Date] = ic.[Last Action Date]
           , cued.EXCEPTION_TYPE [Exception]
           , cui.BEGIN_DATE [Invoice Start Date]
           , cui.END_DATE [Invoice End Date]
      FROM
             CU_INVOICE cui
             INNER JOIN dbo.UBM u
                   ON u.UBM_ID = cui.UBM_ID
             JOIN dbo.UBM_INVOICE ui
                   ON ui.UBM_INVOICE_ID = cui.UBM_INVOICE_ID
             INNER JOIN dbo.cbms_image ci
                   ON ci.CBMS_IMAGE_ID = cui.CBMS_IMAGE_ID
             LEFT JOIN dbo.CU_EXCEPTION_DENORM cued
                   ON cued.CU_INVOICE_ID = cui.CU_INVOICE_ID
             LEFT JOIN dbo.vwCbmsQueueName vq
                   ON vq.queue_id = cued.QUEUE_ID
             LEFT JOIN InvLastAction_CTE ic
                   ON ic.CU_INVOICE_ID = cui.CU_INVOICE_ID
             LEFT JOIN Account_Data_CTE Ac
                   ON Ac.AccountID = cued.Account_ID
                      AND Ac.ClientHierID = cued.Client_Hier_ID
             INNER JOIN CLIENT c
                   ON c.CLIENT_ID = cui.CLIENT_ID
      WHERE
             cui.IS_PROCESSED != 1 -- not processed
             AND u.UBM_NAME = 'ces';

END;
;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Get_NotProcessedInvoices_For_AUS_CES] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Get_NotProcessedInvoices_For_AUS_CES] TO [CBMSApplication]
GO
