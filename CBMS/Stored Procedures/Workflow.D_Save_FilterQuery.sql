SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    [Workflow].[D_Save_FilterQuery]
DESCRIPTION: 
------------------------------------------------------------   
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
 @int_Id   AS INT    
 @bit_IsActive  AS BIT     
------------------------------------------------------------    
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
 @str_Return  AS VARCHAR(100) OUTPUT
------------------------------------------------------------    
 USAGE EXAMPLES:    
 DECLARE @str_Return VARCHAR(100)
 EXEC [Workflow].[D_Save_FilterQuery]  @int_Id=0 ,@bit_IsActive=0 ,@str_Return=@str_Return
 SELECT @str_Return
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
TRK			Ramakrishna Thummala	Summit Energy 
 
 MODIFICATIONS     
 Initials Date   Modification    
------------------------------------------------------------    
 TRK		  Sep-2019  Created

******/
 
CREATE PROCEDURE [Workflow].[D_Save_FilterQuery]      
(      
 @int_Id   AS INT ,      
 @bit_IsActive  AS BIT ,      
 @str_Return  AS VARCHAR(100) OUTPUT      
      
)      
AS      
BEGIN      
 BEGIN TRY      
      
    
  Delete from  Workflow.Workflow_queue_saved_filter_query_value where Workflow_Queue_Saved_Filter_Query_Id =  @int_Id     
    
  Delete from  Workflow.Workflow_queue_saved_filter_query where Workflow_Queue_Saved_Filter_Query_Id =  @int_Id     
    
      
     
  SELECT  @str_Return =  'Deleted successfully '       
 END TRY      
 BEGIN CATCH       
  SELECT @str_Return =  'Error Occured , please try again'      
 END CATCH      
      
END          
GO
GRANT EXECUTE ON  [Workflow].[D_Save_FilterQuery] TO [CBMSApplication]
GO
