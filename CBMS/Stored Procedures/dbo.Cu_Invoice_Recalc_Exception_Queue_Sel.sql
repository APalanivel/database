SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  dbo.Cu_Invoice_Recalc_Exception_Queue_Sel  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
 @Cu_Invoice_Id						INT
 @Account_Id						INT
 @Commodity_Id						INT

  

OUTPUT PARAMETERS:  
 Name								DataType				Default		Description  
-------------------------------------------------------------------------------------  
  
USAGE EXAMPLES:  
-------------------------------------------------------------------------------------  
 BEGIN Transaction
 
 select * from Cu_Invoice_Recalc_Exception_Queue
 EXEC Cu_Invoice_Recalc_Exception_Queue_Merge 35238713,55236,290,100981,'2015-11-06',49
 EXEC Cu_Invoice_Recalc_Exception_Queue_Sel 35238713
 
 
 EXEC Cu_Invoice_Recalc_Exception_Queue_Merge 35238713,55236,290,100982,'2015-11-06',49
 EXEC Cu_Invoice_Recalc_Exception_Queue_Sel 35238713
 select * from Cu_Invoice_Recalc_Exception_Queue
 
 
 ROLLBACK Transaction 
  
  
AUTHOR INITIALS:  
 Initials	Name  
-------------------------------------------------------------------------------------  
 RKV		Ravi Kumar Vegesna  
   
MODIFICATIONS  
  
 Initials		Date			Modification  
-------------------------------------------------------------------------------------  
 RKV			2015-11-06		Created for AS400-PII 
            
              
******/  
CREATE PROCEDURE [dbo].[Cu_Invoice_Recalc_Exception_Queue_Sel]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT = NULL
      ,@Commodity_Id INT = NULL )
AS 
BEGIN  
      
      
     
      SELECT
            cireq.Cu_Invoice_Recalc_Exception_Queue_Id
           ,cireq.Cu_Invoice_Id
           ,cireq.Account_Id
           ,cireq.Commodity_Id
           ,cireq.Status_Cd
           ,c.Code_Value [Status]
           ,cireq.Queue_Date
           ,cireq.User_Info_Id
           ,cireq.Last_Change_Ts
      FROM
            dbo.Cu_Invoice_Recalc_Exception_Queue cireq
            INNER JOIN dbo.Code c
                  ON c.Code_Id = cireq.Status_Cd
      WHERE
            cireq.Cu_Invoice_Id = @Cu_Invoice_Id
            AND ( @Commodity_Id IS NULL
                  OR cireq.Commodity_Id = @Commodity_Id )
            AND ( @Account_Id IS NULL
                  OR cireq.Account_Id = @Account_Id )
        
      
       
END     
   

;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Recalc_Exception_Queue_Sel] TO [CBMSApplication]
GO
