SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:  
	dbo.cbmsCuException_GetForDetServiceMapping

DESCRIPTION:
	Used to return all the unmapped services from either determinant or charges of the gives invoice id.

INPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------
@Ubm_id			INT     
@Ubm_Service_Code VARCHAR(200)

OUTPUT PARAMETERS:
Name      DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsCuException_GetForDetServiceMapping	1, 'Electric'
	EXEC dbo.cbmsCuException_GetForDetServiceMapping	1, 'Misc Charges'
	EXEC dbo.cbmsCuException_GetForDetServiceMapping	1, 'GAS'

	SELECT TOP 10 * FROM dbo.Cu_Invoice_Charge  chg
	WHERE
        chg.ubm_service_code IS NOT NULL
          AND chg.ubm_service_type_id IS NULL
          AND commodity_Type_id >0 AND commodity_Type_id <290
	ORDER BY Cu_invoice_id DESC

	SELECT TOP 10 * FROM dbo.Cu_Invoice_determinant  chg
	WHERE
        chg.ubm_service_code IS NOT NULL
          AND chg.ubm_service_type_id IS NULL
          AND commodity_Type_id >0 AND commodity_Type_id <290
	ORDER BY Cu_invoice_id DESC

AUTHOR INITIALS:
Initials	Name
------------------------------------------------------------
HG			Hari
NR			Narayana Reddy

MODIFICATIONS
Initials	Date		Modification
------------------------------------------------------------
HG			05/16/2011	Comments header added
						-- MAINT-547 fixes the code to return un mapped services from determinant / charges
						-- DISTINCT clause replaced by GROUP BY clause
						-- NOLOCK hints removed
						-- unused @MyAccountId parameter has been removed
NR			2018-10-01	 D20-164 -Replaced Service Not Mapped to Mapping Required - Service Not Mapped

******/

CREATE PROCEDURE [dbo].[cbmsCuException_GetForDetServiceMapping]
    (
        @ubm_id INT
        , @ubm_service_code VARCHAR(200)
    )
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Services TABLE
              (
                  Cu_Invoice_Id INT
                  , Cu_Exception_Id INT
              );

        DECLARE @exception_type_id INT;

        SELECT
            @exception_type_id = CU_EXCEPTION_TYPE_ID
        FROM
            dbo.CU_EXCEPTION_TYPE
        WHERE
            EXCEPTION_TYPE = 'Mapping Required - Service Not Mapped';

        INSERT INTO @Services
             (
                 Cu_Invoice_Id
                 , Cu_Exception_Id
             )
        SELECT
            i.CU_INVOICE_ID
            , e.CU_EXCEPTION_ID
        FROM
            dbo.CU_INVOICE i
            JOIN dbo.CU_INVOICE_DETERMINANT d
                ON d.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION e
                ON e.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION_DETAIL ed
                ON ed.CU_EXCEPTION_ID = e.CU_EXCEPTION_ID
        WHERE
            i.UBM_ID = @ubm_id
            AND d.UBM_SERVICE_CODE = @ubm_service_code
            AND ed.EXCEPTION_TYPE_ID = @exception_type_id
            AND d.UBM_SERVICE_TYPE_ID IS NULL
            AND ed.IS_CLOSED = 0
        GROUP BY
            i.CU_INVOICE_ID
            , e.CU_EXCEPTION_ID;


        INSERT INTO @Services
             (
                 Cu_Invoice_Id
                 , Cu_Exception_Id
             )
        SELECT
            i.CU_INVOICE_ID
            , e.CU_EXCEPTION_ID
        FROM
            dbo.CU_INVOICE i
            JOIN dbo.CU_INVOICE_CHARGE c
                ON c.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION e
                ON e.CU_INVOICE_ID = i.CU_INVOICE_ID
            JOIN dbo.CU_EXCEPTION_DETAIL ed
                ON ed.CU_EXCEPTION_ID = e.CU_EXCEPTION_ID
        WHERE
            i.UBM_ID = @ubm_id
            AND c.UBM_SERVICE_CODE = @ubm_service_code
            AND ed.EXCEPTION_TYPE_ID = @exception_type_id
            AND c.UBM_SERVICE_TYPE_ID IS NULL
            AND ed.IS_CLOSED = 0
        GROUP BY
            i.CU_INVOICE_ID
            , e.CU_EXCEPTION_ID;

        SELECT
            Cu_Invoice_Id
            , Cu_Exception_Id
        FROM
            @Services
        GROUP BY
            Cu_Invoice_Id
            , Cu_Exception_Id;

    END;


GO
GRANT EXECUTE ON  [dbo].[cbmsCuException_GetForDetServiceMapping] TO [CBMSApplication]
GO