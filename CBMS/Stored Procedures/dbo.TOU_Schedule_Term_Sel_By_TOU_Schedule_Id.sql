SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********    
NAME:  dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Id    
    
DESCRIPTION:      
    
Used to select TOU Schedule Term details by given Time_Of_Use_Schedule_Id    
    
INPUT PARAMETERS:  
  
      Name						 DataType          Default     Description    
------------------------------------------------------------    
	  @Time_Of_Use_Schedule_Id   INT    
        
    
OUTPUT PARAMETERS:    
      Name						 DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
    
 Exec dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Id 106
    
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 BCH	  Balaraju    
    
    
 Initials Date		  Modification    
------------------------------------------------------------    
 BCH	  2012-07-06  Created    
    
******/    
CREATE PROCEDURE dbo.TOU_Schedule_Term_Sel_By_TOU_Schedule_Id
    (
     @Time_Of_Use_Schedule_Id INT 
    )
AS 
    BEGIN    
    
        SET NOCOUNT ON ;    
        SELECT  TOUT.Time_Of_Use_Schedule_Term_Id
               ,TOUT.CBMS_IMAGE_ID
               ,TOUT.Start_Dt
               ,CASE WHEN TOUT.End_Dt = '2099-12-31' THEN NULL
                     ELSE TOUT.End_Dt
                END AS End_Dt
               ,COUNT(1) OVER ( ) Term_Count
               ,CASE WHEN EXISTS ( SELECT   1
                                   FROM     dbo.RATE_SCHEDULE
                                   WHERE    Time_Of_Use_Schedule_Term_Id = TOUT.Time_Of_Use_Schedule_Term_Id ) THEN 1
                     ELSE 0
                END AS Rate_Exists
        FROM    dbo.Time_Of_Use_Schedule_Term TOUT
        WHERE   TOUT.Time_Of_Use_Schedule_Id = @Time_Of_Use_Schedule_Id
        ORDER BY TOUT.Start_Dt ASC    
    END     

;
GO
GRANT EXECUTE ON  [dbo].[TOU_Schedule_Term_Sel_By_TOU_Schedule_Id] TO [CBMSApplication]
GO
