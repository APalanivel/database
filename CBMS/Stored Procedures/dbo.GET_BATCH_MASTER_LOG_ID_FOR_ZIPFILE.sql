SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/******************************************************************************************************    
NAME :	dbo.GET_BATCH_MASTER_LOG_ID_FOR_ZIPFILE
   
DESCRIPTION: This procedure used to retreive the BATCH_MASTER_LOG_ID 
for a ZipFile to confirm weather a zipfile is already processed or not.

   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @ZipFileName    VARCHAR(MAX)							ZipFileName
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------    
 UBM_BATCH_MASTER_LOG_ID INT
  
  USAGE EXAMPLES: 
--------------------------------------------------------------------    
EXEC GET_BATCH_MASTER_LOG_ID_FOR_ZIPFILE 'SummitATTXMLFeed1-20100601115959.zip'
  
AUTHOR INITIALS:    
 Initials Name    
-------------------------------------------------------------------    
 ROMY THOMAS
   
 MODIFICATIONS     
 Initials Date  Modification    
--------------------------------------------------------------------    
  
******************************************************************************************************/


CREATE PROCEDURE [dbo].[GET_BATCH_MASTER_LOG_ID_FOR_ZIPFILE]
(
	@ZipFileName	VARCHAR(MAX) 
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT UBM_BATCH_MASTER_LOG_ID 
	FROM UBM_FEED_FILE_MAP
	WHERE FEED_FILE_NAME=@ZipFileName

	SET NOCOUNT OFF

END



GO
GRANT EXECUTE ON  [dbo].[GET_BATCH_MASTER_LOG_ID_FOR_ZIPFILE] TO [CBMSApplication]
GO
