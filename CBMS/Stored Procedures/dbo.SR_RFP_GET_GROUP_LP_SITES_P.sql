SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- EXEC dbo.SR_RFP_GET_GROUP_LP_SITES_P '1','1', 233
CREATE  PROCEDURE dbo.SR_RFP_GET_GROUP_LP_SITES_P
	@user_id varchar(10),
	@session_id varchar(20),
	@rfp_id int
	AS
	set nocount on
select	approval.site_id,
	c.cbms_image_id
	

from 	cbms_image c(nolock),
	sr_rfp_lp_client_approval approval (nolock)
	 
where 	approval.sr_rfp_id = @rfp_id	
	and approval.is_group_lp = 1
	and c.cbms_image_id = approval.cbms_image_id
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_GROUP_LP_SITES_P] TO [CBMSApplication]
GO
