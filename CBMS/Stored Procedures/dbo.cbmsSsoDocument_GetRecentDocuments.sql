SET NUMERIC_ROUNDABORT OFF 
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO

/******
 NAME:  
	dbo.cbmsSsoDocument_GetRecentDocuments
 
 DESCRIPTION:   
 INPUT PARAMETERS:  
 Name			DataType	Default Description  
------------------------------------------------------------  
@MyAccountId	int
@client_id		int			null
@division_id	int			null
@site_id		int			null
 
 OUTPUT PARAMETERS:  

 Name   DataType  Default Description  
------------------------------------------------------------  
 USAGE EXAMPLES:
------------------------------------------------------------  
  
  EXEC dbo.cbmsSsoDocument_GetRecentDocuments 49
  
 AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 CPE		Chaitanya Panduga Eshwar
 
 MODIFICATIONS:
 Initials Date			Modification  
------------------------------------------------------------  
 CPE	  03/25/2011	Modified the SP to replace vwCbmsSSOdocumentOwnerFlat with SSO_DOCUMENT_OWNER_MAP table
						Removed join on vwCbmsSSOdocumentOwnerFlat
            
******/

CREATE PROCEDURE dbo.cbmsSsoDocument_GetRecentDocuments
( 
 @MyAccountId int
,@client_id int = null
,@division_id int = null
,@site_id int = null )
AS 
BEGIN

      SET NOCOUNT ON

      exec cbmsSecurity_GetClientAccess @MyAccountId, @client_id output, @division_id output, @site_id output

      SELECT TOP 5
            SD.SSO_DOCUMENT_ID
           ,SD.DOCUMENT_TITLE
           ,SD.DOCUMENT_REFERENCE_DATE
           ,SD.CBMS_IMAGE_ID
      FROM
            dbo.SSO_DOCUMENT SD
            JOIN ( SELECT DISTINCT
                        SSO_DOCUMENT_ID
                   FROM
                        dbo.SSO_DOCUMENT_OWNER_MAP SDOM
                        JOIN Core.Client_Hier CH
                              ON SDOM.Client_Hier_Id = CH.Client_Hier_Id
                   WHERE
                        ( @Client_Id IS NULL
                          OR CH.client_id = @client_id )
                        AND ( @division_id IS NULL
                              OR CH.Sitegroup_Id = @division_id )
                        AND ( @site_id IS NULL
                              OR CH.Site_Id = @site_id ) ) acc
                  ON SD.SSO_DOCUMENT_ID = acc.SSO_DOCUMENT_ID
      ORDER BY
            SD.DOCUMENT_REFERENCE_DATE DESC
           ,SD.DOCUMENT_TITLE
END


GO
GRANT EXECUTE ON  [dbo].[cbmsSsoDocument_GetRecentDocuments] TO [CBMSApplication]
GO
