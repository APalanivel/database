SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_RPT_GET_ANALYSTS_BY_STATE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@stateIds      	varchar(10)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec SR_SAD_RPT_GET_ANALYSTS_BY_STATE_P  '0'
-- exec SR_SAD_RPT_GET_ANALYSTS_BY_STATE_P  '1,2'

CREATE     PROCEDURE dbo.SR_SAD_RPT_GET_ANALYSTS_BY_STATE_P 
@stateIds varchar(10)
AS
set nocount on
	if(@stateIds = '' or @stateIds = '0')
BEGIN

		select  ui.user_info_id, ui.first_name + ' '+ui.last_name
		from
		utility_detail detail ,
		vendor_state_map stateMap,
		utility_analyst_map analystMap,
		user_info ui,
		region reg,
		state st
		where
		(ui.user_info_id = analystMap.gas_analyst_id or ui.user_info_id = analystMap.power_analyst_id)
		and detail.utility_detail_id = analystMap.utility_detail_id
		and detail.vendor_id = stateMap.vendor_id
		and stateMap.state_id = st.state_id
		and reg.region_id = st.region_id
		and stateMap.is_history = 0
		AND ui.IS_HISTORY = 0
		group by ui.user_info_id,ui.first_name + ' '+ui.last_name
		
		
END

if(@stateIds <> '' and @stateIds <> '0')
BEGIN
		select  ui.user_info_id,ui.first_name + ' '+ui.last_name
		from
		utility_detail detail ,
		vendor_state_map stateMap,
		utility_analyst_map analystMap,
		user_info ui,
		region reg,
		state st
		where
		(ui.user_info_id = analystMap.gas_analyst_id or ui.user_info_id = analystMap.power_analyst_id)
		and detail.utility_detail_id = analystMap.utility_detail_id
		and detail.vendor_id = stateMap.vendor_id
		and stateMap.state_id = st.state_id
		and reg.region_id = st.region_id
		and st.state_id in  ( select IntValue  from dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT(@stateIds))
		and stateMap.is_history = 0
		AND UI.IS_HISTORY = 0
		group by ui.user_info_id,ui.first_name + ' '+ui.last_name
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_GET_ANALYSTS_BY_STATE_P] TO [CBMSApplication]
GO
