SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--New one
--EXEC SR_RFP_LP_GET_HISTORICAL_START_MONTH_P '05/01/2008', '10/01/2009'
CREATE   PROCEDURE dbo.SR_RFP_LP_GET_HISTORICAL_START_MONTH_P
	@from_month datetime,
	@to_month datetime
	AS

	set nocount on
	DECLARE @noOfYears int,
		@curr_date datetime,
		@curr_year int

	/**Logic to identify the historical year to get usage data */
	select @curr_date = getdate()
	select @noOfYears = DATEDIFF(year, @curr_date, @from_month)

	select @curr_year = YEAR(@curr_date)


	if @noOfYears < 0 or @noOfYears = 0
	begin
		set @noOfYears = 1
	end 
	else 
	begin
		declare @year_cnt int, @check bit
		set @year_cnt = 1
		set @check = 0
		while(@year_cnt <= @noOfYears)
		begin
			if (@curr_year =  YEAR(dateadd(yyyy, (-1 *@year_cnt), @from_month)) )
			begin
				set @check = 1
			end
			set @year_cnt = @year_cnt + 1
		end
		if @check = 0
		begin
			select @noOfYears = @noOfYears + 1
		end	
	end

	select dateadd(yyyy, (-1 * @noOfYears), @from_month) as start_month
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_GET_HISTORICAL_START_MONTH_P] TO [CBMSApplication]
GO
