SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_TIRGGER_NOT_OF_FIXED_TYPE_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@dealTicketId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE PROCEDURE dbo.GET_TIRGGER_NOT_OF_FIXED_TYPE_P
@userId varchar,
@sessionId varchar,
@dealTicketId int


 AS
set nocount on		
	select distinct e1.ENTITY_NAME from  RM_DEAL_TICKET_details rdtd 
	JOIN ENTITY e1 ON (e1.entity_id = rdtd.TRIGGER_STATUS_TYPE_ID) 
	where e1.entity_id  =  (SELECT MAX(TRIGGER_STATUS_TYPE_ID) FROM RM_DEAL_TICKET_DETAILS  
	where RM_DEAL_TICKET_ID = @dealTicketId)
GO
GRANT EXECUTE ON  [dbo].[GET_TIRGGER_NOT_OF_FIXED_TYPE_P] TO [CBMSApplication]
GO
