SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********     
NAME:    
	dbo.CBMS_UPDATE_VENDOR_ADDRESS_P    

DESCRIPTION:  This procedure used to update the records in Utility_detail table table

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------------    
@state_id int,
@vendor_id int
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
    
USAGE EXAMPLES:    
------------------------------------------------------------    
--EXEC [dbo].[CBMS_UPDATE_VENDOR_ADDRESS_P] 
--@state_id = 192,  -- Original value = 191
--@vendor_id = 3047

Initials Name    
------------------------------------------------------------    
DR       Deana Ritter

Initials Date  Modification    
------------------------------------------------------------    
DR    8/4/2009	   Removed Linked Server Updates
 DMR		  09/10/2010 Modified for Quoted_Identifier

          
******/
CREATE PROCEDURE [dbo].[CBMS_UPDATE_VENDOR_ADDRESS_P] 
@state_id int,
@vendor_id int

AS


UPDATE 
	VENDOR_STATE_MAP 
SET 
	STATE_ID = @state_id 
WHERE 
	VENDOR_ID = @vendor_id
GO
GRANT EXECUTE ON  [dbo].[CBMS_UPDATE_VENDOR_ADDRESS_P] TO [CBMSApplication]
GO
