SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********  
NAME: dbo.Contract_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id    

DESCRIPTION:     

	To get the Contract_attributes based on the cu_invoice_id, account_id,Commodity_Id.
	
INPUT PARAMETERS:    
Name					DataType  Default Description    
------------------------------------------------------------    
@Cu_Invoice_Id 				INT     
@Account_Id 				INT      
@Commodity_Id				INT     

OUTPUT PARAMETERS:    
Name   DataType  Default Description    
------------------------------------------------------------    

USAGE EXAMPLES:    
------------------------------------------------------------    
EXEC Contract_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id 30562408,6065,291


AUTHOR INITIALS:    
Initials	Name    
------------------------------------------------------------    
RKV			Ravi Kumar Vegesna
 
MODIFICATIONS     
Initials	Date		Modification    
------------------------------------------------------------    
 RKV     2015-09-29		  Created    
 
    
******/
CREATE PROCEDURE [dbo].[Contract_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id]
      @Cu_Invoice_Id INT
     ,@Account_Id INT
     ,@Commodity_Id INT
AS 
BEGIN      
      SET NOCOUNT ON   
   

      SELECT
            eca.EC_Contract_Attribute_Id
           ,eca.EC_Contract_Attribute_Name
           ,ecat.EC_Contract_Attribute_Value
      FROM
            dbo.CU_INVOICE_SERVICE_MONTH cism
            INNER JOIN core.Client_Hier_Account cha
                  ON cism.Account_ID = cha.Account_Id
            INNER JOIN dbo.EC_Contract_Attribute eca
                  ON cha.Commodity_Id = eca.Commodity_Id
                     AND cha.Meter_State_Id = eca.State_Id
            LEFT OUTER JOIN ( dbo.EC_Contract_Attribute_Tracking ecat
                              INNER JOIN core.Client_Hier_Account cha1
                                    ON cha1.Account_Id = @Account_Id
                              INNER JOIN core.Client_Hier_Account scha
                                    ON cha1.Meter_Id = scha.Meter_Id
                                       AND scha.Account_Type = 'Supplier'
                                       AND scha.Supplier_Contract_ID = ecat.Contract_Id
                              INNER   JOIN dbo.CONTRACT c
                                    ON ecat.Contract_Id = c.CONTRACT_ID )
                              ON eca.EC_Contract_Attribute_Id = ecat.EC_Contract_Attribute_Id
                                 AND cism.SERVICE_MONTH >= c.CONTRACT_START_DATE
                                 AND cism.SERVICE_MONTH <= c.CONTRACT_END_DATE
      WHERE
            cha.Commodity_Id = @Commodity_Id
            AND cha.Account_Id = @Account_Id
            AND cism.CU_INVOICE_ID = @CU_INVOICE_ID
END
;
GO
GRANT EXECUTE ON  [dbo].[Contract_Attributes_Sel_By_Cu_Invoice_Id_Account_Id_Commodity_Id] TO [CBMSApplication]
GO
