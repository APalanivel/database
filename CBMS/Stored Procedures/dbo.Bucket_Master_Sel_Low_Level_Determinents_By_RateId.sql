
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[Bucket_Master_Sel_Low_Level_Determinents_By_RateId]

DESCRIPTION:

	To get the determiinant details
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@RateId			INT
@Start_Dt		DATE
@End_Dt			DATE


OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------  

	EXEC Bucket_Master_Sel_Low_Level_Determinents_By_RateId 36528,'01-01-2011','12-31-2011'
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
AL			Ajeesh Lonachan
KVK			Vinay Kumar K

MODIFICATIONS
INITIALS	DATE		MODIFICATION
-----------------------------------------------------------
AL			2012-07-18	Created for Rate Engine API
AL			2012-10-04	Modified to select Rate name
KVK			12/03/2012	Updated the code to not have any hardcoded values while query entity table(as per DBA suggestion).

*/
CREATE PROCEDURE [dbo].[Bucket_Master_Sel_Low_Level_Determinents_By_RateId]
      ( 
       @RateId INT
      ,@Start_Dt DATE
      ,@End_Dt DATE )
AS 
BEGIN

      SET NOCOUNT ON
   
      DECLARE @Entity_Type INT

      SET @Entity_Type = ( SELECT
                              ENTITY_TYPE
                           FROM
                              Entity
                           WHERE
                              ENTITY_DESCRIPTION = 'Billing Determinant Charge Parent'
                           GROUP BY
                              ENTITY_TYPE );

      WITH  Rate_Schedule
              AS ( SELECT
                        RS.RATE_SCHEDULE_ID
                   FROM
                        dbo.RATE_SCHEDULE rs
                   WHERE
                        RS.RATE_ID = @RateId
                        AND ( ( @Start_Dt <= RS.RS_END_DATE
                                AND @End_Dt > = Rs.RS_START_DATE )
                              OR ( RS.RS_START_DATE BETWEEN @Start_Dt AND @End_Dt )
                              OR ( RS.RS_START_DATE < @Start_Dt
                                   AND RS.RS_END_DATE IS NULL ) ))
            SELECT
                  c.CHARGE_NAME AS Name
                 ,bm.External_Reference_Key AS KeyName
                 ,cm.CHARGE_DISPLAY_ID AS Variable_Name
                 ,'input' AS Calculation_Type
                 ,0 AS TOU_Peak_Id
                 ,c.CURRENCY_UNIT_ID AS UOMId
                 ,1 AS IsCharge
                 ,c.CHARGE_PARENT_ID AS RATE_SCHEDULE_ID
                 ,'Month' AS Frequency
            FROM
                  dbo.CHARGE c
                  INNER JOIN dbo.entity e
                        ON e.ENTITY_ID = c.CHARGE_PARENT_TYPE_ID
                  INNER JOIN dbo.entity ct
                        ON ct.ENTITY_ID = c.CHARGE_CALCULATION_TYPE_ID
                  INNER JOIN dbo.Bucket_Master bm
                        ON c.Charge_Bucket_Master_Id = bm.Bucket_Master_Id
                  INNER JOIN dbo.CHARGE_MASTER cm
                        ON CM.CHARGE_MASTER_ID = c.CHARGE_MASTER_ID
            WHERE
                  e.ENTITY_name = 'Rate Schedule'
                  AND e.ENTITY_TYPE = @Entity_Type
                  AND ct.ENTITY_NAME IN ( '$Var', '%Var', '$Unknown' )
                  AND c.CHARGE_PARENT_ID IN ( SELECT
                                                RATE_SCHEDULE_ID
                                              FROM
                                                Rate_Schedule )
            GROUP BY
                  [CHARGE_NAME]
                 ,bm.External_Reference_Key
                 ,cm.CHARGE_DISPLAY_ID
                 ,c.CURRENCY_UNIT_ID
                 ,c.CHARGE_PARENT_ID
            UNION ALL
            SELECT
                  case WHEN cdcal.Code_Value = 'input' THEN BILLING_DETERMINANT_NAME
                       ELSE ''
                  END AS Name
                 ,case WHEN cdcal.Code_Value = 'input' THEN bm.External_Reference_Key
                       ELSE er.Code_Value
                  END AS KeyName
                 ,bdm.BD_DISPLAY_ID AS Variable_Name
                 ,cdcal.Code_Value AS Calculation_Type
                 ,touspd.Time_Of_Use_Schedule_Term_Peak_Id
                 ,bd.BD_UNIT_TYPE_ID AS UOMId
                 ,0 AS IsCharge
                 ,bd.BD_PARENT_ID
                 ,efre.ENTITY_NAME AS Frequency
            FROM
                  dbo.BILLING_DETERMINANT bd
                  INNER JOIN dbo.BILLING_DETERMINANT_MASTER bdm
                        ON BDM.BILLING_DETERMINANT_MASTER_ID = bd.BILLING_DETERMINANT_MASTER_ID
                  INNER JOIN dbo.ENTITY efre
                        ON EFre.ENTITY_ID = bd.BD_FREQUENCY_TYPE_ID
                  INNER JOIN dbo.Code cdcal
                        ON CdCal.Code_Id = bd.Interval_Manipulation_Logic_Cd
                  INNER JOIN dbo.entity e
                        ON e.ENTITY_ID = bd.BD_PARENT_TYPE_ID
                  INNER JOIN dbo.Bucket_Master bm
                        ON bd.bd_Bucket_Master_Id = bm.Bucket_Master_Id
                  LEFT JOIN Time_Of_Use_Schedule_Term_Peak_Billing_Determinant touspd
                        ON TOUSPD.BILLING_DETERMINANT_ID = bd.BILLING_DETERMINANT_ID
                  LEFT JOIN dbo.Time_Of_Use_Schedule_Term_Peak toup
                        ON TOUP.Time_Of_Use_Schedule_Term_Peak_Id = TOUSPD.Time_Of_Use_Schedule_Term_Peak_Id
                  INNER JOIN dbo.Code er
                        ON ER.Code_Id = bm.Bucket_Stream_Cd
            WHERE
                  bd.BD_PARENT_ID IN ( SELECT
                                          RATE_SCHEDULE_ID
                                       FROM
                                          Rate_Schedule )
                  AND e.ENTITY_name = 'Rate Schedule'
                  AND e.ENTITY_TYPE = @Entity_Type
            GROUP BY
                  case WHEN CdCal.Code_Value = 'input' THEN BILLING_DETERMINANT_NAME
                       ELSE ''
                  END
                 ,case WHEN CdCal.Code_Value = 'input' THEN bm.External_Reference_Key
                       ELSE ER.Code_Value
                  END
                 ,bdm.BD_DISPLAY_ID
                 ,cdcal.Code_Value
                 ,touspd.Time_Of_Use_Schedule_Term_Peak_Id
                 ,bd.BD_UNIT_TYPE_ID
                 ,bd.BD_PARENT_ID
                 ,efre.ENTITY_NAME
	
	
	
      SELECT
            R.RATE_ID
           ,R.RATE_NAME
      FROM
            dbo.RATE R
      WHERE
            RATE_ID = @RateId 
END
;
GO

GRANT EXECUTE ON  [dbo].[Bucket_Master_Sel_Low_Level_Determinents_By_RateId] TO [CBMSApplication]
GO
