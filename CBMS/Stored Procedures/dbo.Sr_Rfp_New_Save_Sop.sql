SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.Sr_Rfp_New_Save_Sop

DESCRIPTION: 


INPUT PARAMETERS:    
    Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@supplierContactMap				VARCHAR(200)  
	@supplierContactId				INT  
	@accountTermId					INT  
	@supplierBOCId					INT  
	@productName					VARCHAR(200)  
	@accountGroupId					INT  
	@isBidGroupId					INT  
	@priceComments					VARCHAR(4000)  
	@sopSummaryComments				VARCHAR(4000)  
	@isRecommENDed					INT  
	@currentVENDorName				NVARCHAR(max)
	@pricingSummary					NVARCHAR(max)
	@startDate						NVARCHAR(max)  
	@ENDDate						NVARCHAR(max)  
	@isAccountTerm					VARCHAR(200)  
	@mode							VARCHAR(200)  
	@volume							NUMERIC(32,16)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.Sr_Rfp_New_Save_Sop 9,9,'Cornerstone Energy~Mpetersen',19155,12968,68820,'Basis Price',13433,0,null,null,0,null,null,null,null,'IS_ACCOUNT_TERM','View_SOP',27572.0  

--exec dbo.Sr_Rfp_New_Save_Sop 9,9,'Cornerstone Energy~Mpetersen',19155,12968,68820,'Basis Price',13433,0,null,null,0,null,null,null,null,'IS_ACCOUNT_TERM','View_SOP',27572.0  



--exec dbo.Sr_Rfp_New_Save_Sop
 
-- @userId = 9,  
-- @sessionId = 9,  
-- @supplierContactMap = 'Cornerstone Energy~Mpetersen',  
-- @supplierContactId = 19155,
-- @accountTermId = 12968,
-- @supplierBOCId = 68820,
-- @productName ='Basis Price',
-- @accountGroupId = 13433,
-- @isBidGroupId = 0,
-- @priceComments = 'Test Data',
-- @sopSummaryComments = 'Test Data',
-- @isRecommENDed =0,
-- @currentVENDorName = 'Test Data',  
-- @pricingSummary = 'Test Data',  
-- @startDate = null, 
-- @ENDDate = null,  
-- @isAccountTerm = 'IS_ACCOUNT_TERM',  
-- @mode = 'View_SOP',  
-- @volume = 27572.0
  

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-05-04	Created in place of dbo.SR_RFP_SAVE_SOP_P
							The old script dbo.SR_RFP_SAVE_SOP_P is still using for legacy RFPs
	RR			2016-05-04	GCS-479 Modified data type for inputs @currentVENDorName,@pricingSummary,@startDate,@ENDDate to NVARCHAR(MAX)
				2016-05-10	GCS-887 Saving Pricing_Comments to SOP bid 
******/
CREATE PROCEDURE [dbo].[Sr_Rfp_New_Save_Sop]
      ( 
       @supplierContactMap VARCHAR(200)
      ,@supplierContactId INT
      ,@accountTermId INT
      ,@supplierBOCId INT
      ,@productName VARCHAR(200)
      ,@accountGroupId INT
      ,@isBidGroupId INT
      ,@priceComments VARCHAR(4000)
      ,@sopSummaryComments VARCHAR(4000)
      ,@isRecommENDed INT
      ,@currentVENDorName NVARCHAR(MAX)
      ,@pricingSummary NVARCHAR(MAX)
      ,@startDate NVARCHAR(MAX)
      ,@ENDDate NVARCHAR(MAX)
      ,@isAccountTerm VARCHAR(200)
      ,@mode VARCHAR(200)
      ,@volume NUMERIC(32, 16) )
AS 
BEGIN   
   
      DECLARE
            @SOPSummaryId INT
           ,@SOPId INT
           ,@sopDetailsId INT
           ,@SOPAccountTermId INT
           ,@termId INT
           ,@SOPTermId INT
           ,@SOPBidId INT  
   
      DECLARE
            @summitBidId INT
           ,@supplierCommentsId INT
           ,@supplierServiceId INT  
  
      DECLARE
            @newProductName VARCHAR(200)
           ,@newIsSubProduct BIT  
   
      DECLARE
            @newSummitBidId INT
           ,@newSupplierCommentsId INT
           ,@newSupplierServiceId INT  
   
      DECLARE @newBidId INT  
  
      DECLARE
            @fromMonth DATETIME
           ,@toMonth DATETIME
           ,@noOfMonths INT
           ,@is_sdp INT  
  
      DECLARE
            @SOP_TERM_ID INT
           ,@SOP_FROM_MONTH DATETIME
           ,@SOP_TO_MONTH DATETIME
           ,@SOP_NO_OF_MONTHS INT  
  
      DECLARE
            @RESET_TERM_ID INT
           ,@RESET_FROM_MONTH DATETIME
           ,@RESET_TO_MONTH DATETIME
           ,@RESET_NO_OF_MONTHS INT
           ,@Sr_Rfp_Sop_Details_Id INT
  
 -- SAVING NEW SOP  
  
      SELECT
            @SOPSummaryId = SR_RFP_SOP_SUMMARY_ID
      FROM
            dbo.SR_RFP_SOP_SUMMARY
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND IS_BID_GROUP = @isBidGroupId  
  
      SELECT
            @SOPId = SR_RFP_SOP_ID
      FROM
            dbo.SR_RFP_SOP
      WHERE
            SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
            AND VENDOR_NAME = @supplierContactMap
            AND SR_RFP_SOP_SUMMARY_ID = @SOPSummaryId  
  
      SELECT
            @sopDetailsId = SR_RFP_SOP_DETAILS_ID
      FROM
            dbo.SR_RFP_SOP_DETAILS
      WHERE
            SR_RFP_BID_ID = @supplierBOCId
            AND SR_RFP_SOP_ID = @SOPId  
   
      SELECT
            @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
           ,@supplierCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
           ,@supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID
           ,@newProductName = PRODUCT_NAME
           ,@newIsSubProduct = IS_SUB_PRODUCT
      FROM
            dbo.SR_RFP_BID
      WHERE
            SR_RFP_BID_ID = @supplierBOCId  
  
      IF @SOPSummaryId = ''
            OR @SOPSummaryId IS NULL 
            BEGIN  
  
                  INSERT      INTO dbo.SR_RFP_SOP_SUMMARY
                              ( 
                               SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,CURRENT_VENDOR_NAME
                              ,PRICING_SUMMARY
                              ,START_DATE
                              ,END_DATE
                              ,VOLUME )
                  VALUES
                              ( 
                               @accountGroupId
                              ,@isBidGroupId
                              ,@currentVENDorName
                              ,@pricingSummary
                              ,@startDate
                              ,@ENDDate
                              ,@volume )  
  
                  SELECT
                        @SOPSummaryId = scope_identity()  
  
            END  
      ELSE 
            BEGIN  
   
                  UPDATE
                        dbo.SR_RFP_SOP_SUMMARY
                  SET   
                        CURRENT_VENDOR_NAME = @currentVENDorName
                       ,PRICING_SUMMARY = @pricingSummary
                       ,START_DATE = @startDate
                       ,END_DATE = @ENDDate
                       ,VOLUME = @volume
                  WHERE
                        SR_RFP_SOP_SUMMARY_ID = @SOPSummaryId  
   
            END  
  
      IF @SOPId = ''
            OR @SOPId IS NULL 
            BEGIN  
  
                  INSERT      INTO dbo.SR_RFP_SOP
                              ( 
                               SR_RFP_SOP_SUMMARY_ID
                              ,SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              ,VENDOR_NAME
                              ,COMMENTS
                              ,IS_SAVED )
                  VALUES
                              ( 
                               @SOPSummaryId
                              ,@supplierContactId
                              ,@supplierContactMap
                              ,@sopSummaryComments
                              ,1 )  
  
                  SELECT
                        @SOPId = scope_identity()    
    
            END  
      ELSE 
            BEGIN  
  
                  UPDATE
                        dbo.SR_RFP_SOP
                  SET   
                        COMMENTS = @sopSummaryComments
                       ,IS_SAVED = 1
                  WHERE
                        SR_RFP_SOP_ID = @SOPId  
  
            END  
  
 -- insert new account terms FROM sr_rfp_account_term table  
      IF @isAccountTerm IS NOT NULL
            AND @isAccountTerm = 'IS_ACCOUNT_TERM' 
            BEGIN  
  
                  IF @mode = 'View_SOP' 
                        BEGIN  
  
                              SELECT
                                    @termId = SR_RFP_TERM_ID
                              FROM
                                    dbo.SR_RFP_ACCOUNT_TERM
                              WHERE
                                    SR_RFP_ACCOUNT_TERM_ID = @accountTermId
                                    AND is_sop = 1  
     
                              SELECT
                                    @is_sdp = is_sdp
                              FROM
                                    SR_RFP_ACCOUNT_TERM
                              WHERE
                                    sr_account_group_id = @accountGroupId
                                    AND is_bid_group = @isBidGroupId
                                    AND SR_RFP_ACCOUNT_TERM_ID = @accountTermId  
    
                              INSERT      INTO dbo.sr_rfp_account_term
                                          ( 
                                           SR_RFP_TERM_ID
                                          ,SR_ACCOUNT_GROUP_ID
                                          ,IS_BID_GROUP
                                          ,IS_SOP
                                          ,FROM_MONTH
                                          ,TO_MONTH
                                          ,NO_OF_MONTHS
                                          ,IS_SDP )
                                          SELECT
                                                SR_RFP_TERM_ID
                                               ,SR_ACCOUNT_GROUP_ID
                                               ,IS_BID_GROUP
                                               ,1 AS IS_SOP
                                               ,FROM_MONTH
                                               ,TO_MONTH
                                               ,NO_OF_MONTHS
                                               ,1 AS IS_SDP
                                          FROM
                                                dbo.sr_rfp_account_term
                                          WHERE
                                                sr_account_group_id = @accountGroupId
                                                AND is_bid_group = @isBidGroupId
                                                AND is_sop IS NULL  
  
                              SELECT
                                    @SOPAccountTermId = max(sr_rfp_account_term_id)
                              FROM
                                    dbo.sr_rfp_account_term
                              WHERE
                                    sr_rfp_account_term.SR_RFP_TERM_ID = @termId
                                    AND sr_rfp_account_term_id != @accountTermId
                                    AND is_sop = 1
                                    AND sr_account_group_id = @accountGroupId
                                    AND is_bid_group = @isBidGroupId  
  
  -- THIS REPLICATION WILL NOT BE USED IN SV, BUT JUST MAKE THE ACCOUNT_TERM TABLE SYNC WITH CBMS INTERNAL.  
                              SELECT TOP 1
                                    @SOP_TERM_ID = SR_RFP_TERM_ID
                                   ,@SOP_FROM_MONTH = FROM_MONTH
                                   ,@SOP_TO_MONTH = TO_MONTH
                                   ,@SOP_NO_OF_MONTHS = NO_OF_MONTHS
                              FROM
                                    dbo.SR_RFP_ACCOUNT_TERM
                              WHERE
                                    SR_ACCOUNT_GROUP_ID = @accountGroupId
                                    AND IS_BID_GROUP = @isBidGroupId
                                    AND IS_SOP IS NULL  
  
                        END  
                  ELSE 
                        IF @mode = 'Reset_SOP' 
                              BEGIN  
  
                                    SELECT
                                          @termId = SR_RFP_TERM_ID
                                    FROM
                                          dbo.SR_RFP_ACCOUNT_TERM
                                    WHERE
                                          SR_RFP_ACCOUNT_TERM_ID = @accountTermId
                                          AND is_sop IS NULL  
  
                                    SELECT
                                          @is_sdp = is_sdp
                                    FROM
                                          dbo.SR_RFP_ACCOUNT_TERM
                                    WHERE
                                          sr_account_group_id = @accountGroupId
                                          AND is_bid_group = @isBidGroupId
                                          AND SR_RFP_ACCOUNT_TERM_ID = @accountTermId  
    
                                    INSERT      INTO dbo.sr_rfp_account_term
                                                ( 
                                                 SR_RFP_TERM_ID
                                                ,SR_ACCOUNT_GROUP_ID
                                                ,IS_BID_GROUP
                                                ,IS_SOP
                                                ,FROM_MONTH
                                                ,TO_MONTH
                                                ,NO_OF_MONTHS
                                                ,IS_SDP )
                                                SELECT
                                                      SR_RFP_TERM_ID
                                                     ,SR_ACCOUNT_GROUP_ID
                                                     ,IS_BID_GROUP
                                                     ,1
                                                     ,FROM_MONTH
                                                     ,TO_MONTH
                                                     ,NO_OF_MONTHS
                                                     ,1
                                                FROM
                                                      dbo.sr_rfp_account_term
                                                WHERE
                                                      sr_account_group_id = @accountGroupId
                                                      AND is_bid_group = @isBidGroupId
                                                      AND is_sop IS NULL  
  
                                    SELECT
                                          @SOPAccountTermId = max(sr_rfp_account_term_id)
                                    FROM
                                          dbo.sr_rfp_account_term
                                    WHERE
                                          sr_rfp_account_term.SR_RFP_TERM_ID = @termId
                                          AND is_sop = 1
                                          AND sr_account_group_id = @accountGroupId
                                          AND is_bid_group = @isBidGroupId  

                                    SELECT TOP 1
                                          @RESET_TERM_ID = SR_RFP_TERM_ID
                                         ,@RESET_FROM_MONTH = FROM_MONTH
                                         ,@RESET_TO_MONTH = TO_MONTH
                                         ,@RESET_NO_OF_MONTHS = NO_OF_MONTHS
                                    FROM
                                          dbo.SR_RFP_ACCOUNT_TERM
                                    WHERE
                                          SR_ACCOUNT_GROUP_ID = @accountGroupId
                                          AND IS_BID_GROUP = @isBidGroupId
                                          AND IS_SOP IS NULL  
  
                              END    
            END  
      ELSE 
            IF @isAccountTerm IS NULL 
                  BEGIN  
  
                        IF @mode = 'View_SOP' 
                              BEGIN  
  
                                    SELECT
                                          @termId = SR_RFP_TERM_ID
                                    FROM
                                          SR_RFP_ACCOUNT_TERM
                                    WHERE
                                          SR_RFP_ACCOUNT_TERM_ID = @accountTermId
                                          AND is_sop = 1  
  
                                    SELECT
                                          @SOPAccountTermId = max(sr_rfp_account_term_id)
                                    FROM
                                          sr_rfp_account_term
                                    WHERE
                                          sr_rfp_account_term.SR_RFP_TERM_ID = @termId
                                          AND sr_rfp_account_term_id != @accountTermId
                                          AND is_sop = 1
                                          AND sr_account_group_id = @accountGroupId
                                          AND is_bid_group = @isBidGroupId  
  
                              END  
                        ELSE 
                              IF @mode = 'Reset_SOP' 
                                    BEGIN  
  
                                          SELECT
                                                @termId = SR_RFP_TERM_ID
                                          FROM
                                                dbo.SR_RFP_ACCOUNT_TERM
                                          WHERE
                                                SR_RFP_ACCOUNT_TERM_ID = @accountTermId
                                                AND is_sop IS NULL  
  
                                          SELECT
                                                @SOPAccountTermId = max(sr_rfp_account_term_id)
                                          FROM
                                                dbo.sr_rfp_account_term
                                          WHERE
                                                sr_rfp_account_term.SR_RFP_TERM_ID = @termId
                                                AND is_sop = 1
                                                AND sr_account_group_id = @accountGroupId
                                                AND is_bid_group = @isBidGroupId  
                                    END  
                  END  
  
 -- second insert INTo SR_RFP_BID_REQUIREMENTS table  
  
      INSERT      INTO dbo.SR_RFP_BID_REQUIREMENTS
                  ( 
                   DELIVERY_POINT
                  ,TRANSPORTATION_LEVEL_TYPE_ID
                  ,NOMINATION_TYPE_ID
                  ,BALANCING_TYPE_ID
                  ,COMMENTS
                  ,DELIVERY_POINT_POWER_TYPE_ID
                  ,SERVICE_LEVEL_POWER_TYPE_ID )
                  SELECT
                        DELIVERY_POINT
                       ,TRANSPORTATION_LEVEL_TYPE_ID
                       ,NOMINATION_TYPE_ID
                       ,BALANCING_TYPE_ID
                       ,COMMENTS
                       ,DELIVERY_POINT_POWER_TYPE_ID
                       ,SERVICE_LEVEL_POWER_TYPE_ID
                  FROM
                        dbo.SR_RFP_BID_REQUIREMENTS
                  WHERE
                        SR_RFP_BID_REQUIREMENTS_ID = @summitBidId  
  
      SELECT
            @newSummitBidId = scope_identity()  
  
 -- third insert INTo SR_RFP_SUPPLIER_PRICE_COMMENTS table  
      IF @supplierCommentsId != ''
            AND @supplierCommentsId > 0 
            BEGIN  
  
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              ( 
                               IS_CREDIT_APPROVAL
                              ,NO_CREDIT_COMMENTS
                              ,PRICE_RESPONSE_COMMENTS
                              ,Broker_Included_Type_Id
                              ,Pricing_Comments )
                              SELECT
                                    IS_CREDIT_APPROVAL
                                   ,NO_CREDIT_COMMENTS
                                   ,PRICE_RESPONSE_COMMENTS
                                   ,Broker_Included_Type_Id
                                   ,Pricing_Comments
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId  
  
                  SELECT
                        @newSupplierCommentsId = scope_identity() 
                        
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              ( 
                               PRICE_RESPONSE_COMMENTS
                              ,ARCHIVED_ON_DATE
                              ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                              ,Pricing_Comments )
                              SELECT
                                    PRICE_RESPONSE_COMMENTS
                                   ,ARCHIVED_ON_DATE
                                   ,@newSupplierCommentsId
                                   ,Pricing_Comments
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId  
  
            END  
  
 
  
      INSERT      INTO dbo.SR_RFP_BID
                  ( 
                   PRODUCT_NAME
                  ,SR_RFP_BID_REQUIREMENTS_ID
                  ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  ,SR_RFP_SUPPLIER_SERVICE_ID
                  ,IS_SUB_PRODUCT )
      VALUES
                  ( 
                   @newProductName
                  ,@newSummitBidId
                  ,@newSupplierCommentsId
                  ,NULL
                  ,@newIsSubProduct )  
  
      SELECT
            @newBidId = scope_identity()  
  
      INSERT      INTO dbo.SR_RFP_SOP_DETAILS
                  ( 
                   SR_RFP_SOP_ID
                  ,SR_RFP_ACCOUNT_TERM_ID
                  ,PRODUCT_NAME
                  ,SR_RFP_BID_ID
                  ,PRICE_RESPONSE_COMMENTS
                  ,IS_RECOMMENDED )
      VALUES
                  ( 
                   @SOPId
                  ,@SOPAccountTermId
                  ,@productName
                  ,@newBidId
                  ,@priceComments
                  ,@isRecommended )  
                  
      SELECT
            @Sr_Rfp_Sop_Details_Id = scope_identity()
               
      INSERT      INTO dbo.Sr_Rfp_Sop_Service_Condition_Response
                  ( 
                   Sr_Rfp_Sop_Details_Id
                  ,Sr_Service_Condition_Category_Id
                  ,Category_Display_Seq
                  ,Sr_Service_Condition_Question_Id
                  ,Question_Display_Seq
                  ,Response_Text_Value
                  ,Sr_Rfp_Service_Condition_Template_Question_Map_Id
                  ,Last_Change_Ts
                  ,Comment )
                  SELECT
                        @Sr_Rfp_Sop_Details_Id
                       ,scqm.Sr_Service_Condition_Category_Id
                       ,scqm.Category_Display_Seq
                       ,scqm.Sr_Service_Condition_Question_Id
                       ,scqm.Question_Display_Seq
                       ,tqr.Response_Text_Value
                       ,scqm.Sr_Rfp_Supplier_Service_Condition_Question_Map_Id
                       ,getdate()
                       ,tqr.Comment
                  FROM
                        dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map scqm
                        LEFT JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response tqr
                              ON scqm.Sr_Rfp_Bid_Id = tqr.SR_RFP_BID_ID
                                 AND scqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id = tqr.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                  WHERE
                        scqm.Sr_Rfp_Bid_Id = @supplierBOCId
  
END;

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_New_Save_Sop] TO [CBMSApplication]
GO
