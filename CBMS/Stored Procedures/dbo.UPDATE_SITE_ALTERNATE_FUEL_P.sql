SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.UPDATE_SITE_ALTERNATE_FUEL_P
	@isAlternatePower bit,
	@isAlternateGas bit,
	@cbmsImageID int,
	@siteID int
	AS
	begin
		set nocount on

		update 	site 
		set 	IS_ALTERNATE_POWER = @isAlternatePower,
			IS_ALTERNATE_GAS = @isAlternateGas,
			CBMS_IMAGE_ID = @cbmsImageID
		where 	site_id = @siteID

	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_SITE_ALTERNATE_FUEL_P] TO [CBMSApplication]
GO
