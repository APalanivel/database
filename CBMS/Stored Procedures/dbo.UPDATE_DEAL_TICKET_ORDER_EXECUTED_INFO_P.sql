SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.UPDATE_DEAL_TICKET_ORDER_EXECUTED_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@dealTicketId  	int       	          	
	@orderExecutedDate	datetime  	          	
	@confirmationTypeId	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    PROCEDURE DBO.UPDATE_DEAL_TICKET_ORDER_EXECUTED_INFO_P 

@userId varchar(10),
@sessionId varchar(20),
@dealTicketId int,
@orderExecutedDate datetime, 
@confirmationTypeId int 

AS
set nocount on
	IF @confirmationTypeId > 0
		BEGIN
			UPDATE 
				RM_DEAL_TICKET 
			SET 
				VERBAL_CONFIRMATION_BY = @userId, 
				VERBAL_CONFIRMATION_DATE = @orderExecutedDate, 
				CONFIRMATION_TYPE_ID = @confirmationTypeId
			WHERE
				RM_DEAL_TICKET_ID = @dealTicketId
		END
	ELSE
		BEGIN
			UPDATE 
				RM_DEAL_TICKET 
			SET 
				VERBAL_CONFIRMATION_BY = @userId, 
				VERBAL_CONFIRMATION_DATE = @orderExecutedDate
			WHERE
				RM_DEAL_TICKET_ID = @dealTicketId
		END
GO
GRANT EXECUTE ON  [dbo].[UPDATE_DEAL_TICKET_ORDER_EXECUTED_INFO_P] TO [CBMSApplication]
GO
