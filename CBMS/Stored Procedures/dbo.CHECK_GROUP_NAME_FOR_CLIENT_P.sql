SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.CHECK_GROUP_NAME_FOR_CLIENT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@clientId      	int       	          	
	@name          	varchar(4000)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
	RR			2018-08-13	Global Risk Management - Added optinal input @RM_GROUP_ID
******/

CREATE  PROCEDURE [dbo].[CHECK_GROUP_NAME_FOR_CLIENT_P]
      ( 
       @clientId INT
      ,@name VARCHAR(4000)
      ,@RM_GROUP_ID INT = NULL )
AS 
BEGIN
      SET NOCOUNT ON;
      
      SELECT
            COUNT(GROUP_NAME) AS Group_Cnt
      FROM
            dbo.RM_GROUP
      WHERE
            CLIENT_ID = @clientId
            AND GROUP_NAME = @name
            AND ( @RM_GROUP_ID IS NULL
                  OR RM_GROUP_ID <> @RM_GROUP_ID )
END

GO
GRANT EXECUTE ON  [dbo].[CHECK_GROUP_NAME_FOR_CLIENT_P] TO [CBMSApplication]
GO
