SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE    PROCEDURE [dbo].[cbmsCuInvoiceLabel_GetForCuInvoice]
	( @MyAccountId int
	, @cu_invoice_id int
	)
AS
BEGIN

	   select cl.cu_invoice_label_id
		, cl.cu_invoice_id
		, cl.account_number
		, cl.client_name
		, cl.city
		, cl.state_name
		, cl.vendor_name
	     from cu_invoice_label cl with (nolock)
	    where cl.cu_invoice_id = @cu_invoice_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceLabel_GetForCuInvoice] TO [CBMSApplication]
GO
