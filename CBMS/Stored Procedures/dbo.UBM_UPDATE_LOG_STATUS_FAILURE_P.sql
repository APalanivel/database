SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
 dbo.UBM_UPDATE_LOG_STATUS_FAILURE_P    

 DESCRIPTION:     
 INPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 @status		varchar(200)	null      
 @reason		varchar(500)	null      
 @masterLogId	int				null  
 @@detailedFailureReason varchar(max)	null 
                            
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
 USAGE EXAMPLES:    
------------------------------------------------------------    
  
    
 AUTHOR INITIALS:
 Initials	Name
 NK			Nageshwara Rao Kosuri
 
------------------------------------------------------------    
 MODIFICATIONS  Initials Date	Modification    
------------------------------------------------------------              
 NK		14-Sep-2009		Modified to update detail failure reason

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[UBM_UPDATE_LOG_STATUS_FAILURE_P]
	@status VARCHAR(200),
	@reason VARCHAR(500),
	@masterLogId INT,
	@detailedFailureReason VARCHAR(MAX)
AS

BEGIN

	SET NOCOUNT ON
	DECLARE @statusTypeId INT

	SELECT @statusTypeId = entity_id FROM dbo.Entity WHERE Entity_Name = @status AND Entity_Type = 656

	UPDATE
		dbo.ubm_batch_master_log
		SET status_type_id = @statusTypeId,
					batch_failure_reason = @reason,
					end_date = GETDATE(),
					detail_failure_reason = @detailedFailureReason
	WHERE
		ubm_batch_master_log_id = @masterLogId

END
GO
GRANT EXECUTE ON  [dbo].[UBM_UPDATE_LOG_STATUS_FAILURE_P] TO [CBMSApplication]
GO
