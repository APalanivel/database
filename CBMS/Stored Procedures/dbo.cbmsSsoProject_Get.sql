SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	dbo.cbmsSsoProject_Get

DESCRIPTION:  
	Used to get project from sso_project table based on project_id

INPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  
       
OUTPUT PARAMETERS:  
Name      DataType		Default		Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------

	EXEC dbo.cbmsSsoProject_Get 3620

AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
GB			Geetansu Behera  
CMH			Chad Hattabaugh   
HG			Hari
SKA			Shobhit Kr Agrawal

MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
GB						created
SKA		08/07/09		Modified as per coding standard

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[cbmsSsoProject_Get](
	@project_id INT	)
AS

BEGIN

	SET NOCOUNT ON

	SELECT prj.SSO_PROJECT_ID,
		prj.PROJECT_TITLE,
		prj.PROJECT_DESCRIPTION,
		prj.PROJECTED_END_DATE,
		prj.IS_URGENT,
		prj.COMMODITY_TYPE_ID,
		prj.PROJECT_OWNERSHIP_TYPE_ID,
		prj.PROJECT_STATUS_TYPE_ID,
		sta.ENTITY_NAME AS project_status_type,
		prj.PROJECT_CATEGORY_TYPE_ID,
		cat.ENTITY_NAME AS project_category_type,
		com.ENTITY_NAME AS commodity_type,
		sav.ENTITY_NAME AS project_ownership_type,
		prj.SUS_PROJ_SUBCATEGORY_CD,
		c.CODE_VALUE AS SUS_PROJ_SUBCATEGORY_NAME,
		prj.START_DATE 
	FROM dbo.SSO_PROJECT prj 
		LEFT OUTER JOIN dbo.ENTITY com 
			ON prj.COMMODITY_TYPE_ID = com.ENTITY_ID  
		INNER JOIN dbo.ENTITY sav 
			ON prj.PROJECT_OWNERSHIP_TYPE_ID = sav.ENTITY_ID  
		INNER JOIN dbo.ENTITY sta 
			ON prj.PROJECT_STATUS_TYPE_ID = sta.ENTITY_ID
		INNER JOIN dbo.ENTITY cat 
			ON prj.PROJECT_CATEGORY_TYPE_ID = cat.ENTITY_ID
		LEFT OUTER JOIN dbo.CODE c 
			ON c.CODE_ID = prj.SUS_PROJ_SUBCATEGORY_CD
	WHERE prj.SSO_PROJECT_ID = @project_id 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProject_Get] TO [CBMSApplication]
GO
