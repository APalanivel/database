SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******      
NAME:    [Workflow].[Get_Assigned_Queue_ID]  
DESCRIPTION:  

Suppose invoice with certain Ubm_account_Code is assigned to a user and later one more invoice comes in system. Today it goes to public queue, we need to map it to same user who has earlier invoice. 

Please create one more SP which would have current_invoice_id as a input parameter. SP needs to check if this invoice is part of any system group logic if yes then find out if there is any invoice available in system with matching creteria then send user queue_id. If there are more than 1 user having such invoice then send me the latest user?s queue_id who received invoice.

Once this is implemented then system group would not have mistmatch records. 

------------------------------------------------------------     
 INPUT PARAMETERS:      
 Name   DataType  Default Description      
  
------------------------------------------------------------      
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:      
 EXEC [Workflow].[Get_Assigned_Queue_ID]  
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
TRK   Ramakrishna Thummala Summit Energy 
AP	Arunkumar Palanivel  
   
 MODIFICATIONS       
 Initials Date   Modification      
------------------------------------------------------------      
 AP		Created new procedure to get queue id 
  
******/

CREATE PROCEDURE [Workflow].[Get_Assigned_Queue_ID]
      (
      @Invoice_id INT
     )
AS
      BEGIN



            DECLARE
                  @Ubm_id           INT
                , @ubm_account_code VARCHAR(200)
                , @PROC_NAME        VARCHAR(100) = 'Get_Assigned_Queue_ID'
                , @INPUT_PARAMS     VARCHAR(1000)
                , @ERROR_LINE       INT
                , @ERROR_MESSAGE    VARCHAR(3000),
				@Queue_ID int

            BEGIN TRY

                  SELECT
                        @Ubm_id = UBM_ID
                      , @ubm_account_code = UBM_ACCOUNT_CODE
                  FROM  dbo.CU_INVOICE
                  WHERE CU_INVOICE_ID = @Invoice_id;

                  SELECT      TOP (1)
                              @Queue_ID = CUEX.QUEUE_ID
                  FROM        dbo.CU_INVOICE CU
                              JOIN
                              dbo.CU_EXCEPTION_DENORM CUEX
                                    ON CU.CU_INVOICE_ID = CUEX.CU_INVOICE_ID
                              JOIN
                              dbo.QUEUE Q
                                    ON Q.QUEUE_ID = CUEX.QUEUE_ID
                              JOIN
                              dbo.ENTITY E
                                    ON E.ENTITY_ID = Q.QUEUE_TYPE_ID
                  WHERE       CU.UBM_ID = @Ubm_id
                              AND   CU.UBM_ACCOUNT_CODE = @ubm_account_code
                              AND   CUEX.CU_INVOICE_ID <> @Invoice_id
                              AND   E.ENTITY_NAME = 'Private'
                              AND   CUEX.EXCEPTION_TYPE = 'UNKNOWN ACCOUNT'
                  ORDER BY    CUEX.CU_INVOICE_ID DESC;


				  SELECT @Queue_ID AS Queue_ID 

            END TRY
            BEGIN CATCH


                  -- Entry made to the logging SP to capture the errors.                      
                  SELECT
                        @ERROR_LINE = error_line()
                      , @ERROR_MESSAGE = error_message();

                  INSERT INTO DBO.StoredProc_Error_Log (
                                                         StoredProc_Name
                                                       , Error_Line
                                                       , Error_message
                                                       , Input_Params
                                                   )
                  VALUES
                       ( @PROC_NAME, @ERROR_LINE, @ERROR_MESSAGE, @INPUT_PARAMS );

                  EXEC [dbo].[usp_RethrowError]
                        @ERROR_MESSAGE;

            END CATCH;
      END;
GO
GRANT EXECUTE ON  [Workflow].[Get_Assigned_Queue_ID] TO [CBMSApplication]
GO
