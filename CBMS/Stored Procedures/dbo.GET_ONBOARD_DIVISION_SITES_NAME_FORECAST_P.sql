SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[GET_ONBOARD_DIVISION_SITES_NAME_FORECAST_P]  
     
DESCRIPTION:

      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@userId			VARCHAR
@sessionId		VARCHAR
@clientId		INT                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
   

EXEC dbo.GET_ONBOARD_DIVISION_SITES_NAME_FORECAST_P 1,1,235

EXEC dbo.GET_ONBOARD_DIVISION_SITES_NAME_FORECAST_P 1,1,10069

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------ 
HG			Harihara Suthan G
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
HG			10/18/2010	Comments header added with the default SET statements
						As the pagination changes breaking the existing functionality reverted the changes pre Pagination version.
						Client, Division, Site, VwSitename tables replaced by Client_Hier table

*/

CREATE PROCEDURE dbo.GET_ONBOARD_DIVISION_SITES_NAME_FORECAST_P
	@userId		VARCHAR
	,@sessionId	VARCHAR
	,@clientId	INT
AS
BEGIN

	SET NOCOUNT ON
	DECLARE @Hedge_Type_Id INT

	SELECT
		@Hedge_Type_Id = entity_id
	FROM
		dbo.Entity 
	WHERE
		ENTITY_DESCRIPTION = 'HEDGE_TYPE'
		AND ENTITY_NAME = 'Does not hedge'

	SELECT
		ch.site_Id
		,RTRIM(ch.city) + ', ' + ch.State_name + ' (' + ch.site_name + ')' site_name
		,ch.Sitegroup_Id division_id
		,ch.Sitegroup_Name division_name
	FROM
		core.Client_Hier ch
		INNER JOIN RM_ONBOARD_HEDGE_SETUP setup
			ON ch.Site_Id = setup.SITE_ID
	WHERE
		ch.Client_Id  = @clientId
		AND ( setup.volume_units_type_id <> @Hedge_Type_Id )
	ORDER BY
		Site_name

END
GO
GRANT EXECUTE ON  [dbo].[GET_ONBOARD_DIVISION_SITES_NAME_FORECAST_P] TO [CBMSApplication]
GO
