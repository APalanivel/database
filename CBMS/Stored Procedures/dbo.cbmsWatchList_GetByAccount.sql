SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   PROCEDURE [dbo].[cbmsWatchList_GetByAccount]
	( @MyAccountId int
	, @group_info_id int
	, @account_id int
	)
AS
BEGIN
	set nocount on
	   select w.watch_list_id
		, w.user_info_id
		, ui.queue_id
		, w.account_id
		, w.comments
	     from watch_list w
	     join user_info ui on ui.user_info_id = w.user_info_id
	    where w.account_id = @account_id
--	      and w.group_info_id = @group_info_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsWatchList_GetByAccount] TO [CBMSApplication]
GO
