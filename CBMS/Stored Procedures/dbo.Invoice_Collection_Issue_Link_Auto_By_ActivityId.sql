SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Issue_Link_Auto_By_ActivityId           
                  
Description:                  
            
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                           
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------     
    
 Usage Examples:                      
----------------------------------------------------------------------------------------      
     
 Select * from Invoice_Collection_Issue_Log where invoice_collection_    
  select * from Invoice_Collection_Issue_Event    
   
 declare @User_Info_Id int=17    
    
 Exec dbo.Invoice_Collection_Issue_Link_Auto_By_ActivityId      
        44653,    
  @User_Info_Id    
    
  Select * from Invoice_Collection_Issue_Log where     
  select top 100 * from Invoice_Collection_Issue_Event order by 1 desc   
      
    
    
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 SLP    Sri Lakshmi Pallikonda    
    
 Modifications:                  
    Initials        Date		Modification                  
----------------------------------------------------------------------------------------                    
    SLP				2019-09-18  Link the issues automatically 
	RKV             2019-10-07  Added Next Action Date
	SLP				2019-11-11  Included the logic to use System user info id when user name is 0 / null
	SLP				2019-11-27	MAINT-9485, Auto linked issues must be closed if the Collection_End_Dt<Issue_Link_Start_Date
	RKV             2020-03-27  IC-Toolrevamp add functional to lock the ICQ when we are adding to the issue.
    
**/

CREATE PROCEDURE [dbo].[Invoice_Collection_Issue_Link_Auto_By_ActivityId]
     (
         @Invoice_Collection_Activity_Id INT
         , @User_Info_Id INT
     )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Internal_Issue_event_Type_Cd INT
            , @Issue_Status_Cd_Close INT;

        DECLARE @Invoice_Collection_Issue_Log_Ids TABLE
              (
                  Invoice_Collection_Issue_Log_Id INT
              );


        DECLARE @Invoice_Collection_Account_Config_Ids TABLE
              (
                  Invoice_Collection_Activity_Id INT
                  , Invoice_Collection_Account_Config_Id INT
              );
        CREATE TABLE #Issues_to_Link_By_Activity_Ids
             (
                 Invoice_Collection_Activity_Id INT
                 , Invoice_Collection_Issue_Log_Id INT
             );

        CREATE TABLE #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id INT
                 , Collection_Start_Dt DATE
                 , Collection_End_Dt DATE
                 , Invoice_Collection_Activity_Id INT
             );

        CREATE TABLE #Invoice_Collection_Issue_Log_ids_For_Closing
             (
                 Invoice_Collection_Issue_Log_Id INT
             );

        IF (   @User_Info_Id = 0
               OR   @User_Info_Id IS NULL)
            SELECT
                @User_Info_Id = USER_INFO_ID
            FROM
                dbo.USER_INFO ui
            WHERE
                ui.FIRST_NAME = 'System'
                AND ui.LAST_NAME = 'Conversion';


        SELECT
            @Issue_Status_Cd_Close = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'IC Chase Status'
            AND c.Code_Value = 'Close';

        SELECT
            @Internal_Issue_event_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Std_Column_Name = 'Invoice_Collection_Issue_Event_Type_Cd'
            AND c.Code_Value = 'IC Internal Comments';


        /*Collection the Original Issue If exists or the Latest Issue to be linked for the Activitiy_ID */

        INSERT INTO #Issues_to_Link_By_Activity_Ids
             (
                 Invoice_Collection_Activity_Id
                 , Invoice_Collection_Issue_Log_Id
             )
        SELECT
            icil.Invoice_Collection_Activity_Id
            , MAX(icil.Invoice_Collection_Issue_Log_Id)
        FROM
            dbo.Invoice_Collection_Issue_Log icil
        WHERE
            icil.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
            AND icil.Created_Ts = icil.Last_Change_Ts
            AND icil.Is_Auto_Linked = 0
        GROUP BY
            icil.Invoice_Collection_Activity_Id;

        INSERT INTO #Issues_to_Link_By_Activity_Ids
             (
                 Invoice_Collection_Activity_Id
                 , Invoice_Collection_Issue_Log_Id
             )
        SELECT
            icil1.Invoice_Collection_Activity_Id
            , MAX(icil1.Invoice_Collection_Issue_Log_Id)
        FROM
            dbo.Invoice_Collection_Issue_Log icil1
            INNER JOIN (   SELECT
                                MAX(icil.Last_Change_Ts) Last_Change_Ts
                                , icil.Invoice_Collection_Activity_Id
                           FROM
                                dbo.Invoice_Collection_Issue_Log icil
                           WHERE
                                icil.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
                           GROUP BY
                               icil.Invoice_Collection_Activity_Id) icil2
                ON icil1.Invoice_Collection_Activity_Id = icil2.Invoice_Collection_Activity_Id
                   AND  icil1.Last_Change_Ts = icil2.Last_Change_Ts
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                #Issues_to_Link_By_Activity_Ids itlbai
                           WHERE
                                itlbai.Invoice_Collection_Activity_Id = icil1.Invoice_Collection_Activity_Id)
        GROUP BY
            icil1.Invoice_Collection_Activity_Id;


        /* Collecting the Invoice_Collection_Config_Ids Involved in the given Activity_Id's*/

        INSERT INTO @Invoice_Collection_Account_Config_Ids
             (
                 Invoice_Collection_Activity_Id
                 , Invoice_Collection_Account_Config_Id
             )
        SELECT
            icil.Invoice_Collection_Activity_Id
            , icac.Invoice_Collection_Account_Config_Id
        FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Invoice_Collection_Queue icq
                ON icq.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                ON icil.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN Code isc
                ON icil.Issue_Status_Cd = isc.Code_Id
        WHERE
            isc.Code_Value = 'Open'
            AND icil.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
        GROUP BY
            icil.Invoice_Collection_Activity_Id
            , icac.Invoice_Collection_Account_Config_Id;



        /* Collecting the Chase periods which are greater than Auto link of that activity*/

        INSERT INTO #Invoice_Collection_Queue_Ids
             (
                 Invoice_Collection_Queue_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Activity_Id
             )
        SELECT
            icq.Invoice_Collection_Queue_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , ica.Invoice_Collection_Activity_Id
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN @Invoice_Collection_Account_Config_Ids icaci
                ON icaci.Invoice_Collection_Account_Config_Id = icq.Invoice_Collection_Account_Config_Id
            INNER JOIN dbo.Invoice_Collection_Activity ica
                ON ica.Invoice_Collection_Activity_Id = icaci.Invoice_Collection_Activity_Id
        WHERE
            (   (ica.Issue_Link_Start_Date BETWEEN icq.Collection_Start_Dt
                                           AND     icq.Collection_End_Dt)
                OR  icq.Collection_Start_Dt > ica.Issue_Link_Start_Date)
        GROUP BY
            icq.Invoice_Collection_Queue_Id
            , icq.Collection_Start_Dt
            , icq.Collection_End_Dt
            , ica.Invoice_Collection_Activity_Id;


        INSERT INTO dbo.Invoice_Collection_Issue_Log
             (
                 Invoice_Collection_Activity_Id
                 , Invoice_Collection_Queue_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
                 , Invoice_Collection_Issue_Type_Cd
                 , Issue_Status_Cd
                 , Issue_Entity_Owner_Cd
                 , Issue_Owner_User_Id
                 , Created_User_Id
                 , Updated_User_Id
                 , Type_Of_Issue_Owner_Cd
                 , Is_Blocker
                 , Blocker_Action_Date
                 , Is_Auto_Linked
             )
        OUTPUT
            Inserted.Invoice_Collection_Issue_Log_Id
        INTO @Invoice_Collection_Issue_Log_Ids
        SELECT
            icqi.Invoice_Collection_Activity_Id
            , icqi.Invoice_Collection_Queue_Id
            , icqi.Collection_Start_Dt
            , icqi.Collection_End_Dt
            , icil.Invoice_Collection_Issue_Type_Cd
            , icil.Issue_Status_Cd
            , icil.Issue_Entity_Owner_Cd
            , icil.Issue_Owner_User_Id
            , @User_Info_Id Created_User_Id
            , @User_Info_Id Updated_User_Id
            , icil.Type_Of_Issue_Owner_Cd
            , icil.Is_Blocker
            , icil.Blocker_Action_Date
            , 1 Is_Auto_Linked
        FROM
            #Invoice_Collection_Queue_Ids icqi
            INNER JOIN #Issues_to_Link_By_Activity_Ids itlbai
                ON itlbai.Invoice_Collection_Activity_Id = icqi.Invoice_Collection_Activity_Id
            INNER JOIN dbo.Invoice_Collection_Issue_Log icil
                ON icil.Invoice_Collection_Issue_Log_Id = itlbai.Invoice_Collection_Issue_Log_Id
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Invoice_Collection_Issue_Log icil
                           WHERE
                                icil.Invoice_Collection_Activity_Id = icqi.Invoice_Collection_Activity_Id
                                AND icil.Invoice_Collection_Queue_Id = icqi.Invoice_Collection_Queue_Id);



        INSERT INTO dbo.Invoice_Collection_Issue_Event
             (
                 Invoice_Collection_Issue_Log_Id
                 , Issue_Event_Type_Cd
                 , Event_Desc
                 , Created_User_Id
                 , Updated_User_Id
             )
        SELECT
            icili.Invoice_Collection_Issue_Log_Id
            , @Internal_Issue_event_Type_Cd
            , 'Issue copied on ' + CONVERT(VARCHAR(11), GETDATE(), 1) + ' by System'
            , @User_Info_Id
            , @User_Info_Id
        FROM
            @Invoice_Collection_Issue_Log_Ids icili;


        INSERT INTO dbo.Invoice_Collection_Issue_Event
             (
                 Invoice_Collection_Issue_Log_Id
                 , Issue_Event_Type_Cd
                 , Event_Desc
                 , Created_User_Id
                 , Updated_User_Id
             )
        SELECT
            icili.Invoice_Collection_Issue_Log_Id
            , icie.Issue_Event_Type_Cd
            , icie.Event_Desc
            , @User_Info_Id
            , @User_Info_Id
        FROM
            dbo.Invoice_Collection_Issue_Event icie
            INNER JOIN #Issues_to_Link_By_Activity_Ids itlbai
                ON itlbai.Invoice_Collection_Issue_Log_Id = icie.Invoice_Collection_Issue_Log_Id
            CROSS JOIN @Invoice_Collection_Issue_Log_Ids icili;


        INSERT INTO dbo.Invoice_Collection_Issue_Attachment
             (
                 Invoice_Collection_Issue_Log_Id
                 , Cbms_Image_Id
                 , Created_User_Id
             )
        SELECT
            icili.Invoice_Collection_Issue_Log_Id
            , icia.Cbms_Image_Id
            , @User_Info_Id
        FROM
            dbo.Invoice_Collection_Issue_Attachment icia
            INNER JOIN #Issues_to_Link_By_Activity_Ids itlbai
                ON itlbai.Invoice_Collection_Issue_Log_Id = icia.Invoice_Collection_Issue_Log_Id
            CROSS JOIN @Invoice_Collection_Issue_Log_Ids icili;



        INSERT INTO #Invoice_Collection_Issue_Log_ids_For_Closing
             (
                 Invoice_Collection_Issue_Log_Id
             )
        SELECT
            icil.Invoice_Collection_Issue_Log_Id
        FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN dbo.Invoice_Collection_Activity ica
                ON ica.Invoice_Collection_Activity_Id = icil.Invoice_Collection_Activity_Id
        WHERE
            icil.Collection_End_Dt < ica.Issue_Link_Start_Date
            AND icil.Is_Auto_Linked = 1
            AND ica.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;


        /*Next Action Date copy*/
        INSERT INTO dbo.Invoice_Collection_Activity_Log_Queue_Map
             (
                 Invoice_Collection_Activity_Log_Id
                 , Invoice_Collection_Queue_Id
                 , Collection_Start_Dt
                 , Collection_End_Dt
             )
        SELECT
            ical.Invoice_Collection_Activity_Log_Id
            , icqi.Invoice_Collection_Queue_Id
            , icqi.Collection_Start_Dt
            , icqi.Collection_End_Dt
        FROM
            dbo.Invoice_Collection_Activity_Log ical
            INNER JOIN #Invoice_Collection_Queue_Ids icqi
                ON icqi.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
        WHERE
            ical.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    dbo.Invoice_Collection_Activity_Log_Queue_Map icalq
                               WHERE
                                    icalq.Invoice_Collection_Queue_Id = icqi.Invoice_Collection_Queue_Id
                                    AND icalq.Invoice_Collection_Activity_Log_Id = ical.Invoice_Collection_Activity_Log_Id);


        UPDATE
            icq
        SET
            icq.Next_Action_Dt = ical2.Next_Action_Dt
        FROM
            dbo.Invoice_Collection_Queue icq
            INNER JOIN #Invoice_Collection_Queue_Ids icqi
                ON icqi.Invoice_Collection_Queue_Id = icq.Invoice_Collection_Queue_Id
            INNER JOIN dbo.Invoice_Collection_Activity_Log ical2
                ON ical2.Invoice_Collection_Activity_Id = icqi.Invoice_Collection_Activity_Id
            INNER JOIN (   SELECT
                                icqi.Invoice_Collection_Activity_Id
                                , MAX(ical.Created_Ts) Created_Ts
                           FROM
                                dbo.Invoice_Collection_Activity_Log ical
                                INNER JOIN #Invoice_Collection_Queue_Ids icqi
                                    ON icqi.Invoice_Collection_Activity_Id = ical.Invoice_Collection_Activity_Id
                           GROUP BY
                               icqi.Invoice_Collection_Activity_Id) Nad_Lts
                ON Nad_Lts.Created_Ts = ical2.Created_Ts;


        UPDATE
            icil
        SET
            Issue_Status_Cd = @Issue_Status_Cd_Close
        FROM
            dbo.Invoice_Collection_Issue_Log icil
            INNER JOIN #Invoice_Collection_Issue_Log_ids_For_Closing icilifc
                ON icilifc.Invoice_Collection_Issue_Log_Id = icil.Invoice_Collection_Issue_Log_Id
        WHERE
            icil.Invoice_Collection_Activity_Id = @Invoice_Collection_Activity_Id;

        UPDATE
            icq2
        SET
            Last_Change_Ts = GETDATE()
			,icq2.Is_Locked = 1
        FROM
            dbo.Invoice_Collection_Queue icq2
            INNER JOIN #Invoice_Collection_Queue_Ids icqi
                ON icqi.Invoice_Collection_Queue_Id = icq2.Invoice_Collection_Queue_Id;


        DROP TABLE
            #Issues_to_Link_By_Activity_Ids
            , #Invoice_Collection_Queue_Ids;

    END;








--GO





GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Issue_Link_Auto_By_ActivityId] TO [CBMSApplication]
GO
