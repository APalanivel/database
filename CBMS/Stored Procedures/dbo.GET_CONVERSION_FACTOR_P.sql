SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--select * from RM_CURRENCY_UNIT_CONVERSION where client_id=228



CREATE PROCEDURE dbo.GET_CONVERSION_FACTOR_P

@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@year int --added by Ravi Datt


as
	set nocount on
if @year=0

select CONVERSION_FACTOR from RM_CURRENCY_UNIT_CONVERSION
 where client_id=@clientId and conversion_year=datepart(yyyy,getdate())

else

select CONVERSION_FACTOR from RM_CURRENCY_UNIT_CONVERSION
 where client_id=@clientId  and conversion_year=@year
GO
GRANT EXECUTE ON  [dbo].[GET_CONVERSION_FACTOR_P] TO [CBMSApplication]
GO
