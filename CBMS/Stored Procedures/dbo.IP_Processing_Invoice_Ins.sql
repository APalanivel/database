SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******  
NAME:  
  dbo.IP_Processing_Invoice_Ins  
  
DESCRIPTION:  
  
  
INPUT PARAMETERS:  
 Name    DataType      Default Description  
------------------------------------------------------------  
  @Tvp_IP_Processing_Invoice    Tvp_IP_Processing_Invoice                 
  
OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  
USAGE EXAMPLES:  
------------------------------------------------------------  
  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 SP         SRINIVASARAO PATCHAVA  
  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 SP         16/10/2018   CREATED  
  
******/  
CREATE PROCEDURE [dbo].[IP_Processing_Invoice_Ins]  
    (  
        @Tvp_IP_Processing_Invoice Tvp_IP_Processing_Invoice READONLY  
        , @User_Info_ID INT  
    )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
        INSERT INTO dbo.IP_Processing_Invoice  
             (  
                 Processing_Cu_Invoice_Id  
                 , Cu_Invoice_Id  
                 , Account_Id  
                 , Service_Month  
                 , Is_Reported  
                 , User_Info_ID  
                 , Last_Updated_Ts  
                 , Is_Previously_Reported  
             )  
        SELECT  
            tvp.Processing_Cu_Invoice_Id  
            , tvp.Cu_Invoice_Id  
            , tvp.Account_Id  
            , tvp.Service_Month  
            , tvp.Is_Reported  
            , @User_Info_ID  
            , GETDATE()  
            , tvp.Is_Previously_Reported  
        FROM  
            @Tvp_IP_Processing_Invoice tvp;  
  
    END;  
  
GO
GRANT EXECUTE ON  [dbo].[IP_Processing_Invoice_Ins] TO [CBMSApplication]
GO
