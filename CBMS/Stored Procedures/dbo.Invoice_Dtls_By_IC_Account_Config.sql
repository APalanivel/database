SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.Invoice_Dtls_By_IC_Account_Config         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                         
   
 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
      EXEC dbo.Invoice_Dtls_By_IC_Account_Config 1686

Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date		Modification                
--------------------------------------------------------------------------------------                  
    RKV				2017-02-03  Created For Invoice_Collection.           
    HG		        2017-04-18	PRSUPPORT- 582 , Modified the query to look at Total Usage / Volume bucket to know if the usage is 0 as other service total usage bucket name is Volume.   
******/   
CREATE PROCEDURE [dbo].[Invoice_Dtls_By_IC_Account_Config]
      (
       @Invoice_Collection_Account_Config_Id INT )
AS
BEGIN

      SET NOCOUNT ON;

      SELECT
            cuad.Service_Month
           ,MAX(CASE WHEN bm.Bucket_Name = 'Total cost' THEN cuad.Bucket_Value
                END) Total_cost
           ,MAX(CASE WHEN bm.Bucket_Name IN ( 'Total Usage', 'Volume' ) THEN cuad.Bucket_Value
                END) Total_Usage
           ,MAX(aicm.Account_Invoice_Collection_Month_Id) Account_Invoice_Collection_Month_Id
           ,MIN(cism.Begin_Dt) Invoice_Begin_Dt
           ,MAX(cism.End_Dt) Invoice_End_Dt
           ,c.Commodity_Id
           ,c.Commodity_Name
      FROM
            dbo.Invoice_Collection_Account_Config icac
            INNER JOIN dbo.Cost_Usage_Account_Dtl cuad
                  ON icac.Account_Id = cuad.ACCOUNT_ID
            INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cuad.ACCOUNT_ID = cism.Account_ID
                     AND cuad.Service_Month = cism.SERVICE_MONTH
            INNER JOIN dbo.Bucket_Master bm
                  ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
            INNER JOIN dbo.Commodity c
                  ON c.Commodity_Id = bm.Commodity_Id
            INNER JOIN dbo.Account_Invoice_Collection_Month aicm
                  ON aicm.Invoice_Collection_Account_Config_Id = icac.Invoice_Collection_Account_Config_Id
                     AND aicm.Service_Month = cuad.Service_Month
                     AND aicm.Seq_No = 0
      WHERE
            icac.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND cuad.Service_Month BETWEEN icac.Invoice_Collection_Service_Start_Dt
                                   AND     Invoice_Collection_Service_End_Dt
      GROUP BY
            cuad.Service_Month
           ,c.Commodity_Id
           ,c.Commodity_Name

END;


;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Dtls_By_IC_Account_Config] TO [CBMSApplication]
GO
