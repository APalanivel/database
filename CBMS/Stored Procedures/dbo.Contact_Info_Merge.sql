SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Contact_Info_Merge            
                        
 DESCRIPTION:                        
			To Update and insert data for Contact_Info table                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@tvp_Client_Info				 tvp_Client_Info READONLY
@User_Info_Id					 INT 
                       
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
 
 
 --CLIENT --INSERT UPDATE DELETE*****************************************************************************************************************************
BEGIN TRAN
 --INSERT
 DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( NULL, 'Kurt K. 1','Larsen','kurt k. 1@gmail.com',NULL,NULL,NULL,NULL,NULL,'Comment 1',1,100157,'Insert' )
            ,( NULL, 'Thomas 1','Plenborg','Thomas Plenborg 1@gmail.com',NULL,NULL,NULL,NULL,NULL,'Comment 2',1,100157,'Insert' )
            ,( NULL,'Annette 1','Sadolin','Annette Sadolin 1@gmail.com',NULL,NULL,NULL,NULL,NULL,'Comment 3',1,100157,'Insert' )
             ,( NULL, 'Birgit W. 1','Norgaard','Birgit W. Norgaard 1@gmail.com',NULL,NULL,NULL,NULL,NULL,'Comment 4',1,100157,'Insert' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact_Map where client_id=14526
      select * from vendor_contact_Map where VENDOR_ID=14526
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102254
      ,@Client_Id=14526
      --,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact_Map where client_id=14526
      select * from vendor_contact_Map where VENDOR_ID=14526
ROLLBACK      
--------------   

         

--UPDATE
 DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( 291, 'Kurt K.','Larsen','kurt k.@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Update' )
            ,( NULL, 'Jorgen','Moller','Jorgen Moller@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact where client_id=14526
      select * from vendor_contact where VENDOR_ID=14526
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102254
      ,@Client_Id=14526
      --,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact_Map where client_id=14526
      select * from vendor_contact_Map where VENDOR_ID=14526
--------------   

 
 --DELETE
   
  DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( 291, 'Kurt K.','Larsen','kurt k.@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Update' )
            ,( 292, 'Jorgen','Moller','Jorgen Moller@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' )
             ,( 297, 'Jorgen','Moller','Jorgen Moller@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Delete' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact_Map where client_id=14526
      select * from vendor_contact_Map where VENDOR_ID=14526
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102254
      ,@Client_Id=14526
      --,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM client_contact_Map WHERE client_id = 14526 )
      select * from client_contact_Map where client_id=14526
      select * from vendor_contact_Map where VENDOR_ID=14526
 

 --VENDOR --INSERT UPDATE DELETE*****************************************************************************************************************************



--INSERT
 DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( NULL, 'Carsten','Trolle','Carsten Trolle@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' )
            ,( NULL, 'Soren','Schmidt','Soren Schmidt@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' )
            ,( NULL,'Brian','Ejsing','Brian Ejsing@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' )
             ,( NULL, 'Keith','Pienaar','Keith Pienaar@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
     
      select * from vendor_contact_Map where VENDOR_ID=29960
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102257
      --,@Client_Id=14526
      ,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
           
      select * from vendor_contact_Map where VENDOR_ID=29960
--------------   
--UPDATE
 DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( 300, 'Kurt K.','Larsen','kurt k.@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Update' )
            ,( NULL, 'Jesper','Riis','Jesper Riis@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
           
      select * from vendor_contact_Map where VENDOR_ID=29960
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102257
      --,@Client_Id=14526
      ,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
           
      select * from vendor_contact_Map where VENDOR_ID=29960
--------------   
 --DELETE
   
  DECLARE @tvp_Client_Info tvp_Client_Info;  
  
INSERT      INTO @tvp_Client_Info
(
[Contact_Info_Id] ,
[First_Name] ,
[Last_Name] ,
[Email_Address] ,
[Phone_Number] ,
[Fax_Number] ,
[Job_Position] ,
[Location] ,
[Comment_Id],
[Comment_Text] ,
[Is_Active] ,
Language_Cd,
Operation_Type
)
VALUES
            ( 300, 'Kurt K.','Larsen','kurt k.@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Update' )
            ,( 301, 'Jorgen','Moller','Jorgen Moller@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Insert' )
             ,( 302, 'Jorgen','Moller','Jorgen Moller@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,1,100157,'Delete' );  

--------------
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
           
      select * from vendor_contact_Map where VENDOR_ID=29960
      
 EXEC [dbo].[Contact_Info_Merge] 
       @User_Info_Id =49
      ,@tvp_Client_Info=@tvp_Client_Info
      ,@Contact_Type_Cd=102256
      ,@Contact_Level_Cd=102257
      --,@Client_Id=14526
      ,@Vendor_Id=29960
      SELECT * FROM dbo.Contact_Info WHERE Contact_Level_Cd = 102254 AND Contact_Info_Id IN ( SELECT  Contact_Info_Id  FROM vendor_contact_Map WHERE VENDOR_ID=29960 )
           
      select * from vendor_contact_Map where VENDOR_ID=29960
      

                        
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-17       Created for Invoice Tracking Phase I.               
                       
******/                 
CREATE  PROCEDURE [dbo].[Contact_Info_Merge]
      (
       @tvp_Client_Info tvp_Client_Info READONLY
      ,@Contact_Type_Cd INT
      ,@Contact_Level_Cd INT
      ,@User_Info_Id INT
      ,@Client_Id INT = NULL
      ,@Vendor_Id INT = NULL )
AS
BEGIN                
      SET NOCOUNT ON;    
    
      DECLARE
            @Comment_Type_Cd INT
           ,@Comment_Text VARCHAR(MAX)
           ,@comment_Id INT
           ,@MinId INT
           ,@MaxId INT;

      DECLARE @Contact_Info_Tbl TABLE ( Contact_Info_Id INT );
      CREATE TABLE #Inserted_Contact_Info_Ids ( Contact_Info_Id INT )

      DECLARE @Tvp_Contact_Info_Tbl TABLE
            (
             [Contact_Info_Id] [INT]
            ,[First_Name] [NVARCHAR](60)
            ,[Last_Name] [NVARCHAR](60)
            ,[Email_Address] [NVARCHAR](150)
            ,[Phone_Number] [NVARCHAR](60)
            ,[Fax_Number] [NVARCHAR](60)
            ,[Job_Position] [NVARCHAR](255)
            ,[Location] [NVARCHAR](MAX)
            ,[Comment_Text] [VARCHAR](MAX)
            ,[Comment_Id] [INT]
            ,[Is_Active] [BIT]
            ,[Language_Cd] [INT]
            ,[Operation_Type] VARCHAR(10)
            ,Id INT IDENTITY(1, 1) );
     
--Comments section insert update delete
     
      SELECT
            @Comment_Type_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommentType'
            AND c.Code_Value = 'ContactInfoComment';
    
    
      INSERT      INTO @Tvp_Contact_Info_Tbl
                  ( Contact_Info_Id
                  ,First_Name
                  ,Last_Name
                  ,Email_Address
                  ,Phone_Number
                  ,Fax_Number
                  ,Job_Position
                  ,Location
                  ,Comment_Text
                  ,Comment_Id
                  ,Is_Active
                  ,Language_Cd
                  ,Operation_Type )
                  SELECT
                        tvp.Contact_Info_Id
                       ,tvp.First_Name
                       ,tvp.Last_Name
                       ,tvp.Email_Address
                       ,tvp.Phone_Number
                       ,tvp.Fax_Number
                       ,tvp.Job_Position
                       ,tvp.Location
                       ,tvp.Comment_Text
                       ,tvp.Comment_Id
                       ,tvp.Is_Active
                       ,tvp.Language_Cd
                       ,tvp.Operation_Type
                  FROM
                        @tvp_Client_Info tvp;
       
      
           
      SELECT
            @MinId = MIN(Id)
           ,@MaxId = MAX(Id)
      FROM
            @Tvp_Contact_Info_Tbl;
         
      WHILE @MinId <= @MaxId
            BEGIN   
                  SELECT
                        @Comment_Text = Comment_Text
                  FROM
                        @Tvp_Contact_Info_Tbl
                  WHERE
                        Id = @MinId
                        AND Comment_Text IS NOT NULL
                        AND Comment_Id IS NULL;
						
                  IF @Comment_Text IS NOT NULL
                        BEGIN	
                              INSERT      INTO dbo.Comment
                                          ( Comment_Type_CD
                                          ,Comment_User_Info_Id
                                          ,Comment_Dt
                                          ,Comment_Text )
                                          SELECT
                                                @Comment_Type_Cd
                                               ,@User_Info_Id
                                               ,GETDATE()
                                               ,@Comment_Text;
								   
                              SELECT
                                    @comment_Id = SCOPE_IDENTITY();	
					
                              UPDATE
                                    tvp
                              SET
                                    tvp.Comment_Id = @comment_Id
                              FROM
                                    @Tvp_Contact_Info_Tbl tvp
                              WHERE
                                    Id = @MinId;
		  
                        END;			   

                  SET @MinId = @MinId + 1;
				   
            END;

      DELETE
            c
      FROM
            dbo.Comment c
            INNER JOIN dbo.Contact_Info ci
                  ON c.Comment_ID = ci.Comment_ID
      WHERE
            ci.Contact_Level_Cd = @Contact_Level_Cd
            AND ci.Contact_Type_Cd = @Contact_Type_Cd
            AND EXISTS ( SELECT
                              1
                         FROM
                              @tvp_Client_Info tvp
                         WHERE
                              ci.Contact_Info_Id = tvp.Contact_Info_Id
                              AND tvp.Operation_Type = 'Delete' );

      DELETE
            c
      FROM
            dbo.Comment c
            INNER JOIN dbo.Contact_Info ci
                  ON c.Comment_ID = ci.Comment_ID
      WHERE
            EXISTS ( SELECT
                        1
                     FROM
                        @tvp_Client_Info tvp
                     WHERE
                        ci.Comment_Id = tvp.Comment_Id
                        AND ci.Contact_Info_Id = ci.Contact_Info_Id
                        AND tvp.Operation_Type <> 'Delete'
                        AND tvp.Comment_Text IS NULL );
                              
   
      UPDATE
            c
      SET
            Comment_Text = tvp.Comment_Text
      FROM
            dbo.Comment c
            INNER JOIN @tvp_Client_Info tvp
                  ON c.Comment_ID = tvp.Comment_ID;


	

--Client referesh if client_id is passed and vendor_id is not passed
      DELETE
            cc
      FROM
            dbo.Client_Contact_Map cc
            INNER JOIN dbo.Contact_Info c
                  ON cc.Contact_Info_Id = c.Contact_Info_Id
      WHERE
            CLIENT_ID = @Client_Id
            AND c.Contact_Level_Cd = @Contact_Level_Cd
            AND EXISTS ( SELECT
                              1
                         FROM
                              @tvp_Client_Info tvp
                         WHERE
                              c.Contact_Info_Id = tvp.Contact_Info_Id
                              AND tvp.Operation_Type = 'Delete' );             

 --Vendor referesh if client_id is passed and vendor_id is also passed

      DELETE
            v
      FROM
            dbo.Vendor_Contact_Map v
      WHERE
            VENDOR_ID = @Vendor_Id
            AND EXISTS ( SELECT
                              1
                         FROM
                              @tvp_Client_Info tvp
                         WHERE
                              v.Contact_Info_Id = tvp.Contact_Info_Id
                              AND tvp.Operation_Type = 'Delete' ); 


 --Contact merge
               
      MERGE INTO dbo.Contact_Info AS tgt
      USING
            ( SELECT
                  tvp.Contact_Info_Id
                 ,@Contact_Type_Cd AS Contact_Type_Cd
                 ,@Contact_Level_Cd AS Contact_Level_Cd
                 ,tvp.First_Name
                 ,tvp.Last_Name
                 ,tvp.Email_Address
                 ,tvp.Phone_Number
                 ,tvp.Fax_Number
                 ,tvp.Job_Position
                 ,tvp.Location
                 ,tvp.Comment_Id
                 ,tvp.Is_Active
                 ,@User_Info_Id AS User_Info_Id
                 ,GETDATE() AS TS
                 ,tvp.Language_Cd
                 ,tvp.Operation_Type
              FROM
                  @Tvp_Contact_Info_Tbl tvp ) AS src
      ON ( tgt.Contact_Info_Id = src.Contact_Info_Id
           AND tgt.Contact_Type_Cd = src.Contact_Type_Cd
           AND tgt.Contact_Level_Cd = src.Contact_Level_Cd )
      WHEN MATCHED AND src.Operation_Type <> 'Delete' THEN
            UPDATE SET
                    First_Name = src.First_Name
                   ,Last_Name = src.Last_Name
                   ,Email_Address = src.Email_Address
                   ,Phone_Number = src.Phone_Number
                   ,Fax_Number = src.Fax_Number
                   ,Job_Position = src.Job_Position
                   ,Location = src.Location
                   ,Comment_Id = src.Comment_Id
                   ,Updated_User_Id = src.User_Info_Id
                   ,Last_Change_Ts = src.TS
                   ,Language_Cd = src.Language_Cd
      WHEN MATCHED AND src.Operation_Type = 'Delete'
            AND tgt.Contact_Type_Cd = @Contact_Type_Cd
            AND tgt.Contact_Level_Cd = @Contact_Level_Cd
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Client_Contact_Map cc
                             WHERE
                              cc.CLIENT_ID <> ISNULL(@Client_Id, 0)
                              AND tgt.Contact_Info_Id = cc.Contact_Info_Id )
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              dbo.Vendor_Contact_Map cc
                             WHERE
                              cc.VENDOR_ID <> ISNULL(@Vendor_Id, 0)
                              AND tgt.Contact_Info_Id = cc.Contact_Info_Id ) THEN
            DELETE
      WHEN NOT MATCHED BY TARGET THEN
            INSERT
                   ( Contact_Type_Cd
                   ,Contact_Level_Cd
                   ,First_Name
                   ,Last_Name
                   ,Email_Address
                   ,Phone_Number
                   ,Fax_Number
                   ,Job_Position
                   ,Location
                   ,Comment_Id
                   ,Is_Active
                   ,Created_User_Id
                   ,Created_Ts
                   ,Updated_User_Id
                   ,Last_Change_Ts
                   ,Language_Cd )
            VALUES ( src.Contact_Type_Cd
                   ,src.Contact_Level_Cd
                   ,src.First_Name
                   ,src.Last_Name
                   ,src.Email_Address
                   ,src.Phone_Number
                   ,src.Fax_Number
                   ,src.Job_Position
                   ,src.Location
                   ,src.Comment_Id
                   ,src.Is_Active
                   ,src.User_Info_Id
                   ,src.TS
                   ,src.User_Info_Id
                   ,src.TS
                   ,src.Language_Cd )
      OUTPUT
            Inserted.Contact_Info_Id
            INTO #Inserted_Contact_Info_Ids;

    --collect inserted contact_info     
   
                                             
      INSERT      INTO @Contact_Info_Tbl
                  ( Contact_Info_Id )
                  SELECT
                        Contact_Info_Id
                  FROM
                        #Inserted_Contact_Info_Ids 

     --Client_Contact Merge
                        
      MERGE INTO dbo.Client_Contact_Map AS tgt
      USING
            ( SELECT
                  @Client_Id AS Client_Id
                 ,Contact_Info_Id AS Contact_Info_Id
              FROM
                  @Contact_Info_Tbl tvp
              WHERE
                  @Client_Id IS NOT NULL
                  AND Contact_Info_Id IS NOT NULL ) AS src
      ON ( tgt.CLIENT_ID = src.Client_Id
           AND tgt.Contact_Info_Id = src.Contact_Info_Id )
      WHEN NOT MATCHED BY TARGET THEN
            INSERT
                   ( Client_Id
                   ,Contact_Info_Id )
            VALUES ( src.Client_Id
                   ,src.Contact_Info_Id );  

	  --Vendor_Contact Merge

      MERGE INTO dbo.Vendor_Contact_Map AS tgt
      USING
            ( SELECT
                  @Vendor_Id AS Vendor_Id
                 ,Contact_Info_Id AS Contact_Info_Id
              FROM
                  @Contact_Info_Tbl tvp
              WHERE
                  @Vendor_Id IS NOT NULL
                  AND Contact_Info_Id IS NOT NULL ) AS src
      ON ( tgt.VENDOR_ID = src.Vendor_Id
           AND tgt.Contact_Info_Id = src.Contact_Info_Id )
      WHEN NOT MATCHED BY TARGET THEN
            INSERT
                   ( Vendor_Id
                   ,Contact_Info_Id )
            VALUES ( src.Vendor_Id
                   ,src.Contact_Info_Id );
                        
                        
      DROP TABLE  #Inserted_Contact_Info_Ids       

END;


;


;
GO
GRANT EXECUTE ON  [dbo].[Contact_Info_Merge] TO [CBMSApplication]
GO
