SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
             
NAME: Inv_Sourced_Image_Image_Recipient_Cd_Upd

DESCRIPTION:

To update the recipient cd (Prokarma/CTE) in the Inv_Sourced_Image table

INPUT PARAMETERS:
NAME							DATATYPE		DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
@DE_Recipient_Cd				INT							Code Id of Prokarma/CTE
@Transfer_Dt					DATE						Date the image was transferred

OUTPUT PARAMETERS:
NAME							DATATYPE		DEFAULT		DESCRIPTION
-----------------------------------------------------------------------
USAGE EXAMPLES:
-----------------------------------------------------------------------

	EXEC dbo.Inv_Sourced_Image_Cnt_By_Recipient -- Returns the current date transfer cnt for all recipient

	EXEC dbo.Inv_Sourced_Image_Cnt_By_Recipient -- Returns the specific recipient cnt for the given date.
		 @DE_Recipient_Cd = 100976
		 ,@Transfer_Dt = '2015-03-25'

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------------
HG			Harihara Suthan Ganesan

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------------
HG			2015-03-25	Created for Invoice scraping recipient enh

*/

CREATE PROCEDURE [dbo].[Inv_Sourced_Image_Cnt_By_Recipient]
      (
       @DE_Recipient_Cd INT = NULL
      ,@Transfer_Dt DATE = NULL )
AS
BEGIN

      SET NOCOUNT ON

      SET @Transfer_Dt = ISNULL(@Transfer_Dt, GETDATE())

      SELECT
            rc.Code_Value AS DE_Recipient
           ,rc.Code_Id AS DE_Recipient_Cd
           ,COUNT(isi.INV_SOURCED_IMAGE_ID) AS Image_Cnt
      FROM
            dbo.Code rc
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = rc.Codeset_Id
            LEFT OUTER JOIN dbo.Inv_Sourced_Image isi
                  ON isi.DE_Recipient_Cd = rc.Code_Id
                     AND CAST(isi.Recipient_Transfer_Ts AS DATE) = @Transfer_Dt
      WHERE
            cs.Codeset_Name = 'DataEntryRecipient'
            AND ( @DE_Recipient_Cd IS NULL
                  OR rc.Code_Id = @DE_Recipient_Cd )
      GROUP BY
            rc.Code_Value
           ,rc.Code_Id
           ,rc.Display_Seq
      ORDER BY
            rc.Display_Seq

END
;
GO
GRANT EXECUTE ON  [dbo].[Inv_Sourced_Image_Cnt_By_Recipient] TO [CBMSApplication]
GO
