SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_DELETE_RFP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec dbo.SR_RFP_DELETE_RFP_P 110
-- select * from sr_rfp_bid where sr_rfp_bid_id = 727
-- 
CREATE         PROCEDURE [dbo].[SR_RFP_DELETE_RFP_P] 

@rfpId int
AS
set nocount on
declare @bidId int
declare @bidRequirementsId int
declare @supplierPriceCommentsId int
declare @supplierServiceId int

declare @deliveryFuelId int
declare @balancingToleranceId int
declare @riskManagementId int
declare @alternateFuelId int
declare @pricingScopeId int
declare @miscPowerId int

declare @error as int
set @error = 0



begin tran


delete from sr_rfp_checklist where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)
set @error = @error + @@error

delete from sr_rfp_account_meter_map where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)
set @error = @error + @@error

delete from SR_RFP_LP_DETERMINANT_VALUES where SR_RFP_LOAD_PROFILE_DETERMINANT_ID 
in (select SR_RFP_LOAD_PROFILE_DETERMINANT_ID from  SR_RFP_LOAD_PROFILE_DETERMINANT where SR_RFP_LOAD_PROFILE_SETUP_ID
in (select SR_RFP_LOAD_PROFILE_SETUP_ID from SR_RFP_LOAD_PROFILE_SETUP where SR_RFP_ACCOUNT_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId)))
set @error = @error + @@error

delete from SR_RFP_LOAD_PROFILE_DETERMINANT from  SR_RFP_LOAD_PROFILE_DETERMINANT where SR_RFP_LOAD_PROFILE_SETUP_ID
in (select SR_RFP_LOAD_PROFILE_SETUP_ID from SR_RFP_LOAD_PROFILE_SETUP where SR_RFP_ACCOUNT_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId))
set @error = @error + @@error

delete from SR_RFP_LOAD_PROFILE_SETUP from SR_RFP_LOAD_PROFILE_SETUP where SR_RFP_ACCOUNT_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId)
set @error = @error + @@error

delete from SR_RFP_LP_ACCOUNT_ADDITIONAL_ROW where SR_RFP_ACCOUNT_ID 
in (select sr_rfp_account_id from sr_rfp_account where sr_rfp_id= @rfpId)
set @error = @error + @@error

delete from SR_RFP_LP_SEND_CLIENT where SR_ACCOUNT_GROUP_ID in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id= @rfpId)
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_LP_INTERVAL_DATA where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id= @rfpId))
set @error = @error + @@error
*/

delete from SR_RFP_LP_INTERVAL_DATA where sr_rfp_account_id 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId )
set @error = @error + @@error

delete from SR_RFP_LP_COMMENT where sr_rfp_account_id 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId )
set @error = @error + @@error

/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_LP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id= @rfpId))
set @error = @error + @@error
*/
delete from SR_RFP_LP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_LP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

DELETE SR_RFP_LP_CLIENT_APPROVAL WHERE SR_RFP_ID = @rfpId
set @error = @error + @@error

delete from SR_RFP_LP_ACCOUNT_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_LP_ACCOUNT_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from SR_RFP_CLOSURE where SR_ACCOUNT_GROUP_ID in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error
*/
delete from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_CONTRACT_ADMIN_INITIATIVES where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from SR_RFP_REASON_NOT_WINNING_MAP where SR_RFP_REASON_NOT_WINNING_ID
in (select SR_RFP_REASON_NOT_WINNING_ID from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete from SR_RFP_REASON_NOT_WINNING_MAP where SR_RFP_REASON_NOT_WINNING_ID
in (select SR_RFP_REASON_NOT_WINNING_ID from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error

delete from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_REASON_NOT_WINNING where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from SR_RFP_SEND_SUPPLIER_LOG where SR_RFP_SEND_SUPPLIER_ID
in (select SR_RFP_SEND_SUPPLIER_ID from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete from SR_RFP_SEND_SUPPLIER_LOG where SR_RFP_SEND_SUPPLIER_ID
in (select SR_RFP_SEND_SUPPLIER_ID from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error

delete from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_SEND_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where  sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from sr_rfp_sop_shortlist_details where sr_rfp_sop_shortlist_id in 
(select sr_rfp_sop_shortlist_id from sr_rfp_sop_shortlist where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete from sr_rfp_sop_shortlist_details where sr_rfp_sop_shortlist_id in 
(select sr_rfp_sop_shortlist_id from sr_rfp_sop_shortlist where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId) and is_bid_group = 1)
set @error = @error + @@error

delete from SR_RFP_SOP_SHORTLIST where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_SOP_SHORTLIST where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from SR_RFP_SOP_DETAILS where SR_RFP_SOP_ID
in (select SR_RFP_SOP_ID from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0))
set @error = @error + @@error

delete from SR_RFP_SOP_DETAILS where SR_RFP_SOP_ID
in (select SR_RFP_SOP_ID from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId) and is_bid_group = 1))
set @error = @error + @@error

delete from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete from SR_RFP_SOP where SR_RFP_SOP_SUMMARY_ID
in (select SR_RFP_SOP_SUMMARY_ID from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId) and is_bid_group = 1)
set @error = @error + @@error


DELETE SR_RFP_SOP_DETAILS WHERE SR_RFP_SOP_ID IN (SELECT SR_RFP_SOP_ID FROM SR_RFP_SOP WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IN (SELECT SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP WHERE SR_RFP_ID = @rfpId))
set @error = @error + @@error

DELETE SR_RFP_SOP WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IN (SELECT SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP WHERE SR_RFP_ID = @rfpId)
set @error = @error + @@error

delete from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete from SR_RFP_SOP_SUMMARY where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error
*/
delete from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error
*/
delete from SR_RFP_SOP_CLIENT_APPROVAL where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error
*/
delete from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error
*/
delete from SR_RFP_SOP_SMR where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select change_notice_image_id from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error

delete cbms_image where cbms_image_id in (select flow_verification_image_id from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0)
set @error = @error + @@error
*/
delete from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select change_notice_image_id from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error

delete cbms_image where cbms_image_id in (select flow_verification_image_id from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1)
set @error = @error + @@error
*/
delete from SR_RFP_UTILITY_SWITCH where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete from SR_RFP_ARCHIVE_CHECKLIST where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)
set @error = @error + @@error


delete from SR_RFP_ARCHIVE_ACCOUNT where sr_rfp_account_id in (select sr_rfp_account_id from sr_rfp_account
where  sr_rfp_id=@rfpId)
set @error = @error + @@error

delete SR_RFP_SITE_LP_XML where SR_RFP_ID = @rfpId
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_SMU where SR_RFP_ID = @rfpId)
set @error = @error + @@error
*/
delete SR_RFP_SMU where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete SR_RFP_UPDATE_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error

delete SR_RFP_UPDATE_SUPPLIER where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

--SELECT * FROM SR_RFP_UPDATE_SUPPLIER

DECLARE C_BID CURSOR FAST_FORWARD	FOR
select sr_rfp_bid_id from sr_rfp_bid where sr_rfp_bid_id in (
select sr_rfp_bid_id from sr_rfp_term_product_map where sr_rfp_account_term_id in 
(select sr_rfp_account_term_id from sr_rfp_account_term where sr_rfp_term_id in (select sr_rfp_term_id from sr_rfp_term where sr_rfp_id = @rfpId)))


OPEN C_BID
FETCH NEXT FROM C_BID INTO @bidId
WHILE (@@fetch_status <> -1)
BEGIN
	IF (@@fetch_status <> -2)
	BEGIN

SELECT @bidRequirementsId = SR_RFP_BID_REQUIREMENTS_ID from SR_RFP_BID where SR_RFP_BID_ID = @bidId
SELECT @supplierPriceCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID from SR_RFP_BID where SR_RFP_BID_ID = @bidId
SELECT @supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID from SR_RFP_BID where SR_RFP_BID_ID = @bidId


SELECT @deliveryFuelId = SR_RFP_DELIVERY_FUEL_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
SELECT @balancingToleranceId = SR_RFP_BALANCING_TOLERANCE_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId 
SELECT @riskManagementId = SR_RFP_RISK_MANAGEMENT_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId 
SELECT @alternateFuelId = SR_RFP_ALTERNATE_FUEL_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
SELECT @pricingScopeId = SR_RFP_PRICING_SCOPE_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
SELECT @miscPowerId = SR_RFP_MISCELLANEOUS_POWER_ID from SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
/*
print 'bidId->' + str(@bidId)
print '@bidRequirementsId->' + str(@bidRequirementsId) 
print '@supplierPriceCommentsId->' + str(@supplierPriceCommentsId) 
print '@supplierServiceId->' + str(@supplierServiceId) 
print '@deliveryFuelId->' + str(@deliveryFuelId) 
print '@riskManagementId->' + str(@riskManagementId) 
print '@alternateFuelId->' + str(@alternateFuelId) 
print '@pricingScopeId->' + str(@pricingScopeId) 
print '@miscPowerId->' + str(@miscPowerId) 
print '@balancingToleranceId->' + str(@balancingToleranceId) 
*/

delete sr_rfp_term_product_map where SR_RFP_BID_ID = @bidId 
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_BID_DOCUMENTS where SR_RFP_BID_ID = @bidId)
set @error = @error + @@error
*/
delete SR_RFP_BID_DOCUMENTS where SR_RFP_BID_ID = @bidId
set @error = @error + @@error

delete SR_SW_REMEMBER_BID where SR_RFP_BID_ID = @bidId
set @error = @error + @@error

delete SR_RFP_BID where SR_RFP_BID_ID = @bidId
--print 'Deleted Bid ->' + str(@bidId)
set @error = @error + @@error

delete sr_rfp_term_product_map where SR_RFP_SELECTED_PRODUCTS_ID IN (SELECT SR_RFP_SELECTED_PRODUCTS_ID FROM SR_RFP_SELECTED_PRODUCTS where SR_RFP_BID_REQUIREMENTS_ID = @bidRequirementsId)
set @error = @error + @@error

delete SR_RFP_SELECTED_PRODUCTS where SR_RFP_BID_REQUIREMENTS_ID = @bidRequirementsId
set @error = @error + @@error

delete SR_RFP_BID_REQUIREMENTS where SR_RFP_BID_REQUIREMENTS_ID = @bidRequirementsId
set @error = @error + @@error
delete SR_RFP_SUPPLIER_PRICE_COMMENTS where SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierPriceCommentsId
set @error = @error + @@error
delete SR_RFP_SUPPLIER_SERVICE where SR_RFP_SUPPLIER_SERVICE_ID = @supplierServiceId
set @error = @error + @@error

delete SR_RFP_DELIVERY_FUEL where SR_RFP_DELIVERY_FUEL_ID = @deliveryFuelId
set @error = @error + @@error
delete SR_RFP_BALANCING_TOLERANCE where SR_RFP_BALANCING_TOLERANCE_ID = @balancingToleranceId
set @error = @error + @@error
delete SR_RFP_RISK_MANAGEMENT where SR_RFP_RISK_MANAGEMENT_ID = @riskManagementId
set @error = @error + @@error
delete SR_RFP_ALTERNATE_FUEL where SR_RFP_ALTERNATE_FUEL_ID = @alternateFuelId
set @error = @error + @@error
delete SR_RFP_PRICING_SCOPE where SR_RFP_PRICING_SCOPE_ID = @pricingScopeId
set @error = @error + @@error
delete SR_RFP_MISCELLANEOUS_POWER where SR_RFP_MISCELLANEOUS_POWER_ID = @miscPowerId
set @error = @error + @@error

	END
FETCH NEXT FROM C_BID INTO @bidId
END
CLOSE C_BID
DEALLOCATE C_BID

delete sr_rfp_account_term where sr_rfp_term_id in (select sr_rfp_term_id from sr_rfp_term where sr_rfp_id = @rfpId)
set @error = @error + @@error

delete SR_RFP_SELECTED_PRODUCTS where sr_rfp_id = @rfpId
set @error = @error + @@error

delete from SR_RFP_TERM where SR_RFP_ID = @rfpId
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_COMMUNICATIONS where SR_RFP_ID = @rfpId)
set @error = @error + @@error
*/
delete SR_RFP_COMMUNICATIONS where SR_RFP_ID = @rfpId
set @error = @error + @@error
/*
delete cbms_image where cbms_image_id in (select cbms_image_id from SR_RFP_EMAIL_ATTACHMENT where SR_RFP_EMAIL_LOG_ID in (select SR_RFP_EMAIL_LOG_ID from SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId))
set @error = @error + @@error
*/
delete SR_RFP_EMAIL_ATTACHMENT where SR_RFP_EMAIL_LOG_ID in (select SR_RFP_EMAIL_LOG_ID from SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId)
set @error = @error + @@error

DELETE SR_RFP_SEND_SUPPLIER_LOG WHERE SR_RFP_SEND_SUPPLIER_ID IN (SELECT SR_RFP_SEND_SUPPLIER_ID FROM SR_RFP_SEND_SUPPLIER WHERE SR_RFP_EMAIL_LOG_ID IN (SELECT SR_RFP_EMAIL_LOG_ID FROM SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId))
set @error = @error + @@error

DELETE SR_RFP_SEND_SUPPLIER WHERE SR_RFP_EMAIL_LOG_ID IN (SELECT SR_RFP_EMAIL_LOG_ID FROM SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId)
set @error = @error + @@error

DELETE SR_RFP_UPDATE_SUPPLIER WHERE SR_RFP_EMAIL_LOG_ID IN (SELECT SR_RFP_EMAIL_LOG_ID FROM SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId)
set @error = @error + @@error

delete SR_RFP_EMAIL_LOG where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete SR_SW_REMEMBER_BID where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete SR_SW_RFP_NO_BID where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_account_id from sr_rfp_account where  sr_rfp_id= @rfpId and sr_rfp_bid_group_id is null)
and is_bid_group = 0
set @error = @error + @@error
 
delete SR_SW_RFP_NO_BID where SR_ACCOUNT_GROUP_ID 
in (select sr_rfp_bid_group_id from sr_rfp_account where sr_rfp_id= @rfpId)
and is_bid_group = 1
set @error = @error + @@error

delete SR_SW_RFP_TERMS_CONDITIONS where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete SR_RFP_BID_DOCUMENTS where SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID in (select SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID from SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId)
set @error = @error + @@error
 
DELETE SR_RFP_SEND_SUPPLIER_LOG WHERE SR_RFP_SEND_SUPPLIER_ID IN (SELECT SR_RFP_SEND_SUPPLIER_ID FROM SR_RFP_SEND_SUPPLIER WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IN (SELECT SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId))  
set @error = @error + @@error

DELETE SR_RFP_SEND_SUPPLIER WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IN (SELECT SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId)  
set @error = @error + @@error

DELETE SR_RFP_UPDATE_SUPPLIER WHERE SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IN (SELECT SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID FROM SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId)
set @error = @error + @@error

delete SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete SR_RFP_PRICING_PRODUCT where SR_RFP_ID = @rfpId
set @error = @error + @@error

delete from sr_rfp_account where  sr_rfp_id=@rfpId
set @error = @error + @@error

delete from sr_rfp where sr_rfp_id = @rfpId
set @error = @error + @@error


if @error = 0
	commit tran
else
	rollback tran
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_RFP_P] TO [CBMSApplication]
GO
