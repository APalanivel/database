SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******

NAME: [DBO].[SR_Comments_Accounts_Map_Del]

DESCRIPTION:
	It Deletes SR Comments Accounts Map for Selected Sr Comments Accounts Map Id.

INPUT PARAMETERS:
NAME							DATATYPE	DEFAULT		DESCRIPTION
---------------------------------------------------------------
@SR_Comments_Accounts_Map_Id	INT

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		EXEC SR_Comments_Accounts_Map_Del 38
	ROLLBACK TRAN

AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------          
PNR			PANDARINATH
          
MODIFICATIONS           
INITIALS	DATE		MODIFICATION          
------------------------------------------------------------          
PNR			27-MAY-10	CREATED     

*/  

CREATE PROCEDURE dbo.SR_Comments_Accounts_Map_Del
   (
    @SR_Comments_Accounts_Map_Id INT
   )
AS 
BEGIN

    SET NOCOUNT ON;
   
    DELETE 
   	FROM	
		dbo.SR_COMMENTS_ACCOUNTS_MAP
	WHERE 
		SR_COMMENTS_ACCOUNTS_MAP_ID = @SR_Comments_Accounts_Map_Id
END
GO
GRANT EXECUTE ON  [dbo].[SR_Comments_Accounts_Map_Del] TO [CBMSApplication]
GO
