SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_UPDATE_REASON_FOR_NOT_WINNING_DETAILS_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@accountGroupId	int       	          	
	@isBidGroup    	int       	          	
	@supplierContactVendorMapId	int       	          	
	@comments      	varchar(1000)	          	
	@userId        	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE         PROCEDURE DBO.SR_RFP_UPDATE_REASON_FOR_NOT_WINNING_DETAILS_P

@accountGroupId int,
@isBidGroup int,
@supplierContactVendorMapId int,
@comments varChar(1000),
@userId int

AS
set nocount on
UPDATE	SR_RFP_REASON_NOT_WINNING

SET		COMMENTS = @comments

WHERE	SR_ACCOUNT_GROUP_ID = @accountGroupId
		AND IS_BID_GROUP = @isBidGroup
		AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_REASON_FOR_NOT_WINNING_DETAILS_P] TO [CBMSApplication]
GO
