SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.STORE_ONBOARD_HEDGE_SETUP_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@RMOnboardClientId	int       	          	
	@divisionId    	int       	          	
	@siteId        	int       	          	
	@hedgeLevelId  	int       	          	
	@hedgeTyeId    	int       	          	
	@volumeUnitsTypeId	int       	          	
	@includeInReports	bit       	          	
	@groupId       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE       PROCEDURE [dbo].[STORE_ONBOARD_HEDGE_SETUP_P]
   @userId varchar
 , @sessionId varchar
 , @RMOnboardClientId integer
 , @divisionId integer
 , @siteId integer
 , @hedgeLevelId integer
 , @hedgeTyeId integer
 , @volumeUnitsTypeId integer
 , @includeInReports bit
 , @groupId integer
AS
set nocount on
   IF ( select count(1)
        from   RM_ONBOARD_HEDGE_SETUP hedge  
             , SITE site  
             , DIVISION div  
        where  hedge.RM_onboard_client_id = @RMOnboardClientId
               and hedge.division_id = div.division_id
               and hedge.site_id = site.site_id
               and site.site_id = @siteId
               and div.division_id = @divisionId
      ) > 0 
      BEGIN

         update   RM_Onboard_Hedge_Setup  
         set      hedge_level_type_id = @hedgeLevelId
                , hedge_type_id = @hedgeTyeId
                , volume_units_type_id = @volumeUnitsTypeId
                , include_in_reports = @includeInReports
                , rm_group_id = @groupId
         where    site_id = @siteId
                  and division_id = @divisionId
                  and RM_onboard_client_id = @RMOnboardClientId
 

      END

   ELSE 
      BEGIN

         insert   into RM_Onboard_Hedge_Setup  
                  (
                    RM_onboard_client_id
                  , hedge_level_type_id
                  , division_id
                  , site_id
                  , hedge_type_id
                  , volume_units_type_id
                  , include_in_reports
                  , rm_group_id
                  )
         values   (
                    @RMOnboardClientId
                  , @hedgeLevelId
                  , @divisionId
                  , @siteId
                  , @hedgeTyeId
                  , @volumeUnitsTypeId
                  , @includeInReports
                  , @groupId
                  )

      END
GO
GRANT EXECUTE ON  [dbo].[STORE_ONBOARD_HEDGE_SETUP_P] TO [CBMSApplication]
GO
