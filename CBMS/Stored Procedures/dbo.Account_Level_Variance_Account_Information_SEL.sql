
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********   
NAME:  dbo.Account_Level_Variance_Account_Information_SEL  
 
DESCRIPTION:  Used to select Account information  

INPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
@Account_Id			Int         
    
OUTPUT PARAMETERS:    
      Name              DataType          Default     Description    
------------------------------------------------------------    
    
USAGE EXAMPLES:  

	EXEC Account_Level_Variance_Account_Information_SEL 8846
	EXEC Account_Level_Variance_Account_Information_SEL 1249
	EXEC Account_Level_Variance_Account_Information_SEL 12498
	EXEC Account_Level_Variance_Account_Information_SEL 102196 


------------------------------------------------------------  
AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------  
	NK			Nageswara Rao Kosuri   
	RR			Raghu Reddy      

Modifications
	Initials	Date		Modification  
------------------------------------------------------------  
	NK			10/13/2009	Created
	HG			11/30/2009	Union All to get the suppplier account details eliminated by left join the Supplier_account_meter_map table.
							Code/codeset table join is not required as the sitegroup.Sitegroup_id can be directly mapped with Site.Division_id
							Distinct replaced by Group By Clause.
	RR			2016-11-30	VTE-56 Added Is_Data_Entry_Only to select list
******/  


CREATE PROCEDURE [dbo].[Account_Level_Variance_Account_Information_SEL] @Account_Id INT
AS 
BEGIN

      SET NOCOUNT ON;

      SELECT
            ch.Client_Name AS Client_Name
           ,ch.Sitegroup_Name AS Division_Name
           ,ch.Site_name AS Site_Name
           ,cha.Account_Number AS Account_Number
           ,cha.Account_Type AS Account_Type
           ,ISNULL(acc.Is_Data_Entry_Only, 0) AS Is_Data_Entry_Only
      FROM
            dbo.ACCOUNT acc
            INNER JOIN Core.Client_Hier_Account cha
                  ON acc.ACCOUNT_ID = cha.Account_Id
            INNER JOIN Core.Client_Hier ch
                  ON cha.Client_Hier_Id = ch.Client_Hier_Id
      WHERE
            acc.account_id = @Account_Id
      GROUP BY
            ch.Client_Name
           ,ch.Sitegroup_Name
           ,ch.Site_name
           ,cha.Account_Number
           ,cha.Account_Type
           ,ISNULL(acc.Is_Data_Entry_Only, 0)

END
;
GO

GRANT EXECUTE ON  [dbo].[Account_Level_Variance_Account_Information_SEL] TO [CBMSApplication]
GO
