
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.SR_RFP_GET_SELECTED_SUPPLIERS_P

DESCRIPTION:

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@RFP_ID        	int       	          	
	@StartIndex		INT				1
	@EndIndex		INT				2147483647
	@SortColumn		VARCHAR(100) = 'SR_SUPPLIER_CONTACT_INFO_ID'
    @SortIndex		VARCHAR(10) = 'Asc'

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC SR_RFP_GET_SELECTED_SUPPLIERS_P 197,1,20,'USER_INFO_NAME','desc'
	EXEC SR_RFP_GET_SELECTED_SUPPLIERS_P 197,1,20,'USER_INFO_NAME'
	EXEC SR_RFP_GET_SELECTED_SUPPLIERS_P 197,1,20,'VENDOR_NAME'

	EXEC SR_RFP_GET_SELECTED_SUPPLIERS_P 139,1,20
	EXEC SR_RFP_GET_SELECTED_SUPPLIERS_P 13270,1,20
	

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
	HG			Harihara Suthan G
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	SKA			07/13/2010	Pagination Code added	
	SKA			07/27/2010	Added one more column in select clause  
							Implemented sorting logic      	
	HG			07/28/2010	Added condition to change the @SortColumn input value SR_SUPPLIER_CONTACT_INFO_ID to scInfo.SR_SUPPLIER_CONTACT_INFO_ID as this column present in more than one table.
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-02-03	Global Sourcing - Phase3 - (GCS-463) Added vendor typeID,countryID & commodityID to select list
	RR			2016-03-16	Global Sourcing - Phase3 - (GCS-531) Added vendor contact EMAIL_ADDRESS to select list

******/
CREATE PROCEDURE [dbo].[SR_RFP_GET_SELECTED_SUPPLIERS_P]
      @RFP_ID INT
     ,@StartIndex INT = 1
     ,@EndIndex INT = 2147483647
     ,@SortColumn VARCHAR(100) = 'SR_SUPPLIER_CONTACT_INFO_ID'
     ,@SortIndex VARCHAR(10) = 'Asc'
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @SQL VARCHAR(8000)
      SELECT
            @SortColumn = case WHEN @SortColumn = 'USER_INFO_NAME' THEN 'userInfo.FIRST_NAME + SPACE(1) + userInfo.LAST_NAME'
                               WHEN @SortColumn = 'SR_SUPPLIER_CONTACT_INFO_ID' THEN 'scInfo.SR_SUPPLIER_CONTACT_INFO_ID'
                               ELSE @SortColumn
                          END

      SELECT
            @SQL = ';WITH  SR_RFP_GET_SELECTED_SUPPLIERS_P_pag
              AS ( SELECT
                        scInfo.SR_SUPPLIER_CONTACT_INFO_ID
                       ,userInfo.FIRST_NAME + SPACE(1) + userInfo.LAST_NAME USER_INFO_NAME
                       ,v.VENDOR_ID
                       ,v.VENDOR_NAME
                       ,COUNT(1) OVER ( ) AS Total
                       ,rank() OVER( PARTITION BY v.VENDOR_ID ORDER BY con.COUNTRY_NAME  ) AS Country_Count
					   ,Row_Num = ROW_NUMBER() OVER ( ORDER BY ' + @SortColumn + ' ' + @SortIndex + ')
					   ,v.VENDOR_TYPE_ID
					   ,rfp.COMMODITY_TYPE_ID
					   ,cha.Meter_Country_Id AS Country_Id
					   ,userInfo.EMAIL_ADDRESS
                   FROM
                        dbo.VENDOR v
                        INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srRFPcvMap ON v.vendor_id = srRFPcvMap.vendor_id
                        INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO scInfo ON srRFPcvMap.SR_SUPPLIER_CONTACT_INFO_ID = scInfo.SR_SUPPLIER_CONTACT_INFO_ID
                        INNER JOIN dbo.USER_INFO userInfo ON scInfo.USER_INFO_ID = userInfo.USER_INFO_ID
                        INNER JOIN dbo.SR_RFP rfp ON srRFPcvMap.SR_RFP_ID = rfp.SR_RFP_ID
                        INNER JOIN dbo.SR_RFP_ACCOUNT rfpacc ON rfp.SR_RFP_ID = rfpacc.SR_RFP_ID
                        INNER JOIN Core.Client_Hier_Account cha ON rfpacc.ACCOUNT_ID = cha.Account_Id and rfp.COMMODITY_TYPE_ID=cha.Commodity_Id
                        INNER JOIN dbo.COUNTRY con ON cha.Meter_Country_Id = con.COUNTRY_ID
                   WHERE
                        srRFPcvMap.SR_RFP_ID = ' + str(@RFP_ID) + '
                   GROUP BY
						  scInfo.SR_SUPPLIER_CONTACT_INFO_ID
						 ,userInfo.FIRST_NAME + space(1) + userInfo.LAST_NAME
						 ,v.VENDOR_ID
						 ,v.VENDOR_NAME
						 ,v.VENDOR_TYPE_ID
						 ,rfp.COMMODITY_TYPE_ID
						 ,cha.Meter_Country_Id
						 ,con.COUNTRY_NAME
						 ,userInfo.EMAIL_ADDRESS
                    )
            SELECT
                  SR_SUPPLIER_CONTACT_INFO_ID
                 ,USER_INFO_NAME
                 ,VENDOR_ID
                 ,VENDOR_NAME
                 ,Total
                 ,VENDOR_TYPE_ID
                 ,COMMODITY_TYPE_ID
                 ,Country_Id
                 ,Row_Num
                 ,Country_Count
                 ,EMAIL_ADDRESS
            FROM
                  SR_RFP_GET_SELECTED_SUPPLIERS_P_pag
            WHERE
                  Row_Num BETWEEN ' + cast(@StartIndex AS VARCHAR) + ' AND ' + cast(@EndIndex AS VARCHAR) + ' AND Country_Count = 1'
      EXEC ( @SQL )                  

END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SELECTED_SUPPLIERS_P] TO [CBMSApplication]
GO
