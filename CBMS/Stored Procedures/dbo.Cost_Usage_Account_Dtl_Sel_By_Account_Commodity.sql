
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Cost_Usage_Account_Dtl_Sel_By_Account_Commodity

DESCRIPTION:

	Used to Select the data from Cost_Usage_Account_Dtl table for the given Acccount , period and commodity
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Account_Id			int
	@Client_Hier_Id	     INT
	@Commodity_id			int
	@Begin_Dt				Date
	@End_Dt				Date
	@Uom_Type_Id			INT
	@Currency_Unit_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Account_Dtl_Sel_By_Account_Commodity 54752,15205,67,'1/1/2009','12/1/2009',1594,3

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
	SSR		Sharad Srivastava
	AP		Athmaram Pabbathi

MODIFICATIONS
	Initials	Date		   Modification
------------------------------------------------------------
	HG        1/28/2010	   Created
	SSR		02/17/2010   Removed Input parameter @Client_ID
	SSR		03/02/2010   Refered Account & SUPPLIER_ACCOUNT_METER_MAP table to get the Site_ID	
	HG		03/26/2010   Conversion factors determined by Charge type (intially it was determined by ISNULL function)
	AP		03/26/2012   Addnl Data Changes
						  --Added @Client_Hier_Id as parameter
						  --Replace Account, Entity, SAMM tables with CH & CHA
						  --included Client_Hier_Id in CUAD join
******/

CREATE PROCEDURE dbo.Cost_Usage_Account_Dtl_Sel_By_Account_Commodity
      ( 
       @Account_Id INT
      ,@Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Uom_Type_Id INT
      ,@Currency_Unit_Id INT )
AS 
BEGIN
      SET NOCOUNT ON
	
      SELECT
            cua.Cost_Usage_Account_Dtl_Id
           ,cua.Service_Month
           ,bm.Bucket_Master_Id
           ,bkt_cd.Code_Value AS Bucket_Type
           ,bm.Bucket_Name
           ,cua.Bucket_Value * ( case WHEN bkt_Cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                      WHEN bkt_cd.code_value = 'Determinant' THEN uc.Conversion_Factor
                                 END ) Bucket_Value
           ,UOM_Type_Id
           ,cua.CURRENCY_UNIT_ID
      FROM
            dbo.Cost_Usage_Account_Dtl cua
            JOIN dbo.Bucket_Master bm
                  ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
            JOIN dbo.Code bkt_cd
                  ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
            INNER JOIN ( SELECT
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,ch.Client_Currency_Group_Id
                         FROM
                              Core.Client_Hier ch
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                         WHERE
                              ch.Client_Hier_Id = @Client_Hier_Id
                              AND cha.Commodity_Id = @Commodity_id
                         GROUP BY
                              cha.Client_Hier_Id
                             ,cha.Account_Id
                             ,ch.Client_Currency_Group_Id ) cha
                  ON cha.Client_Hier_Id = cua.Client_Hier_Id
                     AND cha.Account_Id = cua.Account_Id
            LEFT JOIN dbo.consumption_unit_conversion uc
                  ON uc.BASE_UNIT_ID = cua.UOM_Type_Id
                     AND uc.CONVERTED_UNIT_ID = @Uom_Type_Id
            LEFT JOIN dbo.CURRENCY_UNIT_CONVERSION cc
                  ON ( cc.currency_group_id = cha.Client_Currency_Group_Id
                       AND cc.base_unit_id = cua.CURRENCY_UNIT_ID
                       AND cc.converted_unit_id = @Currency_unit_id
                       AND cc.conversion_date = cua.Service_month )
      WHERE
            bm.Commodity_id = @Commodity_id
            AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt
      ORDER BY
            cua.Service_Month
           ,bkt_cd.Code_Value
           ,bm.Bucket_Name

END
;
GO

GRANT EXECUTE ON  [dbo].[Cost_Usage_Account_Dtl_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
