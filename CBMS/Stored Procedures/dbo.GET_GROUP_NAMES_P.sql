SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.GET_GROUP_NAMES_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(1)	          	
	@sessionId     	varchar(1)	          	
	@clientId      	int       	          	
	@hedgeTypeId   	int       	          	
	@hedgeLevelId  	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE [dbo].[GET_GROUP_NAMES_P] 

@userId varchar,
@sessionId varchar,
@clientId int,
@hedgeTypeId int,
@hedgeLevelId int
as
set nocount on
select	group1.RM_GROUP_ID,group1.GROUP_NAME from RM_GROUP group1

where 	group1.RM_GROUP_ID in 
(
select RM_ONBOARD_HEDGE_SETUP.RM_GROUP_ID 

from 
	RM_ONBOARD_CLIENT,
	RM_ONBOARD_HEDGE_SETUP

where
	RM_ONBOARD_CLIENT.client_id=@clientId and 
	RM_ONBOARD_HEDGE_SETUP.HEDGE_TYPE_ID=@hedgeTypeId and
	RM_ONBOARD_HEDGE_SETUP.RM_ONBOARD_CLIENT_ID=RM_ONBOARD_CLIENT.RM_ONBOARD_CLIENT_ID AND
	RM_ONBOARD_HEDGE_SETUP.HEDGE_LEVEL_TYPE_ID IN
	( (select entity_id from entity where entity_type=262 and entity_name='CORPORATE'),@hedgeLevelId)

)

order by group1.GROUP_NAME
GO
GRANT EXECUTE ON  [dbo].[GET_GROUP_NAMES_P] TO [CBMSApplication]
GO
