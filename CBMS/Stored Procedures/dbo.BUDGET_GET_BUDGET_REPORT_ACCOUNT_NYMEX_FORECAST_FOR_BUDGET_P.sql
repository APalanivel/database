SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--exec BUDGET_GET_BUDGET_REPORT_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P '','',1259,'01/01/2006', '12/01/2006' ,10003,null,null,290
--exec BUDGET_GET_BUDGET_REPORT_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P '','',705,'01/01/2007', '12/01/2007'
CREATE           PROCEDURE dbo.BUDGET_GET_BUDGET_REPORT_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P
	@user_id varchar(10),
	@session_id varchar(20),
	@budget_id int,
	@from_month datetime, 
	@to_month datetime ,
	@clientId int,
	@divisionId int,
	@siteId int ,
	@commodityId int

	AS
	begin
	set nocount on

	select	budget_nymex_forecast.month_identifier as monthIdentifier,
		budget_nymex_forecast.price as forecastPrice,
		ba.budget_account_id as budgetAccountId
	from 	budget b
		join budget_account ba  on  b.budget_id =  ba.budget_id and ba.budget_id = @budget_id
		join budget_nymex_forecast on budget_nymex_forecast.account_id = ba.account_id	and budget_nymex_forecast.commodity_type_id = b.commodity_type_id 
		join BUDGET_ACCOUNT_TYPE_VW type_vw on type_vw.budget_id = ba.budget_id and type_vw.budget_account_id = ba.budget_account_id --and type_vw.budget_account_type in('A&B', 'C&D Created')
		join ACCOUNT ACCT on  acct.account_id = ba.account_id 
		JOIN SITE S WITH(NOLOCK) ON (ACCT.site_id = s.site_id and S.SITE_ID = COALESCE(@siteId, s.site_id))
		JOIN DIVISION D WITH(NOLOCK) ON (D.DIVISION_ID = S.DIVISION_ID AND  D.DIVISION_ID = COALESCE(@divisionId, D.DIVISION_ID))
		JOIN CLIENT C WITH(NOLOCK) ON (C.CLIENT_ID = D.CLIENT_ID AND C.CLIENT_ID = COALESCE(@clientId, c.client_id))
	where 	b.commodity_type_id = @commodityId
		and budget_nymex_forecast.month_identifier between @from_month and @to_month
		
			 
	
	end
GO
GRANT EXECUTE ON  [dbo].[BUDGET_GET_BUDGET_REPORT_ACCOUNT_NYMEX_FORECAST_FOR_BUDGET_P] TO [CBMSApplication]
GO
