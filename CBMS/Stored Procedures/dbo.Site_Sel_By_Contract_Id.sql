SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Site_Sel_By_Contract_Id]  
     
DESCRIPTION:

	To Get distinct site list of the given contract_id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
	@Contract_Id	INT			  		
               
OUTPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        

	EXEC dbo.Site_Sel_By_Contract_Id 80009
	
	EXEC dbo.Site_Sel_By_Contract_Id 79973
	
AUTHOR INITIALS:
	INITIALS	NAME
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	HG			25-JUN-10	CREATED

*/
CREATE PROCEDURE dbo.Site_Sel_By_Contract_Id
	@Contract_Id	INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		s.Site_Id
		,s.Site_Name
	FROM
		dbo.Site s
		JOIN dbo.Account uacc
			ON uacc.SITE_ID = s.Site_Id
		JOIN dbo.METER m
			ON m.ACCOUNT_ID = uacc.ACCOUNT_ID
		JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
			ON m.METER_ID = samm.METER_ID
	WHERE
		samm.Contract_ID = @Contract_Id
	GROUP BY
		s.SITE_ID
		,s.SITE_NAME
END
GO
GRANT EXECUTE ON  [dbo].[Site_Sel_By_Contract_Id] TO [CBMSApplication]
GO
