SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Invoice_Collection_Account_Contact_Sel            
                        
 DESCRIPTION:                        
			To get the details of Invoice_Collection_Account_Contact_Sel               
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
@Invoice_Collection_Account_Config_Id INT
@Contact_Level_Cd INT NULL
@Client_Id INT = NULL
@Vendor_Id INT = NULL                             
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            
--All three at once
 
  EXEC [dbo].[Invoice_Collection_Account_Contact_Sel] 
      @Invoice_Collection_Account_Config_Id = 178

 --Client Level
 
  EXEC [dbo].[Invoice_Collection_Account_Contact_Sel] 
      @Invoice_Collection_Account_Config_Id = 1
     ,@Contact_Level_Cd =102255
      
--Vendor level

  EXEC [dbo].[Invoice_Collection_Account_Contact_Sel] 
	   @Invoice_Collection_Account_Config_Id = 1
      ,@Contact_Level_Cd =102254
--Account level

  EXEC [dbo].[Invoice_Collection_Account_Contact_Sel] 
	   @Invoice_Collection_Account_Config_Id = 1
      ,@Contact_Level_Cd =102257 


    EXEC dbo.CODE_SEL_BY_CodeSet_Name 
       @CodeSet_Name = 'ContactLevel'  
                           
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam          
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-21       Created for Invoice Tracking               
                       
******/    
            
CREATE PROCEDURE [dbo].[Invoice_Collection_Account_Contact_Sel]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@Contact_Level_Cd INT = NULL
      ,@Client_Id INT = NULL
      ,@Vendor_Id INT = NULL )
AS 
BEGIN                
      SET NOCOUNT ON;  

      DECLARE
            @Client_Name VARCHAR(200)
           ,@Vendor_Name VARCHAR(200)
           ,@Account_Id INT

		  
      SELECT
            @Account_Id = ica.Account_Id
      FROM
            dbo.Invoice_Collection_Account_Config ica
      WHERE
            ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id	   
		      
      SELECT TOP 1
            @Client_Name = ch.Client_Name
      FROM
            Core.Client_Hier ch
            INNER JOIN core.Client_Hier_Account cha
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
      WHERE
            cha.Account_Id = @Account_Id
     
      SELECT
            ci.Contact_Info_Id
           ,ci.Email_Address
           ,ci.Fax_Number
           ,ci.Phone_Number
           ,ci.First_Name
           ,ci.Last_Name
           ,ci.Job_Position
           ,ci.Location
           ,c.Comment_Text
           ,c.Comment_ID
           ,ica.Is_Primary
           ,ica.Is_Include_In_CC AS Is_CC
           ,cl.Code_Value AS Contact_Level
           ,@Client_Name AS Client_Name
           ,v.VENDOR_NAME AS Vendor_Name
           ,v.VENDOR_ID
           ,v.VENDOR_TYPE_ID
           ,lc.Code_Dsc AS Language_Cd_Value
      FROM
            dbo.Invoice_Collection_Account_Contact ica
            INNER JOIN dbo.Contact_Info ci
                  ON ica.Contact_Info_Id = ci.Contact_Info_Id
            LEFT JOIN dbo.Vendor_Contact_Map vcm
                  ON ci.Contact_Info_Id = vcm.Contact_Info_Id
            LEFT JOIN dbo.VENDOR v
                  ON vcm.VENDOR_ID = v.VENDOR_ID
            LEFT JOIN dbo.Comment c
                  ON ci.Comment_Id = c.Comment_ID
            LEFT JOIN dbo.Code lc
                  ON ci.Language_Cd = lc.Code_Id
            LEFT JOIN dbo.Code cl
                  ON ci.Contact_Level_Cd = cl.Code_Id
      WHERE
            ica.Invoice_Collection_Account_Config_Id = @Invoice_Collection_Account_Config_Id
            AND ( @Contact_Level_Cd IS NULL
                  OR ci.Contact_Level_Cd = @Contact_Level_Cd )

END;

;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Account_Contact_Sel] TO [CBMSApplication]
GO
