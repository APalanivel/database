SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   [dbo].[Sr_Service_Condition_Template_Upd]
           
DESCRIPTION:             
			To upadte service condition template details
			
INPUT PARAMETERS:            
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Template_Name						NVARCHAR(255)
    @Is_Active							BIT				1
    @User_Info_Id						INT
    @Commodity_Id						INT
    @Country_Id							VARCHAR(MAX)
    @SR_PRICING_PRODUCT_ID				VARCHAR(MAX)
    @Sr_Service_Condition_Template_Id	INT
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  
	

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	
	BEGIN TRAN
		SELECT * FROM dbo.Sr_Service_Condition_Template where Template_Name = 'Test Service Condition Template'
		DECLARE @Template_Id Int
			
        EXEC dbo.Sr_Service_Condition_Template_Ins 
			@Template_Name = 'Test Service Condition Template'
			,@Is_Active = 1
			,@User_Info_Id = 16
			,@Sr_Service_Condition_Template_Id = @Template_Id OUT
			,@Commodity_Id =290
			,@Country_Id = '4,6'
			,@SR_PRICING_PRODUCT_ID ='155,156'
		SELECT * FROM dbo.Sr_Service_Condition_Template a 
			JOIN dbo.Sr_Service_Condition_Template_Product_Map b ON a.Sr_Service_Condition_Template_Id = b.Sr_Service_Condition_Template_Id
			JOIN dbo.SR_Pricing_Product_Country_Map c ON b.SR_Pricing_Product_Country_Map_Id = c.SR_Pricing_Product_Country_Map_Id
			where Template_Name = 'Test Service Condition Template'
			
		EXEC dbo.Sr_Service_Condition_Template_Upd 
			@Template_Name = 'Test Service Condition Template Update'
			,@Is_Active = 1
			,@User_Info_Id = 49
			,@Sr_Service_Condition_Template_Id = @Template_Id
			,@Commodity_Id =290
			,@Country_Id = '6,9'
			,@SR_PRICING_PRODUCT_ID ='155,156'
		SELECT * FROM dbo.Sr_Service_Condition_Template a 
			JOIN dbo.Sr_Service_Condition_Template_Product_Map b ON a.Sr_Service_Condition_Template_Id = b.Sr_Service_Condition_Template_Id
			JOIN dbo.SR_Pricing_Product_Country_Map c ON b.SR_Pricing_Product_Country_Map_Id = c.SR_Pricing_Product_Country_Map_Id
			where Template_Name = 'Test Service Condition Template Update'
	ROLLBACK
		
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-03-31	Global Sourcing - Phase3 - GCS-602 Created
******/
CREATE PROCEDURE [dbo].[Sr_Service_Condition_Template_Upd]
      ( 
       @Template_Name NVARCHAR(255)
      ,@Is_Active BIT = 1
      ,@User_Info_Id INT
      ,@Sr_Service_Condition_Template_Id INT
      ,@Commodity_Id INT
      ,@Country_Id VARCHAR(MAX)
      ,@SR_PRICING_PRODUCT_ID VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Tbl_Template_Pricing_Product_Country AS TABLE
            ( 
             Sr_Service_Condition_Template_Id INT
            ,SR_Pricing_Product_Country_Map_Id INT )
      
      UPDATE
            dbo.Sr_Service_Condition_Template
      SET   
            Template_Name = isnull(@Template_Name, Template_Name)
           ,Is_Active = @Is_Active
           ,Updated_User_Id = @User_Info_Id
           ,Last_Change_Ts = getdate()
      WHERE
            Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                  
      INSERT      INTO @Tbl_Template_Pricing_Product_Country
                  ( 
                   Sr_Service_Condition_Template_Id
                  ,SR_Pricing_Product_Country_Map_Id )
                  SELECT
                        @Sr_Service_Condition_Template_Id
                       ,sppcm.SR_Pricing_Product_Country_Map_Id
                  FROM
                        dbo.SR_PRICING_PRODUCT srpr
                        INNER JOIN dbo.SR_Pricing_Product_Country_Map sppcm
                              ON srpr.SR_PRICING_PRODUCT_ID = sppcm.SR_PRICING_PRODUCT_ID
                  WHERE
                        srpr.COMMODITY_TYPE_ID = @Commodity_Id
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.ufn_split(@Country_Id, ',')
                                     WHERE
                                          sppcm.Country_ID = cast(Segments AS INT) )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.ufn_split(@SR_PRICING_PRODUCT_ID, ',')
                                     WHERE
                                          srpr.SR_PRICING_PRODUCT_ID = cast(Segments AS INT) )
                                          
      DELETE
            ssctpm
      FROM
            dbo.Sr_Service_Condition_Template_Product_Map ssctpm
      WHERE
            ssctpm.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              @Tbl_Template_Pricing_Product_Country tppc
                             WHERE
                              tppc.Sr_Service_Condition_Template_Id = @Sr_Service_Condition_Template_Id
                              AND ssctpm.SR_Pricing_Product_Country_Map_Id = tppc.SR_Pricing_Product_Country_Map_Id )
      
      INSERT      INTO dbo.Sr_Service_Condition_Template_Product_Map
                  ( 
                   Sr_Service_Condition_Template_Id
                  ,SR_Pricing_Product_Country_Map_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        Sr_Service_Condition_Template_Id
                       ,SR_Pricing_Product_Country_Map_Id
                       ,@User_Info_Id
                       ,getdate()
                       ,@User_Info_Id
                       ,getdate()
                  FROM
                        @Tbl_Template_Pricing_Product_Country tppc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Sr_Service_Condition_Template_Product_Map ssctpm
                                     WHERE
                                          ssctpm.Sr_Service_Condition_Template_Id = tppc.Sr_Service_Condition_Template_Id
                                          AND ssctpm.SR_Pricing_Product_Country_Map_Id = tppc.SR_Pricing_Product_Country_Map_Id )
                  
      
      
            
END;
;
GO
GRANT EXECUTE ON  [dbo].[Sr_Service_Condition_Template_Upd] TO [CBMSApplication]
GO
