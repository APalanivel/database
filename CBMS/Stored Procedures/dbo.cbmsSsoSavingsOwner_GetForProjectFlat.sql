SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:
	dbo.cbmsSsoSavingsOwner_GetForProjectFlat

DESCRIPTION:

INPUT PARAMETERS:
	Name				DataType		Default	Description
------------------------------------------------------------
    @sso_savings_id		INT    	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.cbmsSsoSavingsOwner_GetForProjectFlat 207
	EXEC dbo.cbmsSsoSavingsOwner_GetForProjectFlat 22
	EXEC dbo.cbmsSsoSavingsOwner_GetForProjectFlat 451



	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	NR			Narayaan Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CPE			04/05/2011	Replaced vwCbmsSSOSavingsOwnerFlat with SSO_SAVINGS_OWNER_MAP table.
	NR			2014-11-27	Added the  Site_Reference_Number column in select list.
******/

CREATE PROCEDURE [dbo].[cbmsSsoSavingsOwner_GetForProjectFlat] ( @sso_savings_id INT )
AS 
BEGIN

      SELECT
            CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Id
              WHEN 'Division' THEN CH.Sitegroup_Id
              WHEN 'Site' THEN CH.Site_Id
            END AS owner_id
           ,CASE CD.Code_Value
              WHEN 'Corporate' THEN CH.Client_Name
              WHEN 'Division' THEN CH.Sitegroup_Name
              WHEN 'Site' THEN RTRIM(CH.City) + ', ' + CH.State_Name + ' (' + CH.Site_name + ')'
            END AS owner_name
           ,Ent.ENTITY_ID AS sso_owner_type_id
           ,Ent.ENTITY_NAME AS owner_type
           ,CH.Client_Id
           ,NULLIF(CH.Sitegroup_Id, 0) AS division_id
           ,NULLIF(CH.Site_Id, 0) AS Site_Id
           ,CD.Display_Seq AS owner_sort
           ,ch.Site_Reference_Number
      FROM
            dbo.SSO_SAVINGS_OWNER_MAP SSOM
            JOIN Core.Client_Hier CH
                  ON SSOM.Client_Hier_Id = CH.Client_Hier_Id
            JOIN dbo.Code CD
                  ON CH.Hier_level_Cd = CD.Code_Id
            JOIN dbo.ENTITY Ent
                  ON CD.Code_Dsc = Ent.ENTITY_NAME
            JOIN dbo.Codeset CS
                  ON CD.Codeset_Id = CS.Codeset_Id
                     AND Ent.ENTITY_DESCRIPTION = 'SSO Owner Type'
                     AND CS.Codeset_Name = 'HierLevel'
      WHERE
            SSOM.SSO_SAVINGS_ID = @sso_savings_id
      ORDER BY
            owner_sort
           ,owner_name

END


;
GO

GRANT EXECUTE ON  [dbo].[cbmsSsoSavingsOwner_GetForProjectFlat] TO [CBMSApplication]
GO
