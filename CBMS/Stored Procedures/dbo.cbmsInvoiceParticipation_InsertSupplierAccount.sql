SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvoiceParticipation_InsertSupplierAccount

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@user_info_id  	int       	          	
	@account_id    	int       	          	
	@site_id       	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE        PROCEDURE [dbo].[cbmsInvoiceParticipation_InsertSupplierAccount]
	( @user_info_id int
	, @account_id int
	, @site_id int
	)
AS
BEGIN

	set nocount on

	insert into invoice_participation_queue
		( event_type, client_id, division_id, site_id, account_id
		, service_month, event_by_id, event_date
		)
	   select 10
		, null client_id
		, null division_id
		, ip.site_id
		, ip.account_id
		, ip.service_month
		, 93
		, getdate()
	     from vwCbmsAccountSite vas
	     join invoice_participation ip on ip.account_id = vas.account_id and ip.site_id = vas.site_id
	    where vas.account_type_id = 37
	      and vas.account_id = @account_id
	      and vas.site_id = @site_id
	      and ip.is_received = 0
	      and ip.is_expected = 1


	EXEC cbmsInvoiceParticipationQueue_Save
		  @user_info_id
		, 2 -- see called sproc for definition
		, null
		, null
		, @site_id 
		, @account_id 
		, null
		, 1

--	set nocount off

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_InsertSupplierAccount] TO [CBMSApplication]
GO
