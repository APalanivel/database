SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE [dbo].[cbmsInvSourceEmailLog_Get]
	( @MyAccountId int
	, @inv_source_email_log_id int
	)
AS
BEGIN

	   select inv_source_email_log_id
		, inv_sourced_image_batch_id
		, originating_email_id
		, from_name
		, from_address
		, subject
		, received_date
	     from inv_source_email_log with (nolock)
	    where inv_source_email_log_id = @inv_source_email_log_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourceEmailLog_Get] TO [CBMSApplication]
GO
