SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[CU_Invoice_Dtl_Sel_By_Account_Id]  

DESCRIPTION:
	To Get Invoices Details for Selected Account Id.

INPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION 
------------------------------------------------------------
@Account_Id		INT						Utility Account
@StartIndex		INT			1
@EndIndex		INT			2147483647

OUTPUT PARAMETERS:
NAME			DATATYPE	DEFAULT		DESCRIPTION
------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

	SET STATISTICS IO ON

	EXEC CU_Invoice_Dtl_Sel_By_Account_Id  101527, 1,15
	EXEC CU_Invoice_Dtl_Sel_By_Account_Id  116867

AUTHOR INITIALS:
INITIALS	NAME
------------------------------------------------------------
PNR			PANDARINATH

MODIFICATIONS
INITIALS	DATE		MODIFICATION
------------------------------------------------------------
PNR			25-MAY-10	CREATED
						Case Condition added in the ORDER BY clause of ROW_NUMBER to sort NULL fields last.
*/

CREATE PROCEDURE dbo.CU_Invoice_Dtl_Sel_By_Account_Id
(
	@Account_Id		INT
	,@StartIndex	INT		= 1
	,@EndIndex		INT		= 2147483647
)
AS
BEGIN

	SET NOCOUNT ON;
	
	WITH Cte_Invoice_List AS
	(
		SELECT
			ci.CU_INVOICE_ID
			,ci.BEGIN_DATE
			,ci.END_DATE
			,cuim.SERVICE_MONTH
			,ci.IS_PROCESSED
			,ci.IS_DUPLICATE
			,ROW_NUMBER() OVER( ORDER BY (CASE WHEN cuim.Service_Month IS NULL THEN 1 ELSE 0 END), cuim.Service_Month) Row_Num
			,COUNT(1) OVER() Total_Rows
		FROM
			dbo.CU_INVOICE_SERVICE_MONTH cuim
  			JOIN dbo.CU_INVOICE ci
				 ON cuim.CU_INVOICE_ID = ci.CU_INVOICE_ID
		WHERE
			cuim.ACCOUNT_ID = @Account_Id
		GROUP BY
			ci.CU_INVOICE_ID
			,ci.BEGIN_DATE
			,ci.END_DATE
			,cuim.SERVICE_MONTH
			,ci.IS_PROCESSED
			,ci.IS_DUPLICATE
	)
	SELECT
		CU_INVOICE_ID
		,BEGIN_DATE
		,END_DATE
		,Service_Month
		,IS_PROCESSED
		,IS_DUPLICATE
		,Total_Rows
	FROM
		Cte_Invoice_List
	WHERE
		Row_Num BETWEEN @StartIndex AND @EndIndex

END
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Dtl_Sel_By_Account_Id] TO [CBMSApplication]
GO
