SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******              
Name:   dbo.Report_DE_Wrong_Month_Invoice       
              
Description:              
			This sproc is used to get the Invoices assigned to the wrong month
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	                 
                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.Report_DE_Wrong_Month_Invoice 
   
   
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-27		Created.
******/
CREATE PROCEDURE [dbo].[Report_DE_Wrong_Month_Invoice] ( @No_Of_Days INT )
AS 
BEGIN

      SET NOCOUNT ON;

      IF OBJECT_ID('tempdb.dbo.#Wrong_Month_Invoices', 'U') IS NOT NULL 
            BEGIN 
                  DROP TABLE #Wrong_Month_Invoices;
            END;
      
      IF OBJECT_ID('tempdb.dbo.#Wrong_Month_Invoice_For_Multi_Months', 'U') IS NOT NULL 
            BEGIN 
                  DROP TABLE #Wrong_Month_Invoice_For_Multi_Months;
            END;

      DECLARE @Report_Date DATE  = DATEADD(dd, -@No_Of_Days, GETDATE()) --'2016-10-23'


      CREATE TABLE #Wrong_Month_Invoices
            ( 
             Cu_Invoice_Id INT
            ,Begin_Dt DATE
            ,End_Dt DATE
            ,Actual_Month DATE
            ,Current_Month DATE
            ,Modified_By_User_Info_Id INT )

      CREATE TABLE #Wrong_Month_Invoice_For_Multi_Months
            ( 
             Cu_Invoice_Id INT
            ,Actual_Month DATE
            ,Current_Month_Cism DATE
            ,Modified_By_User_Info_Id INT )  


      INSERT      INTO #Wrong_Month_Invoices
                  ( 
                   Cu_Invoice_Id
                  ,Begin_Dt
                  ,End_Dt
                  ,Actual_Month
                  ,Current_Month
                  ,Modified_By_User_Info_Id )
                  SELECT
                        cism.CU_INVOICE_ID
                       ,cism.Begin_Dt
                       ,cism.End_Dt
                       ,CASE WHEN DAYS_IN_MONTH_NUM = 31
                                  AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 16 THEN dd.FIRST_DAY_OF_MONTH_D
                             WHEN DAYS_IN_MONTH_NUM = 30
                                  AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                             WHEN DAYS_IN_MONTH_NUM = 29
                                  AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                             WHEN DAYS_IN_MONTH_NUM = 28
                                  AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 14 THEN dd.FIRST_DAY_OF_MONTH_D
                             ELSE DATEADD(mm, 1, dd.FIRST_DAY_OF_MONTH_D)
                        END Actual_Month
                       ,cism.SERVICE_MONTH Current_Month_Cism
                       ,ci.UPDATED_BY_ID
                  FROM
                        dbo.CU_INVOICE_SERVICE_MONTH cism
                        INNER JOIN dbo.CU_INVOICE ci
                              ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                        INNER JOIN meta.DATE_DIM dd
                              ON dd.DATE_D = DATEADD(mm, DATEDIFF(mm, 0, cism.Begin_Dt), 0)
                  WHERE
                        ci.UPDATED_DATE >= @Report_Date
                        AND cism.SERVICE_MONTH <> CASE WHEN DAYS_IN_MONTH_NUM = 31
                                                            AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 16 THEN dd.FIRST_DAY_OF_MONTH_D
                                                       WHEN DAYS_IN_MONTH_NUM = 30
                                                            AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                                                       WHEN DAYS_IN_MONTH_NUM = 29
                                                            AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                                                       WHEN DAYS_IN_MONTH_NUM = 28
                                                            AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 14 THEN dd.FIRST_DAY_OF_MONTH_D
                                                       ELSE DATEADD(mm, 1, dd.FIRST_DAY_OF_MONTH_D)
                                                  END
                        AND CASE WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 25
                                      AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 36 THEN 1
                                 WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 56
                                      AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 65 THEN 2
                                 WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 87
                                      AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 96 THEN 3
                                 WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 118
                                      AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 126 THEN 4
                            END = 1
        

 
      INSERT      INTO #Wrong_Month_Invoice_For_Multi_Months
                  ( 
                   Cu_Invoice_Id
                  ,Actual_Month
                  ,Current_Month_Cism
                  ,Modified_By_User_Info_Id )
                  SELECT
                        X.CU_INVOICE_ID
                       ,x.Actual_Month
                       ,x.Current_Month_Cism
                       ,x.UPDATED_BY_ID
                  FROM
                        ( SELECT
                              cism.CU_INVOICE_ID
                             ,CASE WHEN DAYS_IN_MONTH_NUM = 31
                                        AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 16 THEN dd.FIRST_DAY_OF_MONTH_D
                                   WHEN DAYS_IN_MONTH_NUM = 30
                                        AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                                   WHEN DAYS_IN_MONTH_NUM = 29
                                        AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 15 THEN dd.FIRST_DAY_OF_MONTH_D
                                   WHEN DAYS_IN_MONTH_NUM = 28
                                        AND DATEDIFF(Dd, dd.FIRST_DAY_OF_MONTH_D, cism.Begin_Dt) + 1 <= 14 THEN dd.FIRST_DAY_OF_MONTH_D
                                   ELSE DATEADD(mm, 1, dd.FIRST_DAY_OF_MONTH_D)
                              END Actual_Month
                             ,cism.SERVICE_MONTH Current_Month_Cism
                             ,ci.UPDATED_BY_ID
                             ,ROW_NUMBER() OVER ( PARTITION BY cism.CU_INVOICE_ID ORDER BY cism.SERVICE_MONTH ) row_num
                          FROM
                              dbo.CU_INVOICE_SERVICE_MONTH cism
                              INNER JOIN dbo.CU_INVOICE ci
                                    ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                              INNER JOIN meta.DATE_DIM dd
                                    ON dd.DATE_D = DATEADD(mm, DATEDIFF(mm, 0, cism.Begin_Dt), 0)
                          WHERE
                              ci.UPDATED_DATE >= @Report_Date
                              AND CASE WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 25
                                            AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 36 THEN 1
                                       WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 56
                                            AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 65 THEN 2
                                       WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 87
                                            AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 96 THEN 3
                                       WHEN DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) >= 118
                                            AND DATEDIFF(Dd, cism.Begin_Dt, cism.End_Dt) <= 126 THEN 4
                                  END > 1 ) x
                  WHERE
                        x.row_num = 1
                        AND x.Actual_Month <> x.Current_Month_Cism
          
      INSERT      INTO #Wrong_Month_Invoices
                  ( 
                   Cu_Invoice_Id
                  ,Begin_Dt
                  ,End_Dt
                  ,Actual_Month
                  ,Current_Month
                  ,Modified_By_User_Info_Id )
                  SELECT
                        x.Cu_Invoice_Id
                       ,x.Begin_Dt
                       ,x.End_Dt
                       ,DATEADD(mm, x.row_num - 1, x.Actual_Month) Actual_Month
                       ,x.Current_Month_Cism
                       ,x.Modified_By_User_Info_Id
                  FROM
                        ( SELECT
                              wmifmm.Cu_Invoice_Id
                             ,cism.Begin_Dt
                             ,cism.End_Dt
                             ,wmifmm.Actual_Month
                             ,cism.SERVICE_MONTH Current_Month_Cism
                             ,wmifmm.Modified_By_User_Info_Id
                             ,ROW_NUMBER() OVER ( PARTITION BY wmifmm.CU_INVOICE_ID ORDER BY cism.SERVICE_MONTH ) row_num
                          FROM
                              #Wrong_Month_Invoice_For_Multi_Months wmifmm
                              INNER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                                    ON wmifmm.Cu_Invoice_Id = cism.CU_INVOICE_ID ) x


      SELECT
            Cu_Invoice_Id
           ,Begin_Dt
           ,End_Dt
           ,Actual_Month
           ,Current_Month
           ,Modified_By_User_Info_Id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME UserName
      FROM
            #Wrong_Month_Invoices wmi
            INNER JOIN dbo.USER_INFO ui
                  ON wmi.Modified_By_User_Info_Id = ui.USER_INFO_ID
      GROUP BY
            Cu_Invoice_Id
           ,Begin_Dt
           ,End_Dt
           ,Actual_Month
           ,Current_Month
           ,Modified_By_User_Info_Id
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME
      ORDER BY
            Cu_Invoice_Id
           ,Actual_Month
END 

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_Wrong_Month_Invoice] TO [CBMSApplication]
GO
