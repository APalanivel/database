SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
	 dbo.Tool_For_Get_Rolling_Meters    
    
DESCRIPTION:    
    
 Rolling_Meters.
     
    
INPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
 
    
OUTPUT PARAMETERS:    
 Name					DataType	 Default		Description    
--------------------------------------------------------------------    
    
USAGE EXAMPLES:    
--------------------------------------------------------------------    

EXEC dbo.Tool_For_Get_Rolling_Meters




    
AUTHOR INITIALS:    
	Initials		Name    
--------------------------------------------------------------------    
    NR				Narayana Reddy       
    
MODIFICATIONS    
    
 Initials		Date			Modification    
--------------------------------------------------------------------    
 NR				2019-05-30		Created For - Add Contract Project.
  
******/

CREATE PROC [dbo].[Tool_For_Get_Rolling_Meters]
    (
        @Date DATE = NULL
    )
AS
    SET NOCOUNT ON;

    BEGIN

        DECLARE @Last_Running_Date DATE;



        SET @Date = ISNULL(@Date, CONVERT(DATE, GETDATE()));



        SELECT
            @Last_Running_Date = CONVERT(DATE, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Rolling_Meter';




        SELECT
            samm.ACCOUNT_ID
            , samm.Contract_ID
            , samm.METER_ID
            , samm.METER_ASSOCIATION_DATE
            , samm.METER_DISASSOCIATION_DATE
        FROM
            dbo.SUPPLIER_ACCOUNT_METER_MAP samm
        WHERE
            samm.Is_Rolling_Meter = 1
            AND CAST(samm.METER_DISASSOCIATION_DATE AS DATE) BETWEEN @Last_Running_Date
                                                             AND     @Date;

    END;



GO
GRANT EXECUTE ON  [dbo].[Tool_For_Get_Rolling_Meters] TO [CBMSApplication]
GO
