SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE      PROCEDURE [dbo].[cbmsSsoProjectActivity_GetByProject]
	( @MyAccountId int
	, @project_id int
	
	)
AS
BEGIN

	  
	   select pa.sso_project_activity_id
		, pa.sso_project_id
		, pa.created_by_id
		, pa.activity_date
		, pa.activity_description
		, usr.first_name
		, usr.last_name
		, usr.first_name + ' ' + usr.last_name as full_name
	     from sso_project_activity pa 
	     join user_info usr on pa.created_by_id = usr.user_info_id
	    where sso_project_id = @project_id
	 order by pa.activity_date desc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoProjectActivity_GetByProject] TO [CBMSApplication]
GO
