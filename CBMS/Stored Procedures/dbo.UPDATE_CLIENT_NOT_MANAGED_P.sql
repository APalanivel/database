
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.UPDATE_CLIENT_NOT_MANAGED_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.UPDATE_CLIENT_NOT_MANAGED_P
	@userId = 1,
	@sessionId = -1,
	@clientId = 102

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CH			Chad Hattabaugh
   DR       Deana Ritter
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	 CH			06/22/2009	Modified for Sitegroup divsion table split
	 DR       	07/23/2009  Modified to reinstate dual update to DV for
	                        Account table only.  Removed during next phase
	                        of SV decoupling.
	 DMR		  09/10/2010 Modified for Quoted_Identifier
	 RKV        2017-05-26   Added code to delete IC_Client_Configuration


******/
CREATE PROCEDURE [dbo].[UPDATE_CLIENT_NOT_MANAGED_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@clientId INT )
AS 
BEGIN 
      SET NOCOUNT ON
	
      UPDATE
            dbo.Division_Dtl
      SET   
            not_managed = 1
           ,not_managed_date = GETDATE()
           ,not_managed_by_id = @userId
      WHERE
            Sitegroup_id IN ( SELECT
                                    Sitegroup_id
                              FROM
                                    dbo.sitegroup
                              WHERE
                                    client_id = @clientId )
            AND not_managed = 0

      UPDATE
            site
      SET   
            not_managed = 1
           ,closed = 1
           ,closed_date = GETDATE()
      WHERE
            division_id IN ( SELECT
                              division_id
                             FROM
                              division
                             WHERE
                              client_id = @clientId )
            AND not_managed = 0

      UPDATE
            account
      SET   
            not_managed = 1
           ,not_expected = 1
           ,not_expected_date = GETDATE()
      WHERE
            site_id IN ( SELECT
                              site_id
                         FROM
                              site
                         WHERE
                              division_id IN ( SELECT
                                                division_id
                                               FROM
                                                division
                                               WHERE
                                                client_id = @clientId ) )
            AND not_managed = 0

      UPDATE
            account
      SET   
            not_managed = 1
           ,not_expected = 1
           ,not_expected_date = GETDATE()
      WHERE
            account_id IN ( SELECT DISTINCT
                              suppacc.ACCOUNT_ID
                            FROM
                              account utilacc
                             ,account suppacc
                             ,vendor ven
                             ,contract con
                             ,meter met
                             ,supplier_account_meter_map map
                            WHERE
                              map.meter_id = met.meter_id
                              AND met.account_id = utilacc.account_id
                              AND utilacc.site_id IN ( SELECT
                                                            site_id
                                                       FROM
                                                            site
                                                       WHERE
                                                            division_id IN ( SELECT
                                                                              division_id
                                                                             FROM
                                                                              division
                                                                             WHERE
                                                                              client_id = @clientId ) ) --@siteId 
                              AND map.account_id = suppacc.account_id )
            AND not_managed = 0

      EXEC Invoice_Collection_Client_Config_Del_by_Client_Id 
            @clientId
		
END

;
GO

GRANT EXECUTE ON  [dbo].[UPDATE_CLIENT_NOT_MANAGED_P] TO [CBMSApplication]
GO
