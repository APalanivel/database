SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE         procedure [dbo].[cbmsUtilityProfile_Save]
	( @MyAccountId int
        ,@utility_profile_id int
	,@vendor_id int
	,@country_id int
	,@state_id int
	,@title varchar (200) = null
	,@commodity_type_id int
	,@cbms_image_id int
	,@last_updated_by int
	)
AS
BEGIN

	set nocount on
	
	declare @this_id int
		set @this_id = @utility_profile_id

	if @this_id is null
	begin
	
		insert into utility_profile
			(
			vendor_id
			, country_id
			, state_id
			, title
			, commodity_type_id
			, cbms_image_id
			, last_updated_by
			, last_updated_date
			)	
		values
			(
			@vendor_id
			, @country_id
			, @state_id
			, @title
			, @commodity_type_id
			, @cbms_image_id
			, @last_updated_by
			, getdate()
			)

		set @this_id = @@IDENTITY
	end
	else
	begin

		update utility_profile
		set --vendor_id = @vendor_id
			--, country_id = @country_id
			--, state_id = @state_id
			 title = @title
			, commodity_type_id = @commodity_type_id
			, cbms_image_id = @cbms_image_id
			, last_updated_by = @last_updated_by
			, last_updated_date = getdate()
		where utility_profile_id = @this_id

	end

	exec cbmsUtilityProfile_Get @MyAccountid, @this_id


END
GO
GRANT EXECUTE ON  [dbo].[cbmsUtilityProfile_Save] TO [CBMSApplication]
GO
