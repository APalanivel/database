SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
                         
 NAME: dbo.Supplier_Account_Contract_Dates_Sel_By_Contract_Account_Id                     
                          
 DESCRIPTION:      
		Get the Suppler account dates.               
                          
 INPUT PARAMETERS:      
                         
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------  
 @Account_Id			INT        
                          
 OUTPUT PARAMETERS:      
                               
 Name                               DataType          Default       Description      
-------------------------------------------------------------------------------------                            
 USAGE EXAMPLES:                              
-------------------------------------------------------------------------------------                 


EXEC dbo.Supplier_Account_Contract_Dates_Sel_By_Contract_Account_Id
    @Contract_Id = 1
    , @Account_Id = 1


 AUTHOR INITIALS:    
       
 Initials                   Name      
-------------------------------------------------------------------------------------  
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:    
                           
 Initials               Date            Modification    
-------------------------------------------------------------------------------------  
 NR                     2019-06-13      Created for ADD Contract.                        
                         
******/

CREATE PROCEDURE [dbo].[Supplier_Account_Contract_Dates_Sel_By_Contract_Account_Id]
    (
        @Contract_Id INT
        , @Account_Id INT = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            cha.Account_Id
            , c.CONTRACT_ID
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN dbo.CONTRACT c
                ON c.CONTRACT_ID = cha.Supplier_Contract_ID
        WHERE
            (   @Account_Id IS NULL
                OR  cha.Account_Id = @Account_Id)
            AND cha.Supplier_Contract_ID = @Contract_Id
        GROUP BY
            cha.Account_Id
            , c.CONTRACT_ID
            , cha.Supplier_Account_begin_Dt
            , cha.Supplier_Account_End_Dt
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;



    END;



GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Contract_Dates_Sel_By_Contract_Account_Id] TO [CBMSApplication]
GO
