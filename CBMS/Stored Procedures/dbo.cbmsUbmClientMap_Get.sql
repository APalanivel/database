SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  PROCEDURE [dbo].[cbmsUbmClientMap_Get]
	( @MyAccountId int
	, @ubm_client_map_id int
	)
AS
BEGIN

	   select ubm_client_map_id
		, ubm_id
		, ubm_client_code
		, client_id
		, is_processed
	     from ubm_client_map
	    where ubm_client_map_id = @ubm_client_map_id

END
GO
GRANT EXECUTE ON  [dbo].[cbmsUbmClientMap_Get] TO [CBMSApplication]
GO
