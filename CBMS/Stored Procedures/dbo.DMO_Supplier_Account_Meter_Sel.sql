SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.DMO_Supplier_Account_Meter_Sel
           
DESCRIPTION:             
			To select applicable meters for DMO supplier account
			
INPUT PARAMETERS:            
	Name				DataType		Default		Description  
---------------------------------------------------------------------------------  
	@Client_Id			INT
    @Commodity_Id		INT
    @DMO_Supp_Start_Dt	DATE
    @DMO_Supp_End_Dt	DATE			NULL
    @Site_Id			INT				NULL
    @City				VARCHAR(200)	NULL
    @State_Id			INT				NULL
    @Account_Vendor_Id	INT				NULL
    @Account_Number		VARCHAR(50)		NULL
    @Meter_Number		VARCHAR(50)		NULL
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
	SELECT a.*,b.Client_Id,b.Sitegroup_Id,b.Site_Id FROM dbo.Client_Hier_DMO_Config a 
	INNER JOIN Core.Client_Hier b ON a.Client_Hier_Id = b.Client_Hier_Id
	--WHERE b.Sitegroup_Id>0
	--AND b.Site_Id > 0
	ORDER BY b.Client_Id

    EXEC dbo.Client_Hier_DMO_Config_Sel 876
            
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 141,290,'2017-03-01',NULL,13719
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 235,290,'03/01/2017','04/30/2017',193656	
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 11987,290,'2017-01-01','2017-07-01',314143,null,null,null,null,null

	EXEC dbo.DMO_Supplier_Account_Dates_Validation 11987,314143,290,'2017-01-01','2017-07-31'
	EXEC dbo.DMO_Supplier_Account_Meter_Sel 11987,290,'2017-01-01','2017-07-31',314143,null,null,null,null,null

		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-02-08	Contract placeholder - CP-8 Created
******/

CREATE PROCEDURE [dbo].[DMO_Supplier_Account_Meter_Sel]
      ( 
       @Client_Id INT
      ,@Commodity_Id INT
      ,@DMO_Supp_Start_Dt DATE
      ,@DMO_Supp_End_Dt DATE = NULL
      ,@Site_Id INT
      ,@City VARCHAR(200) = NULL
      ,@State_Id INT = NULL
      ,@Account_Vendor_Id INT = NULL
      ,@Account_Number VARCHAR(50) = NULL
      ,@Meter_Number VARCHAR(50) = NULL )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Is_DMO_Config_Dates_Valid BIT = 1
      
      DECLARE
            @Sitegroup_Id INT
           ,@Account_Hier_level_Cd INT
           
      SELECT
            @Client_Id = ch.Client_Id
           ,@Sitegroup_Id = ch.Sitegroup_Id
           ,@Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
      WHERE
            ch.Site_Id = @Site_Id
      
      
           
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_Id INT
            ,Sitegroup_Id INT
            ,Site_Id INT
            ,Site_Name VARCHAR(200)
            ,City VARCHAR(200)
            ,State_Id INT
            ,State_Name VARCHAR(200)
            ,Account_Id INT
            ,Utility_Account_Number VARCHAR(50)
            ,Meter_Id INT
            ,Meter_Number VARCHAR(50)
            ,Account_Vendor_Name VARCHAR(200)
            ,Rate_Name VARCHAR(200)
            ,Supplier_Account_Id INT
            ,Supplier_Account_Number VARCHAR(50)
            ,Supplier_Account_Begin_Dt DATE
            ,Supplier_Account_End_Dt DATE
            ,Supplier_Contract_ID INT
            ,Account_Invoice_Source_Cd INT
            ,Account_Invoice_Source VARCHAR(200) )
      
      DECLARE @Tbl_Mtrs AS TABLE
            ( 
             Meter_Id INT
            ,Supplier_Account_Id INT )
            
     
           
           
           
      
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_Id
                  ,Sitegroup_Id
                  ,Site_Id
                  ,Site_Name
                  ,City
                  ,State_Id
                  ,State_Name
                  ,Account_Id
                  ,Utility_Account_Number
                  ,Meter_Id
                  ,Meter_Number
                  ,Account_Vendor_Name
                  ,Rate_Name
                  ,Supplier_Account_Id
                  ,Supplier_Account_Number
                  ,Supplier_Account_Begin_Dt
                  ,Supplier_Account_End_Dt
                  ,Supplier_Contract_ID
                  ,Account_Invoice_Source_Cd
                  ,Account_Invoice_Source )
                  SELECT
                        chsite.Client_Hier_Id
                       ,chsite.Sitegroup_Id
                       ,chsite.Site_Id
                       ,chsite.Site_name
                       ,chsite.City
                       ,chsite.State_Id
                       ,chsite.State_Name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Meter_Id
                       ,cha.Meter_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Rate_Name
                       ,chasupp.Account_Id
                       ,chasupp.Account_Number
                       ,suppacc.Supplier_Account_Begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
                       ,chasupp.Supplier_Contract_ID
                       ,cha.Account_Invoice_Source_Cd
                       ,utlinv.ENTITY_NAME
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Hier chsite
                              ON ch.Client_Id = chsite.Client_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON chsite.Client_Hier_Id = cha.Client_Hier_Id
                                 AND cha.Commodity_Id = chdc.Commodity_Id
                        LEFT JOIN dbo.ENTITY utlinv
                              ON cha.Account_Invoice_Source_Cd = utlinv.ENTITY_ID
                        LEFT JOIN core.Client_Hier_Account chasupp
                              ON cha.Meter_Id = chasupp.Meter_Id
                                 AND chasupp.Account_Type = 'Supplier'
                        LEFT JOIN dbo.ACCOUNT suppacc
                              ON chasupp.Account_Id = suppacc.ACCOUNT_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = 0
                        AND ch.Site_Id = 0
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND chsite.Site_Id = @Site_Id
                        AND ( @State_Id IS NULL
                              OR chsite.State_Id = @State_Id )
                        AND ( @Account_Vendor_Id IS NULL
                              OR cha.Account_Vendor_Id = @Account_Vendor_Id )
                        AND ( @City IS NULL
                              OR chsite.City LIKE '%' + @City + '%' )
                        AND ( @Account_Number IS NULL
                              OR cha.Account_Number LIKE '%' + @Account_Number + '%' )
                        AND ( @Meter_Number IS NULL
                              OR cha.Meter_Number LIKE '%' + @Meter_Number + '%' )
                        AND cha.Account_Type = 'Utility'
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     chdc.DMO_End_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) BETWEEN chdc.DMO_Start_Dt
                                                                                                          AND     chdc.DMO_End_Dt ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config nasg
                                          INNER JOIN Core.Client_Hier chsg
                                                ON nasg.Client_Hier_Id = chsg.Client_Hier_Id
                                         WHERE
                                          chsg.Sitegroup_Id > 0
                                          AND chsg.Site_Id = 0
                                          AND chdc.Client_Hier_DMO_Config_Id = nasg.Client_Hier_DMO_Config_Id
                                          AND chsite.Sitegroup_Id = chsg.Sitegroup_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config nasite
                                          INNER JOIN Core.Client_Hier chnasite
                                                ON nasite.Client_Hier_Id = chnasite.Client_Hier_Id
                                         WHERE
                                          chnasite.Site_Id > 0
                                          AND chdc.Client_Hier_DMO_Config_Id = nasite.Client_Hier_DMO_Config_Id
                                          AND chsite.Site_Id = chnasite.Site_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config naacc
                                         WHERE
                                          chdc.Client_Hier_DMO_Config_Id = naacc.Client_Hier_DMO_Config_Id
                                          AND cha.Account_Id = naacc.Account_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Core.Client_Hier_Account conmtr
                                         WHERE
                                          conmtr.Account_Type = 'Supplier'
                                          AND conmtr.Supplier_Contract_ID <> -1
                                          AND conmtr.Meter_Id = cha.Meter_Id
                                          AND ( ( @DMO_Supp_End_Dt IS NULL
                                                  AND ( conmtr.Supplier_Account_begin_Dt >= @DMO_Supp_Start_Dt
                                                        OR conmtr.Supplier_Account_End_Dt >= @DMO_Supp_Start_Dt ) )
                                                OR ( @DMO_Supp_End_Dt IS NOT NULL
                                                     AND ( @DMO_Supp_Start_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                              AND     conmtr.Supplier_Account_End_Dt
                                                           OR @DMO_Supp_End_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                               AND     conmtr.Supplier_Account_End_Dt
                                                           OR conmtr.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                               AND     @DMO_Supp_End_Dt
                                                           OR conmtr.Supplier_Account_End_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                             AND     @DMO_Supp_End_Dt ) ) ) )
                 
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_Id
                  ,Sitegroup_Id
                  ,Site_Id
                  ,Site_Name
                  ,City
                  ,State_Id
                  ,State_Name
                  ,Account_Id
                  ,Utility_Account_Number
                  ,Meter_Id
                  ,Meter_Number
                  ,Account_Vendor_Name
                  ,Rate_Name
                  ,Supplier_Account_Id
                  ,Supplier_Account_Number
                  ,Supplier_Account_Begin_Dt
                  ,Supplier_Account_End_Dt
                  ,Supplier_Contract_ID
                  ,Account_Invoice_Source_Cd
                  ,Account_Invoice_Source )
                  SELECT
                        chsite.Client_Hier_Id
                       ,chsite.Sitegroup_Id
                       ,chsite.Site_Id
                       ,chsite.Site_name
                       ,chsite.City
                       ,chsite.State_Id
                       ,chsite.State_Name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Meter_Id
                       ,cha.Meter_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Rate_Name
                       ,chasupp.Account_Id
                       ,chasupp.Account_Number
                       ,suppacc.Supplier_Account_Begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
                       ,chasupp.Supplier_Contract_ID
                       ,cha.Account_Invoice_Source_Cd
                       ,utlinv.ENTITY_NAME
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Hier chsite
                              ON ch.Sitegroup_Id = chsite.Sitegroup_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON chsite.Client_Hier_Id = cha.Client_Hier_Id
                                 AND cha.Commodity_Id = chdc.Commodity_Id
                        LEFT JOIN dbo.ENTITY utlinv
                              ON cha.Account_Invoice_Source_Cd = utlinv.ENTITY_ID
                        LEFT JOIN core.Client_Hier_Account chasupp
                              ON cha.Meter_Id = chasupp.Meter_Id
                                 AND chasupp.Account_Type = 'Supplier'
                        LEFT JOIN dbo.ACCOUNT suppacc
                              ON chasupp.Account_Id = suppacc.ACCOUNT_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = 0
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND chsite.Site_Id = @Site_Id
                        AND ( @State_Id IS NULL
                              OR chsite.State_Id = @State_Id )
                        AND ( @Account_Vendor_Id IS NULL
                              OR cha.Account_Vendor_Id = @Account_Vendor_Id )
                        AND ( @City IS NULL
                              OR chsite.City LIKE '%' + @City + '%' )
                        AND ( @Account_Number IS NULL
                              OR cha.Account_Number LIKE '%' + @Account_Number + '%' )
                        AND ( @Meter_Number IS NULL
                              OR cha.Meter_Number LIKE '%' + @Meter_Number + '%' )
                        AND cha.Account_Type = 'Utility'
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     chdc.DMO_End_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) BETWEEN chdc.DMO_Start_Dt
                                                                                                          AND     chdc.DMO_End_Dt ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Client_Hier_Not_Applicable_DMO_Config nasite
                                          INNER JOIN Core.Client_Hier chnasite
                                                ON nasite.Client_Hier_Id = chnasite.Client_Hier_Id
                                         WHERE
                                          chnasite.Site_Id > 0
                                          AND chdc.Client_Hier_DMO_Config_Id = nasite.Client_Hier_DMO_Config_Id
                                          AND chsite.Site_Id = chnasite.Site_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config naacc
                                         WHERE
                                          chdc.Client_Hier_DMO_Config_Id = naacc.Client_Hier_DMO_Config_Id
                                          AND cha.Account_Id = naacc.Account_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Core.Client_Hier_Account conmtr
                                         WHERE
                                          conmtr.Account_Type = 'Supplier'
                                          AND conmtr.Supplier_Contract_ID <> -1
                                          AND conmtr.Meter_Id = cha.Meter_Id
                                          AND ( ( @DMO_Supp_End_Dt IS NULL
                                                  AND ( conmtr.Supplier_Account_begin_Dt >= @DMO_Supp_Start_Dt
                                                        OR conmtr.Supplier_Account_End_Dt >= @DMO_Supp_Start_Dt ) )
                                                OR ( @DMO_Supp_End_Dt IS NOT NULL
                                                     AND ( @DMO_Supp_Start_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                              AND     conmtr.Supplier_Account_End_Dt
                                                           OR @DMO_Supp_End_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                               AND     conmtr.Supplier_Account_End_Dt
                                                           OR conmtr.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                               AND     @DMO_Supp_End_Dt
                                                           OR conmtr.Supplier_Account_End_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                             AND     @DMO_Supp_End_Dt ) ) ) )
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_Id
                  ,Sitegroup_Id
                  ,Site_Id
                  ,Site_Name
                  ,City
                  ,State_Id
                  ,State_Name
                  ,Account_Id
                  ,Utility_Account_Number
                  ,Meter_Id
                  ,Meter_Number
                  ,Account_Vendor_Name
                  ,Rate_Name
                  ,Supplier_Account_Id
                  ,Supplier_Account_Number
                  ,Supplier_Account_Begin_Dt
                  ,Supplier_Account_End_Dt
                  ,Supplier_Contract_ID
                  ,Account_Invoice_Source_Cd
                  ,Account_Invoice_Source )
                  SELECT
                        ch.Client_Hier_Id
                       ,ch.Sitegroup_Id
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,ch.City
                       ,ch.State_Id
                       ,ch.State_Name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Meter_Id
                       ,cha.Meter_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Rate_Name
                       ,chasupp.Account_Id
                       ,chasupp.Account_Number
                       ,suppacc.Supplier_Account_Begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
                       ,chasupp.Supplier_Contract_ID
                       ,cha.Account_Invoice_Source_Cd
                       ,utlinv.ENTITY_NAME
                  FROM
                        dbo.Client_Hier_DMO_Config chdc
                        INNER JOIN Core.Client_Hier ch
                              ON chdc.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Hier_Account cha
                              ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                 AND cha.Commodity_Id = chdc.Commodity_Id
                        LEFT JOIN dbo.ENTITY utlinv
                              ON cha.Account_Invoice_Source_Cd = utlinv.ENTITY_ID
                        LEFT JOIN core.Client_Hier_Account chasupp
                              ON cha.Meter_Id = chasupp.Meter_Id
                                 AND chasupp.Account_Type = 'Supplier'
                        LEFT JOIN dbo.ACCOUNT suppacc
                              ON chasupp.Account_Id = suppacc.ACCOUNT_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND ch.Sitegroup_Id = @Sitegroup_Id
                        AND ch.Site_Id = @Site_Id
                        AND chdc.Commodity_Id = @Commodity_Id
                        AND ch.Site_Id = @Site_Id
                        AND ( @State_Id IS NULL
                              OR ch.State_Id = @State_Id )
                        AND ( @Account_Vendor_Id IS NULL
                              OR cha.Account_Vendor_Id = @Account_Vendor_Id )
                        AND ( @City IS NULL
                              OR ch.City LIKE '%' + @City + '%' )
                        AND ( @Account_Number IS NULL
                              OR cha.Account_Number LIKE '%' + @Account_Number + '%' )
                        AND ( @Meter_Number IS NULL
                              OR cha.Meter_Number LIKE '%' + @Meter_Number + '%' )
                        AND cha.Account_Type = 'Utility'
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND chdc.DMO_End_Dt IS NULL
                                AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NULL
                                   AND @DMO_Supp_Start_Dt >= chdc.DMO_Start_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) >= chdc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND chdc.DMO_End_Dt IS NOT NULL
                                   AND @DMO_Supp_Start_Dt BETWEEN chdc.DMO_Start_Dt
                                                          AND     chdc.DMO_End_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) BETWEEN chdc.DMO_Start_Dt
                                                                                                          AND     chdc.DMO_End_Dt ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          dbo.Account_Not_Applicable_DMO_Config naacc
                                         WHERE
                                          chdc.Client_Hier_DMO_Config_Id = naacc.Client_Hier_DMO_Config_Id
                                          AND cha.Account_Id = naacc.Account_Id )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Core.Client_Hier_Account conmtr
                                         WHERE
                                          conmtr.Account_Type = 'Supplier'
                                          AND conmtr.Supplier_Contract_ID <> -1
                                          AND conmtr.Meter_Id = cha.Meter_Id
                                          AND ( ( @DMO_Supp_End_Dt IS NULL
                                                  AND ( conmtr.Supplier_Account_begin_Dt >= @DMO_Supp_Start_Dt
                                                        OR conmtr.Supplier_Account_End_Dt >= @DMO_Supp_Start_Dt ) )
                                                OR ( @DMO_Supp_End_Dt IS NOT NULL
                                                     AND ( @DMO_Supp_Start_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                              AND     conmtr.Supplier_Account_End_Dt
                                                           OR @DMO_Supp_End_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                               AND     conmtr.Supplier_Account_End_Dt
                                                           OR conmtr.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                               AND     @DMO_Supp_End_Dt
                                                           OR conmtr.Supplier_Account_End_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                             AND     @DMO_Supp_End_Dt ) ) ) )
                        
      INSERT      INTO @Tbl_Config
                  ( 
                   Client_Hier_Id
                  ,Sitegroup_Id
                  ,Site_Id
                  ,Site_Name
                  ,City
                  ,State_Id
                  ,State_Name
                  ,Account_Id
                  ,Utility_Account_Number
                  ,Meter_Id
                  ,Meter_Number
                  ,Account_Vendor_Name
                  ,Rate_Name
                  ,Supplier_Account_Id
                  ,Supplier_Account_Number
                  ,Supplier_Account_Begin_Dt
                  ,Supplier_Account_End_Dt
                  ,Supplier_Contract_ID
                  ,Account_Invoice_Source_Cd
                  ,Account_Invoice_Source )
                  SELECT
                        ch.Client_Hier_Id
                       ,ch.Sitegroup_Id
                       ,ch.Site_Id
                       ,ch.Site_name
                       ,ch.City
                       ,ch.State_Id
                       ,ch.State_Name
                       ,cha.Account_Id
                       ,cha.Account_Number
                       ,cha.Meter_Id
                       ,cha.Meter_Number
                       ,cha.Account_Vendor_Name
                       ,cha.Rate_Name
                       ,chasupp.Account_Id
                       ,chasupp.Account_Number
                       ,suppacc.Supplier_Account_Begin_Dt
                       ,suppacc.Supplier_Account_End_Dt
                       ,chasupp.Supplier_Contract_ID
                       ,cha.Account_Invoice_Source_Cd
                       ,utlinv.ENTITY_NAME
                  FROM
                        dbo.Account_DMO_Config adc
                        INNER JOIN Core.Client_Hier_Account cha
                              ON adc.Account_Id = cha.Account_Id
                                 AND adc.Commodity_Id = cha.Commodity_Id
                        INNER JOIN Core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON adc.Updated_User_Id = ui.USER_INFO_ID
                        LEFT JOIN dbo.ENTITY utlinv
                              ON cha.Account_Invoice_Source_Cd = utlinv.ENTITY_ID
                        LEFT JOIN core.Client_Hier_Account chasupp
                              ON cha.Meter_Id = chasupp.Meter_Id
                                 AND chasupp.Account_Type = 'Supplier'
                        LEFT JOIN dbo.ACCOUNT suppacc
                              ON chasupp.Account_Id = suppacc.ACCOUNT_ID
                  WHERE
                        ch.Client_Id = @Client_Id
                        AND adc.Commodity_Id = @Commodity_Id
                        AND cha.Account_Type = 'Utility'
                        AND ch.Site_Id = @Site_Id
                        AND ( @State_Id IS NULL
                              OR ch.State_Id = @State_Id )
                        AND ( @Account_Vendor_Id IS NULL
                              OR cha.Account_Vendor_Id = @Account_Vendor_Id )
                        AND ( @City IS NULL
                              OR ch.City LIKE '%' + @City + '%' )
                        AND ( @Account_Number IS NULL
                              OR cha.Account_Number LIKE '%' + @Account_Number + '%' )
                        AND ( @Meter_Number IS NULL
                              OR cha.Meter_Number LIKE '%' + @Meter_Number + '%' )
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND adc.DMO_End_Dt IS NULL
                                AND @DMO_Supp_Start_Dt >= adc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NULL
                                   AND @DMO_Supp_Start_Dt >= adc.DMO_Start_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) >= adc.DMO_Start_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND adc.DMO_End_Dt IS NOT NULL
                                   AND @DMO_Supp_Start_Dt BETWEEN adc.DMO_Start_Dt
                                                          AND     adc.DMO_End_Dt
                                   AND DATEADD(DD, -DATEPART(DD, @DMO_Supp_End_Dt) + 1, @DMO_Supp_End_Dt) BETWEEN adc.DMO_Start_Dt
                                                                                                          AND     adc.DMO_End_Dt ) )
                        AND NOT EXISTS ( SELECT
                                          1
                                         FROM
                                          Core.Client_Hier_Account conmtr
                                         WHERE
                                          conmtr.Account_Type = 'Supplier'
                                          AND conmtr.Supplier_Contract_ID <> -1
                                          AND conmtr.Meter_Id = cha.Meter_Id
                                          AND ( ( @DMO_Supp_End_Dt IS NULL
                                                  AND ( conmtr.Supplier_Account_begin_Dt >= @DMO_Supp_Start_Dt
                                                        OR conmtr.Supplier_Account_End_Dt >= @DMO_Supp_Start_Dt ) )
                                                OR ( @DMO_Supp_End_Dt IS NOT NULL
                                                     AND ( @DMO_Supp_Start_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                              AND     conmtr.Supplier_Account_End_Dt
                                                           OR @DMO_Supp_End_Dt BETWEEN conmtr.Supplier_Account_begin_Dt
                                                                               AND     conmtr.Supplier_Account_End_Dt
                                                           OR conmtr.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                               AND     @DMO_Supp_End_Dt
                                                           OR conmtr.Supplier_Account_End_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                                             AND     @DMO_Supp_End_Dt ) ) ) )
                                                                                             
      INSERT      INTO @Tbl_Mtrs
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id )
                  SELECT
                        tc.Meter_Id
                       ,NULL
                  FROM
                        @Tbl_Config tc
                  GROUP BY
                        tc.Meter_Id
                  HAVING
                        MAX(tc.Supplier_Account_Id) IS NULL
                    
                   
      INSERT      INTO @Tbl_Mtrs
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id )
                  SELECT
                        tc.Meter_Id
                       ,tc.Supplier_Account_Id
                  FROM
                        @Tbl_Config tc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Mtrs tm
                                     WHERE
                                          tm.Meter_Id = tc.Meter_Id )
                        AND ( ( @DMO_Supp_End_Dt IS NULL
                                AND @DMO_Supp_Start_Dt <= tc.Supplier_Account_begin_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND tc.Supplier_Account_End_Dt IS NULL
                                   AND tc.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                    AND     @DMO_Supp_End_Dt )
                              OR ( @DMO_Supp_End_Dt IS NOT NULL
                                   AND tc.Supplier_Account_End_Dt IS NOT NULL
                                   AND tc.Supplier_Account_begin_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                    AND     @DMO_Supp_End_Dt
                                   AND tc.Supplier_Account_End_Dt BETWEEN @DMO_Supp_Start_Dt
                                                                  AND     @DMO_Supp_End_Dt
                                   AND @DMO_Supp_Start_Dt BETWEEN tc.Supplier_Account_begin_Dt
                                                          AND     tc.Supplier_Account_End_Dt
                                   AND @DMO_Supp_End_Dt BETWEEN tc.Supplier_Account_begin_Dt
                                                        AND     tc.Supplier_Account_End_Dt ) )
                                             
                                            
                       
      INSERT      INTO @Tbl_Mtrs
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id )
                  SELECT
                        tc.Meter_Id
                       ,tc.Supplier_Account_Id
                  FROM
                        @Tbl_Config tc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Mtrs tm
                                     WHERE
                                          tm.Meter_Id = tc.Meter_Id )
                        AND tc.Supplier_Account_End_Dt IS NULL
                        AND tc.Supplier_Contract_ID = -1
                  
      INSERT      INTO @Tbl_Mtrs
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id )
                  SELECT
                        tc.Meter_Id
                       ,tc.Supplier_Account_Id
                  FROM
                        @Tbl_Config tc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Mtrs tm
                                     WHERE
                                          tm.Meter_Id = tc.Meter_Id )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Config tc1
                                     WHERE
                                          tc.Meter_Id = tc1.Meter_Id
                                          AND tc.Supplier_Contract_ID = -1
                                     HAVING
                                          MAX(tc1.Supplier_Account_Begin_Dt) = tc.Supplier_Account_Begin_Dt )                   
                    
      INSERT      INTO @Tbl_Mtrs
                  ( 
                   Meter_Id
                  ,Supplier_Account_Id )
                  SELECT
                        tc.Meter_Id
                       ,tc.Supplier_Account_Id
                  FROM
                        @Tbl_Config tc
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Mtrs tm
                                     WHERE
                                          tm.Meter_Id = tc.Meter_Id )
                        AND EXISTS ( SELECT
                                          1
                                     FROM
                                          @Tbl_Config tc1
                                     WHERE
                                          tc.Meter_Id = tc1.Meter_Id
                                          AND tc.Supplier_Contract_ID <> -1
                                     HAVING
                                          MAX(tc1.Supplier_Account_Begin_Dt) = tc.Supplier_Account_Begin_Dt )       
      
      SELECT
            CASE WHEN tc.Supplier_Account_Id IS NULL THEN 1
                 WHEN @DMO_Supp_End_Dt IS NULL
                      AND tc.Supplier_Account_End_Dt IS NULL
                      AND tc.Supplier_Account_Begin_Dt < @DMO_Supp_Start_Dt
                      AND ( inv.SERVICE_MONTH IS NULL
                            OR inv.SERVICE_MONTH < @DMO_Supp_Start_Dt ) THEN 1
                 WHEN @DMO_Supp_End_Dt IS NULL
                      AND tc.Supplier_Account_End_Dt IS NOT NULL
                      AND tc.Supplier_Account_Begin_Dt < @DMO_Supp_Start_Dt
                      AND tc.Supplier_Account_End_Dt < @DMO_Supp_Start_Dt THEN 1
                 WHEN @DMO_Supp_End_Dt IS NOT NULL
                      AND tc.Supplier_Account_End_Dt IS NULL
                      AND ( ( @DMO_Supp_Start_Dt < tc.Supplier_Account_Begin_Dt
                              AND @DMO_Supp_End_Dt < tc.Supplier_Account_Begin_Dt )
                            OR ( @DMO_Supp_Start_Dt > tc.Supplier_Account_Begin_Dt
                                 AND @DMO_Supp_End_Dt > tc.Supplier_Account_Begin_Dt ) ) THEN 1
                 WHEN @DMO_Supp_End_Dt IS NOT NULL
                      AND tc.Supplier_Account_End_Dt IS NOT NULL
                      AND tc.Supplier_Account_Begin_Dt NOT BETWEEN @DMO_Supp_Start_Dt
                                                       AND         @DMO_Supp_End_Dt
                      AND tc.Supplier_Account_End_Dt NOT BETWEEN @DMO_Supp_Start_Dt
                                                     AND         @DMO_Supp_End_Dt
                      AND @DMO_Supp_Start_Dt NOT BETWEEN tc.Supplier_Account_Begin_Dt
                                             AND         tc.Supplier_Account_End_Dt
                      AND @DMO_Supp_End_Dt NOT BETWEEN tc.Supplier_Account_Begin_Dt
                                           AND         tc.Supplier_Account_End_Dt THEN 1
                 ELSE 0
            END AS Is_Allowed
           ,tc.Client_Hier_Id
           ,tc.Sitegroup_Id
           ,tc.Site_Id
           ,tc.Site_Name
           ,tc.City
           ,tc.State_Id
           ,tc.State_Name
           ,tc.Account_Id
           ,tc.Utility_Account_Number
           ,tc.Meter_Id
           ,tc.Meter_Number
           ,tc.Account_Vendor_Name
           ,tc.Rate_Name
           ,tc.Supplier_Account_Id
           ,tc.Supplier_Account_Number
           ,tc.Supplier_Account_Begin_Dt
           ,tc.Supplier_Account_End_Dt
           ,tc.Supplier_Contract_ID
           ,Account_Invoice_Source_Cd AS Utility_Invoice_Source_Cd
           ,Account_Invoice_Source AS Utility_Invoice_Source
      FROM
            @Tbl_Config tc
            LEFT JOIN ( SELECT
                              cism.Account_ID
                             ,MAX(cism.SERVICE_MONTH) AS SERVICE_MONTH
                        FROM
                              dbo.CU_INVOICE_SERVICE_MONTH cism
                        GROUP BY
                              cism.Account_ID ) inv
                  ON tc.Supplier_Account_Id = inv.Account_ID
            INNER JOIN @Tbl_Mtrs tm
                  ON tm.Meter_Id = tc.Meter_Id
                     AND ( ( tm.Supplier_Account_Id IS NULL
                             AND tc.Supplier_Account_Id IS NULL )
                           OR ( tm.Supplier_Account_Id = tc.Supplier_Account_Id ) )
                                                             
                                                             
END;









;
GO
GRANT EXECUTE ON  [dbo].[DMO_Supplier_Account_Meter_Sel] TO [CBMSApplication]
GO
