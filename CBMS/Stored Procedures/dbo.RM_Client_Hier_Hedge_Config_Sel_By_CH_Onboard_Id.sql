SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                      
Name:                      
        dbo.RM_Client_Hier_Hedge_Config_Sel_By_CH_Onboard_Id                    
                      
Description:                      
        To get site's hedge configurations
                      
Input Parameters:                      
    Name							DataType        Default     Description                        
--------------------------------------------------------------------------------
	@RM_Client_Hier_Onboard_Id		VARCHAR(MAX)
    @StartIndex						INT				1
    @EndIndex						INT				2147483647
                      
 Output Parameters:                            
	Name            Datatype        Default		Description                            
--------------------------------------------------------------------------------
	  
                    
Usage Examples:                          
--------------------------------------------------------------------------------
	
		EXEC dbo.RM_Client_Hier_Hedge_Config_Sel 11236
		EXEC dbo.RM_Client_Hier_Hedge_Config_Sel_By_CH_Onboard_Id 228
		EXEC dbo.RM_Client_Hier_Hedge_Config_Sel_By_CH_Onboard_Id 229
		
                    
 Author Initials:                      
    Initials    Name                      
--------------------------------------------------------------------------------
    RR          Raghu Reddy   
                       
 Modifications:                      
    Initials	Date           Modification                      
--------------------------------------------------------------------------------
    RR			2018-09-29     Global Risk Management - Created 
                     
******/
CREATE PROCEDURE [dbo].[RM_Client_Hier_Hedge_Config_Sel_By_CH_Onboard_Id]
    (
        @RM_Client_Hier_Onboard_Id VARCHAR(MAX)
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @RM_Default_Config_Start_Dt DATE
            , @RM_Default_Config_End_Dt DATE;

        CREATE TABLE #Configs
             (
                 Client_Id INT
                 , RM_Client_Hier_Onboard_Id INT
                 , Commodity_Id INT
                 , Commodity_Name VARCHAR(50)
                 , Country_Id INT
                 , COUNTRY_NAME VARCHAR(200)
                 , Is_Default BIT
                 , RM_Client_Hier_Hedge_Config_Id INT
                 , RM_Forecast_UOM_Type_Id INT
                 , RM_Forecast_UOM VARCHAR(200)
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Hedge_Type_Id INT
                 , Hedge_Type VARCHAR(200)
                 , Max_Hedge_Pct DECIMAL(5, 2)
                 , RM_Risk_Tolerance_Category_Id INT
                 , RM_Risk_Tolerance_Category VARCHAR(100)
                 , Risk_Manager_User_Info_Id INT
                 , Risk_Manager VARCHAR(100)
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Include_In_Reports VARCHAR(5)
                 , Is_Default_Config BIT
             );

        CREATE TABLE #Configs_Final
             (
                 Client_Id INT
                 , RM_Client_Hier_Onboard_Id INT
                 , Commodity_Id INT
                 , Commodity_Name VARCHAR(50)
                 , Country_Id INT
                 , COUNTRY_NAME VARCHAR(200)
                 , Is_Default BIT
                 , RM_Client_Hier_Hedge_Config_Id INT
                 , RM_Forecast_UOM_Type_Id INT
                 , RM_Forecast_UOM VARCHAR(200)
                 , Config_Start_Dt DATE
                 , Config_End_Dt DATE
                 , Hedge_Type_Id INT
                 , Hedge_Type VARCHAR(200)
                 , Max_Hedge_Pct DECIMAL(5, 2)
                 , RM_Risk_Tolerance_Category_Id INT
                 , RM_Risk_Tolerance_Category VARCHAR(100)
                 , Risk_Manager_User_Info_Id INT
                 , Risk_Manager VARCHAR(100)
                 , Client_Contact_Info_Id INT
                 , Client_Contact VARCHAR(100)
                 , Include_In_Reports VARCHAR(5)
                 , Is_Default_Config BIT
                 , Row_Num INT
             );


        DECLARE @Total_Cnt INT;

        SELECT
            @RM_Default_Config_Start_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_Start_Dt'
            AND App_Id = -1;

        SELECT
            @RM_Default_Config_End_Dt = CAST(App_Config_Value AS DATE)
        FROM
            dbo.App_Config
        WHERE
            App_Config_Cd = 'RM_Default_Config_End_Dt'
            AND App_Id = -1;



        ----------------------To get site own config
        INSERT INTO #Configs
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
             )
        SELECT
            ch.Client_Id
            , chob.RM_Client_Hier_Onboard_Id
            , chob.Commodity_Id
            , com.Commodity_Name
            , chob.Country_Id
            , con.COUNTRY_NAME
            , 0 AS Is_Default
            , chhc.RM_Client_Hier_Hedge_Config_Id
            , chob.RM_Forecast_UOM_Type_Id
            , uom.ENTITY_NAME AS RM_Forecast_UOM
            , chhc.Config_Start_Dt
            , chhc.Config_End_Dt
            , chhc.Hedge_Type_Id
            , hdgtyp.ENTITY_NAME AS Hedge_Type
            , chhc.Max_Hedge_Pct
            , chhc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
            , chhc.Risk_Manager_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Risk_Manager
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , CASE chhc.Include_In_Reports WHEN 1 THEN 'Yes'
                  ELSE 'No'
              END AS Include_In_Reports
            , chhc.Is_Default_Config
        FROM
            Core.Client_Hier ch
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON ch.Client_Hier_Id = chob.Client_Hier_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            LEFT JOIN dbo.ENTITY uom
                ON chob.RM_Forecast_UOM_Type_Id = uom.ENTITY_ID
            INNER JOIN dbo.ENTITY hdgtyp
                ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
            LEFT JOIN Trade.RM_Risk_Tolerance_Category rtc
                ON chhc.RM_Risk_Tolerance_Category_Id = rtc.RM_Risk_Tolerance_Category_Id
            LEFT JOIN dbo.Contact_Info ci
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
            LEFT JOIN dbo.Commodity com
                ON chob.Commodity_Id = com.Commodity_Id
            LEFT JOIN dbo.COUNTRY con
                ON chob.Country_Id = con.COUNTRY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON chhc.Risk_Manager_User_Info_Id = ui.USER_INFO_ID
        WHERE
            ch.Site_Id > 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Client_Hier_Onboard_Id, ',') onbid
                           WHERE
                                CAST(onbid.Segments AS INT) = chob.RM_Client_Hier_Onboard_Id);
        --ORDER BY
        --      ch.Site_name



        ----------------------To get default config                       
        INSERT INTO #Configs
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
             )
        SELECT
            ch.Client_Id
            , chob.RM_Client_Hier_Onboard_Id
            , chob.Commodity_Id
            , com.Commodity_Name
            , chob.Country_Id
            , con.COUNTRY_NAME
            , 1 AS Is_Default
            , chhc.RM_Client_Hier_Hedge_Config_Id
            , chob.RM_Forecast_UOM_Type_Id
            , uom.ENTITY_NAME AS RM_Forecast_UOM
            , chhc.Config_Start_Dt
            , chhc.Config_End_Dt
            , chhc.Hedge_Type_Id
            , hdgtyp.ENTITY_NAME AS Hedge_Type
            , chhc.Max_Hedge_Pct
            , chhc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
            , chhc.Risk_Manager_User_Info_Id
            , ui.FIRST_NAME + ' ' + ui.LAST_NAME AS Risk_Manager
            , chhc.Contact_Info_Id AS Client_Contact_Info_Id
            , ci.First_Name + ' ' + ci.Last_Name AS Client_Contact
            , CASE chhc.Include_In_Reports WHEN 1 THEN 'Yes'
                  ELSE 'No'
              END AS Include_In_Reports
            , chhc.Is_Default_Config
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier chclient
                ON ch.Client_Id = chclient.Client_Id
            INNER JOIN Trade.RM_Client_Hier_Onboard chob
                ON chclient.Client_Hier_Id = chob.Client_Hier_Id
                   AND  ch.Country_Id = chob.Country_Id
            INNER JOIN Trade.RM_Client_Hier_Hedge_Config chhc
                ON chob.RM_Client_Hier_Onboard_Id = chhc.RM_Client_Hier_Onboard_Id
            LEFT JOIN dbo.ENTITY uom
                ON chob.RM_Forecast_UOM_Type_Id = uom.ENTITY_ID
            INNER JOIN dbo.ENTITY hdgtyp
                ON chhc.Hedge_Type_Id = hdgtyp.ENTITY_ID
            LEFT JOIN Trade.RM_Risk_Tolerance_Category rtc
                ON chhc.RM_Risk_Tolerance_Category_Id = rtc.RM_Risk_Tolerance_Category_Id
            LEFT JOIN dbo.Contact_Info ci
                ON chhc.Contact_Info_Id = ci.Contact_Info_Id
            LEFT JOIN dbo.Commodity com
                ON chob.Commodity_Id = com.Commodity_Id
            LEFT JOIN dbo.COUNTRY con
                ON chob.Country_Id = con.COUNTRY_ID
            LEFT JOIN dbo.USER_INFO ui
                ON chhc.Risk_Manager_User_Info_Id = ui.USER_INFO_ID
        WHERE
            ch.Site_Id > 0
            AND chclient.Sitegroup_Id = 0
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.ufn_split(@RM_Client_Hier_Onboard_Id, ',') onbid
                           WHERE
                                CAST(onbid.Segments AS INT) = chob.RM_Client_Hier_Onboard_Id)
            AND NOT EXISTS (   SELECT
                                    1
                               FROM
                                    Trade.RM_Client_Hier_Onboard siteob
                               WHERE
                                    ch.Client_Hier_Id = siteob.Client_Hier_Id
                                    AND chob.Commodity_Id = siteob.Commodity_Id);






        INSERT INTO #Configs_Final
             (
                 Client_Id
                 , RM_Client_Hier_Onboard_Id
                 , Commodity_Id
                 , Commodity_Name
                 , Country_Id
                 , COUNTRY_NAME
                 , Is_Default
                 , RM_Client_Hier_Hedge_Config_Id
                 , RM_Forecast_UOM_Type_Id
                 , RM_Forecast_UOM
                 , Config_Start_Dt
                 , Config_End_Dt
                 , Hedge_Type_Id
                 , Hedge_Type
                 , Max_Hedge_Pct
                 , RM_Risk_Tolerance_Category_Id
                 , RM_Risk_Tolerance_Category
                 , Risk_Manager_User_Info_Id
                 , Risk_Manager
                 , Client_Contact_Info_Id
                 , Client_Contact
                 , Include_In_Reports
                 , Is_Default_Config
                 , Row_Num
             )
        SELECT
            Client_Id
            , RM_Client_Hier_Onboard_Id
            , Commodity_Id
            , Commodity_Name
            , Country_Id
            , COUNTRY_NAME
            , Is_Default
            , RM_Client_Hier_Hedge_Config_Id
            , RM_Forecast_UOM_Type_Id
            , RM_Forecast_UOM
            , Config_Start_Dt
            , Config_End_Dt
            , Hedge_Type_Id
            , Hedge_Type
            , Max_Hedge_Pct
            , RM_Risk_Tolerance_Category_Id
            , RM_Risk_Tolerance_Category
            , Risk_Manager_User_Info_Id
            , Risk_Manager
            , Client_Contact_Info_Id
            , Client_Contact
            , Include_In_Reports
            , Is_Default_Config
            , DENSE_RANK() OVER (ORDER BY
                                     RM_Client_Hier_Onboard_Id
                                     , Config_Start_Dt)
        FROM
            #Configs
        GROUP BY
            Client_Id
            , RM_Client_Hier_Onboard_Id
            , Commodity_Id
            , Commodity_Name
            , Country_Id
            , COUNTRY_NAME
            , Is_Default
            , RM_Client_Hier_Hedge_Config_Id
            , RM_Forecast_UOM_Type_Id
            , RM_Forecast_UOM
            , Config_Start_Dt
            , Config_End_Dt
            , Hedge_Type_Id
            , Hedge_Type
            , Max_Hedge_Pct
            , RM_Risk_Tolerance_Category_Id
            , RM_Risk_Tolerance_Category
            , Risk_Manager_User_Info_Id
            , Risk_Manager
            , Client_Contact_Info_Id
            , Client_Contact
            , Include_In_Reports
            , Is_Default_Config;


        SELECT  @Total_Cnt = MAX(Row_Num)FROM   #Configs_Final;

        SELECT
            cf.Client_Id
            , cf.RM_Client_Hier_Onboard_Id
            , cf.Commodity_Id
            , cf.Commodity_Name
            , cf.Country_Id
            , cf.COUNTRY_NAME
            , cf.Is_Default
            , cf.RM_Client_Hier_Hedge_Config_Id
            , cf.RM_Forecast_UOM_Type_Id
            , cf.RM_Forecast_UOM
            , cf.Config_Start_Dt
            , NULLIF(cf.Config_End_Dt, @RM_Default_Config_End_Dt) AS Config_End_Dt
            , cf.Hedge_Type_Id
            , cf.Hedge_Type
            , cf.Max_Hedge_Pct
            , cf.RM_Risk_Tolerance_Category_Id
            , cf.RM_Risk_Tolerance_Category
            , cf.Risk_Manager_User_Info_Id
            , cf.Risk_Manager
            , cf.Client_Contact_Info_Id
            , cf.Client_Contact
            , cf.Include_In_Reports
            , cf.Is_Default_Config
            , cf.Row_Num
            , @Total_Cnt AS Total_Cnt
        FROM
            #Configs_Final cf
        WHERE
            cf.Row_Num BETWEEN @StartIndex
                       AND     @EndIndex
        ORDER BY
            cf.Row_Num
            , cf.Config_Start_Dt;


    END;


GO
GRANT EXECUTE ON  [dbo].[RM_Client_Hier_Hedge_Config_Sel_By_CH_Onboard_Id] TO [CBMSApplication]
GO
