SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: dbo.Utility_Detail_Analyst_Map_Del

DESCRIPTION: It Deletes Utility Detail Analyst Map for Selected Utility_Detail_Analyst_Map_ID.
      
INPUT PARAMETERS:          
	NAME					DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Utility_Detail_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------
  Begin Tran
		EXEC dbo.Utility_Detail_Analyst_Map_Del  3608
		EXEC dbo.Utility_Detail_Analyst_Map_Del  3615
		
  Rollback Tran

	SELECT * FROM dbo.Utility_Detail_Analyst_Map where Utility_Detail_Analyst_Map_ID = 3608
	SELECT * FROM dbo.Utility_Detail_Analyst_Map where Utility_Detail_Analyst_Map_ID = 3615

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------
	PNR			24-August-10	CREATED
*/

CREATE PROCEDURE dbo.Utility_Detail_Analyst_Map_Del
    (
       @Utility_Detail_Analyst_Map_ID INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE 
	FROM
		dbo.Utility_Detail_Analyst_Map 
	WHERE
		Utility_Detail_Analyst_Map_ID = @Utility_Detail_Analyst_Map_ID
END
GO
GRANT EXECUTE ON  [dbo].[Utility_Detail_Analyst_Map_Del] TO [CBMSApplication]
GO
