SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME: dbo.Sr_Rfp_New_Create_Sop    
    
DESCRIPTION:     
    
    
INPUT PARAMETERS:        
      Name                      DataType          Default     Description        
---------------------------------------------------------------------------------        
	@supplierContactMap			VARCHAR(200)      
	@supplierContactId			INT
	@accountTermId				INT   
	@supplierBOCId				INT      
	@productName				VARCHAR(200)
	@accountGroupId				INT 
	@isBidGroup					INT   
	@priceComments				VARCHAR(4000)      
                              
                               
OUTPUT PARAMETERS:             
      Name						DataType          Default     Description        
----------------------------------------------------------------------------------        
    
    
USAGE EXAMPLES:    
----------------------------------------------------------------------------------    
    
    
AUTHOR INITIALS:    
	Initials	Name    
------------------------------------------------------------    
	RR			Raghu Reddy

MODIFICATIONS    
	Initials	Date		Modification    
------------------------------------------------------------    
	RR			2016-05-04	Created in place of dbo.SR_RFP_CREATE_SOP_P
							The old script dbo.SR_RFP_CREATE_SOP_P is still using for legacy RFPs
				2016-05-10	GCS-887 Saving Pricing_Comments to SOP bid   
******/    
CREATE PROCEDURE [dbo].[Sr_Rfp_New_Create_Sop]
      ( 
       @supplierContactMap VARCHAR(200)
      ,@supplierContactId INT
      ,@accountTermId INT
      ,@supplierBOCId INT
      ,@productName VARCHAR(200)
      ,@accountGroupId INT
      ,@isBidGroup INT
      ,@priceComments VARCHAR(4000) )
AS 
BEGIN      
      
      SET NOCOUNT ON;      
      
      DECLARE
            @SOPSummaryId INT
           ,@SOPId INT
           ,@SOPAccountTermId INT
           ,@termId INT
           ,@SOPTermId INT
           ,@SOPBidId INT      
      
      DECLARE
            @summitBidId INT
           ,@supplierCommentsId INT
           ,@supplierServiceId INT      
      
      DECLARE
            @newProductName VARCHAR(200)
           ,@newIsSubProduct BIT      
      
      DECLARE
            @newSummitBidId INT
           ,@newSupplierCommentsId INT
      
      DECLARE
            @newBidId INT
           ,@is_sdp INT      
      
      DECLARE
            @SOP_TERM_ID INT
           ,@SOP_FROM_MONTH DATETIME
           ,@SOP_TO_MONTH DATETIME
           ,@SOP_NO_OF_MONTHS INT
           ,@Sr_Rfp_Sop_Details_Id INT   
      
     
      SELECT
            @SOPSummaryId = SR_RFP_SOP_SUMMARY_ID
      FROM
            dbo.SR_RFP_SOP_SUMMARY
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountGroupId
            AND IS_BID_GROUP = @isBidGroup      
      
      SELECT
            @SOPId = SR_RFP_SOP_ID
      FROM
            dbo.SR_RFP_SOP
      WHERE
            SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID = @supplierContactId
            AND VENDOR_NAME = @supplierContactMap
            AND SR_RFP_SOP_SUMMARY_ID = @SOPSummaryId      
      
      SELECT
            @termId = SR_RFP_TERM_ID
      FROM
            dbo.SR_RFP_ACCOUNT_TERM
      WHERE
            SR_RFP_ACCOUNT_TERM_ID = @accountTermId
            AND sr_account_group_id = @accountGroupId
            AND is_bid_group = @isBidGroup
            AND is_sop IS NULL      
      
      SELECT
            @SOPTermId = SR_RFP_TERM_ID
      FROM
            dbo.SR_RFP_ACCOUNT_TERM
      WHERE
            SR_RFP_TERM_ID = @termId
            AND SR_RFP_ACCOUNT_TERM.IS_SOP = 1
            AND sr_account_group_id = @accountGroupId
            AND is_bid_group = @isBidGroup      
      
      SELECT
            @summitBidId = SR_RFP_BID_REQUIREMENTS_ID
           ,@supplierCommentsId = SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
           ,@supplierServiceId = SR_RFP_SUPPLIER_SERVICE_ID
           ,@newProductName = PRODUCT_NAME
           ,@newIsSubProduct = IS_SUB_PRODUCT
      FROM
            dbo.SR_RFP_BID
      WHERE
            SR_RFP_BID_ID = @supplierBOCId      
    
      IF @SOPSummaryId = ''
            OR @SOPSummaryId IS NULL 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_SOP_SUMMARY
                              ( 
                               SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP )
                  VALUES
                              ( 
                               @accountGroupId
                              ,@isBidGroup )      
        
                  SELECT
                        @SOPSummaryId = scope_identity()        
            END      
      
      IF @SOPId = ''
            OR @SOPId IS NULL 
            BEGIN      
       
                  INSERT      INTO dbo.SR_RFP_SOP
                              ( 
                               SR_RFP_SOP_SUMMARY_ID
                              ,SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID
                              ,VENDOR_NAME
                              ,COMMENTS )
                  VALUES
                              ( 
                               @SOPSummaryId
                              ,@supplierContactId
                              ,@supplierContactMap
                              ,NULL )      
      
                  SELECT
                        @SOPId = scope_identity()      
      
            END      
      
      
      IF @SOPTermId IS NOT NULL
            AND @SOPTermId > 0 
            BEGIN      
        
                  SELECT
                        @SOPAccountTermId = sr_rfp_account_term_id
                  FROM
                        dbo.sr_rfp_account_term
                  WHERE
                        SR_RFP_TERM_ID = @SOPTermId
                        AND sr_account_group_id = @accountGroupId
                        AND is_bid_group = @isBidGroup
                        AND is_sop = 1      
            END      
      ELSE 
            BEGIN      
        
                  SELECT
                        @is_sdp = is_sdp
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        sr_account_group_id = @accountGroupId
                        AND is_bid_group = @isBidGroup
                        AND SR_RFP_ACCOUNT_TERM_ID = @accountTermId      
        
                  INSERT      INTO dbo.sr_rfp_account_term
                              ( 
                               SR_RFP_TERM_ID
                              ,SR_ACCOUNT_GROUP_ID
                              ,IS_BID_GROUP
                              ,IS_SOP
                              ,FROM_MONTH
                              ,TO_MONTH
                              ,NO_OF_MONTHS
                              ,IS_SDP )
                              SELECT
                                    SR_RFP_TERM_ID
                                   ,SR_ACCOUNT_GROUP_ID
                                   ,IS_BID_GROUP
                                   ,1
                                   ,FROM_MONTH
                                   ,TO_MONTH
                                   ,NO_OF_MONTHS
                                   ,@is_sdp
                              FROM
                                    dbo.sr_rfp_account_term
                              WHERE
                                    sr_rfp_account_term_id = @accountTermId      
      
                  SELECT
                        @SOPAccountTermId = scope_identity()      
      
     
                  SELECT TOP 1
                        @SOP_TERM_ID = SR_RFP_TERM_ID
                       ,@SOP_FROM_MONTH = FROM_MONTH
                       ,@SOP_TO_MONTH = TO_MONTH
                       ,@SOP_NO_OF_MONTHS = NO_OF_MONTHS
                  FROM
                        dbo.SR_RFP_ACCOUNT_TERM
                  WHERE
                        SR_RFP_ACCOUNT_TERM_ID = @accountTermId      
      
     
            END      
       
 -- second INSERT INTO SR_RFP_BID_REQUIREMENTS table      
       
      INSERT      INTO dbo.SR_RFP_BID_REQUIREMENTS
                  ( 
                   DELIVERY_POINT
                  ,TRANSPORTATION_LEVEL_TYPE_ID
                  ,NOMINATION_TYPE_ID
                  ,BALANCING_TYPE_ID
                  ,COMMENTS
                  ,DELIVERY_POINT_POWER_TYPE_ID
                  ,SERVICE_LEVEL_POWER_TYPE_ID )
                  SELECT
                        DELIVERY_POINT
                       ,TRANSPORTATION_LEVEL_TYPE_ID
                       ,NOMINATION_TYPE_ID
                       ,BALANCING_TYPE_ID
                       ,COMMENTS
                       ,DELIVERY_POINT_POWER_TYPE_ID
                       ,SERVICE_LEVEL_POWER_TYPE_ID
                  FROM
                        SR_RFP_BID_REQUIREMENTS
                  WHERE
                        SR_RFP_BID_REQUIREMENTS_ID = @summitBidId      
      
      SELECT
            @newSummitBidId = scope_identity()      
       
 -- third INSERT INTO SR_RFP_SUPPLIER_PRICE_COMMENTS table      
      IF @supplierCommentsId != ''
            AND @supplierCommentsId > 0 
            BEGIN      
      
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              ( 
                               IS_CREDIT_APPROVAL
                              ,NO_CREDIT_COMMENTS
                              ,PRICE_RESPONSE_COMMENTS
                              ,Broker_Included_Type_Id
                              ,Pricing_Comments )
                              SELECT
                                    IS_CREDIT_APPROVAL
                                   ,NO_CREDIT_COMMENTS
                                   ,PRICE_RESPONSE_COMMENTS
                                   ,Broker_Included_Type_Id
                                   ,Pricing_Comments
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId      
      
                  SELECT
                        @newSupplierCommentsId = scope_identity()      
      
  -- Added by Nag for archiving price comments      
                  INSERT      INTO dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              ( 
                               PRICE_RESPONSE_COMMENTS
                              ,ARCHIVED_ON_DATE
                              ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID )
                              SELECT
                                    PRICE_RESPONSE_COMMENTS
                                   ,ARCHIVED_ON_DATE
                                   ,@newSupplierCommentsId
                              FROM
                                    dbo.SR_RFP_SUPPLIER_PRICE_COMMENTS_ARCHIVE
                              WHERE
                                    SR_RFP_SUPPLIER_PRICE_COMMENTS_ID = @supplierCommentsId      
      
            END      
      
        
      INSERT      INTO dbo.SR_RFP_BID
                  ( 
                   PRODUCT_NAME
                  ,SR_RFP_BID_REQUIREMENTS_ID
                  ,SR_RFP_SUPPLIER_PRICE_COMMENTS_ID
                  ,SR_RFP_SUPPLIER_SERVICE_ID
                  ,IS_SUB_PRODUCT )
      VALUES
                  ( 
                   @newProductName
                  ,@newSummitBidId
                  ,@newSupplierCommentsId
                  ,NULL
                  ,@newIsSubProduct )      
      
      SELECT
            @newBidId = scope_identity()    
            
      INSERT      INTO dbo.SR_RFP_SOP_DETAILS
                  ( 
                   SR_RFP_SOP_ID
                  ,SR_RFP_ACCOUNT_TERM_ID
                  ,PRODUCT_NAME
                  ,SR_RFP_BID_ID
                  ,PRICE_RESPONSE_COMMENTS
                  ,IS_RECOMMENDED )
      VALUES
                  ( 
                   @SOPId
                  ,@SOPAccountTermId
                  ,@productName
                  ,@newBidId
                  ,@priceComments
                  ,0 )      
                  
      SELECT
            @Sr_Rfp_Sop_Details_Id = scope_identity()
               
      INSERT      INTO dbo.Sr_Rfp_Sop_Service_Condition_Response
                  ( 
                   Sr_Rfp_Sop_Details_Id
                  ,Sr_Service_Condition_Category_Id
                  ,Category_Display_Seq
                  ,Sr_Service_Condition_Question_Id
                  ,Question_Display_Seq
                  ,Response_Text_Value
                  ,Sr_Rfp_Service_Condition_Template_Question_Map_Id
                  ,Last_Change_Ts
                  ,Comment )
                  SELECT
                        @Sr_Rfp_Sop_Details_Id
                       ,scqm.Sr_Service_Condition_Category_Id
                       ,scqm.Category_Display_Seq
                       ,scqm.Sr_Service_Condition_Question_Id
                       ,scqm.Question_Display_Seq
                       ,tqr.Response_Text_Value
                       ,scqm.Sr_Rfp_Supplier_Service_Condition_Question_Map_Id
                       ,getdate()
                       ,tqr.Comment
                  FROM
                        dbo.Sr_Rfp_Supplier_Service_Condition_Question_Map scqm
                        LEFT JOIN dbo.Sr_Rfp_Service_Condition_Template_Question_Response tqr
                              ON scqm.Sr_Rfp_Bid_Id = tqr.SR_RFP_BID_ID
                                 AND scqm.Sr_Rfp_Service_Condition_Template_Question_Map_Id = tqr.Sr_Rfp_Service_Condition_Template_Question_Map_Id
                  WHERE
                        scqm.Sr_Rfp_Bid_Id = @supplierBOCId
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_New_Create_Sop] TO [CBMSApplication]
GO
