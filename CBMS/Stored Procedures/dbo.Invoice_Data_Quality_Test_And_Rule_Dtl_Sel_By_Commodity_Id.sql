SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
      dbo.Invoice_Data_Quality_Test_And_Rule_Dtl_Sel_By_Commodity_Id  
     
 DESCRIPTION:     
      Gets the Test details based on commodity.  
        
 INPUT PARAMETERS:    
 Name					DataType			Default			Description    
----------------------------------------------------------------------------   
 @Commodity_Id			INT       
  
 OUTPUT PARAMETERS:  
 Name					DataType			Default			Description    
----------------------------------------------------------------------------  
 
 USAGE EXAMPLES:    
----------------------------------------------------------------------------   
  
 EXEC dbo.Invoice_Data_Quality_Test_And_Rule_Dtl_Sel_By_Commodity_Id 290  
 EXEC dbo.Invoice_Data_Quality_Test_And_Rule_Dtl_Sel_By_Commodity_Id 291 

   
AUTHOR INITIALS:  
Initials		Name  
----------------------------------------------------------------------------   
 NR				Narayana Reddy  
   
  
 MODIFICATIONS     
 Initials	Date			Modification  
----------------------------------------------------------------------------  
 NR        2019-03-28		Created for Data Quality.  
  
******/

CREATE PROCEDURE [dbo].[Invoice_Data_Quality_Test_And_Rule_Dtl_Sel_By_Commodity_Id]
    (
        @Commodity_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            idqt.Data_Quality_Test_Id
            , Category_Cd.Code_Value AS Data_Category
            , idqt.Test_Description
            , Test_Category_Cd.Code_Value AS Test_Category
            , idqt.Data_Quality_Test_Id AS Data_Quality_Test_Commodity_Id
            , c.Commodity_Id
            , c.Commodity_Name
            , idqt.Execution_Seq_No
            , idqtp.Invoice_Data_Quality_Test_Rule_Id
            , idqtp.Rule_Seq_No
            , idqtp.Bucket_Master_Id
            , bm.Bucket_Name
            , idqtp.Baseline_Bucket_Master_Id
            , baseline.Bucket_Name AS Baseline_Bucket
            , Validation_Type_Cd.Code_Value AS Validation_Type
            , Aggregation_level_cd.Code_Value AS Aggregation_level
            , idqtp.Default_Tolerance
            , Tolerance_Value_Type_Cd.Code_Value AS Tolerance_Value_Type
            , Tolerance_Arithmetic_Op_Cd.Code_Value AS Tolerance_Arithmetic_Op
            , Current_Value_Month_Period_Cd.Code_Value + ' ' + Current_Value_Month_Period_Operator_Cd.Code_Value + ' '
              + CAST(idqtp.Current_Value_Month_Period_Operand AS VARCHAR(10)) AS Current_Value_Month
            , idqtp.Baseline_Value_Operand
            , Baseline_Starting_Period_Cd.Code_Value + ' ' + Baseline_Starting_Period_Operator_Cd.Code_Value + ' '
              + CAST(idqtp.Baseline_Starting_Period_Operand AS VARCHAR(10)) AS Baseline_Start_Month
            , Baseline_End_Period_Cd.Code_Value + ' ' + Baseline_End_Period_Operator_Cd.Code_Value + ' '
              + CAST(idqtp.Baseline_End_Period_Operand AS VARCHAR(10)) AS Baseline_End_Month
            , Comparison_Operator_Cd.Code_Value AS Comparison_Operator
            , idqt.Apply_Uom_Conversion
        FROM
            dbo.Data_Quality_Test idqt
            --INNER JOIN dbo.Data_Quality_Test_Commodity dqtc
            --    ON dqtc.Data_Quality_Test_Id = idqt.Data_Quality_Test_Id
            INNER JOIN dbo.Commodity c
                ON c.Commodity_Id = idqt.Commodity_Id
            INNER JOIN dbo.Invoice_Data_Quality_Test_Rule idqtp
                ON idqtp.Data_Quality_Test_Id = idqt.Data_Quality_Test_Id
            LEFT JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = idqtp.Bucket_Master_Id
            LEFT JOIN dbo.Bucket_Master baseline
                ON baseline.Bucket_Master_Id = idqtp.Baseline_Bucket_Master_Id
            LEFT JOIN dbo.Code Category_Cd
                ON Category_Cd.Code_Id = idqt.Data_Category_Cd
            LEFT JOIN dbo.Code Test_Category_Cd
                ON Test_Category_Cd.Code_Id = idqt.Test_Category_Cd
            LEFT JOIN dbo.Code Validation_Type_Cd
                ON Validation_Type_Cd.Code_Id = idqtp.Validation_Type_Cd
            LEFT JOIN dbo.Code Aggregation_level_cd
                ON Aggregation_level_cd.Code_Id = idqtp.Aggregation_Level_Cd
            LEFT JOIN dbo.Code Tolerance_Value_Type_Cd
                ON Tolerance_Value_Type_Cd.Code_Id = idqtp.Tolerance_Value_Type_Cd
            LEFT JOIN dbo.Code Tolerance_Arithmetic_Op_Cd
                ON Tolerance_Arithmetic_Op_Cd.Code_Id = idqtp.Tolerance_Arithmetic_Op_Cd
            LEFT JOIN dbo.Code Baseline_Starting_Period_Cd
                ON Baseline_Starting_Period_Cd.Code_Id = idqtp.Baseline_Starting_Period_Cd
            LEFT JOIN dbo.Code Baseline_Starting_Period_Operator_Cd
                ON Baseline_Starting_Period_Operator_Cd.Code_Id = idqtp.Baseline_Starting_Period_Operator_Cd
            LEFT JOIN dbo.Code Baseline_End_Period_Cd
                ON Baseline_End_Period_Cd.Code_Id = idqtp.Baseline_End_Period_Cd
            LEFT JOIN dbo.Code Baseline_End_Period_Operator_Cd
                ON Baseline_End_Period_Operator_Cd.Code_Id = idqtp.Baseline_End_Period_Operator_Cd
            LEFT JOIN dbo.Code Comparison_Operator_Cd
                ON Comparison_Operator_Cd.Code_Id = idqtp.Comparison_Operator_Cd
            LEFT JOIN dbo.Code Current_Value_Month_Period_Cd
                ON Current_Value_Month_Period_Cd.Code_Id = idqtp.Current_Value_Month_Period_Cd
            LEFT JOIN dbo.Code Current_Value_Month_Period_Operator_Cd
                ON Current_Value_Month_Period_Operator_Cd.Code_Id = idqtp.Current_Value_Month_Period_Operator_Cd
        WHERE
            c.Commodity_Id = @Commodity_Id
            AND idqt.Is_Active = 1
        ORDER BY
            idqt.Execution_Seq_No;
    END;








GO
GRANT EXECUTE ON  [dbo].[Invoice_Data_Quality_Test_And_Rule_Dtl_Sel_By_Commodity_Id] TO [CBMSApplication]
GO
