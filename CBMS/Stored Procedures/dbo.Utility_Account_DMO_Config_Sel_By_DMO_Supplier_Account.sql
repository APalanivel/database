SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.Utility_Account_DMO_Config_Sel_By_DMO_Supplier_Account
           
DESCRIPTION:             
			To select DMO configurations
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
	@DMO_Supplier_Account_Id			INT


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
            
	EXEC dbo.Utility_Account_DMO_Config_Sel_By_DMO_Supplier_Account 1149430
		
	
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-20	Contract placeholder 
******/

CREATE PROCEDURE [dbo].[Utility_Account_DMO_Config_Sel_By_DMO_Supplier_Account]
      ( 
       @DMO_Supplier_Account_Id INT )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Account_Id INT
           ,@Commodity_Id INT
           ,@Total_Row_Count INT
           ,@Row_Counter INT
           ,@DMO_Start_Dt DATETIME
           ,@DMO_End_Dt DATETIME
      
      DECLARE @Tbl_Meter_Utility AS TABLE
            ( 
             Id INT IDENTITY(1, 1)
            ,Account_Id INT
            ,Commodity_Id INT
            ,Supplier_Account_Begin_Dt DATETIME
            ,Supplier_Account_End_Dt DATETIME )
            
      DECLARE @Tbl_Config AS TABLE
            ( 
             Client_Hier_DMO_Config_Id INT
            ,Client_Hier_Id INT
            ,Hier_level_Cd INT
            ,Account_Id INT
            ,Account_DMO_Config_Id INT
            ,Code_Dsc VARCHAR(255)
            ,Commodity_Id INT
            ,Commodity_Name VARCHAR(50)
            ,DMO_Start_Dt VARCHAR(10)
            ,DMO_End_Dt VARCHAR(10)
            ,Updated_User VARCHAR(100)
            ,Last_Change_Ts DATETIME )
            
            
      
      INSERT      INTO @Tbl_Meter_Utility
                  ( 
                   Account_Id
                  ,Commodity_Id
                  ,Supplier_Account_Begin_Dt
                  ,Supplier_Account_End_Dt )
                  SELECT
                        cha.Account_Id
                       ,cha.Commodity_Id
                       ,acc.Supplier_Account_Begin_Dt
                       ,acc.Supplier_Account_End_Dt
                  FROM
                        Core.Client_Hier_Account cha
                        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                              ON cha.Meter_Id = samm.METER_ID
                        INNER JOIN dbo.ACCOUNT acc
                              ON samm.ACCOUNT_ID = acc.ACCOUNT_ID
                  WHERE
                        samm.ACCOUNT_ID = @DMO_Supplier_Account_Id
                        AND cha.Account_Type = 'Utility'
                  GROUP BY
                        cha.Account_Id
                       ,cha.Commodity_Id
                       ,acc.Supplier_Account_Begin_Dt
                       ,acc.Supplier_Account_End_Dt
      
    
           
      SELECT
            @Total_Row_Count = MAX(Id)
      FROM
            @Tbl_Meter_Utility
            
      SET @Row_Counter = 1 
      WHILE ( @Row_Counter <= @Total_Row_Count ) 
            BEGIN

                  SELECT
                        @Account_Id = Account_Id
                  FROM
                        @Tbl_Meter_Utility
                  WHERE
                        Id = @Row_Counter

                             
            
                  INSERT      INTO @Tbl_Config
                              ( 
                               Client_Hier_DMO_Config_Id
                              ,Client_Hier_Id
                              ,Hier_level_Cd
                              ,Account_Id
                              ,Account_DMO_Config_Id
                              ,Code_Dsc
                              ,Commodity_Id
                              ,Commodity_Name
                              ,DMO_Start_Dt
                              ,DMO_End_Dt
                              ,Updated_User
                              ,Last_Change_Ts )
                              EXEC dbo.Account_DMO_Config_Sel 
                                    @Account_Id = @Account_Id

                  SET @Row_Counter = @Row_Counter + 1

            END
      
      
            
      
            
      SELECT
            tc.Client_Hier_DMO_Config_Id
           ,tc.Client_Hier_Id
           ,tc.Hier_level_Cd
           ,tc.Account_Id
           ,tc.Account_DMO_Config_Id
           ,tc.Code_Dsc
           ,tc.Commodity_Id
           ,tc.Commodity_Name
           ,CAST(tc.DMO_Start_Dt AS DATE) AS DMO_Start_Dt
           ,CAST(tc.DMO_End_Dt AS DATE) AS DMO_End_Dt
           ,tc.Updated_User
           ,tc.Last_Change_Ts
      FROM
            @Tbl_Config tc
            INNER JOIN @Tbl_Meter_Utility tmu
                  ON tc.Account_Id = tmu.Account_Id
                     AND tc.Commodity_Id = tmu.Commodity_Id
      WHERE
            ( ( tc.DMO_End_Dt IS NULL
                AND tmu.Supplier_Account_End_Dt IS NULL
                AND tmu.Supplier_Account_Begin_Dt >= tc.DMO_Start_Dt )
              OR ( tc.DMO_End_Dt IS NULL
                   AND tmu.Supplier_Account_End_Dt IS NOT NULL
                   AND tmu.Supplier_Account_Begin_Dt >= tc.DMO_Start_Dt
                   AND tmu.Supplier_Account_End_Dt >= tc.DMO_Start_Dt )
              OR ( tc.DMO_End_Dt IS NOT  NULL
                   AND tmu.Supplier_Account_End_Dt IS NOT NULL
                   AND tmu.Supplier_Account_Begin_Dt BETWEEN tc.DMO_Start_Dt
                                                     AND     DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt))
                   AND tmu.Supplier_Account_End_Dt BETWEEN tc.DMO_Start_Dt
                                                   AND     DATEADD(DD, -DATEPART(DD, tc.DMO_End_Dt), DATEADD(mm, 1, tc.DMO_End_Dt)) ) )
     
            
END

;
GO
GRANT EXECUTE ON  [dbo].[Utility_Account_DMO_Config_Sel_By_DMO_Supplier_Account] TO [CBMSApplication]
GO
