
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
/******              
NAME:   dbo.Site_Commodity_Image_Changes_Sel_For_Data_Transfer        
    
        
DESCRIPTION:      
 Gets changes from Site_Commodity_image based on Change tracking            
     
          
INPUT PARAMETERS:              
Name   DataType Default  Description              
------------------------------------------------------------              
    
                    
OUTPUT PARAMETERS:              
Name   DataType Default  Description              
------------------------------------------------------------     
    
             
USAGE EXAMPLES:              
------------------------------------------------------------     
 EXEC dbo.Site_Commodity_Image_Changes_Sel_For_Data_Transfer 0, 1000  
           
         
AUTHOR INITIALS:              
Initials Name              
------------------------------------------------------------              
AKR   Ashok Kumar Raju
DMR	  Deana Ritter    
         
         
MODIFICATIONS               
Initials Date Modification              
------------------------------------------------------------              
AKR   2011-12-30 Created
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.    
*****/     
CREATE PROCEDURE [dbo].[Site_Commodity_Image_Changes_Sel_For_Data_Transfer]
      ( 
       @Min_CT_Ver BIGINT
      ,@Max_CT_Ver BIGINT )
AS 
BEGIN    
           
      SELECT
            ch.Client_Hier_Id
           ,NULL AS Account_Id
           ,NULL AS CU_Invoice_Id
           ,cng.Commodity_Id
           ,cng.Service_Month
           ,sci.CBMS_IMAGE_ID
           ,convert(CHAR(1), cng.Sys_CHANGE_OPERATION) AS Sys_Change_Operation
      FROM
            CHANGETABLE(CHANGES dbo.Site_Commodity_Image, @Min_CT_Ver) cng
            INNER JOIN Core.Client_Hier ch
                  ON cng.SITE_id = ch.Site_id
            LEFT JOIN dbo.Site_Commodity_Image sci
                  ON sci.SITE_ID = cng.Site_id
                     AND sci.Commodity_Id = cng.Commodity_Id
                     AND sci.Service_Month = cng.Service_Month
      WHERE
            cng.SYS_CHANGE_VERSION <= @Max_CT_Ver
      GROUP BY
            ch.Client_Hier_Id
           ,cng.Commodity_id
           ,cng.Service_Month
           ,sci.CBMS_IMAGE_ID
           ,cng.Sys_CHANGE_OPERATION     
     
END 


;
GO


GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Changes_Sel_For_Data_Transfer] TO [CBMSApplication]
GRANT EXECUTE ON  [dbo].[Site_Commodity_Image_Changes_Sel_For_Data_Transfer] TO [ETL_Execute]
GO
