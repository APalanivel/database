SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Cu_Exception_Dtl_Del_BY_Cu_Invoice_Id                       
                            
 DESCRIPTION:        
    Remove the records from Cu_Exception_Detail for the given exception type ( defaulted to Failed Recalc).
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Cu_Invoice_Id         INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------        
                 
  BEGIN TRAN          
                   
  EXEC dbo.Cu_Exception_Dtl_Del_BY_Cu_Invoice_Id @Cu_Invoice_Id=70303237      
       
  ROLLBACK     
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2018-10-03      D20-236 - Created for DATA2.0.                          
 PR						2018-10-17      D20-236 - Only need to delete recalc exception entry from denorm and detail table, no change required on exception table
******/


CREATE PROC [dbo].[Cu_Exception_Dtl_Del_BY_Cu_Invoice_Id]
      (
      @Cu_Invoice_Id INT
    , @Exception_Type VARCHAR(255) = 'Failed Recalc' )
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @Cu_Exception_Type_Id INT;

      SELECT
            @Cu_Exception_Type_Id = cet.CU_EXCEPTION_TYPE_ID
      FROM
            dbo.CU_EXCEPTION_TYPE cet
      WHERE
            cet.EXCEPTION_TYPE = @Exception_Type;

      DELETE
            ced
      FROM
            dbo.CU_EXCEPTION_DETAIL ced
            INNER JOIN dbo.CU_EXCEPTION ce
                  ON ced.CU_EXCEPTION_ID = ce.CU_EXCEPTION_ID
      WHERE
            ce.CU_INVOICE_ID = @Cu_Invoice_Id
            AND ced.EXCEPTION_TYPE_ID = @Cu_Exception_Type_Id;

END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Exception_Dtl_Del_BY_Cu_Invoice_Id] TO [CBMSApplication]
GO
