SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********                             
NAME:  Workflow.Insert_User_Last_Visited_Filter                          
                        
DESCRIPTION:  This procedure used to save the data in Workflow.User_Last_Visited_Filter table                          
                        
INPUT PARAMETERS:                            
                         
Name        DataType  Default Description                            
-----------------------------------------------------------------------------                         
@User_Last_Visited_Filter_Id   INT      
@User_Info_Id       INT      
@Workflow_Queue_Id      INT      
@Workflow_Queue_Saved_Filter_Query_Id INT      
@Last_Visited_Ts      DATETIME                       
                                 
OUTPUT PARAMETERS:                            
Name    DataType  Default Description                            
------------------------------------------------------------                            
                            
USAGE EXAMPLES:                            
------------------------------------------------------------                            
EXEC Workflow.Insert_User_Last_Visited_Filter                        
                   
AUTHOR INITIALS:                            
Initials  Name                            
------------------------------------------------------------                            
PRV    Pramod Reddy V                         
                        
                        
MODIFICATIONS                             
 Initials Date   Modification                            
------------------------------------------------------------                            
PRV   2019-09-03  SP Created                        
                        
******/      
      
CREATE PROCEDURE [Workflow].[Insert_User_Last_Visited_Filter]      
(      
    @User_Info_Id INT,      
    @Workflow_Queue_Id INT,      
   @Workflow_Queue_Saved_Filter_Name NVARCHAR(250),      
    @Session_Id INT      
)      
AS      
BEGIN      
    SET NOCOUNT ON;      
      
    IF NOT EXISTS      
    (      
        SELECT 1      
        FROM Workflow.User_Last_Visited_Filter      
        WHERE User_Info_Id = @User_Info_Id      
              AND Session_Id = @Session_Id      
    )      
    BEGIN      
      
          
     INSERT INTO Workflow.User_Last_Visited_Filter      
        (      
            User_Info_Id,      
            Workflow_Queue_Id,      
            Workflow_Queue_Saved_Filter_Name,      
            Last_Visited_Ts,      
            Session_Id      
        )      
        VALUES      
        (@User_Info_Id, @Workflow_Queue_Id, @Workflow_Queue_Saved_Filter_Name,GetDate(),      
         @Session_Id);      
      
    END;      
 ELSE    
  BEGIN      
      
          
    UPDATE Workflow.User_Last_Visited_Filter       
    SET  Workflow_Queue_Saved_Filter_Name=  @Workflow_Queue_Saved_Filter_Name, Last_Visited_Ts=GETDATE()    
        WHERE User_Info_Id = @User_Info_Id    AND Session_Id = @Session_Id      
                
    END;      
     
      
END;      

GO
GRANT EXECUTE ON  [Workflow].[Insert_User_Last_Visited_Filter] TO [CBMSApplication]
GO
