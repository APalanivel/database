SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Batch_Dtl_Status_Upd           
                  
Description:                  
   This sproc is used to fill the ICQ Batch Table.          
                               
 Input Parameters:                  
    Name          DataType   Default   Description                    
--------------------------------------------------------------------------------------                    
                           
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
--------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------       
  exec dbo.Invoice_Collection_Batch_Dtl_Status_Upd  
      
Author Initials:                  
    Initials  Name                  
--------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna    
 Modifications:                  
    Initials        Date   Modification                  
--------------------------------------------------------------------------------------                    
    RKV    2017-02-03  Created For Invoice_Collection.             
                 
******/     
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Dtl_Status_Upd]  
      (  
       @Status_Cd INT  
      ,@Invoice_Collection_Batch_Dtl_Id INT  
      ,@Err_Desc NVARCHAR(MAX) = NULL )  
AS  
BEGIN    
      SET NOCOUNT ON;    
          
        
        
      UPDATE  
            dbo.Invoice_Collection_Batch_Dtl  
      SET  
            Status_Cd = @Status_Cd  
           ,Error_Dsc = ISNULL(@Err_Desc, Error_Dsc)  
           ,Last_Change_Ts = GETDATE()  
      WHERE  
            Invoice_Collection_Batch_Dtl_Id = @Invoice_Collection_Batch_Dtl_Id;  
              
           
  
END;    
    
  
  
  
  
;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Dtl_Status_Upd] TO [CBMSApplication]
GO
