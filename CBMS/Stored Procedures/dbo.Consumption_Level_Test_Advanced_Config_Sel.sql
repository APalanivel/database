SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











/******                              
  
                              
 INPUT PARAMETERS:                
                           
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
@Account_Id			INT
@Commodity_ID      INT               NULL   
@service_month	   Datetime
      
                                  
 OUTPUT PARAMETERS:                
                                 
 Name                        DataType         Default       Description              
---------------------------------------------------------------------------------------------------------------            
                              
 USAGE EXAMPLES:                                  
---------------------------------------------------------------------------------------------------------------                                  
      
--Commodity Level      
   
DECLARE	@return_value INT



EXEC	@return_value = [dbo].[Consumption_Level_Test_Advanced_Config_Sel]
		@Account_Id =1407581 , --1407581 1405819
		@Commodity_Id = 290,
		@Service_Month = N'2017-12-01'

SELECT	'Return Value' = @return_value

GO

  
     
                             
 AUTHOR INITIALS:    
 Arunkumar Palanivel AP          
             
 Initials              Name              
---------------------------------------------------------------------------------------------------------------  
     Steps(added by Arun):

	 1.Find the buckets associated to the account for the commodity and service month.
	 2.If the bucket is Total cost, then we have to give total cost.
		For total cost , we have to give additional info as per the above rule.
	3.check 12 of 18 data points to decide linear analogous

	Method Identification - 12 of 18 Rule
If the selected account has USAGE values for 12 or more months out of the last 18 months (start counting backwards from/not including the service month of the invoice start date), the query service will select Linear Regression variance method and continue with history query.

Note that 12 out of 18 rule does not require that the values be in 12 consecutive months. Just require 12 of 18 months to have usage values.

A value of 0 counts as a value and should be counted toward 12 of 18 rule.

Months marked as "No Data This Period" do NOT count as values toward the 12 of 18 rule.

Estimated usage values (either created by Gap-Filling procedure or entered by a user) will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months with manually-entered values will NOT count as values toward the 12 of 18 rule.

REQUIREMENT UPDATE (1/17/2020): Months that were manually excluded as anomalous usage (see Anomalous Month section within Review Variance page) will NOT count as values toward the 12 of 18 rule.

If the selected account has usage values for less than 12 months out of the last 18 months, the query service will select Analogous Account variance method.
	4.It is possible that for same account /commodity one category can be linear and another one can be analogus. in that case, we have to send two request.
	however SP will give only on result set. 
	5.
                               
 MODIFICATIONS:            
                
 Initials              Date             Modification            
---------------------------------------------------------------------------------------------------------------   
AP					Jan 21 2020			New procedure to get data points for variance testing (as per the requirement mentioned above)         
   
******/
CREATE PROCEDURE [dbo].[Consumption_Level_Test_Advanced_Config_Sel]
      
AS
      BEGIN

            SET NOCOUNT ON;



			
          SELECT a.param_value FROM 
		  
		   ( SELECT
                   vepv.Param_Value , 2 AS rn
            FROM  dbo.Variance_Extraction_Service_Param vesp
                  INNER JOIN
                  dbo.Variance_Engine_Param_Value vepv
                        ON vepv.Variance_Extraction_Service_Param_Id = vesp.Variance_Extraction_Service_Param_Id
            WHERE vesp.Param_Name = 'Extraction_Service_History_Rule_Of'

			UNION
              SELECT
                   vepv.Param_Value,1 AS rn
            FROM  dbo.Variance_Extraction_Service_Param vesp
                  INNER JOIN
                  dbo.Variance_Engine_Param_Value vepv
                        ON vepv.Variance_Extraction_Service_Param_Id = vesp.Variance_Extraction_Service_Param_Id
            WHERE vesp.Param_Name = 'Extraction_Service_History_Rule_Use'

			 UNION
			
              SELECT
                   vepv.Param_Value,3 AS rn
            FROM  dbo.Variance_Extraction_Service_Param vesp
                  INNER JOIN
                  dbo.Variance_Engine_Param_Value vepv
                        ON vepv.Variance_Extraction_Service_Param_Id = vesp.Variance_Extraction_Service_Param_Id
            WHERE vesp.Param_Name = 'Linear_Regression_Outlier_Analysis')A
			ORDER BY a.rn      END;
GO
GRANT EXECUTE ON  [dbo].[Consumption_Level_Test_Advanced_Config_Sel] TO [CBMSApplication]
GO
