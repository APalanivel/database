SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******        
NAME:        
      dbo.Utility_Account_Commodity_Analyst_Merge_All      
         
 DESCRIPTION:         
      Created to to Inset data when a Auto Fill is selected on Analyst Mapping Page Account Level      
            
 INPUT PARAMETERS:        
 Name   DataType Default Description        
------------------------------------------------------------        
 @Vendor_Id  INT      
 @Commodity_Id INT      
 @Analyst_User_Info_Id  INT        
       
 OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
 USAGE EXAMPLES:        
------------------------------------------------------------      
      
 EXEC dbo.Utility_Account_Commodity_Analyst_Merge_All 10,290,11566      
 EXEC dbo.Utility_Account_Commodity_Analyst_Merge_All 25,291,11566      
      
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------       
 AKR  Ashok Kumar Raju      
       
      
 MODIFICATIONS         
 Initials Date  Modification      
------------------------------------------------------------      
 AKR        2012-09-26  Created for POCO      
      
******/      
      
CREATE  PROCEDURE dbo.Utility_Account_Commodity_Analyst_Merge_All
      ( 
       @Vendor_Id INT
      ,@Commodity_Id INT
      ,@Analyst_User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON      
            
      DECLARE @Commodity_Service_Cd INT      
            
      CREATE TABLE #Site_Accounts
            ( 
             Account_Id INT
            ,Site_Analyst_User_Info_Id INT )      
            
      SELECT
            @Commodity_Service_Cd = Code_Id
      FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                  ON c.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommodityServiceSource'
            AND c.Code_Value = 'Invoice'      
      
      INSERT      INTO #Site_Accounts
                  ( 
                   Account_Id
                  ,Site_Analyst_User_Info_Id )
                  SELECT
                        cha.Account_Id
                       ,coalesce(sca.Analyst_User_Info_Id, ca.Analyst_User_Info_Id) Analyst_User_Info_Id
                  FROM
                        dbo.VENDOR_COMMODITY_MAP vcam
                        INNER JOIN core.Client_Hier_Account cha
                              ON cha.Account_Vendor_Id = vcam.VENDOR_ID
                                 AND vcam.COMMODITY_TYPE_ID = cha.Commodity_Id
                        INNER JOIN core.Client_Hier ch
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        INNER JOIN Core.Client_Commodity cc
                              ON ch.Client_Id = cc.Client_Id
                                 AND cc.Commodity_Id = @Commodity_Id
                                 AND cc.Commodity_Service_Cd = @Commodity_Service_Cd
                        LEFT JOIN dbo.Site_Commodity_Analyst sca
                              ON ch.Site_Id = sca.Site_Id
                                 AND sca.Commodity_Id = @Commodity_Id
                        LEFT JOIN dbo.Client_Commodity_Analyst ca
                              ON ca.Client_Commodity_Id = cc.Client_Commodity_Id
                  WHERE
                        vcam.COMMODITY_TYPE_ID = @Commodity_Id
                        AND vcam.VENDOR_ID = @Vendor_Id
                        AND cha.Account_Type = 'Utility'
                  GROUP BY
                        cha.Account_Id
                       ,sca.Analyst_User_Info_Id
                       ,ca.Analyst_User_Info_Id       
                            
      BEGIN TRY                    
            BEGIN TRAN        
                
                   
            DELETE
                  aca
            FROM
                  dbo.Account_Commodity_Analyst aca
                  INNER JOIN #Site_Accounts sa
                        ON aca.Account_Id = sa.Account_Id
                           AND Commodity_Id = @Commodity_Id
                           AND ( @Analyst_User_Info_Id IS NULL
                                 OR sa.Site_Analyst_User_Info_Id = @Analyst_User_Info_Id )    
             
            IF ( @Analyst_User_Info_Id IS NOT NULL ) 
                  BEGIN       
                        MERGE INTO dbo.Account_Commodity_Analyst AS tgt
                              USING 
                                    ( SELECT
                                          Account_Id
                                         ,Site_Analyst_User_Info_Id
                                         ,@Analyst_User_Info_Id Account_Analyst_User_Info_Id
                                      FROM
                                          #Site_Accounts ) src
                              ON src.Account_Id = tgt.Account_Id
                                    AND tgt.Commodity_Id = @Commodity_Id
                              WHEN MATCHED 
                                    THEN      
       UPDATE                           SET
                                          tgt.Analyst_User_Info_Id = src.Account_Analyst_User_Info_Id
                              WHEN NOT MATCHED AND @Analyst_User_Info_Id != isnull(src.Site_Analyst_User_Info_Id, 0)
                                    THEN       
         INSERT
                                          ( 
                                           Account_Id
                                          ,Commodity_Id
                                          ,Analyst_User_Info_Id )
                                        VALUES
                                          ( 
                                           src.Account_Id
                                          ,@Commodity_Id
                                          ,src.Account_Analyst_User_Info_Id ) ;    
                  END      
                
            COMMIT TRAN                    
      END TRY                    
      BEGIN CATCH                    
            IF @@TRANCOUNT > 0 
                  ROLLBACK TRAN                    
                    
            EXEC dbo.usp_RethrowError                    
                    
      END CATCH       
          
END 
;
GO
GRANT EXECUTE ON  [dbo].[Utility_Account_Commodity_Analyst_Merge_All] TO [CBMSApplication]
GO
