SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 CBMS.Budget.Cost_Usage_And_Invoice_Dtl_Sel_By_Account_Id  
    
DESCRIPTION:    
  Script to get Invoices for that account's for a given time period   
    
INPUT PARAMETERS:    
 Name     DataType  Default Description    
---------------------------------------------------------------    
 @Cu_Invoice_Id     INT    
 @dtStart_Date      Date  
 @dtEnd_Date  Date  
    
OUTPUT PARAMETERS:    
 Name     DataType  Default Description    
------------------------------------------------------------    
     
USAGE EXAMPLES:    
------------------------------------------------------------    
   --priority implementation 
 EXEC Budget.[Cost_Usage_And_Invoice_Dtl_Sel_By_Account_Id] 1458983 ,'2017-01-01','2018-12-01',1077438 
   
    
   
 --For SubBuckets  
EXEC Budget.Cost_Usage_And_Invoice_Dtl_Sel_By_Account_Id @intAccount_ID = 26654,@Mter_Id = 20442,@dtStart_Date = '2001-02-01',@dtEnd_Date = '2019-03-03', @Converted_uom_Id = 12 ;  
    
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 PRG  Prasanna R Gachinmani  
  
     
MODIFICATIONS    
 Initials Date   Modification    
------------------------------------------------------------    
 PRG      2019-04-10 Initial Development  
 RKV      2019-06-05 changed the source table to Cost usage account table  
******/
CREATE PROC [Budget].[Cost_Usage_And_Invoice_Dtl_Sel_By_Account_Id]
     (
         @intAccount_ID INT
         , @dtStart_Date DATETIME
         , @dtEnd_Date DATETIME
         , @Meter_Id INT
         , @Converted_uom_Id INT = NULL
         , @Currency_Unit_Id INT = NULL
     )
AS
    BEGIN
        CREATE TABLE #Client_Hier_Account
             (
                 Client_Hier_Id INT
                 , Account_Id INT
             );


        DECLARE
            @Client_Currency_Group_Id INT
            , @Commodity_Id INT
            , @Max_Uom_Type_Id INT
            , @Uom_Type_Name VARCHAR(MAX)
            , @Invoice_Data_Source_Cd INT;

        DECLARE @Max_Uom TABLE
              (
                  Uom_Type_Id INT
                  , Max_Cnt INT
              );

        SELECT
            @Client_Currency_Group_Id = Client_Currency_Group_Id
            , @Commodity_Id = cha.Commodity_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @intAccount_ID
            AND cha.Meter_Id = @Meter_Id
        GROUP BY
            Client_Currency_Group_Id
            , cha.Commodity_Id;

        SELECT
            @Invoice_Data_Source_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'DataSource'
            AND c.Code_Value = 'Invoice';



        INSERT INTO #Client_Hier_Account
             (
                 Client_Hier_Id
                 , Account_Id
             )
        SELECT
            ch.Client_Hier_Id
            , cha.Account_Id
        FROM
            Core.Client_Hier ch
            INNER JOIN Core.Client_Hier_Account AS cha
                ON cha.Client_Hier_Id = ch.Client_Hier_Id
        WHERE
            cha.Account_Id = @intAccount_ID
        GROUP BY
            ch.Client_Hier_Id
            , cha.Account_Id;

        INSERT  @Max_Uom
             (
                 Uom_Type_Id
                 , Max_Cnt
             )
        SELECT
            cuad.UOM_Type_Id
            , COUNT(cuad.UOM_Type_Id) cnt
        FROM
            dbo.Cost_Usage_Account_Dtl AS cuad
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
        WHERE
            cuad.Service_Month BETWEEN @dtStart_Date
                               AND     @dtEnd_Date
            AND bm.Commodity_Id = @Commodity_Id
            AND bm.Bucket_Name = 'Total Usage'
            AND cuad.Data_Source_Cd = @Invoice_Data_Source_Cd
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Client_Hier_Account cha
                           WHERE
                                cha.Account_Id = cuad.ACCOUNT_ID
                                AND cha.Client_Hier_Id = cuad.Client_Hier_ID)
        GROUP BY
            cuad.UOM_Type_Id;


        SELECT  TOP 1
                @Max_Uom_Type_Id = Uom_Type_Id
        FROM
            @Max_Uom
        ORDER BY
            Max_Cnt DESC;

        SELECT
            @Uom_Type_Name = e.ENTITY_NAME
        FROM
            dbo.ENTITY e
        WHERE
            e.ENTITY_ID = @Max_Uom_Type_Id;

        SELECT
            cuad.Service_Month
            , LEFT(rn.CU_Invoice_Id, LEN(rn.CU_Invoice_Id) - 1) AS CU_Invoice_Id
            , LEFT(rn_image.CBMS_Image_Id, LEN(rn_image.CBMS_Image_Id) - 1) AS CBMS_Image_Id
            , SUM(CASE WHEN bm.Bucket_Name = 'Total Cost' THEN cuad.Bucket_Value * cuc.CONVERSION_FACTOR
                  END) Charge_Value
            , SUM(CASE WHEN bm.Bucket_Name = 'Total Usage' THEN cuad.Bucket_Value * ccon.CONVERSION_FACTOR
                  END) Determinant_Value
            , MAX(CASE WHEN @Max_Uom_Type_Id <> cuad.UOM_Type_Id
                            AND bm.Bucket_Name = 'Total Usage' THEN 1
                      ELSE 0
                  END) IS_Converted
            , @Uom_Type_Name AS Uom_Type_Name
        INTO
            #Cost_Usage_Account_Dtls
        FROM
            dbo.Cost_Usage_Account_Dtl AS cuad
            INNER JOIN dbo.Bucket_Master bm
                ON bm.Bucket_Master_Id = cuad.Bucket_Master_Id
            LEFT OUTER JOIN dbo.Code c
                ON c.Code_Id = bm.Bucket_Type_Cd
            LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION ccon
                ON ccon.BASE_UNIT_ID = cuad.UOM_Type_Id
                   AND  ccon.CONVERTED_UNIT_ID = COALESCE(@Max_Uom_Type_Id, bm.Default_Uom_Type_Id)
                   AND  c.Code_Value = 'determinant'
            LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                ON cuc.BASE_UNIT_ID = cuad.CURRENCY_UNIT_ID
                   AND  cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                   AND  cuc.CONVERSION_DATE = cuad.Service_Month
                   AND  cuc.CONVERTED_UNIT_ID = COALESCE(@Currency_Unit_Id, cuad.CURRENCY_UNIT_ID)
                   AND  c.Code_Value = 'Charge'
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, csm.CU_INVOICE_ID) + ','
            FROM
                dbo.CU_INVOICE_SERVICE_MONTH csm
                INNER JOIN dbo.CU_INVOICE AS ci
                    ON ci.CU_INVOICE_ID = csm.CU_INVOICE_ID
            WHERE
                cuad.Service_Month = csm.SERVICE_MONTH
                AND cuad.ACCOUNT_ID = csm.Account_ID
                AND ci.IS_REPORTED = 1
            FOR XML PATH('')) rn(CU_Invoice_Id)
            CROSS APPLY
        (   SELECT
                CONVERT(VARCHAR, ci.CBMS_IMAGE_ID) + ','
            FROM
                dbo.CU_INVOICE ci
                JOIN dbo.CU_INVOICE_SERVICE_MONTH csm
                    ON ci.CU_INVOICE_ID = csm.CU_INVOICE_ID
            WHERE
                cuad.Service_Month = csm.SERVICE_MONTH
                AND cuad.ACCOUNT_ID = csm.Account_ID
                AND ci.IS_REPORTED = 1
            FOR XML PATH('')) rn_image(CBMS_Image_Id)
        WHERE
            cuad.Service_Month BETWEEN @dtStart_Date
                               AND     @dtEnd_Date
            AND bm.Commodity_Id = @Commodity_Id
            AND cuad.Data_Source_Cd = @Invoice_Data_Source_Cd
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Client_Hier_Account cha
                           WHERE
                                cha.Account_Id = cuad.ACCOUNT_ID
                                AND cha.Client_Hier_Id = cuad.Client_Hier_ID)
        GROUP BY
            cuad.Service_Month
            , LEFT(rn.CU_Invoice_Id, LEN(rn.CU_Invoice_Id) - 1)
            , LEFT(rn_image.CBMS_Image_Id, LEN(rn_image.CBMS_Image_Id) - 1);


        SELECT
            Service_Month
            , CU_Invoice_Id
            , CBMS_Image_Id
            , Charge_Value
            , CASE WHEN Determinant_Value > 10 THEN CAST(CAST(ROUND(Determinant_Value, 0) AS INT) AS VARCHAR(50))
                  ELSE CAST(Determinant_Value AS VARCHAR(50))
              END Determinant_Value
            , IS_Converted
            , Uom_Type_Name
        FROM
            #Cost_Usage_Account_Dtls AS cuad;
        DROP TABLE #Cost_Usage_Account_Dtls;

    END;






GO
GRANT EXECUTE ON  [Budget].[Cost_Usage_And_Invoice_Dtl_Sel_By_Account_Id] TO [CBMSApplication]
GO
