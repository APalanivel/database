SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsCuInvoiceEvent_GetForCuInvoice

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@cu_invoice_id 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE     PROCEDURE [dbo].[cbmsCuInvoiceEvent_GetForCuInvoice]
	( @MyAccountId int
	, @cu_invoice_id int
	)
AS
BEGIN


	   select x.cu_invoice_event_id
		, x.cu_invoice_id
		, x.event_date
--		, convert(varchar, x.event_date, 101) event_date
		, ui.first_name + ' ' + ui.last_name event_by
		, x.event_description
	     from cu_invoice_event x
	     join user_info ui on ui.user_info_id = x.event_by_id
	    where x.cu_invoice_id = @cu_invoice_id
	 order by x.event_date asc

END
GO
GRANT EXECUTE ON  [dbo].[cbmsCuInvoiceEvent_GetForCuInvoice] TO [CBMSApplication]
GO
