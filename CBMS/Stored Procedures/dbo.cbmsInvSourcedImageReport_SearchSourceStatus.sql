SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.cbmsInvSourcedImageReport_SearchSourceStatus

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	
	@start_date    	datetime  	          	
	@end_date      	datetime  	          	
	@status_type_id	int       	null      	
	@inv_source_label	varchar(100)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE  procedure [dbo].[cbmsInvSourcedImageReport_SearchSourceStatus]
(
	@MyAccountId int
	,@start_date datetime
	,@end_date datetime
	,@status_type_id int = null
	,@inv_source_label varchar(100) = null
)
as
BEGIN

	set nocount on

	if @start_date is null set @start_date = '1/1/2005'
	if @end_date is null set @end_date = '1/1/' + convert(varchar, year(getdate()) + 1)

--	set nocount off


if @status_type_id is null
	begin

	   select inv_source_label
		, status_type
		, count(*) as rpt_count
	     from inv_sourced_image_report with (nolock)
	    where (sent_date between @start_date and @end_date
			or received_date between @start_date and @end_date
			)
	      and inv_source_label = coalesce(@inv_source_label, inv_source_label)
	group by inv_source_label, status_type

	end

else
	begin

	   select	inv_source_label
		, status_type
		, count(*) as rpt_count
	     from inv_sourced_image_report with (nolock)
	    where	(case @status_type_id 
			when 1072 then sent_date 	--not received
			when 1073 then received_date 	--received
			when 1074 then received_date	--exception
			end) between @start_date and @end_date

	      and status_type_id = @status_type_id
	      and inv_source_label = coalesce(@inv_source_label, inv_source_label)
	group by inv_source_label, status_type
	end

END
GO
GRANT EXECUTE ON  [dbo].[cbmsInvSourcedImageReport_SearchSourceStatus] TO [CBMSApplication]
GO
