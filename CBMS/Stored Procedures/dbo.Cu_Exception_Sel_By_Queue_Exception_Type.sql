SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********           
    
NAME:    
 [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type]        
           
DESCRIPTION:              
   Returns only Failed Recalc Queue .    
          
 INPUT PARAMETERS:              
                         
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          
 @Queue_Id					INT					NULL    
 @Client_Name				VARCHAR(200)		NULL      
 @City						VARCHAR(200)		NULL     
 @Vendor_Name				VARCHAR(200)		NULL     
 @Account_Number			VARCHAR(200)		NULL    
 @Site_Name					VARCHAR(200)		NULL    

 OUTPUT PARAMETERS:
 Name                        DataType         Default       Description            
---------------------------------------------------------------------------------------------------------------          

 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------                                

    
 EXEC [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type]    
   @Queue_Id=152    
  ,@Client_Name=null    
  ,@City=null    
  ,@Vendor_Name=null    
  ,@Account_Number=null    
  ,@Site_Name=null    
  ,@Recalc_Failed_Reason_Type_Cd = NULL    
      
EXEC [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type]    
   @Queue_Id=80391, @Recalc_Type_cd = 100315  
EXEC [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type]    
   @Queue_Id=32464,@Exception_Type='Outside Contract Term' --Unknown Account    
       
SELECT top 100 * FROM Cu_Exception_Denorm ced WHERE ced.EXCEPTION_TYPE='Failed Recalc'    
SELECT * FROM Cu_Exception_Denorm ced WHERE ced.EXCEPTION_TYPE<>'Failed Recalc'    
    
          
 AUTHOR INITIALS:            
           
 Initials           Name            
---------------------------------------------------------------------------------------------------------------                          
 SP					Sandeep Pigilam     
 RKV				Ravi Kumar Vegesna              
    
      
 MODIFICATIONS:          
              
	Initials    Date        Modification          
---------------------------------------------------------------------------------------------------------------          
	SP          2014-05-22  Created Data Operations Enhancement Phase-2                 
							Removed ced.is_manual = 0 condition as the expectation to see the failed recalc irrespective of it is manual or not    
	SP			2014-07-25	Data Operations Enhancement Phase III,Changed logic of contract_recalc_type to return utility recalc when     
							account type is Utility              
	RKV         2015-09-10	AS400-PII Added two parmeters @Recalc_Type and @Recalc_Failed_Reason_Type_Cd and also changed utility recalc type to manula recalc             
	RKV         2016-09-21  Issue Fixed when a particular recalc_type  ?Utility - System Recalc - All? is slected it is given the result set for all the recalc_types
	SP			2017-05-17	small Enhancement,SE2017-124 added filters	@State_Id and removed @State_Name,@Country_Id INT = NULL,@Commodity_Id INT = NULL,@Supplier_Name VARCHAR(500) = NULL																
	RR			2017-07-11	MAINT-5521 Resetting supplier account begin date to first day of the month to match the CU service month 
	RKV         2017-11-22  SE2017-124,Added Parameter @Exception_Status_Type
	SP			2018-01-31	MAINT-6731, Invoice service month(cism) is replaced with invoice start date and invoice end date .
	RKV         2018-10-17  D20-236, Added New Column Opened Date.
	RKV         2019-07-23  MAINT- 9078 picking the recalc_type based on service_Month instead of system date.
	NR			2019-10-30	Add Contract - For Supplier accounts to get the recalc from account commodity recalc table.
	HG			2020-06-19	SE2017-980, Modified the procedure to use Invoice service dates instead of service month to get the recalc types.
*/
CREATE PROCEDURE [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type]
    (
        @Queue_Id INT = NULL
        , @Client_Name VARCHAR(200) = NULL
        , @City VARCHAR(200) = NULL
        , @Vendor_Name VARCHAR(200) = NULL
        , @Account_Number VARCHAR(200) = NULL
        , @Site_Name VARCHAR(200) = NULL
        , @Exception_Type VARCHAR(200) = 'Failed Recalc'
        , @Recalc_Type_cd INT = NULL
        , @Recalc_Failed_Reason_Type_Cd INT = NULL
        , @Country_Id INT = NULL
        , @Commodity_Id INT = NULL
        , @Supplier_Name VARCHAR(500) = NULL
        , @State_Id INT = NULL
        , @ED_CONTRACT_NUMBER VARCHAR(150) = NULL
        , @Exception_Status_Type VARCHAR(200) = NULL
    )
AS
    BEGIN

        SET NOCOUNT ON;

        SET @Client_Name = REPLACE(@Client_Name, '''', '''''');
        SET @City = REPLACE(@City, '''', '''''');
        SET @Vendor_Name = REPLACE(@Vendor_Name, '''', '''''');
        SET @Account_Number = REPLACE(@Account_Number, '''', '''''');
        SET @Account_Number = REPLACE(@Account_Number, SPACE(1), SPACE(0));
        SET @Account_Number = REPLACE(@Account_Number, '-', SPACE(0));
        SET @Account_Number = REPLACE(@Account_Number, '/', SPACE(0));
        SET @Site_Name = REPLACE(@Site_Name, '''', '''''');

        DECLARE @Has_Supplier_Recalc BIT = 0;

        SELECT
            @Has_Supplier_Recalc = 1
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            cs.Codeset_Name = 'Recalculation_Type'
            AND cs.Std_Column_Name = 'Contract_Recalc_Type_id'
            AND c.Code_Id = @Recalc_Type_cd;

        SELECT
            ced.CBMS_IMAGE_ID
            , ced.CBMS_DOC_ID
            , ced.CU_INVOICE_ID
            , ced.QUEUE_ID
            , ced.EXCEPTION_TYPE
            , ced.EXCEPTION_STATUS_TYPE
            , ISNULL(cha.Account_Number, ced.UBM_Account_Number) Account_number
            , (ced.SERVICE_MONTH) service_month
            , CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                  ELSE ced.UBM_Client_Name
              END AS Client_Name
            , CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                  ELSE ced.UBM_Site_Name
              END site_name
            , MAX(CASE WHEN AG.ACCOUNT_GROUP_ID IS NOT NULL
                            AND ced.Account_ID IS NOT NULL
                            AND vage.ENTITY_NAME = 'Utility' THEN vag.VENDOR_NAME -- Invoice resolved to an account and it is group account
                      WHEN cha.Account_Id IS NOT NULL THEN
                          CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                              WHEN cha.Account_Type = 'Supplier' THEN LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                          END                                                     -- Inovoice resolved to an account and it is not a group account
                      ELSE Inv_Lbl.VENDOR_NAME                                    -- Invoice not resolved to an account
                  END) AS Utility_Vendor_Name
            , ced.DATE_IN_QUEUE
            , ced.SORT_ORDER
            , CASE WHEN cha.Account_Type = 'Utility' THEN 'Manual Recalc'
                  ELSE COALESCE(rt.Code_Value, con_rec.Code_Value, irtc.Code_Value)
              END AS contract_recalc_type
            , com.Commodity_Name
            , ced.Account_ID
            , com.Commodity_Id
            , cha.Account_Type
            , rfrt.Code_Dsc AS Recalc_Failed_Reason_Type
            , CASE WHEN cha.Account_Type = 'Supplier' THEN 'Manual Recalc'
                  ELSE COALESCE(irtc.Code_Value, rtc.Code_Value, rt.Code_Value)
              END AS Recalc_Type
            , ced.Recalc_Response_Error_Dsc
            , MAX(ISNULL((CASE WHEN AG.ACCOUNT_GROUP_ID IS NOT NULL
                                    AND ced.Account_ID IS NOT NULL
                                    AND vage.ENTITY_NAME = 'Supplier' THEN vag.VENDOR_NAME
                              WHEN cha.Account_Id IS NOT NULL
                                   AND  cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                          END), scha.Account_Vendor_Name)) AS Supplier_Vendor_Name
            , ce.OPENED_DATE
        INTO
            #Cu_Exception_Dtl
        FROM
            dbo.CU_EXCEPTION_DENORM ced
            INNER JOIN dbo.CU_INVOICE ci
                ON ci.CU_INVOICE_ID = ced.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
                   AND  cism.Account_ID = ced.Account_ID
            LEFT OUTER JOIN dbo.CU_EXCEPTION ce
                ON ced.CU_INVOICE_ID = ce.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.ACCOUNT_GROUP AG
                ON AG.ACCOUNT_GROUP_ID = ci.ACCOUNT_GROUP_ID
            LEFT OUTER JOIN dbo.VENDOR vag
                ON vag.VENDOR_ID = AG.VENDOR_ID
            LEFT JOIN dbo.ENTITY vage
                ON vag.VENDOR_TYPE_ID = vage.ENTITY_ID
            LEFT OUTER JOIN Core.Client_Hier ch
                ON ch.Client_Hier_Id = ced.Client_Hier_ID
            LEFT OUTER JOIN Core.Client_Hier_Account cha
                ON cha.Client_Hier_Id = ced.Client_Hier_ID
                   AND  cha.Account_Id = ced.Account_ID
                   AND  (   cha.Commodity_Id = ced.Commodity_Id
                            OR  ced.Commodity_Id IS NULL)
            LEFT OUTER JOIN dbo.Commodity com
                ON com.Commodity_Id = cha.Commodity_Id
            LEFT OUTER JOIN dbo.CU_INVOICE_LABEL Inv_Lbl
                ON ci.CU_INVOICE_ID = Inv_Lbl.CU_INVOICE_ID
            LEFT OUTER JOIN dbo.Code rfrt
                ON rfrt.Code_Id = ced.Recalc_Failed_Reason_Type_cd
            LEFT OUTER JOIN dbo.Code rtc
                ON rtc.Code_Id = ced.Recalc_Type_cd
            ---
            LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type acirt
                ON ced.Account_ID = acirt.Account_Id
                   AND  cha.Commodity_Id = acirt.Commodity_ID
                   AND  (   cism.Begin_Dt BETWEEN acirt.Start_Dt
                                          AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  cism.End_Dt BETWEEN acirt.Start_Dt
                                            AND     ISNULL(acirt.End_Dt, '9999-12-31')
                            OR  acirt.Start_Dt BETWEEN cism.Begin_Dt
                                               AND     cism.End_Dt
                            OR  ISNULL(acirt.End_Dt, '9999-12-31') BETWEEN cism.Begin_Dt
                                                                   AND     cism.End_Dt)
            LEFT OUTER JOIN dbo.Code irtc
                ON acirt.Invoice_Recalc_Type_Cd = irtc.Code_Id
            LEFT OUTER JOIN dbo.Account_Commodity_Invoice_Recalc_Type sup_recalc
                ON ced.Account_ID = sup_recalc.Account_Id
                   AND  cha.Commodity_Id = sup_recalc.Commodity_ID
                   AND  (   cism.Begin_Dt BETWEEN sup_recalc.Start_Dt
                                          AND     ISNULL(sup_recalc.End_Dt, '9999-12-31')
                            OR  cism.End_Dt BETWEEN sup_recalc.Start_Dt
                                            AND     ISNULL(sup_recalc.End_Dt, '9999-12-31')
                            OR  sup_recalc.Start_Dt BETWEEN cism.Begin_Dt
                                                    AND     cism.End_Dt
                            OR  ISNULL(sup_recalc.End_Dt, '9999-12-31') BETWEEN cism.Begin_Dt
                                                                        AND     cism.End_Dt)
            LEFT OUTER JOIN dbo.Code rt
                ON rt.Code_Id = sup_recalc.Invoice_Recalc_Type_Cd
            LEFT JOIN Core.Client_Hier_Account scha
                ON cha.Client_Hier_Id = scha.Client_Hier_Id
                   AND  cha.Meter_Id = scha.Meter_Id
                   AND  scha.Account_Type = 'Supplier'
                   AND  (   (cism.Begin_Dt BETWEEN scha.Supplier_Account_begin_Dt
                                           AND     scha.Supplier_Account_End_Dt)
                            OR  (cism.End_Dt BETWEEN scha.Supplier_Account_begin_Dt
                                             AND     scha.Supplier_Account_End_Dt))
            LEFT JOIN(dbo.Contract_Recalc_Config crc
                      INNER JOIN dbo.Code con_rec
                          ON con_rec.Code_Id = crc.Supplier_Recalc_Type_Cd)
                ON crc.Contract_Id = scha.Supplier_Contract_ID
                   AND  crc.Commodity_Id = scha.Commodity_Id
                   AND  crc.Is_Consolidated_Contract_Config = 0
                   AND  crc.Start_Dt = sup_recalc.Start_Dt
                   AND  ISNULL(crc.End_Dt, '9999-12-31') = ISNULL(sup_recalc.End_Dt, '9999-12-31')
                   AND  sup_recalc.Source_Cd IS NOT NULL
            OUTER APPLY (   SELECT
                                uac.Account_Vendor_Name + ', '
                            FROM
                                Core.Client_Hier_Account uac
                                INNER JOIN Core.Client_Hier_Account sac
                                    ON sac.Meter_Id = uac.Meter_Id
                            WHERE
                                uac.Account_Type = 'Utility'
                                AND sac.Account_Type = 'Supplier'
                                AND cha.Account_Type = 'Supplier'
                                AND sac.Account_Id = cha.Account_Id
                            GROUP BY
                                uac.Account_Vendor_Name
                            FOR XML PATH('')) UV(Vendor_Name)
        WHERE
            (   @Queue_Id IS NULL
                OR  ced.QUEUE_ID = CONVERT(VARCHAR, @Queue_Id))
            AND (   @Exception_Status_Type IS NULL
                    OR  ced.EXCEPTION_STATUS_TYPE = @Exception_Status_Type)
            AND (   (@Client_Name IS NULL)
                    OR  (ISNULL(ch.Client_Name, ced.UBM_Client_Name) LIKE '%' + @Client_Name + '%'))
            AND (   (@City IS NULL)
                    OR  (ISNULL(ch.City, ced.UBM_City) LIKE '%' + @City + '%'))
            AND (   (@State_Id IS NULL)
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.STATE s
                                   WHERE
                                        s.STATE_ID = @State_Id
                                        AND (ISNULL(ch.State_Name, UBM_State_Name) = s.STATE_NAME)))
            AND (   @Vendor_Name IS NULL
                    OR  (COALESCE(
                             CASE WHEN vage.ENTITY_NAME = 'Utility' THEN vag.VENDOR_NAME
                             END
                             , CASE WHEN cha.Account_Type = 'Utility' THEN cha.Account_Vendor_Name
                                   WHEN cha.Account_Type = 'Supplier' THEN
                                       LEFT(UV.Vendor_Name, LEN(UV.Vendor_Name) - 1)
                               END, Inv_Lbl.VENDOR_NAME) LIKE '%' + @Vendor_Name + '%'))
            AND (   (@Account_Number IS NULL)
                    OR  (   (LEN(@Account_Number) >= 4)
                            AND (REPLACE(
                                     REPLACE(
                                         REPLACE(
                                             (ISNULL(
                                                  AG.GROUP_BILLING_NUMBER
                                                  , ISNULL(cha.Account_Number, ced.UBM_Account_Number))), '/', SPACE(0))
                                         , SPACE(1), SPACE(0)), '-', SPACE(0)) LIKE '%' + @Account_Number + '%'))
                    OR  (   (LEN(@Account_Number) <= 4)
                            AND REPLACE(
                                    REPLACE(
                                        REPLACE(
                                            (ISNULL(
                                                 AG.GROUP_BILLING_NUMBER
                                                 , ISNULL(cha.Account_Number, ced.UBM_Account_Number))), '/', SPACE(0))
                                        , SPACE(1), SPACE(0)), '-', SPACE(0)) = @Account_Number))
            AND (   (@Site_Name IS NULL)
                    OR  (ISNULL(ch.Site_name, ced.UBM_Site_Name) LIKE '%' + @Site_Name + '%'))
            AND ced.EXCEPTION_TYPE = @Exception_Type
            AND (   (@Recalc_Type_cd IS NULL)
                    OR  (   @Has_Supplier_Recalc = 1
                            AND COALESCE(rt.Code_Id, con_rec.Code_Id, irtc.Code_Id) = @Recalc_Type_cd)
                    OR  (   @Has_Supplier_Recalc = 0
                            AND COALESCE(irtc.Code_Id, rtc.Code_Id, rt.Code_Id) = @Recalc_Type_cd))
            AND (   @Recalc_Failed_Reason_Type_Cd IS NULL
                    OR  ced.Recalc_Failed_Reason_Type_cd = @Recalc_Failed_Reason_Type_Cd)
            AND (   @Country_Id IS NULL
                    OR  ch.Country_Id = @Country_Id)
            AND (   @Commodity_Id IS NULL
                    OR  cha.Commodity_Id = @Commodity_Id)
            AND (   @Supplier_Name IS NULL
                    OR  (COALESCE(CASE WHEN vage.ENTITY_NAME = 'Supplier' THEN vag.VENDOR_NAME
                                  END, CASE WHEN cha.Account_Type = 'Supplier' THEN cha.Account_Vendor_Name
                                       END, scha.Account_Vendor_Name) LIKE '%' + @Supplier_Name + '%'))
            AND (   @ED_CONTRACT_NUMBER IS NULL
                    OR  EXISTS (   SELECT
                                        1
                                   FROM
                                        dbo.CONTRACT c
                                   WHERE
                                        c.CONTRACT_ID = scha.Supplier_Contract_ID
                                        AND c.ED_CONTRACT_NUMBER LIKE '%' + @ED_CONTRACT_NUMBER + '%'))
        GROUP BY
            ced.CBMS_IMAGE_ID
            , ced.CBMS_DOC_ID
            , ced.CU_INVOICE_ID
            , ced.QUEUE_ID
            , ced.EXCEPTION_TYPE
            , ced.EXCEPTION_STATUS_TYPE
            , ISNULL(cha.Account_Number, ced.UBM_Account_Number)
            , (ced.SERVICE_MONTH)
            , CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Client_Name
                  ELSE ced.UBM_Client_Name
              END
            , CASE WHEN ced.Client_Hier_ID IS NOT NULL THEN ch.Site_name
                  ELSE ced.UBM_Site_Name
              END
            , ced.DATE_IN_QUEUE
            , ced.SORT_ORDER
            , CASE WHEN cha.Account_Type = 'Utility' THEN 'Manual Recalc'
                  ELSE COALESCE(rt.Code_Value, con_rec.Code_Value, irtc.Code_Value)
              END
            , com.Commodity_Name
            , ced.Account_ID
            , com.Commodity_Id
            , cha.Account_Type
            , rfrt.Code_Dsc
            , CASE WHEN cha.Account_Type = 'Supplier' THEN 'Manual Recalc'
                  ELSE COALESCE(irtc.Code_Value, rtc.Code_Value, rt.Code_Value)
              END
            , ced.Recalc_Response_Error_Dsc
            , ce.OPENED_DATE;




        DELETE
        ced
        FROM
            #Cu_Exception_Dtl ced
        WHERE
            ced.Account_Type = 'Supplier'
            AND ced.EXCEPTION_TYPE = 'Failed Recalc'
            AND (   ced.contract_recalc_type LIKE 'No Recalc%'
                    OR  ced.contract_recalc_type IS NULL)
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Cu_Exception_Dtl ced2
                           WHERE
                                ced.CU_INVOICE_ID = ced2.CU_INVOICE_ID
                                AND ced.CBMS_IMAGE_ID = ced2.CBMS_IMAGE_ID
                                AND ced.Account_ID = ced2.Account_ID
                                AND ced.Commodity_Id = ced2.Commodity_Id
                                AND ced.QUEUE_ID = ced2.QUEUE_ID
                                AND ced2.EXCEPTION_TYPE = 'Failed Recalc'
                                AND ced2.contract_recalc_type NOT LIKE 'No Recalc%'
                                AND ced2.contract_recalc_type IS NOT NULL);



        DELETE
        ced
        FROM
            #Cu_Exception_Dtl ced
        WHERE
            ced.Account_Type = 'Utility'
            AND ced.EXCEPTION_TYPE = 'Failed Recalc'
            AND (   ced.Recalc_Type LIKE 'No Recalc%'
                    OR  ced.Recalc_Type IS NULL)
            AND EXISTS (   SELECT
                                1
                           FROM
                                #Cu_Exception_Dtl ced2
                           WHERE
                                ced.CU_INVOICE_ID = ced2.CU_INVOICE_ID
                                AND ced.CBMS_IMAGE_ID = ced2.CBMS_IMAGE_ID
                                AND ced.Account_ID = ced2.Account_ID
                                AND ced.Commodity_Id = ced2.Commodity_Id
                                AND ced.QUEUE_ID = ced2.QUEUE_ID
                                AND ced2.EXCEPTION_TYPE = 'Failed Recalc'
                                AND ced2.Recalc_Type NOT LIKE 'No Recalc%'
                                AND ced2.Recalc_Type IS NOT NULL);




        SELECT
            ced.CBMS_IMAGE_ID
            , ced.CBMS_DOC_ID
            , ced.CU_INVOICE_ID
            , ced.QUEUE_ID
            , ced.EXCEPTION_TYPE
            , ced.EXCEPTION_STATUS_TYPE
            , ced.Account_number
            , ced.service_month
            , ced.Client_Name
            , ced.site_name
            , ced.Utility_Vendor_Name
            , ced.DATE_IN_QUEUE
            , ced.SORT_ORDER
            , ced.contract_recalc_type
            , ced.Commodity_Name
            , ced.Account_ID
            , ced.Commodity_Id
            , ced.Account_Type
            , ced.Recalc_Failed_Reason_Type
            , ced.Recalc_Type
            , ced.Recalc_Response_Error_Dsc
            , ced.Supplier_Vendor_Name
            , ced.OPENED_DATE
        FROM
            #Cu_Exception_Dtl ced
        GROUP BY
            ced.CBMS_IMAGE_ID
            , ced.CBMS_DOC_ID
            , ced.CU_INVOICE_ID
            , ced.QUEUE_ID
            , ced.EXCEPTION_TYPE
            , ced.EXCEPTION_STATUS_TYPE
            , ced.Account_number
            , ced.service_month
            , ced.Client_Name
            , ced.site_name
            , ced.Utility_Vendor_Name
            , ced.DATE_IN_QUEUE
            , ced.SORT_ORDER
            , ced.contract_recalc_type
            , ced.Commodity_Name
            , ced.Account_ID
            , ced.Commodity_Id
            , ced.Account_Type
            , ced.Recalc_Failed_Reason_Type
            , ced.Recalc_Type
            , ced.Recalc_Response_Error_Dsc
            , ced.Supplier_Vendor_Name
            , ced.OPENED_DATE
        ORDER BY
            ced.DATE_IN_QUEUE ASC;

        DROP TABLE #Cu_Exception_Dtl;

    END;






GO



GRANT EXECUTE ON  [dbo].[Cu_Exception_Sel_By_Queue_Exception_Type] TO [CBMSApplication]
GO
