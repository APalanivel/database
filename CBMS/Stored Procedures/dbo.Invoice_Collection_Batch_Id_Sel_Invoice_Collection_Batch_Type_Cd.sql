SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                
Name:   dbo.[Invoice_Collection_Batch_Id_Sel_Invoice_Collection_Batch_Type_Cd]         
                
Description:                
   This sproc is used to fill the ICQ Batch Table.        
                             
 Input Parameters:                
    Name										DataType   Default   Description                  
--------------------------------------------------------------------------------------                  

 Output Parameters:                      
    Name        DataType   Default   Description                  
--------------------------------------------------------------------------------------                  
                
 Usage Examples:                    
--------------------------------------------------------------------------------------     
  EXEC dbo.[Invoice_Collection_Batch_Id_Sel_Invoice_Collection_Batch_Type_Cd] 102816
    
Author Initials:                
    Initials  Name                
--------------------------------------------------------------------------------------                  
 RKV    Ravi Kumar Vegesna  
 Modifications:                
    Initials        Date   Modification                
--------------------------------------------------------------------------------------                  
    RKV    2019-10-25  Created For Invoice_Collection.           
               
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Batch_Id_Sel_Invoice_Collection_Batch_Type_Cd]
     (
         @Invoice_Collection_Batch_Type_Cd INT
     )
AS
    BEGIN
        SET NOCOUNT ON;


        SELECT
            icbd.Invoice_Collection_Batch_Id
        FROM
            dbo.Invoice_Collection_Batch icbd
            INNER JOIN dbo.Code sc
                ON sc.Code_Id = icbd.Status_Cd
        WHERE
            icbd.Invoice_Collection_Batch_Type_Cd = @Invoice_Collection_Batch_Type_Cd
            AND sc.Code_Value = 'Pending';





    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Batch_Id_Sel_Invoice_Collection_Batch_Type_Cd] TO [CBMSApplication]
GO
