SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Contract_Exception_Route_To_User       
              
Description:              
			This sproc route the queue id updated in the Account_Exception table.      
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------            
    @Routed_To_User_Queue				INT					
    @Contract_Exception_Ids				INT	
    @User_Info_Id						INT				
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
   

    BEGIN TRAN

	Select * from Contract_Exception  where contract_exception_Id =32

    EXEC dbo.Contract_Exception_Route_To_User 
      @Routed_To_User_Queue = 49
     ,@Contract_Exception_Ids ='32'
     ,@User_Info_Id =49

	 	Select * from Contract_Exception  where contract_exception_Id =32


    ROLLBACK TRAN

   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2019-11-26		Created For Add contract.         
             
******/
CREATE PROCEDURE [dbo].[Contract_Exception_Route_To_User]
    (
        @Routed_To_User_Queue INT
        , @Contract_Exception_Ids VARCHAR(MAX)
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @Contract_Exception TABLE
              (
                  Contract_Exception_Id INT
              );


        INSERT  @Contract_Exception
             (
                 Contract_Exception_Id
             )
        SELECT
            us.Segments
        FROM
            dbo.ufn_split(@Contract_Exception_Ids, ',') AS us
        GROUP BY
            us.Segments;


        UPDATE
            ae
        SET
            ae.Queue_Id = @Routed_To_User_Queue
            , ae.Updated_User_Id = @User_Info_Id
            , ae.Last_Change_Ts = GETDATE()
        FROM
            dbo.Contract_Exception ae
            INNER JOIN @Contract_Exception aes
                ON ae.Contract_Exception_Id = aes.Contract_Exception_Id;

    END;




GO
GRANT EXECUTE ON  [dbo].[Contract_Exception_Route_To_User] TO [CBMSApplication]
GO
