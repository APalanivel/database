SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SUPPLIER_CONTACT_VENDOR_MAP_ID_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@rfpId         	int       	          	
	@contactInfoId 	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     PROCEDURE DBO.SR_RFP_GET_SUPPLIER_CONTACT_VENDOR_MAP_ID_P

@rfpId int,
@contactInfoId int

AS
set nocount on


DECLARE @supplierContactVendorMapId int
SELECT @supplierContactVendorMapId = (select 	SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID 

					from 	SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP
					
					where  	SR_RFP_ID = @rfpId
						AND SR_SUPPLIER_CONTACT_INFO_ID = @contactInfoId
					)

RETURN  @supplierContactVendorMapId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_CONTACT_VENDOR_MAP_ID_P] TO [CBMSApplication]
GO
