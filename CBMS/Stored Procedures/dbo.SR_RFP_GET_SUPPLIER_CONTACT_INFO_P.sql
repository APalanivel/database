
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_GET_SUPPLIER_CONTACT_INFO_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default			Description
---------------------------------------------------------------------------------------
	@rfpId         	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default			Description
---------------------------------------------------------------------------------------

USAGE EXAMPLES:
---------------------------------------------------------------------------------------

EXEC dbo.SR_RFP_GET_SUPPLIER_CONTACT_INFO_P 13524

EXEC dbo.SR_RFP_GET_SUPPLIER_CONTACT_INFO_P 13304

EXEC dbo.SR_RFP_GET_SUPPLIER_CONTACT_INFO_P 13163

EXEC dbo.SR_RFP_GET_SUPPLIER_CONTACT_INFO_P 12326


AUTHOR INITIALS:
	Initials	Name
---------------------------------------------------------------------------------------
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date			Modification
---------------------------------------------------------------------------------------
	        	9/21/2010		Modify Quoted Identifier
 DMR			09/10/2010		Modified for Quoted_Identifier
 NR				2016-07-12		GCS_Phase_5(B)_GCS-1167/68 Added Local_Code,User_Info_Id,USERNAME in output. 


******/

CREATE PROCEDURE [dbo].[SR_RFP_GET_SUPPLIER_CONTACT_INFO_P] ( @rfpId INT )
AS 
BEGIN
      SET NOCOUNT ON
     
      SELECT
            v.VENDOR_ID
           ,v.VENDOR_NAME
           ,ssci.SR_SUPPLIER_CONTACT_INFO_ID
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS USER_INFO_NAME
           ,c.Code_Id
           ,ui.locale_code
           ,ui.USER_INFO_ID
           ,ui.USERNAME
      FROM
            dbo.USER_INFO ui
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_INFO ssci
                  ON ui.USER_INFO_ID = ssci.USER_INFO_ID
            INNER JOIN dbo.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP srscvm
                  ON ssci.SR_SUPPLIER_CONTACT_INFO_ID = srscvm.SR_SUPPLIER_CONTACT_INFO_ID
            INNER JOIN dbo.VENDOR v
                  ON srscvm.VENDOR_ID = v.VENDOR_ID
            INNER JOIN dbo.Code c
                  ON c.Code_Value = ui.locale_code
            INNER JOIN dbo.Codeset cs
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            srscvm.SR_RFP_ID = @rfpId
            AND cs.Codeset_Name = 'LocalizationLanguage' 
END



;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_GET_SUPPLIER_CONTACT_INFO_P] TO [CBMSApplication]
GO
