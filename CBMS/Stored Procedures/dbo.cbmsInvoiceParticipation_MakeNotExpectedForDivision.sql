SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForDivision]
	( @MyAccountId int
	, @division_id int
	)
AS
BEGIN

   update invoice_participation
      set is_expected = 0
    where site_id in (
		   select distinct s.site_id
		     from division d 
		     join site s on s.division_id = d.division_id
		    where d.division_id = @division_id
	  )
      and is_received = 0

END




GO
GRANT EXECUTE ON  [dbo].[cbmsInvoiceParticipation_MakeNotExpectedForDivision] TO [CBMSApplication]
GO
