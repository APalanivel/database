SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:   dbo.Invoice_Collection_Queue_Status_Upd           
                  
Description:                  
   This sproc to update Invoice_Collection_Queue    
                               
 Input Parameters:                  
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
@tvp_Invoice_Collection_Queue   tvp_Invoice_Collection_Queue READONLY    
@User_Info_Id       INT    
@Status_Cd        INT       
                     
     
 Output Parameters:                        
    Name        DataType   Default   Description                    
----------------------------------------------------------------------------------------                    
                  
 Usage Examples:                      
----------------------------------------------------------------------------------------     
    
    
BEGIN TRAN      
DECLARE @tvp_Invoice_Collection_Queue tvp_Invoice_Collection_Queue            
              
INSERT      INTO @tvp_Invoice_Collection_Queue    
            ( Invoice_Collection_Queue_Id, Invoice_File_Name )    
VALUES    
            ( 1, 'name.xp' )                
    ,       ( 2, 'name2.xp' )      
        
EXEC dbo.Invoice_Collection_Queue_Status_Upd     
      @tvp_Invoice_Collection_Queue = @tvp_Invoice_Collection_Queue    
     ,@User_Info_Id = 49    
     ,@Status_Cd = 102345    
     
ROLLBACK    
     
     
   SELECT    
      *    
FROM    
      dbo.Code c    
      INNER JOIN dbo.Codeset cs    
            ON c.Codeset_Id = cs.Codeset_Id    
WHERE    
      cs.Codeset_Name = 'ICR Status'    
      AND c.Code_Value = 'Received'    
          
          
Author Initials:                  
    Initials  Name                  
----------------------------------------------------------------------------------------                    
 RKV    Ravi Kumar Vegesna    
 Modifications:                  
    Initials        Date   Modification                  
----------------------------------------------------------------------------------------                    
 RKV             2020-01-06      Created for Tool revamp.           
                 
******/  
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Status_Upd_By_Invoice_Collection_Queue_Id]  
     (  
         @Invoice_Collection_Queue_Id INT  
         , @Collection_Start_Dt DATE = NULL  
         , @Collection_End_Dt DATE = NULL  
         , @Status_Cd INT = NULL  
         , @User_Info_Id INT  
         , @Is_Invoice_Collection_Start_Dt_Changed BIT = NULL  
         , @Is_Invoice_Collection_End_Dt_Changed BIT = NULL
		 , @Invoice_Collection_Queue_Type_Cd INT = NULL  
		 , @Invoice_Collection_Exception_Type_Cd INT = NULL
     )  
AS  
    BEGIN  
        SET NOCOUNT ON;  
  
  
  
  
  
        UPDATE  
            icq  
        SET  
            Status_Cd = CASE WHEN @Status_Cd IS NOT NULL THEN @Status_Cd  
                            ELSE icq.Status_Cd  
                        END  
            , Collection_Start_Dt = CASE WHEN @Collection_Start_Dt IS NOT NULL THEN @Collection_Start_Dt  
                                        ELSE icq.Collection_Start_Dt  
                                    END  
            , Collection_End_Dt = CASE WHEN @Collection_End_Dt IS NOT NULL THEN @Collection_End_Dt  
                                      ELSE icq.Collection_End_Dt  
                                  END  
            , icq.Last_Change_Ts = GETDATE()  
            , icq.Updated_User_Id = @User_Info_Id  
            , Is_Invoice_Collection_Start_Dt_Changed = CASE WHEN @Is_Invoice_Collection_Start_Dt_Changed IS NOT NULL THEN @Is_Invoice_Collection_Start_Dt_Changed  
                                        ELSE icq.Is_Invoice_Collection_Start_Dt_Changed  
                                    END  
            , Is_Invoice_Collection_End_Dt_Changed = CASE WHEN @Is_Invoice_Collection_End_Dt_Changed IS NOT NULL THEN @Is_Invoice_Collection_End_Dt_Changed  
                                        ELSE icq.Is_Invoice_Collection_End_Dt_Changed  
                                    END  
			, Invoice_Collection_Queue_Type_Cd = CASE WHEN @Invoice_Collection_Queue_Type_Cd IS NOT NULL THEN @Invoice_Collection_Queue_Type_Cd  
                                      ELSE icq.Invoice_Collection_Queue_Type_Cd  
									  End
			, Invoice_Collection_Exception_Type_Cd = CASE WHEN @Invoice_Collection_Exception_Type_Cd IS NOT NULL THEN @Invoice_Collection_Exception_Type_Cd  
                                      ELSE icq.Invoice_Collection_Exception_Type_Cd  
                                  END  
   FROM  
            dbo.Invoice_Collection_Queue icq  
        WHERE  
            icq.Invoice_Collection_Queue_Id = @Invoice_Collection_Queue_Id;  
  
    END;  
  
  
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Status_Upd_By_Invoice_Collection_Queue_Id] TO [CBMSApplication]
GO
