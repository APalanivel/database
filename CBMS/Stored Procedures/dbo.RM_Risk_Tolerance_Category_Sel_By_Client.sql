SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/**

NAME: dbo.RM_Risk_Tolerance_Category_Sel_By_Client

    
DESCRIPTION:    

      To get both client-specific and global categories for documents search 


INPUT PARAMETERS:    
     NAME					DATATYPE		DEFAULT           DESCRIPTION    
------------------------------------------------------------------------    
	@Client_Id				INT		

                    

OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
-------------------------------------------------------------------------              


USAGE EXAMPLES:              
-------------------------------------------------------------------------              
	EXEC dbo.RM_Risk_Tolerance_Category_Sel_By_Client 616

	 

AUTHOR INITIALS:              
	INITIALS	NAME              
------------------------------------------------------------              
    RR			Raghu Reddy


MODIFICATIONS:    
	INITIALS	DATE		MODIFICATION              
------------------------------------------------------------              
	RR 			31-07-2018	Created - Global Risk Management

**/
CREATE  PROCEDURE [dbo].[RM_Risk_Tolerance_Category_Sel_By_Client]
     (
         @Client_Id INT
     )
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT
            rtc.RM_Risk_Tolerance_Category_Id
            , rtc.RM_Risk_Tolerance_Category
        FROM
            Trade.RM_Risk_Tolerance_Category rtc
        WHERE
            rtc.Client_Id = @Client_Id
            OR  rtc.Client_Id = -1;



    END;

GO
GRANT EXECUTE ON  [dbo].[RM_Risk_Tolerance_Category_Sel_By_Client] TO [CBMSApplication]
GO
