SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_LP_ADD_DETERMINANT_VALUE_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int,
	@month_identifier datetime,
	@lp_value numeric(32, 16)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
---- Test a new entry
--EXEC dbo.SR_RFP_LP_ADD_DETERMINANT_VALUE_P
--	@user_id = 1,
--	@session_id = -1,
--	@determinant_id = 62,
--	@month_identifier = '2009-04-22 15:57:29.020' ,
--	@lp_value = null


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_LP_ADD_DETERMINANT_VALUE_P
	@user_id varchar(10),
	@session_id varchar(20),
	@determinant_id int,
	@month_identifier datetime,
	@lp_value numeric(32, 16)
	AS
	
SET NOCOUNT ON	

	INSERT INTO SR_RFP_LP_DETERMINANT_VALUES(
												sr_rfp_load_profile_determinant_id,
												month_identifier, 
												lp_value, 
												reading_type_id
											)
									SELECT
												@determinant_id,
												@month_identifier,
												@lp_value,
												1160 --(select entity_id from entity(nolock) where entity_type = 1031 and entity_name = 'Actual LP Value')
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_ADD_DETERMINANT_VALUE_P] TO [CBMSApplication]
GO
