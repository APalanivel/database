
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******

NAME:  
  dbo.SR_RFP_DELETE_SOP_P
  
DESCRIPTION:
  
INPUT PARAMETERS:  
	Name				DataType		Default		Description  
------------------------------------------------------------------
	@accountBidGroupId	INT
	@isBidGroup			INT
	@rfp_Id				INT
	@cbms_image_id		INT				NULL
  
OUTPUT PARAMETERS:  
	Name				DataType		Default		Description  
------------------------------------------------------------------

USAGE EXAMPLES:  
------------------------------------------------------------------  
	BEGIN TRAN
		SELECT * FROM dbo.SR_RFP_SOP_SMR smr JOIN dbo.cbms_image	ci ON smr.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
			WHERE SR_ACCOUNT_GROUP_ID = 10011968 AND IS_BID_GROUP = 1
		SELECT * FROM dbo.cbms_image WHERE CBMS_IMAGE_ID = 77104095
		EXEC  dbo.SR_RFP_DELETE_SOP_P 10011968,1,13258,77104095 
		SELECT * FROM dbo.SR_RFP_SOP_SMR smr JOIN dbo.cbms_image	ci ON smr.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
			WHERE SR_ACCOUNT_GROUP_ID = 10011968 AND IS_BID_GROUP = 1
		SELECT * FROM dbo.cbms_image WHERE CBMS_IMAGE_ID = 77104095
	ROLLBACK TRAN
	
	BEGIN TRANSACTION
		SELECT count(1) FROM dbo.SR_RFP_SOP_SMR smr JOIN dbo.cbms_image	ci ON smr.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
			WHERE SR_ACCOUNT_GROUP_ID = 10011968 AND IS_BID_GROUP = 1
		EXEC dbo.SR_RFP_DELETE_SOP_P 10011968,1,13258,NULL
		SELECT count(1) FROM dbo.SR_RFP_SOP_SMR smr JOIN dbo.cbms_image	ci ON smr.CBMS_IMAGE_ID = ci.CBMS_IMAGE_ID
			WHERE SR_ACCOUNT_GROUP_ID = 10011968 AND IS_BID_GROUP = 1
	ROLLBACK TRANSACTION

AUTHOR INITIALS:  
	Initials	Name  
------------------------------------------------------------
	CPE			Chaitanya Panduga Eshwar
	AKR			Ashok Kumar Raju
	RR			Raghu Reddy
	
MODIFICATIONS  
	Initials	Date		 Modification  
------------------------------------------------------------  
	AKR			2013-01-21	Changes Summit to Schnider Electric as a part of Name Change Project
	AKR			2013-01-29	Updated Usage Example
	RR			2016-02-11	Global Sourcing - Phase3 - GCS-489 Removed document type filter input parameters @docType & @smrtype, 
							Added input optional parameter @cbms_image_id
							Added missing functiolity of deleting image details from dbo.cbms_image table, application is deleting 
							physical files from	image server
	
	
******/
CREATE PROCEDURE [dbo].[SR_RFP_DELETE_SOP_P]
      ( 
       @accountBidGroupId INT
      ,@isBidGroup INT
      ,@rfp_Id INT
      ,@cbms_image_id INT = NULL )
AS 
BEGIN

      SET NOCOUNT ON
      DECLARE @allsitetemp TABLE
            ( 
             Site_Index INT IDENTITY(1, 1)
            ,[site_id] [INT] )

      DECLARE
            @siteId INT
           ,@projectId INT
           ,@smruploadedaccountcount INT
           ,@SiteCnt INT
           ,@SiteIndex INT
           ,@account AS INT
           ,@tempSiteCnt INT

      INSERT      INTO @allsitetemp
                  ( 
                   site_id )
                  SELECT
                        Acct.site_id
                  FROM
                        dbo.sr_rfp_sop_smr smr
                        JOIN dbo.sr_rfp_account rfpacc
                              ON rfpacc.sr_rfp_account_id = smr.sr_account_group_id
                        JOIN dbo.account Acct
                              ON Acct.account_id = rfpacc.account_id
                  WHERE
                        smr.SR_ACCOUNT_GROUP_ID = @accountBidGroupId
                        AND smr.IS_BID_GROUP = @isBidGroup
                  UNION
                  SELECT
                        Acct.site_id
                  FROM
                        dbo.sr_rfp_sop_smr smr
                        JOIN dbo.sr_rfp_account rfpacc
                              ON rfpacc.sr_rfp_bid_group_id = smr.sr_account_group_id
                        JOIN dbo.account Acct
                              ON Acct.account_id = rfpacc.account_id
                  WHERE
                        smr.SR_ACCOUNT_GROUP_ID = @accountBidGroupId
                        AND smr.IS_BID_GROUP = @isBidGroup
      SET @SiteCnt = @@ROWCOUNT

      DELETE
            ci
      FROM
            dbo.cbms_image ci
            INNER JOIN dbo.SR_RFP_SOP_SMR smr
                  ON ci.CBMS_IMAGE_ID = smr.CBMS_IMAGE_ID
      WHERE
            smr.SR_ACCOUNT_GROUP_ID = @accountBidGroupId
            AND smr.IS_BID_GROUP = @isBidGroup
            AND ( @cbms_image_id IS NULL
                  OR smr.cbms_image_id = @cbms_image_id )
      
      DELETE
            dbo.SR_RFP_SOP_SMR
      WHERE
            SR_ACCOUNT_GROUP_ID = @accountBidGroupId
            AND IS_BID_GROUP = @isBidGroup
            AND ( @cbms_image_id IS NULL
                  OR cbms_image_id = @cbms_image_id )
      
      SET @SiteIndex = 1
		
      WHILE ( @SiteIndex < = @SiteCnt ) 
            BEGIN --//while
			
                  SELECT
                        @siteId = Site_ID
                  FROM
                        @allsitetemp
                  WHERE
                        Site_Index = @SiteIndex
			
                  SELECT
                        @projectId = SSO_PROJECT_ID
                  FROM
                        dbo.RFP_SITE_PROJECT
                  WHERE
                        RFP_ID = @rfp_id
                        AND SITE_ID = @siteId
                  IF @@ROWCOUNT > 0 
                        BEGIN --if
				--getting smruploaded accountcount for that site

                              SET @account = 0
  
                              SELECT
                                    @account = count(rfpAcct.sr_rfp_account_id)
                              FROM
                                    dbo.sr_rfp_sop_smr smr
                                    INNER JOIN dbo.sr_rfp_account rfpAcct
                                          ON rfpAcct.sr_rfp_account_id = smr.sr_account_group_id
                                    INNER JOIN dbo.account acct
                                          ON acct.account_id = rfpAcct.account_id
                                    INNER JOIN dbo.cbms_image img
                                          ON img.cbms_image_id = smr.cbms_image_id
                              WHERE
                                    rfpAcct.sr_rfp_id = @rfp_id
                                    AND rfpAcct.is_deleted = 0
                                    AND smr.is_bid_group = 0
                                    AND acct.site_id = @siteId
  
                              SELECT
                                    @account = @account + count(rfpAcct.sr_rfp_bid_group_id)
                              FROM
                                    dbo.sr_rfp_sop_smr smr
                                    INNER JOIN dbo.sr_rfp_account rfpAcct
                                          ON rfpAcct.sr_rfp_bid_group_id = smr.sr_account_group_id
                                    INNER JOIN dbo.account acct
                                          ON acct.account_id = rfpAcct.account_id
                                    INNER JOIN dbo.cbms_image img
                                          ON img.cbms_image_id = smr.cbms_image_id
                              WHERE
                                    rfpAcct.sr_rfp_id = @rfp_id
                                    AND rfpAcct.is_deleted = 0
                                    AND smr.is_bid_group = 1
                                    AND acct.site_id = @siteId

                              SET @smruploadedaccountcount = @account
  
                              IF ( @smruploadedaccountcount ) = 0 
                                    BEGIN--if1  
  
					--finding sendtosupplier is done for that site  
                                          SET @tempSiteCnt = 0
															
                                          SELECT
                                                @tempSiteCnt = count(acct.site_id)
                                          FROM
                                                dbo.sr_rfp_send_supplier supp
                                                INNER JOIN dbo.sr_rfp_account rfpacc
                                                      ON rfpacc.sr_rfp_account_id = supp.sr_account_group_id
                                                INNER JOIN dbo.account acct
                                                      ON acct.account_id = rfpacc.account_id
                                          WHERE
                                                supp.is_bid_group = 0
                                                AND supp.status_type_id = 1090 --for posted
                                                AND rfpacc.sr_rfp_id = @rfp_id
                                                AND rfpacc.is_deleted = 0
                                                AND acct.Site_id = @siteId	-- Added by hari to eliminate the use of @temp table

					
                                          SELECT
                                                @tempSiteCnt = @tempSiteCnt + count(acct.site_id)
                                          FROM
                                                dbo.sr_rfp_send_supplier supp
                                                JOIN dbo.sr_rfp_account rfpacc
                                                      ON rfpacc.sr_rfp_bid_group_id = supp.sr_account_group_id
                                                JOIN dbo.account acct
                                                      ON acct.account_id = rfpacc.account_id
                                          WHERE
                                                supp.is_bid_group = 1
                                                AND supp.status_type_id = 1090 --for posted
                                                AND rfpacc.sr_rfp_id = @rfp_id
                                                AND rfpacc.is_deleted = 0
                                                AND acct.Site_id = @siteId	-- Added by hari to eliminate the use of @temp table

					
					
                                          IF @tempSiteCnt = 0 
                                                BEGIN--if2
  
                                                      DELETE
                                                            dbo.sso_project_step
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND step_no = 3
                                                      DELETE
                                                            dbo.sso_project_activity
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND activity_description = 'Schneider Electric is actively engaged with the supplier community, soliciting proposals and shaping the offers to align with your strategic sourcing profile.  Schneider Electric is building the analysis package around the various offers and for mulating its recommendation for your review'

                                                END --IF2
                                          ELSE 
                                                BEGIN --if sent to rfp

                                                      UPDATE
                                                            dbo.SSO_PROJECT_STEP
                                                      SET   
                                                            IS_ACTIVE = 1
                                                           ,IS_COMPLETE = 0
                                                      WHERE
                                                            SSO_PROJECT_ID = @projectId
                                                            AND STEP_NO = 3
  
                                                END   --sent to rfp

					--finding clientapproval is done for that site  

                                          SET @tempSiteCnt = 0
                                          SELECT
                                                @tempSiteCnt = count(acct.site_id)
                                          FROM
                                                dbo.sr_rfp_sop_client_approval clientapp
                                                JOIN dbo.sr_rfp_account rfpacc
                                                      ON rfpacc.sr_rfp_account_id = clientapp.sr_account_group_id
                                                JOIN dbo.account acct
                                                      ON rfpacc.account_id = acct.account_id
                                          WHERE
                                                rfpacc.sr_rfp_id = @rfp_id
                                                AND rfpacc.is_deleted = 0
                                                AND clientapp.is_bid_group = 0
                                                AND acct.Site_id = @siteId	-- Added by hari to eliminate the use of @temp table

                                          SELECT
                                                @tempSiteCnt = @tempSiteCnt + count(acct.site_id)
                                          FROM
                                                dbo.sr_rfp_sop_client_approval clientapp
                                                JOIN dbo.sr_rfp_account rfpacc
                                                      ON rfpacc.sr_rfp_bid_group_id = clientapp.sr_account_group_id
                                                JOIN dbo.account acct
                                                      ON acct.account_id = rfpacc.account_id
                                          WHERE
                                                rfpacc.sr_rfp_id = @rfp_id
                                                AND rfpacc.is_deleted = 0
                                                AND clientapp.is_bid_group = 1
                                                AND acct.Site_id = @siteId	-- Added by hari to eliminate the use of @temp table
  
                                          IF @tempSiteCnt = 0 
                                                BEGIN--if3
  
                                                      DELETE
                                                            dbo.sso_project_step
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND step_no = 4
                                                      DELETE
                                                            dbo.sso_project_activity
                                                      WHERE
                                                            sso_project_id = @projectId
                                                            AND activity_description = 'Schneider Electric has completed the analysis and prepared a recommendation.  Timely review of the recommendation is essential to ensure implementation.'
  
                                                END --if3
                                    END --end if1
                              ELSE 
                                    BEGIN --else
  
                                          UPDATE
                                                dbo.SSO_PROJECT_STEP
                                          SET   
                                                IS_ACTIVE = 1
                                               ,IS_COMPLETE = 0
                                          WHERE
                                                SSO_PROJECT_ID = @projectId
                                                AND STEP_NO = 3

                                    END
                        END --if

                  SET @SiteIndex = @SiteIndex + 1
            END --//while
END;

;
GO
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_DELETE_SOP_P] TO [CBMSApplication]
GO
