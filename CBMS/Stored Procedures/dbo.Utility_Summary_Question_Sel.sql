SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Utility_Summary_Question_Sel

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Commodity_Id			INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
 EXEC Utility_Summary_Question_Sel 291
	
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SKA			Shobhit Kumar Agrawal
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	SKA			01/12/2011	Created
******/

CREATE PROC [dbo].[Utility_Summary_Question_Sel] ( @Commodity_Id INT )
AS 
    BEGIN
        SET NOCOUNT ON
	
        SELECT  uss.Section_Label
              , usq.Question_Label
              , qcm.Question_Commodity_Map_Id
              , uss.Utility_Summary_Section_Id
              , usq.Utility_Summary_Question_Id
              , usq.Display_Order
        FROM    dbo.Question_Commodity_Map qcm
                INNER JOIN dbo.Utility_Summary_Question usq
                    ON usq.Utility_Summary_Question_Id = qcm.Utility_Summary_Question_Id
                INNER JOIN dbo.Utility_Summary_Section uss
                    ON uss.Utility_Summary_Section_Id = usq.Utility_Summary_Section_Id
        WHERE   qcm.Commodity_Id = @Commodity_Id
                AND usq.Is_Active = 1
        ORDER BY uss.Utility_Summary_Section_Id
              , usq.Display_Order

    END
GO
GRANT EXECUTE ON  [dbo].[Utility_Summary_Question_Sel] TO [CBMSApplication]
GO
