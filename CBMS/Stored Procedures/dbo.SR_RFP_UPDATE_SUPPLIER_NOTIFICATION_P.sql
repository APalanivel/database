SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[SR_RFP_UPDATE_SUPPLIER_NOTIFICATION_P]
	@userId INT,
	@sessionId VARCHAR(100),    
	--@cbmsImageDocId VARCHAR(200),     
	--@cbmsImage image ,    
	--@contentType VARCHAR(200),    
	@cbmsImageId INT,  -- added by Jaya    
	@accountGroupId INT,    
	@isBidGroup BIT,   
	@returnToTarrifDate DATETIME,    
	@returnToTarrifTypeId INT,
	@isSwitchRate BIT,
	@switchSupplierDate DATETIME,
	@switchSupplierTypeId INT,
	@isSwitchSupplier BIT, 
	@rfpId INT
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @entityId INT
		, @DateNow DATETIME
    
	SELECT @entityId = ENTITY_ID FROM dbo.ENTITY (NOLOCK) WHERE Entity_Name = 'Supplier Notification' AND entity_type=100
    
	/*    
	 INSERT INTO CBMS_IMAGE (CBMS_IMAGE_TYPE_ID, CBMS_DOC_ID, CBMS_IMAGE, CONTENT_TYPE, DATE_IMAGED)     
	 VALUES (@entityId, @cbmsImageDocId, @cbmsImage, @contentType, getDate())    
	    
	declare @cbmsImageId INT    
	select @cbmsImageId = (select @@Identity)     
	*/    
	
	--added by Jaya for updating entityid    
     
	UPDATE dbo.CBMS_IMAGE SET CBMS_IMAGE_TYPE_ID = @entityId WHERE CBMS_IMAGE_ID = @cbmsImageId

	SET @DateNow = GETDATE()
	
	UPDATE dbo.SR_RFP_UTILITY_SWITCH
		SET RETURN_TO_TARIFF_DATE = @returnToTarrifDate,
			RETURN_TO_TARIFF_TYPE_ID = @returnToTarrifTypeId,
			IS_SWITCH_RATE_ESTIMATED = @isSwitchRate,
			UTILITY_SWITCH_SUPPLIER_DATE = @switchSupplierDate,
			UTILITY_SWITCH_SUPPLIER_TYPE_ID = @switchSupplierTypeId,
			IS_SWITCH_SUPPLIER_ESTIMATED = @isSwitchSupplier,
			SUPPLIER_NOTICE_IMAGE_ID = @cbmsImageId,
			UPLOADED_BY = @userId,
			UPLOADED_DATE = @DateNow
	WHERE SR_ACCOUNT_GROUP_ID = @accountGroupId

	IF (@isBidGroup = 0 )
	 BEGIN
	 
		 UPDATE dbo.SR_RFP_CHECKLIST
			SET IS_NOTICE_GIVEN = 1
		 WHERE SR_RFP_ACCOUNT_ID =  @accountGroupId
		 
	 END
	ELSE IF (@isBidGroup > 0 )
	 BEGIN
	 
		 UPDATE rfpCheckList
			SET rfpCheckList.IS_NOTICE_GIVEN = 1
		 FROM dbo.SR_RFP_CHECKLIST rfpCheckList
			INNER JOIN dbo.SR_RFP_ACCOUNT rfpAcct ON rfpAcct.SR_RFP_ACCOUNT_ID = rfpCheckList.SR_RFP_ACCOUNT_ID
		 WHERE rfpAcct.SR_RFP_BID_GROUP_ID = @accountGroupId

	 END
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_UPDATE_SUPPLIER_NOTIFICATION_P] TO [CBMSApplication]
GO
