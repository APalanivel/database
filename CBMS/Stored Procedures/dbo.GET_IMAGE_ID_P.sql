SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE dbo.GET_IMAGE_ID_P
	@rateScheduleId INT
AS
BEGIN

	SET NOCOUNT ON

	SELECT cbms_image_id 
	FROM dbo.rate_schedule 
	WHERE rate_schedule_id = @rateScheduleId 

END
GO
GRANT EXECUTE ON  [dbo].[GET_IMAGE_ID_P] TO [CBMSApplication]
GO
