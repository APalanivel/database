
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
/******                            
NAME:                            
 dbo.Single_Signon_User_Map_Sel                           
                            
DESCRIPTION:                            
 This procedure returns signle singnon user details  
                                       
INPUT PARAMETERS:                            
 Name              DataType          Default     Description                            
----------------------------------------------------------------                            
 @sso_provider_cd   INT               
 @client_id   INT            
 @SSO_UserName  VARCHAR(200)                             
                                 
OUTPUT PARAMETERS:                            
 Name              DataType          Default     Description                            
----------------------------------------------------------------                            
                            
USAGE EXAMPLES:                            
------------------------------------------------------------                            
 EXEC dbo.Single_Signon_User_Map_Sel 100506,12210,'aruns1'                  
                                     
AUTHOR INITIALS:                            
  Initials  Name   Date                               
------------------------------------------------------------                            
  V N   Varada Nair  20-Feb-2014          
  A S   Arun Skaria  26-2-2014                        
                                             
MODIFICATIONS:                            
 Initials  Date   Modification                            
------------------------------------------------------------                            
  AS   26-2-2014  Selecting FirstName,LastName, Email,SecurityRoleId      
  VN   14-03-2014  Renamed SP      
  AS   19-3-2014  modified the sp to get the user selected even if the securityRole failes    
  AS   20-3-2014  Selecting UserName  
  AS   25-07-2014 Selecting IsHistory Field
  TP   28-Jun-2017		MAINT-5464 - removed Signon_Provider_Cd column	
******/                            
                      
                            
CREATE PROCEDURE [dbo].[Single_Signon_User_Map_Sel]
      ( 
       @client_id INT
      ,@SSO_UserName VARCHAR(200) )
AS 
BEGIN                            
                            
      SET NOCOUNT ON                            
                
      SELECT
            ssum.Internal_User_ID AS User_Info_Id
           ,ui.FIRST_NAME
           ,ui.LAST_NAME
           ,ui.EMAIL_ADDRESS
           ,ui.USERNAME
           ,usr.Security_Role_Id
           ,ui.IS_HISTORY
      FROM
            dbo.Single_Signon_User_Map AS ssum
            INNER JOIN dbo.USER_INFO ui
                  ON ssum.Internal_User_ID = ui.USER_INFO_ID
            LEFT OUTER JOIN dbo.User_Security_Role usr
                  ON ui.USER_INFO_ID = usr.User_Info_Id
      WHERE
            ssum.Client_ID = @client_id
            AND ssum.Provider_XUser_Name = @SSO_UserName   
                         
END;



;
GO



GRANT EXECUTE ON  [dbo].[Single_Signon_User_Map_Sel] TO [CBMSApplication]
GO
