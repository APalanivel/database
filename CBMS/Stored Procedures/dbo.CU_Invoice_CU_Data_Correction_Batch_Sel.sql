SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******            
NAME:   dbo.CU_Invoice_CU_Data_Correction_Batch_Sel
           
DESCRIPTION:             
			To select pending batch for processing
			
INPUT PARAMETERS:            
	Name						DataType	Default		Description  
---------------------------------------------------------------------------------  
    


OUTPUT PARAMETERS:
	Name								DataType		Default		Description  
---------------------------------------------------------------------------------  

 USAGE EXAMPLES:
---------------------------------------------------------------------------------  
		
			EXEC dbo.CU_Invoice_CU_Data_Correction_Batch_Sel 
		
 AUTHOR INITIALS:            
	Initials	Name            
-------------------------------------------------------------            
	RR			Raghu Reddy

 MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	RR			2017-03-13	Contract placeholder - CP-77 Created
******/

CREATE PROCEDURE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Sel]
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @In_Progress INT
           ,@Pending INT
           
      
      SELECT
            @In_Progress = MAX(CASE WHEN cd.Code_Value = 'In Progress' THEN cd.Code_Id
                               END)
           ,@Pending = MAX(CASE WHEN cd.Code_Value = 'Pending' THEN cd.Code_Id
                           END)
      FROM
            dbo.Code cd
            INNER JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            Codeset_Name = 'Request Status'
      
      DECLARE @CU_Invoice_CU_Data_Correction_Batch_Id_tbl TABLE
            ( 
             CU_Invoice_CU_Data_Correction_Batch_Id INT
            ,ID INT IDENTITY(1, 1) )
            
      INSERT      INTO @CU_Invoice_CU_Data_Correction_Batch_Id_tbl
                  ( 
                   CU_Invoice_CU_Data_Correction_Batch_Id )
                  SELECT
                        ccbdtl.CU_Invoice_CU_Data_Correction_Batch_Id
                  FROM
                        dbo.CU_Invoice_CU_Data_Correction_Batch_Dtl ccbdtl
                        INNER JOIN dbo.CU_Invoice_CU_Data_Correction_Batch ccb
                              ON ccbdtl.CU_Invoice_CU_Data_Correction_Batch_Id = ccb.CU_Invoice_CU_Data_Correction_Batch_Id
                  WHERE
                        ccb.Status_Cd = @In_Progress
                        AND DATEDIFF(minute, ccbdtl.Last_Change_Ts, GETDATE()) > 30
      
      INSERT      INTO @CU_Invoice_CU_Data_Correction_Batch_Id_tbl
                  ( 
                   CU_Invoice_CU_Data_Correction_Batch_Id )
                  SELECT
                        ccb.CU_Invoice_CU_Data_Correction_Batch_Id
                  FROM
                        dbo.CU_Invoice_CU_Data_Correction_Batch ccb
                  WHERE
                        ccb.Status_Cd = @Pending
            
      SELECT
            CU_Invoice_CU_Data_Correction_Batch_Id
      FROM
            @CU_Invoice_CU_Data_Correction_Batch_Id_tbl
      ORDER BY
            ID
      
      
      
END;

;
GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_CU_Data_Correction_Batch_Sel] TO [CBMSApplication]
GO
