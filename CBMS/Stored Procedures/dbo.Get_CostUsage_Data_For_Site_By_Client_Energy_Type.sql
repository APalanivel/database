SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************************************************************************************************      
NAME :  dbo.Get_CostUsage_Data_For_Site_By_Client_Energy_Type 

DESCRIPTION:   
Stored Procedure is used to get the client usage data.

INPUT PARAMETERS:      
Name             DataType               Default        Description      
--------------------------------------------------------------------   
@ClientID			INT
@EnergyType			VARCHAR(50)
@DataCaptureLevel	VARCHAR(20)
@BucketCaptureLevel VARCHAR(20)
@CostUsageKeyFields	Get_CostUsage_Key	READONLY		TVP

OUTPUT PARAMETERS:      
Name   DataType  Default Description      
--------------------------------------------------------------------      


USAGE EXAMPLES:      
--------------------------------------------------------------------      
DECLARE @CostUsageKeyFields Get_CostUsage_Key
DECLARE @BucketMasterIds tvp_Bucket
      
INSERT      INTO @BucketMasterIds
            ( Bucket_Master_Id )
VALUES
            ( 100990 ),
            ( 100991 )
          
INSERT      @CostUsageKeyFields
VALUES
            ( 1, '2012-05-01 00:00:00.000', 341739, NULL ),
            ( 2, '2012-01-01 00:00:00.000', 340013, NULL )

EXEC dbo.Get_CostUsage_Data_For_Site_By_Client_Energy_Type 
      @EnergyType = 'Electric Power'
     ,@BucketMasterIds = @BucketMasterIds
     ,@CostUsageKeyFields = @CostUsageKeyFields
     
AUTHOR INITIALS:      
Initials	  Name      
-------------------------------------------------------------------      
RK          Raghu Kalvapudi     

MODIFICATIONS       
Initials	  Date	    Modification      
--------------------------------------------------------------------
RK		  09/05/2013  Created
************************/
CREATE PROCEDURE [dbo].[Get_CostUsage_Data_For_Site_By_Client_Energy_Type]
      (
       @EnergyType VARCHAR(50)
      ,@BucketMasterIds tvp_Bucket READONLY
      ,@CostUsageKeyFields Get_CostUsage_Key READONLY )
AS
BEGIN

      DECLARE @Commodity_Id INT
	
      SELECT
            @Commodity_Id = c.Commodity_Id
      FROM
            dbo.Commodity c
      WHERE
            c.Commodity_Name = @EnergyType 


      DECLARE @Sites TABLE
            (
             Site_Id INT
            ,Site_Name VARCHAR(200)
            ,Client_Hier_Id INT
            ,Row_No INT
            ,Service_Month DATE )


      DECLARE @Bucket_Masters TABLE
            (
             Bucket_Master_Id INT
            ,Bucket_Name VARCHAR(200)
            ,Bucket_Type VARCHAR(50)
            ,Sort_Order INT )

     
    
      INSERT      INTO @Bucket_Masters
                  (Bucket_Master_Id
                  ,Bucket_Name
                  ,Bucket_Type
                  ,Sort_Order )
                  SELECT
                        bm.Bucket_Master_Id
                       ,bm.Bucket_Name
                       ,c.Code_Value
                       ,bm.Sort_Order
                  FROM
                        @BucketMasterIds bmi
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = bmi.Bucket_Master_Id
                        INNER JOIN dbo.Code c
                              ON c.Code_Id = bm.Bucket_Type_Cd 

     
      INSERT      INTO @Sites
                  (Site_Id
                  ,Site_Name
                  ,Client_Hier_Id
                  ,Row_No
                  ,Service_Month )
                  SELECT
                        ch.SITE_ID
                       ,ch.SITE_NAME
                       ,ch.Client_Hier_Id
                       ,cukf.[ROW_NUMBER]
                       ,cukf.Service_Month
                  FROM
                        @CostUsageKeyFields cukf
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Hier_Id = cukf.Client_Hier_Id

                 
                 
      SELECT
            s.Row_No
           ,s.Site_Id
           ,s.Site_name
           ,convert(VARCHAR(20), cusd.Service_Month, 101) Service_Month
           ,cusd.CURRENCY_UNIT_ID
           ,cusd.UOM_Type_Id
           ,bm.Bucket_Name
           ,bm.Bucket_Master_Id
           ,bm.Bucket_Type
           ,bm.Sort_Order
           ,cusd.Bucket_Value
      INTO
            #Sites_Data
      FROM
            @Sites s
            INNER JOIN dbo.Cost_Usage_Site_Dtl cusd
                  ON s.Client_Hier_Id = cusd.Client_Hier_Id
                     AND cusd.Service_Month = s.Service_Month
            INNER JOIN @Bucket_Masters bm
                  ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
      SELECT
            x.Row_No
           ,x.Site_Id
           ,x.Site_name
           ,convert(VARCHAR(20), x.Service_Month, 101) Service_Month
           ,sd.CURRENCY_UNIT_ID
           ,cu.CURRENCY_UNIT_NAME
           ,sd.UOM_Type_Id
           ,e.ENTITY_NAME AS UOM_Type
           ,bmtmp.Bucket_Name
           ,bmtmp.Bucket_Master_Id
           ,bmtmp.Bucket_Type
           ,bmtmp.Sort_Order
           ,sd.Bucket_Value
      FROM
            @Sites x
            CROSS JOIN @Bucket_Masters bmtmp
            LEFT JOIN #Sites_Data sd
                  ON sd.Bucket_Master_Id = bmtmp.Bucket_Master_Id
                     AND x.Service_Month = sd.Service_Month
                     AND x.Site_Id = sd.Site_Id
            LEFT JOIN dbo.CURRENCY_UNIT cu
                  ON cu.CURRENCY_UNIT_ID = sd.CURRENCY_UNIT_ID
            LEFT JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = sd.UOM_Type_Id
      ORDER BY
            Bucket_Type
           ,Bucket_Name      

END


;
GO
GRANT EXECUTE ON  [dbo].[Get_CostUsage_Data_For_Site_By_Client_Energy_Type] TO [CBMSApplication]
GO
