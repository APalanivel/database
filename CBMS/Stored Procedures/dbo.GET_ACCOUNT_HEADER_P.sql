
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 [dbo].[GET_ACCOUNT_HEADER_P]  
    
DESCRIPTION:    
    
    
INPUT PARAMETERS:    
 Name			DataType	Default		Description    
------------------------------------------------------------    
 @account_id    int                       
    

OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
------------------------------------------------------------    
    

USAGE EXAMPLES:    
------------------------------------------------------------    

exec dbo.GET_ACCOUNT_HEADER_P 522234

exec dbo.GET_ACCOUNT_HEADER_P
      1149409

exec dbo.GET_ACCOUNT_HEADER_P 1149394

AUTHOR INITIALS:    
Initials	Name
------------------------------------------------------------    
SP			Sandeep Pigilam
    
MODIFICATIONS    

Initials	Date		Modification    
------------------------------------------------------------    
SP			2016-11-24	Invoice tracking,Added comments,Added NOT_MANAGED in select list.
HG			2017-03-23	IT-409 Modified to return the account number from CHA ( to show the contract date appended with the account no) as we can have standing data exception created for Supplier acccunt as well
******/ 

CREATE PROCEDURE [dbo].[GET_ACCOUNT_HEADER_P] @account_id INT
AS
BEGIN

      SET NOCOUNT ON

      SELECT
            a.Account_Id
           ,a.Display_Account_Number AS Account_Number
           ,a.Account_Vendor_Id AS VENDOR_ID
           ,a.Account_Vendor_Name AS vendor_name
           ,e.ENTITY_NAME
           ,accGroup.GROUP_BILLING_NUMBER
           ,a.Account_Not_Managed AS NOT_MANAGED
      FROM
            Core.Client_Hier_Account a
            LEFT JOIN ACCOUNT_GROUP accGroup
                  ON accGroup.ACCOUNT_GROUP_ID = a.Account_Group_ID
            LEFT JOIN ENTITY e
                  ON e.ENTITY_ID = a.Account_Invoice_Source_Cd
      WHERE
            a.Account_Id = @account_id
      GROUP BY
            a.Account_Id
           ,a.Display_Account_Number
           ,a.Account_Vendor_Id
           ,a.Account_Vendor_Name
           ,e.ENTITY_NAME
           ,accGroup.GROUP_BILLING_NUMBER
           ,a.Account_Not_Managed

END;

;
GO

GRANT EXECUTE ON  [dbo].[GET_ACCOUNT_HEADER_P] TO [CBMSApplication]
GO
