SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	dbo.Division_Group_Sel_By_SecurityRole_Keyword  

DESCRIPTION:  Procedure used to fetch the division and sitegroups assosciated to the user  

INPUT PARAMETERS:  
	Name				DataType		Default		Description  
------------------------------------------------------------  
	@Security_Role_Id	INT
    @User_Info_Id		INT
    @State_Id			INT				NULL	Not used by the sproc
    @Keyword			VARCHAR(200)	NULL           

OUTPUT PARAMETERS:
	Name				DataType		Default		Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

    EXEC dbo.Division_Group_Sel_By_Client_Keyword
      @Client_Id = 235

	   EXEC dbo.Division_Group_Sel_By_Client_Keyword
      @Client_Id = 235
	  ,@Keyword ='Headquart'




AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	NR			Narayana Reddy
	

MODIFICATIONS
	Initials	Date		Modification 
------------------------------------------------------------
	NR			2018-07-25  Created For Risk Managemnet.

******/
CREATE PROCEDURE [dbo].[Division_Group_Sel_By_Client_Keyword]
    (
        @Client_Id INT
        , @Keyword VARCHAR(200) = NULL
    )
WITH RECOMPILE
AS
    BEGIN

        SET NOCOUNT ON;

        CREATE TABLE #Client_Groups
             (
                 Sitegroup_Id INT PRIMARY KEY CLUSTERED
                 , Sitegroup_Name VARCHAR(200)
                 , Client_Hier_Id INT
                 , Sitegroup_Type_Cd INT
                 , Sitegroup_Type_Name VARCHAR(25)
                 , Division_Not_Managed BIT
             );

        DECLARE @User_Groups TABLE
              (
                  Sitegroup_Id INT PRIMARY KEY CLUSTERED
                  , Sitegroup_Name VARCHAR(200)
                  , Client_Hier_Id INT
                  , Sitegroup_Type_Cd INT
                  , Sitegroup_Type_Name VARCHAR(25)
                  , Division_Not_Managed BIT
              );

        DECLARE
            @Keyword_FT_Search VARCHAR(200)
            , @SQL_Client_Groups VARCHAR(MAX);

        SET @Keyword = NULLIF(@Keyword, '');
        SET @Keyword_FT_Search = '"*' + dbo.udf_StripNonAlphaNumerics(@Keyword) + '*"';



        SET @SQL_Client_Groups = '
      INSERT      INTO #Client_Groups
                  (Sitegroup_Id
                  ,Sitegroup_Name
                  ,Client_Hier_Id
                  ,Sitegroup_Type_Cd
                  ,Sitegroup_Type_Name
                  ,Division_Not_Managed )
                  SELECT
                        ch.Sitegroup_Id
                       ,ch.Sitegroup_Name
                       ,ch.Client_Hier_Id
                       ,ch.Sitegroup_Type_Cd
                       ,ch.Sitegroup_Type_Name
                       ,ch.Division_Not_Managed
                  FROM
                        core.Client_Hier ch  
                  WHERE	ch.Sitegroup_Type_Name in(''Division'',''Global'') AND 
				
                        ch.Client_Id = ' + CAST(@Client_Id AS VARCHAR(20))
                                 + '
                        AND ch.Site_Id = 0
                        AND ch.Sitegroup_Id > 0
                       ';

					  
        SET @SQL_Client_Groups = @SQL_Client_Groups
                                 + CASE WHEN @Keyword IS NOT NULL THEN
                                            ' AND (CONTAINS (( ch.Sitegroup_Name, ch.Sitegroup_Name_FTSearch),' + ''''
                                            + @Keyword_FT_Search + '''' + '))'
                                       ELSE ''
                                   END;

        EXEC (@SQL_Client_Groups);






        SELECT
            sg.Sitegroup_Id
            , sg.Sitegroup_Name
            --, sg.Client_Hier_Id
            --, sg.Sitegroup_Type_Cd
            , sg.Sitegroup_Type_Name
            , sg.Division_Not_Managed
            --, sg.Sitegroup_Type_Name#Sitegroup_Id
            , sg.Site_Cnt
        FROM    (   SELECT  (CASE WHEN chgroup.Sitegroup_Type_Name = 'Division' THEN ISNULL(chgroup.Sitegroup_Id, -1)
                                 WHEN chgroup.Sitegroup_Type_Name = 'Global' THEN ISNULL(chgroup.Sitegroup_Id, 0)
                                 WHEN chgroup.Sitegroup_Type_Name = 'User' THEN ISNULL(chgroup.Sitegroup_Id, 1)
                             END) AS Sitegroup_Id
                            , CASE WHEN chgroup.Sitegroup_Name IS NULL THEN
                                       '........' + CASE WHEN chgroup.Sitegroup_Type_Name = 'Division' THEN 'All Divisions'
                                                        WHEN chgroup.Sitegroup_Type_Name = 'Global' THEN 'Global Groups'
                                                        WHEN chgroup.Sitegroup_Type_Name = 'User' THEN 'User Groups'
                                                    END + '........'
                                  ELSE chgroup.Sitegroup_Name
                              END AS Sitegroup_Name
                            , chgroup.Client_Hier_Id
                            , (CASE WHEN chgroup.Sitegroup_Type_Name = 'Division' THEN 0
                                   ELSE 1
                               END) AS Is_Group
                            , chgroup.Sitegroup_Type_Cd
                            , chgroup.Sitegroup_Type_Name
                            , chgroup.Division_Not_Managed
                            --, (CASE WHEN chgroup.Sitegroup_Type_Name = 'Division' THEN
                            --            ISNULL(
                            --                chgroup.Sitegroup_Type_Name + '#' + CAST(chgroup.Sitegroup_Id AS VARCHAR(20))
                            --                , -1)
                            --       WHEN chgroup.Sitegroup_Type_Name = 'Global' THEN
                            --           ISNULL(
                            --               chgroup.Sitegroup_Type_Name + '#' + CAST(chgroup.Sitegroup_Id AS VARCHAR(20)), 0)
                            --       WHEN chgroup.Sitegroup_Type_Name = 'User' THEN
                            --           ISNULL(
                            --               chgroup.Sitegroup_Type_Name + '#' + CAST(chgroup.Sitegroup_Id AS VARCHAR(20)), 1)
                            --   END) AS Sitegroup_Type_Name#Sitegroup_Id
                            , COUNT(ss.Site_id) AS Site_Cnt
                    FROM
                        #Client_Groups chgroup
                        INNER JOIN dbo.Sitegroup_Site ss
                            ON ss.Sitegroup_id = chgroup.Sitegroup_Id
                    GROUP BY
                        GROUPING SETS(chgroup.Sitegroup_Type_Name, (chgroup.Sitegroup_Id, chgroup.Sitegroup_Name, chgroup.Sitegroup_Type_Name, chgroup.Sitegroup_Type_Cd, chgroup.Client_Hier_Id, chgroup.Division_Not_Managed, chgroup.Client_Hier_Id))) sg
        WHERE
            @Keyword IS NULL
            OR  sg.Sitegroup_Id <> -1
        ORDER BY
            sg.Sitegroup_Type_Name
            , sg.Sitegroup_Type_Cd
            , sg.Sitegroup_Name;

        DROP TABLE #Client_Groups;

    END;
    ;


GO
GRANT EXECUTE ON  [dbo].[Division_Group_Sel_By_Client_Keyword] TO [CBMSApplication]
GO
