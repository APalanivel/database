SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/*********       
NAME:  dbo.CU_Invoice_Service_month_DEL 
     
DESCRIPTION:  
    
INPUT PARAMETERS:        
	Name              DataType         Default     Description        
------------------------------------------------------------   
	@Cu_Invoice_Id	INT
	@Account_Id		INT					NULL
        
        
OUTPUT PARAMETERS:        
Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   
     
	EXEC CU_Invoice_Service_month_DEL 2927370,54363
	EXEC CU_Invoice_Service_month_DEL 2928293
    
------------------------------------------------------------      
AUTHOR INITIALS:      
Initials	Name      
------------------------------------------------------------      
SSR			Sharad Srivastava
    
    
Initials	Date		Modification      
------------------------------------------------------------      
SSR			03/25/2010  Created    
******/     

 CREATE PROCEDURE dbo.CU_Invoice_Service_month_DEL
	@Cu_Invoice_Id AS INT
   ,@Account_Id AS INT = NULL
AS 
BEGIN

    SET  NOCOUNT ON;
    
    DELETE 
	FROM 
		dbo.CU_INVOICE_SERVICE_MONTH
    WHERE       
		(CU_INVOICE_ID = @Cu_Invoice_Id)
		AND (@Account_Id IS NULL or Account_Id = @Account_Id)	
END



GO
GRANT EXECUTE ON  [dbo].[CU_Invoice_Service_month_DEL] TO [CBMSApplication]
GO
