SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:    Trade.Deal_Ticket_Sel_By_Account
              
Description:              
			
                           
 Input Parameters:              
    Name					DataType		Default			Description                
----------------------------------------------------------------------------------------                
	 @Account_Id						INT
                        
 
 Output Parameters:                    
    Name					DataType		Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec Trade.Deal_Ticket_Sel_By_Account 36126
   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RR				Raghu Reddy
 
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RR				2019-05-28		Created GRM
******/ 
CREATE PROCEDURE [Trade].[Deal_Ticket_Sel_By_Account]
      ( 
       @Account_Id INT
      ,@StartIndex INT = 1
      ,@EndIndex INT = 2147483647 )
AS 
BEGIN

      SET NOCOUNT ON;
      

      WITH  CTE_Deal_Ticket
              AS ( SELECT
                        Deal_Ticket_Id
                       ,ROW_NUMBER() OVER ( ORDER BY Deal_Ticket_Id ) AS Row_Num
                       ,COUNT(1) OVER ( ) Total_Rows
                   FROM
                        Trade.Deal_Ticket_Client_Hier_Volume_Dtl
                   WHERE
                        Account_Id = @Account_Id
                   GROUP BY
                        Deal_Ticket_Id
                   HAVING
                        SUM(Total_Volume) > 0)
            SELECT
                  Deal_Ticket_Id
                 ,Total_Rows
            FROM
                  CTE_Deal_Ticket
            WHERE
                  Row_Num BETWEEN @StartIndex AND @EndIndex
            ORDER BY
                  Row_Num; 
			     
      
END;

GO
GRANT EXECUTE ON  [Trade].[Deal_Ticket_Sel_By_Account] TO [CBMSApplication]
GO
