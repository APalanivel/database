
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          

NAME: [DBO].[User_Sel_By_Site_Id]  
     
DESCRIPTION: 
	To Get User names for given Site Id.
      
INPUT PARAMETERS:          
	NAME				DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------          
	@Site_Id			INT	
	@Is_History			BIT		  		
               
OUTPUT PARAMETERS:          
	NAME				DATATYPE			DEFAULT			DESCRIPTION          
------------------------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------------------------          

	EXEC dbo.User_Sel_By_Site_Id 27
	EXEC dbo.User_Sel_By_Site_Id 22011
	EXEC dbo.User_Sel_By_Site_Id 33597

    SELECT TOP 10
      *
    FROM
      core.Client_Hier ch
    WHERE
      EXISTS ( SELECT
                  1
               FROM
                  dbo.Security_Role_Client_Hier srch
                  JOIN dbo.Security_Role sr
                        ON sr.Security_Role_id = srch.Security_Role_Id
                        JOIN dbo.User_Security_Role usr
							ON usr.Security_role_Id = sr.Security_role_id
               WHERE
                  Sr.Is_Corporate = 0 
                  AND ch.Client_Hier_Id = srch.Client_Hier_Id
                  )
                  AND ch.Site_Id> 0

AUTHOR INITIALS:          
	INITIALS	NAME          
------------------------------------------------------------------------------          
	PNR			PANDARINATH
	HG			Harihara Suthan G
	NR			Narayana Reddy
        
MODIFICATIONS:
	INITIALS	DATE			MODIFICATION
------------------------------------------------------------------------------          
	PNR			25-JUN-10		CREATED
	HG			08/03/2010		Added @Is_History parameter to filter the records based on the requirement
									@Is_History = 0 - will fetch active users(users who are not moved to histor)
									@Is_History = 1 - Will fetch inactive users
									@Is_History = NULL will fetch all the users associated with the site.
	NR			2015-11-05		MAINT-3681 Removed the User_Site_Map table and replaced with Security role,client_hier table 
									This script returns only the site level users those who are assigned with the given site

******/
CREATE PROCEDURE [dbo].[User_Sel_By_Site_Id]
      (
       @Site_Id INT
      ,@Is_History BIT = NULL )
AS
BEGIN

      SET NOCOUNT ON;
      
      DECLARE @Division_Level_Users_Roles TABLE ( Security_Role_Id INT );
      
      DECLARE @Corporate_Role_Id INT;

      SELECT
            @Corporate_Role_Id = sr.Security_Role_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.Security_Role sr
                  ON sr.Client_Id = ch.Client_Id
      WHERE
            sr.Is_Corporate = 1
            AND ch.Site_Id = @Site_Id;

      INSERT      INTO @Division_Level_Users_Roles
                  ( Security_Role_Id )
                  SELECT
                        srch.Security_Role_Id
                  FROM
                        Core.Client_Hier ch
                        INNER JOIN Core.Client_Hier sgch
                              ON sgch.Sitegroup_Id = ch.Sitegroup_Id
                                 AND sgch.Site_Id = 0
                        INNER JOIN dbo.Security_Role_Client_Hier srch
                              ON srch.Client_Hier_Id = sgch.Client_Hier_Id
                  WHERE
                        ch.Site_Id = @Site_Id
                  GROUP BY
                        srch.Security_Role_Id;

      SELECT
            ui.FIRST_NAME + SPACE(1) + ui.LAST_NAME AS UserName
      FROM
            dbo.Security_Role_Client_Hier srch
            INNER JOIN Core.Client_Hier ch
                  ON srch.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.User_Security_Role usr
                  ON usr.Security_Role_Id = srch.Security_Role_Id
            INNER JOIN dbo.USER_INFO ui
                  ON ui.USER_INFO_ID = usr.User_Info_Id
      WHERE
            ( ch.Site_Id = @Site_Id )
            AND ( @Is_History IS NULL
                  OR ui.IS_HISTORY = @Is_History )
            AND ui.ACCESS_LEVEL = 1
            AND usr.Security_Role_Id <> @Corporate_Role_Id -- Excludes the corporate users of the client
            AND NOT EXISTS ( SELECT
                              1
                             FROM
                              @Division_Level_Users_Roles dlu
                             WHERE
                              dlu.Security_Role_Id = usr.Security_Role_Id )
      GROUP BY
            ui.FIRST_NAME
           ,ui.LAST_NAME
      ORDER BY
            ui.FIRST_NAME
           ,ui.LAST_NAME;

END;

;
GO

GRANT EXECUTE ON  [dbo].[User_Sel_By_Site_Id] TO [CBMSApplication]
GO
