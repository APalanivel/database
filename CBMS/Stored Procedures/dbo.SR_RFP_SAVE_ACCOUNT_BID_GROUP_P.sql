SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SAVE_ACCOUNT_BID_GROUP_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@rfpAccountId int,
@rfpId int,
@sr_rfp_bid_group_id int
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
--exec dbo.SR_RFP_SAVE_ACCOUNT_BID_GROUP_P 6555, 272, 232

-- TEST 2
---- Update the entry
--EXEC dbo.SR_RFP_SAVE_ACCOUNT_BID_GROUP_P
--@rfpAccountId =19270,
--@rfpId =1624,
--@sr_rfp_bid_group_id = 1


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE dbo.SR_RFP_SAVE_ACCOUNT_BID_GROUP_P

@rfpAccountId int,
@rfpId int,
@sr_rfp_bid_group_id int

	AS
	
set nocount on
	

	UPDATE SR_RFP_ACCOUNT SET SR_RFP_BID_GROUP_ID = @sr_rfp_bid_group_id
	WHERE SR_RFP_ACCOUNT_ID = @rfpAccountId
	AND SR_RFP_ID = @rfpId
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SAVE_ACCOUNT_BID_GROUP_P] TO [CBMSApplication]
GO
