SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   PROCEDURE dbo.GET_SITE_FUELS_OPTIONS_P
	@siteID int
	AS
	begin
		set nocount on

		select  IS_ALTERNATE_POWER,
			IS_ALTERNATE_GAS 
		from    site
		where   site_id = @siteID
	end
GO
GRANT EXECUTE ON  [dbo].[GET_SITE_FUELS_OPTIONS_P] TO [CBMSApplication]
GO
