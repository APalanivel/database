
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.DO_NOT_TRACK_EDIT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@REASON_TYPE_ID INT,
	@ACCOUNT_NUMBER VARCHAR(200)
	@CLIENT_NAME	VARCHAR(50)
	@CLIENT_CITY	VARCHAR(200)
	@STATE_ID		INT
	@VENDOR_NAME	VARCHAR(200)
	@UBM_ID			INT
	@UBM_ACCOUNT_CODE  VARCHAR(200)
	@DO_NOT_TRACK_ID INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

BEGIN TRAN

    EXEC dbo.DO_NOT_TRACK_EDIT_P
      @REASON_TYPE_ID = 314
     ,@ACCOUNT_NUMBER = '18608 58000 02 (FKA 18608 58000 01)'
     ,@CLIENT_NAME = 'AZZ incorporated AZZ incorporated AZZ incorporated AZZ incorporated'
     ,@CLIENT_CITY = 'Westborough'
     ,@STATE_ID = 20
     ,@VENDOR_NAME = 'National Grid of MA'
     ,@UBM_ID = NULL
     ,@UBM_ACCOUNT_CODE = NULL
     ,@DO_NOT_TRACK_ID = 68090

ROLLBACK TRAN

SELECT TOP 10 * FROM DO_NOT_TRACK WHERE Account_ID IS NOT NULL 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2013-12-09	Comments header added
							MAINT-2419, @Client_Name param width changed to 200 as Client_Name column width on DO_NOT_TRACK increased to VARCHAR(200) to match with Client.Client_Name
******/

CREATE PROCEDURE dbo.DO_NOT_TRACK_EDIT_P
      ( @REASON_TYPE_ID INT
      ,@ACCOUNT_NUMBER VARCHAR(200)
      ,@CLIENT_NAME VARCHAR(200)
      ,@CLIENT_CITY VARCHAR(200)
      ,@STATE_ID INT
      ,@VENDOR_NAME VARCHAR(200)
      ,@UBM_ID INT
      ,@UBM_ACCOUNT_CODE VARCHAR(200)
      ,@DO_NOT_TRACK_ID INT )
AS
BEGIN

      SET NOCOUNT ON

      UPDATE
            dbo.DO_NOT_TRACK
      SET
            REASON_TYPE_ID = @REASON_TYPE_ID
           ,ACCOUNT_NUMBER = @ACCOUNT_NUMBER
           ,CLIENT_NAME = @CLIENT_NAME
           ,CLIENT_CITY = @CLIENT_CITY
           ,STATE_ID = @STATE_ID
           ,VENDOR_NAME = @VENDOR_NAME
           ,UBM_ID = @UBM_ID
           ,UBM_ACCOUNT_CODE = @UBM_ACCOUNT_CODE
      WHERE
            DO_NOT_TRACK_ID = @DO_NOT_TRACK_ID

END

;
GO

GRANT EXECUTE ON  [dbo].[DO_NOT_TRACK_EDIT_P] TO [CBMSApplication]
GO
