SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- EXEC SR_SAD_DELETE_RC_CONTRACT_DOCUMENT_P 35569, 1658
CREATE PROCEDURE dbo.SR_SAD_DELETE_RC_CONTRACT_DOCUMENT_P
	@accountId INT,
	@rfpId INT
AS
BEGIN  
  
 SET NOCOUNT ON  
   
 DECLARE @entityId INT  
  , @rcContractDocId INT  
  , @rcDocumentId INT  
  , @imageId INT  
  , @cnt INT
  
 SET @rcDocumentId = 0  
 
 SELECT @entityId = ENTITY_ID FROM dbo.ENTITY (NOLOCK) WHERE Entity_name = 'Contract Review ' AND entity_type = 100  
  
 SELECT @rcContractDocId = rcDoc.sr_rc_contract_document_id  
 FROM dbo.cbms_image cbmsImage  
  INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.contract_image_id = cbmsImage.cbms_image_id  
  INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id  
 WHERE accMap.account_id = @accountId  
  AND accMap.sr_rfp_id = @rfpId  
  AND cbmsImage.cbms_image_type_id = @entityId  
  
 SELECT @rcDocumentId = ISNULL(rcDoc.sr_rc_contract_document_id , 0)  
 FROM dbo.cbms_image cbmsImage   
  INNER JOIN dbo.sr_rc_contract_document rcDoc ON rcDoc.rc_image_id = cbmsImage.cbms_image_id  
  INNER JOIN dbo.SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap ON accMap.sr_rc_contract_document_id = rcDoc.sr_rc_contract_document_id  
 WHERE accMap.account_id = @accountId  
  AND accMap.sr_rfp_id = @rfpId  
  
 SELECT @imageId = Contract_Image_Id FROM dbo.Sr_Rc_Contract_Document WHERE Sr_Rc_Contract_Document_Id = @rcContractDocId  
  
 SELECT @cnt = COUNT(1) FROM dbo.Sr_Rc_Contract_Document_Accounts_Map WHERE Sr_Rc_Contract_Document_Id = @rcContractDocId  
    
 IF (@cnt > 1 or @rcDocumentId > 0)  
  BEGIN  
  
  UPDATE dbo.sr_rc_contract_document  
   SET contract_image_id = NULL  
  WHERE sr_rc_contract_document_id = @rcContractDocId  
  
  END  
 ELSE IF (@cnt = 1)    
  BEGIN  
    
  IF (@rcDocumentId = 0)  
   BEGIN  
  
   DELETE FROM dbo.Sr_Rc_Contract_Document_Accounts_Map WHERE Account_id = @accountId AND Sr_Rfp_Id = @rfpId  
  
   DELETE FROM dbo.sr_rc_contract_document WHERE contract_image_id = @imageId  
  
   --DELETE FROM dbo.cbms_image WHERE cbms_image_id = @imageId  
  
   END  
  ELSE IF (@rcDocumentId > 0 )  
   BEGIN  
  
   UPDATE dbo.Sr_Rc_Contract_Document  
    SET contract_image_id = NULL  
   WHERE sr_rc_contract_document_id = @rcContractDocId  
  
   END  
  END  
  
 UPDATE rfpCheckList  
  SET rfpCheckList.Is_Contract_Reviewed = NULL  
 FROM dbo.Sr_Rfp_CheckList rfpCheckList  
  INNER JOIN dbo.Sr_Rfp_Account rfpAcct ON rfpAcct.Sr_Rfp_Account_Id = rfpCheckList.Sr_Rfp_Account_Id  
 WHERE rfpAcct.account_id = @accountId  
  AND rfpAcct.sr_rfp_id = @rfpId  
  
END  
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_DELETE_RC_CONTRACT_DOCUMENT_P] TO [CBMSApplication]
GO
