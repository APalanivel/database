SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Report_DE_Client_Make_Managed_Dtl 

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default	Description
---------------------------------------------------------------
	@Client_Name                                varchar(200)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------
   execute dbo.Report_DE_Client_Make_Managed_Dtl 'Novartis'


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	AB			Aditya Bharadwaj
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	AB       	2018-10-15	Created 

******/

CREATE PROCEDURE [dbo].[Report_DE_Client_Make_Managed_Dtl]
    (@Client_Name VARCHAR(200))
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  DISTINCT
                ch.Client_Name             [Client],
                ch.Client_Id               [Client ID],
                [Client Not Managed]       = CASE
                                                 WHEN ch.Client_Not_Managed = 1
                                                     THEN
                                                     'Not Managed'
                                                 ELSE
                                                     'Managed Client'
                                             END,
                ch.Sitegroup_Name          [Division],
                ch.Sitegroup_Id            [SGID],
                [Division Not Managed]     = CASE
                                                 WHEN ch.Division_Not_Managed = 1
                                                     THEN
                                                     'Not Managed'
                                                 ELSE
                                                     'Managed Division'
                                             END,
                ch.Site_Id                 [Site ID],
                ch.Site_name               [Site Name],
                [Site Not Managed]         = CASE
                                                 WHEN ch.Site_Not_Managed = 1
                                                     THEN
                                                     'Not Managed'
                                                 ELSE
                                                     'Managed Site'
                                             END,
                cha.Display_Account_Number [Account],
                cha.Account_Type           [Account Type],
                [Account Not Managed]      = CASE
                                                 WHEN cha.Account_Id IS NULL
                                                     THEN
                                                     'No account found'
                                                 WHEN cha.Account_Not_Managed = 1
                                                     THEN
                                                     'Not Managed'
                                                 ELSE
                                                     'Managed Account'
                                             END
        FROM
                Core.Client_Hier         ch
            LEFT JOIN
                Core.Client_Hier_Account cha
                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
            JOIN
                dbo.CLIENT               c
                    ON c.CLIENT_ID = ch.Client_Id
        WHERE
                ch.Client_Name = @Client_Name;

    END;


GO
