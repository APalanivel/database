SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
        
/******         
                       
 NAME: [dbo].[Client_App_Access_Role_Upd]                        
                          
 DESCRIPTION:                          
    To update the  Client App Access Role table.                          
                          
 INPUT PARAMETERS:        
       
 Name                               DataType            Default        Description        
 -------------------------------------------------------------------------------------------------------------      
 @Client_App_Access_Role_Id         INT                 IDENTITY      
 @App_Access_Role_Name              NVARCHAR(255)       NOT NULL      
 @App_Access_Role_Dsc               NVARCHAR(1000)      NULL      
 @Client_Id                         INT                 NOT NULL      
 @Updated_User_Id                   INT                 NULL      
                          
                          
 OUTPUT PARAMETERS:         
                              
 Name                               DataType            Default        Description        
 -------------------------------------------------------------------------------------------------------------      
                          
 USAGE EXAMPLES:                              
 -------------------------------------------------------------------------------------------------------------      
	 BEGIN TRAN        
	 EXEC dbo.Client_App_Access_Role_Upd      
	       
			@Client_App_Access_Role_Id=2      
		   ,@App_Access_Role_Name='TestUpdate'      
		   ,@App_Access_Role_Dsc=null      
		   ,@Client_Id=110      
		   ,@Updated_User_Id=null      
	                       
	 select * from dbo.Client_App_Access_Role where  Client_App_Access_Role_Id=2      
	 ROLLBACK TRAN        
                         
 AUTHOR INITIALS:       
        
 Initials               Name        
--------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                            
                           
 MODIFICATIONS:      
                           
 Initials               Date            Modification      
--------------------------------------------------------------------------------------------------------------      
 NR                     2013-11-25      Created for RA Admin user management                        
                         
******/                          
                        
CREATE PROCEDURE [dbo].[Client_App_Access_Role_Upd]
      ( 
       @Client_App_Access_Role_Id INT
      ,@App_Access_Role_Name NVARCHAR(255)
      ,@App_Access_Role_Dsc NVARCHAR(1000) = NULL
      ,@Client_Id INT
      ,@Updated_User_Id INT = NULL )
AS 
BEGIN       
                 
      SET NOCOUNT ON;                        
                      
      UPDATE
            dbo.Client_App_Access_Role
      SET   
            App_Access_Role_Name = @App_Access_Role_Name
           ,App_Access_Role_Dsc = @App_Access_Role_Dsc
           ,Client_Id = @Client_Id
           ,Updated_User_Id = @Updated_User_Id
           ,Last_Change_Ts = GETDATE()
      WHERE
            Client_App_Access_Role_Id = @Client_App_Access_Role_Id     
            
                                 
                       
END 

;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Upd] TO [CBMSApplication]
GO
