SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: DBO.Rate_Del  

DESCRIPTION: 
	
	It Deletes Rate for Selected Rate Id.
      
INPUT PARAMETERS:          
	NAME			DATATYPE	DEFAULT		DESCRIPTION         
--------------------------------------------------------------------
	@Rate_Id		INT

OUTPUT PARAMETERS:
	NAME			DATATYPE	DEFAULT		DESCRIPTION

------------------------------------------------------------
USAGE EXAMPLES:
------------------------------------------------------------

  Begin Tran
	EXEC dbo.Rate_Del 1
  Rollback Tran

AUTHOR INITIALS:          
	INITIALS	NAME
------------------------------------------------------------
	PNR			PANDARINATH

MODIFICATIONS:
	INITIALS	DATE		MODIFICATION
------------------------------------------------------------
	PNR			26-AUG-10	CREATED

*/

CREATE PROCEDURE dbo.Rate_Del
    (
      @Rate_Id	 INT
    )
AS
BEGIN

    SET NOCOUNT ON;

	DELETE	
	FROM
		dbo.Rate
	WHERE
		RATE_ID = @Rate_Id
END
GO
GRANT EXECUTE ON  [dbo].[Rate_Del] TO [CBMSApplication]
GO
