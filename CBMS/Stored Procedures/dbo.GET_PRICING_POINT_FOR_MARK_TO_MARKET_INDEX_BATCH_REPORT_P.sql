SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE dbo.GET_PRICING_POINT_FOR_MARK_TO_MARKET_INDEX_BATCH_REPORT_P 
@userId varchar,
@sessionId varchar,
@fromDate Varchar(12),
@toDate Varchar(12),
@clientId int,
@siteId int

as
	set nocount on
 
select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Financial'  HEDGE_TYPE			
	
from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex


where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'fixed price'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'financial'
				) AND	

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			

	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
	dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
						       from    ENTITY
						       where   ENTITY_TYPE=286 AND
						               ENTITY_NAME IN('order executed')
						    ) AND
	dealticket.CLIENT_ID=@clientId  AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID	AND

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
					) AND

	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) 

UNION 


select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Financial'  HEDGE_TYPE			


from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex


where	dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
  				     from   ENTITY
			  	     where  ENTITY_TYPE=274 AND
			  	 	    ENTITY_NAME IN('weighted strip price','individual month pricing')
			) AND	

	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'trigger'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'financial'
				) AND	

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			
	
	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND

	dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
					       from    ENTITY
					       where   ENTITY_TYPE=287 AND
						       ENTITY_NAME IN('fixed','closed')
					    ) AND

	dealticket.CLIENT_ID=@clientId AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND

	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
					) AND
	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) 

UNION 


select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Financial'  HEDGE_TYPE			

	
from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex


where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'options'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'financial'
				) AND		

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			
	
	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
	dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
						       from    ENTITY
						       where   ENTITY_TYPE=286 AND
						               ENTITY_NAME IN('order executed')
						    ) AND
	dealticket.CLIENT_ID=@clientId AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'financial'	
							      ) 
					) AND
	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) 


UNION


select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Physical'  HEDGE_TYPE			
	
from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex


where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'fixed price'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'Physical'
				) AND	

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			

	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
	dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
						       from    ENTITY
						       where   ENTITY_TYPE=286 AND
						               ENTITY_NAME IN('order executed')
						    ) AND
	dealticket.CLIENT_ID=@clientId  AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND	

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'Physical'	
							      ) 
					) AND
	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) 

UNION


select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Physical'  HEDGE_TYPE			

from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex

where	dealticket.PRICING_REQUEST_TYPE_ID IN( select ENTITY_ID
  				     from   ENTITY
			  	     where  ENTITY_TYPE=274 AND
			  	 	    ENTITY_NAME IN('weighted strip price','individual month pricing')
			) AND	

	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'trigger'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'Physical'
				) AND	

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			
	
	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND

	dealdetails.TRIGGER_STATUS_TYPE_ID IN( select  ENTITY_ID
					       from    ENTITY
					       where   ENTITY_TYPE=287 AND
						       ENTITY_NAME IN('fixed','closed')
					    ) AND

	dealticket.CLIENT_ID=@clientId  AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID	AND

	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'Physical'	
							      ) 
					) AND
	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101) 


UNION


select 	distinct priceIndex.PRICE_INDEX_ID,
	priceIndex.PRICING_POINT,
	'Physical'  HEDGE_TYPE			

	
from	RM_DEAL_TICKET  dealticket,
	RM_DEAL_TICKET_TRANSACTION_STATUS dealstatus,
	RM_DEAL_TICKET_DETAILS dealdetails,
	RM_DEAL_TICKET_VOLUME_DETAILS dealvolume,
	PRICE_INDEX priceIndex


where	dealticket.DEAL_TYPE_ID=( select ENTITY_ID
				  from	 ENTITY
				  where  ENTITY_TYPE=268 AND
				  	 ENTITY_NAME like 'options'
				) AND	

	dealTicket.HEDGE_TYPE_ID=( select ENTITY_ID
				   from	  ENTITY
				   where  ENTITY_TYPE=273 AND
					  ENTITY_NAME like 'Physical'
				) AND		

	dealTicket.HEDGE_MODE_TYPE_ID IN(   select ENTITY_ID
					  from	 ENTITY
					  where  ENTITY_TYPE=263 AND
					  	 ENTITY_NAME IN('index')
				) AND			
	
	dealTicket.RM_DEAL_TICKET_ID IN ( select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where CONVERT(Varchar(12),@fromDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH										

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where CONVERT(Varchar(12), @toDate, 101) BETWEEN  HEDGE_START_MONTH  AND HEDGE_END_MONTH

					  UNION 	

					  select RM_DEAL_TICKET_ID 
 					  from	RM_DEAL_TICKET
					  where HEDGE_START_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					  UNION										

					  select RM_DEAL_TICKET_ID 					
					  from	RM_DEAL_TICKET					
					  where HEDGE_END_MONTH BETWEEN  CONVERT(Varchar(12),@fromDate, 101)  AND CONVERT(Varchar(12),@toDate, 101)

					) AND

	dealTicket.RM_DEAL_TICKET_ID=dealstatus.RM_DEAL_TICKET_ID AND	
	dealstatus.DEAL_TRANSACTION_STATUS_TYPE_ID IN( select  ENTITY_ID
						       from    ENTITY
						       where   ENTITY_TYPE=286 AND
						               ENTITY_NAME IN('order executed')
						    ) AND
	dealticket.CLIENT_ID=@clientId  AND
	dealticket.PRICE_INDEX_ID=priceIndex.PRICE_INDEX_ID AND

	dealTicket.RM_DEAL_TICKET_ID=dealdetails.RM_DEAL_TICKET_ID AND
	dealdetails.RM_DEAL_TICKET_DETAILS_ID=dealvolume.RM_DEAL_TICKET_DETAILS_ID AND
	dealvolume.SITE_ID IN( select DISTINCT hedge.SITE_ID

				  from	RM_ONBOARD_HEDGE_SETUP hedge

		  		  where	hedge.SITE_ID = @siteId AND
					hedge.INCLUDE_IN_REPORTS=1 AND	
					hedge.HEDGE_TYPE_ID IN(	Select ENTITY_ID
								from   ENTITY
								where  ENTITY_TYPE=273 AND
								       ENTITY_NAME like 'Physical'	
							      ) 
					) AND
	dealdetails.MONTH_IDENTIFIER BETWEEN CONVERT(Varchar(12), @fromDate, 101) AND CONVERT(Varchar(12), @toDate, 101)
GO
GRANT EXECUTE ON  [dbo].[GET_PRICING_POINT_FOR_MARK_TO_MARKET_INDEX_BATCH_REPORT_P] TO [CBMSApplication]
GO
