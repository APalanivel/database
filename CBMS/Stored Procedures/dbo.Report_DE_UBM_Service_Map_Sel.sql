SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*********       

NAME:
     dbo.Report_DE_UBM_Service_Map_Sel
       
DESCRIPTION:          
			
      
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
--------------------------------------------------------------------------  
          
 USAGE EXAMPLES:                            
--------------------------------------------------------------------------  

 EXEC dbo.Report_DE_UBM_Service_Map_Sel
      
 AUTHOR INITIALS:        
       
 Initials              Name        
--------------------------------------------------------------------------  
 AKR                   Ashok Kumar Raju        

  
 MODIFICATIONS:      
          
 Initials              Date             Modification      
--------------------------------------------------------------------------  
 AKR                   2014-10-21      Created.
*********/
CREATE PROCEDURE [dbo].[Report_DE_UBM_Service_Map_Sel]
AS 
BEGIN
      SET NOCOUNT ON 
      
---Service Mappings

      SELECT
            UBM_NAME
           ,UBM_SERVICE_CODE
           ,Commodity_Name AS COMMODITY_NAME
      FROM
            UBM_SERVICE_MAP usm
            JOIN Commodity co
                  ON co.Commodity_Id = usm.COMMODITY_TYPE_ID
            JOIN UBM u
                  ON u.UBM_ID = usm.UBM_ID
      ORDER BY
            UBM_NAME
      
END

;
GO
GRANT EXECUTE ON  [dbo].[Report_DE_UBM_Service_Map_Sel] TO [CBMS_SSRS_Reports]
GO
