SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   etl.Site_Commodity_Image_GetAll       

    
DESCRIPTION: 
	Gets information about images from the Site_Commodity_Image table 
	This is used by teh IP ETL to populate the dv2.Client_Hier_Commodity_Image        
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			09/30/2010	Created
*****/ 
CREATE PROCEDURE etl.Site_Commodity_Image_GetAll
AS 
BEGIN
	SET NOCOUNT ON; 
	
	SELECT 
		ch.Client_Hier_Id
		,0 as Account_id
		,i.Commodity_Id
		,i.Service_Month
		,i.CBMS_IMAGE_ID
	FROM 
		cbms.dbo.Site_Commodity_Image  i 
		INNER JOIN Core.Client_Hier ch 
			ON i.SITE_ID = ch.Site_Id
END

GO
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_GetAll] TO [CBMSApplication]
GRANT EXECUTE ON  [ETL].[Site_Commodity_Image_GetAll] TO [ETL_Execute]
GO
