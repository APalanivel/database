SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 /******        
                           
 NAME:  [dbo].[Client_App_Access_Role_Sel]                        
                            
 DESCRIPTION:        
				TO get the roles information for clint app acess role table .                            
                            
 INPUT PARAMETERS:        
                           
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Client_App_Access_Role_Id             INT          
 @Client_Id                             INT
                         
 OUTPUT PARAMETERS:        
                                 
 Name                                   DataType        Default       Description        
---------------------------------------------------------------------------------------------------------------      
                            
 USAGE EXAMPLES:                                
---------------------------------------------------------------------------------------------------------------     
       
 Exec  dbo.Client_App_Access_Role_Sel 11 ,235         
                           
 AUTHOR INITIALS:      
         
 Initials               Name        
---------------------------------------------------------------------------------------------------------------      
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
---------------------------------------------------------------------------------------------------------------      
 NR                     2014-01-01      Created for RA Admin user management                          
                           
******/      
  
 CREATE PROCEDURE [dbo].[Client_App_Access_Role_Sel]
      ( 
       @Client_App_Access_Role_Id INT
      ,@Client_Id INT )
 AS 
 BEGIN   
      SET NOCOUNT ON   
  
      SELECT
            caar.App_Access_Role_Name
           ,caar.App_Access_Role_Dsc
           ,caar.Client_App_Access_Role_Id
           ,caar.Created_Ts
           ,ui.FIRST_NAME + ' ' + ui.LAST_NAME AS UserName
           ,Users.Users_Count
           ,Groups.Groups_Count
      FROM
            dbo.Client_App_Access_Role caar
            INNER JOIN dbo.USER_INFO ui
                  ON caar.Created_User_Id = ui.USER_INFO_ID
            LEFT JOIN ( SELECT
                              uicaar.Client_App_Access_Role_Id
                             ,COUNT(uii.User_Info_Id) AS Users_Count
                        FROM
                              dbo.User_Info_Client_App_Access_Role_Map uicaar
                              INNER JOIN dbo.USER_INFO uii
                                    ON uicaar.USER_INFO_ID = uii.USER_INFO_ID
                                       AND uii.IS_HISTORY = 0
                                       AND uii.ACCESS_LEVEL = 1
                                       AND uii.CLIENT_ID = @Client_Id
                        GROUP BY
                              uicaar.Client_App_Access_Role_Id ) AS Users
                  ON caar.Client_App_Access_Role_Id = Users.Client_App_Access_Role_Id
            LEFT JOIN ( SELECT
                              caargi.Client_App_Access_Role_Id
                             ,COUNT(caargi.GROUP_INFO_ID) AS Groups_Count
                        FROM
                              dbo.Client_App_Access_Role_Group_Info_Map caargi
                        GROUP BY
                              caargi.Client_App_Access_Role_Id ) AS Groups
                  ON caar.Client_App_Access_Role_Id = Groups.Client_App_Access_Role_Id
      WHERE
            caar.Client_App_Access_Role_Id = @Client_App_Access_Role_Id
            AND caar.Client_Id = @Client_Id
   
             
 END 






;
GO
GRANT EXECUTE ON  [dbo].[Client_App_Access_Role_Sel] TO [CBMSApplication]
GO
