SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    PROCEDURE dbo.GET_CLIENT_DIVISIONS_P
	@client_id int
	AS
	begin
		set nocount on

		select 	division_id, 
	      	 	division_name, 
	       		is_corporate_division, 
			contracting_entity, 
			client_legal_structure, 
			tax_number, 
			duns_number
		from 	division 
		where	client_id = @client_id
		order by is_corporate_division desc, division_name

	end
GO
GRANT EXECUTE ON  [dbo].[GET_CLIENT_DIVISIONS_P] TO [CBMSApplication]
GO
