SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE dbo.DELTOOL_RMDEALTICKET_GETFORCONTRACT_P
	( @MyAccountId int
	, @contract_id int
	)
AS
BEGIN

	   select rm_deal_ticket_id
	     from rm_deal_ticket
	    where contract_id = @contract_id

END
GO
GRANT EXECUTE ON  [dbo].[DELTOOL_RMDEALTICKET_GETFORCONTRACT_P] TO [CBMSApplication]
GO
