SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_LP_BULK_INSERT_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@fileName      	varchar(100)	          	
	@server        	varchar(100)	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec dbo.SR_RFP_LP_BULK_INSERT_P 'LP_4a163d3120d34070ac55c9d5576a554f.txt','nakula'

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/20/2010	Modify Quoted Identifier
	SSR			09/27/2010	Double quotes used to annotate text replaced by Single quote to set quoted identifier on	        		        	
******/
CREATE PROCEDURE [dbo].[SR_RFP_LP_BULK_INSERT_P]
    @fileName VARCHAR(100)
  , @server VARCHAR(100)
AS 
BEGIN

    SET nocount ON
    DECLARE @bulkInsert VARCHAR(8000)
    SET @bulkInsert = ' BULK INSERT SR_RFP_LP_DETERMINANT_VALUES '
        + ' FROM ''\\' + @server + '\TEMP\' + @fileName + ''''
        + ' WITH (MAXERRORS=0, FIRE_TRIGGERS, CHECK_CONSTRAINTS, FORMATFILE =''C:\sourcing_fmt\lp_determinant_values.fmt'') '

    EXEC ( @bulkInsert )
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_BULK_INSERT_P] TO [CBMSApplication]
GO
