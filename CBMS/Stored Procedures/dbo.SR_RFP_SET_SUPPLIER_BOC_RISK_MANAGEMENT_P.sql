SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	dbo.SR_RFP_SET_SUPPLIER_BOC_RISK_MANAGEMENT_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                             DataType          Default     Description    
---------------------------------------------------------------------------------    
@riskId int,
@swapIndexNymexTypeId VARCHAR(10),
@swapIndexNymexComments VARCHAR(4000),
@swapContractFixedTypeId VARCHAR(10),
@swapContractFixedComments VARCHAR(4000),
@expectedVariance VARCHAR(10),
@fixedPriceTriggerTypeId VARCHAR(10),
@fixedPriceTriggerComments VARCHAR(4000),
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    
@riskIdentityId int outPUT


USAGE EXAMPLES:
------------------------------------------------------------
--EXEC  SR_RFP_GET_SUPPLIER_BOC_RISK_MANAGEMENT_P 1,1,931

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
		DR		Deana Ritter

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	  DR     08/04/2009	   Removed Linked Server Updates

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE PROCEDURE [dbo].[SR_RFP_SET_SUPPLIER_BOC_RISK_MANAGEMENT_P] 

@riskId int,
@swapIndexNymexTypeId VARCHAR(10),
@swapIndexNymexComments VARCHAR(4000),
@swapContractFixedTypeId VARCHAR(10),
@swapContractFixedComments VARCHAR(4000),
@expectedVariance VARCHAR(10),
@fixedPriceTriggerTypeId VARCHAR(10),
@fixedPriceTriggerComments VARCHAR(4000),
@riskIdentityId int outPUT

AS
	
SET NOCOUNT ON
	
if @riskId=0
begin

		INSERT INTO SR_RFP_RISK_MANAGEMENT
		(
			SWAP_INDEX_NYMEX_TYPE_ID,
			SWAP_INDEX_NYMEX_COMMENTS,
			SWAP_CONTRACT_FIXED_TYPE_ID,
			SWAP_CONTRACT_FIXED_COMMENTS,
			EXPECTED_VARIANCE,
			FIXED_PRICE_TRIGGER_TYPE_ID,
			FIXED_PRICE_TRIGGER_COMMENTS
		)
		VALUES
		(
			@swapIndexNymexTypeId,
			@swapIndexNymexComments,
			@swapContractFixedTypeId,
			@swapContractFixedComments,
			@expectedVariance,
			@fixedPriceTriggerTypeId,
			@fixedPriceTriggerComments
		)
		
SELECT @riskIdentityId= SCOPE_IDENTITY()

end

else if @riskId>0
begin

		UPDATE SR_RFP_RISK_MANAGEMENT SET
		
		SWAP_INDEX_NYMEX_TYPE_ID=@swapIndexNymexTypeId,
		SWAP_INDEX_NYMEX_COMMENTS=@swapIndexNymexComments,
		SWAP_CONTRACT_FIXED_TYPE_ID=@swapContractFixedTypeId,
		SWAP_CONTRACT_FIXED_COMMENTS=@swapContractFixedComments,
		EXPECTED_VARIANCE=@expectedVariance,
		FIXED_PRICE_TRIGGER_TYPE_ID=@fixedPriceTriggerTypeId,
		FIXED_PRICE_TRIGGER_COMMENTS=@fixedPriceTriggerComments
		
		WHERE	SR_RFP_RISK_MANAGEMENT_ID=@riskId
		
      SELECT @riskIdentityId= @riskId

end	

			
RETURN
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_SET_SUPPLIER_BOC_RISK_MANAGEMENT_P] TO [CBMSApplication]
GO
