SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.GET_VENDOR_SEARCH_LIST_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@entityName    	varchar(200)	          	
	@entityType    	int       	          	
	@stateIdString 	varchar(400)	null      	
	@vendorName    	varchar(200)	null      	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
exec GET_VENDOR_SEARCH_LIST_P 'Supplier',155,null,null  

AUTHOR INITIALS:
	Initials	Name
	
	NK			Nageswara Rao Kosuri
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	 NK			10/26/2009	Modified to incorporate commodity       	
******/

--exec GET_VENDOR_SEARCH_LIST_P 'Supplier',155,'10,11,78,89,98',null
--exec GET_VENDOR_SEARCH_LIST_P 'Supplier',155,null,null
CREATE    PROCEDURE dbo.GET_VENDOR_SEARCH_LIST_P

	 @entityName varchar(200)
       , @entityType int
       , @stateIdString varchar(400) = null
       , @vendorName varchar(200) = null
AS
BEGIN
	set nocount on
	select @vendorName = '%' + @vendorName + '%'

	if @stateIdString is not null
	begin

		declare @t1 table(state_id int)
	
		while charindex(',',@stateIdString) > 0
		begin
		insert into @t1 select substring(@stateIdString,1,(charindex(',',@stateIdString)-1))
		SET @stateIdString = substring(@stateIdString,charindex(',',@stateIdString)+1,len(@stateIdString))
		end
		insert into @t1
		select @stateIdString
	
		
	
		SELECT pvt.vendor_id    
			   , pvt.vendor_name
			   ,[290] as el_Commodity
			   ,[291] as ng_Commodity
		FROM (  
		  select vendor.vendor_id    
			   , vendor.vendor_name        
			   ,Min(cmap.commodity_type_id) Commodity_Id
		   from  vendor vendor (nolock)    
				 inner join entity entity (nolock) on entity.entity_id = vendor.vendor_type_id    
				 inner join vendor_state_map map (nolock) on map.vendor_id = vendor.vendor_id   
				 inner join vendor_commodity_map cmap (nolock) on  cmap.vendor_id = map.vendor_id           
		   inner join @t1 t1 on t1.state_id = map.state_id    
		   where vendor.is_history = 0    
				 and (entity.entity_name = @entityName)    
				 and (entity.entity_type = @entityType)             
				 and (@vendorName is null or vendor.vendor_name like @vendorName) 
		   GROUP BY vendor.vendor_id,vendor.vendor_name  , cmap.commodity_type_id  
	   ) t
		PIVOT( MIN(Commodity_Id) FOR Commodity_id IN ([290],[291])) AS pvt       
	order by pvt.vendor_name    
 end    
 else    
 begin  
	 SELECT pvt.vendor_id    
			   , pvt.vendor_name
			   ,[290] as el_Commodity
			   ,[291] as ng_Commodity
	 FROM (  
	  select vendor.vendor_id    
		   , vendor.vendor_name    
			,Min(cmap.commodity_type_id) Commodity_Id
	   from  vendor vendor (nolock)    
			 inner join entity entity (nolock) on entity.entity_id = vendor.vendor_type_id  
			 inner join vendor_commodity_map cmap (nolock) on  cmap.vendor_id = vendor.vendor_id                        
	   where vendor.is_history = 0    
			 and (entity.entity_name = @entityName)    
			 and (entity.entity_type = @entityType)             
			 and (@vendorName is null or vendor.vendor_name like @vendorName)  
	   GROUP BY vendor.vendor_id, vendor.vendor_name  ,cmap.commodity_type_id  
	   ) t
	PIVOT( MIN(Commodity_Id) FOR Commodity_id IN ([290],[291])) AS pvt
	   order by pvt.vendor_name    
 end 
END
GO
GRANT EXECUTE ON  [dbo].[GET_VENDOR_SEARCH_LIST_P] TO [CBMSApplication]
GO
