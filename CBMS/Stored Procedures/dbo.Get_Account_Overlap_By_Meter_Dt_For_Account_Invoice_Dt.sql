SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********     
NAME:  dbo.Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt

DESCRIPTION:  Used to check if the invoice term overlaps with meter dates of other accounts of the meter associated to the given account. 

INPUT PARAMETERS:      
      Name              DataType          Default     Description      
-----------------------------------------------------------------------------------------------      
  @Account_Id			INT  
  @InvBeginDate			DATETIME  
  @InvEndDate			DATETIME     

OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
-----------------------------------------------------------------------------------------------      
USAGE EXAMPLES:    

 EXEC Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt 35071,'2008-11-10','2009-12-01'  
 EXEC Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt 105559,'2008-10-28','2010-10-31'  
 EXEC Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt 135109,'2009-05-13','2011-04-30'  

-----------------------------------------------------------------------------------------------      
AUTHOR INITIALS:    
Initials				Modification    
-----------------------------------------------------------------------------------------------      
HG						Hariharasuthan Ganesan         

Initials		Date			Modification    
-----------------------------------------------------------------------------------------------      
HG				2020-03-12		MAINT-10059, Created a new sproc to do the invoice date overlapping check by Meter dates
******/
CREATE PROCEDURE [dbo].[Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt]
    (
        @Account_Id   INT
       ,@InvBeginDate DATETIME
       ,@InvEndDate   DATETIME
    )
AS
BEGIN

    SET NOCOUNT ON;

    SELECT
        suppacc2.ACCOUNT_ID
       ,ISNULL(NULLIF(suppacc2.ACCOUNT_NUMBER, ''), 'Not Yet Assigned') AS ACCOUNT_NUMBER
    FROM
        dbo.SUPPLIER_ACCOUNT_METER_MAP samm1
        INNER JOIN dbo.SUPPLIER_ACCOUNT_METER_MAP samm2
            ON samm1.METER_ID = samm2.METER_ID
        INNER JOIN dbo.ACCOUNT suppacc1
            ON samm1.ACCOUNT_ID = suppacc1.ACCOUNT_ID
        INNER JOIN dbo.Supplier_Account_Config sac1
            ON sac1.Account_Id = suppacc1.ACCOUNT_ID
        INNER JOIN dbo.ACCOUNT suppacc2
            ON samm2.ACCOUNT_ID = suppacc2.ACCOUNT_ID
        INNER JOIN dbo.Supplier_Account_Config sac2
            ON sac2.Account_Id = suppacc2.ACCOUNT_ID
    WHERE
        samm1.ACCOUNT_ID = @Account_Id
        AND samm1.ACCOUNT_ID <> samm2.ACCOUNT_ID
        AND (
                samm2.METER_ASSOCIATION_DATE BETWEEN @InvBeginDate
                                             AND     @InvEndDate
                OR ISNULL(samm2.METER_DISASSOCIATION_DATE, '9999-12-31') BETWEEN @InvBeginDate
                                                                         AND     @InvEndDate
                OR @InvBeginDate BETWEEN samm2.METER_ASSOCIATION_DATE
                                 AND     ISNULL(samm2.METER_DISASSOCIATION_DATE, '9999-12-31')
                OR @InvEndDate BETWEEN samm2.METER_ASSOCIATION_DATE
                               AND     ISNULL(samm2.METER_DISASSOCIATION_DATE, '9999-12-31')
            )
    GROUP BY suppacc2.ACCOUNT_ID
            ,ISNULL(NULLIF(suppacc2.ACCOUNT_NUMBER, ''), 'Not Yet Assigned');

END;
GO
GRANT EXECUTE ON  [dbo].[Get_Account_Overlap_By_Meter_Dt_For_Account_Invoice_Dt] TO [CBMSApplication]
GO
