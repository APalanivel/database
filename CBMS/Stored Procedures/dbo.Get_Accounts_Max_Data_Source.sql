SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                          
                
NAME: Get_Accounts_Max_Data_Source   
  
DESCRIPTION:                
                
	To get the Accounts Max Data source  for the selected  Client_Hier_Id,Commodity_id INT And Service_Month
	
  
INPUT PARAMETERS:                          
NAME				DATATYPE DEFAULT  DESCRIPTION                          
------------------------------------------------------------                          
@Client_Hier_Id		INT
@Commodity_id		INT
@Service_Month		DATE 
  
OUTPUT PARAMETERS:                
NAME   DATATYPE DEFAULT  DESCRIPTION                
                       
------------------------------------------------------------                          
USAGE EXAMPLES:                          
------------------------------------------------------------                  
                
    EXEC Get_Accounts_Max_Data_Source    
      @Client_Hier_Id = 1130
     ,@Commodity_id = 291
     ,@Service_Month = '2012-01-01' 

    EXEC Get_Accounts_Max_Data_Source
      @Client_Hier_Id = 1271
     ,@Commodity_id = 290
     ,@Service_Month = '2010-12-01'
      
AUTHOR INITIALS:                          
INITIALS	NAME  
------------------------------------------------------------                          
PKY			Pavan K Yadalam

MODIFICATIONS
INITIALS	DATE		MODIFICATION
---------------------------------------------------------------
PKY			2013-03-14	Created for Calendarization requirement

*/
CREATE PROCEDURE dbo.Get_Accounts_Max_Data_Source
      ( @Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Service_Month DATE )
AS
BEGIN
     
      SET NOCOUNT ON          

      DECLARE @Max_Dsc AS INT    

      SELECT
            @Max_Dsc = MAX(Data_Source_Cd)
      FROM
            dbo.Cost_Usage_Account_Dtl cuad
            INNER JOIN dbo.Bucket_Master bm
                  ON cuad.Bucket_Master_Id = bm.Bucket_Master_Id
      WHERE
            Client_Hier_Id = @Client_Hier_Id
            AND Service_Month = @Service_Month
            AND bm.Commodity_Id = @Commodity_id

      SELECT
            Code_Value AS Data_Source_Code
      FROM
            dbo.Code
      WHERE
            Code_Id = @Max_Dsc

END

;
GO
GRANT EXECUTE ON  [dbo].[Get_Accounts_Max_Data_Source] TO [CBMSApplication]
GO
