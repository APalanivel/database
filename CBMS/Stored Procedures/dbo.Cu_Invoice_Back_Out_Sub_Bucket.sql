SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
                      
Name:             
 dbo.Cu_Inovice_BackOut_Sub_Bucket                 
                        
Description:                        
   To Backout Sub Bucket for Invoice   
  
 Input Parameters:                        
 Name      DataType   Default     Description                          
---------------------------------------------------------------------------------------------          
 @Cu_Invoice_Id      INT          
        
              
Output Parameters:                              
 Name      DataType   Default     Description                          
---------------------------------------------------------------------------------------------          
                        
Usage Examples:                            
---------------------------------------------------------------------------------------------           
exec dbo.Cu_Inovice_BackOut_Sub_Bucket    100001      
        
           
Author Initials:                        
 Initials	Name                        
------------- --------------------------------------------------------------------------------          
 HKT			Harish Kumar Tirumandyam                 
           
Modifications:                        
Initials	Date			Modification                        
------------  ----------------  -----------------------------------------------------------------          
 HKT		2020-06-03      Created For Data Purple                   
            
******/
CREATE PROCEDURE [dbo].[Cu_Invoice_Back_Out_Sub_Bucket]
(
	@Cu_Invoice_Id	INT
	, @User_Info_Id INT
)
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Exception_Status_Cd INT;

		SELECT
			@Exception_Status_Cd = cd.Code_Id
		FROM
			dbo.Code cd
			JOIN dbo.Codeset cs
				ON cd.Codeset_Id = cs.Codeset_Id
		WHERE
			cs.Codeset_Name = 'Exception Status' AND cd.Code_Value = 'Closed';

		UPDATE
			CIC
		SET
			CIC.EC_Invoice_Sub_Bucket_Master_Id = NULL
		FROM
			dbo.CU_INVOICE_CHARGE AS CIC
		WHERE
			CIC.CU_INVOICE_ID = @Cu_Invoice_Id;

		UPDATE
			CID
		SET
			CID.EC_Invoice_Sub_Bucket_Master_Id = NULL
		FROM
			dbo.CU_INVOICE_DETERMINANT AS CID
		WHERE
			CID.CU_INVOICE_ID = @Cu_Invoice_Id;

		UPDATE
			CISDED
		SET
			CISDED.Status_Cd = @Exception_Status_Cd
			, CISDED.Updated_User_Id = @User_Info_Id
			, CISDED.Last_Change_Ts = GETDATE()
		FROM
			dbo.Cu_Invoice_Standing_Data_Exception_Dtl CISDED
			JOIN [dbo].[Cu_Invoice_Standing_Data_Exception] AS [CISDE]
				ON CISDED.Cu_Invoice_Standing_Data_Exception_Id = [CISDE].Cu_Invoice_Standing_Data_Exception_Id
		WHERE
			[CISDE].Cu_Invoice_Id = @Cu_Invoice_Id;

		UPDATE
			CISDE
		SET
			CISDE.Exception_Status_Cd = @Exception_Status_Cd
			, CISDE.Updated_User_Id = @User_Info_Id
			, CISDE.Closed_By_User_Id = @User_Info_Id
			, CISDE.Closed_Ts = GETDATE()
			, CISDE.Last_Change_Ts = GETDATE()
		FROM
			[dbo].[Cu_Invoice_Standing_Data_Exception] CISDE
		WHERE
			Cu_Invoice_Id = @Cu_Invoice_Id;

	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRAN;
		EXEC dbo.usp_RethrowError;

	END CATCH;

END;

GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Back_Out_Sub_Bucket] TO [CBMSApplication]
GO
