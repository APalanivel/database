SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_SAD_CREATE_DEAL_TICKET_P

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@userId        	varchar(10)	          	
	@sessionId     	varchar(20)	          	
	@countryId     	int       	          	
	@commodityId   	int       	          	
	@clientId      	int       	          	
	@termTypeId    	int       	          	
	@fromDate      	datetime  	          	
	@toDate        	datetime  	          	
	@volumeSourceId	int       	          	
	@volumeSourceOtherType	varchar(25)	          	
	@percentageHedged	decimal(32,16)	          	
	@initiatedBy   	int       	          	
	@initiationDate	datetime  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  PROCEDURE dbo.SR_SAD_CREATE_DEAL_TICKET_P
@userId varchar(10),
@sessionId varchar(20),
@countryId int,
@commodityId int,
@clientId int,
@termTypeId int,
@fromDate dateTime,
@toDate dateTime,
@volumeSourceId int,
@volumeSourceOtherType varchar(25),
@percentageHedged decimal(32,16),
@initiatedBy int, 
@initiationDate dateTime

AS
set nocount on
insert into SR_DEAL_TICKET 
		(COUNTRY_ID, COMMODITY_TYPE_ID, CLIENT_ID, VOLUME_TERM_TYPE_ID, 
		FROM_DATE, TO_DATE, VOLUME_SOURCE_TYPE_ID, VOLUME_SOURCE_OTHER_VALUE, 
		PERCENTAGE_HEDGE, INITIATED_BY, INITIATION_DATE)

	values (@countryId, @commodityId, @clientId, @termTypeId, 
	@fromDate, @toDate, @volumeSourceId, @volumeSourceOtherType, 
	@percentageHedged, @initiatedBy, @initiationDate)
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_CREATE_DEAL_TICKET_P] TO [CBMSApplication]
GO
