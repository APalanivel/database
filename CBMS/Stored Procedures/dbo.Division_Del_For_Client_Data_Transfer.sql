SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************************************************************************************************
NAME : dbo.[Division_Del_For_Client_Data_Transfer]

DESCRIPTION: This procedure used to delete a Divions / Sitegroup

 INPUT PARAMETERS:      

 Name			DataType		Default			Description      
--------------------------------------------------------------------        

 @Message		XML   

 OUTPUT PARAMETERS:      

 Name   DataType  Default Description      
--------------------------------------------------------------------      

  USAGE EXAMPLES:
--------------------------------------------------------------------      
	BEGIN TRAN
		EXEC [Division_Del_For_Client_Data_Transfe] 
			@Client_Hier_Id = 
	ROLLBACK TRAN

AUTHOR INITIALS:      

 Initials		Name      
-------------------------------------------------------------------       
 MSV			Muhamed Shahid V

 MODIFICATIONS  

 Initials		Date			Modification  
--------------------------------------------------------------------  
 MSV			05 May 2019		Created 	

*****************************************************************************************************/
CREATE PROCEDURE [dbo].[Division_Del_For_Client_Data_Transfer]
	(
		@Client_Hier_Id INT
	)
AS
BEGIN
	
    SET NOCOUNT ON

    DECLARE
        @Division_Id INT
        ,@User_Info_Id INT
		,@Hier_level_Cd INT
		
    SELECT
        @User_Info_Id = ui.User_Info_Id
    FROM
        dbo.USER_INFO ui
    WHERE
        ui.username = 'conversion'
	
	SELECT	@Division_Id = ch.Sitegroup_Id
			,@Hier_level_Cd = ch.Hier_level_Cd
	FROM Core.Client_Hier ch 
	WHERE ch.Client_Hier_Id = @Client_Hier_Id   

	BEGIN TRY
		BEGIN TRAN;

		IF @Hier_level_Cd = 100015
		BEGIN
		--Division delete
			
			EXEC Delete_Div_From_Roles 
				@Division_Id = @Division_Id

			EXEC Division_History_Del_By_Division_Id 
				@Division_Id= @Division_Id,
				@User_Info_Id = @User_Info_Id
			
		END
		ELSE 
		BEGIN
		--Sitegroup delete

			EXEC SiteGroup_DEL 
				@Sitegroup_id = @Division_Id

		END
		

		COMMIT TRAN
	END TRY
	BEGIN CATCH

		IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN;
		END;

		EXEC dbo.usp_RethrowError;
	END CATCH;
END;

GO
GRANT EXECUTE ON  [dbo].[Division_Del_For_Client_Data_Transfer] TO [CBMSApplication]
GO
