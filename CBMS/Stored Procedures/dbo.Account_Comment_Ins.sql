SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.Account_Comment_Ins

DESCRIPTION:

INPUT PARAMETERS:
	Name					DataType		Default	Description
---------------------------------------------------------------
	@Account_Id				INT
	@Comment_Type			VARCHAR(25)
	@Comment_User_Info_Id	INT
	@Comment_Text			VARCHAR(MAX)

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT * FROM dbo.Account_Comment

	EXEC dbo.CODE_SEL_BY_CodeSet_Name @CodeSet_Name = 'CommentType', @Code_Value = 'AccountComment'
	SELECT TOP 10 * FROM dbo.ACCOUNT

	BEGIN TRANSACTION
		SELECT * FROM dbo.Account_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.Account_Id = 1
		EXEC dbo.Account_Comment_Ins 1, 'AccountComment', 16, 'Test_Account_Comment_Ins_Test'
		SELECT * FROM dbo.Account_Comment a INNER JOIN dbo.Comment b ON a.Comment_Id = b.Comment_ID
			WHERE a.Account_Id = 1
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy
	
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR       	2015-05-19	Created for AS400

******/
CREATE PROCEDURE [dbo].[Account_Comment_Ins]
      ( 
       @Account_Id INT
      ,@Comment_Type VARCHAR(25)
      ,@User_Info_Id INT
      ,@Comment_Text VARCHAR(MAX) )
AS 
BEGIN

      SET NOCOUNT ON;
      
      DECLARE
            @Comment_Id INT
           ,@Comment_Type_CD INT
          
      SELECT
            @Comment_Type_CD = cd.Code_Id
      FROM
            dbo.Code cd
            JOIN dbo.Codeset cs
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'CommentType'
            AND cd.Code_Value = @Comment_Type  
        
      EXEC dbo.Comment_Ins 
            @Comment_Type_CD = @Comment_Type_CD
           ,@Comment_User_Info_Id = @User_Info_Id
           ,@Comment_Dt = NULL
           ,@Comment_Text = @Comment_Text
           ,@Comment_Id = @Comment_Id OUT 
      
      INSERT      INTO dbo.Account_Comment
                  ( Account_Id, Comment_Id )
      VALUES
                  ( @Account_Id, @Comment_Id )
                  
END;



;
GO
GRANT EXECUTE ON  [dbo].[Account_Comment_Ins] TO [CBMSApplication]
GO
