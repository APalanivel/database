
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
/******            
NAME:   Cu_Invoice_Get_Image_Changes_For_HubTransfer         
  
      
DESCRIPTION:    
   
        
INPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------            
  
                  
OUTPUT PARAMETERS:            
Name   DataType Default  Description            
------------------------------------------------------------   
  
           
USAGE EXAMPLES:            
------------------------------------------------------------   
 EXEC etl.Cu_Invoice_Get_Image_Changes 0  
         
       
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
CMH   Chad Hattabaugh    
DMR   Deana Ritter       
       
MODIFICATIONS             
Initials Date Modification            
------------------------------------------------------------            
CMH   12/29/2010 Created  
DMR		12/8/2014	Removed Transaction Isolation Level statements.  Disabled Database level snap shot isolation.
*****/   
CREATE PROCEDURE [dbo].[Cu_Invoice_Get_Image_Changes_For_HubTransfer]
      ( 
       @in_Last_Processed BIGINT = 0 )
AS 
BEGIN  
      
      DECLARE @Data_Source_Cd_Invoice INT;
      
        SELECT
            @Data_Source_Cd_Invoice = cd.CODE_ID
      FROM
            codeset cs
            JOIN code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Data_Source_Cd'
            AND cd.Code_Value = 'Invoice'              
                                     
         
 -- Data is continious: get changes  
      SELECT
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.ACCOUNT_ID
           ,cng.Cu_Invoice_id
           ,convert(DATE, ism.Service_Month) AS Service_Month
           ,ci.CBMS_IMAGE_ID
           ,convert(CHAR(1), cng.Sys_CHANGE_OPERATION) AS sys_Change_Operation
           ,@Data_Source_Cd_Invoice AS Data_Source_Cd
      FROM
            CHANGETABLE(CHANGES CU_Invoice, @in_Last_Processed) cng
            LEFT JOIN cu_Invoice_Service_Month ism
                  ON cng.CU_invoice_Id = ism.CU_invoice_Id
                     AND ism.Service_Month IS NOT NULL
            LEFT JOIN Core.Client_Hier_Account cha
                  ON ism.Account_id = cha.Account_id
            LEFT JOIN dbo.CU_INVOICE ci
                  ON ci.Cu_Invoice_Id = cng.Cu_Invoice_Id
                     AND ci.IS_REPORTED = 1
      WHERE
            ( cng.Sys_Change_Operation = 'd'
              OR ci.CBMS_Image_Id IS NOT NULL )
      GROUP BY
            cha.Client_Hier_Id
           ,cha.Commodity_Id
           ,ism.ACCOUNT_ID
           ,cng.Cu_Invoice_id
           ,convert(DATE, ism.Service_Month)
           ,ci.CBMS_IMAGE_ID
           ,cng.Sys_CHANGE_OPERATION  
END  


;
GO


GRANT EXECUTE ON  [dbo].[Cu_Invoice_Get_Image_Changes_For_HubTransfer] TO [CBMSApplication]
GO
