SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
      
/******        
Name:        
    dbo.User_Info_Upd_New_User_Email_Ts_By_User_Info_Id       
       
Description:        
    Update time stamp to null.. used in SSO
       
Input Parameters:        
    Name    DataType Default Description        
------------------------------------------------------------------------        
   @UserInfoId  int
  
       
Output Parameters:        
 Name  Datatype  Default Description        
------------------------------------------------------------        
       
Usage Examples:      
------------------------------------------------------------     
Begin Tran   
   Execute   User_Info_Upd_New_User_Email_Ts_By_User_Info_Id   49
Rollback       
       
Author Initials:        
 Initials   Name      
------------------------------------------------------------      
 AS    Arun Skaria      
       
 Modifications :        
 Initials   Date   Modification        
------------------------------------------------------------        
    AS  08-Apr-2014  Created  
******/      
CREATE  PROCEDURE dbo.User_Info_Upd_New_User_Email_Ts_By_User_Info_Id   
 (   
  @UserInfoId int
 )  
AS  
BEGIN        
      SET NOCOUNT ON       
           
     Update dbo.USER_INFO 
		Set New_User_EMail_Ts = NULL
     Where USER_INFO_ID = @UserInfoId
       
        
END      
  
  

;
GO
GRANT EXECUTE ON  [dbo].[User_Info_Upd_New_User_Email_Ts_By_User_Info_Id] TO [CBMSApplication]
GO
