SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsUbmInvoice_ShowInvoiceForProcessing]
	( @MyAccountId int )
AS
BEGIN
	   select distinct i.ubm_invoice_id
		, i.cbms_image_id
		, i.ubm_batch_master_log_id
		, min(det.service_start_date) begin_date
	     from ubm_invoice i  with (nolock)
  left outer join ubm_invoice_details det with (nolock) on det.ubm_invoice_id = i.ubm_invoice_id
	    where i.cu_invoice_batch_id is null
	      and i.is_processed = 0
	      and i.is_quarterly = 0
	      and i.ubm_client_id is not null
	      and i.cbms_image_id is not null
	 group by i.ubm_invoice_id
		, i.cbms_image_id
		, i.ubm_batch_master_log_id
END


GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoice_ShowInvoiceForProcessing] TO [CBMSApplication]
GO
