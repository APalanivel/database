SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/******  
NAME:  
 
 dbo.[UBM_SEL_BY_UBM_NAME]
 
 DESCRIPTION:   
 
 Inserts record in the UBM_BATCH_MASTER_LOG table and returns
 the UBM_BATCH_MASTER_LOG_ID.  This procedure was copied
 from the UBM_SET_UBM_MASTER_LOG_P.  The difference between
 this procedure and UBM_SET_UBM_MASTER_LOG_P is that this
 procedure returns the UBM_BATCH_MASTER_LOG_ID.
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@ubmName varchar(20)

 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------
@UBM_BATCH_MASTER_LOG_ID INT

  USAGE EXAMPLES:  
------------------------------------------------------------  
	EXEC UBM_BATCH_MASTER_LOG_INS 'CASS'

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 GP			Garrett Page	10/2/2009
 
    
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  			    

******/


CREATE   PROCEDURE [dbo].[UBM_SEL_BY_UBM_NAME]
(
	@ubmName varchar(20)
)
AS
BEGIN

	set nocount on
	
	SELECT UBM_ID,UBM_NAME FROM UBM WHERE UBM_NAME=@ubmName
	
	RETURN
	
END




GO
GRANT EXECUTE ON  [dbo].[UBM_SEL_BY_UBM_NAME] TO [CBMSApplication]
GO
