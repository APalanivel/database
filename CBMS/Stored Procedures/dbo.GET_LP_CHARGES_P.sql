SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  PROCEDURE dbo.GET_LP_CHARGES_P
	@chargeTypeName VARCHAR(200),
	@contractId INT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @selectQuery varchar(8000)
		, @fromQuery varchar(8000)
		, @whereQuery varchar(8000)
		, @finalQuery varchar(8000)
	
	
	IF @chargeTypeName = 'Baseload'
	 BEGIN

		SELECT distinct CM.CHARGE_MASTER_ID,
			CM.CHARGE_TYPE_ID,  
			chargeType.ENTITY_NAME chargeType,  
			CM.CHARGE_DISPLAY_ID,  
			CM.CHARGE_PARENT_ID,  
			CM.CHARGE_PARENT_TYPE_ID,  
			masterPType.entity_name, 
			C.CHARGE_ID,  
			C.CHARGE_NAME,  
			C.CHARGE_CALCULATION_TYPE_ID,  
			CALCTYPE.ENTITY_NAME CALCNAME,  
			C.CHARGE_PARENT_ID,
			C.CHARGE_PARENT_TYPE_ID,
			chargePType.entity_name, 
			C.CHARGE_EXPRESSION
		FROM CHARGE_MASTER CM (NOLOCK) INNER JOIN CHARGE C (NOLOCK) ON C.CHARGE_MASTER_ID = CM.CHARGE_MASTER_ID
			LEFT JOIN ENTITY CALCTYPE (NOLOCK) ON (CALCTYPE.ENTITY_ID = C.CHARGE_CALCULATION_TYPE_ID)
			INNER JOIN ENTITY chargePType (NOLOCK) ON chargePType.ENTITY_ID = C.CHARGE_PARENT_TYPE_ID
			INNER JOIN ENTITY masterPType (NOLOCK) ON masterPType.entity_id = cm.charge_parent_type_id
			INNER JOIN ENTITY chargeType (NOLOCK) ON chargeType.ENTITY_ID = CM.CHARGE_TYPE_ID 
			INNER JOIN baseload_details bld (NOLOCK) ON bld.baseload_id = cm.charge_parent_id
			INNER JOIN load_profile_specification lps (NOLOCK) ON lps.load_profile_specification_id = bld.load_profile_specification_id
		WHERE chargeType.entity_name = @chargeTypeName
			--AND cm.CHARGE_PARENT_ID = @contractId
			AND lps.contract_id = @contractId

	 END
	ELSE
	 BEGIN

		SELECT distinct CM.CHARGE_MASTER_ID,
			CM.CHARGE_TYPE_ID,  
			chargeType.ENTITY_NAME chargeType,  
			CM.CHARGE_DISPLAY_ID,  
			CM.CHARGE_PARENT_ID,  
			CM.CHARGE_PARENT_TYPE_ID,  
			masterPType.entity_name, 
			C.CHARGE_ID,  
			C.CHARGE_NAME,  
			C.CHARGE_CALCULATION_TYPE_ID,  
			CALCTYPE.ENTITY_NAME CALCNAME,  
			C.CHARGE_PARENT_ID,  
			C.CHARGE_PARENT_TYPE_ID, 
			chargePType.entity_name, 
			C.CHARGE_EXPRESSION
		FROM CHARGE_MASTER CM (NOLOCK) INNER JOIN CHARGE C (NOLOCK) ON C.CHARGE_MASTER_ID = CM.CHARGE_MASTER_ID
			LEFT JOIN ENTITY CALCTYPE (NOLOCK) ON (CALCTYPE.ENTITY_ID = C.CHARGE_CALCULATION_TYPE_ID)
			INNER JOIN ENTITY chargePType (NOLOCK) ON chargePType.ENTITY_ID = C.CHARGE_PARENT_TYPE_ID
			INNER JOIN ENTITY masterPType (NOLOCK) ON masterPType.entity_id = cm.charge_parent_type_id
			INNER JOIN ENTITY chargeType (NOLOCK) ON chargeType.ENTITY_ID = CM.CHARGE_TYPE_ID 
		WHERE chargeType.entity_name = @chargeTypeName
			AND cm.CHARGE_PARENT_ID = @contractId

	 END


END
GO
GRANT EXECUTE ON  [dbo].[GET_LP_CHARGES_P] TO [CBMSApplication]
GO
