SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                        
 NAME: dbo.Contact_Info_Sel            
                        
 DESCRIPTION:                        
			To get the details of invoice collection Client level config                  
                        
 INPUT PARAMETERS:          
                     
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
 @Contact_Info_Id					INT
,@Contact_Level_Cd					INT
,@Contact_Type_Cd					INT
,@Client_Id							INT
,@Vendor_Id							INT
,@Is_Active							INT
                              
 OUTPUT PARAMETERS:          
                           
 Name                        DataType         Default       Description        
---------------------------------------------------------------------------------------------------------------      
                        
 USAGE EXAMPLES:                            
---------------------------------------------------------------------------------------------------------------                            

 --Client Level
 
  EXEC [dbo].[Contact_Info_Sel] 
      @Client_Id = 14525
     ,@Is_Active = 1
     ,@Contact_Level_Cd =102255
      ,@Contact_Type_Cd =102256
      
--Vendor level

  EXEC [dbo].[Contact_Info_Sel] 
	   @Vendor_Id = 14525
      ,@Is_Active = 1
      ,@Contact_Level_Cd =102255
      ,@Contact_Type_Cd =102257
--Account level

  EXEC [dbo].[Contact_Info_Sel] 
	   @Client_Id = 14525
      ,@Is_Active = 1
      ,@Contact_Level_Cd =102254
      ,@Contact_Type_Cd =102256
       
  EXEC [dbo].[Contact_Info_Sel]   
    @Client_Id = 11554
      ,@Is_Active = 1  
      ,@Contact_Level_Cd =102255  
      ,@Contact_Type_Cd =102256
      ,@StartIndex = 2
      ,@EndIndex=3



	  EXEC [dbo].[Contact_Info_Sel]  
 @Contact_Info_Id=null
,  @Vendor_Id=null
,@Total_Row_Count=0 
    ,@Client_Id = 14846  
      ,@Is_Active = 1    
      ,@Contact_Level_Cd =102286    
      ,@Contact_Type_Cd =102328  
      ,@StartIndex = 1  
      ,@EndIndex=25

                          
                       
 AUTHOR INITIALS:        
       
 Initials              Name        
---------------------------------------------------------------------------------------------------------------                      
 SP                    Sandeep Pigilam     
 SP                    Srinivas Patchava     
                         
 MODIFICATIONS:      
          
 Initials              Date             Modification      
---------------------------------------------------------------------------------------------------------------      
 SP                    2016-11-21       Created for Invoice Tracking               
 HG						2019-03-19		Modified to return 300 records irrespective of the endindex as app is taking time to load the data. 
 SP                    2019-07-08       Modified the TOP (@EndIndex) return records                  
******/
CREATE PROCEDURE [dbo].[Contact_Info_Sel]
    (
        @Contact_Info_Id INT = NULL
        , @Contact_Level_Cd INT
        , @Contact_Type_Cd INT
        , @Client_Id INT = NULL
        , @Vendor_Id INT = NULL
        , @Is_Active INT
        , @StartIndex INT = 1
        , @EndIndex INT = 2147483647
        , @Total_Row_Count INT = 0
    )
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE
            @Client_Name VARCHAR(200)
            , @Vendor_Name VARCHAR(200);

        SELECT
            @Vendor_Name = VENDOR_NAME
        FROM
            dbo.VENDOR
        WHERE
            VENDOR_ID = @Vendor_Id;


        SELECT
            @Client_Name = ch.Client_Name
        FROM
            Core.Client_Hier ch
        WHERE
            ch.Client_Id = @Client_Id
            AND ch.Sitegroup_Id = 0;

        IF @Total_Row_Count = 0
            BEGIN
                SELECT
                    @Total_Row_Count = COUNT(1)
                FROM
                    dbo.Contact_Info ci
                WHERE
                    (   @Contact_Info_Id IS NULL
                        OR  ci.Contact_Info_Id = @Contact_Info_Id)
                    AND ci.Contact_Level_Cd = @Contact_Level_Cd
                    AND ci.Contact_Type_Cd = @Contact_Type_Cd
                    AND ci.Is_Active = @Is_Active
                    AND (   EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Client_Contact_Map cc
                                       WHERE
                                            cc.Contact_Info_Id = ci.Contact_Info_Id
                                            AND cc.CLIENT_ID = @Client_Id)
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Vendor_Contact_Map vc
                                           WHERE
                                                vc.Contact_Info_Id = ci.Contact_Info_Id
                                                AND vc.VENDOR_ID = @Vendor_Id));
            END;
        WITH CTE_Contacts_List
        AS (
               SELECT
                    ci.Contact_Info_Id
                    , ci.Email_Address
                    , ci.Fax_Number
                    , ci.Phone_Number
                    , ci.First_Name
                    , ci.Last_Name
                    , ci.Job_Position
                    , ci.Location
                    , c.Comment_Text
                    , c.Comment_ID
                    , ci.Language_Cd
                    , lc.Code_Dsc AS Language_Cd_Value
                    , @Client_Name AS Client_Name
                    , @Vendor_Name AS Vendor_Name
                    , ROW_NUMBER() OVER (ORDER BY
                                             ci.Last_Name
                                             , ci.First_Name) Row_Num
                    , MAX(CASE WHEN icaa.Contact_Info_Id IS NULL THEN 0
                              ELSE 1
                          END) Is_Having_IC_References
               FROM
                    dbo.Contact_Info ci
                    LEFT JOIN dbo.Comment c
                        ON ci.Comment_Id = c.Comment_ID
                    LEFT JOIN dbo.Code lc
                        ON ci.Language_Cd = lc.Code_Id
                    LEFT JOIN dbo.Invoice_Collection_Account_Contact icaa
                        ON icaa.Contact_Info_Id = ci.Contact_Info_Id
               WHERE
                    (   @Contact_Info_Id IS NULL
                        OR  ci.Contact_Info_Id = @Contact_Info_Id)
                    AND ci.Contact_Level_Cd = @Contact_Level_Cd
                    AND ci.Contact_Type_Cd = @Contact_Type_Cd
                    AND ci.Is_Active = @Is_Active
                    AND (   EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Client_Contact_Map cc
                                       WHERE
                                            cc.Contact_Info_Id = ci.Contact_Info_Id
                                            AND cc.CLIENT_ID = @Client_Id)
                            OR  EXISTS (   SELECT
                                                1
                                           FROM
                                                dbo.Vendor_Contact_Map vc
                                           WHERE
                                                vc.Contact_Info_Id = ci.Contact_Info_Id
                                                AND vc.VENDOR_ID = @Vendor_Id))
               GROUP BY
                   ci.Contact_Info_Id
                   , ci.Email_Address
                   , ci.Fax_Number
                   , ci.Phone_Number
                   , ci.First_Name
                   , ci.Last_Name
                   , ci.Job_Position
                   , ci.Location
                   , c.Comment_Text
                   , c.Comment_ID
                   , ci.Language_Cd
                   , lc.Code_Dsc
           )
        SELECT  TOP (@EndIndex)
                cl.Contact_Info_Id
                , cl.Email_Address
                , cl.Fax_Number
                , cl.Phone_Number
                , cl.First_Name
                , cl.Last_Name
                , cl.Job_Position
                , cl.Location
                , cl.Comment_Text
                , cl.Comment_ID
                , cl.Language_Cd
                , cl.Language_Cd_Value
                , cl.Client_Name
                , cl.Vendor_Name
                , @Total_Row_Count AS Total_Row_Count
                , cl.Is_Having_IC_References
        FROM
            CTE_Contacts_List cl
        WHERE
            cl.Row_Num BETWEEN @StartIndex
                       AND     @EndIndex
        ORDER BY
            cl.Last_Name
            , cl.First_Name;
    END;


GO
GRANT EXECUTE ON  [dbo].[Contact_Info_Sel] TO [CBMSApplication]
GO
