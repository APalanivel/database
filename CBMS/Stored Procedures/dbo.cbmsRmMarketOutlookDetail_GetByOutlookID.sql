SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  procedure dbo.cbmsRmMarketOutlookDetail_GetByOutlookID

	(@AccountId int
	, @OutlookId int
	)

as
begin
	select rm_market_outlook_id
		,  rm_market_outlook_detail_id
		, cbms_image_id
		, overview
		, rm_market_outlook_title
		, language_type
	from rm_market_outlook_detail
	where rm_market_outlook_id = @OutlookId
end
GO
GRANT EXECUTE ON  [dbo].[cbmsRmMarketOutlookDetail_GetByOutlookID] TO [CBMSApplication]
GO
