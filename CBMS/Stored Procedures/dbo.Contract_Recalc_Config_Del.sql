SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.Contract_Recalc_Config_Del

DESCRIPTION:

INPUT PARAMETERS:
	Name										DataType		Default	Description
--------------------------------------------------------------------------------------
	@Contract_Recalc_Config_Id		INT   

OUTPUT PARAMETERS:
	Name					DataType		Default	Description
--------------------------------------------------------------------------------------
	
USAGE EXAMPLES:
--------------------------------------------------------------------------------------

   
SELECT  * FROM  Contract_Recalc_Config WHERE   Contract_Recalc_Config_Id = 1

EXEC Contract_Recalc_Config_Del
    @Contract_Recalc_Config_Id = 1
   
SELECT  * FROM  Contract_Recalc_Config WHERE   Contract_Recalc_Config_Id = 1


AUTHOR INITIALS:
	Initials	Name
--------------------------------------------------------------------------------------
	NR			Narayana Reddy
	
MODIFICATIONS
	Initials	Date		Modification
--------------------------------------------------------------------------------------
	NR       	2019-06-26	Created for Add contract

******/


CREATE PROCEDURE [dbo].[Contract_Recalc_Config_Del]
    (
        @Contract_Recalc_Config_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;


        DELETE
        acirt
        FROM
            dbo.Contract_Recalc_Config acirt
        WHERE
            acirt.Contract_Recalc_Config_Id = @Contract_Recalc_Config_Id;


    END;


GO
GRANT EXECUTE ON  [dbo].[Contract_Recalc_Config_Del] TO [CBMSApplication]
GO
