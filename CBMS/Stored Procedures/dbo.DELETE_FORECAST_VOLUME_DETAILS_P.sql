SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE    PROCEDURE dbo.DELETE_FORECAST_VOLUME_DETAILS_P
@userId varchar(10),
@sessionId varchar(20),
@clientId int,
@siteId int,
@month_identifier datetime,
--@asOfDate datetime,
@forecastYear int


 AS
delete  rm_forecast_volume_details  
where
	site_id = @siteId
	and month_identifier= @month_identifier
	and rm_forecast_volume_id =
 			(select rm_forecast_volume_id from rm_forecast_volume(nolock) 
				where  forecast_year=@forecastYear 
				       --and forecast_as_of_date= @asOfDate 
				       and client_id = @clientId)
GO
GRANT EXECUTE ON  [dbo].[DELETE_FORECAST_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
