SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                  
Name:                  
        dbo.Account_Invoice_Processing_Config_Watch_List_Upd                    
                  
Description:                  
			Watch List - Ability to timeband watch list settings.                  
                  
 Input Parameters:                  
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
 @Account_Invoice_Processing_Config_Id		INT
 @Watch_List_Group_Info_Id					INT				 
 @User_Info_Id								INT
                  
 Output Parameters:                        
 Name                                        DataType        Default        Description                    
--------------------------------------------------------------------------------------------------                   
                  
 Usage Examples:                      
--------------------------------------------------------------------------------------------------


BEGIN TRAN 

SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id in (7,8)
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=7
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=8


	 EXEC dbo.Account_Invoice_Processing_Config_Watch_List_Upd
    @Account_Invoice_Processing_Config_Ids = '7,8'
    , @Watch_List_Group_Info_Id = 626  
    , @User_Info_Id = 49


SELECT * FROM [dbo].[Account_Invoice_Processing_Config] where Account_Invoice_Processing_Config_Id in (7,8)
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=7
SELECT * FROM [dbo].[Account_Invoice_Processing_Config_Log] where Account_Invoice_Processing_Config_Id=8

ROLLBACK TRAN

                 
 Author Initials:                  
  Initials        Name                  
--------------------------------------------------------------------------------------------------                   
  NR              Narayana Reddy    
                   
 Modifications:                  
  Initials        Date              Modification                  
--------------------------------------------------------------------------------------------------                   
  NR              2019-01-03		Created for Data2.0 - Watch List - B.                
                 
******/


CREATE PROCEDURE [dbo].[Account_Invoice_Processing_Config_Watch_List_Upd]
    (
        @Account_Invoice_Processing_Config_Ids VARCHAR(MAX)
        , @Watch_List_Group_Info_Id INT = NULL
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        CREATE TABLE #Account_Invoice_Processing_Config_Ids
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Invoice_Processing_Config_Id INT
             );



        CREATE TABLE #Account_Config_Watch_List_History
             (
                 Account_Invoice_Processing_Config_Id INT
                 , New_Watch_List_Group_Info_Id INT
                 , Old_Watch_List_Group_Info_Id INT
             );

        DECLARE
            @New_Watch_List_Group_Info_Id INT
            , @New_Group_Name VARCHAR(200)
            , @Old_Watch_List_Group_Info_Id INT
            , @Old_Group_Name VARCHAR(200)
            , @Change_Type NVARCHAR(255)
            , @Account_Invoice_Processing_Config_Id INT
            , @Counter INT = 1;


        INSERT INTO #Account_Invoice_Processing_Config_Ids
             (
                 Account_Invoice_Processing_Config_Id
             )
        SELECT
            us.Segments
        FROM
            dbo.ufn_split(@Account_Invoice_Processing_Config_Ids, ',') us;




        BEGIN TRY
            BEGIN TRAN;

            WHILE (@Counter <= (SELECT  COUNT(1)FROM    #Account_Invoice_Processing_Config_Ids))
                BEGIN

                    SELECT
                        @Account_Invoice_Processing_Config_Id = aipci.Account_Invoice_Processing_Config_Id
                    FROM
                        #Account_Invoice_Processing_Config_Ids aipci
                    WHERE
                        aipci.Id = @Counter;


                    UPDATE
                        aipc
                    SET
                        aipc.Watch_List_Group_Info_Id = @Watch_List_Group_Info_Id
                        , aipc.Updated_User_Id = @User_Info_Id
                        , aipc.Last_Change_Ts = GETDATE()
                    OUTPUT
                        @Account_Invoice_Processing_Config_Id
                        , Inserted.Watch_List_Group_Info_Id
                        , Deleted.Watch_List_Group_Info_Id
                    INTO #Account_Config_Watch_List_History (Account_Invoice_Processing_Config_Id
                                                             , New_Watch_List_Group_Info_Id
                                                             , Old_Watch_List_Group_Info_Id)
                    FROM
                        dbo.Account_Invoice_Processing_Config aipc
                    WHERE
                        aipc.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                        AND ISNULL(aipc.Watch_List_Group_Info_Id, -1) <> ISNULL(@Watch_List_Group_Info_Id, -1);



                    SELECT
                        @New_Watch_List_Group_Info_Id = acwlh.New_Watch_List_Group_Info_Id
                        , @Old_Watch_List_Group_Info_Id = acwlh.Old_Watch_List_Group_Info_Id
                    FROM
                        #Account_Config_Watch_List_History acwlh
                    WHERE
                        acwlh.Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id;


                    SELECT
                        @New_Group_Name = gi.GROUP_NAME
                    FROM
                        dbo.GROUP_INFO gi
                    WHERE
                        gi.GROUP_INFO_ID = @New_Watch_List_Group_Info_Id;

                    SELECT
                        @Old_Group_Name = gi.GROUP_NAME
                    FROM
                        dbo.GROUP_INFO gi
                    WHERE
                        gi.GROUP_INFO_ID = @Old_Watch_List_Group_Info_Id;



                    SET @Change_Type = CASE WHEN @New_Watch_List_Group_Info_Id IS NULL
                                                 AND @Old_Watch_List_Group_Info_Id IS NOT NULL THEN 'Removed'
                                           ELSE 'Edited'
                                       END;


                    IF (ISNULL(@New_Watch_List_Group_Info_Id, -1) <> ISNULL(@Old_Watch_List_Group_Info_Id, -1))
                        BEGIN

                            EXEC dbo.Account_Invoice_Processing_Config_Log_Ins
                                @Account_Invoice_Processing_Config_Id = @Account_Invoice_Processing_Config_Id
                                , @Field_Name = 'Watch List Group'
                                , @Change_Type = @Change_Type
                                , @Previous_Value = @Old_Group_Name
                                , @Current_Value = @New_Group_Name
                                , @Is_Updated_Using_Apply_All = 0
                                , @Event_By_User_Id = @User_Info_Id;



                        END;



                    SET @Counter = @Counter + 1;


                END;

            COMMIT TRAN;
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0
                ROLLBACK TRAN;

            EXEC dbo.usp_RethrowError;

        END CATCH;

        DROP TABLE #Account_Config_Watch_List_History;
        DROP TABLE #Account_Invoice_Processing_Config_Ids;

    END;






GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Processing_Config_Watch_List_Upd] TO [CBMSApplication]
GO
