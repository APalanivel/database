SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          

NAME: [DBO].[Report_DE_Credit_Balance]  
     
DESCRIPTION: 
	
      
INPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION          
------------------------------------------------------------          
@ACCOUNT_AUDIT_ID		INT				Utility Account
                
OUTPUT PARAMETERS:          
NAME			DATATYPE	DEFAULT		DESCRIPTION   
       
------------------------------------------------------------          
USAGE EXAMPLES:          
------------------------------------------------------------        
  
	EXEC Report_DE_Credit_Balance  235,'2012-01-01'

	EXEC Report_DE_Credit_Balance  '11231','2012-07-21'
     
AUTHOR INITIALS:          
INITIALS	NAME          
------------------------------------------------------------
AKR			Ashok Kumar Raju

MODIFICATIONS

INITIALS	DATE		MODIFICATION
------------------------------------------------------------          
AKR			2012-08-14	CREATED
*/

CREATE PROCEDURE dbo.Report_DE_Credit_Balance
      ( 
       @Client_Id VARCHAR(MAX)
      ,@Batch_Processed_Start_Date DATE )
AS 
BEGIN

      SET NOCOUNT ON ;
   
      DECLARE @Client_Id_List TABLE ( Client_Id INT )

      INSERT      INTO @Client_Id_List
                  ( 
                   Client_Id )
                  SELECT
                        us.Segments
                  FROM
                        dbo.ufn_split(@Client_Id, ',') us

      SELECT
            c.CLIENT_NAME
           ,ci.CU_INVOICE_ID
           ,ui.UBM_CLIENT_NAME
           ,ui.UBM_CLIENT_ID
           ,a.ACCOUNT_NUMBER
           ,v.VENDOR_NAME
           ,ubml.START_DATE
           ,cism.SERVICE_MONTH
           ,ui.PREVIOUS_BALANCE
           ,ui.AMOUNT_DUE
      FROM
            dbo.ubm_invoice ui
            INNER JOIN dbo.cu_INVOICE ci
                  ON ci.UBM_INVOICE_id = ui.UBM_INVOICE_ID
            INNER JOIN @Client_Id_List cil
                  ON cil.Client_Id = ci.CLIENT_ID
            INNER JOIN client c
                  ON ci.CLIENT_ID = c.CLIENT_ID
            INNER JOIN dbo.UBM_BATCH_MASTER_LOG ubml
                  ON ui.UBM_BATCH_MASTER_LOG_ID = ubml.UBM_BATCH_MASTER_LOG_ID
            LEFT JOIN dbo.CU_INVOICE_SERVICE_MONTH cism
                  ON cism.CU_INVOICE_ID = ci.CU_INVOICE_ID
            INNER JOIN dbo.ACCOUNT a
                  ON a.ACCOUNT_ID = cism.Account_ID
            INNER JOIN dbo.VENDOR v
                  ON v.VENDOR_ID = a.VENDOR_ID
            INNER JOIN dbo.vwAccountMeter vw
                  ON vw.account_id = cism.Account_ID
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = vw.SITE_ID
            LEFT JOIN dbo.UBM_CASS_VENDOR ucv
                  ON ucv.VENDOR_name = v.VENDOR_name
      WHERE
            ui.PREVIOUS_BALANCE < 0
            AND ubml.START_DATE >= @Batch_Processed_Start_Date
            AND ui.AMOUNT_DUE <= 0
      GROUP BY
            c.CLIENT_NAME
           ,ci.CU_INVOICE_ID
           ,ui.UBM_CLIENT_NAME
           ,ui.UBM_CLIENT_ID
           ,a.ACCOUNT_NUMBER
           ,v.VENDOR_NAME
           ,ubml.START_DATE
           ,cism.SERVICE_MONTH
           ,ui.PREVIOUS_BALANCE
           ,ui.AMOUNT_DUE
      ORDER BY
            ui.AMOUNT_DUE


END
;

GO
GRANT EXECUTE ON  [dbo].[Report_DE_Credit_Balance] TO [CBMS_SSRS_Reports]
GRANT EXECUTE ON  [dbo].[Report_DE_Credit_Balance] TO [CBMSApplication]
GO
