
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:  Report_DE_Cost_Usage_Sel_By_Client_Commodity_Rolloing_Months       
    
        
DESCRIPTION:              
 Gets a list of missing invoices based on invoice participation     
          
INPUT PARAMETERS:              
Name				DataType Default  Description              
----------------------------------------------------------              
@Client_Name		VARCHAR(MAX)
@Rolling_Months		INT
@Currency_Unit_Id	INT
@Commodity_Id_List	VARCHAR(MAX)
@Site_Not_Managed   BIT
                    
OUTPUT PARAMETERS:              
Name   DataType Default  Description              
----------------------------------------------------------     
    
             
USAGE EXAMPLES:              
----------------------------------------------------------     
    
  
EXEC Report_DE_Cost_Usage_Sel_By_Client_Commodity_Rolloing_Months 'Kraft Foods Group, Inc. (fka Kraft Foods Global, Inc.)',36,'USD','Electric Power|Natural Gas',1,1   
    

    
AUTHOR INITIALS:              
Initials	Name              
----------------------------------------------------------              
RKV			Ravi Kumar vegesna
         
MODIFICATIONS               
Initials	Date		Modification              
----------------------------------------------------------              
RKV			2016-08-19	Created  
HG			2016-10-07	Added Site_Status to the results
*****/
CREATE PROCEDURE [dbo].[Report_DE_Cost_Usage_Sel_By_Client_Commodity_Rolloing_Months]
      (
       @Client_Name VARCHAR(MAX)
      ,@Rolling_Months INT
      ,@Currency_Unit_Name VARCHAR(200) = 'USD'
      ,@Commodity_Name VARCHAR(MAX)
      ,@Site_Not_Managed BIT = NULL
      ,@Include_Current_Month BIT = 0 )
AS
BEGIN

      DECLARE
            @Start_Date DATE
           ,@End_Date DATE
           ,@Client_Currency_Group_Id INT
           ,@Currency_Unit_Id INT;

      CREATE TABLE #Client_Site_Client_Hier_Ids ( Client_Hier_Id INT );
      
      CREATE TABLE #Bucket_Values
            (
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Commodity_id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_value DECIMAL(16, 2)
            ,Total_Usage_Uom VARCHAR(200) );
      
      CREATE TABLE #Billing_Days
            (
             Client_Hier_Id INT
            ,Service_Month DATE
            ,Commodity_id INT
            ,No_Of_Days INT );
      
      DECLARE @Commodity_Id_List TABLE ( Commodity_Id INT );
      
      INSERT      INTO @Commodity_Id_List
                  (Commodity_Id )
                  SELECT
                        com.Commodity_Id
                  FROM
                        dbo.ufn_split(@Commodity_Name, '|') cnl
                        INNER JOIN dbo.Commodity com
                              ON com.Commodity_Name = cnl.Segments
                  WHERE
                        @Commodity_Name IS NOT NULL
                  GROUP BY
                        com.Commodity_Id;
                  
      INSERT      INTO @Commodity_Id_List
                  (Commodity_Id )
                  SELECT
                        com.Commodity_Id
                  FROM
                        dbo.Commodity com
                  WHERE
                        @Commodity_Name IS NULL
                  GROUP BY
                        com.Commodity_Id;

      SELECT
            @Currency_Unit_Id = cu.CURRENCY_UNIT_ID
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_NAME = @Currency_Unit_Name;
      
      SELECT
            @End_Date = CASE WHEN @Include_Current_Month = 0 THEN DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, GETDATE()), 0))
                             ELSE DATEADD(s, -1, DATEADD(mm, DATEDIFF(m, 0, GETDATE()) + 1, 0))
                        END; 
      
      SELECT
            @Start_Date = DATEADD(MONTH, DATEDIFF(MONTH, 0, DATEADD(mm, -@Rolling_Months, @End_Date)), 0);

      SELECT
            @Client_Currency_Group_Id = ch.Client_Currency_Group_Id
      FROM
            Core.Client_Hier ch
      WHERE
            Client_Name = @Client_Name
            AND Site_Id = 0
            AND Sitegroup_Id = 0;  

      INSERT      INTO #Client_Site_Client_Hier_Ids
                  (Client_Hier_Id )
                  SELECT
                        Client_Hier_Id
                  FROM
                        Core.Client_Hier ch
                  WHERE
                        ch.Client_Name = @Client_Name
                        AND ch.Site_Id > 0
                  GROUP BY
                        ch.Client_Hier_Id;
      
      INSERT      INTO #Billing_Days
                  (Client_Hier_Id
                  ,Service_Month
                  ,Commodity_id
                  ,No_Of_Days )
                  SELECT
                        rpt.Client_Hier_Id
                       ,rpt.Service_Month
                       ,rpt.Commodity_Id
                       ,CAST(ROUND(CAST(SUM(rpt.Billing_Days) AS DECIMAL(16, 2)) / CAST(COUNT(rpt.Account_Id) AS DECIMAL(16, 2)), 0) AS INT)
                  FROM
                        ( SELECT
                              cha.Account_Id
                             ,ch.Client_Hier_Id
                             ,Service_Month
                             ,Billing_Days
                             ,cha.Commodity_Id
                          FROM
                              Cost_Usage_Account_Billing_Dtl cabd
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON cabd.Account_Id = cha.Account_Id
                              INNER JOIN #Client_Site_Client_Hier_Ids ch
                                    ON cha.Client_Hier_Id = ch.Client_Hier_Id
                          WHERE
                              cabd.Service_Month BETWEEN @Start_Date
                                                 AND     @End_Date
                          GROUP BY
                              cha.Account_Id
                             ,ch.Client_Hier_Id
                             ,Service_Month
                             ,Billing_Days
                             ,cha.Commodity_Id ) rpt
                  GROUP BY
                        rpt.Client_Hier_Id
                       ,rpt.Service_Month
                       ,rpt.Commodity_Id;

      INSERT      INTO #Bucket_Values
                  ( Client_Hier_Id
                  ,Service_Month
                  ,Commodity_id
                  ,Bucket_Type
                  ,Bucket_value
                  ,Total_Usage_Uom )
                  SELECT
                        cschi.Client_Hier_Id
                       ,cusd.Service_Month
                       ,com.Commodity_Id
                       ,c.Code_Value Bucket_Type
                       ,SUM(CASE WHEN c.Code_Value = 'Determinant' THEN cusd.Bucket_Value * ccon.CONVERSION_FACTOR
                                 WHEN c.Code_Value = 'Charge' THEN cusd.Bucket_Value * cuc.CONVERSION_FACTOR
                            END) Bucket_Value
                       ,e.ENTITY_NAME
                  FROM
                        #Client_Site_Client_Hier_Ids cschi
                        INNER JOIN dbo.Cost_Usage_Site_Dtl cusd
                              ON cusd.Client_Hier_Id = cschi.Client_Hier_Id
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
                        INNER JOIN @Commodity_Id_List com
                              ON com.Commodity_Id = bm.Commodity_Id
                        INNER JOIN dbo.Code c
                              ON bm.Bucket_Type_Cd = c.Code_Id
                        LEFT OUTER JOIN dbo.CONSUMPTION_UNIT_CONVERSION ccon
                              ON ccon.BASE_UNIT_ID = cusd.UOM_Type_Id
                                 AND ccon.CONVERTED_UNIT_ID = bm.Default_Uom_Type_Id
                        LEFT OUTER JOIN dbo.ENTITY e
                              ON e.ENTITY_ID = bm.Default_Uom_Type_Id
                        LEFT OUTER JOIN dbo.CURRENCY_UNIT_CONVERSION cuc
                              ON cuc.CURRENCY_GROUP_ID = @Client_Currency_Group_Id
                                 AND cuc.BASE_UNIT_ID = cusd.CURRENCY_UNIT_ID
                                 AND cuc.CONVERTED_UNIT_ID = @Currency_Unit_Id
                                 AND cuc.CONVERSION_DATE = cusd.Service_Month
                  WHERE
                        cusd.Service_Month BETWEEN @Start_Date
                                           AND     @End_Date
                        AND bm.Bucket_Name IN ( 'Total Cost', 'Total Usage' )
                  GROUP BY
                        cschi.Client_Hier_Id
                       ,cusd.Service_Month
                       ,com.Commodity_Id
                       ,c.Code_Value
                       ,cusd.Bucket_Value
                       ,e.ENTITY_NAME;

      SELECT
            ch.Site_Reference_Number
           ,ch.Site_name
           ,CASE WHEN ch.Site_Not_Managed = 0 THEN 'Active'
                 ELSE 'Inactive'
            END AS Site_Status
           ,ch.Site_Address_Line1 AS [Address]
           ,ch.City
           ,ch.ZipCode
           ,ch.State_Name
           ,ch.Country_Name
           ,c.Commodity_Name
           ,bv.Service_Month
           ,MAX(CASE WHEN bv.Bucket_Type = 'Determinant' THEN bv.Bucket_value
                END) Total_usage
           ,MAX(CASE WHEN bv.Bucket_Type = 'Charge' THEN bv.Bucket_value
                END) Total_Cost
           ,CAST(MAX(CASE WHEN bv.Bucket_Type = 'Charge' THEN bv.Bucket_value
                     END) / NULLIF(MAX(CASE WHEN bv.Bucket_Type = 'Determinant' THEN bv.Bucket_value
                                       END), 0) AS DECIMAL(16, 2)) Unit_Cost
           ,MAX(CASE WHEN bv.Bucket_Type = 'Determinant' THEN bv.Total_Usage_Uom
                END) Total_usage_Uom
           ,bd.No_Of_Days
      FROM
            #Bucket_Values bv
            INNER JOIN Core.Client_Hier ch
                  ON bv.Client_Hier_Id = ch.Client_Hier_Id
            INNER JOIN dbo.Commodity c
                  ON bv.Commodity_id = c.Commodity_Id
            INNER JOIN #Billing_Days bd
                  ON bd.Client_Hier_Id = bv.Client_Hier_Id
                     AND bd.Commodity_id = bv.Commodity_id
                     AND bd.Service_Month = bv.Service_Month
      WHERE
            ( @Site_Not_Managed IS NULL
              OR ch.Site_Not_Managed = @Site_Not_Managed )
      GROUP BY
            ch.Site_Reference_Number
           ,ch.Site_name
           ,CASE WHEN ch.Site_Not_Managed = 0 THEN 'Active'
                 ELSE 'Inactive'
            END
           ,ch.Site_Address_Line1
           ,ch.City
           ,ch.ZipCode
           ,ch.State_Name
           ,ch.Country_Name
           ,c.Commodity_Name
           ,bv.Service_Month
           ,bd.No_Of_Days
      ORDER BY
            bv.Service_Month
           ,ch.Site_name
           ,c.Commodity_Name;


      DROP TABLE #Client_Site_Client_Hier_Ids;
      DROP TABLE #Bucket_Values; 
      DROP TABLE #Billing_Days;        

END;
;
GO

GRANT EXECUTE ON  [dbo].[Report_DE_Cost_Usage_Sel_By_Client_Commodity_Rolloing_Months] TO [CBMSApplication]
GO
