SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********                                   
                            
NAME:                            
     dbo.Sitegroup_Site_Conversion_Sitegroup_Ins                          
                                   
DESCRIPTION:                                      
     inserts data to Sitegroup table from Dtl table                          
                                  
 INPUT PARAMETERS:                                      
                                                 
 Name                        DataType         Default       Description                                    
---------------------------------------------------------------------------------------------------------------                                  
 @Data_Conversion_Batch_Id   INT                            
                                      
 OUTPUT PARAMETERS:                                      
                                                       
 Name                        DataType         Default       Description                                    
---------------------------------------------------------------------------------------------------------------                                  
                                      
 USAGE EXAMPLES:                                                        
---------------------------------------------------------------------------------------------------------------                                                        
                             
 BEGIN TRAN                            
 EXEC Sitegroup_Site_Conversion_Sitegroup_Site_Ins                        
      @Data_Conversion_Batch_Id = 30                           
 ROLLBACK TRAN                               
                              
                                  
 AUTHOR INITIALS:                                    
                                   
 Initials              Name                                    
---------------------------------------------------------------------------------------------------------------                                                  
 AKR                   Ashok Kumar Raju                                    
                            
                              
 MODIFICATIONS:                                  
                                      
 Initials              Date             Modification                                  
---------------------------------------------------------------------------------------------------------------                                  
 AKR                   2014-09-04      Created.                                     
*********/                            
                            
CREATE PROCEDURE [dbo].[Sitegroup_Site_Conversion_Sitegroup_Site_Ins]  
      (   
       @Data_Conversion_Batch_Id INT )  
AS   
BEGIN           
        
        
      DECLARE @Sitegroup_User TABLE  
            (   
             Sitegroup_Name VARCHAR(200)  
            ,Add_User VARCHAR(50)  
            ,Sitegroup_Type VARCHAR(50)  
            ,Is_Sg_Exists BIT DEFAULT ( 0 ) )        
    
    
      DECLARE @Client_Name VARCHAR(200)        
          
          
      SELECT TOP 1  
            @Client_Name = Client_Name  
      FROM  
            ETL.Data_Conversion_Sitegroup_Site_Stage  
      WHERE  
            Client_Name IS NOT NULL        
                                
                                
      INSERT      INTO @Sitegroup_User  
                  (   
                   Sitegroup_Name  
                  ,Add_User  
                  ,Sitegroup_Type )  
                  SELECT  
                        Sitegroup_Name  
                       ,Add_User  
                       ,Sitegroup_Type  
                  FROM  
                        ETL.Data_Conversion_Sitegroup_Site_Stage  
                  WHERE  
                        Add_User IS NOT NULL  
                        AND Sitegroup_Name IS NOT NULL  
                        AND Sitegroup_Type IS NOT NULL  
                  GROUP BY  
                        Sitegroup_Name  
                       ,Add_User  
                       ,Sitegroup_Type        
                                                       
          
      UPDATE  
            su  
      SET     
            Is_Sg_Exists = 1  
      FROM  
            @Sitegroup_User su  
            INNER JOIN dbo.Sitegroup sg  
                  ON su.Sitegroup_Name = sg.Sitegroup_Name  
            INNER JOIN dbo.client cl  
                  ON sg.CLient_Id = cl.Client_Id  
            INNER JOIN dbo.Code c  
                  ON sg.Sitegroup_Type_Cd = c.Code_Id  
            INNER JOIN dbo.USER_INFO ui  
                  ON ui.USERNAME = su.Add_User  
      WHERE  
            ( ( su.Sitegroup_Type = 'Global'  
                AND c.Code_Value = 'Division' )  
              OR ( su.Sitegroup_Type = 'Global'  
                   AND c.Code_Value = 'Global' )  
              OR ( su.Sitegroup_Type = 'User'  
                   AND c.Code_Value = 'User'  
                   AND ui.UserName = su.Add_User ) )  
            AND cl.client_Name = @Client_Name            
          
            
      INSERT      INTO dbo.Sitegroup  
                  (   
                   Sitegroup_Name  
                  ,Sitegroup_Type_Cd  
                  ,Client_Id  
                  ,Is_Smart_Group  
                  ,Is_Publish_To_DV  
                  ,Owner_User_Id  
                  ,Add_User_Id  
                  ,Add_Dt  
                  ,Mod_User_Id  
                  ,Mod_Dt  
                  ,Is_Complete  
                  ,Is_Locked  
                  ,Is_Edited  
                  ,Last_Smartgroup_Refresh_dt )  
                  SELECT  
                        dcb.Sitegroup_Name  
                       ,c.Code_Id  
                       ,cl.CLIENT_ID  
                       ,0  
                       ,1  
                       ,USER_INFO_ID  
                       ,USER_INFO_ID  
                       ,GETDATE()  
                       ,NULL  
                       ,GETDATE()  
                       ,0  
                       ,0  
                       ,0  
                       ,'1900-01-01'  
                  FROM  
                        ETL.Data_Conversion_Batch_Dtl dcb  
                        INNER JOIN dbo.Code c  
                              ON c.Code_Value = dcb.Sitegroup_Type  
                        INNER JOIN dbo.Codeset cs  
                              ON c.Codeset_Id = cs.Codeset_Id  
                                 AND cs.Codeset_Name = 'SiteGroup_Type'  
                        INNER JOIN dbo.CLIENT cl  
                              ON cl.CLIENT_NAME = dcb.Client_Name  
                        INNER JOIN dbo.USER_INFO ui  
                              ON ui.USERNAME = dcb.Add_User  
                        INNER JOIN @Sitegroup_User sgu  
                              ON dcb.Sitegroup_Name = sgu.Sitegroup_Name  
                  WHERE  
                        dcb.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                        AND Is_Data_Validation_Passed = 1  
                        AND Is_Sg_Exists = 0  
                  GROUP BY  
                        dcb.Sitegroup_Name  
                       ,c.Code_Id  
                       ,cl.CLIENT_ID  
                       ,USER_INFO_ID                 
                               
                               
                 
      INSERT      INTO dbo.Sitegroup_Site  
                  (   
                   Sitegroup_id  
                  ,Site_id )  
                  SELECT  
                        sg.Sitegroup_Id  
                       ,s.SITE_ID  
                  FROM  
                        ETL.Data_Conversion_Batch_Dtl dcbd  
                        INNER JOIN dbo.Sitegroup sg  
                              ON dcbd.Sitegroup_Name = sg.Sitegroup_Name  
                        INNER JOIN dbo.CLIENT cl  
                              ON sg.Client_Id = cl.CLIENT_ID  
                                 AND dcbd.Client_Name = cl.CLIENT_NAME  
          INNER JOIN dbo.USER_INFO ui  
                              ON ui.USER_INFO_ID = sg.Add_User_Id  
                        INNER JOIN dbo.SITE s  
                              ON cl.CLIENT_ID = s.Client_ID  
                                 AND dcbd.Site_Name = s.SITE_NAME  
                  WHERE  
                        dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                        AND dcbd.Is_Data_Validation_Passed = 1  
                        AND NOT EXISTS ( SELECT  
                                          1  
                                         FROM  
                                          dbo.Sitegroup_Site ss  
                                         WHERE  
                                          sg.Sitegroup_Id = ss.Sitegroup_id  
                                          AND ss.Site_id = s.SITE_ID )  
                  GROUP BY  
                        sg.Sitegroup_Id  
                       ,s.SITE_ID        
                         
      UPDATE  
            sg  
      SET     
            Is_Complete = 1  
      FROM  
            ( SELECT  
                  dcbd.Client_Name  
                 ,sgu.Sitegroup_Name  
              FROM  
                  ETL.Data_Conversion_Batch_Dtl dcbd  
                  INNER JOIN @Sitegroup_User sgu  
                        ON dcbd.Sitegroup_Name = sgu.Sitegroup_Name  
              WHERE  
                  dcbd.Data_Conversion_Batch_Id = @Data_Conversion_Batch_Id  
                  AND dcbd.Is_Data_Validation_Passed = 1  
                  AND Is_Sg_Exists = 0  
              GROUP BY  
                  dcbd.Client_Name  
                 ,sgu.Sitegroup_Name ) k  
            INNER JOIN dbo.Sitegroup sg  
                  ON k.Sitegroup_Name = sg.Sitegroup_Name  
            INNER JOIN dbo.CLIENT cl  
                  ON sg.Client_Id = cl.CLIENT_ID  
                     AND k.Client_Name = cl.CLIENT_NAME        
                             
        
END
;
GO
GRANT EXECUTE ON  [dbo].[Sitegroup_Site_Conversion_Sitegroup_Site_Ins] TO [ETL_Execute]
GO
