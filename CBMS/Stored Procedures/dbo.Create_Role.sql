
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******************************************************************************************************    
NAME :	dbo.Create_Role 
   
DESCRIPTION: 

	This procedure used to Insert record into SECURITY_ROLE and USER_SECURITY_ROLE_QUEUE  table 
   
 INPUT PARAMETERS:    
 Name             DataType               Default        Description    
--------------------------------------------------------------------      
 @ClientId       INT                                User ID 
 @divisionList	integer_list_tbltype				Comma delimited list of site IDs
 @siteList		integer_list_tbltype				Comma delimited list of division IDs
   
 OUTPUT PARAMETERS:    
 Name   DataType  Default Description    
--------------------------------------------------------------------
@SecurityRoleID INT 

  USAGE EXAMPLES:    
-------------------------------------------------------------------- 
DECLARE @divisionList Division_Ids
INSERT INTO @divisionList VALUES (12208672),(1913)
DECLARE @siteList Site_Ids
INSERT INTO @siteList VALUES (24258),(24268),(24269)
EXEC dbo.Create_Role  11231,@divisionList,@siteList

  
AUTHOR INITIALS:    
 Initials	Name    
-------------------------------------------------------------------    
  KVK		K VINAY KUMAR 
  HG		Harihara Suthan G
  RR		Raghu Reddy
 
 MODIFICATIONS     
 Initials	Date		Modification    
--------------------------------------------------------------------  
 KVK					split the camma separated value by using "ufn_Split"
 HG			4/1/2011	Script modified to remove Role_Name column from Security_Role table as it is not used by application anywhere.
 RR			08/26/2011	MAINT-758 Replaced the comma seperated input param for site list and division list into tvp 

******/


CREATE PROCEDURE [dbo].[Create_Role]
      ( 
       @clientId INT
      ,@divisionIDList Division_Ids READONLY
      ,@siteIDList Site_Ids READONLY )
AS 
BEGIN

      SET NOCOUNT ON

   
		-- Create temp table to store all SiteID's
      CREATE TABLE #totalSites ( Site_Id INT )
						
      DECLARE @SecurityRoleID INT
      DECLARE @DivLevelCode INT
	
      BEGIN TRY

           

		-- insert corresponding Division siteid's into #totalsites table
            INSERT      INTO #totalSites
                        ( 
                         Site_Id )
                        SELECT
                              site_id
                        FROM
                              dbo.SITE s
                        WHERE
                              EXISTS ( SELECT
                                          1
                                       FROM
                                          @divisionIDList dl
                                       WHERE
                                          dl.Division_Id = s.DIVISION_ID ) 

           
					
		-- insert siteid's into #totalsites table			
            INSERT      INTO #totalSites
                        SELECT
                              sl.Site_Id
                        FROM
                              @siteIDList sl
                        WHERE
                              NOT EXISTS ( SELECT
                                                1
                                           FROM
                                                #totalSites ts
                                           WHERE
                                                ts.Site_Id = sl.Site_Id )
                              
				
            INSERT      INTO dbo.Security_Role
                        ( Client_Id, Is_corporate )
            VALUES
                        ( @ClientId, 0 )
					
            SELECT
                  @SecurityRoleID = scope_identity()
			
		--RAO Comments:
		
		--"Change INSERT INTO SECURITY_ROLE_CLIENT_HIER  statement to make sure we are picking up only division IDs not Site Group IDs. 
		--Although  Divisions and Site groups are conceptually same, they are different in fact. They both will have entries in Client_Hier 
		--table and they both will have Site_id as zero. Theoritically it is possible (probablity is low) for a sitegroup id and division id to 
		--be same. When that happens, we may insert client_hier_id of sitegorup into SECURITY_ROLE_CLIENT_HIER table instead of that of division.
		--So, change it to check Hier_Level_Cd instead of site_id = 0 to ensure that we are picking up divisions.
			
            SELECT
                  @DivLevelCode = cd.code_id
            FROM
                  dbo.Code cd
                  INNER JOIN dbo.Codeset cs
                        ON cs.Codeset_Id = cd.Codeset_Id
            WHERE
                  cs.Codeset_Name = 'HierLevel'
                  AND cd.Code_Value = 'Division'	
		
            INSERT      INTO dbo.SECURITY_ROLE_CLIENT_HIER
                        ( 
                         Security_Role_Id
                        ,Client_Hier_Id )
                        SELECT
                              @SecurityRoleID
                             ,ch.Client_Hier_Id
                        FROM
                              Core.Client_Hier ch
                        WHERE
                              ch.Client_Id = @ClientId
                              AND ( EXISTS ( SELECT
                                                1
                                             FROM
                                                #totalSites ts
                                             WHERE
                                                ts.Site_id = ch.Site_id )
                                    OR ( EXISTS ( SELECT
                                                      1
                                                  FROM
                                                      @divisionIDList dl
                                                  WHERE
                                                      dl.Division_Id = ch.Sitegroup_Id )
                                         AND ch.Hier_Level_Cd = @DivLevelCode ) )
        
				    
			
            SELECT
                  @SecurityRoleID AS Security_Role_ID

            DROP TABLE #totalSites

      END TRY
	
      BEGIN CATCH
		
            EXEC usp_RethrowError
	
      END CATCH
	
END

;
GO

GRANT EXECUTE ON  [dbo].[Create_Role] TO [CBMSApplication]
GO
