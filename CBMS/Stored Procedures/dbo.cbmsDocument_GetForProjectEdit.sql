SET NUMERIC_ROUNDABORT OFF
GO

SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*******************************************************************************

NAME:    dbo.cbmsDocument_GetForProjectEdit  
  
DESCRIPTION:
  
INPUT PARAMETERS:  
Name			DataType  Default Description  
------------------------------------------------------------  
@sso_project_id INT                     
  
OUTPUT PARAMETERS:  
Name   DataType  Default Description  
------------------------------------------------------------  

USAGE EXAMPLES:  
------------------------------------------------------------  
	EXEC dbo.cbmsDocument_GetForProjectEdit 17000
	EXEC dbo.cbmsDocument_GetForProjectEdit 16078  
	EXEC dbo.cbmsDocument_GetForProjectEdit 13461  
	EXEC dbo.cbmsDocument_GetForProjectEdit 21497


AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------
 PNR		Pandarinath
 DRG		Dhilu Raichal George


MODIFICATIONS  
 Initials	Date				Modification  
------------------------------------------------------------  
 PNR		03/28/2011			Replaced vwCbmsSsoProjectOwnerFlat,vwCbmsSsoDocumentOwnerFlat views 
								with SSO_PROJECT_OWNER_MAP,SSO_DOCUMENT_OWNER_MAP tables.
 DRG		10-SEP-2014			modified to get category from new table 
*********************************************************************************************/

CREATE PROCEDURE [dbo].[cbmsDocument_GetForProjectEdit] 
	( 
		@sso_project_id INT 
	)
AS 
BEGIN
    
      SET NOCOUNT ON ;
    
      SELECT
            is_checked = case WHEN map.sso_document_id IS NULL THEN 0
                              ELSE 1
                         END
           ,sd.sso_document_id
           ,left(cat.category, len(cat.category) - 1) category_type
           ,sd.document_title
           ,sd.document_reference_date
           ,sd.last_updated_date
           ,i.cbms_image_size
           ,i.CBMS_DOC_ID
      FROM
            dbo.SSO_PROJECT_OWNER_MAP x
            INNER JOIN dbo.SSO_DOCUMENT_OWNER_MAP do
                  ON do.client_hier_id = x.client_hier_id
            INNER JOIN dbo.sso_document sd
                  ON sd.sso_document_id = do.sso_document_id
            CROSS APPLY ( SELECT
                              cdc.Category_Name + ','
                          FROM
                              dbo.Client_Document_Category cdc
                              INNER  JOIN dbo.Client_Document_Category_map cdcm
                                    ON cdcm.Client_Document_Category_Id = cdc.Client_Document_Category_Id
                          WHERE
                              sd.SSO_Document_ID = cdcm.SSO_Document_ID
            FOR
                          XML PATH('') ) cat ( category )
            INNER JOIN dbo.cbms_image i
                  ON i.cbms_image_id = sd.cbms_image_id
            LEFT OUTER JOIN sso_project_document_map map
                  ON map.sso_project_id = x.sso_project_id
                     AND map.sso_document_id = sd.sso_document_id
      WHERE
            x.sso_project_id = @sso_project_id
      GROUP BY
            map.sso_document_id
           ,sd.sso_document_id
           ,cat.category
           ,sd.document_title
           ,sd.document_reference_date
           ,sd.last_updated_date
           ,i.cbms_image_size
           ,i.CBMS_DOC_ID  
END




;
GO

GRANT EXECUTE ON  [dbo].[cbmsDocument_GetForProjectEdit] TO [CBMSApplication]
GO
GO