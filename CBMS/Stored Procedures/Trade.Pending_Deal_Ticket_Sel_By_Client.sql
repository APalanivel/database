SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    Trade.Pending_Deal_Ticket_Sel_By_Client
   
    
DESCRIPTION:   
   
  
    
INPUT PARAMETERS:    
	Name          DataType       Default        Description    
-----------------------------------------------------------------------------    
	@Client_Id		INT
	
  
    
OUTPUT PARAMETERS:  
    
 Name     DataType   Default  Description    
-----------------------------------------------------------------------------    
    
    
    
USAGE EXAMPLES:    
-----------------------------------------------------------------------------    
	
	Exec Trade.Pending_Deal_Ticket_Sel_By_Client 11236
	Exec Trade.Pending_Deal_Ticket_Sel_By_Client 10081
	Exec Trade.Pending_Deal_Ticket_Sel_By_Client 11429,1,5
	Exec Trade.Pending_Deal_Ticket_Sel_By_Client 10003--,1,5
	
       
AUTHOR INITIALS:     
	Initials    Name
-----------------------------------------------------------------------------       
	RR          Raghu Reddy
    
MODIFICATIONS     
	Initials    Date        Modification      
-----------------------------------------------------------------------------       
	RR          2019-01-25	GRM Proejct.
     
    
******/

CREATE PROC [Trade].[Pending_Deal_Ticket_Sel_By_Client]
      ( 
       @Client_Id INT
      ,@Start_Index INT = 1
      ,@End_Index INT = 2147483647 )
AS 
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @Tbl_Dt_Dtls AS TABLE
            ( 
             Deal_Ticket_Id INT
            ,Deal_Ticket_Number NVARCHAR(255)
            ,Sites VARCHAR(600)
            ,Deal_Type VARCHAR(50)
            --,Volume DECIMAL(28, 3)
            ,Market_Price DECIMAL(28, 6)
            ,Price DECIMAL(28, 6)
            ,Date_Initiated VARCHAR(50)
            ,START_DATE VARCHAR(50)
            ,End_Date VARCHAR(50)
            ,STATUS NVARCHAR(255)
            ,Risk_Manager VARCHAR(50)
            ,Row_Num INT
            ,Client_Id INT
            ,Sitegroup_Id INT
            ,Site_Id INT )
            
      DECLARE @Total INT
      
      INSERT      INTO @Tbl_Dt_Dtls
                  ( 
                   Deal_Ticket_Id
                  ,Deal_Ticket_Number
                  ,Sites
                  ,Deal_Type
                  --,Volume
                  ,Market_Price
                  ,Price
                  ,Date_Initiated
                  ,START_DATE
                  ,End_Date
                  ,Status
                  ,Risk_Manager
                  ,Row_Num
                  ,Client_Id
                  ,Sitegroup_Id
                  ,Site_Id )
                  SELECT
                        dt.Deal_Ticket_Id
                       ,dt.Deal_Ticket_Number
                       ,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN 'Multiple'
                             ELSE MAX(RTRIM(ch.City) + ', ' + ch.State_Name + ' (' + ch.Site_name + ')')
                        END AS Sites
                       ,ttype.Code_Value AS Deal_Type
                       --,CAST(SUM(dtvol.Total_Volume) AS DECIMAL(28, 3)) AS Volume
                       ,CAST(NULL AS DECIMAL(28, 6)) AS Market_Price
                       ,CAST(NULL AS DECIMAL(28, 6)) AS Price
                       ,REPLACE(CONVERT(VARCHAR(15), dt.Created_Ts, 106), ' ', '-') AS Date_Initiated
                       ,SUBSTRING(DATENAME(month, dt.Hedge_Start_Dt), 1, 3) + '-' + CAST(DATEPART(year, dt.Hedge_Start_Dt) AS VARCHAR(5)) AS START_DATE
                       ,SUBSTRING(DATENAME(month, dt.Hedge_End_Dt), 1, 3) + '-' + CAST(DATEPART(year, dt.Hedge_End_Dt) AS VARCHAR(5)) AS End_Date
                       ,CASE WHEN COUNT(DISTINCT ws.Workflow_Status_Id) > 1 THEN 'Multiple'
                             ELSE MAX(ws.Workflow_Status_Name)
                        END AS STATUS
                       ,CAST(NULL AS VARCHAR) AS Risk_Manager
                       ,ROW_NUMBER() OVER ( ORDER BY dt.Deal_Ticket_Id DESC )
                       ,ch.Client_Id
                       ,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                             ELSE MAX(ch.Sitegroup_Id)
                        END AS Sitegroup_Id
                       ,CASE WHEN COUNT(DISTINCT ch.Site_Id) > 1 THEN -1
                             ELSE MAX(ch.Site_Id)
                        END AS Site_Id
                  FROM
                        Trade.Deal_Ticket dt
                        INNER JOIN Trade.Deal_Ticket_Client_Hier dtch
                              ON dtch.Deal_Ticket_Id = dt.Deal_Ticket_Id
                        INNER JOIN Core.Client_Hier ch
                              ON ch.Client_Id = dt.Client_Id
                                 AND ch.Client_Hier_Id = dtch.Client_Hier_Id
                        INNER JOIN dbo.Commodity c
                              ON c.Commodity_Id = dt.Commodity_Id
                        INNER JOIN dbo.Workflow w
                              ON w.Workflow_Id = dt.Workflow_Id
                        INNER JOIN dbo.USER_INFO ui
                              ON ui.USER_INFO_ID = dt.Created_User_Id
                        INNER JOIN dbo.Code ttype
                              ON dt.Deal_Ticket_Type_Cd = ttype.Code_Id
                        INNER JOIN dbo.Code atype
                              ON dt.Trade_Action_Type_Cd = atype.Code_Id
                        INNER JOIN dbo.ENTITY ht
                              ON dt.Hedge_Type_Cd = ht.ENTITY_ID
                        INNER JOIN Trade.Deal_Ticket_Client_Hier_Workflow_Status chws
                              ON dtch.Deal_Ticket_Client_Hier_Id = chws.Deal_Ticket_Client_Hier_Id
                        INNER JOIN Trade.Workflow_Status_Map wsm
                              ON chws.Workflow_Status_Map_Id = wsm.Workflow_Status_Map_Id
                        INNER JOIN Trade.Workflow_Status ws
                              ON wsm.Workflow_Status_Id = ws.Workflow_Status_Id
                        LEFT JOIN dbo.USER_INFO cui
                              ON cui.QUEUE_ID = dt.QUEUE_ID
                  WHERE
                        dt.Client_Id = @Client_Id
                        AND chws.Is_Active = 1
                        AND ws.Workflow_Status_Name NOT	IN ( 'Closed', 'Expired', 'Canceled', 'Completed' )
                  GROUP BY
                        dt.Deal_Ticket_Id
                       ,dt.Deal_Ticket_Number
                       ,dt.Created_Ts
                       ,ttype.Code_Value
                       ,dt.Hedge_Start_Dt
                       ,dt.Hedge_End_Dt
                       ,ch.Client_Id
                       
                  
           
      SELECT
            @Total = MAX(Row_Num)
      FROM
            @Tbl_Dt_Dtls
           
      SELECT
            dt.Deal_Ticket_Id
           ,dt.Deal_Ticket_Number
           ,dt.Sites
           ,dt.Deal_Type
           ,SUM(dtvol.Total_Volume) AS Volume
           ,dt.Market_Price
           ,dt.Price
           ,dt.Date_Initiated
           ,dt.START_DATE
           ,dt.End_Date
           ,dt.Status
           ,dt.Risk_Manager
           ,dt.Row_Num
           ,@Total AS Total
           ,dt.Client_Id
           ,dt.Sitegroup_Id
           ,dt.Site_Id
      FROM
            @Tbl_Dt_Dtls dt
            LEFT JOIN Trade.Deal_Ticket_Client_Hier_Volume_Dtl AS dtvol
                  ON dt.Deal_Ticket_Id = dtvol.Deal_Ticket_Id
      WHERE
            dt.Row_Num BETWEEN @Start_Index AND @End_Index
      GROUP BY
            dt.Deal_Ticket_Id
           ,dt.Deal_Ticket_Number
           ,dt.Sites
           ,dt.Deal_Type
           ,dt.Market_Price
           ,dt.Price
           ,dt.Date_Initiated
           ,dt.START_DATE
           ,dt.End_Date
           ,dt.Status
           ,dt.Risk_Manager
           ,dt.Row_Num
           ,dt.Client_Id
           ,dt.Sitegroup_Id
           ,dt.Site_Id
      ORDER BY
            dt.Row_Num 
END;







GO
GRANT EXECUTE ON  [Trade].[Pending_Deal_Ticket_Sel_By_Client] TO [CBMSApplication]
GO
