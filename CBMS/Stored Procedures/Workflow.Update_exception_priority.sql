SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
  
      
/******            
NAME:    [Workflow].[Update_exception_priority_New]         
DESCRIPTION: This stored procedure is created to update the priority of the invoice        
        
the inputs will come with comma delimited if multiple invoices which need to maek/remove priority        
------------------------------------------------------------           
 INPUT PARAMETERS:            
 Name   DataType  Default Description            
  @MODULE_ID INT,      
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,      
    @Logged_In_User INT,      
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3                
    @Exception_Type VARCHAR(MAX) = NULL,      
    @Account_Number VARCHAR(500) = NULL,      
    @Client VARCHAR(500) = NULL,      
    @Site VARCHAR(500) = NULL,      
    @Country VARCHAR(500) = NULL,      
    @State VARCHAR(500) = NULL,      
    @City VARCHAR(500) = NULL,      
    @Commodity VARCHAR(500) = NULL,      
    @Invoice_ID VARCHAR(500) = NULL,      
    @Priority INT = 0,                 --- Yes                
    @Exception_Status INT = NULL,      
    @Comments VARCHAR(500) = NULL,      
    @Start_Date_in_Queue DATETIME = NULL,      
    @End_Date_in_Queue DATETIME = NULL,      
    @Start_Date_in_CBMS DATETIME = NULL,      
    @End_Date_in_CBMS DATETIME = NULL,      
    @Data_Source INT = NULL,      
    @Vendor VARCHAR(100) = NULL,      
    @Vendor_Type VARCHAR(100) = NULL,      
    @Month DATETIME = NULL,      
    @Filename VARCHAR(100) = NULL,      
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices       
    @User_Group_Id_List VARCHAR(MAX),      
    @Invoice_List_TVP Invoice_List READONLY,      
    @Workflow_Queue_Action_id INT         
------------------------------------------------------------            
 OUTPUT PARAMETERS:            
 Name   DataType  Default Description            
------------------------------------------------------------            
 USAGE EXAMPLES:            
 DECLARE  @return_value int         
        
EXEC  @return_value = [Workflow].[Update_exception_priority]         
    @User_id = 51,         
    @Module_Id = 1         
------------------------------------------------------------            
AUTHOR INITIALS:            
Initials Name            
------------------------------------------------------------            
AKP   Arunkumar Summit Energy         
         
 MODIFICATIONS             
 Initials Date   Modification            
------------------------------------------------------------            
 AKP    Sep 3, 2019 Created    
 CPK	Oct-2019  added event log logic   
 AP		Nov 1 2019 System Priority flag should not be removed until an invoice is either posted or closed     
        
******/     
-- =============================================     
           
CREATE PROCEDURE [Workflow].[Update_exception_priority]      
    -- Add the parameters for the stored procedure here                
      
      
    @MODULE_ID INT,      
    @Workflow_Queue_Saved_Filter_Query_Id INT = NULL,      
    @Logged_In_User INT,      
    @Queue_id VARCHAR(MAX) = NULL,     ---  User selected from drop down Id 49,1,2,3                
    @Exception_Type VARCHAR(MAX) = NULL,      
    @Account_Number VARCHAR(500) = NULL,      
    @Client VARCHAR(500) = NULL,      
    @Site VARCHAR(500) = NULL,      
    @Country VARCHAR(500) = NULL,      
    @State VARCHAR(500) = NULL,      
    @City VARCHAR(500) = NULL,      
    @Commodity VARCHAR(500) = NULL,      
    @Invoice_ID VARCHAR(500) = NULL,      
    @Priority INT = 0,                 --- Yes                
    @Exception_Status INT = NULL,      
    @Comments VARCHAR(500) = NULL,      
    @Start_Date_in_Queue DATETIME = NULL,      
    @End_Date_in_Queue DATETIME = NULL,      
    @Start_Date_in_CBMS DATETIME = NULL,      
    @End_Date_in_CBMS DATETIME = NULL,      
    @Data_Source INT = NULL,      
    @Vendor VARCHAR(100) = NULL,      
    @Vendor_Type VARCHAR(100) = NULL,      
    @Month DATETIME = NULL,      
    @Filename VARCHAR(100) = NULL,      
    @invoice_List VARCHAR(MAX) = NULL, ---- if new invoice needs to be added send those invoices       
    @User_Group_Id_List VARCHAR(MAX),      
    @Invoice_List_TVP Invoice_List READONLY,      
    @Workflow_Queue_Action_id INT      
AS      
BEGIN -- SET NOCOUNT ON added to prevent extra result sets from                
    -- interfering with SELECT statements.                
    SET NOCOUNT ON;      
      
    DECLARE @Proc_name VARCHAR(100) = 'Update_Exception_Priority',      
            @Input_Params VARCHAR(1000),      
            @Error_Line INT,      
            @Error_Message VARCHAR(3000),      
            @sql NVARCHAR(MAX),      
            @workflow_queue_action VARCHAR(50),      
            @getdate DATETIME = GETDATE();      
      
    DECLARE @Source_Cd INT,      
            @Userprioritymanual INT,      
            @Client_Id INT;      
    SELECT @Source_Cd = Code_Id      
    FROM dbo.Code      
    WHERE Code_Value = 'Systempriorityclient';      
    SELECT @Userprioritymanual = Code_Id      
    FROM dbo.Code      
    WHERE Code_Value = 'Userprioritymanual';      
      
      
    DECLARE @invoice_table TABLE      
    (      
        Invoice_id INT      
    );      
      
    SELECT @Input_Params      
        = '@User ID:' + CAST(@Logged_In_User AS VARCHAR) + '@Workflow_Queue_Saved_Filter_Query_id:'      
          + CAST(@Workflow_Queue_Saved_Filter_Query_Id AS VARCHAR) + '@invoice_List:' + ISNULL(@invoice_List, '');      
      
      
      
    BEGIN TRY      
        SELECT @workflow_queue_action = Code_Value      
        FROM Workflow.Workflow_Queue_Action WQA      
            JOIN Code C      
                ON WQA.Workflow_Queue_Action_Cd = C.Code_Id      
        WHERE WQA.Workflow_Queue_Action_Id = @Workflow_Queue_Action_id;      
      
        INSERT @invoice_table      
        EXEC [Workflow].[Get_List_Of_Invoices] @MODULE_ID,      
                                               @Workflow_Queue_Saved_Filter_Query_Id,      
                                               @Logged_In_User,      
                                               @Queue_id,      
                                               @Exception_Type,      
                                               @Account_Number,      
                                               @Client,      
                                               @Site,      
                                               @Country,      
                                               @State,          
                                               @City,            
											   @Commodity,       
                                               @Invoice_ID,      
                                               @Priority,      
                                               @Exception_Status,      
                                               NULL,      
                                               @Start_Date_in_Queue,      
                                               @End_Date_in_Queue,      
                                               @Start_Date_in_CBMS,      
                                               @End_Date_in_CBMS,      
                                               @Data_Source,      
                                               @Vendor,      
                                               @Vendor_Type,      
                                               @Month,      
                                               @Filename,      
                                               @invoice_List,      
                                               @User_Group_Id_List,      
                                               @Invoice_List_TVP;       
      
  
 DELETE I FROM @invoice_table I  
 JOIN workflow.Cu_Invoice_Attribute CU  
 ON I.Invoice_id = CU.CU_INVOICE_ID  
 WHERE cu.Source_Cd = @Source_Cd   
  
        IF (@workflow_queue_action = 'MARK AS PRIORITY')      
        BEGIN      
           MERGE [Workflow].[Cu_Invoice_Attribute] [TARGET]      
            USING ( SELECT Invoice_id FROM @invoice_table ) SRC      
            ON SRC.Invoice_id = [TARGET].CU_INVOICE_ID      
            WHEN MATCHED THEN      
                UPDATE SET [TARGET].Is_Priority = 1,      
                           [TARGET].Updated_User_Id = @Logged_In_User,      
                           [TARGET].[Last_Change_Ts] = @getdate,      
                           [TARGET].[Source_Cd] = @Userprioritymanual     
            WHEN NOT MATCHED BY TARGET THEN      
                INSERT      
                (      
                    CU_INVOICE_ID,      
     Is_Priority,      
                    Created_User_Id,      
                    Created_Ts,      
                    Updated_User_Id,      
                    Last_Change_Ts ,    
     Source_Cd     
                )      
                VALUES      
                (SRC.Invoice_id, 1, @Logged_In_User, @getdate, @Logged_In_User, @getdate,@Userprioritymanual);     
      
      
    INSERT INTO dbo.cu_invoice_event            
    (            
       cu_invoice_id    
      ,event_date    
      ,event_by_id    
      ,event_description           
    )         
    SELECT             
     AC.Invoice_id AS CU_INVOICE_ID            
    ,getdate() AS event_date            
    ,@Logged_In_User AS event_by_id           
    ,'Invoice Marked Priority' AS event_description            
    FROM @invoice_table AS AC      
      
        END;      
        ELSE IF (@workflow_queue_action = 'REMOVE PRIORITY')      
        BEGIN      
            UPDATE [TARGET]      
            SET [TARGET].Is_Priority = 0,      
                [TARGET].Updated_User_Id = @Logged_In_User,      
                [TARGET].[Last_Change_Ts] = @getdate,      
                [TARGET].[Source_Cd] = @Userprioritymanual      
            FROM [Workflow].[Cu_Invoice_Attribute] [TARGET]      
                JOIN @invoice_table SRC      
                    ON SRC.Invoice_id = [TARGET].CU_INVOICE_ID;      
  
    INSERT INTO dbo.cu_invoice_event            
    (            
       cu_invoice_id    
      ,event_date    
      ,event_by_id    
      ,event_description           
    )         
    SELECT             
     AC.Invoice_id AS CU_INVOICE_ID            
    ,GETDATE() AS event_date            
    ,@Logged_In_User AS event_by_id           
    ,'Invoice Removed Priority' AS event_description            
    FROM @invoice_table AS AC     
        END;      
    END TRY      
    BEGIN CATCH      
        -- ENTRY MADE TO THE LOGGING SP TO CAPTURE THE ERRORS.               
        SELECT @Error_Line = ERROR_LINE(),      
               @Error_Message = ERROR_MESSAGE();      
      
        INSERT INTO StoredProc_Error_Log      
        (      
            StoredProc_Name,      
            ERROR_LINE,      
            ERROR_MESSAGE,      
            Input_Params      
        )      
        VALUES      
        (@Proc_name, @Error_Line, @Error_Message, @Input_Params);      
      
        RETURN -99;      
    END CATCH;      
END;           
GO
GRANT EXECUTE ON  [Workflow].[Update_exception_priority] TO [CBMSApplication]
GO
