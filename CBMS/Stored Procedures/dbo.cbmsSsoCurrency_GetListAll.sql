SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.cbmsSsoCurrency_GetListAll

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@MyAccountId   	int       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	9/21/2010	Modify Quoted Identifier
******/

CREATE    PROCEDURE [dbo].[cbmsSsoCurrency_GetListAll]

	( @MyAccountId int

	)
AS
BEGIN

	   select currency_unit_id
		, currency_unit_name
	     from currency_unit
	 order by sort_order
		, currency_unit_name 

END
GO
GRANT EXECUTE ON  [dbo].[cbmsSsoCurrency_GetListAll] TO [CBMSApplication]
GO
