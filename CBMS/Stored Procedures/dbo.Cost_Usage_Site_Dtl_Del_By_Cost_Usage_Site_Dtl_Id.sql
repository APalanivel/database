SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.Cost_Usage_Site_Dtl_Del_By_Cost_Usage_Site_Dtl_Id

DESCRIPTION:

	Used to delete data in Cost_Usage_Site_Dtl table for the given Cost_Usate_Site_Dtl_Id
	
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Cost_Usage_Site_Dtl_Id	int

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC dbo.Cost_Usage_Site_Dtl_Del_By_Cost_Usage_Site_Dtl_Id 1


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	1/27/2010	Created

******/

CREATE PROCEDURE [dbo].[Cost_Usage_Site_Dtl_Del_By_Cost_Usage_Site_Dtl_Id]
	@Cost_Usage_Site_Dtl_Id	int
AS
BEGIN

	SET NOCOUNT ON

	DELETE 
		dbo.Cost_Usage_Site_Dtl
	WHERE
		Cost_Usage_Site_Dtl_id = @Cost_Usage_Site_Dtl_Id

END
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Dtl_Del_By_Cost_Usage_Site_Dtl_Id] TO [CBMSApplication]
GO
