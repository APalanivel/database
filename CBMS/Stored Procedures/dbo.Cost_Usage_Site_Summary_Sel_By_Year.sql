SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
	 dbo.Cost_Usage_Site_Summary_Sel_By_Year
	 
DESCRIPTION:  
	This procedure accepts @bucket_list as a TVP and returns the bucket value 
	   and invoice participation details for the given site id and report year.
	 
INPUT PARAMETERS:  
    Name			    DataType		    Default Description  
------------------------------------------------------------  
    @Client_Hier_Id		    INT
    @Report_Year	    INT
    @Bucket_List	    Cost_Usage_Bucket		  READONLY - Table Value Parameter

 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  

 USAGE EXAMPLES:  
------------------------------------------------------------ 
    DECLARE @Bucket_List1 AS Cost_Usage_Bucket

    INSERT INTO @Bucket_List1
    VALUES (1423, 'Total Cost','Charge', 3)
	   ,(1423, 'Volume', 'Determinant' ,1567)

    EXECUTE dbo.Cost_Usage_Site_Summary_Sel_By_Year 1204, 2010, @Bucket_List1




    DECLARE @Bucket_List1 AS Cost_Usage_Bucket

    INSERT INTO @Bucket_List1
    VALUES (58, 'Total Cost','Charge', 3)
	   ,(58, 'Volume', 'Determinant' ,1389)

    EXECUTE dbo.Cost_Usage_Site_Summary_Sel_By_Year 1228, 2010, @Bucket_List1
	

    SELECT * FROM Core.Client_Hier WHERE SIte_Id = 17654

    SELECT TOP 10 * FROM dbo.Cost_Usage_Site_Dtl WHERE data_Source_Cd = 100348
	SELECT * FROM Bucket_Master WHERE Bucket_Master_id = 100019

 AUTHOR INITIALS:
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 HG		  Harihara Suthan G

 MODIFICATIONS
 Initials	  Date	    Modification
------------------------------------------------------------
 AP		  09/14/2011  Created
 AP		  09/28/2011  Removed @Commodity_Id, @UOM_Type_Id params and changed @Bucket_List param as Cost_Usage_Bucket type param
						Used @Sevice_Month table variable instead CTE for fisical year info
						Added Month_Number & Commodity_Name to output
 HG		  10/03/2011  Modifed the query to take the default_uom_type_id in Bucket_Master table if no UOM id passed in @Bucket_List for determinant type buckets
 HG		  2012-03-20  Implemented the Client_Hier_Id changes and the data source code changes for additional data enhancement.
 ******/
 
CREATE PROCEDURE dbo.Cost_Usage_Site_Summary_Sel_By_Year
      ( 
       @Client_Hier_Id INT
      ,@Report_Year INT
      ,@Bucket_List AS dbo.Cost_Usage_Bucket READONLY )
AS 
BEGIN

      SET NOCOUNT ON
  
      DECLARE
            @Currency_Group_id INT
           ,@Site_Id INT
           ,@Fiscal_Year_Start_Month DATE
           ,@Fiscal_Year_End_Month DATE

      DECLARE @Service_Month TABLE
            ( 
             Service_Month DATE
            ,Month_Number SMALLINT )

   	 -- Fiscal service months calculation based on Client_fiscal_offset value
      SELECT
            @Fiscal_Year_Start_Month = dateadd(mm, ch.Client_Fiscal_Offset, dd.Date_D)
           ,@Currency_Group_id = ch.Client_Currency_Group_Id
           ,@Site_Id = ch.Site_Id
      FROM
            Core.Client_Hier ch
            INNER JOIN Meta.Date_Dim dd
                  -- Limited to first day of current year  
                  ON dd.Year_Num = @Report_Year
                     AND dd.Day_Of_Year_Num = 1
      WHERE
            ch.Client_Hier_Id = @Client_Hier_Id

      SET @Fiscal_Year_End_Month = dateadd(mm, 11, @Fiscal_Year_Start_Month);

      WITH  Cte_SiteResult
              AS ( SELECT
                        cusd.Service_Month
                       ,BM.Bucket_Name
                       ,bkt_cd.Code_Value AS Bucket_Type
                       ,sum(case WHEN bkt_cd.Code_Value = 'Determinant' THEN cusd.Bucket_Value * UC.Conversion_Factor
                                 WHEN bkt_cd.Code_Value = 'Charge' THEN cusd.Bucket_Value * CC.Conversion_Factor
                                 ELSE 0
                            END) AS Bucket_Value
                       ,COM.Commodity_Name
                       ,dscd.Code_Value
                   FROM
                        dbo.Cost_Usage_Site_Dtl cusd
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cusd.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        INNER JOIN dbo.Code dscd
                              ON dscd.Code_Id = cusd.Data_Source_Cd
                        INNER JOIN @Bucket_List BM_Id
                              ON bm_id.Commodity_Id = bm.Commodity_Id
                                 AND BM_Id.Bucket_Name = BM.Bucket_Name
                                 AND bm_id.Bucket_Type = bkt_cd.Code_Value
                        INNER JOIN dbo.Commodity COM
                              ON COM.Commodity_Id = BM.Commodity_Id
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_Id = cusd.UOM_Type_Id
                                 AND uc.Converted_Unit_Id = isnull(BM_Id.Bucket_Uom_Id, bm.Default_Uom_Type_Id)
                                 AND bm_id.Bucket_Type = 'Determinant'
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON ( cc.Currency_Group_Id = @Currency_Group_Id
                                   AND cc.Base_Unit_Id = cusd.Currency_Unit_Id
                                   AND cc.Converted_Unit_Id = bm_id.Bucket_Uom_Id
                                   AND bm_id.Bucket_Type = 'Charge'
                                   AND cc.Conversion_Date = cusd.Service_Month )
                   WHERE
                        cusd.Client_Hier_Id = @Client_Hier_Id
                        AND cusd.Service_Month BETWEEN @Fiscal_Year_Start_Month
                                               AND     @Fiscal_Year_End_Month
                   GROUP BY
                        cusd.Service_Month
                       ,BM.Bucket_Name
                       ,bkt_cd.Code_Value
                       ,COM.Commodity_Name
                       ,dscd.Code_Value)
            SELECT
                  fy.Date_D AS Service_month
                 ,fy.Month_Num AS Month_Number
                 ,x.Commodity_Name
                 ,x.Bucket_Name
                 ,X.Bucket_Type
                 ,x.Bucket_Value
                 ,( case WHEN sum(cast(IP.Is_Expected AS INT)) = sum(cast(IP.Is_Received AS INT)) THEN 1
                         ELSE 0
                    END ) AS Is_Complete
                 ,isnull(max(convert(INT, ip.Is_Received)), 0) AS Is_published
                 ,isnull(max(case WHEN ip.Recalc_Under_Review = 1
                                       OR ip.Variance_Under_Review = 1 THEN 1
                                  ELSE 0
                             END), 0) AS Is_Under_Review
                 ,x.Code_Value AS Data_Source_Code
            FROM
                  meta.Date_Dim fy
                  LEFT OUTER JOIN Cte_SiteResult x
                        ON x.service_month = fy.Date_D
                  LEFT OUTER JOIN dbo.Invoice_Participation ip
                        ON ip.Site_Id = @Site_Id
                           AND ip.Service_Month = x.Service_Month
            WHERE
                  fy.Date_D BETWEEN @Fiscal_Year_Start_Month
                            AND     @Fiscal_Year_End_Month
            GROUP BY
                  fy.Date_D
                 ,fy.Month_Num
                 ,x.Commodity_Name
                 ,x.Bucket_Name
                 ,X.Bucket_Type
                 ,x.Bucket_Value
                 ,x.Code_Value
            ORDER BY
                  fy.Date_D

END
;
GO
GRANT EXECUTE ON  [dbo].[Cost_Usage_Site_Summary_Sel_By_Year] TO [CBMSApplication]
GO
