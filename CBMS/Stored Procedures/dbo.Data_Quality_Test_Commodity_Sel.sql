SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:      
	dbo.Data_Quality_Test_Commodity_Sel

DESCRIPTION:
		Created For Data Quality Tests - Global Level Config Page.

		As of now we are added rules for EP only.
		
 INPUT PARAMETERS:      
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------      
	
 OUTPUT PARAMETERS:     
	Name					DataType			Default				 Description      
-------------------------------------------------------------------------------------------------   
   
 USAGE EXAMPLES:      
-------------------------------------------------------------------------------------------------      

	EXEC dbo.Data_Quality_Test_Commodity_Sel 

	
 AUTHOR INITIALS:
 Initials	Name
-------------------------------------------------------------------------------------------------      
 NR			Narayana Reddy
 
 MODIFICATIONS
 Initials	     Date		     Modification
-------------------------------------------------------------------------------------------------      
 NR				2019-04-23		Created Data2.0 -  Data Quality Tests - Global Level Config Page.

******/

CREATE PROCEDURE [dbo].[Data_Quality_Test_Commodity_Sel]
AS
    BEGIN

        SET NOCOUNT ON;


     		SELECT 
				DQT.Commodity_Id, 
				C.Commodity_Name
			FROM dbo.Data_Quality_Test AS DQT
			INNER JOIN dbo.Commodity AS C ON C.Commodity_Id=DQT.Commodity_Id
			GROUP BY DQT.Commodity_Id, 
					 C.Commodity_Name;
			



    END;




GO
GRANT EXECUTE ON  [dbo].[Data_Quality_Test_Commodity_Sel] TO [CBMSApplication]
GO
