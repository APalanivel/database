SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure dbo.seStripDetail_Get
	(
		@StripDetailId int
	)
AS
BEGIN
	set nocount on
	 select StripDetailId
				, StripHeaderId
				, Symbol
				, Volume
				, SortDate
		 from seStripDetail
		where StripDetailId = @StripDetailId

END
GO
GRANT EXECUTE ON  [dbo].[seStripDetail_Get] TO [CBMSApplication]
GO
