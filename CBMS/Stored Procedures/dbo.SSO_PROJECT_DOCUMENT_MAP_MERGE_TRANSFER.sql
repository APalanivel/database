SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME: SSO_PROJECT_DOCUMENT_MAP_MERGE_Changes        
        
DESCRIPTION:        
 Merges changes from SSO_PROJECT_DOCUMENT_MAP_Service to the SSO_PROJECT_DOCUMENT_MAP table        
        
INPUT PARAMETERS:        
 Name					DataType			Default		Description        
-------------------------------------------------------------------------------------------------------  
  @Message				XML								XML string of changes to the SiteGroup_Site table        
 ,@Conversation_Handle  UNIQUEIDENTIFER					Conversation Handle that sent the message         
        
OUTPUT PARAMETERS:        
 Name      DataType  Default Description        
------------------------------------------------------------------------------------------------------        
        
        
USAGE EXAMPLES:        
------------------------------------------------------------------------------------------------------  
  This sp should be executed from SQL Server Service Broker 

        
AUTHOR INITIALS:        
 Initials Name        
------------------------------------------------------------------------------------------------------  
 DSC  Kaushik        


MODIFICATIONS        
        
 Initials	Date(mm/dd/yyyy)	Modification          
------------------------------------------------------------------------------------------------------          
 DSC		09/25/2014			Created   

******/        
CREATE PROCEDURE [dbo].[SSO_PROJECT_DOCUMENT_MAP_MERGE_TRANSFER]
      (
       @Message XML
      ,@Conversation_Handle UNIQUEIDENTIFIER )
AS
BEGIN        
        
      SET NOCOUNT ON;        

      DECLARE
            @Idoc INT
           ,@Op_Code CHAR(1)

      EXEC sp_xml_preparedocument
            @idoc OUTPUT
           ,@Message        
                         

      DECLARE @SSO_PROJECT_DOCUMENT_MAP_Changes TABLE
            (
			 SSO_PROJECT_ID INT 
			,SSO_DOCUMENT_ID INT 
            ,[Op_Code] [CHAR](1)
			)

      INSERT INTO @SSO_PROJECT_DOCUMENT_MAP_Changes
                  (				 
				   SSO_PROJECT_ID
				  ,SSO_DOCUMENT_ID
                  ,Op_Code )
             SELECT
                   SSO_PROJECT_ID
				  ,SSO_DOCUMENT_ID
                  ,Op_Code
                  FROM
				  openxml (@idoc, '/SSO_PROJECT_DOCUMENT_MAP_Changes/SSO_PROJECT_DOCUMENT_MAP_Change',2)        
        WITH ( 
				 SSO_PROJECT_ID INT 
				,SSO_DOCUMENT_ID INT 
				,[Op_Code] [CHAR](1)
          )   

       
       
      EXEC sp_xml_removedocument
            @idoc  



      MERGE dbo.SSO_PROJECT_DOCUMENT_MAP tgt
      USING
            ( SELECT
				  SSO_PROJECT_ID
				 ,SSO_DOCUMENT_ID
                 ,Op_Code
              FROM
                 @SSO_PROJECT_DOCUMENT_MAP_Changes
			) src
      ON tgt.SSO_PROJECT_ID = src.SSO_PROJECT_ID
            AND tgt.SSO_DOCUMENT_ID = src.SSO_DOCUMENT_ID
      WHEN MATCHED AND src.Op_Code = 'D' THEN
            DELETE
      WHEN NOT MATCHED AND src.Op_Code ='I' THEN
            INSERT
                   (
				 	SSO_PROJECT_ID
					, SSO_DOCUMENT_ID	   
				   )
            VALUES (
					src.SSO_PROJECT_ID
					, src.SSO_DOCUMENT_ID	   
					);

		
END

;
GO
GRANT EXECUTE ON  [dbo].[SSO_PROJECT_DOCUMENT_MAP_MERGE_TRANSFER] TO [sb_Execute]
GO
