SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Account_Invoice_Collection_Month_Merge       
              
Description:              
			This sproc is to get the previously chased values in Invoice Collection Queue .      
                           
 Input Parameters:              
    Name										DataType			Default			Description                
-------------------------------------------------------------------------------------------------                
@Invoice_Collection_Account_Config_Id			INT
@Service_Month									DATE
@Invoice_Collection_Global_Config_Value_Id		INT					 NULL
@Account_Invoice_Collection_Frequency_Id		INT					 NULL
@Seq_No											INT                    
 
 Output Parameters:                    
    Name								DataType			Default			Description                
--------------------------------------------------------------------------------------                
              
 Usage Examples:                  
--------------------------------------------------------------------------------------   

   BEGIN Transaction
   Exec dbo.Account_Invoice_Collection_Month_Merge 174,'2014-01-01',Null,143,1
   SELECT * FROM  Account_Invoice_Collection_Month where Invoice_Collection_Account_Config_Id = 174 
   Rollback Transaction
  
   
Author Initials:              
    Initials		Name              
--------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
 Modifications:              
    Initials        Date			Modification              
--------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection. 
	RKV             2020-04-21      Added new parameter  @Invoice_Frequency_Pattern_Cd as part ICtoolrevamp.       
             
******/ 

CREATE PROCEDURE [dbo].[Account_Invoice_Collection_Month_Merge]
      ( 
       @Invoice_Collection_Account_Config_Id INT
      ,@Service_Month DATE
      ,@Invoice_Collection_Global_Config_Value_Id INT = NULL
      ,@Account_Invoice_Collection_Frequency_Id INT = NULL
      ,@Seq_No INT
      ,@Invoice_Frequency_Cd INT = -1
      ,@Is_Manual BIT = 0
	  ,@Invoice_Frequency_Pattern_Cd INT = NULL  )
AS 
BEGIN

      SET NOCOUNT ON;
	  

      SELECT
            @Invoice_Collection_Global_Config_Value_Id = Invoice_Collection_Global_Config_Value_Id
           ,@Account_Invoice_Collection_Frequency_Id = NULL
           ,@Invoice_Frequency_Cd = Invoice_Frequency_Cd
		   ,@Invoice_Frequency_Pattern_Cd = Invoice_Frequency_Pattern_Cd 
      FROM
            dbo.Invoice_Collection_Global_Config_Value icgcv
            INNER JOIN dbo.Code c
                  ON c.Code_Id = icgcv.Invoice_Frequency_Cd
      WHERE
            c.Code_Value = 'Monthly'
            AND @Is_Manual = 1
      
      
      
      MERGE INTO dbo.Account_Invoice_Collection_Month tgt_aicm
            USING 
                  ( SELECT
                        @Invoice_Collection_Account_Config_Id Invoice_Collection_Account_Config_Id
                       ,@Service_Month Service_Month
                       ,@Invoice_Collection_Global_Config_Value_Id Invoice_Collection_Global_Config_Value_Id
                       ,@Account_Invoice_Collection_Frequency_Id Account_Invoice_Collection_Frequency_Id
                       ,@Seq_No Seq_No ) src_aicm
            ON( tgt_aicm.Invoice_Collection_Account_Config_Id = src_aicm.Invoice_Collection_Account_Config_Id
                AND tgt_aicm.Service_Month = src_aicm.Service_Month
                AND tgt_aicm.Seq_No = src_aicm.Seq_No )
            WHEN MATCHED 
                  THEN  UPDATE
                    SET 
                        tgt_aicm.Invoice_Collection_Global_Config_Value_Id = src_aicm.Invoice_Collection_Global_Config_Value_Id
                       ,tgt_aicm.Account_Invoice_Collection_Frequency_Id = src_aicm.Account_Invoice_Collection_Frequency_Id
                       ,tgt_aicm.Invoice_Frequency_Cd = @Invoice_Frequency_Cd
					   ,tgt_aicm.Invoice_Frequency_Pattern_Cd = @Invoice_Frequency_Pattern_Cd
            WHEN NOT MATCHED 
                  THEN INSERT
                        ( 
                         Invoice_Collection_Account_Config_Id
                        ,Service_Month
                        ,Invoice_Collection_Global_Config_Value_Id
                        ,Account_Invoice_Collection_Frequency_Id
                        ,Invoice_Frequency_Cd
						,Invoice_Frequency_Pattern_Cd
                        ,Seq_No )
                    VALUES
                        ( 
                         src_aicm.Invoice_Collection_Account_Config_Id
                        ,src_aicm.Service_Month
                        ,src_aicm.Invoice_Collection_Global_Config_Value_Id
                        ,src_aicm.Account_Invoice_Collection_Frequency_Id
                        ,@Invoice_Frequency_Cd
						,@Invoice_Frequency_Pattern_Cd
                        ,src_aicm.Seq_No );

END


;
GO
GRANT EXECUTE ON  [dbo].[Account_Invoice_Collection_Month_Merge] TO [CBMSApplication]
GO
