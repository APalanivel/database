SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.GET_TEMPLATE_ACCOUNT_ID_P
	@template_invoice_id INT,
	@isInvoice BIT
AS
BEGIN

	SET NOCOUNT ON

	IF (@isInvoice=1)
	 BEGIN

		SELECT temp1.account_id
		FROM dbo.template temp1 INNER JOIN dbo.invoice inv ON inv.template_id = temp1.template_id
		WHERE inv.invoice_id = @template_invoice_id
 
	 END	
	ELSE
	 BEGIN

		SELECT temp1.account_id
		FROM dbo.template temp1
		WHERE temp1.template_id = @template_invoice_id

	 END

END
GO
GRANT EXECUTE ON  [dbo].[GET_TEMPLATE_ACCOUNT_ID_P] TO [CBMSApplication]
GO
