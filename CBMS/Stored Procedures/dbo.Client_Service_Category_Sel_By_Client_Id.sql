SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: dbo.Client_Service_Category_Sel_By_Client_Id
	

DESCRIPTION:

	Used to select from client_service_category 

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
	@Client_Id		INT

	
OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

Exec Client_Service_Category_Sel_By_Client_Id  10069

AUTHOR INITIALS:

	BCH			Balaraju
	
------------------------------------------------------------

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
 BCH		  19/10/2011	created

******/

CREATE PROCEDURE dbo.Client_Service_Category_Sel_By_Client_Id ( @Client_Id INT )
AS 
BEGIN

      SET NOCOUNT ON

      SELECT
            csc.Client_Service_Category_Id
           ,csc.Service_Category_Name
           ,csc.Client_Id
           ,csc.Uom_Id
           ,csc.Service_Level_Cd
           ,c.Code_Value
      FROM
            dbo.Client_Service_Category csc
            JOIN dbo.Code c
                  ON c.Code_Id = csc.Service_Level_Cd
      WHERE
            csc.Client_Id = @Client_Id
       ORDER BY c.Code_Value
			   ,csc.Client_Service_Category_Id

END
GO
GRANT EXECUTE ON  [dbo].[Client_Service_Category_Sel_By_Client_Id] TO [CBMSApplication]
GO
