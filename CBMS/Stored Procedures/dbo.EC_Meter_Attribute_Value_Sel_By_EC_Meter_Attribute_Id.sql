SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id       
              
Description:              
			This sproc get the attribute value details for the given attribute id.       
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
    @EC_Meter_Attribute_Id				INT
    
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

   Exec dbo.EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id 38
   
   Exec dbo.EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id 28

   EXEC EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id 739
   
	
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	NR				Narayana Reddy               
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    NR				2015-04-22		Created For AS400.         
	NR				2020-07-03		SE2017-984 - Added EC_Meter_Attribute_Value in order by clause.
             
******/

CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id]
    (
        @EC_Meter_Attribute_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , emav.EC_Meter_Attribute_Value_Id
            , emav.EC_Meter_Attribute_Value
            , CASE WHEN emat.EC_Meter_Attribute_Value IS NOT NULL THEN 1
                  WHEN ecvma.EC_Meter_Attribute_Value IS NOT NULL THEN 1
                  ELSE 0
              END AS Is_Used
        FROM
            dbo.EC_Meter_Attribute_Value emav
            INNER JOIN dbo.EC_Meter_Attribute ema
                ON emav.EC_Meter_Attribute_Id = ema.EC_Meter_Attribute_Id
            LEFT OUTER JOIN dbo.EC_Meter_Attribute_Tracking emat
                ON emav.EC_Meter_Attribute_Id = emat.EC_Meter_Attribute_Id
                   AND  emav.EC_Meter_Attribute_Value = emat.EC_Meter_Attribute_Value
            LEFT OUTER JOIN dbo.EC_Calc_Val_Meter_Attribute ecvma
                ON emav.EC_Meter_Attribute_Id = ecvma.EC_Meter_Attribute_Id
                   AND  emav.EC_Meter_Attribute_Value = ecvma.EC_Meter_Attribute_Value
        WHERE
            emav.EC_Meter_Attribute_Id = @EC_Meter_Attribute_Id
        GROUP BY
            ema.EC_Meter_Attribute_Id
            , ema.EC_Meter_Attribute_Name
            , emav.EC_Meter_Attribute_Value_Id
            , emav.EC_Meter_Attribute_Value
            , CASE WHEN emat.EC_Meter_Attribute_Value IS NOT NULL THEN 1
                  WHEN ecvma.EC_Meter_Attribute_Value IS NOT NULL THEN 1
                  ELSE 0
              END
        ORDER BY
            emav.EC_Meter_Attribute_Value ASC;
    END;

GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Value_Sel_By_EC_Meter_Attribute_Id] TO [CBMSApplication]
GO
