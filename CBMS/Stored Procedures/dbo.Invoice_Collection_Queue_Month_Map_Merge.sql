SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Collection_Queue_Month_Map_Merge       
              
Description:              
			This sproc is to map the Frequency With the Invoice Collection Queue .
                           
 Input Parameters:              
    Name										DataType			Default			Description                
-------------------------------------------------------------------------------------------------                
	@tvp_Invoice_Collection_Queue_Month_Dtl		TYPE								Has ICQ and Month Id
 
 Output Parameters:                    
    Name								DataType			Default			Description                
--------------------------------------------------------------------------------------                
              
 Usage Examples:                  
--------------------------------------------------------------------------------------   

   BEGIN Transaction
   Exec dbo.Invoice_Collection_Queue_Month_Map_Merge 114,1
   SELECT * FROM  Invoice_Collection_Queue_Month_Map where Invoice_Collection_Queue_Id = 114 
   Rollback Transaction

Author Initials:      
    Initials		Name
--------------------------------------------------------------------------------------
	RKV				Ravi Kumar Vegesna
 Modifications:
    Initials        Date			Modification
--------------------------------------------------------------------------------------
    RKV				2017-01-25		Created For Invoice_Collection.      
******/
CREATE PROCEDURE [dbo].[Invoice_Collection_Queue_Month_Map_Merge]
      (
       @tvp_Invoice_Collection_Queue_Month_Dtl AS tvp_Invoice_Collection_Queue_Month READONLY )
AS
BEGIN

      SET NOCOUNT ON;

      INSERT      INTO dbo.Invoice_Collection_Queue_Month_Map
                  (Invoice_Collection_Queue_Id
                  ,Account_Invoice_Collection_Month_Id
                  ,Created_Ts )
                  SELECT
                        iqm.Invoice_Collection_Queue_Id
                       ,iqm.Account_Invoice_Collection_Month_Id
                       ,GETDATE()
                  FROM
                        @tvp_Invoice_Collection_Queue_Month_Dtl iqm
                  WHERE
                        NOT EXISTS ( SELECT
                                          1
                                     FROM
                                          dbo.Invoice_Collection_Queue_Month_Map qmm
                                     WHERE
                                          qmm.Invoice_Collection_Queue_Id = iqm.Invoice_Collection_Queue_Id
                                          AND qmm.Account_Invoice_Collection_Month_Id = iqm.Account_Invoice_Collection_Month_Id );

      DELETE
            qmm
      FROM
            dbo.Invoice_Collection_Queue_Month_Map qmm
            INNER JOIN @tvp_Invoice_Collection_Queue_Month_Dtl tqm
                  ON tqm.Invoice_Collection_Queue_Id = qmm.Invoice_Collection_Queue_Id
      WHERE
            NOT EXISTS ( SELECT
                              1
                         FROM
                              @tvp_Invoice_Collection_Queue_Month_Dtl qmd
                         WHERE
                              qmd.Account_Invoice_Collection_Month_Id = qmm.Account_Invoice_Collection_Month_Id );
				
				
END;

;
GO
GRANT EXECUTE ON  [dbo].[Invoice_Collection_Queue_Month_Map_Merge] TO [CBMSApplication]
GO
