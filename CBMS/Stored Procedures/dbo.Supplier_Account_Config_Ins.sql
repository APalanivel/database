SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Supplier_Account_Config_Ins                       
                            
 DESCRIPTION:        
    To delete colums from  Client App Access Role   table.                            
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
----------------------------------------------------------------------------------  
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
----------------------------------------------------------------------------------  
                            
 USAGE EXAMPLES:                                
----------------------------------------------------------------------------------  
                 
  BEGIN TRAN   
           
--  SELECT * FROM dbo.Supplier_Account_Config where Account_Id = 1 And Contract_Id = 1     
  DECLARE @Out_Supplier_Account_Config_Id INT  
                
  EXEC dbo.Supplier_Account_Config_Ins  
       @Account_Id =2  
        , @Contract_Id =1  
        , @Supplier_Account_Begin_Dt ='2019-01-01'  
        , @Supplier_Account_End_Dt  = NULL  
        , @User_Info_Id =49   
		,@Supplier_Account_Config_Id = @Out_Supplier_Account_Config_Id Out

		Select Out_Supplier_Account_Config_Id
  
 -- SELECT * FROM dbo.Supplier_Account_Config where Account_Id = 1 And Contract_Id = 1   
      
  ROLLBACK     
                     
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
----------------------------------------------------------------------------------  
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
----------------------------------------------------------------------------------  
 NR                     2013-12-30      Created for - Add contract.                          
                           
******/
CREATE PROCEDURE [dbo].[Supplier_Account_Config_Ins]
    (
        @Account_Id INT
        , @Contract_Id INT
        , @Supplier_Account_Begin_Dt DATE
        , @Supplier_Account_End_Dt DATE = NULL
        , @User_Info_Id INT
        , @Supplier_Account_Config_Id INT OUTPUT
    )
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE
            @Min_Supplier_Account_Begin_Dt DATE
            , @Max_Supplier_Account_End_Dt DATE;


        INSERT INTO dbo.Supplier_Account_Config
             (
                 Account_Id
                 , Contract_Id
                 , Supplier_Account_Begin_Dt
                 , Supplier_Account_End_Dt
                 , Created_User_Id
                 , Created_Ts
                 , Updated_User_Id
                 , Last_Change_Ts
             )
        SELECT
            @Account_Id
            , @Contract_Id
            , @Supplier_Account_Begin_Dt
            , NULLIF(@Supplier_Account_End_Dt, '9999-12-31')
            , @User_Info_Id
            , GETDATE()
            , @User_Info_Id
            , GETDATE()
        WHERE
            NOT EXISTS (   SELECT
                                1
                           FROM
                                dbo.Supplier_Account_Config sac
                           WHERE
                                sac.Account_Id = @Account_Id
                                AND sac.Contract_Id = @Contract_Id
                                AND sac.Supplier_Account_Begin_Dt = @Supplier_Account_Begin_Dt
                                AND ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31') = ISNULL(
                                                                                            @Supplier_Account_End_Dt
                                                                                            , '9999-12-31'));




        SELECT  @Supplier_Account_Config_Id = SCOPE_IDENTITY();

        SELECT
            @Min_Supplier_Account_Begin_Dt = MIN(sac.Supplier_Account_Begin_Dt)
            , @Max_Supplier_Account_End_Dt = MAX(sac.Supplier_Account_End_Dt)
        FROM
            dbo.Supplier_Account_Config sac
        WHERE
            sac.Account_Id = @Account_Id;


        UPDATE
            a
        SET
            a.Supplier_Account_Begin_Dt = @Min_Supplier_Account_Begin_Dt
            , a.Supplier_Account_End_Dt = @Max_Supplier_Account_End_Dt
        FROM
            dbo.ACCOUNT a
        WHERE
            a.ACCOUNT_ID = @Account_Id;








    END;








GO
GRANT EXECUTE ON  [dbo].[Supplier_Account_Config_Ins] TO [CBMSApplication]
GO
