SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.SR_SAD_GET_SA_SM_EMAIL_P
	@user_info_id int
	AS
	set nocount on
select 	info.email_address
from 	sr_sa_sm_map map(nolock),
	user_info info(nolock)
where 	map.sourcing_analyst_id = @user_info_id
	and info.user_info_id = map.sourcing_manager_id
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_GET_SA_SM_EMAIL_P] TO [CBMSApplication]
GO
