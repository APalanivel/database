
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	dbo.SR_SAD_SAVE_SUPPLIER_PROFILE_DETAILS_P

DESCRIPTION: 


INPUT PARAMETERS:    
      Name                          DataType          Default     Description    
---------------------------------------------------------------------------------    
	@userId 	 		            varchar(10)
	@sessionId 	 		            varchar(20)
	@vendorId 	 		            int
	@creditBioComments 	 		    varchar(4000)
	@targetMarketComments 	 		varchar(4000)
	@competitiveAdvantageComments 	varchar(4000)
	@isSummitApproved 	 		    bit
	@summitApprovedComments 	 	varchar(200)
	@isMinorityOwned 	 		    bit
	@minorityOwnedComments 	        varchar(200)
	@isGreenEnergy 	 		        bit     
	@greenEnergyComments 	        varchar(200)
	@isGovtContracts 	 		    bit     
	@govtContractsComments 	 		varchar(200)
                          
                           
OUTPUT PARAMETERS:         
      Name              DataType          Default     Description    
------------------------------------------------------------    


USAGE EXAMPLES:
------------------------------------------------------------
	SELECT TOP 10 * FROM dbo.VENDOR vnd 
		WHERE NOT EXISTS (SELECT * FROM dbo.SR_SUPPLIER_PROFILE ssp WHERE ssp.VENDOR_ID = vnd.VENDOR_ID )
	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_SUPPLIER_PROFILE WHERE VENDOR_ID = 9
		EXEC dbo.SR_SAD_SAVE_SUPPLIER_PROFILE_DETAILS_P 1,1,9,'Credit Bio Comment','Target Market Comment','Competitive Advantage Comment'
			,1,'Summit Approved Comment',1,'Minority Owned Comment',1,'Green Energy Comment',1,'Govt Contracts Comment','Genaral Comment New Vendor insert'
		SELECT * FROM dbo.SR_SUPPLIER_PROFILE WHERE VENDOR_ID = 9
	ROLLBACK TRANSACTION

	BEGIN TRANSACTION
		SELECT * FROM dbo.SR_SUPPLIER_PROFILE WHERE Genaral_Comment = 'Genaral Comment'
		EXEC dbo.SR_SAD_SAVE_SUPPLIER_PROFILE_DETAILS_P 1,1,689,'Credit Bio Comment Update','Target Market Comment Update','Competitive Advantage Comment Update'
			,1,'Summit Approved Comment Update',1,'Minority Owned Comment Update',1,'Green Energy Comment Update',1,'Govt Contracts Comment Update','Genaral Comment Update'
		SELECT * FROM dbo.SR_SUPPLIER_PROFILE WHERE Genaral_Comment = 'Genaral Comment Update'
	ROLLBACK TRANSACTION


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter
	RR			Raghu Reddy 

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	DR			08/04/2009	Removed Linked Server Updates
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2015-07-24	Global Sourcing - Added new input parameter Genaral Comment


******/
CREATE PROCEDURE [dbo].[SR_SAD_SAVE_SUPPLIER_PROFILE_DETAILS_P]
      ( 
       @userId VARCHAR(10)
      ,@sessionId VARCHAR(20)
      ,@vendorId INT
      ,@creditBioComments VARCHAR(4000)
      ,@targetMarketComments VARCHAR(4000)
      ,@competitiveAdvantageComments VARCHAR(4000)
      ,@isSummitApproved BIT
      ,@summitApprovedComments VARCHAR(200)
      ,@isMinorityOwned BIT
      ,@minorityOwnedComments VARCHAR(200)
      ,@isGreenEnergy BIT
      ,@greenEnergyComments VARCHAR(200)
      ,@isGovtContracts BIT
      ,@govtContractsComments VARCHAR(200)
      ,@Genaral_Comment NVARCHAR(200) )
AS 
BEGIN 
      SET NOCOUNT ON;

      IF ( SELECT
            count(*)
           FROM
            dbo.SR_SUPPLIER_PROFILE
           WHERE
            VENDOR_ID = @vendorId ) > 0 
            BEGIN

                  UPDATE
                        dbo.SR_SUPPLIER_PROFILE
                  SET   
                        CREDIT_BIO_COMMENTS = @creditBioComments
                       ,TARGET_MARKET_COMMENTS = @targetMarketComments
                       ,COMPETITIVE_ADVANTAGE_COMMENTS = @competitiveAdvantageComments
                       ,IS_SUMMIT_APPROVED = @isSummitApproved
                       ,SUMMIT_APPROVED_COMMENTS = @summitApprovedComments
                       ,IS_MINORITY_OWNED = @isMinorityOwned
                       ,MINORITY_OWNED_COMMENTS = @minorityOwnedComments
                       ,IS_GREEN_ENERGY = @isGreenEnergy
                       ,GREEN_ENERGY_COMMENTS = @greenEnergyComments
                       ,IS_GOVT_CONTRACTS = @isGovtContracts
                       ,GOVT_CONTRACTS_COMMENTS = @govtContractsComments
                       ,Genaral_Comment = @Genaral_Comment
                  WHERE
                        VENDOR_ID = @vendorId

            END

      ELSE 
            BEGIN

                  DECLARE @sr_supplier_profile_id INT

                  INSERT      INTO dbo.SR_SUPPLIER_PROFILE
                              ( 
                               VENDOR_ID
                              ,CREDIT_BIO_COMMENTS
                              ,TARGET_MARKET_COMMENTS
                              ,COMPETITIVE_ADVANTAGE_COMMENTS
                              ,IS_SUMMIT_APPROVED
                              ,SUMMIT_APPROVED_COMMENTS
                              ,IS_MINORITY_OWNED
                              ,MINORITY_OWNED_COMMENTS
                              ,IS_GREEN_ENERGY
                              ,GREEN_ENERGY_COMMENTS
                              ,IS_GOVT_CONTRACTS
                              ,GOVT_CONTRACTS_COMMENTS
                              ,Genaral_Comment )
                  VALUES
                              ( 
                               @vendorId
                              ,@creditBioComments
                              ,@targetMarketComments
                              ,@competitiveAdvantageComments
                              ,@isSummitApproved
                              ,@summitApprovedComments
                              ,@isMinorityOwned
                              ,@minorityOwnedComments
                              ,@isGreenEnergy
                              ,@greenEnergyComments
                              ,@isGovtContracts
                              ,@govtContractsComments
                              ,@Genaral_Comment )	

                  SELECT
                        @sr_supplier_profile_id = scope_identity()

            END
END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_SAVE_SUPPLIER_PROFILE_DETAILS_P] TO [CBMSApplication]
GO
