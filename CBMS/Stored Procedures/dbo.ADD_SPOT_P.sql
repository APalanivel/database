SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE dbo.ADD_SPOT_P
	@contract_id INT,
	@currency_unit_id INT,
	@spot_name VARCHAR(200),
	@spot_unit_type_id INT,
	@spot_from_date DATETIME,
	@spot_to_date DATETIME,
	@spot_volume DECIMAL(32,16),
	@spot_price DECIMAL(32,16),
	@deal_type_id INT,
	@spot_transaction_date DATETIME,
	@transaction_by_type_id INT,
	@spot_comments VARCHAR(4000)
AS
BEGIN

	SET NOCOUNT ON

	INSERT INTO dbo.spot(contract_id,
		currency_unit_id,
		spot_name,
		spot_unit_type_id,
		spot_from_date,
		spot_to_date,
		spot_volume,
		spot_price,
		deal_type_id,
		spot_transaction_date,
		transaction_by_type_id,
		spot_comments)
	VALUES (@contract_id,
		@currency_unit_id,
		@spot_name,
		@spot_unit_type_id,
		@spot_from_date,
		@spot_to_date,
		@spot_volume,
		@spot_price,
		@deal_type_id,
		@spot_transaction_date,
		@transaction_by_type_id,
		@spot_comments)

END
GO
GRANT EXECUTE ON  [dbo].[ADD_SPOT_P] TO [CBMSApplication]
GO
