SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Cu_Invoice_Account_Commodity_Ins
              
Description:              
        To insert Data to Cu_Invoice_Account_Commodity table.              
              
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
   @Cu_Invoice_Id						 INT
   @Account_Id							 INT
   @Commodity_Id						 INT
        
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
   @Cu_Invoice_Account_Commodity_Id     INT
              
 Usage Examples:                  
----------------------------------------------------------------------------------------                
	BEGIN TRAN
	DECLARE  @Cu_Invoice_Account_Commodity_Id_Out INT 
	select * from Cu_Invoice_Account_Commodity where Cu_Invoice_Id =  23939757 and account_id = 774695
	EXEC   Cu_Invoice_Account_Commodity_Ins 23939757,774695,291,@Cu_Invoice_Account_Commodity_Id_Out OUTPUT      
    select * from Cu_Invoice_Account_Commodity where Cu_Invoice_Id =  23939757 and account_id = 774695
    select @Cu_Invoice_Account_Commodity_Id_Out
    ROLLBACK TRAN         

Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV			   Ravi Kumar Vegesna
 
Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-09-01		Maint-4109: Created For AS400-Enhancement.         
             
******/ 

CREATE PROCEDURE [dbo].[Cu_Invoice_Account_Commodity_Ins]
      ( 
       @Cu_Invoice_Id INT
      ,@Account_Id INT
      ,@Commodity_Id INT
      ,@Cu_Invoice_Account_Commodity_Id INT OUT )
AS 
BEGIN

      SET NOCOUNT ON
      

      INSERT      INTO dbo.Cu_Invoice_Account_Commodity
                  ( 
                   Cu_Invoice_Id
                  ,Account_Id
                  ,Commodity_Id )
      VALUES
                  ( 
                   @Cu_Invoice_Id
                  ,@Account_Id
                  ,@Commodity_Id )

      SET @Cu_Invoice_Account_Commodity_Id = SCOPE_IDENTITY()


END;
;
GO
GRANT EXECUTE ON  [dbo].[Cu_Invoice_Account_Commodity_Ins] TO [CBMSApplication]
GO
