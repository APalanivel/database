SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME:  
dbo.Client_Links_Sel_By_Client_Id

DESCRIPTION:  

	Used to get CLIENT_ID,LINK_TYPE_CD,LINK_URL,LINK_NAME FROM CLIENT_LINKS table

INPUT PARAMETERS:  
Name      DataType Default Description  
------------------------------------------------------------  
@CLIENT_ID INT

OUTPUT PARAMETERS:  
Name      DataType Default Description  
------------------------------------------------------------  
USAGE EXAMPLES:  
------------------------------------------------------------

EXEC [dbo].[Client_Links_Sel_By_Client_Id] 10819

AUTHOR INITIALS:  
Initials	Name  
------------------------------------------------------------  
GB			Geetansu Behera  
CMH		Chad Hattabaugh   
HG			Hari 
SKA		Shobhit Kr Agrawal

MODIFICATIONS   
Initials	Date		Modification  
------------------------------------------------------------  
GB						created
SKA		14-JUL-09 Modified as per coding standard
SKA		23-JUL-09 Comments from scripts has been cleared
SKA		27-JUL-09 Changed the schema from SU to DBO

 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE  PROCEDURE [dbo].[Client_Links_Sel_By_Client_Id]
	@CLIENT_ID INT
AS

BEGIN

SET NOCOUNT ON

	SELECT	[CLIENT_ID],
			[LINK_TYPE_CD],
			[LINK_URL],
			[LINK_NAME]
	FROM dbo.CLIENT_LINKS
	WHERE CLIENT_ID = @CLIENT_ID
	
END
GO
GRANT EXECUTE ON  [dbo].[Client_Links_Sel_By_Client_Id] TO [CBMSApplication]
GO
