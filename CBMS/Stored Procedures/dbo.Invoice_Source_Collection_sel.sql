SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Invoice_Source_Collection_sel       
              
Description: This sproc is to fill the invoice_source_collection_dropdown_fill
     		 InvoiceCollectionSource#InvoiceSourceType#MethodOfContact   
                           
 Input Parameters:              
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
	               
 
 Output Parameters:                    
    Name								DataType			Default			Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   

 Exec dbo.Invoice_Source_Collection_sel


   
  
   
Author Initials:              
    Initials		Name              
----------------------------------------------------------------------------------------                
	RKV				Ravi Kumar Vegesna
	SLP				Sri Lakshmi Pallikonda
 Modifications:              
    Initials        Date			Modification              
----------------------------------------------------------------------------------------                
    RKV				2016-12-29		Created For Invoice_Collection.         
	SLP				2019-11-14		Included Datafeed Partner as part of MAINT-9529

             
******/ 

CREATE PROCEDURE [dbo].[Invoice_Source_Collection_sel]
AS 
BEGIN
      SET NOCOUNT ON 

      DECLARE @Invoice_Source_Collection TABLE
            ( 
             sno INT IDENTITY(1, 1)
            ,Invoice_Source_Collection_Text VARCHAR(250)
            ,Invoice_Source_Collection_Cd VARCHAR(250) )

      DECLARE @UBM_Online INT

      SELECT
            @UBM_Online = c2.Code_Id
      FROM
            dbo.Code c2
            INNER JOIN dbo.Codeset cs2
                  ON c2.Codeset_Id = cs2.Codeset_Id
      WHERE
            cs2.Codeset_Name = 'InvoiceSourceType'
            AND c2.Code_Value = 'Online'
                                                      

      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(@UBM_Online AS VARCHAR) + '#'
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'ubm'

      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#'
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'InvoiceSourceType'
                                          AND c2.Code_Value = 'Online' ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Vendor'

      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value + ' - ' + moc.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#' + CAST(moc.Code_Id AS VARCHAR)
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'MethodOfContact' ) moc
                        CROSS JOIN ( SELECT
                                          c3.Code_Value
                                         ,c3.Code_Id
                                     FROM
                                          dbo.Code c3
                                          INNER JOIN dbo.Codeset cs3
                                                ON c3.Codeset_Id = cs3.Codeset_Id
                                     WHERE
                                          cs3.Codeset_Name = 'InvoiceSourceType'
                                          AND c3.Code_Value = 'Vendor Primary Contact' ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Vendor'

      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#'
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'InvoiceSourceType'
                                          AND c2.Code_Value = 'Mail Redirect' ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Vendor'
      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#'
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'InvoiceSourceType'
                                          AND c2.Code_Value = 'Online' ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Client'

      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value + ' - ' + moc.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#' + CAST(moc.Code_Id AS VARCHAR)
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'MethodOfContact' ) moc
                        CROSS JOIN ( SELECT
                                          c3.Code_Value
                                         ,c3.Code_Id
                                     FROM
                                          dbo.Code c3
                                          INNER JOIN dbo.Codeset cs3
                                                ON c3.Codeset_Id = cs3.Codeset_Id
                                     WHERE
                                          cs3.Codeset_Name = 'InvoiceSourceType'
                                          AND c3.Code_Value IN ( 'Client Primary Contact' ) ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Client'


      INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value + ' - ' + moc.Code_Value
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) + '#' + CAST(moc.Code_Id AS VARCHAR)
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'MethodOfContact' ) moc
                        CROSS JOIN ( SELECT
                                          c3.Code_Value
                                         ,c3.Code_Id
                                     FROM
                                          dbo.Code c3
                                          INNER JOIN dbo.Codeset cs3
                                                ON c3.Codeset_Id = cs3.Codeset_Id
                                     WHERE
                                          cs3.Codeset_Name = 'InvoiceSourceType'
                                          AND c3.Code_Value IN ( 'Account Primary Contact' ) ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Client'


		INSERT      INTO @Invoice_Source_Collection
                  ( 
                   Invoice_Source_Collection_Text
                  ,Invoice_Source_Collection_cd )
                  SELECT
                        c.Code_Value + ' - ' + ist.Code_Value 
                       ,CAST(c.Code_Id AS VARCHAR) + '#' + CAST(ist.Code_Id AS VARCHAR) 
                  FROM
                        dbo.Code c
                        INNER JOIN dbo.Codeset cs
                              ON c.Codeset_Id = cs.Codeset_Id
                        CROSS JOIN ( SELECT
                                          c2.Code_Value
                                         ,c2.Code_Id
                                     FROM
                                          dbo.Code c2
                                          INNER JOIN dbo.Codeset cs2
                                                ON c2.Codeset_Id = cs2.Codeset_Id
                                     WHERE
                                          cs2.Codeset_Name = 'MethodOfContact' ) moc
                        CROSS JOIN ( SELECT
                                          c3.Code_Value
                                         ,c3.Code_Id
                                     FROM
                                          dbo.Code c3
                                          INNER JOIN dbo.Codeset cs3
                                                ON c3.Codeset_Id = cs3.Codeset_Id
                                     WHERE
                                          cs3.Codeset_Name = 'InvoiceSourceType'
                                          AND c3.Code_Value IN ( 'ETL','Partner' ) ) ist
                  WHERE
                        cs.Codeset_Name = 'InvoiceCollectionSource'
                        AND c.Code_Value = 'Data feed'
						AND moc.Code_Value NOT IN ('Others','Telephone')

      SELECT
            Invoice_Source_Collection_cd
           ,Invoice_Source_Collection_Text
      FROM
            @Invoice_Source_Collection
      ORDER BY
            sno
END;

;

GO
GRANT EXECUTE ON  [dbo].[Invoice_Source_Collection_sel] TO [CBMSApplication]
GO
