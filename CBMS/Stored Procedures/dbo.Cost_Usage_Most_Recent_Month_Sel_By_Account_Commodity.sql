
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
	
/******
NAME:
	CBMS.dbo.Cost_Usage_Most_Recent_Month_Sel_By_Account_Commodity

DESCRIPTION:
		
		Used to select most recent received cost usage data for the given account and commodity
		As application dosen't have Client_id information on this page getting the currency group id in a variable and using it in main query.
		This procedure used by variance process.
		
INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
	@Account_Id				INT
	@Site_Client_Hier_Id	INT
	@Commodity_id			INT
	@Begin_Dt				DATE
	@End_Dt					DATE
	@Currency_Unit_Id		INT

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	 EXEC Cost_Usage_Most_Recent_Month_Sel_By_Account_Commodity 
			  @Account_Id = 209976
			 ,@Site_Client_Hier_Id = 53868
			 ,@Begin_Dt = '1/1/2011'
			 ,@End_Dt = '12/1/2011'
			 ,@Currency_Unit_Id = 3
			 ,@Commodity_id = 290

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG		Hari
    AP		Athmaram Pabbathi
    KVK		Vinay k
MODIFICATIONS

	Initials	Date		   Modification
------------------------------------------------------------
	HG        04/27/2010	Created for variance page requirement
	HG		04/29/2010		Unit Cost added as one of the row with other bucket values as per the variance page requirement
    DMR		09/10/2010		 Modified for Quoted_Identifier
	AP		08/09/2011		Removed @UOM_Type_ID parameter and modified the conv logic to use default_uom_type_id from bucket_master. 
							   Replaced script for @currency_group_id with client_heir & client_hier_account tables; included "Total Usage" bucket for volume
							   and added additional filters for Is_Required_For_Variance_Test = 1 and Is_Active = 1 on bucket_master
     AP		08/30/2011		Removed CTE and UNION statment used for Unit_Cost calculation, since it is handled in application
     AP		09/09/2011		Added UNION statement back to the script
	AP		09/12/2011		Replaced CTE_Cu_Account_Dtl with @Cost_Usage_Account_Dtl table variable
	AP		03/27/2012		Added @Site_Client_Hier_Id as a parameter and modified SELECT for currency_group_id and removed CHA table
	HG		2012-07-09		MAINT-1355, modified to fix the Unit cost calculation to consider only Total Usage/ Volume determinant
								--UNION changed to UNION ALL as it is always going to return unique set of values
	AKR      2013-07-16  Modified for Detailed Variance, Added UOM_Id and UOM_Name	
	KVK		 10/01/2013  Added recompile statement in the procedure definition		
	SP		 2016-13-10	Variance Test Enhancements,Used CURRENCY_UNIT_COUNTRY_MAP for default currency_id and Country_Commodity_Uom for default uom_id. 	
							
******/
CREATE PROCEDURE [dbo].[Cost_Usage_Most_Recent_Month_Sel_By_Account_Commodity]
      ( 
       @Account_Id INT
      ,@Site_Client_Hier_Id INT
      ,@Commodity_id INT
      ,@Begin_Dt DATE
      ,@End_Dt DATE
      ,@Currency_Unit_Id INT )
      WITH RECOMPILE
AS 
BEGIN

      SET NOCOUNT ON;
	
      DECLARE
            @Currency_Group_Id INT
           ,@Currency_Name VARCHAR(200)
           ,@Usage_Bucket_Stream_Cd INT
           ,@Country_Id INT
           ,@Country_Default_Uom_Type_Id INT

      DECLARE @Cost_Usage_Account_Dtl TABLE
            ( 
             Service_Month DATE
            ,Account_Id INT
            ,Bucket_Type VARCHAR(25)
            ,Bucket_Name VARCHAR(255)
            ,Bucket_Value DECIMAL(28, 10)
            ,Total_Cost DECIMAL(28, 10)
            ,Volume DECIMAL(28, 10)
            ,Recent_Service_Month DATE
            ,UOM_Id INT
            ,UOM_Name VARCHAR(200) )

      SELECT
            @Usage_Bucket_Stream_Cd = cd.Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code cd
                  ON cd.Codeset_Id = cs.Codeset_Id
      WHERE
            cs.Codeset_Name = 'BucketStream'
            AND cd.Code_Value = 'Usage';
            
      SELECT
            @Currency_Group_Id = ch.Client_Currency_Group_Id
           ,@Country_Id = ch.Country_Id
      FROM
            core.Client_Hier ch
      WHERE
            ch.Client_Hier_Id = @Site_Client_Hier_Id

      SELECT
            @Currency_Unit_Id = MAX(cucm.CURRENCY_UNIT_ID)
      FROM
            dbo.CURRENCY_UNIT_COUNTRY_MAP cucm
      WHERE
            cucm.Country_Id = @Country_Id
	              
      SELECT
            @Currency_Name = cu.CURRENCY_UNIT_NAME
      FROM
            dbo.CURRENCY_UNIT cu
      WHERE
            cu.CURRENCY_UNIT_ID = @Currency_Unit_Id     

      SELECT
            @Country_Default_Uom_Type_Id = ccu.Default_Uom_Type_Id
      FROM
            dbo.Country_Commodity_Uom ccu
      WHERE
            ccu.Commodity_Id = @Commodity_Id
            AND ccu.COUNTRY_ID = @Country_Id
                          

      INSERT      INTO @Cost_Usage_Account_Dtl
                  ( 
                   Service_Month
                  ,Account_Id
                  ,Bucket_Type
                  ,Bucket_Name
                  ,Bucket_Value
                  ,Total_Cost
                  ,Volume
                  ,Recent_Service_Month
                  ,UOM_Id
                  ,UOM_Name )
                  SELECT
                        cua.Service_Month
                       ,@Account_Id Account_ID
                       ,bkt_cd.Code_Value Bucket_Type
                       ,bm.Bucket_Name
                       ,cua.Bucket_Value * ( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cc.Conversion_factor
                                                  WHEN bkt_cd.Code_Value = 'Determinant' THEN uc.Conversion_Factor
                                             END ) Bucket_Value
                       ,( CASE WHEN bkt_cd.Code_Value = 'Charge' THEN cua.Bucket_Value * cc.Conversion_Factor
                          END ) AS Total_Cost
                       ,( CASE WHEN bkt_cd.Code_Value = 'Determinant' THEN cua.Bucket_Value * uc.Conversion_Factor
                          END ) AS Volume
                       ,MAX(cua.Service_Month) OVER ( ) Recent_Service_Month
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Unit_Id
                             ELSE ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                         ELSE bm.Default_Uom_Type_Id
                                    END )
                        END UOM_Id
                       ,CASE WHEN bkt_cd.Code_Value = 'Charge' THEN @Currency_Name
                             ELSE en.ENTITY_NAME
                        END UOM_Name
                  FROM
                        dbo.Cost_Usage_Account_Dtl cua
                        INNER JOIN dbo.Bucket_Master bm
                              ON bm.Bucket_Master_Id = cua.Bucket_Master_Id
                        INNER JOIN dbo.Code bkt_cd
                              ON bkt_cd.Code_Id = bm.Bucket_Type_Cd
                        LEFT JOIN dbo.Consumption_Unit_Conversion uc
                              ON uc.Base_Unit_ID = cua.UOM_Type_Id
                                 AND uc.Converted_Unit_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                                   ELSE bm.Default_Uom_Type_Id
                                                              END )
                        LEFT OUTER JOIN dbo.ENTITY en
                              ON en.ENTITY_ID = ( CASE WHEN bm.Bucket_Stream_Cd = @Usage_Bucket_Stream_Cd THEN @Country_Default_Uom_Type_Id
                                                       ELSE bm.Default_Uom_Type_Id
                                                  END )
                        LEFT JOIN dbo.Currency_Unit_Conversion cc
                              ON ( cc.Currency_Group_Id = @Currency_Group_Id
                                   AND cc.Base_Unit_Id = cua.Currency_Unit_ID
                                   AND cc.Converted_Unit_Id = @Currency_Unit_Id
                                   AND cc.Conversion_Date = cua.Service_Month )
                  WHERE
                        cua.Account_ID = @Account_Id
                        AND cua.Client_Hier_Id = @Site_Client_Hier_Id
                        AND cua.Service_Month BETWEEN @Begin_Dt AND @End_Dt
                        AND bm.Commodity_Id = @Commodity_id
                        AND BM.Is_Required_For_Variance_Test = 1
                        AND BM.Is_Active = 1


      SELECT
            cu.Account_Id
           ,cu.Service_Month
           ,cu.Bucket_Name
           ,cu.Bucket_Type
           ,cu.Bucket_Value
           ,cu.UOM_Id
           ,cu.UOM_Name
      FROM
            @Cost_Usage_Account_Dtl cu
      WHERE
            cu.Service_Month = cu.Recent_Service_Month
      UNION ALL
      SELECT
            uc.Account_Id
           ,uc.Service_Month
           ,uc.Bucket_Name
           ,uc.Bucket_Type
           ,uc.Bucket_Value
           ,@Currency_Unit_Id UOM_Id
           ,@Currency_Name UOM_Name
      FROM
            ( SELECT
                  cu.Account_Id
                 ,cu.Service_Month
                 ,'Unit Cost' Bucket_Name
                 ,'Charge' Bucket_Type
                 ,SUM(cu.Total_Cost) / NULLIF(SUM(cu.Volume), 0) Bucket_Value
              FROM
                  @Cost_Usage_Account_Dtl cu
              WHERE
                  cu.Service_Month = cu.Recent_Service_Month
                  AND cu.Bucket_Name IN ( 'Total Cost', 'Total Usage', 'Volume' )
              GROUP BY
                  cu.Account_id
                 ,cu.Service_Month ) uc
      ORDER BY
            Service_Month
           ,Bucket_Type
           ,Bucket_Name

END;


;
GO





GRANT EXECUTE ON  [dbo].[Cost_Usage_Most_Recent_Month_Sel_By_Account_Commodity] TO [CBMSApplication]
GO
