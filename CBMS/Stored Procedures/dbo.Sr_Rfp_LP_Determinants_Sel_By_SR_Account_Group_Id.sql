SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******               
                           
 NAME:  dbo.Sr_Rfp_Supply_Point_Summary_Sel_By_Rfp_Account_Group_Id                  
                              
 DESCRIPTION:                              
                               
                              
 INPUT PARAMETERS:              
                             
 Name					DataType			Default			Description              
---------------------------------------------------------------------
 @SR_RFP_ID				INT
 @SR_RFP_ACCOUNT_ID		INT
 @SR_RFP_BID_GROUP_ID	INT
                                    
 OUTPUT PARAMETERS:                   
                              
 Name				DataType			Default			Description              
---------------------------------------------------------------------
                              
 USAGE EXAMPLES:              
---------------------------------------------------------------------
  
  
   
   EXEC dbo.Sr_Rfp_LP_Determinants_Sel_By_SR_Account_Group_Id 13137,12103934,0
   EXEC dbo.Sr_Rfp_LP_Determinants_Sel_By_SR_Account_Group_Id 13569,12106083,0

  
 AUTHOR INITIALS:              
             
 Initials			Name              
---------------------------------------------------------------------
 RKV				Ravi Kumar Raju                         
                               
 MODIFICATIONS:            
             
 Initials               Date             Modification            
---------------------------------------------------------------------
 RKV                    2016-06-09      Created for GCS Phase - 5.
                             
******/ 
CREATE PROCEDURE [dbo].[Sr_Rfp_LP_Determinants_Sel_By_SR_Account_Group_Id]
      ( 
       @SR_RFP_ID INT
      ,@SR_Account_Group_Id INT
      ,@IS_BID_GROUP BIT )
AS 
BEGIN 
 
      SET NOCOUNT ON 
      
      SELECT
            'Price (' + srlpd.DETERMINANT_NAME + '(' + e.ENTITY_NAME + '))' AS Price
      FROM
            dbo.SR_RFP_LOAD_PROFILE_SETUP srlps
            INNER JOIN dbo.SR_RFP_ACCOUNT sra
                  ON srlps.SR_RFP_ACCOUNT_ID = sra.SR_RFP_ACCOUNT_ID
            INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT srlpd
                  ON srlps.SR_RFP_LOAD_PROFILE_SETUP_ID = srlpd.SR_RFP_LOAD_PROFILE_SETUP_ID
            INNER JOIN dbo.ENTITY e
                  ON srlpd.DETERMINANT_UNIT_TYPE_ID = e.ENTITY_ID
      WHERE
            srlps.SR_RFP_ID = @SR_RFP_ID
            AND ( ( @IS_BID_GROUP = 0
                    AND @SR_Account_Group_Id = sra.SR_RFP_ACCOUNT_ID )
                  OR ( @IS_BID_GROUP = 1
                       AND @SR_Account_Group_Id = sra.SR_RFP_BID_GROUP_ID ) )
           AND srlpd.IS_CHECKED = 1        
      GROUP BY
            'Price (' + srlpd.DETERMINANT_NAME + '(' + e.ENTITY_NAME + '))'

END

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_LP_Determinants_Sel_By_SR_Account_Group_Id] TO [CBMSApplication]
GO
