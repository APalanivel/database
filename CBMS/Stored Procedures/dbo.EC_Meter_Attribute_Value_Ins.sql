SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                    
Name:   dbo.EC_Meter_Attribute_Value_Ins                 
                    
Description:                    
        To insert Data to EC_Meter_Attribute_Value table.                    
                    
 Input Parameters:                    
    Name						DataType			Default				Description                      
----------------------------------------------------------------------------------------                      
    @EC_Meter_Attribute_Id		INT      
    @EC_Meter_Attribute_Value   NVARCHAR      
    @User_Info_Id			INT      
              
 Output Parameters:                          
    Name						DataType			Default				Description                      
----------------------------------------------------------------------------------------                      
                    
 Usage Examples:                        
----------------------------------------------------------------------------------------                      
  DECLARE  @tvp_EC_Meter_Attribute_Value tvp_EC_Meter_Attribute_Value 
 INSERT @tvp_EC_Meter_Attribute_Value    
  SELECT NULL,'EC_Meter_Attribute_Value_testing1'   
  UNION ALL    
  SELECT NULL,'EC_Meter_Attribute_Value_testing2'   

 BEGIN TRAN    
 SELECT * FROM EC_Meter_Attribute_Value WHERE EC_Meter_Attribute_Value in ('EC_Meter_Attribute_Value_testing1','EC_Meter_Attribute_Value_testing2')  
    EXEC dbo.EC_Meter_Attribute_Value_Ins   
      @tvp_EC_Meter_Attribute_Value = @tvp_EC_Meter_Attribute_Value
      ,@EC_Meter_Attribute_Id = 2
     ,@User_Info_Id = 100  
 SELECT * FROM EC_Meter_Attribute_Value WHERE EC_Meter_Attribute_Value in ('EC_Meter_Attribute_Value_testing1','EC_Meter_Attribute_Value_testing2')  
 ROLLBACK TRAN
 
Author Initials:                    
	Initials		Name                    
---------------------------------------------------------------------------------------                      
	NR				Narayana Reddy                     
 Modifications:                    
    Initials        Date		Modification                    
----------------------------------------------------------------------------------------                      
    NR				2015-04-22  Created For AS400.               
                   
******/       
CREATE PROCEDURE [dbo].[EC_Meter_Attribute_Value_Ins]
      ( 
       @tvp_EC_Meter_Attribute_Value tvp_EC_Meter_Attribute_Value READONLY
      ,@EC_Meter_Attribute_Id INT
      ,@User_Info_Id INT )
AS 
BEGIN      
      SET NOCOUNT ON       
            
      INSERT      INTO dbo.EC_Meter_Attribute_Value
                  ( 
                   EC_Meter_Attribute_Value
                  ,EC_Meter_Attribute_Id
                  ,Created_User_Id
                  ,Created_Ts
                  ,Updated_User_Id
                  ,Last_Change_Ts )
                  SELECT
                        EC_Meter_Attribute_Value
                       ,@EC_Meter_Attribute_Id
                       ,@User_Info_Id
                       ,GETDATE()
                       ,@User_Info_Id
                       ,GETDATE()
                  FROM
                        @tvp_EC_Meter_Attribute_Value    
                  
END 
        

;
GO
GRANT EXECUTE ON  [dbo].[EC_Meter_Attribute_Value_Ins] TO [CBMSApplication]
GO
