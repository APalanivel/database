SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
                           
 NAME: dbo.Consolidated_Account_Exception_Cu_Invoice_Map_Upd_Contract_Id                       
                            
 DESCRIPTION:        
  Get the Suppler account associated invoices count based on Supplier Account.                     
                            
 INPUT PARAMETERS:        
                           
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------    
 @Contract_Id						INT          
                            
 OUTPUT PARAMETERS:        
                                 
 Name                               DataType          Default       Description        
-------------------------------------------------------------------------------------                              
 USAGE EXAMPLES:                                
-------------------------------------------------------------------------------------                   
  BEGIN TRAN

SELECT  * FROM  dbo.Account_Exception ae WHERE  ae.Account_Id = 1;
SELECT
    *
FROM
    dbo.Account_Exception_Cu_Invoice_Map aecim2
    INNER JOIN dbo.Account_Exception ae
        ON ae.Account_Exception_Id = aecim2.Account_Exception_Id
WHERE
    ae.Account_Id = 1;


EXEC dbo.Consolidated_Account_Exception_Cu_Invoice_Map_Upd_Contract_Id
    @Contract_Id = 1
    , @User_Info_Id = 49

SELECT  * FROM  dbo.Account_Exception ae WHERE  ae.Account_Id = 1;
SELECT
    *
FROM
    dbo.Account_Exception_Cu_Invoice_Map aecim2
    INNER JOIN dbo.Account_Exception ae
        ON ae.Account_Exception_Id = aecim2.Account_Exception_Id
WHERE
    ae.Account_Id = 1;


ROLLBACK TRAN               
                           
 AUTHOR INITIALS:      
         
 Initials                   Name        
-------------------------------------------------------------------------------------    
 NR                     Narayana Reddy                              
                             
 MODIFICATIONS:      
                             
 Initials               Date            Modification      
-------------------------------------------------------------------------------------    
 NR                     2019-06-13      Created for ADD Contract. 
 NR						2020-06-02		MAINT-10328 Changed the logic to resolve the exception inv begin and end date insted of Servicemonth.                         
                           
******/

CREATE PROC [dbo].[Consolidated_Account_Exception_Cu_Invoice_Map_Upd_Contract_Id]
    (
        @Contract_Id INT
        , @User_Info_Id INT
    )
AS
    BEGIN
        SET NOCOUNT ON;



        DECLARE
            @Missing_Contract_Exception_Type_Cd INT
            , @New_Exception_Status_Cd INT
            , @In_Progress_Exception_Status_Cd INT
            , @Closed_Exception_Status_Cd INT
            , @Counter INT = 1
            , @Total_Row INT
            , @Account_Exception_Id INT
            , @Cu_Invoice_Id INT;



        CREATE TABLE #Account_List
             (
                 Account_Id INT
                 , CONTRACT_START_DATE DATE
                 , CONTRACT_END_DATE DATE
             );

        CREATE TABLE #Cu_Invoice_List
             (
                 Id INT IDENTITY(1, 1)
                 , Account_Exception_Id INT
                 , Cu_Invoice_Id INT
             );



        SELECT
            @Missing_Contract_Exception_Type_Cd = c.Code_Id
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value = 'Missing Contract'
            AND cs.Codeset_Name = 'Exception Type'
            AND cs.Std_Column_Name = 'Exception_Type_Cd';





        SELECT
            @New_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'New' THEN c.Code_Id
                                           END)
            , @In_Progress_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'In Progress' THEN c.Code_Id
                                                     END)
            , @Closed_Exception_Status_Cd = MAX(CASE WHEN c.Code_Value = 'Closed' THEN c.Code_Id
                                                END)
        FROM
            dbo.Code c
            INNER JOIN dbo.Codeset cs
                ON cs.Codeset_Id = c.Codeset_Id
        WHERE
            c.Code_Value IN ( 'New', 'In Progress', 'Closed' )
            AND cs.Codeset_Name = 'Exception Status'
            AND cs.Std_Column_Name = 'Exception_Status_Cd';


        INSERT INTO #Account_List
             (
                 Account_Id
                 , CONTRACT_START_DATE
                 , CONTRACT_END_DATE
             )
        SELECT
            utility_cha.Account_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE
        FROM
            Core.Client_Hier_Account utility_cha
            INNER JOIN Core.Client_Hier_Account Supplier_Cha
                ON Supplier_Cha.Meter_Id = utility_cha.Meter_Id
                   AND  Supplier_Cha.Client_Hier_Id = utility_cha.Client_Hier_Id
            INNER JOIN dbo.CONTRACT c
                ON Supplier_Cha.Supplier_Contract_ID = c.CONTRACT_ID
            INNER JOIN dbo.Account_Consolidated_Billing_Vendor acbv
                ON utility_cha.Account_Id = acbv.Account_Id
        WHERE
            Supplier_Cha.Supplier_Contract_ID = @Contract_Id
            AND utility_cha.Account_Type = 'Utility'
            AND Supplier_Cha.Account_Type = 'Supplier'
            AND (   c.CONTRACT_START_DATE BETWEEN acbv.Billing_Start_Dt
                                          AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  c.CONTRACT_END_DATE BETWEEN acbv.Billing_Start_Dt
                                            AND     ISNULL(acbv.Billing_End_Dt, '9999-12-31')
                    OR  acbv.Billing_Start_Dt BETWEEN c.CONTRACT_START_DATE
                                              AND     c.CONTRACT_END_DATE
                    OR  ISNULL(acbv.Billing_End_Dt, '9999-12-31') BETWEEN c.CONTRACT_START_DATE
                                                                  AND     c.CONTRACT_END_DATE)
        GROUP BY
            utility_cha.Account_Id
            , c.CONTRACT_START_DATE
            , c.CONTRACT_END_DATE;

        INSERT INTO #Cu_Invoice_List
             (
                 Account_Exception_Id
                 , Cu_Invoice_Id
             )
        SELECT
            aecim.Account_Exception_Id
            , aecim.Cu_Invoice_Id
        FROM
            #Account_List al
            INNER JOIN dbo.Account_Exception ae
                ON ae.Account_Id = al.Account_Id
            INNER JOIN dbo.Account_Exception_Cu_Invoice_Map aecim
                ON aecim.Account_Exception_Id = ae.Account_Exception_Id
        WHERE
            ae.Exception_Type_Cd = @Missing_Contract_Exception_Type_Cd
            AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
            AND EXISTS (   SELECT
                                1
                           FROM
                                dbo.CU_INVOICE_SERVICE_MONTH cism
                           WHERE
                                cism.Account_ID = ae.Account_Id
                                AND cism.CU_INVOICE_ID = aecim.Cu_Invoice_Id
                                AND (   al.CONTRACT_START_DATE BETWEEN cism.Begin_Dt
                                                               AND     cism.End_Dt
                                        OR  al.CONTRACT_END_DATE BETWEEN cism.Begin_Dt
                                                                 AND     cism.End_Dt
                                        OR  cism.Begin_Dt BETWEEN al.CONTRACT_START_DATE
                                                          AND     al.CONTRACT_END_DATE
                                        OR  cism.End_Dt BETWEEN al.CONTRACT_START_DATE
                                                        AND     al.CONTRACT_END_DATE))
        GROUP BY
            aecim.Account_Exception_Id
            , aecim.Cu_Invoice_Id;


        SELECT  @Total_Row = MAX(cil.Id)FROM    #Cu_Invoice_List cil;

        WHILE (@Counter <= @Total_Row)
            BEGIN


                SELECT
                    @Account_Exception_Id = cil.Account_Exception_Id
                    , @Cu_Invoice_Id = cil.Cu_Invoice_Id
                FROM
                    #Cu_Invoice_List cil
                WHERE
                    cil.Id = @Counter;



                UPDATE
                    aecim
                SET
                    aecim.Status_Cd = @Closed_Exception_Status_Cd
                    , aecim.Last_Change_Ts = GETDATE()
                    , aecim.Updated_User_Id = @User_Info_Id
                FROM
                    dbo.Account_Exception_Cu_Invoice_Map aecim
                WHERE
                    aecim.Account_Exception_Id = @Account_Exception_Id
                    AND aecim.Cu_Invoice_Id = @Cu_Invoice_Id
                    AND aecim.Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd );



                UPDATE
                    ae
                SET
                    ae.Exception_Status_Cd = @Closed_Exception_Status_Cd
                    , ae.Last_Change_Ts = GETDATE()
                    , ae.Updated_User_Id = @User_Info_Id
                    , ae.Closed_By_User_Id = @User_Info_Id
                    , ae.Closed_Ts = GETDATE()
                FROM
                    dbo.Account_Exception ae
                WHERE
                    ae.Account_Exception_Id = @Account_Exception_Id
                    AND ae.Exception_Status_Cd IN ( @New_Exception_Status_Cd, @In_Progress_Exception_Status_Cd )
                    AND NOT EXISTS (   SELECT
                                            1
                                       FROM
                                            dbo.Account_Exception_Cu_Invoice_Map aecim
                                       WHERE
                                            aecim.Account_Exception_Id = ae.Account_Exception_Id
                                            AND aecim.Cu_Invoice_Id <> @Cu_Invoice_Id
                                            AND aecim.Status_Cd IN ( @New_Exception_Status_Cd
                                                                     , @In_Progress_Exception_Status_Cd ));



                SET @Counter = @Counter + 1;

            END;



    END;

GO
GRANT EXECUTE ON  [dbo].[Consolidated_Account_Exception_Cu_Invoice_Map_Upd_Contract_Id] TO [CBMSApplication]
GO
