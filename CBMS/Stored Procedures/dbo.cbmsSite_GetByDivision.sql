
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
Name:  
    dbo.cbmsSite_GetByDivision
 
Description:  
    Used for returning the list of Site's for the given filter criteria.
 
Input Parameters:  
    Name			    DataType	 Default Description  
------------------------------------------------------------------------  
    @MyAccountId	    INT
    @Client_Id		    INT		 NULL
    @Division_Id	    INT		 NULL
    @Country_Id	    INT		 NULL
 
Output Parameters:  
 Name  Datatype  Default Description  
------------------------------------------------------------  
 
Usage Examples:
------------------------------------------------------------  
    EXECUTE dbo.cbmsSite_GetByDivision 
      @MyAccountId = 49
     ,@Client_Id = 10069
     ,@Division_Id = NULL
     ,@Country_Id = NULL 
 
Author Initials:  
 Initials	  Name
------------------------------------------------------------
 AP		  Athmaram Pabbathi
 
 Modifications :  
 Initials	  Date	    Modification  
------------------------------------------------------------  
 AP		  2012-03-13  Modified to use Core.Client_Hier table & added Client_Hier_Id in the return result as a part of Addl Data

******/
CREATE  PROCEDURE dbo.cbmsSite_GetByDivision
      ( 
       @MyAccountId INT
      ,@Client_Id INT = NULL
      ,@Division_Id INT = NULL
      ,@Country_Id INT = NULL )
AS 
BEGIN  
      SET NOCOUNT ON  
  
  
      SELECT
            @Client_Id = ui.Client_Id
      FROM
            User_Info ui
      WHERE
            ui.User_Info_Id = @MyAccountId
            AND @Client_Id IS NULL 
  
  
      SELECT
            ch.Client_Hier_Id
           ,ch.Site_Id
           ,rtrim(ch.City) + ', ' + ch.state_name + ' (' + ch.Site_Name + ')' AS Site_Name
           ,ch.Sitegroup_Name AS Division_Name
           ,ch.Sitegroup_Id AS Division_Id
           ,ch.City
           ,ch.Site_Address_Line1 AS Address_Line1
           ,ch.Site_Address_Line2 AS Address_Line2
      FROM
            Core.Client_Hier ch
      WHERE
            ( @Division_Id IS NULL
              OR ch.Sitegroup_Id = @Division_Id )
            AND ( @Client_Id IS NULL
                  OR ch.Client_Id = @Client_Id )
            AND ( @Country_Id IS NULL
                  OR ch.Country_Id = @Country_Id )
            AND ch.Site_Id > 0
      ORDER BY
            Site_Name
  
END
;
GO

GRANT EXECUTE ON  [dbo].[cbmsSite_GetByDivision] TO [CBMSApplication]
GO
