SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE dbo.UPDATE_LEGAL_STRUCTURE_IMAGE_ID_FOR_SITE_P
	@legalStructureCbmsImageID int,
	@siteID int
	AS
	begin
		set nocount on

		update site 
		set    LEGAL_STRUCTURE_CBMS_IMAGE_ID = @legalStructureCbmsImageID 
		where  site_id = @siteID

	end
GO
GRANT EXECUTE ON  [dbo].[UPDATE_LEGAL_STRUCTURE_IMAGE_ID_FOR_SITE_P] TO [CBMSApplication]
GO
