SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********       
NAME:  dbo.Account_Commodity_Analyst_Del      
     
DESCRIPTION: Deletes Analyst for a given Account and commodity from Account_Commodity_Analyst table.
    
INPUT PARAMETERS:        
      Name					DataType          Default     Description        
------------------------------------------------------------        
	  @Account_Id          	INT
      @Commodity_Id         INT
        
        
OUTPUT PARAMETERS:        
      Name              DataType          Default     Description        
------------------------------------------------------------        
        
USAGE EXAMPLES:   

BEGIN TRAN
EXEC DBO.Account_Commodity_Analyst_Del 100,290
ROLLBACK TRAN

------------------------------------------------------------  
    
AUTHOR INITIALS:      
Initials Name      
------------------------------------------------------------      
AKR		 Ashok Kumar Raju
    
    
Initials Date		Modification      
------------------------------------------------------------      
AKR		2012-09-28  Created new sp (for POCO Project)
******/
CREATE PROCEDURE dbo.Account_Commodity_Analyst_Del
      ( 
       @Account_Id INT
      ,@Commodity_Id INT )
AS 
BEGIN
      SET NOCOUNT ON ;
      
      DELETE
            aca
      FROM
            dbo.Account_Commodity_Analyst aca
      WHERE
            aca.Account_Id = @Account_Id
            AND aca.Commodity_Id = @Commodity_Id
                  
END
;
GO
GRANT EXECUTE ON  [dbo].[Account_Commodity_Analyst_Del] TO [CBMSApplication]
GO
