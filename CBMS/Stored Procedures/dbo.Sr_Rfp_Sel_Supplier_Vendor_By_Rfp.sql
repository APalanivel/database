SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
Name:   dbo.Sr_Rfp_Sel_Supplier_Vendor_By_Rfp       
              
Description:              
			
              
 Input Parameters:              
    Name			DataType		Default		Description                
----------------------------------------------------------------------------------------                
	@SR_RFP_ID		INT				
    
 Output Parameters:                    
    Name						DataType		Default		Description                
----------------------------------------------------------------------------------------                
              
 Usage Examples:                  
----------------------------------------------------------------------------------------   
   
	Exec dbo.Sr_Rfp_Sel_Supplier_Vendor_By_Rfp 5
	Exec dbo.Sr_Rfp_Sel_Supplier_Vendor_By_Rfp 13215
	Exec dbo.Sr_Rfp_Sel_Supplier_Vendor_By_Rfp 13162
	
Author Initials:              
	Initials		Name              
----------------------------------------------------------------------------------------                
	RR				Raghu Reddy               
	
Modifications:              
    Initials    Date		Modification              
----------------------------------------------------------------------------------------                
    RR			2016-01-04	Global Sourcing - Phase3 - GCS-428 Created  
				2016-04-21	GCS-679 Removed join on user state(personal) as state is not mandatory in user registration
             
******/ 
CREATE PROCEDURE [dbo].[Sr_Rfp_Sel_Supplier_Vendor_By_Rfp] ( @SR_RFP_ID INT )
AS 
BEGIN

      SET NOCOUNT ON;

            
      SELECT
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      FROM
            dbo.VENDOR vndr
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP sscvm
                  ON vndr.VENDOR_ID = sscvm.VENDOR_ID
            INNER JOIN dbo.SR_SUPPLIER_CONTACT_VENDOR_MAP dist
                  ON dist.SR_SUPPLIER_CONTACT_INFO_ID = sscvm.SR_SUPPLIER_CONTACT_INFO_ID
      WHERE
            sscvm.IS_PRIMARY = 1
            AND dist.IS_PRIMARY = 0
            AND EXISTS ( SELECT
                              1
                         FROM
                              dbo.SR_RFP_ACCOUNT rfpacc
                              INNER JOIN Core.Client_Hier_Account cha
                                    ON rfpacc.ACCOUNT_ID = cha.Account_Id
                         WHERE
                              rfpacc.SR_RFP_ID = @SR_RFP_ID
                              AND cha.Commodity_Id = dist.COMMODITY_TYPE_ID
                              AND cha.Meter_Country_Id = dist.COUNTRY_ID )
      GROUP BY
            vndr.VENDOR_ID
           ,vndr.VENDOR_NAME
      ORDER BY
            vndr.VENDOR_NAME

END;

;

;
GO
GRANT EXECUTE ON  [dbo].[Sr_Rfp_Sel_Supplier_Vendor_By_Rfp] TO [CBMSApplication]
GO
