SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 
 dbo.SiteGroup_INS
 
 DESCRIPTION:   
 
	This SP insert the SiteGroup data 
 
 INPUT PARAMETERS:  
 Name			DataType	Default		Description  
------------------------------------------------------------    
	@Sitegroup_Name VARCHAR(200)
	, @Sitegroup_Type_Cd INT
	, @Client_Id INT
	, @Is_Smart_Group BIT
	, @Owner_User_Id INT
	, @Add_User_Id INT
	, @Is_Locked BIT = 0
	, @Is_Complete BIT = 1
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
  USAGE EXAMPLES:  
------------------------------------------------------------  

	EXEC dbo.SiteGroup_INS 

AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 HG			Hari    
 
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 HG			6/21/2009	Modified the default Date for modified date to current date.
						    
  
******/

CREATE PROCEDURE [dbo].[SiteGroup_INS](
	@Sitegroup_Name VARCHAR(200)
	, @Sitegroup_Type_Cd INT
	, @Client_Id INT
	, @Is_Smart_Group BIT
	, @Owner_User_Id INT
	, @Add_User_Id INT
	, @Is_Locked BIT = 0
	, @Is_Complete BIT = 1
	)
AS
BEGIN
	
	SET NOCOUNT ON
	
	INSERT INTO dbo.Sitegroup
		(Sitegroup_Name
		, Sitegroup_Type_Cd
		, Client_Id
		, Is_Smart_Group
		, Is_Publish_To_DV
		, Owner_User_Id
		, Add_User_Id
		, Add_Dt
		, Mod_Dt
		, Is_Locked
		, Is_Complete)
	VALUES(@Sitegroup_Name
		, @Sitegroup_Type_Cd
		, @Client_Id
		, @Is_Smart_Group
		, 0
		, @Owner_User_Id
		, @Add_User_Id
		, GETDATE()
		, GETDATE()
		, @Is_Locked
		, @Is_Complete)
		
	SELECT SCOPE_IDENTITY() AS SiteGroup_Id
           
END

GO
GRANT EXECUTE ON  [dbo].[SiteGroup_INS] TO [CBMSApplication]
GO
