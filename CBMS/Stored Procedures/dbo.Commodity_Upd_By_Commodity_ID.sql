
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
dbo.Commodity_Upd_By_Commodity_Id  
  
DESCRIPTION:  

	To update the service details for the given commodity
  
INPUT PARAMETERS:  
Name							DataType			Default	Description  
-----------------------------------------------------------------------
@Commodity_Id					INT					
@Commodity_Name					VARCHAR(50)
@Commodity_Dsc					VARCHAR(200)		NULL
@Is_Weather_Adjusted			BIT
@Service_Mapping_Instructions	VARCHAR(MAX)
@Default_Uom_Cd					INT
@Associated_Uom_Cd				VARCHAR(MAX)
@Last_Change_By_User_Id			INT

OUTPUT PARAMETERS:
Name				DataType  Default	Description
------------------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------------------

BEGIN TRAN


	EXEC dbo.Commodity_Upd_By_Commodity_Id 
		@Commodity_Id = 57
		,@Commodity_Name = 'Coal'
		,@Commodity_Dsc = 'Coal'
		,@Is_Weather_Adjusted = 1
		,@Service_Mapping_Instructions = NULL
		,@Default_Uom_Cd = 100109
		,@Associated_Uom_Cd = '100089,100095,100109,100086,100098,100058'
		,@Last_Change_By_User_Id = 16
		,@Is_Client_Specific = 1

ROLLBACK TRAN


AUTHOR INITIALS:
 Initials		Name
------------------------------------------------------------------------  
 GB				Geetansu Behera
 CMH			Chad Hattabaugh
 HG				Hariharasuthan Ganesan


MODIFICATIONS
 Initials	Date		Modification
------------------------------------------------------------------------
 GB						created
 SKA		17-Aug-09	Reformat the script
 SKA		25 Aug 2009	Removed unused parameter (@IS_ALTERNATE_FUEL)
 SS			06-SEP-2009	To make Update SPs consistent,SP modified to return rowcount
 DMR		09/10/2010	Modified for Quoted_Identifier
 HG			2013-04-23	Modified for manage service requirement
 HG			2013-09-04	MAINT-2166, changed the logic to get the entity_description as the EP, NG and other services are having different pattern for entity description

******/

CREATE PROCEDURE [dbo].[Commodity_Upd_By_Commodity_ID]
      ( 
       @Commodity_Id INT
      ,@Commodity_Name VARCHAR(50)
      ,@Commodity_Dsc VARCHAR(200) = NULL
      ,@Is_Weather_Adjusted BIT
      ,@Service_Mapping_Instructions VARCHAR(MAX)
      ,@Default_Uom_Cd INT
      ,@Associated_Uom_Cd VARCHAR(MAX)
      ,@Last_Change_By_User_Id INT
      ,@Is_Client_Specific BIT )
AS 
BEGIN

      SET NOCOUNT ON
		-- Update changes in Commodity
		-- Insert the new Uom in to Entity
		-- Save the conversion factors in to Consumption_Unit_Conversion &  Commodity_Uom_Conversion table


      DECLARE @Commodity_Changes TABLE
            ( 
             Old_Commodity_Name VARCHAR(50)
            ,Old_Default_Uom_Entity_Type_Id INT )

      DECLARE
            @Default_Uom_Entity_Type_Id INT
           ,@Uom_Entity_Type INT
           ,@Is_Commodity_Name_Changed BIT
           ,@Entity_Description VARCHAR(200) = case @Commodity_Name
                                                 WHEN 'Electric Power' THEN 'Unit for electricity'
                                                 WHEN 'Natural Gas' THEN 'Unit for Gas'
                                                 WHEN 'Alternative Natural Gas' THEN 'Unit for alternate fuel'
                                                 ELSE 'Units for ' + @Commodity_Name
                                               END
           ,@Old_Default_Uom_Type_Id INT

      BEGIN TRY
            BEGIN TRAN

            EXEC dbo.Commodity_Uom_Ins_Into_Entity 
                  @Commodity_Id = @Commodity_Id
                 ,@Associated_Uom_Cd = @Associated_Uom_Cd
                 ,@Uom_Entity_Type_Id = @Uom_Entity_Type OUT

            SELECT
                  @Default_Uom_Entity_Type_Id = uomid.ENTITY_ID
            FROM
                  dbo.Code uomcd
                  INNER JOIN dbo.Entity uomid
                        ON uomid.ENTITY_NAME = uomcd.Code_Value
            WHERE
                  uomcd.Code_Id = @Default_Uom_Cd
                  AND uomid.Entity_Type = @Uom_Entity_Type

            UPDATE
                  com
            SET   
                  com.Commodity_Name = @Commodity_Name
                 ,com.Commodity_Dsc = isnull(nullif(@Commodity_Dsc, ''), @Commodity_Name)
                 ,com.Is_Weather_Adjusted = @Is_Weather_Adjusted
                 ,com.Is_Client_Specific = @Is_Client_Specific
                 ,com.Service_Mapping_Instructions = @Service_Mapping_Instructions
                 ,com.Default_UOM_Entity_Type_Id = @Default_Uom_Entity_Type_Id
                 ,com.Last_Change_By_User_Id = @Last_Change_By_User_Id
                 ,com.Last_Change_Ts = getdate()
            OUTPUT
                  DELETED.Commodity_Name
                 ,DELETED.Default_Uom_Entity_Type_Id
                  INTO @Commodity_Changes
                        ( Old_Commodity_Name, Old_Default_Uom_Entity_Type_Id )
            FROM
                  dbo.Commodity com
            WHERE
                  com.Commodity_Id = @Commodity_Id
 
            SELECT
                  @Is_Commodity_Name_Changed = case WHEN @Commodity_Name = Old_Commodity_Name THEN 0
                                                    ELSE 1
                                               END
                 ,@Old_Default_Uom_Type_Id = Old_Default_Uom_Entity_Type_Id
            FROM
                  @Commodity_Changes
 
            UPDATE
                  uom
            SET   
                  ENTITY_DESCRIPTION = @Entity_Description
            FROM
                  dbo.Entity uom
            WHERE
                  @Is_Commodity_Name_Changed = 1
                  AND uom.ENTITY_TYPE = @Uom_Entity_Type
                  AND uom.ENTITY_DESCRIPTION <> @Entity_Description

            UPDATE
                  bm
            SET   
                  Default_Uom_Type_Id = @Default_Uom_Entity_Type_Id
            FROM
                  dbo.Bucket_Master bm
            WHERE
                  Commodity_Id = @Commodity_Id
                  AND Default_Uom_Type_Id = @Old_Default_Uom_Type_Id
                  AND @Old_Default_Uom_Type_Id <> @Default_Uom_Entity_Type_Id
 
            COMMIT TRAN
      END TRY
      BEGIN CATCH
            IF @@TRANCOUNT > 0 
                  BEGIN
                        ROLLBACK
                  END

            EXEC dbo.usp_RethrowError

      END CATCH

END;



;
GO



GRANT EXECUTE ON  [dbo].[Commodity_Upd_By_Commodity_ID] TO [CBMSApplication]
GO
