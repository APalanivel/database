SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [dbo].[cbmsUbmInvoiceBatch_GetForProcessing]
	( @MyAccountId int )
AS
BEGIN
	   select ubm.ubm_name
		, i.ubm_batch_master_log_id
		, count(*) invoice_count
	     from ubm_invoice i  with (nolock)
	     join ubm_batch_master_log l with (nolock) on l.ubm_batch_master_log_id = i.ubm_batch_master_log_id 
	     join ubm with (nolock) on ubm.ubm_id = l.ubm_id
	    where i.cu_invoice_batch_id is null
	      and i.is_processed = 0
	      and i.is_quarterly = 0
	      and i.ubm_client_id is not null
	      and i.cbms_image_id is not null
	 group by ubm.ubm_name 
		, i.ubm_batch_master_log_id
		with rollup
	 order by ubm.ubm_name
		, i.ubm_batch_master_log_id
END


GO
GRANT EXECUTE ON  [dbo].[cbmsUbmInvoiceBatch_GetForProcessing] TO [CBMSApplication]
GO
