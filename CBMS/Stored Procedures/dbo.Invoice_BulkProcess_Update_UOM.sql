SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******                                                
NAME:    [Invoice_BulkProcess_Update_UOM]                                          
DESCRIPTION:                                             
------------------------------------------------------------                                               
 Unit tests    
 [Invoice_BulkProcess_Update_UOM]     4975                                            
------------------------------------------------------------                                                
AUTHOR INITIALS:                                                
Initials Name                                                
------------------------------------------------------------                                                
PK   Prasan kumar                          
                                             
 MODIFICATIONS                                                 
 Initials  Date  Modification                                                
------------------------------------------------------------                                                
                          
******/
CREATE PROCEDURE [dbo].[Invoice_BulkProcess_Update_UOM]
(@batch_detail_id INT)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE
		@statusfail INT
		, @statuspass INT
		, @statusinprogress INT
		, @statuspassauto INT
		, @Current_Bucket_Name VARCHAR(255)
		, @New_Bucket_Name VARCHAR(255)
		, @Current_Sub_Bucket_Name VARCHAR(255)
		, @New_Sub_Bucket_Name VARCHAR(255)
		, @Bucket_code VARCHAR(10)
		, @user_id INT
		, @New_bucket_id INT
		, @New_sub_bucket_id INT
		, @Invoice_id INT
		, @IsUpdatable BIT = 0
		, @IsAutoSubBucketPerformed BIT = 0
		, @Current_UOM VARCHAR(20)
		, @New_UOM VARCHAR(20)
		, @New_UOM_id INT
		, @Commodity_type_id INT
		, @ubm_id INT
		, @state_id INT;


	BEGIN TRY


		CREATE TABLE #Uom_Bucket_Dtl_Map
		(
			Uom_Id	   INT
			, Uom_Name VARCHAR(255)
		);

		CREATE TABLE #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
		(
			EC_Invoice_Sub_Bucket_Master_Id INT
			, Sub_Bucket_Name				VARCHAR(255)
		);


		SELECT
			@user_id = [b].[Requested_User_Id]
			, @Bucket_code = [b_detail].[Bucket_Code]
			, @Current_Bucket_Name = [b_detail].[Current_Bucket_Name]
			, @New_Bucket_Name = [b_detail].[New_Bucket_Name]
			, @Current_Sub_Bucket_Name = [b_detail].[Current_Sub_Bucket_Name]
			, @New_Sub_Bucket_Name = [b_detail].[New_Sub_Bucket_Name]
			, @Invoice_id = [b_detail].[Cu_Invoice_Id]
			, @Current_UOM = [b_detail].[Current_Uom_Name]
			, @New_UOM = [b_detail].[New_Uom_Name]
		FROM
			[LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] AS [b_detail]
			, [LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch] AS [b]
		WHERE
			[b_detail].[XL_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id
			AND [b_detail].[XL_Bulk_Invoice_Process_Batch_Id] = [b].[XL_Bulk_Invoice_Process_Batch_Id];

		SELECT
			@statusinprogress = MAX(CASE WHEN [cd].[Code_Value] = 'In Progress' THEN [cd].[Code_Id] END)
			, @statuspass = MAX(CASE WHEN [cd].[Code_Value] = 'Completed' THEN [cd].[Code_Id] END)
			, @statusfail = MAX(CASE WHEN [cd].[Code_Value] = 'Error' THEN [cd].[Code_Id] END)
			, @statuspassauto = MAX(   CASE WHEN [cd].[Code_Value] = 'Completed Auto Sub bucket' THEN
												[cd].[Code_Id]
									   END
								   )
		FROM
			[dbo].[Code] AS [cd]
			INNER JOIN [dbo].[Codeset] AS [cs]
				ON [cs].[Codeset_Id] = [cd].[Codeset_Id]
		WHERE
			[cd].[Is_Active] = 1 AND [cs].[Codeset_Name] = 'Batch Status';



		UPDATE
			[LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]
		SET
			[Xl_Bulk_Invoice_Process_Batch_Dtl].[Status_Cd] = @statusinprogress
		WHERE
			[Xl_Bulk_Invoice_Process_Batch_Dtl].[XL_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id;


		SELECT
			@Current_Bucket_Name = MAX([bm].[Bucket_Name])
			, @Current_Sub_Bucket_Name = MAX([sb].[Sub_Bucket_Name])
			, @Current_UOM = MAX([e].[ENTITY_NAME])
			, @Commodity_type_id = MAX([id].COMMODITY_TYPE_ID)
		FROM
			[dbo].[CU_INVOICE_DETERMINANT] AS [id]
			INNER JOIN [dbo].[Bucket_Master] AS [bm]
				ON [bm].[Bucket_Master_Id] = [id].[Bucket_Master_Id]
			LEFT JOIN [dbo].[ENTITY] AS [e]
				ON [id].[UNIT_OF_MEASURE_TYPE_ID] = [e].[ENTITY_ID]
			LEFT JOIN [dbo].[EC_Invoice_Sub_Bucket_Master] AS [sb]
				ON [sb].[EC_Invoice_Sub_Bucket_Master_Id] = [id].[EC_Invoice_Sub_Bucket_Master_Id]
		WHERE
			[id].[CU_INVOICE_ID] = @Invoice_id AND [id].[CU_DETERMINANT_CODE] = @Bucket_code;


		SELECT @ubm_id = ci.UBM_ID FROM dbo.CU_INVOICE ci WHERE ci.CU_INVOICE_ID = @Invoice_id;
		SELECT
			@state_id = [cha].[Meter_State_Id]
		FROM
			[dbo].[CU_INVOICE_SERVICE_MONTH] AS [sm]
			INNER JOIN [Core].[Client_Hier_Account] AS [cha]
				ON [cha].[Account_Id] = [sm].[Account_ID]
		WHERE
			[sm].[CU_INVOICE_ID] = @Invoice_id;


		SELECT
			@New_bucket_id = [bm].[Bucket_Master_Id]
		FROM
			[dbo].[Bucket_Master] AS [bm]
			INNER JOIN [dbo].[Code] AS [c]
				ON [bm].[Bucket_Type_Cd] = [c].[Code_Id]
		WHERE
			[c].[Code_Value] = 'Determinant'
			AND [bm].[Is_Shown_On_Invoice] = 1
			AND [bm].[Bucket_Name] = @New_Bucket_Name
			AND [bm].[Commodity_Id] = @Commodity_type_id;


		INSERT INTO #Uom_Bucket_Dtl_Map
			(
				Uom_Id
				, Uom_Name
			)
		SELECT
			Uom_Type_Id AS Uom_Id
			, ENTITY_NAME AS Uom_Name
		FROM
			[dbo].[Bucket_Master_UOM_Map] AS [uom]
			INNER JOIN [dbo].[ENTITY] AS [e]
				ON [uom].[Uom_Type_Id] = [e].[ENTITY_ID]
		WHERE
			[uom].Bucket_Master_Id = @New_bucket_id;

		SELECT @New_UOM_id = Uom_Id FROM #Uom_Bucket_Dtl_Map WHERE Uom_Name = @New_UOM;

		DELETE FROM #Uom_Bucket_Dtl_Map WHERE @New_UOM_id IS NULL;
		INSERT INTO #Uom_Bucket_Dtl_Map
			(
				Uom_Id
				, Uom_Name
			)
		SELECT
			uom.ENTITY_ID AS Uom_Id
			, uom.ENTITY_NAME AS Uom_Name
		FROM
			Commodity com
			JOIN dbo.ENTITY uom
				ON uom.ENTITY_TYPE = com.UOM_Entity_Type
		WHERE
			com.Commodity_Name NOT IN ( 'Electric Power', 'Natural Gas' )
			AND com.[Commodity_Id] = @Commodity_type_id
			AND @New_UOM_id IS NULL;

		SELECT @New_UOM_id = Uom_Id FROM #Uom_Bucket_Dtl_Map WHERE Uom_Name = @New_UOM AND @New_UOM_id IS NULL;


		IF @New_bucket_id IS NOT NULL AND @New_UOM_id IS NOT NULL SET @IsUpdatable = 1;

		INSERT INTO #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
			(
				EC_Invoice_Sub_Bucket_Master_Id
				, Sub_Bucket_Name
			)
		SELECT
			eisbm.EC_Invoice_Sub_Bucket_Master_Id
			, eisbm.Sub_Bucket_Name
		FROM
			dbo.EC_Invoice_Sub_Bucket_Master eisbm
		WHERE
			eisbm.State_Id = @state_id AND eisbm.Bucket_Master_Id = @New_bucket_id;

		IF @IsUpdatable = 1 AND EXISTS (
										   SELECT 1 FROM #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
									   )
		BEGIN


			SELECT
				@New_sub_bucket_id = EC_Invoice_Sub_Bucket_Master_Id
			FROM
				#Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
			WHERE
				Sub_Bucket_Name = @New_Sub_Bucket_Name;


			--AUTO MAPPING              
			IF @New_sub_bucket_id IS NULL
			BEGIN

				SELECT
					@New_sub_bucket_id = EC_Invoice_Sub_Bucket_Master_Id
				FROM
					#Ec_Sub_Invoice_Bucket_Dtl_Uom_Map eisbm
				WHERE
					EXISTS (
							   SELECT
								   1
							   FROM
								   dbo.EC_Invoice_Sub_Bucket_UBM_Uom_Map um_mp
							   WHERE
								   um_mp.EC_Invoice_Sub_Bucket_Master_Id = eisbm.EC_Invoice_Sub_Bucket_Master_Id
								   AND um_mp.Uom_Id = @New_UOM_id
								   AND um_mp.Ubm_Id = @ubm_id
						   );

				SELECT
					@New_sub_bucket_id = MIN(eisbm.EC_Invoice_Sub_Bucket_Master_Id)
				FROM
					dbo.EC_Invoice_Sub_Bucket_Master eisbm
					INNER JOIN dbo.Bucket_Master bm
						ON eisbm.Bucket_Master_Id = bm.Bucket_Master_Id
				WHERE
					eisbm.Bucket_Master_Id = @New_bucket_id
					AND eisbm.State_Id = @state_id
					AND @New_sub_bucket_id IS NULL
				GROUP BY
					eisbm.Bucket_Master_Id
					, bm.Commodity_Id
				HAVING
					COUNT(DISTINCT eisbm.EC_Invoice_Sub_Bucket_Master_Id) = 1; -- Added to get the subbucket only when one to one mapping exists  

				SET @IsAutoSubBucketPerformed = 1;


			END;

			SET @IsUpdatable = CASE WHEN @New_sub_bucket_id IS NOT NULL THEN 1 ELSE 0 END;

		END;
		ELSE SET @IsUpdatable = CASE WHEN @IsUpdatable = 1 AND @New_Sub_Bucket_Name = '' THEN 1 ELSE 0 END;


		UPDATE
			[LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]
		SET
			New_Sub_Bucket_Name = (
									  SELECT
										  Sub_Bucket_Name
									  FROM
										  #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
									  WHERE
										  EC_Invoice_Sub_Bucket_Master_Id = @New_sub_bucket_id
								  )
		WHERE
			XL_Bulk_Data_Process_Batch_Dtl_Id = @batch_detail_id AND @IsUpdatable = 1 AND @IsAutoSubBucketPerformed = 1;


		UPDATE
			[id]
		SET
			[id].[Bucket_Master_Id] = @New_bucket_id
			, [id].[EC_Invoice_Sub_Bucket_Master_Id] = @New_sub_bucket_id
			, [id].[UNIT_OF_MEASURE_TYPE_ID] = @New_UOM_id
		FROM
			[dbo].[CU_INVOICE_DETERMINANT] AS [id]
		WHERE
			[id].[CU_INVOICE_ID] = @Invoice_id AND [id].[CU_DETERMINANT_CODE] = @Bucket_code AND @IsUpdatable = 1;

		UPDATE
			[LOGDB].[dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]
		SET
			[Status_Cd] = CASE WHEN @IsUpdatable = 1 AND @IsAutoSubBucketPerformed = 1 THEN
								   @statuspassauto
							  WHEN @IsUpdatable = 1 THEN
								  @statuspass
							  ELSE
								  @statusfail
						  END
			, [Error_Msg] = CASE WHEN @New_bucket_id IS NULL THEN
									 'Fail. Incorrect Bucket Name Entered.'
								WHEN @New_UOM_id IS NULL THEN
									'Fail. Incorrect UOM Name Entered.'
								WHEN @New_sub_bucket_id IS NULL
									 AND @New_Sub_Bucket_Name <> ''
									 AND @IsAutoSubBucketPerformed = 0
									 AND NOT EXISTS (
														SELECT 1 FROM #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
													) THEN
									'Fail. Incorrect Sub-Bucket Name Entered.'
								WHEN @New_sub_bucket_id IS NULL AND @IsAutoSubBucketPerformed = 1 THEN
									'Fail. Auto-Mapping Couldn''t find Sub - Bucket.'
							END
		WHERE
			[XL_Bulk_Data_Process_Batch_Dtl_Id] = @batch_detail_id;

		SELECT
			@New_Sub_Bucket_Name = Sub_Bucket_Name
		FROM
			#Ec_Sub_Invoice_Bucket_Dtl_Uom_Map
		WHERE
			EC_Invoice_Sub_Bucket_Master_Id = @New_sub_bucket_id;

		INSERT INTO [dbo].[CU_INVOICE_CHANGE_LOG]
			(
				[CU_INVOICE_ID]
				, [CHANGE_TYPE_ID]
				, [FIELD_TYPE_ID]
				, [FIELD_NAME]
				, [PREVIOUS_VALUE]
				, [CURRENT_VALUE]
				, [CHANGE_DATE]
				, [Bucket_Number]
				, [Updated_User_Id]
			)
		SELECT
			[bucket_values].[cu_invoice_id]
			, [bucket_values].[change_type_id]
			, [bucket_values].[field_type_id]
			, [bucket_values].[field_name]
			, [bucket_values].[previous_value]
			, [bucket_values].[current_value]
			, [bucket_values].[change_date]
			, [bucket_values].[Bucket_Number]
			, [bucket_values].[Updated_User_Id]
		FROM(
				VALUES(@Invoice_id
					   , 932
					   , 934
					   , 'Determinant Bucket'
					   , @Current_Bucket_Name
					   , @New_Bucket_Name
					   , GETDATE()
					   , @Bucket_code
					   , @user_id
					  )
					  , (@Invoice_id
						 , 932
						 , 934
						 , 'Determinant Sub Bucket'
						 , @Current_Sub_Bucket_Name
						 , @New_Sub_Bucket_Name
						 , GETDATE()
						 , @Bucket_code
						 , @user_id
						)
					  , (@Invoice_id
						 , 932
						 , 934
						 , 'Determinant UOM'
						 , @Current_UOM
						 , @New_UOM
						 , GETDATE()
						 , @Bucket_code
						 , @user_id
						)
			) AS [bucket_values]([cu_invoice_id], [change_type_id], [field_type_id], [field_name], [previous_value]
								 , [current_value], [change_date], [Bucket_Number], [Updated_User_Id]
								)
		WHERE
			@IsUpdatable = 1;
		--AND [bucket_values].[previous_value] <> [bucket_values].[current_value];                      




		IF OBJECT_ID('tempdb.dbo.#Uom_Bucket_Dtl_Map', 'U') IS NOT NULL DROP TABLE #Uom_Bucket_Dtl_Map;
		IF OBJECT_ID('tempdb.dbo.#Ec_Sub_Invoice_Bucket_Dtl_Uom_Map', 'U') IS NOT NULL DROP TABLE #Ec_Sub_Invoice_Bucket_Dtl_Uom_Map;


	END TRY
	BEGIN CATCH

		DECLARE
			@Error_Line INT
			, @Error_Message VARCHAR(3000);
		-- Entry made to the logging SP to capture the errors.                                          
		SELECT @Error_Line = ERROR_LINE(), @Error_Message = ERROR_MESSAGE();

		INSERT INTO [dbo].[StoredProc_Error_Log]
			(
				[StoredProc_Name]
				, [Error_Line]
				, [Error_message]
				, [Input_Params]
			)
		VALUES
			(
			'[Invoice_BulkProcess_Update_UOM]', @Error_Line, @Error_Message, 'Batch detail id: ' + @batch_detail_id
			);

		EXEC [dbo].[usp_RethrowError]
			@Error_Message;
	END CATCH;
END;
GO
GRANT EXECUTE ON  [dbo].[Invoice_BulkProcess_Update_UOM] TO [CBMSApplication]
GO
