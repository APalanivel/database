SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO

CREATE       PROCEDURE [dbo].[SAVE_FORECAST_VOLUME_DETAILS_P]
   @userId varchar(10)
 , @sessionId varchar(50)
 , @rm_forecast_volume_id int
 , @site_id int
 , @division_id int
 , @volume decimal
 , @month_identifier datetime
 , 
--@volume_type_id int
   @volumeTypeName varchar(200)
 , @siteUnitId int
AS 
   set nocount on
   declare @volume_type_id int

   select   @volume_type_id = entity_id
   from     entity
   where    entity_type = 285
            and entity_name = @volumeTypeName

   IF ( select count(1)
        from   rm_forecast_volume_details  
        where  rm_forecast_volume_id = @rm_forecast_volume_id
               and site_id = @site_id
               and division_id = @division_id
               and month_identifier = @month_identifier
      ) > 0 
      update   rm_forecast_volume_details  
      set      volume = @volume
             , volume_type_id = @volume_type_id
             , volume_units_type_id = @siteUnitId
      where    rm_forecast_volume_id = @rm_forecast_volume_id
               and site_id = @site_id
               and division_id = @division_id
               and month_identifier = @month_identifier
	   	
   ELSE 
      insert   into rm_forecast_volume_details  
               (
                 rm_forecast_volume_id
               , site_id
               , division_id
               , volume
               , month_identifier
               , volume_type_id
               , volume_units_type_id
               )
      values   (
                 @rm_forecast_volume_id
               , @site_id
               , @division_id
               , @volume
               , @month_identifier
               , @volume_type_id
               , @siteUnitId
               )
GO
GRANT EXECUTE ON  [dbo].[SAVE_FORECAST_VOLUME_DETAILS_P] TO [CBMSApplication]
GO
