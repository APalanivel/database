SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*********     
NAME:    [dbo].[Get_Variance_Closed_Reason]  
   
DESCRIPTION:  Used to select code value from code table using the code name    
  
INPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
    
          
      
OUTPUT PARAMETERS:      
      Name              DataType          Default     Description      
------------------------------------------------------------      
      
USAGE EXAMPLES:     
  
 
EXEC [Get_Variance_Closed_Reason] 103310   
  
------------------------------------------------------------    
AUTHOR INITIALS:    
Initials Name    
------------------------------------------------------------    
TRK Ramakrishna Thummala  
AP Arunkumar Palanivel  
  
Initials Date  Modification    
------------------------------------------------------------    

AP Feb 19,2020 Created stored procedure to get variance closure category  
  
******/
CREATE PROCEDURE [dbo].[Get_Variance_Closed_Reason]
(@Closure_Category_Cd INT)
    
AS
    BEGIN

        SET NOCOUNT ON;

        SELECT DISTINCT vr.Closed_Reason_Id AS ID, c.code_dsc AS NAME FROM dbo.Variance_Closed_Reason_Category vr
JOIN dbo.code c 
ON vr.Closed_Reason_cd = c.Code_Id
WHERE vr.Is_Active =1
AND vr.Closure_Category_Cd = @Closure_Category_Cd
ORDER BY c.Code_Dsc
    END;

GO
GRANT EXECUTE ON  [dbo].[Get_Variance_Closed_Reason] TO [CBMSApplication]
GO
