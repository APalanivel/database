SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******
NAME:
 dbo.lec_UtilityAccountNGUsageCommercial_2015

DESCRIPTION:	

 INPUT PARAMETERS:
 Name			DataType	Default		Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name			DataType  Default		Description
------------------------------------------------------------
  
  USAGE EXAMPLES:
------------------------------------------------------------
select top 10 * from dbo.lec_UtilityAccountNGUsageCommercial_2015
select top 100 * from dbo.lec_UtilityAccountNGUsageCommercial_2015


AUTHOR INITIALS:
 Initials	Name  
------------------------------------------------------------
 SP         Sandeep Pigilam

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 SP		 2015-04-08	        Created
 
							
******/
CREATE VIEW [dbo].[lec_UtilityAccountNGUsageCommercial_2015]
AS
SELECT
      *
FROM
      ( SELECT
            x.Client
           ,x.Division
           ,x.Site
           ,x.State
           ,x.Region
           ,x.Account
           ,x.Vendor
           ,SUM(x.ConvertedUsage) AS TotalUsage
           ,x.SERVICE_MONTH AS SrvDate
        FROM
            ( SELECT
                  cha.account_id
                 ,cha.account_number [Account]
                 ,cha.Account_Vendor_Name AS [Vendor]
                 ,ch.SITE_NAME AS [Site]
                 ,ch.Sitegroup_Name AS Division
                 ,ch.CLIENT_NAME AS [Client]
                 ,ch.REGION_NAME AS [Region]
                 ,ch.state_name AS [State]
                 ,SUM(CAST(cu.Bucket_Value AS DECIMAL(32, 16)) * cuc.conversion_factor) AS ConvertedUsage
                 ,CONVERT(VARCHAR(3), DATENAME(month, cu.service_month), 100) + '-' + RIGHT(DATEPART(yy, cu.SERVICE_MONTH), 2) AS service_month
              FROM
                  Core.Client_Hier_Account cha
                  JOIN Core.Client_Hier ch
                        ON cha.Client_Hier_Id = ch.Client_Hier_Id
                  JOIN dbo.Commodity com
                        ON com.Commodity_Id = cha.Commodity_Id
                  LEFT JOIN ( dbo.Cost_Usage_Account_Dtl cu
                              JOIN dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
                                       AND bm.Bucket_Name = 'Total Usage'
                                       AND cu.Service_Month BETWEEN '01/01/2015'
                                                            AND     '12/01/2015' )
                              ON cu.ACCOUNT_ID = cha.ACCOUNT_ID
                                 AND cu.Client_Hier_Id = cha.Client_Hier_Id
                                 AND bm.Commodity_Id = com.Commodity_Id
                  JOIN dbo.ENTITY mmbtu
                        ON mmbtu.ENTITY_NAME = 'mmbtu'
                           AND mmbtu.ENTITY_DESCRIPTION = 'Unit for Gas'
                  LEFT JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                        ON cuc.BASE_UNIT_ID = cu.UOM_Type_Id
                           AND cuc.CONVERTED_UNIT_ID = mmbtu.ENTITY_ID
              WHERE
                  com.Commodity_Name = 'Natural Gas'
                  AND cha.Account_Type = 'Utility'
                  AND ch.Site_Type_Name = 'Commercial'
                  AND ch.client_name != 'AT&T Services, Inc.'
                  AND ch.Site_Not_Managed = 0
                  AND cha.Account_Not_Managed = 0
              GROUP BY
                  cha.account_id
                 ,cha.account_number
                 ,cha.Account_Vendor_Name
                 ,ch.SITE_NAME
                 ,ch.Sitegroup_Name
                 ,ch.CLIENT_NAME
                 ,ch.REGION_NAME
                 ,ch.state_name
                 ,CONVERT(VARCHAR(3), DATENAME(month, cu.service_month), 100) + '-' + RIGHT(DATEPART(yy, cu.SERVICE_MONTH), 2) ) x
        GROUP BY
            x.Client
           ,x.Division
           ,x.Site
           ,x.State
           ,x.Region
           ,x.Account
           ,x.Vendor
           ,x.SERVICE_MONTH ) AS VendorUsage PIVOT  
  ( SUM(TotalUsage) FOR SrvDate IN ( [Jan-15], [Feb-15], [Mar-15], [Apr-15], [May-15], [Jun-15], [Jul-15], [Aug-15], [Sep-15], [Oct-15], [Nov-15], [Dec-15] ) )  
  as TotalUsagePivot;
;
;
;
GO
