SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--SELECT * FROM SR_SW_RFP_SITE_COUNT_FOR_BID_GROUPS_VW

CREATE  view dbo.SR_SW_RFP_SITE_COUNT_FOR_BID_GROUPS_VW
AS
	select 
		bidgroup.sr_rfp_bid_group_id as group_id,
		count(distinct s.site_id) as site_count
		  
	from  
		sr_rfp_account rfp_account(nolock)join account a(nolock)
		on a.account_id = rfp_account.account_id and rfp_account.is_deleted = 0
		join sr_rfp_bid_group bidgroup(nolock) on rfp_account.sr_rfp_bid_group_id = bidgroup.sr_rfp_bid_group_id
		join site s(nolock) on s.site_id = a.site_id
		group by  bidgroup.sr_rfp_bid_group_id
GO
