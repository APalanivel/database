SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_MeterLevelTaxData]
as
select distinct client_name as 'Client Name'
	,city as 'City'
	,state_name as 'State'
	,region_name as 'Region'
	,account_number as 'Account Number'
	,vendor_name as 'Utility'
	,meter_number as 'Meter Number'
	,tax_exempt_status as 'Tax Exempt Status %'
	,tax_review_date as 'Tax Review Date'
	,expiration_date as 'Expiration'
	,description as 'View Exemption Documents: Attachments (Y/N)'

from meter m
join account acct on acct.account_id = m.account_id
join address addr on addr.address_id = m.address_id
join state st on st.state_id = addr.state_id
join region reg on reg.region_id = st.region_id
join site s on s.site_id = addr.address_parent_id
join division d on d.division_id = s.division_id
join client cl on cl.client_id = d.client_id
join vendor v on v.vendor_id = acct.vendor_id
left join meter_cbms_image_map mcim on mcim.meter_id = m.meter_id
where acct.not_managed = '0'

GO
