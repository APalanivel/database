SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--query to find all rfps with 10% tolerance
--in this case only, b/c volume_tolerance may hold % or may be free form
--if percentage is selected, then the column gets updated with the % of 0,5,10
--if not selected, but is_price_varying = '1', then free form may be used

--no bid groups
CREATE  view
[dbo].[lec_rfp_tolerance_issue]
as
select distinct 
		vendor_name
		,username
		,rfp.sr_rfp_id
		,e.entity_name	
		,from_month
		,to_month
		,account_number
		,client_name
		,site_name
		,e1.entity_name as bid_status
		,prod.product_name
from 		sr_rfp rfp
	   join sr_rfp_term term on term.sr_rfp_id = rfp.sr_rfp_id
	   join sr_rfp_account_term aterm on aterm.sr_rfp_term_id = term.sr_rfp_term_id
	   join sr_rfp_term_product_map map on map.sr_rfp_account_term_id = aterm.sr_rfp_account_term_id
	   join sr_rfp_bid bid on bid.sr_rfp_bid_id = map.sr_rfp_bid_id
	   join sr_rfp_supplier_service serv on serv.sr_rfp_supplier_service_id = bid.sr_rfp_supplier_service_id
	   join sr_rfp_balancing_tolerance bal on bal.sr_rfp_balancing_tolerance_id = serv.sr_rfp_balancing_tolerance_id 
	   join entity e on e.entity_id = bal.volume_tolerance
	   join sr_rfp_supplier_contact_vendor_map vmap on vmap.sr_rfp_supplier_contact_vendor_map_id = map.sr_rfp_supplier_contact_vendor_map_id 
	   join vendor v on vmap.vendor_id = v.vendor_id
	   join sr_supplier_contact_info sinfo on sinfo.sr_supplier_contact_info_id = vmap.sr_supplier_contact_info_id
	   join user_info ui on ui.user_info_id = sinfo.user_info_id
	   join sr_rfp_account sacct on sacct.sr_rfp_account_id = aterm.sr_account_group_id
	   join account acct on acct.account_id = sacct.account_id
	   join sr_rfp_selected_products prods on prods.sr_rfp_selected_products_id = map.sr_rfp_selected_products_id
	   join sr_rfp_pricing_product prod on prod.sr_rfp_pricing_product_id = prods.pricing_product_id
--only works for utility accounts that's why using it here
	   join site s on s.site_id = acct.site_id
	   join division d on d.division_id = s.division_id
	   join client cl on cl.client_id = d.client_id
	   join entity e1 on e1.entity_id = sacct.bid_status_type_id
	  where is_price_varying = '1'
	  	and e.entity_name = '10%'
		and (is_bid_group = '0' or is_bid_group = null)
union
select distinct 
		vendor_name
		,username
		,rfp.sr_rfp_id
		,e.entity_name	
		,from_month
		,to_month
		,account_number
		,client_name
		,site_name
		,e1.entity_name as bid_status
		,prod.product_name
	   from sr_rfp rfp
	   join sr_rfp_term term on term.sr_rfp_id = rfp.sr_rfp_id
	   join sr_rfp_account_term aterm on aterm.sr_rfp_term_id = term.sr_rfp_term_id
	   join sr_rfp_term_product_map map on map.sr_rfp_account_term_id = aterm.sr_rfp_account_term_id
	   join sr_rfp_bid bid on bid.sr_rfp_bid_id = map.sr_rfp_bid_id
	   join sr_rfp_supplier_service serv on serv.sr_rfp_supplier_service_id = bid.sr_rfp_supplier_service_id
	   join sr_rfp_balancing_tolerance bal on bal.sr_rfp_balancing_tolerance_id = serv.sr_rfp_balancing_tolerance_id 
	   join entity e on e.entity_id = bal.volume_tolerance
	   join sr_rfp_supplier_contact_vendor_map vmap on vmap.sr_rfp_supplier_contact_vendor_map_id = map.sr_rfp_supplier_contact_vendor_map_id 
	   join vendor v on vmap.vendor_id = v.vendor_id
	   join sr_supplier_contact_info sinfo on sinfo.sr_supplier_contact_info_id = vmap.sr_supplier_contact_info_id
	   join user_info ui on ui.user_info_id = sinfo.user_info_id
	   join sr_rfp_account sacct on sacct.sr_rfp_bid_group_id = aterm.sr_account_group_id
	   join account acct on acct.account_id = sacct.account_id
	   join sr_rfp_selected_products prods on prods.sr_rfp_selected_products_id = map.sr_rfp_selected_products_id
	   join sr_rfp_pricing_product prod on prod.sr_rfp_pricing_product_id = prods.pricing_product_id
--only works for utility accounts that's why using it here
	   join site s on s.site_id = acct.site_id
	   join division d on d.division_id = s.division_id
	   join client cl on cl.client_id = d.client_id
	   join entity e1 on e1.entity_id = sacct.bid_status_type_id
	  where is_price_varying = '1'
		and e.entity_name = '10%'
		and is_bid_group = '1'
	/*
	order by
		 rfp.sr_rfp_id
		,vendor_name
		,client_name
		,prod.product_name
		,from_month
		,to_month
	*/

GO
