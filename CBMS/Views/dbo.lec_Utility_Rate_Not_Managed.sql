SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
 
2nd Query
    Utility Name
    Utility State
    Utility Creation Date
    Rate Name
    Rate Creation Date
*/

create view
[dbo].[lec_Utility_Rate_Not_Managed]
as
select       vendor_name as 'Utility Name'
	     ,state_name as 'State'
	     ,ea.modified_date as 'Utility Creation Date'
	     ,r.rate_name as 'Rate Name'
	     ,ea1.modified_date as 'Rate Creation Date'
	     
from         vendor v 
left join    rate r on r.vendor_id = v.vendor_id
left join    vendor_state_map vsm on vsm.vendor_id = v.vendor_id
left join    entity_audit ea on ea.entity_identifier = v.vendor_id
left join    entity_audit ea1 on ea1.entity_identifier = r.rate_id
left join    state st on st.state_id = vsm.state_id


where        v.vendor_type_id = 289
	     and
	     (ea.entity_id = 509 and ea.audit_type = '1')
	     and
	     ((ea1.entity_id = 503 and ea1.audit_type = '1') or
		(ea1.entity_id is null))





--rate 503
--utility 509

GO
