SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create  View [dbo].[UBM_Unprocessed_Invoices]
as
Select L.Start_Date, I.UBM_CLIENT_NAME, UBM_ACCOUNT_ID, UBM_ACCOUNT_NUMBER, BILL_ADDRESS_LINE1, BILL_ADDRESS_LINE2, BILL_ADDRESS_LINE3, CITY, STATE, ZIP_CODE
From UBM_Invoice I
      inner join
     UBM_Batch_Master_Log L on I.UBM_Batch_Master_Log_ID = L.UBM_Batch_Master_Log_ID
Where L.Start_Date > (GetDate()-(365*2))
  And I.Is_Processed = 0
  And I.UBM_Client_ID Is Null
  And I.CBMS_Image_ID Is Not Null

GO
