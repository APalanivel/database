SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create  view
[dbo].[LEC_DominicUtilityRates]
as

SELECT     dbo.VENDOR.VENDOR_NAME AS UtilityName, dbo.ENTITY.ENTITY_NAME AS CommodityType, dbo.RATE.RATE_NAME AS RateName, 
                      dbo.STATE.STATE_NAME AS State
FROM         dbo.VENDOR_STATE_MAP INNER JOIN
                      dbo.VENDOR ON dbo.VENDOR_STATE_MAP.VENDOR_ID = dbo.VENDOR.VENDOR_ID INNER JOIN
                      dbo.RATE ON dbo.VENDOR.VENDOR_ID = dbo.RATE.VENDOR_ID INNER JOIN
                      dbo.STATE ON dbo.VENDOR_STATE_MAP.STATE_ID = dbo.STATE.STATE_ID INNER JOIN
                      dbo.ENTITY ON dbo.RATE.COMMODITY_TYPE_ID = dbo.ENTITY.ENTITY_ID
WHERE     (dbo.STATE.STATE_NAME = 'al') OR
                      (dbo.STATE.STATE_NAME = 'ga')

GO
