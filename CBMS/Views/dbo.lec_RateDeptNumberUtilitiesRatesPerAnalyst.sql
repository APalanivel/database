SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  view
[dbo].[lec_RateDeptNumberUtilitiesRatesPerAnalyst]
as
select   first_name
 ,last_name
 ,count (distinct v.vendor_id) as NumberofUtilities
 ,count (distinct r.rate_id) as NumberofRates
 ,region_name
 ,state_name
from vendor v

join utility_detail ud on v.vendor_id = ud.vendor_id
join utility_analyst_map uam on ud.utility_detail_id = uam.utility_detail_id
join user_info ui on uam.regulated_analyst_id = ui.user_info_id
join rate r on v.vendor_id = r.vendor_id
join vendor_state_map vsm on v.vendor_id = vsm.vendor_id
join state st on vsm.state_id = st.state_id
join region reg on st.region_id = reg.region_id
where rate_id in 
(
		select  distinct --first_name
		-- ,last_name
		 --,v.vendor_id
		 rate_id
		-- ,region_name
		-- ,state_name
		from vendor v
		
		join utility_detail ud on v.vendor_id = ud.vendor_id
		join utility_analyst_map uam on ud.utility_detail_id = uam.utility_detail_id
		join user_info ui on uam.regulated_analyst_id = ui.user_info_id
		join rate r on v.vendor_id = r.vendor_id
		join vendor_state_map vsm on v.vendor_id = vsm.vendor_id
		join state st on vsm.state_id = st.state_id
		join region reg on st.region_id = reg.region_id
		where rate_name not like '%commercial ses%'
		and v.is_history <> '1'
)
group by first_name
 ,last_name 
  ,state_name
 ,region_name

--order by first_name, last_name

GO
