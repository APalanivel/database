SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view
/*set transaction isolation level read uncommitted
select *
from lec_DivisionSiteContractingData
order by client, site
option (maxdop 1)*/
[dbo].[lec_DivisionSiteContractingData]
as
with CEM_cte
as
(
select MAX(CEM) CEM
	,Client
	,Client_ID
	,ClientType
from (
	select ui.first_name + ' ' + ui.last_name CEM
	,client_name Client
	,c.CLIENT_ID
	,e.ENTITY_NAME ClientType
	from CLIENT_CEM_MAP ccm
	join user_info ui on ui.USER_INFO_ID = ccm.USER_INFO_ID
	join CLIENT c on c.CLIENT_ID = ccm.CLIENT_ID
	join ENTITY e on e.ENTITY_ID  = c.client_type_id
	where c.not_managed !=1 )x
	group by CLIENT,CLIENT_ID,clienttype
	)

	,EPTransportSites as
	(select site_id
from ACCOUNT a 
join METER m on m.ACCOUNT_ID = a.ACCOUNT_ID
join SUPPLIER_ACCOUNT_METER_MAP samm on samm.METER_ID = m.METER_ID 
join CONTRACT c on c.CONTRACT_ID= samm.Contract_ID
where c.COMMODITY_TYPE_ID = 290 and c.CONTRACT_END_DATE > GETDATE()
and (samm.METER_DISASSOCIATION_DATE is null or samm.METER_DISASSOCIATION_DATE > CONTRACT_END_DATE)
and c.CONTRACT_TYPE_ID = 153
group by a.SITE_ID)

	,NGTransportSites as
	(select site_id
from ACCOUNT a 
join METER m on m.ACCOUNT_ID = a.ACCOUNT_ID
join SUPPLIER_ACCOUNT_METER_MAP samm on samm.METER_ID = m.METER_ID 
join CONTRACT c on c.CONTRACT_ID= samm.Contract_ID
where c.COMMODITY_TYPE_ID = 291 and c.CONTRACT_END_DATE > GETDATE()
and (samm.METER_DISASSOCIATION_DATE is null or samm.METER_DISASSOCIATION_DATE > CONTRACT_END_DATE)
and c.CONTRACT_TYPE_ID = 153
group by a.SITE_ID)

	select 
			Client
			,CEM
			,ClientType
			,sg.Sitegroup_Name DivSiteGrpName
			,sg.Sitegroup_Id DivSiteGrpID
			,DivisionNM = case when d.not_managed = 1 then 'NotManaged' else 'Managed' end
			,d.CONTRACTING_ENTITY DivSiteGrpContractingEntity
			,d.TAX_NUMBER DivSiteGrpFEIN
			,d.DUNS_NUMBER DivSiteGrpDUNS
			,Site_Name Site
			,addr.CITY City
			,st.STATE_NAME State 
			,s.Site_ID 
			,SiteNM = case when s.not_managed = 1 then 'NotManaged' when s.NOT_MANAGED = '0' then 'Managed'
			else null end
			,SiteClosed = case when s.CLOSED = 1 then 'Closed' when s.closed = '0' then 'Open' else null end
			,s.CONTRACTING_ENTITY SiteContractingEntity
			,s.TAX_NUMBER SiteFEIN
			,s.DUNS_NUMBER SiteDUNS
			,EPTransportSite = case when epts.SITE_ID is null and s.site_id is not null then 'NotTransport'
			when s.SITE_ID is null then null else 'Transport' end
			,NGTransportSite = case when ngts.SITE_ID is null and s.site_id is not null then 'NotTransport'
			when s.SITE_ID is null then null else 'Transport' end
			
			
	from division_dtl d 
	left join Sitegroup_Site ss on ss.Sitegroup_id = d.sitegroup_id
	left join SITE s on s.SITE_ID = ss.Site_id
	left join Sitegroup sg on sg.Sitegroup_Id = d.Sitegroup_id
	join CEM_cte ce on ce.CLIENT_ID = d.CLIENT_ID
	left join EPTransportSites epts on epts.SITE_ID = s.SITE_ID
	left join NGTransportSites ngts on ngts.SITE_ID = s.SITE_ID
	left join ADDRESS addr on addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
	left join STATE st on st.STATE_ID = addr.STATE_ID
		where d.CLIENT_ID !=11231



GO
