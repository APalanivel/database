SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****      
NAME: dbo.lec_OpenExceptionsPivot            
          
DESCRIPTION:              
 used to get Exceptions Data     
             
INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
                         
      
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
          
USAGE EXAMPLES:      
------------------------------------------------------------      
Select * from dbo.lec_OpenExceptionsPivot          
          
      
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 AKR   Ashok Kumar Raju      
           
MAINTENANCE LOG:              
Initials Date  Modification       
--- ---------- ----------------------------------------------              
AKR  2012-05-01  Modified according to Standards and Implemented the Additional Data Changes
******/      
      
CREATE VIEW [dbo].[lec_OpenExceptionsPivot]
AS
SELECT
      queuetype
     ,exception_type
     ,[Catherine James]
     ,[Jordan Martin]
     ,[Matt McCartney]
     ,[Teresa Meredith]
     ,[Alejandro Calzada]
     ,[Alfredo Castillo]
     ,[David Guerrero]
     ,[Dominic Almaguer]
     ,[Emiliano Morin]
     ,[Fernando Guti?rrez]
     ,[Giovanni Vidal]
     ,[Itzel Perez]
     ,[Jesus Morales]
     ,[Jorge Lopez]
     ,[Marco Aparicio]
     ,[Rodrigo Roa]
     ,[Dhanraj Madurgi]
     ,[Hema Mandhapati]
     ,[Jyotsna Pairdha]
     ,[Kiran Kumar]
     ,[Krishna Kumari Padiloju]
     ,[KV Shiva Prasad]
     ,[Mayukh Ghatak]
     ,[Naga Lakshmi]
     ,[Nagaswathi Pisupati]
     ,[Ramachandra Murthy]
     ,[Ravi Kumar]
     ,[Saroja Kummari]
     ,[Siva Kanth]
     ,[Sravan Bachu]
     ,[Srinivas Sarapu]
     ,[Sujitha Ummiti]
     ,[Sunil Reddy]
     ,[Swetha Mudeyam]
     ,[Abel Martinez]
     ,[Alejandra Aviles]
     ,[Erick Centeno]
     ,[Leonel Estrada]
     ,[Victor Garcia]
FROM
      ( SELECT
            sum(invoicecount) total
           ,queuetype
           ,exception_type
           ,queue_name
        FROM
            ( SELECT
                  count(DISTINCT cu_invoice_id) AS InvoiceCount
                 ,queuetype
                 ,exception_type
                 ,queue_name
              FROM
                  ( SELECT
                        qn.queue_name
                       ,queuetype = case WHEN t.IS_MANUAL = 1
                                              AND queue_type_id = 243 THEN 'Incoming'
                                         WHEN t.IS_MANUAL = 0 THEN 'Exception'
                                    END
                       ,t.exception_type
                       ,t.cu_invoice_id
                    FROM
                        cbms.dbo.cu_exception_denorm t
                        JOIN cbms.dbo.vwcbmsqueuename qn
                              ON qn.queue_id = t.queue_id
                    WHERE
                        qn.queue_name IN ( 'Catherine James', 'Jordan Martin', 'Matt McCartney', 'Teresa Meredith', 'Alejandro Calzada', 'Alfredo Castillo', 'David Guerrero', 'Dominic Almaguer', 'Emiliano Morin', 'Fernando Guti?rrez', 'Giovanni Vidal', 'Itzel Perez', 'Jesus Morales', 'Jorge Lopez', 'Marco Aparicio', 'Rodrigo Roa', 'Dhanraj Madurgi', 'Hema Mandhapati', 'Jyotsna Pairdha', 'Kiran Kumar', 'Krishna Kumari Padiloju', 'KV Shiva Prasad', 'Mayukh Ghatak', 'Naga Lakshmi', 'Nagaswathi Pisupati', 'Ramachandra Murthy', 'Ravi Kumar', 'Saroja Kummari', 'Siva Kanth', 'Sravan Bachu', 'Srinivas Sarapu', 'Sujitha Ummiti', 'Sunil Reddy', 'Swetha Mudeyam', 'Abel Martinez', 'Alejandra Aviles', 'Erick Centeno', 'Leonel Estrada', 'Victor Garcia' ) ) k
              GROUP BY
                  queuetype
                 ,exception_type
                 ,queue_name
              UNION
              SELECT
                  count(cu_invoice_id) cu_invoice_id
                 ,queuetype
                 ,exception_type
                 ,queue_name
              FROM
                  ( SELECT
                        count(1) cu_invoice_id
                       ,'Variance' AS queuetype
                       ,'Variance' AS exception_type
                       ,l.queue_name
                    FROM
                        cbms.dbo.variance_log l
                        INNER JOIN dbo.Commodity com
                              ON l.Commodity_ID = com.Commodity_Id
                    WHERE
                        Partition_Key IS NULL
                        AND com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
                        AND is_failure = 1
                        AND l.queue_name IN ( 'Catherine James', 'Jordan Martin', 'Matt McCartney', 'Teresa Meredith', 'Alejandro Calzada', 'Alfredo Castillo', 'David Guerrero', 'Dominic Almaguer', 'Emiliano Morin', 'Fernando Guti?rrez', 'Giovanni Vidal', 'Itzel Perez', 'Jesus Morales', 'Jorge Lopez', 'Marco Aparicio', 'Rodrigo Roa', 'Dhanraj Madurgi', 'Hema Mandhapati', 'Jyotsna Pairdha', 'Kiran Kumar', 'Krishna Kumari Padiloju', 'KV Shiva Prasad', 'Mayukh Ghatak', 'Naga Lakshmi', 'Nagaswathi Pisupati', 'Ramachandra Murthy', 'Ravi Kumar', 'Saroja Kummari', 'Siva Kanth', 'Sravan Bachu', 'Srinivas Sarapu', 'Sujitha Ummiti', 'Sunil Reddy', 'Swetha Mudeyam', 'Abel Martinez', 'Alejandra Aviles', 'Erick Centeno', 'Leonel Estrada', 'Victor Garcia' )
                    GROUP BY
                        l.queue_name
                       ,Account_ID
                       ,Service_Month
                       ,Site_ID ) k
              GROUP BY
                  queuetype
                 ,exception_type
                 ,queue_name
              UNION
              SELECT
                  count(1) AS InvoiceCount
                 ,queuetype
                 ,exception_type
                 ,queue_name
              FROM
                  ( SELECT
                        l.queue_name
                       ,'Variance' AS queuetype
                       ,'Variance' AS exception_type
                       ,Account_ID
                       ,Service_Month
                       ,Site_ID
                    FROM
                        cbms.dbo.variance_log l
                        INNER JOIN dbo.Commodity com
                              ON l.Commodity_ID = com.Commodity_Id
                    WHERE
                        Partition_Key IS NULL
                        AND com.Commodity_Name NOT IN ( 'Electric Power', 'Natural Gas' )
                        AND is_failure = 1
                        AND l.queue_name IN ( 'Catherine James', 'Jordan Martin', 'Matt McCartney', 'Teresa Meredith', 'Alejandro Calzada', 'Alfredo Castillo', 'David Guerrero', 'Dominic Almaguer', 'Emiliano Morin', 'Fernando Guti?rrez', 'Giovanni Vidal', 'Itzel Perez', 'Jesus Morales', 'Jorge Lopez', 'Marco Aparicio', 'Rodrigo Roa', 'Dhanraj Madurgi', 'Hema Mandhapati', 'Jyotsna Pairdha', 'Kiran Kumar', 'Krishna Kumari Padiloju', 'KV Shiva Prasad', 'Mayukh Ghatak', 'Naga Lakshmi', 'Nagaswathi Pisupati', 'Ramachandra Murthy', 'Ravi Kumar', 'Saroja Kummari', 'Siva Kanth', 'Sravan Bachu', 'Srinivas Sarapu', 'Sujitha Ummiti', 'Sunil Reddy', 'Swetha Mudeyam', 'Abel Martinez', 'Alejandra Aviles', 'Erick Centeno', 'Leonel Estrada', 'Victor Garcia' )
                    GROUP BY
                        l.queue_name
                       ,Account_ID
                       ,Service_Month
                       ,Site_ID ) x
              GROUP BY
                  Queue_Name
                 ,queuetype
                 ,exception_type ) y
        GROUP BY
            Queue_Name
           ,queuetype
           ,exception_type ) AS OpenPivot PIVOT      
( sum(total) FOR queue_name IN ( [Catherine James], [Jordan Martin], [Matt McCartney], [Teresa Meredith], [Alejandro Calzada], [Alfredo Castillo], [David Guerrero], [Dominic Almaguer], [Emiliano Morin], [Fernando Guti?rrez], [Giovanni Vidal], [Itzel Perez], [Jesus Morales], [Jorge Lopez], [Marco Aparicio], [Rodrigo Roa], [Dhanraj Madurgi], [Hema Mandhapati], [Jyotsna Pairdha], [Kiran Kumar], [Krishna Kumari Padiloju], [KV Shiva Prasad], [Mayukh Ghatak], [Naga Lakshmi], [Nagaswathi Pisupati], [Ramachandra Murthy], [Ravi Kumar], [Saroja Kummari], [Siva Kanth], [Sravan Bachu], [Srinivas Sarapu], [Sujitha Ummiti], [Sunil Reddy], [Swetha Mudeyam], [Abel Martinez], [Alejandra Aviles], [Erick Centeno], [Leonel Estrada], [Victor Garcia] ) ) AS pvt      


;
GO
