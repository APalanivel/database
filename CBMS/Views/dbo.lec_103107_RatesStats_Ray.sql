SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view
[dbo].[lec_103107_RatesStats_Ray]
as

--amf00489-34201
/*	
select distinct
	client_name
	,account_number	
	,commodity
	,accountservicelevel
	,state_name
	,region_name
	,sitetype
	,contractcommodity
	,analyst
	,manager
from 
(*/
--contracts
	select distinct 	
		client_name as 'ClientName'
		,account_number as 'AccountNumber'
		--,acct.account_id 
		,e.entity_name as  'ServiceLevel'
		,Commodity = case when acct.account_id in
		(select distinct acct.account_id 
		--,count(distinct commodity_type_id)
		from account acct join meter m on m.account_id = acct.account_id
		join rate r on r.rate_id = m.rate_id  
	group by acct.account_id  having count (distinct commodity_type_id) ='2')then 'EP-NG' 
		when r.commodity_type_id = '291' then 'NG'
		when r.commodity_type_id = '290' then 'EP'
		else null end
	
		
		--,e1.entity_name
		,state_name as 'State'
		,region_name as 'Region'
		,e2.entity_name as  'SiteType'
		,e3.entity_name as  'ContractType'
		,v.vendor_name as 'Utility'
		--,ContractCommodity = case when c.commodity_type_id = '290' then 'EP' when c.commodity_type_id = '291' then 'NG'  end
		,ui.first_name + ' ' +ui.last_name as 'ManagingAnalyst'
		,ui1.first_name +' '+  ui1.last_name as 'Manager'
		,GroupBill = case when account_group_id is not null then 'Yes' else 'No' end
	from   client cl 
	join   division d with (nolock) on d.client_id = cl.client_id
	join   site s with (nolock)on s.division_id = d.division_id
	join   address addr with (nolock)on addr.address_parent_id = s.site_id
	join   meter m with (nolock)on m.address_id = addr.address_id
	join   account acct with (nolock)on acct.account_id = m.account_id
	join   state st with (nolock)on st.state_id = addr.state_id
	join   region reg with (nolock)on reg.region_id = st.region_id
	join   rate r with (nolock)on r.rate_id = m.rate_id
	 join supplier_account_meter_map samm with (nolock)on samm.meter_id = m.meter_id
	 join contract c with (nolock)on c.contract_id = samm.contract_id
	join   entity e with (nolock)on e.entity_id = acct.service_level_type_id
	join   entity e1 with (nolock)on e1.entity_id = r.commodity_type_id
	join   entity e2 with (nolock)on e2.entity_id = s.site_type_id
	 join   entity e3 with (nolock)on e3.entity_id = c.contract_type_id
	join   vendor v with (nolock)on v.vendor_id = acct.vendor_id
	left join   utility_detail ud with (nolock)on ud.vendor_id = v.vendor_id
	left join   utility_analyst_map uam with (nolock)on uam.utility_detail_id = ud.utility_detail_id
	left join   user_info ui with (nolock)on ui.user_info_id = uam.regulated_analyst_id
	left join region_manager_map rmm with (nolock)on rmm.region_id = reg.region_id
	left join user_info ui1 with (nolock)on ui1.user_info_id = rmm.user_info_id
		where (acct.not_managed = '0')
	
	
	union all
	
	--no contracts
	select distinct 	
		client_name as 'ClientName'
		,account_number as 'AccountNumber'
		--,acct.account_id 
		,e.entity_name as 'ServiceLevel'
		,Commodity = case when acct.account_id in
		(select distinct acct.account_id 
		--,count(distinct commodity_type_id)
		from account acct join meter m on m.account_id = acct.account_id
		join rate r on r.rate_id = m.rate_id  
	group by acct.account_id  having count (distinct commodity_type_id) ='2')then 'EP-NG' 
		when r.commodity_type_id = '291' then 'NG'
		when r.commodity_type_id = '290' then 'EP'
		else null end
	
		
		--,e1.entity_name
		,state_name as 'State'
		,region_name as 'Region'
		,e2.entity_name as  'SiteType'
		,null as ContractType
		,v.vendor_name as 'Utility'
		--,null as ContractCommodity
		,ui.first_name + ' ' +ui.last_name as 'ManagingAnalyst'
		,ui1.first_name +' '+  ui1.last_name as 'Manager'
		,GroupBill= case when account_group_id is not null then 'Yes' else 'No' end
	from   client cl 
	join   division d with (nolock) on d.client_id = cl.client_id
	join   site s with (nolock)on s.division_id = d.division_id
	join   address addr with (nolock)on addr.address_parent_id = s.site_id
	join   meter m with (nolock)on m.address_id = addr.address_id
	join   account acct with (nolock)on acct.account_id = m.account_id
	join   state st with (nolock)on st.state_id = addr.state_id
	join   region reg with (nolock)on reg.region_id = st.region_id
	join   rate r with (nolock)on r.rate_id = m.rate_id
	left join supplier_account_meter_map samm with (nolock)on samm.meter_id = m.meter_id
	left join contract c with (nolock)on c.CONTRACT_ID = samm.Contract_Id
	join   entity e with (nolock)on e.entity_id = acct.service_level_type_id
	join   entity e1 with (nolock)on e1.entity_id = r.commodity_type_id
	join   entity e2 with (nolock)on e2.entity_id = s.site_type_id
	left join   entity e3 with (nolock)on e3.entity_id = c.contract_type_id
	join   vendor v with (nolock)on v.vendor_id = acct.vendor_id
	left join   utility_detail ud with (nolock)on ud.vendor_id = v.vendor_id
	left join   utility_analyst_map uam with (nolock)on uam.utility_detail_id = ud.utility_detail_id
	left join   user_info ui with (nolock)on ui.user_info_id = uam.regulated_analyst_id
	left join region_manager_map rmm with (nolock)on rmm.region_id = reg.region_id
	left join user_info ui1 with (nolock)on ui1.user_info_id = rmm.user_info_id
	where c.contract_id is null
	and ( acct.not_managed = '0')
--)y


GO
