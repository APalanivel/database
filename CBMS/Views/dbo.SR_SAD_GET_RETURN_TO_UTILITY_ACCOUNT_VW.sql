SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--SELECT * FROM  SR_SAD_GET_RETURN_TO_UTILITY_ACCOUNT_VW

CREATE   VIEW dbo.SR_SAD_GET_RETURN_TO_UTILITY_ACCOUNT_VW
	AS				

	select  rfp_account.account_id
	from 	sr_rfp_closure rfp_closure(nolock),
		sr_rfp_account rfp_account(nolock)
	where 	rfp_closure.close_action_type_id = (select entity_id from entity where entity_type = 1014 and entity_name = 'Closed - Return to Utility')
		and rfp_closure.is_bid_group = 0
		and rfp_closure.sr_account_group_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0

	union

	select 	rfp_account.account_id
	from 	sr_rfp_account rfp_account(nolock) 
	where  	rfp_account.bid_status_type_id != (select entity_id from entity(nolock) where entity_type = 1029 and entity_name = 'Closed')
		and rfp_account.is_deleted = 0











GO
