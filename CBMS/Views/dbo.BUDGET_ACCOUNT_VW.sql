SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE    VIEW dbo.BUDGET_ACCOUNT_VW
	AS				

	select budget_account.budget_id , count(budget_account.budget_account_id) as no_of_accounts
	from 	budget_account budget_account
		join budget budget on budget.budget_id = budget_account.budget_id
		join budget_account_type_vw accountTypeVw on accountTypeVw.budget_account_id = budget_account.budget_account_id
		and accountTypeVw.budget_id = budget.budget_id
		
			
	group by budget_account.budget_id
GO
