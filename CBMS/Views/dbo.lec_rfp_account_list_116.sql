SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view
[dbo].[lec_rfp_account_list_116]
as
select distinct client_name
	,site_name
	,account_number
	,srrfpacct.account_id
from sr_rfp_Account srrfpacct
join account acct on acct.account_id = srrfpacct.account_id
join meter m on m.account_id = acct.account_id
join address addr on addr.address_id = m.address_id
join site s on s.site_id = addr.address_parent_id
join division d on d.division_id = s.division_id
join client cl on cl.client_id = d.client_id
where sr_rfp_id = 116
GO
