SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_progressiveacctcountsservlevel]
as
select Prior06 = sum(Prior06)
	,January06 = sum (prior06 + jan06)
	,February06 = sum (prior06 + jan06 + feb06)
	,March06 = sum (prior06 + jan06 + feb06 + mar06)
	,April06 = sum (prior06 + jan06 + feb06 + mar06 + apr06)
	,May06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06)
	,June06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06)
	,July06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06)
	,August06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06 + aug06)
	,September06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06 + aug06 + sep06)
	,October06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06 + aug06 + sep06 + oct06)
	,November06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06 + aug06 + sep06 + oct06 + nov06)
	,December06 = sum (prior06 + jan06 + feb06 + mar06 + apr06 + may06 + jun06 + jul06 + aug06 + sep06 + oct06 + nov06 + dec06)
	--,Prior07 = sum(Prior07)
	,January07 = sum (prior07 + jan07)
	,February07 = sum (prior07 + jan07 + feb07)
	,March07 = sum (prior07 + jan07 + feb07 + mar07)
	,April07 = sum (prior07 + jan07 + feb07 + mar07 + apr07)
	,May07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07)
	,June07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07)
	,July07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07)
	,August07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07 + aug07)
	,September07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07 + aug07 + sep07)
	,October07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07 + aug07 + sep07 + oct07)
	,November07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07 + aug07 + sep07 + oct07 + nov07)
	,December07 = sum (prior07 + jan07 + feb07 + mar07 + apr07 + may07 + jun07 + jul07 + aug07 + sep07 + oct07 + nov07 + dec07)
	,SiteType
	,ServiceLevelType
from 
(
	select 'Prior06' = sum (Prior06)
		,'Jan06' = sum (Jan06)
		,'Feb06' = sum (Feb06)
		,'Mar06' = sum(Mar06)
		,'Apr06' = sum(Apr06)
		,'May06' = sum(May06)
		,'Jun06' = sum(Jun06)
		,'Jul06' = sum(Jul06)
		,'Aug06' = sum(Aug06)
		,'Sep06' = sum(Sep06)
		,'Oct06' = sum(Oct06)
		,'Nov06' = sum(Nov06)
		,'Dec06' = sum(Dec06)
		,'Prior07' = sum (Prior07)
		,'Jan07' = sum (Jan07)
		,'Feb07' = sum (Feb07)
		,'Mar07' = sum(Mar07)
		,'Apr07' = sum(Apr07)
		,'May07' = sum(May07)
		,'Jun07' = sum(Jun07)
		,'Jul07' = sum(Jul07)
		,'Aug07' = sum(Aug07)
		,'Sep07' = sum(Sep07)
		,'Oct07' = sum(Oct07)
		,'Nov07' = sum(Nov07)
		,'Dec07' = sum(Dec07)

		,SiteType
		,ServiceLevelType
	from (



---prior06
select 
		Prior06 = count (distinct account_id)
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0'
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date < '2006-01-01'
		group by e.entity_name
		,e1.entity_name
		union all
---prior 07
		select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0'
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = count (distinct account_id)
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date < '2007-01-01'
		group by e.entity_name
		,e1.entity_name
		union all
	--**************************************START2006jan06
--january06
		select 
		Prior06 = '0'
		,Jan06 = count (distinct account_id)
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0'
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2005-12-31' and '2006-02-01'
	group by e.entity_name
		,e1.entity_name
		union all
		
	--february06
		select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = count (distinct account_id)
		,Mar06 = '0'
		,Apr06 = '0'
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-01-31' and '2006-03-01'
		
		group by e.entity_name
		,e1.entity_name
	union all
	--March06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = count (distinct account_id)
		,Apr06 = '0'
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-02-28' and '2006-04-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--April06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 =  count (distinct account_id)
		,May06 = '0'
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-03-31' and '2006-05-01'
		
		group by e.entity_name
		,e1.entity_name
	
	union all
	--May06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = count (distinct account_id)
		,Jun06 = '0'
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-04-30' and '2006-06-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--June06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 = count (distinct account_id)
		,Jul06 = '0'
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-05-31' and '2006-07-01'
		
		group by e.entity_name
			,e1.entity_name
	union all
	--July06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 =  count (distinct account_id)
		,Aug06 = '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-06-30' and '2006-08-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--August06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  count (distinct account_id)
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
		,e1.entity_name 'ServiceLevelType'
			
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-07-31' and '2006-09-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--Sepetember06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = count (distinct account_id)
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-08-31' and '2006-10-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--October06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = count (distinct account_id)
		,Nov06 = '0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-09-30' and '2006-11-01'
		
		group by e.entity_name
		,e1.entity_name
	union all
	--november06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = count (distinct account_id)
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-10-31' and '2006-12-01'
		and ea.modified_date between '2006-09-30' and '2006-11-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--december06
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 = '0'
		,Dec06 = count (distinct account_id)
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-11-30' and '2007-01-01'
		group by e.entity_name
		,e1.entity_name


	--****************************************END 2006

	union all
	--january
		select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = count (distinct account_id)
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2006-12-31' and '2007-02-01'
	group by e.entity_name
		,e1.entity_name
		union all
		
	--february
		select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = count (distinct account_id)
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-01-31' and '2007-03-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--March
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = count (distinct account_id)
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-02-28' and '2007-04-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--April
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = count (distinct account_id)
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-03-31' and '2007-05-01'
		group by e.entity_name
		,e1.entity_name
	
	union all
	--May
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = count (distinct account_id)
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-04-30' and '2007-06-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--June
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = count (distinct account_id)
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-05-31' and '2007-07-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--July
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = count (distinct account_id)
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-06-30' and '2007-08-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--August
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = count (distinct account_id)
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-07-31' and '2007-09-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--Sepetember
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = count (distinct account_id)
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = '0'
				
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-08-31' and '2007-10-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--October
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = count (distinct account_id)
		,Nov07 = '0'
		,Dec07 = '0'
			
		
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-09-30' and '2007-11-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--november
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = count (distinct account_id)
		,Dec07 = '0'
			
		
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-10-31' and '2007-12-01'
		group by e.entity_name
		,e1.entity_name
	union all
	--december
	select 
		Prior06 = '0'
		,Jan06 = '0'
		,Feb06 = '0'
		,Mar06 = '0'
		,Apr06 = '0' 
		,May06 = '0'
		,Jun06 ='0'
		,Jul06 = '0'
		,Aug06 =  '0'
		,Sep06 = '0'
		,Oct06 = '0'
		,Nov06 ='0'
		,Dec06 = '0'
		,Prior07 = '0'
		,Jan07 = '0'
		,Feb07 = '0'
		,Mar07 = '0'
		,Apr07 = '0'
		,May07 = '0'
		,Jun07 = '0'
		,Jul07 = '0'
		,Aug07 = '0'
		,Sep07 = '0'
		,Oct07 = '0'
		,Nov07 = '0'
		,Dec07 = count (distinct account_id)
		
			
		,e.entity_name  'SiteType'
			,e1.entity_name 'ServiceLevelType'
		from account acct
		join entity_audit ea on ea.entity_identifier = acct.account_id
		join site s on s.site_id = acct.site_id
		join entity e on e.entity_id = s.site_type_id
		join entity e1 on e1.entity_id = acct.service_level_type_id
		where account_Type_id = 38
		and acct.not_managed = '0'
		and ea.entity_id = 491
		and ea.audit_type = '1'
		and ea.modified_date between '2007-11-30' and '2008-01-01'
		group by e.entity_name
		,e1.entity_name
	
	
	)x
group by sitetype
	,serviceleveltype
)y
group by sitetype
,ServiceLevelType
GO
