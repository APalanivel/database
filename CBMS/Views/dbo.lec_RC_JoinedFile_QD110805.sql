SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_RC_JoinedFile_QD110805]
as
select distinct client_name as ClientName
	,cl.client_id as ClientID
	,cl.not_managed as ClientNotManaged
	,division_name as DivisionName
	,site_name as SiteName
	,s.site_id as SiteID
	,s.not_managed as SiteNotManaged
	,s.closed as SiteClosed
	,address_line1 as Address
	,addr.city as City
	,state_name as St_Province
	,zipcode as Zip
	,country_name as Country
	,region_name as Region
	,account_number as AccountNumber
	,e1.entity_name as ServiceLevel
	,ea.modified_date as AccountCreatedDate
	,acct.not_managed as AccountNotManaged
	,e2.entity_name as InvoiceSource
	,v.vendor_name as Utility
	,e.entity_name as CommodityType
	,rate_name	as Rate
	,meter_number as MeterNumber
	,m.meter_id as MeterID
	,'yes' as RCDone
	,ui.username as RCCreatedBy
	,rc.creation_date as RCCreatedDate
	,ui1.username as RCReviewedBy
	

from    rc_rate_comparison rc
	join meter m on rc.meter_id = m.meter_id
	join account acct on m.account_id = acct.account_id
	join address addr on m.address_id = addr.address_id
	join site s on addr.address_parent_id = s.site_id
	join division d on s.division_id = d.division_id
	join client cl on d.client_id = cl.client_id
	join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st on addr.state_id = st.state_id
	join region reg on st.region_id = reg.region_id
	join country ctry on st.country_id = ctry.country_id
	join rate r on m.rate_id = r.rate_id
	join entity e on r.commodity_type_id = e.entity_id
	join entity e1 on acct.service_level_type_id = e1.entity_id
	join entity e2 on acct.invoice_source_type_id = e2.entity_id
	join vendor v on acct.vendor_id = v.vendor_id
	join user_info ui on rc.created_by = ui.user_info_id
	left join user_info ui1 on rc.reviewed_by = ui1.user_info_id
	
where ea.entity_id = 491
and audit_type = '1'

union
select distinct client_name as ClientName
		,cl.client_id as ClientID
		,cl.not_managed as ClientNotManaged
		,division_name as DivisionName
		,site_name as SiteName
		,s.site_id as SiteID
		,s.not_managed as SiteNotManaged
		,s.closed as SiteClosed
		,address_line1 as Address
		,addr.city as City
		,state_name as St_Province
		,zipcode as Zip
		,country_name as Country
		,region_name as Region
		,account_number as AccountNumber
		,e1.entity_name as ServiceLevel
		,ea.modified_date as AccountCreatedDate
		,acct.not_managed as AccountNotManaged
		,e2.entity_name as InvoiceSource
		,v.vendor_name as Utility
		,e.entity_name as CommodityType
		,rate_name	as Rate
		,meter_number as MeterNumber
		,m.meter_id as MeterId
		,'no' as RCDone
		,null as RCCreatedBy
		,null as RCCreated
		,null as RCReviewedBy
from 
meter m 
	join account acct on m.account_id = acct.account_id
	join address addr on m.address_id = addr.address_id
	join site s on addr.address_parent_id = s.site_id
	join division d on s.division_id = d.division_id
	join client cl on d.client_id = cl.client_id
	join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st on addr.state_id = st.state_id
	join region reg on st.region_id = reg.region_id
	join country ctry on st.country_id = ctry.country_id
	join rate r on m.rate_id = r.rate_id
	join entity e on r.commodity_type_id = e.entity_id
	join entity e1 on acct.service_level_type_id = e1.entity_id
	join entity e2 on acct.invoice_source_type_id = e2.entity_id
	join vendor v on acct.vendor_id = v.vendor_id
	
where ea.entity_id = 491
and audit_type = '1'
and meter_id not in
(
	select distinct 
		m.meter_id
		
		
	
	from    rc_rate_comparison rc
		join meter m on rc.meter_id = m.meter_id
		join account acct on m.account_id = acct.account_id
		join address addr on m.address_id = addr.address_id
		join site s on addr.address_parent_id = s.site_id
		join division d on s.division_id = d.division_id
		join client cl on d.client_id = cl.client_id
		join entity_audit ea on acct.account_id = ea.entity_identifier
		join state st on addr.state_id = st.state_id
		join region reg on st.region_id = reg.region_id
		join country ctry on st.country_id = ctry.country_id
		join rate r on m.rate_id = r.rate_id
		join entity e on r.commodity_type_id = e.entity_id
		join entity e1 on acct.service_level_type_id = e1.entity_id
		join entity e2 on acct.invoice_source_type_id = e2.entity_id
		join vendor v on acct.vendor_id = v.vendor_id
		join user_info ui on rc.created_by = ui.user_info_id
		left join user_info ui1 on rc.reviewed_by = ui1.user_info_id
		
	where ea.entity_id = 491
	and audit_type = '1'

)


GO
