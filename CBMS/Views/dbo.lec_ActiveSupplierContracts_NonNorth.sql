SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE     view
[dbo].[lec_ActiveSupplierContracts_NonNorth]
as

--query built to accomodate request from jeff sullivan
-- to look for active supplier contracts and have the query run every monday 
select distinct client_name Client
	,state_name State
	,ed_contract_number	 'Contract Number'
	,v.vendor_name Supplier
	,v1.vendor_name Utility
	,e.entity_name Commodity
	,e1.entity_name as ContractRecalcType
from   contract c
join   supplier_account_meter_map samm on samm.contract_id = c.contract_id
join   meter m on m.meter_id = samm.meter_id
join   account acct on acct.account_id = m.account_id
join   address addr on addr.address_id = m.address_id
join   state st on st.state_id = addr.state_id
join   entity e on e.entity_id = c.commodity_type_id
join   vendor v1 on v1.vendor_id = acct.vendor_id
join   account acct1 on acct1.account_id = samm.account_id
join   vendor v on v.vendor_id = acct1.vendor_id
join   site s on s.site_id = addr.address_parent_id
join   division d on d.division_id= s.division_id
join   client cl on cl.client_id = d.client_id
join   entity e1 on e1.entity_id = c.contract_recalc_type_id
where  c.contract_type_id = 153

and    getdate() between contract_start_date and contract_end_date
and st.country_id not in (1,4)
and s.not_managed = '0'
and s.site_type_id = 250

--and ed_contract_number = '21523-0004'
















GO
