SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







--set transaction isolation level read uncommitted
create       view
 [dbo].[lec_FERC_ContractVolumes_Updated]
 as

select distinct client_name [Client]
,cl.client_id [ClientID]
 ,division_name [Division]
 
 ,c.ed_contract_number [ContractNumber]
 ,month_identifier [Month]
 ,'Supplier' [ContractType]
 ,baseload_volume [ContractVolume]

 ,e.entity_name [ContractVolumeUofM]
 ,e1.entity_name Frequency
,ConvertedVolume = CASE when 
			c.contract_id in 
			(select distinct c.contract_id
				from contract c
				join supplier_account_meter_map samm on samm.account_id = c.contract_id and (samm.meter_disassociation_date is null or samm.METER_DISASSOCIATION_DATE > c.CONTRACT_START_DATE)
				join meter m on m.meter_id = samm.meter_id
				join address addr on addr.address_id = m.address_id
				join site s on s.site_id = addr.address_parent_id
				group by c.contract_id
			  having count(distinct s.division_id) > '1')
				
and perclientmv > '0' and lps_frequency_type_id in (237,239) then baseload_volume * (PerDivisionMV/PerClientMV)* cuc.conversion_factor 
			when 
			c.contract_id in 
			(select distinct c.contract_id
				from contract c
				join supplier_account_meter_map samm on samm.account_id = c.contract_id and samm.meter_disassociation_date is null
				join meter m on m.meter_id = samm.meter_id
				join address addr on addr.address_id = m.address_id
				join site s on s.site_id = addr.address_parent_id
				group by c.contract_id
			  having count(distinct s.division_id) > '1')and perclientmv > '0' and lps_frequency_type_id = 238 then baseload_volume * (PerDivisionMV/PerClientMV)* cuc.conversion_factor * 30.4  
 when lps_frequency_type_id in (237,239) then baseload_volume * cuc.conversion_factor
	when lps_frequency_type_id = 238 then baseload_Volume * cuc.conversion_Factor * 30.4  else null end


 ,'MMBtu' as ConvertedUofM
 ,'Monthly' as ConvertedFrequency

 ,baseload_name [Name]

-- ,lps.lps_unit_type_id

 ,e2.entity_name LPSType --can be baseload, swing, or cashout
-- ,pin.pricing_point [PricingPoint]
 ,PricingPoint = case when pin.pricing_point is not null then pin.pricing_point when pin.pricing_point is null and lps.contract_id is not null then 'Fixed'
	else 'NOT Built;CPS Reflects Index pricing'  end
 ,contract_pricing_summary [Contract Pricing Summary]
 ,pin.index_description [IndexDescription]
 ,PriceIndex =  e3.entity_name 
 ,e7.entity_name [PriceIndexUofM]
 ,CPS_Includes_IndexPricing = case when
	--pin.pricing_point is not null and pin.pricing_point <> 'NYMEX' or 
 (c.contract_pricing_summary like '%index%'
or contract_pricing_summary like '%algon%'
or contract_pricing_summary like '%anr%'
or contract_pricing_summary like '%appalac%'
or contract_pricing_summary like '%cal%'
or contract_pricing_summary like '%cegt%'
or contract_pricing_summary like '%chicago%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%cne%'
or contract_pricing_summary like '%dominion%'
or contract_pricing_summary like '%ferc%'
or contract_pricing_summary like '%fgt%'
or contract_pricing_summary like '%gas daily%'
or contract_pricing_summary like '%gdd%'
--or contract_pricing_summary like '%gulf%'
or contract_pricing_summary like '%hsc%'
or contract_pricing_summary like '%if%'
or contract_pricing_summary like '%mid americ%'
or contract_pricing_summary like '%midam%'
or contract_pricing_summary like '%ngi%'
or contract_pricing_summary like '%nwpl%'
or contract_pricing_summary like '%pepl%'
or contract_pricing_summary like '%pg&E%'
or contract_pricing_summary like '%pge%'
or contract_pricing_summary like '%questar%'
or contract_pricing_summary like '%rg&e%'
or contract_pricing_summary like '%san jua%'
or contract_pricing_summary like '%socal%'
or contract_pricing_summary like '%sonat%'
or contract_pricing_summary like '%southern%'
or contract_pricing_summary like '%southpoint%'
or contract_pricing_summary like '%sumas%'
or contract_pricing_summary like '%tco%'
or contract_pricing_summary like '%tgp%'
or contract_pricing_summary like '%tgt%'
or contract_pricing_summary like '%tn zn%'
or contract_pricing_summary like '%transco%'
or contract_pricing_summary like '%trunk%'
or contract_pricing_summary like '%vanguar%'
or contract_pricing_summary like '%ventura%'
or contract_pricing_summary like '%waha%'
or contract_pricing_summary like '%centerpoint%'
or contract_pricing_summary like '%cgpr%'
or contract_pricing_summary like '%cheyenne%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%colorado%'
or contract_pricing_summary like '%columbia%'
or contract_pricing_summary like '%Consumers%'
or contract_pricing_summary like '%Dracut%'
or contract_pricing_summary like '%El Paso%'
or contract_pricing_summary like '%Florida%'
or contract_pricing_summary like '%Henry%'
or contract_pricing_summary like '%Houston%'
or contract_pricing_summary like '%Iroquois%'
or contract_pricing_summary like '%Katy%'
or contract_pricing_summary like '%Louisiana%'
or contract_pricing_summary like '%Mich%'
or contract_pricing_summary like '%Midcontinent%'
or contract_pricing_summary like '%Midwest%'
or contract_pricing_summary like '%Natural%'
or contract_pricing_summary like '%Niagara%'
or contract_pricing_summary like '%Northern%'
or contract_pricing_summary like '%Northwest%'
or contract_pricing_summary like '%Oneok%'
or contract_pricing_summary like '%Panhandle%'
or contract_pricing_summary like '%PG&E%'
or contract_pricing_summary like '%Rocky%'
or contract_pricing_summary like '%So Cal%'
or contract_pricing_summary like '%Southern%'
or contract_pricing_summary like '%TCPL%'
or contract_pricing_summary like '%Tenn%'
or contract_pricing_summary like '%Texas%'
or contract_pricing_summary like '%Transwestern%'

) then 'Yes' else 'No' end
,IndexPricingBuilt_OrIndexPricingInCPS = case when
	(pin.pricing_point is not null and pin.pricing_point <> 'NYMEX') or 
 (c.contract_pricing_summary like '%index%'
or contract_pricing_summary like '%algon%'
or contract_pricing_summary like '%anr%'
or contract_pricing_summary like '%appalac%'
or contract_pricing_summary like '%cal%'
or contract_pricing_summary like '%cegt%'
or contract_pricing_summary like '%chicago%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%cne%'
or contract_pricing_summary like '%dominion%'
or contract_pricing_summary like '%ferc%'
or contract_pricing_summary like '%fgt%'
or contract_pricing_summary like '%gas daily%'
or contract_pricing_summary like '%gdd%'
--or contract_pricing_summary like '%gulf%'
or contract_pricing_summary like '%hsc%'
or contract_pricing_summary like '%if%'
or contract_pricing_summary like '%mid americ%'
or contract_pricing_summary like '%midam%'
or contract_pricing_summary like '%ngi%'
or contract_pricing_summary like '%nwpl%'
or contract_pricing_summary like '%pepl%'
or contract_pricing_summary like '%pg&E%'
or contract_pricing_summary like '%pge%'
or contract_pricing_summary like '%questar%'
or contract_pricing_summary like '%rg&e%'
or contract_pricing_summary like '%san jua%'
or contract_pricing_summary like '%socal%'
or contract_pricing_summary like '%sonat%'
or contract_pricing_summary like '%southern%'
or contract_pricing_summary like '%southpoint%'
or contract_pricing_summary like '%sumas%'
or contract_pricing_summary like '%tco%'
or contract_pricing_summary like '%tgp%'
or contract_pricing_summary like '%tgt%'
or contract_pricing_summary like '%tn zn%'
or contract_pricing_summary like '%transco%'
or contract_pricing_summary like '%trunk%'
or contract_pricing_summary like '%vanguar%'
or contract_pricing_summary like '%ventura%'
or contract_pricing_summary like '%waha%'
or contract_pricing_summary like '%centerpoint%'
or contract_pricing_summary like '%cgpr%'
or contract_pricing_summary like '%cheyenne%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%colorado%'
or contract_pricing_summary like '%columbia%'
or contract_pricing_summary like '%Consumers%'
or contract_pricing_summary like '%Dracut%'
or contract_pricing_summary like '%El Paso%'
or contract_pricing_summary like '%Florida%'
or contract_pricing_summary like '%Henry%'
or contract_pricing_summary like '%Houston%'
or contract_pricing_summary like '%Iroquois%'
or contract_pricing_summary like '%Katy%'
or contract_pricing_summary like '%Louisiana%'
or contract_pricing_summary like '%Mich%'
or contract_pricing_summary like '%Midcontinent%'
or contract_pricing_summary like '%Midwest%'
or contract_pricing_summary like '%Natural%'
or contract_pricing_summary like '%Niagara%'
or contract_pricing_summary like '%Northern%'
or contract_pricing_summary like '%Northwest%'
or contract_pricing_summary like '%Oneok%'
or contract_pricing_summary like '%Panhandle%'
or contract_pricing_summary like '%PG&E%'
or contract_pricing_summary like '%Rocky%'
or contract_pricing_summary like '%So Cal%'
or contract_pricing_summary like '%Southern%'
or contract_pricing_summary like '%TCPL%'
or contract_pricing_summary like '%Tenn%'
or contract_pricing_summary like '%Texas%'
or contract_pricing_summary like '%Transwestern%'

) then 'Yes' else 'No' end
from   contract c
join   supplier_account_meter_map samm on samm.contract_id = c.contract_id
join   meter m on m.meter_id = samm.meter_id
join   account acct on acct.account_id = m.account_id
join   site s on s.site_id = acct.site_id
join   division d on d.division_id = s.division_id
join   client cl on cl.client_id = d.client_id
join   address addr on addr.address_id = s.primary_address_id
join   state st on st.state_id = addr.state_id and st.country_id = '4'
left join   load_profile_specification lps on lps.contract_id = c.contract_id and month_identifier between '2009-01-01' and '2009-12-01'
left join   baseload_details bd on bd.load_profile_specification_id = lps.load_profile_specification_id
left join   baseload b on b.baseload_id = bd.baseload_id
left join   consumption_unit_conversion cuc on cuc.base_unit_id = lps.lps_unit_type_id and cuc.converted_unit_id = 25
left join   entity e1 on e1.entity_id = lps.lps_frequency_type_id
left join   entity e on e.entity_id = lps_unit_type_id
left join   entity e2 on e2.entity_id = lps.lps_type_id
left join   price_index pin on pin.price_index_id = lps.price_index_id
left join   entity e3 on e3.entity_id= pin.index_id
left join entity e7 on e7.entity_id = pin.volume_unit_id

left join (
	select division_id
		,x.client_id
		,sum(meteredvolume) PerDivisionMV
		,x.contract_id
	from (
	(select distinct s.division_id 
			,MeteredVolume = case when cmv.frequency_type_id in (237,239) then (volume * cuc1.conversion_factor)
	when cmv.frequency_type_id = 238 then (Volume * cuc1.conversion_Factor * 30.4)  end
			,m.meter_id
			,cmv.month_identifier
			,c1.contract_id
			,d.client_id
		from contract c1
			join contract_meter_volume cmv on cmv.contract_id = c1.CONTRACT_ID
			join supplier_account_meter_map samm  on samm.contract_id = c1.contract_id and (samm.meter_disassociation_date is null or samm.METER_DISASSOCIATION_DATE > c1.CONTRACT_START_DATE)
			join meter m on m.meter_id = cmv.meter_id
			join address addr on addr.address_id = m.address_id
			join state st on st.state_id = addr.state_id and country_id = '4'
			join site s on s.site_id = addr.address_parent_id
			join division d on d.division_id = s.division_id
			left join   entity e5 on e5.entity_id = cmv.frequency_type_id
			left join   entity e6 on e6.entity_id = cmv.unit_type_id
			left join   consumption_unit_conversion cuc1 on cuc1.base_unit_id = cmv.unit_type_id and cuc1.converted_unit_id = 25
			where c1.contract_type_id = 153
			and month_identifier between '2009-01-01' and '2009-12-01'
			--and c1.contract_id = 55753
			))x
		group by division_id, client_id, contract_id) dmv on dmv.division_id = d.division_id and dmv.contract_id = c.contract_id

	left join (
	select x.client_id
		,sum(meteredvolume) PerClientMV
		,x.contract_id
	from (
	(select distinct s.division_id 
			,MeteredVolume = case when cmv.frequency_type_id in (237,239) then (volume * cuc1.conversion_factor)
	when cmv.frequency_type_id = 238 then (Volume * cuc1.conversion_Factor * 30.4)  end
			,m.meter_id
			,cmv.month_identifier
			,c1.contract_id
			,d.client_id
		from contract c1
			join contract_meter_volume cmv on cmv.contract_id = c1.contract_id
			join supplier_account_meter_map samm  on samm.contract_id = c1.contract_id and (samm.meter_disassociation_date is null or samm.METER_DISASSOCIATION_DATE > c1.CONTRACT_START_DATE)
			join meter m on m.meter_id = cmv.meter_id
			join address addr on addr.address_id = m.address_id
			join state st on st.state_id = addr.state_id and country_id = '4'
			join site s on s.site_id = addr.address_parent_id
			join division d on d.division_id = s.division_id
			left join   entity e5 on e5.entity_id = cmv.frequency_type_id
			left join   entity e6 on e6.entity_id = cmv.unit_type_id
			left join   consumption_unit_conversion cuc1 on cuc1.base_unit_id = cmv.unit_type_id and cuc1.converted_unit_id = 25
			where c1.contract_type_id = 153
			and month_identifier between '2009-01-01' and '2009-12-01'
			--and c1.contract_id = 55753
			))x
		group by client_id, contract_id) clmv on clmv.client_id = cl.client_id  and clmv.contract_id = c.contract_id

	



where 
 c.commodity_type_id = 291
and (lps.contract_id is not null or (c.contract_pricing_summary like '%index%'
or contract_pricing_summary like '%algon%'
or contract_pricing_summary like '%anr%'
or contract_pricing_summary like '%appalac%'
or contract_pricing_summary like '%cal%'
or contract_pricing_summary like '%cegt%'
or contract_pricing_summary like '%chicago%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%cne%'
or contract_pricing_summary like '%dominion%'
or contract_pricing_summary like '%ferc%'
or contract_pricing_summary like '%fgt%'
or contract_pricing_summary like '%gas daily%'
or contract_pricing_summary like '%gdd%'
or contract_pricing_summary like '%gulf%'
or contract_pricing_summary like '%hsc%'
or contract_pricing_summary like '%if%'
or contract_pricing_summary like '%mid americ%'
or contract_pricing_summary like '%midam%'
or contract_pricing_summary like '%ngi%'
or contract_pricing_summary like '%nwpl%'
or contract_pricing_summary like '%pepl%'
or contract_pricing_summary like '%pg&E%'
or contract_pricing_summary like '%pge%'
or contract_pricing_summary like '%questar%'
or contract_pricing_summary like '%rg&e%'
or contract_pricing_summary like '%san jua%'
or contract_pricing_summary like '%socal%'
or contract_pricing_summary like '%sonat%'
or contract_pricing_summary like '%southern%'
or contract_pricing_summary like '%southpoint%'
or contract_pricing_summary like '%sumas%'
or contract_pricing_summary like '%tco%'
or contract_pricing_summary like '%tgp%'
or contract_pricing_summary like '%tgt%'
or contract_pricing_summary like '%tn zn%'
or contract_pricing_summary like '%transco%'
or contract_pricing_summary like '%trunk%'
or contract_pricing_summary like '%vanguar%'
or contract_pricing_summary like '%ventura%'
or contract_pricing_summary like '%waha%'
or contract_pricing_summary like '%centerpoint%'
or contract_pricing_summary like '%cgpr%'
or contract_pricing_summary like '%cheyenne%'
or contract_pricing_summary like '%cig%'
or contract_pricing_summary like '%colorado%'
or contract_pricing_summary like '%columbia%'
or contract_pricing_summary like '%Consumers%'
or contract_pricing_summary like '%Dracut%'
or contract_pricing_summary like '%El Paso%'
or contract_pricing_summary like '%Florida%'
or contract_pricing_summary like '%Henry%'
or contract_pricing_summary like '%Houston%'
or contract_pricing_summary like '%Iroquois%'
or contract_pricing_summary like '%Katy%'
or contract_pricing_summary like '%Louisiana%'
or contract_pricing_summary like '%Mich%'
or contract_pricing_summary like '%Midcontinent%'
or contract_pricing_summary like '%Midwest%'
or contract_pricing_summary like '%Natural%'
or contract_pricing_summary like '%Niagara%'
or contract_pricing_summary like '%Northern%'
or contract_pricing_summary like '%Northwest%'
or contract_pricing_summary like '%Oneok%'
or contract_pricing_summary like '%Panhandle%'
or contract_pricing_summary like '%PG&E%'
or contract_pricing_summary like '%Rocky%'
or contract_pricing_summary like '%So Cal%'
or contract_pricing_summary like '%Southern%'
or contract_pricing_summary like '%TCPL%'
or contract_pricing_summary like '%Tenn%'
or contract_pricing_summary like '%Texas%'
or contract_pricing_summary like '%Transwestern%'


))
--

and ((c.contract_start_date between '2009-01-01' and '2009-12-31') or (contract_end_date between '2009-01-01' and '2009-12-31')
						or (c.contract_start_date < '2009-01-01' and contract_end_date > '2009-12-31'))
						

and c.contract_type_id = 153
and cl.not_managed <> '1'
--and cl.client_id = 11303


--order by contractnumber, month
--option (maxdop 1)



GO
