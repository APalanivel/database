SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE View [dbo].[DMR_UbmInvoice_GetAllForProcessing_VW]
as
   select 
		  i.ubm_invoice_id
		, i.cbms_image_id
		, i.ubm_batch_master_log_id
		, min(det.service_start_date) begin_date
		, i.cu_invoice_Batch_id		
     from ubm_invoice i  with (nolock)
  left outer join ubm_invoice_details det with (nolock) on det.ubm_invoice_id = i.ubm_invoice_id
	 group by i.ubm_invoice_id
		, i.cbms_image_id
		, i.ubm_batch_master_log_id
		, i.cu_invoice_Batch_id

GO
