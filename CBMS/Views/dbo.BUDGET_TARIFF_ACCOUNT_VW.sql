SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- select * from BUDGET_TARIFF_ACCOUNT_VW
--23103


CREATE   VIEW dbo.BUDGET_TARIFF_ACCOUNT_VW
	AS

	select	utilacc.account_id,
		'Tariff' as Tariff_Transport
		,r.commodity_type_id
		
	from	account utilacc
		join meter m on m.account_id = utilacc.account_id
		join rate r on r.rate_id = m.rate_id
		left join BUDGET_TRANSPORT_ACCOUNT_VW vw
		on  vw.account_id = utilacc.account_id 
		and vw.commodity_type_id = r.commodity_type_id
		

	where	utilacc.account_type_id = 38 
		and vw.account_id is null

	group by utilacc.account_id ,r.commodity_type_id



GO
