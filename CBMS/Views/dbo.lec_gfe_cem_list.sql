SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  view 
[dbo].[lec_gfe_cem_list]
as
select distinct client_id
	,cem
from (

	-- OneCEM
select  distinct (first_name + ' ' + last_name) as CEM
		,cl.client_id
	--,cl.client_id
		from  client_cem_map ccm
		join  client cl on cl.client_id = ccm.client_id
		join  user_info ui on ui.user_info_id = ccm.user_info_id
		where (cl.client_type_id = 1455 or cl.client_name like '%cytec%')
		and cl.client_id in (
				(select --distinct count (distinct user_info_id)
				cl.client_id
				from client cl
				join client_cem_map ccm on ccm.client_id = cl.client_id
				group by cl.client_id
				,cl.client_type_id
				,cl.client_name
				having count (distinct user_info_id) ='1'
				and (cl.client_type_id = 1455 or cl.client_name like '%cytec%')))
	union
--TwoCEMs listed

select  distinct max (first_name + ' ' + last_name)+ ', ' + min (first_name + ' ' + last_name) as CEM
		,cl.client_id
	--,cl.client_id
		from  client_cem_map ccm
		join  client cl on cl.client_id = ccm.client_id
		join  user_info ui on ui.user_info_id = ccm.user_info_id
		where 	(cl.client_type_id = 1455 or cl.client_name like '%cytec%')
		and cl.client_id in (
				(select --distinct count (distinct user_info_id)
				cl.client_id
				from client cl
				join client_cem_map ccm on ccm.client_id = cl.client_id
				group by cl.client_id
				,cl.client_type_id
				,cl.client_name
				having count (distinct user_info_id) >'1'
					and (cl.client_type_id = 1455 or cl.client_name like '%cytec%')))
		group by cl.client_id
				,cl.client_name
)y

GO
