SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.SR_ALL_CONTRACT_VW

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
       


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	10/09/2009	Replaced Contract's account_id join with Supplier_account_meter_map by means of contract_id
DMR		  09/10/2010 Modified for Quoted_Identifier

******/
    
    
    
--// Its a generic contract view to join with utility account to get the contract(s) of that account     
    
CREATE  VIEW dbo.SR_ALL_CONTRACT_VW  
 AS        
    
 select  utilacc.account_id,    
  met.meter_id,    
  con.commodity_type_id,    
  con.contract_id,    
  con.ed_contract_number,    
  con.contract_start_date,    
  con.contract_end_date,    
  con.notification_days,    
  suppacc.vendor_id    
      
 from  dbo.account utilacc(nolock)     
  join dbo.meter met(NOLOCK) on met.account_id = utilacc.account_id    
  and utilacc.account_type_id = (select entity_id from entity(nolock) where entity_type = 111 and entity_name = 'Utility')    
  join dbo.supplier_account_meter_map meterMap(NOLOCK) on meterMap.meter_id = met.meter_id    
  and meterMap.is_history = 0    
  join dbo.account suppacc(NOLOCK) on meterMap.account_id = suppacc.account_id    
  join dbo.contract con(NOLOCK) on meterMap.contract_id = con.contract_id
GO
