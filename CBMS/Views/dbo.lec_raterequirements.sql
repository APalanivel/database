SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_raterequirements]
as
select distinct rate_requirements
	,rate_id
	,rate_name
	, vendor_name
from rate r
join vendor v on r.vendor_id = v.vendor_id
where rate_requirements like '%?%'
--order by rate_requirements
GO
