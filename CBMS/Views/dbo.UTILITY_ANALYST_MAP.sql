
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.Utility_Analyst_Map

DESCRIPTION:

	Used to get the analyst list for each utility.

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
   
   SELECT TOP 10 * FROM utility_Analyst_Map

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hari

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	HG        	06/17/2010	Comments header added
							Added UNION to pull the gas analyst and power analyst information from Vendor_commodity_analyst_map table.
	HG			2014-09-09	Modified for Data Transition enhancement, hardcoded group name replaced with the Group_Legacy_Name_Cd column value.
******/
CREATE VIEW [dbo].[UTILITY_ANALYST_MAP]
AS
SELECT
      Utility_Detail_ID
     ,[Regulated_Market] AS Regulated_Analyst_ID
     ,[Quality_Assurance] AS Deregulated_Analyst_ID
     ,[Natural Gas] AS Gas_Analyst_ID
     ,[Electric Power] AS Power_Analyst_ID
     ,[INVOICE_Control] AS Sourcing_Ops_Analyst_ID
FROM
      ( SELECT
            udam.Utility_Detail_ID
           ,gln.Code_Value AS Group_Name
           ,udam.Analyst_Id
        FROM
            dbo.Utility_Detail_Analyst_Map UDAM
            INNER JOIN dbo.GROUP_INFO GI
                  ON UDAM.Group_Info_ID = GI.GROUP_INFO_ID
            INNER JOIN dbo.Code gln
                  ON gln.Code_Id = gi.Group_Legacy_Name_Cd
        WHERE
            GI.GROUP_NAME NOT IN ( 'Gas Analyst', 'Power Analyst' )
        UNION
        SELECT
            ud.UTILITY_DETAIL_ID
           ,com.Commodity_Name
           ,vcam.Analyst_Id
        FROM
            dbo.VENDOR_COMMODITY_MAP vcm
            INNER JOIN dbo.Vendor_Commodity_Analyst_Map vcam
                  ON vcam.Vendor_Commodity_Map_Id = vcm.VENDOR_COMMODITY_MAP_ID
            INNER JOIN dbo.UTILITY_DETAIL ud
                  ON ud.VENDOR_ID = vcm.VENDOR_ID
            INNER JOIN dbo.Commodity com
                  ON com.Commodity_Id = vcm.COMMODITY_TYPE_ID
        WHERE
            com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' ) ) AS p PIVOT ( MAX(Analyst_Id) FOR Group_Name IN ( [Regulated_Market], [Quality_Assurance], [Invoice_Control], [Electric Power], [Natural Gas] ) ) AS pvt
GO
