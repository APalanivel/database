SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  View
[dbo].[LEC_SiteListing] 
as
--Query to determine list of sites, their primary addresses
--and whether the site is managed or closed

select	ClientID	
	,ClientName
	,DivisionID
	,DivisionName
	,SiteId
	,PrimaryAddress
	,PrimaryCity
	,State
	,ZipCode
	,SiteName
	,Region
	,'SiteNotManaged' = isnull (case x.sitenotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,'SiteClosed' = isnull (case x.siteclosed when '1' then 'Yes' else 'No' end, 'Null')


from
(
select cl.client_id as ClientID
	,cl.Client_name as ClientName
	,d.Division_ID as DivisionID
	,Division_Name as DivisionName
	,s.Site_ID as SiteID
	,Address_line1 as PrimaryAddress
	,City as PrimaryCity
	,State_name as State
	,zipcode as ZipCode
	,site_name as SiteName
	,region_name as Region
	,s.not_managed as SiteNotManaged
	,s.closed as SiteClosed
	
from    client cl
join    division d on cl.client_id = d.client_id
join	site s on d.division_id = s.division_id
join    address addr on s.primary_address_id = addr.address_id
join    state st on addr.state_id = st.state_id
join    region r on st.region_id = r.region_id
)
x






GO
