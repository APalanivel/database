SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_userinfo]
as
--dv users
select       distinct 
	     client_name as 'Client Name'
	    ,division_name as 'Division Name'
	    ,site_name as 'Site Name'
	    ,first_name as 'First Name'
	    ,last_name as 'Last Name'
	    ,phone_number as 'Phone Number'
	    ,email_address as 'Email Address'
	    ,'DV' as 'User Type'
from        user_info ui
left join   client cl on cl.client_id = ui.client_id
left join   division d on d.division_id = ui.division_id
left join   site s on s.site_id = ui.site_id
where       access_level = '1'
	    and
	    ui.is_history = '0'


union
--sv users
select       distinct 
	     client_name as 'Client Name'
	    ,division_name as 'Division Name'
	    ,site_name as 'Site Name'
	    ,first_name as 'First Name'
	    ,last_name as 'Last Name'
	    --,work_phone as 'Phone Number'
	    ,'Phone Number ' = case when work_phone <> null then work_phone else phone_number end
	    ,email_address as 'Email Address'
	    ,'SV' as 'User Type'
from        user_info ui
left join   client cl on cl.client_id = ui.client_id
left join   division d on d.division_id = ui.division_id
left join   site s on s.site_id = ui.site_id
left join   sr_supplier_contact_info s_con on s_con.user_info_id = ui.user_info_id
where       access_level is null
	    and
	    ui.is_history = '0'
	    and
	    username <> 'sjett'



GO
