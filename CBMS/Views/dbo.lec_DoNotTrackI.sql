SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_DoNotTrackI]
as
/*
select distinct cl.client_name
	,site_name
	,s.site_id
	,city
	
	,acct.account_number
	,meter_number
	,e.entity_name as commodity
	,v.vendor_name 
	,Managed = case when acct.not_managed = '1' then 'NotManaged' else 'Managed' end
	,DoNotTrack = case when dnt.account_id is not null then	'DoNotTrack' else 'No' end
	,e1.entity_name as reason
from account acct
join meter m on m.account_id = acct.account_id
join address addr on addr.address_id = m.address_id
join state st on st.state_id = addr.state_id
join site s on s.site_id = addr.address_parent_id
join division d on d.division_id = s.division_id
join client cl on cl.client_id = d.client_id
left join do_not_track dnt on dnt.account_id = acct.account_id
join rate r on r.rate_id = m.rate_id
join entity e on e.entity_id = r.commodity_type_id
join vendor v on v.vendor_id = acct.vendor_id
left join entity e1 on e1.entity_id = dnt.reason_type_id
where state_name = 'tx'
and e.entity_name = 'electric power'
*/

select distinct dnt.client_name
	,'Unknown' as site_name
	,'Unknown' as site_id
	,client_city as city
	,account_number
	,'Unknown' as meter_number
	,'Unknown' as Commodity
	,vendor_name
	,'Unknown' as Managed
	,'Yes' as DoNotTrack
	,e.entity_name as 'Reason'
from do_not_track dnt
join state st on st.state_id = dnt.state_id
join entity e on e.entity_id = dnt.reason_type_id
where state_name = 'tx'
and dnt.account_id is null


GO
