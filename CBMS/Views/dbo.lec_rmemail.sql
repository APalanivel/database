SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_rmemail]
as
select *
from rm_email_log
where generation_date > '2005-05-15'
GO
