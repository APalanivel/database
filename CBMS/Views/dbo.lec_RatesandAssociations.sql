SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE    view
[dbo].[lec_RatesandAssociations]
as
select distinct
	Client_name
	,s.site_id
	,city
	,state_name
	,account_number
	,meter_number
	,vendor_name
	,r.rate_id
	,rate_name
	,entity_name
	--,rate_schedule_id
	--,rs_start_date
	--,rs_end_date
from meter m 
join rate r on m.rate_id = r.rate_id
join vendor v on r.vendor_id = v.vendor_id
join account acct on m.account_id = acct.account_id
join address addr on m.address_id = addr.address_id
join site s on addr.address_parent_id = s.site_id
join state st on addr.state_id = st.state_id
join division d on s.division_id = d.division_id
join client cl on d.client_id = cl.client_id
join rate_schedule rs on r.rate_id = rs.rate_id

join entity e on r.commodity_type_id = e.entity_id
--order by vendor_name, rate_name



GO
