SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





create  VIEW dbo.vwSsoProject_LastActivity
AS
SELECT     pa.ACTIVITY_DESCRIPTION, pa.SSO_PROJECT_ACTIVITY_ID, pa.SSO_PROJECT_ID, pa.CREATED_BY_ID, pa.ACTIVITY_DATE
FROM         dbo.SSO_PROJECT p INNER JOIN
                          (SELECT     pa.sso_project_id, MAX(pa.sso_project_activity_id) sso_project_activity_id
                            FROM          sso_project_activity pa
                            GROUP BY pa.sso_project_id) x ON x.sso_project_id = p.SSO_PROJECT_ID INNER JOIN
                      dbo.SSO_PROJECT_ACTIVITY pa ON pa.SSO_PROJECT_ACTIVITY_ID = x.sso_project_activity_id





GO
