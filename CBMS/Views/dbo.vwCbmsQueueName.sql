SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







create view [dbo].[vwCbmsQueueName]
AS


	   select q.queue_id
		, q.queue_type_id
		, qt.entity_name queue_type
	   	, ui.first_name + ' ' + ui.last_name queue_name
	     from queue q
	     join user_info ui on ui.queue_id = q.queue_id
	     join entity qt on qt.entity_id = q.queue_type_id
	    where q.queue_type_id = 243
	union all
	   select q.queue_id
		, q.queue_type_id
		, qt.entity_name queue_type
	   	, gi.group_name queue_name
	     from queue q
	     join group_info gi on gi.queue_id = q.queue_id
	     join entity qt on qt.entity_id = q.queue_type_id
	    where q.queue_type_id = 244











GO
GRANT SELECT ON  [dbo].[vwCbmsQueueName] TO [BPOReportRole]
GO
