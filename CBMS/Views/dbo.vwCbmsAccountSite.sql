SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
 dbo.vwCbmsAccountSite  
 DESCRIPTION:   
 
 INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------

 --select top 5 * from vwCbmsAccountSite where supplier_account_begin_dt is not null
	select top 5 * from vwCbmsAccountSite where Contract_Id =-1
	select top 5 * from vwCbmsAccountSite where Contract_Id =-1 AND Supplier_Account_End_Dt IS NULL

 AUTHOR 
 INITIALS:  Initials Name  
------------------------------------------------------------  
 SS			Subhash		
 HG			Hari
 RR			Raghu Reddy
  
 MODIFICATIONS   
 Initials	Date		Modification  
------------------------------------------------------------  
 SS 		9/22/2009   For adding Supplier Account start and end dates
 HG			10/27/2009	Old Meter_Association_Date and Meter_Disassociation_date logic included back.
 DMR		09/10/2010	Modified for Quoted_Identifier
 RR			2017-03-17	Contract placeholder - Modified join on dbo.contract table, no contract for DMO supplier supplier accounts
******/
  
CREATE VIEW [dbo].[vwCbmsAccountSite]
AS
SELECT
      a.site_id
     ,a.account_id
     ,a.account_type_id
     ,a.account_number
     ,r.commodity_type_id
     ,CAST(NULL AS DATETIME) Supplier_Account_Begin_Dt
     ,CAST(NULL AS DATETIME) Supplier_Account_End_Dt
     ,NULL Contract_Id
FROM
      dbo.account a
      JOIN dbo.meter m
            ON m.account_id = a.account_id
      JOIN dbo.rate r
            ON r.rate_id = m.rate_id
UNION ALL
SELECT
      ad.address_parent_id site_id
     ,a.account_id
     ,a.account_type_id
     ,ISNULL(RTRIM(a.account_number), 'Blank for ' + v.vendor_name) + ' (' + CASE WHEN MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt)) < a.Supplier_Account_Begin_Dt THEN CONVERT(VARCHAR, a.Supplier_Account_Begin_Dt, 101)
                                                                                  ELSE CONVERT(VARCHAR, MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt)), 101)
                                                                             END + '-' + ISNULL(CASE WHEN MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt)) > a.Supplier_Account_End_Dt THEN CONVERT(VARCHAR, a.Supplier_Account_End_Dt, 101)
                                                                                                     ELSE CONVERT(VARCHAR, MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt)), 101)
                                                                                                END, 'Unspecified') + ')'
     ,ISNULL(c.commodity_type_id, r.commodity_type_id) AS commodity_type_id
     ,Supplier_Account_Begin_Dt = CASE WHEN MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt)) < a.Supplier_Account_Begin_Dt THEN a.Supplier_Account_Begin_Dt
                                       ELSE MIN(ISNULL(map.meter_association_date, a.Supplier_Account_Begin_Dt))
                                  END
     ,Supplier_Account_End_Dt = CASE WHEN MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt)) > a.Supplier_Account_End_Dt THEN a.Supplier_Account_End_Dt
                                     ELSE MAX(ISNULL(map.meter_disassociation_date, a.Supplier_Account_End_Dt))
                                END
     ,map.Contract_Id
FROM
      dbo.account a
      JOIN dbo.supplier_account_meter_map map
            ON map.account_id = a.account_id
      LEFT JOIN dbo.contract c
            ON c.contract_id = map.contract_id
      JOIN dbo.vendor v
            ON v.vendor_id = a.vendor_id
      JOIN dbo.meter m
            ON m.meter_id = map.meter_id
      JOIN dbo.rate r
            ON r.rate_id = m.rate_id
      JOIN dbo.address ad
            ON ad.address_id = m.address_id
WHERE
      map.is_history = 0
GROUP BY
      ad.address_parent_id
     ,a.account_id
     ,a.account_type_id
     ,a.account_number
     ,v.vendor_name
     ,ISNULL(c.commodity_type_id, r.commodity_type_id)
     ,a.Supplier_Account_Begin_Dt
     ,a.Supplier_Account_End_Dt
     ,map.Contract_Id

;
GO
