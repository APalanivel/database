SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[CBMS_Replication_Status_vw]
AS	
SELECT
      art.name
     ,t.article_id
     ,s.agent_id
     ,agt.subscriber_db
     ,'UndelivCmdsInDistDB' = SUM(CASE WHEN xact_seqno > h.maxseq THEN 1
                                       ELSE 0
                                  END)
     ,'DelivCmdsInDistDB' = SUM(CASE WHEN xact_seqno <= h.maxseq THEN 1
                                     ELSE 0
                                END)
FROM
      ( SELECT
            article_id
           ,publisher_database_id
           ,xact_seqno
        FROM
            distribution.dbo.MSrepl_commands with ( NOLOCK ) ) as t
      JOIN ( SELECT
                  agent_id
                 ,article_id
                 ,publisher_database_id
             FROM
                  distribution.dbo.MSsubscriptions with ( NOLOCK ) ) AS s
            ON ( t.article_id = s.article_id
                 AND t.publisher_database_id = s.publisher_database_id )
      JOIN ( SELECT
                  agent_id
                 ,'maxseq' = isnull(max(xact_seqno), 0x0)
             FROM
                  distribution.dbo.MSdistribution_history with ( NOLOCK )             
             GROUP BY
                  agent_id ) as h
            ON ( h.agent_id = s.agent_id )
     JOIN distribution.dbo.MSdistribution_agents agt ON s.agent_id = agt.id AND agt.subscriber_id > 0
     JOIN CBMS.dbo.Sysarticles art ON t.article_id = art.artid	
     
GROUP BY
	  art.name
     ,t.article_id
     ,s.agent_id  
     ,agt.subscriber_db

GO
