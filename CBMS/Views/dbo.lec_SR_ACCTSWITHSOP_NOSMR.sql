SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
 
[dbo].[lec_SR_ACCTSWITHSOP_NOSMR]
as

select client_name
	,site_name
	,rfp.sr_rfp_id
	,account_number
	,address_line1
	,state_name
from sr_rfp rfp
join sr_rfp_term term on term.sr_rfp_id = rfp.sr_rfp_id
join sr_rfp_account_term actterm on actterm.sr_rfp_term_id = term.sr_rfp_term_id
left join sr_rfp_sop_smr smr on smr.sr_account_group_id = actterm.sr_account_group_id
join entity e on e.entity_id = rfp.commodity_type_id
join sr_rfp_account rfpacct on rfpacct.sr_rfp_account_id = actterm.sr_account_group_id
join account acct on acct.account_id = rfpacct.account_id
join site s on s.site_id = acct.site_id
join division d on d.division_id = s.division_id
join client cl on cl.client_id = d.client_id
join meter m on m.account_id = acct.account_id
join address addr on addr.address_id = m.address_id
join state st on st.state_id = addr.state_id

where is_sop = '1'
and sr_rfp_sop_smr_id is null
and e.entity_name like '%electric%'
and actterm.is_bid_group = '0'


union
select client_name
	,site_name
	,rfp.sr_rfp_id
	,account_number
	,address_line1
	,state_name
from sr_rfp rfp
join sr_rfp_term term on term.sr_rfp_id = rfp.sr_rfp_id
join sr_rfp_account_term actterm on actterm.sr_rfp_term_id = term.sr_rfp_term_id
left join sr_rfp_sop_smr smr on smr.sr_account_group_id = actterm.sr_account_group_id
join entity e on e.entity_id = rfp.commodity_type_id
join sr_rfp_account rfpacct on rfpacct.sr_rfp_bid_group_id = actterm.sr_account_group_id
join account acct on acct.account_id = rfpacct.account_id
join site s on s.site_id = acct.site_id
join division d on d.division_id = s.division_id
join client cl on cl.client_id = d.client_id
join meter m on m.account_id = acct.account_id
join address addr on addr.address_id = m.address_id
join state st on st.state_id = addr.state_id

where is_sop = '1'
and sr_rfp_sop_smr_id is null
and e.entity_name like '%electric%'
and actterm.is_bid_group = '1'






GO
