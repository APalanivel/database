SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   view
[dbo].[lec_rfp_alternate_issue]
as
select distinct 
		vendor_name
		,username
		,rfp.sr_rfp_id
		,e.entity_name as AlternateFuel
		,from_month
		,to_month
		,account_number
		,client_name
		,site_name
		,e1.entity_name as bid_status
		,prod.product_name
	   from sr_rfp rfp
	   join sr_rfp_term term with (nolock) on term.sr_rfp_id = rfp.sr_rfp_id
   	   join sr_rfp_account_term aterm with (nolock) on aterm.sr_rfp_term_id = term.sr_rfp_term_id
	   join sr_rfp_term_product_map map with (nolock) on map.sr_rfp_account_term_id = aterm.sr_rfp_account_term_id
	   join sr_rfp_bid bid with (nolock) on bid.sr_rfp_bid_id = map.sr_rfp_bid_id
	   join sr_rfp_supplier_service serv with (nolock) on serv.sr_rfp_supplier_service_id = bid.sr_rfp_supplier_service_id
	   join sr_rfp_alternate_fuel fuel with (nolock) on fuel.sr_rfp_alternate_fuel_id = serv.sr_rfp_alternate_fuel_id
	   join entity e with (nolock) on e.entity_id = fuel.buyer_liquidate_type_id
	   join sr_rfp_supplier_contact_vendor_map vmap with (nolock) on vmap.sr_rfp_supplier_contact_vendor_map_id = map.sr_rfp_supplier_contact_vendor_map_id 
	   join vendor v with (nolock) on vmap.vendor_id = v.vendor_id
	   join sr_supplier_contact_info sinfo with (nolock) on sinfo.sr_supplier_contact_info_id = vmap.sr_supplier_contact_info_id
	   join user_info ui with (nolock) on ui.user_info_id = sinfo.user_info_id
	   join sr_rfp_account sacct with (nolock) on sacct.sr_rfp_account_id = aterm.sr_account_group_id
	   join account acct with (nolock) on acct.account_id = sacct.account_id
	   join sr_rfp_selected_products prods with (nolock) on prods.sr_rfp_selected_products_id = map.sr_rfp_selected_products_id
	   join sr_rfp_pricing_product prod with (nolock) on prod.sr_rfp_pricing_product_id = prods.pricing_product_id
--only works for utility accounts that's why using it here
	   join site s with (nolock) on s.site_id = acct.site_id
	   join division d with (nolock) on d.division_id = s.division_id
	   join client cl with (nolock) on cl.client_id = d.client_id
	   join entity e1 with (nolock) on e1.entity_id = sacct.bid_status_type_id

	where (e.entity_name = 'yes'
		or e.entity_name = 'no')
		and (is_bid_group = '0' or is_bid_group is null)

union

select distinct 
		vendor_name
		,username
		,rfp.sr_rfp_id
		,e.entity_name as AlternateFuel
		,from_month
		,to_month
		,account_number
		,client_name
		,site_name
		,e1.entity_name as bid_status
		,prod.product_name
	   from sr_rfp rfp
	   join sr_rfp_term term with (nolock) on term.sr_rfp_id = rfp.sr_rfp_id
   	   join sr_rfp_account_term aterm with (nolock) on aterm.sr_rfp_term_id = term.sr_rfp_term_id
	   join sr_rfp_term_product_map map with (nolock) on map.sr_rfp_account_term_id = aterm.sr_rfp_account_term_id
	   join sr_rfp_bid bid with (nolock) on bid.sr_rfp_bid_id = map.sr_rfp_bid_id
	   join sr_rfp_supplier_service serv with (nolock) on serv.sr_rfp_supplier_service_id = bid.sr_rfp_supplier_service_id
	   join sr_rfp_alternate_fuel fuel with (nolock) on fuel.sr_rfp_alternate_fuel_id = serv.sr_rfp_alternate_fuel_id
	   join entity e with (nolock) on e.entity_id = fuel.buyer_liquidate_type_id
	   join sr_rfp_supplier_contact_vendor_map vmap with (nolock) on vmap.sr_rfp_supplier_contact_vendor_map_id = map.sr_rfp_supplier_contact_vendor_map_id 
	   join vendor v with (nolock) on vmap.vendor_id = v.vendor_id
	   join sr_supplier_contact_info sinfo with (nolock) on sinfo.sr_supplier_contact_info_id = vmap.sr_supplier_contact_info_id
	   join user_info ui with (nolock) on ui.user_info_id = sinfo.user_info_id
	   join sr_rfp_account sacct with (nolock) on sacct.sr_rfp_bid_group_id = aterm.sr_account_group_id
	   join account acct with (nolock) on acct.account_id = sacct.account_id
	   join sr_rfp_selected_products prods with (nolock) on prods.sr_rfp_selected_products_id = map.sr_rfp_selected_products_id
	   join sr_rfp_pricing_product prod with (nolock) on prod.sr_rfp_pricing_product_id = prods.pricing_product_id
--only works for utility accounts that's why using it here
	   join site s with (nolock) on s.site_id = acct.site_id
	   join division d with (nolock) on d.division_id = s.division_id
	   join client cl with (nolock) on cl.client_id = d.client_id
	   join entity e1 with (nolock) on e1.entity_id = sacct.bid_status_type_id

	where (e.entity_name = 'yes'
		or e.entity_name = 'no')
		and (is_bid_group = '1')



GO
