SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_SOPS_EricExceptions]
as
select --convert(varchar, d.closed_date, 101)
		 t.exception_type as exceptions
		, ui.first_name + ' ' + ui.last_name as username
		,(cu_exception_id) 
		,cu_exception_detail_id
		,closed_date
	     from cu_exception_detail d
	     join user_info ui on ui.user_info_id = d.closed_by_id
	     join cu_exception_type t on t.cu_exception_type_id = d.exception_type_id

	    where d.closed_date >= '08/15/2007'
	      and d.is_closed = 1
	      and user_info_id in (	
			select user_info_id
			  from user_info
			 where 
		             username like '%egib%'
			    
			  )

	--order by cu_exception_id



GO
