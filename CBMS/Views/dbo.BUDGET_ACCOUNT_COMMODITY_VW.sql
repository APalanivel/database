SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW dbo.BUDGET_ACCOUNT_COMMODITY_VW
	AS
	select 	utilacc.account_id,
		r.commodity_type_id
	 
	from 	account utilacc
		join meter met on met.account_id = utilacc.account_id
		and utilacc.account_type_id = 38 --//Utility account type
		join rate r on r.rate_id = met.rate_id


	group by utilacc.account_id,
		 r.commodity_type_id



GO
