SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_Demand_Utilities]
as
select distinct 
	vendor_name
	--,e.entity_name as CommmodityType
	--,vendor_type_id
	,state_name as State_Province
	,Region_name as Region
from vendor v 
join vendor_commodity_map vcm on v.vendor_id = vcm.vendor_id
join vendor_state_map vsm on v.vendor_id = vsm.vendor_id
join state st on vsm.state_id = st.state_id
join region reg on st.region_id = reg.region_id
join entity e on vcm.commodity_type_id = e.entity_id

where e.entity_name = 'Electric Power'
and vendor_type_id = 289
and vendor_name like '%utilities%'
or vendor_name like '%muni%'
or vendor_name like '%board%'
or vendor_name like '%public%'
or vendor_name like '%city%'
or vendor_name like '%coop%'
--order by vendor_name

GO
