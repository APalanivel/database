SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_MPPContractBudgetData]
as
select distinct ed_contract_number
	,contract_start_date
	,contract_end_date
	,clearport_index
	,maxdate
	
from   contract c
join   budget_contract_budget bcb on bcb.contract_id = c.contract_id
join   budget_contract_budget_months m on m.budget_contract_budget_id = bcb.budget_contract_budget_id
join   budget_contract_budget_detail d on d.budget_contract_budget_month_id = m.budget_contract_budget_month_id
join   clearport_index ci on ci.clearport_index_id = d.market_id
 join (select distinct ci.clearport_index_id
		,max(index_detail_date) as maxdate
		from clearport_index_detail cid
		join clearport_index_months m on m.clearport_index_month_id = cid.clearport_index_month_id	
		join clearport_index ci on ci.clearport_index_id= m.clearport_index_id
		group by ci.clearport_index_id
		) x on x.clearport_index_id = ci.clearport_index_id






GO
