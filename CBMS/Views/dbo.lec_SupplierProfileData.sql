SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW
[dbo].[lec_SupplierProfileData]
AS
WITH supplier_cte
AS
(
SELECT Vendor_Id
 FROM   account a 
 JOIN   supplier_account_meter_map samm ON samm.account_id = a.account_id
 JOIN   contract c ON c.contract_id = samm.contract_id
 WHERE  c.contract_type_id = 153 AND c.contract_end_Date > getdate() -1 AND (samm.meter_disassociation_date IS NULL)
 )

SELECT
 Vendor_name [Supplier Name]
	,[Summit Approved] = CASE WHEN sd.is_summit_approved = 1 THEN 'Yes' WHEN sd.is_summit_approved = '0' THEN 'No' ELSE NULL END
	,[Minority Owned]= CASE WHEN sd.is_minority_owned= 1 THEN 'Yes' WHEN sd.is_minority_owned = '0' THEN 'No' ELSE NULL END
	,[CurrentContract] = CASE WHEN c.vendor_id IS NOT NULL THEN 'Yes' ELSE 'No' END
		
FROM vendor v
LEFT JOIN sr_supplier_profile sd ON v.vendor_id = sd.vendor_id
LEFT JOIN supplier_cte c ON c.vendor_id = v.vendor_id
WHERE v.vendor_type_id = 288
GROUP BY vendor_name, is_summit_approved,is_minority_owned,c.vendor_id



GO
