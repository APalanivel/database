SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW dbo.BUDGET_QUEUE_ACCOUNT_TYPE_VW
	AS
	select  budget_account.budget_id,
		budget_account.budget_account_id,
		account.account_id,
		case when vw.Tariff_Transport = 'Tariff' and account.service_level_type_id = 861
		then 'C&D'
		when vw.Tariff_Transport = 'Tariff' and account.service_level_type_id = 1024
		then 'C&D'
		when vw.Tariff_Transport = 'Transport' and account.service_level_type_id = 1024
		then 'D'
		else 'A&B'
		end budget_account_type,
		vw.Tariff_Transport
	
		 
	from 	budget_account
		join account on account.account_id = budget_account.account_id
		and budget_account.is_deleted = 0
		join BUDGET_TARIFF_TRANSPORT_VW vw on vw.account_id = account.account_id




GO
