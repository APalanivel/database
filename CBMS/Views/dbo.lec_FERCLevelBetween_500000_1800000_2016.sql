SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/******
NAME:
 dbo.lec_FERCLevelBetween_500000_1800000_2016

DESCRIPTION:	
	
 INPUT PARAMETERS:
 Name			DataType	Default		Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name			DataType  Default		Description
------------------------------------------------------------
  
  USAGE EXAMPLES:
------------------------------------------------------------
select top 10 * from dbo.lec_FERCLevelBetween_500000_1800000_2016 where client = 'Apple American Group'

AUTHOR INITIALS:
 Initials	Name  
------------------------------------------------------------
 BCH		Balaraju

 MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------
 BCH		2013-04-20    Replaced cost_usage_site table with cost_usage_site_dtl and SUPPLIER_ACCOUNT_METER_MAP with core.client_hier_account
					   Romoved site,address,state tables and used core.client_hier instead.						    
					   Replaced hard coded value 25 with value of MMBTU of UNIT OF GAS
					   Replaced contract type id is 153 with CONTRACT TYPE is SUPPLIER.
 AP		2013-04-23  Removed Riskmanagementdeals CTE and used base table directly
					   Added Contract_Id join in sub select for Division & Site values
 AP		2013-05-29  Removed subqueries and used left join for calculating multiple sites & divisions
******/
CREATE VIEW [dbo].[lec_FERCLevelBetween_500000_1800000_2016]
AS
WITH  CPS_Index_cte
        AS ( SELECT
                  c.contract_id
                 ,Built = CASE WHEN lps.CONTRACT_ID IS NULL THEN 'No'
                               ELSE 'Yes'
                          END
                 ,lps.LPS_TYPE_ID
             FROM
                  dbo.[Contract] c
                  LEFT JOIN dbo.LOAD_PROFILE_SPECIFICATION lps
                        ON lps.CONTRACT_ID = c.CONTRACT_ID
                  JOIN dbo.ENTITY ctype
                        ON ctype.ENTITY_ID = c.CONTRACT_TYPE_ID
             WHERE
                  ( c.CONTRACT_START_DATE < '2016-01-01'
                    AND c.CONTRACT_END_DATE > '2016-12-31'
                    OR c.CONTRACT_START_DATE BETWEEN '2016-01-01'
                                             AND     '2016-12-31'
                    OR c.CONTRACT_END_DATE BETWEEN '2016-01-01'
                                           AND     '2016-12-31' )
                  AND ctype.ENTITY_NAME = 'Supplier'
                  AND ( c.contract_pricing_summary LIKE '%index%'
                        OR c.contract_pricing_summary LIKE '%algon%'
                        OR c.contract_pricing_summary LIKE '%anr%'
                        OR c.contract_pricing_summary LIKE '%appalac%'
                        OR c.contract_pricing_summary LIKE '%cal%'
                        OR c.contract_pricing_summary LIKE '%cegt%'
                        OR c.contract_pricing_summary LIKE '%chicago%'
                        OR c.contract_pricing_summary LIKE '%cig%'
                        OR c.contract_pricing_summary LIKE '%cne%'
                        OR c.contract_pricing_summary LIKE '%dominion%'
                        OR c.contract_pricing_summary LIKE '%ferc%'
                        OR c.contract_pricing_summary LIKE '%fgt%'
                        OR c.contract_pricing_summary LIKE '%gas daily%'
                        OR c.contract_pricing_summary LIKE '%gdd%'
                        OR c.contract_pricing_summary LIKE '%hsc%'
                        OR c.contract_pricing_summary LIKE '%if%'
                        OR c.contract_pricing_summary LIKE '%mid americ%'
                        OR c.contract_pricing_summary LIKE '%midam%'
                        OR c.contract_pricing_summary LIKE '%ngi%'
                        OR c.contract_pricing_summary LIKE '%nwpl%'
                        OR c.contract_pricing_summary LIKE '%pepl%'
                        OR c.contract_pricing_summary LIKE '%pg&E%'
                        OR c.contract_pricing_summary LIKE '%pge%'
                        OR c.contract_pricing_summary LIKE '%questar%'
                        OR c.contract_pricing_summary LIKE '%rg&e%'
                        OR c.contract_pricing_summary LIKE '%san jua%'
                        OR c.contract_pricing_summary LIKE '%socal%'
                        OR c.contract_pricing_summary LIKE '%sonat%'
                        OR c.contract_pricing_summary LIKE '%southern%'
                        OR c.contract_pricing_summary LIKE '%southpoint%'
                        OR c.contract_pricing_summary LIKE '%sumas%'
                        OR c.contract_pricing_summary LIKE '%tco%'
                        OR c.contract_pricing_summary LIKE '%tgp%'
                        OR c.contract_pricing_summary LIKE '%tgt%'
                        OR c.contract_pricing_summary LIKE '%tn zn%'
                        OR c.contract_pricing_summary LIKE '%transco%'
                        OR c.contract_pricing_summary LIKE '%trunk%'
                        OR c.contract_pricing_summary LIKE '%vanguar%'
                        OR c.contract_pricing_summary LIKE '%ventura%'
                        OR c.contract_pricing_summary LIKE '%waha%'
                        OR c.contract_pricing_summary LIKE '%centerpoint%'
                        OR c.contract_pricing_summary LIKE '%cgpr%'
                        OR c.contract_pricing_summary LIKE '%cheyenne%'
                        OR c.contract_pricing_summary LIKE '%cig%'
                        OR c.contract_pricing_summary LIKE '%colorado%'
                        OR c.contract_pricing_summary LIKE '%columbia%'
                        OR c.contract_pricing_summary LIKE '%Consumers%'
                        OR c.contract_pricing_summary LIKE '%Dracut%'
                        OR c.contract_pricing_summary LIKE '%El Paso%'
                        OR c.contract_pricing_summary LIKE '%Florida%'
                        OR c.contract_pricing_summary LIKE '%Henry%'
                        OR c.contract_pricing_summary LIKE '%Houston%'
                        OR c.contract_pricing_summary LIKE '%Iroquois%'
                        OR c.contract_pricing_summary LIKE '%Katy%'
                        OR c.contract_pricing_summary LIKE '%Louisiana%'
                        OR c.contract_pricing_summary LIKE '%Mich%'
                        OR c.contract_pricing_summary LIKE '%Midcontinent%'
                        OR c.contract_pricing_summary LIKE '%Midwest%'
                        OR c.contract_pricing_summary LIKE '%Natural%'
                        OR c.contract_pricing_summary LIKE '%Niagara%'
                        OR c.contract_pricing_summary LIKE '%Northern%'
                        OR c.contract_pricing_summary LIKE '%Northwest%'
                        OR c.contract_pricing_summary LIKE '%Oneok%'
                        OR c.contract_pricing_summary LIKE '%Panhandle%'
                        OR c.contract_pricing_summary LIKE '%PG&E%'
                        OR c.contract_pricing_summary LIKE '%Rocky%'
                        OR c.contract_pricing_summary LIKE '%So Cal%'
                        OR c.contract_pricing_summary LIKE '%Southern%'
                        OR c.contract_pricing_summary LIKE '%TCPL%'
                        OR c.contract_pricing_summary LIKE '%Tenn%'
                        OR c.contract_pricing_summary LIKE '%Texas%'
                        OR c.contract_pricing_summary LIKE '%Transwestern%' )
             GROUP BY
                  c.contract_id
                 ,lps.CONTRACT_ID
                 ,lps.LPS_TYPE_ID),
      costusage_cte_client
        AS ( SELECT
                  ch.Client_ID
                 ,SUM(CAST(cus.Bucket_Value AS DECIMAL(32, 16)) * cuc.conversion_factor) AS ClientNGUsage
             FROM
                  dbo.Cost_Usage_Site_Dtl cus
                  JOIN Core.Client_Hier ch
                        ON cus.Client_Hier_Id = ch.Client_Hier_Id
                  JOIN dbo.Bucket_Master bm
                        ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                  JOIN dbo.Commodity com
                        ON bm.Commodity_Id = com.Commodity_Id
                  JOIN dbo.ENTITY mmbtu
                        ON mmbtu.ENTITY_NAME = 'mmbtu'
                           AND mmbtu.ENTITY_DESCRIPTION = 'Unit for Gas'
                  JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                        ON cuc.BASE_UNIT_ID = cus.UOM_Type_Id
                           AND cuc.CONVERTED_UNIT_ID = mmbtu.ENTITY_ID
             WHERE
                  com.Commodity_Name = 'Natural Gas'
                  AND bm.Bucket_Name = 'Total Usage'
                  AND cus.SERVICE_MONTH BETWEEN '2016-01-01'
                                        AND     '2016-12-01'
                  AND ch.Country_Name = 'USA'
             GROUP BY
                  ch.Client_ID),
      costusage_cte_division
        AS ( SELECT
                  ch.Sitegroup_Id AS Division_Id
                 ,SUM(CAST(cus.Bucket_Value AS DECIMAL(32, 16)) * cuc.conversion_factor) AS DivisionNGUsage
             FROM
                  dbo.Cost_Usage_Site_Dtl cus
                  JOIN Core.Client_Hier ch
                        ON cus.Client_Hier_Id = ch.Client_Hier_Id
                  JOIN dbo.Bucket_Master bm
                        ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                  JOIN dbo.Commodity com
                        ON bm.Commodity_Id = com.Commodity_Id
                  JOIN dbo.ENTITY mmbtu
                        ON mmbtu.ENTITY_NAME = 'mmbtu'
                           AND mmbtu.ENTITY_DESCRIPTION = 'Unit for Gas'
                  JOIN dbo.CONSUMPTION_UNIT_CONVERSION cuc
                        ON cuc.BASE_UNIT_ID = cus.UOM_Type_Id
                           AND cuc.CONVERTED_UNIT_ID = mmbtu.ENTITY_ID
             WHERE
                  com.Commodity_Name = 'Natural Gas'
                  AND bm.Bucket_Name = 'Total Usage'
                  AND cus.SERVICE_MONTH BETWEEN '2016-01-01'
                                        AND     '2016-12-01'
                  AND ch.Country_Name = 'USA'
             GROUP BY
                  ch.Sitegroup_Id)
      SELECT
            ch.CLIENT_NAME AS [Client]
           ,[Division] = CASE WHEN multi.multi_div > 1 THEN 'MultipleSeeCBMSForDivisions'
                              ELSE ch.Sitegroup_Name
                         END
           ,[SITE] = CASE WHEN multi.multi_site > 1 THEN 'MultipleSeeCBMSForSites'
                          ELSE ch.Site_name
                     END
           ,c.ED_CONTRACT_NUMBER AS [Contract Number]
           ,lps.MONTH_IDENTIFIER AS [Month]
           ,'Supplier' AS [ContractType]
           ,[ContractMeteredVolumes] = CASE WHEN c.IS_VOLUMES_NOT_REQUIRED != 1 THEN 'Required'
                                            ELSE 'NotRequired'
                                       END
           ,[Possible_PHYSICAL_RMDealTicket] = CASE WHEN rd.contract_id IS NOT NULL THEN 'Yes'
                                                    ELSE 'No'
                                               END
           ,bd.BASELOAD_VOLUME AS [ContractVolume]
           ,e.ENTITY_NAME AS [ContractVolumeUofM]
           ,e1.ENTITY_NAME AS [Frequency]
           ,[ConvertedContractVolume] = CASE WHEN e1.ENTITY_NAME IN ( '30-day Cycle', 'Month' ) THEN bd.BASELOAD_VOLUME * cuc.CONVERSION_FACTOR
                                             WHEN e1.ENTITY_NAME = 'Daily' THEN bd.BASELOAD_VOLUME * cuc.CONVERSION_FACTOR * lps.Days_In_Month_Num
                                             ELSE NULL
                                        END
           ,'MMBtu' AS [ConvertedContractVolumeUofM]
           ,'Monthly' AS [ConvertedFrequency]
           ,b.BASELOAD_NAME AS [Name]
           ,e2.ENTITY_NAME AS [LPSType]
           ,[PricingPoint] = CASE WHEN pin.PRICING_POINT IS NOT NULL THEN pin.PRICING_POINT
                                  WHEN pin.PRICING_POINT IS NULL
                                       AND cc.Built = 'yes' THEN 'Fixed'
                                  WHEN pin.PRICING_POINT IS NULL
                                       AND cc.Built = 'no' THEN 'NotBuilt;CPS Reflects Index Pricing'
                                  WHEN pin.PRICING_POINT IS NULL
                                       AND lps.contract_id IS NOT NULL
                                       AND cc.CONTRACT_ID IS NULL THEN 'Fixed'
                                  ELSE NULL
                             END
           ,c.Contract_Pricing_Summary
           ,pin.INDEX_DESCRIPTION AS [IndexDescription]
           ,e3.ENTITY_NAME AS [PriceIndex]
           ,e7.ENTITY_NAME AS [PriceIndexUofM]
           ,[CPS_Includes_Index_Pricing] = CASE WHEN cc.CONTRACT_ID IS NOT NULL THEN 'Yes'
                                                ELSE 'No'
                                           END
           ,[IndexPricingBuilt_OrIndexPricingInCPS] = CASE WHEN ( pin.PRICING_POINT IS NOT NULL
                                                                  AND pin.PRICING_POINT <> 'nymex' )
                                                                OR cc.CONTRACT_ID IS NOT NULL THEN 'Yes'
                                                           ELSE 'No'
                                                      END
           ,[NYMEXPricingInCPS] = CASE WHEN c.CONTRACT_PRICING_SUMMARY LIKE '%nymex%' THEN 'Yes'
                                       ELSE 'No'
                                  END
           ,cucl.ClientNGUsage
           ,[DivisionNGUsage] = CASE WHEN multi.multi_div > 1 THEN NULL
                                     WHEN cucd.DivisionNGUsage IS NULL THEN 0
                                     ELSE cucd.DivisionNGUsage
                                END
      FROM
            dbo.[Contract] c
            JOIN Core.Client_Hier_Account cha
                  ON cha.Supplier_Contract_ID = c.CONTRACT_ID
            JOIN Core.Client_Hier ch
                  ON ch.Client_Hier_Id = cha.Client_Hier_Id
            JOIN dbo.Commodity com
                  ON com.commodity_id = c.COMMODITY_TYPE_ID
            LEFT JOIN ( SELECT
                              lp.CONTRACT_ID
                             ,lp.LOAD_PROFILE_SPECIFICATION_ID
                             ,lp.LPS_UNIT_TYPE_ID
                             ,lp.LPS_FREQUENCY_TYPE_ID
                             ,lp.LPS_TYPE_ID
                             ,lp.PRICE_INDEX_ID
                             ,lp.MONTH_IDENTIFIER
                             ,lp.LPS_FIXED_PRICE
                             ,dd.Days_In_Month_Num
                             ,dd.First_Day_Of_Month_D
                        FROM
                              dbo.load_profile_specification lp
                              JOIN meta.Date_Dim dd
                                    ON dd.First_Day_Of_Month_D = lp.MONTH_IDENTIFIER
                        WHERE
                              lp.month_identifier BETWEEN '2016-01-01'
                                                  AND     '2016-12-01' ) lps
                  ON lps.CONTRACT_ID = c.CONTRACT_ID
            LEFT JOIN dbo.baseload_details bd
                  ON bd.load_profile_specification_id = lps.load_profile_specification_id
            LEFT JOIN dbo.baseload b
                  ON b.baseload_id = bd.baseload_id
            LEFT JOIN dbo.ENTITY mmbtu
                  ON mmbtu.ENTITY_NAME = 'mmbtu'
                     AND mmbtu.ENTITY_DESCRIPTION = 'Uniot for Gas'
            LEFT JOIN dbo.consumption_unit_conversion cuc
                  ON cuc.base_unit_id = lps.lps_unit_type_id
                     AND cuc.converted_unit_id = mmbtu.ENTITY_ID
            LEFT JOIN dbo.ENTITY con_type
                  ON con_type.ENTITY_ID = c.CONTRACT_TYPE_ID
            LEFT JOIN dbo.entity e1
                  ON e1.entity_id = lps.lps_frequency_type_id
            LEFT JOIN dbo.entity e
                  ON e.entity_id = lps.lps_unit_type_id
            LEFT JOIN dbo.entity e2
                  ON e2.entity_id = lps.lps_type_id
            LEFT JOIN dbo.price_index pin
                  ON pin.price_index_id = lps.price_index_id
            LEFT JOIN dbo.entity e3
                  ON e3.entity_id = pin.index_id
            LEFT JOIN dbo.entity e7
                  ON e7.entity_id = pin.volume_unit_id
            LEFT JOIN CPS_Index_cte cc
                  ON cc.CONTRACT_ID = c.CONTRACT_ID
                     AND ( cc.LPS_TYPE_ID = lps.LPS_TYPE_ID
                           OR cc.LPS_TYPE_ID IS NULL )
            LEFT JOIN costusage_cte_client cucl
                  ON cucl.Client_ID = ch.CLIENT_ID
            LEFT JOIN costusage_cte_division cucd
                  ON cucd.DIVISION_ID = ch.Sitegroup_Id
            LEFT JOIN dbo.RM_DEAL_TICKET rd
                  ON rd.CONTRACT_ID = c.CONTRACT_ID
            LEFT OUTER JOIN ( SELECT
                                    cha.Supplier_Contract_ID
                                   ,COUNT(DISTINCT ch.Client_Hier_Id) AS multi_site
                                   ,COUNT(DISTINCT ch.Sitegroup_Id) AS multi_div
                              FROM
                                    Core.Client_Hier_Account cha
                                    JOIN Core.Client_Hier ch
                                          ON cha.Client_Hier_Id = ch.Client_Hier_Id
                              WHERE
                                    cha.Account_Type = 'Supplier'
                              GROUP BY
                                    cha.Supplier_Contract_ID ) multi
                  ON multi.Supplier_Contract_Id = c.Contract_Id
      WHERE
            cha.Account_Type = 'Supplier'
            AND con_type.ENTITY_NAME = 'Supplier'
            AND ( c.CONTRACT_START_DATE < '2016-01-01'
                  AND c.CONTRACT_END_DATE > '2016-12-31'
                  OR c.CONTRACT_START_DATE BETWEEN '2016-01-01'
                                           AND     '2016-12-31'
                  OR c.CONTRACT_END_DATE BETWEEN '2016-01-01'
                                         AND     '2016-12-31' )
            AND ( cha.Supplier_Meter_Disassociation_Date IS NULL
                  OR cha.Supplier_Meter_Disassociation_Date > c.CONTRACT_START_DATE )
            AND com.Commodity_Name = 'Natural Gas'
            AND ( cucl.ClientNGUsage BETWEEN 500000 AND 1800000 )
            AND ch.Client_Not_Managed != 1
            AND ch.Country_Name = 'USA'
      GROUP BY
            ch.CLIENT_NAME
           ,c.ED_CONTRACT_NUMBER
           ,lps.MONTH_IDENTIFIER
           ,CASE WHEN c.IS_VOLUMES_NOT_REQUIRED != 1 THEN 'Required'
                 ELSE 'NotRequired'
            END
           ,CASE WHEN rd.contract_id IS NOT NULL THEN 'Yes'
                 ELSE 'No'
            END
           ,bd.BASELOAD_VOLUME
           ,e.ENTITY_NAME
           ,e1.ENTITY_NAME
           ,CASE WHEN e1.ENTITY_NAME IN ( '30-day Cycle', 'Month' ) THEN bd.BASELOAD_VOLUME * cuc.CONVERSION_FACTOR
                 WHEN e1.ENTITY_NAME = 'Daily' THEN bd.BASELOAD_VOLUME * cuc.CONVERSION_FACTOR * lps.Days_In_Month_Num
                 ELSE NULL
            END
           ,b.BASELOAD_NAME
           ,e2.ENTITY_NAME
           ,CASE WHEN pin.PRICING_POINT IS NOT NULL THEN pin.PRICING_POINT
                 WHEN pin.PRICING_POINT IS NULL
                      AND cc.Built = 'yes' THEN 'Fixed'
                 WHEN pin.PRICING_POINT IS NULL
                      AND cc.Built = 'no' THEN 'NotBuilt;CPS Reflects Index Pricing'
                 WHEN pin.PRICING_POINT IS NULL
                      AND lps.contract_id IS NOT NULL
                      AND cc.CONTRACT_ID IS NULL THEN 'Fixed'
                 ELSE NULL
            END
           ,c.Contract_Pricing_Summary
           ,pin.INDEX_DESCRIPTION
           ,e3.ENTITY_NAME
           ,e7.ENTITY_NAME
           ,CASE WHEN cc.CONTRACT_ID IS NOT NULL THEN 'Yes'
                 ELSE 'No'
            END
           ,CASE WHEN ( pin.PRICING_POINT IS NOT NULL
                        AND pin.PRICING_POINT <> 'nymex' )
                      OR cc.CONTRACT_ID IS NOT NULL THEN 'Yes'
                 ELSE 'No'
            END
           ,CASE WHEN c.CONTRACT_PRICING_SUMMARY LIKE '%nymex%' THEN 'Yes'
                 ELSE 'No'
            END
           ,cucl.ClientNGUsage
           ,CASE WHEN multi.multi_div > 1 THEN NULL
                 WHEN cucd.DivisionNGUsage IS NULL THEN 0
                 ELSE cucd.DivisionNGUsage
            END
           ,CASE WHEN multi.multi_div > 1 THEN 'MultipleSeeCBMSForDivisions'
                 ELSE ch.Sitegroup_Name
            END
           ,CASE WHEN multi.multi_site > 1 THEN 'MultipleSeeCBMSForSites'
                 ELSE ch.Site_name
            END

;




GO
