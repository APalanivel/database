SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_MultipleMetersPerSite]
as

select division_name
	,s.site_id
	,s.site_name
	,city
	,state_name
	,account_number
	,meter_number
	,entity_name
from
	client cl 
	join   division d with (nolock) on d.client_id = cl.client_id
	join   site s with (nolock)on s.division_id = d.division_id
	join   address addr with (nolock)on addr.address_parent_id = s.site_id
	join   state st with (nolock) on st.state_id = addr.state_id
	join   meter m with (nolock) on m.address_id = addr.address_id
	join   account acct with (nolock) on acct.account_id = m.account_id
	join   rate r with (nolock) on r.rate_id = m.rate_id
	join   entity e with (nolock) on e.entity_id = r.commodity_type_id
where entity_name like '%electric%'
and s.site_id in 

(
	select distinct site_id
		--,numberofmeters
	from 
	(
		select 
			s.site_id
			
			,count (meter_number)as numberofmeters
			
		from   client cl 
		join   division d with (nolock) on d.client_id = cl.client_id
		join   site s with (nolock) on s.division_id = d.division_id
		join   address addr with (nolock) on addr.address_parent_id = s.site_id
		join   state st with (nolock) on st.state_id = addr.state_id
		join   meter m with (nolock) on m.address_id = addr.address_id
		join   account acct with (nolock) on acct.account_id = m.account_id
		join   rate r with (nolock) on r.rate_id = m.rate_id
		join   entity e with (nolock) on e.entity_id = r.commodity_type_id
		where client_name like '%grah%'
		and    e.entity_name like '%electric power%'
		
		group by 
			s.site_id
		
	)x
	where numberofmeters >1	
)
	
union

select division_name
	,s.site_id
	,s.site_name
	,city
	,state_name
	,account_number
	,meter_number
	,entity_name
from
	client cl 
	join   division d with (nolock) on d.client_id = cl.client_id
	join   site s with (nolock) on s.division_id = d.division_id
	join   address addr with (nolock) on addr.address_parent_id = s.site_id
	join   state st with (nolock) on st.state_id = addr.state_id
	join   meter m with (nolock) on m.address_id = addr.address_id
	join   account acct with (nolock) on acct.account_id = m.account_id
	join   rate r with (nolock) on r.rate_id = m.rate_id
	join   entity e with (nolock) on e.entity_id = r.commodity_type_id
where entity_name like '%natural%'
and s.site_id in 

(
	select distinct site_id
		--,numberofmeters
	from 
	(
		select 
			s.site_id
			
			,count (meter_number)as numberofmeters
			
		from   client cl 
		join   division d with (nolock) on d.client_id = cl.client_id
		join   site s with (nolock) on s.division_id = d.division_id
		join   address addr with (nolock) on addr.address_parent_id = s.site_id
		join   state st with (nolock) on st.state_id = addr.state_id
		join   meter m with (nolock) on m.address_id = addr.address_id
		join   account acct with (nolock) on acct.account_id = m.account_id
		join   rate r with (nolock) on r.rate_id = m.rate_id
		join   entity e with (nolock)on e.entity_id = r.commodity_type_id
		where client_name like '%grah%'
		and    e.entity_name like '%natural%'
		
		group by 
			s.site_id
		
	)x
	where numberofmeters >1	
)
		
	
--order by site_name, entity_name, account_number,meter_number	
		
GO
