SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE   view
[dbo].[lec_RateDeptUtilityAnalystMap_0407_deregReg]
as
	

	select  distinct
		vendor_name
		,ui.first_name as AnalystFirstName
		,ui.last_name as AnalystLastName
		,state_name as State
		,region_name as Region
		,e.entity_name as ClientType
		--,'Client Count' = case when client_type_id = 141 then count (distinct cl.client_id) else '0' end
		,count (distinct cl.client_id) as NumberofClients
	from    vendor v 
	join    utility_Detail ud on ud.vendor_id = v.vendor_id
	join    utility_analyst_map uam on uam.utility_detail_id = ud.utility_detail_id
	join    vendor_state_map vsm on vsm.vendor_id = v.vendor_id
	join    state st on st.state_id = vsm.state_id
	join    region r on r.region_id = st.region_id
	join    user_info ui on ui.user_info_id = uam.regulated_analyst_id
	
	join    rate rt on rt.vendor_id = v.vendor_id
	join    meter m on m.rate_id = rt.rate_id
	join    address addr on addr.address_id = m.address_id
	join    site s on s.site_id = addr.address_parent_id
	join    division d on d.division_id = s.division_id
	join    client cl on cl.client_id = d.client_id
	join    entity e on e.entity_id = cl.client_type_id
where v.is_history <> '1'
and cl.not_managed <> '1'
group by 
		vendor_name
		 ,ui.first_name 
		,ui.last_name 
		
		,state_name 
		,region_name 
		,e.entity_name


GO
