SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_sitecontacts]
as


select distinct client_name
	,city
	,state_name	
	,max (username)  CEM
	--,s.site_id
	,s.not_managed
	,s.closed
	,lp_contact_first_name
	,lp_contact_last_name
	,lp_contact_email_address
	--,lp_contact_cc
	,s.contracting_entity
	,s.client_legal_structure	
	,s.tax_number  fein_number
	,s.duns_number
	
from client cl
join division d on cl.client_id = d.client_id
join site s on d.division_id = s.division_id
join address addr on s.site_id = addr.address_parent_id
join state st on addr.state_id = st.state_id
join client_cem_map ccm on ccm.client_id = cl.client_id
join user_info ui on ui.user_info_id = ccm.user_info_id
 
where--
 primary_address_id = addr.address_id
and (lp_contact_first_name is null
or lp_contact_last_name is null
or lp_contact_email_address is null
or s.contracting_entity is null
or s.tax_number is null
or s.duns_number is null)
and s.not_managed = '0'

group by 
client_name
	,city
	,state_name	
	,s.site_id
	,s.not_managed
	,s.closed
	,lp_contact_first_name
	,lp_contact_last_name
	,lp_contact_email_address
	,lp_contact_cc
	,s.contracting_entity
	,s.client_legal_structure	
	,s.tax_number 
	,s.duns_number
	



GO
