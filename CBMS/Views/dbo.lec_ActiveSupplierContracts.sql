SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE      view
[dbo].[lec_ActiveSupplierContracts]
as

--query built to accomodate request from jeff sullivan
-- to look for active supplier contracts and have the query run every monday 
select client_name [Client]
	,e1.entity_name [SiteType]
	,state_name [State]
	,ed_contract_number	 [Contract Number]
	,v.vendor_name [Supplier]
	,v1.vendor_name [Utility]
	,e.entity_name [Commodity]
	,acct1.account_number [SupplierAccount#]
	,cd.Code_Value [SupplierAccountRecalcType]
	,[SupplierAccountExpected] = case when acct1.not_Expected = 1 then 'No' else 'Yes' end
from   contract c
join   supplier_account_meter_map samm on samm.contract_id = c.contract_id
join   meter m on m.meter_id = samm.meter_id
join   account acct on acct.account_id = m.account_id

join   entity e on e.entity_id = c.commodity_type_id
join   vendor v1 on v1.vendor_id = acct.vendor_id
join   account acct1 on acct1.account_id = samm.account_id
join   vendor v on v.vendor_id = acct1.vendor_id
join   site s on s.site_id = acct.SITE_Id
join   ADDRESS addr on addr.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
join   state st on st.state_id = addr.state_id
join   ENTITY e1 on e1.ENTITY_ID = s.SITE_TYPE_ID
join   client cl on cl.client_id = s.client_id
join   Code cd on cd.Code_Id = acct1.Supplier_Account_Recalc_Type_Cd
where  c.contract_type_id = 153

and    getdate() between contract_start_date and contract_end_date
and st.country_id in (1,4)
and s.not_managed = '0'
group by 
client_name 
	,e1.entity_name 
	,state_name 
	,ed_contract_number	 
	,v.vendor_name 
	,v1.vendor_name 
	,e.entity_name 
	,acct1.account_number 
	,cd.Code_Value 
	,acct1.NOT_EXPECTED

--and ed_contract_number = '21523-0004'
















GO
