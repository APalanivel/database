SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE     view
[dbo].[lec_expected_received_percentage_FYR_SiteLevel_DV2]
as

	select 
	distinct 
	client_name  
	,site_id
	,site_name
	,convert(decimal (4,3), total) as Total
from
(
	select 'percentage' as Percentage
		,lec_exp.client_name as client_name
		,lec_exp.site_id
		,lec_exp.site_name
		,lec_rev.total/lec_exp.total as total
	from dbo.lec_expected_received_0907_FYRToDate_SiteLevel_DV2 lec_exp
	join dbo.lec_expected_received_0907_FYRToDate_SiteLevel_DV2 lec_rev with (nolock) on lec_exp.client_name = lec_rev.client_name
	and lec_exp.site_id = lec_rev.site_id
	where lec_exp.invoice_type = 'expected'
	and lec_rev.invoice_type = 'received'
	and lec_exp.total <> '0' 
	and lec_rev.total <> '0'
)
x














GO
