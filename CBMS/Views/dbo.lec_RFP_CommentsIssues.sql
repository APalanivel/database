SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_RFP_CommentsIssues]
as
select distinct term.sr_rfp_id
	,fixed_price_trigger_comments
	,vendor_name
		,username
		,from_month
		,to_month
		,account_number
		,client_name
		,site_name
		,e1.entity_name as bid_status
		,prod.product_name
from sr_Rfp_risk_management rm
join sr_rfp_supplier_service serv on serv.sr_rfp_risk_management_id = rm.sr_rfp_risk_management_id
join sr_rfp_bid bid on bid.sr_rfp_supplier_service_id = serv.sr_rfp_supplier_service_id
join sr_rfp_term_product_map map on map.sr_rfp_bid_id = bid.sr_rfp_bid_id
join sr_rfp_account_term aterm on aterm.sr_rfp_account_term_id = map.sr_rfp_account_term_id
join sr_rfp_term term on term.sr_rfp_term_id = aterm.sr_rfp_term_id
join sr_rfp_supplier_contact_vendor_map vmap with (nolock) on vmap.sr_rfp_supplier_contact_vendor_map_id = map.sr_rfp_supplier_contact_vendor_map_id 
	   join vendor v with (nolock) on vmap.vendor_id = v.vendor_id
	   join sr_supplier_contact_info sinfo with (nolock) on sinfo.sr_supplier_contact_info_id = vmap.sr_supplier_contact_info_id
	   join user_info ui with (nolock) on ui.user_info_id = sinfo.user_info_id
	   join sr_rfp_account sacct with (nolock) on sacct.sr_rfp_account_id = aterm.sr_account_group_id
	   join account acct with (nolock) on acct.account_id = sacct.account_id
	   join sr_rfp_selected_products prods with (nolock) on prods.sr_rfp_selected_products_id = map.sr_rfp_selected_products_id
	   join sr_rfp_pricing_product prod with (nolock) on prod.sr_rfp_pricing_product_id = prods.pricing_product_id
--only works for utility accounts that's why using it here
	   join site s with (nolock) on s.site_id = acct.site_id
	   join division d with (nolock) on d.division_id = s.division_id
	   join client cl with (nolock) on cl.client_id = d.client_id
	   join entity e1 with (nolock) on e1.entity_id = sacct.bid_status_type_id

where fixed_price_trigger_comments is not null
or fixed_price_trigger_comments <> ' '
--order by term.sr_rfp_id
GO
