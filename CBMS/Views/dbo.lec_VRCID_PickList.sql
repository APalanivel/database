SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_VRCID_PickList]
as
select distinct 
	v.vendor_name
	,e.entity_name
	,rate_name
	
	,v.vendor_id
	,r.rate_id
	,e.entity_id
	
from vendor v 
join rate r on r.vendor_id = v.vendor_id
join entity e on e.entity_id = r.commodity_type_id
GO
