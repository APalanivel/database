SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE VIEW dbo.SR_RFP_UTILITY_VW
	AS				

	select	distinct sr_rfp_account.sr_rfp_id, vendor.vendor_id 

	from 	sr_rfp_account(nolock),
		account(nolock),
		vendor(nolock)

	where 	account.account_id = sr_rfp_account.account_id
		and vendor.vendor_id  = account.vendor_id 
		and sr_rfp_account.is_deleted = 0










GO
