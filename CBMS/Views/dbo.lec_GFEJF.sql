SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE    view
[dbo].[lec_GFEJF]
as


select 		 distinct 
		client_name  'ClientName'
		,cl.client_id  'ClientID'
		,ClientNotManaged = case when cl.not_managed = '1' then 'Yes' else 'No' end
		,division_name 'DivisionName' 
		,d.division_id 'DivisionID'
		,DivisionNotManaged = case when d.not_managed = '1' then 'Yes' else 'No' end
		,site_name 'SiteName'
		,s.Site_id 'SiteID'
		,SiteCreated = eas.modified_date
		,site_reference_number 'SiteRef'
		,rg.group_name 'SiteRMGroup'
		,SiteNotManaged = case when s.not_managed = '1' then 'Yes' else 'No' end
		,SiteClosed = case when s.closed = '1' then 'Yes' else 'No' end
		,est.entity_name 'SiteType'
		,address_line1 'AddressLine1'
		,address_line2 'AddressLine2'
		,addr.city 'City'
		,state_name 'State_Province'
		,addr.zipcode 'ZipCode'
		,country_name 'Country'
		,region_name 'Region'
		,acct.account_number 'UtilityAccountNumber'
		,esvl.entity_name 'UtilityAccountServiceLevel'
		,eaacct.modified_date 'AccountCreated'
		,acct.not_expected 'UtilityAccountInvoiceNotExpected'
		,UtilityAccountNotManaged = case when acct.not_managed = '1' then 'Yes' else 'No' end
		,euacctso.entity_name 'UtilityAccountInvoiceSource'
		,v.vendor_name 'Utility'
		,ecomm.entity_name 'CommodityType'
		,r.rate_name 'Rate'
		,meter_number 'MeterNumber'
		--,m.meter_id
		, 'PurchaseMethod' = case when m.meter_id in (select samm.meter_id from supplier_account_meter_map samm 
			join contract c on c.contract_id = samm.contract_id
			where samm.meter_disassociation_date is null and c.contract_type_id = 153 and contract_end_date > getdate()) then 'OnFutureSupplyContract'else 'NotOnFutureSupplyContract' end
		,c.contract_id 'ContractID'
		,base_contract_id 'BaseContractID'
		,ed_contract_number 'ContractNumber'
		,ContractPricingStatus = case when lps.load_profile_specification_id is null then 'NotBuilt' else 'Built' end
		,ecty.entity_name 'ContractType'
		,v1.vendor_name 'ContractedVendor'
		,contract_start_date 'ContractStartDate'	
		,contract_end_date 'ContractEndDate'
		,NotificationDate = contract_end_date - (Notification_Days +15)
		,ContractTriggerRights = isnull (case is_contract_trigger_rights when '1' then 'Yes' else 'No' end, 'Null')
		,FullRequirements = isnull (case is_contract_full_requirement when '1' then 'Yes' else 'No' end, 'Null')
		,contract_pricing_summary 'Contract_Pricing_Summary'
		,contract_comments 'ContractComments'
		,curr.currency_unit_name 'Currency'
		,eren.entity_name 'RenewalType'
		,sacct.account_number 'SupplierAccountNumber'
		,esuasv.entity_name 'SupplierAccountServiceLevel'
		,esacctso.entity_name 'SupplierAccountInvoiceSource'
		,ContractCreatedBy = ui.first_name + ' ' +ui.last_name 
		,eac.modified_date 'ContractCreatedDate'
		,UtilityGroupBillStatus  = case when acct.account_group_id is not null then 'PartofGroupBill' else 'No' end
		,ContractRecalcType = ereca.entity_name 
		,ContractClassification = eclas.entity_name	
		
      	from   
--Contract Section
       supplier_account_meter_map samm 
       join    account sacct with (nolock)on sacct.account_id = samm.account_id 
       join    contract c with (nolock)on c.contract_id = samm.contract_id
       join    vendor v1 with (nolock)on v1.vendor_id = sacct.vendor_id
       join    entity eren with (nolock) on eren.entity_id = c.renewal_type_id
       join    entity esuasv with (nolock)on esuasv.entity_id = sacct.service_level_type_id
  left join    entity esacctso with (nolock)on esacctso.entity_id = sacct.invoice_source_type_id
       join    entity_audit eac with (nolock)on eac.entity_identifier = c.contract_id and eac.entity_id = 494 and eac.audit_type = '1'
 left  join    load_profile_specification lps with (nolock)on lps.contract_id = c.contract_id

       join    entity ecty with (nolock) on ecty.entity_id = c.contract_type_id
       join    currency_unit curr with (nolock) on curr.currency_unit_id = c.currency_unit_id
       join    user_info ui with (nolock)on ui.user_info_id = eac.user_info_id
       join    entity ereca with (nolock) on ereca.entity_id = c.contract_recalc_type_id
       join    entity eclas with (nolock) on eclas.entity_id = c.Contract_Classification_Cd
       join    meter m with (nolock)on m.meter_id = samm.meter_id and (samm.meter_disassociation_date is null
       or samm.METER_DISASSOCIATION_DATE >= c.CONTRACT_START_DATE)
       join    account acct with (nolock)on acct.account_id = m.account_id
       join    rate r with (nolock)on r.rate_id = m.rate_id
       join    vendor v with (nolock)on v.vendor_id = acct.vendor_id
       join    entity ecomm with (nolock)on ecomm.entity_id = r.commodity_type_id
  left join    entity esvl with (nolock)on esvl.entity_id = acct.service_level_type_id
       join    entity_audit eaacct with (nolock)on eaacct.entity_identifier = acct.account_id and eaacct.entity_id = 491 and eaacct.audit_type = '1'
left   join    entity euacctso with (nolock)on euacctso.entity_id = acct.invoice_source_type_id
       join    address addr with (nolock) on addr.address_id = m.address_id
       join    state st with (nolock) on st.state_id = addr.state_id
       join    country ctry with (nolock) on ctry.country_id = st.country_id
       join    region reg with (nolock) on reg.region_id = st.region_id
       join    site s with (nolock)on s.site_id = acct.site_id
       join    division d with (nolock) on d.division_id = s.division_id
       join    client cl with (nolock) on cl.client_id = d.client_id
 left  join    rm_group_site_map rgsm with (nolock) on rgsm.site_id = s.site_id
left   join    rm_group rg with (nolock) on rg.rm_group_id = rgsm.rm_group_id    
       join    entity_audit eas with (nolock) on eas.entity_identifier = s.site_id and eas.entity_id = 506 and eas.audit_type = '1'
       join    entity est with (nolock) on est.entity_id = s.site_type_id

where cl.client_type_id = '1455' or cl.client_name like '%cytec%'
union
--SitesWithAccountNoContracts

select 		 distinct 
		client_name  'ClientName'
		,cl.client_id  'ClientID'
		,ClientNotManaged = case when cl.not_managed = '1' then 'Yes' else 'No' end
		,division_name 'DivisionName' 
		,d.division_id 'DivisionID'
		,DivisionNotManaged = case when d.not_managed = '1' then 'Yes' else 'No' end
		,site_name 'SiteName'
		,s.Site_id 'SiteID'
		,SiteCreated = eas.modified_date
		,site_reference_number 'SiteRef'
		,rg.group_name 'SiteRMGroup'
		,SiteNotManaged = case when s.not_managed = '1' then 'Yes' else 'No' end
		,SiteClosed = case when s.closed = '1' then 'Yes' else 'No' end
		,est.entity_name 'SiteType'
		,address_line1 'AddressLine1'
		,address_line2 'AddressLine2'
		,city 'City'
		,state_name 'State_Province'
		,zipcode 'ZipCode'
		,country_name 'Country'
		,region_name 'Region'
		,acct.account_number 'UtilityAccountNumber'
		,esvl.entity_name 'UtilityAccountServiceLevel'
		,eaacct.modified_date 'AccountCreated'
		,acct.not_expected 'UtilityAccountInvoiceNotExpected'
		,UtilityAccountNotManaged = case when acct.not_managed = '1' then 'Yes' else 'No' end
		,euacctso.entity_name 'UtilityAccountInvoiceSource'
		,v.vendor_name 'Utility'
		,ecomm.entity_name 'CommodityType'
		,r.rate_name 'Rate'
		,meter_number 'MeterNumber'
		,null 'PurchaseMethod'
		,null 'ContractID'
		,null'BaseContractID'
		,null 'ContractNumber'
		,null 'ContractPricingStatus'
		,null 'ContractType'
		,null 'ContractedVendor'
		,null'ContractStartDate'	
		,null 'ContractEndDate'
		,null 'NotificationDate'
		,null 'ContractTriggerRights'
		,null 'FullRequirements'
		,null 'Contract_Pricing_Summary'
		,null 'ContractComments'
		,null 'Currency'
		,null 'RenewalType'
		,null 'SupplierAccountNumber'
		,null 'SupplierAccountServiceLevel'
		,null 'SupplierAccountInvoiceSource'
		,null 'ContractCreatedBy'
		,null 'ContractCreatedDate'
		,null 'UtilityGroupBillStatus'
		,null 'ContractRecalcType'
		,null 'ContractClassification'
		
      	from   

	
       Client cl 
       join    division d with (nolock)on d.client_id = cl.client_id
       join    site s with (nolock)on s.division_id = d.division_id
       join    address addr with (nolock) on addr.address_parent_id = s.site_id
       join    state st with (nolock) on st.state_id = addr.state_id
       join    country ctry with (nolock) on ctry.country_id = st.country_id
       join    region reg with (nolock) on reg.region_id = st.region_id
left   join    rm_group_site_map rgsm with (nolock) on rgsm.site_id = s.site_id
left   join    rm_group rg with (nolock) on rg.rm_group_id = rgsm.rm_group_id    
       join    entity_audit eas with (nolock) on eas.entity_identifier = s.site_id and eas.entity_id = 506 and eas.audit_type = '1'
       join    entity est with (nolock) on est.entity_id = s.site_type_id
       join    meter m with (nolock) on m.address_id = addr.address_id
left   join    supplier_account_meter_map samm with (nolock)on samm.meter_id = m.meter_id
       join    account acct with (nolock)on acct.account_id = m.account_id
       join    rate r with (nolock)on r.rate_id = m.rate_id
       join    vendor v with (nolock)on v.vendor_id = acct.vendor_id
       join    entity ecomm with (nolock)on ecomm.entity_id = r.commodity_type_id
left   join    entity esvl with (nolock)on esvl.entity_id = acct.service_level_type_id
       join    entity_audit eaacct with (nolock)on eaacct.entity_identifier = acct.account_id and eaacct.entity_id = 491 and eaacct.audit_type = '1'
left   join    entity euacctso with (nolock)on euacctso.entity_id = acct.invoice_source_type_id
       
       where   (       samm.meter_id is null or (samm.meter_disassociation_date is not null and m.meter_id not in 
		(select meter_id from supplier_account_meter_map where meter_disassociation_date is null)))
    

and  cl.client_type_id = '1455' or cl.client_name like '%cytec%'
union
--SitesWithNoAccounts

select 		 distinct 
		client_name  'ClientName'
		,cl.client_id  'ClientID'
		,ClientNotManaged = case when cl.not_managed = '1' then 'Yes' else 'No' end
		,division_name 'DivisionName' 
		,d.division_id 'DivisionID'
		,DivisionNotManaged = case when d.not_managed = '1' then 'Yes' else 'No' end
		,site_name 'SiteName'
		,s.Site_id 'SiteID'
		,SiteCreated = eas.modified_date
		,site_reference_number 'SiteRef'
		,rg.group_name 'SiteRMGroup'
		,SiteNotManaged = case when s.not_managed = '1' then 'Yes' else 'No' end
		,SiteClosed = case when s.closed = '1' then 'Yes' else 'No' end
		,est.entity_name 'SiteType'
		,address_line1 'AddressLine1'
		,address_line2 'AddressLine2'
		,city 'City'
		,state_name 'State_Province'
		,zipcode 'ZipCode'
		,country_name 'Country'
		,region_name 'Region'
		,null 'UtilityAccountNumber'
		,null 'UtilityAccountServiceLevel'
		,null 'AccountCreated'
		,null 'UtilityAccountInvoiceNotExpected'
		,null 'UtilityAccountNotManaged'
		,null 'UtilityAccountInvoiceSource'
		,null 'Utility'
		,null 'CommodityType'
		,null 'Rate'
		,null 'MeterNumber'
		--,m.meter_id
		,null 'PurchaseMethod'
		,null 'ContractID'
		,null'BaseContractID'
		,null 'ContractNumber'
		,null 'ContractPricingStatus'
		,null 'ContractType'
		,null 'ContractedVendor'
		,null'ContractStartDate'	
		,null 'ContractEndDate'
		,null 'NotificationDate'
		,null 'ContractTriggerRights'
		,null 'FullRequirements'
		,null 'Contract_Pricing_Summary'
		,null 'ContractComments'
		,null 'Currency'
		,null 'RenewalType'
		,null 'SupplierAccountNumber'
		,null 'SupplierAccountServiceLevel'
		,null 'SupplierAccountInvoiceSource'
		,null 'ContractCreatedBy'
		,null 'ContractCreatedDate'
		,null 'UtilityGroupBillStatus'
		,null 'ContractRecalcType'
		,null 'ContractClassification'
		
      	from   
--ClientsNoUtilityAccounts Section
	
       Client cl 
       join    division d with (nolock)on d.client_id = cl.client_id
       join    site s with (nolock)on s.division_id = d.division_id
       join    address addr with (nolock) on addr.address_parent_id = s.site_id
       join    state st with (nolock) on st.state_id = addr.state_id
       join    country ctry with (nolock) on ctry.country_id = st.country_id
       join    region reg with (nolock) on reg.region_id = st.region_id
      
 left  join    rm_group_site_map rgsm with (nolock) on rgsm.site_id = s.site_id
left   join    rm_group rg with (nolock) on rg.rm_group_id = rgsm.rm_group_id    
       join    entity_audit eas with (nolock) on eas.entity_identifier = s.site_id and eas.entity_id = 506 and eas.audit_type = '1'
       join    entity est with (nolock) on est.entity_id = s.site_type_id
left   join    meter m with (nolock) on m.address_id = addr.address_id
where          m.meter_id is null
and cl.client_type_id = '1455' or cl.client_name like '%cytec%'
      --order by client_name
















GO
