SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--accounts on rfp
create view
[dbo].[lec_RFPS_Client_Specific]
as
select client_name
	,site_name
	,city
	,state_name
	,account_number
	,sr_rfp_id
	,rfp_sent_date
from    sr_rfp_account rfpacct
join    account acct on acct.account_id = rfpacct.account_id
join    meter m on m.account_id = acct.account_id
join    address addr on addr.address_id = m.address_id
join    site s on s.site_id = addr.address_parent_id
join    division d on d.division_id = s.division_id
join    client cl on cl.client_id = d.client_id
join    state st on st.state_id = addr.state_id
join    sr_rfp_checklist chk on chk.sr_rfp_account_id = rfpacct.sr_rfp_account_id
where   client_name like '%corning%'
--order   by sr_rfp_id, rfp_sent_date

GO
