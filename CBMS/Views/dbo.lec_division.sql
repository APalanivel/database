SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view
[dbo].[lec_division] as

select distinct
client_legal_structure
from division
GO
