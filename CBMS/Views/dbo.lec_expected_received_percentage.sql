SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











CREATE        view
[dbo].[lec_expected_received_percentage]
as
select distinct
	client_name  [Client]
	,ubm_name [UBM]
	,convert(decimal (4,3), total) as Total
	,ENTITY_NAME [Client Type]
from
(
	select 'percentage' as Percentage
		,lec_exp.client_name as client_name
		,x.UBM_NAME
		,lec_rev.total/lec_exp.total as total
		,e.ENTITY_NAME 
	from dbo.lec_expected_received_021910 lec_exp
	join dbo.lec_expected_received_021910 lec_rev on lec_exp.client_name = lec_rev.client_name
	join CLIENT c on c.CLIENT_NAME = lec_exp.client_name
	join ENTITY e on e.ENTITY_ID = c.CLIENT_TYPE_ID
	left join (select UBM_NAME
					,c.CLIENT_ID
					from
					CLIENT c
					join dbo.UBM_SERVICE us on us.UBM_SERVICE_ID = c.UBM_SERVICE_ID
					join ubm u on u.UBM_ID = us.UBM_ID) x on x.CLIENT_ID = c.CLIENT_ID
	where lec_exp.invoice_type = 'expected'
	and lec_rev.invoice_type = 'received'
	and lec_exp.total <> 0
)
x















GO
