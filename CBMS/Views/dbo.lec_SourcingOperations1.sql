SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE
view
[dbo].[lec_SourcingOperations1]
as
select ClientName 
,City 
,State_Province 
,SiteName 
,SiteID 
,ClientNotManaged 
,SiteNotManaged 
,SiteClosed 
,SiteType 
,CommodityType 
,AddressLine1 
,AddressLine2 
,ZipCode 
,Country 
,Region 
,AccountNumber 
,UtilityAccountServiceLevel 
,AccountInvoiceNotExpected 
,AccountNotManaged 
,Utility 
,MeterNumber 
,ContractID 
,BaseContractID 
,ContractNumber 
,ContractType 
,ContractedVendor 
,ContractStartDate 
,ContractEndDate 
,Contract_Pricing_Summary 
,ContractComments 
,Currency 
,SupplierAccountNumber 
,SupplierAccountServiceLevel 
,RecalculationType 
,Rate 
,PurchaseMethod 
,FullRequirements 
,GroupBillStatus
 
from (
Select	ClientName
	,ClientID
	,ClientNotManaged = isnull (case x.clientnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,DivisionName
	,DivisionID
	,DivisionNotManaged = isnull (case x.divisionnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,SiteName
	,SiteID
	,SiteCreated
	,SiteRef#
	,SiteRMGroup
	,SiteNotManaged = isnull (case x.sitenotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,SiteClosed = isnull (case x.siteclosed when '1' then 'Yes' else 'No' end, 'Null')
	,SiteType
	,AddressLine1
	,AddressLine2
	,City
	,State_Province
	,ZipCode
	,Country
	,Region
	--,MovedToHistory = isnull (case x.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')
	,AccountNumber 
	,UtilityAccountServiceLevel
	,AccountCreated
	,AccountInvoiceNotExpected = isnull (case x.accountinvoicenotexpected when '1' then 'Yes' else 'No' end, 'Null')
	,AccountNotManaged = isnull (case x.accountnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,UtilityAccountInvoiceSource
	,Utility
	,CommodityType
	,Rate
	,MeterNumber
	,'NotManaged' as PurchaseMethod
	,ContractID
	,BaseContractID
	,ContractNumber
	,'NotBuilt' as ContractPricingStatus
	,ContractType
	,ContractedVendor
	,ContractStartDate
	,ContractEndDate
	,NotificationDays
	,'NotificationDate' = ContractEndDate - (NotificationDays +15)
	,'ContractTriggerRights' = isnull (case x.contracttriggerrights when '1' then 'Yes' else 'No' end, 'Null')
	,FullRequirements = isnull (case x.fullrequirements when '1' then 'Yes' else 'No' end, 'Null')
	,Contract_Pricing_Summary
	,ContractComments
	,Currency
	,RenewalType
	,SupplierAccountNumber
	,SupplierAccountServiceLevel
	,SupplierAccountInvoiceSource
	,ContractCreatedBy
	,ContractCreatedDate
	,RecalculationType
	,groupbillstatus
	
from	
(SELECT  Client_Name as 		ClientName
	,cl.Client_ID as 		ClientID
	,cl.not_managed	as		ClientNotManaged
	,d.division_name as		DivisionName
	,d.Division_ID	as		DivisionID
	,D.Not_Managed as 		DivisionNotManaged
	,Site_name as			SiteName
	,S.Site_id as 			SiteID
	,ea2.modified_date as 		SiteCreated
	,S.site_reference_number as 	SiteRef#
	,rg.group_name as		SiteRMGroup
	,s.not_managed as 		SiteNotManaged
	,s.closed as 			SiteClosed
	,e_7.entity_name as 		SiteType
	,Addr.Address_Line1 as 		AddressLine1
	,Addr.Address_Line2 as		AddressLine2
	,Addr.City as 			City
	,St.State_Name as		State_Province
	,Addr.ZipCode as 		ZipCode
	,Ctry.Country_Name as 		Country
	,Reg.Region_Name as 		Region
	--,S.Is_History as 		MovedToHistory	
	,Acct.Account_Number as 	AccountNumber
	,e_5.entity_name as 		UtilityAccountServiceLevel
	,ea1.modified_date as 		AccountCreated
	,acct.not_expected as 		AccountInvoiceNotExpected
	,acct.not_managed as 		AccountNotManaged
	,E_3.entity_name as		UtilityAccountInvoiceSource
	,V.Vendor_Name as 		Utility
	,E_2.Entity_name as 		CommodityType
	,R.Rate_Name as 		Rate
	,M.Meter_Number as 		MeterNumber
	,'NotManaged' as PurchaseMethod
	,C.Contract_id as 		ContractID
	,C.Base_Contract_id as 		BaseContractID
	,C.ED_Contract_Number as 	ContractNumber
	,null as 			contractpricingstatus
	,E_1.Entity_Name as 		ContractType
	,V_1.Vendor_Name as 		ContractedVendor
	,C.Contract_Start_Date as	ContractStartDate
	,C.contract_end_date as 	ContractEndDate
	,C.Notification_days as		NotificationDays
	,null as 			NotificationDate
	,C.Is_Contract_trigger_rights as ContractTriggerRights
	,C.Is_Contract_full_requirement as FullRequirements
	,C.Contract_Pricing_Summary as Contract_Pricing_Summary
	,C.Contract_Comments as 	ContractComments
	,Cu.Currency_Unit_Name as 	Currency
	,E.Entity_Name as 		RenewalType
	,Acct_1.Account_Number as 	SupplierAccountNumber
	,e_6.entity_name as 		SupplierAccountServiceLevel
	,E_4.entity_name as		SupplierAccountInvoiceSource
	,User_info.Username as 		ContractCreatedBy
	,ea.modified_date as		ContractCreatedDate
	,e_9.entity_name as 		RecalculationType
	,groupbillstatus = case when acct_1.account_group_id is not null then 'yes' else 'no' end





FROM        user_info
	    right outer join	entity_audit ea
	    right outer join    entity e
	    right outer join    contract c
	    right outer join 	currency_unit cu with (nolock) on cu.currency_unit_id = c.currency_unit_id
	         	join    supplier_account_meter_map samm with (nolock) on c.contract_id = samm.contract_id
				 on e.entity_id = c.renewal_type_id
	    right outer join    entity e_1 with (nolock) on c.contract_type_id = e_1.entity_id 
				 on ea.entity_identifier = c.contract_id
				 on user_info.user_info_id = ea.user_info_id
	    right outer join     vendor v_1 with (nolock)
	                join     account acct_1 with (nolock) on v_1.vendor_id = acct_1.vendor_id
				 on samm.account_id = acct_1.account_id
	    full outer join      client cl
		        join     division d
			join     site s on d.division_id = s.division_id 
				 on cl.client_id = d.client_id
			join     account acct with (nolock) on s.site_id = acct.site_id
			join     meter m with (nolock) on acct.account_id = m.account_id
	    full outer join      state st 
			join     address addr with (nolock) on st.state_id = addr.state_id
			join     region reg with (nolock) on st.region_id = reg.region_id
			join     country ctry with (nolock) on st.country_id = ctry.country_id
				 on m.address_id = addr.address_id
	   full outer join       entity e_2 
			join     rate r 
			join     vendor v with (nolock) on r.vendor_id = v.vendor_id 
				 on e_2.entity_id = r.commodity_type_id
				 on m.rate_id = r.rate_id 
				 on samm.meter_id = m.meter_id   
		left join     entity e_3 with (nolock) on acct.invoice_source_type_id = e_3.entity_id
	     left outer join     load_profile_specification lps on c.contract_id = lps.contract_id
	     left outer join     entity e_4 with (nolock) on acct_1.invoice_source_type_id = e_4.entity_id
	   left join account acct1 with (nolock) on acct.account_id = acct1.account_id
           left join entity_audit ea1 on acct1.account_id = ea1.entity_identifier
        	left join entity_audit ea2 with (nolock) on s.site_id = ea2.entity_identifier
	left join rm_group_site_map rmgsm with (nolock) on s.site_id = rmgsm.site_id
	left join rm_group rg on rmgsm.rm_group_id = rg.rm_group_id
			join     entity e_5 with (nolock) on acct.service_level_type_id = e_5.entity_id
			join     entity e_6 with (nolock) on acct_1.service_level_type_id = e_6.entity_id
		   left join     entity e_7 with (nolock) on s.site_type_id = e_7.entity_id
	            left join    entity e_8 on e_8.entity_id = m.purchase_method_type_id
	left join entity e_9 on e_9.entity_id = c.contract_recalc_type_id
WHERE     Client_name is not null
	 AND (acct.ACCOUNT_TYPE_ID = 38)
	 AND (Ea.AUDIT_TYPE = 1)
	 and (Ea.entity_id = 494)
	 and lps.load_profile_specification_id is null
	 and c.contract_id is not null
	 and samm.is_history <> '1' 
	 and ea1.entity_id = 491
	and ea1.audit_type = '1'  
	and ea2.audit_type = '1'
	and ea2.entity_id = 506
	and c.contract_end_date > getdate() - 780
	and acct.not_managed <> '1'
)x

--order by siteid

union

--****************************
	--Query to show JoinedFile where contracts are not null and also built out

Select	distinct ClientName
	,ClientID
	,ClientNotManaged = isnull (case x.clientnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,DivisionName
	,DivisionID
	,DivisionNotManaged = isnull (case x.divisionnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,SiteName
	,SiteID
	,SiteCreated
	,SiteRef#
	,SiteRMGroup
	,SiteNotManaged = isnull (case x.sitenotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,SiteClosed = isnull (case x.siteclosed when '1' then 'Yes' else 'No' end, 'Null')
	,SiteType
	,AddressLine1
	,AddressLine2
	,City
	,State_Province
	,ZipCode
	,Country
	,Region
	--,MovedToHistory = isnull (case x.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')
	,AccountNumber 
	,UtilityAccountServiceLevel
	,AccountCreated
	,AccountInvoiceNotExpected = isnull (case x.accountinvoicenotexpected when '1' then 'Yes' else 'No' end, 'Null')
	,AccountNotManaged = isnull (case x.accountnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
	,UtilityAccountInvoiceSource
	,Utility
	,CommodityType
	,Rate
	,MeterNumber
	,'NotManaged' as PurchaseMethod
	,ContractID
	,BaseContractID
	,ContractNumber
	,'Built' as ContractPricingStatus
	,ContractType
	,ContractedVendor
	,ContractStartDate
	,ContractEndDate
	,NotificationDays
	,'NotificationDate' = ContractEndDate - (NotificationDays +15)
	,'ContractTriggerRights' = isnull (case x.contracttriggerrights when '1' then 'Yes' else 'No' end, 'Null')
	,FullRequirements = isnull (case x.fullrequirements when '1' then 'Yes' else 'No' end, 'Null')
	,Contract_Pricing_Summary
	,ContractComments
	,Currency
	,RenewalType
	,SupplierAccountNumber
	,SupplierAccountServiceLevel
	,SupplierAccountInvoiceSource
	,ContractCreatedBy
	,ContractCreatedDate
	,RecalculationType	
	,groupbillstatus
from	
(SELECT  Client_Name as 		ClientName
	,cl.Client_ID as 		ClientID
	,cl.not_managed	as		ClientNotManaged
	,d.division_name as		DivisionName
	,d.Division_ID	as		DivisionID
	,D.Not_Managed as 		DivisionNotManaged
	,Site_name as			SiteName
	,S.Site_id as 			SiteID
	,ea2.modified_date as		SiteCreated
	,S.Site_reference_number as 	SiteRef#
	,rg.group_name as 		SiteRMGroup
	,s.not_managed as 		SiteNotManaged
	,s.closed as 			SiteClosed
	,e_7.entity_name as 		SiteType
	,Addr.Address_Line1 as 		AddressLine1
	,Addr.Address_Line2 as		AddressLine2
	,Addr.City as 			City
	,St.State_Name as		State_Province
	,Addr.ZipCode as 		ZipCode
	,Ctry.Country_Name as 		Country
	,Reg.Region_Name as 		Region
	--,S.Is_History as 		MovedToHistory	
	,Acct.Account_Number as 	AccountNumber
	,e_5.entity_name as 		UtilityAccountServiceLevel
	,ea1.modified_date as 		AccountCreated
	,acct.not_expected as 		AccountInvoiceNotExpected
	,acct.not_managed as 		AccountNotManaged
	,E_3.entity_name as		UtilityAccountInvoiceSource
	,V.Vendor_Name as 		Utility
	,E_2.Entity_name as 		CommodityType
	,R.Rate_Name as 		Rate
	,M.Meter_Number as 		MeterNumber
	,e_8.entity_name as 		PurchaseMethod
	,C.Contract_id as 		ContractID
	,C.Base_Contract_id as 		BaseContractID
	,C.ED_Contract_Number as 	ContractNumber
	,null as 			contractpricingstatus
	,E_1.Entity_Name as 		ContractType
	,V_1.Vendor_Name as 		ContractedVendor
	,C.Contract_Start_Date as	ContractStartDate
	,C.contract_end_date as 	ContractEndDate
	,C.Notification_days as		NotificationDays
	,null as 			NotificationDate
	,C.Is_Contract_trigger_rights as ContractTriggerRights
	,C.Is_Contract_full_requirement as FullRequirements
	,C.Contract_Pricing_Summary as Contract_Pricing_Summary
	,C.Contract_Comments as 	ContractComments
	,Cu.Currency_Unit_Name as 	Currency
	,E.Entity_Name as 		RenewalType
	,Acct_1.Account_Number as 	SupplierAccountNumber
	,e_6.entity_name as 		SupplierAccountServiceLevel
	,E_4.entity_name as		SupplierAccountInvoiceSource
	,User_info.Username as 		ContractCreatedBy
	,ea.modified_date as		ContractCreatedDate
	,e_9.entity_name as 		RecalculationType
	,groupbillstatus = case when acct_1.account_group_id is not null then 'yes' else 'no' end

FROM        user_info
	    right outer join	entity_audit ea
	    right outer join    entity e
	    right outer join    contract c
	    right outer join 	currency_unit cu with (nolock) on cu.currency_unit_id = c.currency_unit_id
	         	join    supplier_account_meter_map samm on c.contract_id = samm.contract_id
				 on e.entity_id = c.renewal_type_id
	    right outer join    entity e_1  on c.contract_type_id = e_1.entity_id 
				 on ea.entity_identifier = c.contract_id
				 on user_info.user_info_id = ea.user_info_id
	    right outer join     vendor v_1
	                join     account acct_1 on v_1.vendor_id = acct_1.vendor_id
				  on samm.account_id = acct_1.account_id
	    full outer join      client cl
		        join     division d
			join     site s on d.division_id = s.division_id 
				  on cl.client_id = d.client_id
			join     account acct  on s.site_id = acct.site_id
			join     meter m  on acct.account_id = m.account_id

	    full outer join      state st 
			join     address addr with (nolock) on st.state_id = addr.state_id
			join     region reg with (nolock) on st.region_id = reg.region_id
			join     country ctry with (nolock) on st.country_id = ctry.country_id
				  on m.address_id = addr.address_id
	   full outer join       entity e_2 
			join     rate r 
			join     vendor v  on r.vendor_id = v.vendor_id 
				  on e_2.entity_id = r.commodity_type_id
				  on m.rate_id = r.rate_id 
				  on samm.meter_id = m.meter_id   
		left join     entity e_3 on acct.invoice_source_type_id = e_3.entity_id
	     left outer join     load_profile_specification lps on c.contract_id = lps.contract_id
	     left outer join     entity e_4 with (nolock) on acct_1.invoice_source_type_id = e_4.entity_id
		left join account acct1 with (nolock) on acct.account_id = acct1.account_id
           left join entity_audit ea1 on acct1.account_id = ea1.entity_identifier
	left join entity_audit ea2 on s.site_id = ea2.entity_identifier
	left join rm_group_site_map rmgsm on s.site_id = rmgsm.site_id
	left join rm_group rg on rmgsm.rm_group_id = rg.rm_group_id
	left join     entity e_5 on acct.service_level_type_id = e_5.entity_id
		left	join     entity e_6 on acct_1.service_level_type_id = e_6.entity_id
	             left join   entity e_7 on s.site_type_id = e_7.entity_id	
		 left join       entity e_8 on e_8.entity_id = m.purchase_method_type_id
left join entity e_9 on e_9.entity_id = c.contract_recalc_type_id
WHERE     Client_name is not null
	 AND (acct.ACCOUNT_TYPE_ID = 38)
	 AND (Ea.AUDIT_TYPE = 1)
	 and (Ea.entity_id = 494)
	 and lps.load_profile_specification_id is not null
	 and c.contract_id is not null
	 and samm.is_history <> '1' 
	       and ea1.entity_id = 491
	and ea1.audit_type = '1' 
	and ea2.entity_id = 506
	and ea2.audit_type = '1'
and c.contract_end_date > getdate() - 780
	and acct.not_managed <> '1'
)x


--Request to remove where there are no records
-- union
-- --Query to show JoinedFile where contracts are null
-- 
-- Select	ClientName
-- 	,ClientID
-- 	,ClientNotManaged = isnull (case x.clientnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 	,DivisionName
-- 	,DivisionID
-- 	,DivisionNotManaged = isnull (case x.divisionnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 	,SiteName
-- 	,SiteID
-- 	,SiteCreated
-- 	,SiteRef#
-- 	,SiteRMGroup
-- 	,SiteNotManaged = isnull (case x.sitenotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 	,SiteClosed = isnull (case x.siteclosed when '1' then 'Yes' else 'No' end, 'Null')
-- 	,SiteType
-- 	,AddressLine1
-- 	,AddressLine2
-- 	,City
-- 	,State_Province
-- 	,ZipCode
-- 	,Country
-- 	,Region
-- 	--,MovedToHistory = isnull (case x.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')
-- 	,AccountNumber 
-- 	,UtilityAccountServiceLevel
-- 	,AccountCreated
-- 	,AccountInvoiceNotExpected = isnull (case x.accountinvoicenotexpected when '1' then 'Yes' else 'No' end, 'Null')
-- 	,AccountNotManaged = isnull (case x.accountnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 	,UtilityAccountInvoiceSource
-- 	,Utility
-- 	,CommodityType
-- 	,Rate
-- 	,MeterNumber
-- 	,'NotManaged' as PurchaseMethod
-- 	,null as 		ContractID
-- 	,null as 		BaseContractID
-- 	,null as 		ContractNumber
-- 	,null as 		ContractPricingStatus
-- 	,null as 		ContractType
-- 	,null as 		ContractedVendor
-- 	,null as		ContractStartDate
-- 	,null as 		ContractEndDate
-- 	,null as		Notification_Days
-- 	,null as 		NotificationDate
-- 	,null as		ContractTriggerRights
-- 	,null as 		FullRequirements
-- 	,null as		Contract_Pricing_Summary
-- 	,null as 		ContractComments
-- 	,null as 		Currency
-- 	,null as 		RenewalType
-- 	,null as 		SupplierAccountNumber
-- 	,null as		SupplierAccountServiceLevel
-- 	,null as		SupplierAccountInvoiceSource
-- 	,null as 		ContractCreatedBy
-- 	,null as		ContractCreatedDate
-- 	,null as		RecalculationType
-- 	,null as groupbillstatus
-- from
-- (SELECT  Client_Name as 		ClientName
-- 	,cl.Client_ID as 		ClientID
-- 	,cl.not_managed	as		ClientNotManaged
-- 	,d.division_name as		DivisionName
-- 	,d.Division_ID	as		DivisionID
-- 	,D.Not_Managed as 		DivisionNotManaged
-- 	,Site_name as			SiteName
-- 	,S.Site_id as 			SiteID
-- 	,ea1.modified_date as 		SiteCreated
-- 	,S.Site_reference_number as 	SiteRef#
-- 	,rg.group_name as 		SiteRMGroup
-- 	,s.not_managed as 		SiteNotManaged
-- 	,s.closed as 			SiteClosed
-- 	,e_7.entity_name as 		SiteType
-- 	,Addr.Address_Line1 as 		AddressLine1
-- 	,Addr.Address_Line2 as		AddressLine2
-- 	,Addr.City as 			City
-- 	,St.State_Name as		State_Province
-- 	,Addr.ZipCode as 		ZipCode
-- 	,Ctry.Country_Name as 		Country
-- 	,Reg.Region_Name as 		Region
-- 	--,S.Is_History as 		MovedToHistory	
-- 	,Acct.Account_Number as 	AccountNumber	
-- 	,e_5.entity_name as 		UtilityAccountServiceLevel
-- 	,ea.modified_date as 		AccountCreated
-- 	,acct.not_expected as 		AccountInvoiceNotExpected
-- 	,acct.not_managed as 		AccountNotManaged
-- 	,E1.entity_name as		UtilityAccountInvoiceSource
-- 	,V.Vendor_Name as 		Utility
-- 	,E.Entity_name as 		CommodityType
-- 	,R.Rate_Name as 		Rate
-- 	,M.Meter_Number as 		MeterNumber
-- 	,e_8.entity_name as 		PurchaseMethod
-- 	,null as groupbillstatus 
-- 	,null as Expr27
-- 		,null as Expr28
-- 		--,MovedToHistory = isnull (case z.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')	
-- 		, NULL AS Expr1
-- 		,null as expr29
-- 		,null as expr30
-- 		, NULL AS Expr2
-- 		, NULL AS Expr3
-- 		, NULL AS Expr4
-- 		, NULL AS Expr5
-- 		, NULL AS Expr6
-- 		, NULL AS Expr7
-- 		, NULL AS Expr8
-- 		, NULL AS Expr9
-- 		, NULL AS Expr10
-- 		, NULL AS Expr11
-- 		, NULL AS Expr12
-- 		, NULL AS Expr13
-- 		, NULL AS Expr14
-- 		, NULL AS Expr15
-- 		, NULL AS Expr16
-- 		, NULL AS Expr17
-- 		, NULL AS Expr18
-- 		,null as expr19
-- 		, null as expr20
-- 		, null as expr21
-- 		, null as expr22
-- 		, null as expr23
-- 		, null as expr24
-- 		,null as expr25
-- 		,null as expr26
-- 	
-- from 	client cl 
-- 	join division d with (nolock) on cl.client_id = d.client_id
-- 	join site s with (nolock) on d.division_id = s.division_id
-- 	join address addr with (nolock) on s.site_id = addr.address_parent_id
-- 	join meter m with (nolock) on addr.address_id = m.address_id
-- 	join account acct with (nolock) on m.account_id = acct.account_id
-- 	left join supplier_account_meter_map samm on m.meter_id = samm.meter_id
-- 	left join contract c with (nolock) on c.account_id = samm.account_id
-- 	join state st with (nolock) on addr.state_id = st.state_id
-- 	join country ctry with (nolock) on st.country_id = ctry.country_id
-- 	join region reg with (nolock) on st.region_id = reg.region_id
-- 	join rate r with (nolock) on m.rate_id  = r.rate_id
-- 	join vendor v with (nolock) on r.vendor_id = v.vendor_id
-- 	join entity e with (nolock) on r.commodity_type_id = e.entity_id
-- 	left join entity e1 with (nolock) on acct.invoice_source_type_id = e1.entity_id
-- 	join entity_audit ea with (nolock) on acct.account_id = ea.entity_identifier
-- 	join entity_audit ea1 with (nolock) on s.site_id = ea1.entity_identifier
-- left join rm_group_site_map rmgsm with (nolock) on s.site_id = rmgsm.site_id
-- 	left join rm_group rg on rmgsm.rm_group_id = rg.rm_group_id
-- left join     entity e_5 on acct.service_level_type_id = e_5.entity_id
-- 	 left join entity e_7 on s.site_type_id = e_7.entity_id
-- 		--left	join     entity e_6 on acct_1.service_level_type_id = e_6.entity_id
-- 	left join entity e_8 on e_8.entity_id = m.purchase_method_type_id
-- 	where  (samm.account_id is null
-- 		and ea.entity_id = 491
-- 		and ea.audit_type = '1'
-- 		and ea1.entity_id = 506
-- 		and ea1.audit_type = '1'
-- 	and acct.not_managed <> '1'
-- 		)
-- 		
-- 
-- 		or
-- 		 (samm.account_id is not null
-- 		and ea.entity_id = 491
-- 		and ea.audit_type = '1'
-- 		and ea1.entity_id = 506
-- 		and ea1.audit_type = '1'
-- 		and samm.is_history = '1'
-- 	and acct.not_managed <> '1'
-- 		)
-- )
-- x
-- 
-- 	    	     
-- union
-- --Query to show JoinedFile where meters are null
-- Select 		ClientName
-- 		,ClientID
-- 		,ClientNotManaged = isnull (case x.clientnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 		,DivisionName
-- 		,DivisionID
-- 		,DivisionNotManaged = isnull (case x.divisionnotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 		,SiteName
-- 		,SiteID
-- 		,SiteCreated
-- 		,SiteRef#
-- 		,SiteRMGroup
-- 		,SiteNotManaged = isnull (case x.sitenotmanaged when '1' then 'Yes' else 'No' end, 'Null')
-- 		,SiteClosed = isnull (case x.siteclosed when '1' then 'Yes' else 'No' end, 'Null')
-- 		,SiteType
-- 		,AddressLine1
-- 		,AddressLine2
-- 		,City
-- 		,State_Province
-- 		,ZipCode
-- 
-- 		,Country
-- 		,Region
-- 		,null as Expr31
-- 		,null as Expr27
-- 		,null as Expr28
-- 		--,MovedToHistory = isnull (case z.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')	
-- 		, NULL AS Expr1
-- 		,null as expr34
-- 		,null as expr29
-- 		,null as expr30
-- 		, NULL AS Expr2
-- 		, NULL AS Expr3
-- 		, NULL AS Expr4
-- 		, NULL AS Expr5
-- 		, NULL AS Expr6
-- 		, NULL AS Expr7
-- 		, NULL AS Expr8
-- 		, NULL AS Expr9
-- 		, NULL AS Expr10
-- 		, NULL AS Expr11
-- 		, NULL AS Expr12
-- 		, NULL AS Expr13
-- 		, NULL AS Expr14
-- 		, NULL AS Expr15
-- 		, NULL AS Expr16
-- 		, NULL AS Expr17
-- 		, NULL AS Expr18
-- 		,null as expr19
-- 		, null as expr20
-- 		, null as expr21
-- 		, null as expr22
-- 		, null as expr23
-- 		, null as expr24
-- 		,null as expr25
-- 		,null as expr26
-- 		,null as expr27
-- 		,null as expr37
-- from 
-- 
-- (SELECT  	Client_Name as 		ClientName
-- 		,cl.Client_ID as 		ClientID
-- 		,cl.not_managed	as		ClientNotManaged
-- 		,d.division_name as		DivisionName
-- 		,d.Division_ID	as		DivisionID
-- 		,D.Not_Managed as 		DivisionNotManaged
-- 		,Site_name as			SiteName
-- 		,S.Site_id as 			SiteID
-- 		,ea.modified_date as 		SiteCreated
-- 		,S.site_reference_number as 	SiteRef#
-- 		,rg.group_name	as 		SiteRMGroup
-- 		,s.not_managed as 		SiteNotManaged
-- 		,s.closed as 			SiteClosed
-- 		,e_7.entity_name as		SiteType
-- 		,Addr.Address_Line1 as 		AddressLine1
-- 		,Addr.Address_Line2 as		AddressLine2
-- 		,Addr.City as 			City
-- 		,St.State_Name as		State_Province
-- 		,Addr.ZipCode as 		ZipCode
-- 		,Ctry.Country_Name as 		Country
-- 		,Reg.Region_Name as 		Region	
-- 		--,S.Is_History as 		MovedToHistory	
-- 		--NULL AS AddressType
-- 		,null as Expr31
-- 		,null as Expr27
-- 		,null as Expr28
-- 		--,MovedToHistory = isnull (case z.movedtohistory when '1' then 'Yes' else 'No' end, 'Null')	
-- 		, NULL AS Expr1
-- 		,null as expr34
-- 		,null as expr29
-- 		,null as expr30
-- 		, NULL AS Expr2
-- 		, NULL AS Expr3
-- 		, NULL AS Expr4
-- 		, NULL AS Expr5
-- 		, NULL AS Expr6
-- 		, NULL AS Expr7
-- 		, NULL AS Expr8
-- 		, NULL AS Expr9
-- 		, NULL AS Expr10
-- 		, NULL AS Expr11
-- 		, NULL AS Expr12
-- 		, NULL AS Expr13
-- 		, NULL AS Expr14
-- 		, NULL AS Expr15
-- 		, NULL AS Expr16
-- 		, NULL AS Expr17
-- 		, NULL AS Expr18
-- 		,null as expr19
-- 		, null as expr20
-- 		, null as expr21
-- 		, null as expr22
-- 		, null as expr23
-- 		, null as expr24
-- 		,null as expr26
-- 		,null as expr37
-- 
-- 
-- FROM         	Client cl
-- 	              join division d
-- 		      join site s with (nolock) on d.division_id = s.division_id 
-- 			   on cl.client_id = d.client_id
-- 		      join address addr with (nolock) on s.site_id = addr.address_parent_id
-- 		      join state st with (nolock) on addr.state_id = st.state_id
-- 		      join region reg with (nolock) on st.region_id = reg.region_id
-- 		      join country ctry with (nolock) on st.country_id = ctry.country_id
-- 	   left outer join meter m with (nolock) on addr.address_id = m.address_id
-- 	    left join entity_audit ea with (nolock) on s.site_id = ea.entity_identifier
-- 	left join rm_group_site_map rmgsm with (nolock) on s.site_id = rmgsm.site_id
-- 	left join rm_group rg with (nolock) on rmgsm.rm_group_id = rg.rm_group_id
-- 		 left join entity e_7 with (nolock) on s.site_type_id = e_7.entity_id
-- Where 		      meter_id is null
-- 			and ea.entity_id = 506
-- 			and ea.audit_type = '1'
--  
--   )x 



)z

















GO
