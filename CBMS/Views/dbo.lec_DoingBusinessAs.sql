SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  view
[dbo].[lec_DoingBusinessAs]
as
--11/13/06
--LCox
--Requested by roll out team to put on F drive, same place as
--JF, update nightly - DTS package
--Shows clients, sites and site dba names

select client_name as 'Client Name'
	,'Client Not Managed' = case when clientnotmanaged = '1' then 'Yes' else 'No' end
	,doing_business_as as 'DBA Name'
	,site_id as 'Site ID'
	,site_name as 'Site Name'
	,'Site Not Managed' = case when sitenotmanaged = '1' then 'Yes' else 'No' end
	,address as 'Address'
	,'CEM' = max (cem)
from (
	select    Client_name 
		  ,cl.not_managed as clientnotmanaged
		  ,doing_business_as 
		  ,s.Site_id 
		  ,s.not_managed as sitenotmanaged
		  ,site_name 
		  ,'Address' = address_line1 + ' / ' + addr.city + ' / ' + state_name
		  ,'CEM' = ui.first_name + ' ' + ui.last_name
	from      client cl 
	join      division d on d.client_id = cl.client_id
	join      site s on s.division_id = d.division_id
	join      address addr on addr.address_parent_id = s.site_id
	join      client_cem_map ccm on ccm.client_id = cl.client_id
	join      user_info ui on ui.user_info_id = ccm.user_info_id
	join      state st on st.state_id = addr.state_id
	
	where     s.primary_address_id = addr.address_id
	group by  client_name
		  ,doing_business_as
		  ,s.site_id
		  ,site_name
		  ,addr.address_line1
		  ,addr.city
		  ,ui.first_name
		  ,ui.last_name
		  ,cl.not_managed
		  ,s.not_managed
		  ,st.state_name
)x
group by 
		client_name
		,doing_business_as
		,site_id
		,site_name
		,address
		,clientnotmanaged
		,sitenotmanaged

GO
