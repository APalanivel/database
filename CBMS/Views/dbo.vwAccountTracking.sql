SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
 NAME:  
	dbo.vwAccountTracking  
 
 DESCRIPTION:   
 INPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 OUTPUT PARAMETERS:  
 Name   DataType  Default Description  
------------------------------------------------------------  
 
 USAGE EXAMPLES:  
------------------------------------------------------------  
select top 5 * from dbo.vwAccountTracking where begin_date is not null      
select top 5 * from dbo.vwAccountTracking where begin_date is not null       

select count(1) from dbo.vwAccountTracking  
go                  
select count(1) from dbo.vwAccountTracking_O
 
 AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 MODIFICATIONS   
 Initials Date  Modification  
------------------------------------------------------------  
 09/22/2009     For adding Supplier Account start and end dates
 10/16/2009	Use of Contract table not required as SAMM has account_id for reference
  DMR		  09/10/2010 Modified for Quoted_Identifier
******/         
  
CREATE VIEW [dbo].[vwAccountTracking]
AS 
SELECT      'account'                AS source_table
          , 'Utility'                AS account_type
          , a.account_id
          , a.account_group_id
          , a.account_number
          , a.ubm_id
          , a.ubm_account_code
          , m.meter_id
          , m.meter_number
          , c.client_id
          , c.client_name
          , ad.city
          , st.state_name
          , v.vendor_id
          , v.vendor_name
          , CAST( NULL AS DATETIME ) AS begin_date
          , CAST( NULL AS DATETIME ) AS end_date
FROM        client AS c WITH(  NOLOCK )
JOIN        division AS d WITH(  NOLOCK )
ON          d.client_id = c.client_id
JOIN        site AS s WITH(  NOLOCK )
ON          s.division_id = d.division_id
JOIN        address AS ad WITH(  NOLOCK )
ON          ad.address_id = s.primary_address_id
JOIN        state AS st WITH(  NOLOCK )
ON          st.state_id = ad.state_id
JOIN        account AS a WITH(  NOLOCK )
ON          a.site_id = s.site_id
JOIN        meter AS m WITH(  NOLOCK )
ON          m.account_id = a.account_id
JOIN        vendor AS v WITH(  NOLOCK )
ON          v.vendor_id = a.vendor_id
UNION ALL
SELECT      'account'                AS source_table
          , 'Supplier'               AS account_type
          , a.account_id
          , a.account_group_id
          , a.account_number
          , a.ubm_id
          , a.ubm_account_code
          , m.meter_id
          , m.meter_number
          , c.client_id
          , c.client_name
          , ad.city
          , st.state_name
          , v.vendor_id
          , v.vendor_name
          , a.Supplier_Account_Begin_Dt AS begin_date
          , a.Supplier_Account_End_Dt AS end_date
FROM        dbo.client AS c WITH(  NOLOCK )
JOIN        dbo.division AS d WITH(  NOLOCK )
ON          d.client_id = c.client_id
JOIN        dbo.site AS s WITH(  NOLOCK )
ON          s.division_id = d.division_id
JOIN        dbo.address AS ad WITH(  NOLOCK )
ON          ad.address_parent_id = s.site_id
JOIN        dbo.state AS st WITH(  NOLOCK )
ON          st.state_id = ad.state_id
JOIN        dbo.meter AS m WITH(  NOLOCK )
ON          m.address_id = ad.address_id
JOIN        dbo.supplier_account_meter_map AS map WITH(  NOLOCK )
ON          map.meter_id = m.meter_id
JOIN        dbo.account AS a WITH(  NOLOCK )
ON          a.account_id = map.account_id
JOIN        dbo.vendor AS v WITH(  NOLOCK )
ON          v.vendor_id = a.vendor_id
GROUP BY    a.account_id
          , a.account_group_id
          , a.account_number
          , a.ubm_id
          , a.ubm_account_code
          , m.meter_id
          , m.meter_number
          , c.client_id
          , c.client_name
          , ad.city
          , st.state_name
          , v.vendor_id
          , v.vendor_name
          , a.Supplier_Account_Begin_Dt
          , a.Supplier_Account_End_Dt
UNION ALL
SELECT      'do_not_track'           AS source_table
          , 'Unknown DNT'            AS account_type
          , dnt.do_not_track_id      AS account_id
          , NULL                     AS account_group_id
          , dnt.account_number
          , dnt.ubm_id
          , dnt.ubm_account_code
          , NULL                     AS meter_id
          , NULL                     AS meter_number
          , NULL                     AS client_id
          , dnt.client_name
          , dnt.client_city          AS city
          , st.state_name
          , NULL                     AS vendor_id
          , dnt.vendor_name
          , CAST( NULL AS DATETIME ) AS begin_date
          , CAST( NULL AS DATETIME ) AS end_date
FROM        dbo.do_not_track AS dnt WITH(  NOLOCK )
JOIN        dbo.state AS st WITH(  NOLOCK )
ON          st.state_id = dnt.state_id
WHERE       dnt.account_id IS NULL
GO
