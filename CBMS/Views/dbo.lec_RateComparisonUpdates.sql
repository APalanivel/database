SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view
[dbo].[lec_RateComparisonUpdates]
as

--lec 12/22/06 - view created to show rate comparisons where
-- marked as not reviewed
--rate comparison has been performed
--created less than 9/1/06
--with appropriate rate applied
--NEED TO UPDATE TO REVIEWED


select client_name
	,division_name
	,site_name
	,account_number
	,meter_number
	,m.meter_id
	,rate_comparison_name
	,creation_date
	,e.entity_name as 'Result'
	,reviewed_by
	,e1.entity_name as 'Status'
from    rc_rate_comparison rc
join    meter m on m.meter_id = rc.meter_id
join    account acct on acct.account_id = m.account_id
join    address addr on addr.address_id = m.address_id
join    site s on s.site_id = addr.address_parent_id
join    division d on d.division_id = s.division_id
join    client cl on cl.client_id = d.client_id
join    entity e on e.entity_id = rc.result_type_id
join    entity e1 on e1.entity_id = rc.status_type_id
where   e1.entity_name = 'not reviewed'
and     e.entity_name like '%appropriate%'
and     creation_date < '2006-09-01'

/*
update rc_rate_comparison
set    reviewed_by = '16'
	,status_type_id = '809'
where  rc_rate_comparison_id in 
(
	select distinct rc_rate_comparison_id
	from    rc_rate_comparison rc
	join    meter m on m.meter_id = rc.meter_id
	join    account acct on acct.account_id = m.account_id
	join    address addr on addr.address_id = m.address_id
	join    site s on s.site_id = addr.address_parent_id
	join    division d on d.division_id = s.division_id
	join    client cl on cl.client_id = d.client_id
	join    entity e on e.entity_id = rc.result_type_id
	join    entity e1 on e1.entity_id = rc.status_type_id
	where   e1.entity_name = 'not reviewed'
	and     e.entity_name like '%appropriate%'
	and     creation_date < '2006-09-01'
)
*/
GO
