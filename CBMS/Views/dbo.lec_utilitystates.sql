SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_utilitystates]
as
select distinct state_name
		,vendor_name
		
from vendor v 
join rate r on r.vendor_id = v.vendor_id
join entity e on e.entity_id = r.commodity_type_id
join vendor_state_map vsm on vsm.vendor_Id = v.vendor_id
join state st on st.state_id = vsm.state_id
where v.is_history <> '1'

GO
