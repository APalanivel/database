SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE VIEW dbo.Division AS
SELECT 
	Si.SiteGroup_Name Division_Name
	,Di.SiteGroup_Id Division_Id
	,Si.Client_Id
	,Di.SBA_TYPE_ID
	,Di.TERM_PREFERRED_TYPE_ID
	,Di.CONTRACT_REVIEWER_TYPE_ID
	,Di.DECISION_MAKER_TYPE_ID
	,Di.PRICE_INDEX_ID
	,Di.SIGNATORY_TYPE_ID
	,Di.IS_INTEREST_MINORITY_SUPPLIERS
	,Di.IS_CORPORATE_HEDGE
	,Di.NAICS_CODE
	,Di.TAX_NUMBER
	,Di.DUNS_NUMBER
	,Di.IS_CORPORATE_DIVISION
	,Di.TRIGGER_RIGHTS
	,Di.MISC_COMMENTS
	,Di.IS_HISTORY
	,Di.NOT_MANAGED
	,Di.NOT_MANAGED_BY_ID
	,Di.NOT_MANAGED_DATE
	,Di.CONTRACTING_ENTITY
	,Di.CLIENT_LEGAL_STRUCTURE
	,Di.CBMS_IMAGE_ID
	,Di.ROW_VERSION
FROM dbo.SiteGroup Si
	INNER JOIN dbo.Division_Dtl Di ON Di.SiteGroup_Id = Si.SiteGroup_Id
	INNER JOIN dbo.Code cd ON cd.Code_Id = Si.SiteGroup_Type_Cd
WHERE cd.Code_Value = 'Division'
GO
