SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW DBAM.REPL_Identity_Range
AS
SELECT
      Table_Name
     ,ident_current(Table_Name) Current_Identity_Value
     ,start_range
     ,END_range
     ,case WHEN ident_current(Table_Name) BETWEEN start_range
                                          AND     END_range THEN 'True'
           ELSE 'False'
      END AS Is_In_Range
     
FROM
      dbam.REPL_IDENT_RANGE_MGMT
WHERE
      Database_name = db_name()
      AND start_range > 0
      AND object_id(Table_name) IS NOT NULL
GO
