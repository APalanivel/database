SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_RFP_TypeIssues]
as




select sa.sr_rfp_id
	,ci.*
	,ui.first_name + ' ' + ui.last_name as UploadedBy
from sr_rfp_lp_client_approval app
join sr_rfp_account sa on app.sr_account_group_id = sa.sr_rfp_account_id

join cbms_image ci on ci.cbms_image_id = app.cbms_image_id
join user_info ui on app.uploaded_by = ui.user_info_id
where content_type like '%plain%'
and cbms_image_type_id = 1205
and app.is_bid_group = '0' or is_bid_group is null




GO
