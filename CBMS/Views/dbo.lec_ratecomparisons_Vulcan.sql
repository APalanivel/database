SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--Lynn 12/13/06
--New site Rollout Query - Built to meet request from Rates Managers
-- to track new site rollouts and when rollout rate comparisons are performed
--by analysts.

--*/
--meters with rate comparisons 
--with sso docs
--with entity audit
CREATE  view 
[dbo].[lec_ratecomparisons_Vulcan]
as
select distinct cast((convert (varchar(10), ea2.modified_date, 101))as datetime) as 'Account Creation Date'
	,cast ((convert(char(10), rollout_email_date,101))as datetime) as 'Rollout Date'
	,client_name as 'Client'
	,addr.city as 'City'
	,state_name as 'ST'
	,region_name as 'Region'
	,e.entity_name as 'Commodity'
	,account_number as 'Account#'
	,acct.account_id
	,meter_number as 'Meter #'
	,v.vendor_name as 'Utility'
	,rate_name as'Utility Rate'
	,e1.entity_name as 'Service Level'
	,ui2.first_name + ' ' + ui2.last_name as 'Responsible Analyst'
	,cast((convert (char(10),(rollout_email_date + 42),101))as datetime) as 'RC Review Deadline'
	--,e3.entity_name as 'Status'
	--,e4.entity_name as 'RC Outcome'
	--,cast((convert (char(10),ea1.modified_date,101))as datetime) as 'Date RC Posted to DV'
	,m.comments as 'Comments'
	,'yes' as RCDone
	--,ui.first_name + ' ' + ui.last_name as 'RCCreatedBy'
	,cast ((convert (char (10) ,rc.creation_date ,101))as datetime) as 'RCCreationDate'
	--,ui1.first_name + ' ' + ui1.last_name  as 'RCReviewedBy'
	

from    rc_rate_comparison rc
	join meter m with (nolock) on rc.meter_id = m.meter_id
	join account acct with (nolock)on m.account_id = acct.account_id
	join address addr with (nolock)on m.address_id = addr.address_id
	join site s with (nolock)on addr.address_parent_id = s.site_id
	join division d with (nolock)on s.division_id = d.division_id
	join client cl with (nolock)on d.client_id = cl.client_id
	--join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st with (nolock)on addr.state_id = st.state_id
	join region reg with (nolock)on st.region_id = reg.region_id
	join country ctry with (nolock)on st.country_id = ctry.country_id
	join rate r with (nolock)on m.rate_id = r.rate_id
	join entity e with (nolock)on r.commodity_type_id = e.entity_id
	left join entity e1 with (nolock)on acct.service_level_type_id = e1.entity_id
	left join entity e2 with (nolock)on acct.invoice_source_type_id = e2.entity_id
	join vendor v with (nolock)on acct.vendor_id = v.vendor_id
	left join user_info ui with (nolock)on rc.created_by = ui.user_info_id
	left join user_info ui1 with (nolock)on rc.reviewed_by = ui1.user_info_id
	left join utility_detail ud with (nolock)on v.vendor_id = ud.vendor_id
	left join utility_analyst_map uam with (nolock)on ud.utility_detail_id = uam.utility_detail_id
	left join user_info ui2 with (nolock)on uam.regulated_analyst_id = ui2.user_info_id
	left join entity e3 with (nolock)on e3.entity_id = rc.status_type_id
	left join entity e4 with (nolock)on e4.entity_id = rc.result_type_id
	left join sso_document ssodoc with (nolock)on ssodoc.cbms_image_id = rc.cbms_image_id
	left join entity_audit ea1 with (nolock)on ea1.entity_identifier = ssodoc.sso_document_id
	left join user_info ui4 with (nolock)on ui4.user_info_id = ea1.user_info_id
	left join entity_audit ea2 on ea2.entity_identifier = acct.account_id
where 
( (e1.entity_name = 'a' or e1.entity_name = 'b' or e1.entity_name = 'c'or e1.entity_name = 'd')
and ssodoc.category_type_id = 784
and (ea1.audit_type = '1' and ea1.entity_id = 1040)) 
and (ea2.audit_type = '1' and ea2.entity_id = 491)
and acct.not_managed = '0'
and cl.client_name like '%vulcan%'
union

--meters with rate comparisons 
--with sso docs
--with either no entity audit or a different entity_id

select distinct  cast((convert (varchar(10), ea2.modified_date, 101))as datetime) as 'Account Creation Date'
	,cast ((convert(char(10), rollout_email_date,101))as datetime) as 'Rollout Date'
	,client_name as 'Client'
	,addr.city as 'City'
	,state_name as 'ST'
	,region_name as 'Region'
	,e.entity_name as 'Commodity'

	,account_number as 'Account#'
	,acct.account_id
	,meter_number as 'Meter #'
	,v.vendor_name as 'Utility'
	,rate_name as'Utility Rate'
	,e1.entity_name as 'Service Level'
	,ui2.first_name + ' ' + ui2.last_name as 'Responsible Analyst'
	,cast ((convert (char(10),(rollout_email_date + 42),101))as datetime) as 'RC Review Deadline'
	--,e3.entity_name as 'Status'
	--,e4.entity_name as 'RC Outcome'
	--, null as 'Date RC Posted to DV'
	,m.comments as 'Comments'
	,'yes' as RCDone
	--,ui.first_name + ' ' + ui.last_name as 'RCCreatedBy'
	,convert (char (10) ,rc.creation_date ,101) as 'RCCreationDate'
	--,ui1.first_name + ' ' + ui1.last_name  as 'RCReviewedBy'
	

from    rc_rate_comparison rc
	join meter m with (nolock) on rc.meter_id = m.meter_id
	join account acct with (nolock)on m.account_id = acct.account_id
	join address addr with (nolock)on m.address_id = addr.address_id
	join site s with (nolock)on addr.address_parent_id = s.site_id
	join division d with (nolock)on s.division_id = d.division_id
	join client cl with (nolock)on d.client_id = cl.client_id
	--join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st with (nolock)on addr.state_id = st.state_id
	join region reg with (nolock)on st.region_id = reg.region_id
	join country ctry with (nolock)on st.country_id = ctry.country_id
	join rate r with (nolock)on m.rate_id = r.rate_id
	join entity e with (nolock)on r.commodity_type_id = e.entity_id
	left join entity e1 with (nolock)on acct.service_level_type_id = e1.entity_id
	left join entity e2 with (nolock)on acct.invoice_source_type_id = e2.entity_id
	join vendor v with (nolock)on acct.vendor_id = v.vendor_id
	left join user_info ui with (nolock)on rc.created_by = ui.user_info_id
	left join user_info ui1 with (nolock)on rc.reviewed_by = ui1.user_info_id
	left join utility_detail ud with (nolock)on v.vendor_id = ud.vendor_id
	left join utility_analyst_map uam with (nolock)on ud.utility_detail_id = uam.utility_detail_id
	left join user_info ui2 with (nolock)on uam.regulated_analyst_id = ui2.user_info_id
	left join entity e3 with (nolock)on e3.entity_id = rc.status_type_id
	left join entity e4 with (nolock)on e4.entity_id = rc.result_type_id
	left join sso_document ssodoc with (nolock)on ssodoc.cbms_image_id = rc.cbms_image_id
	left join entity_audit ea2 on ea2.entity_identifier = acct.account_id
where 
 (e1.entity_name = 'a' or e1.entity_name = 'b'or e1.entity_name = 'c'or e1.entity_name = 'd')
and ssodoc.category_type_id = 784
and (ea2.audit_type = '1' and ea2.entity_id = 491)
and acct.not_managed = '0'
and cl.client_name like '%vulcan%'
and m.meter_id not in
(
--meters with rate comparisons 
--with sso docs
--with entity audit
	select distinct m.meter_id
		
	from    rc_rate_comparison rc
		join meter m with (nolock) on rc.meter_id = m.meter_id
		join account acct with (nolock)on m.account_id = acct.account_id
		join address addr with (nolock)on m.address_id = addr.address_id
		join site s with (nolock)on addr.address_parent_id = s.site_id
		join division d with (nolock)on s.division_id = d.division_id
		join client cl with (nolock)on d.client_id = cl.client_id
		--join entity_audit ea on acct.account_id = ea.entity_identifier
		join state st with (nolock)on addr.state_id = st.state_id
		join region reg with (nolock)on st.region_id = reg.region_id
		join country ctry with (nolock)on st.country_id = ctry.country_id
		join rate r with (nolock)on m.rate_id = r.rate_id
		join entity e with (nolock)on r.commodity_type_id = e.entity_id
		left join entity e1 with (nolock)on acct.service_level_type_id = e1.entity_id
		left join entity e2 with (nolock)on acct.invoice_source_type_id = e2.entity_id
		join vendor v with (nolock)on acct.vendor_id = v.vendor_id
		left join user_info ui with (nolock)on rc.created_by = ui.user_info_id
		left join user_info ui1 with (nolock)on rc.reviewed_by = ui1.user_info_id
		left join utility_detail ud with (nolock)on v.vendor_id = ud.vendor_id
		left join utility_analyst_map uam with (nolock)on ud.utility_detail_id = uam.utility_detail_id
		left join user_info ui2 with (nolock)on uam.regulated_analyst_id = ui2.user_info_id
		left join entity e3 with (nolock)on e3.entity_id = rc.status_type_id
		left join entity e4 with (nolock)on e4.entity_id = rc.result_type_id
		left join sso_document ssodoc with (nolock)on ssodoc.cbms_image_id = rc.cbms_image_id
		left join entity_audit ea1 with (nolock)on ea1.entity_identifier = ssodoc.sso_document_id
		left join user_info ui4 with (nolock)on ui4.user_info_id = ea1.user_info_id
		left join entity_audit ea2 on ea2.entity_identifier = acct.account_id
	where 
	( (e1.entity_name = 'a' or e1.entity_name = 'b'or e1.entity_name = 'c'or e1.entity_name = 'd')
	and ssodoc.category_type_id = 784
	and (ea1.audit_type = '1' and ea1.entity_id = 1040)) 
	and (ea2.audit_type = '1' and ea2.entity_id = 491)
and acct.not_managed = '0'
and cl.client_name like '%vulcan%'
)

union
--meters with rate comparisons 
--with no sso docs

select distinct  cast((convert (varchar(10), ea2.modified_date, 101))as datetime) as 'Account Creation Date'
	,cast((convert(char(10), rollout_email_date,101))as datetime) as 'Rollout Date'
	,client_name as 'Client'
	,addr.city as 'City'
	,state_name as 'ST'
	,region_name as 'Region'
	,e.entity_name as 'Commodity'
	,account_number as 'Account#'
	,acct.account_id
	,meter_number as 'Meter #'
	,v.vendor_name as 'Utility'
	,rate_name as'Utility Rate'
	,e1.entity_name as 'Service Level'
	,ui2.first_name + ' ' + ui2.last_name as 'Responsible Analyst'
	,cast((convert (char(10),(rollout_email_date + 42),101))as datetime) as 'RC Review Deadline'
	--,e3.entity_name as 'Status'
	--,e4.entity_name as 'RC Outcome'
	--,null as 'Date RC Posted to DV'
	,m.comments as 'Comments'
	,'yes' as RCDone
	--,ui.first_name + ' ' + ui.last_name as 'RCCreatedBy'
	,cast ((convert (char (10) ,rc.creation_date ,101))as datetime) as 'RCCreationDate'
	--,ui1.first_name + ' ' + ui1.last_name  as 'RCReviewedBy'
	

from    rc_rate_comparison rc
	join meter m with (nolock) on rc.meter_id = m.meter_id
	join account acct with (nolock)on m.account_id = acct.account_id
	join address addr with (nolock)on m.address_id = addr.address_id
	join site s with (nolock)on addr.address_parent_id = s.site_id
	join division d with (nolock)on s.division_id = d.division_id
	join client cl with (nolock)on d.client_id = cl.client_id
	--join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st with (nolock)on addr.state_id = st.state_id
	join region reg with (nolock)on st.region_id = reg.region_id
	join country ctry with (nolock)on st.country_id = ctry.country_id
	join rate r with (nolock)on m.rate_id = r.rate_id
	join entity e with (nolock)on r.commodity_type_id = e.entity_id
	left join entity e1 with (nolock)on acct.service_level_type_id = e1.entity_id
	left join entity e2 with (nolock)on acct.invoice_source_type_id = e2.entity_id
	join vendor v with (nolock)on acct.vendor_id = v.vendor_id
	left join user_info ui with (nolock)on rc.created_by = ui.user_info_id
	left join user_info ui1 with (nolock)on rc.reviewed_by = ui1.user_info_id
	left join utility_detail ud with (nolock)on v.vendor_id = ud.vendor_id
	left join utility_analyst_map uam with (nolock)on ud.utility_detail_id = uam.utility_detail_id
	left join user_info ui2 with (nolock)on uam.regulated_analyst_id = ui2.user_info_id
	left join entity e3 with (nolock)on e3.entity_id = rc.status_type_id
	left join entity e4 with (nolock)on e4.entity_id = rc.result_type_id
	left join sso_document ssodoc with (nolock)on ssodoc.cbms_image_id = rc.cbms_image_id
	left join entity_audit ea2 on ea2.entity_identifier = acct.account_id
where 
( (e1.entity_name = 'a' or e1.entity_name = 'b'or e1.entity_name = 'c'or e1.entity_name = 'd')
and ssodoc.sso_document_id is null)
and (ea2.entity_id = 491 and ea2.audit_type = '1')
and acct.not_managed = '0'
and cl.client_name like '%vulcan%'
union

--********************************no rate comparisons at all

select distinct  cast((convert (varchar(10), ea2.modified_date, 101))as datetime) as 'Account Creation Date'
	,cast((convert(char(10), rollout_email_date,101))as datetime) as 'Rollout Date'
	,client_name as 'Client'
	,addr.city as 'City'
	,state_name as 'ST'
	,region_name as 'Region'
	,e.entity_name as 'Commodity'
	,account_number as 'Account#'
	,acct.account_id
	,meter_number as 'Meter#'
	,v.vendor_name as 'Utility'
	,rate_name as 'Utility Rate'
	,e1.entity_name as 'Service Level'
	,ui2.first_name + ' ' + ui2.last_name as 'Responsible Analyst'
	,cast ((convert (char(10),(rollout_email_date + 42),101)) as datetime) as 'RC Review Deadline'
	--,null as 'Status'
	--,null as 'RC Outcome'
	--,null as 'Date RC Posted to DV'
	,m.comments as 'Comments'
	



,'RCDone' = case when acct.account_id in (select account_id from rc_rate_comparison) then 'OtherMeter'
 else 'No' end
	--,null as RCCreatedBy
	,null as creation_date 
	--,null as RCReviewedBy
from 
meter m 
	join account acct with (nolock)on m.account_id = acct.account_id
	join address addr with (nolock)on m.address_id = addr.address_id
	join site s with (nolock)on addr.address_parent_id = s.site_id
	join division d with (nolock)on s.division_id = d.division_id
	join client cl with (nolock)on d.client_id = cl.client_id
	--join entity_audit ea on acct.account_id = ea.entity_identifier
	join state st with (nolock)on addr.state_id = st.state_id
	join region reg with (nolock)on st.region_id = reg.region_id
	join country ctry with (nolock)on st.country_id = ctry.country_id
	join rate r with (nolock)on m.rate_id = r.rate_id
	join entity e with (nolock)on r.commodity_type_id = e.entity_id
	left join entity e1 with (nolock)on acct.service_level_type_id = e1.entity_id
	left join entity e2 with (nolock)on acct.invoice_source_type_id = e2.entity_id
	join vendor v with (nolock)on acct.vendor_id = v.vendor_id
	left join utility_detail ud with (nolock)on v.vendor_id = ud.vendor_id
	left join utility_analyst_map uam with (nolock)on ud.utility_detail_id = uam.utility_detail_id
	join user_info ui2 with (nolock)on uam.regulated_analyst_id = ui2.user_info_id
	left join rc_rate_comparison rc on rc.meter_id = m.meter_id
	left join entity_audit ea2 on ea2.entity_identifier = acct.account_id
	
where 
 (e1.entity_name = 'a' or e1.entity_name = 'b'or e1.entity_name = 'c'or e1.entity_name = 'd')
and rc.meter_id is null
and (ea2.entity_id = 491 and audit_type = '1')
and acct.not_managed = '0'

and cl.client_name like '%vulcan%'





GO
