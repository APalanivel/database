SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view
[dbo].[lec_ActiveData_Client_TransportTariff_ServiceLevelBreakdown]
as
select AsOfDate
	,Client
	
	,Sites
	,Accounts
	,Meters
	,Utilities
	,Rates
	,x.ClientType
	,atr.SrvLevelATransport
	,atar.SrvLevelATariff
	,btr.SrvLevelBTransport
	,btar.SrvLevelBTariff
	,ctr.SrvLevelCTransport
	,ctar.SrvLevelCTariff
	,dtr.SrvLevelDTransport
	,dtar.SrvLevelDTariff
	--,b.SrvLevelB
	--,c.SrvLevelC
	--,d.SrvLevelD

from 
(
	select distinct getdate() as 'AsOfDate'
		,Client_name as Client
		,cl.client_id as ClientID
		,count (distinct s.site_id)  as 'Sites'
		,count (distinct acct.account_id) as 'Accounts'
		,count (distinct m.meter_id)  as 'Meters'
		,count (distinct v.vendor_id)  as 'Utilities'
		,count (distinct r.rate_id) as  'Rates'
		,clty.entity_name  as 'ClientType'
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id --and e.entity_name = 'a'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
	
	where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'

--and cl.client_name like '%abercrombie%'
		--and clty.entity_name <> 'gfe'
	group  by clty.entity_name, cl.Client_id,cl.client_name
		
)x
--TransportA
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelATransport'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'a'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
		join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
	
	where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		--and clty.entity_name <> 'gfe'
--and cl.client_name like '%abercrombie%'
	group  by clty.entity_name,cl.client_id) atr on atr.ClientType = x.ClientType and atr.clientid = x.clientId

--TariffA
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelATariff'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'a'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		and acct.account_id not in (
				select distinct acct.account_id
		
						from 
						client cl
						join division d on d.client_id = cl.client_id
						join site s on s.division_id = d.division_id
						join account acct on acct.site_id = s.site_id		
				join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'a'		
						join meter m on m.account_id = acct.account_id
						join vendor v on v.vendor_id = acct.vendor_id
						join rate r on r.rate_id = m.rate_id
						join entity clty on clty.entity_id = cl.client_type_id
						join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
						join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
					
					where   cl.not_managed = '0'
						and s.not_managed = '0'
						and acct.not_managed = '0'
						--and clty.entity_name <> 'gfe'
				--and cl.client_name like '%abercrombie%'
					) group by clty.entity_name, cl.client_id) atar on atar.Clienttype = x.clienttype and atar.clientid = x.clientid
	
	
--TransportB
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelBTransport'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'B'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
		join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
	
	where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		--and clty.entity_name <> 'gfe'
--and cl.client_name like '%abercrombie%'
	group  by clty.entity_name,cl.client_id) Btr on btr.ClientType = x.ClientType and btr.clientid = x.clientid

--TariffB
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelBTariff'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'B'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		and acct.account_id not in (
				select distinct acct.account_id
		
						from 
						client cl
						join division d on d.client_id = cl.client_id
						join site s on s.division_id = d.division_id
						join account acct on acct.site_id = s.site_id		
				join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'b'		
						join meter m on m.account_id = acct.account_id
						join vendor v on v.vendor_id = acct.vendor_id
						join rate r on r.rate_id = m.rate_id
						join entity clty on clty.entity_id = cl.client_type_id
						join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
						join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
					
					where   cl.not_managed = '0'
						and s.not_managed = '0'
						and acct.not_managed = '0'
						--and clty.entity_name <> 'gfe'
				--and cl.client_name like '%abercrombie%'
					) group by clty.entity_name,cl.client_id) Btar on btar.Clienttype = x.clienttype and btar.clientid = x.clientid
					
					--TransportC
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelCTransport'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'C'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
		join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
	
	where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		--and clty.entity_name <> 'gfe'
--and cl.client_name like '%abercrombie%'
	group  by clty.entity_name,cl.client_id) ctr on ctr.ClientType = x.ClientType and ctr.clientid= x.clientid

--TariffC
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelCTariff'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'C'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		and acct.account_id not in (
				select distinct acct.account_id
		
						from 
						client cl
						join division d on d.client_id = cl.client_id
						join site s on s.division_id = d.division_id
						join account acct on acct.site_id = s.site_id		
				join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'C'		
						join meter m on m.account_id = acct.account_id
						join vendor v on v.vendor_id = acct.vendor_id
						join rate r on r.rate_id = m.rate_id
						join entity clty on clty.entity_id = cl.client_type_id
						join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
						join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
					
					where   cl.not_managed = '0'
						and s.not_managed = '0'
						and acct.not_managed = '0'
						--and clty.entity_name <> 'gfe'
				--and cl.client_name like '%abercrombie%'
					) group by clty.entity_name,cl.client_id) ctar on ctar.Clienttype = x.clienttype and ctar.clientid = x.clientid
					
					
										--TransportD
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelDTransport'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'D'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
		join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
	
	where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		--and clty.entity_name <> 'gfe'
--and cl.client_name like '%abercrombie%'
	group  by clty.entity_name,cl.client_id) dtr on dtr.ClientType = x.ClientType and dtr.clientid = x.clientid

--TariffD
left join (
select distinct count (distinct acct.account_id) as 'SrvLevelDTariff'
		,clty.entity_name  as 'ClientType'
		,cl.client_id as ClientID
	from 
		client cl
		join division d on d.client_id = cl.client_id
		join site s on s.division_id = d.division_id
		join account acct on acct.site_id = s.site_id		
join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'd'		
		join meter m on m.account_id = acct.account_id
		join vendor v on v.vendor_id = acct.vendor_id
		join rate r on r.rate_id = m.rate_id
		join entity clty on clty.entity_id = cl.client_type_id
		where   cl.not_managed = '0'
		and s.not_managed = '0'
		and acct.not_managed = '0'
		and acct.account_id not in (
				select distinct acct.account_id
		
						from 
						client cl
						join division d on d.client_id = cl.client_id
						join site s on s.division_id = d.division_id
						join account acct on acct.site_id = s.site_id		
				join entity e on e.entity_id = acct.service_level_type_id and e.entity_name = 'd'		
						join meter m on m.account_id = acct.account_id
						join vendor v on v.vendor_id = acct.vendor_id
						join rate r on r.rate_id = m.rate_id
						join entity clty on clty.entity_id = cl.client_type_id
						join supplier_account_meter_map samm on samm.meter_id = m.meter_id and samm.meter_disassociation_date is null
						join contract c on c.contract_id = samm.contract_id and c.contract_type_id = 153 and c.contract_end_date > getdate()
					
					where   cl.not_managed = '0'
						and s.not_managed = '0'
						and acct.not_managed = '0'
						--and clty.entity_name <> 'gfe'
				--and cl.client_name like '%abercrombie%'
					) group by clty.entity_name,cl.client_id) dtar on dtar.Clienttype = x.clienttype and dtar.clientid = x.clientid


GO
