SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  view
[dbo].[lec_CompleteUserSite_DivisionList]
as
	--complete list from user_division_map and user_site_map tables
	select distinct 
			x.user_info_id
			,x.site_id
			,cl.client_name
			,cl.client_id
			,convert(varchar,x.user_info_id) + ','+ convert(varchar,x.site_id) as string
		from 
		(
			select user_info_id 
				,usm.site_id
			 from user_site_map usm -- access to certain specific sites
			union 
			select udm.user_info_id 
				,s.site_id
			 from  user_division_map udm -- access to all sites for that division
				join division d with (nolock) on d.division_id = udm.division_id
				join site s with (nolock) on s.division_id = d.division_id
		
			--order by user_info_id
		)x join site s on s.site_id = x.site_id
		join division d on d.division_id = s.division_id
		join client cl on cl.client_id = d.client_id

GO
