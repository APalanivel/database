SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
		dbo.BUDGET_UTILITY_CONTRACT_VW

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
select count(1) FROM dbo.BUDGET_UTILITY_CONTRACT_VW


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	 SS       	10/12/2009	Replaced Contract's account_id reference by by means of contract_id to link Supplier_account_meter_map.
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/  

CREATE VIEW dbo.BUDGET_UTILITY_CONTRACT_VW
AS  
  
 SELECT DISTINCT utilacc.account_id,  
  con.commodity_type_id,  
  con.contract_id,  
  con.ed_contract_number,  
  con.contract_start_date,  
  con.contract_end_date,  
  suppacc.vendor_id,  
  suppacc.account_id AS supp_account_id  
 FROM dbo.account utilacc(NOLOCK)  
  INNER JOIN dbo.meter met(NOLOCK) ON met.account_id = utilacc.account_id  
  INNER JOIN dbo.supplier_account_meter_map meterMap(NOLOCK) ON meterMap.meter_id = met.meter_id  
  INNER JOIN dbo.account suppacc(NOLOCK) ON meterMap.account_id = suppacc.account_id  
  INNER JOIN dbo.contract con(NOLOCK) ON con.CONTRACT_ID = meterMap.Contract_Id
 WHERE utilacc.account_type_id = 38 --//Utility account type  
  AND meterMap.is_history = 0  
  AND con.contract_type_id= 154 --// Utility Contract type
GO
