
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [DBAM].[Database_Dictionary]
AS
SELECT
      class
     ,CASE WHEN Class = 0 THEN 'Database'
           WHEN Class = 1
                AND minor_id = 0 THEN 'Table'
           WHEN Class = 1
                AND minor_id >= 1 THEN 'Column'
           WHEN Class = 2 THEN 'Parameter'                
           WHEN Class = 3 THEN 'Schema'
           WHEN Class = 4 THEN 'Database principal'
           WHEN Class = 5 THEN 'Assembly'
           WHEN Class = 6 THEN 'Type'
           WHEN Class = 7 THEN 'Index'
           WHEN Class = 10 THEN 'XML schema collection'
           WHEN Class = 15 THEN 'Message type'
           WHEN Class = 16 THEN 'Service contract'
           WHEN Class = 17 THEN 'Service'
           WHEN Class = 18 THEN 'Remote service binding'
           WHEN Class = 19 THEN 'Route'
           WHEN Class = 20 THEN 'Dataspace (filegroup or partition scheme)'
           WHEN Class = 21 THEN 'Partition function'
           WHEN Class = 22 THEN 'Database file'
           WHEN Class = 27 THEN 'Plan guide'
      END class_desc
     ,object_schema_name(p.major_id) Table_Schema
     ,object_name(major_id) Table_Name
     ,CASE WHEN minor_id = 0 THEN NULL
           ELSE col_name(major_id, minor_id)
      END Column_Name
     ,name
     ,value
FROM
      sys.extended_properties p

GO

