SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW dbam.Replicated_Tables_Publisher
AS
SELECT
      A.NAME Pub_Table_Name
     ,A.dest_table Sub_Table_Name
     ,P.NAME Publication_Name
     ,cast(A.schema_option AS BIGINT) Schema_Option
     ,sub.dest_db
     ,a.filter_clause
     ,P.post_snapshot_script
     ,case sub.update_mode
        WHEN 1 THEN 'Updateable'
        ELSE 'Read Only'
      END Update_Mode
FROM
      dbo.sysarticles A
      INNER JOIN Syspublications p
            ON a.pubid = p.pubid
      INNER JOIN dbo.syssubscriptions sub
            ON A.artid = sub.artid
WHERE
      sub.dest_db != 'Virtual'
UNION ALL
SELECT
      mart.name
     ,mart.destination_object
     ,mpub.name
     ,cast(mart.schema_option AS BIGINT) Schema_Option
     ,msub.[db_name]
     ,NULL AS Filter_Clause
     ,NULL AS Post_snapshot_Script
     ,'Merge' AS Update_mode
FROM
      dbo.sysmergearticles mart
      INNER JOIN sysmergepublications mpub
            ON mart.pubid = mpub.pubid
      INNER JOIN sysmergesubscriptions msub
            ON mpub.pubid = msub.pubid
WHERE
      msub.[db_name] != db_name()	      
GO
GRANT SELECT ON  [DBAM].[Replicated_Tables_Publisher] TO [public]
GO
