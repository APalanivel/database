SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  view
[dbo].[lec_Exceptions_SOPS]
as
select --convert(varchar, d.closed_date, 101)
		 t.exception_type as exceptions
		, ui.first_name + ' ' + ui.last_name as username
		, count(*) as total
	     from cu_exception_detail d
	     join user_info ui on ui.user_info_id = d.closed_by_id
	     join cu_exception_type t on t.cu_exception_type_id = d.exception_type_id

	    where d.closed_date >= '12/31/2006'
	      and d.is_closed = 1
	      and user_info_id in (	
			select user_info_id
			  from user_info
			 where username like '%egibbons%'
			    or username like '%jwebb%'
		            or username like '%jgee%'
			    or username like '%kabbott%'
			    or username like '%lblandford%'
		  	    or username like '%mgade%'
			    or username like '%tware%'
			    or username like '%tjones%'
			    or username like '%vmccurdy%'
			  )

	 group by 
		 t.exception_type
		, ui.first_name + ' ' + ui.last_name

	-- order by ui.first_name + ' ' + ui.last_name
		--, convert(varchar, d.closed_date, 101)
		--, t.exception_type



GO
