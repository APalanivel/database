SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






















CREATE                     view
[dbo].[lec_expected_received_0907_FYRToDate_June]
as


	select 'Expected' invoice_type
			,client_name
			
			
			--, 'Aug06' = isNull(sum(case when x.service_month = '08/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Sep06' = isNull(sum(case when x.service_month = '09/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Oct06' = isNull(sum(case when x.service_month = '10/01/06' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'Nov06' = isNull(sum(case when x.service_month = '11/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Dec06' = isNull(sum(case when x.service_month = '12/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			----, 'Jan07' = isNull(sum(case when x.service_month = '01/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'Feb07' = isNull(sum(case when x.service_month = '02/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Mar07' = isNull(sum(case when x.service_month = '03/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Apr07' = isNull(sum(case when x.service_month = '04/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'May07' = isNull(sum(case when x.service_month = '05/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jun07' = isNull(sum(case when x.service_month = '06/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jul07' = isNull(sum(case when x.service_month = '07/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			, 'Total' = convert (decimal (6,1),sum (account_count))
		     from (
			   select ip.service_month
				, count(distinct ip.account_id) account_count
				,client_name
			     from division d  WITH (NOLOCK)
			     join site s WITH (NOLOCK) on s.division_id = d.division_id
			     join vwAccountMeter vam WITH (NOLOCK) on vam.site_id = s.site_id
			     join invoice_participation ip WITH (NOLOCK) on ip.account_id = vam.account_id
			     join client cl WITH (NOLOCK)on d.client_id = cl.client_id
			    --where ip.service_month between '08/01/06' and '07/01/07'
				where ip.service_month between '06/01/07' and '07/01/07'
			      and ip.is_expected = 1
				and 
( client_name like '%Darden Restaurants%'
)
--darden june

			 group by ip.service_month
				  ,cl.client_name
			  ) x
			 group by client_name
		union all
		   select 'Received' invoice_type
			,client_name
			
--, 'Aug06' = isNull(sum(case when x.service_month = '08/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Sep06' = isNull(sum(case when x.service_month = '09/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Oct06' = isNull(sum(case when x.service_month = '10/01/06' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'Nov06' = isNull(sum(case when x.service_month = '11/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Dec06' = isNull(sum(case when x.service_month = '12/01/06' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			----, 'Jan07' = isNull(sum(case when x.service_month = '01/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'Feb07' = isNull(sum(case when x.service_month = '02/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Mar07' = isNull(sum(case when x.service_month = '03/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			--, 'Apr07' = isNull(sum(case when x.service_month = '04/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			--, 'May07' = isNull(sum(case when x.service_month = '05/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jun07' = isNull(sum(case when x.service_month = '06/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jul07' = isNull(sum(case when x.service_month = '07/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			, 'Total' = convert (decimal (6,1),sum (account_count))
		     		     from (
			   select ip.service_month
				, count(distinct ip.account_id) account_count
				,client_name
			     from division d WITH (NOLOCK) 
			     join site s WITH (NOLOCK) on s.division_id = d.division_id
			     join vwAccountMeter vam WITH (NOLOCK) on vam.site_id = s.site_id
			     join invoice_participation ip WITH (NOLOCK) on ip.account_id = vam.account_id
			     join client cl WITH (NOLOCK)on d.client_id = cl.client_id
			--  where ip.service_month between '08/01/06' and '07/01/07'
 			 where ip.service_month between '06/01/07' and '07/01/07'
			      and ip.is_received = 1
						and 
( client_name like '%Darden Restaurants%'
)

			 group by ip.service_month
				  ,cl.client_name
			  ) x
			 group by client_name
			
	--order by client_name

	
	
	
	
	
	
	
























GO
