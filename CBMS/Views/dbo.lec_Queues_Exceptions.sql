SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view
[dbo].[lec_Queues_Exceptions]
as

select distinct	
	queue_name [Analyst]
	,d.CU_INVOICE_ID [Invoice ID]
	,d.EXCEPTION_STATUS_TYPE [Status]
	,c.IMAGE_COMMENT [Comment] --concatenate
	,c.COMMENTDATE [Comment Date]
	,d.EXCEPTION_TYPE [Type]
	,d.SERVICE_MONTH [Month]
	,ubm_SITE_NAME [Site]
	,ubm_CITY [City]
	,ubm_STATE_NAME [State]
	,ubm_ACCOUNT_NUMBER [Account Number]
	,v.VENDOR_NAME [Vendor]
	,DATE_IN_QUEUE [Date in Queue]
	--,ui.FIRST_NAME + ' ' + ui.last_name [QueueName]
from CU_EXCEPTION_DENORM d
join vwCbmsQueueName q on q.queue_id = d.QUEUE_ID
LEFT JOIN account a ON a.account_id = d.account_id
LEFT JOIN vendor v ON v.vendor_id = a.vendor_id
left join user_info ui on ui.FIRST_NAME + ' ' + ui.last_name = q.queue_name
join 	USER_INFO_GROUP_INFO_MAP m on m.USER_INFO_ID = ui.USER_INFO_ID and m.GROUP_INFO_ID = 81		
left join (select cuc.cu_invoice_id		
				,image_comment		
				,x.commentdate
				from cu_invoice_comment cuc
				join (select MAX(comment_date)commentdate
						,cu_invoice_id
							from CU_INVOICE_COMMENT	
							group by cu_invoice_id	
							)x on x.CU_INVOICE_ID = cuc.cu_invoice_id and x.commentdate = cuc.COMMENT_DATE
							group by cuc.CU_INVOICE_ID, image_comment,x.commentdate) c on c.CU_INVOICE_ID = d.CU_INVOICE_ID

GO
