SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

































--invoice participation using scott's stored procedure
CREATE                      view
[dbo].[lec_expected_received_021910]
as


	select 'Expected' invoice_type
			,client_name
			
			
			
			
			
			
			/*
			, 'Feb07' = isNull(sum(case when x.service_month = '02/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Mar07' = isNull(sum(case when x.service_month = '03/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Apr07' = isNull(sum(case when x.service_month = '04/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			, 'May07' = isNull(sum(case when x.service_month = '05/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jun07' = isNull(sum(case when x.service_month = '06/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Jul07' = isNull(sum(case when x.service_month = '07/01/07' then convert (decimal(4,1), x.account_count)else 0 end), 0)
			*/
			, 'Total' = convert (decimal (16,1),sum (account_count))
		     from (
			   select ip.service_month
				, count(distinct ip.account_id) account_count
				,client_name
			     from invoice_participation ip 
			     join vwAccountMeter vam on vam.account_id = ip.ACCOUNT_ID
			     join METER m on m.METER_ID = vam.meter_id
			     join ACCOUNT a on a.ACCOUNT_ID = m.ACCOUNT_ID
			     join SITE s on s.SITE_ID = a.SITE_ID and s.SITE_ID = ip.SITE_ID
			     join CLIENT c on c.CLIENT_ID = s.Client_ID
			        
			  where ip.service_month between  '2010-03-01' and '2011-02-01'
			      and ip.is_expected = 1
						and a.NOT_MANAGED !=1
						and s.NOT_MANAGED !=1
				 group by ip.service_month
				  ,c.client_name
			  ) x
			 group by client_name
		union all
		   select 'Received' invoice_type
			,client_name
			/*
			, 'Feb07' = isNull(sum(case when x.service_month = '02/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			, 'Mar07' = isNull(sum(case when x.service_month = '03/01/07' then convert (decimal(4,1), x.account_count) else 0 end), 0)
			*/
			, 'Total' = convert (decimal (16,1),sum (account_count))
		     from (
			   select ip.service_month
				, count(distinct ip.account_id) account_count
				,client_name
			     from invoice_participation ip 
			     join vwAccountMeter vam on vam.account_id = ip.ACCOUNT_ID
			     join METER m on m.METER_ID = vam.meter_id
			     join ACCOUNT a on a.ACCOUNT_ID = m.ACCOUNT_ID
			     join SITE s on s.SITE_ID = a.SITE_ID and s.SITE_ID = ip.SITE_ID
			     join CLIENT c on c.CLIENT_ID = s.Client_ID
			        
			  where ip.service_month between '2010-03-01' and '2011-02-01'
			      and ip.is_received = 1
						--and c.client_name like '%agc%'
						and a.NOT_MANAGED !=1
						and s.NOT_MANAGED !=1
			 group by ip.service_month
				  ,c.client_name
			  ) x
			 group by client_name
			
	--order by client_name

	
	
	/*
	select *
	from dbo.lec_expected_received_percentage*/

	
	
	


































GO
