SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view 
[dbo].[lec_BillSourcingQueryCommercialData]
as
select distinct Client
		,SiteCount
		,SiteCountOnContract
		,Utility
		,State
		,Region
		,core.Commodity
		,Analyst
		,Manager
from 
(select distinct Client
	,count (distinct site_id) as SiteCount
	,Utility
	,State
	,Region
	,Commodity
	,Analyst	
	,Manager
from (
	select distinct 
		Client_name as Client
		,s.site_id
		,v.vendor_name as Utility
		,e.entity_name as Commodity
		,st.state_name as State
		,region_name as Region
		,Analyst = case when r.commodity_type_id = '291' then ui.first_name + ' ' + ui.last_name
			when r.commodity_type_id = '290'then ui2.first_name + ' ' + ui2.last_name end
		,Manager = case when r.commodity_type_id = '291' then ui1.first_name + ' ' + ui1.last_name
			when r.commodity_type_id = '290' then ui3.first_name + ' ' + ui3.last_name end
	from   client cl 
	join   division d on d.client_id = cl.client_id
	join   site s on s.division_id = d.division_id
	join   account acct on acct.site_id  = s.site_id and acct.account_type_id = '38'
	join   vendor v on v.vendor_id = acct.vendor_id
	join   meter m on m.account_id = acct.account_id
	join   rate r on r.rate_id = m.rate_id
	join   entity e on e.entity_id = r.commodity_type_id
	join   address addr on addr.address_id = s.primary_address_id
	join   state st on st.state_id = addr.state_id
	join   region reg on reg.region_id = st.region_id
	join   country ctry on ctry.country_id = st.country_id and ctry.country_id in ('1','2','4')
	left join  utility_detail ud on ud.vendor_id = v.vendor_id
	left join  utility_analyst_map map on map.utility_detail_id = ud.utility_detail_id
	left join  user_info ui on ui.user_info_id = map.gas_analyst_id
	left join  sr_sa_sm_map smap on smap.sourcing_analyst_id = map.gas_analyst_id
	left join  user_info ui1 on ui1.user_info_id = smap.sourcing_manager_id

	left join  user_info ui2 on ui2.user_info_id = map.power_analyst_id
	left join  sr_sa_sm_map smap2 on smap2.sourcing_analyst_id = map.power_analyst_id
	left join  user_info ui3 on ui3.user_info_id = smap2.sourcing_manager_id
			

	where cl.not_managed <> '1'
	and   s.not_managed <> '1'
	and   acct.not_managed <> '1'
	and   (cl.client_type_id = '141' or cl.client_id = 11231)
	
)c
group by client
	,utility
	,analyst
	,manager
	,commodity
	,state
	,region
)core

	left join (
					select count (distinct s.site_id) SiteCountOnContract
						,v1.vendor_name
						,commodity = case when c.commodity_type_id = 290 then 'Electric Power' else 'Natural Gas' end

						,client_name
					from   contract c
					join   supplier_account_meter_map samm on samm.contract_id = c.contract_id
					join   meter m on m.meter_id = samm.meter_id and samm.meter_disassociation_date is null
					join   account a on a.account_id = m.account_id and a.service_level_type_id <> '1024'
					join   account a1 on a1.account_id = samm.account_id
					join   vendor v on v.vendor_id = a1.vendor_id
					join   vendor v1 on v1.vendor_id = a.vendor_id
					join   site s on s.site_id = a.site_id and s.not_managed <> '1' and a.not_managed <> '1'
					join   division d on d.division_id = s.division_id
					join   client cl on cl.client_id = d.client_id and   (cl.client_type_id = '141' or cl.client_id = 11231)
				  where   c.contract_end_date > getdate() 
	
					group by v1.vendor_name, commodity_type_id,cl.client_name)sitesupcounts on sitesupcounts.vendor_name = core.utility and sitesupcounts.commodity = core.commodity
						and sitesupcounts.client_name = core.client
				 

GO
