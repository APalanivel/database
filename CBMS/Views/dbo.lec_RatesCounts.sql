SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_RatesCounts] as
with account_ven_rate_cte as 
(
select distinct 
ca.Account_Number
,ca.account_id
,ca.account_vendor_id [vendor_id]
,ca.Account_Vendor_Name 
,ca.Rate_Id
,Commodity = case
 when ca.account_id in
		(select distinct Account_Id
from Core.Client_Hier_Account
where Commodity_Id = 290 -- Electric Power
and Account_Not_Managed =0
and Account_Id in 
(select distinct Account_Id
from Core.Client_Hier_Account
where Commodity_Id = 291-- Natural Gas--
and Account_Not_Managed =0))
	then 'EP-NG'--3070 
	when ca.account_id in	
	(select distinct Account_Id
from Core.Client_Hier_Account
where Commodity_Id = 290 -- Electric Power
and Account_Type = 'Utility'
and Account_Not_Managed =0
and Account_Id not in 
(select distinct Account_Id
from Core.Client_Hier_Account 
where Commodity_Id != 290
and Account_Not_Managed =0))-- Electric Power--
then 'EP'--224673
when ca.account_id in	
(
select distinct Account_Id
from Core.Client_Hier_Account
where Commodity_Id = 291 -- Natural Gas
and Account_Type = 'Utility'
and Account_Not_Managed =0
and Account_Id not in 
(select distinct Account_Id
from Core.Client_Hier_Account
where Commodity_Id != 291
and Account_Not_Managed =0))-- Natural Gas
then 'NG'--28516
else 'OtherServicesOnly' end

from Core.Client_Hier_Account ca
join Core.Client_Hier ch on ch.Client_Hier_Id = ca.Client_Hier_Id
where ca.Account_Type = 'Utility'
and ca.Account_Not_Managed = 0

)
,Vendor_Analyst_CTE as
(
select v.vendor_id
,v.VENDOR_NAME
	,ui1.FIRST_NAME + ' ' + ui1.LAST_NAME [Manager]
	,ui.FIRST_NAME + ' ' + ui.LAST_NAME [ManagingAnalyst]
	,STATE_NAME [State]
	,REGION_NAME [Region]
from vendor v
join utility_detail ud on ud.vendor_id = v.vendor_id
join utility_detail_analyst_map udam on udam.utility_detail_id = ud.utility_detail_id
join user_info ui on ui.USER_INFO_ID = udam.Analyst_ID
join GROUP_INFO gi on gi.GROUP_INFO_ID = udam.Group_Info_ID
join VENDOR_STATE_MAP vsm on vsm.VENDOR_ID = v.VENDOR_ID
join STATE st on st.STATE_ID = vsm.STATE_ID
join REGION reg on reg.REGION_ID = st.REGION_ID
join REGION_MANAGER_MAP rmm on rmm.REGION_ID = reg.REGION_ID
join user_info ui1 on ui1.USER_INFO_ID = rmm.USER_INFO_ID
where gi.GROUP_NAME = 'regulated Markets'
)

Select Account_vendor_name [Utility]

,COUNT (distinct ac.rate_id) [Count of Rates by Utility]
,COUNT (distinct ac.account_id) [Count of Accounts by Utility]
,ManagingAnalyst
,Manager
,State
,Region
,ac.Commodity
from account_ven_rate_cte ac
join Vendor_Analyst_CTE vc on vc.VENDOR_ID = ac.vendor_id
group by Account_Vendor_Name
,Commodity
,Manager
,ManagingAnalyst
,State
,Region


/*

select distinct Account_Id
from Core.Client_Hier_Account
where Account_Type = 'Utility'
and Account_Not_Managed =0
--All 278103*/
GO
