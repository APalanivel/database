SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******
NAME:
		dbo.SR_RFP_GET_CONTRACT_DETAILS_FOR_CHECKLIST_P   

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DR			Deana Ritter

MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	DR			10/9/2009	Adjusted joins for movement of Account_ID and Contract_ID to SAMM
******/
CREATE     VIEW [dbo].[SR_CONTRACT_VW]
AS select   utilacc.account_id
          , met.meter_id
          , con.commodity_type_id
          , con.contract_id
          , con.ed_contract_number
          , con.contract_start_date
          , con.contract_end_date
          , ( con.contract_end_date - con.notification_days ) contract_notice_date
          , suppacc.vendor_id
   from     account utilacc
            join meter met
               on met.account_id = utilacc.account_id
            join ENTITY E1
               on utilacc.account_type_id = E1.entity_id
            join supplier_account_meter_map meterMap
               on meterMap.meter_id = met.meter_id
            join account suppacc
               on meterMap.account_id = suppacc.account_id
            join contract con ( NOLOCK )
                              on meterMap.Contract_Id = Con.CONTRACT_ID
            join entity E2
               on con.contract_type_id = E2.entity_id
   where    E1.entity_type = 111
            and E1.entity_name = 'Utility'
            And E2.Entity_type = 129
            And E2.Entity_name = 'Supplier'
            and meterMap.is_history = 0









GO
