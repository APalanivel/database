SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE    view
[dbo].[lec_LoadProfile_SMRTracker]
as
select distinct srr.sr_rfp_id as RFP_ID
	,max (ui.first_name + ' ' + ui.last_name) as CEM
	,Client_Name
	--,Site_Name
	
	,lp_contact_first_name + ' ' + lp_contact_last_name   as Client_SiteContact
	,addr.City
	,State_name as State
	,s.site_id as SiteID
	,account_number as AccountNumber
	,e.entity_name as CommodityType
	,lp_sent_to_client_date as DateLoadProfileSent
	,lp_sent_to_client_date + 3 as DateLoadProfileDue
	,'LPStatus' = case when lp_approved_date is null and lp_sent_to_client_date +3 < getdate() then 'overdue' 
			when lp_approved_date is null and lp_sent_to_client_date is null then null
			else 'received' end
	,lp_approved_date as LPProfileReceived
	,sent_reco_date as DateSMRSent
	,sent_reco_date + 3 as DateSMRDue
	,'SMRReceived' = case when is_client_approved = '1' then 'Yes' else 'No' end
	,ui1.first_name + ' ' + ui1.last_name as SourcingAnalyst
	,null as 'Comments'
---have to think about null, null
from sr_rfp srr
join sr_rfp_account srracct with (nolock)on srracct.sr_rfp_id = srr.sr_rfp_id
join sr_rfp_account_meter_map srramm with (nolock)on srramm.sr_rfp_account_id = srracct.sr_rfp_account_id
join meter m with (nolock)on m.meter_id = srramm.meter_id
join address addr with (nolock)on addr.address_id = m.address_id
join site s with (nolock)on s.site_id = addr.address_parent_id
join division d with (nolock)on d.division_id = s.division_id
join client cl with (nolock)on cl.client_id = d.client_id
join client_cem_map ccm with (nolock)on ccm.client_id = cl.client_id
join user_info ui with (nolock)on ui.user_info_id = ccm.user_info_id
join state st with (nolock)on st.state_id = addr.state_id
join rate r with (nolock)on r.rate_id = m.rate_id
join entity e with (nolock)on e.entity_id = r.commodity_type_id
join sr_rfp_checklist srrch with (nolock)on srrch.sr_rfp_account_id = srracct.sr_rfp_account_id
join account acct with (nolock)on acct.account_id = srracct.account_id
join user_info ui1 with (nolock)on ui1.user_info_id = srr.initiated_by

group by srr.sr_rfp_id
	,client_name
	,site_name
	,lp_contact_first_name + ' ' + lp_contact_last_name
	,addr.city
	,state_name
	,s.site_id
	,e.entity_name
	,lp_sent_to_client_date
	,lp_approved_date
	,account_number
	,ui1.first_name + ' ' + ui1.last_name
	,sent_reco_date
	,is_client_approved
--order by srr.sr_rfp_id






GO
