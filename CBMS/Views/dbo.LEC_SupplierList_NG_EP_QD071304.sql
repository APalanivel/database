SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create  view
[dbo].[LEC_SupplierList_NG_EP_QD071304]
as
select distinct vendor_name, entity_name as CommodityType
from vendor v
inner join vendor_commodity_map vcm on v.vendor_id = vcm.vendor_id
inner join entity e on vcm.commodity_type_id = e.entity_id
where vendor_type_id = 288 and (entity_name = 'Natural Gas' or entity_name = 'Electric Power')

GO
