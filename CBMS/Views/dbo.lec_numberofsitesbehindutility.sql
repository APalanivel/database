SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_numberofsitesbehindutility]
as


select count (account_number) as NumberofAccounts
	--,count (site_id) as NumberofSites
	,vendor_name	
from 
(
	select distinct client_name
		,s.site_id
		,addr.address_line1
		,city
		,state_name	
		,vendor_name
		,account_number
		,meter_number
		
	from client cl
	join division d on cl.client_id = d.client_id
	join site s on d.division_id = s.division_id
	join address addr on s.site_id = addr.address_parent_id
	join meter m on addr.address_id = m.address_id
	join account acct on m.account_id = acct.account_id
	join vendor v on acct.vendor_id = v.vendor_id
	join vendor_state_map vsm on v.vendor_id = vsm.vendor_id
	join vendor_commodity_map vcm on v.vendor_id = vcm.vendor_id
	join state st on vsm.state_id = st.state_id
	join rate r on m.rate_id = r.rate_id
	where state_name = 'ny'
	and r.commodity_type_id = 291
	and acct.not_managed = '0'
)
x

group by vendor_name
GO
