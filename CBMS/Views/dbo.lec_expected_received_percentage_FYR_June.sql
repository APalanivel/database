SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










create          view
[dbo].[lec_expected_received_percentage_FYR_June]
as
select 
	client_name  
	,convert(decimal (4,3), total) as Total
from
(
	select 'percentage' as Percentage
		,lec_exp.client_name as client_name
		,lec_rev.total/lec_exp.total as total
	from dbo.lec_expected_received_0907_FYRToDate_June lec_exp
	join dbo.lec_expected_received_0907_FYRToDate_June lec_rev on lec_exp.client_name = lec_rev.client_name
	where lec_exp.invoice_type = 'expected'
	and lec_rev.invoice_type = 'received'
	and lec_exp.total <> 0
)
x










GO
