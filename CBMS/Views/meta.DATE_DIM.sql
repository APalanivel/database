SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [meta].[DATE_DIM]
WITH
     SCHEMABINDING
AS
SELECT
      dde.Calendar_Day_Id AS DATE_ID
     ,dde.Date_D AS DATE_D
     ,dde.First_Day_Of_Month_d AS FIRST_DAY_OF_MONTH_D
     ,dde.Last_Day_Of_Month_d AS LAST_DAY_OF_MONTH_D
     ,dde.Days_In_Month_Num AS DAYS_IN_MONTH_NUM
     ,dde.Year_Num AS YEAR_NUM
     ,dde.Quarter_Num AS QUARTER_NUM
     ,dde.Month_Num AS MONTH_NUM
     ,dde.Month_Name AS MONTH_NAME
     ,dde.Week_Of_Year_Num AS WEEK_OF_YEAR_NUM
     ,dde.Full_Week_Of_Month_Num AS FULL_WEEK_OF_MONTH_NUM
     ,dde.Day_Of_Year_Num AS DAY_OF_YEAR_NUM
     ,dde.Day_Of_Month_Num AS DAY_OF_MONTH_NUM
     ,dde.Day_Of_Week_Num AS DAY_OF_WEEK_NUM
     ,dde.Day_Of_Week_Name AS DAY_NAME
     ,dde.Is_Weekday AS IS_WEEKDAY
     ,dde.Semi_Annual_Num AS SemiAnnual_Num
FROM
      meta.Date_Dim_Expd dde
WHERE
      dde.Day_Of_Month_Num = 1

;
GO

CREATE UNIQUE CLUSTERED INDEX [uix_Date_Dim_Date_Dt] ON [meta].[DATE_DIM] ([DATE_D]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Day_Name] ON [meta].[DATE_DIM] ([DAY_NAME]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_First_Day_Of_Month_Dt] ON [meta].[DATE_DIM] ([FIRST_DAY_OF_MONTH_D]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Is_WeekDay] ON [meta].[DATE_DIM] ([IS_WEEKDAY]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Last_Day_Of_Month_Dt] ON [meta].[DATE_DIM] ([LAST_DAY_OF_MONTH_D]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Year_Num_Day_Of_Year_Num] ON [meta].[DATE_DIM] ([YEAR_NUM], [DAY_OF_YEAR_NUM]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Year_Num_Month_Num] ON [meta].[DATE_DIM] ([YEAR_NUM], [MONTH_NUM]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_Date_Dim_Year_Num_Quarter_Num] ON [meta].[DATE_DIM] ([YEAR_NUM], [QUARTER_NUM]) ON [DB_DATA01]

GO
CREATE NONCLUSTERED INDEX [uix_DATE_DIM_YEAR_NUM_SEMIANNUAL_NUM] ON [meta].[DATE_DIM] ([YEAR_NUM], [SemiAnnual_Num]) ON [DB_DATA01]
