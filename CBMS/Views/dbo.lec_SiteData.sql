SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--/*
CREATE   view
[dbo].[lec_SiteData]
as
--*/
select Client
	,Site	
	,City
	,State
	,ContractingEntity
	,LegalStructure
	,TaxNumber
	,DunsNumber
	,NAICS
	,NotManaged
	,max (CEMName)as CEM_Name
from
(
	select client_name as Client
		,site_name as Site
		,addr.city as City
		,state_name as State
		,s.contracting_entity as ContractingEntity
		,s.client_legal_structure as LegalStructure
		,s.tax_number as TaxNumber
		,s.duns_number as DunsNumber
		,s.naics_code as NAICS
		,'NotManaged' = case when s.not_managed = '1' then 'Yes' else 'No' end
		,ui.first_name + ' ' + ui.last_name as CEMName
	from client cl
	join division d on cl.client_id = d.client_id
	join site s on s.division_id = d.division_id
	join address addr on addr.address_parent_id = s.site_id
	join state st on st.state_id = addr.state_id
	join client_cem_map ccm on ccm.client_id = cl.client_id
	join user_info ui on ui.user_info_id = ccm.user_info_id
	
	--order by client_name, site_name
)x
group by
 Client
	,Site	
	,City
	,State
	,ContractingEntity
	,LegalStructure
	,TaxNumber
	,DunsNumber
	,NAICS
	,NotManaged
	

GO
