SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--SELECT * FROM SR_SW_RFP_SITE_COUNT_FOR_BID_GROUPS_VW
--select * from SR_SW_RFP_SITE_ID_BID_GROUPS_VW
CREATE view dbo.SR_SW_RFP_SITE_ID_BID_GROUPS_VW
AS
	select 	s.site_id,sitecount.group_id 
	from  
		sr_rfp_account rfp_account(nolock)join account a(nolock)
		on a.account_id = rfp_account.account_id and rfp_account.is_deleted = 0
		join sr_rfp_bid_group bidgroup(nolock) on rfp_account.sr_rfp_bid_group_id = bidgroup.sr_rfp_bid_group_id
		join SR_SW_RFP_SITE_COUNT_FOR_BID_GROUPS_VW sitecount on bidgroup.sr_rfp_bid_group_id = sitecount.group_id 
		and sitecount.site_count = 1
		join site s(nolock) on s.site_id = a.site_id
		group by  sitecount.group_id ,s.site_id
	union 
	select '-1 ' as site_id ,
		  group_id 
	from 	SR_SW_RFP_SITE_COUNT_FOR_BID_GROUPS_VW
		where site_count > 1
GO
