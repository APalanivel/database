SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








--select * from SR_RC_RATE_IMAGE_VW

CREATE    VIEW dbo.SR_RC_RATE_IMAGE_VW
	AS				

select 	rcRate.account_id, 
	max(rcRate.cbms_image_id) as cbms_image_id,
	max(rcRateImage.date_imaged)as date_imaged

from 	RC_RATE_COMPARISON rcRate(nolock),
	cbms_image rcRateImage(nolock)
where 	rcRateImage.cbms_image_id = rcRate.cbms_image_id
group by rcRate.account_id












GO
