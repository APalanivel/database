SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE    view
 [dbo].[lec_ATTBudgetAccountData_073108]
 as
select distinct 
	site_name as Site
	,state_name as State
	,variablecost as Variable$
	,Transportcost as Transport$
	,TransmissionCost as Transmission$
	,distcost as Distribution$
	,OtherBundled as OtherBundled$
	,OtherFixed as OtherFixed$
	,SourcingTax as SourcingTax$
	,Ratestax as RatesTax$
	,account_number as Account
	,rate_name as Rate
	,vendor_name as Vendor
	,Month_identifier as [Month]
	,Commodity
	,TotalCostPreTax$ = variablecost + transportcost + distcost + otherbundled+ otherfixed +transmissioncost
	,TotalCost$ = variablecost + transportcost + transmissioncost+distcost + otherbundled+otherfixed + sourcingtax + ratestax
	,budget_usage as BudgetUsage
	--,UnitCost = (variablecost + transportcost + distcost + otherbundled+otherfixed + sourcingtax + ratestax)/budget_usage
	--,Volume
	,VolumeType
	,usagetype as UsageType
	,Transport_Tariff
	,Division_Name
from (
	select distinct
		site_name
		,budget_usage
		,state_name
		,volume
		,e2.entity_id
		,variablecost = case when budget_usage is not null and variable_value is not null then
		 variable_value * budget_usage else '0' end
		,transportcost = case when budget_usage is not null and transportation_value is not null then
		transportation_value * budget_usage else '0' end
		,distcost = case when budget_usage is not null and distribution_value is not null then
		distribution_value * budget_usage else '0' end
		,transmissioncost = case when budget_usage is not null and transmission_value is not null then
		transmission_value * budget_usage else '0' end
		,otherbundled = case when budget_usage is not null and other_bundled_value is not null then
		budget_usage * other_bundled_value else '0' end
	
		,otherfixed = case when other_fixed_costs is not null then other_fixed_costs_value else '0' end
		,sourcingtax = case when budget_usage is not null and  sourcing_tax_value is not null
		then budget_usage * sourcing_tax_value else '0' end
		,ratestax = case when budget_usage is not null and  rates_tax_value is not null
		then budget_usage * rates_tax_value else '0' end
		,account_number
		,rt.rate_name
		,v.vendor_name
		,bd.month_identifier
	--	,b.budget_id
		,e.entity_name commodity
		,e1.entity_name usagetype
		,e2.entity_name as volumetype
		,Transport_Tariff = case when y.account_id is not null then 'Transport' else 'Tariff' end
		,division_name
	from  budget b
	join    budget_account ba on ba.budget_id = b.budget_id
	join    client cl on cl.client_id = b.client_id
	join budget_details bd on bd.budget_account_id = ba.budget_account_id
	join account acct on acct.account_id = ba.account_id
	join site s on s.site_id = acct.site_id
	join entity e on e.entity_id = b.commodity_type_id
	left join budget_usage bu on bu.account_id = ba.account_id
	and bu.commodity_type_id = b.commodity_type_id
	and bu.month_identifier = bd.month_identifier
	join address addr on addr.address_id = s.primary_address_id 
	join state st on st.state_id = addr.state_id
	left join entity e1 on e1.entity_id = bu.budget_usage_type_id
	join entity e2 on e2.entity_id = bu.volume_unit_type_id
	join vendor v on v.vendor_id = acct.vendor_id
	join 
		(select distinct max(r.rate_name) rate_name
				,a.account_id
				from account a 
				join meter m on m.account_id = a.account_id
				join rate r on r.rate_id = m.rate_id and r.commodity_type_id = 291
				group by a.account_id) rt on rt.account_id = acct.account_id
	
	left join (select acct.account_id
			from supplier_account_meter_map samm
			join meter m on m.meter_id = samm.meter_id
			join contract c on c.contract_id = samm.contract_id
			join account acct on acct.account_id = m.account_id
			join site s on s.site_id = acct.site_id
			join division d on d.division_id = s.division_id
			join client cl on cl.client_id = d.client_id
						where cl.client_id = '11231'
			and samm.meter_disassociation_date is null
			and c.commodity_type_id = '291'
			and c.contract_end_date > getdate()
			AND c.contract_type_id = '153')y on y.account_id = ba.account_id
	join division d on d.division_id = s.division_id
and ba.is_deleted <> '1'
	where b.budget_ID IN ('4808')

)x


union
select distinct 
	site_name as Site
	,state_name as State
	,variablecost as Variable$
	,Transportcost as Transport$
	,TransmissionCost as Transmission$
	,distcost as Distribution$
	,OtherBundled as OtherBundled$
	,OtherFixed as OtherFixed$
	,SourcingTax as SourcingTax$
	,Ratestax as RatesTax$
	,account_number as Account
	,rate_name as Rate
	,vendor_name as Vendor
	,Month_identifier as [Month]
	,Commodity
	,TotalCostPreTax$ = variablecost + transportcost + distcost + otherbundled+ otherfixed +transmissioncost
	,TotalCost$ = variablecost + transportcost + transmissioncost+distcost + otherbundled+otherfixed + sourcingtax + ratestax
	,budget_usage as BudgetUsage
	--,UnitCost = (variablecost + transportcost + distcost + otherbundled+otherfixed + sourcingtax + ratestax)/budget_usage
	--,Volume
	,VolumeType
	,usagetype as UsageType
	,Transport_Tariff
	,Division_Name
from (
	select distinct
		site_name
		,state_name
		,budget_usage
		,volume
		,e2.entity_id
		,variablecost = case when budget_usage is not null and variable_value is not null then
		 variable_value * budget_usage else '0' end
		,transportcost = case when budget_usage is not null and transportation_value is not null then
		transportation_value * budget_usage else '0' end
		,distcost = case when budget_usage is not null and distribution_value is not null then
		distribution_value * budget_usage else '0' end
		,transmissioncost = case when budget_usage is not null and transmission_value is not null then
		transmission_value * budget_usage else '0' end
		,otherbundled = case when budget_usage is not null and other_bundled_value is not null then
		budget_usage * other_bundled_value else '0' end
	
		,otherfixed = case when other_fixed_costs is not null then other_fixed_costs_value else '0' end
		,sourcingtax = case when budget_usage is not null and  sourcing_tax_value is not null
		then budget_usage * sourcing_tax_value else '0' end
		,ratestax = case when budget_usage is not null and  rates_tax_value is not null
		then budget_usage * rates_tax_value else '0' end
		,account_number
		,rt.rate_name
		,v.vendor_name
		,bd.month_identifier
		,b.budget_id
		,e.entity_name commodity
		,e1.entity_name usagetype
		,e2.entity_name as volumetype
		,Transport_Tariff = case when y.account_id is not null then 'Transport' else 'Tariff' end
		,division_name
	from  budget b
	join    budget_account ba on ba.budget_id = b.budget_id
	join    client cl on cl.client_id = b.client_id
	join budget_details bd on bd.budget_account_id = ba.budget_account_id
	join account acct on acct.account_id = ba.account_id
	join site s on s.site_id = acct.site_id 
	join address addr on addr.address_id = s.primary_address_id 
	join state st on st.state_id = addr.state_id
	join entity e on e.entity_id = b.commodity_type_id
	left join budget_usage bu on bu.account_id = ba.account_id
	and bu.commodity_type_id = b.commodity_type_id
	and bu.month_identifier = bd.month_identifier
	left join entity e1 on e1.entity_id = bu.budget_usage_type_id
	left join entity e2 on e2.entity_id = bu.volume_unit_type_id
	join vendor v on v.vendor_id = acct.vendor_id
	join 
		(select distinct max(r.rate_name) rate_name
				,a.account_id
				from account a 
				join meter m on m.account_id = a.account_id
				join rate r on r.rate_id = m.rate_id and r.commodity_type_id = 290
				group by a.account_id) rt on rt.account_id = acct.account_id
	left join (select acct.account_id
			from supplier_account_meter_map samm
			join meter m on m.meter_id = samm.meter_id
			join contract c on c.contract_id = samm.contract_id
			join account acct on acct.account_id = m.account_id
			join site s on s.site_id = acct.site_id
			join division d on d.division_id = s.division_id
			join client cl on cl.client_id = d.client_id
			where cl.client_id = '141'
			and samm.meter_disassociation_date is null
			and c.commodity_type_id = '290'
			and c.contract_end_date > getdate()
			AND c.contract_type_id = '153')y on y.account_id = ba.account_id
	join division d on d.division_id = s.division_id
--and ba.is_deleted <> '1'
	where b.budget_ID IN ('4807')

)x











GO
