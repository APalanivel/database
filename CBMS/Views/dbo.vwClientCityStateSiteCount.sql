SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE  view [dbo].[vwClientCityStateSiteCount]
AS
	   select d.client_id
		, ad.city
		, ad.state_id
		, count(*) site_count
	     from division d WITH (NOLOCK)
	     join site s  WITH (NOLOCK)on s.division_id = d.division_id
	     join address ad  WITH (NOLOCK)on ad.address_id = s.primary_address_id
	 group by d.client_id
		, ad.city
		, ad.state_id




GO
