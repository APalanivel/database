SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****      
NAME: dbo.lec_DRT_QueueData            
          
DESCRIPTION:              
 used to get Queue Data     
             
INPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
                         
      
OUTPUT PARAMETERS:      
 Name   DataType  Default Description      
------------------------------------------------------------      
          
USAGE EXAMPLES:      
------------------------------------------------------------      
Select * from lec_DRT_QueueData          
          
      
AUTHOR INITIALS:      
 Initials Name      
------------------------------------------------------------      
 AKR   Ashok Kumar Raju      
           
MAINTENANCE LOG:              
Initials Date  Modification       
--- ---------- ----------------------------------------------              
AKR  2012-05-01  Modified according to Standards and Implemented the Additional Data Changes
******/      
      
CREATE VIEW [dbo].[lec_DRT_QueueData]
AS
SELECT
      x.queue_name AS Analyst
     ,getdate() AS Date
     ,sum(x.incoming_private_count_blue) AS [Incoming_Total]
     ,sum(x.incoming_private_count_red) AS [Incoming_Red]
     ,sum(x.variance_count_blue) AS [Variance_Total]
     ,sum(x.variance_count_red) AS [Variance_Red]
     ,sum(x.exception_private_count_blue) AS [Exceptions_Total]
     ,sum(x.exception_private_count_red) AS [Exceptions_Red]
FROM
      ( SELECT
            count(t.cu_invoice_id) AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.cu_exception_denorm t
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
            AND qn.queue_type_id = 243
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,count(t.cu_invoice_id) AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.cu_exception_denorm t
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
            AND getdate() - t.date_in_queue >= 5
            AND qn.queue_type_id = 243
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,count(t.cu_invoice_id) AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.user_info_group_info_map ugm
            JOIN dbo.group_info gi
                  ON gi.group_info_id = ugm.group_info_id
            JOIN dbo.cu_exception_denorm t
                  ON t.queue_id = gi.queue_id
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,count(t.cu_invoice_id) AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.user_info_group_info_map ugm
            JOIN dbo.group_info gi
                  ON gi.group_info_id = ugm.group_info_id
            JOIN dbo.cu_exception_denorm t
                  ON t.queue_id = gi.queue_id
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
            AND getdate() - t.date_in_queue >= 5
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,count(t.cu_invoice_id) AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.cu_exception_denorm t
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 0
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,count(t.cu_invoice_id) AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.cu_exception_denorm t
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 0
            AND getdate() - t.date_in_queue >= 5
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,count(t.cu_invoice_id) AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.user_info_group_info_map ugm
            JOIN dbo.group_info gi
                  ON gi.group_info_id = ugm.group_info_id
            JOIN dbo.cu_exception_denorm t
                  ON t.queue_id = gi.queue_id
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,count(t.cu_invoice_id) AS exception_public_count_red
           ,0 AS variance_count_blue
           ,0 AS variance_count_red
           ,qn.queue_name
        FROM
            dbo.user_info_group_info_map ugm
            JOIN dbo.group_info gi
                  ON gi.group_info_id = ugm.group_info_id
            JOIN dbo.cu_exception_denorm t
                  ON t.queue_id = gi.queue_id
            JOIN dbo.vwcbmsqueuename qn
                  ON qn.queue_id = t.queue_id
        WHERE
            t.is_manual = 1
            AND getdate() - t.date_in_queue >= 5
        GROUP BY
            qn.queue_name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,count(variance_count_blue) AS variance_count_blue
           ,0 AS variance_count_red
           ,k.queue_name
        FROM
            ( SELECT
                  0 AS incoming_private_count_blue
                 ,0 AS incoming_private_count_red
                 ,0 AS incoming_public_count_blue
                 ,0 AS incoming_public_count_red
                 ,0 AS exception_private_count_blue
                 ,0 AS exception_private_count_red
                 ,0 AS exception_public_count_blue
                 ,0 AS exception_public_count_red
                 ,count(1) AS variance_count_blue
                 ,0 AS variance_count_red
                 ,l.queue_name
              FROM
                  dbo.variance_log l
                  INNER JOIN dbo.Commodity com
                        ON l.Commodity_ID = com.Commodity_Id
              WHERE
                  l.Partition_Key IS NULL
                  AND l.is_failure = 1
                  AND com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
              GROUP BY
                  l.Account_ID
                 ,l.Service_Month
                 ,l.Site_ID
                 ,l.Queue_Name ) k
        GROUP BY
            k.Queue_Name
        UNION ALL
        SELECT
            0 AS incoming_private_count_blue
           ,0 AS incoming_private_count_red
           ,0 AS incoming_public_count_blue
           ,0 AS incoming_public_count_red
           ,0 AS exception_private_count_blue
           ,0 AS exception_private_count_red
           ,0 AS exception_public_count_blue
           ,0 AS exception_public_count_red
           ,0 AS variance_count_blue
           ,count(1) AS variance_count_red
           ,m.queue_name
        FROM
            ( SELECT
                  0 AS incoming_private_count_blue
                 ,0 AS incoming_private_count_red
                 ,0 AS incoming_public_count_blue
                 ,0 AS incoming_public_count_red
                 ,0 AS exception_private_count_blue
                 ,0 AS exception_private_count_red
                 ,0 AS exception_public_count_blue
                 ,0 AS exception_public_count_red
                 ,0 AS variance_count_blue
                 ,count(1) AS variance_count_red
                 ,l.queue_name
              FROM
                  dbo.variance_log l
                  INNER JOIN dbo.Commodity com
                        ON l.Commodity_ID = com.Commodity_Id
              WHERE
                  l.Partition_Key IS NULL
                  AND l.is_failure = 1
                  AND com.Commodity_Name IN ( 'Electric Power', 'Natural Gas' )
                  AND getdate() - l.Queue_Dt >= 5
              GROUP BY
                  l.Account_ID
                 ,l.Service_Month
                 ,l.Site_ID
                 ,l.Queue_Name ) m
        GROUP BY
            m.Queue_Name ) x
WHERE
      queue_name IN ( SELECT
                        ui.first_name + space(1) + ui.last_name AS analyst
                      FROM
                        dbo.user_info ui
                        JOIN dbo.user_info_group_info_map uigim
                              ON uigim.user_info_id = ui.user_info_id
                      WHERE
                        uigim.group_info_id = 7
                        AND ui.is_history = 0 )
GROUP BY
      x.queue_name        
        
;
GO
