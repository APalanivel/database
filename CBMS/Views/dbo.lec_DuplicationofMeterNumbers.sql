SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_DuplicationofMeterNumbers]
as
		

select distinct client_name
	,site_name
	,state_name
	,account_number
	,meter_number
	
from client cl
join division d on d.client_id = cl.client_id
join site s on s.division_id = d.division_id
join account acct on acct.site_id = s.site_id
join meter m on m.account_id = acct.account_id
join address addr on addr.address_id = m.address_id
join state st on st.state_id = addr.state_id
where m.meter_number in (

		select --count (distinct meter_id) as NumberofMeters
			
			meter_number as meternumber
		from meter
		group by meter_number
			
	
			having count (distinct meter_id) >1
		--order by count (distinct meter_id)
)
and acct.not_managed = '0'
--order by meter_number
GO
