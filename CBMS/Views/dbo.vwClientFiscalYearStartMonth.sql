SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE  view [dbo].[vwClientFiscalYearStartMonth]
AS
	   select client_id
		, 'start_month' = case e.entity_name
			when 'January' 		then 1
			when 'February' 	then 2
			when 'March' 		then 3
			when 'April' 		then 4
			when 'May' 		then 5
			when 'June' 		then 6
			when 'July' 		then 7
			when 'August' 		then 8
			when 'September' 	then 9
			when 'October' 		then 10
			when 'November' 	then 11
			when 'December' 	then 12
		  end
	     from client c WITH (NOLOCK)
	     join entity e on e.entity_id = c.fiscalyear_startmonth_type_id










GO
