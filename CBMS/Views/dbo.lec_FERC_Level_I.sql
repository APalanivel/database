SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******
NAME:
 dbo.lec_FERC_Level_I

DESCRIPTION:	

 INPUT PARAMETERS:
 Name			DataType	Default		Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name			DataType  Default		Description
------------------------------------------------------------
  
  USAGE EXAMPLES:
------------------------------------------------------------
select top 10 * from dbo.lec_FERC_Level_I

AUTHOR INITIALS:
 Initials	  Name  
------------------------------------------------------------
 BCH		  Balaraju
 AP		  Athmaram Pabbathi

 MODIFICATIONS
 Initials	  Date	    Modification
------------------------------------------------------------
 BCH		  2014-04-17  Replaced cost_usage_site table with cost_usage_site_dtl.
					   Romoved site,division,client,address,state,country tables used core.client_hier instead.
					   Replaced hard coded value 25 with value of MMBTU of UNIT OF GAS
					   Replaced contract type id is 153 with ACCOUNT TYPE is SUPPLIER.
					   Replaced dbo.invoice_participation_site with invoice_participation , dbo.supplier_account_meter_map with core.client_hier_account
 AP		  2014-06-01  Modified sub select used for calculating IP
******/
CREATE VIEW [dbo].[lec_FERC_Level_I]
AS
SELECT
      xy.client_name AS [Client]
     ,xy.client_id AS [ClientID]
     ,xy.entity_name AS [Client Type]
     ,xy.CEM
     ,count(DISTINCT xy.site_id) AS [TotalSites]
     ,count(DISTINCT xy.msid) AS [TotalManagedSites]
     ,count(DISTINCT xy.cussid) AS [SitesWithCostUsage]
     ,sum(xy.ngusage) AS [Usage]
     ,max(xy.NumberServiceMonths) AS MaxServiceMonths
     ,isnull(sum(xy.ipsrcvd) / nullif(sum(xy.ipsexp), 0), 0) AS IP
     ,case WHEN sum(xy.possibleindexpricing) > '0' THEN 'Yes'
           ELSE 'No'
      END AS PossibleIndexPricing
FROM
      ( SELECT
            ch.client_name
           ,ch.client_id
           ,e.entity_name
           ,z.NumberServiceMonths
           ,x.CEM
           ,z.site_id cussid
           ,ch.Sitegroup_Id AS division_name
           ,ch.state_name
           ,ch.city
           ,ch.site_name
           ,ch.site_id
           ,msid = case WHEN ch.Site_Not_Managed <> '1' THEN ch.site_id
                        ELSE NULL
                   END
           ,z.ngusage
           ,v.ipsrcvd
           ,v.ipsexp
           ,PossibleIndexPricing = case WHEN y.contract_id IS NOT NULL
                                             OR u.contract_id IS NOT NULL THEN 1
                                        ELSE 0
                                   END
        FROM
            core.client_hier ch
            LEFT JOIN ( SELECT
                              ngusage = sum(cast(cus.Bucket_Value AS DECIMAL(32, 16)) * cuc.conversion_factor)
                             ,ch.Site_Id
                             ,count(DISTINCT cus.service_month) AS NumberServiceMonths
                        FROM
                              Core.Client_Hier ch
                              JOIN dbo.Cost_Usage_Site_Dtl cus
                                    ON ch.Client_Hier_Id = cus.Client_Hier_Id
                              JOIN dbo.Bucket_Master bm
                                    ON bm.Bucket_Master_Id = cus.Bucket_Master_Id
                              JOIN dbo.Commodity com
                                    ON com.Commodity_Id = bm.Commodity_Id
                              JOIN dbo.ENTITY uom
                                    ON uom.entity_name = 'mmbtu'
                                       AND uom.ENTITY_DESCRIPTION = 'Unit for Gas'
                              JOIN dbo.consumption_unit_conversion cuc
                                    ON cuc.base_unit_id = cus.UOM_Type_Id
                                       AND cuc.converted_unit_id = uom.ENTITY_ID
                        WHERE
                              ( cus.service_month BETWEEN '2014-01-01'
                                                  AND     '2014-12-01' )
                              AND bm.Bucket_Name = 'Total Usage'
                              AND com.Commodity_Name = 'Natural Gas'
                        GROUP BY
                              ch.Site_Id ) z
                  ON ch.Site_Id = z.Site_Id
            JOIN dbo.ENTITY e
                  ON e.ENTITY_ID = ch.Client_Type_Id
            JOIN ( SELECT
                        max(ui.First_Name + space(1) + ui.Last_Name) CEM
                       ,m.Client_Id
                   FROM
                        dbo.client_cem_map m
                        JOIN dbo.user_info ui
                              ON ui.user_info_id = m.user_info_id
                                 AND ui.is_history <> '1'
                   GROUP BY
                        m.Client_Id ) x
                  ON x.client_id = ch.client_id
            LEFT JOIN ( SELECT
                              ipsexp = sum(cast(ip.IS_EXPECTED AS DECIMAL(32, 16)))
                             ,ipsrcvd = sum(cast(ip.IS_RECEIVED AS DECIMAL(32, 16)))
                             ,ip.site_id
                        FROM
                              dbo.INVOICE_PARTICIPATION ip
                              JOIN core.Client_Hier ch
                                    ON ip.SITE_ID = ch.Site_Id
                              JOIN ( SELECT
                                          cha.Account_Id
                                         ,cha.Client_Hier_Id
                                     FROM
                                          Core.Client_Hier_Account cha
                                          JOIN dbo.Commodity com
                                                ON cha.Commodity_Id = com.Commodity_Id
                                     WHERE
                                          com.Commodity_Name = 'Natural Gas'
                                     GROUP BY
                                          cha.Account_Id
                                         ,cha.Client_Hier_Id ) cha
                                    ON ip.Account_Id = cha.Account_Id
                                       AND ch.Client_Hier_Id = cha.Client_Hier_Id
                        WHERE
                              ip.service_month BETWEEN '2014-01-01'
                                               AND     '2014-12-31'
                        GROUP BY
                              ip.site_id ) v
                  ON v.site_id = ch.site_id
            LEFT JOIN ( SELECT
                              c.contract_id
                             ,ch.site_id
                        FROM
                              dbo.[Contract] c
                              JOIN ( SELECT
                                          cha.Supplier_Contract_Id
                                         ,cha.Client_Hier_Id
                                         ,cha.Supplier_Meter_Disassociation_Date
                                     FROM
                                          Core.Client_Hier_Account cha
                                     WHERE
                                          cha.Account_Type = 'Supplier'
                                     GROUP BY
                                          cha.Supplier_Contract_Id
                                         ,cha.Client_Hier_Id
                                         ,cha.Supplier_Meter_Disassociation_Date ) cha
                                    ON cha.Supplier_Contract_Id = c.contract_id
                                       AND ( cha.Supplier_Meter_Disassociation_Date IS NULL
                                             OR cha.Supplier_Meter_Disassociation_Date > c.CONTRACT_START_DATE )
                              JOIN Core.client_hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                              JOIN dbo.Commodity comm
                                    ON comm.Commodity_Id = c.COMMODITY_TYPE_ID
                              JOIN dbo.load_profile_specification lps
                                    ON c.contract_id = lps.contract_id
                              JOIN dbo.price_index p
                                    ON p.price_index_id = lps.price_index_id
                                       AND p.pricing_point <> 'nymex'
                        WHERE
                              ( ( c.contract_start_date BETWEEN '2014-01-01'
                                                        AND     '2014-12-31'
                                  OR c.contract_end_date BETWEEN '2014-01-01'
                                                         AND     '2014-12-31'
                                  OR ( c.contract_start_date < '2014-01-01'
                                       AND c.contract_end_date > '2014-12-31' ) )
                                AND comm.Commodity_Name = 'Natural Gas' )
                        GROUP BY
                              c.contract_id
                             ,ch.site_id ) y
                  ON y.site_id = ch.site_id
            LEFT JOIN ( SELECT
                              c.Contract_ID
                             ,ch.Site_Id
                        FROM
                              dbo.[Contract] c
                              JOIN ( SELECT
                                          cha.Supplier_Contract_Id
                                         ,cha.Client_Hier_Id
                                         ,cha.Supplier_Meter_Disassociation_Date
                                     FROM
                                          Core.Client_Hier_Account cha
                                     WHERE
                                          cha.Account_Type = 'Supplier'
                                     GROUP BY
                                          cha.Supplier_Contract_Id
                                         ,cha.Client_Hier_Id
                                         ,cha.Supplier_Meter_Disassociation_Date ) cha
                                    ON cha.Supplier_Contract_Id = c.contract_id
                                       AND ( cha.Supplier_Meter_Disassociation_Date IS NULL
                                             OR cha.Supplier_Meter_Disassociation_Date > c.CONTRACT_START_DATE )
                              JOIN Core.client_hier ch
                                    ON ch.Client_Hier_Id = cha.Client_Hier_Id
                              JOIN dbo.Commodity com
                                    ON com.Commodity_Id = c.COMMODITY_TYPE_ID
                        WHERE
                              ( ( c.contract_start_date BETWEEN '2014-01-01'
                                                        AND     '2014-12-31'
                                  OR c.contract_end_date BETWEEN '2014-01-01'
                                                         AND     '2014-12-31'
                                  OR ( c.contract_start_date < '2014-01-01'
                                       AND contract_end_date > '2014-12-31' ) )
                                AND ( c.contract_pricing_summary LIKE '%index%'
                                      OR c.contract_pricing_summary LIKE '%algon%'
                                      OR c.contract_pricing_summary LIKE '%anr%'
                                      OR c.contract_pricing_summary LIKE '%appalac%'
                                      OR c.contract_pricing_summary LIKE '%cal%'
                                      OR c.contract_pricing_summary LIKE '%cegt%'
                                      OR c.contract_pricing_summary LIKE '%chicago%'
                                      OR c.contract_pricing_summary LIKE '%cig%'
                                      OR c.contract_pricing_summary LIKE '%cne%'
                                      OR c.contract_pricing_summary LIKE '%dominion%'
                                      OR c.contract_pricing_summary LIKE '%ferc%'
                                      OR c.contract_pricing_summary LIKE '%fgt%'
                                      OR c.contract_pricing_summary LIKE '%gas daily%'
                                      OR c.contract_pricing_summary LIKE '%gdd%'
                                      OR c.contract_pricing_summary LIKE '%hsc%'
                                      OR c.contract_pricing_summary LIKE '%if%'
                                      OR c.contract_pricing_summary LIKE '%mid americ%'
                                      OR c.contract_pricing_summary LIKE '%midam%'
                                      OR c.contract_pricing_summary LIKE '%ngi%'
                                      OR c.contract_pricing_summary LIKE '%nwpl%'
                                      OR c.contract_pricing_summary LIKE '%pepl%'
                                      OR c.contract_pricing_summary LIKE '%pg&E%'
                                      OR c.contract_pricing_summary LIKE '%pge%'
                                      OR c.contract_pricing_summary LIKE '%questar%'
                                      OR c.contract_pricing_summary LIKE '%rg%e%'
                                      OR c.contract_pricing_summary LIKE '%san jua%'
                                      OR c.contract_pricing_summary LIKE '%socal%'
                                      OR c.contract_pricing_summary LIKE '%sonat%'
                                      OR c.contract_pricing_summary LIKE '%southern%'
                                      OR c.contract_pricing_summary LIKE '%southpoint%'
                                      OR c.contract_pricing_summary LIKE '%sumas%'
                                      OR c.contract_pricing_summary LIKE '%tco%'
                                      OR c.contract_pricing_summary LIKE '%tgp%'
                                      OR c.contract_pricing_summary LIKE '%tgt%'
                                      OR c.contract_pricing_summary LIKE '%tn zn%'
                                      OR c.contract_pricing_summary LIKE '%transco%'
                                      OR c.contract_pricing_summary LIKE '%trunk%'
                                      OR c.contract_pricing_summary LIKE '%vanguar%'
                                      OR c.contract_pricing_summary LIKE '%ventura%'
                                      OR c.contract_pricing_summary LIKE '%waha%'
                                      OR c.contract_pricing_summary LIKE '%centerpoint%'
                                      OR c.contract_pricing_summary LIKE '%cgpr%'
                                      OR c.contract_pricing_summary LIKE '%cheyenne%'
                                      OR c.contract_pricing_summary LIKE '%cig%'
                                      OR c.contract_pricing_summary LIKE '%colorado%'
                                      OR c.contract_pricing_summary LIKE '%columbia%'
                                      OR c.contract_pricing_summary LIKE '%Consumers%'
                                      OR c.contract_pricing_summary LIKE '%Dracut%'
                                      OR c.contract_pricing_summary LIKE '%El Paso%'
                                      OR c.contract_pricing_summary LIKE '%Florida%'
                                      OR c.contract_pricing_summary LIKE '%Henry%'
                                      OR c.contract_pricing_summary LIKE '%Houston%'
                                      OR c.contract_pricing_summary LIKE '%Iroquois%'
                                      OR c.contract_pricing_summary LIKE '%Katy%'
                                      OR c.contract_pricing_summary LIKE '%Louisiana%'
                                      OR c.contract_pricing_summary LIKE '%Mich%'
                                      OR c.contract_pricing_summary LIKE '%Midcontinent%'
                                      OR c.contract_pricing_summary LIKE '%Midwest%'
                                      OR c.contract_pricing_summary LIKE '%Natural%'
                                      OR c.contract_pricing_summary LIKE '%Niagara%'
                                      OR c.contract_pricing_summary LIKE '%Northern%'
                                      OR c.contract_pricing_summary LIKE '%Northwest%'
                                      OR c.contract_pricing_summary LIKE '%Oneok%'
                                      OR c.contract_pricing_summary LIKE '%Panhandle%'
                                      OR c.contract_pricing_summary LIKE '%PG&E%'
                                      OR c.contract_pricing_summary LIKE '%Rocky%'
                                      OR c.contract_pricing_summary LIKE '%So Cal%'
                                      OR c.contract_pricing_summary LIKE '%Southern%'
                                      OR c.contract_pricing_summary LIKE '%TCPL%'
                                      OR c.contract_pricing_summary LIKE '%Tenn%'
                                      OR c.contract_pricing_summary LIKE '%Texas%'
                                      OR c.contract_pricing_summary LIKE '%Transwestern%' )
                                AND com.Commodity_Name = 'Natural Gas' )
                        GROUP BY
                              c.contract_id
                             ,ch.site_id ) u
                  ON u.site_id = ch.site_id
        WHERE
            ch.country_name = 'USA'
            AND ch.Client_Not_Managed <> '1'
        GROUP BY
            ch.client_name
           ,ch.client_id
           ,e.entity_name
           ,z.NumberServiceMonths
           ,x.CEM
           ,z.site_id
           ,ch.Sitegroup_Id
           ,ch.state_name
           ,ch.city
           ,ch.site_name
           ,ch.site_id
           ,case WHEN ch.Site_Not_Managed <> '1' THEN ch.site_id
                 ELSE NULL
            END
           ,z.ngusage
           ,v.ipsrcvd
           ,v.ipsexp
           ,case WHEN y.contract_id IS NOT NULL
                      OR u.contract_id IS NOT NULL THEN 1
                 ELSE 0
            END ) xy
GROUP BY
      xy.client_name
     ,xy.entity_name
     ,xy.CEM
     ,xy.client_id
HAVING
      sum(xy.ngusage) > 1800000 
;


GO
