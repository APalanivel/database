SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_Utility_SA_Map]
as

select distinct vendor_name as 'Utility'
	,ui.first_name + ' '+ ui.last_name as 'Gas Analyst'
	,ui1.first_name + ' ' + ui1.last_name as 'Power Analyst'
	,state_name as 'State'
	,region_name as 'Region'
from utility_analyst_map uam
join user_info ui on ui.user_info_id = uam.gas_analyst_id
join user_info ui1 on ui1.user_info_id = uam.power_analyst_id
join utility_detail ud on ud.utility_detail_id = uam.utility_detail_id
join vendor v on v.vendor_Id = ud.vendor_id
join vendor_state_map vsm on vsm.vendor_id = v.vendor_id
join state st on st.state_id = vsm.state_id
join region reg on reg.region_id = st.region_id



GO
