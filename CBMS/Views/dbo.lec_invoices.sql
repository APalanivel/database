SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_invoices]
as	
select   client_name
		,site_name
		, account_number
		, i.invoice_id
		,savings_value
		,savings_id
		,savings_name
		,username
		, site.site_id
		
		
	  from savings s
	join  user_info ui on s.user_info_id= ui.user_info_id
	join  invoice i on s.invoice_id = i.invoice_id
	join  account a on i.account_id = a.account_id
	join  site on a.site_id = site.site_id
	join division d on site.division_id = d.division_id
	join client cl on d.client_id = cl.client_id
GO
