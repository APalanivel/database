SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO













CREATE     VIEW dbo.SR_SAD_CONTRACT_VW
	AS				

	select 	utilacc.account_id,
		DBO.SR_SAD_FN_GET_CONTRACT_ID_OF_ACCOUNT(utilacc.account_id, convert(datetime, convert( varchar(10), getdate(), 101 ))) as contract_id
		--dbo.SR_SAD_FN_GET_CONTRACT_INFO(met.meter_id, convert(datetime, convert( varchar(10), getdate(), 101 ))) as contract_id
	 
	from 	account utilacc(nolock) 
	where 	utilacc.account_type_id = (select entity_id from entity(nolock) where entity_type = 111 and entity_name = 'Utility')









GO
