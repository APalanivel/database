SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create View [dbo].[DMR_Cass_Facility]
as

Select   B.Util_Bill_Header_ID
       , C.SubCustomer_Name
       , F.SubCustomer_ID
       , F.Address_Line1
       , F.Address_Line2
       , F.City   
       , F.State_Code
       , F.ZIPCode
       
From ubm_cass_bill B
       inner join
     ubm_cass_Facility F on B.Site_ID = F.Site_ID
       inner join
     ubm_cass_customer C on F.SubCustomer_ID = C.SubCustomer_ID
Group By
         B.Util_Bill_Header_ID
       , C.SubCustomer_Name
       , F.SubCustomer_ID
       , F.Address_Line1
       , F.Address_Line2
       , F.City   
       , F.State_Code
       , F.ZIPCode


GO
