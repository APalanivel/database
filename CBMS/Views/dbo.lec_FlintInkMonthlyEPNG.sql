SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
 dbo.[lec_FlintInkMonthlyEPNG

DESCRIPTION:	

 INPUT PARAMETERS:
 Name			DataType	Default		Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name			DataType  Default		Description
------------------------------------------------------------
  
  USAGE EXAMPLES:
------------------------------------------------------------
select top 10 * from dbo.lec_FlintInkMonthlyEPNG

AUTHOR INITIALS:
 Initials	Name  
------------------------------------------------------------
 BCH		Balaraju

 MODIFICATIONS
 Initials	Date			Modification
------------------------------------------------------------
 BCH		2012-04-18	    Replaced cost_usage_site table with cost_usage_site_dtl and account table with core.client_hier_account.
							Replaced dbo.invoice_participation_site table with dbo.invoice_participation
							Used DEMAND Bucket of ELECTRIC POWER id instead of el_actual_demand_uom_type_id
							Used BILLED DEMAND Bucket of ELECTRIC POWER id instead of el_billing_demand_uom_type_id
							Used CONTRACT DEMAND Bucket of ELECTRIC POWER id instead of el_contract_demand_uom_type_id
							Used KW of Unit for Electricity id instead of 11.
							

******/
CREATE VIEW [dbo].[lec_FlintInkMonthlyEPNG]
AS
SELECT
      y.site_name AS 'Site'
     ,y.division_name AS 'Division'
     ,y.service_month AS 'ServiceMonth'
     ,y.averagebdep AS '# days in service period EP'
     ,y.epusage AS 'TotalEPUsage(kWh)'
     ,y.elcost AS 'TotalEPCost(USD$)'
     ,'UnitEPCost_$/kWh' = case WHEN y.epusage <> '0'
                                     AND y.elcost <> '0' THEN y.elcost / y.epusage
                                ELSE '0'
                           END
     ,y.EPComplete
     ,y.averagebdng AS '# days in service period NG'
     ,y.ngusage AS 'TotalNGUsage(Dth)'
     ,y.ngcost AS 'TotalNGCost(USD$)'
     ,'UnitNGCost_$/Dth' = case WHEN y.ngusage <> '0'
                                     AND y.ngcost <> '0' THEN y.ngcost / y.ngusage
                                ELSE '0'
                           END
     ,y.NGComplete
FROM
      ( SELECT
            x.site_name
           ,x.service_month
           ,x.averagebdng
           ,x.averagebdep
           ,x.division_name
           ,x.city
           ,sum(x.ngindusage) NGUsage
           ,sum(x.elindusage) EPUsage
           ,sum(x.el_inddemand) ELDemand
           ,sum(x.ng_ind_cost) NGCost
           ,sum(x.el_ind_cost) ELCost
           ,sum(x.ip_ind_rcvd) IPRcvd
           ,sum(x.ip_ind_exp) IPExp
           ,x.ng_is_complete AS NGComplete
           ,x.el_is_complete AS EPComplete
        FROM
            ( SELECT
                  y.client_name
                 ,y.service_month
                 ,y.division_name
                 ,y.country_name
                 ,y.state_name
                 ,y.city
                 ,y.site_name
                 ,y.ngindusage
                 ,y.elindusage
                 ,el_inddemand = case WHEN ( y.el_indactualdemand ) <> '0' THEN el_indactualdemand
                                      WHEN y.el_indbillingdemand <> '0' THEN el_indbillingDemand
                                      WHEN y.el_indcontractdemand <> '0' THEN el_indcontractdemand
                                      ELSE '0'
                                 END
                 ,y.ng_ind_cost
                 ,y.el_ind_cost
                 ,ip_ind_exp = cast (y.ip_ind_exp AS DECIMAL(32, 16))
                 ,ip_ind_rcvd = cast (y.ip_ind_rcvd AS DECIMAL(32, 16))
                 ,max(y.ng_is_complete) AS ng_is_complete
                 ,max(y.el_is_complete) AS el_is_complete
                 ,y.averagebdng
                 ,y.averagebdep
              FROM
                  ( SELECT
                        ch.client_name
                       ,cus.service_month
                       ,ch.Sitegroup_Name AS division_name
                       ,ch.country_name
                       ,ch.state_name
                       ,ch.city
                       ,ch.site_name
                       ,ngindusage = case WHEN ( case WHEN c.Commodity_Name = 'Natural Gas'
                                                           AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                 END ) <> '0' THEN ( ( case WHEN c.Commodity_Name = 'Natural Gas'
                                                                                 AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                                       END ) * cuc.conversion_factor )
                                          ELSE '0'
                                     END
                       ,elindusage = case WHEN ( case WHEN c.Commodity_Name = 'Electric Power'
                                                           AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                 END ) <> '0' THEN ( ( case WHEN c.Commodity_Name = 'Electric Power'
                                                                                 AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                                       END ) * cucep.conversion_factor )
                                          ELSE '0'
                                     END
                       ,el_indactualdemand = ( SELECT
                                                sum(cast(cu.Bucket_Value * cucead.conversion_factor AS DECIMAL(32, 16))) AS el_actualdemand
                                               FROM
                                                dbo.Cost_Usage_Account_Dtl cu
                                                JOIN dbo.ENTITY kwuom
                                                      ON kwuom.ENTITY_NAME = 'kw'
                                                         AND kwuom.ENTITY_DESCRIPTION = 'Unit for electricity'
                                                JOIN dbo.Bucket_Master bm
                                                      ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
                                                JOIN dbo.Commodity com
                                                      ON com.Commodity_Id = bm.Commodity_Id
                                                JOIN dbo.consumption_unit_conversion cucead
                                                      ON cucead.base_unit_id = cu.UOM_Type_Id
                                                         AND cucead.converted_unit_id = kwuom.ENTITY_ID
                                               WHERE
                                                cu.Client_Hier_Id = cus.Client_Hier_Id
                                                AND cu.service_month = cus.service_month
                                                AND bm.Bucket_Name = 'Demand'
                                                AND com.Commodity_name = 'Electric Power' )
                       ,el_indbillingdemand = ( SELECT
                                                      sum(cast(cu.Bucket_Value * cucebd.conversion_factor AS DECIMAL(32, 16))) AS el_billingdemand
                                                FROM
                                                      dbo.Cost_Usage_Account_Dtl cu
                                                      JOIN dbo.ENTITY kwuom
                                                            ON kwuom.ENTITY_NAME = 'kw'
                                                               AND kwuom.ENTITY_DESCRIPTION = 'Unit for electricity'
                                                      JOIN dbo.Bucket_Master bm
                                                            ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = bm.Commodity_Id
                                                      JOIN dbo.consumption_unit_conversion cucebd
                                                            ON cucebd.base_unit_id = cu.UOM_Type_Id
                                                               AND cucebd.converted_unit_id = kwuom.ENTITY_ID
                                                WHERE
                                                      cu.Client_Hier_Id = cus.Client_Hier_Id
                                                      AND cu.service_month = cus.service_month
                                                      AND bm.Bucket_Name = 'Billed Demand'
                                                      AND com.Commodity_name = 'Electric Power' )
                       ,el_indcontractdemand = ( SELECT
                                                      sum(cast(cu.Bucket_Value * cucecd.conversion_factor AS DECIMAL(32, 16))) AS el_contractdemand
                                                 FROM
                                                      dbo.Cost_Usage_Account_Dtl cu
                                                      JOIN dbo.ENTITY kwuom
                                                            ON kwuom.ENTITY_NAME = 'kw'
                                                               AND kwuom.ENTITY_DESCRIPTION = 'Unit for electricity'
                                                      JOIN dbo.Bucket_Master bm
                                                            ON bm.Bucket_Master_Id = cu.Bucket_Master_Id
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = bm.Commodity_Id
                                                      JOIN dbo.consumption_unit_conversion cucecd
                                                            ON cucecd.base_unit_id = cu.UOM_Type_Id
                                                               AND cucecd.converted_unit_id = kwuom.ENTITY_ID
                                                 WHERE
                                                      cu.Client_Hier_Id = cus.Client_Hier_Id
                                                      AND cu.service_month = cus.service_month
                                                      AND bm.Bucket_Name = 'Contract Demand'
                                                      AND com.Commodity_name = 'Electric Power' )
                       ,ng_ind_cost = cast(cucUSD.conversion_factor * ( ( case WHEN cha.account_type = 'Utility'
                                                                                    AND c.Commodity_Name = 'Natural Gas'
                                                                                    AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                          END ) + ( case WHEN cha.account_type = 'Supplier'
                                                                                              AND c.Commodity_Name = 'Natural Gas'
                                                                                              AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                                    END ) ) AS DECIMAL(32, 16))
                       ,el_ind_cost = cast(cucUSD.conversion_factor * ( ( case WHEN cha.account_type = 'Utility'
                                                                                    AND c.Commodity_Name = 'Electric Power'
                                                                                    AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                          END ) + ( case WHEN cha.account_type = 'Supplier'
                                                                                              AND c.Commodity_Name = 'Electric Power'
                                                                                              AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                                    END ) ) AS DECIMAL(32, 16))
                       ,ip_ind_exp = sum(cast(ips.IS_EXPECTED AS DECIMAL(32, 16)))
                       ,ip_ind_rcvd = sum(cast(ips.IS_RECEIVED AS DECIMAL(32, 16)))
                       ,ng_is_complete = case WHEN co.commodity_name = 'Natural Gas' THEN case WHEN sum(case WHEN ips.is_expected = 1 THEN 1
                                                                                                             ELSE 0
                                                                                                        END) = sum(case WHEN ips.is_expected = 1
                                                                                                                             AND ips.is_received = 1 THEN 1
                                                                                                                        ELSE 0
                                                                                                                   END) THEN 'Yes'
                                                                                               ELSE 'No'
                                                                                          END
                                         END
                       ,el_is_complete = case WHEN co.commodity_name = 'Electric Power' THEN case WHEN sum(case WHEN ips.is_expected = 1 THEN 1
                                                                                                                ELSE 0
                                                                                                           END) = sum(case WHEN ips.is_expected = 1
                                                                                                                                AND ips.is_received = 1 THEN 1
                                                                                                                           ELSE 0
                                                                                                                      END) THEN 'Yes'
                                                                                                  ELSE 'No'
                                                                                             END
                                         END
                       ,z.averagebdng
                       ,z.averagebdep
                    FROM
                        dbo.Cost_Usage_Site_Dtl cus
                        JOIN Core.Client_Hier ch
                              ON cus.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT JOIN ( dbo.Bucket_Master bm
                                    JOIN dbo.Commodity com
                                          ON com.Commodity_Id = bm.Commodity_Id
                                    JOIN dbo.ENTITY Dthuom
                                          ON Dthuom.ENTITY_NAME = 'Dth'
                                             AND Dthuom.ENTITY_DESCRIPTION = 'Unit for Gas'
                                    JOIN dbo.consumption_unit_conversion cuc
                                          ON cuc.converted_unit_id = Dthuom.ENTITY_ID )
                                    ON cuc.base_unit_id = cus.UOM_Type_Id
                                       AND bm.Bucket_Master_Id = cus.Bucket_Master_Id
                                       AND bm.Bucket_Name = 'Total Usage'
                                       AND com.Commodity_Name = 'Natural Gas'
                        LEFT JOIN ( dbo.Bucket_Master bmr
                                    JOIN dbo.Commodity comm
                                          ON comm.Commodity_Id = bmr.Commodity_Id
                                    JOIN dbo.ENTITY kWhuom
                                          ON kWhuom.ENTITY_NAME = 'kWh'
                                             AND kWhuom.ENTITY_DESCRIPTION = 'Unit for electricity'
                                    JOIN consumption_unit_conversion cucep
                                          ON cucep.converted_unit_id = kWhuom.ENTITY_ID )
                                    ON bmr.Bucket_Master_Id = cus.Bucket_Master_Id
                                       AND cucep.base_unit_id = cus.UOM_Type_Id
                                       AND bm.Bucket_Name = 'Total Usage'
                                       AND com.Commodity_Name = 'Natural Gas'
                        LEFT JOIN Core.Client_Hier_Account cha
                              ON cha.Client_Hier_Id = ch.Client_Hier_Id
                        LEFT JOIN dbo.Commodity c
                              ON c.Commodity_Id = cha.Commodity_Id
                        LEFT JOIN dbo.Bucket_Master bmm
                              ON bmm.Bucket_Master_Id = cus.Bucket_Master_Id
                                 AND bmm.Bucket_Name IN ( 'total Cost', 'Total Usage' )
                        LEFT JOIN dbo.CURRENCY_UNIT usdCur
                              ON usdCur.CURRENCY_UNIT_NAME = 'USD'
                        LEFT JOIN currency_unit_conversion cucUSD
                              ON ( cucUSD.currency_group_id = ch.Client_Currency_Group_Id
                                   AND cucUSD.conversion_date = cus.service_month
                                   AND cucUSD.converted_unit_id = usdCur.CURRENCY_UNIT_ID
                                   AND cucUSD.base_unit_id = cus.currency_unit_id )
                        LEFT JOIN ( dbo.invoice_participation ips
                                    JOIN Core.Client_Hier cch
                                          ON ips.SITE_ID = cch.Site_Id
                                    JOIN Core.Client_Hier_Account ccha
                                          ON cch.Client_Hier_Id = ccha.Client_Hier_Id
                                    JOIN dbo.Commodity co
                                          ON ccha.Commodity_Id = co.Commodity_Id )
                                    ON ips.site_id = ch.site_id
                                       AND ips.service_month = cus.service_month
                                       AND co.Commodity_Name IN ( 'Natural Gas', 'Electric Power' )
                        LEFT JOIN ( SELECT
                                          a.site_id
                                         ,sum(a.abdng) averagebdng
                                         ,sum(a.abdep) averagebdep
                                         ,a.service_month
                                    FROM
                                          ( SELECT
                                                y.site_id
                                               ,avg(abdng) abdng
                                               ,'0' abdep
                                               ,y.service_month
                                            FROM
                                                ( SELECT
                                                      chr.site_id
                                                     ,avg(cuism.billing_days) abdng
                                                     ,'0' abdep
                                                     ,cuism.service_month
                                                  FROM
                                                      cu_invoice cui
                                                      JOIN cu_invoice_service_month cuism
                                                            ON cuism.cu_invoice_id = cui.cu_invoice_id
                                                      JOIN Core.Client_Hier_Account cha
                                                            ON cha.Account_Id = cuism.Account_ID
                                                      JOIN Core.Client_Hier chr
                                                            ON chr.Client_Hier_Id = cha.Client_Hier_Id
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = cha.Commodity_Id
                                                  WHERE
                                                      cui.is_reported = '1'
                                                      AND cui.is_processed = '1'
                                                      AND is_duplicate <> '1'
                                                      AND chr.client_name = 'Flint Group'
                                                      AND cha.Account_Type = 'Utility'
                                                      AND cuism.service_month BETWEEN '2009-09-01'
                                                                              AND     '2012-01-01'
                                                      AND com.commodity_name = 'Natural Gas'
                                                  GROUP BY
                                                      chr.site_id
                                                     ,cuism.SERVICE_MONTH
                                                  UNION
                                                  SELECT
                                                      ch.site_id
                                                     ,avg(cuism.billing_days) abdng
                                                     ,'0' abdep
                                                     ,cuism.service_month
                                                  FROM
                                                      dbo.cu_invoice cui
                                                      JOIN dbo.cu_invoice_service_month cuism
                                                            ON cuism.cu_invoice_id = cui.cu_invoice_id
                                                      JOIN Core.Client_Hier_Account cha
                                                            ON cha.Account_Id = cuism.Account_ID
                                                      JOIN Core.Client_Hier ch
                                                            ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = cha.Commodity_Id
                                                  WHERE
                                                      cui.is_reported = '1'
                                                      AND cui.is_processed = '1'
                                                      AND cui.is_duplicate <> '1'
                                                      AND ch.client_name = 'Flint Group'
                                                      AND cha.Account_Type = 'Supplier'
                                                      AND cuism.service_month BETWEEN '2009-09-01'
                                                                              AND     '2012-01-01'
                                                      AND com.commodity_name = 'Natural Gas'
                                                  GROUP BY
                                                      ch.site_id
                                                     ,cuism.service_month ) y
                                            GROUP BY
                                                y.site_id
                                               ,y.service_month
                                            UNION
                                            SELECT
                                                z.site_id
                                               ,'0' abdng
                                               ,avg(z.abdep) abdep
                                               ,z.service_month
                                            FROM
                                                ( SELECT
                                                      ch.site_id
                                                     ,'0' abdng
                                                     ,avg(cuism.billing_days) abdep
                                                     ,cuism.service_month
                                                  FROM
                                                      dbo.cu_invoice cui
                                                      JOIN dbo.cu_invoice_service_month cuism
                                                            ON cuism.cu_invoice_id = cui.cu_invoice_id
                                                      JOIN Core.Client_Hier_Account cha
                                                            ON cha.Account_Id = cuism.Account_ID
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = cha.Commodity_Id
                                                      JOIN Core.Client_Hier ch
                                                            ON ch.Client_Hier_Id = cha.Client_Hier_Id
                                                  WHERE
                                                      cui.is_reported = '1'
                                                      AND cui.is_processed = '1'
                                                      AND is_duplicate <> '1'
                                                      AND ch.client_name = 'Flint Group'
                                                      AND cha.Account_Type = 'Utility'
                                                      AND cuism.service_month BETWEEN '2009-09-01'
                                                                              AND     '2012-01-01'
                                                      AND com.commodity_name = 'Electric Power'
                                                  GROUP BY
                                                      ch.site_id
                                                     ,cuism.service_month
                                                  UNION
                                                  SELECT
                                                      cch.site_id
                                                     ,'0' abdng
                                                     ,avg(cuism.billing_days) abdep
                                                     ,cuism.service_month
                                                  FROM
                                                      dbo.cu_invoice cui
                                                      JOIN dbo.cu_invoice_service_month cuism
                                                            ON cuism.cu_invoice_id = cui.cu_invoice_id
                                                      JOIN Core.Client_Hier_Account cha
                                                            ON cha.Account_Id = cuism.Account_ID
                                                      JOIN dbo.Commodity com
                                                            ON com.Commodity_Id = cha.Commodity_Id
                                                      JOIN Core.Client_Hier cch
                                                            ON cch.Client_Hier_Id = cha.Client_Hier_Id
                                                  WHERE
                                                      cui.is_reported = '1'
                                                      AND cui.is_processed = '1'
                                                      AND is_duplicate <> '1'
                                                      AND cch.client_name = 'Flint Group'
                                                      AND cha.Account_Type = 'Supplier'
                                                      AND cuism.service_month BETWEEN '2009-09-01'
                                                                              AND     '2012-01-01'
                                                      AND com.commodity_name = 'Electric Power'
                                                  GROUP BY
                                                      cch.site_id
                                                     ,cuism.service_month ) z
                                            GROUP BY
                                                z.site_id
                                               ,z.service_month ) a
                                    GROUP BY
                                          a.site_id
                                         ,a.service_month ) z
                              ON z.site_id = ch.site_id
                                 AND z.service_month = cus.service_month
                    WHERE
                        ch.client_name = 'Flint Group'
                        AND cus.service_month BETWEEN '2009-09-01'
                                              AND     '2012-01-01'
                        AND ch.Site_Not_Managed <> '1'
                        AND ch.Site_Closed <> '1'
                        AND ch.region_id <> '6'
                        AND cha.Account_Type = 'Utility'
                    GROUP BY
                        ch.client_name
                       ,cus.service_month
                       ,ch.Sitegroup_Name
                       ,ch.country_name
                       ,ch.state_name
                       ,ch.city
                       ,ch.site_name
                       ,case WHEN ( case WHEN c.Commodity_Name = 'Natural Gas'
                                              AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                    END ) <> '0' THEN ( ( case WHEN c.Commodity_Name = 'Natural Gas'
                                                                    AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                          END ) * cuc.conversion_factor )
                             ELSE '0'
                        END
                       ,case WHEN ( case WHEN c.Commodity_Name = 'Electric Power'
                                              AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                    END ) <> '0' THEN ( ( case WHEN c.Commodity_Name = 'Electric Power'
                                                                    AND bmm.Bucket_Name = 'Total Usage' THEN cus.bucket_value
                                                          END ) * cucep.conversion_factor )
                             ELSE '0'
                        END
                       ,cast(cucUSD.conversion_factor * ( ( case WHEN cha.account_type = 'Utility'
                                                                      AND c.Commodity_Name = 'Natural Gas'
                                                                      AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                            END ) + ( case WHEN cha.account_type = 'Supplier'
                                                                                AND c.Commodity_Name = 'Natural Gas'
                                                                                AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                      END ) ) AS DECIMAL(32, 16))
                       ,cast(cucUSD.conversion_factor * ( ( case WHEN cha.account_type = 'Utility'
                                                                      AND c.Commodity_Name = 'Electric Power'
                                                                      AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                            END ) + ( case WHEN cha.account_type = 'Supplier'
                                                                                AND c.Commodity_Name = 'Electric Power'
                                                                                AND bmm.Bucket_Name = 'Total Cost' THEN cus.bucket_value
                                                                      END ) ) AS DECIMAL(32, 16))
                       ,ips.SITE_ID
                       ,ips.SERVICE_MONTH
                       ,co.commodity_name
                       ,z.averagebdng
                       ,z.averagebdep
                       ,cus.Client_Hier_Id ) y
              GROUP BY
                  y.client_name
                 ,y.service_month
                 ,y.division_name
                 ,y.country_name
                 ,y.state_name
                 ,y.city
                 ,y.site_name
                 ,y.ngindusage
                 ,y.elindusage
                 ,case WHEN ( y.el_indactualdemand ) <> '0' THEN el_indactualdemand
                       WHEN y.el_indbillingdemand <> '0' THEN el_indbillingDemand
                       WHEN y.el_indcontractdemand <> '0' THEN el_indcontractdemand
                       ELSE '0'
                  END
                 ,y.ng_ind_cost
                 ,y.el_ind_cost
                 ,cast (y.ip_ind_exp AS DECIMAL(32, 16))
                 ,cast (y.ip_ind_rcvd AS DECIMAL(32, 16))
                 ,y.averagebdng
                 ,y.averagebdep ) x
        GROUP BY
            x.site_name
           ,x.service_month
           ,x.averagebdng
           ,x.averagebdep
           ,x.division_name
           ,x.city
           ,x.ng_is_complete
           ,x.el_is_complete ) y
GROUP BY
      y.site_name
     ,y.division_name
     ,y.service_month
     ,y.averagebdep
     ,y.epusage
     ,y.elcost
     ,case WHEN y.epusage <> '0'
                AND y.elcost <> '0' THEN y.elcost / y.epusage
           ELSE '0'
      END
     ,y.EPComplete
     ,y.averagebdng
     ,y.ngusage
     ,y.ngcost
     ,case WHEN y.ngusage <> '0'
                AND y.ngcost <> '0' THEN y.ngcost / y.ngusage
           ELSE '0'
      END
     ,y.NGComplete
;
GO
