SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_dntlist]
as
select distinct 
	client_name
	,client_city
	,state_name
	,account_number
	,vendor_name
	,date_added
	,ubm.ubm_name
	,entity_name
from do_not_track dnt 
left join state st on dnt.state_id = st.state_id
left join ubm on dnt.ubm_id = ubm.ubm_id
left join entity e on dnt.reason_type_id = e.entity_id


GO
