SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
 dbo.lec_VENComments

DESCRIPTION:	

 INPUT PARAMETERS:
 Name			DataType	Default		Description
------------------------------------------------------------
 
 OUTPUT PARAMETERS:
 Name			DataType  Default		Description
------------------------------------------------------------
  
  USAGE EXAMPLES:
------------------------------------------------------------
select top 10 * from dbo.lec_VENComments

AUTHOR INITIALS:
 Initials	Name  
------------------------------------------------------------
 BCH		Balaraju

 MODIFICATIONS
 Initials	Date		    Modification
------------------------------------------------------------
 BCH		2012-04-17    Addnl Data Changes
					   -- Replaced cost_usage table with cost_usage_account_dtl and account table with core.client_hier_account, Core.Client_Hier
					   -- Replaced Cost_Usage_Comment_Map with Cost_Usage_Account_Dtl_Comment_Map

******/
CREATE VIEW [dbo].[lec_VENComments]
AS
SELECT
      com.Comment_Dt [Comment Date]
     ,ui.FIRST_NAME + ' ' + ui.LAST_NAME [Comment By]
     ,vl.queue_name [Queue]
     ,vl.Execution_Dt [Variance Run Date]
     ,cuad.SERVICE_MONTH [Month]
     ,vl.Client_Name [Client]
     ,vl.Site_Name [Site]
     ,vl.State_Name [State]
     ,cha.ACCOUNT_NUMBER [Account Number]
     ,vl.Vendor_name [Vendor]
     ,com.Comment_Text [Comments]
FROM
      dbo.Variance_Log vl
      JOIN Core.Client_Hier ch
            ON ch.Site_Id = vl.Site_Id
      JOIN core.Client_Hier_Account cha
            ON cha.Account_Id = vl.Account_ID
               AND cha.Client_Hier_Id = ch.Client_Hier_Id
               AND cha.Commodity_Id = vl.Commodity_Id
      JOIN dbo.Cost_Usage_Account_Dtl cuad
            ON cuad.Client_Hier_Id = cha.Client_Hier_Id
               AND cuad.Account_Id = cha.Account_Id
               AND vl.Service_Month = cuad.Service_Month
      JOIN dbo.Cost_Usage_Account_Dtl_Comment_Map ccm
            ON ccm.Account_Id = cuad.Account_Id
               AND ccm.Commodity_Id = cha.Commodity_Id
               AND ccm.Service_Month = cuad.Service_Month
      JOIN dbo.Comment com
            ON com.Comment_ID = ccm.Comment_ID
      JOIN dbo.USER_INFO ui
            ON ui.USER_INFO_ID = com.Comment_User_Info_Id
WHERE
      ( com.Comment_Text LIKE '%ven sent%'
        OR com.Comment_Text LIKE '%sent ven%'
        OR com.comment_text LIKE '%ven send%' )
      AND vl.Is_Failure = 1
      AND com.comment_dt < getdate() - 22
      AND Partition_Key IS NULL
GROUP BY
      com.Comment_Dt
     ,vl.Execution_Dt
     ,cuad.SERVICE_MONTH
     ,vl.Client_Name
     ,vl.Site_Name
     ,vl.State_Name
     ,com.Comment_Text
     ,ui.FIRST_NAME
     ,ui.LAST_NAME
     ,cha.ACCOUNT_NUMBER
     ,vl.Vendor_name
     ,vl.queue_name
;
GO
