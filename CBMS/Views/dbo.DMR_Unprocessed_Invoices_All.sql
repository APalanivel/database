SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create  View [dbo].[DMR_Unprocessed_Invoices_All]
as
Select  Invoice_Identifier, CBMS_Image_ID, UBM_Account_ID, UBM_Account_Number, UBM_Vendor_Name, UI.UBM_BATCH_MASTER_LOG_ID, L.START_DATE
From UBM_Invoice UI
       inner join
     UBM_Batch_Master_Log L on UI.UBM_Batch_master_Log_ID = L.UBM_Batch_Master_Log_id
       inner join
     UBM_Invoice_Details UID on UI.UBM_Invoice_ID = UID.UBM_Invoice_ID
Where is_processed = 0
  And UBM_Client_ID Is Null
  And L.Start_Date > Convert(varchar(10), GetDate()-365, 101)
  And (Item_Name Like '%Demand%'
       or Item_Name like '%Elect%'
       or Item_Name like '%GAS%')
Group By  Invoice_Identifier, CBMS_Image_ID, UBM_Account_ID, UBM_Account_Number, UBM_Vendor_Name, UI.UBM_BATCH_MASTER_LOG_ID, L.START_DATE




GO
