SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create  view
[dbo].[LEC_AlternateFuelSites]
as

select Client_name as Client
	,s.site_id as SiteID
	,address_line1 as AddressLine1
	,city as City
	,state_name as State
	,region_name as Region
	,e.entity_name as AlternateFuelType
from site s
left join alternate_fuel af on s.site_id = af.site_id
join division d on s.division_id = d.division_id
join client cl on d.client_id = cl.client_id
join address addr on s.primary_address_id = addr.address_id
join state st on addr.state_id = st.state_id
join region r on st.region_id = r.region_id
left join entity e on af.alternate_fuel_type_id = e.entity_id

where 
--region_name = 'se'
is_alternate_gas = 1
--and entity_type = 116



GO
