SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   view
[dbo].[LEC_RollOut_072806]
as

select distinct 
	ClientName
	,DivisionName
	,sn.site_name as 'SiteView'
	--,SiteID
	--,SiteCreated
	--,City
	--,State_Province
	--,Region
	,CommodityType
	,Utility
	,AccountNumber
	--,AccountCreated
	,MeterNumber
	,PurchaseMethod
	,ContractType
	,ContractedVendor
	,ContractNumber
	--,ContractCreatedDate as ContractCreated
from dbo.cbms_joinedfile_qd_072505 jf
join dbo.vwsitename sn on sn.site_id = jf.siteid




GO
