SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view
[dbo].[lec_ActiveData]
as

select getdate() as Today
	,count (distinct cl.client_id) as ActiveClients
	,count (distinct s.site_id) as ActiveSites
	,count (distinct acct.account_id) as ActiveUtilityAccounts
	,count (distinct m.meter_id) as ActiveMeters
	,count (distinct v.vendor_id) as ActiveUtilities
	,count (distinct r.rate_id) as ActiveRates
from 
	client cl
	join division d on d.client_id = cl.client_id
	join site s on s.division_id = d.division_id
	join account acct on acct.site_id = s.site_id				
	join meter m on m.account_id = acct.account_id
	join vendor v on v.vendor_id = acct.vendor_id
	join rate r on r.rate_id = m.rate_id

where   cl.not_managed = '0'
	and s.not_managed = '0'
	and acct.not_managed = '0'
	
	
GO
