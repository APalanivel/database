CREATE SERVICE [//Change_Control/Service/CBMS/Security_Role_Transfer]
AUTHORIZATION [sb_CBMS_Service]
ON QUEUE [dbo].[Security_Role_Transfer_Queue]
(
[//Change_Control/Contract/Security_Role_Transfer]
)
GO
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/Security_Role_Transfer] TO [sb_DV2_Service]
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/Security_Role_Transfer] TO [sb_DVDEHub_Service]
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/Security_Role_Transfer] TO [sb_SV_Service]
GO
