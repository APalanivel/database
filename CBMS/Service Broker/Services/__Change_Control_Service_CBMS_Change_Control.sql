CREATE SERVICE [//Change_Control/Service/CBMS/Change_Control]
AUTHORIZATION [sb_CBMS_Service]
ON QUEUE [dbo].[Change_Control_Queue]
(
[//Change_Control/Contract/Client_Hier_Change],
[//Change_Control/Contract/Table_Change]
)
GO
