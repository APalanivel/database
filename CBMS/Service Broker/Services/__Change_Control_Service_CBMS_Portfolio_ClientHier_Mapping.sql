CREATE SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]
AUTHORIZATION [dbo]
ON QUEUE [dbo].[Portfolio_ClientHier_Management_Queue]
(
[//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio],
[//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio]
)
GO
