CREATE SERVICE [//Change_Control/Service/CBMS/User_Info_Transfer]
AUTHORIZATION [sb_CBMS_Service]
ON QUEUE [dbo].[User_Info_Transfer_Queue]
(
[//Change_Control/Contract/Client_App_Access_Role_Transfer],
[//Change_Control/Contract/Single_Signon_User_Map_Transfer],
[//Change_Control/Contract/Sr_Supplier_Contact_Info_Transfer],
[//Change_Control/Contract/User_Info_Client_App_Access_Role_Map_Transfer],
[//Change_Control/Contract/User_Info_Group_Info_Map_Transfer],
[//Change_Control/Contract/User_Info_Transfer]
)
GO
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/User_Info_Transfer] TO [sb_DV2_Service]
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/User_Info_Transfer] TO [sb_DVDEHub_Service]
GRANT CONTROL ON SERVICE:: [//Change_Control/Service/CBMS/User_Info_Transfer] TO [sb_SV_Service]
GO
