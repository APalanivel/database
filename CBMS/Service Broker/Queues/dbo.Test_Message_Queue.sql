CREATE QUEUE [dbo].[Test_Message_Queue] 
WITH STATUS=ON, 
RETENTION=OFF, 
ACTIVATION (
STATUS=ON, 
PROCEDURE_NAME=[dbo].[Transmission_Receive_Messages_For_Test_Message_Queue], 
MAX_QUEUE_READERS=1, 
EXECUTE AS N'sb_CBMS_Service'
)
ON [DB_DATA01]
GO
CREATE EVENT NOTIFICATION [DisabledTargetQueueNotification]
ON QUEUE [dbo].[Test_Message_Queue]
FOR BROKER_QUEUE_DISABLED
TO SERVICE 'DisabledQueueNotificationService', 'C82B6052-0805-41E6-8228-785BBA94B399'
GO
