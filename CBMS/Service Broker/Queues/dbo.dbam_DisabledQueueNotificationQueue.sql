CREATE QUEUE [dbo].[dbam_DisabledQueueNotificationQueue] 
WITH STATUS=ON, 
RETENTION=OFF, 
ACTIVATION (
STATUS=ON, 
PROCEDURE_NAME=[DBAM].[ServiceBroker_Send_DisabledQueueEmailNotification], 
MAX_QUEUE_READERS=1, 
EXECUTE AS OWNER
)
ON [DB_DATA01]
GO
