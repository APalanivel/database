CREATE QUEUE [dbo].[Security_Role_Transfer_Queue] 
WITH STATUS=ON, 
RETENTION=OFF
ON [DB_DATA01]
CREATE EVENT NOTIFICATION [DisabledTargetQueueNotification]
ON QUEUE [dbo].[Security_Role_Transfer_Queue]
FOR BROKER_QUEUE_DISABLED
TO SERVICE 'DisabledQueueNotificationService', 'C82B6052-0805-41E6-8228-785BBA94B399'

GO
