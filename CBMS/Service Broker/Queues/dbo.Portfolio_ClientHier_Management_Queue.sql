CREATE QUEUE [dbo].[Portfolio_ClientHier_Management_Queue] 
WITH STATUS=ON, 
RETENTION=OFF, 
ACTIVATION (
STATUS=ON, 
PROCEDURE_NAME=[dbo].[Change_Control_Receive_Messages_For_Portfolio_ClientHier_Management_Queue], 
MAX_QUEUE_READERS=1, 
EXECUTE AS OWNER
)
ON [DB_DATA01]
CREATE EVENT NOTIFICATION [DisabledTargetQueueNotification]
ON QUEUE [dbo].[Portfolio_ClientHier_Management_Queue]
FOR BROKER_QUEUE_DISABLED
TO SERVICE 'DisabledQueueNotificationService', 'C82B6052-0805-41E6-8228-785BBA94B399'

GO
