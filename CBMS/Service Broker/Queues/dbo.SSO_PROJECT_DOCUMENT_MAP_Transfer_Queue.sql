CREATE QUEUE [dbo].[SSO_PROJECT_DOCUMENT_MAP_Transfer_Queue] 
WITH STATUS=ON, 
RETENTION=OFF, 
ACTIVATION (
STATUS=ON, 
PROCEDURE_NAME=[dbo].[Change_Control_Receive_Messages_For_SSO_PROJECT_DOCUMENT_MAP_Transfer_Queue], 
MAX_QUEUE_READERS=1, 
EXECUTE AS N'sb_CBMS_Service'
)
ON [DB_DATA01]
GO
CREATE EVENT NOTIFICATION [DisabledTargetQueueNotification]
ON QUEUE [dbo].[SSO_PROJECT_DOCUMENT_MAP_Transfer_Queue]
FOR BROKER_QUEUE_DISABLED
TO SERVICE 'DisabledQueueNotificationService', 'C82B6052-0805-41E6-8228-785BBA94B399'
GO
