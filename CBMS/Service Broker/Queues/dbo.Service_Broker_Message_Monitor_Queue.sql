CREATE QUEUE [dbo].[Service_Broker_Message_Monitor_Queue] 
WITH STATUS=ON, 
RETENTION=OFF, 
ACTIVATION (
STATUS=ON, 
PROCEDURE_NAME=[dbo].[Change_Control_Receive_Messages_For_Service_Broker_Message_Monitor_Queue], 
MAX_QUEUE_READERS=1, 
EXECUTE AS OWNER
)
ON [DB_DATA01]
GO
CREATE EVENT NOTIFICATION [DisabledTargetQueueNotification]
ON QUEUE [dbo].[Service_Broker_Message_Monitor_Queue]
FOR BROKER_QUEUE_DISABLED
TO SERVICE 'DisabledQueueNotificationService', 'C82B6052-0805-41E6-8228-785BBA94B399'
GO
