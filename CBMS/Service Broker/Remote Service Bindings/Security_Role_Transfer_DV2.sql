CREATE REMOTE SERVICE BINDING [Security_Role_Transfer_DV2]
AUTHORIZATION [dbo]
TO SERVICE '//Change_Control/Service/DV2/Security_Role_Transfer'
WITH USER =[sb_DV2_Service],
ANONYMOUS = OFF
GO
