CREATE CONTRACT [//Transmission/Contract/Cbms_Currency_And_Price_Change]
AUTHORIZATION [dbo] ( 
[//Transmission/Message/Broker_Ack] SENT BY TARGET,
[//Transmission/Message/Test_Message] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Price_Index_Change] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Price_Index_Value_Change] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Currency_Conversion_Change] SENT BY INITIATOR
)
GO
