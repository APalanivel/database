CREATE CONTRACT [//Change_Control/Contract/Security_Role_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/User_Security_Role_Transfer] SENT BY INITIATOR,
[//Change_Control/Message/Security_Role_Client_Hier_Transfer] SENT BY INITIATOR,
[//Change_Control/Message/Security_Role_Transfer] SENT BY INITIATOR
)
GO
