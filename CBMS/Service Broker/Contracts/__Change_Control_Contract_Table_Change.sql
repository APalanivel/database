CREATE CONTRACT [//Change_Control/Contract/Table_Change]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Check_Threshold] SENT BY ANY,
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Table_Change_Info] SENT BY INITIATOR,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR
)
GO
