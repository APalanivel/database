CREATE CONTRACT [//Change_Control/Contract/SSO_PROJECT_DOCUMENT_MAP_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/SSO_PROJECT_DOCUMENT_MAP_Transfer] SENT BY INITIATOR
)
GO
