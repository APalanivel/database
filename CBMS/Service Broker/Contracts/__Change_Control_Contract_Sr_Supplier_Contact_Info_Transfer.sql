CREATE CONTRACT [//Change_Control/Contract/Sr_Supplier_Contact_Info_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Sr_Supplier_Contact_Info_Transfer] SENT BY INITIATOR
)
GO
