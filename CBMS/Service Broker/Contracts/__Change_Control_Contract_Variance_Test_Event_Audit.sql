CREATE CONTRACT [//Change_Control/Contract/Variance_Test_Event_Audit]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Variance_Test_Event_Audit] SENT BY INITIATOR
)
GO
