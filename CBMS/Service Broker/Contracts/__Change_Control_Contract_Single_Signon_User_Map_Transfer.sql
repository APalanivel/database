CREATE CONTRACT [//Change_Control/Contract/Single_Signon_User_Map_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Single_Signon_User_Map_Transfer] SENT BY INITIATOR
)
GO
