CREATE CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Mapping_Client_To_Portfolio] SENT BY INITIATOR,
[//Change_Control/Message/UnMapping_Client_From_Portfolio] SENT BY INITIATOR,
[//Change_Control/Message/Division_Del] SENT BY ANY
)
GO
