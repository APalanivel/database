CREATE CONTRACT [//Change_Control/Contract/Client_Hier_Change]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Client_Hier_Changes] SENT BY INITIATOR,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR
)
GO
