CREATE CONTRACT [//Change_Control/Contract/SB_Message_Monitor]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/SB_Message_Monitor_Ins] SENT BY INITIATOR,
[//Change_Control/Message/SB_Message_Monitor_Upd] SENT BY INITIATOR
)
GO
