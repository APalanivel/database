CREATE CONTRACT [//Transmission/Contract/Test_Message]
AUTHORIZATION [dbo] ( 
[//Transmission/Message/Broker_Ack] SENT BY TARGET,
[//Transmission/Message/Test_Message] SENT BY ANY
)
GO
