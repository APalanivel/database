CREATE CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Mapping_Site_To_Portfolio] SENT BY INITIATOR,
[//Change_Control/Message/Delete_Portfolio_Site] SENT BY ANY
)
GO
