CREATE CONTRACT [//Change_Control/Contract/User_Info_Client_App_Access_Role_Map_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/User_Info_Client_App_Access_Role_Map_Transfer] SENT BY INITIATOR
)
GO
