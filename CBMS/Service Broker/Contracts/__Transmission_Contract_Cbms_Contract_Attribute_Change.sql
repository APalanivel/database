CREATE CONTRACT [//Transmission/Contract/Cbms_Contract_Attribute_Change]
AUTHORIZATION [dbo] ( 
[//Transmission/Message/Broker_Ack] SENT BY TARGET,
[//Transmission/Message/Test_Message] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Contract_Header_Change] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Contract_Meter_Change] SENT BY INITIATOR,
[//Transmission/Message/Cbms_Contract_Meter_Volume_Change] SENT BY INITIATOR
)
GO
