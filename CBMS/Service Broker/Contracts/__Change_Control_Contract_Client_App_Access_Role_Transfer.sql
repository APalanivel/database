CREATE CONTRACT [//Change_Control/Contract/Client_App_Access_Role_Transfer]
AUTHORIZATION [dbo] ( 
[//Change_Control/Message/Broker_Ack] SENT BY TARGET,
[//Change_Control/Message/Test_Message] SENT BY INITIATOR,
[//Change_Control/Message/Client_App_Access_Role_Group_Info_Map_Transfer] SENT BY INITIATOR,
[//Change_Control/Message/Client_App_Access_Role_Transfer] SENT BY INITIATOR
)
GO
