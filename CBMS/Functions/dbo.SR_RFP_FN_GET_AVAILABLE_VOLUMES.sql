
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.SR_RFP_FN_GET_AVAILABLE_VOLUMES

DESCRIPTION:
    This function is to get the Gas volumes for Tariff Transport Report 

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    @AccountId		INT
    @FromDate		VARCHAR(12)
    @AsOfDate		VARCHAR(12)


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi

MODIFICATIONS:
    Initials	Date		   Modification
------------------------------------------------------------
					   --Addnl Data Changes
    AP		08/11/2011   Removed Cost_Usage table and used Cost_Usage_Account_Dtl, Bucket_Master, Commodity, Code tables
    AP		03/28/2012   Replaced multiple IF statements & used SELECT with WHERE clause
******/

CREATE FUNCTION dbo.SR_RFP_FN_GET_AVAILABLE_VOLUMES
      ( 
       @AccountId INT
      ,@FromDate VARCHAR(12)
      ,@AsOfDate VARCHAR(12) )
RETURNS DECIMAL(32, 16)
AS 
BEGIN

      DECLARE
            @MaxValue DECIMAL(32, 16)
           ,@EL_Unit_of_Measure_Type_ID INT
           ,@Commodity_ID INT
           ,@Actual_Demand_Usage DECIMAL(32, 16)
           ,@Billed_Demand_Usage DECIMAL(32, 16)
           ,@Contract_Demand_Usage DECIMAL(32, 16)
           

      SELECT
            @EL_Unit_of_Measure_Type_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'kW'
            AND uom.Entity_Description = 'Unit for electricity'

      SELECT
            @Commodity_ID = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'
            		
      SELECT
            @MaxValue = isnull(max(case WHEN BM.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage' ) THEN CUAD.Bucket_Value
                                   END), -1)
           ,@Actual_Demand_Usage = isnull(max(case WHEN BM.Bucket_Name IN ( 'Demand' ) THEN CUAD.Bucket_Value
                                              END), -1)
           ,@Billed_Demand_Usage = isnull(max(case WHEN BM.Bucket_Name IN ( 'Billed Demand' ) THEN CUAD.Bucket_Value
                                              END), -1)
           ,@Contract_Demand_Usage = isnull(max(case WHEN BM.Bucket_Name IN ( 'Contract Demand' ) THEN CUAD.Bucket_Value
                                                END), -1)
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Code CD
                  ON CD.Code_Id = BM.Bucket_Type_Cd
      WHERE
            CUAD.Account_ID = @AccountId
            AND CUAD.UOM_Type_Id = @EL_Unit_of_Measure_Type_ID
            AND CUAD.Service_Month BETWEEN convert(DATETIME, @FromDate)
                                   AND     convert(DATETIME, @AsOfDate)
            AND BM.Commodity_Id = @Commodity_ID
            AND BM.Bucket_Name IN ( 'Total Usage', 'On-Peak Usage', 'Off-Peak Usage', 'Other-Peak Usage', 'Demand', 'Billed Demand', 'Contract Demand' )
            AND BM.Is_Active = 1
            AND CD.Code_Value = 'Determinant'
 

      SELECT
            @MaxValue = @Actual_Demand_Usage
      WHERE
            @MaxValue < @Actual_Demand_Usage 

      SELECT
            @MaxValue = @Billed_Demand_Usage
      WHERE
            @MaxValue < @Billed_Demand_Usage 

      SELECT
            @MaxValue = @Contract_Demand_Usage
      WHERE
            @MaxValue < @Contract_Demand_Usage 

  
      RETURN @MaxValue

END

;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_AVAILABLE_VOLUMES] TO [CBMSApplication]
GO
