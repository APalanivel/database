SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from meter order by account_id, rate_id
--select dbo.SR_RFP_FN_GET_RATES_FOR_TARIFF_TRANSPORT_REPORT(45,373) 

CREATE    FUNCTION [dbo].[SR_RFP_FN_GET_RATES_FOR_TARIFF_TRANSPORT_REPORT](@account_id int, @rate_id int) 
RETURNS varchar(8000) 
AS 

BEGIN 
	
declare @st varchar(2000), @rate varchar(200)
	IF @rate_id > 0
	BEGIN
		
		set @st=''
		select @st=@st+', '+rate_name from rate r
		where rate_id in (select distinct m.rate_id 
			from 	meter m(nolock)		     
			where 	m.account_id =@account_id			
				and m.rate_id = @rate_id)
		
		
	END
	ELSE IF @rate_id = 0
	BEGIN
		set @st=''
		select @st=@st+', '+rate_name from rate r
		where rate_id in (select distinct m.rate_id 
			from 	meter m(nolock)		     
			where 	m.account_id =@account_id)
	END

	if @st=''
	set @st = null
return substring(@st,3,len(@st))

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RATES_FOR_TARIFF_TRANSPORT_REPORT] TO [CBMSApplication]
GO
