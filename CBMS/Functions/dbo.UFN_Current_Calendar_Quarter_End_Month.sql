
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.UFN_Current_Calendar_Quarter_End_Month     

DESCRIPTION:     

INPUT PARAMETERS:        
Name					DataType  Default Description        
------------------------------------------------------------        
@Service_Month			DATETIME

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select dbo.UFN_Current_Calendar_Quarter_End_Month('2015-10-20')

 
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
RKV		Ravi Kumar Vegesna

MODIFICATIONS:  

Initials Date			Modification        
------------------------------------------------------------        
RKV      2015-09-30		Created
RKV      2016-06-10     Changed the logic of current calendar quarter to be till the bill month 

******/    
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



CREATE FUNCTION [dbo].[UFN_Current_Calendar_Quarter_End_Month]
      ( 
       @Service_Month DATETIME )
RETURNS DATE
BEGIN
  
          
      RETURN CAST(DATEADD(dd,-(DAY(@Service_Month)-1),@Service_Month) AS DATE)
    
     
      
END

;

;
GO

GRANT EXECUTE ON  [dbo].[UFN_Current_Calendar_Quarter_End_Month] TO [CBMSApplication]
GO
