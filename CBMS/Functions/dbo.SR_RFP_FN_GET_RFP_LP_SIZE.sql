SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_RFP_FN_GET_RFP_LP_SIZE(250) 




CREATE    FUNCTION [dbo].[SR_RFP_FN_GET_RFP_LP_SIZE](@rfp_id int)
		 

RETURNS bigint
AS 

BEGIN 
	declare @total_lp_size bigint
	
	set @total_lp_size = 0

	if (select 	count(setup.sr_rfp_account_id) 
		from 	sr_rfp_account rfp_account(nolock),
			sr_rfp_load_profile_setup setup(nolock)
		where 	rfp_account.sr_rfp_id = @rfp_id
			and rfp_account.is_deleted = 0
			and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id) > 0
	begin

		declare @commodity varchar(50), @rfp_account_id int, @converted_unit_id int, @lp_size bigint
			
		select  @commodity = en.entity_name 
		from   	   sr_rfp rfp(nolock),
			   entity en (nolock)
		where      rfp.sr_rfp_id = @rfp_id
			   and en.entity_id = rfp.commodity_type_id
				
		if @commodity = 'Natural Gas'
		begin
			select @converted_unit_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
			select	@total_lp_size = sum(value.lp_value * conv.conversion_factor)
		
			from 	sr_rfp_account rfp_account(nolock),
				sr_rfp_load_profile_setup setup(nolock),
				sr_rfp_load_profile_determinant determinant(nolock),
				sr_rfp_lp_determinant_values value (nolock),
				entity en(nolock),
				consumption_unit_conversion conv(nolock)
			
			where	rfp_account.sr_rfp_id = @rfp_id
				and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
				and rfp_account.is_deleted = 0
				and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
				and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
				and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
				and value.reading_type_id = en.entity_id
				and value.lp_value is not null
				and conv.base_unit_id = determinant.determinant_unit_type_id
				and conv.converted_unit_id =@converted_unit_id

		end
		else if @commodity = 'Electric Power'
		begin
			select @converted_unit_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kW'
			DECLARE C_LP_SIZE CURSOR FAST_FORWARD
			FOR 

			select	max(value.lp_value)
			
			from 	sr_rfp_account rfp_account(nolock),
				sr_rfp_load_profile_setup setup(nolock),
				sr_rfp_load_profile_determinant determinant(nolock),
				sr_rfp_lp_determinant_values value (nolock),
				entity en(nolock)
			
			where	rfp_account.sr_rfp_id = @rfp_id
				and setup.sr_rfp_account_id =  rfp_account.sr_rfp_account_id
				and rfp_account.is_deleted = 0
				and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
				and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
				and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
				and value.reading_type_id = en.entity_id
				and value.lp_value is not null
				and determinant.determinant_unit_type_id = @converted_unit_id
			group by rfp_account.sr_rfp_account_id

			OPEN C_LP_SIZE
			FETCH NEXT FROM C_LP_SIZE INTO @lp_size
			WHILE (@@fetch_status <> -1)
			BEGIN
				IF (@@fetch_status <> -2)
				BEGIN
					if @total_lp_size is null  
					begin
						select @total_lp_size = @lp_size
					end
					else if @lp_size is not null
					begin
						select @total_lp_size = @total_lp_size + @lp_size
					end
				END
				FETCH NEXT FROM C_LP_SIZE INTO @lp_size
			END
			CLOSE C_LP_SIZE
			DEALLOCATE C_LP_SIZE


		end
	
	
		
				
	
	end		
				

		 
	RETURN @total_lp_size
	
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RFP_LP_SIZE] TO [CBMSApplication]
GO
