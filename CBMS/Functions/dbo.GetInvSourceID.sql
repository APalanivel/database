SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function dbo.GetInvSourceID(
	@cbms_doc_id varchar(200)
)
returns int
as
begin
	declare @inv varchar(60)
	if charindex('_',@cbms_doc_id)=0
	begin
		return 0
	end
	select @inv=left(@cbms_doc_id,charindex('_',@cbms_doc_id)-1)
	if isnumeric(@inv)=1
	begin	
		return @inv
	end
	else
	begin
		return 0
	end
	return 0
end



GO
GRANT EXECUTE ON  [dbo].[GetInvSourceID] TO [CBMSApplication]
GO
