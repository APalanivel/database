SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_740727781]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Deal_Ticket_Id], NULL as [Client_Id], NULL as [Deal_Ticket_Number], NULL as [Commodity_Id], NULL as [Is_Client_Generated], NULL as [Hedge_Start_Dt], NULL as [Hedge_End_Dt], NULL as [Hedge_Type_Cd], NULL as [Hedge_Allocation_Type_Cd], NULL as [Price_Index_Id], NULL as [Currency_Unit_Id], NULL as [Uom_Type_Id], NULL as [Deal_Ticket_Frequency_Cd], NULL as [Trade_Pricing_Option_Cd], NULL as [Trade_Action_Type_Cd], NULL as [Deal_Ticket_Type_Cd], NULL as [Deal_Status_Cd], NULL as [Workflow_Id], NULL as [Is_Individual_Site_Pricing_Required], NULL as [Comment_Id], NULL as [Created_User_Id], NULL as [Created_Ts], NULL as [Updated_User_Id], NULL as [Last_Change_Ts], NULL as [Is_Bid], NULL as [Fixed_Dt], NULL as [Hedge_Mode_Type_Id], NULL as [Queue_Id], NULL as [Last_Routed_By_User_Id], NULL as [Last_Routed_Ts]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Deal_Ticket_Id], t.[Client_Id], t.[Deal_Ticket_Number], t.[Commodity_Id], t.[Is_Client_Generated], t.[Hedge_Start_Dt], t.[Hedge_End_Dt], t.[Hedge_Type_Cd], t.[Hedge_Allocation_Type_Cd], t.[Price_Index_Id], t.[Currency_Unit_Id], t.[Uom_Type_Id], t.[Deal_Ticket_Frequency_Cd], t.[Trade_Pricing_Option_Cd], t.[Trade_Action_Type_Cd], t.[Deal_Ticket_Type_Cd], t.[Deal_Status_Cd], t.[Workflow_Id], t.[Is_Individual_Site_Pricing_Required], t.[Comment_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[Is_Bid], t.[Fixed_Dt], t.[Hedge_Mode_Type_Id], t.[Queue_Id], t.[Last_Routed_By_User_Id], t.[Last_Routed_Ts]
	from [cdc].[hvr_740727781_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Deal_Ticket_Id], t.[Client_Id], t.[Deal_Ticket_Number], t.[Commodity_Id], t.[Is_Client_Generated], t.[Hedge_Start_Dt], t.[Hedge_End_Dt], t.[Hedge_Type_Cd], t.[Hedge_Allocation_Type_Cd], t.[Price_Index_Id], t.[Currency_Unit_Id], t.[Uom_Type_Id], t.[Deal_Ticket_Frequency_Cd], t.[Trade_Pricing_Option_Cd], t.[Trade_Action_Type_Cd], t.[Deal_Ticket_Type_Cd], t.[Deal_Status_Cd], t.[Workflow_Id], t.[Is_Individual_Site_Pricing_Required], t.[Comment_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[Is_Bid], t.[Fixed_Dt], t.[Hedge_Mode_Type_Id], t.[Queue_Id], t.[Last_Routed_By_User_Id], t.[Last_Routed_Ts]
	from [cdc].[hvr_740727781_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_740727781] TO [public]
GO
