SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE     FUNCTION dbo.SR_RFP_FN_GET_BID_GROUP_STATUS(@groupId int) 
RETURNS varchar(50) 
AS 
begin
declare @status varchar(50)

declare @temp_table table (sno int identity,status varchar(50))

insert into @temp_table

SELECT  distinct ENTITY_NAME
FROM 	SR_RFP_ACCOUNT,ENTITY
WHERE 	SR_RFP_ACCOUNT.SR_RFP_BID_GROUP_ID = @groupId
	and SR_RFP_ACCOUNT.BID_STATUS_TYPE_ID=ENTITY.ENTITY_ID
	and SR_RFP_ACCOUNT.is_deleted = 0
	and ENTITY.ENTITY_TYPE = 1029 

/*if (select count(1) from @temp_table where status = 'Closed') = 1
	set @status = 'Closed'
else
	set @status = 'Not Closed'*/

set @status = 'Closed' 
declare @status1 varchar(50)
declare @sid int

set @sid=1

while (select count(status) from @temp_table)>0
begin

set @status1=(select top 1 status from @temp_table)

if (@status1='Open' or @status1='New' or @status1='Bid Placed' or @status1='Expired' or @status1='Closed Awarded' or @status1='Closed Not Awarded' or @status1='Refresh' or @status1='Processing')

begin
set @status='Not Closed'
break
end

delete @temp_table where sno=@sid

set @sid=@sid+1


end



return @status
end 

GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_BID_GROUP_STATUS] TO [CBMSApplication]
GO
