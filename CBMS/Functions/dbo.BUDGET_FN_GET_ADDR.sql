SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION dbo.BUDGET_FN_GET_ADDR(@account_id int) 
RETURNS varchar(1000) 
AS 

BEGIN 	
	declare @address varchar(200)
	set @address = ''
	select @address = @address+', '+addr.address_line1
	from 	meter m(nolock)
		join address addr(nolock) on addr.address_id = m.address_id
		and m.account_id = @account_id
	group by addr.address_line1
	select @address = @address+', '+addr.city+', '+s.state_name+', '+addr.zipcode
	from 	meter m(nolock)
		join address addr(nolock) on addr.address_id = m.address_id
		and m.account_id = @account_id
		join state s on s.state_id = addr.state_id
	group by addr.city,s.state_name,addr.zipcode	
	set @address = substring(@address,3,len(@address))


	return @address
END

GO
GRANT EXECUTE ON  [dbo].[BUDGET_FN_GET_ADDR] TO [CBMSApplication]
GO
