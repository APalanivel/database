SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE FUNCTION dbo.SR_RFP_FN_CHECK_INDUSTRIAL_ACCOUNT( @accountId int) 
RETURNS int

AS 



BEGIN 


declare @entityId int
select @entityId = entity_id from entity where entity_type = 708 and entity_name='C' and entity_description = 'Commercial' 
	DECLARE @cnt int
	
	
		select @cnt =  	count(1) 
			from 	account 
			where 	service_level_type_id = @entityId
				and account_id = @accountId
				
	

	RETURN @cnt
END 








GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_CHECK_INDUSTRIAL_ACCOUNT] TO [CBMSApplication]
GO
