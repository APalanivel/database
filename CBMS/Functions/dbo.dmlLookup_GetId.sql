SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create FUNCTION [dbo].[dmlLookup_GetId]
	( @LookupTypeName varchar(50)
	, @LookupName varchar(50)
	) returns int
AS
BEGIN
	return (
		select l.LookupId
        	  from dmlLookup l
		  join dmlLookupType t on l.LookupTypeId = t.LookupTypeId
	         where l.LookupName = @LookupName
		   and t.LookupTypeName = @LookupTypeName
		)
END



GO
GRANT EXECUTE ON  [dbo].[dmlLookup_GetId] TO [CBMSApplication]
GO
