
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS        

DESCRIPTION:     

INPUT PARAMETERS:        
Name					DataType  Default Description        
------------------------------------------------------------        
@accountgroupId		INT,
@isBidGroupId		INT 

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select * from dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS (745,1)
select * from dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS (12019462,0)  
select * from dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS (10012036,1)
select * from dbo.UFN_SR_RFP_FN_GET_ACCOUNT_TERMS (12105078,0)  


 
AUTHOR INITIALS:        
	Initials	Name        
------------------------------------------------------------        
	SKA			Shobhit Kumar Agrawal   
	RR			Raghu Reddy

MODIFICATIONS:  

	Initials	Date		Modification        
------------------------------------------------------------        
	SKA			06/Jan/2010	Created to replace all the scalar function from SR_RFP_GET_VIEW_BIDS_SOP_INFO_P.sql
	SKA			07/23/2010	All columns has been qualified
	SSR			08/23/2010	Replaced hardcoded values with @isBidGroupId
	DMR			09/10/2010	Modified for Quoted_Identifier
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values

******/    
CREATE FUNCTION [dbo].[UFN_SR_RFP_FN_GET_ACCOUNT_TERMS]
      ( 
       @accountgroupId INT
      ,@isBidGroupId INT )
RETURNS @AccountGroup_ID TABLE
      ( 
       AllAccountTermsId VARCHAR(2000) )
AS 
BEGIN
	
      IF NOT EXISTS ( SELECT
                        1
                      FROM
                        dbo.SR_RFP_ACCOUNT_TERM accterm
                        INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP prdmap
                              ON prdmap.SR_RFP_ACCOUNT_TERM_ID = accterm.SR_RFP_ACCOUNT_TERM_ID
                      WHERE
                        accterm.SR_ACCOUNT_GROUP_ID = @accountgroupId
                        AND accterm.IS_BID_GROUP = @isBidGroupId
                        AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL ) 
            BEGIN	
                  IF ( SELECT
                        count(SR_RFP_SEND_SUPPLIER_ID)
                       FROM
                        dbo.SR_RFP_SEND_SUPPLIER supplier
                        INNER JOIN dbo.ENTITY ent
                              ON ent.ENTITY_ID = supplier.status_type_id
                       WHERE
                        supplier.sr_account_group_id = @accountgroupId
                        AND supplier.is_bid_group = @isBidGroupId
                        AND ent.entity_description = 'RFP sent to supplier status'
                        AND ent.entity_name = 'Post' ) > 0 
                        INSERT      INTO @AccountGroup_ID
                                    SELECT
                                          '-1'
            END
      ELSE 
            INSERT      INTO @AccountGroup_ID
                        SELECT
                              accterm.SR_RFP_ACCOUNT_TERM_ID
                        FROM
                              dbo.SR_RFP_ACCOUNT_TERM accterm
                              INNER JOIN dbo.SR_RFP_TERM_PRODUCT_MAP prdmap
                                    ON prdmap.SR_RFP_ACCOUNT_TERM_ID = accterm.SR_RFP_ACCOUNT_TERM_ID
                        WHERE
                              accterm.SR_ACCOUNT_GROUP_ID = @accountgroupId
                              AND accterm.IS_BID_GROUP = @isBidGroupId
                              AND prdmap.SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL
                        GROUP BY
                              accterm.SR_RFP_ACCOUNT_TERM_ID
      RETURN
END

;
GO

GRANT SELECT ON  [dbo].[UFN_SR_RFP_FN_GET_ACCOUNT_TERMS] TO [CBMSApplication]
GO
