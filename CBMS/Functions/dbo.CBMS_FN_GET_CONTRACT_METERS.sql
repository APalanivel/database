SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE  FUNCTION dbo.CBMS_FN_GET_CONTRACT_METERS(@contract_id int) 
RETURNS varchar(2000) 
AS 



BEGIN 
	DECLARE @meterStr varchar(2000), @meter varchar(200)
	DECLARE C_METER CURSOR FAST_FORWARD
	FOR select m.meter_number from meter m(nolock),sr_all_contract_vw vw(nolock)
		where 	m.meter_id = vw.meter_id and vw.contract_id = @contract_id
	OPEN C_METER
	FETCH NEXT FROM C_METER INTO @meter
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			IF @meterStr IS NULL OR @meterStr = ''
				SELECT @meterStr = @meter 
			ELSE
				SELECT @meterStr = @meterStr +', '+ @meter 
		END
	FETCH NEXT FROM C_METER INTO @meter
	END
	CLOSE C_METER
	DEALLOCATE C_METER

RETURN @meterStr
END 













GO
GRANT EXECUTE ON  [dbo].[CBMS_FN_GET_CONTRACT_METERS] TO [CBMSApplication]
GO
