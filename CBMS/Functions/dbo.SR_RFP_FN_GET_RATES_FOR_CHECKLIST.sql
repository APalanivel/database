SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE     FUNCTION [dbo].[SR_RFP_FN_GET_RATES_FOR_CHECKLIST](@account_id int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	
declare @st varchar(2000)
	set @st=''
	select @st=@st+', '+rate_name from rate r
	where rate_id in (select distinct rate_id
		from 	meter (nolock)
		where 	account_id =@account_id)
	group by r.rate_name
	if @st=''
	set @st = null
return substring(@st,3,len(@st))

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RATES_FOR_CHECKLIST] TO [CBMSApplication]
GO
