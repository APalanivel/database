SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.UFN_Previous_Calendar_Year_End_Month   

DESCRIPTION:     

INPUT PARAMETERS:        
Name					DataType  Default Description        
------------------------------------------------------------        
@Service_Month			DATETIME

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select dbo.UFN_Previous_Calendar_Year_End_Month('2015-10-20')

 
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
RKV		Ravi Kumar Vegesna

MODIFICATIONS:  

Initials Date			Modification        
------------------------------------------------------------        
RKV      2015-09-30		Created

******/    
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[UFN_Previous_Calendar_Year_End_Month]
      ( 
       @Service_Month DATETIME )
RETURNS DATE
BEGIN
     
     
     RETURN  DATEADD(mm, DATEDIFF(mm,0,DATEADD(MILLISECOND, -3, DATEADD(YEAR,DATEDIFF(YEAR, 0, DATEADD(YEAR, -1, @Service_Month)) + 1, 0))), 0)
    
          
END

;
GO
GRANT EXECUTE ON  [dbo].[UFN_Previous_Calendar_Year_End_Month] TO [CBMSApplication]
GO
