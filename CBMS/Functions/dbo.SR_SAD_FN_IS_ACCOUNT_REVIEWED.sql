SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[SR_SAD_FN_IS_ACCOUNT_REVIEWED](@accountId int, @year int) 
RETURNS int
AS 

BEGIN 
	RETURN (select count(1) from SR_MASTER_CHECKLIST_REVIEW_STATUS(nolock) where account_id = @accountId and checklist_year = @year)
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_IS_ACCOUNT_REVIEWED] TO [CBMSApplication]
GO
