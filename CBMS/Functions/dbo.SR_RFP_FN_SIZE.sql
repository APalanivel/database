SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO










CREATE  FUNCTION DBO.SR_RFP_FN_SIZE (@rfpid int)
RETURNS @t_lp_size table
([Duplicate_group_id] [int], [lp_size] [numeric] (32,16)
)
as
begin


declare @commodity varchar(50), 
	@converted_unit_id int

select  @commodity = en.entity_name 
from    sr_rfp rfp(nolock),entity en (nolock)
where   rfp.sr_rfp_id = @rfpid
	and en.entity_id = rfp.commodity_type_id	
if @commodity = 'Natural Gas'
begin
	select @converted_unit_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
end else 
begin
	select @converted_unit_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kW'
end

--CREATE table @t_lp_size (Duplicate_group_id int, lp_size numeric(32,16))


if @commodity = 'Natural Gas'
begin
	insert into @t_lp_size
--
	select	rfp_account.sr_rfp_bid_group_id,
		isnull(sum(value.lp_value * conv.conversion_factor), -1)

	from 	sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		consumption_unit_conversion conv(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = @rfpid
		and rfp_account.sr_rfp_bid_group_id is not null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')		
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and conv.base_unit_id = determinant.determinant_unit_type_id
		and conv.converted_unit_id = @converted_unit_id
	group by rfp_account.sr_rfp_bid_group_id

	insert into @t_lp_size

	select	rfp_account.sr_rfp_account_id,
		isnull(sum(value.lp_value * conv.conversion_factor), -1)

	from 	sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		consumption_unit_conversion conv(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = @rfpid
		and rfp_account.sr_rfp_bid_group_id is null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and conv.base_unit_id = determinant.determinant_unit_type_id
		and conv.converted_unit_id = @converted_unit_id
	group by rfp_account.sr_rfp_account_id

end else if @commodity = 'Electric Power'
begin
	
	declare @t_lp_el_temp table( sr_rfp_bid_group_id int, lp_size numeric(32,16))

	insert into @t_lp_el_temp
	select	rfp_account.sr_rfp_bid_group_id,
		isnull(max(value.lp_value), -1)

	from 	sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = @rfpid
		and rfp_account.sr_rfp_bid_group_id is not null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and determinant.determinant_unit_type_id = @converted_unit_id
	group by rfp_account.sr_rfp_account_id,rfp_account.sr_rfp_bid_group_id
	order by rfp_account.sr_rfp_bid_group_id

	insert into @t_lp_size

	select 	sr_rfp_bid_group_id,
		isnull(sum(lp_size), -1)
	from 	@t_lp_el_temp
	group by sr_rfp_bid_group_id
		
	insert into @t_lp_size

	select	rfp_account.sr_rfp_account_id,
		isnull(max(value.lp_value), -1)

	from 	sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = @rfpid
		and rfp_account.sr_rfp_bid_group_id is null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and determinant.determinant_unit_type_id = @converted_unit_id
	group by rfp_account.sr_rfp_account_id

end


return
end








GO
GRANT SELECT ON  [dbo].[SR_RFP_FN_SIZE] TO [CBMSApplication]
GO
