SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO





CREATE   FUNCTION dbo.SR_RFP_SIZE()
RETURNS @lp_size table
([rfp_id] [int], [lp_size] [numeric] (32,16))
as
begin

declare @converted_unit_id int


--CREATE table @t_lp_size (Duplicate_group_id int, lp_size numeric(32,16))
declare @t_lp_size table( rfp_id int, Duplicate_group_id int, lp_size numeric(32,16))

	insert into @t_lp_size

	select	rfp.sr_rfp_id,
		rfp_account.sr_rfp_bid_group_id,
		isnull(sum(value.lp_value * conv.conversion_factor), -1)

	from 	sr_rfp rfp,
		sr_rfp_account rfp_account(nolock),
		entity en(nolock),
		sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		consumption_unit_conversion conv(nolock)
			
	where	rfp_account.sr_rfp_id = rfp.sr_rfp_id
		and rfp.COMMODITY_TYPE_ID = 291 -- Natural Gas entity ID 
		and rfp_account.sr_rfp_bid_group_id is not null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')		
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and conv.base_unit_id = determinant.determinant_unit_type_id
		and conv.converted_unit_id = 25 --select entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
	group by rfp_account.sr_rfp_bid_group_id,rfp.sr_rfp_id

	insert into @t_lp_size

	select	rfp.sr_rfp_id,
		rfp_account.sr_rfp_account_id,
		isnull(sum(value.lp_value * conv.conversion_factor), -1)
	from 	sr_rfp rfp,
		sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		consumption_unit_conversion conv(nolock),
		sr_rfp_account rfp_account(nolock)
	where	rfp_account.sr_rfp_id = rfp.sr_rfp_id
		and rfp.COMMODITY_TYPE_ID = 291 -- Natural Gas entity ID 
		and rfp_account.sr_rfp_bid_group_id is null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and conv.base_unit_id = determinant.determinant_unit_type_id
		and conv.converted_unit_id = 25 --select entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
	group by rfp_account.sr_rfp_account_id,rfp.sr_rfp_id


	
	declare @t_lp_el_temp table( sr_rfp_id int, sr_rfp_bid_group_id int, lp_size numeric(32,16))

	insert into @t_lp_el_temp
	select	rfp.sr_rfp_id,
		rfp_account.sr_rfp_bid_group_id,
		isnull(max(value.lp_value), -1)

	from 	sr_rfp rfp,
		sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = rfp.sr_rfp_id
		and rfp.COMMODITY_TYPE_ID = 290 --For Ele
		and rfp_account.sr_rfp_bid_group_id is not null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and determinant.determinant_unit_type_id = 11 --Kw
	group by rfp_account.sr_rfp_account_id,rfp_account.sr_rfp_bid_group_id,rfp.sr_rfp_id
	order by rfp_account.sr_rfp_bid_group_id

	insert into @t_lp_size

	select 	sr_rfp_id,
		sr_rfp_bid_group_id,
		isnull(sum(lp_size), -1)
	from 	@t_lp_el_temp
	group by sr_rfp_bid_group_id,sr_rfp_id
		
	insert into @t_lp_size

	select	rfp.sr_rfp_id,
		rfp_account.sr_rfp_account_id,
		isnull(max(value.lp_value), -1)

	from 	sr_rfp rfp,
		sr_rfp_load_profile_setup setup(nolock),
		sr_rfp_load_profile_determinant determinant(nolock),
		sr_rfp_lp_determinant_values value (nolock),
		entity en(nolock),
		sr_rfp_account rfp_account(nolock)
	
	where	rfp_account.sr_rfp_id = rfp.sr_rfp_id
		and rfp.COMMODITY_TYPE_ID = 290 --For Ele
		and rfp_account.sr_rfp_bid_group_id is null
		and setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
		and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
		and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
		and value.reading_type_id = en.entity_id
		and value.lp_value is not null
		and determinant.determinant_unit_type_id = 11--kw
	group by rfp_account.sr_rfp_account_id,rfp.sr_rfp_id


insert into @lp_size
select rfp_id ,isnull(sum(lp_size),0) from @t_lp_size group by rfp_id order by rfp_id

return
end







GO
GRANT SELECT ON  [dbo].[SR_RFP_SIZE] TO [CBMSApplication]
GO
