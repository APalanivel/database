SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.UFN_Previous_Custom_Term_Start_Month     

DESCRIPTION:     

INPUT PARAMETERS:        
Name					DataType  Default Description        
------------------------------------------------------------        
@Service_Month			DATETIME

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select dbo.UFN_Previous_Custom_Term_Start_Month('2015-10-20')

 
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
RKV		Ravi Kumar Vegesna

MODIFICATIONS:  

Initials Date			Modification        
------------------------------------------------------------        
RKV      2015-09-30		Created

******/
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[UFN_Previous_Custom_Term_Start_Month]
     (
         @Service_Month DATETIME
     )
RETURNS DATE
    BEGIN



        RETURN DATEADD(
                   yy, -1
                   , CAST(CASE WHEN MONTH(@Service_Month) < '4' THEN
                                   CONVERT(VARCHAR, DATEPART(YY, @Service_Month) - 1) + '0401'
                              ELSE CONVERT(VARCHAR, DATEPART(YY, @Service_Month)) + '0401'
                          END
AS                     DATE));




    END;

GO
GRANT EXECUTE ON  [dbo].[UFN_Previous_Custom_Term_Start_Month] TO [CBMSApplication]
GO
