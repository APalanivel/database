
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.SR_RFP_FN_GET_ACCOUNT_TERMS        

DESCRIPTION:     

INPUT PARAMETERS:        
	Name					DataType  Default Description        
------------------------------------------------------------        
	@accountgroupId		INT
	@isBidGroupId		INT 

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select dbo.SR_RFP_FN_GET_ACCOUNT_TERMS (745,1)
select dbo.SR_RFP_FN_GET_ACCOUNT_TERMS (12019462,0)  
select dbo.SR_RFP_FN_GET_ACCOUNT_TERMS (10012036,1)
select dbo.SR_RFP_FN_GET_ACCOUNT_TERMS (12105078,0)  


 
AUTHOR INITIALS:        
	Initials	Name        
------------------------------------------------------------        
	RR			Raghu Reddy

MODIFICATIONS:  

	Initials	Date		Modification        
------------------------------------------------------------        
	RR			2016-03-14	Global Sourcing - Phase3 - GCS-524 - Modified "RFP sent to supplier status" entity values

******/  

CREATE     FUNCTION [dbo].[SR_RFP_FN_GET_ACCOUNT_TERMS]
      ( 
       @accountgroupId INT
      ,@isBidGroupId INT )
RETURNS VARCHAR(2000)
AS 
BEGIN 
      DECLARE @allAccountTermsId VARCHAR(2000)
      SET @allAccountTermsId = ''
      SELECT
            @allAccountTermsId = @allAccountTermsId + ', ' + str(SR_RFP_ACCOUNT_TERM_ID)
      FROM
            SR_RFP_ACCOUNT_TERM
      WHERE
            SR_RFP_ACCOUNT_TERM_ID IN ( SELECT 	DISTINCT
                                          SR_RFP_ACCOUNT_TERM_ID
                                        FROM
                                          SR_RFP_TERM_PRODUCT_MAP
                                        WHERE
                                          SR_RFP_ACCOUNT_TERM.SR_ACCOUNT_GROUP_ID = @accountgroupId
                                          AND SR_RFP_ACCOUNT_TERM.IS_BID_GROUP = @isBidGroupId
                                          AND SR_RFP_ACCOUNT_TERM.SR_RFP_ACCOUNT_TERM_ID = SR_RFP_TERM_PRODUCT_MAP.SR_RFP_ACCOUNT_TERM_ID
                                          AND SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID IS NOT NULL )

 
      IF @allAccountTermsId IS NULL
            OR @allAccountTermsId = '' 
            BEGIN			
                  IF ( SELECT
                        count(SR_RFP_SEND_SUPPLIER_ID)
                       FROM
                        SR_RFP_SEND_SUPPLIER supplier
                       WHERE
                        supplier.sr_account_group_id = @accountgroupId
                        AND supplier.is_bid_group = @isBidGroupId
                        AND status_type_id = ( SELECT
                                                entity_id
                                               FROM
                                                entity
                                               WHERE
                                                entity_type = 1013
                                                AND entity_name = 'Post' ) ) > 0 
                        BEGIN			
                              SELECT
                                    @allAccountTermsId = '-1'
                        END
			
            END

      IF @allAccountTermsId = '' 
            SET @allAccountTermsId = NULL

      IF @allAccountTermsId <> '-1' 
            SELECT
                  @allAccountTermsId = substring(@allAccountTermsId, 3, len(@allAccountTermsId))
	
      RETURN @allAccountTermsId

END




;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_ACCOUNT_TERMS] TO [CBMSApplication]
GO
