SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[SR_RFP_FN_GET_LP_CLIENT_APPROVAL_IMAGE_ID](@rfp_id int, @accountId int) 
RETURNS bigint

AS 



BEGIN 
	DECLARE @cbmsImageId int
	
	
	select @cbmsImageId = approval.cbms_image_id 
				from sr_rfp_account rfp_account(nolock),
				sr_rfp_lp_client_approval approval(nolock),		
				account a(nolock)
				 
			where	rfp_account.sr_rfp_id = @rfp_id
				and approval.sr_account_group_id = rfp_account.sr_rfp_account_id
				and rfp_account.is_deleted = 0
				and a.account_id = rfp_account.account_id
				and a.account_id = @accountId
				
	

	RETURN @cbmsImageId
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_LP_CLIENT_APPROVAL_IMAGE_ID] TO [CBMSApplication]
GO
