SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_1268729662]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Deal_Ticket_Client_Hier_Workflow_Status_Id], NULL as [Deal_Ticket_Client_Hier_Id], NULL as [Workflow_Status_Map_Id], NULL as [Workflow_Status_Comment], NULL as [Is_Active], NULL as [Last_Notification_Sent_Ts], NULL as [Created_Ts], NULL as [Updated_User_Id], NULL as [Last_Change_Ts], NULL as [CBMS_Image_Id], NULL as [Trade_Month], NULL as [Workflow_Task_Id], NULL as [Contract_Id]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_E325975C
	    when 1 then __$operation
	    else
			case __$min_op_E325975C 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [Deal_Ticket_Client_Hier_Workflow_Status_Id], [Deal_Ticket_Client_Hier_Id], [Workflow_Status_Map_Id], [Workflow_Status_Comment], [Is_Active], [Last_Notification_Sent_Ts], [Created_Ts], [Updated_User_Id], [Last_Change_Ts], [CBMS_Image_Id], [Trade_Month], [Workflow_Task_Id], [Contract_Id]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_E325975C 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1268729662_CT] c with (nolock)   
			where  ( (c.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = t.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_E325975C, __$count_E325975C, t.[Deal_Ticket_Client_Hier_Workflow_Status_Id], t.[Deal_Ticket_Client_Hier_Id], t.[Workflow_Status_Map_Id], t.[Workflow_Status_Comment], t.[Is_Active], t.[Last_Notification_Sent_Ts], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[CBMS_Image_Id], t.[Trade_Month], t.[Workflow_Task_Id], t.[Contract_Id] 
		from [cdc].[hvr_1268729662_CT] t with (nolock) inner join 
		(	select  r.[Deal_Ticket_Client_Hier_Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_E325975C,
		    count(*) as __$count_E325975C 
			from [cdc].[hvr_1268729662_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_E325975C and
		    ( (t.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = m.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1268729662_CT] c with (nolock) 
							where  ( (c.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = t.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_E325975C
	    when 1 then __$operation
	    else
			case __$min_op_E325975C 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_E325975C
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_E325975C 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [Deal_Ticket_Client_Hier_Workflow_Status_Id], [Deal_Ticket_Client_Hier_Id], [Workflow_Status_Map_Id], [Workflow_Status_Comment], [Is_Active], [Last_Notification_Sent_Ts], [Created_Ts], [Updated_User_Id], [Last_Change_Ts], [CBMS_Image_Id], [Trade_Month], [Workflow_Task_Id], [Contract_Id]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_E325975C 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1268729662_CT] c with (nolock)
			where  ( (c.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = t.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_E325975C, __$count_E325975C, 
		m.__$update_mask , t.[Deal_Ticket_Client_Hier_Workflow_Status_Id], t.[Deal_Ticket_Client_Hier_Id], t.[Workflow_Status_Map_Id], t.[Workflow_Status_Comment], t.[Is_Active], t.[Last_Notification_Sent_Ts], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[CBMS_Image_Id], t.[Trade_Month], t.[Workflow_Task_Id], t.[Contract_Id]
		from [cdc].[hvr_1268729662_CT] t with (nolock) inner join 
		(	select  r.[Deal_Ticket_Client_Hier_Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_E325975C,
		    count(*) as __$count_E325975C, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_1268729662_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_E325975C and
		    ( (t.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = m.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1268729662_CT] c with (nolock)
							where  ( (c.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = t.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[Deal_Ticket_Client_Hier_Workflow_Status_Id], t.[Deal_Ticket_Client_Hier_Id], t.[Workflow_Status_Map_Id], t.[Workflow_Status_Comment], t.[Is_Active], t.[Last_Notification_Sent_Ts], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[CBMS_Image_Id], t.[Trade_Month], t.[Workflow_Task_Id], t.[Contract_Id]
		from [cdc].[hvr_1268729662_CT] t  with (nolock) inner join 
		(	select  r.[Deal_Ticket_Client_Hier_Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_E325975C
			from [cdc].[hvr_1268729662_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_E325975C and
		    ( (t.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = m.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1268729662_CT] c with (nolock)
							where  ( (c.[Deal_Ticket_Client_Hier_Workflow_Status_Id] = t.[Deal_Ticket_Client_Hier_Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_1268729662] TO [public]
GO
