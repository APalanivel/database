SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




Create  FUNCTION [dbo].[dmlLookupType_GetId]
	( @LookupTypeName varchar(50)
	) returns int
AS
BEGIN
	return (
		select t.LookupTypeId
        	  from dmlLookupType t 
	         where t.LookupTypeName = @LookupTypeName
		)
END




GO
GRANT EXECUTE ON  [dbo].[dmlLookupType_GetId] TO [CBMSApplication]
GO
