SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--select dbo.SR_RFP_FN_GET_UTILITIES_UNDER_SITE (10349,1)


CREATE  FUNCTION dbo.SR_RFP_FN_GET_UTILITIES_UNDER_SITE(@siteId int,@rfpId int) 
RETURNS varchar(2000) 
AS 



BEGIN 
	DECLARE @allUtilitiesName varchar(2000), @currentUtilityName varchar(200)
	DECLARE C_UTILITY CURSOR FAST_FORWARD
	FOR 
		SELECT VENDOR.VENDOR_NAME
		FROM VENDOR,ACCOUNT,SITE,SR_RFP_ACCOUNT
		WHERE ACCOUNT.SITE_ID = SITE.SITE_ID AND 
		ACCOUNT.VENDOR_ID=VENDOR.VENDOR_ID AND
		SR_RFP_ACCOUNT.ACCOUNT_ID=ACCOUNT.ACCOUNT_ID AND
		SR_RFP_ACCOUNT.is_deleted = 0 and
		SITE.SITE_ID=@siteId AND SR_RFP_ACCOUNT.ACCOUNT_ID IN 
		(SELECT ACCOUNT_ID FROM ACCOUNT WHERE SITE_ID=@siteId)AND
		SR_RFP_ACCOUNT.SR_RFP_ID=@rfpId 

	OPEN C_UTILITY
	FETCH NEXT FROM C_UTILITY INTO @currentUtilityName
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			IF @allUtilitiesName IS NULL OR  @allUtilitiesName=@currentUtilityName
				SELECT @allUtilitiesName = @currentUtilityName 
			ELSE if @allUtilitiesName IS NOT NULL and @allUtilitiesName!=@currentUtilityName
				--SELECT @allUtilitiesName = @allUtilitiesName +', '+ @currentUtilityName 
				SELECT @allUtilitiesName='Multiple Utilities'
		END
	FETCH NEXT FROM C_UTILITY INTO @currentUtilityName
	END
	CLOSE C_UTILITY
	DEALLOCATE C_UTILITY

RETURN @allUtilitiesName
END 













GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_UTILITIES_UNDER_SITE] TO [CBMSApplication]
GO
