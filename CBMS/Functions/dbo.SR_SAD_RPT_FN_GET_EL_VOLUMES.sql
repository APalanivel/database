
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	CBMS.dbo.SR_SAD_RPT_FN_GET_EL_VOLUMES
		
DESCRIPTION:
    This function is to get the available volumes 

INPUT PARAMETERS:
    Name		 DataType	   Default		Description
------------------------------------------------------------
    @AccountID	 INT
    @FromDate	 VARCHAR(12)
    @AsOfDate	 VARCHAR(12)


OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
    SELECT dbo.SR_SAD_RPT_FN_GET_EL_VOLUMES(6678,'01/01/2006','04/12/2006')


AUTHOR INITIALS:
    Initials	 Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi

MODIFICATIONS:
    Initials	 Date	   Modification
------------------------------------------------------------
    DMR		 09/10/2010  Modified for Quoted_Identifier
    AP		 07/28/2011  Modified the SP logic and Remove Cost_Usage table & use Cost_Usage_Account_Dtl, Bucket_Master, Code, Commodity tables
    AP		 08/22/2011  Added qualifiers to all objects with the owner name.
    AP		 03/29/2012  Removed NOLOCK hints and multiple SELECTs by using CASE Statement in the 1st select
******/

CREATE FUNCTION [dbo].[SR_SAD_RPT_FN_GET_EL_VOLUMES]
      ( 
       @AccountID INT
      ,@FromDate VARCHAR(12)
      ,@AsOfDate VARCHAR(12) )
RETURNS DECIMAL(32, 16)
AS 
BEGIN 
      DECLARE
            @MaxValue DECIMAL(32, 16)
           ,@Demand_Usage DECIMAL(32, 16)
           ,@Billed_Demand_Usage DECIMAL(32, 16)
           ,@UOM_Entity_ID INT
           ,@Commodity_Id INT
    
      SELECT
            @UOM_Entity_ID = UOM.Entity_ID
      FROM
            dbo.Entity UOM
      WHERE
            UOM.Entity_Description = 'Unit for electricity'
            AND UOM.Entity_Name = 'kW'

      SELECT
            @Commodity_Id = com.Commodity_Id
      FROM
            dbo.Commodity COM
      WHERE
            COM.Commodity_Name = 'Electric Power'


      SELECT
            @Demand_Usage = max(case WHEN bm.Bucket_Name = 'Demand' THEN CUAD.Bucket_Value
                                END)
           ,@Billed_Demand_Usage = max(case WHEN bm.Bucket_Name = 'Billed Demand' THEN CUAD.Bucket_Value
                                       END)
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Code CD
                  ON CD.Code_Id = BM.Bucket_Type_Cd
      WHERE
            CUAD.Account_ID = @AccountID
            AND CUAD.Service_Month BETWEEN convert(DATE, @FromDate)
                                   AND     convert(DATE, @AsOfDate)
            AND CUAD.UOM_Type_Id = @UOM_Entity_ID
            AND bm.Commodity_Id = @Commodity_Id
            AND CD.Code_Value = 'Determinant'
            AND BM.Bucket_Name IN ( 'Demand', 'Billed Demand' )
            AND BM.Is_Category = 1
	    

      SET @MaxValue = @Demand_Usage
	
      SELECT
            @MaxValue = @Billed_Demand_Usage
      WHERE
            @MaxValue < @Billed_Demand_Usage


      RETURN @MaxValue

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_EL_VOLUMES] TO [CBMSApplication]
GO
