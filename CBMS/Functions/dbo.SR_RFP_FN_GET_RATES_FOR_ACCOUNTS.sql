SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_RFP_FN_GET_RATES_FOR_ACCOUNTS(1440,1799, 290)
--select dbo.SR_RFP_FN_GET_RATES_FOR_ACCOUNTS(1440,0,290)
--select * from rate where rate_name like '%SC11 Negotiated%'


CREATE       FUNCTION [dbo].[SR_RFP_FN_GET_RATES_FOR_ACCOUNTS](@account_id int, @rateId int, @commodityId int) 
RETURNS varchar(2000) 
AS 



BEGIN 
	declare @st varchar(2000), @rate varchar(200)
	IF @rateId > 0
	BEGIN
		
		set @st=''
		select @st=@st+', '+rate_name from rate r
		where rate_id in (select distinct m.rate_id 
			from 	meter m(nolock)		     
			where 	m.account_id =@account_id			
				and m.rate_id = @rateId
				and r.commodity_type_id = @commodityId)
		
		
	END
	ELSE IF @rateId = 0
	BEGIN
		set @st=''
		select @st=@st+', '+rate_name from rate r
		where rate_id in (select distinct m.rate_id 
			from 	meter m(nolock)		     
			where 	m.account_id =@account_id
				and r.commodity_type_id = @commodityId)
	END
	if @st=''
	set @st = null
return substring(@st,3,len(@st))
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RATES_FOR_ACCOUNTS] TO [CBMSApplication]
GO
