SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.udf_StripNonAlphaNumerics

DESCRIPTION:
	This is a function to remove any non alpha numeric characters. The with SCHEMABINDING is required to
	make this function deterministic and available for use within computed columns.


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     @string        varchar	 

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT dbo.udf_StripNonAlphaNumerics('asdf$%adsf!#4')

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	 DMR		Deana Ritter
MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
    DMR			02/07/2015	Created from http://stackoverflow.com/questions/9636045/t-sql-strip-all-non-alpha-and-non-numeric-characters
								        	
******/

CREATE FUNCTION [dbo].[udf_StripNonAlphaNumerics]
(
  @s VARCHAR(max)
)
RETURNS VARCHAR(max)
WITH SCHEMABINDING
AS
BEGIN
  DECLARE @p INT = 1, @n VARCHAR(max) = '';
  WHILE @p <= len(@s)
  BEGIN
    IF substring(@s, @p, 1) LIKE '[A-Za-z0-9]'
    BEGIN
      SET @n += substring(@s, @p, 1);
    END 
    SET @p += 1;
  END
  RETURN(@n);
END




GO
GRANT EXECUTE ON  [dbo].[udf_StripNonAlphaNumerics] TO [CBMSApplication]
GO
