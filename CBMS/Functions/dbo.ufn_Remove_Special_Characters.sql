SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.ufn_Remove_string

DESCRIPTION:
	This is a function to remove special characters. The list of special characters are picked from
	App_Config and passed on to this function as a parameter


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     @string        varchar
	 @invChars    	varchar       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT dbo.ufn_Remove_Special_Characters('asdf$%adsf!#4','%#$')

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
    KC        	08/26/2009		
    KC          08/27/2009      Modified the varchar argument as max. Use max for sql 2005/2008, 
								If need to use for SQL2000 use a discrete value
	KVK			11/15/2018		Changed datatype from VARCHAR to NVARCHAR					        	
******/


CREATE FUNCTION [dbo].[ufn_Remove_Special_Characters](  
	@expression NVARCHAR(MAX),
	@invalid_string VARCHAR(100)
)
RETURNS NVARCHAR(MAX)
with execute as SELF AS
BEGIN

	DECLARE @counter INT,
		@invalid_Char_Length INT  

	SET @counter = 0
	SET @invalid_Char_Length = LEN(@invalid_string)
  
	WHILE @counter <= @invalid_Char_Length
	 BEGIN

		SET @expression = REPLACE(@expression, SUBSTRING(@invalid_string, (@invalid_Char_Length - @counter), 1), SPACE(0))
		SET @counter = @counter + 1

	 END

	RETURN @expression

END


GO
GRANT EXECUTE ON  [dbo].[ufn_Remove_Special_Characters] TO [CBMSApplication]
GO
