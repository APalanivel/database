SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[SR_RFP_FN_GET_CONTRACT_NUMBER](@contract_id int) 
RETURNS varchar(100)
AS 

BEGIN 
	RETURN (select ed_contract_number from contract(nolock) where contract_id = @contract_id)
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_CONTRACT_NUMBER] TO [CBMSApplication]
GO
