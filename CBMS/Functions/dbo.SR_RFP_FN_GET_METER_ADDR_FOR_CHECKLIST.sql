SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION dbo.SR_RFP_FN_GET_METER_ADDR_FOR_CHECKLIST(@account_id INT, @rfpId INT)
	RETURNS VARCHAR(4000)
AS
BEGIN

	DECLARE @meterStr VARCHAR(4000)
	
	SELECT @meterStr = COALESCE(@meterStr + ', ','') + m.meter_number+' '+ addr.address_line1  
	FROM meter m
		JOIN dbo.address addr ON addr.address_id = m.address_id
		JOIN dbo.sr_rfp_account_meter_map map ON map.meter_id = m.meter_id
		JOIN dbo.sr_rfp_account rfpAccount ON rfpAccount.account_id = m.account_id
			AND rfpAccount.sr_rfp_account_id = map.sr_rfp_account_id
		JOIN dbo.sr_rfp rfp ON rfp.sr_rfp_id = rfpAccount.sr_rfp_id
	WHERE m.account_id =@account_id
		AND rfp.sr_rfp_id = @rfpId
		AND rfpAccount.is_deleted = 0		
	GROUP BY m.meter_number
		, addr.address_line1
     
	RETURN @meterStr

END

GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_METER_ADDR_FOR_CHECKLIST] TO [CBMSApplication]
GO
