SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[SR_RFP_FN_GET_RC_DOCUMENT_IMAGE_ID]( @accountId int) 
RETURNS bigint

AS 



BEGIN 
	DECLARE @cbmsImageId int
	
	
		select @cbmsImageId =  	rcContract.rc_image_id 
			from 	SR_RC_CONTRACT_DOCUMENT rcContract(nolock),
				SR_RC_CONTRACT_DOCUMENT_ACCOUNTS_MAP accMap(nolock)
			where 	rcContract.sr_rc_contract_document_id = accMap.sr_rc_contract_document_id
				and accMap.account_id =  @accountId
				
	

	RETURN @cbmsImageId
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RC_DOCUMENT_IMAGE_ID] TO [CBMSApplication]
GO
