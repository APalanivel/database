SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_990352480]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [CBMS_IMAGE_ID], NULL as [CBMS_IMAGE_TYPE_ID], NULL as [CBMS_DOC_ID], NULL as [DATE_IMAGED], NULL as [BILLING_DAYS_ADJUSTMENT], NULL as [CBMS_IMAGE_SIZE], NULL as [CONTENT_TYPE], NULL as [INV_SOURCED_IMAGE_ID], NULL as [is_reported], NULL as [Cbms_Image_Path], NULL as [App_ConfigID], NULL as [CBMS_Image_Directory], NULL as [CBMS_Image_FileName], NULL as [CBMS_Image_Location_Id], NULL as [Last_Change_Ts], NULL as [msrepl_tran_version], NULL as [CBMS_DOC_ID_FTSearch]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_FDB47C88
	    when 1 then __$operation
	    else
			case __$min_op_FDB47C88 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [CBMS_IMAGE_ID], [CBMS_IMAGE_TYPE_ID], [CBMS_DOC_ID], [DATE_IMAGED], [BILLING_DAYS_ADJUSTMENT], [CBMS_IMAGE_SIZE], [CONTENT_TYPE], [INV_SOURCED_IMAGE_ID], [is_reported], [Cbms_Image_Path], [App_ConfigID], [CBMS_Image_Directory], [CBMS_Image_FileName], [CBMS_Image_Location_Id], [Last_Change_Ts], [msrepl_tran_version], [CBMS_DOC_ID_FTSearch]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_FDB47C88 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_990352480_CT] c with (nolock)   
			where  ( (c.[CBMS_IMAGE_ID] = t.[CBMS_IMAGE_ID]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_FDB47C88, __$count_FDB47C88, t.[CBMS_IMAGE_ID], t.[CBMS_IMAGE_TYPE_ID], t.[CBMS_DOC_ID], t.[DATE_IMAGED], t.[BILLING_DAYS_ADJUSTMENT], t.[CBMS_IMAGE_SIZE], t.[CONTENT_TYPE], t.[INV_SOURCED_IMAGE_ID], t.[is_reported], t.[Cbms_Image_Path], t.[App_ConfigID], t.[CBMS_Image_Directory], t.[CBMS_Image_FileName], t.[CBMS_Image_Location_Id], t.[Last_Change_Ts], t.[msrepl_tran_version], t.[CBMS_DOC_ID_FTSearch] 
		from [cdc].[hvr_990352480_CT] t with (nolock) inner join 
		(	select  r.[CBMS_IMAGE_ID], max(r.__$seqval) as __$max_seqval_FDB47C88,
		    count(*) as __$count_FDB47C88 
			from [cdc].[hvr_990352480_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[CBMS_IMAGE_ID]) m
		on t.__$seqval = m.__$max_seqval_FDB47C88 and
		    ( (t.[CBMS_IMAGE_ID] = m.[CBMS_IMAGE_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_990352480_CT] c with (nolock) 
							where  ( (c.[CBMS_IMAGE_ID] = t.[CBMS_IMAGE_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_FDB47C88
	    when 1 then __$operation
	    else
			case __$min_op_FDB47C88 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_FDB47C88
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_FDB47C88 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [CBMS_IMAGE_ID], [CBMS_IMAGE_TYPE_ID], [CBMS_DOC_ID], [DATE_IMAGED], [BILLING_DAYS_ADJUSTMENT], [CBMS_IMAGE_SIZE], [CONTENT_TYPE], [INV_SOURCED_IMAGE_ID], [is_reported], [Cbms_Image_Path], [App_ConfigID], [CBMS_Image_Directory], [CBMS_Image_FileName], [CBMS_Image_Location_Id], [Last_Change_Ts], [msrepl_tran_version], [CBMS_DOC_ID_FTSearch]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_FDB47C88 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_990352480_CT] c with (nolock)
			where  ( (c.[CBMS_IMAGE_ID] = t.[CBMS_IMAGE_ID]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_FDB47C88, __$count_FDB47C88, 
		m.__$update_mask , t.[CBMS_IMAGE_ID], t.[CBMS_IMAGE_TYPE_ID], t.[CBMS_DOC_ID], t.[DATE_IMAGED], t.[BILLING_DAYS_ADJUSTMENT], t.[CBMS_IMAGE_SIZE], t.[CONTENT_TYPE], t.[INV_SOURCED_IMAGE_ID], t.[is_reported], t.[Cbms_Image_Path], t.[App_ConfigID], t.[CBMS_Image_Directory], t.[CBMS_Image_FileName], t.[CBMS_Image_Location_Id], t.[Last_Change_Ts], t.[msrepl_tran_version], t.[CBMS_DOC_ID_FTSearch]
		from [cdc].[hvr_990352480_CT] t with (nolock) inner join 
		(	select  r.[CBMS_IMAGE_ID], max(r.__$seqval) as __$max_seqval_FDB47C88,
		    count(*) as __$count_FDB47C88, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_990352480_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[CBMS_IMAGE_ID]) m
		on t.__$seqval = m.__$max_seqval_FDB47C88 and
		    ( (t.[CBMS_IMAGE_ID] = m.[CBMS_IMAGE_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_990352480_CT] c with (nolock)
							where  ( (c.[CBMS_IMAGE_ID] = t.[CBMS_IMAGE_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[CBMS_IMAGE_ID], t.[CBMS_IMAGE_TYPE_ID], t.[CBMS_DOC_ID], t.[DATE_IMAGED], t.[BILLING_DAYS_ADJUSTMENT], t.[CBMS_IMAGE_SIZE], t.[CONTENT_TYPE], t.[INV_SOURCED_IMAGE_ID], t.[is_reported], t.[Cbms_Image_Path], t.[App_ConfigID], t.[CBMS_Image_Directory], t.[CBMS_Image_FileName], t.[CBMS_Image_Location_Id], t.[Last_Change_Ts], t.[msrepl_tran_version], t.[CBMS_DOC_ID_FTSearch]
		from [cdc].[hvr_990352480_CT] t  with (nolock) inner join 
		(	select  r.[CBMS_IMAGE_ID], max(r.__$seqval) as __$max_seqval_FDB47C88
			from [cdc].[hvr_990352480_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[CBMS_IMAGE_ID]) m
		on t.__$seqval = m.__$max_seqval_FDB47C88 and
		    ( (t.[CBMS_IMAGE_ID] = m.[CBMS_IMAGE_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_990352480_CT] c with (nolock)
							where  ( (c.[CBMS_IMAGE_ID] = t.[CBMS_IMAGE_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_990352480] TO [public]
GO
