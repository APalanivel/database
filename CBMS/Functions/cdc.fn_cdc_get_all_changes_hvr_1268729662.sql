SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_1268729662]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Deal_Ticket_Client_Hier_Workflow_Status_Id], NULL as [Deal_Ticket_Client_Hier_Id], NULL as [Workflow_Status_Map_Id], NULL as [Workflow_Status_Comment], NULL as [Is_Active], NULL as [Last_Notification_Sent_Ts], NULL as [Created_Ts], NULL as [Updated_User_Id], NULL as [Last_Change_Ts], NULL as [CBMS_Image_Id], NULL as [Trade_Month], NULL as [Workflow_Task_Id], NULL as [Contract_Id]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Deal_Ticket_Client_Hier_Workflow_Status_Id], t.[Deal_Ticket_Client_Hier_Id], t.[Workflow_Status_Map_Id], t.[Workflow_Status_Comment], t.[Is_Active], t.[Last_Notification_Sent_Ts], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[CBMS_Image_Id], t.[Trade_Month], t.[Workflow_Task_Id], t.[Contract_Id]
	from [cdc].[hvr_1268729662_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Deal_Ticket_Client_Hier_Workflow_Status_Id], t.[Deal_Ticket_Client_Hier_Id], t.[Workflow_Status_Map_Id], t.[Workflow_Status_Comment], t.[Is_Active], t.[Last_Notification_Sent_Ts], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[CBMS_Image_Id], t.[Trade_Month], t.[Workflow_Task_Id], t.[Contract_Id]
	from [cdc].[hvr_1268729662_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1268729662', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_1268729662] TO [public]
GO
