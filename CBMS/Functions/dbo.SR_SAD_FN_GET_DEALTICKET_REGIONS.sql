SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





--select dbo.SR_SAD_FN_GET_DEALTICKET_REGIONS(1)


CREATE        FUNCTION dbo.SR_SAD_FN_GET_DEALTICKET_REGIONS(@dealTicketId int) 

RETURNS varchar(2000) 
AS 

BEGIN 
	

	declare @st varchar(2000)
	set @st=''
		select 	@st=@st+', '+rg.region_name 
		from 	SITE st , SR_DEAL_TICKET srDT, account acc, 
			SR_DEAL_TICKET_ACCOUNT_MAP accMap, REGION rg,
			STATE state, ADDRESS address	
		where 	srDT.SR_DEAL_TICKET_ID = accMap.SR_DEAL_TICKET_ID
			and accMap.account_id = acc.account_id
			and st.primary_address_id = address.address_id 
			and address.state_id = state.state_id 
			and state.region_id = rg.region_id
			and acc.site_id = st.site_id
			and accMap.SR_DEAL_TICKET_ID = @dealTicketId
	if @st=''
	set @st = null
return substring(@st,3,len(@st))
end




GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_DEALTICKET_REGIONS] TO [CBMSApplication]
GO
