SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_990352480]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [CBMS_IMAGE_ID], NULL as [CBMS_IMAGE_TYPE_ID], NULL as [CBMS_DOC_ID], NULL as [DATE_IMAGED], NULL as [BILLING_DAYS_ADJUSTMENT], NULL as [CBMS_IMAGE_SIZE], NULL as [CONTENT_TYPE], NULL as [INV_SOURCED_IMAGE_ID], NULL as [is_reported], NULL as [Cbms_Image_Path], NULL as [App_ConfigID], NULL as [CBMS_Image_Directory], NULL as [CBMS_Image_FileName], NULL as [CBMS_Image_Location_Id], NULL as [Last_Change_Ts], NULL as [msrepl_tran_version], NULL as [CBMS_DOC_ID_FTSearch]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[CBMS_IMAGE_ID], t.[CBMS_IMAGE_TYPE_ID], t.[CBMS_DOC_ID], t.[DATE_IMAGED], t.[BILLING_DAYS_ADJUSTMENT], t.[CBMS_IMAGE_SIZE], t.[CONTENT_TYPE], t.[INV_SOURCED_IMAGE_ID], t.[is_reported], t.[Cbms_Image_Path], t.[App_ConfigID], t.[CBMS_Image_Directory], t.[CBMS_Image_FileName], t.[CBMS_Image_Location_Id], t.[Last_Change_Ts], t.[msrepl_tran_version], t.[CBMS_DOC_ID_FTSearch]
	from [cdc].[hvr_990352480_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[CBMS_IMAGE_ID], t.[CBMS_IMAGE_TYPE_ID], t.[CBMS_DOC_ID], t.[DATE_IMAGED], t.[BILLING_DAYS_ADJUSTMENT], t.[CBMS_IMAGE_SIZE], t.[CONTENT_TYPE], t.[INV_SOURCED_IMAGE_ID], t.[is_reported], t.[Cbms_Image_Path], t.[App_ConfigID], t.[CBMS_Image_Directory], t.[CBMS_Image_FileName], t.[CBMS_Image_Location_Id], t.[Last_Change_Ts], t.[msrepl_tran_version], t.[CBMS_DOC_ID_FTSearch]
	from [cdc].[hvr_990352480_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_990352480', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_990352480] TO [public]
GO
