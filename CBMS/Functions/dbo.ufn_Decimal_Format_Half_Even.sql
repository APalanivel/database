SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME: dbo.ufn_Decimal_Format_Half_Even
    
DESCRIPTION:    
    
     This function is retun to round the decimal by half even mode.
    
INPUT PARAMETERS:    
      NAME					DATATYPE				DEFAULT           DESCRIPTION    
------------------------------------------------------------------------------------------------------------------------	
    @Val					decimal(32,16)
	@digits					INT	
                    
OUTPUT PARAMETERS:              
      NAME              DATATYPE    DEFAULT           DESCRIPTION       
                       
USAGE EXAMPLES:              
------------------------------------------------------------------------------------------------------------------------          
      select [dbo].[ufn_Decimal_Format_Half_Even](3.5,0)
	  select [dbo].[ufn_Decimal_Format_Half_Even](4.5,0)

	  select [dbo].[ufn_Decimal_Format_Half_Even](3.9,0)
	  select [dbo].[ufn_Decimal_Format_Half_Even](3.3,0)
      
	  select [dbo].[ufn_Decimal_Format_Half_Even](4.9,0)
	  select [dbo].[ufn_Decimal_Format_Half_Even](4.3,0)
	   
AUTHOR INITIALS:              
      INITIALS    NAME              
------------------------------------------------------------------------------------------------------------------------             
	  KVK		  Vinay Kacham
              
MODIFICATIONS:    
      INITIALS    DATE   MODIFICATION              
------------------------------------------------------------------------------------------------------------------------              
      RMG 		  03/07/2013	 CREATED
******/  


CREATE FUNCTION [dbo].[ufn_Decimal_Format_Half_Even]
      ( 
       @Val DECIMAL(32, 16)
      ,@Digits INT )
RETURNS DECIMAL(32, 16)
AS 
BEGIN
      RETURN case WHEN abs(@Val - round(@Val, @Digits, 1)) * power(10, @Digits+1) = 5
            THEN round(@Val, @Digits, case WHEN convert(INT, round(abs(@Val) * power(10,@Digits), 0, 1)) % 2 = 1 THEN 0 ELSE 1 END)
            ELSE round(@Val, @Digits)
        END
END
;
GO
GRANT EXECUTE ON  [dbo].[ufn_Decimal_Format_Half_Even] TO [CBMSApplication]
GO
