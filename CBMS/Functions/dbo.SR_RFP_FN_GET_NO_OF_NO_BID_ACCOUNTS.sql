SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  FUNCTION [dbo].[SR_RFP_FN_GET_NO_OF_NO_BID_ACCOUNTS](@rfp_id int) 
RETURNS int
AS 



BEGIN 
	DECLARE @count int, @cnt1 int, @cnt2 int
 	set @count = 0
	set @cnt1 = 0
	set @cnt2 = 0 

		select 	@cnt1 = isnull(count(nobid.sr_account_group_id),0)
		from	sr_sw_rfp_no_bid nobid(nolock),
			sr_rfp_account account(nolock)
		
		where	account.sr_rfp_account_id = nobid.sr_account_group_id
			and account.sr_rfp_id = @rfp_id
			and account.is_deleted = 0

		select 	@cnt2 = isnull(count(account.SR_RFP_ACCOUNT_ID),0)
		from	sr_sw_rfp_no_bid nobid(nolock),
			sr_rfp_account account(nolock)					
		where	account.sr_rfp_bid_group_id = nobid.sr_account_group_id
			and account.sr_rfp_id = @rfp_id					
			and account.is_deleted = 0

		set @count = @cnt1 + @cnt2
RETURN @count
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_NO_OF_NO_BID_ACCOUNTS] TO [CBMSApplication]
GO
