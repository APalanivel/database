SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--select dbo.SR_RFP_FN_GET_MANUAL_SOP_SMR_INFO (2,0,'Manual SOP')
--select dbo.SR_RFP_FN_GET_MANUAL_SOP_SMR_INFO (2,0,'SMR')
--select dbo.SR_RFP_FN_GET_FILENAME (1)


CREATE  FUNCTION dbo.SR_RFP_FN_GET_FILENAME(@cbmsImageId int) 
RETURNS varchar(200)
AS 


BEGIN 
	DECLARE @fileName varchar(200)
	
	select @fileName=cbms_doc_id from cbms_image where cbms_image_id=@cbmsImageId

RETURN @fileName
END 















GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_FILENAME] TO [CBMSApplication]
GO
