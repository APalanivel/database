SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_1511077265]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [SR_SUPPLIER_CONTACT_INFO_ID], NULL as [USER_INFO_ID], NULL as [WORK_PHONE], NULL as [CELL_PHONE], NULL as [FAX], NULL as [ADDRESS_LINE1], NULL as [ADDRESS_LINE2], NULL as [CITY], NULL as [STATE_ID], NULL as [ZIP], NULL as [ADDITIONAL_CONTACT_INFO], NULL as [VENDOR_TYPE_ID], NULL as [PRIVILEGE_TYPE_ID], NULL as [IS_RFP_PREFERENCE_WEBSITE], NULL as [IS_RFP_PREFERENCE_EMAIL], NULL as [IS_UPDATED], NULL as [EMAIL_SENT_DATE], NULL as [Last_Change_Ts], NULL as [Termination_Contact_Email_Address], NULL as [Is_Default_Termination_Contact_Email_For_Vendor], NULL as [Registration_Contact_Email_Address], NULL as [Is_Default_Registration_Contact_Email_For_Vendor], NULL as [Comment_Id]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_A264D2F7
	    when 1 then __$operation
	    else
			case __$min_op_A264D2F7 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [SR_SUPPLIER_CONTACT_INFO_ID], [USER_INFO_ID], [WORK_PHONE], [CELL_PHONE], [FAX], [ADDRESS_LINE1], [ADDRESS_LINE2], [CITY], [STATE_ID], [ZIP], [ADDITIONAL_CONTACT_INFO], [VENDOR_TYPE_ID], [PRIVILEGE_TYPE_ID], [IS_RFP_PREFERENCE_WEBSITE], [IS_RFP_PREFERENCE_EMAIL], [IS_UPDATED], [EMAIL_SENT_DATE], [Last_Change_Ts], [Termination_Contact_Email_Address], [Is_Default_Termination_Contact_Email_For_Vendor], [Registration_Contact_Email_Address], [Is_Default_Registration_Contact_Email_For_Vendor], [Comment_Id]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_A264D2F7 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1511077265_CT] c with (nolock)   
			where  ( (c.[SR_SUPPLIER_CONTACT_INFO_ID] = t.[SR_SUPPLIER_CONTACT_INFO_ID]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_A264D2F7, __$count_A264D2F7, t.[SR_SUPPLIER_CONTACT_INFO_ID], t.[USER_INFO_ID], t.[WORK_PHONE], t.[CELL_PHONE], t.[FAX], t.[ADDRESS_LINE1], t.[ADDRESS_LINE2], t.[CITY], t.[STATE_ID], t.[ZIP], t.[ADDITIONAL_CONTACT_INFO], t.[VENDOR_TYPE_ID], t.[PRIVILEGE_TYPE_ID], t.[IS_RFP_PREFERENCE_WEBSITE], t.[IS_RFP_PREFERENCE_EMAIL], t.[IS_UPDATED], t.[EMAIL_SENT_DATE], t.[Last_Change_Ts], t.[Termination_Contact_Email_Address], t.[Is_Default_Termination_Contact_Email_For_Vendor], t.[Registration_Contact_Email_Address], t.[Is_Default_Registration_Contact_Email_For_Vendor], t.[Comment_Id] 
		from [cdc].[hvr_1511077265_CT] t with (nolock) inner join 
		(	select  r.[SR_SUPPLIER_CONTACT_INFO_ID], max(r.__$seqval) as __$max_seqval_A264D2F7,
		    count(*) as __$count_A264D2F7 
			from [cdc].[hvr_1511077265_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[SR_SUPPLIER_CONTACT_INFO_ID]) m
		on t.__$seqval = m.__$max_seqval_A264D2F7 and
		    ( (t.[SR_SUPPLIER_CONTACT_INFO_ID] = m.[SR_SUPPLIER_CONTACT_INFO_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1511077265_CT] c with (nolock) 
							where  ( (c.[SR_SUPPLIER_CONTACT_INFO_ID] = t.[SR_SUPPLIER_CONTACT_INFO_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_A264D2F7
	    when 1 then __$operation
	    else
			case __$min_op_A264D2F7 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_A264D2F7
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_A264D2F7 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [SR_SUPPLIER_CONTACT_INFO_ID], [USER_INFO_ID], [WORK_PHONE], [CELL_PHONE], [FAX], [ADDRESS_LINE1], [ADDRESS_LINE2], [CITY], [STATE_ID], [ZIP], [ADDITIONAL_CONTACT_INFO], [VENDOR_TYPE_ID], [PRIVILEGE_TYPE_ID], [IS_RFP_PREFERENCE_WEBSITE], [IS_RFP_PREFERENCE_EMAIL], [IS_UPDATED], [EMAIL_SENT_DATE], [Last_Change_Ts], [Termination_Contact_Email_Address], [Is_Default_Termination_Contact_Email_For_Vendor], [Registration_Contact_Email_Address], [Is_Default_Registration_Contact_Email_For_Vendor], [Comment_Id]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_A264D2F7 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1511077265_CT] c with (nolock)
			where  ( (c.[SR_SUPPLIER_CONTACT_INFO_ID] = t.[SR_SUPPLIER_CONTACT_INFO_ID]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_A264D2F7, __$count_A264D2F7, 
		m.__$update_mask , t.[SR_SUPPLIER_CONTACT_INFO_ID], t.[USER_INFO_ID], t.[WORK_PHONE], t.[CELL_PHONE], t.[FAX], t.[ADDRESS_LINE1], t.[ADDRESS_LINE2], t.[CITY], t.[STATE_ID], t.[ZIP], t.[ADDITIONAL_CONTACT_INFO], t.[VENDOR_TYPE_ID], t.[PRIVILEGE_TYPE_ID], t.[IS_RFP_PREFERENCE_WEBSITE], t.[IS_RFP_PREFERENCE_EMAIL], t.[IS_UPDATED], t.[EMAIL_SENT_DATE], t.[Last_Change_Ts], t.[Termination_Contact_Email_Address], t.[Is_Default_Termination_Contact_Email_For_Vendor], t.[Registration_Contact_Email_Address], t.[Is_Default_Registration_Contact_Email_For_Vendor], t.[Comment_Id]
		from [cdc].[hvr_1511077265_CT] t with (nolock) inner join 
		(	select  r.[SR_SUPPLIER_CONTACT_INFO_ID], max(r.__$seqval) as __$max_seqval_A264D2F7,
		    count(*) as __$count_A264D2F7, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_1511077265_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[SR_SUPPLIER_CONTACT_INFO_ID]) m
		on t.__$seqval = m.__$max_seqval_A264D2F7 and
		    ( (t.[SR_SUPPLIER_CONTACT_INFO_ID] = m.[SR_SUPPLIER_CONTACT_INFO_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1511077265_CT] c with (nolock)
							where  ( (c.[SR_SUPPLIER_CONTACT_INFO_ID] = t.[SR_SUPPLIER_CONTACT_INFO_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[SR_SUPPLIER_CONTACT_INFO_ID], t.[USER_INFO_ID], t.[WORK_PHONE], t.[CELL_PHONE], t.[FAX], t.[ADDRESS_LINE1], t.[ADDRESS_LINE2], t.[CITY], t.[STATE_ID], t.[ZIP], t.[ADDITIONAL_CONTACT_INFO], t.[VENDOR_TYPE_ID], t.[PRIVILEGE_TYPE_ID], t.[IS_RFP_PREFERENCE_WEBSITE], t.[IS_RFP_PREFERENCE_EMAIL], t.[IS_UPDATED], t.[EMAIL_SENT_DATE], t.[Last_Change_Ts], t.[Termination_Contact_Email_Address], t.[Is_Default_Termination_Contact_Email_For_Vendor], t.[Registration_Contact_Email_Address], t.[Is_Default_Registration_Contact_Email_For_Vendor], t.[Comment_Id]
		from [cdc].[hvr_1511077265_CT] t  with (nolock) inner join 
		(	select  r.[SR_SUPPLIER_CONTACT_INFO_ID], max(r.__$seqval) as __$max_seqval_A264D2F7
			from [cdc].[hvr_1511077265_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[SR_SUPPLIER_CONTACT_INFO_ID]) m
		on t.__$seqval = m.__$max_seqval_A264D2F7 and
		    ( (t.[SR_SUPPLIER_CONTACT_INFO_ID] = m.[SR_SUPPLIER_CONTACT_INFO_ID]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1511077265_CT] c with (nolock)
							where  ( (c.[SR_SUPPLIER_CONTACT_INFO_ID] = t.[SR_SUPPLIER_CONTACT_INFO_ID]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_1511077265] TO [public]
GO
