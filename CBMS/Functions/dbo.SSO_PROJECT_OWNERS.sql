SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE function dbo.SSO_PROJECT_OWNERS(@sso_project_id int)
returns varchar(2000)
as
begin
 
 declare @st varchar(2000), @rate varchar(200)
 set @st=''
 select @st=@st+', '+owner_name from (select distinct owner_name from vwCbmsSSOProjectOwnerFlat where sso_project_id = @sso_project_id) x
 
return substring(@st,3,len(@st))
end
 
 
 

GO
GRANT EXECUTE ON  [dbo].[SSO_PROJECT_OWNERS] TO [CBMSApplication]
GO
