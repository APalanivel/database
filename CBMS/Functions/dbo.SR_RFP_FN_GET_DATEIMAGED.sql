SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











--select dbo.SR_RFP_FN_GET_MANUAL_SOP_SMR_INFO (2,0,'Manual SOP')
--select dbo.SR_RFP_FN_GET_MANUAL_SOP_SMR_INFO (2,0,'SMR')
--select dbo.SR_RFP_FN_GET_FILENAME (1) 
-- select dbo.SR_RFP_FN_GET_FILENAME(10)


CREATE   FUNCTION dbo.SR_RFP_FN_GET_DATEIMAGED(@cbmsImageId int) 
RETURNS datetime
AS 


BEGIN 
	DECLARE @dateImaged datetime
	
	select @dateImaged=date_imaged from cbms_image where cbms_image_id=@cbmsImageId

RETURN @dateImaged
END 

















GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_DATEIMAGED] TO [CBMSApplication]
GO
