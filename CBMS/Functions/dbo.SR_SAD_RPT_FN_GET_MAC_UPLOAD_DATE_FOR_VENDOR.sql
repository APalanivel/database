SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select  dbo.SR_SAD_RPT_FN_GET_MAC_UPLOAD_DATE_FOR_VENDOR(1091)
CREATE   FUNCTION  [dbo].[SR_SAD_RPT_FN_GET_MAC_UPLOAD_DATE_FOR_VENDOR](@supplier_id int)
returns varchar(10) 
AS
		
BEGIN
DECLARE @uploadDate datetime

set @uploadDate =(
select sr_sup_prof_doc_mac.uploaded_date from sr_supplier_profile_documents sr_sup_prof_doc_mac 

where sr_sup_prof_doc_mac.cbms_image_id = (

	select 
		max(sr_sup_prof_doc_mac.cbms_image_id)
		from
		 vendor supplier
		,sr_supplier_profile sr_sup_prof
		,sr_supplier_profile_documents sr_sup_prof_doc_mac
		,entity mac_type
		,cbms_image ci_image_mac
		
		where 
			mac_type.entity_id = ci_image_mac.cbms_image_type_id
			and  mac_type.entity_name = 'Mutual Confidentiality Agreement'
			and  mac_type.entity_type = 1035
			and ci_image_mac.cbms_image_id  = sr_sup_prof_doc_mac.cbms_image_id  
			and sr_sup_prof_doc_mac.sr_supplier_profile_id = sr_sup_prof.sr_supplier_profile_id
			and supplier.vendor_id = sr_sup_prof.vendor_id
			and supplier.vendor_id = @supplier_id
)
)
return convert(varchar(10), @uploadDate, 101) 
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_MAC_UPLOAD_DATE_FOR_VENDOR] TO [CBMSApplication]
GO
