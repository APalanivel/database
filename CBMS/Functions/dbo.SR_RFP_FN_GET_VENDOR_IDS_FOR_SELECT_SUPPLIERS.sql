SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from sr_rfp_account where sr_rfp_id =70
--select dbo.SR_RFP_FN_GET_VENDOR_IDS_FOR_SELECT_SUPPLIERS (8583)
--select * from entity where entity_type =155

CREATE       FUNCTION [dbo].[SR_RFP_FN_GET_VENDOR_IDS_FOR_SELECT_SUPPLIERS](@account_id int) 

RETURNS varchar(2000) 
AS 

BEGIN 
	DECLARE @contractStr varchar(2000) 
		

	set @contractStr = ''
	
	select @contractStr = v.vendor_id
	from account a,vendor v
	where a.vendor_id = v.vendor_id 
	and a.account_id = @account_id 
	and v.vendor_type_id = (select entity_id from entity where entity_name ='utility' and entity_type =155)


RETURN @contractStr
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_VENDOR_IDS_FOR_SELECT_SUPPLIERS] TO [CBMSApplication]
GO
