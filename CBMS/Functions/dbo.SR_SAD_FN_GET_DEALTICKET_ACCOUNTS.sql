SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--select dbo.SR_SAD_FN_GET_DEALTICKET_ACCOUNTS(4)


CREATE      FUNCTION dbo.SR_SAD_FN_GET_DEALTICKET_ACCOUNTS(@dealTicketId int) 

RETURNS varchar(2000) 
AS 

BEGIN 
	

	declare @st varchar(2000), @rate varchar(200)
	set @st=''
		select @st=@st+', '+acc.account_number 
		from SR_DEAL_TICKET_ACCOUNT_MAP accMap , SR_DEAL_TICKET srDT, account acc
		where srDT.SR_DEAL_TICKET_ID = accMap.SR_DEAL_TICKET_ID
		and accMap.account_id = acc.account_id
		and accMap.SR_DEAL_TICKET_ID = @dealTicketId

	if @st=''
	set @st = null
return substring(@st,3,len(@st))
end















GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_DEALTICKET_ACCOUNTS] TO [CBMSApplication]
GO
