
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	cbms_prod.dbo.SR_RFP_GET_RFP_CREDIT_INFORMATION_P

DESCRIPTION:


INPUT PARAMETERS:
	Name					DataType	Default	Description
-----------------------------------------------------------------      	
	@sr_account_group_id	INT
    @is_bid_group			BIT    	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	EXEC SR_RFP_GET_RFP_CREDIT_INFORMATION_P 13260,0,0,1,200 
	EXEC SR_RFP_GET_RFP_SUMMARY_P 13260,0,0,1 ,100 

	SELECT * FROM dbo.SR_RFP_ACCOUNT WHERE SR_RFP_ID = 13260

	SELECT dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE (10011989,1)
	SELECT dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE (12104610,0) + 
				dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE (12104611,0) + dbo.SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE (12104612,0)



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
	RR			2016-04-25	GCS-761 Modified the output bigint to decimal as the total sum of individual account differing on
							complaring group sum as it is decimal

******/
CREATE        FUNCTION [dbo].[SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE]
      ( 
       @sr_account_group_id INT
      ,@is_bid_group BIT )
RETURNS DECIMAL(32, 16)
AS 
BEGIN 
      DECLARE @total_lp_size DECIMAL(32, 16)
      SET @total_lp_size = 0
      DECLARE
            @commodity VARCHAR(50)
           ,@rfp_account_id INT
           ,@converted_unit_id INT
           ,@lp_size DECIMAL(32, 16)
           
      IF @is_bid_group = 1 
            BEGIN
                  SELECT
                        @commodity = en.entity_name
                  FROM
                        dbo.SR_RFP_ACCOUNT rfp_account
                        INNER JOIN dbo.SR_RFP rfp
                              ON rfp.sr_rfp_id = rfp_account.sr_rfp_id
                        INNER JOIN dbo.ENTITY en
                              ON en.entity_id = rfp.commodity_type_id
                  WHERE
                        rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
                        AND rfp_account.is_deleted = 0
                  GROUP BY
                        en.entity_name
                  
                  IF @commodity = 'Natural Gas' 
                        BEGIN
                              SELECT
                                    @converted_unit_id = entity_id
                              FROM
                                    entity
                              WHERE
                                    entity_type = 102
                                    AND entity_name = 'MMBtu'
                              SELECT
                                    @total_lp_size = sum(value.lp_value * conv.conversion_factor)
                              FROM
                                    dbo.SR_RFP_LOAD_PROFILE_SETUP setup
                                    INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT determinant
                                          ON setup.SR_RFP_LOAD_PROFILE_SETUP_ID = determinant.SR_RFP_LOAD_PROFILE_SETUP_ID
                                    INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES value
                                          ON determinant.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = value.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                                    INNER JOIN dbo.ENTITY en
                                          ON value.reading_type_id = en.entity_id
                                    INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION conv
                                          ON conv.base_unit_id = determinant.determinant_unit_type_id
                              WHERE
                                    EXISTS ( SELECT
                                                1
                                             FROM
                                                sr_rfp_account rfp_account
                                             WHERE
                                                rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
                                                AND rfp_account.is_deleted = 0
                                                AND rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id )
                                    AND en.entity_type = 1031
                                    AND ( en.entity_name = 'Actual LP Value'
                                          OR en.entity_name = 'Actual Avg LP Value' )
                                    AND value.lp_value IS NOT NULL
                                    AND conv.converted_unit_id = @converted_unit_id
                        END
                  ELSE 
                        IF @commodity = 'Electric Power' 
                              BEGIN
                                    SELECT
                                          @converted_unit_id = entity_id
                                    FROM
                                          entity
                                    WHERE
                                          entity_type = 101
                                          AND entity_name = 'kW'
					
                                    SET @total_lp_size = 0
                                    SELECT
                                          @total_lp_size = @total_lp_size + max(value.lp_value)
                                    FROM
                                          dbo.SR_RFP_ACCOUNT rfp_account
                                          INNER JOIN dbo.SR_RFP_LOAD_PROFILE_SETUP setup
                                                ON rfp_account.SR_RFP_ACCOUNT_ID = setup.SR_RFP_ACCOUNT_ID
                                          INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT determinant
                                                ON setup.SR_RFP_LOAD_PROFILE_SETUP_ID = determinant.SR_RFP_LOAD_PROFILE_SETUP_ID
                                          INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES value
                                                ON determinant.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = value.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                                          INNER JOIN dbo.ENTITY en
                                                ON value.reading_type_id = en.entity_id
                                    WHERE
                                          rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
                                          AND rfp_account.is_deleted = 0
                                          AND en.entity_type = 1031
                                          AND ( en.entity_name = 'Actual LP Value'
                                                OR en.entity_name = 'Actual Avg LP Value' )
                                          AND value.lp_value IS NOT NULL
                                          AND determinant.determinant_unit_type_id = @converted_unit_id
                                    GROUP BY
                                          rfp_account.sr_rfp_account_id
					
                              END
						
            END
      ELSE 
            BEGIN
                  SELECT
                        @commodity = en.entity_name
                  FROM
                        sr_rfp_account rfp_account
                       ,sr_rfp rfp
                       ,entity en
                  WHERE
                        rfp_account.sr_rfp_account_id = @sr_account_group_id
                        AND rfp_account.is_deleted = 0
                        AND rfp.sr_rfp_id = rfp_account.sr_rfp_id
                        AND en.entity_id = rfp.commodity_type_id	
                  IF @commodity = 'Natural Gas' 
                        BEGIN
                              SELECT
                                    @converted_unit_id = entity_id
                              FROM
                                    entity
                              WHERE
                                    entity_type = 102
                                    AND entity_name = 'MMBtu'
                              SELECT
                                    @total_lp_size = sum(value.lp_value * conv.conversion_factor)
                              FROM
                                    dbo.SR_RFP_LOAD_PROFILE_SETUP setup
                                    INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT determinant
                                          ON setup.SR_RFP_LOAD_PROFILE_SETUP_ID = determinant.SR_RFP_LOAD_PROFILE_SETUP_ID
                                    INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES value
                                          ON determinant.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = value.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                                    INNER JOIN dbo.ENTITY en
                                          ON value.reading_type_id = en.entity_id
                                    INNER JOIN dbo.CONSUMPTION_UNIT_CONVERSION conv
                                          ON conv.base_unit_id = determinant.determinant_unit_type_id
                                    INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account
                                          ON rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
                              WHERE
                                    setup.sr_rfp_account_id = @sr_account_group_id
                                    AND rfp_account.is_deleted = 0
                                    AND en.entity_type = 1031
                                    AND ( en.entity_name = 'Actual LP Value'
                                          OR en.entity_name = 'Actual Avg LP Value' )
                                    AND value.lp_value IS NOT NULL
                                    AND conv.converted_unit_id = @converted_unit_id

                        END
                  ELSE 
                        IF @commodity = 'Electric Power' 
                              BEGIN
                                    SELECT
                                          @converted_unit_id = entity_id
                                    FROM
                                          entity
                                    WHERE
                                          entity_type = 101
                                          AND entity_name = 'kW'
                                    SELECT
                                          @total_lp_size = max(value.lp_value)
                                    FROM
                                          dbo.SR_RFP_LOAD_PROFILE_SETUP setup
                                          INNER JOIN dbo.SR_RFP_LOAD_PROFILE_DETERMINANT determinant
                                                ON setup.SR_RFP_LOAD_PROFILE_SETUP_ID = determinant.SR_RFP_LOAD_PROFILE_SETUP_ID
                                          INNER JOIN dbo.SR_RFP_LP_DETERMINANT_VALUES value
                                                ON determinant.SR_RFP_LOAD_PROFILE_DETERMINANT_ID = value.SR_RFP_LOAD_PROFILE_DETERMINANT_ID
                                          INNER JOIN dbo.ENTITY en
                                                ON value.READING_TYPE_ID = en.ENTITY_ID
                                          INNER JOIN dbo.SR_RFP_ACCOUNT rfp_account
                                                ON rfp_account.sr_rfp_account_id = setup.sr_rfp_account_id
                                    WHERE
                                          setup.sr_rfp_account_id = @sr_account_group_id
                                          AND rfp_account.is_deleted = 0
                                          AND en.entity_type = 1031
                                          AND ( en.entity_name = 'Actual LP Value'
                                                OR en.entity_name = 'Actual Avg LP Value' )
                                          AND value.lp_value IS NOT NULL
                                          AND determinant.determinant_unit_type_id = @converted_unit_id
                              END
	
            END	

      IF @total_lp_size IS NULL 
            BEGIN
                  SET @total_lp_size = -1
            END

      RETURN @total_lp_size
	
END 
;
GO

GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_ACCOUNT_GROUP_LP_SIZE] TO [CBMSApplication]
GO
