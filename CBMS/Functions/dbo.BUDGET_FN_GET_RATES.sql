SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE function dbo.BUDGET_FN_GET_RATES(@account_id int, @commodityid int)
returns varchar(2000)
as
begin
 declare @st varchar(2000), @rate varchar(200)
 set @st=''
 select @st=@st+', '+rate_name from rate 
 where rate_id in (select rate_id from meter
 where meter.rate_id=rate.rate_id and meter.account_id=@account_id
 and rate.commodity_type_id=@commodityid)
 
return substring(@st,3,len(@st))
end
 


GO
GRANT EXECUTE ON  [dbo].[BUDGET_FN_GET_RATES] TO [CBMSApplication]
GO
