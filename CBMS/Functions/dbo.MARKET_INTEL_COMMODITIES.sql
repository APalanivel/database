
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
  
MARKET_INTEL_COMMODITIES

 DESCRIPTION:  
  
  
 INPUT PARAMETERS:  
 Name				DataType		Default		Description  
------------------------------------------------------------  
 @site_id			INT  
 @begin_date		DATETIME  
 @end_date			DATETIME  
  
  
 OUTPUT PARAMETERS:  
 Name				DataType		Default		Description  
------------------------------------------------------------  
  
 USAGE EXAMPLES:  
------------------------------------------------------------  
 set Statistics Profile on   
 
 select dbo.MARKET_INTEL_COMMODITIES(2172)
   
   
AUTHOR INITIALS:    
 Initials  Name    
------------------------------------------------------------    
 NR    Narayana Reddy      
   
 MODIFICATIONS     
 Initials  Date   Modification    
------------------------------------------------------------    
 NR    2016-02-04  MAINT-3788 Replaced DV2-Commodity-Type(Read as '-' to '_') by Commodity. 
 
 ******/   
 
CREATE FUNCTION [dbo].[MARKET_INTEL_COMMODITIES] ( @market_intel_id INT )
RETURNS VARCHAR(2000)
AS 
BEGIN
      DECLARE
            @st VARCHAR(2000)
           ,@rate VARCHAR(200)
      SET @st = ''
      SELECT
            @st = @st + ', ' + c.Commodity_Name
      FROM
            dbo.Commodity c
      WHERE
            Commodity_Id IN ( SELECT
                                    mic.commodity_type_id
                              FROM
                                    market_intel_commodity_map mic
                              WHERE
                                    mic.commodity_type_id = c.Commodity_Id
                                    AND mic.market_intel_id = @market_intel_id )
 
      RETURN SUBSTRING(@st,3,LEN(@st))
END
;
GO

GRANT EXECUTE ON  [dbo].[MARKET_INTEL_COMMODITIES] TO [CBMSApplication]
GO
