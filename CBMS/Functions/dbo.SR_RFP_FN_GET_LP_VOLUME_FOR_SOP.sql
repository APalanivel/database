SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











-- select * from account where account_number = '1047126000'  -- 27776

-- select * from sr_rfp_account where sr_rfp_id = 107

-- select * from sr_rfp_account where sr_rfp_id = 229
--select dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(9,1) 
--select dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(98,1) 
--select dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(1118,0)   -- 11090418

--select dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(1119,0)  -- 19324936

--select dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(1120,0)   -- 17908029


-- select * from sr_rfp_account where account_id = 27776  -- 1120
-- 


CREATE  FUNCTION dbo.SR_RFP_FN_GET_LP_VOLUME_FOR_SOP(@sr_account_group_id int, @is_bid_group bit) 

RETURNS bigint
AS 

BEGIN 
	declare @total_lp_size bigint
	set @total_lp_size = 0
	declare @commodity varchar(50), @rfp_account_id int, @converted_unit_id int, @lp_size bigint
	if @is_bid_group = 1
	begin
		select  @commodity = en.entity_name 
		from   sr_rfp_account rfp_account(nolock),
			sr_rfp rfp(nolock),
			entity en (nolock)
		where  rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
			and rfp_account.is_deleted = 0
			   and rfp.sr_rfp_id = rfp_account.sr_rfp_id
			   and en.entity_id = rfp.commodity_type_id	
		group by en.entity_name
	end
	else if @is_bid_group = 0
	begin
		select  @commodity = en.entity_name 
		from   sr_rfp_account rfp_account(nolock),
			sr_rfp rfp(nolock),
			entity en (nolock)
		where  rfp_account.sr_rfp_account_id = @sr_account_group_id
			and rfp_account.is_deleted = 0
			   and rfp.sr_rfp_id = rfp_account.sr_rfp_id
			   and en.entity_id = rfp.commodity_type_id	
		group by en.entity_name
	end

	if @commodity = 'Natural Gas'
	begin
		select @converted_unit_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
	end 
	else if @commodity = 'Electric Power'
	begin
		select @converted_unit_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kWh'
	end



	if @is_bid_group = 1
	begin

		select	@total_lp_size = sum(value.lp_value * conv.conversion_factor)
	
		from 	sr_rfp_account rfp_account(nolock),
			sr_rfp_load_profile_setup setup(nolock),
			sr_rfp_load_profile_determinant determinant(nolock),
			sr_rfp_lp_determinant_values value (nolock),
			entity en(nolock),
			consumption_unit_conversion conv(nolock)
		
		where	rfp_account.sr_rfp_bid_group_id = @sr_account_group_id 
			and rfp_account.is_deleted = 0
			AND setup.sr_rfp_account_id = rfp_account.sr_rfp_account_id
			and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
			and determinant.determinant_name = 'Total Usage'
			and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
			and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
			and value.reading_type_id = en.entity_id
			and value.lp_value is not null
			and conv.base_unit_id = determinant.determinant_unit_type_id
			and conv.converted_unit_id =@converted_unit_id
	 end
	 else if @is_bid_group = 0
	 begin

		select	@total_lp_size = sum(value.lp_value * conv.conversion_factor)
	
		from 	sr_rfp_load_profile_setup setup(nolock),
			sr_rfp_load_profile_determinant determinant(nolock),
			sr_rfp_lp_determinant_values value (nolock),
			entity en(nolock),
			consumption_unit_conversion conv(nolock),
			sr_rfp_account rfp_account(nolock)
		
		where	setup.sr_rfp_account_id = @sr_account_group_id 
			and rfp_account.sr_rfp_account_id  = setup.sr_rfp_account_id 
			and rfp_account.is_deleted = 0
			and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
			and determinant.determinant_name = 'Total Usage'
			and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
			and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
			and value.reading_type_id = en.entity_id
			and value.lp_value is not null
			and conv.base_unit_id = determinant.determinant_unit_type_id
			and conv.converted_unit_id =@converted_unit_id

	 end	

	if @total_lp_size is null  
		begin
			set @total_lp_size = -1
		end

	RETURN @total_lp_size
	
END 





















GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_LP_VOLUME_FOR_SOP] TO [CBMSApplication]
GO
