SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_2036732398]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [RM_Client_Hier_Hedge_Config_Id], NULL as [RM_Client_Hier_Onboard_Id], NULL as [Config_Start_Dt], NULL as [Config_End_Dt], NULL as [RM_Forecast_UOM_Type_Id], NULL as [Hedge_Type_Id], NULL as [Max_Hedge_Pct], NULL as [RM_Risk_Tolerance_Category_Id], NULL as [Risk_Manager_User_Info_Id], NULL as [Contact_Info_Id], NULL as [Include_In_Reports], NULL as [Is_Default_Config], NULL as [Created_User_Id], NULL as [Created_Ts], NULL as [Last_Updated_By], NULL as [Last_Updated_Ts]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_2036732398', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[RM_Client_Hier_Hedge_Config_Id], t.[RM_Client_Hier_Onboard_Id], t.[Config_Start_Dt], t.[Config_End_Dt], t.[RM_Forecast_UOM_Type_Id], t.[Hedge_Type_Id], t.[Max_Hedge_Pct], t.[RM_Risk_Tolerance_Category_Id], t.[Risk_Manager_User_Info_Id], t.[Contact_Info_Id], t.[Include_In_Reports], t.[Is_Default_Config], t.[Created_User_Id], t.[Created_Ts], t.[Last_Updated_By], t.[Last_Updated_Ts]
	from [cdc].[hvr_2036732398_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_2036732398', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[RM_Client_Hier_Hedge_Config_Id], t.[RM_Client_Hier_Onboard_Id], t.[Config_Start_Dt], t.[Config_End_Dt], t.[RM_Forecast_UOM_Type_Id], t.[Hedge_Type_Id], t.[Max_Hedge_Pct], t.[RM_Risk_Tolerance_Category_Id], t.[Risk_Manager_User_Info_Id], t.[Contact_Info_Id], t.[Include_In_Reports], t.[Is_Default_Config], t.[Created_User_Id], t.[Created_Ts], t.[Last_Updated_By], t.[Last_Updated_Ts]
	from [cdc].[hvr_2036732398_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_2036732398', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_2036732398] TO [public]
GO
