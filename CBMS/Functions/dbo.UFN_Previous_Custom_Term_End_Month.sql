SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*********        
NAME:        
dbo.UFN_Previous_Calendar_Year_Start_Month     

DESCRIPTION:     

INPUT PARAMETERS:        
Name					DataType  Default Description        
------------------------------------------------------------        
@Service_Month			DATETIME

OUTPUT PARAMETERS:        

Name   DataType  Default Description        
------------------------------------------------------------        
USAGE EXAMPLES:        
------------------------------------------------------------        
  
select dbo.UFN_Previous_Custom_Term_End_Month('2018-11-20')

 
AUTHOR INITIALS:        
Initials Name        
------------------------------------------------------------        
RKV		Ravi Kumar Vegesna

MODIFICATIONS:  

Initials Date			Modification        
------------------------------------------------------------        
RKV      2018-05-22		Created

******/
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE FUNCTION [dbo].[UFN_Previous_Custom_Term_End_Month]
    (
        @Service_Month DATETIME
    )
RETURNS DATE
    BEGIN



        RETURN DATEADD(
                   yy, -1
                   , CAST(CASE WHEN MONTH(@Service_Month) < '4' THEN
                                   CONVERT(VARCHAR, DATEPART(YY, @Service_Month)) + '0301'
                              ELSE CONVERT(VARCHAR, DATEPART(YY, @Service_Month) + 1) + '0301'
                          END
AS                     DATE));




    END;

GO
GRANT EXECUTE ON  [dbo].[UFN_Previous_Custom_Term_End_Month] TO [CBMSApplication]
GO
