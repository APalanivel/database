SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_1281477877]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [User_PassCode_Id], NULL as [User_Info_Id], NULL as [PassCode], NULL as [Active_From_Dt], NULL as [Created_By_Id], NULL as [Last_Change_Ts]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1281477877', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_3234B52C
	    when 1 then __$operation
	    else
			case __$min_op_3234B52C 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [User_PassCode_Id], [User_Info_Id], [PassCode], [Active_From_Dt], [Created_By_Id], [Last_Change_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_3234B52C 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1281477877_CT] c with (nolock)   
			where  ( (c.[User_PassCode_Id] = t.[User_PassCode_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_3234B52C, __$count_3234B52C, t.[User_PassCode_Id], t.[User_Info_Id], t.[PassCode], t.[Active_From_Dt], t.[Created_By_Id], t.[Last_Change_Ts] 
		from [cdc].[hvr_1281477877_CT] t with (nolock) inner join 
		(	select  r.[User_PassCode_Id], max(r.__$seqval) as __$max_seqval_3234B52C,
		    count(*) as __$count_3234B52C 
			from [cdc].[hvr_1281477877_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[User_PassCode_Id]) m
		on t.__$seqval = m.__$max_seqval_3234B52C and
		    ( (t.[User_PassCode_Id] = m.[User_PassCode_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1281477877', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1281477877_CT] c with (nolock) 
							where  ( (c.[User_PassCode_Id] = t.[User_PassCode_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_3234B52C
	    when 1 then __$operation
	    else
			case __$min_op_3234B52C 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_3234B52C
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_3234B52C 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [User_PassCode_Id], [User_Info_Id], [PassCode], [Active_From_Dt], [Created_By_Id], [Last_Change_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_3234B52C 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1281477877_CT] c with (nolock)
			where  ( (c.[User_PassCode_Id] = t.[User_PassCode_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_3234B52C, __$count_3234B52C, 
		m.__$update_mask , t.[User_PassCode_Id], t.[User_Info_Id], t.[PassCode], t.[Active_From_Dt], t.[Created_By_Id], t.[Last_Change_Ts]
		from [cdc].[hvr_1281477877_CT] t with (nolock) inner join 
		(	select  r.[User_PassCode_Id], max(r.__$seqval) as __$max_seqval_3234B52C,
		    count(*) as __$count_3234B52C, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_1281477877_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[User_PassCode_Id]) m
		on t.__$seqval = m.__$max_seqval_3234B52C and
		    ( (t.[User_PassCode_Id] = m.[User_PassCode_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1281477877', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1281477877_CT] c with (nolock)
							where  ( (c.[User_PassCode_Id] = t.[User_PassCode_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[User_PassCode_Id], t.[User_Info_Id], t.[PassCode], t.[Active_From_Dt], t.[Created_By_Id], t.[Last_Change_Ts]
		from [cdc].[hvr_1281477877_CT] t  with (nolock) inner join 
		(	select  r.[User_PassCode_Id], max(r.__$seqval) as __$max_seqval_3234B52C
			from [cdc].[hvr_1281477877_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[User_PassCode_Id]) m
		on t.__$seqval = m.__$max_seqval_3234B52C and
		    ( (t.[User_PassCode_Id] = m.[User_PassCode_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1281477877', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1281477877_CT] c with (nolock)
							where  ( (c.[User_PassCode_Id] = t.[User_PassCode_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_1281477877] TO [public]
GO
