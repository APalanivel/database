SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO






CREATE Function dbo.SR_SAD_RPT_FN_GET_CSV_TO_INT ( @Array varchar(1000)) 
returns @IntTable table 
	(IntValue int)
AS
begin

	declare @separator char(1)
	set @separator = ','

	declare @separator_position int 
	declare @array_value varchar(1000) 
	
	set @array = @array + ','
	
	while patindex('%,%' , @array) <> 0 
	begin
	
	  select @separator_position =  patindex('%,%' , @array)
	  select @array_value = left(@array, @separator_position - 1)
	
		Insert @IntTable
		Values (Cast(@array_value as int))

	  select @array = stuff(@array, 1, @separator_position, '')
	end

	return
end







GO
GRANT SELECT ON  [dbo].[SR_SAD_RPT_FN_GET_CSV_TO_INT] TO [CBMSApplication]
GO
