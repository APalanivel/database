
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
		SR_SAD_FN_GET_CONTRACT_NOTIFICATION

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
       	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
SELECT met.meter_id,con.contract_id,con.contract_end_date,en.ENTITY_NAME Contract_Type
FROM meter met 
      JOIN supplier_account_meter_map map ON map.meter_id = met.meter_id AND map.is_history = 0
      JOIN contract con ON con.contract_id = map.contract_id
      JOIN dbo.ENTITY en ON con.CONTRACT_TYPE_ID = en.ENTITY_ID 
WHERE met.meter_id = 336159
ORDER BY en.ENTITY_NAME,con.contract_end_date
  
DECLARE @batch_execution_date DATETIME='2013-09-01'
DECLARE @Current_Date DATETIME ='2013-09-01'
SELECT dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 3, @batch_execution_date), ( dateadd(mm, 4, @batch_execution_date) - 1 ), 0) as [120_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 2, @batch_execution_date), ( dateadd(mm, 3, @batch_execution_date) - 1 ), 0) as [90_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 1, @batch_execution_date), ( dateadd(mm, 2, @batch_execution_date) - 1 ), 0) as [60_Days]
,dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, @batch_execution_date, ( dateadd(mm, 1, @batch_execution_date) - 1 ), 1) AS current_month_con_OR_30_Days  


DECLARE @batch_execution_date DATETIME='2013-10-01'
DECLARE @Current_Date DATETIME ='2013-10-01'
SELECT dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 3, @batch_execution_date), ( dateadd(mm, 4, @batch_execution_date) - 1 ), 0) as [120_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 2, @batch_execution_date), ( dateadd(mm, 3, @batch_execution_date) - 1 ), 0) as [90_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 1, @batch_execution_date), ( dateadd(mm, 2, @batch_execution_date) - 1 ), 0) as [60_Days]
,dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, @batch_execution_date, ( dateadd(mm, 1, @batch_execution_date) - 1 ), 1) AS current_month_con_OR_30_Days  

DECLARE @batch_execution_date DATETIME='2013-11-01'
DECLARE @Current_Date DATETIME ='2013-11-01'
SELECT dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 3, @batch_execution_date), ( dateadd(mm, 4, @batch_execution_date) - 1 ), 0) as [120_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 2, @batch_execution_date), ( dateadd(mm, 3, @batch_execution_date) - 1 ), 0) as [90_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, dateadd(mm, 1, @batch_execution_date), ( dateadd(mm, 2, @batch_execution_date) - 1 ), 0) as [60_Days]
,dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(336159, @Current_Date, @batch_execution_date, ( dateadd(mm, 1, @batch_execution_date) - 1 ), 1) AS current_month_con_OR_30_Days  



DECLARE @batch_execution_date DATETIME='2013-09-01'
DECLARE @Current_Date DATETIME ='2013-09-01'
SELECT dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 3, @batch_execution_date), ( dateadd(mm, 4, @batch_execution_date) - 1 ), 0) as [120_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 2, @batch_execution_date), ( dateadd(mm, 3, @batch_execution_date) - 1 ), 0) as [90_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 1, @batch_execution_date), ( dateadd(mm, 2, @batch_execution_date) - 1 ), 0) as [60_Days]
,dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, @batch_execution_date, ( dateadd(mm, 1, @batch_execution_date) - 1 ), 1) AS current_month_con_OR_30_Days  

DECLARE @batch_execution_date DATETIME='2013-10-01'
DECLARE @Current_Date DATETIME ='2013-10-01'
SELECT dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 3, @batch_execution_date), ( dateadd(mm, 4, @batch_execution_date) - 1 ), 0) as [120_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 2, @batch_execution_date), ( dateadd(mm, 3, @batch_execution_date) - 1 ), 0) as [90_Days]
, dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, dateadd(mm, 1, @batch_execution_date), ( dateadd(mm, 2, @batch_execution_date) - 1 ), 0) as [60_Days]
,dbo.SR_SAD_FN_GET_CONTRACT_NOTIFICATION(337542, @Current_Date, @batch_execution_date, ( dateadd(mm, 1, @batch_execution_date) - 1 ), 1) AS current_month_con_OR_30_Days  



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	SS			Subhash Subramanyam
	SSR			Sharad Srivastava
	RR			Raghu Reddy
	
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009	Autogenerated script
	SS       	10/12/2009	Replaced Contract's account_id reference by by means of contract_id to link Supplier_account_meter_map.
	SSR			03/04/2010	Entity table replaced by Commodity
							Meter table join removed as meter can be filtered from SAMM table.
							Cursor @@FETCH_STATUS -1 and -2 removed as @@FETCH_STATUS 0 gives the status of successful fetch
	RR			2013-11-11	Maint-2155 Script failed to return even if meter's CONTRACT is expiring in current month and no future contract
							and also failed to return even supplier contract is ending if it has future utility contract
							and vice versa
	RR			2016-06-14	GCS-5 GCS-1037 Added @Conatrct_Id input as the data sending at each contract level                	
******/

CREATE FUNCTION [dbo].[SR_SAD_FN_GET_CONTRACT_NOTIFICATION]
      ( 
       @meter_id INT
      ,@currentDate DATETIME
      ,@fromDate DATETIME
      ,@toDate DATETIME
      ,@is_current BIT
      ,@Conatrct_Id INT )
RETURNS VARCHAR(500)
AS 
BEGIN

      DECLARE @contractStr VARCHAR(500)
           
      DECLARE @Cte_Con TABLE
            ( 
             contract_id INT
            ,contract_start_date DATETIME
            ,CONTRACT_END_DATE DATETIME
            ,Contract_Type VARCHAR(200)
            ,Commodity_Name VARCHAR(50) )
            
      INSERT      INTO @Cte_Con
                  ( 
                   contract_id
                  ,contract_start_date
                  ,CONTRACT_END_DATE
                  ,Contract_Type
                  ,Commodity_Name )
                  SELECT
                        con.contract_id
                       ,con.CONTRACT_START_DATE
                       ,con.CONTRACT_END_DATE
                       ,typ.ENTITY_NAME Contract_Type
                       ,com.Commodity_Name
                  FROM
                        dbo.meter met
                        INNER JOIN dbo.supplier_account_meter_map map
                              ON map.meter_id = met.meter_id
                                 AND map.is_history = 0
                        INNER JOIN dbo.account suppacc
                              ON map.account_id = suppacc.account_id
                        INNER JOIN dbo.contract con
                              ON con.contract_id = map.contract_id
                        INNER JOIN dbo.ENTITY typ
                              ON con.CONTRACT_TYPE_ID = typ.ENTITY_ID
                        INNER JOIN dbo.Commodity com
                              ON com.Commodity_id = con.commodity_type_id
                  WHERE
                        met.METER_ID = @meter_id
                        AND con.CONTRACT_END_DATE >= @fromDate
      
      SELECT
            @contractStr = con.Commodity_Name + ',' + CONVERT(VARCHAR, con.contract_id) + ',' + CONVERT(VARCHAR(10), con.CONTRACT_END_DATE, 101)
      FROM
            @Cte_Con con
      WHERE
            con.Contract_Type = 'Supplier'
            AND con.contract_id = @Conatrct_Id
            AND ( ( @is_current = 0
                    AND con.CONTRACT_END_DATE BETWEEN @fromDate
                                              AND     @toDate )
                  OR ( @is_current = 1
                       AND con.CONTRACT_END_DATE BETWEEN @fromDate
                                                 AND     @toDate
                       AND EXISTS ( SELECT
                                          1
                                    FROM
                                          @Cte_Con con1
                                    WHERE
                                          con1.Contract_Type = 'Supplier'
                                    HAVING
                                          MAX(con1.CONTRACT_END_DATE) = con.CONTRACT_END_DATE ) ) )
                                    
      SELECT
            @contractStr = con.Commodity_Name + ',' + CONVERT(VARCHAR, con.contract_id) + ',' + CONVERT(VARCHAR(10), con.CONTRACT_END_DATE, 101)
      FROM
            @Cte_Con con
      WHERE
            con.Contract_Type = 'Utility'
            AND con.contract_id = @Conatrct_Id
            AND ( ( @is_current = 0
                    AND con.CONTRACT_END_DATE BETWEEN @fromDate
                                              AND     @toDate )
                  OR ( @is_current = 1
                       AND con.CONTRACT_END_DATE BETWEEN @fromDate
                                                 AND     @toDate
                       AND EXISTS ( SELECT
                                          1
                                    FROM
                                          @Cte_Con con1
                                    WHERE
                                          con1.Contract_Type = 'Utility'
                                    HAVING
                                          MAX(con1.CONTRACT_END_DATE) = con.CONTRACT_END_DATE ) ) )
                                          
                                          
      
  
      RETURN @contractStr  

END;

;
GO


GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_CONTRACT_NOTIFICATION] TO [CBMSApplication]
GO
