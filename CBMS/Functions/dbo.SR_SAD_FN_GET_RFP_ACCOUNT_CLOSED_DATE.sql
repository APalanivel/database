SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  FUNCTION [dbo].[SR_SAD_FN_GET_RFP_ACCOUNT_CLOSED_DATE](@srRFPAccountId int) 

RETURNS datetime
AS 
	
BEGIN 
	DECLARE @account_closed_date datetime	

	select @account_closed_date =  closure.close_date 
			 from	sr_rfp_closure closure , sr_rfp_account rfpAaccount
			 where	rfpAaccount.sr_rfp_account_id = closure.sr_account_group_id
				and rfpAaccount.sr_rfp_account_id = @srRFPAccountId
				and rfpAaccount.is_deleted = 0

	

	RETURN @account_closed_date
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_RFP_ACCOUNT_CLOSED_DATE] TO [CBMSApplication]
GO
