SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_SAD_RPT_FN_GET_SO(111)

CREATE FUNCTION  [dbo].[SR_SAD_RPT_FN_GET_SO](@supplier_id int)
returns varchar(150) 
AS
	
BEGIN

declare @so_type varchar(150) 

	select @so_type = so_type.entity_name
	from
	sr_supplier_profile sr_sup_prof 
	join sr_supplier_profile_documents sr_sup_prof_doc_so 
	on sr_sup_prof_doc_so.sr_supplier_profile_id = sr_sup_prof.sr_supplier_profile_id
	and sr_sup_prof.vendor_id = @supplier_id
	join cbms_image ci_image_so on ci_image_so.cbms_image_id = sr_sup_prof_doc_so.cbms_image_id
 	join entity so_type on ci_image_so.cbms_image_type_id = so_type.entity_id 
	and  so_type.entity_name = 'Statement of Organization'
	and  so_type.entity_type = 1035 


return @so_type
	

	


END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_SO] TO [CBMSApplication]
GO
