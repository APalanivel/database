SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- select dbo.SR_SAD_FN_GET_METER_ADDR_FOR_SYSTEM_CHECKLIST(577,0 ) 
CREATE    FUNCTION dbo.SR_SAD_FN_GET_METER_ADDR_FOR_SYSTEM_CHECKLIST(@account_id int, @rfpId int) 
RETURNS varchar(4000) 
AS 



BEGIN 

DECLARE @meterStr varchar(4000)

IF(@rfpId = 0)

BEGIN
	set @meterStr = ''
	select @meterStr = @meterStr+', '+m.meter_number+' '+ad.address_line1
	from   meter m(nolock), address ad(nolock)
	where  m.account_id = @account_id
	   and ad.ADDRESS_ID = m.ADDRESS_ID			
	
END
ELSE if(@rfpId > 0)
BEGIN
	set @meterStr = ''
	select @meterStr = @meterStr+', '+m.meter_number+' '+addr.address_line1
	from meter m(nolock), address addr(nolock) , 
	    sr_rfp_account_meter_map map(nolock), 
	    sr_rfp_account rfpAccount(nolock) 
	where rfpAccount.sr_rfp_id = @rfpId
		and rfpAccount.account_id = @account_id
		and map.sr_rfp_account_id = rfpAccount.sr_rfp_account_id
		and rfpAccount.is_deleted = 0
		and m.meter_id = map.meter_id
		and addr.address_id = m.address_id

END


if @meterStr=''
		set @meterStr = null
	return substring(@meterStr,3,len(@meterStr))
END 



GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_METER_ADDR_FOR_SYSTEM_CHECKLIST] TO [CBMSApplication]
GO
