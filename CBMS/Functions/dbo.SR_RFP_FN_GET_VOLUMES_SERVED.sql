
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	CBMS.dbo.SR_RFP_FN_GET_VOLUMES_SERVED

DESCRIPTION:


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
    @AccountId		INT
    @FromDate		VARCHAR(12)
    @AsOfDate		VARCHAR(12)


OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------
USE CBMS_TK3

select dbo.SR_RFP_FN_GET_VOLUMES_SERVED(1320,'01/01/2004','01/01/2009')
select dbo.SR_RFP_FN_GET_VOLUMES_SERVED(14643,'01/01/2007','01/01/2010')


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
    AP			Athmaram Pabbathi
    RR			Raghu Reddy
    
MODIFICATIONS:
	Initials	Date		Modification
------------------------------------------------------------
	        	04/28/2009  Autogenerated script
	 SKA		13/01/2010  Reference Account_id from Cu_Invoice_account_map to cu_invoice
	 SSR		03/18/2010  Removed Cu_invoice_Account_map(Account_id) with cu_invoice_service_month(Account_id)	
	 SSR		04/30/2010  Removed HardCoded Entity_type Values 
	 DMR		09/10/2010  Modified for Quoted_Identifier	
	 AP			08/10/2011  Removed Cost_Usage table and used Cost_Usage_Account_Dtl, Bucket_Master, Code tables
	 AP			08/22/2011  Added qualifiers to all objects with the owner name.
	 AP			04/17/2012  Removed reference of CU_Invoice table and used Service_Month colum from CU_Invoice_Service_Month
	 RR			2015-07-29	Global Sourcing - Added NULLIF condition to avoid divide by zero error 
******/  
  
CREATE FUNCTION [dbo].[SR_RFP_FN_GET_VOLUMES_SERVED]
      ( 
       @AccountId INT
      ,@FromDate VARCHAR(12)
      ,@AsOfDate VARCHAR(12) )
RETURNS DECIMAL(32, 16)
AS 
BEGIN  

      DECLARE
            @Result DECIMAL(32, 16)
           ,@BillingCycle INT
           ,@Volume_Served DECIMAL(32, 16)
           ,@KW_type_ID INT
           ,@KWH_type_ID INT
           ,@Billed_Volume_Served DECIMAL(32, 16)
           ,@Actual_Volume_Served DECIMAL(32, 16)
           ,@Commodity_ID INT

      SELECT
            @KW_Type_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'kW'
            AND uom.Entity_Description = 'Unit for electricity'
	
      SELECT
            @KWH_Type_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Name = 'kWh'
            AND uom.Entity_Description = 'Unit for electricity'
            
      SELECT
            @Commodity_ID = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'
    
    
      SELECT
            @Billed_Volume_Served = isnull(max(case WHEN BM.Bucket_Name = 'Billed Demand' THEN CUAD.Bucket_Value
                                               END), -1)
           ,@Actual_Volume_Served = isnull(max(case WHEN BM.Bucket_Name = 'Demand' THEN CUAD.Bucket_Value
                                               END), -1)
      FROM
            dbo.Cost_Usage_Account_Dtl CUAD
            INNER JOIN dbo.Bucket_Master BM
                  ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
            INNER JOIN dbo.Code CD
                  ON CD.Code_ID = BM.Bucket_Type_Cd
      WHERE
            cuad.Account_Id = @AccountID
            AND BM.Commodity_Id = @Commodity_ID
            AND BM.Bucket_Name IN ( 'Billed Demand', 'Demand' )
            AND CD.Code_Value = 'Determinant'
            AND BM.Is_Active = 1
            AND CUAD.Service_Month BETWEEN convert(DATE, @FromDate)
                                   AND     convert(DATE, @AsOfDate)
            AND CUAD.UOM_Type_Id = @KW_type_ID

      SET @Volume_Served = @Billed_Volume_Served
    

      SELECT
            @Volume_Served = @Actual_Volume_Served
      WHERE
            @Volume_Served = -1 
	 
       
      SELECT
            @Result = @Volume_Served
      WHERE
            @Volume_Served != -1 


      IF @Volume_Served = -1 
            BEGIN  
                  SELECT
                        @Volume_Served = isnull(max(CUAD.Bucket_Value), 0)
                  FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                  WHERE
                        cuad.Account_id = @AccountID
                        AND BM.Commodity_Id = @Commodity_ID
                        AND BM.Bucket_Name = 'Total Usage'
                        AND BM.Is_Active = 1
                        AND CUAD.Service_Month BETWEEN convert(DATE, @FromDate)
                                               AND     convert(DATE, @AsOfDate)
                        AND CUAD.UOM_Type_Id = @KWH_type_ID


                  IF @Volume_Served != 0 
                        BEGIN  
		
                              SELECT
                                    @BillingCycle = datediff(DAY, min(CISM.Service_Month), max(CISM.Service_Month)) * 24
                              FROM
                                    dbo.Cost_Usage_Account_Dtl CUAD
                                    INNER JOIN dbo.CU_Invoice_Service_Month CISM
                                          ON CISM.Account_ID = CUAD.Account_ID
                                             AND CISM.Service_Month = CUAD.Service_Month
                              WHERE
                                    CUAD.Account_ID = @AccountID
                                    AND CUAD.Service_Month BETWEEN convert(DATE, @FromDate)
                                                           AND     convert(DATE, @AsOfDate) 

                              SET @Result = ( @Volume_Served * 50 ) / nullif(@BillingCycle, 0)
                        END  
                  ELSE 
                        BEGIN  
                              SET @Result = -1  
                        END  
            END  
      ELSE 
            BEGIN  
                  SET @Result = @Volume_Served  
            END  

      RETURN @Result  
END;

;
GO


GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_VOLUMES_SERVED] TO [CBMSApplication]
GO
