SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- SELECT dbo.SR_RFP_FN_GET_RFP_CLOSE_DATE(1) 

CREATE FUNCTION [dbo].[SR_RFP_FN_GET_RFP_CLOSE_DATE](@rfp_id int) 
RETURNS datetime
AS 



BEGIN 
	DECLARE @account_due_date datetime, 
		@group_due_date datetime,
		@close_date datetime
	select 	@account_due_date = max(rfp_closure.close_date)
	from 	sr_rfp_closure rfp_closure(nolock),
		sr_rfp_account rfp_account(nolock) 
	where	rfp_account.sr_rfp_id = @rfp_id
		and rfp_closure.sr_account_group_id = rfp_account.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and rfp_closure.is_bid_group = 0
		
	select 	@group_due_date = max(rfp_closure.close_date)
	from 	sr_rfp_closure rfp_closure(nolock),
		sr_rfp_account rfp_account(nolock) 
	where	rfp_account.sr_rfp_id = @rfp_id
		and rfp_closure.sr_account_group_id = rfp_account.sr_rfp_bid_group_id
		and rfp_account.is_deleted = 0
		and rfp_closure.is_bid_group = 1

if @account_due_date > @group_due_date
	set @close_date = @group_due_date
else
	set @close_date = @account_due_date

RETURN @close_date
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RFP_CLOSE_DATE] TO [CBMSApplication]
GO
