SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[dmlLookup_GetIdFromType]
	( @LookupTypeName varchar(50)
	, @LookupName varchar(50)
	) returns int
AS
BEGIN
	return (
		select l.LookupId
        	  from dmlLookup l
		  join dmlLookupType t on t.LookupTypeId = l.LookupTypeId
	         where l.LookupName = @LookupName
		   and t.LookupTypeName = @LookupTypeName
		)
END


GO
GRANT EXECUTE ON  [dbo].[dmlLookup_GetIdFromType] TO [CBMSApplication]
GO
