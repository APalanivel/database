SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE  FUNCTION [dbo].[SR_SAD_RPT_FN_GET_SUMMITIZED_CONTRACTS] (
@SUPPLIER_ID INT
)
RETURNS INT
AS
BEGIN
DECLARE
	@SUMMITIZED_CONTRACTS INT

SELECT  @SUMMITIZED_CONTRACTS =   COUNT(ENTITY.ENTITY_NAME)

FROM
		SR_SUPPLIER_PROFILE SUP_PROFILE,
       		SR_SUPPLIER_PROFILE_DOCUMENTS INNER JOIN
           	CBMS_IMAGE ON SR_SUPPLIER_PROFILE_DOCUMENTS.CBMS_IMAGE_ID =
CBMS_IMAGE.CBMS_IMAGE_ID INNER JOIN
           	ENTITY ON CBMS_IMAGE.CBMS_IMAGE_TYPE_ID = ENTITY.ENTITY_ID
WHERE     ENTITY.ENTITY_NAME = 'Summitized Contract'
	  AND SR_SUPPLIER_PROFILE_DOCUMENTS.SR_SUPPLIER_PROFILE_ID =
SUP_PROFILE.SR_SUPPLIER_PROFILE_ID
	  AND  SUP_PROFILE.VENDOR_ID  = @SUPPLIER_ID
RETURN @SUMMITIZED_CONTRACTS
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_SUMMITIZED_CONTRACTS] TO [CBMSApplication]
GO
