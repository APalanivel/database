SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- exec SR_RFP_GET_RFP_QUEUE_P '1','1',0,0,0,null,null,0,1,223


-- select * from sr_rfp_send_supplier where bid_status_type_id = 1223
-- SELECT dbo.SR_RFP_FN_GET_NO_OF_ACCOUNTS(136,1152)
-- select * from entity(nolock) where entity_type = 1029 and entity_name = 'Refresh'

CREATE    FUNCTION [dbo].[SR_RFP_FN_GET_NO_OF_ACCOUNTS](@rfp_id int, @status_type_id int) 
RETURNS int
AS 

BEGIN 
	DECLARE @count int, @count1 int , @count2 int ,@status_processing_id int, @staus_open_id int, @status_bid_placed_id int, @status_refresh_id int
 	set @count = 0
	set @count1 = 0
	set @count2 = 0
	select @status_processing_id = entity_id from entity(nolock) where entity_type = 1029 and entity_name = 'Processing'
	select @staus_open_id = entity_id from entity(nolock) where entity_type = 1029 and entity_name = 'Open'
	select @status_bid_placed_id = entity_id from entity(nolock) where entity_type = 1029 and entity_name = 'Bid Placed'
	select @status_refresh_id = entity_id from entity(nolock) where entity_type = 1029 and entity_name = 'Refresh'
	if @status_type_id = 0
	begin
		select 	@count = count(sr_rfp_account_id)
		from 	sr_rfp_account(nolock) 
		where	sr_rfp_id = @rfp_id
			and is_deleted = 0
	end
	else if @status_type_id = @staus_open_id
	begin
		
		select @count1 = count(rfpAccount.sr_rfp_account_id)
		from 	sr_rfp_account rfpAccount(nolock) ,
			sr_rfp_send_supplier sendSupp(nolock)
		where	rfpAccount.sr_rfp_id = @rfp_id
			and rfpAccount.sr_rfp_account_id = sendSupp.sr_account_group_id
			and rfpAccount.is_deleted = 0
			and sendSupp.is_bid_group = 0
			and sendSupp.bid_status_type_id in (@status_type_id , @status_refresh_id)



		select @count2 = count(rfpAccount.sr_rfp_account_id)
		from 	sr_rfp_account rfpAccount(nolock) ,
			sr_rfp_send_supplier sendSupp(nolock)
		where	rfpAccount.sr_rfp_id = @rfp_id
			and rfpAccount.sr_rfp_bid_group_id = sendSupp.sr_account_group_id
			and rfpAccount.is_deleted = 0
			and sendSupp.is_bid_group = 1
			and sendSupp.bid_status_type_id in (@status_type_id , @status_refresh_id)
		
		
		set @count =  @count1 +  @count2


	end
	else if @status_type_id = @status_bid_placed_id
	begin
		
		select @count1 = count(distinct rat.sr_account_group_id) 
		from	sr_rfp_selected_products rsp(nolock),
			sr_rfp_term_product_map rtpm(nolock),
			sr_rfp_account_term rat(nolock),					
			sr_rfp_account rfp_account(nolock)
			
		where	rsp.sr_rfp_id = @rfp_id and
			rsp.sr_rfp_selected_products_id = rtpm.sr_rfp_selected_products_id and
			rtpm.BID_DATE is not null and					
			rtpm.sr_rfp_account_term_id = rat.sr_rfp_account_term_id and 
			rat.is_bid_group = 0 
			and rat.sr_account_group_id = rfp_account.sr_rfp_account_id
			and rfp_account.is_deleted = 0
		


		select @count2 = count(distinct acc.SR_RFP_ACCOUNT_ID)
		from	sr_rfp_selected_products rsp(nolock),
			sr_rfp_term_product_map rtpm(nolock),
			sr_rfp_account_term rat(nolock),					
			sr_rfp_account acc(nolock)
	 
		where	rsp.sr_rfp_id = @rfp_id and
			rsp.sr_rfp_selected_products_id = rtpm.sr_rfp_selected_products_id and
			rtpm.BID_DATE is not null and
			IS_NO_BID <> 1 and					
			rtpm.sr_rfp_account_term_id = rat.sr_rfp_account_term_id and 
			acc.SR_RFP_BID_GROUP_ID  = rat.sr_account_group_id and
			rat.is_bid_group = 1
			and acc.is_deleted = 0	


		
		set @count =  @count1 +  @count2


	end


RETURN @count
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_NO_OF_ACCOUNTS] TO [CBMSApplication]
GO
