SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/******
NAME:
	 dbo.ufn_split
		
DESCRIPTION:

	Table frunction that splits the delimited Input into a 	recordset based on the delimiter 
	@Input = delimted list
	@delimiter = seperates object in the @Input List 

INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------
@Input		VARCHAR(8000), 
@Delimiter	CHAR(1)       	         

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------

SELECT * FROM dbo.ufn_split ('3,4,678,89,9,2345'  ,',')

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
Created		7/19/04		By Chad Hattabaugh
Modified	7/28/10     By HAri Changed Input Parameter from VARCHAR (8000) to VARCHAR(MAX)
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
  
CREATE FUNCTION [dbo].[ufn_split]
      (
       @Input VARCHAR(MAX)
      ,@Delimiter CHAR(1) )
RETURNS @RCTable TABLE ( Segments VARCHAR(8000) )
BEGIN     
      DECLARE
            @Start INT
           ,@End INT    
     
      SET @Start = 0    
      SET @End = 0     
     
      WHILE @End < DATALENGTH(@Input)     
            BEGIN    
                  SET @END = CHARINDEX(@Delimiter, @INPUT, @start)     
                  IF @End = 0 
                        SET @End = LEN(@Input) + 1    
    
                  INSERT
                        @RCTABLE ( Segments )
                        SELECT
                              LTRIM(RTRIM(( SUBSTRING(@Input, @Start, @End - @Start) )))    
    
                  SET @Start = @End + 1    
            END     
      RETURN     
END
GO
GRANT SELECT ON  [dbo].[ufn_split] TO [CBMSApplication]
GO
