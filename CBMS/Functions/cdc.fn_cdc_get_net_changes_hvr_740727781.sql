SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_740727781]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Deal_Ticket_Id], NULL as [Client_Id], NULL as [Deal_Ticket_Number], NULL as [Commodity_Id], NULL as [Is_Client_Generated], NULL as [Hedge_Start_Dt], NULL as [Hedge_End_Dt], NULL as [Hedge_Type_Cd], NULL as [Hedge_Allocation_Type_Cd], NULL as [Price_Index_Id], NULL as [Currency_Unit_Id], NULL as [Uom_Type_Id], NULL as [Deal_Ticket_Frequency_Cd], NULL as [Trade_Pricing_Option_Cd], NULL as [Trade_Action_Type_Cd], NULL as [Deal_Ticket_Type_Cd], NULL as [Deal_Status_Cd], NULL as [Workflow_Id], NULL as [Is_Individual_Site_Pricing_Required], NULL as [Comment_Id], NULL as [Created_User_Id], NULL as [Created_Ts], NULL as [Updated_User_Id], NULL as [Last_Change_Ts], NULL as [Is_Bid], NULL as [Fixed_Dt], NULL as [Hedge_Mode_Type_Id], NULL as [Queue_Id], NULL as [Last_Routed_By_User_Id], NULL as [Last_Routed_Ts]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_976BF551
	    when 1 then __$operation
	    else
			case __$min_op_976BF551 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [Deal_Ticket_Id], [Client_Id], [Deal_Ticket_Number], [Commodity_Id], [Is_Client_Generated], [Hedge_Start_Dt], [Hedge_End_Dt], [Hedge_Type_Cd], [Hedge_Allocation_Type_Cd], [Price_Index_Id], [Currency_Unit_Id], [Uom_Type_Id], [Deal_Ticket_Frequency_Cd], [Trade_Pricing_Option_Cd], [Trade_Action_Type_Cd], [Deal_Ticket_Type_Cd], [Deal_Status_Cd], [Workflow_Id], [Is_Individual_Site_Pricing_Required], [Comment_Id], [Created_User_Id], [Created_Ts], [Updated_User_Id], [Last_Change_Ts], [Is_Bid], [Fixed_Dt], [Hedge_Mode_Type_Id], [Queue_Id], [Last_Routed_By_User_Id], [Last_Routed_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_976BF551 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_740727781_CT] c with (nolock)   
			where  ( (c.[Deal_Ticket_Id] = t.[Deal_Ticket_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_976BF551, __$count_976BF551, t.[Deal_Ticket_Id], t.[Client_Id], t.[Deal_Ticket_Number], t.[Commodity_Id], t.[Is_Client_Generated], t.[Hedge_Start_Dt], t.[Hedge_End_Dt], t.[Hedge_Type_Cd], t.[Hedge_Allocation_Type_Cd], t.[Price_Index_Id], t.[Currency_Unit_Id], t.[Uom_Type_Id], t.[Deal_Ticket_Frequency_Cd], t.[Trade_Pricing_Option_Cd], t.[Trade_Action_Type_Cd], t.[Deal_Ticket_Type_Cd], t.[Deal_Status_Cd], t.[Workflow_Id], t.[Is_Individual_Site_Pricing_Required], t.[Comment_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[Is_Bid], t.[Fixed_Dt], t.[Hedge_Mode_Type_Id], t.[Queue_Id], t.[Last_Routed_By_User_Id], t.[Last_Routed_Ts] 
		from [cdc].[hvr_740727781_CT] t with (nolock) inner join 
		(	select  r.[Deal_Ticket_Id], max(r.__$seqval) as __$max_seqval_976BF551,
		    count(*) as __$count_976BF551 
			from [cdc].[hvr_740727781_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Id]) m
		on t.__$seqval = m.__$max_seqval_976BF551 and
		    ( (t.[Deal_Ticket_Id] = m.[Deal_Ticket_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_740727781_CT] c with (nolock) 
							where  ( (c.[Deal_Ticket_Id] = t.[Deal_Ticket_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_976BF551
	    when 1 then __$operation
	    else
			case __$min_op_976BF551 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_976BF551
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_976BF551 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [Deal_Ticket_Id], [Client_Id], [Deal_Ticket_Number], [Commodity_Id], [Is_Client_Generated], [Hedge_Start_Dt], [Hedge_End_Dt], [Hedge_Type_Cd], [Hedge_Allocation_Type_Cd], [Price_Index_Id], [Currency_Unit_Id], [Uom_Type_Id], [Deal_Ticket_Frequency_Cd], [Trade_Pricing_Option_Cd], [Trade_Action_Type_Cd], [Deal_Ticket_Type_Cd], [Deal_Status_Cd], [Workflow_Id], [Is_Individual_Site_Pricing_Required], [Comment_Id], [Created_User_Id], [Created_Ts], [Updated_User_Id], [Last_Change_Ts], [Is_Bid], [Fixed_Dt], [Hedge_Mode_Type_Id], [Queue_Id], [Last_Routed_By_User_Id], [Last_Routed_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_976BF551 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_740727781_CT] c with (nolock)
			where  ( (c.[Deal_Ticket_Id] = t.[Deal_Ticket_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_976BF551, __$count_976BF551, 
		m.__$update_mask , t.[Deal_Ticket_Id], t.[Client_Id], t.[Deal_Ticket_Number], t.[Commodity_Id], t.[Is_Client_Generated], t.[Hedge_Start_Dt], t.[Hedge_End_Dt], t.[Hedge_Type_Cd], t.[Hedge_Allocation_Type_Cd], t.[Price_Index_Id], t.[Currency_Unit_Id], t.[Uom_Type_Id], t.[Deal_Ticket_Frequency_Cd], t.[Trade_Pricing_Option_Cd], t.[Trade_Action_Type_Cd], t.[Deal_Ticket_Type_Cd], t.[Deal_Status_Cd], t.[Workflow_Id], t.[Is_Individual_Site_Pricing_Required], t.[Comment_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[Is_Bid], t.[Fixed_Dt], t.[Hedge_Mode_Type_Id], t.[Queue_Id], t.[Last_Routed_By_User_Id], t.[Last_Routed_Ts]
		from [cdc].[hvr_740727781_CT] t with (nolock) inner join 
		(	select  r.[Deal_Ticket_Id], max(r.__$seqval) as __$max_seqval_976BF551,
		    count(*) as __$count_976BF551, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_740727781_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Id]) m
		on t.__$seqval = m.__$max_seqval_976BF551 and
		    ( (t.[Deal_Ticket_Id] = m.[Deal_Ticket_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_740727781_CT] c with (nolock)
							where  ( (c.[Deal_Ticket_Id] = t.[Deal_Ticket_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[Deal_Ticket_Id], t.[Client_Id], t.[Deal_Ticket_Number], t.[Commodity_Id], t.[Is_Client_Generated], t.[Hedge_Start_Dt], t.[Hedge_End_Dt], t.[Hedge_Type_Cd], t.[Hedge_Allocation_Type_Cd], t.[Price_Index_Id], t.[Currency_Unit_Id], t.[Uom_Type_Id], t.[Deal_Ticket_Frequency_Cd], t.[Trade_Pricing_Option_Cd], t.[Trade_Action_Type_Cd], t.[Deal_Ticket_Type_Cd], t.[Deal_Status_Cd], t.[Workflow_Id], t.[Is_Individual_Site_Pricing_Required], t.[Comment_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts], t.[Is_Bid], t.[Fixed_Dt], t.[Hedge_Mode_Type_Id], t.[Queue_Id], t.[Last_Routed_By_User_Id], t.[Last_Routed_Ts]
		from [cdc].[hvr_740727781_CT] t  with (nolock) inner join 
		(	select  r.[Deal_Ticket_Id], max(r.__$seqval) as __$max_seqval_976BF551
			from [cdc].[hvr_740727781_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Deal_Ticket_Id]) m
		on t.__$seqval = m.__$max_seqval_976BF551 and
		    ( (t.[Deal_Ticket_Id] = m.[Deal_Ticket_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_740727781', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_740727781_CT] c with (nolock)
							where  ( (c.[Deal_Ticket_Id] = t.[Deal_Ticket_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_740727781] TO [public]
GO
