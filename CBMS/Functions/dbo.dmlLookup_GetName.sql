SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[dmlLookup_GetName]
	(@Id int) returns varchar(50)
AS
BEGIN
	return (
		select LookupName
        	  from dmlLookup
	         where LookupID = @Id
		)
END


GO
GRANT EXECUTE ON  [dbo].[dmlLookup_GetName] TO [CBMSApplication]
GO
