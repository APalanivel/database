SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select dbo.SR_RFP_FN_GET_METER_ADDR_ARCHIVE(164)

CREATE    FUNCTION [dbo].[SR_RFP_FN_GET_METER_ADDR_ARCHIVE](@rfp_account_id int) 
RETURNS varchar(4000) 
AS 
begin

	/*DECLARE @meterStr varchar(4000)
	set @meterStr = ''
	select @meterStr = @meterStr+', '+archive_meter.meter_number+' '+archive_meter.meter_address
		from 	sr_rfp_account_meter_map map(nolock),	 
			sr_rfp_archive_meter archive_meter(nolock),
			sr_rfp_account rfp_account(nolock)
 
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and archive_meter.sr_rfp_account_meter_map_id = map.sr_rfp_account_meter_map_id			
		
	return substring(@meterStr,3,len(@meterStr))
end*/

DECLARE @meterStr varchar(4000)
	DECLARE @meter varchar(1500)
	DECLARE @address varchar(2000)
	set @meter = ''
	select @meter = @meter+', '+archive_meter.meter_number
		from 	sr_rfp_account_meter_map map(nolock),	 
			sr_rfp_archive_meter archive_meter(nolock),
			sr_rfp_account rfp_account(nolock)
 
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and archive_meter.sr_rfp_account_meter_map_id = map.sr_rfp_account_meter_map_id		
		
	set @meter = substring(@meter,3,len(@meter))

	set @address = ''
	select @address = @address+', '+archive_meter.meter_address
		from 	sr_rfp_account_meter_map map(nolock),	 
			sr_rfp_archive_meter archive_meter(nolock),
			sr_rfp_account rfp_account(nolock)
 
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and archive_meter.sr_rfp_account_meter_map_id = map.sr_rfp_account_meter_map_id
		group by archive_meter.meter_address
		
	set @address = substring(@address,3,len(@address))

	if @meter=''
		set @meter = null
	if @address=''
		set @address = null
	 
	if @meter is not null and @address is not null
	begin
		set @meterStr = @meter +' , '+@address
	end
	else if @meter is not null and @address is null
	begin
		set @meterStr = @meter
	end
	else if @meter is null and @address is not null
	begin
		set @meterStr = @address
	end
		
	if @meterStr=''
	set @meterStr = null
return @meterStr
end       

/*
BEGIN 
	DECLARE @meterStr varchar(4000), @meter varchar(500)
	DECLARE C_METER CURSOR FAST_FORWARD
	FOR select archive_meter.meter_number+' '+archive_meter.meter_address
		from 	sr_rfp_account_meter_map map(nolock),	 
			sr_rfp_archive_meter archive_meter(nolock),
			sr_rfp_account rfp_account(nolock)
 
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and archive_meter.sr_rfp_account_meter_map_id = map.sr_rfp_account_meter_map_id	
	OPEN C_METER
	FETCH NEXT FROM C_METER INTO @meter
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			IF @meterStr IS NULL OR @meterStr = ''
				SELECT @meterStr = @meter 
			ELSE
				SELECT @meterStr = @meterStr +', '+ @meter 
		END
	FETCH NEXT FROM C_METER INTO @meter
	END
	CLOSE C_METER
	DEALLOCATE C_METER

RETURN @meterStr
END 

*/
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_METER_ADDR_ARCHIVE] TO [CBMSApplication]
GO
