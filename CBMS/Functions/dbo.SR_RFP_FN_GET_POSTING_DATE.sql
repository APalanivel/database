SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_RFP_FN_GET_POSTING_DATE(445,'','' )as posting_date

--select dbo.SR_RFP_FN_GET_POSTING_DATE(444,'01/01/2006',' ' )as posting_date
-- SELECT dbo.SR_RFP_FN_GET_POSTING_DATE(364,'01/01/2006','') 

CREATE     FUNCTION [dbo].[SR_RFP_FN_GET_POSTING_DATE](@rfp_id int,@posted_from Varchar(12),@posted_to Varchar(12)) 
RETURNS datetime
AS 

BEGIN
declare @generationDate datetime

if (@posted_from = '')
 begin 
	select @posted_from = null
 end

if ( @posted_to = '')
 begin 
	select @posted_to = null
 end

IF((@posted_from IS NOT NULL ) AND ( @posted_to IS NOT NULL ))

   BEGIN 
 select @generationDate=max(generation_date)
                     from sr_rfp_send_supplier_log(nolock) 
                     where sr_rfp_id =@rfp_id
                     and generation_date  between   @posted_from AND   @posted_to 
    END
ELSE IF((@posted_from IS NULL) AND ( @posted_to IS NOT NULL ))
   BEGIN          
        select @generationDate= max(generation_date)
                     from sr_rfp_send_supplier_log(nolock) 
                     where sr_rfp_id =@rfp_id
                     and  generation_date <=  @posted_to
   END 
ELSE IF((@posted_from IS NOT NULL) AND ( @posted_to IS NULL ))
     BEGIN
         select @generationDate= max(generation_date)
                     from sr_rfp_send_supplier_log(nolock) 
                     where sr_rfp_id =@rfp_id
                     and generation_date >=  @posted_from

     END 
ELSE
   BEGIN
         select @generationDate= max(generation_date)
                     from sr_rfp_send_supplier_log(nolock) 
                     where sr_rfp_id =@rfp_id

     END 
return @generationDate
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_POSTING_DATE] TO [CBMSApplication]
GO
