SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select  dbo.SR_SAD_RPT_FN_GET_SO_UPLOAD_DATE_FOR_VENDOR(850)
CREATE   FUNCTION  [dbo].[SR_SAD_RPT_FN_GET_SO_UPLOAD_DATE_FOR_VENDOR](@supplier_id int)
returns varchar(10) 
AS
		
BEGIN
DECLARE @uploadDate datetime

set @uploadDate =(
select sr_sup_prof_doc_so.uploaded_date from sr_supplier_profile_documents sr_sup_prof_doc_so 

where sr_sup_prof_doc_so.cbms_image_id = (

	select 
		max(sr_sup_prof_doc_so.cbms_image_id)
		from
		 vendor supplier
		,sr_supplier_profile sr_sup_prof
		,sr_supplier_profile_documents sr_sup_prof_doc_so
		,entity so_type
		,cbms_image ci_image_so
		
		where 
			so_type.entity_id = ci_image_so.cbms_image_type_id
			and  so_type.entity_name = 'Statement of Organization'
			and  so_type.entity_type = 1035
			and ci_image_so.cbms_image_id  = sr_sup_prof_doc_so.cbms_image_id  
			and sr_sup_prof_doc_so.sr_supplier_profile_id = sr_sup_prof.sr_supplier_profile_id
			and supplier.vendor_id = sr_sup_prof.vendor_id
			and supplier.vendor_id = @supplier_id
)
)
return convert(varchar(10), @uploadDate, 101) 
END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_SO_UPLOAD_DATE_FOR_VENDOR] TO [CBMSApplication]
GO
