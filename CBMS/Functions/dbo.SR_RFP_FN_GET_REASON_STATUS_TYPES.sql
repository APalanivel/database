SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_RFP_FN_GET_REASON_STATUS_TYPES(21 , 1, 4, 1) 
--select dbo.SR_RFP_FN_GET_REASON_STATUS_TYPES(608 , 0, 42, 229) 

CREATE    FUNCTION [dbo].[SR_RFP_FN_GET_REASON_STATUS_TYPES](@accountGroupId int, @isBidGroup int, @contactInfoId int, @reasonNotWinningId int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	

	declare @st varchar(2000), @rate varchar(200)
	set @st=''
	select @st=@st+', '+en_reason.entity_name from entity en_reason
	where en_reason.entity_id in (select 	distinct map.not_winning_reason_type_id
		from 	sr_rfp_supplier_contact_vendor_map suppMap(nolock),
			SR_RFP_REASON_NOT_WINNING_MAP map,
			SR_RFP_REASON_NOT_WINNING notWin 
		where 
			suppMap.sr_rfp_supplier_contact_vendor_map_id = notWin.sr_rfp_supplier_contact_vendor_map_id	
			and suppMap.sr_supplier_contact_info_id=@contactInfoId
			and map.sr_rfp_reason_not_winning_id = notWin.sr_rfp_reason_not_winning_id
			and notWin.sr_rfp_reason_not_winning_id = @reasonNotWinningId			
			and notWin.sr_account_group_id = @accountGroupId
			and notWin.is_bid_group = @isBidGroup)



if @st=''
	set @st = null
return substring(@st,3,len(@st))
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_REASON_STATUS_TYPES] TO [CBMSApplication]
GO
