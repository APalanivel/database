SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_401250574]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Workflow_Status_Id], NULL as [Workflow_Status_Name], NULL as [Workflow_Status_Description], NULL as [Is_Notification_Required], NULL as [Notification_Template_Id], NULL as [Notification_Frequency_Cd], NULL as [Managed_By_Group_Info_Id], NULL as [Created_User_Id], NULL as [Created_Ts], NULL as [Updated_User_Id], NULL as [Last_Change_Ts]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_401250574', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_A6CD6582
	    when 1 then __$operation
	    else
			case __$min_op_A6CD6582 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [Workflow_Status_Id], [Workflow_Status_Name], [Workflow_Status_Description], [Is_Notification_Required], [Notification_Template_Id], [Notification_Frequency_Cd], [Managed_By_Group_Info_Id], [Created_User_Id], [Created_Ts], [Updated_User_Id], [Last_Change_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_A6CD6582 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_401250574_CT] c with (nolock)   
			where  ( (c.[Workflow_Status_Id] = t.[Workflow_Status_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_A6CD6582, __$count_A6CD6582, t.[Workflow_Status_Id], t.[Workflow_Status_Name], t.[Workflow_Status_Description], t.[Is_Notification_Required], t.[Notification_Template_Id], t.[Notification_Frequency_Cd], t.[Managed_By_Group_Info_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts] 
		from [cdc].[hvr_401250574_CT] t with (nolock) inner join 
		(	select  r.[Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_A6CD6582,
		    count(*) as __$count_A6CD6582 
			from [cdc].[hvr_401250574_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_A6CD6582 and
		    ( (t.[Workflow_Status_Id] = m.[Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_401250574', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_401250574_CT] c with (nolock) 
							where  ( (c.[Workflow_Status_Id] = t.[Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_A6CD6582
	    when 1 then __$operation
	    else
			case __$min_op_A6CD6582 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_A6CD6582
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_A6CD6582 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [Workflow_Status_Id], [Workflow_Status_Name], [Workflow_Status_Description], [Is_Notification_Required], [Notification_Template_Id], [Notification_Frequency_Cd], [Managed_By_Group_Info_Id], [Created_User_Id], [Created_Ts], [Updated_User_Id], [Last_Change_Ts]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_A6CD6582 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_401250574_CT] c with (nolock)
			where  ( (c.[Workflow_Status_Id] = t.[Workflow_Status_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_A6CD6582, __$count_A6CD6582, 
		m.__$update_mask , t.[Workflow_Status_Id], t.[Workflow_Status_Name], t.[Workflow_Status_Description], t.[Is_Notification_Required], t.[Notification_Template_Id], t.[Notification_Frequency_Cd], t.[Managed_By_Group_Info_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts]
		from [cdc].[hvr_401250574_CT] t with (nolock) inner join 
		(	select  r.[Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_A6CD6582,
		    count(*) as __$count_A6CD6582, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_401250574_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_A6CD6582 and
		    ( (t.[Workflow_Status_Id] = m.[Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_401250574', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_401250574_CT] c with (nolock)
							where  ( (c.[Workflow_Status_Id] = t.[Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[Workflow_Status_Id], t.[Workflow_Status_Name], t.[Workflow_Status_Description], t.[Is_Notification_Required], t.[Notification_Template_Id], t.[Notification_Frequency_Cd], t.[Managed_By_Group_Info_Id], t.[Created_User_Id], t.[Created_Ts], t.[Updated_User_Id], t.[Last_Change_Ts]
		from [cdc].[hvr_401250574_CT] t  with (nolock) inner join 
		(	select  r.[Workflow_Status_Id], max(r.__$seqval) as __$max_seqval_A6CD6582
			from [cdc].[hvr_401250574_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Workflow_Status_Id]) m
		on t.__$seqval = m.__$max_seqval_A6CD6582 and
		    ( (t.[Workflow_Status_Id] = m.[Workflow_Status_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_401250574', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_401250574_CT] c with (nolock)
							where  ( (c.[Workflow_Status_Id] = t.[Workflow_Status_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_401250574] TO [public]
GO
