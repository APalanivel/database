SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_RFP_LP_FN_GET_ALL_CC_EMAIL(10982)

--select * from division where client_Id =235
--select * from site where division_id = 231

CREATE    FUNCTION [dbo].[SR_RFP_LP_FN_GET_ALL_CC_EMAIL](@site_id int) 
RETURNS varchar(2000) 
AS 
BEGIN 


	declare @all_cc_emails as varchar(2000)	
	set @all_cc_emails = ''
	declare @clientId int
	set @clientId = 0


	select	@clientId = cli.client_id,
		@all_cc_emails = isnull(sit.lp_contact_cc,'')
	from 	site sit(nolock),
		division div(nolock),
	 	client cli(nolock) 	
	where	sit.site_id = @site_id
		and div.division_id = sit.division_id
		and cli.client_id = div.client_id
		
	SELECT 	
		@all_cc_emails =  ( @all_cc_emails  + ',' + EMAIL_ADDRESS )
	FROM 
		USER_INFO 
	WHERE 
		USER_INFO_ID IN (select user_info_id from client_cem_map where client_id=@clientId)
	


RETURN @all_cc_emails
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_LP_FN_GET_ALL_CC_EMAIL] TO [CBMSApplication]
GO
