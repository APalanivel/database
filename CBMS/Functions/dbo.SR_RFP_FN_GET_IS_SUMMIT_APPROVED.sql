SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select dbo.SR_RFP_FN_GET_IS_SUMMIT_APPROVED(7896) 
CREATE   FUNCTION [dbo].[SR_RFP_FN_GET_IS_SUMMIT_APPROVED](@vendorId int) 
RETURNS bit  
AS 

BEGIN 
	
	DECLARE @isSummitApproved bit

	if(SELECT  count(IS_SUMMIT_APPROVED)
	FROM  	SR_SUPPLIER_PROFILE
	WHERE   VENDOR_ID = @vendorId ) > 0
	begin
		select @isSummitApproved = 1
	end
	

RETURN @isSummitApproved
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_IS_SUMMIT_APPROVED] TO [CBMSApplication]
GO
