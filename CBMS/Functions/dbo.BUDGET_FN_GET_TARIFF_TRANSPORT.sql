SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--select dbo.SR_RFP_FN_GET_CONTRACTS(1906, convert(datetime, convert( varchar(10), getdate(), 101 )))


CREATE FUNCTION dbo.BUDGET_FN_GET_TARIFF_TRANSPORT(@account_id int, @currentDate datetime , @commodityId int) 

RETURNS varchar(20) 
AS 
	
BEGIN 
	declare @str varchar(20) 
	if (select count(contract_id) from sr_all_contract_vw 
		where account_id = @account_id 
		and commodity_type_id = @commodityId
		and contract_end_date >= @currentDate ) > 0
		set @str = 'Transport'
	else
		set @str = 'Tariff'
	

	RETURN @str
END 


GO
GRANT EXECUTE ON  [dbo].[BUDGET_FN_GET_TARIFF_TRANSPORT] TO [CBMSApplication]
GO
