SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   FUNCTION [dbo].[SR_RFP_FN_GET_SUPPLIER_COMMENTS](@rfpBidId int) 
RETURNS varchar(4000) 
AS 



BEGIN 
	declare @supplierComments varchar(4000)
	declare @conditionalid int

select @supplierComments = SR_RFP_SUPPLIER_PRICE_COMMENTS.PRICE_RESPONSE_COMMENTS from SR_RFP_SUPPLIER_PRICE_COMMENTS,
		SR_RFP_BID where SR_RFP_BID.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID=SR_RFP_SUPPLIER_PRICE_COMMENTS.SR_RFP_SUPPLIER_PRICE_COMMENTS_ID 
		AND SR_RFP_BID.SR_RFP_BID_ID=@rfpBidId

	return @supplierComments
END 


--end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_SUPPLIER_COMMENTS] TO [CBMSApplication]
GO
