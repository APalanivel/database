SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION dbo.SR_RFP_FN_GET_CHECKLIST_RATES(@rfp_account_id INT, @commodityId INT)
	RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @st VARCHAR(2000)

	SELECT @st = COALESCE(@st + ', ','') + rate_name
	FROM dbo.rate r
		JOIN dbo.meter m (NOLOCK) ON m.rate_id = r.rate_id
		JOIN dbo.sr_rfp_account_meter_map map(NOLOCK) ON map.meter_id = m.meter_id
		JOIN dbo.sr_rfp_account rfp_account(NOLOCK) ON rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
	WHERE map.sr_rfp_account_id = @rfp_account_id
		AND rfp_account.is_deleted = 0
		AND r.commodity_type_id = @commodityId

	RETURN @st

END

GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_CHECKLIST_RATES] TO [CBMSApplication]
GO
