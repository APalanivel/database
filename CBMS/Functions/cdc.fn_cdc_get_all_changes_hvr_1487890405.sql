SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_1487890405]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Client_Commodity_Id], NULL as [Commodity_Id], NULL as [Client_Id], NULL as [Scope_Cd], NULL as [UOM_Cd], NULL as [Frequency_Cd], NULL as [Hier_Level_Cd], NULL as [Row_Version], NULL as [Commodity_Service_Cd], NULL as [Allow_for_DE], NULL as [Last_Change_Ts], NULL as [Owner_User_Id], NULL as [DE_Config_Last_Change_By_User_Id], NULL as [DE_Config_Last_Change_Ts], NULL as [DE_Config_Uom_Cd], NULL as [Allow_Utility_Map]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Client_Commodity_Id], t.[Commodity_Id], t.[Client_Id], t.[Scope_Cd], t.[UOM_Cd], t.[Frequency_Cd], t.[Hier_Level_Cd], t.[Row_Version], t.[Commodity_Service_Cd], t.[Allow_for_DE], t.[Last_Change_Ts], t.[Owner_User_Id], t.[DE_Config_Last_Change_By_User_Id], t.[DE_Config_Last_Change_Ts], t.[DE_Config_Uom_Cd], t.[Allow_Utility_Map]
	from [cdc].[hvr_1487890405_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[Client_Commodity_Id], t.[Commodity_Id], t.[Client_Id], t.[Scope_Cd], t.[UOM_Cd], t.[Frequency_Cd], t.[Hier_Level_Cd], t.[Row_Version], t.[Commodity_Service_Cd], t.[Allow_for_DE], t.[Last_Change_Ts], t.[Owner_User_Id], t.[DE_Config_Last_Change_By_User_Id], t.[DE_Config_Last_Change_Ts], t.[DE_Config_Uom_Cd], t.[Allow_Utility_Map]
	from [cdc].[hvr_1487890405_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_1487890405] TO [public]
GO
