SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_net_changes_hvr_1487890405]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return

	select NULL as __$start_lsn,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [Client_Commodity_Id], NULL as [Commodity_Id], NULL as [Client_Id], NULL as [Scope_Cd], NULL as [UOM_Cd], NULL as [Frequency_Cd], NULL as [Hier_Level_Cd], NULL as [Row_Version], NULL as [Commodity_Service_Cd], NULL as [Allow_for_DE], NULL as [Last_Change_Ts], NULL as [Owner_User_Id], NULL as [DE_Config_Last_Change_By_User_Id], NULL as [DE_Config_Last_Change_Ts], NULL as [DE_Config_Uom_Cd], NULL as [Allow_Utility_Map]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 0)

	union all
	
	select __$start_lsn,
	    case __$count_D9429838
	    when 1 then __$operation
	    else
			case __$min_op_D9429838 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		null as __$update_mask , [Client_Commodity_Id], [Commodity_Id], [Client_Id], [Scope_Cd], [UOM_Cd], [Frequency_Cd], [Hier_Level_Cd], [Row_Version], [Commodity_Service_Cd], [Allow_for_DE], [Last_Change_Ts], [Owner_User_Id], [DE_Config_Last_Change_By_User_Id], [DE_Config_Last_Change_Ts], [DE_Config_Uom_Cd], [Allow_Utility_Map]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_D9429838 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1487890405_CT] c with (nolock)   
			where  ( (c.[Client_Commodity_Id] = t.[Client_Commodity_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_D9429838, __$count_D9429838, t.[Client_Commodity_Id], t.[Commodity_Id], t.[Client_Id], t.[Scope_Cd], t.[UOM_Cd], t.[Frequency_Cd], t.[Hier_Level_Cd], t.[Row_Version], t.[Commodity_Service_Cd], t.[Allow_for_DE], t.[Last_Change_Ts], t.[Owner_User_Id], t.[DE_Config_Last_Change_By_User_Id], t.[DE_Config_Last_Change_Ts], t.[DE_Config_Uom_Cd], t.[Allow_Utility_Map] 
		from [cdc].[hvr_1487890405_CT] t with (nolock) inner join 
		(	select  r.[Client_Commodity_Id], max(r.__$seqval) as __$max_seqval_D9429838,
		    count(*) as __$count_D9429838 
			from [cdc].[hvr_1487890405_CT] r with (nolock)   
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Client_Commodity_Id]) m
		on t.__$seqval = m.__$max_seqval_D9429838 and
		    ( (t.[Client_Commodity_Id] = m.[Client_Commodity_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1487890405_CT] c with (nolock) 
							where  ( (c.[Client_Commodity_Id] = t.[Client_Commodity_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
	select __$start_lsn,
	    case __$count_D9429838
	    when 1 then __$operation
	    else
			case __$min_op_D9429838 
				when 2 then 2
				when 4 then
				case __$operation
					when 1 then 1
					else 4
					end
				else
				case __$operation
					when 2 then 4
					when 4 then 4
					else 1
					end
			end
		end as __$operation,
		case __$count_D9429838
		when 1 then
			case __$operation
			when 4 then __$update_mask
			else null
			end
		else	
			case __$min_op_D9429838 
			when 2 then null
			else
				case __$operation
				when 1 then null
				else __$update_mask 
				end
			end	
		end as __$update_mask , [Client_Commodity_Id], [Commodity_Id], [Client_Id], [Scope_Cd], [UOM_Cd], [Frequency_Cd], [Hier_Level_Cd], [Row_Version], [Commodity_Service_Cd], [Allow_for_DE], [Last_Change_Ts], [Owner_User_Id], [DE_Config_Last_Change_By_User_Id], [DE_Config_Last_Change_Ts], [DE_Config_Uom_Cd], [Allow_Utility_Map]
	from
	(
		select t.__$start_lsn as __$start_lsn, __$operation,
		case __$count_D9429838 
		when 1 then __$operation 
		else
		(	select top 1 c.__$operation
			from [cdc].[hvr_1487890405_CT] c with (nolock)
			where  ( (c.[Client_Commodity_Id] = t.[Client_Commodity_Id]) )  
			and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
			and (c.__$start_lsn <= @to_lsn)
			and (c.__$start_lsn >= @from_lsn)
			order by c.__$seqval) end __$min_op_D9429838, __$count_D9429838, 
		m.__$update_mask , t.[Client_Commodity_Id], t.[Commodity_Id], t.[Client_Id], t.[Scope_Cd], t.[UOM_Cd], t.[Frequency_Cd], t.[Hier_Level_Cd], t.[Row_Version], t.[Commodity_Service_Cd], t.[Allow_for_DE], t.[Last_Change_Ts], t.[Owner_User_Id], t.[DE_Config_Last_Change_By_User_Id], t.[DE_Config_Last_Change_Ts], t.[DE_Config_Uom_Cd], t.[Allow_Utility_Map]
		from [cdc].[hvr_1487890405_CT] t with (nolock) inner join 
		(	select  r.[Client_Commodity_Id], max(r.__$seqval) as __$max_seqval_D9429838,
		    count(*) as __$count_D9429838, 
		    [sys].[ORMask](r.__$update_mask) as __$update_mask
			from [cdc].[hvr_1487890405_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Client_Commodity_Id]) m
		on t.__$seqval = m.__$max_seqval_D9429838 and
		    ( (t.[Client_Commodity_Id] = m.[Client_Commodity_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with mask'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and
				  (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1487890405_CT] c with (nolock)
							where  ( (c.[Client_Commodity_Id] = t.[Client_Commodity_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 			   )
	 			 )
	 			) 	
	) Q
	
	union all
	
		select t.__$start_lsn as __$start_lsn,
		case t.__$operation
			when 1 then 1
			else 5
		end as __$operation,
		null as __$update_mask , t.[Client_Commodity_Id], t.[Commodity_Id], t.[Client_Id], t.[Scope_Cd], t.[UOM_Cd], t.[Frequency_Cd], t.[Hier_Level_Cd], t.[Row_Version], t.[Commodity_Service_Cd], t.[Allow_for_DE], t.[Last_Change_Ts], t.[Owner_User_Id], t.[DE_Config_Last_Change_By_User_Id], t.[DE_Config_Last_Change_Ts], t.[DE_Config_Uom_Cd], t.[Allow_Utility_Map]
		from [cdc].[hvr_1487890405_CT] t  with (nolock) inner join 
		(	select  r.[Client_Commodity_Id], max(r.__$seqval) as __$max_seqval_D9429838
			from [cdc].[hvr_1487890405_CT] r with (nolock)
			where  (r.__$start_lsn <= @to_lsn)
			and (r.__$start_lsn >= @from_lsn)
			group by   r.[Client_Commodity_Id]) m
		on t.__$seqval = m.__$max_seqval_D9429838 and
		    ( (t.[Client_Commodity_Id] = m.[Client_Commodity_Id]) ) 	
		where lower(rtrim(ltrim(@row_filter_option))) = N'all with merge'
			and ( [sys].[fn_cdc_check_parameters]( N'hvr_1487890405', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 1) = 1)
			and (t.__$start_lsn <= @to_lsn)
			and (t.__$start_lsn >= @from_lsn)
			and ((t.__$operation = 2) or (t.__$operation = 4) or 
				 ((t.__$operation = 1) and 
				   (2 not in 
				 		(	select top 1 c.__$operation
							from [cdc].[hvr_1487890405_CT] c with (nolock)
							where  ( (c.[Client_Commodity_Id] = t.[Client_Commodity_Id]) )  
							and ((c.__$operation = 2) or (c.__$operation = 4) or (c.__$operation = 1))
							and (c.__$start_lsn <= @to_lsn)
							and (c.__$start_lsn >= @from_lsn)
							order by c.__$seqval
						 ) 
	 				)
	 			 )
	 			)
	 
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_net_changes_hvr_1487890405] TO [public]
GO
