SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









--select dbo.SR_RFP_FN_GET_SOP_ACCOUNT_TERMS (472,1)
--select dbo.SR_RFP_FN_GET_SOP_ACCOUNT_TERMS (324,1)

--select * from SR_RFP_ACCOUNT WHERE SR_RFP_ID=1

--SELECT * from SR_RFP_ACCOUNT_TERM

CREATE    FUNCTION dbo.SR_RFP_FN_GET_SOP_ACCOUNT_TERMS(@accountgroupId int,@isBidGroupId int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	

	declare @st varchar(2000), @rate varchar(200)
	set @st=''
	select @st=@st+', '+str(SR_RFP_ACCOUNT_TERM_ID) from SR_RFP_ACCOUNT_TERM
	where SR_RFP_ACCOUNT_TERM_ID in (SELECT  DISTINCT SR_RFP_ACCOUNT_TERM_ID
					FROM 		
						SR_RFP_SOP_DETAILS
					WHERE 
						SR_RFP_ACCOUNT_TERM.SR_ACCOUNT_GROUP_ID = @accountgroupId 
						AND SR_RFP_ACCOUNT_TERM.IS_BID_GROUP=@isBidGroupId AND 
						SR_RFP_ACCOUNT_TERM.IS_SOP=1)


 

	if @st=''
	set @st = null
return substring(@st,3,len(@st))
end



GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_SOP_ACCOUNT_TERMS] TO [CBMSApplication]
GO
