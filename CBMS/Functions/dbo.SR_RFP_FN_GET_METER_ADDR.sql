SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select dbo.SR_RFP_FN_GET_METER_ADDR(164) 
-- select dbo.SR_RFP_FN_GET_METER_ADDR(1764) 
-- select * from sr_rfp_account where sr_rfp_account_id = 164
CREATE      FUNCTION [dbo].[SR_RFP_FN_GET_METER_ADDR](@rfp_account_id int) 
RETURNS varchar(4000) 
AS 

BEGIN 


	DECLARE @meterStr varchar(4000)
	DECLARE @meter varchar(1500)
	DECLARE @address varchar(2000)
	set @meter = ''
	select @meter = @meter+', '+m.meter_number
		from 	sr_rfp_account_meter_map map(nolock),
			meter m(nolock),
			address addr(nolock),
			sr_rfp_account rfp_account(nolock)
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0
			and m.meter_id = map.meter_id
			and m.address_id = addr.address_id		
		
	set @meter = substring(@meter,3,len(@meter))

	set @address = ''
	select @address = @address+', '+addr.address_line1
		from 	sr_rfp_account_meter_map map(nolock),
		meter m(nolock), 
		address addr(nolock) ,
		sr_rfp_account rfp_account(nolock)
	where 	map.sr_rfp_account_id = @rfp_account_id
		and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
		and rfp_account.is_deleted = 0
		and m.meter_id = map.meter_id
		and addr.address_id = m.address_id
	group by addr.address_id, addr.address_line1
		
	set @address = substring(@address,3,len(@address))

	if @meter=''
		set @meter = null
	if @address=''
		set @address = null
	 
	if @meter is not null and @address is not null
	begin
		set @meterStr = @meter +' , '+@address
	end
	else if @meter is not null and @address is null
	begin
		set @meterStr = @meter
	end
	else if @meter is null and @address is not null
	begin
		set @meterStr = @address
	end
		
	if @meterStr=''
	set @meterStr = null
return @meterStr
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_METER_ADDR] TO [CBMSApplication]
GO
