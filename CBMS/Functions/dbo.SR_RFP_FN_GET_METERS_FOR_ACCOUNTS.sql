SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
-- select dbo.SR_RFP_FN_GET_METERS_FOR_ACCOUNTS(970,290) 
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE   FUNCTION [dbo].[SR_RFP_FN_GET_METERS_FOR_ACCOUNTS](@account_id int , @commodityId int) 
RETURNS varchar(8000) 

AS 

BEGIN 

	declare @meterStr varchar(4000)
		
	    set @meterStr=''
	    select @meterStr=@meterStr+'~ '+str(m.meter_id)+' | '+m.meter_number+' | '+ad.ADDRESS_LINE1 + ' ' + ad.ADDRESS_LINE2+' | '+v.vendor_name+' | '+r.rate_name+' | '+rateType.entity_name
	    from   meter m, rate r , account a, vendor v, address ad,entity rateType
	    where  m.ACCOUNT_ID = a.ACCOUNT_ID and	
		   r.RATE_ID = m.RATE_ID and
		  r.commodity_type_id = @commodityId and	
		   v.vendor_id = a.vendor_id  and
		   ad.ADDRESS_ID = m.ADDRESS_ID and
		   m.purchase_method_type_id=rateType.entity_id and
		   m.account_id=@account_id
		
		
	if @meterStr=''
	set @meterStr = null
return substring(@meterStr,3,len(@meterStr))

END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_METERS_FOR_ACCOUNTS] TO [CBMSApplication]
GO
