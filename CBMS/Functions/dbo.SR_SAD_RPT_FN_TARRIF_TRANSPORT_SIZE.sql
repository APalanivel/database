
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------
    SELECT dbo.SR_SAD_RPT_FN_TARRIF_TRANSPORT_SIZE (291, 24204,'01/06/2005','01/05/2006')
    SELECT dbo.SR_SAD_RPT_FN_TARRIF_TRANSPORT_SIZE (291, 24195,'01/06/2005','01/05/2006')
    SELECT dbo.SR_RFP_FN_GET_VOLUMES_SERVED(24195,'01/06/2005','01/05/2006')
    SELECT dbo.SR_SAD_RPT_FN_TARRIF_TRANSPORT_SIZE (290, 2043,null,null)

AUTHOR INITIALS:
    Initials	 Name
------------------------------------------------------------
    AP		 Athmaram Pabbathi

MODIFICATIONS:
Initials	  Date	    Modification
------------------------------------------------------------
DMR		  09/10/2010  Modified for Quoted_Identifier
AP		  09/19/2011  Following Changes made for ADLDT changes
					   - Replaced UDF SR_RFP_FN_GET_GAS_VOLUMES_SERVED with Select script.
					   - Removed unused variable @TotalSize
******/
CREATE       FUNCTION [dbo].[SR_SAD_RPT_FN_TARRIF_TRANSPORT_SIZE]
      ( 
       @CommodityId INT
      ,@AccountId INT
      ,@FormDate VARCHAR(12)
      ,@ToDate VARCHAR(12) )
RETURNS BIGINT
AS 
BEGIN
      DECLARE
            @Total BIGINT
           ,@Converted_Unit_ID INT
           ,@ElectricPowerId INT
           ,@NaturalGasId INT

      SELECT
            @ElectricPowerId = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Electric Power'

      SELECT
            @NaturalGasId = com.Commodity_Id
      FROM
            dbo.Commodity com
      WHERE
            com.Commodity_Name = 'Natural Gas'


      SELECT
            @Converted_Unit_ID = uom.Entity_ID
      FROM
            dbo.Entity uom
      WHERE
            uom.Entity_Description = 'Unit for Gas'
            AND uom.Entity_Name = 'MMBtu'	


      IF ( @CommodityId = @ElectricPowerId ) 
            BEGIN
                  SET @Total = -1
                  
                  IF ( SELECT
                        count(cvw.contract_id)
                       FROM
                        dbo.SR_Contract_VW cvw
                       WHERE
                        cvw.Account_Id = @AccountId
                        AND @ToDate BETWEEN cvw.contract_start_date
                                    AND     cvw.contract_end_date ) > 0 
                        BEGIN
                              SET @Total = dbo.SR_RFP_FN_GET_VOLUMES_SERVED(@AccountId, @FormDate, @ToDate)
                        END
            END
      ELSE 
-- Natural Gas
            BEGIN
                  SELECT
                        @Total = isnull(sum(CUAD.Bucket_Value * UC.Conversion_Factor), 0)
                  FROM
                        dbo.Cost_Usage_Account_Dtl CUAD
                        INNER JOIN dbo.Bucket_Master BM
                              ON BM.Bucket_Master_Id = CUAD.Bucket_Master_Id
                        INNER JOIN dbo.Consumption_Unit_Conversion UC
                              ON UC.Base_Unit_ID = CUAD.UOM_Type_Id
                  WHERE
                        CUAD.Account_ID = @AccountId
                        AND UC.Converted_Unit_ID = @Converted_Unit_ID
                        AND BM.Commodity_Id = @CommodityId
                        AND BM.Bucket_Name = 'Total Usage'
                        AND BM.Is_Active = 1
                        AND CUAD.Service_Month BETWEEN convert(DATE, @FormDate)
                                               AND     convert(DATE, @ToDate)
                        AND @CommodityId = @NaturalGasId
            END
		
      RETURN @Total 

END
;
GO

GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_TARRIF_TRANSPORT_SIZE] TO [CBMSApplication]
GO
