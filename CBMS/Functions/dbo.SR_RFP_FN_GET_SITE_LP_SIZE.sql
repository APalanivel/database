SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
















--select dbo.SR_RFP_FN_GET_SITE_LP_SIZE(1, 1, 1) 




CREATE         FUNCTION dbo.SR_RFP_FN_GET_SITE_LP_SIZE(@rfp_id int,@sr_account_group_id int,@site_id int)
		 

RETURNS bigint
AS 

BEGIN 
	declare @total_lp_size bigint
			set @total_lp_size = 0
			declare @commodity varchar(50), @rfp_account_id int, @converted_unit_id int, @lp_size bigint
			begin
				select  @commodity = en.entity_name 
				from   sr_rfp_account rfp_account(nolock),
					   sr_rfp rfp(nolock),
					   entity en (nolock)
				where  rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
					and rfp_account.is_deleted = 0
					   and rfp.sr_rfp_id = rfp_account.sr_rfp_id
					   and en.entity_id = rfp.commodity_type_id
					   and rfp.sr_rfp_id = @rfp_id
				group by en.entity_name
				if @commodity = 'Natural Gas'
				begin
					select @converted_unit_id = entity_id from entity(nolock) where entity_type = 102 and entity_name = 'MMBtu'
					select	@total_lp_size = sum(value.lp_value * conv.conversion_factor)
				
					from 	sr_rfp_load_profile_setup setup(nolock),
						sr_rfp_load_profile_determinant determinant(nolock),
						sr_rfp_lp_determinant_values value (nolock),
						entity en(nolock),
						consumption_unit_conversion conv(nolock)
					
					where	setup.sr_rfp_account_id in
						(
							select ra.sr_rfp_account_id
							from sr_rfp_account ra(nolock),account a(nolock)
							where ra.sr_rfp_id =@rfp_id 
							and ra.sr_rfp_bid_group_id = @sr_account_group_id
							and ra.is_deleted = 0
							and a.account_id = ra.account_id 
							and a.site_id = @site_id
						)
						and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
						and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
						and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
						and value.reading_type_id = en.entity_id
						and value.lp_value is not null
						and conv.base_unit_id = determinant.determinant_unit_type_id
						and conv.converted_unit_id =@converted_unit_id

				end
				else if @commodity = 'Electric Power'
				begin
					select @converted_unit_id = entity_id from entity(nolock) where entity_type = 101 and entity_name = 'kW'
					set @total_lp_size = 0
					select	@total_lp_size = @total_lp_size + max(value.lp_value)
					
					from 	sr_rfp_account rfp_account(nolock),
						account a(nolock),
						sr_rfp_load_profile_setup setup(nolock),
						sr_rfp_load_profile_determinant determinant(nolock),
						sr_rfp_lp_determinant_values value (nolock),
						entity en(nolock)
					
					where	a.site_id = @site_id
						and rfp_account.account_id = a.account_id
						and rfp_account.sr_rfp_bid_group_id = @sr_account_group_id
						and rfp_account.is_deleted = 0
						and setup.sr_rfp_account_id =  rfp_account.sr_rfp_account_id
						and determinant.sr_rfp_load_profile_setup_id = setup.sr_rfp_load_profile_setup_id
						and value.sr_rfp_load_profile_determinant_id = determinant.sr_rfp_load_profile_determinant_id
						and en.entity_type = 1031 and (en.entity_name = 'Actual LP Value' or en.entity_name = 'Actual Avg LP Value')
						and value.reading_type_id = en.entity_id
						and value.lp_value is not null
						and determinant.determinant_unit_type_id = @converted_unit_id
					group by rfp_account.sr_rfp_account_id
				end
	
		
				
				
						
			 end
			 
		RETURN @total_lp_size
	
END 


















GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_SITE_LP_SIZE] TO [CBMSApplication]
GO
