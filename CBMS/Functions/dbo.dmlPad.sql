SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create function dbo.dmlPad
	( @StrIn varchar(255)
	, @FinalLen int
	, @PadChar char(1)
	) returns varchar(255)
AS
BEGIN
	declare @CurrentLen int
	declare @StrOut varchar(255)
	set @StrOut = @StrIn
	set @CurrentLen = len(@StrOut)
	while @CurrentLen < @FinalLen
	begin
		set @StrOut = @PadChar + @StrOut
		set @CurrentLen = len(@StrOut)
	end
	return @StrOut
END


GO
GRANT EXECUTE ON  [dbo].[dmlPad] TO [CBMSApplication]
GO
