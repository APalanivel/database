SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create function dbo.MARKET_INTEL_STATES(@market_intel_id int)
returns varchar(2000)
as
begin
 declare @st varchar(2000), @rate varchar(200)
 set @st=''
 select @st=@st+', '+state_name from state
 where state_id in (select state_id from market_intel_state_map
 where market_intel_state_map.state_id=state.state_id and market_intel_state_map.market_intel_id=@market_intel_id)
 
return substring(@st,3,len(@st))
end



GO
GRANT EXECUTE ON  [dbo].[MARKET_INTEL_STATES] TO [CBMSApplication]
GO
