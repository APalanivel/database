SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:
	This function removes all trailing and preceding spaces of a string,
	and converts all occurences of more than one consecutive space to one space


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

create FUNCTION [dbo].[removeExtraSpaces]
(
	@value varchar(500)
) 
returns varchar(500)
AS
BEGIN
	
	declare @tooManySpaces bit
	set @tooManySpaces = 1
	set @value = ltrim(@value)
	set @value = rtrim(@value)
	
	while (@tooManySpaces = 1)
	begin
		if patindex('%  %', @value) > 0 -- look for two consecutive spaces
			set @value = replace(@value, '  ', ' ') -- replace two spaces with one space
		else
			set @tooManySpaces = 0 -- all two consecutive spaces occurences have been removed
	end

	return @value
END
GO
GRANT EXECUTE ON  [dbo].[removeExtraSpaces] TO [CBMSApplication]
GO
