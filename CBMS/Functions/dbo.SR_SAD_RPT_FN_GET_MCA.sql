SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select dbo.SR_SAD_RPT_FN_GET_MCA(1091)

CREATE  FUNCTION  [dbo].[SR_SAD_RPT_FN_GET_MCA](@supplier_id int)
returns varchar(150) 
AS
	
BEGIN
declare @mca_type varchar(150) 

	select @mca_type = mac_type.entity_name
	from
	sr_supplier_profile sr_sup_prof 
	join sr_supplier_profile_documents sr_sup_prof_doc_mac 
	on sr_sup_prof_doc_mac.sr_supplier_profile_id = sr_sup_prof.sr_supplier_profile_id
	and sr_sup_prof.vendor_id = @supplier_id
	join cbms_image ci_image_mac on ci_image_mac.cbms_image_id = sr_sup_prof_doc_mac.cbms_image_id
 	join entity mac_type on ci_image_mac.cbms_image_type_id = mac_type.entity_id 
	and mac_type.entity_name = 'Mutual Confidentiality Agreement'
	and  mac_type.entity_type = 1035

return @mca_type

	


END
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_RPT_FN_GET_MCA] TO [CBMSApplication]
GO
