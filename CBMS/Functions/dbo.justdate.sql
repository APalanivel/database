SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[justdate] (@capturedate datetime)  
returns datetime
as
begin

return cast(cast(datepart(mm,@capturedate) as varchar)+'-'+cast(datepart(dd,@capturedate) as varchar)+'-'+
cast(datepart(yyyy,@capturedate) as varchar) as datetime)
end
GO
GRANT EXECUTE ON  [dbo].[justdate] TO [CBMSApplication]
GO
