SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE  FUNCTION dbo.BUDGET_FN_GET_RATES2(@account_id int, @commodityId int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	DECLARE @rateStr varchar(2000), @rate varchar(200)
	DECLARE C_RATE CURSOR FAST_FORWARD
	FOR 	select distinct r.rate_name 
		from 	meter m(nolock), 
		     	rate r(nolock)
		     
		where 	m.account_id = @account_id
			and r.rate_id = m.rate_id
			and r.commodity_type_id = @commodityId
	OPEN C_RATE
	FETCH NEXT FROM C_RATE INTO @rate
	WHILE (@@fetch_status <> -1)
	BEGIN
		IF (@@fetch_status <> -2)
		BEGIN
			IF @rateStr IS NULL OR @rateStr = ''
				SELECT @rateStr = @rate 
			ELSE
				SELECT @rateStr = @rateStr +', '+ @rate 
		END
	FETCH NEXT FROM C_RATE INTO @rate
	END
	CLOSE C_RATE
	DEALLOCATE C_RATE

RETURN @rateStr
END 

GO
GRANT EXECUTE ON  [dbo].[BUDGET_FN_GET_RATES2] TO [CBMSApplication]
GO
