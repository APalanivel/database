SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

-- select dbo.SR_SAD_FN_GET_STATE_NAMES_FOR_CONTACT(8)


CREATE      FUNCTION [dbo].[SR_SAD_FN_GET_STATE_NAMES_FOR_CONTACT](@userid int) 
RETURNS VARCHAR(2000)
AS 
BEGIN 
	declare @st varchar(2000), @rate varchar(200)
	set @st=''
		select @st=@st+', '+S.STATE_NAME 
		FROM 	STATE S,
			SR_SUPPLIER_CONTACT_VENDOR_MAP MAP
		WHERE 	MAP.SR_SUPPLIER_CONTACT_INFO_ID = @userid
			AND MAP.STATE_ID =  S.STATE_ID 
		group by S.STATE_NAME

	if @st=''
	set @st = null
return substring(@st,3,len(@st))
end
GO
GRANT EXECUTE ON  [dbo].[SR_SAD_FN_GET_STATE_NAMES_FOR_CONTACT] TO [CBMSApplication]
GO
