SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

CREATE FUNCTION [dbo].[SR_RFP_FN_GET_VENDOR_NAME](@vendor_id int) 
RETURNS varchar(200)
AS 

BEGIN 
	RETURN (select vendor_name from vendor(nolock) where vendor_id = @vendor_id)
END
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_VENDOR_NAME] TO [CBMSApplication]
GO
