SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

	create function [cdc].[fn_cdc_get_all_changes_hvr_1511077265]
	(	@from_lsn binary(10),
		@to_lsn binary(10),
		@row_filter_option nvarchar(30)
	)
	returns table
	return
	
	select NULL as __$start_lsn,
		NULL as __$seqval,
		NULL as __$operation,
		NULL as __$update_mask, NULL as [SR_SUPPLIER_CONTACT_INFO_ID], NULL as [USER_INFO_ID], NULL as [WORK_PHONE], NULL as [CELL_PHONE], NULL as [FAX], NULL as [ADDRESS_LINE1], NULL as [ADDRESS_LINE2], NULL as [CITY], NULL as [STATE_ID], NULL as [ZIP], NULL as [ADDITIONAL_CONTACT_INFO], NULL as [VENDOR_TYPE_ID], NULL as [PRIVILEGE_TYPE_ID], NULL as [IS_RFP_PREFERENCE_WEBSITE], NULL as [IS_RFP_PREFERENCE_EMAIL], NULL as [IS_UPDATED], NULL as [EMAIL_SENT_DATE], NULL as [Last_Change_Ts], NULL as [Termination_Contact_Email_Address], NULL as [Is_Default_Termination_Contact_Email_For_Vendor], NULL as [Registration_Contact_Email_Address], NULL as [Is_Default_Registration_Contact_Email_For_Vendor], NULL as [Comment_Id]
	where ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 0)

	union all
	
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[SR_SUPPLIER_CONTACT_INFO_ID], t.[USER_INFO_ID], t.[WORK_PHONE], t.[CELL_PHONE], t.[FAX], t.[ADDRESS_LINE1], t.[ADDRESS_LINE2], t.[CITY], t.[STATE_ID], t.[ZIP], t.[ADDITIONAL_CONTACT_INFO], t.[VENDOR_TYPE_ID], t.[PRIVILEGE_TYPE_ID], t.[IS_RFP_PREFERENCE_WEBSITE], t.[IS_RFP_PREFERENCE_EMAIL], t.[IS_UPDATED], t.[EMAIL_SENT_DATE], t.[Last_Change_Ts], t.[Termination_Contact_Email_Address], t.[Is_Default_Termination_Contact_Email_For_Vendor], t.[Registration_Contact_Email_Address], t.[Is_Default_Registration_Contact_Email_For_Vendor], t.[Comment_Id]
	from [cdc].[hvr_1511077265_CT] t with (nolock)    
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4)
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
		
	union all	
		
	select t.__$start_lsn as __$start_lsn,
		t.__$seqval as __$seqval,
		t.__$operation as __$operation,
		t.__$update_mask as __$update_mask, t.[SR_SUPPLIER_CONTACT_INFO_ID], t.[USER_INFO_ID], t.[WORK_PHONE], t.[CELL_PHONE], t.[FAX], t.[ADDRESS_LINE1], t.[ADDRESS_LINE2], t.[CITY], t.[STATE_ID], t.[ZIP], t.[ADDITIONAL_CONTACT_INFO], t.[VENDOR_TYPE_ID], t.[PRIVILEGE_TYPE_ID], t.[IS_RFP_PREFERENCE_WEBSITE], t.[IS_RFP_PREFERENCE_EMAIL], t.[IS_UPDATED], t.[EMAIL_SENT_DATE], t.[Last_Change_Ts], t.[Termination_Contact_Email_Address], t.[Is_Default_Termination_Contact_Email_For_Vendor], t.[Registration_Contact_Email_Address], t.[Is_Default_Registration_Contact_Email_For_Vendor], t.[Comment_Id]
	from [cdc].[hvr_1511077265_CT] t with (nolock)     
	where (lower(rtrim(ltrim(@row_filter_option))) = 'all update old')
	    and ( [sys].[fn_cdc_check_parameters]( N'hvr_1511077265', @from_lsn, @to_lsn, lower(rtrim(ltrim(@row_filter_option))), 0) = 1)
		and (t.__$operation = 1 or t.__$operation = 2 or t.__$operation = 4 or
		     t.__$operation = 3 )
		and (t.__$start_lsn <= @to_lsn)
		and (t.__$start_lsn >= @from_lsn)
	
GO
GRANT SELECT ON  [cdc].[fn_cdc_get_all_changes_hvr_1511077265] TO [public]
GO
