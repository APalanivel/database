SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function dbo.marktest(@account_id int, @commodityid int)
returns varchar(2000)
as
begin
	declare @st varchar(2000), @rate varchar(200)
	set @st=''
	select @st=@st+', '+rate_name from rate
--	select @st=case when @st is null then rate_name else ','+rate_name end from rate
	where rate_id in (select rate_id from meter
	where meter.rate_id=rate.rate_id and meter.account_id=@account_id
	and rate.commodity_type_id=@commodityid)
--select substring(@st,2,len(@st))

return substring(@st,3,len(@st))
end

GO
GRANT EXECUTE ON  [dbo].[marktest] TO [CBMSApplication]
GO
