SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










--select dbo.SR_RFP_FN_GET_GROUP_LP_IMAGE_ID(2) 


CREATE  FUNCTION dbo.SR_RFP_FN_GET_GROUP_LP_IMAGE_ID(@site_id int, @rfp_id int) 

RETURNS int
AS 
	
BEGIN 
	declare @cbms_image_id int
	select @cbms_image_id = cbms_image_id from sr_rfp_lp_client_approval(nolock) 
	where 	site_id = @site_id 
		and sr_rfp_id = @rfp_id
		and is_group_lp = 1

	RETURN isnull(@cbms_image_id, 0)
END 











GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_GROUP_LP_IMAGE_ID] TO [CBMSApplication]
GO
