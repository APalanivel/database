SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS OFF
GO
/******
NAME:
	
		
DESCRIPTION:


INPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------

OUTPUT PARAMETERS:
Name		DataType	Default		Description
------------------------------------------------------------


USAGE EXAMPLES:
------------------------------------------------------------


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS:
Initials	Date		Modification
------------------------------------------------------------
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/

--select * from meter order by account_id, rate_id
--select dbo.SR_RFP_FN_GET_RATES_ARCHIVE(1449) 

CREATE      FUNCTION [dbo].[SR_RFP_FN_GET_RATES_ARCHIVE](@rfp_account_id int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	
	declare @st varchar(2000), @rate varchar(200)
	set @st=''
	select @st=@st+', '+rate_name from sr_rfp_archive_meter archive_meter
	where sr_rfp_account_meter_map_id in (select distinct map.sr_rfp_account_meter_map_id
		from 	sr_rfp_account_meter_map map(nolock),	 			
			sr_rfp_account rfp_account(nolock)
		     
		where 	map.sr_rfp_account_id = @rfp_account_id
			and rfp_account.sr_rfp_account_id = map.sr_rfp_account_id
			and rfp_account.is_deleted = 0)
		group by archive_meter.rate_name

	if @st=''
	set @st = null
return substring(@st,3,len(@st))
end
GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_RATES_ARCHIVE] TO [CBMSApplication]
GO
