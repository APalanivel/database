SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--select dbo.SR_RFP_FN_GET_SM_SD_EMAIL_IDS (1099)

CREATE    FUNCTION dbo.SR_RFP_FN_GET_SM_SD_EMAIL_IDS(@userid int) 
RETURNS varchar(2000) 
AS 

BEGIN 
	

	declare @allEmailAddresses varchar(2000)
	DECLARE @managerGroupId int
	DECLARE @directorGroupId int
	
	select @managerGroupId=0
	select @directorGroupId=0	
	
	select @managerGroupId=(select group_info_id from group_info where group_name='sourcing.manager')
	select @directorGroupId=(select group_info_id from group_info where group_name='sourcing.director')

	set @allEmailAddresses=''
	select @allEmailAddresses=@allEmailAddresses+', '+email_address from user_info
	where user_info_id in (		
			select sourcing_manager_id from SR_SA_SM_MAP
			where sourcing_analyst_id=@userid and sourcing_manager_id in(
				select user_info_id from USER_INFO_GROUP_INFO_MAP where group_info_id in(
					@managerGroupId,@directorGroupId
				)	
			)
		
		)

	if @allEmailAddresses=''
	set @allEmailAddresses = null
return substring(@allEmailAddresses,3,len(@allEmailAddresses))
end





GO
GRANT EXECUTE ON  [dbo].[SR_RFP_FN_GET_SM_SD_EMAIL_IDS] TO [CBMSApplication]
GO
