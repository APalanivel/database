SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.ufn_Data_Mask

DESCRIPTION:
	Mask the data with the specified character and returns it back
		Alphabet's with X
		Numbers with 0

INPUT PARAMETERS:
	Name					DataType		Default	Description
------------------------------------------------------------
     @Input_String			nvarchar		
	 @Mask_Numeric_Value    BIT				0		Defines if the numeric values should be masked as 0

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
     
USAGE EXAMPLES:
------------------------------------------------------------

	SELECT dbo.ufn_Data_Mask('hganesan@ctepl.com','X','0','X',0,0)
    SELECT
      dbo.ufn_Data_Mask('hganesan123@ctepl.com', 'X', '0', 'X', 0, 0)
     ,dbo.ufn_Data_Mask('hganesan123@ctepl.com', 'X', '0', 'X', 1, 0)

	SELECT dbo.ufn_Data_Mask('Peter Parker','X','0','X',0,1)
	SELECT dbo.ufn_Data_Mask('+34 629778494','X','0','X',1,0)

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------

MODIFICATIONS
	Initials	Date		Modification
------------------------------------------------------------
    HG			2018-04-25	Created     	
******/
CREATE FUNCTION [dbo].[ufn_Data_Mask]
      (
       @Input_String VARCHAR(MAX)
      ,@String_Replacement_Char CHAR(1) = 'X'
      ,@Numeric_Replacement_Char CHAR(1) = '0'
      ,@Spl_Char_Replacement_Char CHAR(1) = 'X'
      ,@Mask_Numeric_Values BIT = 0 
      ,@Mask_Spl_Charcters BIT = 0)
RETURNS VARCHAR(MAX)
BEGIN

      DECLARE
            @loop_increment INT = 1
           ,@result VARCHAR(MAX) = '';

      SET @Input_String = UPPER(@Input_String);
      WHILE ( @loop_increment <= DATALENGTH(@Input_String) )
            BEGIN

                  SET @result = @result + CASE WHEN PATINDEX('%[A-Z]%', SUBSTRING(@Input_String, @loop_increment, 1)) = 1 THEN @String_Replacement_Char
                                               WHEN PATINDEX('%[0-9]%', SUBSTRING(@Input_String, @loop_increment, 1)) = 1
                                                    AND @Mask_Numeric_Values = 1 THEN @Numeric_Replacement_Char
                                               WHEN PATINDEX('%[^0-9]%', SUBSTRING(@Input_String, @loop_increment, 1)) = 1
                                                    AND PATINDEX('%[^A-Z]%', SUBSTRING(@Input_String, @loop_increment, 1)) = 1  
                                                    AND @Mask_Spl_Charcters = 1 THEN @Spl_Char_Replacement_Char
                                               ELSE SUBSTRING(@Input_String, @loop_increment, 1)
                                          END;

                  SET @loop_increment = @loop_increment + 1;

            END;

      RETURN @result;

END;


GO
GRANT EXECUTE ON  [dbo].[ufn_Data_Mask] TO [CBMSApplication]
GO
