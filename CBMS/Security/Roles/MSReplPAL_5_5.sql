CREATE ROLE [MSReplPAL_5_5]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_5', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_5', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_5', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_5', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_5', N'SUMMIT01\SQL_DataManager'
GO
