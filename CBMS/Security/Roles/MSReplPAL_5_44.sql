CREATE ROLE [MSReplPAL_5_44]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_44', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_44', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_44', N'Khush'
GO
EXEC sp_addrolemember N'MSReplPAL_5_44', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_44', N'SUMMIT01\SQL_DataManager'
GO
