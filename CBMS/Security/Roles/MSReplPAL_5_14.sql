CREATE ROLE [MSReplPAL_5_14]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_14', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_14', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_14', N'Khush'
GO
EXEC sp_addrolemember N'MSReplPAL_5_14', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_14', N'SUMMIT01\SQL_DataManager'
GO
