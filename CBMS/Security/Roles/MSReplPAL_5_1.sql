CREATE ROLE [MSReplPAL_5_1]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_1', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_1', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_1', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_1', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_1', N'SUMMIT01\SQL_DataManager'
GO
