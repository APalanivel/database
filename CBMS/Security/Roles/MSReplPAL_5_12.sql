CREATE ROLE [MSReplPAL_5_12]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_12', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_12', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_12', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_12', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_12', N'SUMMIT01\SQL_DataManager'
GO
