CREATE ROLE [MSReplPAL_5_13]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_13', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_13', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_13', N'Khush'
GO
EXEC sp_addrolemember N'MSReplPAL_5_13', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_13', N'SUMMIT01\SQL_DataManager'
GO
