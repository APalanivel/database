/****** Object:  DatabaseRole [BPOReportRole]    Script Date: 06/06/2011 15:42:04 ******/
CREATE ROLE [CBMS_SSRS_Reports] AUTHORIZATION [dbo]GRANT VIEW DATABASE STATE TO [CBMS_SSRS_Reports]

EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'badusumilli'

EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'bsirikonda'



EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'rvintha'

EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'spigilam'

EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'SUMMIT01\SQL_AdHoc_Access'



EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'vkumar'





EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'SUMMIT01\TidalProd'


EXEC sp_addrolemember N'CBMS_SSRS_Reports', N'CBMSReports'

GO
