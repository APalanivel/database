CREATE ROLE [MSReplPAL_5_9]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_9', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_9', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_9', N'Khush'
GO
EXEC sp_addrolemember N'MSReplPAL_5_9', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_9', N'SUMMIT01\SQL_DataManager'
GO
