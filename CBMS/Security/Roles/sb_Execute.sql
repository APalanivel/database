CREATE ROLE [sb_Execute]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'sb_Execute', N'vkumar'

GO
EXEC sp_addrolemember N'sb_Execute', N'sb_CBMS_Service'
GO
EXEC sp_addrolemember N'sb_Execute', N'sb_DV2_Service'
GO
EXEC sp_addrolemember N'sb_Execute', N'sb_DVDEHub_Service'
GO
EXEC sp_addrolemember N'sb_Execute', N'sb_SV_Service'
GO
