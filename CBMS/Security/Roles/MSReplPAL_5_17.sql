CREATE ROLE [MSReplPAL_5_17]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_17', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_17', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_17', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_17', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_17', N'SUMMIT01\SQL_DataManager'
GO
