CREATE ROLE [CBMSApplication]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CBMSApplication', N'akolipaka'
GO
EXEC sp_addrolemember N'CBMSApplication', N'badusumilli'
GO
EXEC sp_addrolemember N'CBMSApplication', N'bsirikonda'
GO
EXEC sp_addrolemember N'CBMSApplication', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'CBMSApplication', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'CBMSApplication', N'CBMSReports'
GO
EXEC sp_addrolemember N'CBMSApplication', N'dv2netuser'
GO
EXEC sp_addrolemember N'CBMSApplication', N'Hari'
GO
EXEC sp_addrolemember N'CBMSApplication', N'HKurma'
GO
EXEC sp_addrolemember N'CBMSApplication', N'kgonella'
GO
EXEC sp_addrolemember N'CBMSApplication', N'ngangireddy'
GO
EXEC sp_addrolemember N'CBMSApplication', N'NKosuri'
GO
EXEC sp_addrolemember N'CBMSApplication', N'NReddy'
GO
EXEC sp_addrolemember N'CBMSApplication', N'PRajput'
GO
EXEC sp_addrolemember N'CBMSApplication', N'PVuyyuru'
GO
EXEC sp_addrolemember N'CBMSApplication', N'rchejarla'
GO
EXEC sp_addrolemember N'CBMSApplication', N'RVegesna'
GO
EXEC sp_addrolemember N'CBMSApplication', N'rvintha'
GO
EXEC sp_addrolemember N'CBMSApplication', N'SUMMIT01\Development Team'
GO
EXEC sp_addrolemember N'CBMSApplication', N'SUMMIT01\SQL_AdHoc_Access'
GO
EXEC sp_addrolemember N'CBMSApplication', N'SUMMIT01\SQL_DataManager'
GO
EXEC sp_addrolemember N'CBMSApplication', N'SUMMIT01\TidalProd'
GO
EXEC sp_addrolemember N'CBMSApplication', N'UBM_Monitoring'
GO
EXEC sp_addrolemember N'CBMSApplication', N'vkumar'
GO
GRANT VIEW DEFINITION TO [CBMSApplication]
GRANT VIEW DATABASE STATE TO [CBMSApplication]
