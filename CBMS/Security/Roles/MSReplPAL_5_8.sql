CREATE ROLE [MSReplPAL_5_8]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_8', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_8', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_8', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_8', N'SUMMIT01\SQL_DataManager'
GO
