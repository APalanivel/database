CREATE ROLE [MSReplPAL_5_111]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_111', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_111', N'SUMMIT01\SQL_Backup_Managers'
GO
EXEC sp_addrolemember N'MSReplPAL_5_111', N'vkumar'
GO
