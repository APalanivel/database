CREATE ROLE [MSReplPAL_5_7]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_7', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_7', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_7', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_7', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_7', N'SUMMIT01\SQL_DataManager'
GO
