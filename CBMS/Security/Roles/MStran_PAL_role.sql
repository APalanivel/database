CREATE ROLE [MStran_PAL_role]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_111'

GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_1'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_12'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_17'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_2'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_5'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_7'
GO
EXEC sp_addrolemember N'MStran_PAL_role', N'MSReplPAL_5_8'
GO
