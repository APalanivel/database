CREATE ROLE [MSReplPAL_5_25]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MSReplPAL_5_25', N'CBMSJavaUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_25', N'CBMSNetUser'
GO
EXEC sp_addrolemember N'MSReplPAL_5_25', N'Khush'
GO
EXEC sp_addrolemember N'MSReplPAL_5_25', N'ReplicationSVC'
GO
EXEC sp_addrolemember N'MSReplPAL_5_25', N'SUMMIT01\SQL_DataManager'
GO
