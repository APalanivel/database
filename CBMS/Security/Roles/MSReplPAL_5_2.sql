CREATE ROLE [MSReplPAL_5_2]
AUTHORIZATION [dbo]
EXEC sp_addrolemember N'MSReplPAL_5_2', N'dv2netuser'

GO
EXEC sp_addrolemember N'MSReplPAL_5_2', N'ReplicationSVC'
GO
