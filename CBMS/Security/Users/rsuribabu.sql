IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rsuribabu')
CREATE LOGIN [rsuribabu] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rsuribabu] FOR LOGIN [rsuribabu]
GO
