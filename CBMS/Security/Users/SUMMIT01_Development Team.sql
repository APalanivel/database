IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SUMMIT01\Development Team')
CREATE LOGIN [SUMMIT01\Development Team] FROM WINDOWS
GO
CREATE USER [SUMMIT01\Development Team] FOR LOGIN [SUMMIT01\Development Team]
GO
GRANT CONNECT TO [SUMMIT01\Development Team]
