IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rchejarla')
CREATE LOGIN [rchejarla] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rchejarla] FOR LOGIN [rchejarla]
GO
GRANT VIEW DEFINITION TO [rchejarla]
GRANT VIEW DATABASE STATE TO [rchejarla]
