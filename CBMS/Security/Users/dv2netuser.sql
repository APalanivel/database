IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'dv2netuser')
CREATE LOGIN [dv2netuser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [dv2netuser] FOR LOGIN [dv2netuser]
GO
