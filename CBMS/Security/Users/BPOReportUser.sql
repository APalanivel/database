IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'BPOReportUser')
CREATE LOGIN [BPOReportUser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [BPOReportUser] FOR LOGIN [BPOReportUser]
GO
