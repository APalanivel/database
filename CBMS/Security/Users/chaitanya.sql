IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'chaitanya')
CREATE LOGIN [chaitanya] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [chaitanya] FOR LOGIN [chaitanya]
GO
GRANT CONNECT TO [chaitanya]
GRANT CONNECT REPLICATION TO [chaitanya]
GRANT VIEW DEFINITION TO [chaitanya]
GRANT VIEW DATABASE STATE TO [chaitanya]
