IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Sandeep')
CREATE LOGIN [Sandeep] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Sandeep] FOR LOGIN [Sandeep]
GO
