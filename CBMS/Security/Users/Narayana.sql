IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Narayana')
CREATE LOGIN [Narayana] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Narayana] FOR LOGIN [Narayana]
GO
