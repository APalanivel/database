IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'UBM_Monitoring')
CREATE LOGIN [UBM_Monitoring] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [UBM_Monitoring] FOR LOGIN [UBM_Monitoring]
GO
GRANT CONNECT TO [UBM_Monitoring]
