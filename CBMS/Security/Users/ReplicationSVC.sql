IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ReplicationSVC')
CREATE LOGIN [ReplicationSVC] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ReplicationSVC] FOR LOGIN [ReplicationSVC]
GO
GRANT CONNECT TO [ReplicationSVC]
GRANT CONNECT REPLICATION TO [ReplicationSVC]
