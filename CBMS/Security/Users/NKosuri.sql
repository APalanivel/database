IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'NKosuri')
CREATE LOGIN [NKosuri] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [NKosuri] FOR LOGIN [NKosuri]
GO
GRANT VIEW DEFINITION TO [NKosuri]
