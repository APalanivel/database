CREATE USER [smuhamed] FOR LOGIN [smuhamed]
GO
GRANT SHOWPLAN TO [smuhamed]
GRANT VIEW DEFINITION TO [smuhamed]
GRANT VIEW DATABASE STATE TO [smuhamed]
