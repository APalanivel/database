IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rvintha')
CREATE LOGIN [rvintha] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rvintha] FOR LOGIN [rvintha]
GO
GRANT CREATE DEFAULT TO [rvintha]
GRANT CREATE PROCEDURE TO [rvintha]
GRANT CREATE ROLE TO [rvintha]
GRANT CREATE SCHEMA TO [rvintha]
GRANT CREATE TABLE TO [rvintha]
GRANT CREATE VIEW TO [rvintha]
GRANT SHOWPLAN TO [rvintha]
GRANT VIEW DEFINITION TO [rvintha]
