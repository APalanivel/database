IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'HKurma')
CREATE LOGIN [HKurma] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [HKurma] FOR LOGIN [HKurma]
GO
GRANT VIEW DEFINITION TO [HKurma]
