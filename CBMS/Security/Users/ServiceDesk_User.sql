IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ServiceDesk_User')
CREATE LOGIN [ServiceDesk_User] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ServiceDesk_User] FOR LOGIN [ServiceDesk_User]
GO
