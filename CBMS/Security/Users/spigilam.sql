IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'spigilam')
CREATE LOGIN [spigilam] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [spigilam] FOR LOGIN [spigilam]
GO
GRANT SHOWPLAN TO [spigilam]
GRANT VIEW DEFINITION TO [spigilam]
