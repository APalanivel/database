IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'DBAMReporting')
CREATE LOGIN [DBAMReporting] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [dbamReporting] FOR LOGIN [DBAMReporting]
GO
