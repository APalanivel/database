IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'alonachan')
CREATE LOGIN [alonachan] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [alonachan] FOR LOGIN [alonachan]
GO
