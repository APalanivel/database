IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'rkodela')
CREATE LOGIN [rkodela] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [rkodela] FOR LOGIN [rkodela]
GO
