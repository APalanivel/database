IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'badusumilli')
CREATE LOGIN [badusumilli] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [badusumilli] FOR LOGIN [badusumilli]
GO
GRANT SHOWPLAN TO [badusumilli]
