IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'QlikUser')
CREATE LOGIN [QlikUser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [QlikUser] FOR LOGIN [QlikUser]
GO
