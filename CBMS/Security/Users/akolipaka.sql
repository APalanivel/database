IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'akolipaka')
CREATE LOGIN [akolipaka] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [akolipaka] FOR LOGIN [akolipaka]
GO
