IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'CBMSReports')
CREATE LOGIN [CBMSReports] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [CBMSReports] FOR LOGIN [CBMSReports]
GO
GRANT CONNECT TO [CBMSReports]
