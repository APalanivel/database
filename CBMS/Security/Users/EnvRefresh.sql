IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'EnvRefresh')
CREATE LOGIN [EnvRefresh] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [EnvRefresh] FOR LOGIN [EnvRefresh]
GO
