IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SUMMIT01\Business Solutions Team')
CREATE LOGIN [SUMMIT01\Business Solutions Team] FROM WINDOWS
GO
CREATE USER [SUMMIT01\Business Solutions Team] FOR LOGIN [SUMMIT01\Business Solutions Team]
GO
GRANT CONNECT TO [SUMMIT01\Business Solutions Team]
