IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'apulikkal')
CREATE LOGIN [apulikkal] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [apulikkal] FOR LOGIN [apulikkal]
GO
GRANT SHOWPLAN TO [apulikkal]
GRANT VIEW DEFINITION TO [apulikkal]
