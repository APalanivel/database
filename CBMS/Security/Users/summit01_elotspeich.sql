IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SUMMIT01\elotspeich')
CREATE LOGIN [SUMMIT01\elotspeich] FROM WINDOWS
GO
CREATE USER [summit01\elotspeich] FOR LOGIN [SUMMIT01\elotspeich]
GO
GRANT CONNECT TO [summit01\elotspeich]
