IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'kgonella')
CREATE LOGIN [kgonella] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [kgonella] FOR LOGIN [kgonella]
GO
