IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'PVuyyuru')
CREATE LOGIN [PVuyyuru] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [PVuyyuru] FOR LOGIN [PVuyyuru]
GO
GRANT VIEW DEFINITION TO [PVuyyuru]
