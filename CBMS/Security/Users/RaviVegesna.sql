IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'RaviVegesna')
CREATE LOGIN [RaviVegesna] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [RaviVegesna] FOR LOGIN [RaviVegesna]
GO
