IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'TestRunner')
CREATE LOGIN [TestRunner] WITH PASSWORD = 'p@ssw0rd'
GO
