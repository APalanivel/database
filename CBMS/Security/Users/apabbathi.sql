IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'apabbathi')
CREATE LOGIN [apabbathi] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [apabbathi] FOR LOGIN [apabbathi]
GO
