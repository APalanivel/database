IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'bsirikonda')
CREATE LOGIN [bsirikonda] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [bsirikonda] FOR LOGIN [bsirikonda]
GO
GRANT SHOWPLAN TO [bsirikonda]
