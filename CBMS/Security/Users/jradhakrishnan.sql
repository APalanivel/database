IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'jradhakrishnan')
CREATE LOGIN [jradhakrishnan] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [jradhakrishnan] FOR LOGIN [jradhakrishnan]
GO
