IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'RVegesna')
CREATE LOGIN [RVegesna] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [RVegesna] FOR LOGIN [RVegesna]
GO
