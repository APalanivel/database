IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SVReplication')
CREATE LOGIN [SVReplication] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SVReplication] FOR LOGIN [SVReplication]
GO
GRANT CONNECT TO [SVReplication]
