CREATE TABLE [dbo].[INV_SOURCE_EXCLUDE_FOLDER]
(
[INV_SOURCE_EXCLUDE_FOLDER_ID] [int] NOT NULL IDENTITY(1, 1),
[INV_SOURCE_ID] [int] NOT NULL,
[EXCLUDE_FOLDER_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Invoice]
ALTER TABLE [dbo].[INV_SOURCE_EXCLUDE_FOLDER] ADD
CONSTRAINT [FK_INV_SOURCE_EXCLUDE_FOLDER_INV_SOURCE] FOREIGN KEY ([INV_SOURCE_ID]) REFERENCES [dbo].[INV_SOURCE] ([INV_SOURCE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Any folder to exclude from the invoice scraping process.  Tables references the INV_Source list', 'SCHEMA', N'dbo', 'TABLE', N'INV_SOURCE_EXCLUDE_FOLDER', NULL, NULL
GO

ALTER TABLE [dbo].[INV_SOURCE_EXCLUDE_FOLDER] ADD 
CONSTRAINT [PK_INV_SOURCE_EXCLUDE_FOLDER] PRIMARY KEY CLUSTERED  ([INV_SOURCE_EXCLUDE_FOLDER_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [IX_Inv_Source_ID] ON [dbo].[INV_SOURCE_EXCLUDE_FOLDER] ([INV_SOURCE_ID]) ON [DB_INDEXES01]




GO
