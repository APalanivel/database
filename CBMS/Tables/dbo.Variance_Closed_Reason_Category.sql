CREATE TABLE [dbo].[Variance_Closed_Reason_Category]
(
[Closed_Reason_Id] [int] NOT NULL IDENTITY(1, 1),
[Closure_Category_Cd] [int] NOT NULL,
[Closed_Reason_Cd] [int] NOT NULL,
[Is_External_Comments_Required] [bit] NULL,
[Is_Active] [bit] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Closed_Reason_Category] ADD CONSTRAINT [PK__Variance__D40417837034674F] PRIMARY KEY CLUSTERED  ([Closed_Reason_Id]) ON [DB_DATA01]
GO
