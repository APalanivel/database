CREATE TABLE [dbo].[RECALC_HEADER]
(
[RECALC_HEADER_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CU_INVOICE_ID] [int] NOT NULL,
[ACCOUNT_ID] [int] NOT NULL,
[CONTRACT_ID] [int] NOT NULL,
[GRAND_TOTAL] [decimal] (32, 16) NULL,
[DATE_PERFORMED] [datetime] NOT NULL,
[POST_DATA] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RECALC_XML] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Recalc_Header__Last_Change_Ts] DEFAULT (getdate()),
[Commodity_Id] [int] NOT NULL CONSTRAINT [df_RECALC_HEADER__Commodity_Id] DEFAULT ((-1)),
[Created_User_Id] [int] NULL,
[Recalc_Failure_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[RECALC_HEADER] ADD CONSTRAINT [PK_RECALC_HEADER] PRIMARY KEY CLUSTERED  ([RECALC_HEADER_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[RECALC_HEADER] ADD CONSTRAINT [unc_Recalc_Header__Account_Id__Cu_Invoice_ID__Commodity_Id] UNIQUE NONCLUSTERED  ([ACCOUNT_ID], [CU_INVOICE_ID], [Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_CU_INVOICE_ID] ON [dbo].[RECALC_HEADER] ([CU_INVOICE_ID]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[RECALC_HEADER] ADD CONSTRAINT [unc_Recalc_Header__Cu_Invoice_ID__Account_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([CU_INVOICE_ID], [ACCOUNT_ID], [Commodity_Id]) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[RECALC_HEADER] ADD CONSTRAINT [FK_RECALC_HEADER_CU_INVOICE] FOREIGN KEY ([CU_INVOICE_ID]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used in recalculating supply contract invoices', 'SCHEMA', N'dbo', 'TABLE', N'RECALC_HEADER', NULL, NULL
GO
