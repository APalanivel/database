CREATE TABLE [dbo].[Variance_Engine_Param_Value]
(
[Variance_Engine_Param_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Processing_Engine_Id] [int] NOT NULL,
[Variance_Extraction_Service_Param_Id] [int] NOT NULL,
[Param_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Engine_Param_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Engine_Param_Value] ADD CONSTRAINT [pk_Variance_Engine_Param_Value] PRIMARY KEY CLUSTERED  ([Variance_Engine_Param_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Engine_Param_Value] ADD CONSTRAINT [un_Variance_Engine_Param_Value__Variance_Processing_Engine_Id__Variance_Extraction_Service_Param_Id] UNIQUE NONCLUSTERED  ([Variance_Processing_Engine_Id], [Variance_Extraction_Service_Param_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Engine_Param_Value] ADD CONSTRAINT [fk_Variance_Extraction_Service_Param__Variance_Engine_Param_Value] FOREIGN KEY ([Variance_Extraction_Service_Param_Id]) REFERENCES [dbo].[Variance_Extraction_Service_Param] ([Variance_Extraction_Service_Param_Id])
GO
ALTER TABLE [dbo].[Variance_Engine_Param_Value] ADD CONSTRAINT [fk_Variance_Processing_Engine__Variance_Engine_Param_Value] FOREIGN KEY ([Variance_Processing_Engine_Id]) REFERENCES [dbo].[Variance_Processing_Engine] ([Variance_Processing_Engine_Id])
GO
