CREATE TABLE [dbo].[Varince_Processing_Engine]
(
[Varince_Processing_Engine_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Engine_Cd] [int] NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Varince_Processing_Engine__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Varince_Processing_Engine] ADD CONSTRAINT [pk_Varince_Processing_Engine] PRIMARY KEY CLUSTERED  ([Varince_Processing_Engine_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Varince_Processing_Engine] ADD CONSTRAINT [un_Varince_Processing_Engine__Variance_Engine_Cd] UNIQUE NONCLUSTERED  ([Variance_Engine_Cd]) ON [DB_DATA01]
GO
