CREATE TABLE [dbo].[EXCEPTION_QUEUE_PRIORITY]
(
[EXCEPTION_QUEUE_PRIORITY_ID] [int] NOT NULL IDENTITY(1, 1),
[EXCEPTION_TYPE_ID] [int] NOT NULL,
[QUEUE_ID] [int] NOT NULL,
[PRIORITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table stores the exception queue priority in C&U tool', 'SCHEMA', N'dbo', 'TABLE', N'EXCEPTION_QUEUE_PRIORITY', NULL, NULL
GO

ALTER TABLE [dbo].[EXCEPTION_QUEUE_PRIORITY] WITH NOCHECK ADD
CONSTRAINT [QUEUE_EXCEPTION_QUEUE_FK] FOREIGN KEY ([QUEUE_ID]) REFERENCES [dbo].[QUEUE] ([QUEUE_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[EXCEPTION_QUEUE_PRIORITY] ADD 
CONSTRAINT [EXCEPTION_QUEUE_PRIORITY_PK] PRIMARY KEY CLUSTERED  ([EXCEPTION_QUEUE_PRIORITY_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [EXCEPTION_QUEUE_PRIORITY_N_2] ON [dbo].[EXCEPTION_QUEUE_PRIORITY] ([EXCEPTION_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [EXCEPTION_QUEUE_PRIORITY_N_1] ON [dbo].[EXCEPTION_QUEUE_PRIORITY] ([QUEUE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

ALTER TABLE [dbo].[EXCEPTION_QUEUE_PRIORITY] ADD
CONSTRAINT [ENTITY_EXCEPTION_QUEUE_FK] FOREIGN KEY ([EXCEPTION_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])










GO
