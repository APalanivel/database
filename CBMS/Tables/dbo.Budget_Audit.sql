CREATE TABLE [dbo].[Budget_Audit]
(
[Audit_Function] [smallint] NOT NULL,
[Audit_Ts] [datetime] NOT NULL,
[Budget_Id] [int] NOT NULL
) ON [PRIMARY]
CREATE CLUSTERED INDEX [CIX_Budget_Audit__Budget_ID__Audit_Ts] ON [dbo].[Budget_Audit] ([Budget_Id], [Audit_Ts]) ON [PRIMARY]

GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to Budget table', 'SCHEMA', N'dbo', 'TABLE', N'Budget_Audit', NULL, NULL
GO
