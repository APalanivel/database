CREATE TABLE [dbo].[Variance_Parameter_Commodity_Map]
(
[Commodity_Id] [int] NOT NULL,
[Variance_Parameter_Id] [int] NOT NULL,
[variance_process_Engine_id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Parameter_Commodity_Map] ADD CONSTRAINT [pk_Variance_Parameter_Commodity_Map] PRIMARY KEY CLUSTERED  ([Commodity_Id], [Variance_Parameter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Parameter_Commodity_Map] ADD CONSTRAINT [fk_Commodity__Variance_Parameter_Commodity_Map] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
ALTER TABLE [dbo].[Variance_Parameter_Commodity_Map] ADD CONSTRAINT [FK_Variance_Processing_Engine_Id] FOREIGN KEY ([variance_process_Engine_id]) REFERENCES [dbo].[Variance_Processing_Engine] ([Variance_Processing_Engine_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of variance parameter to commodity', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Parameter_Commodity_Map', NULL, NULL
GO
