CREATE TABLE [dbo].[Comment]
(
[Comment_ID] [int] NOT NULL IDENTITY(1, 1),
[Comment_Type_CD] [int] NULL,
[Comment_User_Info_Id] [int] NOT NULL,
[Comment_Dt] [datetime] NOT NULL CONSTRAINT [DF__Comment__Comment__7F5F20B2] DEFAULT (getdate()),
[Comment_Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comment] ADD CONSTRAINT [PK_Comment] PRIMARY KEY NONCLUSTERED  ([Comment_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'General Comments table. Distinction of where the comments are used - comment_type_cd', 'SCHEMA', N'dbo', 'TABLE', N'Comment', NULL, NULL
GO
