CREATE TABLE [Trade].[RM_Forecast_Volume_Batch]
(
[RM_Forecast_Volume_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Client_Hier_Onboard_Id] [int] NOT NULL,
[Forecast_Start_Dt] [date] NOT NULL,
[Forecast_End_Dt] [date] NOT NULL,
[Current_Step] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Current_Step_Status_Cd] [int] NOT NULL,
[Batch_Status_Cd] [int] NOT NULL,
[Requested_User_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Forecast_Volume_BatchCreated_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Forecast_Volume_BatchLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Forecast_Volume_Batch] ADD CONSTRAINT [pk_RM_Forecast_Volume_Batch] PRIMARY KEY CLUSTERED  ([RM_Forecast_Volume_Batch_Id]) ON [DB_DATA01]
GO
