CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment]
(
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL,
[Cu_Invoice_Account_Commodity_Comment_Id] [int] NOT NULL IDENTITY(1, 1)
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment] ADD CONSTRAINT [PK__Cu_Invoi__5B1020AF5FF87D1C] PRIMARY KEY NONCLUSTERED  ([Cu_Invoice_Account_Commodity_Comment_Id]) ON [DB_DATA01]
GO
CREATE CLUSTERED INDEX [Cu_Invoice_Account_Commodity_Comment__Comment_ID] ON [dbo].[Cu_Invoice_Account_Commodity_Comment] ([Comment_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Cu_Invoice_Account_Commodity_Comment] ON [dbo].[Cu_Invoice_Account_Commodity_Comment] ([Cu_Invoice_Id], [Account_Id], [Commodity_Id]) INCLUDE ([Comment_Id], [Cu_Invoice_Account_Commodity_Comment_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment] ADD CONSTRAINT [fk_ACCOUNT__Cu_Invoice_Account_Commodity_Comment] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment] ADD CONSTRAINT [fk_Comment__Cu_Invoice_Account_Commodity_Comment] FOREIGN KEY ([Comment_Id]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment] ADD CONSTRAINT [fk_Commodity__Cu_Invoice_Account_Commodity_Comment] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment] ADD CONSTRAINT [fk_CU_INVOICE_Cu_Invoice_Account_Commodity_Comment] FOREIGN KEY ([Cu_Invoice_Id]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
