CREATE TABLE [dbo].[Commodity_UOM_Conversion]
(
[Base_Commodity_Id] [int] NOT NULL,
[Base_UOM_Cd] [int] NOT NULL,
[Converted_Commodity_Id] [int] NOT NULL,
[Converted_UOM_Cd] [int] NOT NULL,
[Conversion_Factor] [decimal] (32, 16) NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Commodity_UOM_Conversion_Is_Active] DEFAULT ((1))
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Conversion factors used to convert different commodity unit of measures', 'SCHEMA', N'dbo', 'TABLE', N'Commodity_UOM_Conversion', NULL, NULL
GO

ALTER TABLE [dbo].[Commodity_UOM_Conversion] ADD 
CONSTRAINT [pk_Commodity_UOM_Conversion] PRIMARY KEY CLUSTERED  ([Base_Commodity_Id], [Base_UOM_Cd], [Converted_Commodity_Id], [Converted_UOM_Cd]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
CREATE NONCLUSTERED INDEX [ix_Commodity_UOM_Conversion__Converted_Commodity_Id__Converted_UOM_Cd] ON [dbo].[Commodity_UOM_Conversion] ([Converted_Commodity_Id], [Converted_UOM_Cd]) INCLUDE ([Base_Commodity_Id], [Base_UOM_Cd], [Conversion_Factor]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Commodity_UOM_Conversion] ADD
CONSTRAINT [fk_Commodity_UOM_Conversion_Commidity_Base_Commodity_Id] FOREIGN KEY ([Base_Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
ALTER TABLE [dbo].[Commodity_UOM_Conversion] ADD
CONSTRAINT [fk_Commodity_UOM_Conversion_Commidity_Converted_Commodity_Id] FOREIGN KEY ([Converted_Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])


GO
