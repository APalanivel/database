CREATE TABLE [dbo].[Market_Price_Point_State_Map]
(
[Market_Price_Point_ID] [int] NOT NULL,
[State_ID] [int] NOT NULL,
[rowguid] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__Market_Pr__rowgu__1342251B] DEFAULT (newid())
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'States that the Named Price Point applies to.', 'SCHEMA', N'dbo', 'TABLE', N'Market_Price_Point_State_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Market_Price_Point_State_Map] ADD 
CONSTRAINT [PK_MARKET_PRICE_POINT_STATE_MAP] PRIMARY KEY CLUSTERED  ([Market_Price_Point_ID], [State_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
CREATE UNIQUE NONCLUSTERED INDEX [index_117861224] ON [dbo].[Market_Price_Point_State_Map] ([rowguid]) ON [DB_INDEXES01]



GO
