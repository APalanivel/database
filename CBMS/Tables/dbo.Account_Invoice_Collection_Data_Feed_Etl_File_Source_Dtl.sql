CREATE TABLE [dbo].[Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl]
(
[Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Invoice_Collection_Source_Id] [int] NOT NULL,
[File_Source_Cd] [int] NOT NULL,
[Data_Feed_Type_Cd] [int] NULL,
[Vendor_Id] [int] NULL,
[URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Login_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Passcode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Generic_Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Website_Instructions] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instruction_Document_Image_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] ADD CONSTRAINT [pk_Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] ADD CONSTRAINT [un_Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] UNIQUE NONCLUSTERED  ([Account_Invoice_Collection_Source_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Account_Invoice_Collection_Data_Feed_Etl_File_Source_Dtl] FOREIGN KEY ([Account_Invoice_Collection_Source_Id]) REFERENCES [dbo].[Account_Invoice_Collection_Source] ([Account_Invoice_Collection_Source_Id])
GO
