CREATE TABLE [Trade].[Rm_Budget]
(
[Rm_Budget_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Budget_NAME] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Index_Id] [int] NOT NULL,
[Budget_Type_Cd] [int] NOT NULL,
[Budget_Level_Cd] [int] NULL,
[Budget_Start_Dt] [date] NOT NULL,
[Budget_End_Dt] [date] NOT NULL,
[Is_Client_Generated] [bit] NOT NULL CONSTRAINT [Df_Rm_Budget__Is_Client_Generated] DEFAULT ((0)),
[Uom_Type_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Rm_Forecast_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Rm_Budget__Created_Ts] DEFAULT (getdate()),
[Is_Budget_Approved] [bit] NOT NULL CONSTRAINT [Df_Rm_Budget__Is_Budget_Approved] DEFAULT ((0)),
[Approved_By_User_Info_Id] [int] NULL,
[Approved_Ts] [datetime] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df__Trade_Rm_Budget__Last_Change_Ts] DEFAULT (getdate()),
[RM_Budget_Participant_Sites_Type_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget] ADD CONSTRAINT [PK_Rm_Budget__Rm_Budget_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget] ADD CONSTRAINT [Uk_Trade_RM_Budget] UNIQUE NONCLUSTERED  ([Client_Id], [Budget_NAME], [Commodity_Id]) ON [DB_DATA01]
GO
