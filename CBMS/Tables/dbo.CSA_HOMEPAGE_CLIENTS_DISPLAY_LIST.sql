CREATE TABLE [dbo].[CSA_HOMEPAGE_CLIENTS_DISPLAY_LIST]
(
[CSA_HOMEPAGE_CLIENTS_DISPLAY_LIST_ID] [int] NOT NULL IDENTITY(1, 1),
[USER_INFO_ID] [int] NOT NULL,
[CLIENT_ID] [int] NOT NULL,
[CLIENT_DISPLAY_ORDER] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table was designed later for CSA Homepage functionality to store the clients list preference for a CSA', 'SCHEMA', N'dbo', 'TABLE', N'CSA_HOMEPAGE_CLIENTS_DISPLAY_LIST', NULL, NULL
GO
