CREATE TABLE [dbo].[EC_Contract_Attribute_Tracking]
(
[EC_Contract_Attribute_Tracking_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[EC_Contract_Attribute_Id] [int] NOT NULL,
[EC_Contract_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute_Tracking__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute_Tracking__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Tracking] ADD CONSTRAINT [unc_EC_Contract_Attribute_Tracking__Contract_Id__EC_Contract_Attribute_Id] UNIQUE CLUSTERED  ([Contract_Id], [EC_Contract_Attribute_Id]) ON [DB_DATA01]

GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Tracking] ADD CONSTRAINT [pk_EC_Contract_Attribute_Tracking] PRIMARY KEY NONCLUSTERED  ([EC_Contract_Attribute_Tracking_Id]) ON [DB_DATA01]
GO

ALTER TABLE [dbo].[EC_Contract_Attribute_Tracking] ADD CONSTRAINT [fk_Contract__EC_Contract_Attribute_Tracking] FOREIGN KEY ([Contract_Id]) REFERENCES [dbo].[CONTRACT] ([CONTRACT_ID])
GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Tracking] ADD CONSTRAINT [fk_EC_Contract_Attribute__EC_Contract_Attribute_Tracking] FOREIGN KEY ([EC_Contract_Attribute_Id]) REFERENCES [dbo].[EC_Contract_Attribute] ([EC_Contract_Attribute_Id])
GO
