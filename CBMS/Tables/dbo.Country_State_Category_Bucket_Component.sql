CREATE TABLE [dbo].[Country_State_Category_Bucket_Component]
(
[Country_State_Category_Bucket_Component_Id] [int] NOT NULL IDENTITY(1, 1),
[Country_State_Category_Bucket_Aggregation_Rule_Id] [int] NOT NULL,
[Calculation_Param_Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Source_Id] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Source_Type_Cd] [int] NOT NULL,
[Uom_Type_Id] [int] NOT NULL,
[Default_Value] [decimal] (28, 2) NOT NULL CONSTRAINT [df__CSCBC_Default_Value] DEFAULT ((0)),
[Is_Use_Default_Value_When_Null] [bit] NOT NULL CONSTRAINT [df__CSCBC_Is_Use_Default_Value_When_Null] DEFAULT ((0))
) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[Country_State_Category_Bucket_Component] ADD CONSTRAINT [PK_Country_State_Category_Bucket_Component] PRIMARY KEY CLUSTERED  ([Country_State_Category_Bucket_Component_Id]) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[Country_State_Category_Bucket_Component] ADD CONSTRAINT [FK_Country_State_Category_Bucket_Component__Country_State_Category_Bucket_Aggregation_Rule] FOREIGN KEY ([Country_State_Category_Bucket_Aggregation_Rule_Id]) REFERENCES [dbo].[Country_State_Category_Bucket_Aggregation_Rule] ([Country_State_Category_Bucket_Aggregation_Rule_Id])
GO
