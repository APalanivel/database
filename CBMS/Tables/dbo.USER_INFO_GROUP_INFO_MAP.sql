CREATE TABLE [dbo].[USER_INFO_GROUP_INFO_MAP]
(
[GROUP_INFO_ID] [int] NOT NULL,
[USER_INFO_ID] [int] NOT NULL,
[Assigned_User_Id] [int] NOT NULL CONSTRAINT [df__User_Info_Group_Info_Map__Assigned_User_Id] DEFAULT ((-1)),
[Assigned_Ts] [datetime] NOT NULL CONSTRAINT [df_User_Info_Group_Info_Map__Assigned_Ts] DEFAULT (getdate())
) ON [DBData_UserInfo]
ALTER TABLE [dbo].[USER_INFO_GROUP_INFO_MAP] ADD
CONSTRAINT [USER_INFO_USER_GROUP_MAP_FK] FOREIGN KEY ([USER_INFO_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Junction between user and Groups. Used to grant permissions to application features', 'SCHEMA', N'dbo', 'TABLE', N'USER_INFO_GROUP_INFO_MAP', NULL, NULL
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/******
NAME:	tr_User_Info_Group_Info_Map_Transfer

DESCRIPTION: Trigger to send changes for DV2 server through service_broker.



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	TP			Anoop
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
	TP			01 Mar 2016		Modified to add Hub replication message
******/
CREATE TRIGGER [dbo].[tr_User_Info_Group_Info_Map_Transfer] ON [dbo].[USER_INFO_GROUP_INFO_MAP]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT
                        i.User_Info_Id AS User_Info_Id
                       ,i.Group_Info_Id AS Group_Info_Id
                       ,d.User_Info_Id AS Old_User_Info_Id
                       ,d.Group_Info_Id AS Old_Group_Info_Id
                       ,i.Assigned_User_Id
                       ,i.Assigned_Ts
                       ,case WHEN i.User_Info_Id IS NOT NULL			-- Checking one column is sufficent
                                  AND d.User_Info_Id IS NULL THEN 'I'
                             WHEN i.User_Info_Id IS NULL
                                  AND d.User_Info_Id IS NOT NULL THEN 'D'
                             WHEN i.User_Info_Id IS NOT NULL
                                  AND d.User_Info_Id IS NOT NULL THEN 'U'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.User_Info_Id = d.User_Info_Id
                                 AND i.GROUP_INFO_ID = d.GROUP_INFO_ID
            FOR
                       XML PATH('User_Info_Group_Info_Map_Change')
                          ,ELEMENTS
                          ,ROOT('User_Info_Group_Info_Map_Changes') )

      IF @Message IS NOT NULL 
            BEGIN
	
                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'

							-- Cycle through the targets in Change_Control Targt and sent the message to each    
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.USER_INFO_GROUP_INFO_MAP')     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0 
                        BEGIN    
						
                              EXEC Service_Broker_Conversation_Handle_Sel 
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                              SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Info_Group_Info_Map_Transfer] (@Message);

                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    

                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  

            END
            
END



GO




ALTER TABLE [dbo].[USER_INFO_GROUP_INFO_MAP] ADD 
CONSTRAINT [USER_INFO_GROUP_INFO_MAP_PK] PRIMARY KEY CLUSTERED  ([GROUP_INFO_ID], [USER_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
CREATE NONCLUSTERED INDEX [USER_GROUP_MAP_N_1] ON [dbo].[USER_INFO_GROUP_INFO_MAP] ([GROUP_INFO_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [USER_GROUP_MAP_N_2] ON [dbo].[USER_INFO_GROUP_INFO_MAP] ([USER_INFO_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

GO



ALTER TABLE [dbo].[USER_INFO_GROUP_INFO_MAP] ADD
CONSTRAINT [GROUP_INFO_USER_GROUP_MAP_FK] FOREIGN KEY ([GROUP_INFO_ID]) REFERENCES [dbo].[GROUP_INFO] ([GROUP_INFO_ID])
GO
GRANT SELECT ON  [dbo].[USER_INFO_GROUP_INFO_MAP] TO [BPOReportRole]
GO
