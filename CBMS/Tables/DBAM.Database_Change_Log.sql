CREATE TABLE [DBAM].[Database_Change_Log]
(
[Object_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Object_Revision] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Deployment_Ts] [datetime] NOT NULL CONSTRAINT [df_Database_Change_Log__Deployment_ts] DEFAULT (getdate()),
[Change_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Object_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Review_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Release_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [DBAM].[Database_Change_Log] ADD CONSTRAINT [pk_Database_Change_Log] PRIMARY KEY CLUSTERED  ([Object_Name], [Object_Revision], [Deployment_Ts]) ON [PRIMARY]
GO
