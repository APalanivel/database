CREATE TABLE [Workflow].[Workflow_Queue_Search_Filter]
(
[Workflow_Queue_Search_Filter_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Id] [int] NOT NULL,
[Search_Filter_Id] [int] NOT NULL,
[Filter_Type_Cd] [int] NOT NULL,
[App_Data_Load_Service_Id] [int] NULL,
[Is_Default] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter__Is_Default] DEFAULT ((0)),
[Data_Source_Query] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Key_Field_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Smart_Search_Filter] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter__Is_Smart_Search_Filter] DEFAULT ((0)),
[Display_Seq] [int] NOT NULL,
[CSS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter__Last_Change_Ts] DEFAULT (getdate()),
[CanDropdownSearchFilter] [bit] NOT NULL CONSTRAINT [DF__Workflow___CanDr__23EA16CF] DEFAULT ((1))
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter] ADD CONSTRAINT [pk_Workflow_Queue_Search_Filter] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter] ADD CONSTRAINT [un_Workflow_Queue_Search_Filter__Workflow_Queue_Id__Search_Filter_Id] UNIQUE NONCLUSTERED  ([Workflow_Queue_Id], [Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter] ADD CONSTRAINT [fk_App_Data_Load_Service__Workflow_Queue_Search_Filter] FOREIGN KEY ([App_Data_Load_Service_Id]) REFERENCES [Workflow].[App_Data_Load_Service] ([App_Data_Load_Service_Id])
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter] ADD CONSTRAINT [fk_Search_Filter__Workflow_Queue_Search_Filter] FOREIGN KEY ([Search_Filter_Id]) REFERENCES [Workflow].[Search_Filter] ([Search_Filter_Id])
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter] ADD CONSTRAINT [fk_Workflow_Queue__Workflow_Queue_Search_Filter] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
