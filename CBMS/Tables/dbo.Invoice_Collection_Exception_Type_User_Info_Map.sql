CREATE TABLE [dbo].[Invoice_Collection_Exception_Type_User_Info_Map]
(
[Invoice_Collection_Exception_Type_User_Info_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Exception_Type_Cd] [int] NOT NULL,
[User_Info_Id] [int] NOT NULL,
[Created_User_Id] [bit] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Exception_Type_User_Info_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Exception_Type_User_Info_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Exception_Type_User_Info_Map] ADD CONSTRAINT [PK_Invoice_Collection_Exception_Type_User_Info_Map] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Exception_Type_User_Info_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Exception_Type_User_Info_Map] ADD CONSTRAINT [unc_Invoice_Collection_Exception_Type_User_Info_Map] UNIQUE NONCLUSTERED  ([Invoice_Collection_Exception_Type_Cd]) ON [DB_DATA01]
GO
