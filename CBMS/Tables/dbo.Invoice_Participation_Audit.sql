CREATE TABLE [dbo].[Invoice_Participation_Audit]
(
[Audit_Function] [smallint] NULL,
[Audit_Ts] [datetime] NULL,
[Invoice_Participation_Id] [int] NULL,
[Site_Id] [int] NULL,
[Account_ID] [int] NULL,
[Service_Month] [date] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to Invoice_Participation', 'SCHEMA', N'dbo', 'TABLE', N'Invoice_Participation_Audit', NULL, NULL
GO

CREATE NONCLUSTERED INDEX [cix_Invoice_Participation_Audit_Audit_Ts_Account_ID_Site_Id_Service_Month] ON [dbo].[Invoice_Participation_Audit] ([Audit_Ts], [Account_ID], [Site_Id], [Service_Month]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [cix_Invoice_Participation_Audit_Invoice_Participation_id_Audit_Ts] ON [dbo].[Invoice_Participation_Audit] ([Audit_Ts], [Invoice_Participation_Id]) ON [DB_INDEXES01]

GO
