CREATE TABLE [dbo].[EL_MARKET_DATA]
(
[PRICE_POINT_ID] [int] NOT NULL IDENTITY(1, 1),
[PRICE_POINT] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOURCE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point
--when adding to this table
create trigger [dbo].[trg_ins_El_Market_Data]
on [dbo].[EL_MARKET_DATA]
for insert
as 
begin
set nocount on

	insert into market_price_point (market_price_index_id,market_price_point,interval_type,market_price_point_short_name)
	select market_price_index_id,price_point,'h',left(price_point,50)
	from market_price_index a 
	inner join inserted b on a.market_price_index_name=b.source

end


GO
ALTER TABLE [dbo].[EL_MARKET_DATA] ADD CONSTRAINT [PK_EL_MARKET_DATA] PRIMARY KEY CLUSTERED  ([PRICE_POINT_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Electric power price points/region sources', 'SCHEMA', N'dbo', 'TABLE', N'EL_MARKET_DATA', NULL, NULL
GO
