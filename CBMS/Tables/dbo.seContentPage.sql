CREATE TABLE [dbo].[seContentPage]
(
[ContentPageId] [int] NOT NULL IDENTITY(1, 1),
[PageLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seContentPage', NULL, NULL
GO

ALTER TABLE [dbo].[seContentPage] ADD 
CONSTRAINT [PK_seContentPage] PRIMARY KEY CLUSTERED  ([ContentPageId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
