CREATE TABLE [dbo].[SR_RFP_ARCHIVE_SUPPLIER]
(
[SR_RFP_ARCHIVE_SUPPLIER_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID] [int] NOT NULL,
[SUPPLIER_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTACT_NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_NO_BID] [bit] NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the supplier and supplier contact information at the time of closing of a RFP account, so that if later these information change, the information at the time of closure can be referenced from this table', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ARCHIVE_SUPPLIER', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_ARCHIVE_SUPPLIER] ADD 
CONSTRAINT [PK__SR_RFP_ARCHIVE_S__3F27380A] PRIMARY KEY CLUSTERED  ([SR_RFP_ARCHIVE_SUPPLIER_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_RFP_ARCHIVE_SUPPLIER] ADD
CONSTRAINT [FK_SR_RFP_ARCHIVE_SUPPLIER_SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP] FOREIGN KEY ([SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID]) REFERENCES [dbo].[SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP] ([SR_RFP_SUPPLIER_CONTACT_VENDOR_MAP_ID])

GO
