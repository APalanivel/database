CREATE TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO]
(
[SR_SUPPLIER_CONTACT_INFO_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[USER_INFO_ID] [int] NOT NULL,
[WORK_PHONE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CELL_PHONE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_ID] [int] NULL,
[ZIP] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDITIONAL_CONTACT_INFO] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_TYPE_ID] [int] NOT NULL,
[PRIVILEGE_TYPE_ID] [int] NULL,
[IS_RFP_PREFERENCE_WEBSITE] [bit] NULL,
[IS_RFP_PREFERENCE_EMAIL] [bit] NULL,
[IS_UPDATED] [bit] NULL,
[EMAIL_SENT_DATE] [datetime] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF_Supplier_Contact_Info_Last_Change_Ts] DEFAULT (getdate()),
[Termination_Contact_Email_Address] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Default_Termination_Contact_Email_For_Vendor] [bit] NOT NULL CONSTRAINT [df_Sr_Supplier_Contact_Info_Is_Default_Termination_Contact] DEFAULT ((0)),
[Registration_Contact_Email_Address] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Default_Registration_Contact_Email_For_Vendor] [bit] NOT NULL CONSTRAINT [df_Sr_Supplier_Contact_Info_Is_Default_Registration_Contact] DEFAULT ((0)),
[Comment_Id] [int] NULL
) ON [DBData_Sourcing] TEXTIMAGE_ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the vendor contact information both from CBMS as well as SW', 'SCHEMA', N'dbo', 'TABLE', N'SR_SUPPLIER_CONTACT_INFO', NULL, NULL
GO

ALTER TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO] ADD
CONSTRAINT [FK_SR_SUPPLIER_CONTACT_INFO_USER_INFO1] FOREIGN KEY ([USER_INFO_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Sr_Supplier_Contact_Info_Transfer

DESCRIPTION: Trigger to send changes for DV2 server through service_broker.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	HG			Harihara Suthan Ganesan

MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
	DSC			02/04/2014		Modified to handle USB-2 Issue.
								(Sr_Supplier_Contact_Info table is updated from sv)
	HG			2015-07-17		CBMS Sourcing Enhancement - New columns added to the xml message to copy the values in SV

******/
CREATE TRIGGER [dbo].[tr_Sr_Supplier_Contact_Info_Transfer] ON [dbo].[SR_SUPPLIER_CONTACT_INFO]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255);

      DECLARE @Last_Change_Ts TABLE
            ( SR_SUPPLIER_CONTACT_INFO_ID INT
            ,Last_Change_Ts DATETIME );

      IF NOT update(Last_Change_Ts)
            UPDATE
                  sr
            SET
                  last_Change_Ts = getdate()
            OUTPUT
                  inserted.SR_SUPPLIER_CONTACT_INFO_ID
                 ,inserted.Last_Change_Ts
                  INTO 
					@Last_Change_Ts
                        ( SR_SUPPLIER_CONTACT_INFO_ID, Last_Change_Ts )
            FROM
                  dbo.SR_SUPPLIER_CONTACT_INFO sr
                  INNER JOIN INSERTED i
                        ON sr.SR_SUPPLIER_CONTACT_INFO_ID = i.SR_SUPPLIER_CONTACT_INFO_ID;			

      IF SESSION_USER != N'sb_CBMS_Service'
            BEGIN

                  SET @Message = ( SELECT
                                    isnull(i.SR_SUPPLIER_CONTACT_INFO_ID, d.SR_SUPPLIER_CONTACT_INFO_ID) AS SR_SUPPLIER_CONTACT_INFO_ID
                                   ,i.USER_INFO_ID
                                   ,i.WORK_PHONE
                                   ,i.CELL_PHONE
                                   ,i.FAX
                                   ,i.ADDRESS_LINE1
                                   ,i.ADDRESS_LINE2
                                   ,i.CITY
                                   ,i.STATE_ID
                                   ,i.ZIP
                                   ,i.ADDITIONAL_CONTACT_INFO
                                   ,i.VENDOR_TYPE_ID
                                   ,i.PRIVILEGE_TYPE_ID
                                   ,i.IS_RFP_PREFERENCE_WEBSITE
                                   ,i.IS_RFP_PREFERENCE_EMAIL
                                   ,i.IS_UPDATED
                                   ,i.EMAIL_SENT_DATE
                                   ,CASE WHEN i.SR_SUPPLIER_CONTACT_INFO_ID IS NOT NULL
                                              AND d.SR_SUPPLIER_CONTACT_INFO_ID IS NULL THEN 'I'
                                         WHEN i.SR_SUPPLIER_CONTACT_INFO_ID IS NULL
                                              AND d.SR_SUPPLIER_CONTACT_INFO_ID IS NOT NULL THEN 'D'
                                         WHEN i.SR_SUPPLIER_CONTACT_INFO_ID IS NOT NULL
                                              AND d.SR_SUPPLIER_CONTACT_INFO_ID IS NOT NULL THEN 'U'
                                    END AS Op_Code
                                   ,isnull(l.Last_Change_Ts, i.Last_Change_Ts) AS Last_Change_Ts
                                   ,i.Termination_Contact_Email_Address
                                   ,i.Is_Default_Termination_Contact_Email_For_Vendor
                                   ,i.Registration_Contact_Email_Address
                                   ,i.Is_Default_Registration_Contact_Email_For_Vendor
                                   ,i.Comment_Id
                                   FROM
                                    INSERTED i
                                    FULL JOIN DELETED d
                                          ON i.SR_SUPPLIER_CONTACT_INFO_ID = d.SR_SUPPLIER_CONTACT_INFO_ID
                                    LEFT JOIN @Last_Change_Ts l
                                          ON l.SR_SUPPLIER_CONTACT_INFO_ID = i.SR_SUPPLIER_CONTACT_INFO_ID
                        FOR
                                   XML PATH('Sr_Supplier_Contact_Info_Change')
                                      ,ELEMENTS
                                      ,ROOT('Sr_Supplier_Contact_Info_Changes') );

                  IF @Message IS NOT NULL
                        BEGIN

                              SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer';
                              SET @Target_Service = N'//Change_Control/Service/SV/User_Info_Transfer';
                              SET @Target_Contract = N'//Change_Control/Contract/Sr_Supplier_Contact_Info_Transfer';

                              EXEC Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                                    SEND ON CONVERSATION @Conversation_Handle    
												MESSAGE TYPE [//Change_Control/Message/Sr_Supplier_Contact_Info_Transfer] (@Message);

						
                        END;
            END;

END;
;
GO




ALTER TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO] ADD 
CONSTRAINT [PK__SR_SUPPLIER_CONT__63CEACD4] PRIMARY KEY CLUSTERED  ([SR_SUPPLIER_CONTACT_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO


ALTER TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO] ADD
CONSTRAINT [FK_SR_SUPPLIER_CONTACT_INFO_ENTITY1] FOREIGN KEY ([PRIVILEGE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO] ADD
CONSTRAINT [FK_SR_SUPPLIER_CONTACT_INFO_STATE] FOREIGN KEY ([STATE_ID]) REFERENCES [dbo].[STATE] ([STATE_ID])

ALTER TABLE [dbo].[SR_SUPPLIER_CONTACT_INFO] ADD
CONSTRAINT [FK_SR_SUPPLIER_CONTACT_INFO_ENTITY] FOREIGN KEY ([VENDOR_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
