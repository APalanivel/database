CREATE TABLE [dbo].[Variance_Closed_Reason_Category_Map]
(
[Variance_Closed_Reason_id] [int] NOT NULL,
[Variance_Category_Cd] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Closed_Reason_Category_Map] ADD CONSTRAINT [FK_Variance_Closed_Reason_id] FOREIGN KEY ([Variance_Closed_Reason_id]) REFERENCES [dbo].[Variance_Closed_Reason] ([Variance_Closed_Reason_ID])
GO
