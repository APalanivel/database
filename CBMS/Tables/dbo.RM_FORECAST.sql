CREATE TABLE [dbo].[RM_FORECAST]
(
[RM_FORECAST_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[FORECAST_TYPE_ID] [int] NOT NULL,
[PRICE_INDEX_ID] [int] NULL,
[FORECAST_FROM_DATE] [datetime] NOT NULL,
[FORECAST_TO_DATE] [datetime] NULL,
[FORECAST_AS_OF_DATE] [datetime] NULL
) ON [DBData_Risk_MGMT]
ALTER TABLE [dbo].[RM_FORECAST] ADD
CONSTRAINT [PRICE_INDEX_FORECAST_FK_1] FOREIGN KEY ([PRICE_INDEX_ID]) REFERENCES [dbo].[PRICE_INDEX] ([PRICE_INDEX_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table to store NYMEX/Basis forecast details. This table has a child, RM_FORECAST_DETAILS table where details of forecast for all the 4 modes are stored.', 'SCHEMA', N'dbo', 'TABLE', N'RM_FORECAST', NULL, NULL
GO

ALTER TABLE [dbo].[RM_FORECAST] ADD 
CONSTRAINT [RM_FORECAST_PK] PRIMARY KEY CLUSTERED  ([RM_FORECAST_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
CREATE NONCLUSTERED INDEX [RM_FORECAST_N_2] ON [dbo].[RM_FORECAST] ([FORECAST_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [RM_FORECAST_N_1] ON [dbo].[RM_FORECAST] ([PRICE_INDEX_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]


ALTER TABLE [dbo].[RM_FORECAST] ADD
CONSTRAINT [ENTITY_FORECAST_FK_1] FOREIGN KEY ([FORECAST_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])









GO
