CREATE TABLE [dbo].[ACCOUNT_GROUP]
(
[ACCOUNT_GROUP_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[GROUP_BILLING_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_ID] [int] NULL
) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[ACCOUNT_GROUP] ADD CONSTRAINT [ACCOUNT_GROUP_PK] PRIMARY KEY CLUSTERED  ([ACCOUNT_GROUP_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Group__Vendor_ID] ON [dbo].[ACCOUNT_GROUP] ([VENDOR_ID], [ACCOUNT_GROUP_ID]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', 'FK in Account table. Used by invoice processing to group bill/accounts together. 75% of the groupings as of 8/3/211 are tying supplier accounts to utility accounts.', 'SCHEMA', N'dbo', 'TABLE', N'ACCOUNT_GROUP', NULL, NULL
GO
