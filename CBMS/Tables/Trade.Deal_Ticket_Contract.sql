CREATE TABLE [Trade].[Deal_Ticket_Contract]
(
[Deal_Ticket_Id] [int] NOT NULL,
[CONTRACT_ID] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Contract__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Contract] ADD CONSTRAINT [pk_Deal_Ticket_Contract] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Id], [CONTRACT_ID]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Contract__CONTRACT_ID] ON [Trade].[Deal_Ticket_Contract] ([CONTRACT_ID]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Contract] ADD CONSTRAINT [fk_CONTRACT__Deal_Ticket_Contract] FOREIGN KEY ([CONTRACT_ID]) REFERENCES [dbo].[CONTRACT] ([CONTRACT_ID])
GO
ALTER TABLE [Trade].[Deal_Ticket_Contract] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Contract] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
