CREATE TABLE [dbo].[EC_Invoice_Sub_Bucket_Master]
(
[EC_Invoice_Sub_Bucket_Master_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[State_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Invoice_Sub_Bucket_Master__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
ALTER TABLE [dbo].[EC_Invoice_Sub_Bucket_Master] ADD 
CONSTRAINT [pk_EC_Invoice_Sub_Bucket_Master] PRIMARY KEY CLUSTERED  ([EC_Invoice_Sub_Bucket_Master_Id]) ON [DB_DATA01]
GO

ALTER TABLE [dbo].[EC_Invoice_Sub_Bucket_Master] ADD CONSTRAINT [unc_EC_Invoice_Sub_Bucket_Master__State_Id__Bucket_Master_Id__Sub_Bucket_Name] UNIQUE NONCLUSTERED  ([State_Id], [Bucket_Master_Id], [Sub_Bucket_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Invoice_Sub_Bucket_Master] ADD CONSTRAINT [fk_Bucket_Master__EC_Invoice_Sub_Bucket_Master] FOREIGN KEY ([Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
