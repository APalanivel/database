CREATE TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL]
(
[BUDGET_CONTRACT_BUDGET_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1),
[BUDGET_CONTRACT_BUDGET_MONTH_ID] [int] NOT NULL,
[VOLUME] [decimal] (32, 16) NULL,
[MARKET_ID] [int] NULL,
[FUEL] [decimal] (32, 16) NULL,
[MULTIPLIER] [decimal] (32, 16) NULL,
[ADDER] [decimal] (32, 16) NULL,
[VOLUME_UNIT_TYPE_ID] [int] NULL,
[TAX] [decimal] (32, 16) NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[IS_NYMEX_FORECAST] [bit] NULL
) ON [DBData_Budget]
ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ADD
CONSTRAINT [FK_BUDGET_BUDGET_CONTRACT_BUDGET_MONTH_ID] FOREIGN KEY ([BUDGET_CONTRACT_BUDGET_MONTH_ID]) REFERENCES [dbo].[BUDGET_CONTRACT_BUDGET_MONTHS] ([BUDGET_CONTRACT_BUDGET_MONTH_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Volume and multipliers used by month for that contract for budget reasons', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_CONTRACT_BUDGET_DETAIL', NULL, NULL
GO

ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ADD 
CONSTRAINT [XPKBUDGET_CONTRACT_BUDGET_DETAIL] PRIMARY KEY CLUSTERED  ([BUDGET_CONTRACT_BUDGET_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
CREATE NONCLUSTERED INDEX [IX_MONTH] ON [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ([BUDGET_CONTRACT_BUDGET_MONTH_ID]) ON [DB_INDEXES01]


ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ADD
CONSTRAINT [FK_BUDGET_MARKET_ID] FOREIGN KEY ([MARKET_ID]) REFERENCES [dbo].[CLEARPORT_INDEX] ([CLEARPORT_INDEX_ID])
ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ADD
CONSTRAINT [FK_BUDGET_VOLUME_UNIT_TYPE_ID] FOREIGN KEY ([VOLUME_UNIT_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET_DETAIL] ADD
CONSTRAINT [FK_BUDGET_CURRENCY_UNIT_ID] FOREIGN KEY ([CURRENCY_UNIT_ID]) REFERENCES [dbo].[CURRENCY_UNIT] ([CURRENCY_UNIT_ID])


GO
