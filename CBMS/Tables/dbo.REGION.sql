CREATE TABLE [dbo].[REGION]
(
[REGION_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[REGION_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[REGION_DESCRIPTION] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Row_Version] [timestamp] NOT NULL
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Region_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Region table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		
		SELECT CLIENT_HIER_ID,REGION_ID,Region_Name FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
				
		UPDATE Region
		SET Region_NAME = 'TEST'
		WHERE Region_ID = 1
		
		SELECT CLIENT_HIER_ID,REGION_ID,Region_Name FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
		
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/31/2010  Added the Last_Change_TS column for update
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.

******/

CREATE TRIGGER [dbo].[tr_Region_upd_Client_Hier] ON [dbo].[REGION]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            ch
      SET   
            REGION_NAME = i.REGION_NAME
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier ch
            INNER JOIN INSERTED i
                  ON i.REGION_ID = ch.REGION_ID
            INNER JOIN DELETED d
                  ON i.REGION_ID = d.REGION_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT i.REGION_NAME
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT d.REGION_NAME
                                                        FOR
                                                                  XML RAW ))            

END 
;
GO
ALTER TABLE [dbo].[REGION] ADD CONSTRAINT [REGION_PK] PRIMARY KEY CLUSTERED  ([REGION_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE UNIQUE NONCLUSTERED INDEX [REGION_U_1] ON [dbo].[REGION] ([REGION_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Geographic regions defined for work distribution', 'SCHEMA', N'dbo', 'TABLE', N'REGION', NULL, NULL
GO
