CREATE TABLE [dbo].[Invoice_Collection_Account_Config_Group_Audit]
(
[IC_Account_Config_Group_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Invoice_Collection_Account_Config_Group_ID] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_ts] [datetime] NULL CONSTRAINT [DF_Invoice_Collection_Account_Config_Group_Audit] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config_Group_Audit] ADD CONSTRAINT [PK_Invoice_Collection_Account_Config_Group_Audit] PRIMARY KEY CLUSTERED  ([IC_Account_Config_Group_Audit_Id]) ON [DB_DATA01]
GO
