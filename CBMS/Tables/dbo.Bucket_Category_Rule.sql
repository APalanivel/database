CREATE TABLE [dbo].[Bucket_Category_Rule]
(
[Category_Bucket_Master_Id] [int] NOT NULL,
[Child_Bucket_Master_Id] [int] NOT NULL,
[Priority_Order] [smallint] NOT NULL,
[Aggregation_Type_CD] [int] NOT NULL,
[CU_Aggregation_Level_Cd] [int] NOT NULL,
[Is_Aggregate_Category_Bucket] [bit] NULL CONSTRAINT [DF__Bucket_Category_Rule__Is_Aggregate_Category_Bucket] DEFAULT ((0))
) ON [DBData_Cost_Usage]
CREATE NONCLUSTERED INDEX [IX_Bucket_Category_Rule__Category_Bucket_Master_Id__Priority_Order__CU_Aggregation_Level_Cd] ON [dbo].[Bucket_Category_Rule] ([Category_Bucket_Master_Id], [Priority_Order], [CU_Aggregation_Level_Cd]) ON [DB_INDEXES01]

CREATE CLUSTERED INDEX [cix_Bucket_Category_Rule_Priority_Order] ON [dbo].[Bucket_Category_Rule] ([CU_Aggregation_Level_Cd], [Category_Bucket_Master_Id], [Child_Bucket_Master_Id], [Aggregation_Type_CD], [Priority_Order], [Is_Aggregate_Category_Bucket]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

ALTER TABLE [dbo].[Bucket_Category_Rule] ADD CONSTRAINT [PK_Bucket_Category_Rule] PRIMARY KEY NONCLUSTERED  ([CU_Aggregation_Level_Cd], [Category_Bucket_Master_Id], [Child_Bucket_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Part of the Bucket rules engine.  Junction table for rule recursion also contains type of aggregation and priority order.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Category_Rule', NULL, NULL
GO






ALTER TABLE [dbo].[Bucket_Category_Rule] ADD
CONSTRAINT [FK_Bucket_Master_Bucket_Category_Rule_Category_Bucket_Master_Id] FOREIGN KEY ([Category_Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
ALTER TABLE [dbo].[Bucket_Category_Rule] ADD
CONSTRAINT [FK_Bucket_Master_Bucket_Category_Rule_Child_Bucket_Master_Id] FOREIGN KEY ([Child_Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
