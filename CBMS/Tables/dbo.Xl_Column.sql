CREATE TABLE [dbo].[Xl_Column]
(
[Xl_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[Column_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Type_Cd] [int] NOT NULL,
[Data_Length] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Width] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Lookup_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Xl_Column__Is_Active] DEFAULT ((1))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Column] ADD CONSTRAINT [pk_Xl_Column] PRIMARY KEY CLUSTERED  ([Xl_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Column] ADD CONSTRAINT [un_Xl_Column__Column_Name] UNIQUE NONCLUSTERED  ([Column_Name]) ON [DB_DATA01]
GO
