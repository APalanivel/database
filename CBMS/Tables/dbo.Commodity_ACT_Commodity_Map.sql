CREATE TABLE [dbo].[Commodity_ACT_Commodity_Map]
(
[Commodity_ACT_Commodity_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Commodity_Id] [int] NOT NULL,
[ACT_Commodity_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Commodity_ACT_Commodity_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Commodity_ACT_Commodity_Map] ADD CONSTRAINT [pk_Commodity_ACT_Commodity_Map] PRIMARY KEY CLUSTERED  ([Commodity_ACT_Commodity_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Commodity_ACT_Commodity_Map] ADD CONSTRAINT [un_Commodity_ACT_Commodity_Map] UNIQUE NONCLUSTERED  ([Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Commodity_ACT_Commodity_Map] ADD CONSTRAINT [fk_Commodity_ACT_Commodity_Map__ACT_Commodity] FOREIGN KEY ([ACT_Commodity_Id]) REFERENCES [dbo].[ACT_Commodity] ([ACT_Commodity_Id])
GO
ALTER TABLE [dbo].[Commodity_ACT_Commodity_Map] ADD CONSTRAINT [fk_Commodity_ACT_Commodity_Map__Commodity] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
