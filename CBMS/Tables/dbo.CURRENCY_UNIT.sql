CREATE TABLE [dbo].[CURRENCY_UNIT]
(
[CURRENCY_UNIT_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CURRENCY_UNIT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SYMBOL] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SORT_ORDER] [int] NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'general list of currencies', 'SCHEMA', N'dbo', 'TABLE', N'CURRENCY_UNIT', NULL, NULL
GO

ALTER TABLE [dbo].[CURRENCY_UNIT] ADD 
CONSTRAINT [CURRENCY_UNIT_PK] PRIMARY KEY CLUSTERED  ([CURRENCY_UNIT_ID]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

GO
