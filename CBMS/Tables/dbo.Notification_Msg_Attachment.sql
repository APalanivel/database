CREATE TABLE [dbo].[Notification_Msg_Attachment]
(
[Notification_Msg_Attachment_Id] [int] NOT NULL IDENTITY(1, 1),
[Notification_Msg_Queue_Id] [int] NOT NULL,
[Cbms_Image_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Notification_Msg_Attachment] ADD CONSTRAINT [pk_Notification_Msg_Attachment] PRIMARY KEY CLUSTERED  ([Notification_Msg_Attachment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Notification_Msg_Attachment] ADD CONSTRAINT [un_Notification_Msg_Attachment__Notification_Msg_Queue_Id__Cbms_Image_Id] UNIQUE NONCLUSTERED  ([Notification_Msg_Queue_Id], [Cbms_Image_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Notification_Msg_Attachment] ADD CONSTRAINT [fk_Notification_Msg_Queue__Notification_Msg_Attachment] FOREIGN KEY ([Notification_Msg_Queue_Id]) REFERENCES [dbo].[Notification_Msg_Queue] ([Notification_Msg_Queue_Id])
GO
