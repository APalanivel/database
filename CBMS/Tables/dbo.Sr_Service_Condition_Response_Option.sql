CREATE TABLE [dbo].[Sr_Service_Condition_Response_Option]
(
[Sr_Service_Condition_Response_Option_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Service_Condition_Question_Id] [int] NOT NULL,
[Option_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display_Seq] [int] NOT NULL,
[Is_Comment_Required] [bit] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Response_Option__Is_Comment_Required] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Response_Option__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Response_Option__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Response_Option] ADD CONSTRAINT [pk_Sr_Service_Condition_Response_Option] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Response_Option_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Response_Option] ADD CONSTRAINT [un_Sr_Service_Condition_Response_Option__Sr_Service_Condition_Question_Id__Option_Value] UNIQUE NONCLUSTERED  ([Sr_Service_Condition_Question_Id], [Option_Value]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Response_Option] ADD CONSTRAINT [fk_Sr_Service_Condition_Question__Sr_Service_Condition_Response_Option] FOREIGN KEY ([Sr_Service_Condition_Question_Id]) REFERENCES [dbo].[Sr_Service_Condition_Question] ([Sr_Service_Condition_Question_Id])
GO
