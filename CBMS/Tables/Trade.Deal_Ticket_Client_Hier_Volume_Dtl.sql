CREATE TABLE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl]
(
[Deal_Ticket_Client_Hier_Volume_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL CONSTRAINT [df_Trade_Deal_Ticket_Client_Hier_Volume_Dtl_Account_Id] DEFAULT ((-1)),
[Deal_Month] [date] NOT NULL,
[Total_Volume] [decimal] (28, 6) NOT NULL,
[Uom_Type_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_Volume_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_Volume_Dtl__Last_Change_Ts] DEFAULT (getdate()),
[Contract_Id] [int] NULL CONSTRAINT [df_Trade_Deal_Ticket_Client_Hier_Volume_Dtl_Contract_Id] DEFAULT ((-1)),
[Trade_Number] [int] NULL,
[Trade_Executed_Ts] [datetime] NULL,
[Trade_Price_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl] ADD CONSTRAINT [pk_Deal_Ticket_Client_Hier_Volume_Dtl] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Hier_Volume_Dtl_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Client_Hier_Volume_Dtl__Client_Hier_Id__Deal_Month] ON [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl] ([Client_Hier_Id], [Deal_Month]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl] ADD CONSTRAINT [un_Deal_Ticket_Client_Hier_Volume_Dtl__Deal_Ticket_Id__Client_Hier_Id__Deal_Month__Account_Id__Contract_Id] UNIQUE NONCLUSTERED  ([Deal_Ticket_Id], [Client_Hier_Id], [Deal_Month], [Account_Id], [Contract_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Volume_Dtl] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Client_Hier_Volume_Dtl] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
