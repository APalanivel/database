CREATE TABLE [dbo].[Cu_Invoice_Recalc_Exception_Queue]
(
[Cu_Invoice_Recalc_Exception_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Queue_Date] [datetime] NOT NULL,
[User_Info_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Exception_Queue] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Exception_Queue] ADD CONSTRAINT [pk_Cu_Invoice_Recalc_Exception_Queue] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Recalc_Exception_Queue_Id]) ON [DB_DATA01]
GO
