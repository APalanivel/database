CREATE TABLE [dbo].[Service_Broker_Conversation_Handler]
(
[Target_Service] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Target_Contract] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Conversation_Handle] [uniqueidentifier] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Service_Broker_Conversation_Handler__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Service broker conversion handle , used to process the messages in order.', 'SCHEMA', N'dbo', 'TABLE', N'Service_Broker_Conversation_Handler', NULL, NULL
GO

ALTER TABLE [dbo].[Service_Broker_Conversation_Handler] ADD CONSTRAINT [pk_Service_Broker_Conversation_Handler] PRIMARY KEY CLUSTERED  ([Target_Service], [Target_Contract]) ON [DB_DATA01]
GO
