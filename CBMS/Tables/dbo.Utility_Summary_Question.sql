CREATE TABLE [dbo].[Utility_Summary_Question]
(
[Utility_Summary_Question_Id] [int] NOT NULL IDENTITY(1, 1),
[Utility_Summary_Section_Id] [int] NOT NULL,
[Question_Label] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display_Order] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Utility_Summary__Question_Is_Active] DEFAULT ((1))
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Questions for each Utility_Summary_Section. Answers to the questions are stored in the individual Utility_Dtl_* tables', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Summary_Question', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Summary_Question] ADD CONSTRAINT [unc_Utility_Summary_Question__Utility_Summary_Section_Id__Question_Label] UNIQUE CLUSTERED  ([Utility_Summary_Section_Id], [Question_Label]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Summary_Question] ADD CONSTRAINT [pk_Utility_Summary_Question] PRIMARY KEY NONCLUSTERED  ([Utility_Summary_Question_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

GO
