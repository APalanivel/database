CREATE TABLE [dbo].[GFE_Change]
(
[TableName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_GFE_Change_ColumnName] DEFAULT (''),
[CaptureDate] [datetime] NOT NULL CONSTRAINT [DF_GFE_Change_CaptureDate] DEFAULT (getdate()),
[OldValue] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_GFE_Change_OldValue] DEFAULT (''),
[NewValue] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_GFE_Change_NewValue] DEFAULT (''),
[EventType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_GFE_Change_EventType] DEFAULT ('INS'),
[PrimaryKey] [varchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_GFE_Change_PrimaryKey] DEFAULT ('NA')
) ON [DBData_Deprecated]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'GFE_Change', NULL, NULL
GO

ALTER TABLE [dbo].[GFE_Change] ADD 
CONSTRAINT [PK_GFE_Change] PRIMARY KEY CLUSTERED  ([TableName], [ColumnName], [CaptureDate]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]
GO
