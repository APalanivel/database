CREATE TABLE [dbo].[ICQ_Log]
(
[ICQ_Log] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Invoice_Collection_Queue_Id] [int] NULL,
[ICQ_Log_Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[User_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__ICQ_Log__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
