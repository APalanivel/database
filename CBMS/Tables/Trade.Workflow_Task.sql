CREATE TABLE [Trade].[Workflow_Task]
(
[Workflow_Task_Id] [int] NOT NULL IDENTITY(1, 1),
[Task_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Task_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Task__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Task__Last_Change_Ts] DEFAULT (getdate()),
[Display_order] [int] NULL,
[Is_CBMS_Task] [bit] NULL,
[Display_In_CBMS_Application] [bit] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Task] ADD CONSTRAINT [pk_Workflow_Task] PRIMARY KEY CLUSTERED  ([Workflow_Task_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Task] ADD CONSTRAINT [un_Workflow_Task__Task_Name] UNIQUE NONCLUSTERED  ([Task_Name]) ON [DB_DATA01]
GO
