CREATE TABLE [dbo].[Invoice_Collection_Activity]
(
[Invoice_Collection_Activity_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Activity_Type_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Activity__Created_Ts] DEFAULT (getdate()),
[Is_Automatic_Link] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Activity_Is_Automatic_Link] DEFAULT ((0)),
[Issue_Link_Start_Date] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Activity] ADD CONSTRAINT [pk_Invoice_Collection_Activity] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Activity_Id]) ON [DB_DATA01]
GO
