CREATE TABLE [dbo].[Variance_Account_Month_Anomalous_Status]
(
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Is_Anomalous] [bit] NOT NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [DF_Create_Ts_Variance_Account_Month_Anomalous_Status] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Updated_Ts] [datetime] NULL CONSTRAINT [DF_Update_Ts_Variance_Account_Month_Anomalous_Status] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Account_Month_Anomalous_Status] ADD CONSTRAINT [PK__Variance__6F3DDE5C31FCCD15] PRIMARY KEY CLUSTERED  ([Account_Id], [Service_Month], [Is_Anomalous]) ON [DB_DATA01]
GO
