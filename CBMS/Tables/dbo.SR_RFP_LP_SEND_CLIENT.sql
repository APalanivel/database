CREATE TABLE [dbo].[SR_RFP_LP_SEND_CLIENT]
(
[SR_RFP_LP_SEND_CLIENT_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_ACCOUNT_GROUP_ID] [int] NOT NULL,
[IS_BID_GROUP] [bit] NULL,
[SR_RFP_EMAIL_LOG_ID] [int] NOT NULL,
[SR_RFP_SITE_LP_XML_ID] [int] NOT NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A table to store the Send Client information', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_LP_SEND_CLIENT', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_LP_SEND_CLIENT] ADD 
CONSTRAINT [PK__SR_RFP_LP_SEND_C__4DDF6BB5] PRIMARY KEY CLUSTERED  ([SR_RFP_LP_SEND_CLIENT_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_RFP_LP_SEND_CLIENT] ADD
CONSTRAINT [FK_SR_RFP_LP_SEND_CLIENT_SR_RFP_SITE_LP_XML] FOREIGN KEY ([SR_RFP_SITE_LP_XML_ID]) REFERENCES [dbo].[SR_RFP_SITE_LP_XML] ([SR_RFP_SITE_LP_XML_ID])
ALTER TABLE [dbo].[SR_RFP_LP_SEND_CLIENT] ADD
CONSTRAINT [FK_SR_RFP_LP_SEND_CLIENT_SR_RFP_EMAIL_LOG] FOREIGN KEY ([SR_RFP_EMAIL_LOG_ID]) REFERENCES [dbo].[SR_RFP_EMAIL_LOG] ([SR_RFP_EMAIL_LOG_ID])

GO
