CREATE TABLE [dbo].[Contract_Meter_Volume_Dtl]
(
[Contract_Meter_Volume_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[CONTRACT_METER_VOLUME_ID] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Volume] [decimal] (32, 16) NULL,
[Is_Edited] [bit] NOT NULL CONSTRAINT [df_Contract_Meter_Volume_Dtl_Is_Edited] DEFAULT ((0)),
[Created_User_Id] [bit] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Meter_Volume_Dtl_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [bit] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Meter_Volume_Dtl_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Contract_Meter_Volume_Dtl_Info_Transfer

DESCRIPTION: Trigger on Contract_Meter_Volume_Dtl to send change data to the 
	Change control service. 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RR			Raghu Reddy

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	RR			2020-04-27		SE2017-688 - Created
******/
CREATE TRIGGER [dbo].[tr_Contract_Meter_Volume_Dtl_Info_Transfer]
ON [dbo].[Contract_Meter_Volume_Dtl]
FOR INSERT, UPDATE, DELETE
AS
    BEGIN

        DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
            , @Message_Contract_Meter_Volume_Dtl XML
            , @From_Service NVARCHAR(255)
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255);


        DECLARE @Contract_Type_Id_List VARCHAR(MAX);

        SELECT
            @Contract_Type_Id_List = ac.App_Config_Value
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Exclude_Contract_Type_Id_List'
            AND ac.Is_Active = 1;

        SET @Message_Contract_Meter_Volume_Dtl = (   SELECT
                                                            CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NOT NULL
                                                                      AND  del.CONTRACT_METER_VOLUME_ID IS NULL THEN 'I'
                                                                WHEN Ins.CONTRACT_METER_VOLUME_ID IS NOT NULL
                                                                     AND   del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                    'U'
                                                                WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                     AND   del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                    'D'
                                                            END AS Sys_Change_Operation
                                                            , ISNULL(
                                                                  Ins.CONTRACT_METER_VOLUME_ID
                                                                  , del.CONTRACT_METER_VOLUME_ID) AS Contract_Meter_Volume_Id
                                                            , cmv.CONTRACT_ID AS Contract_Id
                                                            , cmv.METER_ID AS Meter_Id
                                                            , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                        AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                       NULL
                                                                  ELSE Ins.Volume
                                                              END Volume
                                                            , ISNULL(Ins.Bucket_Master_Id, del.Bucket_Master_Id) AS timeOfDay_Id
                                                            , bm.Bucket_Name AS timeOfDay
                                                            , cmv.MONTH_IDENTIFIER AS [Month]
                                                            , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                        AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                       NULL
                                                                  ELSE e.ENTITY_NAME
                                                              END UOM
                                                            , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                        AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                       NULL
                                                                  ELSE cmv.UNIT_TYPE_ID
                                                              END Uom_Id
                                                            , @@SERVERNAME AS Server_Name
                                                            , e1.ENTITY_NAME AS Meter_Volume_Frequency_Type
                                                            , cmv.FREQUENCY_TYPE_ID AS Meter_Volume_Frequency_Type_Id
                                                     FROM
                                                            INSERTED Ins
                                                            FULL OUTER JOIN DELETED del
                                                                ON del.CONTRACT_METER_VOLUME_ID = Ins.CONTRACT_METER_VOLUME_ID
                                                            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                                                                ON cmv.CONTRACT_METER_VOLUME_ID = ISNULL(
                                                                                                      Ins.CONTRACT_METER_VOLUME_ID
                                                                                                      , del.CONTRACT_METER_VOLUME_ID)
                                                            INNER JOIN dbo.ENTITY e
                                                                ON e.ENTITY_ID = cmv.UNIT_TYPE_ID
                                                            INNER JOIN dbo.CONTRACT c
                                                                ON c.CONTRACT_ID = cmv.CONTRACT_ID
                                                            INNER JOIN dbo.ENTITY e1
                                                                ON e1.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
                                                            INNER JOIN dbo.Bucket_Master bm
                                                                ON bm.Bucket_Master_Id = ISNULL(
                                                                                             Ins.Bucket_Master_Id
                                                                                             , del.Bucket_Master_Id)
                                                     WHERE
                                                         c.Is_Published_To_EC = 1
                                                         AND NOT EXISTS (   SELECT
                                                                                1
                                                                            FROM
                                                                                dbo.ufn_split(
                                                                                    @Contract_Type_Id_List, ',') us
                                                                            WHERE
                                                                                c.CONTRACT_TYPE_ID = us.Segments)
                                                     GROUP BY
                                                         CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NOT NULL
                                                                   AND  del.CONTRACT_METER_VOLUME_ID IS NULL THEN 'I'
                                                             WHEN Ins.CONTRACT_METER_VOLUME_ID IS NOT NULL
                                                                  AND   del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                 'U'
                                                             WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                  AND   del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                 'D'
                                                         END
                                                         , ISNULL(
                                                               Ins.CONTRACT_METER_VOLUME_ID
                                                               , del.CONTRACT_METER_VOLUME_ID)
                                                         , cmv.CONTRACT_ID
                                                         , cmv.METER_ID
                                                         , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                     AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                    NULL
                                                               ELSE Ins.Volume
                                                           END
                                                         , cmv.MONTH_IDENTIFIER
                                                         , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                     AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                    NULL
                                                               ELSE e.ENTITY_NAME
                                                           END
                                                         , CASE WHEN Ins.CONTRACT_METER_VOLUME_ID IS NULL
                                                                     AND del.CONTRACT_METER_VOLUME_ID IS NOT NULL THEN
                                                                    NULL
                                                               ELSE cmv.UNIT_TYPE_ID
                                                           END
                                                         , e1.ENTITY_NAME
                                                         , cmv.FREQUENCY_TYPE_ID
                                                         , ISNULL(Ins.Bucket_Master_Id, del.Bucket_Master_Id)
                                                         , bm.Bucket_Name
                                                     FOR XML PATH('Contract_Meter_Volume_Change'), ELEMENTS, ROOT('Contract_Meter_Volume_Changes'));

        IF @Message_Contract_Meter_Volume_Dtl IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                -- Cycle through the targets in Change_Control Targt and sent the message to each
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Contract_Meter_Volume');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Volume_Change]
                            (@Message_Contract_Meter_Volume_Dtl);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;



    END;
GO
ALTER TABLE [dbo].[Contract_Meter_Volume_Dtl] ADD CONSTRAINT [PK_Contract_Meter_Volume_Dtl__Contract_Meter_Volume_Dtl_Id] PRIMARY KEY CLUSTERED  ([Contract_Meter_Volume_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Meter_Volume_Dtl] ADD CONSTRAINT [FK_Contract_Meter_Volume_Dtl__Bucket_Master_Id] FOREIGN KEY ([Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
ALTER TABLE [dbo].[Contract_Meter_Volume_Dtl] ADD CONSTRAINT [FK_Contract_Meter_Volume_Dtl__CONTRACT_METER_VOLUME_ID] FOREIGN KEY ([CONTRACT_METER_VOLUME_ID]) REFERENCES [dbo].[CONTRACT_METER_VOLUME] ([CONTRACT_METER_VOLUME_ID])
GO
