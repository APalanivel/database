CREATE TABLE [dbo].[Market_Price_Point]
(
[Market_Price_Point_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Market_Price_Index_ID] [int] NOT NULL,
[Market_Price_Point] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Interval_Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Market_Price_Point_Short_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Market_Pr__Marke__0276C075] DEFAULT (''),
[Is_Show_On_DV] [bit] NOT NULL CONSTRAINT [show_on_dv] DEFAULT ((1)),
[rowguid] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__Market_Pr__rowgu__02179919] DEFAULT (newid())
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Named listing of Market Price Points to display on RA.  Populated via triggers of ClearPort_Index and EL_Market_Data tables.', 'SCHEMA', N'dbo', 'TABLE', N'Market_Price_Point', NULL, NULL
GO

ALTER TABLE [dbo].[Market_Price_Point] ADD 
CONSTRAINT [PK_MARKET_PRICE_POINT] PRIMARY KEY CLUSTERED  ([Market_Price_Point_ID], [Market_Price_Index_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
ALTER TABLE [dbo].[Market_Price_Point] ADD CONSTRAINT [no_dupe_price_point] UNIQUE NONCLUSTERED  ([Market_Price_Point]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]

CREATE UNIQUE NONCLUSTERED INDEX [index_277861794] ON [dbo].[Market_Price_Point] ([rowguid]) ON [DB_INDEXES01]



GO
