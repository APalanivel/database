CREATE TABLE [dbo].[Variance_Category_Required_Map]
(
[Variance_Category_Required_Map_id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Category_cd] [int] NULL,
[Required_Variance_Category_Cd] [int] NULL,
[Commodity_id] [int] NULL,
[Category_Required] [bit] NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [DF__Variance___Creat__6942AE94] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DF__Variance___Last___6A36D2CD] DEFAULT (getdate()),
[Bucket_Master_Id] [int] NOT NULL CONSTRAINT [DF__Variance___Bucke__4EE3BE13] DEFAULT ((-1))
) ON [DB_DATA01]
GO
