CREATE TABLE [dbo].[Variance_Extraction_Service_Param]
(
[Variance_Extraction_Service_Param_Id] [int] NOT NULL IDENTITY(1, 1),
[Param_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Type_Cd] [int] NOT NULL,
[Extract_Service_Param_Level_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Extraction_Service_Param__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service_Param] ADD CONSTRAINT [pk_Variance_Extraction_Service_Param] PRIMARY KEY CLUSTERED  ([Variance_Extraction_Service_Param_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service_Param] ADD CONSTRAINT [un_Variance_Extraction_Service_Param__Param_Name] UNIQUE NONCLUSTERED  ([Param_Name]) ON [DB_DATA01]
GO
