CREATE TABLE [Trade].[RM_Client_Hier_Hedge_Config]
(
[RM_Client_Hier_Hedge_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Client_Hier_Onboard_Id] [int] NOT NULL,
[Config_Start_Dt] [date] NOT NULL,
[Config_End_Dt] [date] NOT NULL,
[RM_Forecast_UOM_Type_Id] [int] NULL,
[Hedge_Type_Id] [int] NULL,
[Max_Hedge_Pct] [decimal] (5, 2) NULL,
[RM_Risk_Tolerance_Category_Id] [int] NULL,
[Risk_Manager_User_Info_Id] [int] NULL,
[Contact_Info_Id] [int] NULL,
[Include_In_Reports] [bit] NULL,
[Is_Default_Config] [bit] NULL CONSTRAINT [DFRM_Client_Hier_Hedge_ConfigIs_Default_Config] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_Hedge_ConfigCreated_Ts] DEFAULT (getdate()),
[Last_Updated_By] [int] NOT NULL,
[Last_Updated_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_Hedge_ConfigLast_Updated_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Hedge_Config] ADD CONSTRAINT [pk_RM_Client_Hier_Hedge_Config] PRIMARY KEY CLUSTERED  ([RM_Client_Hier_Hedge_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Hedge_Config] ADD CONSTRAINT [un_RM_Client_Hier_Hedge_Config__RM_Client_Hier_Onboard_Id__Config_Start_Dt__Config_End_Dt] UNIQUE NONCLUSTERED  ([RM_Client_Hier_Onboard_Id], [Config_Start_Dt], [Config_End_Dt]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_RM_Client_Hier_Hedge_Config__RM_Risk_Tolerance_Category_Id] ON [Trade].[RM_Client_Hier_Hedge_Config] ([RM_Risk_Tolerance_Category_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Hedge_Config] ADD CONSTRAINT [fk_RM_Client_Hier_Onboard__RM_Client_Hier_Hedge_Config] FOREIGN KEY ([RM_Client_Hier_Onboard_Id]) REFERENCES [Trade].[RM_Client_Hier_Onboard] ([RM_Client_Hier_Onboard_Id])
GO
ALTER TABLE [Trade].[RM_Client_Hier_Hedge_Config] ADD CONSTRAINT [fk_RM_Risk_Tolerance_Category__RM_Client_Hier_Hedge_Config] FOREIGN KEY ([RM_Risk_Tolerance_Category_Id]) REFERENCES [Trade].[RM_Risk_Tolerance_Category] ([RM_Risk_Tolerance_Category_Id])
GO
