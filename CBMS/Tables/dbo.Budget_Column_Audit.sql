CREATE TABLE [dbo].[Budget_Column_Audit]
(
[Budget_Column_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Budget_Id] [int] NULL,
[Audit_Column_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Previous_Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Value] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Audit_By_User_Id] [int] NULL,
[Audit_Ts] [datetime] NULL
) ON [DBData_Budget]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Audit tracking of each change to the columns within a budget.  This contains both previous value and current value.', 'SCHEMA', N'dbo', 'TABLE', N'Budget_Column_Audit', NULL, NULL
GO

ALTER TABLE [dbo].[Budget_Column_Audit] ADD CONSTRAINT [PK_Budget_Column_Audit] PRIMARY KEY NONCLUSTERED  ([Budget_Column_Audit_Id]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
CREATE CLUSTERED INDEX [IX_Budget_Column_Audit_Budget_Id_Audit_Column_Name] ON [dbo].[Budget_Column_Audit] ([Budget_Id], [Audit_Column_Name]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
