CREATE TABLE [Workflow].[Cu_Invoice_Attribute]
(
[CU_INVOICE_ID] [int] NOT NULL,
[Is_Priority] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Attribute__Is_Priority] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Attribute__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Cu_Invoice_Attribute__Last_Change_Ts] DEFAULT (getdate()),
[Source_Cd] [int] NULL
) ON [DB_DATA01]
GO
CREATE CLUSTERED INDEX [CLIX_CU_Invoice_Attribute_CU_INVOICE_ID] ON [Workflow].[Cu_Invoice_Attribute] ([CU_INVOICE_ID]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Attribute] ADD CONSTRAINT [fk_CU_INVOICE__Cu_Invoice_Attribute] FOREIGN KEY ([CU_INVOICE_ID]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
