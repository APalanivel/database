CREATE TABLE [dbo].[RM_Client_Comment]
(
[RM_Client_Comment_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL,
[Cbms_Image_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [k_RM_Client_Comment_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [k_RM_Client_Comment_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Client_Comment] ADD CONSTRAINT [pk_RM_Client_Comment] PRIMARY KEY CLUSTERED  ([RM_Client_Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Client_Comment] ADD CONSTRAINT [fk_RM_Client_Comment_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[RM_Client_Comment] ADD CONSTRAINT [fk_RM_Client_Comment_Comment_Id] FOREIGN KEY ([Comment_Id]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
