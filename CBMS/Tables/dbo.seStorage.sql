CREATE TABLE [dbo].[seStorage]
(
[StorageId] [int] NOT NULL IDENTITY(1, 1),
[ReportDate] [datetime] NOT NULL,
[WeekOfYear] [int] NULL,
[Volume] [int] NOT NULL,
[Change] [int] NULL,
[WeekEnding] [datetime] NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seStorage', NULL, NULL
GO

ALTER TABLE [dbo].[seStorage] ADD 
CONSTRAINT [PK_seStorage] PRIMARY KEY CLUSTERED  ([StorageId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
