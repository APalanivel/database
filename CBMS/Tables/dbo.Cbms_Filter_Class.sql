CREATE TABLE [dbo].[Cbms_Filter_Class]
(
[Cbms_Filter_Class_Id] [int] NOT NULL IDENTITY(1, 1),
[Filter_Class_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Base_Stmt] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Filter_Class] ADD CONSTRAINT [pk_Cbms_Filter_Class] PRIMARY KEY CLUSTERED  ([Cbms_Filter_Class_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Filter_Class] ADD CONSTRAINT [un_Cbms_Filter_Class__Filter_Class_Name] UNIQUE NONCLUSTERED  ([Filter_Class_Name]) ON [DB_DATA01]
GO
