CREATE TABLE [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map]
(
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[IC_Bots_Process_Id] [int] NULL,
[Enroll_In_Bot] [bit] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [Df_IC_IC_Bots_Process_Invoice_Collection_Account_Config_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [Df_IC_Bots_Process_Invoice_Collection_Account_Config_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IC_Bots_Process_Invoice_Collection_Account_Config_Map] ADD CONSTRAINT [pk_IC_Bots_Process_Invoice_Collection_Account_Config_Map__Invoice_Collection_Account_Config_Id] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Account_Config_Id]) ON [DB_DATA01]
GO
