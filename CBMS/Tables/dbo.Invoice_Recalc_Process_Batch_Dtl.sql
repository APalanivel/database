CREATE TABLE [dbo].[Invoice_Recalc_Process_Batch_Dtl]
(
[Invoice_Recalc_Process_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Recalc_Process_Batch_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Error_Dsc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Recalc_Process_Batch_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Recalc_Process_Batch_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Recalc_Process_Batch_Dtl] ADD CONSTRAINT [pk_Invoice_Recalc_Process_Batch_Dtl] PRIMARY KEY NONCLUSTERED  ([Invoice_Recalc_Process_Batch_Dtl_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [uix_Invoice_Recalc_Process_Batch_Dtl__Invoice_Recalc_Process_Batch_Id] ON [dbo].[Invoice_Recalc_Process_Batch_Dtl] ([Invoice_Recalc_Process_Batch_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Recalc_Process_Batch_Dtl] ADD CONSTRAINT [unc_Invoice_Recalc_Process_Batch_Dtl__Invoice_Recalc_Process_Batch_Id__Cu_Invoice_Id] UNIQUE CLUSTERED  ([Invoice_Recalc_Process_Batch_Id], [Cu_Invoice_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Recalc_Process_Batch_Dtl] ADD CONSTRAINT [fk_Invoice_Recalc_Process_Batch__Invoice_Recalc_Process_Batch_Dtl] FOREIGN KEY ([Invoice_Recalc_Process_Batch_Id]) REFERENCES [dbo].[Invoice_Recalc_Process_Batch] ([Invoice_Recalc_Process_Batch_Id])
GO
