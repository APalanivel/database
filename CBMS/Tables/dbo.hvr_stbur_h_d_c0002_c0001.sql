CREATE TABLE [dbo].[hvr_stbur_h_d_c0002_c0001]
(
[parallel_session] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__paral__7F89FA32] DEFAULT ((0)),
[is_busy] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__is_bu__007E1E6B] DEFAULT ((0)),
[tbl_name] [varchar] (124) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stbur__tbl_n__017242A4] DEFAULT (''),
[hvr_op] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__hvr_o__026666DD] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[hvr_stbur_h_d_c0002_c0001] ADD CONSTRAINT [PK__hvr_stbu__ACCE1AB57DA1B1C0] PRIMARY KEY NONCLUSTERED  ([parallel_session]) ON [DB_DATA01]
GO
