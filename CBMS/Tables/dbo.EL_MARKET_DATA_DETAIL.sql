CREATE TABLE [dbo].[EL_MARKET_DATA_DETAIL]
(
[PRICE_POINT_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1),
[PRICE_POINT_VALUE] [decimal] (18, 4) NULL,
[PRICE_POINT_DATE] [datetime] NULL,
[PRICE_POINT_ID] [int] NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point_detail
--when adding to this table
create trigger [dbo].[trg_ins_El_Market_Data_Detail]
on [dbo].[EL_MARKET_DATA_DETAIL]
for insert
as 
begin
set nocount on

	insert into market_price_point_detail (market_price_point_id,
	market_price_point_value,market_price_point_Detail_date,market_price_point_future_date)
	select  market_price_point_id,
	price_point_value,--b.price_point,
	case when charindex('day-ahead',b.price_point,0)>0 then dateadd(dd,1,dbo.justdate(price_point_date))
	when charindex('real-time',b.price_point,0)>0 then dbo.justdate(price_point_date)
	else dbo.justdate(price_point_date)  end as detail_date,
	price_point_date
	from market_price_point a
	inner join el_market_data b on a.market_price_point=b.price_point
	inner join inserted d on d.price_point_id=b.price_point_id

end

GO
ALTER TABLE [dbo].[EL_MARKET_DATA_DETAIL] ADD CONSTRAINT [PK_EL_MARKET_DATA_DETAIL] PRIMARY KEY CLUSTERED  ([PRICE_POINT_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
CREATE NONCLUSTERED INDEX [IX_PRICE_POINT_DATE] ON [dbo].[EL_MARKET_DATA_DETAIL] ([PRICE_POINT_DATE], [PRICE_POINT_ID], [PRICE_POINT_VALUE]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Electric power price data tied to price points', 'SCHEMA', N'dbo', 'TABLE', N'EL_MARKET_DATA_DETAIL', NULL, NULL
GO
