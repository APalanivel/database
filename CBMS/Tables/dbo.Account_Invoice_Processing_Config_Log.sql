CREATE TABLE [dbo].[Account_Invoice_Processing_Config_Log]
(
[Account_Invoice_Processing_Config_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Invoice_Processing_Config_Id] [int] NOT NULL,
[Field_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Change_Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Previous_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Updated_Using_Apply_All] [bit] NOT NULL CONSTRAINT [DF__Account_I__Is_Up__4C62A1F3] DEFAULT ((0)),
[Event_By_User_Id] [int] NOT NULL,
[Event_Ts] [datetime] NOT NULL CONSTRAINT [DF__Account_I__Event__4D56C62C] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Processing_Config_Log] ADD CONSTRAINT [pk_Account_Invoice_Processing_Config_Log] PRIMARY KEY CLUSTERED  ([Account_Invoice_Processing_Config_Log_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Invoice_Processing_Config_Log__Account_Invoice_Processing_Config_Id] ON [dbo].[Account_Invoice_Processing_Config_Log] ([Account_Invoice_Processing_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Processing_Config_Log] ADD CONSTRAINT [fk_Account_Invoice_Processing_Config__Account_Invoice_Processing_Config_Log] FOREIGN KEY ([Account_Invoice_Processing_Config_Id]) REFERENCES [dbo].[Account_Invoice_Processing_Config] ([Account_Invoice_Processing_Config_Id])
GO
ALTER TABLE [dbo].[Account_Invoice_Processing_Config_Log] ADD CONSTRAINT [RefAccount_Invoice_Processing_Config] FOREIGN KEY ([Account_Invoice_Processing_Config_Id]) REFERENCES [dbo].[Account_Invoice_Processing_Config] ([Account_Invoice_Processing_Config_Id])
GO
