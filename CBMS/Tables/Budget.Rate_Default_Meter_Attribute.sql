CREATE TABLE [Budget].[Rate_Default_Meter_Attribute]
(
[Rate_Default_Meter_Attribute_Id] [int] NOT NULL IDENTITY(1, 1),
[RATE_ID] [int] NOT NULL,
[EC_Meter_Attribute_Id] [int] NOT NULL,
[Attribute_Value_Type_Cd] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[EC_Meter_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Attribute_Value_Requirement_Option_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Rate_Default_Meter_Attribute__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Rate_Default_Meter_Attribute__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Rate_Default_Meter_Attribute] ADD CONSTRAINT [pk_Rate_Default_Meter_Attribute] PRIMARY KEY CLUSTERED  ([Rate_Default_Meter_Attribute_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Rate_Default_Meter_Attribute] ADD CONSTRAINT [un_Rate_Default_Meter_Attribute__RATE_ID__EC_Meter_Attribute_Id__Attribute_Value_Type_Cd__Start_Dt__End_Dt] UNIQUE NONCLUSTERED  ([RATE_ID], [EC_Meter_Attribute_Id], [Attribute_Value_Type_Cd], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Rate_Default_Meter_Attribute] ADD CONSTRAINT [fk_EC_Meter_Attribute__Rate_Default_Meter_Attribute] FOREIGN KEY ([EC_Meter_Attribute_Id]) REFERENCES [dbo].[EC_Meter_Attribute] ([EC_Meter_Attribute_Id])
GO
ALTER TABLE [Budget].[Rate_Default_Meter_Attribute] ADD CONSTRAINT [fk_RATE__Rate_Default_Meter_Attribute] FOREIGN KEY ([RATE_ID]) REFERENCES [dbo].[RATE] ([RATE_ID])
GO
