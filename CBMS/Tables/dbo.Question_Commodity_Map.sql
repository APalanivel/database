CREATE TABLE [dbo].[Question_Commodity_Map]
(
[Question_Commodity_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Commodity_Id] [int] NOT NULL,
[Utility_Summary_Question_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Junction between Utilty_Section_Question and commoditites', 'SCHEMA', N'dbo', 'TABLE', N'Question_Commodity_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Question_Commodity_Map] ADD CONSTRAINT [unc_Question_Commodity_Map__Commodity_Utility__Summary_Question] UNIQUE CLUSTERED  ([Commodity_Id], [Utility_Summary_Question_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Question_Commodity_Map] ADD CONSTRAINT [pk_Question_Commodity_Map] PRIMARY KEY NONCLUSTERED  ([Question_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Question_Commodity_Map__Utility_Summary_Question_Id] ON [dbo].[Question_Commodity_Map] ([Utility_Summary_Question_Id]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Question_Commodity_Map] ADD
CONSTRAINT [fk_Commodity__Question_Commodity_Map] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
ALTER TABLE [dbo].[Question_Commodity_Map] ADD
CONSTRAINT [fk_Utility_Summary_Question__Question_Commodity_Map] FOREIGN KEY ([Utility_Summary_Question_Id]) REFERENCES [dbo].[Utility_Summary_Question] ([Utility_Summary_Question_Id])


GO
