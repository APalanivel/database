CREATE TABLE [dbo].[CONVERSION_COMMERCIAL_COST_USAGE_SITE]
(
[SITE_ID] [int] NULL,
[SITE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SERVICE_MONTH] [datetime] NOT NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[EL_UNIT_OF_MEASURE_TYPE_ID] [int] NULL,
[NG_UNIT_OF_MEASURE_TYPE_ID] [int] NULL,
[EL_USAGE] [decimal] (32, 16) NULL,
[EL_ON_PEAK_USAGE] [decimal] (32, 16) NULL,
[EL_OFF_PEAK_USAGE] [decimal] (32, 16) NULL,
[EL_INT_PEAK_USAGE] [decimal] (32, 16) NULL,
[EL_DEMAND] [decimal] (32, 16) NULL,
[EL_DEMAND_TOTAL] [decimal] (32, 16) NULL,
[EL_MARKETER_COST] [decimal] (32, 16) NULL,
[EL_UTILITY_COST] [decimal] (32, 16) NULL,
[EL_COST] [decimal] (32, 16) NULL,
[EL_TAX] [decimal] (32, 16) NULL,
[NG_USAGE] [decimal] (32, 16) NULL,
[NG_MARKETER_COST] [decimal] (32, 16) NULL,
[NG_UTILITY_COST] [decimal] (32, 16) NULL,
[NG_COST] [decimal] (32, 16) NULL,
[NG_TAX] [decimal] (32, 16) NULL,
[TOTAL_COST] [decimal] (32, 16) NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'CONVERSION_COMMERCIAL_COST_USAGE_SITE', NULL, NULL
GO
