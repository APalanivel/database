CREATE TABLE [dbo].[Contract_Notification_Log_Meter_Dtl]
(
[Contract_Flow_Verification_Meter_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Notification_Log_Id] [int] NOT NULL,
[Meter_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Notification_Log_Meter_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log_Meter_Dtl] ADD CONSTRAINT [pk_Contract_Notification_Log_Meter_Dtl] PRIMARY KEY CLUSTERED  ([Contract_Flow_Verification_Meter_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log_Meter_Dtl] ADD CONSTRAINT [un_Contract_Notification_Log_Meter_Dtl__Contract_Notification_Log_Id__Meter_Id] UNIQUE NONCLUSTERED  ([Contract_Notification_Log_Id], [Meter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log_Meter_Dtl] ADD CONSTRAINT [fk_Contract_Notification_Log__Contract_Notification_Log_Meter_Dtl] FOREIGN KEY ([Contract_Notification_Log_Id]) REFERENCES [dbo].[Contract_Notification_Log] ([Contract_Notification_Log_Id])
GO
