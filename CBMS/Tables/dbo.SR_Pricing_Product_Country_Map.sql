CREATE TABLE [dbo].[SR_Pricing_Product_Country_Map]
(
[SR_Pricing_Product_Country_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[SR_Pricing_Product_Id] [int] NOT NULL,
[Country_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_SR_Pricing_Product_Country_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[SR_Pricing_Product_Country_Map] ADD CONSTRAINT [pk_SR_Pricing_Product_Country_Map] PRIMARY KEY CLUSTERED  ([SR_Pricing_Product_Country_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[SR_Pricing_Product_Country_Map] ADD CONSTRAINT [unc_SR_Pricing_Product_Country_Map_SR_Product_Id_Country_Id] UNIQUE NONCLUSTERED  ([SR_Pricing_Product_Id], [Country_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[SR_Pricing_Product_Country_Map] ADD CONSTRAINT [fk_SR_Pricing_Product_Country_Pricing_Product] FOREIGN KEY ([SR_Pricing_Product_Id]) REFERENCES [dbo].[SR_PRICING_PRODUCT] ([SR_PRICING_PRODUCT_ID])
GO
