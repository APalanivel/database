CREATE TABLE [Budget].[Calculation_Tester_Audit]
(
[Calculation_Tester_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Calculation_Request_Type_Cd] [int] NULL,
[Account_Id] [int] NULL,
[Meter_Id] [int] NULL,
[Term_Start_Dt] [date] NULL,
[Term_End_Dt] [date] NULL,
[Data_Type_Cd] [int] NULL,
[Verification_Status_Cd] [int] NULL,
[Verified_User_Id] [int] NULL,
[Verified_Ts] [datetime] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Calculation_Tester_Audit] ADD CONSTRAINT [PK__Calculation_Tester_Audit] PRIMARY KEY CLUSTERED  ([Calculation_Tester_Audit_Id]) ON [DB_DATA01]
GO
