CREATE TABLE [dbo].[Data_Quality_Test_Commodity]
(
[Data_Quality_Test_Commodity_Id] [int] NOT NULL IDENTITY(1, 1),
[Data_Quality_Test_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Execution_Seq_No] [smallint] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Data_Quality_Test_Commodity__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Data_Quality_Test_Commodity__Last_Change_Ts] DEFAULT (getdate()),
[Is_Active] [bit] NOT NULL CONSTRAINT [DF__Data_Qual__Is_Ac__0380E524] DEFAULT ((1))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Data_Quality_Test_Commodity] ADD CONSTRAINT [pk_Data_Quality_Test_Commodity] PRIMARY KEY CLUSTERED  ([Data_Quality_Test_Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Data_Quality_Test_Commodity] ADD CONSTRAINT [un_Data_Quality_Test_Commodity__Data_Quality_Test_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([Data_Quality_Test_Id], [Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Data_Quality_Test_Commodity] ADD CONSTRAINT [fk_Data_Quality_Test__Data_Quality_Test_Commodity] FOREIGN KEY ([Data_Quality_Test_Id]) REFERENCES [dbo].[Data_Quality_Test] ([Data_Quality_Test_Id])
GO
