CREATE TABLE [dbo].[App_Menu_Advanced_Visualization_Config]
(
[App_Menu_Advanced_Visualization_Config_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[App_Menu_Id] [int] NOT NULL,
[Base_URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Advanced_Visualization_URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Report_Title_Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Report_Desc_Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Report_Bread_Crumb_Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL,
[App_GUID] [uniqueidentifier] NOT NULL CONSTRAINT [df_App_Menu_Advanced_Visualization_Config__App_GUID] DEFAULT (newid())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Menu_Advanced_Visualization_Config] ADD CONSTRAINT [PK_App_Menu_Advanced_Visulization_Config] PRIMARY KEY CLUSTERED  ([App_Menu_Advanced_Visualization_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Menu_Advanced_Visualization_Config] ADD CONSTRAINT [fk_App_Menu_Advanced_Visualization_Config_App_Menu_App_Menu_Id] FOREIGN KEY ([App_Menu_Id]) REFERENCES [dbo].[APP_MENU] ([APP_MENU_ID])
GO
