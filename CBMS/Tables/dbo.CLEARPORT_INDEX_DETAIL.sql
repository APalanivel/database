CREATE TABLE [dbo].[CLEARPORT_INDEX_DETAIL]
(
[CLEARPORT_INDEX_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1),
[INDEX_DETAIL_VALUE] [decimal] (32, 16) NULL,
[INDEX_DETAIL_DATE] [datetime] NULL,
[CLEARPORT_INDEX_MONTH_ID] [int] NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point_detail
--when adding to this table
create trigger [dbo].[trg_ins_Clearport_Index_Detail]
on [dbo].[CLEARPORT_INDEX_DETAIL]
for insert
as 
begin
set nocount on

	insert into market_price_point_detail (market_price_point_id,
	market_price_point_value,market_price_point_Detail_date,market_price_point_future_date)
	select distinct a.market_price_point_id,
	index_detail_value,--'m',
	index_detail_date,
	clearport_index_month
	from market_price_point a
	inner join market_price_index b on a.market_price_index_id=1
	inner join clearport_index c on a.market_price_point=c.clearport_index
	inner join clearport_index_months d on d.clearport_index_id=c.clearport_index_id
	inner join inserted e on e.clearport_index_month_id=d.clearport_index_month_id
	where b.market_price_index_name='clearport'

end


GO
ALTER TABLE [dbo].[CLEARPORT_INDEX_DETAIL] ADD CONSTRAINT [PK_CLEARPORT_INDEX_DETAIL] PRIMARY KEY CLUSTERED  ([CLEARPORT_INDEX_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
CREATE NONCLUSTERED INDEX [IX_MONTH] ON [dbo].[CLEARPORT_INDEX_DETAIL] ([CLEARPORT_INDEX_MONTH_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_MONTH_DATE] ON [dbo].[CLEARPORT_INDEX_DETAIL] ([CLEARPORT_INDEX_MONTH_ID], [INDEX_DETAIL_DATE]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_DETAIL_DATE] ON [dbo].[CLEARPORT_INDEX_DETAIL] ([INDEX_DETAIL_DATE], [CLEARPORT_INDEX_MONTH_ID], [INDEX_DETAIL_VALUE]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Clearport Pricing Index values', 'SCHEMA', N'dbo', 'TABLE', N'CLEARPORT_INDEX_DETAIL', NULL, NULL
GO
