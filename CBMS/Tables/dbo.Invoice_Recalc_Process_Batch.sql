CREATE TABLE [dbo].[Invoice_Recalc_Process_Batch]
(
[Invoice_Recalc_Process_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[Status_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Recalc_Process_Batch__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Recalc_Process_Batch__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Recalc_Process_Batch] ADD CONSTRAINT [pk_Invoice_Recalc_Process_Batch] PRIMARY KEY CLUSTERED  ([Invoice_Recalc_Process_Batch_Id]) ON [DB_DATA01]
GO
