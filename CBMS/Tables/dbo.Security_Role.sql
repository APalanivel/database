CREATE TABLE [dbo].[Security_Role]
(
[Security_Role_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Client_Id] [int] NOT NULL,
[Is_Corporate] [bit] NOT NULL CONSTRAINT [df_Security_Role_Is_Corporate] DEFAULT ((0)),
[Show_Large_Datasets] [bit] NOT NULL CONSTRAINT [df_Security_Role_Show_Large_Datasets] DEFAULT ((1)),
[Add_Ts] [datetime] NOT NULL CONSTRAINT [df_Security_Role_Add_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Security_Role_Last_Change_Ts] DEFAULT (getdate())
) ON [DBData_UserInfo]
CREATE NONCLUSTERED INDEX [IX_Security_Role__Client_ID__Security_Role_ID] ON [dbo].[Security_Role] ([Client_Id], [Security_Role_Id]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', 'Groups of unique security access list', 'SCHEMA', N'dbo', 'TABLE', N'Security_Role', NULL, NULL
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/******
NAME:	tr_Security_Role_Transfer

DESCRIPTION: Trigger on dbo.Security_Role to send changes for DV2 through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	TP			Anoop
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	TP			12 Feb 2014		Created
******/
CREATE TRIGGER [dbo].[tr_Security_Role_Transfer] ON [dbo].[Security_Role]
      FOR INSERT,UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
		   ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT 
                        isnull(i.Security_Role_Id, d.Security_Role_Id) AS Security_Role_Id
                       ,i.Client_Id
                       ,i.Is_Corporate
                       ,i.Show_Large_Datasets
                       ,i.Add_Ts
                       ,i.Last_Change_Ts
                       ,case WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NULL THEN 'I'
                             WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'U'
                             WHEN i.Security_Role_Id IS NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'D'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Security_Role_Id = d.Security_Role_Id
            FOR
                       XML PATH('Security_Role_Change')
                          ,ELEMENTS
                          ,ROOT('Security_Role_Changes') );

      IF @Message IS NOT NULL 
            BEGIN
	
				  SET @From_Service = N'//Change_Control/Service/CBMS/Security_Role_Transfer'

				  -- Cycle through the targets in Change_Control Targt and sent themessage to each    
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Security_Role')     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0 
                        BEGIN    
						
							EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT

                            ;SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Security_Role_Transfer] (@Message);

                              
                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  

            END

END




;
GO

ALTER TABLE [dbo].[Security_Role] ADD 
CONSTRAINT [pk_Security_Role] PRIMARY KEY CLUSTERED  ([Security_Role_Id]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
ALTER TABLE [dbo].[Security_Role] ADD
CONSTRAINT [fk_Security_Role_CLient] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
