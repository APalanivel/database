CREATE TABLE [dbo].[Account_Invoice_Collection_Frequency]
(
[Account_Invoice_Collection_Frequency_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Seq_No] [smallint] NOT NULL,
[Is_Primary] [bit] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Frequency__Is_Primary] DEFAULT ((0)),
[Invoice_Frequency_Cd] [int] NOT NULL,
[Invoice_Frequency_Pattern_Cd] [int] NULL,
[Expected_Invoice_Raised_Day] [tinyint] NULL,
[Expected_Invoice_Raised_Month] [tinyint] NULL,
[Expected_Invoice_Raised_Day_Lag] [tinyint] NULL,
[Expected_Invoice_Received_Day] [tinyint] NULL,
[Expected_Invoice_Received_Month] [tinyint] NULL,
[Expected_Invoice_Received_Day_Lag] [tinyint] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Frequency__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Frequency__Last_Change_Ts] DEFAULT (getdate()),
[Custom_Days] [int] NULL,
[Custom_Invoice_Received_Day] [int] NULL,
[Next_Config_Run_Date] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Frequency] ADD CONSTRAINT [pk_Account_Invoice_Collection_Frequency] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Frequency_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Frequency] ADD CONSTRAINT [un_Account_Invoice_Collection_Frequency] UNIQUE NONCLUSTERED  ([Invoice_Collection_Account_Config_Id], [Seq_No]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Frequency] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Account_Invoice_Collection_Frequency] FOREIGN KEY ([Invoice_Collection_Account_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config] ([Invoice_Collection_Account_Config_Id])
GO
