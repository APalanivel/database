CREATE TABLE [dbo].[Cost_Usage_Account_Dtl_Comment_Map]
(
[Cost_Usage_Account_Dtl_Comment_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Comment_ID] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cost_Usage_Account_Dtl_Comment_Map] ADD CONSTRAINT [PK__Cost_Usa__42AEBF6315DE0413] PRIMARY KEY CLUSTERED  ([Cost_Usage_Account_Dtl_Comment_Map_Id]) ON [DB_DATA01]
GO
