CREATE TABLE [dbo].[Account_Invoice_Collection_Online_Source_Dtl]
(
[Account_Invoice_Collection_Online_Source_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Invoice_Collection_Source_Id] [int] NOT NULL,
[URL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Login_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Passcode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instruction] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Instruction_Document_Image_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Online_Source_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Online_Source_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Online_Source_Dtl] ADD CONSTRAINT [pk_Account_Invoice_Collection_Online_Source_Dtl] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Online_Source_Dtl_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Account_Invoice_Collection_Online_Source_Dtl__Account_Invoice_Collection_Source_Id] ON [dbo].[Account_Invoice_Collection_Online_Source_Dtl] ([Account_Invoice_Collection_Source_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Online_Source_Dtl] ADD CONSTRAINT [fk_Account_Invoice_Collection_Source__Account_Invoice_Collection_Online_Source_Dtl] FOREIGN KEY ([Account_Invoice_Collection_Source_Id]) REFERENCES [dbo].[Account_Invoice_Collection_Source] ([Account_Invoice_Collection_Source_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'In invoice collection process the type of Invoice Source Online is captured in this table', 'SCHEMA', N'dbo', 'TABLE', N'Account_Invoice_Collection_Online_Source_Dtl', NULL, NULL
GO
