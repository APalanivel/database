CREATE TABLE [dbo].[Variance_Historical_Data_Point]
(
[Variance_Log_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Baseline_Value] [decimal] (28, 10) NULL,
[High_Tolerance] [decimal] (28, 10) NULL,
[Low_Tolerance] [decimal] (28, 10) NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Historical_Data_Point__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Historical_Data_Point] ADD CONSTRAINT [pk_Variance_Historical_Data_Point] PRIMARY KEY CLUSTERED  ([Variance_Log_Id], [Service_Month]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Historical_Data_Point] ADD CONSTRAINT [fk_Variance_Log__Variance_Historical_Data_Point] FOREIGN KEY ([Variance_Log_Id]) REFERENCES [dbo].[Variance_Log] ([Variance_Log_Id])
GO
