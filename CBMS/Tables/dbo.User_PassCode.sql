CREATE TABLE [dbo].[User_PassCode]
(
[User_PassCode_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[User_Info_Id] [int] NOT NULL,
[PassCode] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active_From_Dt] [date] NOT NULL CONSTRAINT [DF_User_PassCode_Active_From_Dt] DEFAULT ('01/01/1900'),
[Created_By_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF_User_PassCode_Last_Change_Ts] DEFAULT (getdate()),
[Password_Salt] [nvarchar] (55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_UserInfo]
GO
ALTER TABLE [dbo].[User_PassCode] ADD CONSTRAINT [PK_User_PassCode] PRIMARY KEY CLUSTERED  ([User_PassCode_Id]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
GO
CREATE NONCLUSTERED INDEX [ix_User_Pascode__User_Info_ID] ON [dbo].[User_PassCode] ([User_Info_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[User_PassCode] ADD CONSTRAINT [FK_User_Info_User_PassCode] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Maintains the list of current and Past Login Credentials', 'SCHEMA', N'dbo', 'TABLE', N'User_PassCode', NULL, NULL
GO
