CREATE TABLE [dbo].[Budget_Currency_Map_Audit]
(
[Audit_Function] [smallint] NOT NULL,
[Audit_Ts] [datetime] NOT NULL,
[Budget_Currency_Map_Id] [int] NOT NULL,
[Budget_Id] [int] NOT NULL,
[Currency_Unit_Id] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to the Budget_Currency_Map', 'SCHEMA', N'dbo', 'TABLE', N'Budget_Currency_Map_Audit', NULL, NULL
GO
