CREATE TABLE [dbo].[Client_Document_Category]
(
[Client_Document_Category_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Category_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Id] [int] NOT NULL,
[Is_Default] [bit] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO


ALTER TABLE [dbo].[Client_Document_Category] ADD CONSTRAINT [PK_Document_Client_Category] PRIMARY KEY CLUSTERED  ([Client_Document_Category_Id]) ON [DB_DATA01]
GO
