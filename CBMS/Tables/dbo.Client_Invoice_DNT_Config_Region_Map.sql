CREATE TABLE [dbo].[Client_Invoice_DNT_Config_Region_Map]
(
[Client_Invoice_DNT_Config_Region_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Invoice_DNT_Config_Id] [int] NOT NULL,
[REGION_ID] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config_Region_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Region_Map] ADD CONSTRAINT [pk_Client_Invoice_DNT_Config_Region_Map] PRIMARY KEY CLUSTERED  ([Client_Invoice_DNT_Config_Region_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Region_Map] ADD CONSTRAINT [un_Client_Invoice_DNT_Config_Region_Map__Client_Invoice_DNT_Config_Region_Map_Id__REGION_ID] UNIQUE NONCLUSTERED  ([Client_Invoice_DNT_Config_Region_Map_Id], [REGION_ID]) ON [DB_DATA01]
GO
