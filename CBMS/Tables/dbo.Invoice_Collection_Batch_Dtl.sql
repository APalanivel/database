CREATE TABLE [dbo].[Invoice_Collection_Batch_Dtl]
(
[Invoice_Collection_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Invoice_Collection_Batch_Id] [int] NOT NULL,
[Collection_Start_Dt] [date] NULL,
[Collection_End_Dt] [date] NULL,
[Status_Cd] [int] NOT NULL,
[Error_Dsc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Batch_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Batch_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Batch_Dtl] ADD CONSTRAINT [pk_Invoice_Collection_Batch_Dtl] PRIMARY KEY NONCLUSTERED  ([Invoice_Collection_Batch_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Batch_Dtl] ADD CONSTRAINT [unc_Invoice_Collection_Batch_Dtl__Invoice_Collection_Account_Config_Id__Invoice_Collection_Batch_Id] UNIQUE CLUSTERED  ([Invoice_Collection_Account_Config_Id], [Invoice_Collection_Batch_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [uix_Invoice_Collection_Batch_Dtl__Invoice_Collection_Batch_Id] ON [dbo].[Invoice_Collection_Batch_Dtl] ([Invoice_Collection_Batch_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Batch_Dtl] ADD CONSTRAINT [fk_Invoice_Collection_Batch__Invoice_Collection_Batch_Dtl] FOREIGN KEY ([Invoice_Collection_Batch_Id]) REFERENCES [dbo].[Invoice_Collection_Batch] ([Invoice_Collection_Batch_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Batch_Dtl] ADD CONSTRAINT [RefInvoice_Collection_Batc2100] FOREIGN KEY ([Invoice_Collection_Batch_Id]) REFERENCES [dbo].[Invoice_Collection_Batch] ([Invoice_Collection_Batch_Id])
GO
