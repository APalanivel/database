CREATE TABLE [dbo].[Utility_Dtl_Volume_Requirement]
(
[Utility_Dtl_Volume_Requirement_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Utility_Volume_Dsc] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Utility_UOM_Cd] [int] NULL,
[Time_Period] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Time_Req] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Req] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'the ‘Volume Requirement to Transport’ specific data', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Volume_Requirement', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Volume_Requirement] ADD CONSTRAINT [unc_Utility_Dtl_Volume_Requirement__Question_Commodity_Map_Id__Vendor_Commodity_Map_Id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Volume_Requirement] ADD CONSTRAINT [pk_Utility_Dtl_Volume_Requirement] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Volume_Requirement_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Volume_Requirement__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Volume_Requirement] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Volume_Requirement] ADD
CONSTRAINT [fk_Question_Commodity_Map__Utility_Dtl_Volume_Requirement] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Volume_Requirement] ADD
CONSTRAINT [fk_VENDOR_COMMODITY_MAP__Utility_Dtl_Volume_Requirement] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])


GO
