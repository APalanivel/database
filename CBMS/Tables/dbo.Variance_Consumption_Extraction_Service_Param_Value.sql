CREATE TABLE [dbo].[Variance_Consumption_Extraction_Service_Param_Value]
(
[Variance_Consumption_Extraction_Service_Param_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Consumption_Level_Id] [int] NOT NULL,
[Variance_Extraction_Service_Param_Id] [int] NOT NULL,
[UOM_Cd] [int] NULL,
[Param_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Consumption_Extraction_Service_Param_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Variance_Consumption_Extraction_Service_Param_Value__Last_Change_Ts] DEFAULT (getdate()),
[Variance_Category_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Consumption_Extraction_Service_Param_Value] ADD CONSTRAINT [pk_Variance_Consumption_Extraction_Service_Param_Value] PRIMARY KEY CLUSTERED  ([Variance_Consumption_Extraction_Service_Param_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Consumption_Extraction_Service_Param_Value] ADD CONSTRAINT [un_Variance_Consumption_Extraction_Service_Param_Value] UNIQUE NONCLUSTERED  ([Variance_Consumption_Level_Id], [Variance_Extraction_Service_Param_Id], [UOM_Cd], [Variance_Category_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Consumption_Extraction_Service_Param_Value] ADD CONSTRAINT [fk_Variance_Consumption_Level__Variance_Consumption_Extraction_Service_Param_Value] FOREIGN KEY ([Variance_Consumption_Level_Id]) REFERENCES [dbo].[Variance_Consumption_Level] ([Variance_Consumption_Level_Id])
GO
ALTER TABLE [dbo].[Variance_Consumption_Extraction_Service_Param_Value] ADD CONSTRAINT [fk_Variance_Extraction_Service_Param__Variance_Consumption_Extraction_Service_Param_Value] FOREIGN KEY ([Variance_Extraction_Service_Param_Id]) REFERENCES [dbo].[Variance_Extraction_Service_Param] ([Variance_Extraction_Service_Param_Id])
GO
