CREATE TABLE [dbo].[Application]
(
[Application_Id] [int] NOT NULL IDENTITY(1, 1),
[Application_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'Application', NULL, NULL
GO

ALTER TABLE [dbo].[Application] ADD CONSTRAINT [pk_App] PRIMARY KEY NONCLUSTERED  ([Application_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Application] ADD CONSTRAINT [uc_App_App_Name] UNIQUE CLUSTERED  ([Application_Name]) ON [PRIMARY]
GO
