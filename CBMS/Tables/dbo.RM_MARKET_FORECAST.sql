CREATE TABLE [dbo].[RM_MARKET_FORECAST]
(
[RM_MARKET_FORECAST_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RM_MARKET_ID] [smallint] NOT NULL,
[Hi_Range_Price] [decimal] (9, 5) NOT NULL,
[Low_Range_Price] [decimal] (9, 5) NOT NULL,
[Risk_Tolerance_Type_id] [int] NOT NULL,
[Forecast_Year] [smallint] NOT NULL,
[Forecast_Quarter] [tinyint] NOT NULL,
[AsofDate] [smalldatetime] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management market data - last date data received 9/9/2011', 'SCHEMA', N'dbo', 'TABLE', N'RM_MARKET_FORECAST', NULL, NULL
GO

ALTER TABLE [dbo].[RM_MARKET_FORECAST] ADD 
CONSTRAINT [PK_RM_MARKET_FORECAST] PRIMARY KEY CLUSTERED  ([RM_MARKET_FORECAST_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]

GO
