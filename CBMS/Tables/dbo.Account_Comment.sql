CREATE TABLE [dbo].[Account_Comment]
(
[Account_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Comment] ADD CONSTRAINT [pk_Account_Comment] PRIMARY KEY CLUSTERED  ([Account_Id], [Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Comment] ADD CONSTRAINT [fk_ACCOUNT__Account_Comment] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
ALTER TABLE [dbo].[Account_Comment] ADD CONSTRAINT [fk_Comment__Account_Comment] FOREIGN KEY ([Comment_Id]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
