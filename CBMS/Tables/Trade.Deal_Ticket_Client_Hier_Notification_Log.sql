CREATE TABLE [Trade].[Deal_Ticket_Client_Hier_Notification_Log]
(
[Deal_Ticket_Client_Hier_Notification_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Client_Hier_Workflow_Status_Id] [int] NOT NULL,
[Notification_Msg_Dtl_Id] [int] NOT NULL,
[Workflow_Task_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Notification_Log] ADD CONSTRAINT [pk_Deal_Ticket_Client_Hier_Notification_Log] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Hier_Notification_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Notification_Log] ADD CONSTRAINT [un_Deal_Ticket_Client_Hier_Notification_Log__Deal_Ticket_Client_Hier_Workflow_Status_Id__Notification_Msg_Dtl_Id] UNIQUE NONCLUSTERED  ([Deal_Ticket_Client_Hier_Workflow_Status_Id], [Notification_Msg_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Notification_Log] ADD CONSTRAINT [fk_Deal_Ticket_Client_Hier_Workflow_Status__Deal_Ticket_Client_Hier_Notification_Log] FOREIGN KEY ([Deal_Ticket_Client_Hier_Workflow_Status_Id]) REFERENCES [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ([Deal_Ticket_Client_Hier_Workflow_Status_Id])
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Notification_Log] ADD CONSTRAINT [fk_Notification_Msg_Dtl__Deal_Ticket_Client_Hier_Notification_Log] FOREIGN KEY ([Notification_Msg_Dtl_Id]) REFERENCES [dbo].[Notification_Msg_Dtl] ([Notification_Msg_Dtl_Id])
GO
