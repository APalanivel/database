CREATE TABLE [Workflow].[Workflow_Queue_Search_Filter_Group_Invoice_Map]
(
[Workflow_Queue_Search_Filter_Group_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter_Group_Invoice_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Group_Invoice_Map] ADD CONSTRAINT [pk_Workflow_Queue_Search_Filter_Group_Invoice_Map] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Search_Filter_Group_Id], [Cu_Invoice_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Group_Invoice_Map] ADD CONSTRAINT [fk_Workflow_Queue_Search_Filter_Group__Workflow_Queue_Search_Filter_Group_Invoice_Map] FOREIGN KEY ([Workflow_Queue_Search_Filter_Group_Id]) REFERENCES [Workflow].[Workflow_Queue_Search_Filter_Group] ([Workflow_Queue_Search_Filter_Group_Id])
GO
