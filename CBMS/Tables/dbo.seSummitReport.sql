CREATE TABLE [dbo].[seSummitReport]
(
[ReportId] [int] NOT NULL IDENTITY(1000, 1),
[ReportTypeId] [int] NOT NULL,
[ReportDate] [datetime] NOT NULL,
[Headline] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DirFileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [int] NOT NULL,
[ContentTypeId] [int] NOT NULL,
[HideFromDemo] [bit] NOT NULL,
[ClientName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBMS_IMAGE_ID] [int] NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seSummitReport', NULL, NULL
GO

ALTER TABLE [dbo].[seSummitReport] ADD 
CONSTRAINT [PK_seSummitReport] PRIMARY KEY CLUSTERED  ([ReportId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
