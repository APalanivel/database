CREATE TABLE [dbo].[Client_Service_Category_Commodity]
(
[Client_Service_Category_Commodity_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Client_Service_Category_Id] [int] NOT NULL,
[Commodity_id] [int] NOT NULL,
[Sub_Client_Service_Category_Id] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table between Client_Service_Category and Commodity table.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Service_Category_Commodity', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Service_Category_Commodity] ADD CONSTRAINT [unc_Client_Service_Category_Commodity__Client_Service_Category_Id__Commodity_id__Sub_Client_Service_Category_Id] UNIQUE CLUSTERED  ([Client_Service_Category_Id], [Commodity_id], [Sub_Client_Service_Category_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Client_Service_Category_Commodity] ADD CONSTRAINT [pk_Client_Service_Category_Commodity] PRIMARY KEY NONCLUSTERED  ([Client_Service_Category_Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Client_Service_Category_Commodity__Client_Service_Category_Id] ON [dbo].[Client_Service_Category_Commodity] ([Client_Service_Category_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Client_Service_Category_Commodity__Sub_Client_Service_Category_Id] ON [dbo].[Client_Service_Category_Commodity] ([Sub_Client_Service_Category_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Client_Service_Category_Commodity] ADD
CONSTRAINT [fk_Client_Service_Category__Client_Service_Category_Commodity] FOREIGN KEY ([Client_Service_Category_Id]) REFERENCES [dbo].[Client_Service_Category] ([Client_Service_Category_Id])
ALTER TABLE [dbo].[Client_Service_Category_Commodity] ADD
CONSTRAINT [fk_Client_Service_Category__Sub_Client_Service_Category_Commodity] FOREIGN KEY ([Sub_Client_Service_Category_Id]) REFERENCES [dbo].[Client_Service_Category] ([Client_Service_Category_Id])
GO
