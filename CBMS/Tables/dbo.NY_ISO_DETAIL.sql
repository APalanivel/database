CREATE TABLE [dbo].[NY_ISO_DETAIL]
(
[ZONE_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1),
[ZONE_ID] [int] NULL,
[DATE] [datetime] NULL,
[PTID] [int] NULL,
[LBMP] [decimal] (18, 2) NULL,
[MARGINAL_COST_LOSSES] [decimal] (18, 2) NULL,
[MARGINAL_COST_CONGESTION] [decimal] (18, 2) NULL,
[PRICING_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SCARCITY] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point_detail
--when adding to this table
create trigger [dbo].[trg_ins_NY_Iso_Detail]
on [dbo].[NY_ISO_DETAIL]
for insert
as 
begin
set nocount on

	--need to account for new zones here in a separate query
	--if there is a possibility of adding new zones to this
	insert into market_price_point_detail (market_price_point_id,
	market_price_point_value,market_price_point_Detail_date,market_price_point_future_date)
	select  market_price_point_id,
	lbmp,--b.price_point,
	case when charindex('day-ahead',a.market_price_point,0)>0 then dateadd(dd,1,dbo.justdate([date]))
	when charindex('real-time',a.market_price_point,0)>0 then dbo.justdate([date])
	else dbo.justdate([date])  end as detail_date,
	[date]
	from market_price_point a
	inner join(
	select distinct b.zone_id,a.zone_name,
	pricing_type from ny_iso a inner join
	inserted b on a.zone_id=b.zone_id) b
	on a.market_price_point=b.zone_name+' '+b.pricing_type
	inner join inserted d on d.zone_id=b.zone_id and d.pricing_type=b.pricing_type

end



GO
ALTER TABLE [dbo].[NY_ISO_DETAIL] ADD CONSTRAINT [PK_NY_ISO_DETAIL] PRIMARY KEY CLUSTERED  ([ZONE_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pricing for different zones of NYISO, most recent data is 8/2009', 'SCHEMA', N'dbo', 'TABLE', N'NY_ISO_DETAIL', NULL, NULL
GO
