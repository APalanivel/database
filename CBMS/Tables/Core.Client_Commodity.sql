CREATE TABLE [Core].[Client_Commodity]
(
[Client_Commodity_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Commodity_Id] [int] NOT NULL,
[Client_Id] [int] NOT NULL,
[Scope_Cd] [int] NULL,
[UOM_Cd] [int] NULL,
[Frequency_Cd] [int] NOT NULL,
[Hier_Level_Cd] [int] NULL,
[Commodity_Service_Cd] [int] NOT NULL CONSTRAINT [DF__Client_Co__Commo__08C95AB2] DEFAULT ((0)),
[Allow_for_DE] [bit] NOT NULL CONSTRAINT [df_Client_Commodity__Allow_For_DE] DEFAULT ((1)),
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Client_Commodity__Last_Change_Ts] DEFAULT (getdate()),
[Owner_User_Id] [int] NULL,
[DE_Config_Last_Change_By_User_Id] [int] NULL,
[DE_Config_Last_Change_Ts] [datetime] NULL,
[DE_Config_Uom_Cd] [int] NULL,
[Allow_Utility_Map] [bit] NULL
) ON [DBData_CoreClient]
GO
ALTER TABLE [Core].[Client_Commodity] ADD CONSTRAINT [pk_Client_Commodity] PRIMARY KEY NONCLUSTERED  ([Client_Commodity_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [Core].[Client_Commodity] ADD CONSTRAINT [unc_Cleint_Commodity_Client_Id_Division_Id_Site_Id_Commodity_Id] UNIQUE CLUSTERED  ([Client_Id], [Commodity_Id], [Scope_Cd]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_Client_Commodity__Commodity_Id] ON [Core].[Client_Commodity] ([Commodity_Id]) INCLUDE ([Client_Commodity_Id], [Client_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [Core].[Client_Commodity] WITH NOCHECK ADD CONSTRAINT [fk_Client_Commodity_Client] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [Core].[Client_Commodity] WITH NOCHECK ADD CONSTRAINT [fk_Client_Commodity_Commodity] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Associates a client to a Commodity', 'SCHEMA', N'Core', 'TABLE', N'Client_Commodity', NULL, NULL
GO
