CREATE TABLE [dbo].[Service_Broker_Poison_Message]
(
[Queue_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message_Received_Ts] [datetime] NOT NULL,
[Conversation_Group_Id] [uniqueidentifier] NOT NULL,
[Message_Sequence_Number] [int] NOT NULL,
[Conversation_Handle] [uniqueidentifier] NOT NULL,
[SERVICE_NAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CONTRACT_NAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message_Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Error_text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MESSAGE_Body] [varbinary] (max) NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Holds the failed service broker messages', 'SCHEMA', N'dbo', 'TABLE', N'Service_Broker_Poison_Message', NULL, NULL
GO

ALTER TABLE [dbo].[Service_Broker_Poison_Message] ADD CONSTRAINT [pk_Service_Broker_Poison_Message] PRIMARY KEY CLUSTERED  ([Queue_Name], [Conversation_Group_Id], [Conversation_Handle], [Message_Sequence_Number]) ON [DB_DATA01]
GO
