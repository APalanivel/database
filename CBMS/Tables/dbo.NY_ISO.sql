CREATE TABLE [dbo].[NY_ISO]
(
[ZONE_ID] [int] NOT NULL,
[ZONE_NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZONE_SHEET_NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point
--when adding to this table
create trigger [dbo].[trg_ins_NY_Iso]
on [dbo].[NY_ISO]
for insert
as 
begin
set nocount on

	raiserror('Market Price table was not updated, please contact the help desk with this error from trg_ins_NY_Iso',16,1)
	rollback tran
--	insert into market_price_point (market_price_index_id,market_price_point,interval_type,market_price_point_short_name)
-- 	select market_price_index_id,zone_name+' '+pricing_type,'h',left(zone_name+' '+pricing_type,50)
-- 	from market_price_index a
-- 	inner join inserted b on a.market_price_index_name='ny_iso'
-- 	inner join 
-- 	(select distinct zone_id,
-- 	pricing_type from 
-- 	ny_iso_detail ) c on c.zone_id=b.zone_id
-- 

end

GO
ALTER TABLE [dbo].[NY_ISO] ADD CONSTRAINT [PK_NY_ISO] PRIMARY KEY CLUSTERED  ([ZONE_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'New York Independent System Operator (NYISO) - operations/dispatch of electric power in capital(Albany) region.  List of price zones', 'SCHEMA', N'dbo', 'TABLE', N'NY_ISO', NULL, NULL
GO
