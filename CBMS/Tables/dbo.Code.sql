CREATE TABLE [dbo].[Code]
(
[Code_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Codeset_Id] [int] NOT NULL,
[Code_Dsc] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code_Value] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Display_Seq] [int] NOT NULL CONSTRAINT [df_Code_DisPlay_Sq] DEFAULT ((0)),
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Code_Is_Active] DEFAULT ((1)),
[Is_Default] [bit] NULL CONSTRAINT [df_Code_Is_Default] DEFAULT ((0))
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', 'general codes used through the application', 'SCHEMA', N'dbo', 'TABLE', N'Code', NULL, NULL
GO

ALTER TABLE [dbo].[Code] ADD CONSTRAINT [unc_Code_Codeset_Id_Code_Value] UNIQUE CLUSTERED  ([Code_Value], [Codeset_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]

ALTER TABLE [dbo].[Code] ADD CONSTRAINT [pk_Code] PRIMARY KEY NONCLUSTERED  ([Code_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]

ALTER TABLE [dbo].[Code] ADD
CONSTRAINT [fk_Code_Codeset] FOREIGN KEY ([Codeset_Id]) REFERENCES [dbo].[Codeset] ([Codeset_Id])
GO
