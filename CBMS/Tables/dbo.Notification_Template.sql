CREATE TABLE [dbo].[Notification_Template]
(
[Notification_Template_Id] [int] NOT NULL IDENTITY(1, 1),
[Notification_Type_Cd] [int] NOT NULL,
[Notification_Category_Cd] [int] NOT NULL,
[Default_Email_Subject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Default_Email_Body] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display_Seq] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Notification_Template__Is_Active] DEFAULT ((1))
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Templates for system notifications (asof 7/2015, only RA login template is available)', 'SCHEMA', N'dbo', 'TABLE', N'Notification_Template', NULL, NULL
GO

ALTER TABLE [dbo].[Notification_Template] ADD CONSTRAINT [pk_Notification_Template] PRIMARY KEY CLUSTERED  ([Notification_Template_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Notification_Template__Notification_Type_Cd__Notification_Category_Cd] ON [dbo].[Notification_Template] ([Notification_Type_Cd], [Notification_Category_Cd]) ON [DB_DATA01]
GO
