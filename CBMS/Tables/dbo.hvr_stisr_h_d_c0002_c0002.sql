CREATE TABLE [dbo].[hvr_stisr_h_d_c0002_c0002]
(
[hvr_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_c__093EB4B8] DEFAULT (''),
[hvr_tx_seq] [binary] (24) NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_t__0A32D8F1] DEFAULT ((0x00)),
[hvr_tx_countdown] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_t__0B26FD2A] DEFAULT ((0)),
[integ_rows_done] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__0C1B2163] DEFAULT ((0)),
[integ_blocked] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__0D0F459C] DEFAULT ((0)),
[integ_unblocked] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__0E0369D5] DEFAULT ((0)),
[history_scan_start_tstamp] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__histo__0EF78E0E] DEFAULT ((0)),
[history_scan_start_addr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__histo__0FEBB247] DEFAULT (''),
[history_emit_seq] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__histo__10DFD680] DEFAULT ('')
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[hvr_stisr_h_d_c0002_c0002] ADD CONSTRAINT [PK__hvr_stis__47A3772707566C46] PRIMARY KEY NONCLUSTERED  ([hvr_cap_loc]) ON [DB_DATA01]
GO
