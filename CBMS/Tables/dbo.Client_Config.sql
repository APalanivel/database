CREATE TABLE [dbo].[Client_Config]
(
[Client_Config_ID] [int] NOT NULL IDENTITY(1, 1),
[Client_Config_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Config_Cd] [int] NOT NULL,
[Permission_Info_Id] [int] NULL,
[Is_Active] [bit] NOT NULL,
[Display_Sequence] [int] NOT NULL,
[Created_User_ID] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [Client_Config_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Changed_Ts] [datetime] NOT NULL CONSTRAINT [Client_Config_Last_Changed_Ts] DEFAULT (getdate()),
[Nav_Link] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Config] ADD CONSTRAINT [pk_Client_Config] PRIMARY KEY CLUSTERED  ([Client_Config_ID]) ON [DB_DATA01]
GO
