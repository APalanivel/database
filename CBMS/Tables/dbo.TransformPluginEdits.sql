CREATE TABLE [dbo].[TransformPluginEdits]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Timestamp] [datetime] NOT NULL CONSTRAINT [DF_TransformPluginEdits_Timestamp] DEFAULT (getutcdate()),
[EditedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[CorrelationId] [uniqueidentifier] NOT NULL,
[MappingPlugin_Id] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformPluginEdits] ADD CONSTRAINT [PK_dbo.TransformPluginEdits] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_MappingPlugin_Id] ON [dbo].[TransformPluginEdits] ([MappingPlugin_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformPluginEdits] ADD CONSTRAINT [FK_dbo.TransformPluginEdits_dbo.TransformPlugins_MappingPlugin_Id] FOREIGN KEY ([MappingPlugin_Id]) REFERENCES [dbo].[TransformPlugins] ([Id])
GO
