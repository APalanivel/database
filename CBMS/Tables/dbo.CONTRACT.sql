CREATE TABLE [dbo].[CONTRACT]
(
[CONTRACT_ID] [int] NOT NULL IDENTITY(20000, 1) NOT FOR REPLICATION,
[CBMS_IMAGE_ID] [int] NULL,
[RENEWAL_TYPE_ID] [int] NULL,
[CONTRACT_TYPE_ID] [int] NOT NULL,
[BASE_CONTRACT_ID] [int] NULL,
[COMMODITY_TYPE_ID] [int] NOT NULL,
[CURRENCY_UNIT_ID] [int] NOT NULL,
[CONTRACT_START_DATE] [datetime] NOT NULL,
[CONTRACT_END_DATE] [datetime] NOT NULL,
[ED_CONTRACT_NUMBER] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_CONTRACT_TRIGGER_RIGHTS] [bit] NULL,
[IS_CONTRACT_FULL_REQUIREMENT] [bit] NULL,
[NOTIFICATION_DAYS] [decimal] (12, 2) NULL,
[CONTRACT_COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTRACT_PRICING_SUMMARY] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTRACT_RECALC_TYPE_ID] [int] NULL,
[IS_ONLY_ONE_BASELOAD] [bit] NULL,
[IS_VOLUMES_NOT_REQUIRED] [bit] NULL,
[IS_VOLUMES_NOT_REQUIRED_CHANGED_USER_ID] [int] NULL,
[Contract_Classification_Cd] [int] NULL,
[Is_Contract_Heat_Rate] [bit] NOT NULL CONSTRAINT [DFCONTRACTIs_Contract_Heat_Rate] DEFAULT ((0)),
[Heat_Rate] [decimal] (28, 10) NULL,
[Retail_Adder] [decimal] (28, 10) NULL,
[Wires_Cost] [decimal] (28, 10) NULL,
[ED_CONTRACT_NUMBER_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([ED_CONTRACT_NUMBER]),(0))) PERSISTED,
[Contract_Product_Type_Cd] [int] NOT NULL CONSTRAINT [DFCONTRACTContract_Product_Type_Cd] DEFAULT ((-1)),
[Is_Notification_Required_On_Termination] [bit] NOT NULL CONSTRAINT [DFCONTRACTIs_Notification_Required_On_Termination] DEFAULT ((0)),
[Is_Notification_Required_On_Registration_Validation] [bit] NOT NULL CONSTRAINT [DFCONTRACTIs_Notification_Required_On_Registration_Validation] DEFAULT ((0)),
[Is_Published_To_EC] [bit] NOT NULL CONSTRAINT [DFCONTRACTIs_Published_To_EC] DEFAULT ((0)),
[Published_To_EC_By_User_Id] [int] NULL,
[Contract_Volume_Frequency_Cd] [int] NULL,
[Advance_Termination_Notofication_Days] [smallint] NULL CONSTRAINT [DFCONTRACTAdvance_Termination_Notofication_Days] DEFAULT ((0)),
[SR_SUPPLIER_CONTACT_INFO_ID] [int] NULL,
[Contract_Created_Ts] [datetime] NOT NULL CONSTRAINT [df_CONTRACT__Contract_Created_Ts] DEFAULT (getdate()),
[Service_Desk_Ticket_XId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Contract_Admin_User_Info_Id] [int] NULL,
[Original_Contract_End_Date] [date] NULL,
[Contract_Meter_Volume_Source_Cd] [int] NULL,
[Contract_Meter_Volume_Type_Cd] [int] NULL
) ON [DBData_Contract_RateLibrary]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Contract_Info_Transfer

DESCRIPTION: Trigger on Contract to send change data to the 
	Change control service. 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV         Ravi Kumar Vegesna	
	NR			Narayana Reddy
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV			2015-12-14	Created
	HG			2017-01-13	MAINT-4789, state name column added to the meter message
	NR			2017-09-13	SE2017-251 - Excluded the Utility Contract types.
	HG			2019-07-25	SE2017-733 Sending ACT Commodity name in place of CBMS Commodity name
	RR			2020-04-27	SE2017-688 Added timeOfDay_Id(Bucket_Master_Id) and timeOfDay(Bucket_Name)
******/
CREATE TRIGGER [dbo].[tr_Contract_Info_Transfer]
ON [dbo].[CONTRACT]
FOR UPDATE, DELETE
AS
    BEGIN

        DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
            , @Message_Contract XML
            , @Message_Contract_Meter XML
            , @Message_Contract_Meter_Volume XML
            , @From_Service NVARCHAR(255)
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255)
            , @Is_Published_To_EC_Old BIT
            , @Is_Published_To_EC_New BIT
            , @Contract_id INT
            , @Sys_Change_Operation CHAR(1)
            , @Message_Contract_Meter_Volume_Dtl XML;


        DECLARE @Contract_Type_Ids VARCHAR(MAX);

        DECLARE @Contract_Type_Id_List TABLE
              (
                  Contract_Type_Id INT
              );

        SELECT
            @Contract_Type_Ids = ac.App_Config_Value
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Exclude_Contract_Type_Id_List'
            AND ac.Is_Active = 1;

        INSERT INTO @Contract_Type_Id_List
             (
                 Contract_Type_Id
             )
        SELECT  us.Segments FROM    dbo.ufn_split(@Contract_Type_Ids, ',') us;



        SET @Message_Contract = (   SELECT
                                        CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                  AND   del.CONTRACT_ID IS NOT NULL THEN 'U'
                                            WHEN ins.CONTRACT_ID IS NULL
                                                 AND del.CONTRACT_ID IS NOT NULL THEN 'D'
                                        END AS Sys_Change_Operation
                                        , ISNULL(ins.CONTRACT_ID, del.CONTRACT_ID) AS Contract_Id
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN
                                                   ISNULL(act_com.ACT_Commodity_XName, c.Commodity_Name)
                                              ELSE NULL
                                          END AS Commodity_Name
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN cu.CURRENCY_UNIT_NAME
                                              ELSE NULL
                                          END AS Currency_Name
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.CONTRACT_START_DATE
                                              ELSE NULL
                                          END AS Contract_Start_Dt
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.CONTRACT_END_DATE
                                              ELSE NULL
                                          END AS Contract_End_Dt
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.ED_CONTRACT_NUMBER
                                              ELSE NULL
                                          END AS Ed_Contract_Number
                                        , @@SERVERNAME AS Server_Name
                                    FROM
                                        INSERTED ins
                                        FULL OUTER JOIN DELETED del
                                            ON del.CONTRACT_ID = ins.CONTRACT_ID
                                        INNER JOIN dbo.Commodity c
                                            ON (c.Commodity_Id = ISNULL(ins.COMMODITY_TYPE_ID, del.COMMODITY_TYPE_ID))
                                        LEFT OUTER JOIN(dbo.Commodity_ACT_Commodity_Map com_act_map
                                                        INNER JOIN dbo.ACT_Commodity act_com
                                                            ON act_com.ACT_Commodity_Id = com_act_map.ACT_Commodity_Id)
                                            ON com_act_map.Commodity_Id = c.Commodity_Id
                                        INNER JOIN dbo.CURRENCY_UNIT cu
                                            ON (cu.CURRENCY_UNIT_ID = ISNULL(ins.CURRENCY_UNIT_ID, del.CURRENCY_UNIT_ID))
                                        INNER JOIN dbo.CONTRACT con
                                            ON (con.CONTRACT_ID = ISNULL(ins.CONTRACT_ID, del.CONTRACT_ID))
                                    WHERE
                                        (   ins.Is_Published_To_EC = 1
                                            OR  (   del.Is_Published_To_EC = 1
                                                    AND ins.CONTRACT_ID IS NULL))
                                        AND NOT EXISTS (   SELECT
                                                                1
                                                           FROM
                                                                @Contract_Type_Id_List ctd
                                                           WHERE
                                                                con.CONTRACT_TYPE_ID = ctd.Contract_Type_Id)
                                    GROUP BY
                                        CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                  AND   del.CONTRACT_ID IS NOT NULL THEN 'U'
                                            WHEN ins.CONTRACT_ID IS NULL
                                                 AND del.CONTRACT_ID IS NOT NULL THEN 'D'
                                        END
                                        , ISNULL(ins.CONTRACT_ID, del.CONTRACT_ID)
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN
                                                   ISNULL(act_com.ACT_Commodity_XName, c.Commodity_Name)
                                              ELSE NULL
                                          END
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN cu.CURRENCY_UNIT_NAME
                                              ELSE NULL
                                          END
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.CONTRACT_START_DATE
                                              ELSE NULL
                                          END
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.CONTRACT_END_DATE
                                              ELSE NULL
                                          END
                                        , CASE WHEN ins.CONTRACT_ID IS NOT NULL
                                                    AND del.CONTRACT_ID IS NOT NULL THEN ins.ED_CONTRACT_NUMBER
                                              ELSE NULL
                                          END
                                    FOR XML PATH('Contract_Change'), ELEMENTS, ROOT('Contract_Changes'));


        IF @Message_Contract IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                -- Cycle through the targets in Change_Control Targt and sent the message to each
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Contract');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Header_Change]
                            (@Message_Contract);



                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;

        SET @Message_Contract_Meter = (   SELECT
                                                'U' AS Sys_Change_Operation
                                                , ins.CONTRACT_ID AS Contract_Id
                                                , samm.METER_ID AS Meter_Id
                                                , cha.Meter_Number AS Meter_Number
                                                , samm.METER_ASSOCIATION_DATE AS Meter_Association_Dt
                                                , samm.METER_DISASSOCIATION_DATE AS Meter_Disassociation_Dt
                                                , cha.Meter_Country_Name AS Country_Name
                                                , cha.Meter_State_Name AS State_Name
                                                , ISNULL(act_com.ACT_Commodity_XName, com.Commodity_Name) AS Commodity_Name
                                                , @@SERVERNAME AS Server_Name
                                          FROM
                                                dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                                INNER JOIN Core.Client_Hier_Account cha
                                                    ON samm.METER_ID = cha.Meter_Id
                                                INNER JOIN dbo.Commodity com
                                                    ON cha.Commodity_Id = com.Commodity_Id
                                                INNER JOIN INSERTED ins
                                                    ON ins.CONTRACT_ID = samm.Contract_ID
                                                FULL OUTER JOIN DELETED del
                                                    ON del.CONTRACT_ID = ins.CONTRACT_ID
                                                INNER JOIN dbo.CONTRACT c
                                                    ON (c.CONTRACT_ID = ISNULL(ins.CONTRACT_ID, del.CONTRACT_ID))
                                                LEFT OUTER JOIN(dbo.Commodity_ACT_Commodity_Map com_act_map
                                                                INNER JOIN dbo.ACT_Commodity act_com
                                                                    ON act_com.ACT_Commodity_Id = com_act_map.ACT_Commodity_Id)
                                                    ON com_act_map.Commodity_Id = com.Commodity_Id
                                          WHERE
                                                ins.Is_Published_To_EC = 1
                                                AND del.Is_Published_To_EC = 0
                                                AND ins.CONTRACT_ID IS NOT NULL
                                                AND del.CONTRACT_ID IS NOT NULL
                                                AND NOT EXISTS (   SELECT
                                                                        1
                                                                   FROM
                                                                        @Contract_Type_Id_List ctd
                                                                   WHERE
                                                                        c.CONTRACT_TYPE_ID = ctd.Contract_Type_Id)
                                          GROUP BY
                                              ins.CONTRACT_ID
                                              , samm.METER_ID
                                              , cha.Meter_Number
                                              , samm.METER_ASSOCIATION_DATE
                                              , samm.METER_DISASSOCIATION_DATE
                                              , cha.Meter_Country_Name
                                              , cha.Meter_State_Name
                                              , ISNULL(act_com.ACT_Commodity_XName, com.Commodity_Name)
                                          FOR XML PATH('Contract_Meter_Change'), ELEMENTS, ROOT('Contract_Meter_Changes'));

        SET @Message_Contract_Meter_Volume = (   SELECT
                                                        'U' AS Sys_Change_Operation
                                                        , cmv.CONTRACT_METER_VOLUME_ID AS Contract_Meter_Volume_Id
                                                        , cmv.CONTRACT_ID AS Contract_Id
                                                        , cmv.METER_ID AS Meter_Id
                                                        , cmv.VOLUME AS Volume
                                                        , '' AS timeOfDay_Id
                                                        , '' AS timeOfDay
                                                        , cmv.MONTH_IDENTIFIER AS Month
                                                        , e.ENTITY_NAME UOM
                                                        , cmv.UNIT_TYPE_ID Uom_Id
                                                        , @@SERVERNAME AS Server_Name
                                                        , e1.ENTITY_NAME Meter_Volume_Frequency_Type
                                                        , cmv.FREQUENCY_TYPE_ID Meter_Volume_Frequency_Type_Id
                                                 FROM
                                                        dbo.CONTRACT_METER_VOLUME cmv
                                                        INNER JOIN dbo.ENTITY e
                                                            ON e.ENTITY_ID = cmv.UNIT_TYPE_ID
                                                        INNER JOIN INSERTED ins
                                                            ON ins.CONTRACT_ID = cmv.CONTRACT_ID
                                                        FULL OUTER JOIN DELETED del
                                                            ON del.CONTRACT_ID = ins.CONTRACT_ID
                                                        INNER JOIN dbo.CONTRACT c
                                                            ON c.CONTRACT_ID = cmv.CONTRACT_ID
                                                        INNER JOIN dbo.ENTITY e1
                                                            ON e1.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
                                                 WHERE
                                                     ins.Is_Published_To_EC = 1
                                                     AND del.Is_Published_To_EC = 0
                                                     AND ins.CONTRACT_ID IS NOT NULL
                                                     AND del.CONTRACT_ID IS NOT NULL
                                                     AND NOT EXISTS (   SELECT
                                                                            1
                                                                        FROM
                                                                            @Contract_Type_Id_List ctd
                                                                        WHERE
                                                                            c.CONTRACT_TYPE_ID = ctd.Contract_Type_Id)
                                                     AND NOT EXISTS (   SELECT
                                                                            1
                                                                        FROM
                                                                            dbo.Code cd
                                                                            INNER JOIN dbo.Codeset cs
                                                                                ON cs.Codeset_Id = cd.Codeset_Id
                                                                        WHERE
                                                                            cs.Codeset_Name = 'Contractmetervolumetype'
                                                                            AND cd.Code_Value = 'On/Off Peak'
                                                                            AND cd.Code_Id = c.Contract_Meter_Volume_Type_Cd)
                                                 /*Total volumes meesage(dbo.CONTRACT_METER_VOLUME data) should not send if the volume format is On/Off Peak*/
                                                 GROUP BY
                                                     cmv.CONTRACT_METER_VOLUME_ID
                                                     , cmv.CONTRACT_ID
                                                     , cmv.METER_ID
                                                     , cmv.VOLUME
                                                     , cmv.MONTH_IDENTIFIER
                                                     , e.ENTITY_NAME
                                                     , cmv.UNIT_TYPE_ID
                                                     , e1.ENTITY_NAME
                                                     , cmv.FREQUENCY_TYPE_ID
                                                 FOR XML PATH('Contract_Meter_Volume_Change'), ELEMENTS, ROOT('Contract_Meter_Volume_Changes'));

        IF @Message_Contract_Meter IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                -- Cycle through the targets in Change_Control Targt and sent the message to each
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Supplier_Account_Meter_Map');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Change]
                            (@Message_Contract_Meter);



                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;

        IF @Message_Contract_Meter_Volume IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Contract_Meter_Volume');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Volume_Change]
                            (@Message_Contract_Meter_Volume);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;

        SET @Message_Contract_Meter_Volume_Dtl = (   SELECT
                                                            'U' AS Sys_Change_Operation
                                                            , cmv.CONTRACT_METER_VOLUME_ID AS Contract_Meter_Volume_Id
                                                            , cmv.CONTRACT_ID AS Contract_Id
                                                            , cmv.METER_ID AS Meter_Id
                                                            , cmvd.Volume AS Volume
                                                            , cmvd.Bucket_Master_Id AS timeOfDay_Id
                                                            , bm.Bucket_Name AS timeOfDay
                                                            , cmv.MONTH_IDENTIFIER AS Month
                                                            , e.ENTITY_NAME UOM
                                                            , cmv.UNIT_TYPE_ID Uom_Id
                                                            , @@SERVERNAME AS Server_Name
                                                            , e1.ENTITY_NAME Meter_Volume_Frequency_Type
                                                            , cmv.FREQUENCY_TYPE_ID Meter_Volume_Frequency_Type_Id
                                                     FROM
                                                            dbo.Contract_Meter_Volume_Dtl cmvd
                                                            INNER JOIN dbo.CONTRACT_METER_VOLUME cmv
                                                                ON cmv.CONTRACT_METER_VOLUME_ID = cmvd.CONTRACT_METER_VOLUME_ID
                                                            INNER JOIN dbo.ENTITY e
                                                                ON e.ENTITY_ID = cmv.UNIT_TYPE_ID
                                                            INNER JOIN INSERTED ins
                                                                ON ins.CONTRACT_ID = cmv.CONTRACT_ID
                                                            FULL OUTER JOIN DELETED del
                                                                ON del.CONTRACT_ID = ins.CONTRACT_ID
                                                            INNER JOIN dbo.CONTRACT c
                                                                ON c.CONTRACT_ID = cmv.CONTRACT_ID
                                                            INNER JOIN dbo.ENTITY e1
                                                                ON e1.ENTITY_ID = cmv.FREQUENCY_TYPE_ID
                                                            INNER JOIN dbo.Bucket_Master bm
                                                                ON bm.Bucket_Master_Id = cmvd.Bucket_Master_Id
                                                     WHERE
                                                         ins.Is_Published_To_EC = 1
                                                         AND del.Is_Published_To_EC = 0
                                                         AND ins.CONTRACT_ID IS NOT NULL
                                                         AND del.CONTRACT_ID IS NOT NULL
                                                         AND NOT EXISTS (   SELECT
                                                                                1
                                                                            FROM
                                                                                @Contract_Type_Id_List ctd
                                                                            WHERE
                                                                                c.CONTRACT_TYPE_ID = ctd.Contract_Type_Id)
                                                     GROUP BY
                                                         cmv.CONTRACT_METER_VOLUME_ID
                                                         , cmv.CONTRACT_ID
                                                         , cmv.METER_ID
                                                         , cmvd.Volume
                                                         , cmv.MONTH_IDENTIFIER
                                                         , e.ENTITY_NAME
                                                         , cmv.UNIT_TYPE_ID
                                                         , e1.ENTITY_NAME
                                                         , cmv.FREQUENCY_TYPE_ID
                                                         , cmvd.Bucket_Master_Id
                                                         , bm.Bucket_Name
                                                     FOR XML PATH('Contract_Meter_Volume_Change'), ELEMENTS, ROOT('Contract_Meter_Volume_Changes'));

        IF @Message_Contract_Meter_Volume_Dtl IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Contract_Meter_Volume');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Volume_Change]
                            (@Message_Contract_Meter_Volume_Dtl);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;

    END;

GO
ALTER TABLE [dbo].[CONTRACT] ADD CONSTRAINT [CONTRACT_END_DATE] CHECK (([CONTRACT_END_DATE]>=[CONTRACT_START_DATE]))
GO
ALTER TABLE [dbo].[CONTRACT] ADD CONSTRAINT [pk_CONTRACT] PRIMARY KEY CLUSTERED  ([CONTRACT_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [ix_CONTRACT__COMMODITY_TYPE_ID] ON [dbo].[CONTRACT] ([COMMODITY_TYPE_ID]) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [ix_CONTRACT__CONTRACT_END_DATE__INCL] ON [dbo].[CONTRACT] ([CONTRACT_END_DATE]) INCLUDE ([CONTRACT_ID], [CONTRACT_TYPE_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [ix_Contract_CONTRACT_ID__CONTRACT_END_DATE__ED_CONTRACT_NUMBER__COMMODITY_TYPE_ID] ON [dbo].[CONTRACT] ([CONTRACT_ID], [CONTRACT_END_DATE], [ED_CONTRACT_NUMBER], [COMMODITY_TYPE_ID]) INCLUDE ([CONTRACT_PRICING_SUMMARY], [CONTRACT_START_DATE]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_CONTRACT__CONTRACT_START_DATE__CONTRACT_END_DATE__ED_CONTRACT_NUMBER__CONTRACT_ID] ON [dbo].[CONTRACT] ([CONTRACT_START_DATE], [CONTRACT_END_DATE], [ED_CONTRACT_NUMBER], [CONTRACT_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [ix_CONTRACT__CONTRACT_TYPE_ID__COMMODITY_TYPE_ID] ON [dbo].[CONTRACT] ([CONTRACT_TYPE_ID], [COMMODITY_TYPE_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [ix_CONTRACT__ED_CONTRACT_NUMBER] ON [dbo].[CONTRACT] ([ED_CONTRACT_NUMBER]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
ALTER TABLE [dbo].[CONTRACT] ADD CONSTRAINT [CURRENCY_UNIT_CONTRACT_FK] FOREIGN KEY ([CURRENCY_UNIT_ID]) REFERENCES [dbo].[CURRENCY_UNIT] ([CURRENCY_UNIT_ID])
GO
ALTER TABLE [dbo].[CONTRACT] ADD CONSTRAINT [fk_Commodity_Contract] FOREIGN KEY ([COMMODITY_TYPE_ID]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
ALTER TABLE [dbo].[CONTRACT] WITH NOCHECK ADD CONSTRAINT [IS_VOLUMES_NOT_REQUIRED_CHANGED_USER_ID_FK] FOREIGN KEY ([IS_VOLUMES_NOT_REQUIRED_CHANGED_USER_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', 'Shell of the contract(start, end, basic terms) - utility or supplier agreements', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT', NULL, NULL
GO
CREATE FULLTEXT INDEX ON [dbo].[CONTRACT] KEY INDEX [pk_CONTRACT] ON [CBMS_Contract_FTS]
GO
ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ADD ([ED_CONTRACT_NUMBER] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ADD ([ED_CONTRACT_NUMBER_FTSearch] LANGUAGE 1042)
GO
CREATE FULLTEXT INDEX ON [dbo].[CONTRACT] KEY INDEX [pk_CONTRACT] ON [CBMS_Contract_FTS]
GO
ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ADD ([ED_CONTRACT_NUMBER] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ADD ([ED_CONTRACT_NUMBER_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ENABLE
GO

ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ENABLE
GO


ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ENABLE
GO


ALTER FULLTEXT INDEX ON [dbo].[CONTRACT] ENABLE
GO
