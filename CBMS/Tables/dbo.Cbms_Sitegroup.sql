CREATE TABLE [dbo].[Cbms_Sitegroup]
(
[Cbms_Sitegroup_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Module_Cd] [int] NOT NULL,
[Group_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Id] [int] NULL,
[Group_Type_Cd] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFCbms_SitegroupCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFCbms_SitegroupLast_Change_Ts] DEFAULT (getdate()),
[Last_Refresh_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup] ADD CONSTRAINT [pk_Cbms_Sitegroup] PRIMARY KEY CLUSTERED  ([Cbms_Sitegroup_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup] ADD CONSTRAINT [un_Cbms_Sitegroup__Client_Id__Module_Cd__Group_Name] UNIQUE NONCLUSTERED  ([Client_Id], [Module_Cd], [Group_Name]) ON [DB_DATA01]
GO
