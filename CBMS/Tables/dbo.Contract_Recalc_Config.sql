CREATE TABLE [dbo].[Contract_Recalc_Config]
(
[Contract_Recalc_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[Supplier_Recalc_Type_Cd] [int] NOT NULL,
[Determinant_Source_Cd] [int] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Recalc_Config__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Recalc_Config__Last_Change_Ts] DEFAULT (getdate()),
[Is_Consolidated_Contract_Config] [bit] NOT NULL CONSTRAINT [df_Contract_Recalc_Config__Is_Consolidated_Contract_Config] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Recalc_Config] ADD CONSTRAINT [PK_Contract_Recalc_Config_Id] PRIMARY KEY CLUSTERED  ([Contract_Recalc_Config_Id]) ON [DB_DATA01]
GO
