CREATE TABLE [dbo].[Invoice_Data_Quality_Log]
(
[Invoice_Data_Quality_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Id] [int] NOT NULL,
[Account_ID] [int] NULL,
[Commodity_ID] [int] NULL,
[Service_Month] [date] NULL,
[UOM_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Baseline_UOM_Id] [int] NULL,
[Test_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Category] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Execution_User_Info_Id] [int] NOT NULL,
[Execution_Ts] [datetime] NOT NULL,
[Execution_Group_No] [int] NOT NULL,
[Is_Failure] [bit] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Log__Is_Failure] DEFAULT ((0)),
[Is_Closed] [bit] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Log__Is_Closed] DEFAULT ((0)),
[Current_Value] [decimal] (16, 4) NULL,
[Baseline_Value] [decimal] (16, 4) NULL,
[Is_Inclusive] [bit] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Log__Is_Inclusive] DEFAULT ((0)),
[Is_Multiple_Condition] [bit] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Log__Is_Multiple_Condition] DEFAULT ((0)),
[Baseline_Desc] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Baseline_Start_Service_Month] [date] NULL,
[Baseline_End_Service_Month] [date] NULL,
[Partition_Key] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Log] ADD CONSTRAINT [pk_Invoice_Data_Quality_Log] PRIMARY KEY CLUSTERED  ([Invoice_Data_Quality_Log_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Data_Quality_Log__Account_ID__Commodity_ID__Service_Month] ON [dbo].[Invoice_Data_Quality_Log] ([Account_ID], [Commodity_ID], [Service_Month]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Data_Quality_Log__Cu_Invoice_Id] ON [dbo].[Invoice_Data_Quality_Log] ([Cu_Invoice_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Data_Quality_Log__Partition_Key] ON [dbo].[Invoice_Data_Quality_Log] ([Partition_Key]) ON [DB_DATA01]
GO
