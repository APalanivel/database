CREATE TABLE [dbo].[Service_Broker_Message_Procedure]
(
[Queue_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message_Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PROCEDURE_NAME] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Has the list of stored procedures to be called when the particular service broker message type is received', 'SCHEMA', N'dbo', 'TABLE', N'Service_Broker_Message_Procedure', NULL, NULL
GO

ALTER TABLE [dbo].[Service_Broker_Message_Procedure] ADD CONSTRAINT [pk_Service_Broker_Message_Procedure] PRIMARY KEY CLUSTERED  ([Queue_Name], [Message_Type]) ON [DB_DATA01]
GO
