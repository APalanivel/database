CREATE TABLE [dbo].[Contract_Comment]
(
[Contract_Comment_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Comment] ADD CONSTRAINT [pk_Contract_Comment] PRIMARY KEY CLUSTERED  ([Contract_Comment_Id]) ON [DB_DATA01]
GO
