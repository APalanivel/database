CREATE TABLE [Trade].[Deal_Ticket_Client_Hier]
(
[Deal_Ticket_Client_Hier_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier] ADD CONSTRAINT [pk_Deal_Ticket_Client_Hier] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Hier_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier] ADD CONSTRAINT [un_Deal_Ticket_Client_Hier__Deal_Ticket_Id__Client_Hier_Id] UNIQUE NONCLUSTERED  ([Deal_Ticket_Id], [Client_Hier_Id]) ON [DB_DATA01]
GO
