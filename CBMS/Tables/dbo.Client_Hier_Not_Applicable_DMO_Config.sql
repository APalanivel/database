CREATE TABLE [dbo].[Client_Hier_Not_Applicable_DMO_Config]
(
[Client_Hier_Id] [int] NOT NULL,
[Client_Hier_DMO_Config_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Hier_Not_Applicable_DMO_Config__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_Not_Applicable_DMO_Config] ADD CONSTRAINT [pk_Client_Hier_Not_Applicable_DMO_Config] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Client_Hier_DMO_Config_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Not_Applicable_DMO_Config__Client_Hier_DMO_Config_Id] ON [dbo].[Client_Hier_Not_Applicable_DMO_Config] ([Client_Hier_DMO_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_Not_Applicable_DMO_Config] ADD CONSTRAINT [fk_Client_Hier_DMO_Config__Client_Hier_Not_Applicable_DMO_Config] FOREIGN KEY ([Client_Hier_DMO_Config_Id]) REFERENCES [dbo].[Client_Hier_DMO_Config] ([Client_Hier_DMO_Config_Id])
GO
