CREATE TABLE [dbo].[Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map]
(
[Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Ubm_Bucket_Charge_Map_Id] [int] NOT NULL,
[State_Id] [int] NOT NULL,
[Ubm_Sub_Bucket_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NOT NULL,
[Created_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map] ADD CONSTRAINT [pk_Ubm_Bucket_Charge_Map_Id__State_Id] PRIMARY KEY CLUSTERED  ([Ubm_Bucket_Charge_Map_Id], [Ubm_Sub_Bucket_Code], [State_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map] ADD CONSTRAINT [fk_Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map__EC_Invoice_Sub_Bucket_Master_Id] FOREIGN KEY ([EC_Invoice_Sub_Bucket_Master_Id]) REFERENCES [dbo].[EC_Invoice_Sub_Bucket_Master] ([EC_Invoice_Sub_Bucket_Master_Id])
GO
ALTER TABLE [dbo].[Ubm_Bucket_Charge_Ec_Invoice_Sub_Bucket_Map] ADD CONSTRAINT [fk_Ubm_Bucket_Charge_Map__Ubm_Bucket_Charge_Map_Id] FOREIGN KEY ([Ubm_Bucket_Charge_Map_Id]) REFERENCES [dbo].[UBM_BUCKET_CHARGE_MAP] ([UBM_BUCKET_CHARGE_MAP_ID])
GO
