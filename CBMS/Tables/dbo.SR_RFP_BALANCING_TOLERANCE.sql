CREATE TABLE [dbo].[SR_RFP_BALANCING_TOLERANCE]
(
[SR_RFP_BALANCING_TOLERANCE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IS_PRICE_VARYING] [bit] NULL,
[VOLUME_TOLERANCE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ABOVE_TOLERANCE_COMMENTS] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BELOW_TOLERANCE_COMMENTS] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BALANCING_FREQUENCY_TYPE_ID] [int] NULL,
[TOLERANCE_COMMENTS] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rowguid] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__SR_RFP_BA__rowgu__46EF13AF] DEFAULT (newsequentialid())
) ON [DBData_Sourcing] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create trigger [MSmerge_del_72F54B6000734EC9BCC3E0920C3FE24F] on [CBMS].[dbo].[SR_RFP_BALANCING_TOLERANCE] FOR DELETE   AS 
    declare @is_mergeagent bit, @at_publisher bit, @retcode smallint 

    set rowcount 0
    set transaction isolation level read committed

            select @is_mergeagent = convert(bit, sessionproperty('replication_agent'))
            select @at_publisher = 0 
    if (select trigger_nestlevel()) = 1 and @is_mergeagent = 1
        return 
    declare @article_rows_deleted int
    select @article_rows_deleted = count(*) from deleted
    if @article_rows_deleted=0
        return
    declare @tablenick int, @replnick binary(6), 
            @lineage varbinary(311), @newgen bigint, @oldmaxversion int, @child_newgen bigint, 
            @child_oldmaxversion int, @child_metadatarows_updated int, @cv varbinary(1),
            @logical_record_parent_oldmaxversion int, @logical_record_lineage varbinary(311), @logical_record_parent_regular_lineage varbinary(311), @logical_record_parent_gencur bigint,
            @num_parent_rows int, @logical_record_parent_rowguid uniqueidentifier, @parent_row_inserted bit, @rowguid uniqueidentifier 
    declare @dt datetime, @nickbin varbinary(8), @error int
     
    set nocount on
    select @tablenick = 10519000   
    if @article_rows_deleted = 1 select @rowguid = rowguidcol from deleted
    select @oldmaxversion= maxversion_at_cleanup from dbo.sysmergearticles where nickname = @tablenick
    select @dt = getdate()

    select @replnick = 0x00cb5738a6d7
    set @nickbin= @replnick + 0xFF

    select @newgen = NULL
        select top 1 @newgen = generation from [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock, updlock, readpast) 
        where art_nick = 10519000     and genstatus = 0    
        
            and  changecount <= (1000 - isnull(@article_rows_deleted,0))
    if @newgen is NULL
    begin
        insert into [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F]  with (rowlock)
            (guidsrc, genstatus, art_nick, nicknames, coldate, changecount)
               values (newid(), 0, @tablenick, @nickbin, @dt, @article_rows_deleted)
        select @error = @@error, @newgen = @@identity    
        if @error<>0 or @newgen is NULL
            goto FAILURE
    end
    else
    begin
        -- now update the changecount of the generation we go to reflect the number of rows we put in this generation
        update [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F]  with (rowlock)
            set changecount = changecount + @article_rows_deleted
            where generation = @newgen
        if @@error<>0 goto FAILURE
    end
  
    set @lineage = { fn UPDATELINEAGE(0x0, @replnick, @oldmaxversion+1) }  
    if @article_rows_deleted = 1
        insert into [dbo].[MSmerge_tsvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock) (rowguid, tablenick, type, lineage, generation)
            select @rowguid, @tablenick, 1, isnull((select { fn UPDATELINEAGE(COALESCE(c.lineage, @lineage), @replnick, @oldmaxversion+1) } from 
            [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F] c with (rowlock) where c.tablenick = @tablenick and c.rowguid = @rowguid),@lineage), @newgen
    else
        insert into [dbo].[MSmerge_tsvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock) (rowguid, tablenick, type, lineage, generation)
            select d.rowguidcol, @tablenick, 1, { fn UPDATELINEAGE(COALESCE(c.lineage, @lineage), @replnick, @oldmaxversion+1) }, @newgen from 
            deleted d left outer join [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F] c with (rowlock) on c.tablenick = @tablenick and c.rowguid = d.rowguidcol 
             
    if @@error <> 0
        GOTO FAILURE  
        delete [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F]  with (rowlock)
        from deleted d, [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F] cont with (rowlock)
        where cont.tablenick = @tablenick and cont.rowguid = d.rowguidcol
        option (force order, loop join)

    if @@error <> 0
        GOTO FAILURE

    
    return
FAILURE:
    if @@trancount > 0
        rollback tran
    raiserror (20041, 16, -1)
    return  
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create trigger [MSmerge_ins_72F54B6000734EC9BCC3E0920C3FE24F] on [CBMS].[dbo].[SR_RFP_BALANCING_TOLERANCE] for insert   as 
    declare @is_mergeagent bit, @at_publisher bit, @retcode smallint 

    set rowcount 0
    set transaction isolation level read committed

        select @is_mergeagent = convert(bit, sessionproperty('replication_agent'))
        select @at_publisher = 0 
    if (select trigger_nestlevel()) = 1 and @is_mergeagent = 1
        return  
    declare @article_rows_inserted int
    select @article_rows_inserted =  count(*) from inserted 
    if @article_rows_inserted = 0 
        return
    declare @tablenick int, @rowguid uniqueidentifier
    , @replnick binary(6), @lineage varbinary(311), @colv1 varbinary(1), @cv varbinary(1)
    , @ccols int, @newgen bigint, @version int, @curversion int
    , @oldmaxversion int, @child_newgen bigint, @child_oldmaxversion int, @child_metadatarows_updated int 
    , @logical_record_parent_rowguid uniqueidentifier 
    , @num_parent_rows int, @parent_row_inserted bit, @ts_rows_exist bit, @marker uniqueidentifier 
    declare @dt datetime
    declare @nickbin varbinary(8)
    declare @error int 
    set nocount on
    set @tablenick = 10519000   
    set @lineage = 0x0
    set @retcode = 0
    select @oldmaxversion= maxversion_at_cleanup from dbo.sysmergearticles where nickname = @tablenick
    select @dt = getdate()

    select @replnick = 0x00cb5738a6d7 
    set @nickbin= @replnick + 0xFF

    select @newgen = NULL
        select top 1 @newgen = generation from [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock, updlock, readpast) 
        where art_nick = 10519000    and genstatus = 0
            and  changecount <= (1000 - isnull(@article_rows_inserted,0))
    if @newgen is NULL
    begin
        insert into [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock)
            (guidsrc, genstatus, art_nick, nicknames, coldate, changecount)
             values   (newid(), 0, @tablenick, @nickbin, @dt, @article_rows_inserted)
        select @error = @@error, @newgen = @@identity    
        if @error<>0 or @newgen is NULL
            goto FAILURE
    end
    else
    begin
        -- now update the changecount of the generation we go to reflect the number of rows we put in this generation
        update [dbo].[MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F]  with (rowlock)
            set changecount = changecount + @article_rows_inserted
            where generation = @newgen
        if @@error<>0 goto FAILURE
    end
    set @lineage = { fn UPDATELINEAGE (0x0, @replnick, 1) }
            set @colv1 = NULL
    if (@@error <> 0)
    begin
        goto FAILURE
    end

    select @ts_rows_exist = 0 
        select @ts_rows_exist = 1 where exists (select ts.rowguid from inserted i, [dbo].[MSmerge_tsvw_72F54B6000734EC9BCC3E0920C3FE24F] ts with (rowlock) where ts.tablenick = @tablenick and ts.rowguid = i.rowguidcol)     
    if @ts_rows_exist = 1
    begin    
        select @version = max({fn GETMAXVERSION(lineage)}) from [dbo].[MSmerge_tsvw_72F54B6000734EC9BCC3E0920C3FE24F] where 
            tablenick = @tablenick and rowguid in (select rowguidcol from inserted) 

        if @version is not null
        begin
            -- reset lineage and colv to higher version...
            set @curversion = 0
            while (@curversion <= @version)
            begin
                set @lineage = { fn UPDATELINEAGE (@lineage, @replnick, @oldmaxversion+1) }
                set @curversion= { fn GETMAXVERSION(@lineage) }
            end

            if (@colv1 IS NOT NULL)
                set @colv1 = { fn UPDATECOLVBM(@colv1, @replnick, 0x01, 0x00, { fn GETMAXVERSION(@lineage) }) }    
                delete from [dbo].[MSmerge_tsvw_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock) where tablenick = @tablenick and rowguid in
                    (select rowguidcol from inserted) 
        end
    end 
    select @marker = newid()  
        insert into [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F] with (rowlock) (tablenick, rowguid, lineage, colv1, generation, partchangegen, marker) 
        select @tablenick, rowguidcol, @lineage, @colv1, @newgen, (-@newgen), @marker
        from inserted i where not exists
        (select rowguid from [dbo].[MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F] with (readcommitted, rowlock, readpast) where tablenick = @tablenick and rowguid = i.rowguidcol)  
    if @@error <> 0 
        goto FAILURE   

    return
FAILURE:
    if @@trancount > 0
        rollback tran
    raiserror (20041, 16, -1)
    return     
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create trigger MSmerge_upd_72F54B6000734EC9BCC3E0920C3FE24F on [CBMS].[dbo].[SR_RFP_BALANCING_TOLERANCE] FOR UPDATE   AS 
    declare @is_mergeagent bit, @at_publisher bit, @retcode int 

    set rowcount 0
    set transaction isolation level read committed

        select @is_mergeagent = convert(bit, sessionproperty('replication_agent'))
        select @at_publisher = 0  
    if (select trigger_nestlevel()) = 1 and @is_mergeagent = 1
        return   
    declare @article_rows_updated int
    -- Should use @@rowcount below but there is a bug because of which sometimes in the presence of 
    -- other triggers on the table, the @@rowcount cannot be relied on.
    select @article_rows_updated = count(*) from inserted 
    
    if @article_rows_updated=0
        return
    declare @contents_rows_updated int, @updateerror int, @rowguid uniqueidentifier
    , @bm varbinary(500), @missingbm varbinary(500), @lineage varbinary(311), @cv varbinary(1), @partchangebm varbinary(500), @joinchangebm varbinary(500), @logicalrelationchangebm varbinary(500)
    , @tablenick int, @partchange int, @joinchange int, @logicalrelationchange int, @oldmaxversion int
    , @partgen bigint, @newgen bigint, @child_newgen bigint, @child_oldmaxversion int, @child_metadatarows_updated int
    , @logical_record_parent_oldmaxversion int, @logical_record_lineage varbinary(311), @logical_record_parent_regular_lineage varbinary(311), @logical_record_parent_gencur bigint, @logical_record_parent_rowguid uniqueidentifier
    , @replnick binary(6), @num_parent_rows int, @parent_row_inserted bit 
    declare @dt datetime
    declare @nickbin varbinary(8)
    declare @error int
    declare @null_lineage_updated bit 
    set nocount on

    set @tablenick = 10519000   
    
    select @replnick = 0x00cb5738a6d7 
    select @nickbin = @replnick + 0xFF
    
    select @null_lineage_updated = 0

    select @oldmaxversion = maxversion_at_cleanup from dbo.sysmergearticles where nickname = @tablenick
    select @dt = getdate()
    
    -- Use intrinsic funtion to set bits for updated columns
    set @bm = columns_updated() 
    select @newgen = NULL
    select top 1 @newgen = generation from MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F with (rowlock, updlock, readpast) 
        where art_nick = 10519000    and 
            genstatus = 0  and
            changecount <= (1000 - isnull(@article_rows_updated,0))
    if @newgen is NULL
    begin
        insert into MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F with (rowlock)
        (guidsrc, genstatus, art_nick, nicknames, coldate, changecount)
             values   (newid(), 0, @tablenick, @nickbin, @dt, @article_rows_updated)
        select @error = @@error, @newgen = @@identity    
        if @error<>0 or @newgen is NULL
            goto FAILURE
    end     
    else
    begin
        -- now update the changecount of the generation we go to reflect the number of rows we put in this generation
        update MSmerge_genvw_72F54B6000734EC9BCC3E0920C3FE24F with (rowlock)
            set changecount = changecount + @article_rows_updated
            where generation = @newgen 
        if @@error<>0 goto FAILURE
    end 

    /* save a copy of @bm */
    declare @origin_bm varbinary(500)
    set  @origin_bm =  @bm

    /* only do the map down when needed */
    set @missingbm = 0x00     
	set @partchangebm = 0x00  
	set @joinchangebm = 0x00  
	set @logicalrelationchangebm = 0x00 
    if update([rowguid])
    begin
        if @@trancount > 0
            rollback tran
                
        RAISERROR (20062, 16, -1)
    end  
	/* See if the partition might have changed */ 
		set @partchange = 0     
	/* See if a column used in a join filter changed */ 
		set @joinchange = 0 
	/* See if a column used in a logical record relationship changed */ 
		set @logicalrelationchange = 0 
       set @lineage = { fn UPDATELINEAGE(0x0, @replnick, @oldmaxversion+1) }
            set @cv = NULL
     
    if @joinchange = 1 or @partchange = 1
        set @partgen = @newgen
 
    else    
        set @partgen = NULL 
        update MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F with (rowlock)
        set lineage = { fn UPDATELINEAGE(lineage, @replnick, @oldmaxversion+1) }, 
            generation = @newgen, 
            partchangegen = case when (@partchange = 1 or @joinchange = 1) then @newgen else partchangegen end, 
             colv1 = NULL  
        FROM inserted as I JOIN MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F as V with (rowlock)
        ON (I.rowguidcol=V.rowguid)
        and V.tablenick = @tablenick
        option (force order, loop join)

        select @updateerror = @@error, @contents_rows_updated = @@rowcount 
          
        if @article_rows_updated <> @contents_rows_updated
        begin  
            insert into MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F with (rowlock) (tablenick, rowguid, lineage, colv1, generation, partchangegen) 
            select @tablenick, rowguidcol, @lineage, @cv, @newgen, @partgen
            from inserted i 
            where not exists (select rowguid from MSmerge_ctsv_72F54B6000734EC9BCC3E0920C3FE24F with (readcommitted, rowlock, readpast) where tablenick = @tablenick and rowguid = i.rowguidcol)     
            if @@error <> 0
                GOTO FAILURE
        end     

    return
FAILURE:
    if @@trancount > 0
        rollback tran
    raiserror (20041, 16, -1)
    return  
GO


EXEC sp_addextendedproperty N'MS_Description', N'This table would store the balancing tolerance component of Supplier Service Conditions in a Bid for a RFP for both power & gas conditions', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_BALANCING_TOLERANCE', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_BALANCING_TOLERANCE] ADD 
CONSTRAINT [PK__SR_RFP_BALANCING__05EFB7CF] PRIMARY KEY CLUSTERED  ([SR_RFP_BALANCING_TOLERANCE_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
CREATE UNIQUE NONCLUSTERED INDEX [index_759074586] ON [dbo].[SR_RFP_BALANCING_TOLERANCE] ([rowguid]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[SR_RFP_BALANCING_TOLERANCE] ADD
CONSTRAINT [FK_SR_RFP_BALANCING_TOLERANCE_ENTITY] FOREIGN KEY ([BALANCING_FREQUENCY_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
