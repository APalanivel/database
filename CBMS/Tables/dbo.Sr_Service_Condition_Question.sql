CREATE TABLE [dbo].[Sr_Service_Condition_Question]
(
[Sr_Service_Condition_Question_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Question_Label] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Comment_Shown] [bit] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question__Is_Comment_Shown] DEFAULT ((0)),
[Comment_Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response_Type_Cd] [int] NOT NULL,
[Default_Sr_Service_Condition_Category_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question__Last_Change_Ts] DEFAULT (getdate()),
[Is_Response_Required] [bit] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question__Is_Response_Required] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question] ADD CONSTRAINT [pk_Sr_Service_Condition_Question] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Question_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question] ADD CONSTRAINT [un_Sr_Service_Condition_Question__Question_Label] UNIQUE NONCLUSTERED  ([Question_Label]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question] ADD CONSTRAINT [fk_Sr_Service_Condition_Category__Sr_Service_Condition_Question] FOREIGN KEY ([Default_Sr_Service_Condition_Category_Id]) REFERENCES [dbo].[Sr_Service_Condition_Category] ([Sr_Service_Condition_Category_Id])
GO
