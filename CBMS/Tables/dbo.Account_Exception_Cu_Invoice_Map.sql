CREATE TABLE [dbo].[Account_Exception_Cu_Invoice_Map]
(
[Account_Exception_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Exception_Cu_Invoice_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Exception_Cu_Invoice_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Exception_Cu_Invoice_Map] ADD CONSTRAINT [Pk_Account_Exception_Cu_Invoice_Map__Account_Exception_Id_Cu_Invoice_Id_Status_Cd] PRIMARY KEY CLUSTERED  ([Account_Exception_Id], [Cu_Invoice_Id]) ON [DB_DATA01]
GO
