CREATE TABLE [dbo].[SR_SA_SM_MAP]
(
[SR_SA_SM_MAP_ID] [int] NOT NULL IDENTITY(1, 1),
[SOURCING_ANALYST_ID] [int] NOT NULL,
[SOURCING_MANAGER_ID] [int] NOT NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the mappings between a Sourcing Analyst and his/her manager', 'SCHEMA', N'dbo', 'TABLE', N'SR_SA_SM_MAP', NULL, NULL
GO

ALTER TABLE [dbo].[SR_SA_SM_MAP] WITH NOCHECK ADD
CONSTRAINT [FK_SR_SA_SM_MAP_USER_INFO] FOREIGN KEY ([SOURCING_ANALYST_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[SR_SA_SM_MAP] WITH NOCHECK ADD
CONSTRAINT [FK_SR_SA_SM_MAP_USER_INFO1] FOREIGN KEY ([SOURCING_MANAGER_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[SR_SA_SM_MAP] ADD 
CONSTRAINT [PK__SR_SA_SM_MAP__31CE3A0D] PRIMARY KEY CLUSTERED  ([SR_SA_SM_MAP_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]



GO
