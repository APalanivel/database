CREATE TABLE [ETL].[Data_Conversion_Batch]
(
[Data_Conversion_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[File_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Conversion_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Batch_Start_Ts] [datetime] NULL,
[Batch_End_Ts] [datetime] NULL,
[Email_Address] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Data upload  (Utility accounts/ Manual sitegroup) batch process header.', 'SCHEMA', N'ETL', 'TABLE', N'Data_Conversion_Batch', NULL, NULL
GO

ALTER TABLE [ETL].[Data_Conversion_Batch] ADD CONSTRAINT [PK__Data_Con__3E3501DC7F2BE32F] PRIMARY KEY CLUSTERED  ([Data_Conversion_Batch_Id]) ON [PRIMARY]
GO
