CREATE TABLE [dbo].[GRESB_Data_Variable]
(
[GRESB_Data_Variable_Id] [int] NOT NULL CONSTRAINT [DF__GRESB_Dat__GRESB__559A6095] DEFAULT ((0)),
[Data_Variable_XKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__GRESB_Dat__Data___568E84CE] DEFAULT (''),
[Data_Variable_XKey_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data_Level_Cd] [int] NOT NULL CONSTRAINT [DF__GRESB_Dat__Data___5782A907] DEFAULT ((0)),
[DataType_Cd] [int] NOT NULL CONSTRAINT [DF__GRESB_Dat__DataT__5876CD40] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL CONSTRAINT [DF__GRESB_Dat__Creat__596AF179] DEFAULT ((0)),
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__GRESB_Dat__Creat__5A5F15B2] DEFAULT ('01-jan-1900 00:00:00'),
[Updated_User_Id] [int] NOT NULL CONSTRAINT [DF__GRESB_Dat__Updat__5B5339EB] DEFAULT ((0)),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__GRESB_Dat__Last___5C475E24] DEFAULT ('01-jan-1900 00:00:00'),
[Parent_GRESB_Data_Variable_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[GRESB_Data_Variable] ADD CONSTRAINT [PK__GRESB_Da__7F2BA51353B21823] PRIMARY KEY CLUSTERED  ([GRESB_Data_Variable_Id]) ON [DB_DATA01]
GO
