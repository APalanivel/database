CREATE TABLE [dbo].[Filtered_Code_Permission]
(
[Codeset_Filtered_Cd] [int] NOT NULL,
[Filtered_Code_Id] [int] NOT NULL,
[PERMISSION_INFO_ID] [int] NOT NULL
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Permissions assigned to filters', 'SCHEMA', N'dbo', 'TABLE', N'Filtered_Code_Permission', NULL, NULL
GO

ALTER TABLE [dbo].[Filtered_Code_Permission] ADD CONSTRAINT [pk_Filtered_Code_Permission] PRIMARY KEY CLUSTERED  ([Codeset_Filtered_Cd], [Filtered_Code_Id], [PERMISSION_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [Ix_Filtered_Code_Permission__Codeset_Filtered_Code__Filtered_Code_Id] ON [dbo].[Filtered_Code_Permission] ([Codeset_Filtered_Cd], [Filtered_Code_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [ix_Filtered_Code_Permission__Permission_Info_Id] ON [dbo].[Filtered_Code_Permission] ([PERMISSION_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
ALTER TABLE [dbo].[Filtered_Code_Permission] ADD CONSTRAINT [fk_Filtered_Code__Codeset_Filtered_Cd__Filtered_Code_Id] FOREIGN KEY ([Codeset_Filtered_Cd], [Filtered_Code_Id]) REFERENCES [dbo].[Filtered_Code] ([Codeset_Filtered_Cd], [Filtered_Code_Id])
GO
ALTER TABLE [dbo].[Filtered_Code_Permission] ADD CONSTRAINT [fk_PERMISSION_INFO__PERMISSION_INFO_ID] FOREIGN KEY ([PERMISSION_INFO_ID]) REFERENCES [dbo].[PERMISSION_INFO] ([PERMISSION_INFO_ID])
GO
