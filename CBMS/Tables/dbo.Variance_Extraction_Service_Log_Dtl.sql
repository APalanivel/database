CREATE TABLE [dbo].[Variance_Extraction_Service_Log_Dtl]
(
[Variance_Extraction_Service_Log_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Extraction_Service_Log_Id] [int] NOT NULL,
[Request_Data] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Response_Data] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Extraction_Service_Log_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service_Log_Dtl] ADD CONSTRAINT [pk_Variance_Extraction_Service_Log_Dtl] PRIMARY KEY CLUSTERED  ([Variance_Extraction_Service_Log_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service_Log_Dtl] ADD CONSTRAINT [fk_Variance_Extraction_Service_Log__Variance_Extraction_Service_Log_Dtl] FOREIGN KEY ([Variance_Extraction_Service_Log_Id]) REFERENCES [dbo].[Variance_Extraction_Service_Log] ([Variance_Extraction_Service_Log_Id])
GO
