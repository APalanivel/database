CREATE TABLE [dbo].[Filtered_Time_Zone]
(
[Time_Zone_Id] [int] NOT NULL,
[Tz_Filtered_Module_Cd] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Filtered_Time_Zone] ADD CONSTRAINT [pk_Filtered_Time_Zone] PRIMARY KEY CLUSTERED  ([Time_Zone_Id], [Tz_Filtered_Module_Cd]) ON [DB_DATA01]
GO
