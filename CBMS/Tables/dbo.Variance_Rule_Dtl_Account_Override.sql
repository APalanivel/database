CREATE TABLE [dbo].[Variance_Rule_Dtl_Account_Override]
(
[Variance_Rule_Dtl_Id] [int] NOT NULL,
[ACCOUNT_ID] [int] NOT NULL,
[Tolerance] [decimal] (16, 4) NULL,
[Test_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Table to track accounts for which the default variance rule has been overridden', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Rule_Dtl_Account_Override', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Rule_Dtl_Account_Override] ADD 
CONSTRAINT [PK_Variance_Rule_Dtl_Account_Override] PRIMARY KEY CLUSTERED  ([Variance_Rule_Dtl_Id], [ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [IX_Variance_Rule_Dtl_Account_Override__Account_ID] ON [dbo].[Variance_Rule_Dtl_Account_Override] ([ACCOUNT_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Variance_Rule_Dtl_Account_Override] ADD
CONSTRAINT [FK_ACCOUNTVariance_Rule_Dtl_Account_Override] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
ALTER TABLE [dbo].[Variance_Rule_Dtl_Account_Override] ADD
CONSTRAINT [FK_Variance_Rule_DtlVariance_Rule_Dtl_Account_Override] FOREIGN KEY ([Variance_Rule_Dtl_Id]) REFERENCES [dbo].[Variance_Rule_Dtl] ([Variance_Rule_Dtl_Id])


GO
