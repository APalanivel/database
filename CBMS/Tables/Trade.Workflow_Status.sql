CREATE TABLE [Trade].[Workflow_Status]
(
[Workflow_Status_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Status_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_Status_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Notification_Required] [bit] NOT NULL,
[Notification_Template_Id] [int] NULL,
[Notification_Frequency_Cd] [int] NULL,
[Managed_By_Group_Info_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Status__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Status__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Status] ADD CONSTRAINT [pk_Workflow_Status] PRIMARY KEY CLUSTERED  ([Workflow_Status_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Status] ADD CONSTRAINT [un_Workflow_Status__Workflow_Status_Name] UNIQUE NONCLUSTERED  ([Workflow_Status_Name]) ON [DB_DATA01]
GO
