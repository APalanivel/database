CREATE TABLE [Trade].[Workflow_Transition_Level]
(
[Workflow_Transition_Level_Id] [int] NOT NULL IDENTITY(1, 1),
[Level_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Transition_Level__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Transition_Level__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Transition_Level] ADD CONSTRAINT [pk_WWorkflow_Transition_Level] PRIMARY KEY CLUSTERED  ([Workflow_Transition_Level_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Transition_Level] ADD CONSTRAINT [un_Workflow_Transition_Level__Level_Name] UNIQUE NONCLUSTERED  ([Level_Name]) ON [DB_DATA01]
GO
