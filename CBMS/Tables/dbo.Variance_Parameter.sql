CREATE TABLE [dbo].[Variance_Parameter]
(
[Variance_Parameter_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Category_Cd] [int] NOT NULL,
[Parameter] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Variance Parameter used for tests (example - Total Cost/ Unit Cost/ Total Usage)', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Parameter', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Parameter] ADD 
CONSTRAINT [PK_Variance_Parameter] PRIMARY KEY CLUSTERED  ([Variance_Parameter_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
