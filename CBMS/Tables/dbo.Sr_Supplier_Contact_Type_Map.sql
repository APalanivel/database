CREATE TABLE [dbo].[Sr_Supplier_Contact_Type_Map]
(
[Sr_Supplier_Contact_Type_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[SR_SUPPLIER_CONTACT_INFO_ID] [int] NULL,
[Contact_Type_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Supplier_Contact_Type_Map] ADD CONSTRAINT [Pk_Sr_Supplier_Contact_Type_Map] PRIMARY KEY CLUSTERED  ([Sr_Supplier_Contact_Type_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Supplier_Contact_Type_Map] ADD CONSTRAINT [fk_SR_SUPPLIER_CONTACT_INFO__Sr_Supplier_Contact_Type_Map] FOREIGN KEY ([SR_SUPPLIER_CONTACT_INFO_ID]) REFERENCES [dbo].[SR_SUPPLIER_CONTACT_INFO] ([SR_SUPPLIER_CONTACT_INFO_ID])
GO
