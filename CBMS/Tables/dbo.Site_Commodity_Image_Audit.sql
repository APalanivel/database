CREATE TABLE [dbo].[Site_Commodity_Image_Audit]
(
[Audit_Function] [smallint] NOT NULL,
[Audit_Ts] [datetime] NOT NULL CONSTRAINT [df_Site_Commodity_Image_Audit] DEFAULT (getdate()),
[Site_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to Site_Commodity_Image', 'SCHEMA', N'dbo', 'TABLE', N'Site_Commodity_Image_Audit', NULL, NULL
GO

CREATE CLUSTERED INDEX [Site_Commodity_Image_Audit_Site_id_Commodity_id_Service_Month] ON [dbo].[Site_Commodity_Image_Audit] ([Site_Id], [Commodity_Id], [Service_Month]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]



GO
