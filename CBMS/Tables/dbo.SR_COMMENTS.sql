CREATE TABLE [dbo].[SR_COMMENTS]
(
[SR_COMMENTS_ID] [int] NOT NULL IDENTITY(1, 1),
[COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the comments entered from the Manage Comments section of the SA Dashboard', 'SCHEMA', N'dbo', 'TABLE', N'SR_COMMENTS', NULL, NULL
GO

ALTER TABLE [dbo].[SR_COMMENTS] ADD 
CONSTRAINT [PK__SR_COMMENTS__6582E83D] PRIMARY KEY CLUSTERED  ([SR_COMMENTS_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
