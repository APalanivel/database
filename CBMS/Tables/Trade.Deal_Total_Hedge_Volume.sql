CREATE TABLE [Trade].[Deal_Total_Hedge_Volume]
(
[Deal_Ticket_Id] [int] NOT NULL,
[Deal_Month] [date] NOT NULL,
[Total_Volume] [decimal] (28, 6) NOT NULL,
[Uom_Type_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Total_Hedge_Volume__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Total_Hedge_Volume__Last_Change_Ts] DEFAULT (getdate()),
[Client_Generated_Price] [decimal] (28, 6) NULL,
[Trigger_Target_Price] [decimal] (28, 6) NULL,
[Client_Generated_Fixed_Dt] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Total_Hedge_Volume] ADD CONSTRAINT [pk_Deal_Total_Hedge_Volume] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Id], [Deal_Month]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Total_Hedge_Volume] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Total_Hedge_Volume] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
