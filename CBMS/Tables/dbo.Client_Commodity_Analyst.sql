CREATE TABLE [dbo].[Client_Commodity_Analyst]
(
[Client_Commodity_Id] [int] NOT NULL,
[Analyst_User_Info_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table between Client_Commodity and the Analyst responsible (User_Info_ID)', 'SCHEMA', N'dbo', 'TABLE', N'Client_Commodity_Analyst', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Commodity_Analyst] WITH NOCHECK ADD
CONSTRAINT [fk_Client_Commodity_Analyst__User_Info] FOREIGN KEY ([Analyst_User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[Client_Commodity_Analyst] ADD 
CONSTRAINT [pk_Client_Commodity_Analyst] PRIMARY KEY CLUSTERED  ([Client_Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Client_Commodity_Analyst_Analyst_User_Info_Id_incl] ON [dbo].[Client_Commodity_Analyst] ([Analyst_User_Info_Id]) INCLUDE ([Client_Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Client_Commodity_Analyst] WITH NOCHECK ADD
CONSTRAINT [fk_Client_Commodity_Analyst__Core_Client_Commodity] FOREIGN KEY ([Client_Commodity_Id]) REFERENCES [Core].[Client_Commodity] ([Client_Commodity_Id]) NOT FOR REPLICATION


GO
