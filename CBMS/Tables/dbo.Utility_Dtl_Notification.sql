CREATE TABLE [dbo].[Utility_Dtl_Notification]
(
[Utility_Dtl_Notification_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Industrial_Flag_Cd] [int] NOT NULL,
[Commercial_Flag_Cd] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'captures the Notifications/Deadlines for EP and NG commodities of an Utility', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Notification', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Notification] ADD CONSTRAINT [unc_Utility_Dtl_Notification__Question_Commodity_Map_Id__Vendor_Commodity_Map_Id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Notification] ADD CONSTRAINT [PK_Utility_Dtl_Notification] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Notification_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Notification__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Notification] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Notification__Comment_ID] ON [dbo].[Utility_Dtl_Notification] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Notification] ADD
CONSTRAINT [fk_Question_Commodity_Map__Utility_Dtl_Notification] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Notification] ADD
CONSTRAINT [fk_VENDOR_COMMODITY_MAP__Utility_Dtl_Notification] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Notification] ADD
CONSTRAINT [fk_Comment__Utility_Dtl_Notification] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
