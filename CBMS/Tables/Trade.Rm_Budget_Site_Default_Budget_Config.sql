CREATE TABLE [Trade].[Rm_Budget_Site_Default_Budget_Config]
(
[Rm_Budget_Site_Default_Budget_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Rm_Budget_Id] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Rm_Budget_Site_Default_Budget_Config__Created_Ts] DEFAULT (getdate()),
[Last_Updated_By] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Rm_Budget_Site_Default_Budget_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Site_Default_Budget_Config] ADD CONSTRAINT [PK_Rm_Budget_Site_Default_Budget_Config__Rm_Budget_Site_Default_Budget_Config_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Site_Default_Budget_Config_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Rm_Budget_Site_Default_Budget_Config__Client_Hier_Id] ON [Trade].[Rm_Budget_Site_Default_Budget_Config] ([Client_Hier_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Site_Default_Budget_Config] ADD CONSTRAINT [Uk_Trade_Rm_Budget_Site_Default_Budget_Config] UNIQUE NONCLUSTERED  ([Rm_Budget_Id], [Client_Hier_Id], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Site_Default_Budget_Config] ADD CONSTRAINT [FK_Rm_Budget_Site_Default_Budget_Config__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
