CREATE TABLE [Trade].[Rm_Budget_Volume_Price_Dtl]
(
[Rm_Budget_Volume_Price_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Rm_Budget_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Forecast_Volume] [decimal] (28, 3) NULL,
[Total_Volume_Hedged] [decimal] (28, 3) NULL,
[WACOG_Hedge_Price] [decimal] (28, 3) NULL,
[Budget_Target_Price] [decimal] (28, 3) NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Volume_Price_Dtl] ADD CONSTRAINT [PK_Rm_Budget_Volume_Price_Dtl__Rm_Budget_Volume_Price_Dtl_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Volume_Price_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Volume_Price_Dtl] ADD CONSTRAINT [Uk__Trade_Rm_Budget_Volume_Price_Dtl] UNIQUE NONCLUSTERED  ([Rm_Budget_Id], [Client_Hier_Id], [Service_Month]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Volume_Price_Dtl] ADD CONSTRAINT [FK_Rm_Budget_Volume_Price_Dtl__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
