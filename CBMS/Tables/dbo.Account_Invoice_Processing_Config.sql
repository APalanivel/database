CREATE TABLE [dbo].[Account_Invoice_Processing_Config]
(
[Account_Invoice_Processing_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Processing_Instruction_Category_Cd] [int] NOT NULL,
[Config_Start_Dt] [date] NOT NULL,
[Config_End_Dt] [date] NULL,
[Processing_Instruction] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Watch_List_Group_Info_Id] [int] NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [DF__Account_Invoice_Processing_Config_Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_Account_Invoice_Processing_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF_Account_Invoice_Processing_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Processing_Config] ADD CONSTRAINT [pk_Account_Invoice_Processing_Config] PRIMARY KEY CLUSTERED  ([Account_Invoice_Processing_Config_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Invoice_Processing_Config__Account_Id__Invoice_Processing_Config_Type_Cd__Config_Start_Dt__Config_End_Dt] ON [dbo].[Account_Invoice_Processing_Config] ([Account_Id], [Processing_Instruction_Category_Cd], [Config_Start_Dt], [Config_End_Dt]) ON [DB_DATA01]
GO
