CREATE TABLE [dbo].[hvr_stisr_h_d_c0002_c0001]
(
[hvr_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_c__7324234D] DEFAULT (''),
[hvr_tx_seq] [binary] (24) NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_t__74184786] DEFAULT ((0x00)),
[hvr_tx_countdown] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__hvr_t__750C6BBF] DEFAULT ((0)),
[integ_rows_done] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__76008FF8] DEFAULT ((0)),
[integ_blocked] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__76F4B431] DEFAULT ((0)),
[integ_unblocked] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__integ__77E8D86A] DEFAULT ((0)),
[history_scan_start_tstamp] [int] NOT NULL CONSTRAINT [DF__hvr_stisr__histo__78DCFCA3] DEFAULT ((0)),
[history_scan_start_addr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__histo__79D120DC] DEFAULT (''),
[history_emit_seq] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stisr__histo__7AC54515] DEFAULT ('')
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[hvr_stisr_h_d_c0002_c0001] ADD CONSTRAINT [PK__hvr_stis__47A37727713BDADB] PRIMARY KEY NONCLUSTERED  ([hvr_cap_loc]) ON [DB_DATA01]
GO
