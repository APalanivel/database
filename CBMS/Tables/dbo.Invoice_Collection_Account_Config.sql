CREATE TABLE [dbo].[Invoice_Collection_Account_Config]
(
[Invoice_Collection_Account_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Invoice_Collection_Service_Start_Dt] [date] NOT NULL,
[Invoice_Collection_Service_End_Dt] [date] NULL,
[Is_Chase_Activated] [bit] NOT NULL CONSTRAINT [DF__Invoice_Collection_Account_Config__Is_Chase_Activated] DEFAULT ((0)),
[Chase_Status_Updated_User_Id] [int] NULL,
[Chase_Status_Lat_Updated_Ts] [datetime] NULL,
[Invoice_Collection_Officer_User_Id] [int] NULL,
[Chase_Priority_Cd] [int] NULL,
[Invoice_Pattern_Cd] [int] NULL,
[Additional_Invoices_Per_Period] [tinyint] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Config__Last_Change_Ts] DEFAULT (getdate()),
[Invoice_Collection_Alternative_Account_Number] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[is_Config_Complete] [bit] NULL CONSTRAINT [DF__Invoice_Collection_Account_Config__Is_Exceptions_Closed] DEFAULT ((0)),
[Invoice_Collection_Owner_User_Id] [int] NULL,
[Service_Type_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config] ADD CONSTRAINT [pk_Invoice_Collection_Account_Config] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Account_Config_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Invoice_Collection_Account_Config__Account_Id_Invoice_Collection_Service_Start_Dt_Invoice_Collection_Service_End_Dt] ON [dbo].[Invoice_Collection_Account_Config] ([Account_Id], [Invoice_Collection_Service_Start_Dt], [Invoice_Collection_Service_End_Dt]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'To include the account in the invoice collection process', 'SCHEMA', N'dbo', 'TABLE', N'Invoice_Collection_Account_Config', NULL, NULL
GO
