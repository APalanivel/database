CREATE TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch]
(
[Cu_Invoice_Workflow_Action_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Action_Id] [int] NOT NULL,
[Lookup_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Requested_User_Id] [int] NOT NULL,
[Requested_Ts] [datetime] NOT NULL CONSTRAINT [DF__Cu_Invoic__Reque__32032BFC] DEFAULT (getdate()),
[Status_Cd] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Cu_Invoic__Last___32F75035] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch] ADD CONSTRAINT [pk_Cu_Invoice_Workflow_Action_Batch] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Workflow_Action_Batch_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch] ADD CONSTRAINT [fk_Workflow_Queue_Action__Cu_Invoice_Workflow_Action_Batch] FOREIGN KEY ([Workflow_Queue_Action_Id]) REFERENCES [Workflow].[Workflow_Queue_Action] ([Workflow_Queue_Action_Id])
GO
