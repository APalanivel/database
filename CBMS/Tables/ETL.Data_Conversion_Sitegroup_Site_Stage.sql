CREATE TABLE [ETL].[Data_Conversion_Sitegroup_Site_Stage]
(
[Client_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Division_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sitegroup_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sitegroup_Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Add_User] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Address] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Staging table to bulk load the  Manual sitegroups. Used by the data upload tool runs from tidal.', 'SCHEMA', N'ETL', 'TABLE', N'Data_Conversion_Sitegroup_Site_Stage', NULL, NULL
GO
