CREATE TABLE [dbo].[App_Global_Config]
(
[App_Global_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[App_Module_Cd] [int] NOT NULL,
[Config_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Config_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Permission_Info_Id] [int] NULL,
[Display_Seq] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_App_Global_Config__Is_Active] DEFAULT ((1)),
[Nav_Link] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config__Last_Change_Ts] DEFAULT (getdate()),
[Is_Default] [bit] NOT NULL CONSTRAINT [df_App_Global_Config__Is_Default] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config] ADD CONSTRAINT [pk_App_Global_Config] PRIMARY KEY CLUSTERED  ([App_Global_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config] ADD CONSTRAINT [un_App_Global_Config__App_Module_Cd__Config_Name] UNIQUE NONCLUSTERED  ([App_Module_Cd], [Config_Name]) ON [DB_DATA01]
GO
