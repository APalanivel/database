CREATE TABLE [Trade].[Deal_Ticket]
(
[Deal_Ticket_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Deal_Ticket_Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Is_Client_Generated] [bit] NOT NULL CONSTRAINT [df_Deal_Ticket__Is_Client_Generated] DEFAULT ((0)),
[Hedge_Start_Dt] [date] NOT NULL,
[Hedge_End_Dt] [date] NOT NULL,
[Hedge_Type_Cd] [int] NOT NULL,
[Hedge_Allocation_Type_Cd] [int] NOT NULL,
[Price_Index_Id] [int] NULL,
[Currency_Unit_Id] [int] NOT NULL,
[Uom_Type_Id] [int] NOT NULL,
[Deal_Ticket_Frequency_Cd] [int] NOT NULL,
[Trade_Pricing_Option_Cd] [int] NOT NULL,
[Trade_Action_Type_Cd] [int] NOT NULL,
[Deal_Ticket_Type_Cd] [int] NOT NULL,
[Deal_Status_Cd] [int] NULL,
[Workflow_Id] [int] NOT NULL,
[Is_Individual_Site_Pricing_Required] [bit] NOT NULL CONSTRAINT [df_Deal_Ticket__Is_Individual_Site_Pricing_Required] DEFAULT ((0)),
[Comment_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket__Last_Change_Ts] DEFAULT (getdate()),
[Is_Bid] [bit] NULL,
[Hedge_Mode_Type_Id] [int] NULL,
[Queue_Id] [int] NULL,
[Last_Routed_By_User_Id] [int] NULL,
[Last_Routed_Ts] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket] ADD CONSTRAINT [pk_Deal_Ticket] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket__Client_Id] ON [Trade].[Deal_Ticket] ([Client_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket] ADD CONSTRAINT [fk_Workflow__Deal_Ticket] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
