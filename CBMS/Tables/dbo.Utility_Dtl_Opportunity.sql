CREATE TABLE [dbo].[Utility_Dtl_Opportunity]
(
[Utility_Dtl_Opportunity_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Program_Dsc] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Program_Req] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Savings_Potential_Cd] [int] NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'This table captures the ‘Other Rate Opportunities (not transport related)’ section for EP and NG', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Opportunity', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Opportunity] ADD CONSTRAINT [unc_Utility_Dtl_Opportunity__Question_Commodity_Map_Id__Vendor_Commodity_Map_Id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Opportunity] ADD CONSTRAINT [pk_Utility_Dtl_Opportunity] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Opportunity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Opportunity__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Opportunity] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Opportunity__Comment_ID] ON [dbo].[Utility_Dtl_Opportunity] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Opportunity] ADD
CONSTRAINT [fk_Question_Commodity_Map__Utility_Dtl_Opportunity] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Opportunity] ADD
CONSTRAINT [fk_VENDOR_COMMODITY_MAP__Utility_Dtl_Opportunity] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Opportunity] ADD
CONSTRAINT [fk_Comment_Utility_Dtl_Opportunity] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
