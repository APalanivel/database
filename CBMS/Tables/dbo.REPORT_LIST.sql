CREATE TABLE [dbo].[REPORT_LIST]
(
[REPORT_LIST_ID] [int] NOT NULL IDENTITY(1, 1),
[REPORT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[REPORT_DESCRIPTION] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INPUT_ACTION_CLASS] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OUTPUT_ACTION_CLASS] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[REPORT_URL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Deprecated]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table stores the actual report names and their target actions', 'SCHEMA', N'dbo', 'TABLE', N'REPORT_LIST', NULL, NULL
GO

ALTER TABLE [dbo].[REPORT_LIST] ADD 
CONSTRAINT [REPORT_LIST_PK] PRIMARY KEY CLUSTERED  ([REPORT_LIST_ID]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]

GO
