CREATE TABLE [dbo].[Site_Commodity_Analyst]
(
[Site_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Analyst_User_Info_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of CBMS Site ID to CommodityID to Analyst User ID. Not sure what this is used for. There''s only 128 records?', 'SCHEMA', N'dbo', 'TABLE', N'Site_Commodity_Analyst', NULL, NULL
GO

ALTER TABLE [dbo].[Site_Commodity_Analyst] WITH NOCHECK ADD
CONSTRAINT [fk_Site_Commodity_Analyst__User_Info] FOREIGN KEY ([Analyst_User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[Site_Commodity_Analyst] ADD 
CONSTRAINT [pk_Site_Commodity_Analyst] PRIMARY KEY CLUSTERED  ([Site_Id], [Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Site_Commodity_Analyst_Analyst_User_Info_Id_incl] ON [dbo].[Site_Commodity_Analyst] ([Analyst_User_Info_Id]) INCLUDE ([Commodity_Id], [Site_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Site_Commodity_Analyst] ADD
CONSTRAINT [fk_Site_Commodity_Analyst__Commodity] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
ALTER TABLE [dbo].[Site_Commodity_Analyst] ADD
CONSTRAINT [fk_Site_Commodity_Analyst__Site] FOREIGN KEY ([Site_Id]) REFERENCES [dbo].[SITE] ([SITE_ID])

GO
