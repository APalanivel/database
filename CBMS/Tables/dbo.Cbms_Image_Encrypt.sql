CREATE TABLE [dbo].[Cbms_Image_Encrypt]
(
[CBMS_IMAGE_ID] [bigint] NOT NULL,
[Encrypt_Cbms_Image_Id] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Image_Encrypt] ADD CONSTRAINT [pk_Cbms_Image_Encrypt] PRIMARY KEY CLUSTERED  ([CBMS_IMAGE_ID]) ON [DB_DATA01]
GO
