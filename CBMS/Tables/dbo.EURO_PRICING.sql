CREATE TABLE [dbo].[EURO_PRICING]
(
[EURO_PRICING_ID] [int] NOT NULL IDENTITY(1, 1),
[DETAIL_DATE] [datetime] NULL,
[PRICE_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YEAR] [int] NULL,
[INTERVAL] [int] NULL,
[VALUE] [decimal] (32, 16) NULL,
[PRICE_POINT] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[future_date] AS (case  when [price_type]='m' then CONVERT([smalldatetime],(CONVERT([varchar],[interval],(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) when [price_type]='q' AND [interval]=(1) then CONVERT([smalldatetime],(CONVERT([varchar],(1),(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) when [price_type]='q' AND [interval]=(2) then CONVERT([smalldatetime],(CONVERT([varchar],(4),(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) when [price_type]='q' AND [interval]=(3) then CONVERT([smalldatetime],(CONVERT([varchar],(7),(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) when [price_type]='q' AND [interval]=(4) then CONVERT([smalldatetime],(CONVERT([varchar],(10),(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) when [price_type]='y' then CONVERT([smalldatetime],(CONVERT([varchar],[interval],(0))+'-1-')+CONVERT([varchar],[year],(0)),(0)) end)
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--update market_price_point
--and market_price_point_detail
--when adding to this table
create trigger [dbo].[trg_ins_Euro_Pricing]
on [dbo].[EURO_PRICING]
for insert
as 
begin
set nocount on
	--insert any new price points
	insert into market_price_point (market_price_index_id,market_price_point,interval_type,market_price_point_short_name)
	select distinct a.market_price_index_id,price_point+' '+
	case price_type when 'y' then 'Yearly'
	when 'q' then 'Quarterly'
	when 'm' then 'Monthly'
	end
	,price_type,left(price_point,50)+' '+case price_type when 'y' then 'Yearly'
	when 'q' then 'Quarterly'
	when 'm' then 'Monthly'
	end
	from market_price_index a 
	inner join inserted b on a.market_price_index_name='euro_pricing' 
	left outer join market_price_point c on c.market_price_point=
	price_point+' '+
	case price_type when 'y' then 'Yearly'
	when 'q' then 'Quarterly'
	when 'm' then 'Monthly'
	end
	where c.market_price_point is null

	--insert new price point detail data

	insert into market_price_point_detail (market_price_point_id,
	market_price_point_value,market_price_point_Detail_date,market_price_point_future_date)
	
	select market_price_point_id,
	value,--b.price_point,
	detail_date,
	future_date
	from market_price_point a
	inner join
	inserted b on b.price_point+' '+
	case price_type when 'y' then 'Yearly'
	when 'q' then 'Quarterly'
	when 'm' then 'Monthly'
	end=a.market_price_point
	where future_date is not null and value is not null

end


GO
ALTER TABLE [dbo].[EURO_PRICING] ADD CONSTRAINT [PK_EURO_PRICING] PRIMARY KEY CLUSTERED  ([EURO_PRICING_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated.  Last data entered:  4/28/2009', 'SCHEMA', N'dbo', 'TABLE', N'EURO_PRICING', NULL, NULL
GO
