CREATE TABLE [dbo].[Ec_Client_Account_Group_Account]
(
[Ec_Client_Account_Group_Account_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Ec_Client_Account_Group_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Ec_Client_Account_Group_Account__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group_Account] ADD CONSTRAINT [pk_Ec_Client_Account_Group_Account] PRIMARY KEY CLUSTERED  ([Ec_Client_Account_Group_Account_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group_Account] ADD CONSTRAINT [uix_Ec_Client_Account_Group_Account__Account_Id__Ec_Client_Account_Group_Id] UNIQUE NONCLUSTERED  ([Ec_Client_Account_Group_Id], [Account_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group_Account] ADD CONSTRAINT [fk_Ec_Client_Account_Group_Account__Account_Id] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group_Account] ADD CONSTRAINT [fk_Ec_Client_Account_Group_Account__Ec_Client_Account_Group_Id] FOREIGN KEY ([Ec_Client_Account_Group_Id]) REFERENCES [dbo].[Ec_Client_Account_Group] ([Ec_Client_Account_Group_Id])
GO
