CREATE TABLE [dbo].[Variance_Consumption_Level]
(
[Variance_Consumption_Level_Id] [int] NOT NULL IDENTITY(1, 1),
[Consumption_Level_Desc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Id] [int] NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Variance Consumption Levels per Commodity', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Consumption_Level', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Consumption_Level] ADD 
CONSTRAINT [PK_Variance_Consumption_Level] PRIMARY KEY CLUSTERED  ([Variance_Consumption_Level_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
ALTER TABLE [dbo].[Variance_Consumption_Level] ADD
CONSTRAINT [FK_CommodityVariance_Consumption_Level] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
