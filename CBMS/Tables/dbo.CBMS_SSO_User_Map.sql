CREATE TABLE [dbo].[CBMS_SSO_User_Map]
(
[CBMS_SSO_User_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[User_Info_Id] [int] NOT NULL,
[CBMS_SSO_App_Id] [int] NOT NULL,
[SSO_XUser_Id] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_CBMS_SSO_User_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_CBMS_SSO_User_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_SSO_User_Map] ADD CONSTRAINT [pk_CBMS_SSO_User_Map] PRIMARY KEY CLUSTERED  ([CBMS_SSO_User_Map_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_CBMS_SSO_User_Map__CBMS_SSO_App_Id] ON [dbo].[CBMS_SSO_User_Map] ([CBMS_SSO_App_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_SSO_User_Map] ADD CONSTRAINT [un_CBMS_SSO_User_Map__User_Info_Id__CBMS_SSO_App_Id] UNIQUE NONCLUSTERED  ([User_Info_Id], [CBMS_SSO_App_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_SSO_User_Map] ADD CONSTRAINT [fk_CBMS_SSO_App__CBMS_SSO_User_Map] FOREIGN KEY ([CBMS_SSO_App_Id]) REFERENCES [dbo].[CBMS_SSO_App] ([CBMS_SSO_App_Id])
GO
