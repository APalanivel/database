CREATE TABLE [dbo].[hvr_strrr_h_d_c0002_c0002]
(
[tbl_name] [varchar] (124) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_strrr__tbl_n__45CB0E33] DEFAULT (''),
[recov_label] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_strrr__recov__46BF326C] DEFAULT (''),
[recov_seq] [int] NOT NULL CONSTRAINT [DF__hvr_strrr__recov__47B356A5] DEFAULT ((0)),
[recov_sub_seq] [int] NOT NULL CONSTRAINT [DF__hvr_strrr__recov__48A77ADE] DEFAULT ((0)),
[recov_abort] [datetime] NOT NULL CONSTRAINT [DF__hvr_strrr__recov__499B9F17] DEFAULT ('01-jan-1900 00:00:00'),
[recov_sql] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_strrr__recov__4A8FC350] DEFAULT ('')
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [hvr_strrr_h_d_c0002_c0002__x0] ON [dbo].[hvr_strrr_h_d_c0002_c0002] ([tbl_name], [recov_label]) ON [DB_DATA01]
GO
