CREATE TABLE [dbo].[Sr_Service_Condition_Question_Locale_Value]
(
[Sr_Service_Condition_Question_Locale_Value_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Service_Condition_Question_Id] [int] NOT NULL,
[Language_Cd] [int] NOT NULL,
[Question_Label_Locale_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comment_Label_Locale_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question_Locale_Value__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Sr_Service_Condition_Question_Locale_Value__Last_Change_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question_Locale_Value] ADD CONSTRAINT [pk_Sr_Service_Condition_Question_Locale_Value] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Question_Locale_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question_Locale_Value] ADD CONSTRAINT [un_Sr_Service_Condition_Question_Locale_Value__Sr_Service_Condition_Question_Id__Language_Cd] UNIQUE NONCLUSTERED  ([Sr_Service_Condition_Question_Id], [Language_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question_Locale_Value] ADD CONSTRAINT [fk_Sr_Service_Condition_Question__Sr_Service_Condition_Question_Locale_Value] FOREIGN KEY ([Sr_Service_Condition_Question_Id]) REFERENCES [dbo].[Sr_Service_Condition_Question] ([Sr_Service_Condition_Question_Id])
GO
