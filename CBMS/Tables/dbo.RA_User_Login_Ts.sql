CREATE TABLE [dbo].[RA_User_Login_Ts]
(
[User_Login_Ts_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[USER_INFO_ID] [int] NOT NULL,
[Login_Ts] [datetime] NOT NULL
) ON [DB_DATA01]


GO


ALTER TABLE [dbo].[RA_User_Login_Ts] ADD CONSTRAINT [pk_RA_User_Login_Ts] PRIMARY KEY CLUSTERED  ([User_Login_Ts_Id]) ON [DB_DATA01]
GO
