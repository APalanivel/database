CREATE TABLE [dbo].[Xl_Layout]
(
[Xl_Layout_Id] [int] NOT NULL IDENTITY(1, 1),
[Xl_Bulk_Data_Process_Id] [int] NOT NULL,
[Layout_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Start_Index] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Cell_Padding] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Xl_Layout__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout] ADD CONSTRAINT [pk_Xl_Layout] PRIMARY KEY CLUSTERED  ([Xl_Layout_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout] ADD CONSTRAINT [un_Xl_Layout__Xl_Bulk_Data_Process_Id__Layout_Name] UNIQUE NONCLUSTERED  ([Xl_Bulk_Data_Process_Id], [Layout_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout] ADD CONSTRAINT [fk_Xl_Bulk_Data_Process__Xl_Layout] FOREIGN KEY ([Xl_Bulk_Data_Process_Id]) REFERENCES [dbo].[Xl_Bulk_Data_Process] ([Xl_Bulk_Data_Process_Id])
GO
