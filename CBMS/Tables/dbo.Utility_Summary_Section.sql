CREATE TABLE [dbo].[Utility_Summary_Section]
(
[Utility_Summary_Section_Id] [int] NOT NULL IDENTITY(1, 1),
[Section_Label] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'List of Utility detail sections used in CBMS', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Summary_Section', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Summary_Section] ADD CONSTRAINT [unc_Utility_Summary_Section__Section_Label] UNIQUE CLUSTERED  ([Section_Label]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Summary_Section] ADD CONSTRAINT [pk_Utility_Summary_Section] PRIMARY KEY NONCLUSTERED  ([Utility_Summary_Section_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

GO
