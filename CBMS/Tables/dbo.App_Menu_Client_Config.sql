CREATE TABLE [dbo].[App_Menu_Client_Config]
(
[App_Menu_Client_Config_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[APP_MENU_ID] [int] NOT NULL,
[App_Menu_Advanced_Visualization_Config_Id] [int] NOT NULL,
[Client_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Menu_Client_Config] ADD CONSTRAINT [PK_App_Menu_Client_Config] PRIMARY KEY CLUSTERED  ([App_Menu_Client_Config_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_App_Menu_Client_Config] ON [dbo].[App_Menu_Client_Config] ([APP_MENU_ID], [App_Menu_Advanced_Visualization_Config_Id], [Client_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[App_Menu_Client_Config] ADD CONSTRAINT [fk_App_Menu_Client_Config_App_Menu_Advanced_Visualization_Config_App_Menu_Advanced_Visualization_Config_Id] FOREIGN KEY ([App_Menu_Advanced_Visualization_Config_Id]) REFERENCES [dbo].[App_Menu_Advanced_Visualization_Config] ([App_Menu_Advanced_Visualization_Config_Id])
GO
ALTER TABLE [dbo].[App_Menu_Client_Config] ADD CONSTRAINT [fk_App_Menu_Client_Config_APP_MENU_App_Menu_Id] FOREIGN KEY ([APP_MENU_ID]) REFERENCES [dbo].[APP_MENU] ([APP_MENU_ID])
GO
