CREATE TABLE [dbo].[Geographic_Region_Permission_Info_Map]
(
[Geographic_Region_Group_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Geographic_Region_Id] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Permission_Info_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Geographic_Region_Permission_Info_Map_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region_Permission_Info_Map] ADD CONSTRAINT [un_Geographic_Region_Permission_Info_Map__Geographic_Region_Id__Permission_Info_Id] UNIQUE NONCLUSTERED  ([Geographic_Region_Id], [Permission_Info_Id]) ON [DB_DATA01]
GO
