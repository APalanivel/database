CREATE TABLE [dbo].[hvr_stinr_h_d_c0002_c0002]
(
[last_integ_end] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__last___79FC7128] DEFAULT ('01-jan-1900 00:00:00'),
[trailer_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__trail__7AF09561] DEFAULT (''),
[trailer_cap_begin] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__trail__7BE4B99A] DEFAULT ('01-jan-1900 00:00:00'),
[trailer_cap_begin_int] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__trail__7CD8DDD3] DEFAULT ((0)),
[leader_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__leade__7DCD020C] DEFAULT (''),
[leader_cap_begin] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__leade__7EC12645] DEFAULT ('01-jan-1900 00:00:00'),
[leader_cap_begin_int] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__leade__7FB54A7E] DEFAULT ((0)),
[session_name] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__sessi__00A96EB7] DEFAULT (''),
[history_scan_load] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__histo__019D92F0] DEFAULT ((0)),
[history_scan_start_tstamp] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__histo__0291B729] DEFAULT ((0)),
[history_scan_start_addr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__histo__0385DB62] DEFAULT (''),
[history_emit_seq] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__histo__0479FF9B] DEFAULT ('')
) ON [DB_DATA01]
GO
GRANT SELECT ON  [dbo].[hvr_stinr_h_d_c0002_c0002] TO [public]
GO
