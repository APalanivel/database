CREATE TABLE [dbo].[Account_DMO_Config]
(
[Account_DMO_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[DMO_Start_Dt] [date] NOT NULL,
[DMO_End_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_DMO_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Account_DMO_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_DMO_Config] ADD CONSTRAINT [pk_Account_DMO_Config] PRIMARY KEY NONCLUSTERED  ([Account_DMO_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_DMO_Config] ADD CONSTRAINT [unc_Account_DMO_Config__Account_Id__Commodity_Id__IPO_Start_Dt__IPO_End_Dt] UNIQUE CLUSTERED  ([Account_Id], [Commodity_Id], [DMO_Start_Dt], [DMO_End_Dt]) ON [DB_DATA01]
GO
