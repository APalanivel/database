CREATE TABLE [dbo].[BASELOAD]
(
[BASELOAD_ID] [int] NOT NULL IDENTITY(1, 1),
[BASELOAD_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Baseload names for a Contract', 'SCHEMA', N'dbo', 'TABLE', N'BASELOAD', NULL, NULL
GO

ALTER TABLE [dbo].[BASELOAD] ADD 
CONSTRAINT [BASELOAD_PK] PRIMARY KEY CLUSTERED  ([BASELOAD_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

GO
