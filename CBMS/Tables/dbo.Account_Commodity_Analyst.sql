CREATE TABLE [dbo].[Account_Commodity_Analyst]
(
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Analyst_User_Info_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction Table mapping account and commodity to Analyst.  Used primarily by Sourcing.', 'SCHEMA', N'dbo', 'TABLE', N'Account_Commodity_Analyst', NULL, NULL
GO

ALTER TABLE [dbo].[Account_Commodity_Analyst] WITH NOCHECK ADD
CONSTRAINT [fk_Account_Commodity_Analyst__User_Info] FOREIGN KEY ([Analyst_User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[Account_Commodity_Analyst] ADD 
CONSTRAINT [PK_Account_Commodity_Analyst] PRIMARY KEY CLUSTERED  ([Account_Id], [Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Account_Commodity_Analyst_Analyst_User_Info_Id_incl] ON [dbo].[Account_Commodity_Analyst] ([Analyst_User_Info_Id]) INCLUDE ([Account_Id], [Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Account_Commodity_Analyst] ADD
CONSTRAINT [fk_Account_Commodity_Analyst__Account] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
ALTER TABLE [dbo].[Account_Commodity_Analyst] ADD
CONSTRAINT [fk_Account_Commodity_Analyst__Commodity] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])

GO
