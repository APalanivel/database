CREATE TABLE [dbo].[SR_RFP_SOP_SHORTLIST]
(
[SR_RFP_SOP_SHORTLIST_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_ACCOUNT_GROUP_ID] [int] NOT NULL,
[IS_BID_GROUP] [bit] NULL,
[REVISED_DUE_DATE] [datetime] NOT NULL,
[Time_Zone_Id] [int] NULL,
[Revised_Due_Dt_By_Timezone] [datetime] NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the master table to store the shortlisted product/term for each supplier contact bids for this RFP', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_SOP_SHORTLIST', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_SOP_SHORTLIST] ADD 
CONSTRAINT [PK_SR_RFP_SOP_SHORTLIST] PRIMARY KEY CLUSTERED  ([SR_RFP_SOP_SHORTLIST_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
