CREATE TABLE [dbo].[Group_Info_Category]
(
[Group_Info_Category_Id] [int] NOT NULL IDENTITY(1, 1),
[Category_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sort_Order] [int] NOT NULL,
[APP_MENU_PROFILE_ID] [int] NOT NULL
) ON [DBData_UserInfo]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Sets of Security Groups', 'SCHEMA', N'dbo', 'TABLE', N'Group_Info_Category', NULL, NULL
GO

ALTER TABLE [dbo].[Group_Info_Category] ADD CONSTRAINT [unc_Group_Info_Category_Category_Name_App_Menu_Profie_Id] UNIQUE CLUSTERED  ([Category_Name], [APP_MENU_PROFILE_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]

ALTER TABLE [dbo].[Group_Info_Category] ADD CONSTRAINT [PK_Group_Info_Category] PRIMARY KEY NONCLUSTERED  ([Group_Info_Category_Id]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]

CREATE NONCLUSTERED INDEX [ix_Group_Info_Category__App_Menu_Profile_Id] ON [dbo].[Group_Info_Category] ([APP_MENU_PROFILE_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Group_Info_Category] ADD
CONSTRAINT [fk__App_Menu_Profile_Id__Group_Info_Category] FOREIGN KEY ([APP_MENU_PROFILE_ID]) REFERENCES [dbo].[APP_MENU_PROFILE] ([APP_MENU_PROFILE_ID])


GO
