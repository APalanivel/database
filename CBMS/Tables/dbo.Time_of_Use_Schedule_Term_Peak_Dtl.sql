CREATE TABLE [dbo].[Time_of_Use_Schedule_Term_Peak_Dtl]
(
[Time_Of_Use_Schedule_Term_Peak_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Time_Of_Use_Schedule_Term_Peak_Id] [int] NOT NULL,
[Start_Time] [time] (0) NOT NULL,
[End_Time] [time] (0) NOT NULL,
[Day_Of_Week_Cd] [int] NULL,
[Is_Holiday_Active] [bit] NOT NULL CONSTRAINT [df_Time_Of_Use_Schedule_Term_Peak_Dtl__Is_Holiday_Active] DEFAULT ((0))
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'TOU Term Peak Details', 'SCHEMA', N'dbo', 'TABLE', N'Time_of_Use_Schedule_Term_Peak_Dtl', NULL, NULL
GO

CREATE UNIQUE CLUSTERED INDEX [ucix_Time_of_Use_Schedule_Term_Peak_Dtl] ON [dbo].[Time_of_Use_Schedule_Term_Peak_Dtl] ([Time_Of_Use_Schedule_Term_Peak_Id], [Start_Time], [End_Time], [Day_Of_Week_Cd], [Is_Holiday_Active]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_of_Use_Schedule_Term_Peak_Dtl] ADD CONSTRAINT [pk_Time_of_Use_Schedule_Term_Peak_Dtl] PRIMARY KEY NONCLUSTERED  ([Time_Of_Use_Schedule_Term_Peak_Dtl_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_of_Use_Schedule_Term_Peak_Dtl] ADD
CONSTRAINT [fk_Time_Of_Use_Schedule_Term_Peak__Time_of_Use_Schedule_Term_Peak_Dtl] FOREIGN KEY ([Time_Of_Use_Schedule_Term_Peak_Id]) REFERENCES [dbo].[Time_Of_Use_Schedule_Term_Peak] ([Time_Of_Use_Schedule_Term_Peak_Id])
GO
