CREATE TABLE [Workflow].[Workflow_Queue_Search_Filter_Param]
(
[Workflow_Queue_Search_Filter_Param_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Search_Filter_Id] [int] NOT NULL,
[Control_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Param_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Workflow___Creat__42B8B6B2] DEFAULT (getdate()),
[Data_Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Param] ADD CONSTRAINT [pk_Workflow_Queue_Search_Filter_Param] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Search_Filter_Param_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Param] ADD CONSTRAINT [uix_Workflow_Queue_Search_Filter_Param__Workflow_Queue_Search_Filter_Id__Control_Name__Param_Name] UNIQUE NONCLUSTERED  ([Workflow_Queue_Search_Filter_Id], [Control_Name], [Param_Name]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Param] ADD CONSTRAINT [fk_Workflow_Queue_Search_Filter__Workflow_Queue_Search_Filter_Param] FOREIGN KEY ([Workflow_Queue_Search_Filter_Id]) REFERENCES [Workflow].[Workflow_Queue_Search_Filter] ([Workflow_Queue_Search_Filter_Id])
GO
