CREATE TABLE [dbo].[RM_Default_Workflow]
(
[RM_Default_Workflow_Id] [int] NOT NULL IDENTITY(1, 1),
[Country_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Hedge_Type_Id] [int] NOT NULL,
[Workflow_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Default_WorkflowCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Default_WorkflowLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Default_Workflow] ADD CONSTRAINT [pk_RM_Default_Workflow] PRIMARY KEY CLUSTERED  ([RM_Default_Workflow_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Default_Workflow] ADD CONSTRAINT [un_RM_Default_Workflow_Country_Id__Commodity_Id__Hedge_Type_Id] UNIQUE NONCLUSTERED  ([Country_Id], [Commodity_Id], [Hedge_Type_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Default_Workflow] ADD CONSTRAINT [fk_RM_Default_Workflow__Workflow_Id] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
ALTER TABLE [dbo].[RM_Default_Workflow] ADD CONSTRAINT [fk_Workflow__RM_Default_Workflow] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
