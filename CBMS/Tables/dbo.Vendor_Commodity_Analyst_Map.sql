CREATE TABLE [dbo].[Vendor_Commodity_Analyst_Map]
(
[Vendor_Commodity_Analyst_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Analyst_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping from vendor commodity map table to analyst', 'SCHEMA', N'dbo', 'TABLE', N'Vendor_Commodity_Analyst_Map', NULL, NULL
GO

CREATE NONCLUSTERED INDEX [IX_Vendor_Commodity_Analyst_Map] ON [dbo].[Vendor_Commodity_Analyst_Map] ([Vendor_Commodity_Map_Id], [Analyst_Id]) ON [DB_INDEXES01]

GO
