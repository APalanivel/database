CREATE TABLE [dbo].[Variance_Extraction_Service]
(
[Variance_Extraction_Service_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Processing_Engine_Id] [int] NOT NULL,
[Extraction_Service_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extraction_Service_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Extraction_Service__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service] ADD CONSTRAINT [pk_Variance_Extraction_Service] PRIMARY KEY CLUSTERED  ([Variance_Extraction_Service_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service] ADD CONSTRAINT [un_Variance_Extraction_Service__Variance_Processing_Engine_Id__Extraction_Service_Name] UNIQUE NONCLUSTERED  ([Variance_Processing_Engine_Id], [Extraction_Service_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service] ADD CONSTRAINT [fk_Variance_Processing_Engine__Variance_Extraction_Service] FOREIGN KEY ([Variance_Processing_Engine_Id]) REFERENCES [dbo].[Variance_Processing_Engine] ([Variance_Processing_Engine_Id])
GO
