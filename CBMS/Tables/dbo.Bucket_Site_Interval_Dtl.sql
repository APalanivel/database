CREATE TABLE [dbo].[Bucket_Site_Interval_Dtl]
(
[Client_Hier_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Service_Start_Dt] [date] NOT NULL,
[Service_End_Dt] [date] NOT NULL,
[Bucket_Daily_Avg_Value] [decimal] (28, 10) NOT NULL,
[Data_Source_Cd] [int] NOT NULL,
[Uom_Type_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Bucket_Site_Interval_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Changed_Ts] [datetime] NOT NULL CONSTRAINT [df_Bucket_Site_Interval_Dtl__Last_Changed_Ts] DEFAULT (getdate())
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Accounts interval cost & Usage will be summarized to Site level and stored into this table.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Site_Interval_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[Bucket_Site_Interval_Dtl] ADD CONSTRAINT [pk_Bucket_Site_Interval_Dtl] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Bucket_Master_Id], [Data_Source_Cd], [Service_Start_Dt], [Service_End_Dt]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
ALTER TABLE [dbo].[Bucket_Site_Interval_Dtl] ENABLE CHANGE_TRACKING
GO
