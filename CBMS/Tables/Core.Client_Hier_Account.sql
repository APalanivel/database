CREATE TABLE [Core].[Client_Hier_Account]
(
[Account_Type] [char] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Account_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Invoice_Source_Cd] [int] NULL,
[Account_Is_Data_Entry_Only] [bit] NOT NULL,
[Account_Not_Expected] [bit] NOT NULL,
[Account_Not_Managed] [bit] NOT NULL,
[Account_Service_level_Cd] [int] NULL,
[Account_UBM_Id] [int] NULL,
[Account_UBM_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_UBM_Account_Code] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Vendor_Id] [int] NOT NULL,
[Account_Vendor_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Account_Eligibility_Dt] [date] NULL,
[Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY] [bit] NULL,
[Meter_Id] [int] NOT NULL,
[Meter_Number] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Address_ID] [int] NULL,
[Meter_Address_Line_1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Address_Line_2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_ZipCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_State_Id] [int] NULL,
[Meter_State_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Country_Id] [int] NULL,
[Meter_Country_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Geo_Lat] [decimal] (32, 16) NULL,
[Meter_Geo_Long] [decimal] (32, 16) NULL,
[Commodity_Id] [int] NOT NULL,
[Rate_Id] [int] NULL,
[Rate_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supplier_Account_begin_Dt] [date] NULL,
[Supplier_Account_End_Dt] [date] NULL,
[Supplier_Contract_ID] [int] NOT NULL,
[Supplier_Account_Recalc_Type_Cd] [int] NULL,
[Supplier_Account_Recalc_Type_Dsc] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Change_TS] [datetime] NULL CONSTRAINT [DF__Client_Hi__Last___3A2D42CA] DEFAULT (getdate()),
[Account_Group_ID] [int] NULL,
[Account_Number_Search] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Vendor_Type_ID] [int] NULL,
[Supplier_Meter_Association_Date] [date] NULL,
[Supplier_Meter_Disassociation_Date] [date] NULL,
[Meter_Number_Search] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display_Account_Number] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account_Not_Managed_Dt] [date] NULL,
[Account_Created_Ts] [datetime] NULL,
[Account_Not_Expected_Dt] [date] NULL,
[Account_Analyst_Mapping_Cd] [int] NULL,
[Account_Is_Broker] [bit] NOT NULL CONSTRAINT [df_Client_Hier_Account__Account_Is_Broker] DEFAULT ((0)),
[Template_Cu_Invoice_Id] [int] NULL,
[Template_Cu_Invoice_Updated_User_Id] [int] NULL,
[Template_Cu_Invoice_Last_Change_Ts] [datetime] NULL,
[Is_Invoice_Posting_Blocked] [bit] NOT NULL CONSTRAINT [df_Client_Hier_Account_Is_Invoice_Posting_Blocked] DEFAULT ((0)),
[Client_Hier_FTSearch_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Meter_Number_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_Number]),(0))) PERSISTED,
[Account_Number_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Account_Number]),(0))) PERSISTED,
[Account_Vendor_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Account_Vendor_Name]),(0))) PERSISTED,
[Account_UBM_Account_Code_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Account_UBM_Account_Code]),(0))) PERSISTED,
[Account_UBM_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Account_UBM_Name]),(0))) PERSISTED,
[Display_Account_Number_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Display_Account_Number]),(0))) PERSISTED,
[Meter_Address_Line_1_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_Address_Line_1]),(0))) PERSISTED,
[Meter_Address_Line_2_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_Address_Line_2]),(0))) PERSISTED,
[Meter_City_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_City]),(0))) PERSISTED,
[Meter_Country_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_Country_Name]),(0))) PERSISTED,
[Meter_State_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Meter_State_Name]),(0))) PERSISTED,
[Alternate_Account_Number] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Type_Cd] [int] NULL,
[Supplier_Account_Config_Id] [int] NOT NULL CONSTRAINT [df_Client_Hier_Account__Supplier_Account_Config_Id] DEFAULT ((-1))
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******  
NAME: tr_Client_Hier_Account_Change_Control  
  
DESCRIPTION: Trigger on Core.Client_Hier_Account to send change data to the   
 Change control service.   
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 CPE   Chaitanya Panduga Eshwar  
 HG   Hariharasuthan Ganesan  
 SP   Sandeep Pigilam   
 RR   Raghu Reddy  
 NR   Narayana Reddy  
  
 'core.tr_Client_Hier_Account_Change_Control'  
MODIFICATIONS  
  
 Initials Date  Modification  
------------------------------------------------------------  
 CPE   2013-02-26 Created  
 HG   2014-05-27 MAINT-2745, Modified to use new architecture of sending messages with fixed conversation group   
 HG   2014-07-11 Template_* columns added to CHA xml  
 SP   2014-07-30 For Data Operations Enhancement Phase IIII,Added [Is_Level2_Recalc_Validation_Required],Is_Invoice_Posting_Blocked columns.  
 HG   2015-07-17 CBMS Sourcing Enh - Alternate_Account_Number column added to xml message to push the changes to Other databases  
 RR   2015-10-20 Global Sourcing - Phase2 - Meter_Type_Cd column added to xml message   
 HG   2016-03-22 Is_Level2_Validation_Required column removed as the base column is removed from account table for AS400 enhancement  
 NR   2019-04-03  Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column.  
 NR   2019-06-11 Add COntract - Stop the Not yet assighned accounts from CBMS to hub. 
 RKV  2020-01-16  Removed UBM Details 
 NR	  2020-06-01	MAINT-10322 - Removed join filter on contract_Id in fullouter.
******/
CREATE TRIGGER [Core].[tr_Client_Hier_Account_Change_Control]
ON [Core].[Client_Hier_Account]
FOR INSERT, UPDATE, DELETE
AS
    BEGIN

        DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
            , @Message XML
            , @From_Service NVARCHAR(255)
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255);

        DECLARE @Client_Hier_Account_Changes TABLE
              (
                  SYS_CHANGE_OPERATION CHAR(1)
                  , Account_Type CHAR(8)
                  , Client_Hier_Id INT
                  , Account_Id INT
                  , Account_Number VARCHAR(50)
                  , Account_Invoice_Source_Cd INT
                  , Account_Is_Data_Entry_Only BIT
                  , Account_Not_Expected BIT
                  , Account_Not_Managed BIT
                  , Account_Service_level_Cd INT
                  , Account_Vendor_Id INT
                  , Account_Vendor_Name VARCHAR(200)
                  , Account_Eligibility_Dt DATE
                  , Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY BIT
                  , Meter_Id INT
                  , Meter_Number VARCHAR(50)
                  , Meter_Address_ID INT
                  , Meter_Address_Line_1 VARCHAR(200)
                  , Meter_Address_Line_2 VARCHAR(200)
                  , Meter_City VARCHAR(200)
                  , Meter_ZipCode VARCHAR(30)
                  , Meter_State_Id INT
                  , Meter_State_Name VARCHAR(200)
                  , Meter_Country_Id INT
                  , Meter_Country_Name VARCHAR(200)
                  , Meter_Geo_Lat DECIMAL(32, 16)
                  , Meter_Geo_Long DECIMAL(32, 16)
                  , Commodity_Id INT
                  , Rate_Id INT
                  , Rate_Name VARCHAR(200)
                  , Supplier_Account_begin_Dt DATE
                  , Supplier_Account_End_Dt DATE
                  , Supplier_Contract_ID INT
                  , Supplier_Account_Recalc_Type_Cd INT
                  , Supplier_Account_Recalc_Type_Dsc VARCHAR(25)
                  , Last_Change_TS DATETIME
                  , Account_Group_ID INT
                  , Account_Number_Search VARCHAR(8000)
                  , Account_Vendor_Type_ID INT
                  , Supplier_Meter_Association_Date DATE
                  , Supplier_Meter_Disassociation_Date DATE
                  , Meter_Number_Search VARCHAR(8000)
                  , Display_Account_Number VARCHAR(500)
                  , Account_Not_Managed_Dt DATE
                  , Account_Created_Ts DATETIME
                  , Account_Not_Expected_Dt DATE
                  , Account_Analyst_Mapping_Cd INT
                  , Account_Is_Broker BIT
                  , Template_Cu_Invoice_Id INT
                  , Template_Cu_Invoice_Updated_User_Id INT
                  , Template_Cu_Invoice_Last_Change_Ts DATETIME
                  , Is_Invoice_Posting_Blocked BIT
                  , Alternate_Account_Number NVARCHAR(200)
                  , Meter_Type_Cd INT
                  , Supplier_Account_Config_Id INT
                  , Deleted_Account_Number VARCHAR(50)
                  , Deleted_Account_Type CHAR(8)
              );

        INSERT INTO @Client_Hier_Account_Changes
             (
                 SYS_CHANGE_OPERATION
                 , Account_Type
                 , Client_Hier_Id
                 , Account_Id
                 , Account_Number
                 , Account_Invoice_Source_Cd
                 , Account_Is_Data_Entry_Only
                 , Account_Not_Expected
                 , Account_Not_Managed
                 , Account_Service_level_Cd
                 , Account_Vendor_Id
                 , Account_Vendor_Name
                 , Account_Eligibility_Dt
                 , Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY
                 , Meter_Id
                 , Meter_Number
                 , Meter_Address_ID
                 , Meter_Address_Line_1
                 , Meter_Address_Line_2
                 , Meter_City
                 , Meter_ZipCode
                 , Meter_State_Id
                 , Meter_State_Name
                 , Meter_Country_Id
                 , Meter_Country_Name
                 , Meter_Geo_Lat
                 , Meter_Geo_Long
                 , Commodity_Id
                 , Rate_Id
                 , Rate_Name
                 , Supplier_Account_begin_Dt
                 , Supplier_Account_End_Dt
                 , Supplier_Contract_ID
                 , Supplier_Account_Recalc_Type_Cd
                 , Supplier_Account_Recalc_Type_Dsc
                 , Last_Change_TS
                 , Account_Group_ID
                 , Account_Number_Search
                 , Account_Vendor_Type_ID
                 , Supplier_Meter_Association_Date
                 , Supplier_Meter_Disassociation_Date
                 , Meter_Number_Search
                 , Display_Account_Number
                 , Account_Not_Managed_Dt
                 , Account_Created_Ts
                 , Account_Not_Expected_Dt
                 , Account_Analyst_Mapping_Cd
                 , Account_Is_Broker
                 , Template_Cu_Invoice_Id
                 , Template_Cu_Invoice_Updated_User_Id
                 , Template_Cu_Invoice_Last_Change_Ts
                 , Is_Invoice_Posting_Blocked
                 , Alternate_Account_Number
                 , Meter_Type_Cd
                 , Supplier_Account_Config_Id
                 , Deleted_Account_Number
                 , Deleted_Account_Type
             )
        SELECT
            CASE WHEN ins.Client_Hier_Id IS NOT NULL
                      AND   del.Client_Hier_Id IS NULL THEN 'I'
                WHEN ins.Client_Hier_Id IS NOT NULL
                     AND del.Client_Hier_Id IS NOT NULL THEN 'U'
                WHEN ins.Client_Hier_Id IS NULL
                     AND del.Client_Hier_Id IS NOT NULL THEN 'D'
            END AS SYS_CHANGE_OPERATION
            , ins.Account_Type
            , ISNULL(ins.Client_Hier_Id, del.Client_Hier_Id) AS Client_Hier_Id
            , ISNULL(ins.Account_Id, del.Account_Id) AS Account_Id
            , ins.Account_Number
            , ins.Account_Invoice_Source_Cd
            , ins.Account_Is_Data_Entry_Only
            , ins.Account_Not_Expected
            , ins.Account_Not_Managed
            , ins.Account_Service_level_Cd
            , ins.Account_Vendor_Id
            , ins.Account_Vendor_Name
            , ins.Account_Eligibility_Dt
            , ins.Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY
            , ISNULL(ins.Meter_Id, del.Meter_Id) AS Meter_Id
            , ins.Meter_Number
            , ins.Meter_Address_ID
            , ins.Meter_Address_Line_1
            , ins.Meter_Address_Line_2
            , ins.Meter_City
            , ins.Meter_ZipCode
            , ins.Meter_State_Id
            , ins.Meter_State_Name
            , ins.Meter_Country_Id
            , ins.Meter_Country_Name
            , ins.Meter_Geo_Lat
            , ins.Meter_Geo_Long
            , ins.Commodity_Id
            , ins.Rate_Id
            , ins.Rate_Name
            , ins.Supplier_Account_begin_Dt
            , ins.Supplier_Account_End_Dt
            , ISNULL(ins.Supplier_Contract_ID, del.Supplier_Contract_ID) AS Supplier_Contract_ID
            , ins.Supplier_Account_Recalc_Type_Cd
            , ins.Supplier_Account_Recalc_Type_Dsc
            , ins.Last_Change_TS
            , ins.Account_Group_ID
            , ins.Account_Number_Search
            , ins.Account_Vendor_Type_ID
            , ins.Supplier_Meter_Association_Date
            , ins.Supplier_Meter_Disassociation_Date
            , ins.Meter_Number_Search
            , ins.Display_Account_Number
            , ins.Account_Not_Managed_Dt
            , ins.Account_Created_Ts
            , ins.Account_Not_Expected_Dt
            , ins.Account_Analyst_Mapping_Cd
            , ins.Account_Is_Broker
            , ins.Template_Cu_Invoice_Id
            , ins.Template_Cu_Invoice_Updated_User_Id
            , ins.Template_Cu_Invoice_Last_Change_Ts
            , ins.Is_Invoice_Posting_Blocked
            , ins.Alternate_Account_Number
            , ins.Meter_Type_Cd
            , ISNULL(ins.Supplier_Account_Config_Id, del.Supplier_Account_Config_Id) AS Supplier_Account_Config_Id
            , del.Account_Number AS Deleted_Account_Number
            , del.Account_Type AS Deleted_Account_Type
        FROM
            INSERTED ins
            FULL OUTER JOIN DELETED del
                ON del.Client_Hier_Id = ins.Client_Hier_Id
                   AND  del.Account_Id = ins.Account_Id
                   AND  del.Meter_Id = ins.Meter_Id
                   AND  del.Supplier_Account_Config_Id = ins.Supplier_Account_Config_Id;


        SET @Message = (   SELECT
                                CASE WHEN chac.Account_Type = 'Supplier'
                                          AND   chac.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                                          AND   chac.Account_Number = 'Not Yet Assigned'
                                          AND   chac.Deleted_Account_Number <> 'Not Yet Assigned'
                                          AND   chac.Deleted_Account_Number IS NOT NULL THEN 'D'
                                    ELSE chac.SYS_CHANGE_OPERATION
                                END AS SYS_CHANGE_OPERATION
                                , chac.Account_Type
                                , chac.Client_Hier_Id
                                , chac.Account_Id
                                , chac.Account_Number
                                , chac.Account_Invoice_Source_Cd
                                , chac.Account_Is_Data_Entry_Only
                                , chac.Account_Not_Expected
                                , chac.Account_Not_Managed
                                , chac.Account_Service_level_Cd
                                , chac.Account_Vendor_Id
                                , chac.Account_Vendor_Name
                                , chac.Account_Eligibility_Dt
                                , chac.Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY
                                , chac.Meter_Id
                                , chac.Meter_Number
                                , chac.Meter_Address_ID
                                , chac.Meter_Address_Line_1
                                , chac.Meter_Address_Line_2
                                , chac.Meter_City
                                , chac.Meter_ZipCode
                                , chac.Meter_State_Id
                                , chac.Meter_State_Name
                                , chac.Meter_Country_Id
                                , chac.Meter_Country_Name
                                , chac.Meter_Geo_Lat
                                , chac.Meter_Geo_Long
                                , chac.Commodity_Id
                                , chac.Rate_Id
                                , chac.Rate_Name
                                , chac.Supplier_Account_begin_Dt
                                , chac.Supplier_Account_End_Dt
                                , chac.Supplier_Contract_ID
                                , chac.Supplier_Account_Recalc_Type_Cd
                                , chac.Supplier_Account_Recalc_Type_Dsc
                                , chac.Last_Change_TS
                                , chac.Account_Group_ID
                                , chac.Account_Number_Search
                                , chac.Account_Vendor_Type_ID
                                , chac.Supplier_Meter_Association_Date
                                , chac.Supplier_Meter_Disassociation_Date
                                , chac.Meter_Number_Search
                                , chac.Display_Account_Number
                                , chac.Account_Not_Managed_Dt
                                , chac.Account_Created_Ts
                                , chac.Account_Not_Expected_Dt
                                , chac.Account_Analyst_Mapping_Cd
                                , chac.Account_Is_Broker
                                , chac.Template_Cu_Invoice_Id
                                , chac.Template_Cu_Invoice_Updated_User_Id
                                , chac.Template_Cu_Invoice_Last_Change_Ts
                                , chac.Is_Invoice_Posting_Blocked
                                , chac.Alternate_Account_Number
                                , chac.Meter_Type_Cd
                                , chac.Supplier_Account_Config_Id
                           FROM
                                @Client_Hier_Account_Changes chac
                           WHERE
                                (   (   chac.Account_Type = 'Utility'
                                        OR  chac.Deleted_Account_Type = 'Utility')
                                    OR  (   (   chac.Account_Type = 'Supplier'
                                                AND chac.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                                                AND chac.Account_Number <> 'Not Yet Assigned'
                                                AND (   chac.Deleted_Account_Number IS NULL
                                                        OR  chac.Deleted_Account_Number <> 'Not Yet Assigned'))
                                            OR  (   chac.Account_Type = 'Supplier'
                                                    AND chac.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                                                    AND chac.Account_Number = 'Not Yet Assigned'
                                                    AND chac.Deleted_Account_Number <> 'Not Yet Assigned'
                                                    AND chac.Deleted_Account_Number IS NOT NULL)
                                            OR  (   (   chac.Account_Type = 'Supplier'
                                                        OR  chac.Deleted_Account_Type = 'Supplier')
                                                    AND chac.SYS_CHANGE_OPERATION = 'D')))
                           GROUP BY
                               CASE WHEN chac.Account_Type = 'Supplier'
                                         AND chac.SYS_CHANGE_OPERATION IN ( 'I', 'U' )
                                         AND chac.Account_Number = 'Not Yet Assigned'
                                         AND chac.Deleted_Account_Number <> 'Not Yet Assigned'
                                         AND chac.Deleted_Account_Number IS NOT NULL THEN 'D'
                                   ELSE chac.SYS_CHANGE_OPERATION
                               END
                               , chac.Account_Type
                               , chac.Client_Hier_Id
                               , chac.Account_Id
                               , chac.Account_Number
                               , chac.Account_Invoice_Source_Cd
                               , chac.Account_Is_Data_Entry_Only
                               , chac.Account_Not_Expected
                               , chac.Account_Not_Managed
                               , chac.Account_Service_level_Cd
                               , chac.Account_Vendor_Id
                               , chac.Account_Vendor_Name
                               , chac.Account_Eligibility_Dt
                               , chac.Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY
                               , chac.Meter_Id
                               , chac.Meter_Number
                               , chac.Meter_Address_ID
                               , chac.Meter_Address_Line_1
                               , chac.Meter_Address_Line_2
                               , chac.Meter_City
                               , chac.Meter_ZipCode
                               , chac.Meter_State_Id
                               , chac.Meter_State_Name
                               , chac.Meter_Country_Id
                               , chac.Meter_Country_Name
                               , chac.Meter_Geo_Lat
                               , chac.Meter_Geo_Long
                               , chac.Commodity_Id
                               , chac.Rate_Id
                               , chac.Rate_Name
                               , chac.Supplier_Account_begin_Dt
                               , chac.Supplier_Account_End_Dt
                               , chac.Supplier_Contract_ID
                               , chac.Supplier_Account_Recalc_Type_Cd
                               , chac.Supplier_Account_Recalc_Type_Dsc
                               , chac.Last_Change_TS
                               , chac.Account_Group_ID
                               , chac.Account_Number_Search
                               , chac.Account_Vendor_Type_ID
                               , chac.Supplier_Meter_Association_Date
                               , chac.Supplier_Meter_Disassociation_Date
                               , chac.Meter_Number_Search
                               , chac.Display_Account_Number
                               , chac.Account_Not_Managed_Dt
                               , chac.Account_Created_Ts
                               , chac.Account_Not_Expected_Dt
                               , chac.Account_Analyst_Mapping_Cd
                               , chac.Account_Is_Broker
                               , chac.Template_Cu_Invoice_Id
                               , chac.Template_Cu_Invoice_Updated_User_Id
                               , chac.Template_Cu_Invoice_Last_Change_Ts
                               , chac.Is_Invoice_Posting_Blocked
                               , chac.Alternate_Account_Number
                               , chac.Meter_Type_Cd
                               , chac.Supplier_Account_Config_Id
                           FOR XML PATH('Client_Hier_Account_Change'), ELEMENTS, ROOT('Client_Hier_Account_Changes'));


        IF @Message IS NOT NULL
            BEGIN

                SET @From_Service = N'//Change_Control/Service/CBMS/Change_Control';

                -- Cycle through the targets in Change_Control Targt and sent the message to each  
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'Core.Client_Hier_Account');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Change_Control/Message/Client_Hier_Account_Changes]
                            (@Message);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;
    END;

    ;






GO
ALTER TABLE [Core].[Client_Hier_Account] ADD CONSTRAINT [pk_Client_Hier_Account] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Account_Id], [Meter_Id], [Supplier_Account_Config_Id], [Supplier_Contract_ID]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_Core_Client_Hier_Account__Account_Id__Client_Hier_Id__Account_Type] ON [Core].[Client_Hier_Account] ([Account_Id], [Client_Hier_Id], [Account_Type]) INCLUDE ([Account_Group_ID], [Account_Not_Managed], [Account_Number], [Account_Vendor_Id]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account__Account_Type__Account_Not_Managed__Account_Service_level_Cd] ON [Core].[Client_Hier_Account] ([Account_Type], [Account_Not_Managed], [Account_Service_level_Cd]) INCLUDE ([Account_Analyst_Mapping_Cd], [Account_Id], [Account_Number], [Account_Vendor_Id], [Account_Vendor_Name], [Client_Hier_Id], [Commodity_Id], [Meter_Id], [Supplier_Contract_ID]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_Core_Client_Hier_Account__Account_Type__Account_Vendor_Id_Include] ON [Core].[Client_Hier_Account] ([Account_Type], [Account_Vendor_Id]) INCLUDE ([Account_Id], [Account_Service_level_Cd], [Account_Vendor_Name], [Commodity_Id], [Meter_Id], [Supplier_Account_begin_Dt], [Supplier_Account_End_Dt]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account__Account_Type__Meter_Id] ON [Core].[Client_Hier_Account] ([Account_Type], [Meter_Id]) INCLUDE ([Account_Vendor_Id], [Account_Vendor_Name], [Account_Vendor_Type_ID], [Client_Hier_Id], [Supplier_Contract_ID]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_Core__Client_Hier_Account__Account_UBM_Id__Account_UBM_Account_Code] ON [Core].[Client_Hier_Account] ([Account_UBM_Id], [Account_UBM_Account_Code]) INCLUDE ([Account_Group_ID], [Account_Id], [Account_Number], [Account_Number_Search], [Account_Type], [Account_Vendor_Id], [Account_Vendor_Name], [Commodity_Id], [Is_Invoice_Posting_Blocked], [Meter_City], [Meter_Number], [Meter_Number_Search], [Meter_State_Name], [Supplier_Account_begin_Dt], [Supplier_Account_End_Dt], [Supplier_Contract_ID]) ON [DB_INDEXES01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_Client_Hier_Account__Client_Hier_FTSearch_ID] ON [Core].[Client_Hier_Account] ([Client_Hier_FTSearch_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account_Client_Hier_Id_Account_Not_Managed] ON [Core].[Client_Hier_Account] ([Client_Hier_Id], [Account_Not_Managed]) INCLUDE ([Account_Id], [Account_Number], [Account_Vendor_Name], [Commodity_Id], [Meter_Number]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Core_Client_Hier_Account__Commodity_Id] ON [Core].[Client_Hier_Account] ([Commodity_Id]) INCLUDE ([Meter_Id], [Rate_Name]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account__Meter_Id__Account_ID__Account_Type__Include] ON [Core].[Client_Hier_Account] ([Meter_Id], [Account_Id], [Account_Type]) INCLUDE ([Account_Number], [Account_Service_level_Cd], [Account_Vendor_Id], [Account_Vendor_Name], [Client_Hier_Id], [Commodity_Id], [Display_Account_Number], [Rate_Name], [Supplier_Contract_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account__Rate_Id_INC] ON [Core].[Client_Hier_Account] ([Rate_Id]) INCLUDE ([Account_Type]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account__Supplier_Contract_ID__Client_Hier_Id] ON [Core].[Client_Hier_Account] ([Supplier_Contract_ID], [Client_Hier_Id], [Account_Type], [Account_Vendor_Type_ID], [Account_Vendor_Name]) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Dimensional rollup of account level information Joining Supplier and utility Accounts with meter information', 'SCHEMA', N'Core', 'TABLE', N'Client_Hier_Account', NULL, NULL
GO
CREATE FULLTEXT INDEX ON [Core].[Client_Hier_Account] KEY INDEX [UIX_Client_Hier_Account__Client_Hier_FTSearch_ID] ON [CBMS_Client_Hier_Account_FTSearch]
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Type] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Account_Code] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Vendor_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_1] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_2] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_City] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_ZipCode] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_State_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Country_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Rate_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Supplier_Account_Recalc_Type_Dsc] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number_Search] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number_Search] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Display_Account_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Vendor_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Account_Code_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Display_Account_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_1_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_2_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_City_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Country_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_State_Name_FTSearch] LANGUAGE 1042)
GO
CREATE FULLTEXT INDEX ON [Core].[Client_Hier_Account] KEY INDEX [UIX_Client_Hier_Account__Client_Hier_FTSearch_ID] ON [CBMS_Client_Hier_Account_FTSearch]
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Account_Code] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Vendor_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_1] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_2] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_City] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_State_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Country_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number_Search] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number_Search] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Display_Account_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_Vendor_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Account_Code_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Account_UBM_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Display_Account_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_1_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Address_Line_2_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_City_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_Country_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ADD ([Meter_State_Name_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ENABLE
GO


ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ENABLE
GO


ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ENABLE
GO


ALTER FULLTEXT INDEX ON [Core].[Client_Hier_Account] ENABLE
GO
