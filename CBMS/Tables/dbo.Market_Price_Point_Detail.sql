CREATE TABLE [dbo].[Market_Price_Point_Detail]
(
[Market_Price_Point_ID] [int] NOT NULL,
[Market_Price_Point_Value] [decimal] (32, 16) NOT NULL,
[Market_Price_Point_Detail_Date] [smalldatetime] NOT NULL,
[Market_Price_Point_Future_Date] [smalldatetime] NOT NULL,
[rowguid] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__Market_Pr__rowgu__0AACDF1A] DEFAULT (newid())
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Detail of Price Points based on the named listing', 'SCHEMA', N'dbo', 'TABLE', N'Market_Price_Point_Detail', NULL, NULL
GO

ALTER TABLE [dbo].[Market_Price_Point_Detail] ADD 
CONSTRAINT [PK_MARKET_PRICE_POINT_DETAIL] PRIMARY KEY CLUSTERED  ([Market_Price_Point_ID], [Market_Price_Point_Detail_Date], [Market_Price_Point_Future_Date]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
CREATE UNIQUE NONCLUSTERED INDEX [index_229861623] ON [dbo].[Market_Price_Point_Detail] ([rowguid]) ON [DB_INDEXES01]



GO
