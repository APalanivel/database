CREATE TABLE [dbo].[RPT_CU_INVOICE_DETAIL]
(
[SESSION_UID] [uniqueidentifier] NOT NULL,
[ITEM_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CU_INVOICE_ID] [int] NOT NULL,
[COMMODITY_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UNIT_OF_MEASURE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ITEM_VALUE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Reports]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Charges and Determinants of invoices as a reporting table', 'SCHEMA', N'dbo', 'TABLE', N'RPT_CU_INVOICE_DETAIL', NULL, NULL
GO

CREATE CLUSTERED INDEX [CX_Session_UID] ON [dbo].[RPT_CU_INVOICE_DETAIL] ([SESSION_UID], [CU_INVOICE_ID]) WITH (FILLFACTOR=90) ON [DBData_Reports]



GO
