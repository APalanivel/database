CREATE TABLE [dbo].[Client_Invoice_DNT_Config_Country_Map]
(
[Client_Invoice_DNT_Config_Country_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Invoice_DNT_Config_Id] [int] NOT NULL,
[COUNTRY_ID] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config_Country_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Country_Map] ADD CONSTRAINT [pk_Client_Invoice_DNT_Config_Country_Map] PRIMARY KEY CLUSTERED  ([Client_Invoice_DNT_Config_Country_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Country_Map] ADD CONSTRAINT [un_Client_Invoice_DNT_Config_Country_Map__Client_Invoice_DNT_Config_Id__COUNTRY_ID] UNIQUE NONCLUSTERED  ([Client_Invoice_DNT_Config_Id], [COUNTRY_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Country_Map] ADD CONSTRAINT [fk_Client_Invoice_DNT_Config__Client_Invoice_DNT_Config_Country_Map] FOREIGN KEY ([Client_Invoice_DNT_Config_Id]) REFERENCES [dbo].[Client_Invoice_DNT_Config] ([Client_Invoice_DNT_Config_Id])
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_Country_Map] ADD CONSTRAINT [RefCOUNTRY2888] FOREIGN KEY ([COUNTRY_ID]) REFERENCES [dbo].[COUNTRY] ([COUNTRY_ID])
GO
