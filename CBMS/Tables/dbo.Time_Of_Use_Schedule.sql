CREATE TABLE [dbo].[Time_Of_Use_Schedule]
(
[Time_Of_Use_Schedule_Id] [int] NOT NULL IDENTITY(1, 1),
[Schedule_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Id] [int] NOT NULL,
[VENDOR_ID] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Rate Schedules Names for Time Of Use (TOU) Power rates in CBMS', 'SCHEMA', N'dbo', 'TABLE', N'Time_Of_Use_Schedule', NULL, NULL
GO

CREATE UNIQUE CLUSTERED INDEX [ucix_Time_Of_Use_Schedule__VENDOR_ID__Commodity_Id__Schedule_Name] ON [dbo].[Time_Of_Use_Schedule] ([VENDOR_ID], [Commodity_Id], [Schedule_Name]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule] ADD CONSTRAINT [pk_Time_Of_Use_Schedule] PRIMARY KEY NONCLUSTERED  ([Time_Of_Use_Schedule_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule__Commodity_Id] ON [dbo].[Time_Of_Use_Schedule] ([Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule__VENDOR_ID] ON [dbo].[Time_Of_Use_Schedule] ([VENDOR_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule__Comment_ID] ON [dbo].[Time_Of_Use_Schedule] ([Comment_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule] ADD
CONSTRAINT [fk_VENDOR__Time_Of_Use_Schedule] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])
ALTER TABLE [dbo].[Time_Of_Use_Schedule] ADD
CONSTRAINT [fk_Comment__Time_Of_Use_Schedule] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
