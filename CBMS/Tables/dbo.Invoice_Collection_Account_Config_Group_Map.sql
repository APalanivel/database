CREATE TABLE [dbo].[Invoice_Collection_Account_Config_Group_Map]
(
[IC_Account_Config_GroupId_Mapping_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Invoice_Collection_Account_Config_Group_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [DF_Invoice_Collection_Account_Config_GroupId_Mapping] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DF_IC_Account_Config_GroupId_Mapping_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config_Group_Map] ADD CONSTRAINT [PK_Invoice_Collection_Account_Config_GroupId_Mapping] PRIMARY KEY CLUSTERED  ([IC_Account_Config_GroupId_Mapping_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config_Group_Map] ADD CONSTRAINT [FK_Invoice_Collection_Account_Config_GroupId_Mapping__Invoice_Collection_Account_Config_Group] FOREIGN KEY ([Invoice_Collection_Account_Config_Group_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config_Group] ([Invoice_Collection_Account_Config_Group_ID])
GO
