CREATE TABLE [dbo].[Geographic_Region]
(
[Geographic_Region_Id] [int] NOT NULL IDENTITY(1, 1),
[Module_Cd] [int] NOT NULL,
[Geographic_Region_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Geographic_Region_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Geographic_Region__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Geographic_Region__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region] ADD CONSTRAINT [pk_Geographic_Region] PRIMARY KEY CLUSTERED  ([Geographic_Region_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region] ADD CONSTRAINT [un_Geographic_Region__Module_Cd__Geographic_Region_Name] UNIQUE NONCLUSTERED  ([Module_Cd], [Geographic_Region_Name]) ON [DB_DATA01]
GO
