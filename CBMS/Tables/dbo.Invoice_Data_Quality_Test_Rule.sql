CREATE TABLE [dbo].[Invoice_Data_Quality_Test_Rule]
(
[Invoice_Data_Quality_Test_Rule_Id] [int] NOT NULL IDENTITY(1, 1),
[Rule_Seq_No] [smallint] NOT NULL,
[Bucket_Master_Id] [int] NULL,
[Baseline_Bucket_Master_Id] [int] NULL,
[Validation_Type_Cd] [int] NOT NULL,
[Aggregation_Level_Cd] [int] NOT NULL,
[Default_Tolerance] [decimal] (16, 4) NULL,
[Tolerance_Value_Type_Cd] [int] NOT NULL,
[Tolerance_Arithmetic_Op_Cd] [int] NOT NULL,
[Current_Value_Month_Period_Cd] [int] NOT NULL,
[Current_Value_Month_Period_Operator_Cd] [int] NOT NULL,
[Current_Value_Month_Period_Operand] [int] NOT NULL,
[Baseline_Value_Operand] [int] NULL,
[Baseline_Starting_Period_Cd] [int] NOT NULL,
[Baseline_Starting_Period_Operator_Cd] [int] NOT NULL,
[Baseline_Starting_Period_Operand] [int] NOT NULL,
[Baseline_End_Period_Cd] [int] NOT NULL,
[Baseline_End_Period_Operator_Cd] [int] NOT NULL,
[Baseline_End_Period_Operand] [int] NOT NULL,
[Comparison_Operator_Cd] [int] NULL,
[Skip_Test_If_Data_Not_Exist] [bit] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Test_Rule__Skip_Test_If_Data_Not_Exist] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Test_Rule__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Test_Rule__Last_Change_Ts] DEFAULT (getdate()),
[Data_Quality_Test_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Test_Rule] ADD CONSTRAINT [pk_Invoice_Data_Quality_Test_Rule] PRIMARY KEY CLUSTERED  ([Invoice_Data_Quality_Test_Rule_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Test_Rule] ADD CONSTRAINT [fk_Bucket_Master__Invoice_Data_Quality_Test_Rule] FOREIGN KEY ([Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Test_Rule] ADD CONSTRAINT [fk_Data_Quality_Test__Invoice_Data_Quality_Test_Rule] FOREIGN KEY ([Data_Quality_Test_Id]) REFERENCES [dbo].[Data_Quality_Test] ([Data_Quality_Test_Id])
GO
