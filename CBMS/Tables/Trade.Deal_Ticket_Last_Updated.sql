CREATE TABLE [Trade].[Deal_Ticket_Last_Updated]
(
[Deal_Ticket_Last_Updated_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [Trade_Deal_Ticket_Last_Updated__Last_Change_Ts] DEFAULT (getdate()),
[Is_Transferred] [bit] NULL CONSTRAINT [Trade_Deal_Ticket_Last_Updated__Is_Transferred] DEFAULT ((0))
) ON [DB_DATA01]
GO
