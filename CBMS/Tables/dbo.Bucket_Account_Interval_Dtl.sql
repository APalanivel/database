CREATE TABLE [dbo].[Bucket_Account_Interval_Dtl]
(
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Service_Start_Dt] [date] NOT NULL,
[Service_End_Dt] [date] NOT NULL,
[Bucket_Daily_Avg_Value] [decimal] (28, 10) NOT NULL,
[Data_Source_Cd] [int] NOT NULL,
[Uom_Type_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Bucket_Account_Interval_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Changed_Ts] [datetime] NOT NULL CONSTRAINT [df_Bucket_Account_Interval_Dtl__Last_Changed_Ts] DEFAULT (getdate())
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Table to store account interval cost and usage which will be derived from cu_invoice.

Stores the mean value for the defined period.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', NULL, NULL
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Account_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'Default big decimal for Schneider Electric', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Bucket_Daily_Avg_Value'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Bucket_Master_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Client_Hier_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Created_User_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Currency_Unit_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Data_Source_Cd'
GO

EXEC sp_addextendedproperty N'MS_Description', 'Dates whose finest grain is one day.  This date marks the end of a date span, possibly an effectivity date end of some sort.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Service_End_Dt'
GO

EXEC sp_addextendedproperty N'MS_Description', 'Date-only Starting date.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Service_Start_Dt'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Uom_Type_Id'
GO

EXEC sp_addextendedproperty N'MS_Description', 'An identifying column which is a foreign key from a parent Primary Key that is defined under the ID domain.', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Account_Interval_Dtl', 'COLUMN', N'Updated_User_Id'
GO

ALTER TABLE [dbo].[Bucket_Account_Interval_Dtl] ADD CONSTRAINT [pk_Bucket_Account_Interval_Dtl] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Account_Id], [Bucket_Master_Id], [Data_Source_Cd], [Service_Start_Dt], [Service_End_Dt]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
ALTER TABLE [dbo].[Bucket_Account_Interval_Dtl] ENABLE CHANGE_TRACKING
GO
