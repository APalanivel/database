CREATE TABLE [Trade].[Rm_Budget_Comment]
(
[Rm_Budget_Comment_Id] [int] NOT NULL IDENTITY(1, 1),
[Rm_Budget_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Rm_Budget_Comment__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Comment] ADD CONSTRAINT [PK_Rm_Budget_Comment__Rm_Budget_Comment_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Comment] ADD CONSTRAINT [Uk_Trade_Rm_Budget_Comment] UNIQUE NONCLUSTERED  ([Rm_Budget_Id], [Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Comment] ADD CONSTRAINT [FK_Rm_Budget_Comment__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
