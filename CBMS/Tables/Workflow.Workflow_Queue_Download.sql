CREATE TABLE [Workflow].[Workflow_Queue_Download]
(
[Workflow_Queue_Download_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Download_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_Queue_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_Queue_Id] [int] NOT NULL,
[File_Name_Format] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extension_Type] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Download__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Download__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Download] ADD CONSTRAINT [pk_Workflow_Queue_Download] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Download_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Download] ADD CONSTRAINT [un_Workflow_Queue__Workflow_Queue_Download_Name__Workflow_Queue_File_Name] UNIQUE NONCLUSTERED  ([Workflow_Queue_Download_Name], [Workflow_Queue_File_Name]) ON [DB_DATA01]
GO
