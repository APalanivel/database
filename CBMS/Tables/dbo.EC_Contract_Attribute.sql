CREATE TABLE [dbo].[EC_Contract_Attribute]
(
[EC_Contract_Attribute_Id] [int] NOT NULL IDENTITY(1, 1),
[State_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[EC_Contract_Attribute_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Attribute_Type_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute] ADD CONSTRAINT [pk_EC_Contract_Attribute] PRIMARY KEY CLUSTERED  ([EC_Contract_Attribute_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute] ADD CONSTRAINT [unc_EC_Contract_Attribute__State_id__Commodity_id__EC_Contract_Attribute_Name] UNIQUE NONCLUSTERED  ([State_Id], [Commodity_Id], [EC_Contract_Attribute_Name]) ON [DB_DATA01]
GO
