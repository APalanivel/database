CREATE TABLE [dbo].[Invoice_Data_Quality_Test_Comment]
(
[Invoice_Data_Quality_Test_Comment_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Test_Comment__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Data_Quality_Test_Comment__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Test_Comment] ADD CONSTRAINT [pk_Invoice_Data_Quality_Test_Comment] PRIMARY KEY CLUSTERED  ([Invoice_Data_Quality_Test_Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Data_Quality_Test_Comment] ADD CONSTRAINT [un_Invoice_Data_Quality_Test_Comment__Cu_Invoice_Id__Comment_Id] UNIQUE NONCLUSTERED  ([Cu_Invoice_Id], [Comment_Id]) ON [DB_DATA01]
GO
