CREATE TABLE [dbo].[Client_App_Access_Role]
(
[Client_App_Access_Role_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[App_Access_Role_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[App_Access_Role_Dsc] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_App_Access_Role__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL,
[App_Access_Role_Type_Cd] [int] NOT NULL CONSTRAINT [DF__Client_Ap__App_A__25FF5A47] DEFAULT ((-1))
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client to Application module mapping.  This is based on modules purchased.', 'SCHEMA', N'dbo', 'TABLE', N'Client_App_Access_Role', NULL, NULL
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/******
NAME:	tr_Client_App_Access_Role_Transfer

DESCRIPTION: Trigger to send changes for DV2 server
through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	TP			Anoop
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
	TP			01 Mar 2016		Modified to add Hub replication message
******/
CREATE TRIGGER [dbo].[tr_Client_App_Access_Role_Transfer] ON [dbo].[Client_App_Access_Role]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT
                        isnull(i.Client_App_Access_Role_Id, d.Client_App_Access_Role_Id) AS Client_App_Access_Role_Id
                       ,i.App_Access_Role_Name
                       ,i.App_Access_Role_Dsc
                       ,i.Client_Id
                       ,i.Created_User_Id
                       ,i.Created_Ts
                       ,i.Updated_User_Id
                       ,i.Last_Change_Ts
                       ,i.App_Access_Role_Type_Cd
                       ,case WHEN i.Client_App_Access_Role_Id IS NOT NULL
                                  AND d.Client_App_Access_Role_Id IS NULL THEN 'I'
                             WHEN i.Client_App_Access_Role_Id IS NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'D'
                             WHEN i.Client_App_Access_Role_Id IS NOT NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'U'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Client_App_Access_Role_Id = d.Client_App_Access_Role_Id
            FOR
                       XML PATH('Client_App_Access_Role_Change')
                          ,ELEMENTS
                          ,ROOT('Client_App_Access_Role_Changes') )




      IF @Message IS NOT NULL 
            BEGIN
	
				
                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DV2/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/Client_App_Access_Role_Transfer'					
					
                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Client_App_Access_Role_Transfer] (@Message);

                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DVDEHub/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/Client_App_Access_Role_Transfer'					
					
                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Client_App_Access_Role_Transfer] (@Message);


            END

END;

GO


ALTER TABLE [dbo].[Client_App_Access_Role] ADD CONSTRAINT [pk_Client_App_Access_Role] PRIMARY KEY CLUSTERED  ([Client_App_Access_Role_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Client_App_Access_Role__App_Access_Role_Name__CLIENT_ID] ON [dbo].[Client_App_Access_Role] ([App_Access_Role_Name], [Client_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_App_Access_Role__CLIENT_ID] ON [dbo].[Client_App_Access_Role] ([Client_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Client_App_Access_Role] ADD CONSTRAINT [fk_Client__Client_App_Access_Role] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
