CREATE TABLE [meta].[Date_Dim_Expd]
(
[Calendar_Day_Id] [int] NOT NULL,
[Date_D] [date] NOT NULL,
[Date_Val] [int] NOT NULL,
[First_Day_Of_Month_d] [date] NOT NULL,
[Last_Day_Of_Month_d] [date] NOT NULL,
[First_Day_Of_Quarter_d] [date] NOT NULL,
[Last_Day_Of_Quarter_d] [date] NOT NULL,
[Days_In_Month_Num] [int] NOT NULL,
[Year_Num] [int] NOT NULL,
[Semi_Annual_Num] [int] NOT NULL,
[Quarter_Num] [int] NOT NULL,
[Month_Num] [int] NOT NULL,
[Week_Of_Year_Num] [int] NOT NULL,
[Week_Of_Quarter_Num] [int] NOT NULL,
[Week_Num] [int] NOT NULL,
[Full_Week_Of_Month_Num] [int] NOT NULL,
[Day_Of_Year_Num] [int] NOT NULL,
[Day_Of_Quarter_Num] [int] NOT NULL,
[Day_Of_Month_Num] [int] NOT NULL,
[Day_Of_Week_Num] [int] NOT NULL,
[Month_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Day_Of_Week_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Weekday] [bit] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Date Dimension Daily from 1/1/1900 - 12/31/2100', 'SCHEMA', N'meta', 'TABLE', N'Date_Dim_Expd', NULL, NULL
GO

ALTER TABLE [meta].[Date_Dim_Expd] ADD CONSTRAINT [pk_Date_Dim_Expd] PRIMARY KEY CLUSTERED  ([Calendar_Day_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
