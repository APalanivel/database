CREATE TABLE [dbo].[Variance_Batch_Rerun_Logs]
(
[Variance_Batch_Rerun_Log_id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NULL,
[Commodity_Id] [int] NULL,
[Service_Month] [date] NULL,
[Status_Cd] [int] NULL,
[Error_Msg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Hier_Id] [int] NULL,
[Created_Ts] [datetime2] NULL CONSTRAINT [DF_Variance_Batch_Rerun_Log_Created_TS] DEFAULT (getdate()),
[Changes_Ts] [datetime2] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Batch_Rerun_Logs] ADD CONSTRAINT [PK__Variance_Batch_Rerun_Log_id] PRIMARY KEY CLUSTERED  ([Variance_Batch_Rerun_Log_id]) ON [DB_DATA01]
GO
