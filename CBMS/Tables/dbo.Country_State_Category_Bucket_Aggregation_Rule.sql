CREATE TABLE [dbo].[Country_State_Category_Bucket_Aggregation_Rule]
(
[Country_State_Category_Bucket_Aggregation_Rule_Id] [int] NOT NULL IDENTITY(1, 1),
[Category_Bucket_Master_Id] [int] NOT NULL,
[Country_Id] [int] NULL,
[State_Id] [int] NULL,
[Priority_Order] [smallint] NOT NULL,
[CU_Aggregation_Level_Cd] [int] NOT NULL,
[Numerator_Formula] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Denominator_Formula] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Aggregate_Category_Bucket] [bit] NOT NULL CONSTRAINT [df__CSBCR_Is_Aggregate_Category_Bucket] DEFAULT ((0)),
[Is_Aggregate_When_All_Component_Is_Non_Zero] [bit] NOT NULL CONSTRAINT [df__CSBCR_Is_Aggregate_When_Data_Exist_For_All_Bucket] DEFAULT ((0))
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Country_State_Category_Bucket_Aggregation_Rule] ADD CONSTRAINT [PK_Country_State_Category_Bucket_Aggregation_Rule] PRIMARY KEY CLUSTERED  ([Country_State_Category_Bucket_Aggregation_Rule_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Country_State_Category_Bucket_Aggregation_Rule] ADD CONSTRAINT [unc_Country_State_Category_Bucket_Aggregation_Rule__Category_Bucket_Master_Id__COUNTRY_ID__STATE_ID__CU_Aggregation_Level_Cd] UNIQUE NONCLUSTERED  ([Category_Bucket_Master_Id], [Country_Id], [State_Id], [CU_Aggregation_Level_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Country_State_Category_Bucket_Aggregation_Rule] ADD CONSTRAINT [FK_Bucket_Master_CY_ST_Bucket_Category_Bucket_Aggregation_Rule] FOREIGN KEY ([Category_Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
