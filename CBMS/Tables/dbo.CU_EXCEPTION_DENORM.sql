CREATE TABLE [dbo].[CU_EXCEPTION_DENORM]
(
[CBMS_IMAGE_ID] [int] NOT NULL,
[CBMS_DOC_ID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CU_INVOICE_ID] [int] NOT NULL,
[QUEUE_ID] [int] NOT NULL,
[EXCEPTION_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCEPTION_STATUS_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SERVICE_MONTH] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_MANUAL] [bit] NOT NULL CONSTRAINT [DF_CU_EXCEPTION_DENORM_IS_MANUAL] DEFAULT (0),
[DATE_IN_QUEUE] [datetime] NOT NULL,
[SORT_ORDER] [int] NULL,
[Client_Hier_ID] [int] NULL,
[Account_ID] [int] NULL,
[UBM_Account_Number] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_Client_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_Site_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_State_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Commodity_Id] [int] NULL,
[Recalc_Failed_Reason_Type_cd] [int] NULL,
[Recalc_Type_cd] [int] NULL,
[Recalc_Response_Error_Dsc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date_In_CBMS] [datetime] NULL
) ON [DBData_Invoice]
GO
CREATE NONCLUSTERED INDEX [ix_Cu_Exception_Denorm_CHID_Account_Id] ON [dbo].[CU_EXCEPTION_DENORM] ([Client_Hier_ID], [Account_ID]) ON [DBData_Invoice]
GO
CREATE NONCLUSTERED INDEX [IX_CU_EXCEPTION_DENORM] ON [dbo].[CU_EXCEPTION_DENORM] ([CU_INVOICE_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [Idx_CU_EXCEPTION_DENORM] ON [dbo].[CU_EXCEPTION_DENORM] ([CU_INVOICE_ID], [Account_ID]) INCLUDE ([CBMS_DOC_ID], [Client_Hier_ID], [Date_In_CBMS], [DATE_IN_QUEUE], [EXCEPTION_STATUS_TYPE], [EXCEPTION_TYPE], [QUEUE_ID], [SERVICE_MONTH], [UBM_Account_Number], [UBM_Client_Name], [UBM_Site_Name]) ON [DBData_Invoice]
GO
CREATE NONCLUSTERED INDEX [test_index] ON [dbo].[CU_EXCEPTION_DENORM] ([EXCEPTION_TYPE]) INCLUDE ([CU_INVOICE_ID], [QUEUE_ID]) ON [DBData_Invoice]
GO
CREATE CLUSTERED INDEX [ci__cu_exception_denorm__queue_id__Include] ON [dbo].[CU_EXCEPTION_DENORM] ([QUEUE_ID], [IS_MANUAL], [Account_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
GRANT SELECT ON  [dbo].[CU_EXCEPTION_DENORM] TO [BPOReportRole]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Invoice Exception Denormalized.  Contains plain text of Exception type.  Use primarily by user queues.  Legacy.  Combination of CU_Exception, CU_Exception_Detail and CU_Exception_Type', 'SCHEMA', N'dbo', 'TABLE', N'CU_EXCEPTION_DENORM', NULL, NULL
GO
