CREATE TABLE [dbo].[Service_Broker_Message_Monitor]
(
[Queue_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message_Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message_Received_Ts] [datetime] NOT NULL,
[Message_Sequence_Number] [int] NOT NULL,
[Conversation_Group_Id] [uniqueidentifier] NOT NULL,
[Conversation_Handle] [uniqueidentifier] NOT NULL,
[Service_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Contract_name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Error_Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message_Body] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message_Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message_Completed_Ts] [datetime] NULL,
[Message_Batch_Count] [int] NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'History of service broker messages processed', 'SCHEMA', N'dbo', 'TABLE', N'Service_Broker_Message_Monitor', NULL, NULL
GO

CREATE CLUSTERED INDEX [ix_Message_Monitor_QueueName_ReceivedTS] ON [dbo].[Service_Broker_Message_Monitor] ([Queue_Name], [Message_Received_Ts]) ON [DB_DATA01]
GO
