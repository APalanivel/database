CREATE TABLE [dbo].[cbms_image]
(
[CBMS_IMAGE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CBMS_IMAGE_TYPE_ID] [int] NULL,
[CBMS_DOC_ID] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DATE_IMAGED] [datetime] NOT NULL,
[BILLING_DAYS_ADJUSTMENT] [smallint] NULL,
[CBMS_IMAGE_SIZE] [decimal] (18, 0) NULL,
[CONTENT_TYPE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INV_SOURCED_IMAGE_ID] [int] NULL,
[is_reported] [bit] NULL CONSTRAINT [DF__cbms_imag__is_re__3CEFDCD2] DEFAULT ((1)),
[Cbms_Image_Path] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[App_ConfigID] [int] NULL,
[CBMS_Image_Directory] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBMS_Image_FileName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBMS_Image_Location_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_cbms_Image_Last_Change_Ts] DEFAULT (getdate()),
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_77798E11_3D61_4238_81E4_CDA15C31FBB2_990352480] DEFAULT (newid()),
[CBMS_DOC_ID_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([CBMS_DOC_ID]),0)) PERSISTED
) ON [DBData_Image]
GO


ALTER TABLE [dbo].[cbms_image] ADD 
CONSTRAINT [CBMS_IMAGE_PK] PRIMARY KEY CLUSTERED  ([CBMS_IMAGE_ID]) WITH (FILLFACTOR=90) ON [DBData_Image]
CREATE NONCLUSTERED INDEX [IX_CBMS_IMAGE_CBMS_DOC_ID] ON [dbo].[cbms_image] ([CBMS_DOC_ID], [CBMS_IMAGE_ID], [CBMS_IMAGE_TYPE_ID], [DATE_IMAGED], [INV_SOURCED_IMAGE_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_CBMS_IMAGE__CBMS_IMAGE_ID__CBMS_DOC_ID] ON [dbo].[cbms_image] ([CBMS_IMAGE_ID], [CBMS_DOC_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_CBMS_IMAGE_CBMS_IMAGE_TYPE_ID_CBMS_IMAGE_ID] ON [dbo].[cbms_image] ([CBMS_IMAGE_TYPE_ID], [CBMS_IMAGE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

GO


CREATE FULLTEXT INDEX ON [dbo].[cbms_image] KEY INDEX [CBMS_IMAGE_PK] ON [CBMS_CBMS_Image_FTSearch]
GO

ALTER FULLTEXT INDEX ON [dbo].[cbms_image] ADD ([CBMS_DOC_ID] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [dbo].[cbms_image] ADD ([CBMS_DOC_ID_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [dbo].[cbms_image] ENABLE
GO

EXEC sp_addextendedproperty N'MS_Description', 'Images available to the applications', 'SCHEMA', N'dbo', 'TABLE', N'cbms_image', NULL, NULL
GO
