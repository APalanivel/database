CREATE TABLE [dbo].[Variance_Rules_Exclusion]
(
[Variance_Rules_Exclusion_id] [int] NOT NULL IDENTITY(1, 1),
[Commodity_id] [int] NULL,
[Country_id] [int] NULL,
[Category_id] [int] NULL,
[Baseline_Cd] [int] NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime2] NULL CONSTRAINT [DF__Variance___Creat__7AF31A27] DEFAULT (getdate()),
[Updated_user_id] [int] NULL,
[Last_Change_TS] [datetime2] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Rules_Exclusion] ADD CONSTRAINT [PK_Variance_Rules_Exclusion_id] PRIMARY KEY CLUSTERED  ([Variance_Rules_Exclusion_id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [IX_Test] ON [dbo].[Variance_Rules_Exclusion] ([Country_id], [Category_id], [Baseline_Cd]) ON [DB_DATA01]
GO
