CREATE TABLE [dbo].[Invoice_Collection_Global_Config_Value]
(
[Invoice_Collection_Global_Config_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Invoice_Frequency_Cd] [int] NOT NULL,
[Invoice_Frequency_Pattern_Cd] [int] NOT NULL,
[Expected_Invoice_Raised_Day] [tinyint] NOT NULL,
[Expected_Invoice_Raised_Month] [tinyint] NOT NULL,
[Expected_Invoice_Raised_Day_Lag] [tinyint] NOT NULL,
[Expected_Invoice_Received_Day] [tinyint] NOT NULL,
[Expected_Invoice_Received_Month] [tinyint] NOT NULL,
[Expected_Invoice_Received_Day_Lag] [tinyint] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Global_Config_Value__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Global_Config_Value__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Global_Config_Value] ADD CONSTRAINT [pk_Invoice_Collection_Global_Config_Value] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Global_Config_Value_Id]) ON [DB_DATA01]
GO
