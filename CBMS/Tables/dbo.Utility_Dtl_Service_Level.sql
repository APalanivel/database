CREATE TABLE [dbo].[Utility_Dtl_Service_Level]
(
[Utility_Dtl_Service_Level_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Industrial_Flag_Cd] [int] NOT NULL,
[Commercial_Flag_Cd] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'captures the ‘Level of Service’ section related questions/information', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Service_Level', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Service_Level] ADD CONSTRAINT [unc_Utility_Dtl_Service_Level__Utllity_Question__Commodity_Map_id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Service_Level] ADD CONSTRAINT [pk_Utility_Dtl_Service_Level] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Service_Level_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Service_Level__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Service_Level] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Service_Level__Comment_ID] ON [dbo].[Utility_Dtl_Service_Level] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Service_Level] ADD
CONSTRAINT [fk_Question_Commodity_Map__Utility_Dtl_Service_Level] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Service_Level] ADD
CONSTRAINT [fk_VENDOR_COMMODITY_MAP_Utility_Dtl_Service_Level] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Service_Level] ADD
CONSTRAINT [fk_Comment__Utility_Dtl_Service_Level] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
