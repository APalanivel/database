CREATE TABLE [dbo].[CBMS_Image_Location]
(
[CBMS_Image_Location_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Image_Drive] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active_Directory] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active_File_Cnt] [int] NOT NULL CONSTRAINT [df_CBMS_Image_Location_Active_File_Cnt] DEFAULT ((0)),
[Max_File_Cnt] [int] NOT NULL CONSTRAINT [df_CBMS_Image_Location_Max_File_Cnt] DEFAULT ((1000)),
[Is_Active] [bit] NOT NULL CONSTRAINT [df_CBMS_Image_Location_Is_Active] DEFAULT ((1)),
[Start_Dt] [datetime] NOT NULL CONSTRAINT [df_CBMS_Image_Location_Start_Dt] DEFAULT ('01/01/1900'),
[End_Dt] [datetime] NOT NULL CONSTRAINT [df_CBMD_Image_Location_End_Dt] DEFAULT ('12/31/2099'),
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [DF__CBMS_Imag__msrep__512BDFA9] DEFAULT (newid()),
[Authentication_Type_Cd] [int] NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Storage_Type_Cd] [int] NULL
) ON [DBData_Image]
ALTER TABLE [dbo].[CBMS_Image_Location] ADD 
CONSTRAINT [pk_CBMS_Image_Location] PRIMARY KEY CLUSTERED  ([CBMS_Image_Location_Id]) WITH (FILLFACTOR=90) ON [DBData_Image]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Contains details about the path where images are stored', 'SCHEMA', N'dbo', 'TABLE', N'CBMS_Image_Location', NULL, NULL
GO
