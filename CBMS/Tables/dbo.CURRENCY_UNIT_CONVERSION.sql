CREATE TABLE [dbo].[CURRENCY_UNIT_CONVERSION]
(
[CURRENCY_GROUP_ID] [int] NOT NULL,
[BASE_UNIT_ID] [int] NOT NULL,
[CONVERTED_UNIT_ID] [int] NOT NULL,
[CONVERSION_FACTOR] [decimal] (32, 16) NOT NULL,
[CONVERSION_DATE] [datetime] NOT NULL
) ON [DBData_Cost_Usage]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Currency_Unit_Conversion_Transmission

DESCRIPTION: Trigger on Currency_Unit_Conversion to send the changes to Sys

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2016-02-09	Created to send the Currency conversion changes to sys
******/
CREATE TRIGGER [dbo].[tr_Currency_Unit_Conversion_Transmission] ON [dbo].[CURRENCY_UNIT_CONVERSION]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Currency_Conversion XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255)
           ,@EC_Transmission_Currency_Conv_Cut_Off_Dt DATE
           ,@EC_Tranmission_Currency_Conversion_Group VARCHAR(MAX);

      DECLARE @Currency_Group_Id TABLE
            (
             Currency_Group_Id INT );

      SELECT
            @EC_Tranmission_Currency_Conversion_Group = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'EC_Transmission_Currency_Conv_Group';

      SELECT
            @EC_Transmission_Currency_Conv_Cut_Off_Dt = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'EC_Transmission_Currency_Conv_Cut_Off_Dt';

      INSERT      INTO @Currency_Group_Id
                  (Currency_Group_Id )
                  SELECT
                        cg.currency_group_id
                  FROM
                        dbo.ufn_split(@EC_Tranmission_Currency_Conversion_Group, ',') cgv
                        INNER JOIN dbo.CURRENCY_GROUP cg
                              ON cg.currency_group_name = cgv.Segments
                  GROUP BY
                        cg.currency_group_id;

      SET @Message_Currency_Conversion = ( SELECT
                                                bcu.CURRENCY_UNIT_NAME AS Base_Currency_Unit
                                               ,ccu.CURRENCY_UNIT_NAME AS Target_Currency_Unit
                                               ,dd.FIRST_DAY_OF_MONTH_D AS First_Day_Of_Month
                                               ,dd.LAST_DAY_OF_MONTH_D AS Last_Day_Of_Month
                                               ,curconv.CONVERSION_FACTOR AS Conversion_Factor
											   ,cg.currency_group_name AS Currency_Group
                                               ,curconv.Sys_Change_Operation
                                               ,@@SERVERNAME AS Server_Name
                                           FROM
                                                ( SELECT
                                                      ISNULL(ins.CURRENCY_GROUP_ID, del.CURRENCY_GROUP_ID) AS Currency_Group_Id
                                                     ,ISNULL(ins.BASE_UNIT_ID, del.BASE_UNIT_ID) AS Base_Unit_Id
                                                     ,ISNULL(ins.CONVERTED_UNIT_ID, del.CONVERTED_UNIT_ID) AS Converted_Unit_Id
                                                     ,ISNULL(ins.CONVERSION_DATE, del.CONVERSION_DATE) AS Conversion_Date
                                                     ,ins.CONVERSION_FACTOR
                                                     ,CASE WHEN ins.CURRENCY_GROUP_ID IS NULL
                                                                AND del.CURRENCY_GROUP_ID IS NOT NULL THEN 'D'
                                                           WHEN ins.CURRENCY_GROUP_ID IS NOT NULL
                                                                AND del.CURRENCY_GROUP_ID IS NOT NULL THEN 'U'
                                                           WHEN ins.CURRENCY_GROUP_ID IS NOT NULL
                                                                AND del.CURRENCY_GROUP_ID IS NULL THEN 'I'
                                                      END AS Sys_Change_Operation
                                                  FROM
                                                      Inserted ins
                                                      FULL JOIN Deleted del
                                                            ON del.CURRENCY_GROUP_ID = ins.CURRENCY_GROUP_ID
                                                               AND del.BASE_UNIT_ID = ins.BASE_UNIT_ID
                                                               AND del.CONVERTED_UNIT_ID = ins.CONVERTED_UNIT_ID
                                                               AND del.CONVERSION_DATE = ins.CONVERSION_DATE ) curconv
                                                INNER JOIN dbo.CURRENCY_UNIT bcu
                                                      ON bcu.CURRENCY_UNIT_ID = curconv.Base_Unit_Id
                                                INNER JOIN dbo.CURRENCY_UNIT ccu
                                                      ON ccu.CURRENCY_UNIT_ID = curconv.Converted_Unit_Id
                                                INNER JOIN meta.DATE_DIM dd
                                                      ON dd.DATE_D = curconv.Conversion_Date
												INNER JOIN dbo.CURRENCY_GROUP cg
													ON cg.currency_group_id = curconv.Currency_Group_Id
                                           WHERE
                                                curconv.Conversion_Date >= @EC_Transmission_Currency_Conv_Cut_Off_Dt
                                                AND EXISTS ( SELECT
                                                                  1
                                                             FROM
                                                                  @Currency_Group_Id ccg
                                                             WHERE
                                                                  ccg.Currency_Group_Id = curconv.Currency_Group_Id )
            FOR
                                           XML PATH('Currency_Conversion_Change')
                                              ,ELEMENTS
                                              ,ROOT('Currency_Conversion_Changes') );

      IF @Message_Currency_Conversion IS NOT NULL
            BEGIN

                  SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Currency_And_Price_Change';

				  -- Cycle through the targets in Change_Control Targt and sent the message to each
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        dbo.Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Currency_Unit_Conversion');

                  OPEN cDialog;
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                  WHILE @@Fetch_Status = 0
                        BEGIN

                              EXEC dbo.Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                              SEND ON CONVERSATION @Conversation_Handle
											MESSAGE TYPE [//Transmission/Message/Cbms_Currency_Conversion_Change] (@Message_Currency_Conversion);

                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                        END;
                  CLOSE cDialog;
                  DEALLOCATE cDialog;

            END;

END;
;
GO

EXEC sp_addextendedproperty N'MS_Description', 'Conversion factor for converting between currencies', 'SCHEMA', N'dbo', 'TABLE', N'CURRENCY_UNIT_CONVERSION', NULL, NULL
GO

ALTER TABLE [dbo].[CURRENCY_UNIT_CONVERSION] ADD 
CONSTRAINT [PK_CURRENCY_UNIT_CONVERSION] PRIMARY KEY CLUSTERED  ([CURRENCY_GROUP_ID], [BASE_UNIT_ID], [CONVERTED_UNIT_ID], [CONVERSION_FACTOR], [CONVERSION_DATE]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
CREATE NONCLUSTERED INDEX [IX_CONVERTED_UNIT_ID] ON [dbo].[CURRENCY_UNIT_CONVERSION] ([CONVERTED_UNIT_ID], [CONVERSION_DATE]) ON [DB_INDEXES01]



GO
