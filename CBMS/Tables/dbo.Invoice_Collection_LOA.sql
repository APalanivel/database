CREATE TABLE [dbo].[Invoice_Collection_LOA]
(
[Invoice_Collection_LOA_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Client_Config_Id] [int] NOT NULL,
[Seq_No] [smallint] NOT NULL,
[LOA_Valid_From_Dt] [date] NOT NULL,
[LOA_Valid_End_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_LOA__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_LOA__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA] ADD CONSTRAINT [pk_Invoice_Collection_LOA] PRIMARY KEY CLUSTERED  ([Invoice_Collection_LOA_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA] ADD CONSTRAINT [un_Invoice_Collection_LOA__Invoice_Collection_Client_Config_Id__Seq_No] UNIQUE NONCLUSTERED  ([Invoice_Collection_Client_Config_Id], [Seq_No]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA] ADD CONSTRAINT [fk_Invoice_Collection_Client_Config__Invoice_Collection_LOA] FOREIGN KEY ([Invoice_Collection_Client_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Client_Config] ([Invoice_Collection_Client_Config_Id])
GO
