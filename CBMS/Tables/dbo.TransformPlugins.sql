CREATE TABLE [dbo].[TransformPlugins]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[GroupingKey] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[Name] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[OriginalCreationDate] [datetime] NULL,
[OriginallyPublishedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[LastPublishedOn] [datetime] NOT NULL CONSTRAINT [DF_TransformPlugins_LastPublishedOn] DEFAULT (getutcdate()),
[LastPublishedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[LastConfiguredOn] [datetime] NOT NULL CONSTRAINT [DF_TransformPlugins_LastConfiguredOn] DEFAULT (getutcdate()),
[LastConfiguredBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[PublishPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[ExecutionPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[Status] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[InputExtension] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[OutputExtension] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[SpecifiedPollPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[SpecifiedOutputPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[SpecifiedPollInterval] [int] NOT NULL CONSTRAINT [DF_TransformPlugins_SpecifiedPollInterval] DEFAULT ((60)),
[IsActive] [bit] NOT NULL CONSTRAINT [DF_TransformPlugins_IsActive] DEFAULT ((0)),
[CorrelationId] [uniqueidentifier] NOT NULL,
[TransformType_Id] [bigint] NULL,
[Description] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[MappingConfigData] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformPlugins] ADD CONSTRAINT [PK_dbo.TransformPlugins] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TranformType_Id] ON [dbo].[TransformPlugins] ([TransformType_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformPlugins] ADD CONSTRAINT [FK_dbo.TransformPlugins_dbo.TransformTypes_TranformType_Id] FOREIGN KEY ([TransformType_Id]) REFERENCES [dbo].[TransformTypes] ([Id])
GO
