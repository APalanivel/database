CREATE TABLE [dbo].[CONTRACT_METER_VOLUME]
(
[CONTRACT_METER_VOLUME_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[METER_ID] [int] NOT NULL,
[CONTRACT_ID] [int] NOT NULL,
[VOLUME] [numeric] (32, 16) NULL,
[MONTH_IDENTIFIER] [datetime] NOT NULL,
[UNIT_TYPE_ID] [int] NULL,
[FREQUENCY_TYPE_ID] [int] NULL,
[Is_Edited] [bit] NOT NULL CONSTRAINT [df_Contract_Meter_Volume_Is_Edited] DEFAULT ((0))
) ON [DBData_Contract_RateLibrary]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Contract_Meter_Volume_Info_Transfer

DESCRIPTION: Trigger on SUPPLIER_ACCOUNT_METER_MAP to send change data to the 
	Change control service. 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	RKV			2015-12-14	Created
	NR			2017-09-13	SE2017-251 -  Excluded the Utility Contract types.
	RR			2020-04-27	SE2017-688 Added timeOfDay_Id(Bucket_Master_Id) and timeOfDay(Bucket_Name)
******/
CREATE TRIGGER [dbo].[tr_Contract_Meter_Volume_Info_Transfer]
ON [dbo].[CONTRACT_METER_VOLUME]
FOR INSERT, UPDATE, DELETE
AS
    BEGIN

        DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
            , @Message_Contract_Meter_Volume XML
            , @From_Service NVARCHAR(255)
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255);


        DECLARE @Contract_Type_Id_List VARCHAR(MAX);

        SELECT
            @Contract_Type_Id_List = ac.App_Config_Value
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Exclude_Contract_Type_Id_List'
            AND ac.Is_Active = 1;






        SET @Message_Contract_Meter_Volume = (   SELECT
                                                        CASE WHEN Ins.CONTRACT_ID IS NOT NULL
                                                                  AND  del.CONTRACT_ID IS NULL THEN 'I'
                                                            WHEN Ins.CONTRACT_ID IS NOT NULL
                                                                 AND   del.CONTRACT_ID IS NOT NULL THEN 'U'
                                                            WHEN Ins.CONTRACT_ID IS NULL
                                                                 AND   del.CONTRACT_ID IS NOT NULL THEN 'D'
                                                        END AS Sys_Change_Operation
                                                        , ISNULL(
                                                              Ins.CONTRACT_METER_VOLUME_ID, del.CONTRACT_METER_VOLUME_ID) AS Contract_Meter_Volume_Id
                                                        , ISNULL(Ins.CONTRACT_ID, del.CONTRACT_ID) Contract_Id
                                                        , ISNULL(Ins.METER_ID, del.METER_ID) Meter_Id
                                                        , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                    AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                              ELSE Ins.VOLUME
                                                          END Volume
                                                        , '' AS timeOfDay_Id
                                                        , '' AS timeOfDay
                                                        , ISNULL(Ins.MONTH_IDENTIFIER, del.MONTH_IDENTIFIER) AS [Month]
                                                        , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                    AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                              ELSE e.ENTITY_NAME
                                                          END UOM
                                                        , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                    AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                              ELSE Ins.UNIT_TYPE_ID
                                                          END Uom_Id
                                                        , @@SERVERNAME AS Server_Name
                                                        , e1.ENTITY_NAME AS Meter_Volume_Frequency_Type
                                                        , ISNULL(Ins.FREQUENCY_TYPE_ID, del.FREQUENCY_TYPE_ID) AS Meter_Volume_Frequency_Type_Id
                                                 FROM
                                                        INSERTED Ins
                                                        FULL OUTER JOIN DELETED del
                                                            ON del.CONTRACT_ID = Ins.CONTRACT_ID
                                                               AND del.METER_ID = Ins.METER_ID
                                                        INNER JOIN dbo.ENTITY e
                                                            ON (e.ENTITY_ID = ISNULL(Ins.UNIT_TYPE_ID, del.UNIT_TYPE_ID))
                                                        INNER JOIN dbo.CONTRACT c
                                                            ON (c.CONTRACT_ID = ISNULL(Ins.CONTRACT_ID, del.CONTRACT_ID))
                                                        INNER JOIN dbo.ENTITY e1
                                                            ON (e1.ENTITY_ID = ISNULL(
                                                                                   Ins.FREQUENCY_TYPE_ID
                                                                                   , del.FREQUENCY_TYPE_ID))
                                                 WHERE
                                                     c.Is_Published_To_EC = 1
                                                     AND NOT EXISTS (   SELECT
                                                                            1
                                                                        FROM
                                                                            dbo.ufn_split(@Contract_Type_Id_List, ',') us
                                                                        WHERE
                                                                            c.CONTRACT_TYPE_ID = us.Segments)
                                                     AND NOT EXISTS (   SELECT
                                                                            1
                                                                        FROM
                                                                            dbo.Code cd
                                                                            INNER JOIN dbo.Codeset cs
                                                                                ON cs.Codeset_Id = cd.Codeset_Id
                                                                        WHERE
                                                                            cs.Codeset_Name = 'Contractmetervolumetype'
                                                                            AND cd.Code_Value = 'On/Off Peak'
                                                                            AND cd.Code_Id = c.Contract_Meter_Volume_Type_Cd)
                                                 /*Total volumes meesage(dbo.CONTRACT_METER_VOLUME data) should not send if the volume format is On/Off Peak*/
                                                 GROUP BY
                                                     CASE WHEN Ins.CONTRACT_ID IS NOT NULL
                                                               AND  del.CONTRACT_ID IS NULL THEN 'I'
                                                         WHEN Ins.CONTRACT_ID IS NOT NULL
                                                              AND   del.CONTRACT_ID IS NOT NULL THEN 'U'
                                                         WHEN Ins.CONTRACT_ID IS NULL
                                                              AND   del.CONTRACT_ID IS NOT NULL THEN 'D'
                                                     END
                                                     , ISNULL(
                                                           Ins.CONTRACT_METER_VOLUME_ID, del.CONTRACT_METER_VOLUME_ID)
                                                     , ISNULL(Ins.CONTRACT_ID, del.CONTRACT_ID)
                                                     , ISNULL(Ins.METER_ID, del.METER_ID)
                                                     , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                 AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                           ELSE Ins.VOLUME
                                                       END
                                                     , ISNULL(Ins.MONTH_IDENTIFIER, del.MONTH_IDENTIFIER)
                                                     , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                 AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                           ELSE e.ENTITY_NAME
                                                       END
                                                     , CASE WHEN Ins.CONTRACT_ID IS NULL
                                                                 AND del.CONTRACT_ID IS NOT NULL THEN NULL
                                                           ELSE Ins.UNIT_TYPE_ID
                                                       END
                                                     , e1.ENTITY_NAME
                                                     , ISNULL(Ins.FREQUENCY_TYPE_ID, del.FREQUENCY_TYPE_ID)
                                                 FOR XML PATH('Contract_Meter_Volume_Change'), ELEMENTS, ROOT('Contract_Meter_Volume_Changes'));

        IF @Message_Contract_Meter_Volume IS NOT NULL
            BEGIN

                SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                -- Cycle through the targets in Change_Control Targt and sent the message to each
                DECLARE cDialog CURSOR FOR(
                    SELECT
                        Target_Service
                        , Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Contract_Meter_Volume');
                OPEN cDialog;
                FETCH NEXT FROM cDialog
                INTO
                    @Target_Service
                    , @Target_Contract;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        EXEC Service_Broker_Conversation_Handle_Sel
                            @From_Service
                            , @Target_Service
                            , @Target_Contract
                            , @Conversation_Handle OUTPUT;
                        SEND ON CONVERSATION
                            @Conversation_Handle
                            MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Volume_Change]
                            (@Message_Contract_Meter_Volume);

                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                    END;
                CLOSE cDialog;
                DEALLOCATE cDialog;

            END;



    END;
GO
ALTER TABLE [dbo].[CONTRACT_METER_VOLUME] ADD CONSTRAINT [CONTRACT_METER_VOLUME_PK] PRIMARY KEY CLUSTERED  ([CONTRACT_METER_VOLUME_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [CONTRACT_METER_VOLUME_N_2] ON [dbo].[CONTRACT_METER_VOLUME] ([CONTRACT_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Contract_Meter_Volume__Contract_id__Month_Identifier] ON [dbo].[CONTRACT_METER_VOLUME] ([CONTRACT_ID], [MONTH_IDENTIFIER]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [CONTRACT_METER_VOLUME_FK_1] ON [dbo].[CONTRACT_METER_VOLUME] ([METER_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_CONTRACT_METER_VOLUME__METER_ID__VOLUME__MONTH_IDENTIFIER] ON [dbo].[CONTRACT_METER_VOLUME] ([METER_ID], [VOLUME], [MONTH_IDENTIFIER]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[CONTRACT_METER_VOLUME] ADD CONSTRAINT [METER_CONTACT_METER_VOLUME_FK_1] FOREIGN KEY ([METER_ID]) REFERENCES [dbo].[METER] ([METER_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Volumes by frequency by meter on a contract', 'SCHEMA', N'dbo', 'TABLE', N'CONTRACT_METER_VOLUME', NULL, NULL
GO
