CREATE TABLE [dbo].[Commodity]
(
[Commodity_Id] [int] NOT NULL IDENTITY(100000, 1) NOT FOR REPLICATION,
[Commodity_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Commodity_Dsc] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Alternate_Fuel] [bit] NOT NULL CONSTRAINT [df_Commodity_Is_Alternate_Fuel] DEFAULT ((0)),
[Is_Sustainable] [bit] NOT NULL CONSTRAINT [df_Commodity_Is_Substainable] DEFAULT ((0)),
[Base_UOM_Cd] [int] NULL,
[Display_Sq] [int] NOT NULL CONSTRAINT [df_Commodity_Display_Sq] DEFAULT ((100)),
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Commodity_Is_Active] DEFAULT ((1)),
[Source_Cd] [int] NOT NULL,
[Default_UOM_Entity_Type_Id] [int] NULL,
[UOM_Entity_Type] [int] NULL,
[Bucket_Aggregation_Cd] [int] NULL,
[Service_Mapping_Instructions] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Weather_Adjusted] [bit] NOT NULL CONSTRAINT [df_Commodity__Is_Weather_Adjusted] DEFAULT ((0)),
[Emission_Last_Change_By_User_Id] [int] NULL,
[Emission_Last_Change_Ts] [datetime] NULL,
[Created_By_User_Id] [int] NOT NULL CONSTRAINT [df_Commodity__Created_By_User_Id] DEFAULT ((-1)),
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Commodity__Created_Ts] DEFAULT (getdate()),
[Last_Change_By_User_Id] [int] NOT NULL CONSTRAINT [df_Commodity__Last_Change_By_User_Id] DEFAULT ((-1)),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Commodity__Last_Change_Ts] DEFAULT (getdate()),
[Is_Dtl_Reporting_Enabled] [bit] NOT NULL CONSTRAINT [df_Commodity__Is_Dtl_Reporting_Enabled] DEFAULT ((0)),
[Is_Client_Specific] [bit] NULL CONSTRAINT [DF_Commodity_Is_Client_Specific] DEFAULT ((0))
) ON [DBData_CoreClient] TEXTIMAGE_ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_Commodity_Commodity_ID__Commodity_Name] ON [dbo].[Commodity] ([Commodity_Id], [Commodity_Name]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', 'General list of Commodities', 'SCHEMA', N'dbo', 'TABLE', N'Commodity', NULL, NULL
GO

ALTER TABLE [dbo].[Commodity] ADD CONSTRAINT [unc_Commodity_Commodity_Name] UNIQUE CLUSTERED  ([Commodity_Name]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Commodity] ADD CONSTRAINT [pk_Commodity] PRIMARY KEY NONCLUSTERED  ([Commodity_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Commodity_Commodity_ID__Is_Alternate_Fuel] ON [dbo].[Commodity] ([Commodity_Id], [Is_Alternate_Fuel]) INCLUDE ([Commodity_Name], [Default_UOM_Entity_Type_Id], [UOM_Entity_Type]) ON [DB_INDEXES01]





GO
