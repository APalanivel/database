CREATE TABLE [dbo].[Time_Zone]
(
[Time_Zone_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Time_Zone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Time_Zone_Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GMT] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GMT_Offset] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Default] [bit] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Time_Zone] ADD CONSTRAINT [pk_Time_Zone__Time_Zone_Id] PRIMARY KEY CLUSTERED  ([Time_Zone_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Time_Zone] ADD CONSTRAINT [un_Time_Zone__Time_Zone] UNIQUE NONCLUSTERED  ([Time_Zone]) ON [DB_DATA01]
GO
