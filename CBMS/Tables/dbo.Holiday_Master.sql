CREATE TABLE [dbo].[Holiday_Master]
(
[Holiday_Master_Id] [int] NOT NULL IDENTITY(1, 1),
[Year_Identifier] [int] NOT NULL,
[Holiday_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Holiday_Dt] [date] NOT NULL,
[COUNTRY_ID] [int] NOT NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Time of use schedules master listing of Holidays.  Holidays listed through 12/31/2019', 'SCHEMA', N'dbo', 'TABLE', N'Holiday_Master', NULL, NULL
GO

CREATE UNIQUE CLUSTERED INDEX [ucix_Holiday_Master__COUNTRY_ID__Year_Identifier__Holiday_Name] ON [dbo].[Holiday_Master] ([COUNTRY_ID], [Year_Identifier], [Holiday_Name]) WITH (FILLFACTOR=90) ON [DBData_Invoice]

ALTER TABLE [dbo].[Holiday_Master] ADD CONSTRAINT [pk_Holiday_Master] PRIMARY KEY NONCLUSTERED  ([Holiday_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]

ALTER TABLE [dbo].[Holiday_Master] ADD
CONSTRAINT [fk_COUNTRY__Holiday_Master] FOREIGN KEY ([COUNTRY_ID]) REFERENCES [dbo].[COUNTRY] ([COUNTRY_ID])
GO
