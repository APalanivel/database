CREATE TABLE [dbo].[App_Global_Config_Search_Filter]
(
[App_Global_Config_Search_Filter_Id] [int] NOT NULL IDENTITY(1, 1),
[App_Global_Config_Id] [int] NOT NULL,
[App_Search_Filter_Id] [int] NOT NULL,
[Filter_Display_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter_Type_Cd] [int] NOT NULL,
[Is_Default] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Is_Default] DEFAULT ((0)),
[Is_Hidden] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Is_Hidden] DEFAULT ((0)),
[Data_Source_Query] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Key_Field_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Smart_Search_Filter] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Is_Smart_Search_Filter] DEFAULT ((0)),
[Display_Seq] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config_Search_Filter__Last_Change_Ts] DEFAULT (getdate()),
[URL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Search_Filter] ADD CONSTRAINT [pk_App_Global_Config_Search_Filter] PRIMARY KEY CLUSTERED  ([App_Global_Config_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Search_Filter] ADD CONSTRAINT [un_App_Global_Config_Search_Filter__App_Global_Config_Id__App_Search_Filter_Id] UNIQUE NONCLUSTERED  ([App_Global_Config_Id], [App_Search_Filter_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_App_Global_Config_Search_Filter__App_Search_Filter_Id] ON [dbo].[App_Global_Config_Search_Filter] ([App_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Search_Filter] ADD CONSTRAINT [fk_App_Global_Config__App_Global_Config_Search_Filter] FOREIGN KEY ([App_Global_Config_Id]) REFERENCES [dbo].[App_Global_Config] ([App_Global_Config_Id])
GO
ALTER TABLE [dbo].[App_Global_Config_Search_Filter] ADD CONSTRAINT [fk_App_Search_Filter__App_Global_Config_Search_Filter] FOREIGN KEY ([App_Search_Filter_Id]) REFERENCES [dbo].[App_Search_Filter] ([App_Search_Filter_Id])
GO
