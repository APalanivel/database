CREATE TABLE [dbo].[ADDRESS]
(
[ADDRESS_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ADDRESS_TYPE_ID] [int] NOT NULL,
[STATE_ID] [int] NOT NULL,
[ADDRESS_LINE1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ADDRESS_LINE2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ZIPCODE] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PHONE_NUMBER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_PRIMARY_ADDRESS] [bit] NULL CONSTRAINT [Set_To_Zero2] DEFAULT ((0)),
[ADDRESS_PARENT_ID] [int] NOT NULL,
[ADDRESS_PARENT_TYPE_ID] [int] NOT NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero3] DEFAULT ((0)),
[GEO_LAT] [decimal] (32, 16) NULL,
[GEO_LONG] [decimal] (32, 16) NULL,
[SQUARE_FOOTAGE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Row_Version] [timestamp] NULL,
[Is_System_Generated_Geocode] [bit] NOT NULL CONSTRAINT [df_ADDRESS_Is_System_Generated_Geocode] DEFAULT ((1))
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Address_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Address table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT Site_Address_Line1,Site_Address_Line2,CITY,ZIPCODE,state_id,GEO_LAT,GEO_LONG FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995

		SELECT * FROM ADDRESS WHERE ADDRESS_ID = 11750
				
		UPDATE ADDRESS
		SET ADDRESS_LINE1 = 'Address-1'
			,Address_Line2 = 'Address-2'
			,CITY = 'Test-City'
			,ZIPCODE = 'Test-Zip'
			,state_id = 45
		WHERE ADDRESS_ID = 11750
				
		SELECT Site_Address_Line1,Site_Address_Line2,CITY,ZIPCODE,state_id,GEO_LAT,GEO_LONG FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995

	ROLLBACK TRAN

	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	HG		Harihara Suthan G
	RC      Rao Chejarla
	BCH		Balaraju Chalumuri
	DSC		Kaushik
	
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/26/2010	Added the update condition for State, as we can update the state for an address
			08/31/2010	Added the Last_Change_TS column for update

	HG		01/05/2011	Country_id, country_name,Region_id and Region_Name column added to UPDATE clause as change in Address of a site should reflect to Client_Hier.
						Geo_Lat, Geo_Lang columns included in  BINARY CHECK SUM comparison
						(These two columns are not modified by the users, based on the address zip code application find these address and updating it)
						GHG_Region_Key and GHG_Region_Name Column also added to the update statement as these 2 depends on the address of a site.
	RC      04/11/2012  Removed references to GHG_Region_Key and GHG_Region_Name						
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
	DSC		2014-07-03	Added code to update portfolio duplicate site address.
******/
CREATE TRIGGER [dbo].[tr_Address_upd_Client_Hier] ON [dbo].[ADDRESS]
      FOR UPDATE
AS
BEGIN

      SET NOCOUNT ON;

      UPDATE
            ch
      SET
            Site_Address_Line1 = i.ADDRESS_LINE1
           ,Site_Address_Line2 = i.ADDRESS_LINE2
           ,CITY = i.CITY
           ,ZIPCODE = i.ZIPCODE
           ,GEO_LAT = i.GEO_LAT
           ,GEO_LONG = i.GEO_LONG
           ,State_ID = i.State_ID
           ,State_Name = st.State_Name
           ,Region_Id = st.REGION_ID
           ,Region_Name = rg.Region_Name
           ,Country_Id = st.Country_Id
           ,COUNTRY_NAME = cy.Country_Name
           ,Last_Change_TS = getdate()
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.SITE_ID
            INNER JOIN INSERTED i
                  ON i.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
            INNER JOIN DELETED d
                  ON i.ADDRESS_ID = d.ADDRESS_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = i.STATE_ID
            INNER JOIN dbo.Region rg
                  ON rg.Region_Id = st.REGION_ID
            INNER JOIN dbo.Country cy
                  ON cy.Country_Id = st.COUNTRY_ID
      WHERE
            hashbytes('SHA1', ( SELECT
                                    i.ADDRESS_LINE1
                                   ,i.ADDRESS_LINE2
                                   ,i.CITY
                                   ,i.ZIPCODE
                                   ,i.State_ID
                                   ,i.GEO_LAT
                                   ,i.GEO_LONG
                      FOR
                                XML RAW )) != hashbytes('SHA1', ( SELECT
                                                                        d.ADDRESS_LINE1
   ,d.ADDRESS_LINE2
                                                                       ,d.CITY
                                                                       ,d.ZIPCODE
                                                                       ,d.State_ID
                                                                       ,d.GEO_LAT
                                                                       ,d.GEO_LONG
                                                        FOR
                                                                  XML RAW ))   
		  --AND s.Portfolio_Client_Hier_Reference_Number IS NULL                     


-- update portfolio duplicate site address also.
      
      DECLARE
            @Portfolio_Client_Hier_Reference_Number INT
           ,@Site_Id INT

      SELECT
            @Portfolio_Client_Hier_Reference_Number = ch.Client_Hier_Id
           ,@Site_Id = s.Site_Id
      FROM
            inserted i
            INNER JOIN site s
                  ON s.PRIMARY_ADDRESS_ID = I.Address_Id
            INNER JOIN core.Client_Hier ch
                  ON ch.Site_Id = s.Site_Id
      WHERE
            s.Portfolio_Client_Hier_Reference_Number IS NULL
            AND ch.Portfolio_Client_Id IS NOT NULL
	  
      IF @Portfolio_Client_Hier_Reference_Number IS NOT NULL
            BEGIN
                  UPDATE
                        a
                  SET
                        a.ADDRESS_TYPE_ID = i.ADDRESS_TYPE_ID
                       ,a.STATE_ID = i.STATE_ID
                       ,a.ADDRESS_LINE1 = i.ADDRESS_LINE1
                       ,a.ADDRESS_LINE2 = i.ADDRESS_LINE2
                       ,a.CITY = i.CITY
                       ,a.ZIPCODE = i.ZIPCODE
                       ,a.PHONE_NUMBER = i.PHONE_NUMBER
                       ,a.IS_PRIMARY_ADDRESS = i.IS_PRIMARY_ADDRESS
                       ,a.ADDRESS_PARENT_ID = s.site_Id
                       ,a.ADDRESS_PARENT_TYPE_ID = i.ADDRESS_PARENT_TYPE_ID
                       ,a.IS_HISTORY = i.IS_HISTORY
                       ,a.GEO_LAT = i.GEO_LAT
                       ,a.GEO_LONG = i.GEO_LONG
                       ,a.SQUARE_FOOTAGE = i.SQUARE_FOOTAGE
                       ,a.Is_System_Generated_Geocode = i.Is_System_Generated_Geocode
                  FROM
                        site s
                        INNER JOIN dbo.ADDRESS a
                              ON A.ADDRESS_ID = S.PRIMARY_ADDRESS_ID
                        INNER JOIN site s2
                              ON s2.SITE_ID = @Site_Id
                        INNER JOIN inserted i
                              ON i.address_Id = s2.PRIMARY_ADDRESS_ID
                  WHERE
                        s.Portfolio_Client_Hier_Reference_Number = @Portfolio_Client_Hier_Reference_Number







            END


END;
;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Address_upd_Client_Hier_Account
	
DESCRIPTION:   
	This will update the existing record of Core.Client_Hier_Account table whenever any updates happen in Address table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT * FROM CORE.CLIENT_HIER_Account WHERE meter_address_id = 11750
		SELECT * FROM ADDRESS WHERE ADDRESS_ID = 11750
				
		UPDATE ADDRESS
		SET ADDRESS_LINE1 = 'Address-1'
			,Address_Line2 = 'Address-2'
			,CITY = 'Test-City'
			,ZIPCODE = 'Test-Zip'
			,STATE_ID = 45
		WHERE ADDRESS_ID = 11750
		
		SELECT * FROM CORE.CLIENT_HIER_Account WHERE meter_address_id = 11750
		
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	HG		Harihara Suthan G
	BCH		Balaraju Chalumuri

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/26/2010	Created
	HG		01/06/2011	Meter_Country_id & Meter_Country_Name column added to the UPDATE clause.
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
******/

CREATE TRIGGER [dbo].[tr_Address_upd_Client_Hier_Account] ON [dbo].[ADDRESS]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 

      UPDATE
            Core.Client_Hier_Account
      SET   
            Meter_Address_Line_1 = i.ADDRESS_LINE1
           ,Meter_Address_Line_2 = i.ADDRESS_LINE2
           ,Meter_City = i.CITY
           ,Meter_Country_Id = st.Country_Id
           ,Meter_Country_Name = cy.Country_Name
           ,Meter_ZipCode = i.ZIPCODE
           ,Meter_State_Id = i.STATE_ID
           ,Meter_State_Name = st.STATE_NAME
           ,Meter_Geo_Lat = i.GEO_LAT
           ,Meter_Geo_Long = i.GEO_LONG
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                  ON i.ADDRESS_ID = cha.Meter_Address_ID
            INNER JOIN DELETED d
                  ON i.ADDRESS_ID = d.ADDRESS_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = i.STATE_ID
            INNER JOIN dbo.Country cy
                  ON cy.Country_id = st.Country_Id
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.ADDRESS_LINE1
                                   ,i.ADDRESS_LINE2
                                   ,i.CITY
                                   ,i.ZIPCODE
                                   ,i.STATE_ID
                                   ,i.GEO_LAT
                                   ,i.GEO_LONG
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.ADDRESS_LINE1
                                                                       ,d.ADDRESS_LINE2
                                                                       ,d.CITY
                                                                       ,d.ZIPCODE
                                                                       ,d.STATE_ID
                                                                       ,d.GEO_LAT
                                                                       ,d.GEO_LONG
                                                        FOR
                                                                  XML RAW ))

END 
;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Address_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Address table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT Site_Address_Line1,Site_Address_Line2,CITY,ZIPCODE,GEO_LAT,GEO_LONG,state_id,state_name FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
		SELECT * FROM ADDRESS WHERE ADDRESS_ID = 11750
				
		UPDATE ADDRESS
		SET ADDRESS_LINE1 = 'Address-1'
			,Address_Line2 = 'Address-2'
			,CITY = 'Test-City'
			,ZIPCODE = 'Test-Zip'
			,state_id = 45
		WHERE ADDRESS_ID = 11750
				
		SELECT Site_Address_Line1,Site_Address_Line2,CITY,ZIPCODE,state_id,GEO_LAT,GEO_LONG FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995

	ROLLBACK TRAN
SELECT Site_Address_Line1,Site_Address_Line2,CITY,ZIPCODE,state_id,GEO_LAT,GEO_LONG FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995

	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	HG		Harihara Suthan G
	RC      Rao Chejarla
	BCH		Balaraju Chalumuri
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/26/2010	Added the update condition for State, as we can update the state for an address
			08/31/2010	Added the Last_Change_TS column for update

	HG		01/05/2011	Country_id, country_name,Region_id and Region_Name column added to UPDATE clause as change in Address of a site should reflect to Client_Hier.
						Geo_Lat, Geo_Lang columns included in  BINARY CHECK SUM comparison
						(These two columns are not modified by the users, based on the address zip code application find these address and updating it)
						GHG_Region_Key and GHG_Region_Name Column also added to the update statement as these 2 depends on the address of a site.
	RC      04/11/2012  Removed references to GHG_Region_Key and GHG_Region_Name						
	BCH		2013-04-12  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.

******/

CREATE TRIGGER [dbo].[tr_Address_upd_Client_Hier_balaraju] ON [dbo].[ADDRESS]
      FOR UPDATE
AS
BEGIN

      SET NOCOUNT ON;

      UPDATE
            ch
      SET   
            Site_Address_Line1 = i.ADDRESS_LINE1
           ,Site_Address_Line2 = i.ADDRESS_LINE2
           ,CITY = i.CITY
           ,ZIPCODE = i.ZIPCODE
           ,GEO_LAT = i.GEO_LAT
           ,GEO_LONG = i.GEO_LONG
           ,State_ID = i.State_ID
           ,State_Name = st.State_Name
           ,Region_Id = st.REGION_ID
           ,Region_Name = rg.Region_Name
           ,Country_Id = st.Country_Id
           ,COUNTRY_NAME = cy.Country_Name
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier ch
            INNER JOIN dbo.SITE s
                  ON s.SITE_ID = ch.SITE_ID
            INNER JOIN INSERTED i
                  ON i.ADDRESS_ID = s.PRIMARY_ADDRESS_ID
            INNER JOIN DELETED d
                  ON i.ADDRESS_ID = d.ADDRESS_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = i.STATE_ID
            INNER JOIN dbo.Region rg
                  ON rg.Region_Id = st.REGION_ID
            INNER JOIN dbo.Country cy
                  ON cy.Country_Id = st.COUNTRY_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.ADDRESS_LINE1
                                   ,i.ADDRESS_LINE2
                                   ,i.CITY
                                   ,i.ZIPCODE
                                   ,i.State_ID
                                   ,i.GEO_LAT
                                   ,i.GEO_LONG
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.ADDRESS_LINE1
                                                                       ,d.ADDRESS_LINE2
                                                                       ,d.CITY
                                                                       ,d.ZIPCODE
                                                                       ,d.State_ID
                                                                       ,d.GEO_LAT
                                                                       ,d.GEO_LONG
                                                        FOR
                                                                  XML RAW ))                        

END;
;
GO
ALTER TABLE [dbo].[ADDRESS] ADD CONSTRAINT [ADDRESS_PK] PRIMARY KEY CLUSTERED  ([ADDRESS_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [IX_Address_Address_ID__State_id] ON [dbo].[ADDRESS] ([ADDRESS_ID], [STATE_ID]) INCLUDE ([ADDRESS_LINE1], [CITY]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ADDRESS_N_4] ON [dbo].[ADDRESS] ([ADDRESS_ID], [STATE_ID], [ADDRESS_LINE1], [ADDRESS_LINE2], [CITY], [ZIPCODE]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_address_address_Parent_id] ON [dbo].[ADDRESS] ([ADDRESS_PARENT_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ADDRESS_N_1] ON [dbo].[ADDRESS] ([STATE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_ADDRESS__STATE_ID] ON [dbo].[ADDRESS] ([STATE_ID]) INCLUDE ([ADDRESS_ID], [CITY]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[ADDRESS] ADD CONSTRAINT [ENTITY_ADDRESS_FK_2] FOREIGN KEY ([ADDRESS_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[ADDRESS] ADD CONSTRAINT [ENTITY_ADDRESS_FK_3] FOREIGN KEY ([ADDRESS_PARENT_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[ADDRESS] ADD CONSTRAINT [STATE_ADDRESS_FK_4] FOREIGN KEY ([STATE_ID]) REFERENCES [dbo].[STATE] ([STATE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the site/meter addresses', 'SCHEMA', N'dbo', 'TABLE', N'ADDRESS', NULL, NULL
GO
