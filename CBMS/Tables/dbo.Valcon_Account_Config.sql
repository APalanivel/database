CREATE TABLE [dbo].[Valcon_Account_Config]
(
[Valcon_Account_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Contract_Id] [int] NOT NULL,
[Source_Cd] [int] NOT NULL,
[Is_Config_Complete] [bit] NOT NULL,
[Comment_Id] [int] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Valcon_Account_Config__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Valcon_Account_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Valcon_Account_Config] ADD CONSTRAINT [PK_Valcon_Account_Config__Valcon_Account_Config_Id] PRIMARY KEY CLUSTERED  ([Valcon_Account_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Valcon_Account_Config] ADD CONSTRAINT [un_Valcon_Account_Config__Account_Id_Contract_Id] UNIQUE NONCLUSTERED  ([Account_Id], [Contract_Id]) ON [DB_DATA01]
GO
