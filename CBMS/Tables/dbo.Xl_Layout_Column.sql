CREATE TABLE [dbo].[Xl_Layout_Column]
(
[Xl_Layout_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[Xl_Layout_Id] [int] NOT NULL,
[Xl_Column_Id] [int] NOT NULL,
[Is_Required] [bit] NOT NULL CONSTRAINT [df_Xl_Layout_Column__Is_Required] DEFAULT ((1)),
[Is_Locked] [bit] NOT NULL CONSTRAINT [df_Xl_Layout_Column__Is_Locked] DEFAULT ((0)),
[Display_Seq] [int] NOT NULL,
[Default_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout_Column] ADD CONSTRAINT [pk_Xl_Layout_Column] PRIMARY KEY CLUSTERED  ([Xl_Layout_Column_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Xl_Layout_Column__Xl_Column_Id] ON [dbo].[Xl_Layout_Column] ([Xl_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout_Column] ADD CONSTRAINT [un_Xl_Layout_Column__Xl_Layout_Id__Xl_Column_Id] UNIQUE NONCLUSTERED  ([Xl_Layout_Id], [Xl_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Layout_Column] ADD CONSTRAINT [fk_Xl_Column__Xl_Layout_Column] FOREIGN KEY ([Xl_Column_Id]) REFERENCES [dbo].[Xl_Column] ([Xl_Column_Id])
GO
ALTER TABLE [dbo].[Xl_Layout_Column] ADD CONSTRAINT [fk_Xl_Layout__Xl_Layout_Column] FOREIGN KEY ([Xl_Layout_Id]) REFERENCES [dbo].[Xl_Layout] ([Xl_Layout_Id])
GO
