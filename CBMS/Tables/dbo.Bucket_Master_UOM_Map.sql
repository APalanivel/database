CREATE TABLE [dbo].[Bucket_Master_UOM_Map]
(
[Bucket_Master_Id] [int] NOT NULL,
[Uom_Type_Id] [int] NOT NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table listing of Buckets to available UOM', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Master_UOM_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Bucket_Master_UOM_Map] ADD 
CONSTRAINT [PK_Bucket_Master_Uom_Map] PRIMARY KEY CLUSTERED  ([Uom_Type_Id], [Bucket_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
ALTER TABLE [dbo].[Bucket_Master_UOM_Map] ADD
CONSTRAINT [fk_Bucket_Master_Bucket_Master_Uom_Map] FOREIGN KEY ([Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id])
GO
