CREATE TABLE [dbo].[USER_INFO]
(
[USER_INFO_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[USERNAME] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[QUEUE_ID] [int] NULL,
[FIRST_NAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MIDDLE_NAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMAIL_ADDRESS] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero18] DEFAULT ((0)),
[ACCESS_LEVEL] [int] NULL,
[CLIENT_ID] [int] NULL,
[DIVISION_ID] [int] NULL,
[SITE_ID] [int] NULL,
[ALLOW_AUTO] [bit] NOT NULL CONSTRAINT [DF_USER_INFO_ALLOW_AUTO] DEFAULT ((0)),
[PHONE_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_DEMO_USER] [bit] NOT NULL CONSTRAINT [DF__USER_INFO__IS_DEMO_USER] DEFAULT ((0)),
[EXPIRATION_DATE] [datetime] NULL,
[PROSPECT_NAME] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CEM_HOMEPAGE] [bit] NULL CONSTRAINT [DF__USER_INFO__CEM_HOMEPAGE] DEFAULT ((0)),
[CELL_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAX_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_NUMBER_EXT] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address_1] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[address_2] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state_id] [int] NULL,
[zipcode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country_id] [int] NULL,
[currency_unit_id] [int] NOT NULL CONSTRAINT [currency_default] DEFAULT ((3)),
[locale_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [locale_default] DEFAULT ('en-US'),
[city] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[el_uom_id] [int] NOT NULL CONSTRAINT [df_el_uom_id] DEFAULT ((12)),
[ng_uom_id] [int] NOT NULL CONSTRAINT [df_ng_uom_id] DEFAULT ((25)),
[Default_View] [int] NOT NULL CONSTRAINT [DF__USER_INFO__Default_View] DEFAULT ((-1)),
[sitesearch_viewedpromo] [bit] NOT NULL CONSTRAINT [DF__User_Info__sitesearch_viewedpromo] DEFAULT ((0)),
[Failed_Login_Attempts] [smallint] NULL CONSTRAINT [DF__USER_INFO__Failed_Login_Attempts] DEFAULT ((0)),
[New_User_EMail_Ts] [datetime] NULL,
[Job_Title] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL CONSTRAINT [df__User_Info__Created_User_Id] DEFAULT ((-1)),
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__USER_INFO__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DF__USER_INFO__Last_Change_Ts] DEFAULT (getdate()),
[Has_Accepted_Data_Privacy_Agreement] [smallint] NOT NULL CONSTRAINT [df__User_Info__Has_Accepted_Data_Privacy_Agreement] DEFAULT ((-1)),
[Last_Login_Email_Reminder_Ts] [datetime] NULL
) ON [DBData_UserInfo]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******
NAME:	tr_User_Info_Send_Transfer

DESCRIPTION: Trigger on dbo.User_Info to send changes for DV2,SV and DVDEHub through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/03/2014		Created
******/
CREATE TRIGGER [dbo].[tr_User_Info_Transfer] ON [dbo].[USER_INFO]
      FOR DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
		   ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	
      IF session_user != N'sb_CBMS_Service' 
            BEGIN


                  SET @Message = ( 
								   SELECT 
											d.USER_INFO_ID
											,'D' AS Op_Code
                                   FROM
											DELETED d
								   FOR
										   XML PATH('User_Info_Change')
											  ,ELEMENTS
											  ,ROOT('User_Info_Changes') 
								 )

                  IF @Message IS NOT NULL 
                        BEGIN
	
							SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'

							-- Cycle through the targets in Change_Control Targt and sent themessage to each    
                              DECLARE cDialog CURSOR
                              FOR
                              ( SELECT
                                    Target_Service
                                   ,Target_Contract
                                FROM
                                    Service_broker_target
                                WHERE
                                    Table_Name = 'dbo.User_Info')     
                              OPEN cDialog    
                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                              WHILE @@Fetch_Status = 0 
                                    BEGIN    
						
										  EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT


                                         ;
                                          SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Info_Transfer] (@Message);

                                          
                                          FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                                    END    
                              CLOSE cDialog    
                              DEALLOCATE cDIalog  

                        END
            END
END





;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.trg_ins_User_Info

DESCRIPTION:
--Mark McCallon
--11-6-07
--Insert, or delete user_sal
--based on values of access_level, 
--client_id, division_id and site_id
--12-20-07
--small bug with internal users and null client_id
--in user_sal


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	 DMR		Deana Ritter
	 HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	7/20/2009	Autogenerated script
	DMR			08/20/2009	Modified for Sitegrouping.  Removed references to division
						    Added in join from User_Sal to Client for internal users
						    
	HG			2013-10-14	MAINT-2269, Modified the trigger to remove the Ins/upd/Del on User_Sal
							As a short term User_Sal table will be loaded every day night through a batch process and replicated to RA						    
							Once all the RA objects modified to remove the User_Sal this table will be dropped
******/

CREATE TRIGGER [dbo].[trg_ins_User_Info] ON [dbo].[USER_INFO]
      FOR INSERT
AS
BEGIN

      IF UPDATE(locale_code)
            BEGIN
                  UPDATE
                        dbo.USER_INFO
                  SET
                        el_uom_id = case WHEN a.locale_code = 'en-US' THEN 12
                                         WHEN a.locale_code = 'en-GB' THEN 1463
                                         WHEN a.locale_code = 'fr-FR' THEN 1463
                                         WHEN a.locale_code = 'fr-CA' THEN 12
                                         WHEN a.locale_code = 'nl-BE' THEN 1463
                                         WHEN a.locale_code = 'de-DE' THEN 1463
                                         ELSE 12
                                    END
                       ,ng_uom_id = case WHEN a.locale_code = 'en-US' THEN 25
                                         WHEN a.locale_code = 'en-GB' THEN 28
                                         WHEN a.locale_code = 'fr-FR' THEN 1459
                                         WHEN a.locale_code = 'fr-CA' THEN 21
                                         WHEN a.locale_code = 'nl-BE' THEN 1369
                                         WHEN a.locale_code = 'de-DE' THEN 1459
                                         ELSE 25
                                    END
                  FROM
                        INSERTED a
                        INNER JOIN dbo.USER_INFO b
                              ON a.user_info_id = b.user_info_id

            END

END



;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.trg_upd_User_Info

DESCRIPTION:
--Mark McCallon
--9-17-07
--Update User_sal, user_division_map, and user_site_map
--based is_history column in user_info
--11-20-07
--added update to client column
--12-10-07
--fixed bug with update
--12-19-07
--bug with user_info updates


INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------
  	          	

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DMR			Deana Ritter
	DSC			Kaushik
	RKV			Ravi Kumar Vegesna
	
MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	        	07/20/2009	Autogenerated script
	DMR			08/20/2009	Modified for Sitegrouping.  Removed references to division
						    Added in join from User_Sal to Client for internal users
    DMR			5/19/2010   added additional conditions.  The if Update statement always
							executed as the entire record is saved each time.  This will
							eliminate unnecessary calls for deleting and updating User_SAL.						    

	HG			2013-10-14	MAINT-2269, Modified the trigger to remove the Ins/upd/Del on User_Sal
							As a short term User_Sal table will be loaded every day night through a batch process and replicated to RA						    
							Once all the RA objects modified to remove the User_Sal this table will be dropped
							
	HG			2014-01-29	HG 2014-01-29 Removed the UPDATE statement which makes User_Site_Map and User_Division_Map Is_History = 0
							All the DV2 objects which refers this User_Site_Map and User_Division_Map has been modified already to use Security_Role tables 
    
	DSC			2014-03-11  MAINT-2513, Replace user_Info replication with Service_Broker objects
	RKV			2018-04-17  SE2017-520, Added new column Has_Accepted_Data_Privacy_Agreement
	KVK			02/19/2019		added new column Last_Login_Email_Reminder_Ts
******/
CREATE TRIGGER [dbo].[trg_upd_User_Info]
ON [dbo].[USER_INFO]
FOR UPDATE
NOT FOR REPLICATION
AS
    BEGIN

        SET NOCOUNT ON;



        DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
            , @Message XML
            , @From_Service NVARCHAR(255)
            , @Target_Service NVARCHAR(255)
            , @Target_Contract NVARCHAR(255);

        IF SESSION_USER != N'sb_CBMS_Service'
            BEGIN
                SET @Message = (   SELECT
                                        i.USER_INFO_ID
                                        , i.USERNAME
                                        , i.QUEUE_ID
                                        , i.FIRST_NAME
                                        , i.MIDDLE_NAME
                                        , i.LAST_NAME
                                        , i.EMAIL_ADDRESS
                                        , i.IS_HISTORY
                                        , i.ACCESS_LEVEL
                                        , i.CLIENT_ID
                                        , i.DIVISION_ID
                                        , i.SITE_ID
                                        , i.ALLOW_AUTO
                                        , i.PHONE_NUMBER
                                        , i.IS_DEMO_USER
                                        , i.EXPIRATION_DATE
                                        , i.PROSPECT_NAME
                                        , i.CEM_HOMEPAGE
                                        , i.CELL_NUMBER
                                        , i.FAX_NUMBER
                                        , i.PHONE_NUMBER_EXT
                                        , i.address_1
                                        , i.address_2
                                        , i.state_id
                                        , i.zipcode
                                        , i.country_id
                                        , i.currency_unit_id
                                        , i.locale_code
                                        , i.city
                                        , i.el_uom_id
                                        , i.ng_uom_id
                                        , i.Default_View
                                        , i.sitesearch_viewedpromo
                                        , i.Failed_Login_Attempts
                                        , i.New_User_EMail_Ts
                                        , i.Job_Title
                                        , i.Created_User_Id
                                        , i.Created_Ts
                                        , i.Updated_User_Id
                                        , 'U' AS Op_Code
                                        , i.Last_Change_Ts
                                        , i.Has_Accepted_Data_Privacy_Agreement
										, i.Last_Login_Email_Reminder_Ts
                                   FROM
                                        INSERTED i
                                   GROUP BY
                                       i.USER_INFO_ID
                                       , i.USERNAME
                                       , i.QUEUE_ID
                                       , i.FIRST_NAME
                                       , i.MIDDLE_NAME
                                       , i.LAST_NAME
                                       , i.EMAIL_ADDRESS
                                       , i.IS_HISTORY
                                       , i.ACCESS_LEVEL
                                       , i.CLIENT_ID
                                       , i.DIVISION_ID
                                       , i.SITE_ID
                                       , i.ALLOW_AUTO
                                       , i.PHONE_NUMBER
                                       , i.IS_DEMO_USER
                                       , i.EXPIRATION_DATE
                                       , i.PROSPECT_NAME
                                       , i.CEM_HOMEPAGE
                                       , i.CELL_NUMBER
                                       , i.FAX_NUMBER
                                       , i.PHONE_NUMBER_EXT
                                       , i.address_1
                                       , i.address_2
                                       , i.state_id
                                       , i.zipcode
                                       , i.country_id
                                       , i.currency_unit_id
                                       , i.locale_code
                                       , i.city
                                       , i.el_uom_id
                                       , i.ng_uom_id
                                       , i.Default_View
                                       , i.sitesearch_viewedpromo
                                       , i.Failed_Login_Attempts
                                       , i.New_User_EMail_Ts
                                       , i.Job_Title
                                       , i.Created_User_Id
                                       , i.Created_Ts
                                       , i.Updated_User_Id
                                       , i.Last_Change_Ts
                                       , i.Has_Accepted_Data_Privacy_Agreement
									   , i.Last_Login_Email_Reminder_Ts
                                   FOR XML PATH('User_Info_Change'), ELEMENTS, ROOT('User_Info_Changes'));

                IF @Message IS NOT NULL
                    BEGIN

                        SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer';

                        -- Cycle through the targets in Change_Control Targt and sent the message to each    
                        DECLARE cDialog CURSOR FOR(
                            SELECT
                                Target_Service
                                , Target_Contract
                            FROM
                                dbo.Service_Broker_Target
                            WHERE
                                Table_Name = 'dbo.User_Info');
                        OPEN cDialog;
                        FETCH NEXT FROM cDialog
                        INTO
                            @Target_Service
                            , @Target_Contract;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                EXEC dbo.Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                    , @Target_Service
                                    , @Target_Contract
                                    , @Conversation_Handle OUTPUT;
                                SEND ON CONVERSATION
                                    @Conversation_Handle
                                    MESSAGE TYPE [//Change_Control/Message/User_Info_Transfer]
                                    (@Message);


                                FETCH NEXT FROM cDialog
                                INTO
                                    @Target_Service
                                    , @Target_Contract;
                            END;
                        CLOSE cDialog;
                        DEALLOCATE cDialog;

                    END;
            END;


        IF UPDATE(IS_HISTORY)
           AND  EXISTS (   SELECT
                                1
                           FROM
                                INSERTED I
                                INNER JOIN DELETED D
                                    ON I.USER_INFO_ID = D.USER_INFO_ID
                           WHERE
                                I.IS_HISTORY <> D.IS_HISTORY)
            BEGIN

                DELETE
                usr
                FROM
                    INSERTED a
                    INNER JOIN dbo.User_Security_Role usr
                        ON usr.User_Info_Id = a.USER_INFO_ID
                WHERE
                    a.IS_HISTORY = 1;
            END;

    END;

GO
ALTER TABLE [dbo].[USER_INFO] ADD CONSTRAINT [USER_INFO_PK] PRIMARY KEY CLUSTERED  ([USER_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
GO
CREATE NONCLUSTERED INDEX [IX_USER_INFO__Client_id__User_Info_ID__Access_Level] ON [dbo].[USER_INFO] ([CLIENT_ID], [USER_INFO_ID], [ACCESS_LEVEL]) INCLUDE ([EMAIL_ADDRESS], [FIRST_NAME], [LAST_NAME], [USERNAME]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_USER_INFO__IS_HISTORY__CLIENT_ID] ON [dbo].[USER_INFO] ([IS_HISTORY], [CLIENT_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [USER_INFO_N_1] ON [dbo].[USER_INFO] ([QUEUE_ID], [FIRST_NAME], [LAST_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [USER_INFO_QueueID_FirstName_LastName] ON [dbo].[USER_INFO] ([QUEUE_ID], [USER_INFO_ID], [FIRST_NAME], [LAST_NAME]) ON [DB_INDEXES01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [USER_INFO_U_1] ON [dbo].[USER_INFO] ([USERNAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[USER_INFO] ADD CONSTRAINT [FK_QUEUEUSER_INFO] FOREIGN KEY ([QUEUE_ID]) REFERENCES [dbo].[QUEUE] ([QUEUE_ID])
GO
ALTER TABLE [dbo].[USER_INFO] WITH NOCHECK ADD CONSTRAINT [QUEUE_USER_INFO_FK] FOREIGN KEY ([QUEUE_ID]) REFERENCES [dbo].[QUEUE] ([QUEUE_ID])
GO
GRANT SELECT ON  [dbo].[USER_INFO] TO [BPOReportRole]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Information about people who have access to Applications', 'SCHEMA', N'dbo', 'TABLE', N'USER_INFO', NULL, NULL
GO
