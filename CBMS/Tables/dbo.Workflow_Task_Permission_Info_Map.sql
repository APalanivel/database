CREATE TABLE [dbo].[Workflow_Task_Permission_Info_Map]
(
[Geographic_Region_Group_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Task_Id] [int] NOT NULL,
[Permission_Info_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Task_Permission_Info_Map_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
