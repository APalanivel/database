CREATE TABLE [dbo].[Cu_Invoice_Standing_Data_Exception_Dtl]
(
[Cu_Invoice_Standing_Data_Exception_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Standing_Data_Exception_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [df_Cu_Invoice_Standing_Data_Exception_Dtl__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Cu_Invoice_Standing_Data_Exception_Dtl__Last_Change_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Standing_Data_Exception_Dtl] ADD CONSTRAINT [pk_Cu_Invoice_Standing_Data_Exception_Dtl] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Standing_Data_Exception_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Standing_Data_Exception_Dtl] ADD CONSTRAINT [Un_Cu_Invoice_Standing_Data_Exception_Dtl] UNIQUE NONCLUSTERED  ([Cu_Invoice_Standing_Data_Exception_Id], [Account_Id], [Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Standing_Data_Exception_Dtl] ADD CONSTRAINT [fk_Cu_Invoice__Cu_Invoice_Standing_Data_Exception_Dtl] FOREIGN KEY ([Cu_Invoice_Standing_Data_Exception_Id]) REFERENCES [dbo].[Cu_Invoice_Standing_Data_Exception] ([Cu_Invoice_Standing_Data_Exception_Id])
GO
