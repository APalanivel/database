CREATE TABLE [dbo].[Client_Config_Output_Column]
(
[Client_Config_Output_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Config_ID] [int] NULL,
[Display_Column_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Data_Source_Column_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime] NULL,
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL,
[Display_Seq] [int] NULL,
[Date_Format] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Header_CSS] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Config_Output_Column] ADD CONSTRAINT [PK__Client_C__EDD7482F5220B768] PRIMARY KEY CLUSTERED  ([Client_Config_Output_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Config_Output_Column] ADD CONSTRAINT [fk_Client_Config__Client_Config_Output_Column] FOREIGN KEY ([Client_Config_ID]) REFERENCES [dbo].[Client_Config] ([Client_Config_ID])
GO
