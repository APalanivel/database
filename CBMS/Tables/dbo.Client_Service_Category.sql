CREATE TABLE [dbo].[Client_Service_Category]
(
[Client_Service_Category_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Service_Category_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Id] [int] NOT NULL,
[Uom_Id] [int] NOT NULL,
[Service_Level_Cd] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client to Service/Commodity/UOM mapping.  Allows licents to custome the name of the Service.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Service_Category', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Service_Category] ADD CONSTRAINT [unc_Client_Service_Category__Client_Id__Service_Level_Cd__Service_Category_Name] UNIQUE CLUSTERED  ([Client_Id], [Service_Level_Cd], [Service_Category_Name]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Client_Service_Category] ADD CONSTRAINT [pk_Client_Service_Category] PRIMARY KEY NONCLUSTERED  ([Client_Service_Category_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Client_Service_Category__Client_Id] ON [dbo].[Client_Service_Category] ([Client_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

GO
