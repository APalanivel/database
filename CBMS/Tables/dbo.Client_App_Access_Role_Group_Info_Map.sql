CREATE TABLE [dbo].[Client_App_Access_Role_Group_Info_Map]
(
[Client_App_Access_Role_Id] [int] NOT NULL,
[GROUP_INFO_ID] [int] NOT NULL,
[Assigned_User_Id] [int] NOT NULL,
[Assigned_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table of Group Info to App Access mapping', 'SCHEMA', N'dbo', 'TABLE', N'Client_App_Access_Role_Group_Info_Map', NULL, NULL
GO

SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

/******
NAME:	tr_Client_App_Access_Role_Group_Info_Map_Transfer

DESCRIPTION: Trigger to send changes to DV2 server through service_broker.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	TP			Anoop

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
	TP			01 Mar 2016		Modified to add Hub replication message	
******/
CREATE TRIGGER [dbo].[tr_Client_App_Access_Role_Group_Info_Map_Transfer] ON [dbo].[Client_App_Access_Role_Group_Info_Map]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT
                        isnull(i.Client_App_Access_Role_Id, d.Client_App_Access_Role_Id) AS Client_App_Access_Role_Id
                       ,isnull(i.Group_Info_Id, d.Group_Info_Id) AS Group_Info_Id
                       ,i.Assigned_User_Id
                       ,i.Assigned_Ts
                       ,case WHEN i.Client_App_Access_Role_Id IS NOT NULL				-- one Column check is sufficient
                                  AND d.Client_App_Access_Role_Id IS NULL THEN 'I'
                             WHEN i.Client_App_Access_Role_Id IS NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'D'
                             WHEN i.Client_App_Access_Role_Id IS NOT NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'U'
                        END AS Op_Code
                       ,getdate() Last_Change_Ts
                       FROM
                        Inserted i
                        FULL JOIN Deleted d
                              ON i.Client_App_Access_Role_Id = d.Client_App_Access_Role_Id
                                 AND i.Group_Info_Id = d.Group_Info_Id
            FOR
                       XML PATH('Client_App_Access_Role_Group_Info_Map_Change')
                          ,ELEMENTS
                          ,ROOT('Client_App_Access_Role_Group_Info_Map_Changes') )

      IF @Message IS NOT NULL 
            BEGIN
	
                  				
                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DV2/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/Client_App_Access_Role_Transfer'					
					
                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
				MESSAGE TYPE [//Change_Control/Message/Client_App_Access_Role_Group_Info_Map_Transfer] (@Message);

                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DVDEHub/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/Client_App_Access_Role_Transfer'					
					
                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
				MESSAGE TYPE [//Change_Control/Message/Client_App_Access_Role_Group_Info_Map_Transfer] (@Message);

            END

END



GO


ALTER TABLE [dbo].[Client_App_Access_Role_Group_Info_Map] ADD
CONSTRAINT [fk_Client_App_Access_Role__Client_App_Access_Role_Group_Info_Map] FOREIGN KEY ([Client_App_Access_Role_Id]) REFERENCES [dbo].[Client_App_Access_Role] ([Client_App_Access_Role_Id])
GO
ALTER TABLE [dbo].[Client_App_Access_Role_Group_Info_Map] ADD CONSTRAINT [pk_Client_App_Access_Role_Group_Info_Map] PRIMARY KEY CLUSTERED  ([Client_App_Access_Role_Id], [GROUP_INFO_ID]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_App_Access_Role_Group_Info_Map__GROUP_INFO_ID] ON [dbo].[Client_App_Access_Role_Group_Info_Map] ([GROUP_INFO_ID]) ON [DB_INDEXES01]
GO

ALTER TABLE [dbo].[Client_App_Access_Role_Group_Info_Map] ADD CONSTRAINT [fk_Client_App_Access_Role__Group_Info] FOREIGN KEY ([GROUP_INFO_ID]) REFERENCES [dbo].[GROUP_INFO] ([GROUP_INFO_ID])
GO
