CREATE TABLE [dbo].[SR_EMAIL_TEMPLATE]
(
[SR_EMAIL_TEMPLATE_ID] [int] NOT NULL IDENTITY(1, 1),
[EMAIL_TYPE_ID] [int] NOT NULL,
[SUBJECT] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CONTENT] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Sourcing] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the email templates for this application', 'SCHEMA', N'dbo', 'TABLE', N'SR_EMAIL_TEMPLATE', NULL, NULL
GO

ALTER TABLE [dbo].[SR_EMAIL_TEMPLATE] ADD 
CONSTRAINT [PK__SR_EMAIL_TEMPLAT__74C52BCD] PRIMARY KEY CLUSTERED  ([SR_EMAIL_TEMPLATE_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_EMAIL_TEMPLATE] ADD
CONSTRAINT [FK_SR_EMAIL_TEMPLATE_ENTITY] FOREIGN KEY ([EMAIL_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])

GO
