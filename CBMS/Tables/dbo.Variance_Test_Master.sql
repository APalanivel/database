CREATE TABLE [dbo].[Variance_Test_Master]
(
[Variance_Test_Id] [int] NOT NULL IDENTITY(1, 1),
[Is_Inclusive] [bit] NOT NULL CONSTRAINT [DF__Variance___Is_In__182ACE7C] DEFAULT ((0)),
[Is_Multiple_Condition] [bit] NOT NULL CONSTRAINT [DF__Variance___Is_Mu__191EF2B5] DEFAULT ((0))
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Variance test information regarding ''inclusive'' or ''multiple condition testing''', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Test_Master', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Test_Master] ADD 
CONSTRAINT [PK_Variance_Test_Master] PRIMARY KEY CLUSTERED  ([Variance_Test_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
