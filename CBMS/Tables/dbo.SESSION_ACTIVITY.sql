CREATE TABLE [dbo].[SESSION_ACTIVITY]
(
[SESSION_INFO_ID] [int] NOT NULL,
[ACCESS_DATE] [datetime] NOT NULL,
[PAGE_URL] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_UserInfo]
GO
EXEC sp_addextendedproperty N'MS_Description', 'lec 8/16/11 - Used to track CBMS only session activity.  DV also has a session_activity table of same name- but it is tracking DV session activities.', 'SCHEMA', N'dbo', 'TABLE', N'SESSION_ACTIVITY', NULL, NULL
GO

CREATE CLUSTERED INDEX [CI_Session_Activity] ON [dbo].[SESSION_ACTIVITY] ([ACCESS_DATE]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]



GO
