CREATE TABLE [Trade].[RM_Client_Hier_Forecast_Volume]
(
[RM_Client_Hier_Forecast_Volume_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Forecast_Volume] [decimal] (28, 0) NOT NULL,
[Uom_Id] [int] NOT NULL,
[Volume_Source_Cd] [int] NOT NULL,
[Data_Change_Level_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_Forecast_VolumeCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_Forecast_VolumeLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Forecast_Volume] ADD CONSTRAINT [pk_RM_Client_Hier_Forecast_Volume] PRIMARY KEY CLUSTERED  ([RM_Client_Hier_Forecast_Volume_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Forecast_Volume] ADD CONSTRAINT [un_RM_Client_Hier_Forecast_Volume__Client_Hier_Id__Commodity_Id__Service_Month] UNIQUE NONCLUSTERED  ([Client_Hier_Id], [Commodity_Id], [Service_Month]) ON [DB_DATA01]
GO
