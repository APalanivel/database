CREATE TABLE [dbo].[General_UOM_Conversion]
(
[Base_UOM_Cd] [int] NOT NULL,
[Converted_UOM_Cd] [int] NOT NULL,
[Conversion_Factor] [decimal] (32, 16) NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_General_UOM_Conversion_Is_Active] DEFAULT ((1))
) ON [DBData_Cost_Usage]
CREATE CLUSTERED INDEX [CIX_General_UOM_Conversion__Base_UOM_Cd__Converted_UOM_Cd] ON [dbo].[General_UOM_Conversion] ([Base_UOM_Cd], [Converted_UOM_Cd]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

ALTER TABLE [dbo].[General_UOM_Conversion] ADD CONSTRAINT [pk_General_UOM_Conversion] PRIMARY KEY NONCLUSTERED  ([Base_UOM_Cd], [Converted_UOM_Cd]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

GO
EXEC sp_addextendedproperty N'MS_Description', 'Non-Commodity specific conversions', 'SCHEMA', N'dbo', 'TABLE', N'General_UOM_Conversion', NULL, NULL
GO
