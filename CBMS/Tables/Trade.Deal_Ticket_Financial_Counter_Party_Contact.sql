CREATE TABLE [Trade].[Deal_Ticket_Financial_Counter_Party_Contact]
(
[Financial_Counterparty_Client_Contact_Map_Id] [int] NOT NULL,
[Deal_Ticket_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Financial_Counter_Party_Contact__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Financial_Counter_Party_Contact] ADD CONSTRAINT [pk_Deal_Ticket_Financial_Counter_Party_Contact] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Id], [Financial_Counterparty_Client_Contact_Map_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Financial_Counter_Party_Contact] ON [Trade].[Deal_Ticket_Financial_Counter_Party_Contact] ([Financial_Counterparty_Client_Contact_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Financial_Counter_Party_Contact] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Financial_Counter_Party_Contact] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
