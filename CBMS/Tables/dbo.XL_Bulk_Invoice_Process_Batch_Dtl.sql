CREATE TABLE [dbo].[XL_Bulk_Invoice_Process_Batch_Dtl]
(
[XL_Bulk_Data_Process_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[XL_Bulk_Invoice_Process_Batch_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Bucket_Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Bucket_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Uom_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New_Bucket_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New_Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New_Uom_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Run_Data_Quality_Test] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Run_Recalc] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Run_Variance_Test] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status_Cd] [int] NOT NULL,
[Error_Msg] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_XL_Bulk_Invoice_Process_Batch_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] ADD CONSTRAINT [pk_XL_Bulk_Invoice_Process_Batch_Dtl] PRIMARY KEY CLUSTERED  ([XL_Bulk_Data_Process_Batch_Dtl_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_XL_Bulk_Invoice_Process_Batch_Dtl__Cu_Invoice_Id__Bucket_Code] ON [dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] ([Cu_Invoice_Id], [Bucket_Code]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_XL_Bulk_Invoice_Process_Batch_Dtl__XL_Bulk_Invoice_Process_Batch_Id] ON [dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] ([XL_Bulk_Invoice_Process_Batch_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[XL_Bulk_Invoice_Process_Batch_Dtl] ADD CONSTRAINT [fk_XL_Bulk_Invoice_Process_Batch__XL_Bulk_Invoice_Process_Batch_Dtl] FOREIGN KEY ([XL_Bulk_Invoice_Process_Batch_Id]) REFERENCES [dbo].[XL_Bulk_Invoice_Process_Batch] ([XL_Bulk_Invoice_Process_Batch_Id])
GO
