CREATE TABLE [dbo].[Geographic_Region_Country_Map]
(
[Geographic_Region_Country_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Geographic_Region_Id] [int] NOT NULL,
[COUNTRY_ID] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region_Country_Map] ADD CONSTRAINT [pk_Geographic_Region_Country_Map] PRIMARY KEY CLUSTERED  ([Geographic_Region_Country_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region_Country_Map] ADD CONSTRAINT [un_Geographic_Region_Country_Map__Geographic_Region_Id__COUNTRY_ID] UNIQUE NONCLUSTERED  ([Geographic_Region_Id], [COUNTRY_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Geographic_Region_Country_Map] ADD CONSTRAINT [fk_COUNTRY__Geographic_Region_Country_Map] FOREIGN KEY ([COUNTRY_ID]) REFERENCES [dbo].[COUNTRY] ([COUNTRY_ID])
GO
ALTER TABLE [dbo].[Geographic_Region_Country_Map] ADD CONSTRAINT [fk_Geographic_Region__Geographic_Region_Country_Map] FOREIGN KEY ([Geographic_Region_Id]) REFERENCES [dbo].[Geographic_Region] ([Geographic_Region_Id])
GO
