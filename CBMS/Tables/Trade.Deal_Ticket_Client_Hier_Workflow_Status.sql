CREATE TABLE [Trade].[Deal_Ticket_Client_Hier_Workflow_Status]
(
[Deal_Ticket_Client_Hier_Workflow_Status_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Client_Hier_Id] [int] NOT NULL,
[Workflow_Status_Map_Id] [int] NOT NULL,
[Workflow_Status_Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_Workflow_Status__Is_Active] DEFAULT ((1)),
[Last_Notification_Sent_Ts] [datetime] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_Workflow_Status__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_Workflow_Status__Last_Change_Ts] DEFAULT (getdate()),
[CBMS_Image_Id] [int] NULL,
[Trade_Month] [date] NULL,
[Workflow_Task_Id] [int] NULL,
[Contract_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ADD CONSTRAINT [pk_Deal_Ticket_Client_Hier_Workflow_Status] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Hier_Workflow_Status_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Client_Hier_Workflow_Status_Deal_Ticket_Client_Hier_Id_Is_Active] ON [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ([Deal_Ticket_Client_Hier_Id], [Is_Active]) INCLUDE ([Last_Change_Ts], [Workflow_Status_Map_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Client_Hier_Workflow_Status_Workflow_Task_Id] ON [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ([Workflow_Task_Id]) INCLUDE ([Deal_Ticket_Client_Hier_Id], [Deal_Ticket_Client_Hier_Workflow_Status_Id], [Last_Change_Ts], [Workflow_Status_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ADD CONSTRAINT [fk_Deal_Ticket_Client_Hier__Deal_Ticket_Client_Hier_Workflow_Status] FOREIGN KEY ([Deal_Ticket_Client_Hier_Id]) REFERENCES [Trade].[Deal_Ticket_Client_Hier] ([Deal_Ticket_Client_Hier_Id])
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ADD CONSTRAINT [fk_Workflow_Status_Map__Deal_Ticket_Client_Hier_Workflow_Status] FOREIGN KEY ([Workflow_Status_Map_Id]) REFERENCES [Trade].[Workflow_Status_Map] ([Workflow_Status_Map_Id])
GO
