CREATE TABLE [dbo].[RM_Scenario_Planning_Iteration_Dtl]
(
[RM_Scenario_Planning_Iteration_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Scenario_Input_Name_Cd] [int] NOT NULL,
[Scenario_Input_Value] [decimal] (32, 16) NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Planning_Iteration_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Planning_Iteration_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management scenario report iteration details', 'SCHEMA', N'dbo', 'TABLE', N'RM_Scenario_Planning_Iteration_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[RM_Scenario_Planning_Iteration_Dtl] ADD CONSTRAINT [pk_RM_Scenario_Planning_Iteration_Dtl] PRIMARY KEY CLUSTERED  ([RM_Scenario_Planning_Iteration_Id], [Service_Month], [Scenario_Input_Name_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Scenario_Planning_Iteration_Dtl] ADD CONSTRAINT [fk_RM_Scenario_Planning_Iteration__RM_Scenario_Planning_Iteration_Dtl] FOREIGN KEY ([RM_Scenario_Planning_Iteration_Id]) REFERENCES [dbo].[RM_Scenario_Planning_Iteration] ([RM_Scenario_Planning_Iteration_Id])
GO
