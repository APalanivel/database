CREATE TABLE [Trade].[deal_tickets]
(
[deal_tickets_unique_id] [bigint] NOT NULL CONSTRAINT [DF__deal_tick__deal___7419028B] DEFAULT ((0)),
[deal_tickets_site_details_id] [bigint] NOT NULL CONSTRAINT [DF__deal_tick__deal___750D26C4] DEFAULT ((0)),
[deal_tickets_price_id] [bigint] NULL,
[cbms_deal_ticket_id] [int] NOT NULL CONSTRAINT [DF__deal_tick__cbms___76014AFD] DEFAULT ((0)),
[cbms_trade_number] [int] NOT NULL CONSTRAINT [DF__deal_tick__cbms___76F56F36] DEFAULT ((0)),
[cbms_contract_id] [int] NOT NULL CONSTRAINT [DF__deal_tick__cbms___77E9936F] DEFAULT ((0)),
[cbms_site_client_hier_id] [int] NOT NULL CONSTRAINT [DF__deal_tick__cbms___78DDB7A8] DEFAULT ((0)),
[cbms_counterparty_id] [int] NULL,
[period_from_date] [date] NOT NULL CONSTRAINT [DF__deal_tick__perio__79D1DBE1] DEFAULT ('01-jan-0001'),
[trade_price] [float] NULL,
[mid_price] [float] NULL,
[market_price] [float] NULL,
[initial_quote] [float] NULL,
[status] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[comment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[data_amended_by] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[date_data_amended] [datetime2] (6) NULL,
[transfer_timestamp] [datetime2] (6) NULL,
[assignee] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[id] [int] NOT NULL CONSTRAINT [DF__deal_tickets__id__7AC6001A] DEFAULT ((0)),
[trade_date] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[deal_tickets] ADD CONSTRAINT [PK__deal_tic__3213E83F7230BA19] PRIMARY KEY CLUSTERED  ([id]) ON [DB_DATA01]
GO
