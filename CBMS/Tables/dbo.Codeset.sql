CREATE TABLE [dbo].[Codeset]
(
[Codeset_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Codeset_Name] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[codeset_Dsc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Std_Column_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Grouping of Codes', 'SCHEMA', N'dbo', 'TABLE', N'Codeset', NULL, NULL
GO

ALTER TABLE [dbo].[Codeset] ADD CONSTRAINT [unc_Codeset_Codeset_Name] UNIQUE CLUSTERED  ([Codeset_Name]) WITH (FILLFACTOR=90) ON [DBData_Reference]

ALTER TABLE [dbo].[Codeset] ADD CONSTRAINT [PK_Codeset] PRIMARY KEY NONCLUSTERED  ([Codeset_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]

GO
