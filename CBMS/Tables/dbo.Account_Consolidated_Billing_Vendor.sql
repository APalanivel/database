CREATE TABLE [dbo].[Account_Consolidated_Billing_Vendor]
(
[Account_Consolidated_Billing_Vendor_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Account_Id] [int] NOT NULL,
[Billing_Start_Dt] [date] NOT NULL,
[Billing_End_Dt] [date] NULL,
[Invoice_Vendor_Type_Id] [int] NOT NULL,
[Supplier_Vendor_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df__Account_Consolidated_Billing_Vendor__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df__Account_Consolidated_Billing_Vendor__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
CREATE NONCLUSTERED INDEX [ix_Account_Consolidated_Billing_Vendor_Account_Id] ON [dbo].[Account_Consolidated_Billing_Vendor] ([Account_Id]) INCLUDE ([Billing_End_Dt], [Billing_Start_Dt]) ON [DB_DATA01]

GO
ALTER TABLE [dbo].[Account_Consolidated_Billing_Vendor] ADD CONSTRAINT [pk_Account_Consolidated_Billing_Vendor] PRIMARY KEY CLUSTERED  ([Account_Consolidated_Billing_Vendor_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Account_Consolidated_Billing_Vendor__Account_Id__Billing_Start_Dt__Billing_End_Dt] ON [dbo].[Account_Consolidated_Billing_Vendor] ([Account_Id], [Billing_Start_Dt], [Billing_End_Dt]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Consolidated_Billing_Vendor__Supplier_Vendor_Id] ON [dbo].[Account_Consolidated_Billing_Vendor] ([Supplier_Vendor_Id]) ON [DB_DATA01]
GO
