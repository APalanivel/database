CREATE TABLE [Workflow].[User_Last_Visited_Filter]
(
[User_Last_Visited_Filter_Id] [int] NOT NULL IDENTITY(1, 1),
[User_Info_Id] [int] NOT NULL,
[Workflow_Queue_Id] [int] NOT NULL,
[Workflow_Queue_Saved_Filter_Query_Id] [int] NULL,
[Last_Visited_Ts] [datetime] NOT NULL CONSTRAINT [df_User_Last_Visited_Filter__Last_Visited_Ts] DEFAULT (getdate()),
[Session_Id] [int] NOT NULL,
[Workflow_Queue_Saved_Filter_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[User_Last_Visited_Filter] ADD CONSTRAINT [pk_User_Last_Visited_Filter] PRIMARY KEY CLUSTERED  ([User_Info_Id], [User_Last_Visited_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[User_Last_Visited_Filter] ADD CONSTRAINT [un_User_Last_Visited_Filter__User_Info_Id__Workflow_Queue_Id] UNIQUE NONCLUSTERED  ([User_Info_Id], [Workflow_Queue_Id], [Session_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[User_Last_Visited_Filter] ADD CONSTRAINT [fk_Workflow_Queue__User_Last_Visited_Filter] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
