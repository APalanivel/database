CREATE TABLE [dbo].[Client_Hier_Account_Attribute_Tracking]
(
[Client_Hier_Account_Attribute_Tracking_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Client_Attribute_Id] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NOT NULL,
[Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_Account_Attribute_Tracking] ADD CONSTRAINT [pk_Client_Hier_Account_Attribute_Tracking] PRIMARY KEY CLUSTERED  ([Client_Hier_Account_Attribute_Tracking_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [IX_Client_Hier_Account_Attribute_Tracking__Account_Id] ON [dbo].[Client_Hier_Account_Attribute_Tracking] ([Account_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Account_Attribute_Tracking__Client_Attribute_Id] ON [dbo].[Client_Hier_Account_Attribute_Tracking] ([Client_Attribute_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_Account_Attribute_Tracking] ADD CONSTRAINT [un_Client_Hier_Account_Attribute_Tracking__Client_Hier_Id__Account_Id__Commodity_Id__Client_Attribute_id__Start_Dt__End_Dt] UNIQUE NONCLUSTERED  ([Client_Hier_Id], [Account_Id], [Commodity_Id], [Client_Attribute_Id], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
