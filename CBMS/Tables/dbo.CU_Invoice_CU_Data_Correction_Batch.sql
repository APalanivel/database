CREATE TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch]
(
[CU_Invoice_CU_Data_Correction_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NULL,
[Status_Cd] [int] NOT NULL,
[Is_Notification_Required] [bit] NOT NULL CONSTRAINT [df_CU_Invoice_CU_Data_Correction_Batch__Is_Notification_Required] DEFAULT ((0)),
[Requested_User_Id] [int] NOT NULL,
[Requested_Ts] [datetime] NOT NULL CONSTRAINT [df_CU_Invoice_CU_Data_Correction_Batch__Requested_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_CU_Invoice_CU_Data_Correction_Batch__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch] ADD CONSTRAINT [pk_CU_Invoice_CU_Data_Correction_Batch] PRIMARY KEY CLUSTERED  ([CU_Invoice_CU_Data_Correction_Batch_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_CU_Invoice_CU_Data_Correction_Batch__Contract_Id] ON [dbo].[CU_Invoice_CU_Data_Correction_Batch] ([Contract_Id]) ON [DB_DATA01]
GO
