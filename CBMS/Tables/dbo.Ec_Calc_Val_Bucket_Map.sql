CREATE TABLE [dbo].[Ec_Calc_Val_Bucket_Map]
(
[Ec_Calc_Val_Bucket_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Ec_Calc_Val_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Ec_Calc_Val_Bucket_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Map] ADD CONSTRAINT [pk_Ec_Calc_Val_Bucket_Map] PRIMARY KEY CLUSTERED  ([Ec_Calc_Val_Bucket_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Map] ADD CONSTRAINT [unc_Ec_Calc_Val_Bucket_Map__Ec_Calc_Val_Id__Bucket_Master_Id] UNIQUE NONCLUSTERED  ([Ec_Calc_Val_Id], [Bucket_Master_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Map] ADD CONSTRAINT [fk_Ec_Calc_Val__Ec_Calc_Val_Bucket_Map] FOREIGN KEY ([Ec_Calc_Val_Id]) REFERENCES [dbo].[EC_Calc_Val] ([EC_Calc_Val_Id])
GO
