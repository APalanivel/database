CREATE TABLE [dbo].[Security_Role_Client_Hier]
(
[Security_Role_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL
) ON [DBData_UserInfo]
GO

EXEC sp_addextendedproperty N'MS_Description', N'Client Hierarchys available for a security role. Only divisions will have a record in the Security_Role_Client_Hier, user/global sitegroups are not.', 'SCHEMA', N'dbo', 'TABLE', N'Security_Role_Client_Hier', NULL, NULL
GO


ALTER TABLE [dbo].[Security_Role_Client_Hier] ADD 
CONSTRAINT [pk_Security_Role_Client_Hier] PRIMARY KEY CLUSTERED  ([Security_Role_Id], [Client_Hier_Id]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
CREATE NONCLUSTERED INDEX [IX_Security_Role_Client_Hier__Security_Role_Id] ON [dbo].[Security_Role_Client_Hier] ([Client_Hier_Id], [Security_Role_Id]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Security_Role_Client_Hier] ADD
CONSTRAINT [fk_Security_Role_Client_Hier_Security_Role] FOREIGN KEY ([Security_Role_Id]) REFERENCES [dbo].[Security_Role] ([Security_Role_Id])




GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/******
NAME: tr_Security_Role_Client_Hier__Update_Security_Role_Last_Change_Ts

DESCRIPTION: Update the Security_Role Last_Change_Ts when a Security_Role_Client_Hier record changes

AUTHOR INITIALS:
	Initials	Name
-------------------------------------------------------------------------------------------------------------------------------------------------------------
	CMH		Chad Hattabaugh
	RR		Raghu Reddy
	TP		Anoop
	
MODIFICATIONS

	Initials	Date		Modification
-------------------------------------------------------------------------------------------------------------------------------------------------------------

	CMH			02/24/2012	Created
	RR			2012-05-09	MAINT-1248 Replaced inner join with full outer join in the sub select between the inserted and deleted tables.
							No records will be returned by the sub select when insert,delete happens as only one table inserted,deleted will have the data.
	KVK			10/16/2012  changed the threshold check to 15min.

	TP			12 Feb 2014	modified to send the transfer messages (maint-2513 - user info service broker)
******/
CREATE TRIGGER [dbo].[tr_Security_Role_Client_Hier__Update_Security_Role_Last_Change_Ts] ON [dbo].[Security_Role_Client_Hier]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            sr
      SET   
            Last_Change_Ts = getdate()
      FROM
            Security_Role sr
            INNER JOIN ( SELECT
                              isnull(i.Security_Role_Id, d.Security_Role_Id) AS Security_Role_Id
                             ,isnull(i.Client_Hier_Id, d.Client_Hier_Id) AS Client_Hier_Id
                         FROM
                              INSERTED i
                              FULL OUTER JOIN DELETED d
                                    ON i.Security_Role_Id = d.Security_Role_Id
                                       AND i.Client_Hier_Id = d.Client_Hier_Id ) AS x
                  ON x.Security_Role_Id = sr.Security_Role_Id
            INNER JOIN Core.Client_Hier CH
                  ON CH.Client_Hier_Id = x.Client_Hier_Id
      WHERE
            datediff(MINUTE, CH.Client_Hier_Created_Ts, getdate()) > 15


      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
		   ,@From_Service NVARCHAR(255)
           ,@Message XML
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	
      SET @Message = ( SELECT 
                        i.Security_Role_Id AS Security_Role_Id
                       ,i.Client_Hier_Id AS Client_Hier_Id
                       ,d.Security_Role_Id AS Old_Security_Role_Id
                       ,d.Client_Hier_Id AS Old_Client_Hier_Id
                       ,case WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NULL THEN 'I'
                             WHEN i.Security_Role_Id IS NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'D'
                             WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'U'  /**One column check is enough**/
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Security_Role_Id = d.Security_Role_Id
                                 AND i.Client_Hier_Id = d.Client_Hier_Id
            FOR
                       XML PATH('Security_Role_Client_Hier_Change')
                          ,ELEMENTS
                          ,ROOT('Security_Role_Client_Hier_Changes') );

      IF @Message IS NOT NULL 
            BEGIN

			SET @From_Service = N'//Change_Control/Service/CBMS/Security_Role_Transfer'
	
							-- Cycle through the targets in Change_Control Targt and sent themessage to each    
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Security_Role_Client_Hier')     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0 
                        BEGIN    
						
							EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT
						 
                            ;SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Security_Role_Client_Hier_Transfer] (@Message);


                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    

                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  

            END
END;


;
GO
GO
