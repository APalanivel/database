CREATE TABLE [Stage].[Bucket_Site_Interval_Dtl_Transfer]
(
[Bucket_Site_Interval_Dtl_Transfer_Id] [int] NOT NULL IDENTITY(1, 1),
[Sys_Change_Operation] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Service_Start_Dt] [date] NOT NULL,
[Service_End_Dt] [date] NOT NULL,
[Bucket_Daily_Avg_Value] [decimal] (28, 10) NULL,
[Data_Source_Cd] [int] NOT NULL,
[Uom_Type_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Created_User_Id] [int] NULL,
[Updated_User_Id] [int] NULL,
[Created_Ts] [datetime] NULL,
[Last_Changed_Ts] [datetime] NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Staging table to hold the data transferred from DVDEHub', 'SCHEMA', N'Stage', 'TABLE', N'Bucket_Site_Interval_Dtl_Transfer', NULL, NULL
GO

ALTER TABLE [Stage].[Bucket_Site_Interval_Dtl_Transfer] ADD CONSTRAINT [pk_Bucket_Site_Interval_Dtl_Transfer] PRIMARY KEY CLUSTERED  ([Bucket_Site_Interval_Dtl_Transfer_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
CREATE UNIQUE NONCLUSTERED INDEX [unc_Bucket_Site_Interval_Dtl_Transfer__Client_Hier_Id__Bucket_Master_Id] ON [Stage].[Bucket_Site_Interval_Dtl_Transfer] ([Client_Hier_Id], [Bucket_Master_Id], [Service_Start_Dt], [Service_End_Dt], [Data_Source_Cd]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
