CREATE TABLE [dbo].[APP_MENU]
(
[APP_MENU_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[APP_MENU_PROFILE_ID] [int] NOT NULL,
[PERMISSION_INFO_ID] [int] NOT NULL,
[DISPLAY_TEXT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DISPLAY_IMAGE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MENU_DESCRIPTION] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TARGET_ACTION] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MENU_LEVEL] [smallint] NOT NULL,
[DISPLAY_ORDER] [smallint] NOT NULL,
[PARENT_MENU_ID] [int] NULL,
[APP_MODULE_ID] [int] NULL,
[TARGET_SERVER] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Only_Advanced_Visualization] [bit] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'individual menu items that exist in applications', 'SCHEMA', N'dbo', 'TABLE', N'APP_MENU', NULL, NULL
GO

ALTER TABLE [dbo].[APP_MENU] ADD 
CONSTRAINT [PK_APP_MENU] PRIMARY KEY CLUSTERED  ([APP_MENU_ID]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
