CREATE TABLE [Trade].[Deal_Ticket_Audit]
(
[Deal_Ticket_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Audit_Type_Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[User_Info_Id] [int] NOT NULL,
[Audit_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Audit__Audit_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Audit] ADD CONSTRAINT [pk_Deal_Ticket_Audit] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Audit_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Audit__Deal_Ticket_Id] ON [Trade].[Deal_Ticket_Audit] ([Deal_Ticket_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Audit] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Audit] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
