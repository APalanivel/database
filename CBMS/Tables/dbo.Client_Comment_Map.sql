CREATE TABLE [dbo].[Client_Comment_Map]
(
[Client_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table regarding client comments/notes.  Junction is between the client and comment tables.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Comment_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Comment_Map] ADD 
CONSTRAINT [pk_Client_Comment_Map] PRIMARY KEY CLUSTERED  ([Client_Id], [Comment_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
ALTER TABLE [dbo].[Client_Comment_Map] ADD
CONSTRAINT [fk_Client__Client_Comment_Map] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
ALTER TABLE [dbo].[Client_Comment_Map] ADD
CONSTRAINT [fk_Comment__Client_Comment_Map] FOREIGN KEY ([Comment_Id]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
