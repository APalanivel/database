CREATE TABLE [dbo].[Filtered_Code_Hier_Type]
(
[Filtered_Code_Hier_Type_Id] [int] NOT NULL,
[Filtered_Code_Hier_Type_Dsc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Reference] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of Child/Parent hieracrchy relationship', 'SCHEMA', N'dbo', 'TABLE', N'Filtered_Code_Hier_Type', NULL, NULL
GO

ALTER TABLE [dbo].[Filtered_Code_Hier_Type] ADD CONSTRAINT [pk_Filtered_Code_Hier_Type] PRIMARY KEY CLUSTERED  ([Filtered_Code_Hier_Type_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
