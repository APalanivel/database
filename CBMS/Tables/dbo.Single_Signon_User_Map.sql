CREATE TABLE [dbo].[Single_Signon_User_Map]
(
[Provider_XUser_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_ID] [int] NOT NULL,
[Internal_User_ID] [int] NOT NULL
) ON [DBData_UserInfo]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of CBMS user IDs to CBMS Client ID to SIngleSignOnUserName', 'SCHEMA', N'dbo', 'TABLE', N'Single_Signon_User_Map', NULL, NULL
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	tr_Single_Signon_User_Map_Transfer

DESCRIPTION: Trigger to send changes for DV2 server through service_broker.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
******/
CREATE TRIGGER [dbo].[tr_Single_Signon_User_Map_Transfer] ON [dbo].[Single_Signon_User_Map]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT
                        isnull(i.Provider_XUser_Name, d.Provider_XUser_Name) AS Provider_XUser_Name
                       ,isnull(i.Client_ID, d.Client_ID) AS Client_ID
                       ,i.Internal_User_ID
                       ,case WHEN i.Client_Id IS NOT NULL
                                  AND d.Client_Id IS NULL THEN 'I'
                             WHEN i.Client_Id IS NULL
                                  AND d.Client_Id IS NOT NULL THEN 'D'
                             WHEN i.Client_Id IS NOT NULL
                                  AND d.Client_Id IS NOT NULL THEN 'U'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Provider_XUser_Name = d.Provider_XUser_Name
                                 AND i.Client_ID = d.Client_ID
            FOR
                       XML PATH('Single_Signon_User_Map_Change')
                          ,ELEMENTS
                          ,ROOT('Single_Signon_User_Map_Changes') );

      IF @Message IS NOT NULL 
            BEGIN
	
                  SET @Conversation_Handle = NULL;
                  SET @From_Service = '//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = '//Change_Control/Service/DV2/User_Info_Transfer'
                  SET @Target_Contract = '//Change_Control/Contract/Single_Signon_User_Map_Transfer'

                  
                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Single_Signon_User_Map_Transfer] (@Message);

            END
END;
;
GO


ALTER TABLE [dbo].[Single_Signon_User_Map] ADD 
CONSTRAINT [pk_Single_Signon_User_Map] PRIMARY KEY CLUSTERED  ([Provider_XUser_Name], [Client_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
GO
