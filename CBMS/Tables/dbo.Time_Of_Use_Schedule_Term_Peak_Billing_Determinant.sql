CREATE TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant]
(
[BILLING_DETERMINANT_ID] [int] NOT NULL,
[Time_Of_Use_Schedule_Term_Peak_Id] [int] NOT NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of term / peak billing determinants', 'SCHEMA', N'dbo', 'TABLE', N'Time_Of_Use_Schedule_Term_Peak_Billing_Determinant', NULL, NULL
GO

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] ADD 
CONSTRAINT [pk_Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] PRIMARY KEY CLUSTERED  ([BILLING_DETERMINANT_ID], [Time_Of_Use_Schedule_Term_Peak_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term_Peak_Billing_Determinant__Time_Of_Use_Schedule_Term_Peak_Id] ON [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] ([Time_Of_Use_Schedule_Term_Peak_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] ADD
CONSTRAINT [fk_BILLING_DETERMINANT__Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] FOREIGN KEY ([BILLING_DETERMINANT_ID]) REFERENCES [dbo].[BILLING_DETERMINANT] ([BILLING_DETERMINANT_ID])
ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] ADD
CONSTRAINT [fk_Time_Of_Use_Schedule_Term_Peak__Time_Of_Use_Schedule_Term_Peak_Billing_Determinant] FOREIGN KEY ([Time_Of_Use_Schedule_Term_Peak_Id]) REFERENCES [dbo].[Time_Of_Use_Schedule_Term_Peak] ([Time_Of_Use_Schedule_Term_Peak_Id])
GO
