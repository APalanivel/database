CREATE TABLE [dbo].[Standing_Data_Exception_Group_Map]
(
[Standing_Data_Exception_Group_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Exception_Type_Cd] [int] NULL,
[Exception_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Managed_By_Group_Info_Id] [int] NULL,
[Managed_By_User_Info_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Standing_Data_Exception_Group_Map_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Standing_Data_Exception_Group_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Standing_Data_Exception_Group_Map] ADD CONSTRAINT [pk_Standing_Data_Exception_Group_Map] PRIMARY KEY CLUSTERED  ([Standing_Data_Exception_Group_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Standing_Data_Exception_Group_Map] ADD CONSTRAINT [un_Standing_Data_Exception_Group_Map] UNIQUE NONCLUSTERED  ([Exception_Type_Cd]) ON [DB_DATA01]
GO
