CREATE TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak]
(
[Time_Of_Use_Schedule_Term_Peak_Id] [int] NOT NULL IDENTITY(1, 1),
[Peak_Name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Time_Of_Use_Schedule_Term_Id] [int] NOT NULL,
[SEASON_ID] [int] NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maping of terms to peak', 'SCHEMA', N'dbo', 'TABLE', N'Time_Of_Use_Schedule_Term_Peak', NULL, NULL
GO

CREATE UNIQUE CLUSTERED INDEX [ucix_Time_Of_Use_Schedule_Term_Peak__Time_Of_Use_Schedule_Term_Id__SEASON_ID__Peak_Name] ON [dbo].[Time_Of_Use_Schedule_Term_Peak] ([Time_Of_Use_Schedule_Term_Id], [SEASON_ID], [Peak_Name]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak] ADD CONSTRAINT [pk_Time_Of_Use_Schedule_Term_Peak] PRIMARY KEY NONCLUSTERED  ([Time_Of_Use_Schedule_Term_Peak_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term_Peak__Time_Of_Use_Schedule_Term_Id] ON [dbo].[Time_Of_Use_Schedule_Term_Peak] ([Time_Of_Use_Schedule_Term_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term_Peak__SEASON_ID] ON [dbo].[Time_Of_Use_Schedule_Term_Peak] ([SEASON_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak] ADD
CONSTRAINT [fk_SEASON__Time_Of_Use_Schedule_Term_Peak] FOREIGN KEY ([SEASON_ID]) REFERENCES [dbo].[SEASON] ([SEASON_ID])
ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Peak] ADD
CONSTRAINT [fk_Time_Of_Use_Schedule_Term__Time_Of_Use_Schedule_Term_Peak] FOREIGN KEY ([Time_Of_Use_Schedule_Term_Id]) REFERENCES [dbo].[Time_Of_Use_Schedule_Term] ([Time_Of_Use_Schedule_Term_Id])
GO
