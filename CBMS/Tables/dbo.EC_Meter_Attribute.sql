CREATE TABLE [dbo].[EC_Meter_Attribute]
(
[EC_Meter_Attribute_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[State_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[EC_Meter_Attribute_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Attribute_Type_Cd] [int] NOT NULL,
[Is_Used_In_Calc_Vals] [bit] NOT NULL CONSTRAINT [df_EC_Meter_Attribute__Is_Used_In_Calc_Vals] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute__Last_Change_Ts] DEFAULT (getdate()),
[Attribute_Use_Cd] [int] NOT NULL CONSTRAINT [DF__EC_Meter___Attri__3F98EEB1] DEFAULT ((-1)),
[Vendor_Type_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute] ADD CONSTRAINT [pk_EC_Meter_Attribute] PRIMARY KEY CLUSTERED  ([EC_Meter_Attribute_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute] ADD CONSTRAINT [unc_EC_Meter_Attribute__State_Id__Commodity_Id__Meter_Attribute_Type__Cd_EC_Meter_Attribute_Name] UNIQUE NONCLUSTERED  ([State_Id], [Commodity_Id], [Vendor_Type_Cd], [EC_Meter_Attribute_Name]) ON [DB_DATA01]
GO
