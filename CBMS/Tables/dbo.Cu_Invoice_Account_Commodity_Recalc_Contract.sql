CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract]
(
[Cu_Invoice_Account_Commodity_Recalc_Contract_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Account_Commodity_Id] [int] NOT NULL,
[Contract_Id] [int] NOT NULL,
[Contract_Recalc_Begin_Dt] [date] NOT NULL,
[Contract_Recalc_End_Dt] [date] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_Recalc_Contract__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_Recalc_Contract__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract] ADD CONSTRAINT [pk_Cu_Invoice_Account_Commodity_Recalc_Contract] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Account_Commodity_Recalc_Contract_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract] ADD CONSTRAINT [uix_Cu_Invoice_Account_Commodity_Recalc_Contract__Cu_Invoice_Account_Commodity_Id__Contract_Id] UNIQUE NONCLUSTERED  ([Cu_Invoice_Account_Commodity_Id], [Contract_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Contract] ADD CONSTRAINT [fk_Cu_Invoice_Account_Commodity__Cu_Invoice_Account_Commodity_Recalc_Contract] FOREIGN KEY ([Cu_Invoice_Account_Commodity_Id]) REFERENCES [dbo].[Cu_Invoice_Account_Commodity] ([Cu_Invoice_Account_Commodity_Id])
GO
