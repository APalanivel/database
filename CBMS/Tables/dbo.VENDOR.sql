CREATE TABLE [dbo].[VENDOR]
(
[VENDOR_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[VENDOR_TYPE_ID] [int] NOT NULL,
[VENDOR_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VENDOR_DESCRIPTION] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero29] DEFAULT ((0))
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Vendor_upd_Client_Hier_Account
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier_Account table when ever the updates happen in Vendor table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		UPDATE VENDOR
		SET vendor_name = 'Test_Vendor'
		WHERE 
		VENDOR_ID = 866
		
		SELECT * FROM VENDOR where vendor_id = 866
		SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE Account_Vendor_Name = 'Test_Vendor'
		
	ROLLBACK TRAN	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri


 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/20/2010	Created
			10/18/2010	Added update for Column Account_Vendor_Type_ID
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
	
******/

CREATE TRIGGER [dbo].[tr_Vendor_upd_Client_Hier_Account] ON [dbo].[VENDOR]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
      UPDATE
            cha
      SET   
            Account_Vendor_Name = i.VENDOR_NAME
           ,Account_Vendor_Type_ID = i.Vendor_Type_ID
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                  ON i.Vendor_Id = cha.Account_Vendor_Id
            INNER JOIN DELETED d
                  ON i.Vendor_Id = d.Vendor_Id
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.VENDOR_NAME
                                   ,i.Vendor_Type_ID
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.VENDOR_NAME
                                                                       ,d.Vendor_Type_ID
                                                        FOR
                                                                  XML RAW ))
                        
END 
;
GO
ALTER TABLE [dbo].[VENDOR] ADD CONSTRAINT [VENDOR_PK] PRIMARY KEY CLUSTERED  ([VENDOR_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [VENDOR_U_1] ON [dbo].[VENDOR] ([VENDOR_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [VENDOR_N_1] ON [dbo].[VENDOR] ([VENDOR_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[VENDOR] ADD CONSTRAINT [ENTITY_VENDOR_FK] FOREIGN KEY ([VENDOR_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table stores the vendor information, both utility and suppliers', 'SCHEMA', N'dbo', 'TABLE', N'VENDOR', NULL, NULL
GO
