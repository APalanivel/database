CREATE TABLE [Trade].[Workflow_Status_Map]
(
[Workflow_Status_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Id] [int] NOT NULL,
[Workflow_Status_Id] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Workflow_Status_Map__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Status_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Status_Map__Last_Change_Ts] DEFAULT (getdate()),
[Workflow_Status_Default_Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Status_Map] ADD CONSTRAINT [pk_Workflow_Status_Map] PRIMARY KEY CLUSTERED  ([Workflow_Status_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Status_Map] ADD CONSTRAINT [un_Workflow_Status_Map__Workflow_Id__Workflow_Status_Id] UNIQUE NONCLUSTERED  ([Workflow_Id], [Workflow_Status_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Status_Map] ADD CONSTRAINT [fk_Workflow__Workflow_Status_Map] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
ALTER TABLE [Trade].[Workflow_Status_Map] ADD CONSTRAINT [fk_Workflow_Status__Workflow_Status_Map] FOREIGN KEY ([Workflow_Status_Id]) REFERENCES [Trade].[Workflow_Status] ([Workflow_Status_Id])
GO
