CREATE TABLE [Budget].[Account_Commodity_Primary_Meter]
(
[Account_Commodity_Primary_Meter_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Meter_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Primary_Meter__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Primary_Meter__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Primary_Meter] ADD CONSTRAINT [pk_Account_Commodity_Primary_Meter] PRIMARY KEY CLUSTERED  ([Account_Commodity_Primary_Meter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Primary_Meter] ADD CONSTRAINT [un_Account_Commodity_Primary_Meter__Account_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([Account_Id], [Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Primary_Meter] ADD CONSTRAINT [fk__Account_Commodity_Primary_Meter__Account_Id] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
