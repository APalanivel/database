CREATE TABLE [dbo].[Account_Variance_Consumption_Level]
(
[ACCOUNT_ID] [int] NOT NULL,
[Variance_Consumption_Level_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Variance consumption level associated to an account. From these variance consumption levels, variance rules for invoice processing are associated. Account may have multiple variance_consumption_level_ids, but only one per commodity.', 'SCHEMA', N'dbo', 'TABLE', N'Account_Variance_Consumption_Level', NULL, NULL
GO

CREATE CLUSTERED INDEX [CI_Account_Variance_Consumption_Level_Account_ID] ON [dbo].[Account_Variance_Consumption_Level] ([ACCOUNT_ID], [Variance_Consumption_Level_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Account_Variance_Consumption_Level] ADD CONSTRAINT [PK_Account_Variance_Consumption_Level] PRIMARY KEY NONCLUSTERED  ([Variance_Consumption_Level_Id], [ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [IX_Account_Variance_Consumption_Level__Account_ID] ON [dbo].[Account_Variance_Consumption_Level] ([ACCOUNT_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Account_Variance_Consumption_Level] ADD
CONSTRAINT [FK_ACCOUNTAccount_Variance_Consumption_Level] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
ALTER TABLE [dbo].[Account_Variance_Consumption_Level] ADD
CONSTRAINT [FK_Variance_Consumption_LevelAccount_Variance_Consumption_Level] FOREIGN KEY ([Variance_Consumption_Level_Id]) REFERENCES [dbo].[Variance_Consumption_Level] ([Variance_Consumption_Level_Id])




GO
