CREATE TABLE [dbo].[Contract_Cu_Invoice_Map]
(
[Contract_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Cu_Invoice_Map] ADD CONSTRAINT [Pk_Contract_Cu_Invoice_Map__Contract_Id__Cu_Invoice_Id_Account_Id] PRIMARY KEY CLUSTERED  ([Contract_Id], [Cu_Invoice_Id], [Account_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Contract_Cu_Invoice_Map__Cu_Invoice_Id] ON [dbo].[Contract_Cu_Invoice_Map] ([Cu_Invoice_Id]) ON [DB_DATA01]
GO
