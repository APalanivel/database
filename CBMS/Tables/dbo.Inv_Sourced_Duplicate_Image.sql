CREATE TABLE [dbo].[Inv_Sourced_Duplicate_Image]
(
[Inv_Sourced_Duplicate_Image_Id] [int] NOT NULL IDENTITY(1, 1),
[Image_File_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Image_Hash_Value] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Duplicate_Image_Inv_Source_Id] [int] NOT NULL,
[Original_Inv_Sourced_Image_Id] [int] NOT NULL,
[Keyword] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Processed_Ts] [datetime] NOT NULL CONSTRAINT [df_Inv_Sourced_Duplicate_Image__Processed_Ts] DEFAULT (getdate()),
[Inv_Sourced_Image_Batch_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Inv_Sourced_Duplicate_Image] ADD CONSTRAINT [pk_Inv_Sourced_Duplicate_Image] PRIMARY KEY CLUSTERED  ([Inv_Sourced_Duplicate_Image_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [IX_Inv_Sourced_Duplicate_Image__Duplicate_Image_Inv_Source_Id__Inv_Sourced_Image_Batch_Id__Include] ON [dbo].[Inv_Sourced_Duplicate_Image] ([Duplicate_Image_Inv_Source_Id], [Inv_Sourced_Image_Batch_Id]) INCLUDE ([Keyword]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Inv_Sourced_Duplicate_Image] ADD CONSTRAINT [fk_INV_SOURCE__Inv_Sourced_Duplicate_Image] FOREIGN KEY ([Duplicate_Image_Inv_Source_Id]) REFERENCES [dbo].[INV_SOURCE] ([INV_SOURCE_ID])
GO
ALTER TABLE [dbo].[Inv_Sourced_Duplicate_Image] ADD CONSTRAINT [fk_INV_SOURCED_IMAGE__Inv_Sourced_Duplicate_Image] FOREIGN KEY ([Original_Inv_Sourced_Image_Id]) REFERENCES [dbo].[INV_SOURCED_IMAGE] ([INV_SOURCED_IMAGE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Listing of images that are considered duplicates.  Includes a hash value of the image.', 'SCHEMA', N'dbo', 'TABLE', N'Inv_Sourced_Duplicate_Image', NULL, NULL
GO
