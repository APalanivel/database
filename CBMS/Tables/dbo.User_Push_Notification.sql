CREATE TABLE [dbo].[User_Push_Notification]
(
[User_Push_Notification_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[User_Info_Id] [int] NOT NULL,
[Notification_Type_Cd] [int] NOT NULL,
[Notification_Mode_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_User_Push_Notification__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[User_Push_Notification] ADD CONSTRAINT [pk_User_Push_Notification] PRIMARY KEY CLUSTERED  ([User_Push_Notification_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[User_Push_Notification] ADD CONSTRAINT [un_User_Push_Notification__User_Info_Id__Notification_Type_Cd__Notification_Mode_Cd] UNIQUE NONCLUSTERED  ([User_Info_Id], [Notification_Type_Cd], [Notification_Mode_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[User_Push_Notification] ADD CONSTRAINT [fk_USER_INFO__User_Email_Subscription_Report] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO
