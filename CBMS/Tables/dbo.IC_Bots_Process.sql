CREATE TABLE [dbo].[IC_Bots_Process]
(
[IC_Bots_Process_Id] [int] NOT NULL IDENTITY(1, 1),
[IC_Bots_Process_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IC_Bots_Process_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [Df_IC_Bots_Process__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [Df_IC_Bots_Processg__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IC_Bots_Process] ADD CONSTRAINT [pk_IC_Bots_Process__IC_Bots_Process_Id] PRIMARY KEY CLUSTERED  ([IC_Bots_Process_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IC_Bots_Process] ADD CONSTRAINT [unc_IC_Bots_Process_IC_Bots_Process_Name] UNIQUE NONCLUSTERED  ([IC_Bots_Process_Name]) ON [DB_DATA01]
GO
