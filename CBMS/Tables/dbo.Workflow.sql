CREATE TABLE [dbo].[Workflow]
(
[Workflow_Id] [int] NOT NULL IDENTITY(1, 1),
[Module_Cd] [int] NOT NULL,
[Workflow_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Active] [bit] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFWorkflowCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFWorkflowLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Workflow] ADD CONSTRAINT [pk_Workflow] PRIMARY KEY CLUSTERED  ([Workflow_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Workflow] ADD CONSTRAINT [unq_Workflow] UNIQUE NONCLUSTERED  ([Module_Cd], [Workflow_Name]) ON [DB_DATA01]
GO
