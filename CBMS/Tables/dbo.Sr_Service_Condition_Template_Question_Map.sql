CREATE TABLE [dbo].[Sr_Service_Condition_Template_Question_Map]
(
[Sr_Service_Condition_Template_Question_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Service_Condition_Template_Category_Map_Id] [int] NOT NULL,
[Sr_Service_Condition_Question_Id] [int] NOT NULL,
[Display_Seq] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template_Question_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template_Question_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Question_Map] ADD CONSTRAINT [pk_Sr_Service_Condition_Template_Question_Map] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Template_Question_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Question_Map] ADD CONSTRAINT [un_Sr_Service_Condition_Template_Question_Map__Sr_Service_Condition_Template_Category_Map_Id__Display_Seq] UNIQUE NONCLUSTERED  ([Sr_Service_Condition_Template_Category_Map_Id], [Display_Seq]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Question_Map] ADD CONSTRAINT [un_Sr_Service_Condition_Template_Question_Map__Sr_Service_Condition_Template_Category_Map_Id__Sr_Service_Condition_Question_Id] UNIQUE NONCLUSTERED  ([Sr_Service_Condition_Template_Category_Map_Id], [Sr_Service_Condition_Question_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Question_Map] ADD CONSTRAINT [fk_Sr_Service_Condition_Question__Sr_Service_Condition_Template_Question_Map] FOREIGN KEY ([Sr_Service_Condition_Question_Id]) REFERENCES [dbo].[Sr_Service_Condition_Question] ([Sr_Service_Condition_Question_Id])
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Question_Map] ADD CONSTRAINT [fk_Sr_Service_Condition_Template_Category_Map__Sr_Service_Condition_Template_Question_Map] FOREIGN KEY ([Sr_Service_Condition_Template_Category_Map_Id]) REFERENCES [dbo].[Sr_Service_Condition_Template_Category_Map] ([Sr_Service_Condition_Template_Category_Map_Id])
GO
