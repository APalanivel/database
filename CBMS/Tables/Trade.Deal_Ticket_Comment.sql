CREATE TABLE [Trade].[Deal_Ticket_Comment]
(
[Deal_Ticket_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Comment__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Comment] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Comment] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
