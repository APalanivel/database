CREATE TABLE [Trade].[RM_Forecast_Volume_Batch_Dtl]
(
[RM_Forecast_Volume_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Forecast_Volume_Batch_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Forecast_Service_Month] [date] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Error_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Forecast_Volume_Batch_DtlCreated_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Forecast_Volume_Batch_DtlLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Forecast_Volume_Batch_Dtl] ADD CONSTRAINT [pk_RM_Forecast_Volume_Batch_Dtl] PRIMARY KEY CLUSTERED  ([RM_Forecast_Volume_Batch_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Forecast_Volume_Batch_Dtl] ADD CONSTRAINT [un_RM_Forecast_Volume_Batch_Dtl__RM_Forecast_Volume_Batch_Id__Client_Hier_Id__Forecast_Service_Month] UNIQUE NONCLUSTERED  ([RM_Forecast_Volume_Batch_Id], [Client_Hier_Id], [Forecast_Service_Month]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Forecast_Volume_Batch_Dtl] ADD CONSTRAINT [fk_RM_Forecast_Volume_Batch__RM_Forecast_Volume_Batch_Dtl] FOREIGN KEY ([RM_Forecast_Volume_Batch_Id]) REFERENCES [Trade].[RM_Forecast_Volume_Batch] ([RM_Forecast_Volume_Batch_Id])
GO
