CREATE TABLE [dbo].[Client_Group_Info_Map]
(
[Client_Id] [int] NOT NULL,
[Group_Info_Id] [int] NOT NULL,
[Assigned_User_Id] [int] NOT NULL,
[Assigned_Ts] [datetime] NOT NULL CONSTRAINT [DF__Client_Gr__Assig__084EFBCF] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junctio table between Client and Group_Info.  Used in menus', 'SCHEMA', N'dbo', 'TABLE', N'Client_Group_Info_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Group_Info_Map] ADD CONSTRAINT [pk_Client_Group_Info_Map] PRIMARY KEY NONCLUSTERED  ([Client_Id], [Group_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Group_Info_Map__Group_Info_Id] ON [dbo].[Client_Group_Info_Map] ([Group_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Group_Info_Map] ADD CONSTRAINT [fk_Client__Client_Group_Info_Map] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[Client_Group_Info_Map] ADD CONSTRAINT [fk_Group_Info__Client_Group_Info_Map] FOREIGN KEY ([Group_Info_Id]) REFERENCES [dbo].[GROUP_INFO] ([GROUP_INFO_ID])
GO
