CREATE TABLE [dbo].[StoredProc_Error_Log]
(
[StoredProc_Error_Log_ID] [int] NOT NULL IDENTITY(1, 1),
[StoredProc_Name] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error_Line] [int] NULL,
[Error_message] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Input_Params] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error_Time_Stamp] [datetime] NULL CONSTRAINT [DF__StoredPro__Error__6D631D95] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[StoredProc_Error_Log] ADD CONSTRAINT [PK__StoredPr__7D3012E96B7AD523] PRIMARY KEY CLUSTERED  ([StoredProc_Error_Log_ID]) ON [DB_DATA01]
GO
