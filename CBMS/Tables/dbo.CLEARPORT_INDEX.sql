CREATE TABLE [dbo].[CLEARPORT_INDEX]
(
[CLEARPORT_INDEX_ID] [int] NOT NULL IDENTITY(1, 1),
[CLEARPORT_INDEX] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLEARPORT_INDEX_SYMBOL] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Mark McCallon
--11-12-07
--Update market_price_point
--when adding to this table
create trigger [dbo].[trg_ins_Clearport_Index]
on [dbo].[CLEARPORT_INDEX]
for insert
as
begin
set nocount on


	insert into market_price_point (market_price_index_id,market_price_point,interval_type,market_price_point_short_name)
	select 1,clearport_index,'m',left(clearport_index,50) from inserted where clearport_index_id not in(236,237,238)

end



GO
ALTER TABLE [dbo].[CLEARPORT_INDEX] ADD CONSTRAINT [PK_CLEARPORT_INDEX] PRIMARY KEY CLUSTERED  ([CLEARPORT_INDEX_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Clearport Pricing Index names and symbols', 'SCHEMA', N'dbo', 'TABLE', N'CLEARPORT_INDEX', NULL, NULL
GO
