CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity]
(
[Cu_Invoice_Account_Commodity_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity] ADD CONSTRAINT [pk_Cu_Invoice_Account_Commodity] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Account_Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity] ADD CONSTRAINT [uix_Cu_Invoice_Account_Commodity__Cu_Invoice_Id__Account_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([Cu_Invoice_Id], [Account_Id], [Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity] ADD CONSTRAINT [fk_CU_INVOICE__Cu_Invoice_Account_Commodity] FOREIGN KEY ([Cu_Invoice_Id]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
