CREATE TABLE [dbo].[Utility_Dtl_Transport]
(
[Utility_Dtl_Transport_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Industrial_Flag_Cd] [int] NOT NULL,
[Commercial_Flag_Cd] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'captures the ‘Transport Information’ section data for EP and NG', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Transport', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Transport] ADD CONSTRAINT [unc_Utility_Dtl_Transport__Utility_Question_Commodity_Map_ID] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Transport] ADD CONSTRAINT [pk_Utility_Dtl_Transport] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Transport_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Transport__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Transport] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Transport__Comment_ID] ON [dbo].[Utility_Dtl_Transport] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Transport] ADD
CONSTRAINT [fk_Utility_Dtl_Transport__Question_Commodity_Map] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Transport] ADD
CONSTRAINT [fk_Utility_Dtl_Transport__VENDOR_COMMODITY_MAP] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Transport] ADD
CONSTRAINT [fk_Comment__Utility_Dtl_Transport] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
