CREATE TABLE [dbo].[SR_SUPPLIER_PROFILE_DOCUMENTS]
(
[SR_SUPPLIER_PROFILE_DOCUMENTS_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_SUPPLIER_PROFILE_ID] [int] NOT NULL,
[CBMS_IMAGE_ID] [int] NOT NULL,
[UPLOADED_BY] [int] NOT NULL,
[UPLOADED_DATE] [datetime] NOT NULL,
[REVIEW_DATE] [datetime] NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'The table to store the links to uploaded documents from Supplier Profile section', 'SCHEMA', N'dbo', 'TABLE', N'SR_SUPPLIER_PROFILE_DOCUMENTS', NULL, NULL
GO

ALTER TABLE [dbo].[SR_SUPPLIER_PROFILE_DOCUMENTS] ADD 
CONSTRAINT [PK__SR_SUPPLIER_PROF__359ECAF1] PRIMARY KEY CLUSTERED  ([SR_SUPPLIER_PROFILE_DOCUMENTS_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
