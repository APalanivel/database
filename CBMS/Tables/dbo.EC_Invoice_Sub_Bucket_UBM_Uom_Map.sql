CREATE TABLE [dbo].[EC_Invoice_Sub_Bucket_UBM_Uom_Map]
(
[EC_Invoice_Sub_Bucket_Master_Id] [int] NOT NULL,
[Ubm_Id] [int] NOT NULL,
[Uom_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Invoice_Sub_Bucket_UBM_Uom_Map] ADD CONSTRAINT [pk_EC_Invoice_Sub_Bucket_UBM_Uom_Map] PRIMARY KEY CLUSTERED  ([EC_Invoice_Sub_Bucket_Master_Id], [Ubm_Id], [Uom_Id]) ON [DB_DATA01]
GO
