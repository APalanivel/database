CREATE TABLE [dbo].[RM_MARKET_DATA_VALUE]
(
[RM_MARKET_DATA_ID] [smallint] NOT NULL,
[rm_market_data_date] [smalldatetime] NOT NULL,
[volume] [decimal] (9, 2) NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management market data - last date data received 9/9/2011', 'SCHEMA', N'dbo', 'TABLE', N'RM_MARKET_DATA_VALUE', NULL, NULL
GO

ALTER TABLE [dbo].[RM_MARKET_DATA_VALUE] ADD 
CONSTRAINT [PK_market_data_value] PRIMARY KEY CLUSTERED  ([RM_MARKET_DATA_ID], [rm_market_data_date]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
