CREATE TABLE [dbo].[CLIENTCEM]
(
[NMKL1] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NRKLX] [decimal] (6, 0) NOT NULL,
[NRHFD] [decimal] (6, 0) NOT NULL,
[NMUSR] [char] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'CLIENTCEM', NULL, NULL
GO
