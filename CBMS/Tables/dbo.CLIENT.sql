CREATE TABLE [dbo].[CLIENT]
(
[CLIENT_ID] [int] NOT NULL IDENTITY(10000, 1) NOT FOR REPLICATION,
[CLIENT_TYPE_ID] [int] NOT NULL,
[UBM_SERVICE_ID] [int] NULL,
[REPORT_FREQUENCY_TYPE_ID] [int] NOT NULL,
[CLIENT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FISCALYEAR_STARTMONTH_TYPE_ID] [int] NOT NULL,
[UBM_START_DATE] [datetime] NULL,
[DSM_STRATEGY] [bit] NULL,
[IS_SEP_ISSUED] [bit] NULL,
[SEP_ISSUE_DATE] [datetime] NULL,
[IS_HISTORY] [bit] NULL,
[CURRENCY_GROUP_ID] [int] NULL,
[NOT_MANAGED] [bit] NOT NULL CONSTRAINT [DF_CLIENT_NOT_MANAGED] DEFAULT ((0)),
[NOT_MANAGED_BY_ID] [int] NULL,
[NOT_MANAGED_DATE] [datetime] NULL,
[RISK_PROFILE_TYPE_ID] [int] NOT NULL CONSTRAINT [DF__CLIENT__RISK_PRO__073B7592] DEFAULT ((0)),
[Row_Version] [timestamp] NULL,
[User_Passcode_Expiration_Duration] [smallint] NULL CONSTRAINT [DF_Client_User_Passcode_Expiration_Duration] DEFAULT ((90)),
[User_Max_Failed_Login_Attempts] [smallint] NULL CONSTRAINT [DF_Client_User_Max_Failed_Login_Attempts] DEFAULT ((5)),
[User_PassCode_Reuse_Limit] [smallint] NULL CONSTRAINT [DF_Client_User_PassCode_Reuse_Limit] DEFAULT ((1)),
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_4D735309_9F31_4DCA_AAD3_C3613F5EF8D1_1796005975] DEFAULT (newid()),
[Portfolio_Client_Id] [int] NULL,
[Analyst_Mapping_Cd] [int] NOT NULL CONSTRAINT [DF_Client__Analyst_Mapping_Cd] DEFAULT ((0)),
[Is_Single_Signon_Login] [bit] NOT NULL CONSTRAINT [df__Client__Is_Single_SignOn_Login] DEFAULT ((0)),
[Variance_Exception_Notice_Contact_Email] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Host_Url] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_CoreClient] TEXTIMAGE_ON [DBData_CoreClient]
ALTER TABLE [dbo].[CLIENT] ADD
CONSTRAINT [ENTITY_CLIENT_FK_1] FOREIGN KEY ([CLIENT_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[CLIENT] ADD
CONSTRAINT [ENTITY_CLIENT_FK_3] FOREIGN KEY ([FISCALYEAR_STARTMONTH_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[CLIENT] ADD
CONSTRAINT [ENTITY_CLIENT_FK_2] FOREIGN KEY ([REPORT_FREQUENCY_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Client name and highest level client information', 'SCHEMA', N'dbo', 'TABLE', N'CLIENT', NULL, NULL
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Client_del_Client_Hier
	
DESCRIPTION:   
	This will delete the record from Core.Clinet_Hier table when ever the enetery get deleted from Client table.
	
 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		INSERT INTO CLIENT(CLIENT_TYPE_ID,REPORT_FREQUENCY_TYPE_ID,CLIENT_NAME,FISCALYEAR_STARTMONTH_TYPE_ID,NOT_MANAGED,RISK_PROFILE_TYPE_ID,CURRENCY_GROUP_ID)
		VALUES(1,1,'TEST-CL',1,1,1,1)
		
		SELECT * FROM CLIENT WHERE CLIENT_NAME = 'TEST-CL'
		SELECT * FROM CORE.CLIENT_HIER WHERE CLIENT_NAME = 'TEST-CL'
		
		DELETE FROM CLIENT WHERE CLIENT_NAME = 'TEST-CL'
		
		SELECT * FROM CLIENT WHERE CLIENT_NAME = 'TEST-CL'
		SELECT * FROM CORE.CLIENT_HIER WHERE CLIENT_NAME = 'TEST-CL'
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created

******/
CREATE TRIGGER [dbo].[tr_Client_del_Client_Hier] ON [dbo].[CLIENT]
      FOR DELETE
AS
BEGIN
      SET NOCOUNT ON ;
      DELETE
            Core.Client_Hier
      FROM
            Core.Client_Hier ch
            INNER JOIN DELETED d ON ch.Client_Id = d.Client_Id
END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      
    
NAME:      
 tr_Client_ins_Client_Hier    
     
DESCRIPTION:       
 This will insert the new record into Core.Clinet_Hier table when ever the new clint added into the Client table.    
 Corporate level Hier_level_Cd will be used of Client_Hier table    
     
 USAGE EXAMPLES:    
------------------------------------------------------------    
     
 BEGIN TRAN    
     
  INSERT INTO CLIENT(CLIENT_TYPE_ID,REPORT_FREQUENCY_TYPE_ID,CLIENT_NAME,FISCALYEAR_STARTMONTH_TYPE_ID,NOT_MANAGED,RISK_PROFILE_TYPE_ID,CURRENCY_GROUP_ID)    
  VALUES(1,1,'TEST-CL',1,1,1,1,45,10,10)    
      
  SELECT * FROM CORE.CLIENT_HIER WHERE CLIENT_NAME = 'TEST-CL'    
     
 ROLLBACK TRAN    
     
AUTHOR INITIALS:    
 Initials Name    
------------------------------------------------------------    
 SKA  Shobhit Kumar Agrawal    
 PNR  Pandarinath    
 HG   Harihara Suthan G    
 AP   Athmaram Pabbathi
 AKR        Ashok Kumar Raju
     
 MODIFICATIONS:    
 Initials	Date		Modification    
------------------------------------------------------------    
 SKA		08/02/2010	Created    
 PNR		10/14/2010	Added client.User_Passcode_Expiration_Duration,client.User_Max_Failed_Login_Attempts    
						,User_PassCode_Reuse_Limit in select list for DV_SV_Password Expiration enhancement.    
 HG			01/13/2011	Logic added to update client_fiscal_offset column if any change in it  
 HG			10/19/2011	MAINT-868 , fixed the code to populate the followings columns  
					   Client_Type_Id  
					   Report_Frequency_Type_Id  
					   Fiscalyear_Startmonth_Type_Id  
					   Ubm_Service_Id  
					   Dsm_Strategy  
					   Is_Sep_Issued  
					   Sep_Issue_Date  
 AP	     03/08/2012		Included Portfolio_Client_Id in the INSERT list; as a part of Blackstone project
 AKR        2012-09-18  Added Client_Analyst_Mapping_Cd to the Core.Client_hier table for insert, trigger
 KVK		10/15/2014	Added New column Client_Host_Url
******/    
    
CREATE TRIGGER [dbo].[tr_Client_ins_Client_Hier] ON [dbo].[CLIENT]
      FOR INSERT
AS
BEGIN    
        
      SET NOCOUNT ON;    
     
      DECLARE @HierCd INT    
            
      SELECT
            @HierCd = Code_Id
      FROM
            dbo.Codeset cs
            JOIN dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Corporate'    
               
      INSERT      Core.Client_Hier
                  (Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_Managed
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Client_Analyst_Mapping_Cd 
				  ,Client_Host_Url
				  )
                  SELECT
                        @HierCd
                       ,i.Client_Id
                       ,i.Client_Name
                       ,i.Currency_Group_Id
                       ,i.Not_Managed
                       ,i.User_Passcode_Expiration_Duration
                       ,i.User_Max_Failed_Login_Attempts
                       ,i.User_PassCode_Reuse_Limit
                       ,CASE fsm.ENTITY_NAME
                          WHEN 'January' THEN 0
                          WHEN 'February' THEN -11
                          WHEN 'March' THEN -10
                          WHEN 'April' THEN -9
                          WHEN 'May' THEN -8
                          WHEN 'June' THEN -7
                          WHEN 'July' THEN -6
                          WHEN 'August' THEN -5
                          WHEN 'September' THEN -4
                          WHEN 'October' THEN -3
                          WHEN 'November' THEN -2
                          WHEN 'December' THEN -1
                        END
                       ,i.Client_Type_Id
                       ,i.Report_Frequency_Type_Id
                       ,i.Fiscalyear_Startmonth_Type_Id
                       ,i.Ubm_Service_Id
                       ,i.Dsm_Strategy
                       ,i.Is_Sep_Issued
                       ,i.Sep_Issue_Date
                       ,i.Portfolio_Client_Id
                       ,i.Analyst_Mapping_Cd
					   ,i.Client_Host_Url
                  FROM
                        INSERTED i
                        JOIN dbo.Entity fsm
                              ON fsm.Entity_id = i.FiscalYear_StartMonth_Type_id    
    
END 
;
GO
GO


GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******      

    

NAME:      

 tr_Client_upd_Client_Hier    

     

DESCRIPTION:       

 This will update the existing record of Core.Clinet_Hier table when ever the updates happen in Client table.    

    

USAGE EXAMPLES:    

------------------------------------------------------------    

     

 BEGIN TRAN     

     

  INSERT INTO CLIENT(CLIENT_TYPE_ID,REPORT_FREQUENCY_TYPE_ID,CLIENT_NAME,FISCALYEAR_STARTMONTH_TYPE_ID,NOT_MANAGED,RISK_PROFILE_TYPE_ID,CURRENCY_GROUP_ID)    

  VALUES(1,1,'TEST-CL',1,1,1,45)    

      

  SELECT CURRENCY_GROUP_ID FROM CLIENT WHERE CLIENT_NAME =  'TEST-CL'    

  SELECT Client_Currency_Group_Id FROM CORE.CLIENT_HIER WHERE CLIENT_NAME = 'TEST-CL'    

      

  UPDATE CLIENT     

  SET CURRENCY_GROUP_ID = 2    

  WHERE CLIENT_NAME = 'TEST-CL'    

      

  SELECT CURRENCY_GROUP_ID FROM CLIENT WHERE CLIENT_NAME =  'TEST-CL'    

  SELECT Client_Currency_Group_Id FROM CORE.CLIENT_HIER WHERE CLIENT_NAME = 'TEST-CL'    

      

 ROLLBACK TRAN    

     

AUTHOR INITIALS:    

 Initials Name    

------------------------------------------------------------    

 SKA  Shobhit Kumar Agrawal    

 PNR  Pandarinath    

 HG   Harihara Suthan G    

 AP	 Athmaram Pabbathi

 BCH  Balaraju Chalumuri

 KVK  VInay K

  

 MODIFICATIONS:    

 Initials	Date		Modification    

------------------------------------------------------------    

 SKA		08/02/2010	Created    

			08/31/2010	Added the Last_Change_TS column for update

 PNR		10/14/2010	Added client.User_Passcode_Expiration_Duration,client.User_Max_Failed_Login_Attempts    

						,User_PassCode_Reuse_Limit in Update list ( Project Name : DV_SV_Password Expiration )    

 HG			01/13/2011	Logic added to update client_fiscal_offset column if any change in it  

 HG			10/19/2011	MAINT-868 , fixed the code to populate the followings columns

						   Client_Type_Id  

						   Report_Frequency_Type_Id  

						   Fiscalyear_Startmonth_Type_Id  

						   Ubm_Service_Id  

						   Dsm_Strategy  

						   Is_Sep_Issued  

						   Sep_Issue_Date  

 AP	     03/08/2012	Included Portfolio_Client_Id in the UPDATE LIST; as a part of Blackstone project

 AKR        2012-09-18  Added update for Client_Analyst_Mapping_Cd column in Core.client_hier table

 BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.

 KVK		06/26/2014	Modified to send Portfolio map message if any change in Portfolio_CLient_Id column
 KVK		10/15/2014	Added Column Client_Host_Url
******/    
CREATE TRIGGER [dbo].[tr_Client_upd_Client_Hier] ON [dbo].[CLIENT]
      FOR UPDATE
AS
BEGIN    

      SET NOCOUNT ON;     



      UPDATE
            ch
      SET
            ch.Client_Name = i.Client_Name
           ,ch.Client_Not_managed = i.Not_managed
           ,ch.Client_Currency_Group_Id = i.CURRENCY_GROUP_ID
           ,ch.Last_Change_TS = getdate()
           ,ch.User_Passcode_Expiration_Duration = i.User_Passcode_Expiration_Duration
           ,ch.User_Max_Failed_Login_Attempts = i.User_Max_Failed_Login_Attempts
           ,ch.User_PassCode_Reuse_Limit = i.User_PassCode_Reuse_Limit
           ,ch.Client_Fiscal_Offset = CASE fsm.ENTITY_NAME
                                        WHEN 'January' THEN 0
                                        WHEN 'February' THEN -11
                                        WHEN 'March' THEN -10
                                        WHEN 'April' THEN -9
                                        WHEN 'May' THEN -8
                                        WHEN 'June' THEN -7
                                        WHEN 'July' THEN -6
                                        WHEN 'August' THEN -5
                                        WHEN 'September' THEN -4
                                        WHEN 'October' THEN -3
                                        WHEN 'November' THEN -2
                                        WHEN 'December' THEN -1
                                      END
           ,ch.Client_Type_Id = i.Client_Type_Id
           ,ch.Report_Frequency_Type_Id = i.Report_Frequency_Type_Id
           ,ch.Fiscalyear_Startmonth_Type_Id = i.Fiscalyear_Startmonth_Type_Id
           ,ch.Ubm_Service_Id = i.Ubm_Service_Id
           ,ch.DSM_STRATEGY = i.DSM_STRATEGY
           ,ch.Is_Sep_Issued = i.Is_Sep_Issued
           ,ch.Sep_Issue_Date = i.Sep_Issue_Date
           ,ch.Portfolio_Client_Id = i.Portfolio_Client_Id
           ,ch.Client_Analyst_Mapping_Cd = i.Analyst_Mapping_Cd
           ,ch.Client_Host_Url = i.Client_Host_Url
      FROM
            Core.Client_Hier ch
            INNER JOIN INSERTED i
                  ON i.Client_Id = ch.Client_Id
            INNER JOIN DELETED d
                  ON i.Client_id = d.Client_Id
            INNER JOIN dbo.Entity fsm
                  ON fsm.Entity_id = i.FiscalYear_StartMonth_Type_id
      WHERE
            hashbytes('SHA1', ( SELECT
                                    i.Client_Name
                                   ,i.Not_Managed
                                   ,i.CURRENCY_GROUP_ID
                                   ,i.User_PassCode_Expiration_Duration
                                   ,i.User_Max_Failed_Login_Attempts
                                   ,i.User_PassCode_Reuse_Limit
                                   ,i.FiscalYear_StartMonth_Type_id
                                   ,i.Client_Type_Id
                                   ,i.Report_Frequency_Type_Id
                                   ,i.Ubm_Service_Id
                                   ,i.DSM_STRATEGY
                                   ,i.Is_Sep_Issued
                                   ,i.Sep_Issue_Date
                                   ,i.Portfolio_Client_Id
                                   ,i.Analyst_Mapping_Cd
                                   ,i.Client_Host_Url
                      FOR
                                XML RAW )) != hashbytes('SHA1', ( SELECT
                                                                        d.Client_Name
                                                                       ,d.Not_Managed
                                                                       ,d.CURRENCY_GROUP_ID
                                                                       ,d.User_PassCode_Expiration_Duration
                                                                       ,d.User_Max_Failed_Login_Attempts
                                                                       ,d.User_PassCode_Reuse_Limit
                                                                       ,d.FiscalYear_StartMonth_Type_id
                                                                       ,d.Client_Type_Id
                                                                       ,d.Report_Frequency_Type_Id
                                                                       ,d.Ubm_Service_Id
                                                                       ,d.DSM_STRATEGY
                                                                       ,d.Is_Sep_Issued
                                                                       ,d.Sep_Issue_Date
                                                                       ,d.Portfolio_Client_Id
                                                                       ,d.Analyst_Mapping_Cd
                                                                       ,d.Client_Host_Url
                                                        FOR
                                                                  XML RAW ))    

                 



      DECLARE @Old_Portfolio_Client_Id INT
      DECLARE @New_Portfolio_Client_Id INT
      DECLARE @Client_Id INT
      DECLARE @Conversation_Handle UNIQUEIDENTIFIER; 

      IF update(Portfolio_Client_Id)
            BEGIN

                  SELECT
                        @Old_Portfolio_Client_Id = d.Portfolio_CLient_Id
                       ,@New_Portfolio_Client_Id = i.Portfolio_CLient_Id
                       ,@Client_Id = i.Client_Id
                  FROM
                        INSERTED i
                        JOIN DELETED d
                              ON i.Client_ID = d.CLIENT_ID
                              
                  IF EXISTS ( SELECT
                                    us.Segments
                              FROM
                                    dbo.App_Config AS ac
                                    CROSS APPLY dbo.ufn_split(ac.App_Config_Value, ',') AS us
                              WHERE
                                    ac.App_Config_Cd = 'Portfolio_ClientHier_Management'
                                    AND ( Segments = @Old_Portfolio_CLient_Id
                                          OR us.Segments = @New_Portfolio_CLient_Id ) )
                        BEGIN
                              DECLARE @Message XML 

                              IF @Old_Portfolio_CLient_Id IS NULL
                                    AND @New_Portfolio_CLient_Id IS NOT NULL -- then its a new Mapp
                                    BEGIN
                                          SET @Message = ( SELECT
                                                            @Client_Id Client_Id
                                                           ,@New_Portfolio_Client_Id Portfolio_Client_Id
                                                FOR
                                                           XML PATH('Client')
                                                              ,ELEMENTS
                                                              ,ROOT('Mapping_Client_To_Portfolio') )
                                                              
                                          IF @Message IS NOT NULL
                                                BEGIN

                                                      BEGIN DIALOG CONVERSATION @Conversation_Handle
														FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]
														TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
														ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];

                                                      SEND ON CONVERSATION @Conversation_Handle
														MESSAGE TYPE [//Change_Control/Message/Mapping_Client_To_Portfolio] (@Message) 
                                                END
                                    END
                              ELSE
                                    IF @Old_Portfolio_CLient_Id IS NOT NULL
                                          AND @New_Portfolio_CLient_Id IS NULL -- then its a un-Map
                                          BEGIN
                                                SET @Message = ( SELECT
                                                                  @Client_Id Client_Id
                                                                 ,@Old_Portfolio_Client_Id Portfolio_Client_Id
                                                      FOR
                                                                 XML PATH('Client')
                                                                    ,ELEMENTS
                                                                    ,ROOT('UnMapping_Client_From_Portfolio') )

                                                IF @Message IS NOT NULL
                                                      BEGIN
                                                            BEGIN DIALOG CONVERSATION @Conversation_Handle
															FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]
															TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
															ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Client_To_Portfolio];    

                                                            SEND ON CONVERSATION @Conversation_Handle
															MESSAGE TYPE [//Change_Control/Message/UnMapping_Client_From_Portfolio] (@Message) 																			

                                                      END 
                                          END
                        END 
            END
END
;
GO





ALTER TABLE [dbo].[CLIENT] ADD
CONSTRAINT [FK_RISK_PROFILE_TYPE_ID] FOREIGN KEY ([RISK_PROFILE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[CLIENT] ADD 
CONSTRAINT [CLIENT_PK] PRIMARY KEY CLUSTERED  ([CLIENT_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [CLIENT_N_4] ON [dbo].[CLIENT] ([CLIENT_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [CLIENT_N_3] ON [dbo].[CLIENT] ([UBM_SERVICE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [CLIENT_N_1] ON [dbo].[CLIENT] ([REPORT_FREQUENCY_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE UNIQUE NONCLUSTERED INDEX [CLIENT_U_1] ON [dbo].[CLIENT] ([CLIENT_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [CLIENT_N_2] ON [dbo].[CLIENT] ([FISCALYEAR_STARTMONTH_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_CLIENT_Row_Version] ON [dbo].[CLIENT] ([Row_Version]) ON [DB_INDEXES01]

