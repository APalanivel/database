CREATE TABLE [dbo].[Notification_Msg_Queue]
(
[Notification_Msg_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Notification_Template_Id] [int] NOT NULL,
[Requested_User_Id] [int] NOT NULL,
[Data_Upload_Request_Type_Id] [int] NULL,
[Email_Subject] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Body] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Notification_Msg_Queue__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Email subject and body for system notifications, and requestor user ID', 'SCHEMA', N'dbo', 'TABLE', N'Notification_Msg_Queue', NULL, NULL
GO

ALTER TABLE [dbo].[Notification_Msg_Queue] ADD CONSTRAINT [pk_Notification_Msg_Queue] PRIMARY KEY CLUSTERED  ([Notification_Msg_Queue_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Notification_Msg_Queue__Notification_Template_Id] ON [dbo].[Notification_Msg_Queue] ([Notification_Template_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Notification_Msg_Queue] ADD CONSTRAINT [fk_Notification_Template__Notification_Msg_Queue] FOREIGN KEY ([Notification_Template_Id]) REFERENCES [dbo].[Notification_Template] ([Notification_Template_Id])
GO
