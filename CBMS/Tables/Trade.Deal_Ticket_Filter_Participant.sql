CREATE TABLE [Trade].[Deal_Ticket_Filter_Participant]
(
[Deal_Ticket_Filter_Participant_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Participant_Id] [int] NOT NULL,
[Deal_Participant_Type_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Filter_Participant__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Filter_Participant] ADD CONSTRAINT [pk_Deal_Ticket_Filter_Participant] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Filter_Participant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Filter_Participant] ADD CONSTRAINT [un_Deal_Ticket_Filter_Participant__Deal_Ticket_Id__Participant_Id__Deal_Participant_Type_Cd] UNIQUE NONCLUSTERED  ([Deal_Ticket_Id], [Participant_Id], [Deal_Participant_Type_Cd]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Filter_Participant__Participant_Id__Deal_Participant_Type_Cd] ON [Trade].[Deal_Ticket_Filter_Participant] ([Participant_Id], [Deal_Participant_Type_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Filter_Participant] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Filter_Participant] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
