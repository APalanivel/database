CREATE TABLE [Workflow].[Workflow_Queue_Output_Column]
(
[Workflow_Queue_Output_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Id] [int] NOT NULL,
[Display_Column_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Source_Column_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Visible_To_User] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Is_Visible_To_User] DEFAULT ((0)),
[Is_Unique_Reference_Column] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Is_Unique_Reference_Column] DEFAULT ((0)),
[Is_Default] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Is_Default] DEFAULT ((1)),
[is_Fixed] [bit] NOT NULL,
[HyperLink_URL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Allow_Sorting] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Allow_Sorting] DEFAULT ((1)),
[Column_Data_Type_Cd] [int] NULL,
[Header_CSS] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Row_CSS] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Output_Column__Last_Change_Ts] DEFAULT (getdate()),
[Display_Order] [int] NULL,
[Target_Server] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Link_Type_CD] [int] NULL,
[Is_Label] [bit] NULL CONSTRAINT [Is_Label_Workflow_Queue_Output_Column] DEFAULT ((0)),
[Data_Format] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Grid_Label] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Output_Column] ADD CONSTRAINT [pk_Workflow_Queue_Output_Column] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Output_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Output_Column] ADD CONSTRAINT [un_Workflow_Queue_Output_Column__Workflow_Queue_Id__Display_Column_Name] UNIQUE NONCLUSTERED  ([Workflow_Queue_Id], [Display_Column_Name]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Output_Column] ADD CONSTRAINT [fk_Workflow_Queue__Workflow_Queue_Output_Column] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
