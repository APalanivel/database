CREATE TABLE [dbo].[dmlLookupType]
(
[LookupTypeId] [int] NOT NULL,
[Code] [uniqueidentifier] NOT NULL,
[LookupTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'dmlLookupType', NULL, NULL
GO
