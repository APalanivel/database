CREATE TABLE [Workflow].[Search_Filter_Cascading_Data_Source]
(
[Search_Filter_Cascading_Data_Source_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Search_Filter_Id] [int] NOT NULL,
[Parent_Workflow_Queue_Search_Filter_Id] [int] NULL,
[Parameter_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Search_Filter_Cascading_Data_Source__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Search_Filter_Cascading_Data_Source__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Search_Filter_Cascading_Data_Source] ADD CONSTRAINT [pk_Search_Filter_Cascading_Data_Source] PRIMARY KEY CLUSTERED  ([Search_Filter_Cascading_Data_Source_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Search_Filter_Cascading_Data_Source] ADD CONSTRAINT [un_Search_Filter_Cascading_Data_Source__Workflow_Queue_Search_Filter_Id__Parent_Workflow_Queue_Search_Filter_Id] UNIQUE NONCLUSTERED  ([Workflow_Queue_Search_Filter_Id], [Parent_Workflow_Queue_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Search_Filter_Cascading_Data_Source] ADD CONSTRAINT [fk_Workflow_Queue_Search_Filter__Search_Filter_Cascading_Data_Source] FOREIGN KEY ([Workflow_Queue_Search_Filter_Id]) REFERENCES [Workflow].[Workflow_Queue_Search_Filter] ([Workflow_Queue_Search_Filter_Id])
GO
ALTER TABLE [Workflow].[Search_Filter_Cascading_Data_Source] ADD CONSTRAINT [RefWorkflow_Queue_Search_Filter2749] FOREIGN KEY ([Parent_Workflow_Queue_Search_Filter_Id]) REFERENCES [Workflow].[Workflow_Queue_Search_Filter] ([Workflow_Queue_Search_Filter_Id])
GO
