CREATE TABLE [dbo].[Cost_Usage_Site_Dtl]
(
[Cost_Usage_Site_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Service_Month] [date] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Bucket_Value] [decimal] (28, 10) NOT NULL,
[UOM_Type_Id] [int] NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[Created_By_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cost_Usage_Site_DtlN_Created_Dt] DEFAULT (getdate()),
[Updated_Ts] [datetime] NOT NULL CONSTRAINT [df_Cost_Usage_Site_DtlN_Updated_Dt] DEFAULT (getdate()),
[Updated_By_Id] [int] NULL,
[Data_Source_Cd] [int] NULL,
[Client_Hier_Id] [int] NOT NULL,
[Estimated_Bucket_Value] [decimal] (28, 10) NULL,
[Actual_Bucket_Value] [decimal] (28, 10) NULL,
[Data_Type_Cd] [int] NOT NULL
) ON [DB_DATA01]
CREATE NONCLUSTERED INDEX [IX_Cost_Usage_Site_Dtl__Client_Hier_Id__CURRENCY_UNIT_ID__Service_Month] ON [dbo].[Cost_Usage_Site_Dtl] ([Client_Hier_Id], [CURRENCY_UNIT_ID], [Service_Month]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Cost_Usage_Site_Dtl] ADD 
CONSTRAINT [pk_Cost_Usage_Site_Dtl_New] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Bucket_Master_Id], [Service_Month]) WITH (FILLFACTOR=90) ON [DB_DATA01]ALTER TABLE [dbo].[Cost_Usage_Site_Dtl] ENABLE CHANGE_TRACKING
GO

CREATE NONCLUSTERED INDEX [ix_Cost_Usage_Site_Dtl__Bucket_Master_Id__Client_Hier_Id__Service_Month__Include] ON [dbo].[Cost_Usage_Site_Dtl] ([Bucket_Master_Id], [Client_Hier_Id], [Service_Month]) ON [DB_DATA01]

CREATE NONCLUSTERED INDEX [ix_Cost_Usage_Site_Dtl_Service_Month_Data_source_Cd] ON [dbo].[Cost_Usage_Site_Dtl] ([Service_Month], [Data_Source_Cd]) ON [DB_DATA01]

GO


ALTER TABLE [dbo].[Cost_Usage_Site_Dtl] ENABLE CHANGE_TRACKING
GO
