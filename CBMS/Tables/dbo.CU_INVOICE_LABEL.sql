CREATE TABLE [dbo].[CU_INVOICE_LABEL]
(
[CU_INVOICE_LABEL_ID] [int] NOT NULL IDENTITY(1, 1),
[CU_INVOICE_ID] [int] NOT NULL,
[ACCOUNT_NUMBER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMMODITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [DF__CU_INVOIC__Creat__36C7E119] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DF__CU_INVOIC__Last___37BC0552] DEFAULT (getdate())
) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[CU_INVOICE_LABEL] ADD CONSTRAINT [PK_CU_INVOICE_LABEL] PRIMARY KEY CLUSTERED  ([CU_INVOICE_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[CU_INVOICE_LABEL] ADD CONSTRAINT [FK_CU_INVOICE_LABEL_CU_INVOICE] FOREIGN KEY ([CU_INVOICE_ID]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Free text labels of account number/client name/city/state/vendor', 'SCHEMA', N'dbo', 'TABLE', N'CU_INVOICE_LABEL', NULL, NULL
GO
