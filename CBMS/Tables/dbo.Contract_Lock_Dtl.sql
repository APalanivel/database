CREATE TABLE [dbo].[Contract_Lock_Dtl]
(
[Contract_Lock_Dtl_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Contract_Id] [int] NULL,
[Start_Dt] [date] NOT NULL,
[Expiration_Dt] [date] NOT NULL,
[Nymex_Price] [decimal] (28, 10) NOT NULL,
[Percentage_Locked] [decimal] (5, 2) NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Pricing locked to a contract.  Contains lock start & end date.', 'SCHEMA', N'dbo', 'TABLE', N'Contract_Lock_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[Contract_Lock_Dtl] ADD CONSTRAINT [unc_Contract_Lock_Dtl_Contract_Id_Start_Dt_Expiration_Dt] UNIQUE CLUSTERED  ([Contract_Id], [Start_Dt], [Expiration_Dt]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Contract_Lock_Dtl] ADD CONSTRAINT [PK_Contract_Lock_Dtl] PRIMARY KEY NONCLUSTERED  ([Contract_Lock_Dtl_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

GO
