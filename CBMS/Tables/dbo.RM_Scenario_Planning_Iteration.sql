CREATE TABLE [dbo].[RM_Scenario_Planning_Iteration]
(
[RM_Scenario_Planning_Iteration_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Scenario_Report_Id] [int] NOT NULL,
[Iteration_Num] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Planning_Iteration__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Planning_Iteration__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management scenario report iterations', 'SCHEMA', N'dbo', 'TABLE', N'RM_Scenario_Planning_Iteration', NULL, NULL
GO

ALTER TABLE [dbo].[RM_Scenario_Planning_Iteration] ADD CONSTRAINT [pk_RM_Scenario_Planning_Iteration] PRIMARY KEY CLUSTERED  ([RM_Scenario_Planning_Iteration_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Scenario_Planning_Iteration] ADD CONSTRAINT [un_RM_Scenario_Planning_Iteration__RM_Scenario_Report_Id__Iteration_Num] UNIQUE NONCLUSTERED  ([RM_Scenario_Report_Id], [Iteration_Num]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Scenario_Planning_Iteration] ADD CONSTRAINT [fk_RM_Scenario_Report__RM_Scenario_Planning_Iteration] FOREIGN KEY ([RM_Scenario_Report_Id]) REFERENCES [dbo].[RM_Scenario_Report] ([RM_Scenario_Report_Id])
GO
