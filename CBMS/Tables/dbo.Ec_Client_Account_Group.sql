CREATE TABLE [dbo].[Ec_Client_Account_Group]
(
[Ec_Client_Account_Group_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Client_Id] [int] NOT NULL,
[Ec_Account_Group_Type_Id] [int] NOT NULL,
[Account_Group_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Ec_Client_Account_Group__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group] ADD CONSTRAINT [pk_Ec_Client_Account_Group] PRIMARY KEY CLUSTERED  ([Ec_Client_Account_Group_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group] ADD CONSTRAINT [uix_Ec_Client_Account_Group__Client_Id__Ec_Account_Group_Type_Id__Account_Group_Name] UNIQUE NONCLUSTERED  ([Client_Id], [Ec_Account_Group_Type_Id], [Account_Group_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group] ADD CONSTRAINT [fk_Ec_Client_Account_Group__Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[Ec_Client_Account_Group] ADD CONSTRAINT [fk_Ec_Client_Account_Group__Ec_Account_Group_Type_Id] FOREIGN KEY ([Ec_Account_Group_Type_Id]) REFERENCES [dbo].[Ec_Account_Group_Type] ([Ec_Account_Group_Type_Id])
GO
