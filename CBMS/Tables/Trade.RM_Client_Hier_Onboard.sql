CREATE TABLE [Trade].[RM_Client_Hier_Onboard]
(
[RM_Client_Hier_Onboard_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Country_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_OnboardCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_Hier_OnboardLast_Change_Ts] DEFAULT (getdate()),
[RM_Forecast_UOM_Type_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Onboard] ADD CONSTRAINT [pk_RM_Client_Hier_Onboard] PRIMARY KEY CLUSTERED  ([RM_Client_Hier_Onboard_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Client_Hier_Onboard] ADD CONSTRAINT [un_RM_Client_Hier_Onboard__Client_Hier_Id__Country_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([Client_Hier_Id], [Country_Id], [Commodity_Id]) ON [DB_DATA01]
GO
