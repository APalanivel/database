CREATE TABLE [dbo].[Market_Price_Index]
(
[Market_Price_Index_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Market_Price_Index_Name] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[rowguid] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__Market_Pr__rowgu__7C5EBFC3] DEFAULT (newid())
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Master Listing of the Market Price Index Name', 'SCHEMA', N'dbo', 'TABLE', N'Market_Price_Index', NULL, NULL
GO

ALTER TABLE [dbo].[Market_Price_Index] ADD 
CONSTRAINT [PK_MARKET_Price_Index] PRIMARY KEY CLUSTERED  ([Market_Price_Index_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
ALTER TABLE [dbo].[Market_Price_Index] ADD CONSTRAINT [no_dupe_index] UNIQUE NONCLUSTERED  ([Market_Price_Index_Name]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]

CREATE UNIQUE NONCLUSTERED INDEX [index_245861680] ON [dbo].[Market_Price_Index] ([rowguid]) ON [DB_INDEXES01]



GO
