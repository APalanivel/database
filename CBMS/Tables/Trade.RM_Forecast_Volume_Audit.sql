CREATE TABLE [Trade].[RM_Forecast_Volume_Audit]
(
[RM_Forecast_Volume_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Current_Forecast_Volume] [decimal] (28, 0) NULL,
[Previous_Forecast_Volume] [decimal] (28, 0) NULL,
[Previous_Uom_Id] [int] NULL,
[Current_Uom_Id] [int] NULL,
[Previous_Volume_Source_Cd] [int] NULL,
[Current_Volume_Source_Cd] [int] NULL,
[Previous_Data_Change_Level_Cd] [int] NULL,
[Current_Data_Change_Level_Cd] [int] NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Forecast_Volume_AuditLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Forecast_Volume_Audit] ADD CONSTRAINT [pk_RM_Forecast_Volume_Audit] PRIMARY KEY CLUSTERED  ([RM_Forecast_Volume_Audit_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_RM_Forecast_Volume_Audit__Client_Hier_Id__Account_Id__Commodity_Id__Service_Month] ON [Trade].[RM_Forecast_Volume_Audit] ([Client_Hier_Id], [Account_Id], [Commodity_Id], [Service_Month]) ON [DB_DATA01]
GO
