CREATE TABLE [dbo].[RPT_SERVICE_MONTH]
(
[session_uid] [uniqueidentifier] NOT NULL,
[service_month] [datetime] NOT NULL,
[report_year] [int] NULL,
[month_number] [int] NULL
) ON [DBData_Reports]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of Report Year to Report Service Months', 'SCHEMA', N'dbo', 'TABLE', N'RPT_SERVICE_MONTH', NULL, NULL
GO

ALTER TABLE [dbo].[RPT_SERVICE_MONTH] ADD 
CONSTRAINT [PK_RPT_SERVICE_MONTH] PRIMARY KEY CLUSTERED  ([session_uid], [service_month]) WITH (FILLFACTOR=90) ON [DBData_Reports]

GO
