CREATE TABLE [dbo].[SR_RFP_BID]
(
[SR_RFP_BID_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PRODUCT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SR_RFP_BID_REQUIREMENTS_ID] [int] NULL,
[SR_RFP_SUPPLIER_PRICE_COMMENTS_ID] [int] NULL,
[SR_RFP_SUPPLIER_SERVICE_ID] [int] NULL,
[IS_SUB_PRODUCT] [bit] NOT NULL CONSTRAINT [DF_SR_RFP_BID_IS_SUB_PRODUCT] DEFAULT ((0)),
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_CD23998F_3238_4392_BBA7_50BFD1F4F213_775074643] DEFAULT (newid())
) ON [DBData_Sourcing]
GO


EXEC sp_addextendedproperty N'MS_Description', N'This table would store a bid component of a RFP. It is made up of the sub-components like product name, bid requirements, supplier price comments and supplier service conditions', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_BID', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_BID] ADD 
CONSTRAINT [PK__SR_RFP_BID__06E3DC08] PRIMARY KEY CLUSTERED  ([SR_RFP_BID_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
