CREATE TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl]
(
[Cu_Invoice_Workflow_Action_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Workflow_Action_Batch_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Cu_Invoic__Creat__2E329B18] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Cu_Invoic__Last___2F26BF51] DEFAULT (getdate()),
[Error_Msg] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl] ADD CONSTRAINT [pk_Cu_Invoice_Workflow_Action_Batch_Dtl] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Workflow_Action_Batch_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl] ADD CONSTRAINT [u] UNIQUE NONCLUSTERED  ([Cu_Invoice_Workflow_Action_Batch_Id], [Cu_Invoice_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Cu_Invoice_Workflow_Action_Batch_Dtl] ADD CONSTRAINT [fk_Cu_Invoice_Workflow_Action_Batch__Cu_Invoice_Workflow_Action_Batch_Dtl] FOREIGN KEY ([Cu_Invoice_Workflow_Action_Batch_Id]) REFERENCES [Workflow].[Cu_Invoice_Workflow_Action_Batch] ([Cu_Invoice_Workflow_Action_Batch_Id])
GO
