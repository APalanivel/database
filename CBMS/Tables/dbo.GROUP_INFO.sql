CREATE TABLE [dbo].[GROUP_INFO]
(
[GROUP_INFO_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[QUEUE_ID] [int] NULL,
[GROUP_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GROUP_DESCRIPTION] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROUP_EMAIL_ADDRESS] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Group_Info_Category_Id] [int] NULL,
[Is_Fee_Module] [bit] NOT NULL CONSTRAINT [DF__GROUP_INFO__Is_Fee_Module] DEFAULT ((0)),
[Is_Required_For_RA_Login] [bit] NOT NULL CONSTRAINT [df__GROUP_INFO__Is_Required_For_RA_Login] DEFAULT ((0)),
[Is_Shown_On_Watch_List] [bit] NOT NULL CONSTRAINT [df_Group_Info_Is_Shown_On_Watch_List] DEFAULT ((0)),
[Is_Shown_On_VEN_CC_List] [bit] NOT NULL CONSTRAINT [df_Group_Info_Is_Shown_On_VEN_CC_List] DEFAULT ((0)),
[Group_Legacy_Name_Cd] [int] NULL
) ON [DBData_UserInfo]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Security groups used to grant access to features in the applications', 'SCHEMA', N'dbo', 'TABLE', N'GROUP_INFO', NULL, NULL
GO

ALTER TABLE [dbo].[GROUP_INFO] WITH NOCHECK ADD
CONSTRAINT [QUEUE_GROUP_INFO_FK] FOREIGN KEY ([QUEUE_ID]) REFERENCES [dbo].[QUEUE] ([QUEUE_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[GROUP_INFO] ADD 
CONSTRAINT [GROUP_INFO_PK] PRIMARY KEY CLUSTERED  ([GROUP_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
CREATE NONCLUSTERED INDEX [GROUP_INFO_N_1] ON [dbo].[GROUP_INFO] ([QUEUE_ID], [GROUP_NAME]) ON [DB_INDEXES01]

CREATE UNIQUE NONCLUSTERED INDEX [GROUP_INFO_U_1] ON [dbo].[GROUP_INFO] ([GROUP_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Group_Info__Group_Info_Category_Id] ON [dbo].[GROUP_INFO] ([Group_Info_Category_Id]) ON [DB_INDEXES01]


ALTER TABLE [dbo].[GROUP_INFO] ADD
CONSTRAINT [fk__Group_Info_Category__Group_Info] FOREIGN KEY ([Group_Info_Category_Id]) REFERENCES [dbo].[Group_Info_Category] ([Group_Info_Category_Id])









GO
GRANT SELECT ON  [dbo].[GROUP_INFO] TO [BPOReportRole]
GO
