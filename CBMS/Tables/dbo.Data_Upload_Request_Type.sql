CREATE TABLE [dbo].[Data_Upload_Request_Type]
(
[Data_Upload_Request_Type_Id] [int] NOT NULL IDENTITY(1, 1),
[Request_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Request_Handler_Url] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Handle_Immediate] [bit] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Master listof CBMS upload types.', 'SCHEMA', N'dbo', 'TABLE', N'Data_Upload_Request_Type', NULL, NULL
GO

ALTER TABLE [dbo].[Data_Upload_Request_Type] ADD CONSTRAINT [unc__Data_Upload_Request_Type__Request_Name] UNIQUE CLUSTERED  ([Request_Name]) ON [PRIMARY]

ALTER TABLE [dbo].[Data_Upload_Request_Type] ADD CONSTRAINT [pk_Data_Upload_Request_Type] PRIMARY KEY NONCLUSTERED  ([Data_Upload_Request_Type_Id]) ON [PRIMARY]

GO
