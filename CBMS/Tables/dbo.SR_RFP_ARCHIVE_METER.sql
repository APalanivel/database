CREATE TABLE [dbo].[SR_RFP_ARCHIVE_METER]
(
[SR_RFP_ARCHIVE_METER_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_RFP_ACCOUNT_METER_MAP_ID] [int] NOT NULL,
[METER_NUMBER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[METER_ADDRESS] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_TAX_EXEMPT] [bit] NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the meter level information for a RFP when a RFP account/group is closed. This table would store the archived information related to a meter in CBMS, so that if these information change in the future, the archived info at the time of closing a RFP account can be referenced from this table.', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ARCHIVE_METER', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_ARCHIVE_METER] ADD 
CONSTRAINT [PK__SR_RFP_ARCHIVE_M__03134B24] PRIMARY KEY CLUSTERED  ([SR_RFP_ARCHIVE_METER_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_RFP_ARCHIVE_METER] ADD
CONSTRAINT [FK_SR_RFP_ARCHIVE_METER_SR_RFP_ACCOUNT_METER_MAP] FOREIGN KEY ([SR_RFP_ACCOUNT_METER_MAP_ID]) REFERENCES [dbo].[SR_RFP_ACCOUNT_METER_MAP] ([SR_RFP_ACCOUNT_METER_MAP_ID])

GO
