CREATE TABLE [dbo].[RM_Client_Workflow_Status_Message_Map]
(
[RM_Client_Workflow_Status_Message_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Workflow_Status_Map_Id] [int] NOT NULL,
[Workflow_Status_Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [RM_Client_Workflow_Status_Message_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [RM_Client_Workflow_Status_Message_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
