CREATE TABLE [dbo].[Variance_Closed_Reason]
(
[Variance_Closed_Reason_ID] [int] NOT NULL IDENTITY(1, 1),
[Reason_Cd] [int] NOT NULL,
[Closed_DT] [date] NOT NULL,
[Account_Id] [int] NULL,
[Commodity_Id] [int] NULL,
[Service_Month] [date] NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Variance_Closed_Reason] ADD CONSTRAINT [PK_Variance_Closed_Reason] PRIMARY KEY NONCLUSTERED  ([Variance_Closed_Reason_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
CREATE NONCLUSTERED INDEX [IX_Variance_Closed_Reason__Account_id__commodity_id__service_month] ON [dbo].[Variance_Closed_Reason] ([Account_Id], [Commodity_Id], [Service_Month]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Reasons that variance tests are closed and the date.  Variance tests may be closed for multiple reasons.  Variance Closed reason text value is maintained in Code/Codeset', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Closed_Reason', NULL, NULL
GO
