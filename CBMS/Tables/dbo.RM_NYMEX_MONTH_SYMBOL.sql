CREATE TABLE [dbo].[RM_NYMEX_MONTH_SYMBOL]
(
[MONTH_INDEX] [int] NOT NULL,
[MONTH_CODE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MONTH_NAME] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is a bootstrap data table to store the symbol for months as used by Finwin', 'SCHEMA', N'dbo', 'TABLE', N'RM_NYMEX_MONTH_SYMBOL', NULL, NULL
GO

ALTER TABLE [dbo].[RM_NYMEX_MONTH_SYMBOL] ADD 
CONSTRAINT [PK_RM_NYMEX_MONTH_SYMBOL] PRIMARY KEY CLUSTERED  ([MONTH_INDEX]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
