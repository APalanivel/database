CREATE TABLE [dbo].[Invoice_Collection_Activity_Log]
(
[Invoice_Collection_Activity_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Activity_Id] [int] NOT NULL,
[Next_Action_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Activity_Log__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Activity_Log] ADD CONSTRAINT [pk_Invoice_Collection_Activity_Log] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Activity_Log_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Activity_Log__Invoice_Collection_Activity_Id] ON [dbo].[Invoice_Collection_Activity_Log] ([Invoice_Collection_Activity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Activity_Log] ADD CONSTRAINT [fk_Invoice_Collection_Activity__Invoice_Collection_Activity_Log] FOREIGN KEY ([Invoice_Collection_Activity_Id]) REFERENCES [dbo].[Invoice_Collection_Activity] ([Invoice_Collection_Activity_Id])
GO
