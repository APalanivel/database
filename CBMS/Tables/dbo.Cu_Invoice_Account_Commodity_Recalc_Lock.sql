CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock]
(
[Cu_Invoice_Account_Commodity_Recalc_Lock_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Account_Commodity_Id] [int] NOT NULL,
[Is_Locked] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_Recalc_Lock__Is_Locked] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_Recalc_Lock__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock] ADD CONSTRAINT [pk_Cu_Invoice_Account_Commodity_Recalc_Lock] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Account_Commodity_Recalc_Lock_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock] ADD CONSTRAINT [uix_Cu_Invoice_Account_Commodity_Recalc_Lock__Cu_Invoice_Account_Commodity_Id] UNIQUE NONCLUSTERED  ([Cu_Invoice_Account_Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Recalc_Lock] ADD CONSTRAINT [fk_Cu_Invoice_Account_Commodity__Cu_Invoice_Account_Commodity_Recalc_Lock] FOREIGN KEY ([Cu_Invoice_Account_Commodity_Id]) REFERENCES [dbo].[Cu_Invoice_Account_Commodity] ([Cu_Invoice_Account_Commodity_Id])
GO
