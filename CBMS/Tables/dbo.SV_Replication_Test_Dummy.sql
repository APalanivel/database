CREATE TABLE [dbo].[SV_Replication_Test_Dummy]
(
[Ident_Col] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[String_Col] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_D953CBC0_4C37_4144_8BF2_D1920F65B833_1521634897] DEFAULT (newid())
) ON [DB_DATA01]
GO


EXEC sp_addextendedproperty N'MS_Description', N'Test table for SV replication.  Table is contained within the R/W publication and used to test on SV that updates are successfully passed.  It is in the primary R/W package.', 'SCHEMA', N'dbo', 'TABLE', N'SV_Replication_Test_Dummy', NULL, NULL
GO

ALTER TABLE [dbo].[SV_Replication_Test_Dummy] ADD 
CONSTRAINT [PK_SV_Replication_Test_Dummy] PRIMARY KEY CLUSTERED  ([Ident_Col]) ON [DB_DATA01]
GO
