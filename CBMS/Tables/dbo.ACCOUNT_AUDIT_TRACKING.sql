CREATE TABLE [dbo].[ACCOUNT_AUDIT_TRACKING]
(
[TRACKING_ID] [int] NOT NULL IDENTITY(1, 1),
[ACCOUNT_AUDIT_ID] [int] NOT NULL,
[AUDIT_STATUS_TYPE_ID] [int] NOT NULL,
[EVENT_DATE] [datetime] NOT NULL,
[CLOSED_REASON_TYPE_id] [int] NULL
) ON [DBData_Deprecated]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Was previously used for a process whereby analysts would randomly audit accounts for accuracy.  Degraded.', 'SCHEMA', N'dbo', 'TABLE', N'ACCOUNT_AUDIT_TRACKING', NULL, NULL
GO

ALTER TABLE [dbo].[ACCOUNT_AUDIT_TRACKING] ADD 
CONSTRAINT [PK_ACCOUNT_AUDIT_TRACKING] PRIMARY KEY CLUSTERED  ([TRACKING_ID]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]
GO
