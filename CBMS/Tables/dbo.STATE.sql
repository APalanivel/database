CREATE TABLE [dbo].[STATE]
(
[STATE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[REGION_ID] [int] NOT NULL,
[COUNTRY_ID] [int] NOT NULL,
[STATE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Row_Version] [timestamp] NOT NULL,
[State_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([State_Name]),0)) PERSISTED
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_State_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in State table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT CLIENT_HIER_ID,STATE_NAME,REGION_ID,COUNTRY_ID,COUNTRY_NAME FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
				
		UPDATE STATE
		SET STATE_NAME = 'TEST-TX'
			,REGION_ID = 4
			,COUNTRY_ID = 3
		WHERE STATE_ID = 44
		
		SELECT CLIENT_HIER_ID,STATE_NAME,REGION_ID,COUNTRY_ID,COUNTRY_NAME FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
		
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	HG		Harihara Suthan G
	BCH		Balaraju Chalumuri

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/26/2010	Added the join with Country as Country can be change for State
			08/31/2010  Added the Last_Change_TS column for update
	HG		01/06/2011	Region_Name column added in UPDATE clause
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.

			
******/

CREATE TRIGGER [dbo].[tr_State_upd_Client_Hier] ON [dbo].[STATE]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            ch
      SET   
            STATE_NAME = i.STATE_NAME
           ,REGION_ID = i.REGION_ID
           ,Region_Name = rg.Region_Name
           ,COUNTRY_ID = i.COUNTRY_ID
           ,Country_Name = ct.Country_Name
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier ch
            INNER JOIN INSERTED i
                  ON i.STATE_ID = ch.STATE_ID
            INNER JOIN DELETED d
                  ON i.STATE_ID = d.STATE_ID
            INNER JOIN dbo.Region rg
                  ON rg.Region_id = i.Region_Id
            INNER JOIN dbo.COUNTRY ct
                  ON ct.COUNTRY_ID = i.COUNTRY_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.STATE_NAME
                                   ,i.REGION_ID
                                   ,i.COUNTRY_ID
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.STATE_NAME
                                                                       ,d.REGION_ID
                                                                       ,d.COUNTRY_ID
                                                        FOR
                                                                  XML RAW ))

END 
;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_State_upd_Client_Hier_Account
	
DESCRIPTION:   
	This will update the existing record of Core.CLIENT_HIER_ACCOUNT table whenever any updates happen in State table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID =3456 
				
		UPDATE STATE
		SET STATE_NAME = 'TEST-TX'
			,COUNTRY_ID = 5
		WHERE STATE_ID = 41
		
		SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID =3456 
		
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri


 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/25/2010	Created
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.


******/

CREATE TRIGGER [dbo].[tr_State_upd_Client_Hier_Account] ON [dbo].[STATE]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            cha
      SET   
            cha.Meter_State_Name = i.STATE_NAME
           ,cha.METER_COUNTRY_ID = i.COUNTRY_ID
           ,cha.METER_COUNTRY_NAME = ct.COUNTRY_NAME
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                  ON i.STATE_ID = cha.Meter_State_Id
            INNER JOIN DELETED d
                  ON i.STATE_ID = d.STATE_ID
            INNER JOIN dbo.COUNTRY ct
                  ON ct.COUNTRY_ID = i.COUNTRY_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.STATE_NAME
                                   ,i.COUNTRY_ID
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.STATE_NAME
                                                                       ,d.COUNTRY_ID
                                                        FOR
                                                                  XML RAW ))

END 
;
GO

ALTER TABLE [dbo].[STATE] ADD 
CONSTRAINT [STATE_PK] PRIMARY KEY CLUSTERED  ([STATE_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[STATE] ENABLE CHANGE_TRACKING
GO

CREATE NONCLUSTERED INDEX [STATE_N_2] ON [dbo].[STATE] ([REGION_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [STATE_N_1] ON [dbo].[STATE] ([COUNTRY_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE UNIQUE NONCLUSTERED INDEX [STATE_U_1] ON [dbo].[STATE] ([STATE_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE FULLTEXT INDEX ON [dbo].[STATE] KEY INDEX [STATE_PK] ON [CBMS_FTSearch]
GO

ALTER FULLTEXT INDEX ON [dbo].[STATE] ADD ([STATE_NAME] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [dbo].[STATE] ADD ([State_Name_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [dbo].[STATE] ENABLE
GO

EXEC sp_addextendedproperty N'MS_Description', 'General List of States', 'SCHEMA', N'dbo', 'TABLE', N'STATE', NULL, NULL
GO


ALTER TABLE [dbo].[STATE] ADD
CONSTRAINT [COUNTRY_STATE_FK] FOREIGN KEY ([COUNTRY_ID]) REFERENCES [dbo].[COUNTRY] ([COUNTRY_ID])
ALTER TABLE [dbo].[STATE] ADD
CONSTRAINT [REGION_STATE_FK] FOREIGN KEY ([REGION_ID]) REFERENCES [dbo].[REGION] ([REGION_ID])







GO
