CREATE TABLE [dbo].[SR_RFP_Comment]
(
[SR_RFP_Id] [int] NOT NULL,
[Comment_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[SR_RFP_Comment] ADD CONSTRAINT [pk_SR_RFP_Comment] PRIMARY KEY CLUSTERED  ([SR_RFP_Id], [Comment_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[SR_RFP_Comment] ADD CONSTRAINT [fk_Comment__SR_RFP_Comment] FOREIGN KEY ([Comment_Id]) REFERENCES [dbo].[Comment] ([Comment_ID])
GO
ALTER TABLE [dbo].[SR_RFP_Comment] ADD CONSTRAINT [fk_SR_RFP__SR_RFP_Comment] FOREIGN KEY ([SR_RFP_Id]) REFERENCES [dbo].[SR_RFP] ([SR_RFP_ID])
GO
