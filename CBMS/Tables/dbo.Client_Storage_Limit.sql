CREATE TABLE [dbo].[Client_Storage_Limit]
(
[Client_Storage_Limit_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL CONSTRAINT [df_Client_Storage_Limit__Client_Id] DEFAULT ((-1)),
[Image_Type_Id] [int] NOT NULL,
[Storage_Limit] [bigint] NOT NULL,
[Storage_Space_Used] [decimal] (32, 16) NOT NULL CONSTRAINT [df_Client_Storage_Limit__Storage_Space_Used] DEFAULT ((0.0)),
[Storage_UOM_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Storage_Limit__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Storage_Limit__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Client Image storage limits for remote storage.  Relates to the image server.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Storage_Limit', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Storage_Limit] ADD CONSTRAINT [PK_Client_Storage_Limit] PRIMARY KEY CLUSTERED  ([Client_Storage_Limit_Id]) ON [DB_DATA01]
GO
