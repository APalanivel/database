CREATE TABLE [dbo].[CLIENT_CURRENCY_GROUP_MAP]
(
[currency_group_id] [int] NOT NULL,
[client_id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Maps the Client to the Currency Group', 'SCHEMA', N'dbo', 'TABLE', N'CLIENT_CURRENCY_GROUP_MAP', NULL, NULL
GO

ALTER TABLE [dbo].[CLIENT_CURRENCY_GROUP_MAP] ADD 
CONSTRAINT [PK_CLIENT_CURRENCY_GROUP_MAP] PRIMARY KEY CLUSTERED  ([client_id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_client_currency_group_map__currency_group_id] ON [dbo].[CLIENT_CURRENCY_GROUP_MAP] ([currency_group_id]) INCLUDE ([client_id]) ON [DB_INDEXES01]




GO
