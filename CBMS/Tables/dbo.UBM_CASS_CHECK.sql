CREATE TABLE [dbo].[UBM_CASS_CHECK]
(
[UTIL_BILL_HEADER_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CHECK_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAY_DATE] [datetime] NULL,
[UBM_BATCH_MASTER_LOG_ID] [int] NULL
) ON [DBData_Deprecated]
ALTER TABLE [dbo].[UBM_CASS_CHECK] ADD
CONSTRAINT [FK__UBM_CASS___UBM_B__2D9C1FC8] FOREIGN KEY ([UBM_BATCH_MASTER_LOG_ID]) REFERENCES [dbo].[UBM_BATCH_MASTER_LOG] ([UBM_BATCH_MASTER_LOG_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the CHECK table of the CASS feed', 'SCHEMA', N'dbo', 'TABLE', N'UBM_CASS_CHECK', NULL, NULL
GO

CREATE CLUSTERED INDEX [CX_UBM_BATCH_MASTER_LOG_ID] ON [dbo].[UBM_CASS_CHECK] ([UBM_BATCH_MASTER_LOG_ID]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]




GO
