CREATE TABLE [dbo].[BUDGET]
(
[BUDGET_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CLIENT_ID] [int] NOT NULL,
[COMMODITY_TYPE_ID] [int] NOT NULL,
[BUDGET_START_YEAR] [int] NOT NULL,
[BUDGET_START_MONTH] [int] NOT NULL,
[BUDGET_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DUE_DATE] [smalldatetime] NOT NULL,
[DATE_INITIATED] [smalldatetime] NOT NULL,
[BUDGET_DATE] [smalldatetime] NULL,
[INITIATED_BY] [int] NOT NULL,
[POSTED_BY] [int] NULL,
[DATE_POSTED] [smalldatetime] NULL,
[CBMS_IMAGE_ID] [int] NULL,
[COMMENTS] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_SHOWN_ON_CEM_HOME_PAGE] [bit] NOT NULL CONSTRAINT [DF__BUDGET__IS_SHOWN__4B1AA172] DEFAULT ((1)),
[IS_POSTED_TO_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET__IS_POSTE__4C0EC5AB] DEFAULT ((0)),
[STATUS_TYPE_ID] [int] NOT NULL,
[IS_SHOWN_COMMENTS_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET__IS_SHOWN__4D02E9E4] DEFAULT ((0)),
[ORIGINAL_BUDGET_ID] [int] NULL,
[IS_INCLUDE_IN_ANALYSTS_QUEUES] [bit] NOT NULL CONSTRAINT [DF__BUDGET__IS_INCLU__533C1512] DEFAULT ((0)),
[Row_Version] [timestamp] NOT NULL,
[Analyst_Queue_Display_Cd] [int] NOT NULL CONSTRAINT [df_Budget_Analyst_Queue_Display_Cd] DEFAULT ((0))
) ON [DBData_Budget]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.tr_Budget_Audit

DESCRIPTION:

	Used to log DML actions happening on Budget in to Budget_Audit table.

	Audit_Function	Values
			1	- Insert
			0	- Update
		   -1   - Delete

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------


	BEGIN TRAN
		
			UPDATE 
				Budget
				SET Budget_Name = 'Eaton_NG_2008_Rev1' 
			WHERE
				Budget_id = 1548
	
			SELECT * FROM Budget_Audit WHERE Budget_id = 1548

	ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	06/03/2010	Created

******/

CREATE TRIGGER [dbo].[tr_Budget_Audit]
	ON [dbo].[BUDGET]
	FOR INSERT, UPDATE, DELETE
AS
BEGIN

	SET NOCOUNT ON;

	INSERT dbo.Budget_Audit
	(
		Audit_Function
		,Audit_Ts
		,Budget_id
	)
	SELECT
		CASE
			WHEN d.Budget_id IS NULL THEN 1		-- Inserted
			WHEN i.Budget_id IS NULL THEN -1	-- Deleted
			ELSE 0								-- Updated
		END
		,GETDATE()
		,ISNULL(i.Budget_id, d.Budget_id)
	FROM
		INSERTED i
		FULL OUTER JOIN DELETED d
			ON d.Budget_Id = i.Budget_Id

END
GO
ALTER TABLE [dbo].[BUDGET] ADD CONSTRAINT [pk_BUDGET] PRIMARY KEY CLUSTERED  ([BUDGET_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
CREATE NONCLUSTERED INDEX [ix_BUDGET__BUDGET_START_YEAR__INCL] ON [dbo].[BUDGET] ([BUDGET_START_YEAR]) INCLUDE ([BUDGET_START_MONTH], [COMMODITY_TYPE_ID], [DUE_DATE], [POSTED_BY], [STATUS_TYPE_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_CLIENT_ID] ON [dbo].[BUDGET] ([CLIENT_ID], [BUDGET_NAME], [COMMODITY_TYPE_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Account_ID_Commodity_Type] ON [dbo].[BUDGET] ([CLIENT_ID], [BUDGET_START_YEAR], [COMMODITY_TYPE_ID], [BUDGET_ID]) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Identifies a Client''s budget Budget header information at Client level - Start year, whether it''s posted to dv…', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET', NULL, NULL
GO
