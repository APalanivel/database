CREATE TABLE [dbo].[User_Preference]
(
[User_Info_Id] [int] NOT NULL,
[User_Preference_Cd] [int] NOT NULL,
[User_Preference_Value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Id] [int] NOT NULL CONSTRAINT [df__User_Preference__Client_Id] DEFAULT ((-1)),
[User_Preference_Dtl] [xml] NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[User_Preference] ADD CONSTRAINT [pk_User_Preference] PRIMARY KEY CLUSTERED  ([User_Info_Id], [Client_Id], [User_Preference_Cd]) ON [DB_DATA01]
GO
