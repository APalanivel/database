CREATE TABLE [dbo].[Utility_Document]
(
[Utility_Document_Id] [int] NOT NULL IDENTITY(1, 1),
[VENDOR_ID] [int] NOT NULL,
[CBMS_IMAGE_ID] [int] NOT NULL,
[Utility_Document_Type_ID] [int] NOT NULL
) ON [DBData_CoreClient]
ALTER TABLE [dbo].[Utility_Document] WITH NOCHECK ADD
CONSTRAINT [fk_CBMS_IMAGE__Utility_Document] FOREIGN KEY ([CBMS_IMAGE_ID]) REFERENCES [dbo].[cbms_image] ([CBMS_IMAGE_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'Documents uploaded in CBMS for a utility', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Document', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Document] ADD 
CONSTRAINT [pk_Utility_Document] PRIMARY KEY CLUSTERED  ([Utility_Document_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Utility_Document__VENDOR_ID] ON [dbo].[Utility_Document] ([VENDOR_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Document__CBMS_IMAGE_ID] ON [dbo].[Utility_Document] ([CBMS_IMAGE_ID]) ON [DB_INDEXES01]


ALTER TABLE [dbo].[Utility_Document] ADD
CONSTRAINT [fk_VENDOR__Utility_Document] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])




GO
