CREATE TABLE [Workflow].[Workflow_Queue]
(
[Workflow_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Workflow_Queue_Category_Cd] [int] NOT NULL,
[Queue_Display_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Permission_Info_Id] [int] NULL,
[Queue_Result_Data_Source_Proc_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Workflow_Queue__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue] ADD CONSTRAINT [pk_Workflow_Queue] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue] ADD CONSTRAINT [un_Workflow_Queue__Workflow_Queue_Name__Workflow_Queue_Category_Cd] UNIQUE NONCLUSTERED  ([Workflow_Queue_Name], [Workflow_Queue_Category_Cd]) ON [DB_DATA01]
GO
