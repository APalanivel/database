CREATE TABLE [dbo].[Entity_Name_Alias]
(
[Entity_Id] [int] NOT NULL,
[Entity_Name_Alias] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Entity table alias.  Legacy code references entity text values that can not be changed.', 'SCHEMA', N'dbo', 'TABLE', N'Entity_Name_Alias', NULL, NULL
GO

ALTER TABLE [dbo].[Entity_Name_Alias] ADD CONSTRAINT [pk_Entity_Name_Alias] PRIMARY KEY CLUSTERED  ([Entity_Id]) ON [DBData_Reference]
GO
ALTER TABLE [dbo].[Entity_Name_Alias] ADD CONSTRAINT [fk_Entity__Entity_Alias_Name] FOREIGN KEY ([Entity_Id]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
