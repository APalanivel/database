CREATE TABLE [dbo].[RM_Scenario_Report]
(
[RM_Scenario_Report_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Scenario_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Is_Include_Trigger] [bit] NOT NULL CONSTRAINT [df_RM_Scenario_Report__Is_Include_Trigger] DEFAULT ((0)),
[Start_Service_Month] [date] NOT NULL,
[End_Service_Month] [date] NOT NULL,
[Uom_Type_Id] [int] NOT NULL,
[Currency_Unit_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Report__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_RM_Scenario_Report__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management scenario report header, scenario name, start/end month', 'SCHEMA', N'dbo', 'TABLE', N'RM_Scenario_Report', NULL, NULL
GO

ALTER TABLE [dbo].[RM_Scenario_Report] ADD CONSTRAINT [pk_RM_Scenario_Report] PRIMARY KEY NONCLUSTERED  ([RM_Scenario_Report_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Scenario_Report] ADD CONSTRAINT [unc_RM_Scenario_Report__Client_Id__Scenario_Name] UNIQUE CLUSTERED  ([Client_Id], [Scenario_Name]) ON [DB_DATA01]
GO
