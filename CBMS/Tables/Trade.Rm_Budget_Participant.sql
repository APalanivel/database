CREATE TABLE [Trade].[Rm_Budget_Participant]
(
[Rm_Budget_Participant_Id] [int] NOT NULL IDENTITY(1, 1),
[Rm_Budget_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Participant] ADD CONSTRAINT [PK_Rm_Budget_Participant__Rm_Budget_Participant_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Participant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Participant] ADD CONSTRAINT [Uk_Trade_Rm_Budget_Participant] UNIQUE NONCLUSTERED  ([Rm_Budget_Id], [Client_Hier_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Participant] ADD CONSTRAINT [FK_Rm_Budget_Participant__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
