CREATE TABLE [dbo].[Utility_Dtl_Balancing_Requirement]
(
[Utility_Dtl_Balancing_Requirement_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Industrial_Flag_Cd] [int] NOT NULL,
[Commercial_Flag_Cd] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Utility Natural Gas Balancing Requirements', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Balancing_Requirement', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Balancing_Requirement] ADD CONSTRAINT [unc_Utility_Dtl_Balancing_Req__Question_Commodity_Map_Id__Vendor_Commodity_Map_Id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Balancing_Requirement] ADD CONSTRAINT [pk_Utility_Dtl_Balancing_Requirement] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Balancing_Requirement_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Balancing_Requirement__Vendor_Commodity_Map_ID] ON [dbo].[Utility_Dtl_Balancing_Requirement] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Balancing_Requirement__Comment_ID] ON [dbo].[Utility_Dtl_Balancing_Requirement] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Balancing_Requirement] ADD
CONSTRAINT [fk_Utility_Dtl_Balancing_Requirement__Question_Commodity_Map_Id] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Balancing_Requirement] ADD
CONSTRAINT [fk_Utility_Dtl_Balancing_Requirement__Vendor_Commodity_Map_ID] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Balancing_Requirement] ADD
CONSTRAINT [fk_Utility_Dtl_Balancing_Requirement__Comment_ID] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
