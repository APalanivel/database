CREATE TABLE [dbo].[Invoice_Collection_Queue_Month_Map]
(
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Account_Invoice_Collection_Month_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue_Month_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Queue_Month_Map] ADD CONSTRAINT [pk_Invoice_Collection_Queue_Month_Map] PRIMARY KEY NONCLUSTERED  ([Invoice_Collection_Queue_Id], [Account_Invoice_Collection_Month_Id]) ON [DB_DATA01]
GO
CREATE CLUSTERED INDEX [cix_Invoice_Collection_Queue_Month_Map__Account_Invoice_Collection_Month_Id] ON [dbo].[Invoice_Collection_Queue_Month_Map] ([Account_Invoice_Collection_Month_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Queue_Month_Map] ADD CONSTRAINT [fk_Invoice_Collection_Queue__Invoice_Collection_Queue_Month_Map] FOREIGN KEY ([Invoice_Collection_Queue_Id]) REFERENCES [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Queue_Id])
GO
