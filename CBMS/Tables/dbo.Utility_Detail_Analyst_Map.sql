CREATE TABLE [dbo].[Utility_Detail_Analyst_Map]
(
[Utility_Detail_Analyst_Map_ID] [int] NOT NULL IDENTITY(1, 1),
[Utility_Detail_ID] [int] NOT NULL,
[Group_Info_ID] [int] NOT NULL,
[Analyst_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of analyst to group to utility details', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Detail_Analyst_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Detail_Analyst_Map] WITH NOCHECK ADD
CONSTRAINT [FK_Utility_Detail_Analyst_Map__USER_INFO] FOREIGN KEY ([Analyst_ID]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[Utility_Detail_Analyst_Map] ADD 
CONSTRAINT [PK_Utility_Detail_Analyst_Map] PRIMARY KEY CLUSTERED  ([Utility_Detail_Analyst_Map_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [IX_Utility_Detail_Analyst_Map__Analyst_ID__Utility_Detail_ID__Group_Info_ID] ON [dbo].[Utility_Detail_Analyst_Map] ([Analyst_ID], [Utility_Detail_ID], [Group_Info_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Utility_Detail_Analyst_Map__Group_Info_ID] ON [dbo].[Utility_Detail_Analyst_Map] ([Group_Info_ID]) INCLUDE ([Analyst_ID], [Utility_Detail_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Utility_Detail_Analyst_Map__Utility_Detail_ID] ON [dbo].[Utility_Detail_Analyst_Map] ([Utility_Detail_ID]) INCLUDE ([Analyst_ID], [Group_Info_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Detail_Analyst_Map] ADD
CONSTRAINT [FK_Utility_Detail_Analyst_Map__Group_Info] FOREIGN KEY ([Group_Info_ID]) REFERENCES [dbo].[GROUP_INFO] ([GROUP_INFO_ID])

ALTER TABLE [dbo].[Utility_Detail_Analyst_Map] ADD
CONSTRAINT [FK_Utility_Detail_Analyst_Map__UTILITY_DETAIL] FOREIGN KEY ([Utility_Detail_ID]) REFERENCES [dbo].[UTILITY_DETAIL] ([UTILITY_DETAIL_ID])




GO
