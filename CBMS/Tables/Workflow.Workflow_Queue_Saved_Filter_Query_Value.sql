CREATE TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query_Value]
(
[Workflow_Queue_Saved_Filter_Query_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Saved_Filter_Query_Id] [int] NOT NULL,
[Workflow_Queue_Search_Filter_Id] [int] NOT NULL,
[Selected_Value] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Saved_Filter_Query_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Saved_Filter_Query_Value__Last_Change_Ts] DEFAULT (getdate()),
[Selected_KeyValue] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query_Value] ADD CONSTRAINT [pk_Workflow_Queue_Saved_Filter_Query_Value] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Saved_Filter_Query_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query_Value] ADD CONSTRAINT [un_Workflow_Queue_Saved_Filter_Query_Value__Workflow_Queue_Saved_Filter_Query_Id__Workflow_Queue_Search_Filter_Id] UNIQUE NONCLUSTERED  ([Workflow_Queue_Saved_Filter_Query_Id], [Workflow_Queue_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query_Value] ADD CONSTRAINT [fk_Workflow_Queue_Saved_Filter_Query__Workflow_Queue_Saved_Filter_Query_Value] FOREIGN KEY ([Workflow_Queue_Saved_Filter_Query_Id]) REFERENCES [Workflow].[Workflow_Queue_Saved_Filter_Query] ([Workflow_Queue_Saved_Filter_Query_Id])
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query_Value] ADD CONSTRAINT [fk_Workflow_Queue_Search_Filter__Workflow_Queue_Saved_Filter_Query_Value] FOREIGN KEY ([Workflow_Queue_Search_Filter_Id]) REFERENCES [Workflow].[Workflow_Queue_Search_Filter] ([Workflow_Queue_Search_Filter_Id])
GO
