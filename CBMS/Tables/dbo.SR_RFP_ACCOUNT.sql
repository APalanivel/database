CREATE TABLE [dbo].[SR_RFP_ACCOUNT]
(
[SR_RFP_ACCOUNT_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ACCOUNT_ID] [int] NOT NULL,
[SR_RFP_ID] [int] NOT NULL,
[SR_RFP_BID_GROUP_ID] [int] NULL,
[BID_STATUS_TYPE_ID] [int] NOT NULL,
[RISK_TOLERANCE_TYPE_ID] [int] NULL,
[IS_DELETED] [bit] NULL CONSTRAINT [DF__SR_RFP_AC__IS_DE__7C435672] DEFAULT ((0)),
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_18D38570_1A31_4B8E_8B05_222BAAE33F8F_615074073] DEFAULT (newid())
) ON [DBData_Sourcing]
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [PK__SR_RFP_ACCOUNT__332B7579] PRIMARY KEY CLUSTERED  ([SR_RFP_ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
CREATE NONCLUSTERED INDEX [IX_SR_RFP_Account_Account_ID_IS_Deleted] ON [dbo].[SR_RFP_ACCOUNT] ([ACCOUNT_ID], [IS_DELETED], [SR_RFP_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SR_RFP] ON [dbo].[SR_RFP_ACCOUNT] ([ACCOUNT_ID], [SR_RFP_ID], [IS_DELETED]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SR_RFP_ACCOUNT__IS_DELETED] ON [dbo].[SR_RFP_ACCOUNT] ([IS_DELETED]) INCLUDE ([ACCOUNT_ID], [BID_STATUS_TYPE_ID], [SR_RFP_ACCOUNT_ID], [SR_RFP_BID_GROUP_ID], [SR_RFP_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SR_RFP_ACCOUNT__IS_DELETED__BID_STATUS_TYPE_ID__Include] ON [dbo].[SR_RFP_ACCOUNT] ([IS_DELETED], [BID_STATUS_TYPE_ID], [ACCOUNT_ID], [SR_RFP_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SR_RFP_ACCOUNT__SR_RFP_ID__SR_RFP_ACCOUNT_ID] ON [dbo].[SR_RFP_ACCOUNT] ([SR_RFP_ID], [SR_RFP_ACCOUNT_ID]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_ACCOUNT] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_ENTITY] FOREIGN KEY ([BID_STATUS_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_ENTITY1] FOREIGN KEY ([RISK_TOLERANCE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_SR_RFP] FOREIGN KEY ([SR_RFP_ID]) REFERENCES [dbo].[SR_RFP] ([SR_RFP_ID])
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_SR_RFP_BID_GROUP] FOREIGN KEY ([SR_RFP_BID_GROUP_ID]) REFERENCES [dbo].[SR_RFP_BID_GROUP] ([SR_RFP_BID_GROUP_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the table to store all the accounts selected for a particular RFP', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ACCOUNT', NULL, NULL
GO
