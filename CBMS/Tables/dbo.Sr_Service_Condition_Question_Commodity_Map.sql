CREATE TABLE [dbo].[Sr_Service_Condition_Question_Commodity_Map]
(
[Sr_Service_Condition_Question_Commodity_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Sr_Service_Condition_Question_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question_Commodity_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Question_Commodity_Map_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Question_Commodity_Map] ADD CONSTRAINT [fk_Sr_Service_Condition_Question_Commodity_Map__Sr_Service_Condition_Question] FOREIGN KEY ([Sr_Service_Condition_Question_Id]) REFERENCES [dbo].[Sr_Service_Condition_Question] ([Sr_Service_Condition_Question_Id])
GO
