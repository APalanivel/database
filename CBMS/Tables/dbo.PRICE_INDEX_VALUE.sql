CREATE TABLE [dbo].[PRICE_INDEX_VALUE]
(
[PRICE_INDEX_VALUE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PRICE_INDEX_ID] [int] NOT NULL,
[INDEX_VALUE] [decimal] (32, 16) NOT NULL,
[INDEX_MONTH] [datetime] NOT NULL
) ON [DBData_Risk_MGMT]
ALTER TABLE [dbo].[PRICE_INDEX_VALUE] ADD
CONSTRAINT [PRICE_INDEX_PRICE_INDEX_VALUE_FK] FOREIGN KEY ([PRICE_INDEX_ID]) REFERENCES [dbo].[PRICE_INDEX] ([PRICE_INDEX_ID])
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Price_Index_Value_Transmission

DESCRIPTION: Trigger on Price_Index to send change data to the 
	Change control service.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2016-02-02	Created to send the price_index changes to sys
******/
CREATE TRIGGER [dbo].[tr_Price_Index_Value_Transmission] ON [dbo].[PRICE_INDEX_VALUE]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Price_Index_Value XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255)
           ,@EC_Transmission_Price_Point_Value_Cut_Off_Dt DATE;

      SELECT
            @EC_Transmission_Price_Point_Value_Cut_Off_Dt = App_Config_Value
      FROM
            dbo.App_Config
      WHERE
            App_Config_Cd = 'EC_Transmission_Price_Point_Value_Cut_Off_Dt';

      SET @Message_Price_Index_Value = ( SELECT
                                          prcv.Price_Index_Value_Id
                                         ,prcv.Price_Index_Id
                                         ,dd.FIRST_DAY_OF_MONTH_D AS StartDate
                                         ,dd.LAST_DAY_OF_MONTH_D AS EndDate
                                         ,prcv.Price
                                         ,prci.CURRENCY_UNIT_ID AS Currency_Unit_Id
                                         ,cu.CURRENCY_UNIT_NAME AS Currency_Unit_Name
                                         ,prcv.Sys_Change_Operation
                                         ,@@SERVERNAME AS Server_Name
                                         FROM
                                          ( SELECT
                                                ISNULL(ins.PRICE_INDEX_VALUE_ID, del.PRICE_INDEX_VALUE_ID) AS Price_Index_Value_Id
                                               ,ISNULL(ins.PRICE_INDEX_ID, del.PRICE_INDEX_ID) AS Price_Index_Id
                                               ,ISNULL(ins.INDEX_MONTH, del.INDEX_MONTH) AS Index_Month
                                               ,ISNULL(ins.INDEX_VALUE, del.INDEX_VALUE) AS Price
                                               ,CASE WHEN ins.PRICE_INDEX_VALUE_ID IS NULL
                                                          AND del.PRICE_INDEX_VALUE_ID IS NOT NULL THEN 'D'
                                                     WHEN ins.PRICE_INDEX_VALUE_ID IS NOT NULL
                                                          AND del.PRICE_INDEX_VALUE_ID IS NOT NULL THEN 'U'
                                                     WHEN ins.PRICE_INDEX_VALUE_ID IS NOT NULL
                                                          AND del.PRICE_INDEX_VALUE_ID IS NULL THEN 'I'
                                                END AS Sys_Change_Operation
                                            FROM
                                                Inserted ins
                                                FULL JOIN Deleted del
                                                      ON del.PRICE_INDEX_VALUE_ID = ins.PRICE_INDEX_VALUE_ID ) prcv
                                          INNER JOIN dbo.PRICE_INDEX prci
                                                ON prci.PRICE_INDEX_ID = prcv.Price_Index_Id
                                          INNER JOIN meta.DATE_DIM dd
                                                ON dd.DATE_D = prcv.Index_Month
                                          LEFT OUTER JOIN dbo.CURRENCY_UNIT cu
                                                ON cu.CURRENCY_UNIT_ID = prci.CURRENCY_UNIT_ID
                                         WHERE
                                          dd.DATE_D >= @EC_Transmission_Price_Point_Value_Cut_Off_Dt
            FOR
                                         XML PATH('Price_Index_Value_Change')
                                            ,ELEMENTS
                                            ,ROOT('Price_Index_Value_Changes') ); 

      IF @Message_Price_Index_Value IS NOT NULL
            BEGIN

                  SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Currency_And_Price_Change';

				  -- Cycle through the targets in Change_Control Targt and sent the message to each
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        dbo.Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Price_Index_Value');

                  OPEN cDialog;
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                  WHILE @@Fetch_Status = 0
                        BEGIN

                              EXEC Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                              SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Transmission/Message/Cbms_Price_Index_Value_Change] (@Message_Price_Index_Value);

                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                        END;
                  CLOSE cDialog;
                  DEALLOCATE cDialog;

            END;

END;


;
GO

EXEC sp_addextendedproperty N'MS_Description', N'This table would store the actual value monthwise for the price indexes', 'SCHEMA', N'dbo', 'TABLE', N'PRICE_INDEX_VALUE', NULL, NULL
GO

ALTER TABLE [dbo].[PRICE_INDEX_VALUE] ADD 
CONSTRAINT [PRICE_INDEX_VALUE_PK] PRIMARY KEY CLUSTERED  ([PRICE_INDEX_VALUE_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
CREATE NONCLUSTERED INDEX [PRICE_INDEX_VALUE_N_1] ON [dbo].[PRICE_INDEX_VALUE] ([PRICE_INDEX_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE UNIQUE NONCLUSTERED INDEX [PRICE_INDEX_VALUE_U_1] ON [dbo].[PRICE_INDEX_VALUE] ([PRICE_INDEX_ID], [INDEX_MONTH]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]











GO
