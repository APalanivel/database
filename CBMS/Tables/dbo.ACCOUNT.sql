CREATE TABLE [dbo].[ACCOUNT]
(
[ACCOUNT_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ACCOUNT_GROUP_ID] [int] NULL,
[VENDOR_ID] [int] NOT NULL,
[SITE_ID] [int] NULL,
[INVOICE_SOURCE_TYPE_ID] [int] NULL,
[ACCOUNT_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACCOUNT_TYPE_ID] [int] NOT NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero] DEFAULT ((0)),
[IS_IN_INVOICE_CHECKLIST] [bit] NULL,
[NOT_EXPECTED] [bit] NOT NULL CONSTRAINT [DF_ACCOUNT_NOT_EXPECTED] DEFAULT ((0)),
[NOT_MANAGED] [bit] NOT NULL CONSTRAINT [DF_ACCOUNT_NOT_MANAGED] DEFAULT ((0)),
[NOT_EXPECTED_DATE] [datetime] NULL,
[NOT_EXPECTED_BY_ID] [int] NULL,
[USER_INFO_ID] [int] NULL,
[SERVICE_LEVEL_TYPE_ID] [int] NULL,
[UBM_ID] [int] NULL,
[UBM_ACCOUNT_CODE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DM_WATCH_LIST] [bit] NULL,
[RA_WATCH_LIST] [bit] NULL,
[PROCESSING_INSTRUCTIONS] [varchar] (6000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELIGIBILITY_DATE] [datetime] NULL,
[CONSOLIDATED_BILLING_POSTED_TO_UTILITY] [bit] NULL CONSTRAINT [cbpu_zero] DEFAULT ((0)),
[PROCESSING_INSTRUCTIONS_UPDATED_USER_ID] [int] NULL,
[PROCESSING_INSTRUCTIONS_UPDATED_DATE] [datetime] NULL,
[Row_Version] [timestamp] NULL,
[Is_Data_Entry_Only] [bit] NOT NULL CONSTRAINT [DF__ACCOUNT__Is_Data__7B8E8FCE] DEFAULT ((0)),
[Supplier_Account_Recalc_Type_Cd] [int] NULL,
[Supplier_Account_Is_Default_Setting] [int] NULL,
[Supplier_Account_Begin_Dt] [datetime] NULL,
[Supplier_Account_End_Dt] [datetime] NULL,
[Account_Number_Search] AS (replace(replace(replace([account_number],'/',space((0))),space((1)),space((0))),'-',space((0)))) PERSISTED,
[Analyst_Mapping_Cd] [int] NULL,
[Is_Broker_Account] [bit] NOT NULL CONSTRAINT [DF__ACCOUNT__Is_Brok__39DB6A6B] DEFAULT ((0)),
[Template_Cu_Invoice_Id] [int] NULL,
[Template_Cu_Invoice_Updated_User_Id] [int] NULL,
[Template_Cu_Invoice_Last_Change_Ts] [datetime] NULL,
[Is_Invoice_Posting_Blocked] [bit] NOT NULL CONSTRAINT [df_ACCOUNT_Is_Invoice_Posting_Blocked] DEFAULT ((0)),
[Alternate_Account_Number] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Supplier_Account_Determinant_Source_Cd] [int] NULL
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_ACCOUNT_Del_Client_Hier_Account
	
DESCRIPTION:   
	the records get deleted from Core.Client_Hier_Account table while deleting the record from Account table.
 
 USAGE EXAMPLES:
------------------------------------------------------------
	
BEGIN TRAN


	SELECT * FROM Core.Client_Hier_Account WHERE ACCOUNT_ID = 289866
				
	DELETE ACCOUNT
	WHERE ACCOUNT_ID = 289866

	SELECT * FROM ACCOUNT WHERE ACCOUNT_ID = 289866

ROLLBACK TRAN
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/20/2010	Created

******/


CREATE TRIGGER [dbo].[tr_ACCOUNT_Del_Client_Hier_Account] ON [dbo].[ACCOUNT]
      FOR DELETE
AS
BEGIN
      SET NOCOUNT ON ; 
	
      DELETE
            cha
      FROM
            core.Client_Hier_Account cha
            INNER JOIN DELETED d ON d.ACCOUNT_ID = cha.ACCOUNT_ID
END 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******              
NAME:        
 dbo.tr_ACCOUNT_upd_Client_Hier_Account        
        
DESCRIPTION:        
 This will update the existing record of Core.Clinet_Hier_Account table whenever any updates happen in ACCOUNT table.        
        
 USAGE EXAMPLES:        
------------------------------------------------------------        
        
 BEGIN TRAN        
        
  SELECT * FROM ACCOUNT where ACCOUNT_ID = 164        
        
  UPDATE ACCOUNT        
  SET vendor_id = 65        
  , site_id = 1925        
  , Supplier_Account_Recalc_Type_Cd = 1        
  WHERE        
  ACCOUNT_ID = 164        
              
  SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID = 164            
              
 ROLLBACK TRAN            
             
AUTHOR INITIALS:            
 Initials Name            
------------------------------------------------------------            
 SKA  Shobhit Kumar Agrawal           
 RR   Raghu Reddy           
 AP   Athmaram Pabbathi          
 HG   Harihara Suthan Ganesan          
 AKR  Ashok Kumar Raju        
 BCH  Balaraju Chalumuri         
 SP   Sandeep Pigilam      
 RKV  Ravi vegesna         
         
        
 MODIFICATIONS:            
 Initials  Date   Modification            
------------------------------------------------------------            
 SKA   08/20/2010  Created            
    09/17/2010  Added the update script for Client_Hier_Id (Movement of Account from one Site to another Site)            
    09/22/2010  Added update statement for Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY            
    09/23/2010  Added update statement for Supplier_Account_Recalc_Type_Cd/Dsc            
    10/18/2010  Added update for Column Account_Vendor_Type_ID            
    10/28/2010  Added update for Column Account_Number_Search,Account_Group_ID & WATCHLIST-GROUP-INFO-ID            
        Delete the update statement for Account_RA_WatchList            
    11/23/2010  Added the statement for Account_Number_Search & Meter_Number_Search              
                       
 RR    06/08/2011  Added update statememnt to populate Display_Account_Number          
 HG    07/12/2011  Added ISNULL check in the update statement which used to update the Display_Account_Number          
        (As this Display_Account_Number coulmn gets inserted with NULL we need this validation)           
 AP    05/04/2012  Fixed the case statement used of updating Display_Account_Number (ADLDT-202)          
        with ADLDT changes we are replacing the base tables/view with CH & CHA in the SP during these this issue was was identified during Testing by CTE QC Team.          
 HG    2012-07-25  Code added to update Account_Not_Expected_Dt column as it is required in DVDEHub for DE III enhancement.          
 AKR   2012-10-08  Modified the code to update Account_Analyst_Mapping_Code and Account_Is_Broker        
 BCH   2013-04-12  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.        
 RR    2013-08-13  MAINT-1903 Whenever the maximum meter disassociation date of the supplier account is greater than supplier accoung end Date, it is appending         
        the values with begin(Supplier_Account_Begin_Dt) date of the supplier account instead of supplier account end(Supplier_Accoung_End_Dt)date         
        for Display_Account_Number        
 RR    2013-08-13  Data Operations-2 Modified to update Template Invoice information columns        
 SP    2014-07-30  For Data Operations Enhancement Phase IIII,Added [Is_Level2_Recalc_Validation_Required],[Is_Invoice_Posting_Blocked] columns.        
 SP    2014-08-08  MAINT-2920, modified to update the Last_Change_Ts column to help the error message processing objects in DVDEHub depend on this column value to re process the message.        
 HG    2015-07-17  CBMS Sourcing Enh - Changes made to update Alternate_Account_Column to CHA      
 RKV   2015-10-19  AS400 PII -   Changes made to remove the column Is_Level2_Recalc_Validation_Required.      
 NR    2017-03-07  Contract Placeholder.      
- Update the DMO supplier account number end date as ?Unspecified? if DMO has open end date.      
        - Populate the Supplier_Account_End_Dt as 9999-12-31 if the DMO has open end date.      
        - Removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.      
NR    2019-04-03  Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.      
NR    2019-07-08  Add Contract - Added Max Condition for Legacy account to show the display acc number.    
RKV  2019-12-30 D20-1762   removed ubm_Account_Code  
      
******/

CREATE TRIGGER [dbo].[tr_ACCOUNT_upd_Client_Hier_Account]
ON [dbo].[ACCOUNT]
FOR UPDATE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Legacy_Account_Dt DATETIME;


        SELECT
            @Legacy_Account_Dt = CONVERT(DATETIME, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Legacy_Account';




        DECLARE @Total_Supplier_Accoount_Config_Ids TABLE
              (
                  Account_Id INT
                  , Total_Supplier_Accoount_Config_Ids INT
              );

        INSERT INTO @Total_Supplier_Accoount_Config_Ids
             (
                 Account_Id
                 , Total_Supplier_Accoount_Config_Ids
             )
        SELECT
            i.ACCOUNT_ID
            , ISNULL(COUNT(DISTINCT sac.Supplier_Account_Config_Id), 0)
        FROM
            INSERTED i
            INNER JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = i.ACCOUNT_ID
        GROUP BY
            i.ACCOUNT_ID;
        UPDATE
            cha
        SET
            Account_Number = i.ACCOUNT_NUMBER
            , Account_Number_Search = i.Account_Number_Search
            , Account_Invoice_Source_Cd = i.INVOICE_SOURCE_TYPE_ID
            , Account_Is_Data_Entry_Only = ISNULL(i.Is_Data_Entry_Only, 0)
            , Account_Not_Expected = i.NOT_EXPECTED
            , Account_Not_Managed = i.NOT_MANAGED
            , Account_Service_level_Cd = i.SERVICE_LEVEL_TYPE_ID
            , Account_Vendor_Id = i.VENDOR_ID
            , Account_Vendor_Name = ven.VENDOR_NAME
            , Account_Eligibility_Dt = i.ELIGIBILITY_DATE
            , Account_Group_ID = i.ACCOUNT_GROUP_ID
            , Supplier_Account_Recalc_Type_Cd = i.Supplier_Account_Recalc_Type_Cd
            , Supplier_Account_Recalc_Type_Dsc = cd.Code_Value
            , Account_Vendor_Type_ID = ven.VENDOR_TYPE_ID
            , Account_Not_Expected_Dt = i.NOT_EXPECTED_DATE
            , Last_Change_TS = GETDATE()
            , Account_Analyst_Mapping_Cd = i.Analyst_Mapping_Cd
            , Account_Is_Broker = i.Is_Broker_Account
            , Template_Cu_Invoice_Id = i.Template_Cu_Invoice_Id
            , Template_Cu_Invoice_Updated_User_Id = i.Template_Cu_Invoice_Updated_User_Id
            , Template_Cu_Invoice_Last_Change_Ts = i.Template_Cu_Invoice_Last_Change_Ts
            , Is_Invoice_Posting_Blocked = i.Is_Invoice_Posting_Blocked
            , Alternate_Account_Number = i.Alternate_Account_Number
            , cha.Display_Account_Number = i.ACCOUNT_NUMBER
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                ON i.ACCOUNT_ID = cha.Account_Id
            INNER JOIN DELETED d
                ON i.ACCOUNT_ID = d.ACCOUNT_ID
            INNER JOIN dbo.VENDOR ven
                ON ven.VENDOR_ID = i.VENDOR_ID
            LEFT JOIN dbo.Code cd
                ON cd.Code_Id = i.Supplier_Account_Recalc_Type_Cd
        WHERE
            HASHBYTES('SHA1', (   SELECT
                                        i.ACCOUNT_TYPE_ID
                                        , i.ACCOUNT_NUMBER
                                        , i.INVOICE_SOURCE_TYPE_ID
                                        , i.Is_Data_Entry_Only
                                        , i.NOT_EXPECTED
                                        , i.NOT_MANAGED
                                        , i.SERVICE_LEVEL_TYPE_ID
                                        , i.VENDOR_ID
                                        , i.ELIGIBILITY_DATE
                                        , i.ACCOUNT_GROUP_ID
                                        , i.Supplier_Account_Recalc_Type_Cd
                                        , i.NOT_EXPECTED_DATE
                                        , i.Analyst_Mapping_Cd
                                        , i.Is_Broker_Account
                                        , i.Template_Cu_Invoice_Id
                                        , i.Template_Cu_Invoice_Updated_User_Id
                                        , i.Template_Cu_Invoice_Last_Change_Ts
                                        , i.Is_Invoice_Posting_Blocked
                                        , i.Alternate_Account_Number
                                  FOR XML RAW)) != HASHBYTES('SHA1', (   SELECT
                                                                                d.ACCOUNT_TYPE_ID
                                                                                , d.ACCOUNT_NUMBER
                                                                                , d.INVOICE_SOURCE_TYPE_ID
                                                                                , d.Is_Data_Entry_Only
                                                                                , d.NOT_EXPECTED
                                                                                , d.NOT_MANAGED
                                                                                , d.SERVICE_LEVEL_TYPE_ID
                                                                                , d.VENDOR_ID
                                                                                , d.ELIGIBILITY_DATE
                                                                                , d.ACCOUNT_GROUP_ID
                                                                                , d.Supplier_Account_Recalc_Type_Cd
                                                                                , d.NOT_EXPECTED_DATE
                                                                                , d.Analyst_Mapping_Cd
                                                                                , d.Is_Broker_Account
                                                                                , d.Template_Cu_Invoice_Id
                                                                                , d.Template_Cu_Invoice_Updated_User_Id
                                                                                , d.Template_Cu_Invoice_Last_Change_Ts
                                                                                , d.Is_Invoice_Posting_Blocked
                                                                                , d.Alternate_Account_Number
                                                                         FOR XML RAW));

        --To update the Client_hier_Id in case the account move from one site to another site.            
        UPDATE
            cha
        SET
            cha.Client_Hier_Id = ch.Client_Hier_Id
            , cha.Last_Change_TS = GETDATE()
        FROM
            INSERTED i
            INNER JOIN DELETED d
                ON i.ACCOUNT_ID = d.ACCOUNT_ID
            INNER JOIN dbo.METER m
                ON m.ACCOUNT_ID = i.ACCOUNT_ID
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Meter_Id = m.METER_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = i.SITE_ID
        WHERE
            i.SITE_ID <> d.SITE_ID;

        WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + CASE WHEN supact.Supplier_Account_End_Dt IS NULL THEN 'Unspecified'
                                    WHEN MAX(ISNULL(CHA.Supplier_Meter_Disassociation_Date, CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                        CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)), 101)
                                END + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN INSERTED I
                        ON CHA.Account_Id = I.ACCOUNT_ID
                    JOIN dbo.Supplier_Account_Config supact
                        ON supact.Account_Id = I.ACCOUNT_ID
                    JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                        ON tsaci.Account_Id = supact.Account_Id
               WHERE
                    tsaci.Total_Supplier_Accoount_Config_Ids = 1
                    AND supact.Created_Ts <= @Legacy_Account_Dt
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
                   , supact.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;




        UPDATE
            CHA
        SET
            CHA.Display_Account_Number = CHA.Account_Number
            , CHA.Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            INNER JOIN Inserted I
                ON I.ACCOUNT_ID = CHA.Account_Id
            INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                ON tsaci.Account_Id = I.ACCOUNT_ID
        WHERE
            tsaci.Total_Supplier_Accoount_Config_Ids > 1
            AND ISNULL(CHA.Display_Account_Number, '') <> ISNULL(CHA.Account_Number, '');

    END;;
GO
ALTER TABLE [dbo].[ACCOUNT] ADD CONSTRAINT [ACCOUNT_PK] PRIMARY KEY CLUSTERED  ([ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ACCOUNT_N_4] ON [dbo].[ACCOUNT] ([ACCOUNT_GROUP_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Account__Account_Group_ID__Account_ID__Account_Number__ACCOUNT_TYPE_ID] ON [dbo].[ACCOUNT] ([ACCOUNT_GROUP_ID], [ACCOUNT_ID], [ACCOUNT_NUMBER], [ACCOUNT_TYPE_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Account__Supplier_Account_Begin_Dt__Supplier_Account_End_Dt] ON [dbo].[ACCOUNT] ([ACCOUNT_ID], [Supplier_Account_Begin_Dt], [Supplier_Account_End_Dt]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ACCOUNT_N_6] ON [dbo].[ACCOUNT] ([ACCOUNT_NUMBER]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Account__Account_Number_Search__ACCOUNT_ID] ON [dbo].[ACCOUNT] ([Account_Number_Search], [ACCOUNT_ID]) INCLUDE ([SITE_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ACCOUNT_N_1] ON [dbo].[ACCOUNT] ([ACCOUNT_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_ACCOUNT__ACCOUNT_TYPE_ID__NOT_MANAGED__SERVICE_LEVEL_TYPE_ID] ON [dbo].[ACCOUNT] ([ACCOUNT_TYPE_ID], [NOT_MANAGED], [SERVICE_LEVEL_TYPE_ID]) INCLUDE ([ACCOUNT_ID], [ACCOUNT_NUMBER], [SITE_ID], [VENDOR_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_ACCOUNT_Row_Version] ON [dbo].[ACCOUNT] ([Row_Version]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ACCOUNT_N_3] ON [dbo].[ACCOUNT] ([SITE_ID], [ACCOUNT_ID], [SERVICE_LEVEL_TYPE_ID], [VENDOR_ID], [NOT_MANAGED], [ACCOUNT_NUMBER], [RA_WATCH_LIST], [ACCOUNT_GROUP_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX__ACCOUNT__UBM_ID__UBM_ACCOUNT_CODE] ON [dbo].[ACCOUNT] ([UBM_ID], [UBM_ACCOUNT_CODE]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ACCOUNT_N_2] ON [dbo].[ACCOUNT] ([VENDOR_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[ACCOUNT] WITH NOCHECK ADD CONSTRAINT [ACCOUNT_GROUP_ACCOUNT_FK] FOREIGN KEY ([ACCOUNT_GROUP_ID]) REFERENCES [dbo].[ACCOUNT_GROUP] ([ACCOUNT_GROUP_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] WITH NOCHECK ADD CONSTRAINT [ENTITY_ACCOUNT_FK_1] FOREIGN KEY ([ACCOUNT_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] WITH NOCHECK ADD CONSTRAINT [ENTITY_ACCOUNT_FK_2] FOREIGN KEY ([INVOICE_SOURCE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] ADD CONSTRAINT [ENTITY_ACCOUNT_FK_3] FOREIGN KEY ([SERVICE_LEVEL_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] ADD CONSTRAINT [FK_ACCOUNT_UBM] FOREIGN KEY ([UBM_ID]) REFERENCES [dbo].[UBM] ([UBM_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] WITH NOCHECK ADD CONSTRAINT [SITE_ACCOUNT_FK] FOREIGN KEY ([SITE_ID]) REFERENCES [dbo].[SITE] ([SITE_ID])
GO
ALTER TABLE [dbo].[ACCOUNT] WITH NOCHECK ADD CONSTRAINT [VENDOR_ACCOUNT_FK] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the account related information, both utility and supplier accounts', 'SCHEMA', N'dbo', 'TABLE', N'ACCOUNT', NULL, NULL
GO
