CREATE TABLE [dbo].[seContent]
(
[ContentId] [int] NOT NULL IDENTITY(1, 1),
[ContentLocaleId] [int] NOT NULL,
[ContentPlacementId] [int] NOT NULL,
[Version] [int] NOT NULL,
[CurrentStatusId] [int] NOT NULL,
[ContentText] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seContent', NULL, NULL
GO
