CREATE TABLE [dbo].[SR_RFP_SUPPLIER_PRICE_COMMENTS]
(
[SR_RFP_SUPPLIER_PRICE_COMMENTS_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IS_CREDIT_APPROVAL] [bit] NULL,
[NO_CREDIT_COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICE_RESPONSE_COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_F0BAFF90_9F8F_4C9C_A745_09453BDB1E5E_1399076866] DEFAULT (newid()),
[Broker_Included_Type_Id] [int] NULL,
[Pricing_Comments] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Sourcing] TEXTIMAGE_ON [DBData_Sourcing]
GO



EXEC sp_addextendedproperty N'MS_Description', N'This table would store the supplier price and comments section of a bid in a RFP', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_SUPPLIER_PRICE_COMMENTS', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_SUPPLIER_PRICE_COMMENTS] ADD 
CONSTRAINT [PK__SR_RFP_SUPPLIER___2C1560B7] PRIMARY KEY CLUSTERED  ([SR_RFP_SUPPLIER_PRICE_COMMENTS_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
