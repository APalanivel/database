CREATE TABLE [dbo].[SSO_PROJECT_DOCUMENT_MAP]
(
[SSO_PROJECT_ID] [int] NOT NULL,
[SSO_DOCUMENT_ID] [int] NOT NULL
) ON [DBData_SSO]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of Documents in RA to Projects in RA', 'SCHEMA', N'dbo', 'TABLE', N'SSO_PROJECT_DOCUMENT_MAP', NULL, NULL
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   tr_Sso_Project_Document_Map_Transfer

    
DESCRIPTION:     Transfer data to destinations servers.

         
USAGE EXAMPLES:          
------------------------------------------------------------ 
     
AUTHOR INITIALS:          
Initials	NAME          
------------------------------------------------------------          
DSC			Kaushik
     
     
MODIFICATIONS           
Initials	Date			Modification          
------------------------------------------------------------          
DSC			09/25/2014		CREATED 
*****/ 

CREATE TRIGGER [dbo].[tr_Sso_Project_Document_Map_Transfer]
ON [dbo].[SSO_PROJECT_DOCUMENT_MAP]
FOR INSERT, UPDATE, DELETE
AS 
BEGIN

	SET NOCOUNT ON; 
	

-- Sending service broker msg to update sitegroup_site changes in destination servers.
  
  IF SESSION_USER != N'sb_CBMS_Service' 
            BEGIN
  
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER = NULL
           ,@Message XML
		   ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 


                  SET @Message = ( SELECT
										 isnull(i.SSO_PROJECT_ID, d.SSO_PROJECT_ID) AS SSO_PROJECT_ID
										,isnull(i.SSO_DOCUMENT_ID,d.SSO_DOCUMENT_ID) AS SSO_DOCUMENT_ID
										,CASE WHEN i.SSO_PROJECT_ID IS NOT NULL
												  AND d.SSO_PROJECT_ID IS NULL THEN 'I'
											 WHEN i.SSO_PROJECT_ID IS NULL
												  AND d.SSO_PROJECT_ID IS NOT NULL THEN 'D'
										 END AS Op_Code
                                   FROM
                                    Inserted i
                                    FULL JOIN Deleted d
                                          ON d.SSO_PROJECT_ID = i.SSO_PROJECT_ID
										     AND d.SSO_DOCUMENT_ID = i.SSO_DOCUMENT_ID
									FOR
									   XML PATH('SSO_PROJECT_DOCUMENT_MAP_Change')
										  ,ELEMENTS
										  ,ROOT('SSO_PROJECT_DOCUMENT_MAP_Changes') )


               IF @Message IS NOT NULL 
                        BEGIN
	
							
								BEGIN DIALOG CONVERSATION @Conversation_Handle    
								FROM SERVICE [//Change_Control/Service/CBMS/SSO_PROJECT_DOCUMENT_MAP_Transfer]
								TO SERVICE  N'//Change_Control/Service/DV2/SSO_PROJECT_DOCUMENT_MAP_Transfer'
								ON CONTRACT [//Change_Control/Contract/SSO_PROJECT_DOCUMENT_MAP_Transfer]
								;
								SEND ON CONVERSATION @Conversation_Handle    
								MESSAGE TYPE [//Change_Control/Message/SSO_PROJECT_DOCUMENT_MAP_Transfer] (@Message);
						
								END CONVERSATION @Conversation_Handle

                                          
                        END
      
	  END
	



END







;
GO


ALTER TABLE [dbo].[SSO_PROJECT_DOCUMENT_MAP] ADD 
CONSTRAINT [PK_SSO_PROJECT_DOCUMENT_MAP] PRIMARY KEY CLUSTERED  ([SSO_PROJECT_ID], [SSO_DOCUMENT_ID]) WITH (FILLFACTOR=90) ON [DBData_SSO]

GO
