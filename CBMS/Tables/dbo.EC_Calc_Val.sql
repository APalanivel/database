CREATE TABLE [dbo].[EC_Calc_Val]
(
[EC_Calc_Val_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[State_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Calc_Value_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Starting_Period_Cd] [int] NOT NULL,
[Starting_Period_Operator_Cd] [int] NOT NULL,
[Starting_Period_Operand] [smallint] NOT NULL,
[End_Period_Cd] [int] NOT NULL,
[End_Period_Operator_Cd] [int] NOT NULL,
[End_Period_Operand] [smallint] NOT NULL,
[Aggregation_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Calc_Val__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Calc_Val__Last_Change_Ts] DEFAULT (getdate()),
[Ec_Account_Group_Type_Id] [int] NULL,
[Start_Dt_Reference_Ec_Meter_Attribute_Id] [int] NULL,
[Start_Dt_Meter_Attribute_Precedence_Cd] [int] NULL,
[IDM_Commodity_Measurement_Group_Id] [int] NULL,
[Multiple_Option_Value_Selection_Cd] [int] NULL,
[Multiple_Option_No_Of_Month] [int] NULL,
[Multiple_Option_Total_Aggregation_Cd] [int] NULL,
[Monthly_settings_Cd] [int] NULL,
[Monthly_Setting_Start_Month_Num] [int] NULL,
[Monthly_Setting_End_Month_Num] [int] NULL,
[Supplier_Account_Source_Type_Cd] [int] NULL,
[Calc_Val_Type_Cd] [int] NULL,
[Uom_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Calc_Val] ADD CONSTRAINT [pk_EC_Calc_Val] PRIMARY KEY CLUSTERED  ([EC_Calc_Val_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Calc_Val] ADD CONSTRAINT [fk_EC_Calc_Val__Ec_Account_Group_Type_Id] FOREIGN KEY ([Ec_Account_Group_Type_Id]) REFERENCES [dbo].[Ec_Account_Group_Type] ([Ec_Account_Group_Type_Id])
GO
