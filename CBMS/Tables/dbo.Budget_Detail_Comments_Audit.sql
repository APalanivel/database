CREATE TABLE [dbo].[Budget_Detail_Comments_Audit]
(
[Audit_Function] [smallint] NOT NULL,
[Audit_Ts] [datetime] NOT NULL,
[Budget_Detail_Comments_Id] [int] NOT NULL,
[Budget_Account_Id] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to budget_Detail_Coments', 'SCHEMA', N'dbo', 'TABLE', N'Budget_Detail_Comments_Audit', NULL, NULL
GO
