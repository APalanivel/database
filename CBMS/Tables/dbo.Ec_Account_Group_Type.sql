CREATE TABLE [dbo].[Ec_Account_Group_Type]
(
[Ec_Account_Group_Type_Id] [int] NOT NULL IDENTITY(1, 1),
[State_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Group_Type_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Ec_Account_Group_Type__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Account_Group_Type] ADD CONSTRAINT [pk_Ec_Account_Group_Type] PRIMARY KEY CLUSTERED  ([Ec_Account_Group_Type_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Account_Group_Type] ADD CONSTRAINT [uix_Ec_Account_Group_Type__State_Id__Commodity_Id__Group_Type_Name] UNIQUE NONCLUSTERED  ([State_Id], [Commodity_Id], [Group_Type_Name]) ON [DB_DATA01]
GO
