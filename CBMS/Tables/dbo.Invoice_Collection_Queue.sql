CREATE TABLE [dbo].[Invoice_Collection_Queue]
(
[Invoice_Collection_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Invoice_Collection_Queue_Type_Cd] [int] NOT NULL,
[Invoice_Collection_Exception_Type_Cd] [int] NULL,
[Collection_Start_Dt] [date] NOT NULL,
[Collection_End_Dt] [date] NOT NULL,
[Is_Manual] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Is_Manual] DEFAULT ((0)),
[Invoice_Request_Type_Cd] [int] NULL,
[Status_Cd] [int] NOT NULL,
[Is_Invoice_Collection_Start_Dt_Changed] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Invoice_Collection_Start_Dt_Changed] DEFAULT ((0)),
[Is_Invoice_Collection_End_Dt_Changed] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Invoice_Collection_End_Dt_Changed] DEFAULT ((0)),
[Is_Locked] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Is_Locked] DEFAULT ((0)),
[Received_Status_Updated_Dt] [date] NULL,
[Invoice_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Last_Change_Ts] DEFAULT (getdate()),
[Download_Attempt_Cnt] [int] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Download_Attempt_Cnt] DEFAULT ((0)),
[Next_Action_Dt] [date] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Next_Action_Dt] DEFAULT ('1900-01-01'),
[Is_Not_Default_Vendor] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue__Is_Not_Default_Vendor] DEFAULT ((0)),
[Manual_ICR_Vendor_Id] [int] NULL,
[Last_BOT_Attempt_Date] [date] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Queue] ADD CONSTRAINT [pk_Invoice_Collection_Queue] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [IX_Invoice_Collection_Queue__Invoice_Collection_Account_Config_Id__Invoice_Collection_Queue_Id] ON [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Account_Config_Id], [Invoice_Collection_Queue_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Invoice_Collection_Queue__Invoice_Collection_Queue_Type_Cd__Include] ON [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Queue_Type_Cd]) INCLUDE ([Collection_End_Dt], [Collection_Start_Dt], [Invoice_Collection_Account_Config_Id], [Invoice_Collection_Exception_Type_Cd], [Invoice_Collection_Queue_Id], [Invoice_Request_Type_Cd], [Is_Invoice_Collection_End_Dt_Changed], [Is_Invoice_Collection_Start_Dt_Changed], [Is_Manual], [Status_Cd]) ON [DB_INDEXES01]
GO
