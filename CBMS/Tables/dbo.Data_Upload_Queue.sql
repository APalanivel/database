CREATE TABLE [dbo].[Data_Upload_Queue]
(
[Data_Upload_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Data_Upload_Request_Type_Id] [int] NOT NULL,
[Requested_User_Info_Id] [int] NOT NULL,
[Cbms_Image_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Create_Ts] [datetime] NOT NULL CONSTRAINT [DF__Data_Uplo__Creat__18B80BBD] DEFAULT (getdate()),
[Last_Update_Ts] [datetime] NULL,
[SOURCE_DSC] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Status of  Data Uploads for Budget, Cost Usage & Nymex Forecast in CBMS only', 'SCHEMA', N'dbo', 'TABLE', N'Data_Upload_Queue', NULL, NULL
GO

ALTER TABLE [dbo].[Data_Upload_Queue] ADD 
CONSTRAINT [pk_Data_Upload_Queue] PRIMARY KEY CLUSTERED  ([Data_Upload_Queue_Id]) ON [PRIMARY]
ALTER TABLE [dbo].[Data_Upload_Queue] ADD
CONSTRAINT [fk_Data_Upload_Request_Type__Data_Upload_Queue] FOREIGN KEY ([Data_Upload_Request_Type_Id]) REFERENCES [dbo].[Data_Upload_Request_Type] ([Data_Upload_Request_Type_Id])
GO
