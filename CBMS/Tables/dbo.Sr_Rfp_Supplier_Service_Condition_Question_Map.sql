CREATE TABLE [dbo].[Sr_Rfp_Supplier_Service_Condition_Question_Map]
(
[Sr_Rfp_Supplier_Service_Condition_Question_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Rfp_Service_Condition_Template_Question_Map_Id] [int] NOT NULL,
[Sr_Service_Condition_Category_Id] [int] NULL,
[Category_Display_Seq] [int] NULL,
[Sr_Service_Condition_Question_Id] [int] NULL,
[Question_Display_Seq] [int] NULL,
[Posted_User_Id] [int] NOT NULL,
[Posted_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Supplier_Service_Condition_Question_Map__Posted_Ts] DEFAULT (getdate()),
[Sr_Rfp_Bid_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Supplier_Service_Condition_Question_Map] ADD CONSTRAINT [pk_Sr_Rfp_Supplier_Service_Condition_Question_Map] PRIMARY KEY CLUSTERED  ([Sr_Rfp_Supplier_Service_Condition_Question_Map_Id]) ON [DB_DATA01]
GO
