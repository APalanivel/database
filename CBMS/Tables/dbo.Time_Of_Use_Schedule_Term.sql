CREATE TABLE [dbo].[Time_Of_Use_Schedule_Term]
(
[Time_Of_Use_Schedule_Term_Id] [int] NOT NULL IDENTITY(1, 1),
[Start_Dt] [datetime] NOT NULL,
[End_Dt] [datetime] NOT NULL CONSTRAINT [df_Time_Of_Use_Schedule_Term__End_Dt] DEFAULT ('2099-12-31'),
[Time_Of_Use_Schedule_Id] [int] NOT NULL,
[CBMS_IMAGE_ID] [int] NULL,
[Base_Time_Of_Use_Schedule_Term_Id] [int] NULL
) ON [DBData_Contract_RateLibrary]
ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term] WITH NOCHECK ADD
CONSTRAINT [fk_CBMS_IMAGE__Time_Of_Use_Schedule_Term] FOREIGN KEY ([CBMS_IMAGE_ID]) REFERENCES [dbo].[cbms_image] ([CBMS_IMAGE_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of terms to TOU Rates', 'SCHEMA', N'dbo', 'TABLE', N'Time_Of_Use_Schedule_Term', NULL, NULL
GO

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term] ADD 
CONSTRAINT [pk_Time_Of_Use_Schedule_Term] PRIMARY KEY CLUSTERED  ([Time_Of_Use_Schedule_Term_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]
CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term__Time_Of_Use_Schedule_Id] ON [dbo].[Time_Of_Use_Schedule_Term] ([Time_Of_Use_Schedule_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term__CBMS_IMAGE_ID] ON [dbo].[Time_Of_Use_Schedule_Term] ([CBMS_IMAGE_ID]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]


ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term] ADD
CONSTRAINT [fk_Time_Of_Use_Schedule__Time_Of_Use_Schedule_Term] FOREIGN KEY ([Time_Of_Use_Schedule_Id]) REFERENCES [dbo].[Time_Of_Use_Schedule] ([Time_Of_Use_Schedule_Id])
GO
