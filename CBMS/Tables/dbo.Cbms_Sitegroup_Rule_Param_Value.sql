CREATE TABLE [dbo].[Cbms_Sitegroup_Rule_Param_Value]
(
[CBMS_Sitegroup_Rule_Id] [int] NOT NULL,
[Cbms_Filter_Param_Id] [int] NOT NULL,
[Param_Value] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Rule_Param_Value] ADD CONSTRAINT [pk_Cbms_Sitegroup_Rule_Param_Value] PRIMARY KEY CLUSTERED  ([CBMS_Sitegroup_Rule_Id], [Cbms_Filter_Param_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Rule_Param_Value] ADD CONSTRAINT [fk_Cbms_Filter_Param__Cbms_Sitegroup_Rule_Param_Value] FOREIGN KEY ([Cbms_Filter_Param_Id]) REFERENCES [dbo].[Cbms_Filter_Param] ([Cbms_Filter_Param_Id])
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Rule_Param_Value] ADD CONSTRAINT [fk_CBMS_Sitegroup_Rule__Cbms_Sitegroup_Rule_Param_Value] FOREIGN KEY ([CBMS_Sitegroup_Rule_Id]) REFERENCES [dbo].[CBMS_Sitegroup_Rule] ([CBMS_Sitegroup_Rule_Id])
GO
