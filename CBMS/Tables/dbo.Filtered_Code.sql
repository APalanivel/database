CREATE TABLE [dbo].[Filtered_Code]
(
[Codeset_Filtered_Cd] [int] NOT NULL,
[Filtered_Code_Id] [int] NOT NULL,
[Display_Seq] [int] NOT NULL,
[Is_Default] [bit] NULL
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', N'RA modules and values from the code table for the filter dropdowns/display order', 'SCHEMA', N'dbo', 'TABLE', N'Filtered_Code', NULL, NULL
GO

ALTER TABLE [dbo].[Filtered_Code] ADD 
CONSTRAINT [PK_Filtered_Code] PRIMARY KEY CLUSTERED  ([Codeset_Filtered_Cd], [Filtered_Code_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
