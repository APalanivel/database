CREATE TABLE [dbo].[Invoice_Collection_Chase_Log]
(
[Invoice_Collection_Chase_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Activity_Id] [int] NOT NULL,
[Invoice_Collection_Method_Of_Contact_Cd] [int] NOT NULL,
[Contact_Info_Id] [int] NULL,
[Contact_Name] [nvarchar] (121) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Address] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Chase_Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Chase_Log__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Chase_Log__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Chase_Log] ADD CONSTRAINT [pk_Invoice_Collection_Chase_Log] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Chase_Log_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Chase_Log__Contact_Info_Id] ON [dbo].[Invoice_Collection_Chase_Log] ([Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Chase_Log__Invoice_Collection_Activity_Id__Invoice_Collection_Method_Of_Contact_Cd__Contact_Info_Id] ON [dbo].[Invoice_Collection_Chase_Log] ([Invoice_Collection_Activity_Id], [Invoice_Collection_Method_Of_Contact_Cd], [Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Chase_Log] ADD CONSTRAINT [fk_Invoice_Collection_Activity__Invoice_Collection_Chase_Log] FOREIGN KEY ([Invoice_Collection_Activity_Id]) REFERENCES [dbo].[Invoice_Collection_Activity] ([Invoice_Collection_Activity_Id])
GO
