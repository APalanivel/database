CREATE TABLE [dbo].[Client_Exception_Analyst_Map]
(
[Client_Id] [int] NOT NULL,
[Exception_Type_Cd] [int] NOT NULL,
[Analyst_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Exception_Analyst_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Exception_Analyst_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Exception_Analyst_Map] ADD CONSTRAINT [Pk_Client_Exception_Analyst_Map__Client_Id_Exception_Type_Cd] PRIMARY KEY CLUSTERED  ([Client_Id], [Exception_Type_Cd]) ON [DB_DATA01]
GO
