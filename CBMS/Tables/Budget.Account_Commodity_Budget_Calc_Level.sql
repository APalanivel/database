CREATE TABLE [Budget].[Account_Commodity_Budget_Calc_Level]
(
[Account_Commodity_Budget_Calc_Level_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Budget_Calc_Level_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Budget_Calc_Level__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Budget_Calc_Level__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Budget_Calc_Level] ADD CONSTRAINT [pk_Account_Commodity_Budget_Calc_Level] PRIMARY KEY CLUSTERED  ([Account_Commodity_Budget_Calc_Level_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Budget_Calc_Level] ADD CONSTRAINT [un_Account_Commodity_Budget_Calc_Level__Account_Id__Commodity_Id] UNIQUE NONCLUSTERED  ([Account_Id], [Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Budget].[Account_Commodity_Budget_Calc_Level] ADD CONSTRAINT [fk__Budget.Account_Commodity_Budget_Calc_Level__Account_Id] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
