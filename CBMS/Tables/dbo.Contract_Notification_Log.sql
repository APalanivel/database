CREATE TABLE [dbo].[Contract_Notification_Log]
(
[Contract_Notification_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[Meter_Term_Dt] [date] NOT NULL,
[Notification_Msg_Queue_Id] [int] NOT NULL,
[Is_On_Demand] [bit] NOT NULL CONSTRAINT [df_Contract_Notification_Log__Is_On_Demand] DEFAULT ((0)),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Notification_Log__Last_Change_Ts] DEFAULT (getdate()),
[Advance_Notofication_Days] [int] NOT NULL CONSTRAINT [df__Contract_Notification_Log__Advance_Notofication_Days] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log] ADD CONSTRAINT [pk_Contract_Notification_Log] PRIMARY KEY CLUSTERED  ([Contract_Notification_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log] ADD CONSTRAINT [un_Contract_Notification_Log__Contract_Id__Meter_Term_Dt__Notification_Msg_Queue_Id] UNIQUE NONCLUSTERED  ([Contract_Id], [Meter_Term_Dt], [Notification_Msg_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Notification_Log] ADD CONSTRAINT [fk_CONTRACT__Contract_Notification_Log] FOREIGN KEY ([Contract_Id]) REFERENCES [dbo].[CONTRACT] ([CONTRACT_ID])
GO
