CREATE TABLE [dbo].[Invoice_Collection_Batch]
(
[Invoice_Collection_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Batch_Type_Cd] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Batch__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Batch__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Batch] ADD CONSTRAINT [pk_Invoice_Collection_Batch] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Batch_Id]) ON [DB_DATA01]
GO
