CREATE TABLE [dbo].[Division_Dtl]
(
[SBA_TYPE_ID] [int] NULL,
[CLIENT_ID] [int] NOT NULL,
[TERM_PREFERRED_TYPE_ID] [int] NULL,
[CONTRACT_REVIEWER_TYPE_ID] [int] NULL,
[DECISION_MAKER_TYPE_ID] [int] NULL,
[PRICE_INDEX_ID] [int] NULL,
[SIGNATORY_TYPE_ID] [int] NULL,
[IS_INTEREST_MINORITY_SUPPLIERS] [bit] NULL,
[IS_CORPORATE_HEDGE] [bit] NULL,
[NAICS_CODE] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DUNS_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_CORPORATE_DIVISION] [bit] NULL,
[TRIGGER_RIGHTS] [bit] NULL CONSTRAINT [Set_To_Zero8] DEFAULT ((0)),
[MISC_COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_HISTORY] [bit] NULL,
[NOT_MANAGED] [bit] NOT NULL CONSTRAINT [DF_DIVISION_NOT_MANAGED] DEFAULT ((0)),
[NOT_MANAGED_BY_ID] [int] NULL,
[NOT_MANAGED_DATE] [datetime] NULL,
[CONTRACTING_ENTITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_LEGAL_STRUCTURE] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CBMS_IMAGE_ID] [int] NULL,
[Row_Version] [timestamp] NULL,
[SiteGroup_Id] [int] NOT NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_95CB8938_3079_46BD_A026_75C701941C17_1460004778] DEFAULT (newid())
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Divsion_Dtl_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Division_Not_Managed columns get update of Division table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT NOT_MANAGED FROM division_dtl WHERE SiteGroup_id = 1860
		SELECT DIVISION_NOT_MANAGED FROM core.client_hier WHERE sitegroup_id = 1860 AND site_id = 0
		
		UPDATE division_dtl
		SET NOT_MANAGED = 0
		WHERE SiteGroup_id = 1860
		
		SELECT NOT_MANAGED FROM division_dtl WHERE SiteGroup_id = 1860
		SELECT DIVISION_NOT_MANAGED FROM core.client_hier WHERE sitegroup_id = 1860 AND site_id = 0
		
	ROLLBACK TRAN
	
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri
	
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/31/2010  Added the Last_Change_TS column for update
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
	
******/

CREATE TRIGGER [dbo].[tr_Divsion_Dtl_upd_Client_Hier] ON [dbo].[Division_Dtl]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON;
	
      UPDATE
            Core.Client_Hier
      SET   
            Division_Not_Managed = i.NOT_MANAGED
           ,Last_Change_TS = GETDATE()
      FROM
            INSERTED i
            INNER JOIN Core.Client_Hier ch
                  ON i.Sitegroup_Id = ch.Sitegroup_Id
            INNER JOIN DELETED d
                  ON i.Sitegroup_id = d.Sitegroup_Id
      WHERE
            HASHBYTES('SHA1', ( SELECT i.NOT_MANAGED
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT d.NOT_MANAGED
                                                        FOR
                                                                  XML RAW ))
END
;
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [pk_Divsion_Dtl] PRIMARY KEY NONCLUSTERED  ([SiteGroup_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Division_Dtl_Client_Id] ON [dbo].[Division_Dtl] ([CLIENT_ID]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_division_dtl__CLIENT_ID__SiteGroup_Id] ON [dbo].[Division_Dtl] ([CLIENT_ID], [SiteGroup_Id]) INCLUDE ([NOT_MANAGED]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_3] ON [dbo].[Division_Dtl] ([CONTRACT_REVIEWER_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_4] ON [dbo].[Division_Dtl] ([DECISION_MAKER_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_6] ON [dbo].[Division_Dtl] ([PRICE_INDEX_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_DIVISION_Row_Version] ON [dbo].[Division_Dtl] ([Row_Version]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_7] ON [dbo].[Division_Dtl] ([SBA_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_5] ON [dbo].[Division_Dtl] ([SIGNATORY_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE CLUSTERED INDEX [CIX_Division_Dtl__SiteGroup_ID] ON [dbo].[Division_Dtl] ([SiteGroup_Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [DIVISION_N_2] ON [dbo].[Division_Dtl] ([TERM_PREFERRED_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [CLIENT_DIVISION_FK] FOREIGN KEY ([CLIENT_ID]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [ENTITY_DIVISION_FK_1] FOREIGN KEY ([TERM_PREFERRED_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [ENTITY_DIVISION_FK_2] FOREIGN KEY ([CONTRACT_REVIEWER_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [ENTITY_DIVISION_FK_3] FOREIGN KEY ([DECISION_MAKER_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [ENTITY_DIVISION_FK_4] FOREIGN KEY ([SIGNATORY_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [ENTITY_DIVISION_FK_5] FOREIGN KEY ([SBA_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
ALTER TABLE [dbo].[Division_Dtl] ADD CONSTRAINT [R_301] FOREIGN KEY ([PRICE_INDEX_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Detail information about Sitegroups with a Type of Division', 'SCHEMA', N'dbo', 'TABLE', N'Division_Dtl', NULL, NULL
GO
