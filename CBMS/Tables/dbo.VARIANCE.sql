CREATE TABLE [dbo].[VARIANCE]
(
[VARIANCE_ID] [int] NOT NULL IDENTITY(1, 1),
[VARIANCE_TYPE_ID] [int] NOT NULL,
[MAXIMUM_TOLERANCE] [int] NOT NULL,
[MINIMUM_TOLERANCE] [int] NOT NULL,
[IS_ENABLED] [bit] NULL CONSTRAINT [Set_To_Zero28] DEFAULT ((0))
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated.  Used prior to implementation of Variance_Log', 'SCHEMA', N'dbo', 'TABLE', N'VARIANCE', NULL, NULL
GO

ALTER TABLE [dbo].[VARIANCE] ADD 
CONSTRAINT [VARIANCE_PK] PRIMARY KEY CLUSTERED  ([VARIANCE_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [VARIANCE_N_1] ON [dbo].[VARIANCE] ([VARIANCE_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

ALTER TABLE [dbo].[VARIANCE] ADD
CONSTRAINT [ENTITY_VARIANCE_FK] FOREIGN KEY ([VARIANCE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])





GO
