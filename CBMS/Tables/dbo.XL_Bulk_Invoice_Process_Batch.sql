CREATE TABLE [dbo].[XL_Bulk_Invoice_Process_Batch]
(
[XL_Bulk_Invoice_Process_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[XL_Bulk_Data_Process_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Requested_User_Id] [int] NOT NULL,
[Requested_Ts] [datetime] NOT NULL CONSTRAINT [df_XL_Bulk_Invoice_Process_Batch__Requested_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL,
[Processing_Step] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[XL_Bulk_Invoice_Process_Batch] ADD CONSTRAINT [pk_XL_Bulk_Invoice_Process_Batch] PRIMARY KEY CLUSTERED  ([XL_Bulk_Invoice_Process_Batch_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_XL_Bulk_Invoice_Process_Batch__XL_Bulk_Data_Process_Id] ON [dbo].[XL_Bulk_Invoice_Process_Batch] ([XL_Bulk_Data_Process_Id]) ON [DB_DATA01]
GO
