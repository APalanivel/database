CREATE TABLE [dbo].[Variance_Resut_Exception]
(
[Variance_Resut_Exception_id] [int] NOT NULL IDENTITY(1, 1),
[Commodity_id] [int] NULL,
[Country_id] [int] NULL,
[Category_id] [int] NULL,
[Client_id] [int] NULL,
[Test_Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime2] NULL,
[Updated_user_id] [int] NULL,
[Last_Change_TS] [datetime2] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Resut_Exception] ADD CONSTRAINT [PK__Variance__A91747710A555948] PRIMARY KEY CLUSTERED  ([Variance_Resut_Exception_id]) ON [DB_DATA01]
GO
