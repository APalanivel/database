CREATE TABLE [dbo].[Invoice_Collection_Client_Config]
(
[Invoice_Collection_Client_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Client_Config__Is_Active] DEFAULT ((1)),
[Invoice_Collection_Service_Start_Dt] [date] NOT NULL,
[Invoice_Collection_Service_End_Dt] [date] NOT NULL,
[Invoice_Collection_Officer_User_Id] [int] NOT NULL,
[Invoice_Collection_Coordinator_User_Id] [int] NULL,
[Invoice_Collection_Service_Level_Cd] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Client_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Client_Config__Last_Change_Ts] DEFAULT (getdate()),
[Invoice_Collection_Setup_Instruction_Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Client_Config] ADD CONSTRAINT [pk_Invoice_Collection_Client_Config] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Client_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Client_Config] ADD CONSTRAINT [un_Invoice_Collection_Client_Config__Client_Id__Is_Active] UNIQUE NONCLUSTERED  ([Client_Id], [Is_Active]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Client_Config__Invoice_Collection_Coordinator_User_Id] ON [dbo].[Invoice_Collection_Client_Config] ([Invoice_Collection_Coordinator_User_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Client_Config__Invoice_Collection_Officer_User_Id] ON [dbo].[Invoice_Collection_Client_Config] ([Invoice_Collection_Officer_User_Id]) ON [DB_DATA01]
GO
