CREATE TABLE [dbo].[UBM]
(
[UBM_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[UBM_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Generic] [bit] NULL
) ON [DBData_UBM]
GO
EXEC sp_addextendedproperty N'MS_Description', N'A bootstrap data table to store the UBMs (Utility Bill Managers)', 'SCHEMA', N'dbo', 'TABLE', N'UBM', NULL, NULL
GO

ALTER TABLE [dbo].[UBM] ADD 
CONSTRAINT [UBM_PK] PRIMARY KEY CLUSTERED  ([UBM_ID]) WITH (FILLFACTOR=90) ON [DBData_UBM]
CREATE UNIQUE NONCLUSTERED INDEX [UBM_U_1] ON [dbo].[UBM] ([UBM_NAME]) WITH (FILLFACTOR=90) ON [DBData_UBM]






GO
