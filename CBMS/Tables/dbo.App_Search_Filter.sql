CREATE TABLE [dbo].[App_Search_Filter]
(
[App_Search_Filter_Id] [int] NOT NULL IDENTITY(1, 1),
[Filter_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Search_Filter__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Search_Filter__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Search_Filter] ADD CONSTRAINT [pk_App_Search_Filter] PRIMARY KEY CLUSTERED  ([App_Search_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Search_Filter] ADD CONSTRAINT [un_App_Search_Filter__Filter_Name] UNIQUE NONCLUSTERED  ([Filter_Name]) ON [DB_DATA01]
GO
