CREATE TABLE [dbo].[Account_Invoice_Collection_Month]
(
[Account_Invoice_Collection_Month_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Invoice_Collection_Global_Config_Value_Id] [int] NULL,
[Account_Invoice_Collection_Frequency_Id] [int] NULL,
[Seq_No] [smallint] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Month__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Month__Last_Change_Ts] DEFAULT (getdate()),
[Invoice_Frequency_Cd] [int] NOT NULL CONSTRAINT [DF__Account_I__Invoi__76480E36] DEFAULT ((-1)),
[Invoice_Frequency_Pattern_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month] ADD CONSTRAINT [pk_Account_Invoice_Collection_Month] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Month_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Invoice_Collection_Month__Account_Invoice_Collection_Frequency_Id] ON [dbo].[Account_Invoice_Collection_Month] ([Account_Invoice_Collection_Frequency_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month] ADD CONSTRAINT [un_Account_Invoice_Collection_Month] UNIQUE NONCLUSTERED  ([Invoice_Collection_Account_Config_Id], [Service_Month], [Invoice_Collection_Global_Config_Value_Id], [Account_Invoice_Collection_Frequency_Id], [Seq_No]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Invoice_Collection_Month__Invoice_Collection_Global_Config_Value_Id] ON [dbo].[Account_Invoice_Collection_Month] ([Invoice_Collection_Global_Config_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Account_Invoice_Collection_Month] FOREIGN KEY ([Invoice_Collection_Account_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config] ([Invoice_Collection_Account_Config_Id])
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month] ADD CONSTRAINT [fk_Invoice_Collection_Global_Config_Value___Account_Invoice_Collection_Month] FOREIGN KEY ([Invoice_Collection_Global_Config_Value_Id]) REFERENCES [dbo].[Invoice_Collection_Global_Config_Value] ([Invoice_Collection_Global_Config_Value_Id])
GO
