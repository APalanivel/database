CREATE TABLE [dbo].[Budget_Account_Queue]
(
[Budget_Account_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Budget_Account_Id] [int] NOT NULL,
[Analyst_Type_Cd] [int] NOT NULL,
[Queue_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Budget_Account_Queue__Created_Ts] DEFAULT (getdate()),
[Routed_By_User_Id] [int] NULL,
[Routed_Ts] [datetime] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Budget_Account_Queue__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Budget_Account_Queue] ADD CONSTRAINT [pk_Budget_Account_Queue] PRIMARY KEY NONCLUSTERED  ([Budget_Account_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Budget_Account_Queue] ADD CONSTRAINT [unc_Budget_Account_Queue__BUDGET_ACCOUNT_ID__Analyst_Type_Cd] UNIQUE CLUSTERED  ([Budget_Account_Id], [Analyst_Type_Cd]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Budget_Account_Queue__Queue_Id] ON [dbo].[Budget_Account_Queue] ([Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Budget_Account_Queue] ADD CONSTRAINT [fk_BUDGET_ACCOUNT__Budget_Account_Queue] FOREIGN KEY ([Budget_Account_Id]) REFERENCES [dbo].[BUDGET_ACCOUNT] ([BUDGET_ACCOUNT_ID])
GO
