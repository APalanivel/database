CREATE TABLE [dbo].[Account_Invoice_Collection_Source]
(
[Account_Invoice_Collection_Source_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Invoice_Collection_Source_Cd] [int] NULL,
[Invoice_Source_Type_Cd] [int] NULL,
[Invoice_Source_Method_of_Contact_Cd] [int] NULL,
[Is_Primary] [bit] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Source__Is_Primary] DEFAULT ((0)),
[Collection_Instruction] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Source__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Source__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Source] ADD CONSTRAINT [pk_Account_Invoice_Collection_Source] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Source_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Account_Invoice_Collection_Source__Invoice_Collection_Account_Config_Id_Invoice_Collection_Source_Cd_Invoice_Source_Type_Cd] ON [dbo].[Account_Invoice_Collection_Source] ([Invoice_Collection_Account_Config_Id], [Invoice_Collection_Source_Cd], [Invoice_Source_Type_Cd]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Source] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Account_Invoice_Collection_Source] FOREIGN KEY ([Invoice_Collection_Account_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config] ([Invoice_Collection_Account_Config_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'In invoice collection process the type of Invoice Source is captured in this table', 'SCHEMA', N'dbo', 'TABLE', N'Account_Invoice_Collection_Source', NULL, NULL
GO
