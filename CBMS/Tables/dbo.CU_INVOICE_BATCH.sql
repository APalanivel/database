CREATE TABLE [dbo].[CU_INVOICE_BATCH]
(
[CU_INVOICE_BATCH_ID] [int] NOT NULL IDENTITY(1, 1),
[BATCH_DATE] [datetime] NOT NULL,
[INVOICE_COUNT] [int] NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stage 3 Batch job header, including batch start timestamp and number of invoices per batch', 'SCHEMA', N'dbo', 'TABLE', N'CU_INVOICE_BATCH', NULL, NULL
GO

ALTER TABLE [dbo].[CU_INVOICE_BATCH] ADD 
CONSTRAINT [PK_CU_INVOICE_BATCH] PRIMARY KEY CLUSTERED  ([CU_INVOICE_BATCH_ID], [BATCH_DATE]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
