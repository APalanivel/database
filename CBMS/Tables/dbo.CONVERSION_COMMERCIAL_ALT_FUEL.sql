CREATE TABLE [dbo].[CONVERSION_COMMERCIAL_ALT_FUEL]
(
[Service_Month] [date] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Bucket_Value] [numeric] (28, 10) NULL,
[UOM_Type_Id] [int] NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[Created_By_Id] [int] NULL,
[Created_Ts] [datetime] NULL,
[Updated_Ts] [datetime] NULL,
[Updated_By_Id] [int] NULL,
[SITE_ID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'CONVERSION_COMMERCIAL_ALT_FUEL', NULL, NULL
GO
