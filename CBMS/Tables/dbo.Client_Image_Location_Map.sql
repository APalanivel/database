CREATE TABLE [dbo].[Client_Image_Location_Map]
(
[Client_Image_Location_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[CBMS_Image_Location_Id] [int] NOT NULL,
[Client_Id] [int] NOT NULL CONSTRAINT [df_Client_Image_Location_Map__Client_Id] DEFAULT ((-1)),
[Image_Type_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Image_Location_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Image_Location_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Junction table between Client and CBMS_Image.  The physical location where images are stored, can be customer specific.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Image_Location_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Image_Location_Map] ADD CONSTRAINT [PK_Client_Image_Location_Map] PRIMARY KEY CLUSTERED  ([Client_Image_Location_Map_Id]) ON [DB_DATA01]
GO
