CREATE TABLE [Stage].[Cost_Usage_Transfer_Queue]
(
[Cost_Usage_Transfer_Queue_Id] [int] NOT NULL IDENTITY(100001, 1),
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Is_Aggregated_To_Site] [bit] NOT NULL CONSTRAINT [df_Cost_Usage_Transfer_Queue__Is_Aggregated_To_Site] DEFAULT ((0)),
[Is_Variance_Tested] [bit] NOT NULL CONSTRAINT [df_Cost_Usage_Transfer_Queue__Is_Variance_Tested] DEFAULT ((0))
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Queue table to hold the data transferred from DVDEHub which needs Site aggregation or variance testing.', 'SCHEMA', N'Stage', 'TABLE', N'Cost_Usage_Transfer_Queue', NULL, NULL
GO

ALTER TABLE [Stage].[Cost_Usage_Transfer_Queue] ADD CONSTRAINT [pk_Cost_Usage_Transfer_Queue] PRIMARY KEY CLUSTERED  ([Cost_Usage_Transfer_Queue_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
