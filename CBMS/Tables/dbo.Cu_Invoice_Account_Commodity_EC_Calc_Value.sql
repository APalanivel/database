CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value]
(
[Cu_Invoice_Account_Commodity_EC_Calc_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Account_Commodity_Id] [int] NOT NULL,
[Ec_Calc_Val_Id] [int] NOT NULL,
[Calc_Value] [decimal] (28, 10) NULL,
[Uom_Id] [int] NULL,
[Currency_Unit_Id] [int] NULL,
[Is_Value_Locked] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_EC_Calc_Value__Is_Value_Locked] DEFAULT ((0)),
[Comment_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Account_Commodity_EC_Calc_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL,
[Expected_Slots] [int] NULL,
[Actual_Slots] [int] NULL,
[Missing_Slots] [int] NULL,
[Edited_Slots] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value] ADD CONSTRAINT [pk_Cu_Invoice_Account_Commodity_EC_Calc_Value] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Account_Commodity_EC_Calc_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value] ADD CONSTRAINT [uix_Cu_Invoice_Account_Commodity_EC_Calc_Value__Cu_Invoice_Account_Commodity_Id__Ec_Calc_Val_Id] UNIQUE NONCLUSTERED  ([Cu_Invoice_Account_Commodity_Id], [Ec_Calc_Val_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_EC_Calc_Value] ADD CONSTRAINT [fk_Cu_Invoice_Account_Commodity__Cu_Invoice_Account_Commodity_EC_Calc_Value] FOREIGN KEY ([Cu_Invoice_Account_Commodity_Id]) REFERENCES [dbo].[Cu_Invoice_Account_Commodity] ([Cu_Invoice_Account_Commodity_Id])
GO
