CREATE TABLE [dbo].[Data_Quality_Test]
(
[Data_Quality_Test_Id] [int] NOT NULL IDENTITY(1, 1),
[Data_Category_Cd] [int] NOT NULL,
[Test_Description] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Test_Category_Cd] [int] NOT NULL,
[Has_Multiple_Condition] [bit] NULL CONSTRAINT [df_Data_Quality_Test__Has_Multiple_Condition] DEFAULT ((0)),
[Is_Inclusive] [bit] NOT NULL CONSTRAINT [df_Data_Quality_Test__Is_Inclusive] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Data_Quality_Test__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Data_Quality_Test__Last_Change_Ts] DEFAULT (getdate()),
[Apply_Uom_Conversion] [bit] NOT NULL CONSTRAINT [DF__Data_Qual__Apply__5392A637] DEFAULT ((1)),
[Commodity_Id] [int] NULL,
[Execution_Seq_No] [smallint] NULL,
[Is_Active] [bit] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Data_Quality_Test] ADD CONSTRAINT [pk_Data_Quality_Test] PRIMARY KEY CLUSTERED  ([Data_Quality_Test_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Data_Quality_Test] ADD CONSTRAINT [un_Data_Quality_Test__Data_Category_Cd__Test_Description_Commodity_Id] UNIQUE NONCLUSTERED  ([Commodity_Id], [Data_Category_Cd], [Test_Description]) ON [DB_DATA01]
GO
