CREATE TABLE [dbo].[Cost_Usage_Account_Billing_Dtl]
(
[Cost_Usage_Account_Billing_Dtl_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Billing_Start_Dt] [date] NULL,
[Billing_End_Dt] [date] NULL,
[Billing_Days] [int] NOT NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Account to Service Month billing days.  Number of billing days per month may vary for consolidated bills.', 'SCHEMA', N'dbo', 'TABLE', N'Cost_Usage_Account_Billing_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[Cost_Usage_Account_Billing_Dtl] ADD CONSTRAINT [unc_Cost_Usage_Account_Billing_Dtl__Account_Id__Service_Month] UNIQUE CLUSTERED  ([Account_Id], [Service_Month]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]

ALTER TABLE [dbo].[Cost_Usage_Account_Billing_Dtl] ADD CONSTRAINT [pk_Cost_Usage_Account_Billing_Dtl] PRIMARY KEY NONCLUSTERED  ([Cost_Usage_Account_Billing_Dtl_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
ALTER TABLE [dbo].[Cost_Usage_Account_Billing_Dtl] ENABLE CHANGE_TRACKING
GO
