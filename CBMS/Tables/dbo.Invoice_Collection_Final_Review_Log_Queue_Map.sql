CREATE TABLE [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map]
(
[Invoice_Collection_Final_Review_Log_Queue_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Final_Review_Log_Id] [int] NOT NULL,
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Collection_Start_Dt] [date] NOT NULL,
[Collection_End_Dt] [date] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Final_Review_Log_Queue_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map] ADD CONSTRAINT [pk_Invoice_Collection_Final_Review_Log_Queue_Map] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Final_Review_Log_Queue_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map] ADD CONSTRAINT [un_Invoice_Collection_Final_Review_Log_Queue_Map] UNIQUE NONCLUSTERED  ([Invoice_Collection_Final_Review_Log_Id], [Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Final_Review_Log_Queue_Map__Invoice_Collection_Queue_Id] ON [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map] ([Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map] ADD CONSTRAINT [fk_Invoice_Collection_Final_Review_Log__Invoice_Collection_Final_Review_Log_Queue_Map] FOREIGN KEY ([Invoice_Collection_Final_Review_Log_Id]) REFERENCES [dbo].[Invoice_Collection_Final_Review_Log] ([Invoice_Collection_Final_Review_Log_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Final_Review_Log_Queue_Map] ADD CONSTRAINT [fk_Invoice_Collection_Queue__Invoice_Collection_Final_Review_Log_Queue_Map] FOREIGN KEY ([Invoice_Collection_Queue_Id]) REFERENCES [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Queue_Id])
GO
