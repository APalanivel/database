CREATE TABLE [dbo].[Account_Ubm_Account_Code_Map]
(
[Account_Ubm_Account_Code_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Account_Id] [int] NOT NULL,
[Ubm_Id] [int] NOT NULL,
[Ubm_Account_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Ubm_Account_Code_Map__Created_Dt] DEFAULT (getdate()),
[Updated_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Ubm_Account_Code_Map__Updated_Dt] DEFAULT (getdate())
) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[Account_Ubm_Account_Code_Map] ADD CONSTRAINT [Account_Ubm_Account_Code_Map_PK] PRIMARY KEY CLUSTERED  ([Account_Ubm_Account_Code_Map_Id]) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[Account_Ubm_Account_Code_Map] ADD CONSTRAINT [un_Account_Ubm_Account_Code_Map__Ubm_Id__Account_Id] UNIQUE NONCLUSTERED  ([Account_Id], [Ubm_Id]) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[Account_Ubm_Account_Code_Map] ADD CONSTRAINT [un_Account_Ubm_Account_Code_Map__Ubm_Id__Ubm_Account_Code] UNIQUE NONCLUSTERED  ([Ubm_Account_Code], [Ubm_Id]) ON [DBData_CoreClient]
GO
