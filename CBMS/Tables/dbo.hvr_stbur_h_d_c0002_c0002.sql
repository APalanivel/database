CREATE TABLE [dbo].[hvr_stbur_h_d_c0002_c0002]
(
[parallel_session] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__paral__15A48B9D] DEFAULT ((0)),
[is_busy] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__is_bu__1698AFD6] DEFAULT ((0)),
[tbl_name] [varchar] (124) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stbur__tbl_n__178CD40F] DEFAULT (''),
[hvr_op] [int] NOT NULL CONSTRAINT [DF__hvr_stbur__hvr_o__1880F848] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[hvr_stbur_h_d_c0002_c0002] ADD CONSTRAINT [PK__hvr_stbu__ACCE1AB513BC432B] PRIMARY KEY NONCLUSTERED  ([parallel_session]) ON [DB_DATA01]
GO
