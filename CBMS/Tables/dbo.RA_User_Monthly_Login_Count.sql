CREATE TABLE [dbo].[RA_User_Monthly_Login_Count]
(
[USER_INFO_ID] [int] NOT NULL,
[Login_Month] [date] NOT NULL,
[Login_Cnt] [int] NOT NULL
) ON [DB_DATA01]
GO


ALTER TABLE [dbo].[RA_User_Monthly_Login_Count] ADD CONSTRAINT [pk_RA_User_Monthly_Login_Count] PRIMARY KEY CLUSTERED  ([USER_INFO_ID], [Login_Month]) ON [DB_DATA01]
GO
