CREATE TABLE [dbo].[IC_Supllier_Account_Changes_Queue]
(
[IC_Supllier_Account_Changes_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Event_By] [int] NOT NULL,
[Event_Dt] [datetime] NOT NULL,
[IC_Supllier_Account_Changes_Batch_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IC_Supllier_Account_Changes_Queue] ADD CONSTRAINT [pk_IC_Supllier_Account_Changes_Queue] PRIMARY KEY NONCLUSTERED  ([IC_Supllier_Account_Changes_Queue_Id]) ON [DB_DATA01]
GO
