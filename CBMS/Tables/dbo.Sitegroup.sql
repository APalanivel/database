CREATE TABLE [dbo].[Sitegroup]
(
[Sitegroup_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sitegroup_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sitegroup_Type_Cd] [int] NOT NULL,
[Client_Id] [int] NULL,
[Is_Smart_Group] [bit] NOT NULL CONSTRAINT [df_Sitegroup_Is_Smart_Group] DEFAULT ((0)),
[Is_Publish_To_DV] [bit] NOT NULL CONSTRAINT [df_Sitegroup_Is_Publish_To_Dv] DEFAULT ((0)),
[Owner_User_Id] [int] NOT NULL,
[Add_User_Id] [int] NOT NULL,
[Add_Dt] [datetime] NOT NULL CONSTRAINT [df_Sitegroup_Add_Dt] DEFAULT (getdate()),
[Mod_User_Id] [int] NULL,
[Mod_Dt] [datetime] NOT NULL CONSTRAINT [df_Sitegroup_Mod_Dt] DEFAULT (getdate()),
[Is_Complete] [bit] NOT NULL CONSTRAINT [df_Sitegroup_Is_Complete] DEFAULT ((0)),
[Is_Locked] [bit] NOT NULL CONSTRAINT [df_Sitegroup_Is_Locked] DEFAULT ((0)),
[Is_Edited] [bit] NOT NULL CONSTRAINT [df_Sitegroup_Is_Edited] DEFAULT ((0)),
[Last_Smartgroup_Refresh_dt] [datetime] NOT NULL CONSTRAINT [df_Sitegroup_Last_Smartgroup_Refresh_dt] DEFAULT ('01/01/1900'),
[Portfolio_Client_Hier_Reference_Number] [int] NULL
) ON [DBData_CoreClient]


GO

EXEC sp_addextendedproperty N'MS_Description', 'Groups of sites', 'SCHEMA', N'dbo', 'TABLE', N'Sitegroup', NULL, NULL
GO



SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Sitegroup_del_Client_Hier
	
DESCRIPTION:   
	This will delete the record from Core.Clinet_Hier table whenever the entry get deleted from SiteGroup table.
	
 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		INSERT INTO Sitegroup(Sitegroup_Name,Sitegroup_Type_Cd,Client_Id,Is_Smart_Group,Is_Publish_To_DV,Owner_User_Id,Add_User_Id,Add_Dt,Mod_Dt,Is_Complete,Is_Locked,Is_Edited,Last_Smartgroup_Refresh_dt)
		VALUES('TEST-SG',100001,10069,1,0,1,1,GETDATE(),GETDATE(),1,1,1,GETDATE())
		
		SELECT * FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'
		SELECT * FROM CORE.CLIENT_HIER WHERE SITEGROUP_NAME = 'TEST-SG'
		
		DELETE FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'
		
		SELECT * FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'
		SELECT * FROM CORE.CLIENT_HIER WHERE SITEGROUP_NAME = 'TEST-SG'
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created

******/
CREATE TRIGGER [dbo].[tr_Sitegroup_del_Client_Hier] ON [dbo].[Sitegroup]
      FOR DELETE
AS
BEGIN
      SET NOCOUNT ON ; 
      DELETE
            Core.Client_Hier
      FROM
            DELETED d
            INNER JOIN Core.Client_Hier ch ON d.Sitegroup_Id = ch.Sitegroup_Id
END 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 tr_Sitegroup_upd_Client_Hier  
   
DESCRIPTION:     
 This will update the existing record of Core.Clinet_Hier table whenever any update happen in Sitegroup table.  
  
   
 USAGE EXAMPLES:  
------------------------------------------------------------  
   
 BEGIN TRAN   
  INSERT INTO Sitegroup(Sitegroup_Name,Sitegroup_Type_Cd,Client_Id,Is_Smart_Group,Is_Publish_To_DV,Owner_User_Id,Add_User_Id,Add_Dt,Mod_Dt,Is_Complete,Is_Locked,Is_Edited,Last_Smartgroup_Refresh_dt)  
  VALUES('TEST-SG',100001,10069,1,0,1,1,GETDATE(),GETDATE(),1,1,1,GETDATE())  
    
  SELECT Sitegroup_Type_Cd FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'  
  SELECT Sitegroup_Type_Cd FROM CORE.CLIENT_HIER WHERE SITEGROUP_NAME = 'TEST-SG'  
      
  UPDATE Sitegroup   
  SET Sitegroup_Type_Cd = 100003  
  WHERE SITEGROUP_NAME = 'TEST-SG'  
    
  SELECT Sitegroup_Type_Cd FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'  
  SELECT Sitegroup_Type_Cd FROM CORE.CLIENT_HIER WHERE SITEGROUP_NAME = 'TEST-SG'  
 ROLLBACK TRAN  
  
AUTHOR INITIALS:  
 Initials Name  
------------------------------------------------------------  
 SKA  Shobhit Kumar Agrawal  
 BCH  Balaraju Chalumuri

  
 MODIFICATIONS:  
 Initials	Date		Modification  
------------------------------------------------------------
 SKA		08/02/2010	Created
			08/31/2010  Added the Last_Change_TS column for update
 HG			01/05/2011	Sitegroup_Type_Dsc column added to the UPDATE statement
 HG			01/13/2011	Sitegroup_Type_Dsc column renamed to Sitegroup_Type_Name
 HG			10/31/2011	MAINT-868, Added code to insert/update the sitegroup changes in to Client_hier only if the sitegroup save process is complete(Sitegroup.Is_Complete = 0)
						Removed UPDATE statement and added MERGE statement as smart group save process completed only after the Sitegroup.Is_Complete = 1
 BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
 HG			2014-11-11	MAINT-3219, modified the UPDATE section in MERGE statement to update the Sitegroup_owner_user_id in CH table.
 HG			2016-07-18	Modified to the code to use Aliased column names as the xml hashbytes comparison is generating the different hashkey value.
******/
CREATE TRIGGER [dbo].[tr_Sitegroup_upd_Client_Hier] ON [dbo].[Sitegroup]
      FOR UPDATE
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @sgHierCd INT
           ,@divHierCd INT;          
           
      SELECT
            @sgHierCd = Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Site group';          
            
      SELECT
            @divHierCd = Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Division';

      MERGE INTO Core.Client_Hier tgt
      USING
            ( SELECT
                  CASE WHEN sgt.Code_Value = 'Division' THEN @divHierCd
                       ELSE @sgHierCd
                  END AS Hier_level_Cd
                 ,ch.Client_Id
                 ,ch.Client_Name
                 ,ch.Client_Currency_Group_Id
                 ,ch.Client_Not_Managed
                 ,i.Sitegroup_Id
                 ,i.Sitegroup_Name
                 ,0 Division_Not_Managed
                 ,i.Sitegroup_Type_Cd
                 ,sgt.Code_Value AS Sitegroup_Type_Name
                 ,ch.User_Passcode_Expiration_Duration
                 ,ch.User_Max_Failed_Login_Attempts
                 ,ch.User_PassCode_Reuse_Limit
                 ,ch.Client_Fiscal_Offset
                 ,i.Owner_User_Id
                 ,ch.Client_Type_Id
                 ,ch.Report_Frequency_Type_Id
                 ,ch.Fiscalyear_Startmonth_Type_Id
                 ,ch.Ubm_Service_Id
                 ,ch.Dsm_Strategy
                 ,ch.Is_Sep_Issued
                 ,ch.Sep_Issue_Date
              FROM
                  INSERTED i
                  JOIN Core.Client_Hier ch
                        ON i.Client_Id = ch.Client_Id
                  JOIN dbo.Code sgt
                        ON sgt.Code_Id = i.Sitegroup_Type_Cd
              WHERE
                  ch.Sitegroup_Id = 0
                  AND i.Is_Complete = 1 ) src
      ON src.Sitegroup_Id = tgt.Sitegroup_Id
      WHEN NOT MATCHED THEN
            INSERT
                   (Hier_level_Cd
                   ,Client_Id
                   ,Client_Name
                   ,Client_Currency_Group_Id
                   ,Client_Not_Managed
                   ,Sitegroup_Id
                   ,Sitegroup_Name
                   ,Division_Not_Managed
                   ,Sitegroup_Type_cd
                   ,Sitegroup_Type_Name
                   ,User_Passcode_Expiration_Duration
                   ,User_Max_Failed_Login_Attempts
                   ,User_PassCode_Reuse_Limit
                   ,Client_Fiscal_Offset
                   ,Sitegroup_Owner_User_id
                   ,Client_Type_Id
                   ,Report_Frequency_Type_Id
                   ,Fiscalyear_Startmonth_Type_Id
                   ,Ubm_Service_Id
                   ,Dsm_Strategy
                   ,Is_Sep_Issued
                   ,Sep_Issue_Date )
            VALUES (src.Hier_level_Cd
                   ,src.Client_Id
                   ,src.Client_Name
                   ,src.Client_Currency_Group_Id
                   ,src.Client_Not_Managed
                   ,src.Sitegroup_Id
                   ,src.Sitegroup_Name
                   ,src.Division_Not_Managed
                   ,src.Sitegroup_Type_Cd
                   ,src.Sitegroup_Type_Name
                   ,src.User_Passcode_Expiration_Duration
                   ,src.User_Max_Failed_Login_Attempts
                   ,src.User_PassCode_Reuse_Limit
                   ,src.Client_Fiscal_Offset
                   ,src.Owner_User_Id
                   ,src.Client_Type_Id
                   ,src.Report_Frequency_Type_Id
                   ,src.Fiscalyear_Startmonth_Type_Id
                   ,src.Ubm_Service_Id
                   ,src.Dsm_Strategy
                   ,src.Is_Sep_Issued
                   ,src.Sep_Issue_Date )
      WHEN MATCHED AND   HASHBYTES('SHA1', ( SELECT
                                                src.Sitegroup_Name AS Sitegroup_Name
                                               ,src.Sitegroup_Type_Cd AS Sitegroup_Type_Cd
                                               ,src.Owner_User_Id AS Owner_User_Id
                                   FOR
                                             XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                                    tgt.Sitegroup_Name AS Sitegroup_Name
                                                                                   ,tgt.Sitegroup_Type_cd AS Sitegroup_Type_Cd
                                                                                   ,tgt.Sitegroup_Owner_User_id AS Owner_User_Id
                                                                     FOR
                                                                               XML RAW )) THEN
            UPDATE SET
                    tgt.Sitegroup_Name = src.Sitegroup_Name
                   ,tgt.Sitegroup_Type_cd = src.Sitegroup_Type_Cd
                   ,tgt.Sitegroup_Type_Name = src.Sitegroup_Type_Name
                   ,tgt.Sitegroup_Owner_User_id = src.Owner_User_Id
                   ,tgt.Last_Change_TS = GETDATE();
END;
;

;

;
GO
GO
GO
GO

ALTER TABLE [dbo].[Sitegroup] ADD
CONSTRAINT [fk_Sitegroup_Client] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
ALTER TABLE [dbo].[Sitegroup] ADD 
CONSTRAINT [pk_Sitegroup] PRIMARY KEY CLUSTERED  ([Sitegroup_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]ALTER TABLE [dbo].[Sitegroup] ENABLE CHANGE_TRACKING
GO

ALTER TABLE [dbo].[Sitegroup] ADD CONSTRAINT [unc_SiteGroup_Client_Id_Owner_User_id_SiteGroup_Name] UNIQUE NONCLUSTERED  ([Client_Id], [Owner_User_Id], [Sitegroup_Name]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_SiteGroup_SiteGroupTypeCode_ClientID_SitegroupID] ON [dbo].[Sitegroup] ([Sitegroup_Id], [Sitegroup_Type_Cd], [Client_Id]) ON [DB_INDEXES01]





CREATE NONCLUSTERED INDEX [ix_SiteGroup_SiteGroupTypeCode] ON [dbo].[Sitegroup] ([Sitegroup_Type_Cd], [Client_Id]) ON [DB_INDEXES01]



GO

GO

GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:          
 tr_Sitegroup_ins_Client_Hier        
         
DESCRIPTION:           
 This will insert the new record into Core.Client_Hier table when ever the new Sitegroup added into the Sitegroup table.        
 Sitegroup/Division level Hier_level_Cd will be used of Client_Hier table        
         
 USAGE EXAMPLES:        
------------------------------------------------------------        
         
 BEGIN TRAN        
         
  INSERT INTO Sitegroup(Sitegroup_Name,Sitegroup_Type_Cd,Client_Id,Is_Smart_Group,Is_Publish_To_DV,Owner_User_Id,Add_User_Id,Add_Dt,Mod_Dt,Is_Complete,Is_Locked,Is_Edited,Last_Smartgroup_Refresh_dt)        
  VALUES('TEST-SG',100001,10069,1,0,1,1,GETDATE(),GETDATE(),1,1,1,GETDATE())        
          
  SELECT Sitegroup_Type_Cd FROM Sitegroup WHERE SITEGROUP_NAME = 'TEST-SG'        
  SELECT Sitegroup_Type_Cd FROM CORE.CLIENT_HIER WHERE SITEGROUP_NAME = 'TEST-SG'        
         
 ROLLBACK TRAN        
       
AUTHOR INITIALS:        
 Initials	Name        
------------------------------------------------------------        
 SKA		Shobhit Kumar Agrawal        
 PNR		Pandarinath        
 HG			Harihara Suthan G  
 RK			Rajesh Kasoju
 DSC		Kaushik


 MODIFICATIONS:        
 Initials	Date		Modification        
------------------------------------------------------------        
 SKA		08/02/2010	Created        
			09/01/2010	Removed SiteGroup_Change_TS column        
 PNR		10/14/2010	Added client_Hier.User_Passcode_Expiration_Duration,client_Hier.User_Max_Failed_Login_Attempts        
						,client_Hier.User_PassCode_Reuse_Limit in select list ( Project Name : DV_SV_Password Expiration )        
 HG			01/05/2011	Sitegroup_Type_Dsc column added to the INSERT statement        
 HG			01/13/2011	Sitegroup_Type_Dsc column renamed to Sitegroup_Type_Name        
						Script modified to populate column Client_Fiscal_OffSet.        
 RR			09/14/2011	Sitegroup_Owner_User_Id column added to insert statement      
 HG			10/19/2011	MAINT-868 , fixed the code to populate the followings columns      
						   Client_Type_Id      
						   Report_Frequency_Type_Id      
						   Fiscalyear_Startmonth_Type_Id      
						   Ubm_Service_Id      
						   Dsm_Strategy      
						   Is_Sep_Issued      
						   Sep_Issue_Date      
 HG			10/31/2011	MAINT-868 , As discussed in the maintenance change discussion meeting added condition to insert the record in to client_hier only if the sitegroup creation process is complete (Sitegroup.Is_Complete = 1)
 RK         03/09/2012  Added Portfolio_Client_Id column to the list of populated columns.
 HG			2013-04-26	MAINT-1885, modified the script to insert Client_Analyst_Mapping_Cd
 DSC		2014-06-30	Modified to add Portfolio_Client_Hier_Reference_Number
******/
CREATE TRIGGER [dbo].[tr_Sitegroup_ins_Client_Hier] ON [dbo].[Sitegroup]
      FOR INSERT
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE
            @sgHierCd INT
           ,@divHierCd INT

      SELECT
            @sgHierCd = Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Site group'  

      SELECT
            @divHierCd = Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_id = c.Codeset_Id
      WHERE
            cs.std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Division'

      INSERT      Core.Client_Hier
                  ( 
                   Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_managed
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Division_Not_Managed
                  ,Sitegroup_Type_cd
                  ,Sitegroup_Type_Name
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Sitegroup_Owner_User_Id
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Client_Analyst_Mapping_Cd 
				  ,Portfolio_Client_Hier_Reference_Number
				  )
                  SELECT
                        case WHEN sgt.Code_Value = 'Division' THEN @divHierCd
                             ELSE @sgHierCd
                        END AS Hier_level_Cd
                       ,ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Not_managed
                       ,i.Sitegroup_Id
                       ,i.Sitegroup_Name
                       ,0 --div.Not_Managed        
                       ,i.Sitegroup_Type_Cd
                       ,sgt.Code_Value
                       ,ch.User_Passcode_Expiration_Duration
                       ,ch.User_Max_Failed_Login_Attempts
                       ,ch.User_PassCode_Reuse_Limit
                       ,ch.Client_Fiscal_Offset
                       ,i.Owner_User_Id
                       ,ch.Client_Type_Id
                       ,ch.Report_Frequency_Type_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,ch.Ubm_Service_Id
                       ,ch.Dsm_Strategy
                       ,ch.Is_Sep_Issued
                       ,ch.Sep_Issue_Date
                       ,ch.Portfolio_Client_Id
                       ,ch.Client_Analyst_Mapping_Cd
					   ,i.Portfolio_Client_Hier_Reference_Number
                  FROM
                        INSERTED i
                        INNER JOIN Core.Client_Hier ch
                              ON i.Client_Id = ch.Client_Id
                        INNER JOIN dbo.Code sgt
                              ON sgt.Code_id = i.Sitegroup_Type_Cd
                  WHERE
                        ch.Sitegroup_id = 0
                        AND i.Is_Complete = 1

END


;


;
;
GO
