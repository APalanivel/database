CREATE TABLE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response]
(
[Sr_Rfp_Service_Condition_Template_Question_Response_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Rfp_Service_Condition_Template_Question_Map_Id] [int] NOT NULL,
[SR_RFP_BID_ID] [int] NOT NULL,
[Response_Text_Value] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Service_Condition_Template_Question_Response__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Service_Condition_Template_Question_Response__Last_Change_Ts] DEFAULT (getdate()),
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_F6414F8A_002B_42DE_B2EA_127CA0EF86E4_146000700] DEFAULT (newid())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] ADD CONSTRAINT [pk_Sr_Rfp_Service_Condition_Template_Question_Response] PRIMARY KEY CLUSTERED  ([Sr_Rfp_Service_Condition_Template_Question_Response_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Sr_Rfp_Service_Condition_Template_Question_Response__Sr_Rfp_Service_Condition_Template_Question_Map_Id__SR_RFP_BID_ID] ON [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] ([Sr_Rfp_Service_Condition_Template_Question_Map_Id], [SR_RFP_BID_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Response] WITH NOCHECK ADD CONSTRAINT [fk_SR_RFP_BID__Sr_Rfp_Service_Condition_Template_Question_Response] FOREIGN KEY ([SR_RFP_BID_ID]) REFERENCES [dbo].[SR_RFP_BID] ([SR_RFP_BID_ID]) NOT FOR REPLICATION
GO
