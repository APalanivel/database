CREATE TABLE [dbo].[hvr_stinr_h_d_c0002_c0001]
(
[last_integ_end] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__last___63E1DFBD] DEFAULT ('01-jan-1900 00:00:00'),
[trailer_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__trail__64D603F6] DEFAULT (''),
[trailer_cap_begin] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__trail__65CA282F] DEFAULT ('01-jan-1900 00:00:00'),
[trailer_cap_begin_int] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__trail__66BE4C68] DEFAULT ((0)),
[leader_cap_loc] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__leade__67B270A1] DEFAULT (''),
[leader_cap_begin] [datetime] NOT NULL CONSTRAINT [DF__hvr_stinr__leade__68A694DA] DEFAULT ('01-jan-1900 00:00:00'),
[leader_cap_begin_int] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__leade__699AB913] DEFAULT ((0)),
[session_name] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__sessi__6A8EDD4C] DEFAULT (''),
[history_scan_load] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__histo__6B830185] DEFAULT ((0)),
[history_scan_start_tstamp] [int] NOT NULL CONSTRAINT [DF__hvr_stinr__histo__6C7725BE] DEFAULT ((0)),
[history_scan_start_addr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__histo__6D6B49F7] DEFAULT (''),
[history_emit_seq] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_stinr__histo__6E5F6E30] DEFAULT ('')
) ON [DB_DATA01]
GO
GRANT SELECT ON  [dbo].[hvr_stinr_h_d_c0002_c0001] TO [public]
GO
