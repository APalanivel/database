CREATE TABLE [dbo].[CURRENCY_GROUP]
(
[currency_group_id] [int] NOT NULL IDENTITY(1, 1),
[currency_group_name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'groups of currency conversions factors used by clients', 'SCHEMA', N'dbo', 'TABLE', N'CURRENCY_GROUP', NULL, NULL
GO

ALTER TABLE [dbo].[CURRENCY_GROUP] ADD 
CONSTRAINT [PK_CURRENCY_GROUP] PRIMARY KEY CLUSTERED  ([currency_group_id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
