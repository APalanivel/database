CREATE TABLE [dbo].[CBMS_SSO_App]
(
[CBMS_SSO_App_Id] [int] NOT NULL IDENTITY(1, 1),
[App_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[App_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Auth_Type_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_CBMS_SSO_App__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_CBMS_SSO_App__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_SSO_App] ADD CONSTRAINT [pk_CBMS_SSO_App] PRIMARY KEY CLUSTERED  ([CBMS_SSO_App_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_SSO_App] ADD CONSTRAINT [un_CBMS_SSO_App__App_Name] UNIQUE NONCLUSTERED  ([App_Name]) ON [DB_DATA01]
GO
