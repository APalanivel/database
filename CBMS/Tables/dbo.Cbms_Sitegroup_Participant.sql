CREATE TABLE [dbo].[Cbms_Sitegroup_Participant]
(
[Cbms_Sitegroup_Participant_Id] [int] NOT NULL IDENTITY(1, 1),
[Cbms_Sitegroup_Id] [int] NOT NULL,
[Group_Participant_Id] [int] NOT NULL,
[Group_Participant_Type_Cd] [int] NOT NULL,
[Include_New_Contract_Extensions] [bit] NOT NULL CONSTRAINT [DFCbms_Sitegroup_ParticipantInclude_New_Contract_Extensions] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFCbms_Sitegroup_ParticipantCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFCbms_Sitegroup_ParticipantLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Participant] ADD CONSTRAINT [pk_Cbms_Sitegroup_Participant] PRIMARY KEY CLUSTERED  ([Cbms_Sitegroup_Participant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Participant] ADD CONSTRAINT [un_Cbms_Sitegroup_Participant__Cbms_Sitegroup_Id__Group_Participant_Id__Group_Participant_Type_Cd] UNIQUE NONCLUSTERED  ([Cbms_Sitegroup_Id], [Group_Participant_Id], [Group_Participant_Type_Cd]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Cbms_Sitegroup_Participant__Group_Participant_Id] ON [dbo].[Cbms_Sitegroup_Participant] ([Group_Participant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Sitegroup_Participant] ADD CONSTRAINT [fk_Cbms_Sitegroup__Cbms_Sitegroup_Participant] FOREIGN KEY ([Cbms_Sitegroup_Id]) REFERENCES [dbo].[Cbms_Sitegroup] ([Cbms_Sitegroup_Id])
GO
