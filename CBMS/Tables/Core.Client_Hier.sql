CREATE TABLE [Core].[Client_Hier]
(
[Client_Hier_Id] [int] NOT NULL IDENTITY(1, 1),
[Hier_level_Cd] [int] NOT NULL,
[Client_Id] [int] NOT NULL,
[Client_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Currency_Group_Id] [int] NOT NULL,
[Sitegroup_Id] [int] NOT NULL CONSTRAINT [DF__Client_Hi__Siteg__3E065EA1] DEFAULT ((0)),
[Sitegroup_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Client_Hi__Siteg__3EFA82DA] DEFAULT (''),
[Site_Id] [int] NOT NULL CONSTRAINT [DF__Client_Hi__Site___3FEEA713] DEFAULT ((0)),
[Site_name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Client_Hi__Site___40E2CB4C] DEFAULT (''),
[Site_Address_Line1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site_Address_Line2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State_Id] [int] NULL,
[State_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country_Id] [int] NULL,
[Country_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Geo_Lat] [decimal] (32, 16) NULL,
[Geo_Long] [decimal] (32, 16) NULL,
[Site_Not_Managed] [bit] NOT NULL CONSTRAINT [DF__Client_Hi__Site___27EDCBA7] DEFAULT ((0)),
[Site_Closed] [bit] NOT NULL CONSTRAINT [DF__Client_Hi__Site___28E1EFE0] DEFAULT ((0)),
[Site_Closed_Dt] [date] NOT NULL CONSTRAINT [DF__Client_Hi__Site___43BF37F7] DEFAULT ('12/31/2099'),
[Region_ID] [int] NOT NULL CONSTRAINT [DF__Client_Hi__Regio__2E2FE98B] DEFAULT ((0)),
[Region_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Not_Managed] [bit] NOT NULL CONSTRAINT [DF__Client_Hi__Clien__29D61419] DEFAULT ((0)),
[Division_Not_Managed] [bit] NOT NULL CONSTRAINT [DF__Client_Hi__Divis__2ACA3852] DEFAULT ((0)),
[Sitegroup_Type_cd] [int] NULL,
[Sitegroup_Type_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Change_TS] [datetime] NOT NULL CONSTRAINT [DF__Client_Hi__Last___2D333A5F] DEFAULT (getdate()),
[User_Passcode_Expiration_Duration] [smallint] NULL,
[User_Max_Failed_Login_Attempts] [smallint] NULL,
[User_PassCode_Reuse_Limit] [smallint] NULL,
[Client_Fiscal_Offset] [int] NULL,
[Client_Type_Id] [int] NULL,
[Report_Frequency_Type_Id] [int] NULL,
[Fiscalyear_Startmonth_Type_Id] [int] NULL,
[Ubm_Service_Id] [int] NULL,
[Dsm_Strategy] [bit] NULL,
[Is_Sep_Issued] [bit] NULL,
[Sep_Issue_Date] [datetime] NULL,
[Sitegroup_Owner_User_id] [int] NULL,
[Client_Hier_Created_Ts] [datetime] NULL CONSTRAINT [df_Client_Hier__Client_Hier_Created_Ts] DEFAULT (getdate()),
[Portfolio_Client_Id] [int] NULL,
[Site_Type_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Client_Analyst_Mapping_Cd] [int] NOT NULL CONSTRAINT [df_Client_Hier__Client_Analyst_Mapping_Cd] DEFAULT ((0)),
[Site_Analyst_Mapping_Cd] [int] NULL,
[Weather_Station_Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site_Search_Combo] AS ((((((((((isnull([SITE_NAME],'')+' | ')+isnull([Site_ADDRESS_LINE1],''))+' | ')+isnull([City],''))+' | ')+isnull([State_Name],''))+' | ')+isnull([ZIPCODE],''))+' | ')+isnull([COUNTRY_NAME],'')) PERSISTED NOT NULL,
[Site_Reference_Number] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Doing_Business_As] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Portfolio_Client_Hier_Reference_Number] [int] NULL,
[Client_Host_Url] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDM_Node_XId] [int] NULL,
[Is_IDM_Enabled] [int] NULL,
[Client_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Client_Name]),(0))) PERSISTED,
[Site_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Site_Name]),(0))) PERSISTED,
[Sitegroup_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Sitegroup_Name]),(0))) PERSISTED,
[Site_Search_FTSearch] AS (CONVERT([varchar](1045),[dbo].[udf_StripNonAlphaNumerics]((((((((((isnull([SITE_NAME],'')+' | ')+isnull([Site_ADDRESS_LINE1],''))+' | ')+isnull([City],''))+' | ')+isnull([State_Name],''))+' | ')+isnull([ZIPCODE],''))+' | ')+isnull([COUNTRY_NAME],'')),(0))),
[CITY_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([City]),(0))) PERSISTED,
[State_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([State_Name]),(0))) PERSISTED,
[Site_Address_Line1_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Site_Address_Line1]),(0))) PERSISTED,
[Site_Address_Line2_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Site_Address_Line2]),(0))) PERSISTED,
[ZipCode_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([ZipCode]),(0))) PERSISTED,
[Country_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Country_Name]),(0))) PERSISTED,
[Region_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Region_Name]),(0))) PERSISTED,
[Sitegroup_Type_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Sitegroup_Type_Name]),(0))) PERSISTED,
[Site_Type_Name_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Site_Type_Name]),(0))) PERSISTED,
[Site_Reference_Number_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Site_Reference_Number]),(0))) PERSISTED,
[Doing_Business_As_FTSearch] AS (CONVERT([varchar](200),[dbo].[udf_StripNonAlphaNumerics]([Doing_Business_As]),(0))) PERSISTED,
[Time_Zone] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Client_Hier_Change_Control

DESCRIPTION: Trigger on Core.CLient_Hier to send change data to the 
	Change control service. 


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh
	HG			Hariharasuthan Ganesan
	KVK			Vinay K
	DSC			Kaushik

MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	CMH			10/01/2011		Created
	HG			2014-05-27		MAINT-2745, Modified to use new architecture of sending messages with fixed conversation group
								Added Site_Reference_Number and Doing_Business_As columns for MAINT-2798
	KVK			2014-06-30		Modified to populate values to New column Portfolio_Client_Hier_Reference_Number
	KVK			2014-10-15		Added new column Client_Host_Url 
	DSC			2015-01-13		Added new columns Is_IDM_Node_XId_Enabled,IDM_Node_XId
	HG			2017-02-27		Added Time zone column to Client_Hier
******/
CREATE TRIGGER [Core].[tr_Client_Hier_Change_Control] ON [Core].[Client_Hier]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255);

      SET @Message = ( SELECT
                        CASE WHEN ins.Client_Hier_Id IS NOT NULL
                                  AND del.Client_Hier_Id IS NULL THEN 'I'
                             WHEN ins.Client_Hier_Id IS NOT NULL
                                  AND del.Client_Hier_Id IS NOT NULL THEN 'U'
                             WHEN ins.Client_Hier_Id IS NULL
                                  AND del.Client_Hier_Id IS NOT NULL THEN 'D'
                        END AS SYS_CHANGE_OPERATION
                       ,ISNULL(ins.Client_Hier_Id, del.Client_Hier_Id) AS Client_Hier_Id
                       ,ins.Hier_level_Cd
                       ,ins.Client_Id
                       ,ins.Client_Name
                       ,ins.Client_Currency_Group_Id
                       ,ins.Sitegroup_Id
                       ,ins.Sitegroup_Name
                       ,ins.Site_Id
                       ,ins.Site_name
                       ,ins.Site_Address_Line1
                       ,ins.Site_Address_Line2
                       ,ins.City
                       ,ins.ZipCode
                       ,ins.State_Id
                       ,ins.State_Name
                       ,ins.Country_Id
                       ,ins.Country_Name
                       ,ins.Geo_Lat
                       ,ins.Geo_Long
                       ,ins.Site_Not_Managed
                       ,ins.Site_Closed
                       ,ins.Site_Closed_Dt
                       ,ins.Region_ID
                       ,ins.Region_Name
                       ,ins.Client_Not_Managed
                       ,ins.Division_Not_Managed
                       ,ins.Sitegroup_Type_cd
                       ,ins.Sitegroup_Type_Name
                       ,ins.Site_Search_Combo
                       ,ins.Last_Change_TS
                       ,ins.User_Passcode_Expiration_Duration
                       ,ins.User_Max_Failed_Login_Attempts
                       ,ins.User_PassCode_Reuse_Limit
                       ,ins.Client_Fiscal_Offset
                       ,ins.Client_Type_Id
                       ,ins.Report_Frequency_Type_Id
                       ,ins.Fiscalyear_Startmonth_Type_Id
                       ,ins.Ubm_Service_Id
                       ,ins.Dsm_Strategy
                       ,ins.Is_Sep_Issued
                       ,ins.Sep_Issue_Date
                       ,ins.Sitegroup_Owner_User_id
                       ,ins.Client_Hier_Created_Ts
                       ,ins.Portfolio_Client_Id
                       ,ins.Site_Type_Name
                       ,ins.Client_Analyst_Mapping_Cd
                       ,ins.Site_Analyst_Mapping_Cd
                       ,ins.Weather_Station_Code
                       ,ins.Site_Reference_Number
                       ,ins.Doing_Business_As
                       ,ins.Portfolio_Client_Hier_Reference_Number
                       ,ins.Client_Host_Url
                       ,ins.Is_IDM_Enabled
                       ,ins.IDM_Node_XId
                       ,ins.Time_Zone
                       FROM
                        INSERTED ins
                        FULL OUTER JOIN DELETED del
                              ON del.Client_Hier_Id = ins.Client_Hier_Id
            FOR
                       XML PATH('Client_Hier_Change')
                          ,ELEMENTS
                          ,ROOT('Client_Hier_Changes') );

      IF @Message IS NOT NULL
            BEGIN
	
                  SET @From_Service = N'//Change_Control/Service/CBMS/Change_Control';

				  -- Cycle through the targets in Change_Control Targt and sent the message to each
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'Core.Client_Hier');     
                  OPEN cDialog;    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;      
                  WHILE @@Fetch_Status = 0
                        BEGIN    
						
                              EXEC Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                              SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/Client_Hier_Changes] (@Message);

                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                        END;
                  CLOSE cDialog;
                  DEALLOCATE cDialog;

            END;
END;
;
GO
ALTER TABLE [Core].[Client_Hier] ADD CONSTRAINT [pk_Client_Hier] PRIMARY KEY CLUSTERED  ([Client_Hier_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier__Client_Hier_ID__Client_Id__Sitegroup_Id__Site_Id] ON [Core].[Client_Hier] ([Client_Hier_Id], [Client_Id], [Sitegroup_Id], [Site_Id]) INCLUDE ([City], [Client_Name], [Site_name], [State_Name]) ON [DB_INDEXES01]
GO
ALTER TABLE [Core].[Client_Hier] ADD CONSTRAINT [unc_Client_Hier_Client_Id_Sitegroup_Id_Site_Id] UNIQUE NONCLUSTERED  ([Client_Id], [Site_Id], [Sitegroup_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Client_Id_Site_Not_Managed_Site_Is_Closed] ON [Core].[Client_Hier] ([Client_Id], [Site_Not_Managed], [Site_Closed]) INCLUDE ([Site_Id], [State_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier__Country_Region_Site_Type_Inc] ON [Core].[Client_Hier] ([Country_Id], [Region_ID], [Site_Type_Name]) INCLUDE ([Client_Hier_Id], [Geo_Lat], [Geo_Long]) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [ix_Client_hier__Site_Id] ON [Core].[Client_Hier] ([Site_Id]) INCLUDE ([City], [Client_Hier_Id], [Client_Id], [Site_Address_Line1], [Site_name], [Site_Not_Managed], [Site_Search_Combo], [Sitegroup_Id], [State_Name], [ZipCode]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier__Site_id__Sitegroup_Id__Sitegroup_Type_Name__Sitegroup_Owner_User_id] ON [Core].[Client_Hier] ([Site_Id], [Sitegroup_Id], [Sitegroup_Type_Name], [Sitegroup_Owner_User_id]) INCLUDE ([Sitegroup_Name]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Hier_Sitegroup_id_Client_Hier_Id] ON [Core].[Client_Hier] ([Sitegroup_Id], [Client_Hier_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Client_Hier_Sitegroup_Id_Site_Id_Client_Hier_Id_INCL] ON [Core].[Client_Hier] ([Sitegroup_Id], [Site_Id], [Client_Hier_Id]) INCLUDE ([City], [Client_Id], [Client_Name], [Country_Id], [Site_name], [State_Id], [State_Name]) ON [DB_INDEXES01]
GO
ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO
EXEC sp_addextendedproperty N'MS_Description', 'Dimensional rollup of Client Hierarchy data based on Client, Sitegroup, and Site', 'SCHEMA', N'Core', 'TABLE', N'Client_Hier', NULL, NULL
GO
CREATE FULLTEXT INDEX ON [Core].[Client_Hier] KEY INDEX [pk_Client_Hier] ON [CBMS_Client_Hier_FTSearch]
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Client_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Address_Line1] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Address_Line2] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([City] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([ZipCode] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([State_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Country_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Region_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Type_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Type_Name] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Weather_Station_Code] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Search_Combo] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Reference_Number] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Doing_Business_As] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Client_Host_Url] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Client_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Search_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([CITY_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([State_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Address_Line1_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Address_Line2_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([ZipCode_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Country_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Type_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Type_Name_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Reference_Number_FTSearch] LANGUAGE 1042)
GO
ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Doing_Business_As_FTSearch] LANGUAGE 1042)
GO

ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO















CREATE FULLTEXT INDEX ON [Core].[Client_Hier] KEY INDEX [pk_Client_Hier] ON [CBMS_Client_Hier_FTSearch]
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ENABLE
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ENABLE
GO

ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Client_Name] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Name] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_name] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([City] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([State_Name] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Search_Combo] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Client_Name_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Name_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Sitegroup_Name_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([Site_Search_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([CITY_FTSearch] LANGUAGE 1042)
GO

ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ADD ([State_Name_FTSearch] LANGUAGE 1042)
GO


ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ENABLE
GO


ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ENABLE
GO

ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO



ALTER FULLTEXT INDEX ON [Core].[Client_Hier] ENABLE
GO



ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO















ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO




















ALTER TABLE [Core].[Client_Hier] ENABLE CHANGE_TRACKING
GO
