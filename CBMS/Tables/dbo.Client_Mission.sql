CREATE TABLE [dbo].[Client_Mission]
(
[Client_Id] [int] NOT NULL,
[Mission_Type_Cd] [int] NOT NULL,
[Mission_Statement] [varchar] (7000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Statement_HTML] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_C58D612E_5FBB_4FF4_A2C7_12CE679D91C8_1279889664] DEFAULT (newid())
) ON [DBData_CoreClient] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create trigger [dbo].[sp_MSsync_upd_trig_CLIENT_MISSION_9] on [dbo].[Client_Mission] for update not for replication as  
 declare @rc int
 select @rc = @@ROWCOUNT 

 if @rc = 0 return 
 if update (msrepl_tran_version) return 
 update [dbo].[Client_Mission] set msrepl_tran_version = newid() from [dbo].[Client_Mission], inserted  
     where      [dbo].[Client_Mission].[Client_Id] = inserted.[Client_Id]      and
    [dbo].[Client_Mission].[Mission_Type_Cd] = inserted.[Mission_Type_Cd] 
 
GO
EXEC sp_settriggerorder N'[dbo].[sp_MSsync_upd_trig_CLIENT_MISSION_9]', 'first', 'update', null
GO

EXEC sp_addextendedproperty N'MS_Description', 'Client Mission Statement - shows on Sustainability tab on DV.', 'SCHEMA', N'dbo', 'TABLE', N'Client_Mission', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Mission] ADD 
CONSTRAINT [pk_Client_Mission] PRIMARY KEY CLUSTERED  ([Client_Id], [Mission_Type_Cd]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
