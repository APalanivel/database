CREATE TABLE [dbo].[TransformExecutions]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[InputPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[OutputPath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[ArchivePath] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[ReceivedOn] [datetime] NOT NULL CONSTRAINT [DF_TransformExecutions_ReceivedOn] DEFAULT (getutcdate()),
[ReceivedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[OriginallyProcessedOn] [datetime] NULL,
[OriginallyProcessedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[LastProcessedOn] [datetime] NULL,
[LastProcessedBy] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[LastFailedOn] [datetime] NULL,
[Status] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[IsFullyProcessed] [bit] NOT NULL CONSTRAINT [DF_TransformExecutions_IsFullyProcessed] DEFAULT ((0)),
[FileHash] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[FailureDetails] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[CorrelationId] [uniqueidentifier] NOT NULL,
[AssociatedAssembly_Id] [bigint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformExecutions] ADD CONSTRAINT [PK_dbo.TransformExecutions] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_AssociatedAssembly_Id] ON [dbo].[TransformExecutions] ([AssociatedAssembly_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformExecutions] ADD CONSTRAINT [FK_dbo.TransformExecutions_dbo.TransformPlugins_AssociatedAssembly_Id] FOREIGN KEY ([AssociatedAssembly_Id]) REFERENCES [dbo].[TransformPlugins] ([Id])
GO
