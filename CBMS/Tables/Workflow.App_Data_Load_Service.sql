CREATE TABLE [Workflow].[App_Data_Load_Service]
(
[App_Data_Load_Service_Id] [int] NOT NULL IDENTITY(1, 1),
[App_Data_Load_Service_Method] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [df_App_Data_Load_Service__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_App_Data_Load_Service__Last_Change_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[App_Data_Load_Service] ADD CONSTRAINT [pk_App_Data_Load_Service] PRIMARY KEY CLUSTERED  ([App_Data_Load_Service_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[App_Data_Load_Service] ADD CONSTRAINT [un_App_Data_Load_Service__App_Data_Load_Service_Method] UNIQUE NONCLUSTERED  ([App_Data_Load_Service_Method]) ON [DB_DATA01]
GO
