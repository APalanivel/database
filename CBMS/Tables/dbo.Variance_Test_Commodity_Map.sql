CREATE TABLE [dbo].[Variance_Test_Commodity_Map]
(
[Commodity_Id] [int] NOT NULL,
[Variance_Test_Id] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping variance test to commodity ID', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Test_Commodity_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Test_Commodity_Map] ADD CONSTRAINT [PK33] PRIMARY KEY NONCLUSTERED  ([Commodity_Id], [Variance_Test_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]

ALTER TABLE [dbo].[Variance_Test_Commodity_Map] ADD
CONSTRAINT [RefCommodity38] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
ALTER TABLE [dbo].[Variance_Test_Commodity_Map] ADD
CONSTRAINT [RefVariance_Test39] FOREIGN KEY ([Variance_Test_Id]) REFERENCES [dbo].[Variance_Test_Master] ([Variance_Test_Id])
GO
