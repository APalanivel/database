CREATE TABLE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map]
(
[Sr_Rfp_Service_Condition_Template_Question_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Service_Condition_Template_Question_Map_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Service_Condition_Template_Question_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Service_Condition_Template_Question_Map__Last_Change_Ts] DEFAULT (getdate()),
[SR_RFP_ACCOUNT_TERM_ID] [int] NOT NULL,
[SR_RFP_SELECTED_PRODUCTS_ID] [int] NOT NULL,
[Sr_Service_Condition_Category_Id] [int] NULL,
[Category_Display_Seq] [int] NULL,
[Sr_Service_Condition_Question_Id] [int] NULL,
[Question_Display_Seq] [int] NULL,
[Is_Post_To_Supplier] [bit] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Service_Condition_Template_Question_Map] ADD CONSTRAINT [pk_Sr_Rfp_Service_Condition_Template_Question_Map] PRIMARY KEY CLUSTERED  ([Sr_Rfp_Service_Condition_Template_Question_Map_Id]) ON [DB_DATA01]
GO
