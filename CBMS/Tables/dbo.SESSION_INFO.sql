CREATE TABLE [dbo].[SESSION_INFO]
(
[SESSION_INFO_ID] [int] NOT NULL IDENTITY(1, 1),
[IP_ADDRESS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[APP_SERVER_ID] [int] NULL,
[USER_INFO_ID] [int] NULL,
[LAST_UPDATED] [datetime] NULL,
[SESSION_XML] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_UserInfo] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'lec 8/16/11 - Used to track CBMS only session info.  DV also has a session_info table of same name- but it is tracking DV session info.', 'SCHEMA', N'dbo', 'TABLE', N'SESSION_INFO', NULL, NULL
GO

ALTER TABLE [dbo].[SESSION_INFO] ADD 
CONSTRAINT [PK_SESSION_INFO] PRIMARY KEY CLUSTERED  ([SESSION_INFO_ID]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]




GO
