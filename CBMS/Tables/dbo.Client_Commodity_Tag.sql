CREATE TABLE [dbo].[Client_Commodity_Tag]
(
[Client_Commodity_Id] [int] NOT NULL,
[Tag_Id] [int] NOT NULL,
[Assigned_User_Id] [int] NOT NULL,
[Assigned_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Commodity_Tag] ADD CONSTRAINT [pk_Client_Commodity_Tag] PRIMARY KEY CLUSTERED  ([Client_Commodity_Id], [Tag_Id], [Assigned_User_Id]) ON [DB_DATA01]
GO
