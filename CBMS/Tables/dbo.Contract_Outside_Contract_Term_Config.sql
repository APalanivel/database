CREATE TABLE [dbo].[Contract_Outside_Contract_Term_Config]
(
[Contract_Outside_Contract_Term_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Valcon_Contract_Config_Id] [int] NOT NULL,
[OCT_Parameter_Cd] [int] NOT NULL,
[OCT_Tolerance_Date_Cd] [int] NOT NULL,
[OCT_Dt_Range_Cd] [int] NOT NULL,
[Config_Start_Dt] [date] NULL,
[Config_End_Dt] [date] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Outside_Contract_Term_Config__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Contract_Outside_Contract_Term_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Outside_Contract_Term_Config] ADD CONSTRAINT [PK_Contract_Outside_Contract_Term_Config_Id] PRIMARY KEY CLUSTERED  ([Contract_Outside_Contract_Term_Config_Id]) ON [DB_DATA01]
GO
