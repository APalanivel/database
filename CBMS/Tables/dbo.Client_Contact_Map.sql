CREATE TABLE [dbo].[Client_Contact_Map]
(
[CLIENT_ID] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFClient_Contact_MapCreated_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Contact_Map] ADD CONSTRAINT [pk_Client_Contact] PRIMARY KEY CLUSTERED  ([CLIENT_ID], [Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Contact__Contact_Info_Id] ON [dbo].[Client_Contact_Map] ([Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Contact_Map__Contact_Info_Id] ON [dbo].[Client_Contact_Map] ([Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Contact_Map] ADD CONSTRAINT [fk_Contact_Info__Client_Contact] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [dbo].[Client_Contact_Map] ADD CONSTRAINT [fk_Contact_Info__Client_Contact_Map] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
