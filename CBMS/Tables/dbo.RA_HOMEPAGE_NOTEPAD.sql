CREATE TABLE [dbo].[RA_HOMEPAGE_NOTEPAD]
(
[RA_HOMEPAGE_NOTEPAD_ID] [int] NOT NULL IDENTITY(1, 1),
[USER_INFO_ID] [int] NOT NULL,
[RA_NOTEPAD_DATA] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table was created later for RA dashboard', 'SCHEMA', N'dbo', 'TABLE', N'RA_HOMEPAGE_NOTEPAD', NULL, NULL
GO
