CREATE TABLE [dbo].[Variance_Test_Event_Audit]
(
[Variance_Test_Event_Audit_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Variance_Test_Event_Type_Cd] [int] NOT NULL,
[Variance_Routed_To_User_Id] [int] NULL,
[Event_By_User_Id] [int] NOT NULL,
[Event_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Test_Event_Audit__Event_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Audit trail of variance event (code table) to account/service month combination', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Test_Event_Audit', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Test_Event_Audit] ADD CONSTRAINT [PK_Variance_Test_Event_Audit] PRIMARY KEY CLUSTERED  ([Variance_Test_Event_Audit_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Variance_Test_Event_Audit__Account_Id__Service_Month] ON [dbo].[Variance_Test_Event_Audit] ([Account_Id], [Service_Month]) ON [DB_DATA01]
GO
