CREATE TABLE [Workflow].[Workflow_Queue_Action]
(
[Workflow_Queue_Action_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Id] [int] NOT NULL,
[Workflow_Queue_Action_Cd] [int] NOT NULL,
[Permission_Info_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Action__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Action__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Action] ADD CONSTRAINT [pk_Workflow_Queue_Action] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Action_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Action] ADD CONSTRAINT [un_Workflow_Queue_Action__Workflow_Queue_Id__Workflow_Queue_Action_Cd] UNIQUE NONCLUSTERED  ([Workflow_Queue_Id], [Workflow_Queue_Action_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Action] ADD CONSTRAINT [fk_Workflow_Queue__Workflow_Queue_Action] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
