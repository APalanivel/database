CREATE TABLE [dbo].[SR_SW_REMEMBER_BID]
(
[SR_SW_REMEMBER_BID_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[SR_RFP_BID_ID] [int] NOT NULL,
[CLIENT_ID] [int] NULL,
[SR_RFP_ID] [int] NULL,
[SR_SUPPLIER_CONTACT_INFO_ID] [int] NOT NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_E121A13E_5493_47D8_8AEC_00E06E1D9BED_1575077493] DEFAULT (newid())
) ON [DBData_Sourcing]
GO


EXEC sp_addextendedproperty N'MS_Description', N'Mapping table of RFP ID to bids and clients and supplier contacts', 'SCHEMA', N'dbo', 'TABLE', N'SR_SW_REMEMBER_BID', NULL, NULL
GO

ALTER TABLE [dbo].[SR_SW_REMEMBER_BID] ADD 
CONSTRAINT [PK_SR_SW_REMEMBER_BID] PRIMARY KEY CLUSTERED  ([SR_SW_REMEMBER_BID_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
