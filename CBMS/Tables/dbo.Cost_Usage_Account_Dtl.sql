CREATE TABLE [dbo].[Cost_Usage_Account_Dtl]
(
[Cost_Usage_Account_Dtl_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Service_Month] [date] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Bucket_Value] [decimal] (28, 10) NOT NULL,
[UOM_Type_Id] [int] NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[Created_By_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cost_Usage_Account_DtlN_Created_Dt] DEFAULT (getdate()),
[Updated_Ts] [datetime] NOT NULL CONSTRAINT [df_Cost_Usage_Account_DtlN_Updated_Dt] DEFAULT (getdate()),
[Updated_By_Id] [int] NULL,
[Row_Version] [timestamp] NOT NULL,
[ACCOUNT_ID] [int] NOT NULL,
[Data_Source_Cd] [int] NOT NULL,
[Client_Hier_ID] [int] NOT NULL,
[Data_Type_Cd] [int] NOT NULL
) ON [DB_DATA01]
CREATE NONCLUSTERED INDEX [ix_Cost_Usage_Account_Dtl__Account_Id__Service_Month] ON [dbo].[Cost_Usage_Account_Dtl] ([ACCOUNT_ID], [Service_Month]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ADD 
CONSTRAINT [pk_Cost_Usage_Account_Dtl] PRIMARY KEY CLUSTERED  ([Client_Hier_ID], [ACCOUNT_ID], [Bucket_Master_Id], [Service_Month]) ON [DB_DATA01]ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ENABLE CHANGE_TRACKING
GO

ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ADD CONSTRAINT [un_Cost_Usage_Account_Dtl__Cost_Usage_Account_Dtl_Id] UNIQUE NONCLUSTERED  ([Cost_Usage_Account_Dtl_Id]) ON [DB_DATA01]

CREATE NONCLUSTERED INDEX [ix_Cost_Usage_Account_Dtl__Bucket_Master_Id__ACCOUNT_ID__Service_Month] ON [dbo].[Cost_Usage_Account_Dtl] ([Bucket_Master_Id], [ACCOUNT_ID], [Service_Month]) ON [DB_DATA01]

CREATE NONCLUSTERED INDEX [ix_Cost_usage_Account_Dtl_Service_Month_Data_Source_Cd] ON [dbo].[Cost_Usage_Account_Dtl] ([Service_Month], [Data_Source_Cd]) ON [DB_DATA01]



ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] WITH NOCHECK ADD
CONSTRAINT [fk_Bucket_Master_Cost_Usage_Account_Dtl1] FOREIGN KEY ([Bucket_Master_Id]) REFERENCES [dbo].[Bucket_Master] ([Bucket_Master_Id]) NOT FOR REPLICATION
GO




ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ENABLE CHANGE_TRACKING
GO















ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ENABLE CHANGE_TRACKING
GO



ALTER TABLE [dbo].[Cost_Usage_Account_Dtl] ENABLE CHANGE_TRACKING
GO
