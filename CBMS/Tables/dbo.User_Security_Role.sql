CREATE TABLE [dbo].[User_Security_Role]
(
[Security_Role_Id] [int] NOT NULL,
[User_Info_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_user_Security_Role_last_Change_Ts] DEFAULT (getdate())
) ON [DBData_UserInfo]
CREATE NONCLUSTERED INDEX [ix_User_Security_Role__Security_Role_Id] ON [dbo].[User_Security_Role] ([Security_Role_Id]) INCLUDE ([User_Info_Id]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', 'List the security Roles a user has access to', 'SCHEMA', N'dbo', 'TABLE', N'User_Security_Role', NULL, NULL
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/******
NAME:	tr_User_Security_Role_Transfer

DESCRIPTION: Trigger on dbo.Security_Role_Client_Hier to send changes for DV2
Through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	TP			Anoop
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	TP			12 Feb 2014		Created
******/ 
CREATE TRIGGER [dbo].[tr_User_Security_Role_Del_Transfer] ON [dbo].[User_Security_Role]
      AFTER DELETE
AS
BEGIN 
	
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
		   ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	
      SET @Message = ( SELECT
                        NULL AS Security_Role_Id
                       ,NULL AS User_Info_Id
                       ,d.Security_Role_Id AS Old_Security_Role_Id
                       ,d.User_Info_Id AS Old_User_Info_Id
                       ,NULL AS Last_Change_Ts
                       ,'D' Op_Code
                       FROM
                        DELETED d
            FOR
                       XML PATH('User_Security_Role_Change')
                          ,ELEMENTS
                          ,ROOT('User_Security_Role_Changes') );

    
      IF @Message IS NOT NULL
            BEGIN

			SET @From_Service = N'//Change_Control/Service/CBMS/Security_Role_Transfer'

			-- Cycle through the targets in Change_Control Targt and sent the message to each    
                  DECLARE cDialog CURSOR
                  FOR
                  ( 
					SELECT
							Target_Service
							,Target_Contract
                    FROM
							Service_Broker_Target
                    WHERE
							Table_Name = 'dbo.User_Security_Role'
				)     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0
                        BEGIN  

							EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT

                             ;SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Security_Role_Transfer] (@Message);

								FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  


            END
END












;
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:	tr_User_Security_Role_Transfer

DESCRIPTION: Trigger on dbo.Security_Role_Client_Hier to send changes for DV2
Through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	TP			Anoop
	
MODIFICATIONS
	Initials	Date			Modification
------------------------------------------------------------
	TP			12 Feb 2014		Created
******/ 
CREATE TRIGGER [dbo].[tr_User_Security_Role_Transfer] ON [dbo].[User_Security_Role]
      AFTER INSERT
AS
BEGIN 
	
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
		   ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	
      SET @Message = ( SELECT
                        i.Security_Role_Id
                       ,i.User_Info_Id
                       ,NULL AS Old_Security_Role_Id
                       ,NULL AS Old_User_Info_Id
                       ,i.Last_Change_Ts
                       ,'I' AS Op_Code
                       FROM
                        INSERTED i
            FOR
                       XML PATH('User_Security_Role_Change')
                          ,ELEMENTS
                          ,ROOT('User_Security_Role_Changes') );



      IF @Message IS NOT NULL
            BEGIN

			SET @From_Service = N'//Change_Control/Service/CBMS/Security_Role_Transfer'

			-- Cycle through the targets in Change_Control Targt and sent the message to each    
                  DECLARE cDialog CURSOR
                  FOR
                  ( 
					SELECT
							Target_Service
							,Target_Contract
                    FROM
							Service_Broker_Target
                    WHERE
							Table_Name = 'dbo.User_Security_Role'
				)     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0
                        BEGIN  

							EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT

                             ;SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Security_Role_Transfer] (@Message);

								FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  


            END
END











;
GO

ALTER TABLE [dbo].[User_Security_Role] ADD
CONSTRAINT [fk_User_Security_Role_User_Info] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])

ALTER TABLE [dbo].[User_Security_Role] ADD 
CONSTRAINT [pk_User_Security_Role] PRIMARY KEY CLUSTERED  ([User_Info_Id], [Security_Role_Id]) WITH (FILLFACTOR=90) ON [DBData_UserInfo]
ALTER TABLE [dbo].[User_Security_Role] ADD
CONSTRAINT [fk_User_Security_Role_Security_Role] FOREIGN KEY ([Security_Role_Id]) REFERENCES [dbo].[Security_Role] ([Security_Role_Id])

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******          
NAME:   tr_User_Security_Role_last_Change_Ts       

    
DESCRIPTION:  
	Udpates the Last_Change_Ts if it was not updated        
	
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
DSC			Kaushik     
     
MODIFICATIONS           
Initials	Date			Modification          
------------------------------------------------------------          
CMH			12/14/2010		Created
DSC			03/09/2014		Modified To send msg even only Last_Change_Ts column is updated
*****/ 
CREATE TRIGGER [dbo].[tr_User_Security_Role_last_Change_Ts] ON [dbo].[User_Security_Role]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      IF NOT update(Last_Change_Ts) 
            UPDATE
                  usr
            SET   
                  last_Change_Ts = getdate()
            FROM
                  dbo.User_Security_Role usr
                  INNER JOIN INSERTED i
                        ON usr.Security_Role_Id = i.Security_Role_id
                           AND usr.User_Info_Id = i.user_Info_id




      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
		   ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	
      SET @Message = ( SELECT
                        i.Security_Role_Id
                       ,i.User_Info_Id
                       ,d.Security_Role_Id AS Old_Security_Role_Id
                       ,d.User_Info_Id AS Old_User_Info_Id
                       ,usr.Last_Change_Ts
                       ,case WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NULL THEN 'I'  /** When Primary key Column is updated need to Insert and Delete **/
                             WHEN i.Security_Role_Id IS NOT NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'U'
                             WHEN i.Security_Role_Id IS NULL
                                  AND d.Security_Role_Id IS NOT NULL THEN 'D'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Security_Role_Id = d.Security_Role_Id
                                 AND i.User_Info_Id = d.User_Info_Id
                        LEFT JOIN dbo.User_Security_Role usr
                              ON usr.Security_Role_Id = i.Security_Role_id
                                 AND usr.User_Info_Id = i.user_Info_id
            FOR
                       XML PATH('User_Security_Role_Change')
                          ,ELEMENTS
                          ,ROOT('User_Security_Role_Changes') );

      IF @Message IS NOT NULL 
            BEGIN

				  SET @From_Service = N'//Change_Control/Service/CBMS/Security_Role_Transfer'

			-- Cycle through the targets in Change_Control Targt and sent themessage to each    
               
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.User_Security_Role')     
                  OPEN cDialog    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract      
                  WHILE @@Fetch_Status = 0
                        BEGIN  

							EXEC Service_Broker_Conversation_Handle_Sel @From_Service, @Target_Service, @Target_Contract, @Conversation_Handle OUTPUT

                             ;SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Security_Role_Transfer] (@Message);

								FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract    
                        END    
                  CLOSE cDialog    
                  DEALLOCATE cDIalog  
            END


END
		






;
GO
GO
