CREATE TABLE [dbo].[SITE]
(
[SITE_ID] [int] NOT NULL IDENTITY(10000, 1) NOT FOR REPLICATION,
[SITE_TYPE_ID] [int] NOT NULL,
[SITE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DIVISION_ID] [int] NOT NULL,
[UBMSITE_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRIMARY_ADDRESS_ID] [int] NULL,
[SITE_REFERENCE_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SITE_PRODUCT_SERVICE] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCTION_SCHEDULE] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHUTDOWN_SCHEDULE] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_ALTERNATE_POWER] [bit] NULL CONSTRAINT [Set_To_Zero11] DEFAULT ((0)),
[IS_ALTERNATE_GAS] [bit] NULL CONSTRAINT [Set_To_Zero12] DEFAULT ((0)),
[DOING_BUSINESS_AS] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAICS_CODE] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DUNS_NUMBER] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero13] DEFAULT ((0)),
[HISTORY_REASON_TYPE_ID] [int] NULL,
[CBMS_IMAGE_ID] [int] NULL,
[CLOSED] [bit] NOT NULL CONSTRAINT [DF_SITE_CLOSED] DEFAULT ((0)),
[CLOSED_DATE] [datetime] NULL,
[NOT_MANAGED] [bit] NOT NULL CONSTRAINT [DF_SITE_NOT_MANAGED] DEFAULT ((0)),
[CLOSED_BY_ID] [int] NULL,
[USER_INFO_ID] [int] NULL,
[CONTRACTING_ENTITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_LEGAL_STRUCTURE] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LEGAL_STRUCTURE_CBMS_IMAGE_ID] [int] NULL,
[IS_PREFERENCE_BY_EMAIL] [bit] NULL,
[IS_PREFERENCE_BY_DV] [bit] NULL,
[LP_CONTACT_EMAIL_ADDRESS] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LP_CONTACT_FIRST_NAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LP_CONTACT_LAST_NAME] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LP_CONTACT_CC] [varchar] (1500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROLLOUT_EMAIL_DATE] [datetime] NULL,
[ROLLOUT_EMAIL_SENT_BY] [int] NULL,
[Row_Version] [timestamp] NULL,
[Client_ID] [int] NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_B21C0939_2B4B_47BB_8CD8_3D1788462B68_916002840] DEFAULT (newid()),
[Analyst_Mapping_Cd] [int] NULL,
[Weather_Station_Code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Portfolio_Client_Hier_Reference_Number] [int] NULL,
[Is_IDM_Enabled] [bit] NOT NULL CONSTRAINT [df_Is_IDM_Enabled] DEFAULT ((0)),
[IDM_Node_XId] [int] NULL,
[Is_System_Generated_Weather_Station] [bit] NOT NULL CONSTRAINT [df_SITE_Is_System_Generated_Weather_Station] DEFAULT ((1)),
[Is_System_Generated_Timezone] [bit] NOT NULL CONSTRAINT [df_SITE_Is_System_Generated_Timezone] DEFAULT ((1)),
[Time_Zone_Id] [int] NULL
) ON [DBData_CoreClient]
ALTER TABLE [dbo].[SITE] WITH NOCHECK ADD
CONSTRAINT [ENTITY_SITE_FK_2] FOREIGN KEY ([HISTORY_REASON_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SITE] WITH NOCHECK ADD
CONSTRAINT [ADDRESS_SITE_FK] FOREIGN KEY ([PRIMARY_ADDRESS_ID]) REFERENCES [dbo].[ADDRESS] ([ADDRESS_ID])
ALTER TABLE [dbo].[SITE] WITH NOCHECK ADD
CONSTRAINT [ENTITY_SITE_FK_1] FOREIGN KEY ([SITE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])



GO
EXEC sp_addextendedproperty N'MS_Description', 'Physical site belonging to a client', 'SCHEMA', N'dbo', 'TABLE', N'SITE', NULL, NULL
GO

ALTER TABLE [dbo].[SITE] WITH NOCHECK ADD
CONSTRAINT [FK_ROLLOUT_EMAIL_SENT_BY] FOREIGN KEY ([ROLLOUT_EMAIL_SENT_BY]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID]) NOT FOR REPLICATION
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Site_del_Client_Hier
	
DESCRIPTION:   
	This will delete the record from Core.Clinet_Hier table whenever the entry get deleted from Site table.
	
 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		INSERT INTO Site(SITE_TYPE_ID,SITE_NAME,DIVISION_ID,Client_ID,CLOSED,NOT_MANAGED,PRIMARY_ADDRESS_ID)
		VALUES(250,'TEST-ST',571,10069,0,0,11815)
		
		SELECT SITE_NAME,DIVISION_ID FROM Site WHERE SITE_NAME = 'TEST-ST'
		SELECT SITE_NAME,Sitegroup_ID FROM CORE.CLIENT_HIER WHERE SITE_NAME = 'TEST-ST'
				
		DELETE SGS
		FROM SITEGROUP_SITE SGS
		INNER JOIN SITE S ON S.SITE_ID = SGS.SITE_ID
		WHERE S.SITE_NAME = 'TEST-ST'
		
		DELETE FROM SITE WHERE SITE_NAME = 'TEST-ST'
		
		SELECT SITE_NAME,DIVISION_ID FROM Site WHERE SITE_NAME = 'TEST-ST'
		SELECT SITE_NAME,Sitegroup_ID FROM CORE.CLIENT_HIER WHERE SITE_NAME = 'TEST-ST'
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	DSC		Kaushik

 MODIFICATIONS:
 Initials	Date			Modification
------------------------------------------------------------
	SKA		08/02/2010		Created
	DSC		2014-06-18		Added code to send message for processing Blackstone client
******/
CREATE TRIGGER [dbo].[tr_Site_del_Client_Hier] ON [dbo].[SITE]
      FOR DELETE
AS
BEGIN
      SET NOCOUNT ON ; 
	
      



-- Sending msg to process for portfolio client

  -- send Message to delete duplicate site 
  DECLARE @Message XML = ( SELECT
								ch.Client_Hier_Id
							   ,d.Client_Id
							   ,'D' AS Op_Code
						   FROM
								DELETED d 
								INNER JOIN dbo.CLIENT c
										ON d.Client_ID = c.CLIENT_ID
								INNER JOIN Core.Client_Hier ch
								ON ch.site_Id = d.site_Id
						   WHERE
								c.Portfolio_Client_Id > 0 AND d.Portfolio_Client_Hier_Reference_Number IS NULL
		FOR
						   XML PATH('Site')
							  ,ELEMENTS
							  ,ROOT('Site_Info') )  

	  IF @Message IS NOT NULL 
	        BEGIN

	              DECLARE @Conversation_Handle UNIQUEIDENTIFIER;    

	              BEGIN DIALOG CONVERSATION @Conversation_Handle
					FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
					TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
					ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

				  SEND ON CONVERSATION @Conversation_Handle
					MESSAGE TYPE [//Change_Control/Message/Delete_Portfolio_Site] (@Message)    

	        END


      DELETE
            Core.Client_Hier
      FROM
            Core.Client_Hier ch
            INNER JOIN DELETED d ON ch.Site_Id = d.Site_Id



END









;
GO


SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:	dbo.Site.tr_Site_INS_SiteGroup_Site


DESCRIPTION:
	Insets records into the Sitegroup_Site table for new sites based on the Division_id



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			07/08/2009	Created    
	DMR		  09/10/2010 Modified for Quoted_Identifier    	
******/

CREATE TRIGGER [dbo].[tr_Site_INS_SiteGroup_Site]
ON [dbo].[SITE] FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON 

	INSERT SiteGroup_Site (SiteGroup_Id, Site_Id) 
	SELECT Division_Id, Site_Id
	FROM Inserted
	
	SET NOCOUNT OFF
END

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Site_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Site table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		INSERT INTO Site(SITE_TYPE_ID,SITE_NAME,DIVISION_ID,Client_ID,CLOSED,NOT_MANAGED,PRIMARY_ADDRESS_ID)
		VALUES(250,'TEST-ST',571,10069,0,0,11815)
		
		SELECT SITE_NAME,DIVISION_ID FROM Site WHERE SITE_NAME = 'TEST-ST'
		SELECT SITE_NAME,Sitegroup_ID FROM CORE.CLIENT_HIER WHERE SITE_NAME = 'TEST-ST'
				
		UPDATE Site
		SET DIVISION_ID = 569
		WHERE SITE_NAME = 'TEST-ST'
		
		SELECT SITE_NAME,DIVISION_ID FROM Site WHERE SITE_NAME = 'TEST-ST'
		SELECT SITE_NAME,Sitegroup_ID FROM CORE.CLIENT_HIER WHERE SITE_NAME = 'TEST-ST'
	ROLLBACK TRAN
	
	BEGIN TRAN	
			
		UPDATE Site
		SET client_id = 11231
		,PRIMARY_ADDRESS_ID = 10
		WHERE SITE_id = 36695
		
		SELECT * FROM Site WHERE SITE_id = 36695
		SELECT * FROM CORE.CLIENT_HIER WHERE SITE_id = 36695
	ROLLBACK TRAN
	
AUTHOR INITIALS:
 Initials	  Name
------------------------------------------------------------
 SKA		  Shobhit Kumar Agrawal
 PNR		  Pandarinath
 HG			  Harihara Suthan G
 AP			  Athmaram Pabbathi
 KVK		  Vinay K
 AKR          Ashok Kumar Raju
 CPE		  Chaitanya Panduga Eshwar
 BCH		  Balaraju Chalumuri
 SP			  Sandeep Pigilam
 DSC		  Kaushik


 MODIFICATIONS:
 Initials	Date		    Modification
------------------------------------------------------------
 SKA		08/02/2010    Created
			08/31/2010    Added the Last_Change_TS column for update
			09/07/2010    Client_ID & Client_Name added in update statement
						   Modified the logic to get the GHG_Region_Key
 PNR		10/21/2010    Added client.User_Passcode_Expiration_Duration,client.User_Max_Failed_Login_Attempts
						   ,client.User_PassCode_Reuse_Limit in Update list ( Project Name : DV_SV_Password Expiration )
 HG			01/05/2011    Sitegroup_Type_Dsc column added in the update statement
 HG			01/13/2011    Sitegroup_Type_Dsc column renamed to Sitegroup_Type_Name
 AP			03/16/2012    Added Site_Type_Name in update statement
 KVK		04/11/2012    Updated the trigger to remove the dropped tables 
						GHG_Region_Country,
						GHG_Region_ZipCode,
						GHG_Region
 AKR        2012-09-17  Added Site_Analyst_Mapping_Cd Column to Core.Client_Hier table						
 CPE		2012-10-28  Added the COLUMN Weather_Station_Code
 BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion WITH HASHBYTES FUNCTION.
 SP			2014-05-13  MAINT-2798, Added Site_Reference_Number.
 DSC		2014-06-18	Added code to send message for processing Blackstone client
 DSC		2014-01-13	Added New columns Is_IDM_Enabled,IDM_Node_XId
 HG			2017-02-27	Added Time zone column to Client_Hier
 HG			2017-04-17	Aliased the Site_Id and Client_id column returned to return to form portfolio client hier
******/
CREATE TRIGGER [dbo].[tr_Site_upd_Client_Hier] ON [dbo].[SITE]
      FOR UPDATE
AS
BEGIN
    
      SET NOCOUNT ON; 
  
      UPDATE
            Core.Client_Hier
      SET
            Client_Id = i.Client_ID
           ,Client_Name = cl.CLIENT_NAME
           ,Sitegroup_Id = sg.Sitegroup_Id
           ,Sitegroup_Name = sg.Sitegroup_Name
           ,Division_Not_Managed = div.NOT_MANAGED
           ,Sitegroup_Type_cd = sg.Sitegroup_Type_Cd
           ,Sitegroup_Type_Name = SgType.Code_Value
           ,Site_Id = i.SITE_ID
           ,Site_name = i.SITE_NAME
           ,Site_Address_Line1 = ad.ADDRESS_LINE1
           ,Site_Address_Line2 = ad.ADDRESS_LINE2
           ,City = ad.CITY
           ,ZipCode = ad.ZIPCODE
           ,State_Id = st.STATE_ID
           ,State_Name = st.STATE_NAME
           ,Country_Id = st.COUNTRY_ID
           ,Country_Name = cnt.COUNTRY_NAME
           ,Geo_Lat = ad.GEO_LAT
           ,Geo_Long = ad.GEO_LONG
           ,Site_Not_Managed = i.NOT_MANAGED
           ,Site_Closed = i.CLOSED
           ,Site_Closed_Dt = ISNULL(i.CLOSED_DATE, '1900-01-01')
           ,Region_ID = rg.REGION_ID
           ,Region_Name = rg.REGION_NAME
           ,Last_Change_TS = GETDATE()
           ,User_Passcode_Expiration_Duration = cl.User_Passcode_Expiration_Duration
           ,User_Max_Failed_Login_Attempts = cl.User_Max_Failed_Login_Attempts
           ,User_PassCode_Reuse_Limit = cl.User_PassCode_Reuse_Limit
           ,Site_Type_Name = stype.ENTITY_NAME
           ,Site_Analyst_Mapping_Cd = i.Analyst_Mapping_Cd
           ,Weather_Station_Code = i.Weather_Station_Code
           ,Site_Reference_Number = i.SITE_REFERENCE_NUMBER
           ,Portfolio_Client_Hier_Reference_Number = i.Portfolio_Client_Hier_Reference_Number
           ,Is_IDM_Enabled = i.Is_IDM_Enabled
           ,IDM_Node_XId = i.IDM_Node_XId
           ,Time_Zone = tz.Time_Zone
      FROM
            Core.Client_Hier ch
            INNER JOIN DELETED d
                  ON ch.Site_Id = d.SITE_ID
                     AND d.DIVISION_ID = ch.Sitegroup_Id
            INNER JOIN INSERTED i
                  ON i.SITE_ID = ch.Site_Id
            INNER JOIN dbo.Sitegroup sg
                  ON sg.Sitegroup_Id = i.DIVISION_ID
            INNER JOIN dbo.Code SgType
                  ON SgType.Code_Id = sg.Sitegroup_Type_Cd
            INNER JOIN dbo.Division_Dtl div
                  ON div.SiteGroup_Id = sg.Sitegroup_Id
            INNER JOIN dbo.ADDRESS ad
                  ON ad.ADDRESS_ID = i.PRIMARY_ADDRESS_ID
            INNER JOIN dbo.STATE st
                  ON st.STATE_ID = ad.STATE_ID
            INNER JOIN dbo.REGION rg
                  ON rg.REGION_ID = st.REGION_ID
            INNER JOIN dbo.COUNTRY cnt
                  ON cnt.COUNTRY_ID = st.COUNTRY_ID
            INNER JOIN dbo.CLIENT cl
                  ON cl.CLIENT_ID = i.Client_ID
            INNER JOIN dbo.ENTITY stype
                  ON stype.ENTITY_ID = i.SITE_TYPE_ID
            LEFT OUTER JOIN dbo.Time_Zone tz
                  ON tz.Time_Zone_Id = i.Time_Zone_Id
      WHERE
            HASHBYTES('SHA1', ( SELECT
                                    i.SITE_NAME
                                   ,i.PRIMARY_ADDRESS_ID
                                   ,i.CLOSED
                                   ,i.CLOSED_DATE
                                   ,i.NOT_MANAGED
                                   ,i.Client_ID
                                   ,i.DIVISION_ID
                                   ,i.SITE_TYPE_ID
                                   ,i.Analyst_Mapping_Cd
                                   ,i.Weather_Station_Code
                                   ,i.SITE_REFERENCE_NUMBER
                                   ,i.Is_IDM_Enabled
                                   ,i.IDM_Node_XId
                                   ,ISNULL(i.Time_Zone_Id, 0) AS Time_Zone_Id
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT
                                                                        d.SITE_NAME
                                                                       ,d.PRIMARY_ADDRESS_ID
                                                                       ,d.CLOSED
                                                                       ,d.CLOSED_DATE
                                                                       ,d.NOT_MANAGED
                                                                       ,d.Client_ID
                                                                       ,d.DIVISION_ID
                                                                       ,d.SITE_TYPE_ID
                                                                       ,d.Analyst_Mapping_Cd
                                                                       ,d.Weather_Station_Code
                                                                       ,d.SITE_REFERENCE_NUMBER
                                                                       ,d.Is_IDM_Enabled
                                                                       ,d.IDM_Node_XId
                                                                       ,ISNULL(d.Time_Zone_Id, 0) AS Time_Zone_Id
                                                        FOR
                                                                  XML RAW ));


				-- Sending msg to process for portfolio client


      DECLARE @Message XML = ( SELECT
                                    i.SITE_ID AS Site_Id
                                   ,i.Client_ID AS Client_Id
                                   ,'U' AS Op_Code
                               FROM
                                    INSERTED i
                                    JOIN dbo.CLIENT c
                                          ON i.Client_ID = c.CLIENT_ID
                               WHERE
                                    c.Portfolio_Client_Id > 0
                                    AND i.Portfolio_Client_Hier_Reference_Number IS NULL
            FOR
                               XML PATH('Site')
                                  ,ELEMENTS
                                  ,ROOT('Site_Info') );  

      IF @Message IS NOT NULL
            BEGIN

                  DECLARE @Conversation_Handle UNIQUEIDENTIFIER;    

                  BEGIN DIALOG CONVERSATION @Conversation_Handle
									FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
									TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
									ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

                  SEND ON CONVERSATION @Conversation_Handle
									MESSAGE TYPE [//Change_Control/Message/Mapping_Site_To_Portfolio] (@Message);    

            END;


END;
;
GO


SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/******
NAME:	dbo.Site.tr_Site_UPD_SiteGroup_Site


DESCRIPTION:
	Updates the Sitegroup_Site.Divsion_Id when the Site.Divsion_Id changes



AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	CMH			Chad Hattabaugh

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	CMH			07/08/2009	Created        	
 DMR		  09/10/2010 Modified for Quoted_Identifier


******/
CREATE TRIGGER [dbo].[tr_Site_UPD_SiteGroup_Site]
ON [dbo].[SITE] FOR UPDATE
AS 
BEGIN
	SET NOCOUNT ON 

	IF update(Division_id)
		UPDATE SiteGroup_Site 
			SET SiteGroup_Id = i.Division_id 
		FROM Sitegroup_Site ss
			INNER JOIN Deleted d ON ss.Sitegroup_Id = d.Division_id and ss.Site_Id = d.Site_id
			INNER JOIN Inserted i ON d.Site_id = i.Site_Id
		WHERE ss.SiteGroup_Id != i.Division_Id
		
	SET NOCOUNT OFF
END

GO






ALTER TABLE [dbo].[SITE] ADD
CONSTRAINT [fk_Site_Cleint] FOREIGN KEY ([Client_ID]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
ALTER TABLE [dbo].[SITE] ADD
CONSTRAINT [DIVISION_SITE_FK] FOREIGN KEY ([DIVISION_ID]) REFERENCES [dbo].[Division_Dtl] ([SiteGroup_Id])

ALTER TABLE [dbo].[SITE] ADD 
CONSTRAINT [SITE_PK] PRIMARY KEY CLUSTERED  ([SITE_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
ALTER TABLE [dbo].[SITE] ENABLE CHANGE_TRACKING
GO

CREATE NONCLUSTERED INDEX [ix_Site_Client_id] ON [dbo].[SITE] ([Client_ID]) INCLUDE ([DIVISION_ID], [SITE_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Site__Client_id__Site_ID__Not_Managed] ON [dbo].[SITE] ([Client_ID], [SITE_ID], [NOT_MANAGED]) INCLUDE ([DIVISION_ID], [PRIMARY_ADDRESS_ID], [SITE_NAME]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [SITE_N_1] ON [dbo].[SITE] ([DIVISION_ID], [PRIMARY_ADDRESS_ID], [SITE_TYPE_ID], [CLOSED], [NOT_MANAGED], [SITE_NAME], [SITE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [SITE_N_2] ON [dbo].[SITE] ([SITE_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Site__Client_id] ON [dbo].[SITE] ([Client_ID], [SITE_NAME]) INCLUDE ([PRIMARY_ADDRESS_ID]) ON [DB_INDEXES01]

CREATE UNIQUE NONCLUSTERED INDEX [SITE_U_1] ON [dbo].[SITE] ([SITE_NAME], [DIVISION_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [SITE_N_3] ON [dbo].[SITE] ([PRIMARY_ADDRESS_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]





GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 tr_Site_ins_Client_Hier  
   
DESCRIPTION:     
 This will insert the new record into Core.Clinet_Hier table whenever the new site added into the Site table.  
 Site level Hier_level_Cd will be used of Client_Hier table  
 Here from App it will insert the record first in Site table then Address and others and finaly update the site table again.  
   
USAGE EXAMPLES:  
------------------------------------------------------------  
   
 BEGIN TRAN  
   
  INSERT INTO Site(SITE_TYPE_ID,SITE_NAME,DIVISION_ID,Client_ID,CLOSED,NOT_MANAGED,PRIMARY_ADDRESS_ID)  
  VALUES(250,'TEST-ST',571,10069,0,0,10)  
    
  SELECT * FROM Site WHERE SITE_NAME = 'TEST-ST'  
  SELECT * FROM CORE.CLIENT_HIER WHERE SITE_NAME = 'TEST-ST'  
   
 ROLLBACK TRAN  
   
AUTHOR INITIALS:  
 Initials	Name  
------------------------------------------------------------  
 SKA		Shobhit Kumar Agrawal  
 HG			Harihara Suthan G  
 PNR		Pandarinath  
 RK			Rajesh Kasoju
 AP			Athmaram Pabbathi
 RC			Rao Chejarla
 AKR		Ashok Kumar Raju
 CPE		Chaitanya Panduga Eshwar
 SP			Sandeep Pigilam
 DSC		Kaushik
 
MODIFICATIONS:
 Initials	Date		Modification  
------------------------------------------------------------  
 SKA		08/02/2010	Created  
 HG			09/02/2010	Removed the Sitegroup_Change_Ts from INSERT statement as it was removed from the table.  
 SKA		09/08/2010	Modified the logic to get the GHG_Region_Key  
 PNR		10/21/2010	Added client_Hier.User_Passcode_Expiration_Duration,client_Hier.User_Max_Failed_Login_Attempts  
							,client_Hier.User_PassCode_Reuse_Limit in select as list ( Project Name : DV_SV_Password Expiration )  

 HG			01/07/2011	Sitegroup_Type_Dsc column added in the update statement  
 HG			01/13/2011	Sitegroup_Type_Dsc column renamed to Sitegroup_Type_Name  
						Script modified to populate column Client_Fiscal_OffSet.

 HG			10/19/2011	MAINT-868 , fixed the code to populate the followings columns
							 Client_Type_Id    
							 Report_Frequency_Type_Id    
							 Fiscalyear_Startmonth_Type_Id    
							 Ubm_Service_Id    
							 Dsm_Strategy    
							 Is_Sep_Issued    
							 Sep_Issue_Date 
 RK			03/09/2012	Added Portfolio_Client_Id column to the populated list of columns 					 
 AP			03/16/2012	Added Site_Type_Name in Insert statement
 RC			04/11/2012	Removed referenced to GHG_Region_Key and GHG_Region_Name columns	
 AKR		2012-09-17	Added the Column Site_Analyst_Mapping_Cd Column
 CPE		2012-10-28	Added the column Weather_Station_Code
 HG			2013-04-26	MAINT-1885, modified the script to insert Client_Analyst_Mapping_Cd
 SP			2014-05-13  MAINT-2798, Added Site_Reference_Number.
 DSC		2014-06-18	Added code to send message for processing Blackstone client
 DSC		2014-01-13	Added New columns Is_IDM_Enabled,IDM_Node_XId
 HG			2017-02-27	Added Time zone column to Client_Hier
 HG			2017-04-17	Aliased the Site_Id and Client_id column returned to return to form portfolio client hier
******/
CREATE TRIGGER [dbo].[tr_Site_ins_Client_Hier] ON [dbo].[SITE]
      FOR INSERT
AS
BEGIN

      SET NOCOUNT ON;

      DECLARE @HierCd INT;  

      SELECT
            @HierCd = Code_Id
      FROM
            dbo.Codeset cs
            INNER JOIN dbo.Code c
                  ON cs.Codeset_Id = c.Codeset_Id
      WHERE
            cs.Std_Column_Name = 'Hier_level_Cd'
            AND c.Code_Value = 'Site';  


      INSERT      Core.Client_Hier
                  ( Hier_level_Cd
                  ,Client_Id
                  ,Client_Name
                  ,Client_Currency_Group_Id
                  ,Client_Not_Managed
                  ,Sitegroup_Id
                  ,Sitegroup_Name
                  ,Division_Not_Managed
                  ,Sitegroup_Type_cd
                  ,Sitegroup_Type_Name
                  ,Site_Id
                  ,Site_name
                  ,Site_Address_Line1
                  ,Site_Address_Line2
                  ,City
                  ,ZipCode
                  ,State_Id
                  ,State_Name
                  ,Country_Id
                  ,Country_Name
                  ,Geo_Lat
                  ,Geo_Long
                  ,Site_Not_Managed
                  ,Site_Closed
                  ,Site_Closed_Dt
                  ,Region_ID
                  ,Region_Name
                  ,User_Passcode_Expiration_Duration
                  ,User_Max_Failed_Login_Attempts
                  ,User_PassCode_Reuse_Limit
                  ,Client_Fiscal_Offset
                  ,Client_Type_Id
                  ,Report_Frequency_Type_Id
                  ,Fiscalyear_Startmonth_Type_Id
                  ,Ubm_Service_Id
                  ,Dsm_Strategy
                  ,Is_Sep_Issued
                  ,Sep_Issue_Date
                  ,Portfolio_Client_Id
                  ,Site_Type_Name
                  ,Client_Analyst_Mapping_Cd
                  ,Site_Analyst_Mapping_Cd
                  ,Weather_Station_Code
                  ,Site_Reference_Number
                  ,Portfolio_Client_Hier_Reference_Number
                  ,Is_IDM_Enabled
                  ,IDM_Node_XId
                  ,Time_Zone )
                  SELECT
                        @HierCd
                       ,ch.Client_Id
                       ,ch.Client_Name
                       ,ch.Client_Currency_Group_Id
                       ,ch.Client_Not_Managed
                       ,i.DIVISION_ID
                       ,sg.Sitegroup_Name
                       ,div.NOT_MANAGED
                       ,sg.Sitegroup_Type_Cd
                       ,SgType.Code_Value
                       ,i.Site_Id
                       ,i.Site_name
                       ,ad.ADDRESS_LINE1
                       ,ad.ADDRESS_LINE2
                       ,ad.CITY
                       ,ad.ZIPCODE
                       ,st.STATE_ID
                       ,st.STATE_NAME
                       ,st.COUNTRY_ID
                       ,cnt.COUNTRY_NAME
                       ,ad.GEO_LAT
                       ,ad.GEO_LONG
                       ,i.NOT_MANAGED
                       ,i.CLOSED
                       ,ISNULL(i.CLOSED_DATE, '1900-01-01')
                       ,ISNULL(rg.REGION_ID, 0)
                       ,rg.REGION_NAME
                       ,ch.User_Passcode_Expiration_Duration
                       ,ch.User_Max_Failed_Login_Attempts
                       ,ch.User_PassCode_Reuse_Limit
                       ,ch.Client_Fiscal_Offset
                       ,ch.Client_Type_Id
                       ,ch.Report_Frequency_Type_Id
                       ,ch.Fiscalyear_Startmonth_Type_Id
                       ,ch.Ubm_Service_Id
                       ,ch.Dsm_Strategy
                       ,ch.Is_Sep_Issued
                       ,ch.Sep_Issue_Date
                       ,ch.Portfolio_Client_Id
                       ,stype.ENTITY_NAME
                       ,ch.Client_Analyst_Mapping_Cd
                       ,i.Analyst_Mapping_Cd
                       ,i.Weather_Station_Code
                       ,i.Site_Reference_Number
                       ,i.Portfolio_Client_Hier_Reference_Number
                       ,i.Is_IDM_Enabled
                       ,i.IDM_Node_XId
                       ,tz.Time_Zone
                  FROM
                        INSERTED i
                        INNER JOIN Core.Client_Hier ch
                              ON i.Client_Id = ch.Client_Id
                                 AND i.DIVISION_ID = ch.Sitegroup_Id
                        INNER JOIN dbo.Sitegroup sg
                              ON sg.Sitegroup_Id = i.DIVISION_ID
                        INNER JOIN dbo.Division_Dtl div
                              ON div.SiteGroup_Id = sg.Sitegroup_Id
                        INNER JOIN dbo.Code SgType
                              ON SgType.Code_Id = sg.Sitegroup_Type_Cd
                        LEFT JOIN dbo.ADDRESS ad
                              ON ad.ADDRESS_ID = i.PRIMARY_ADDRESS_ID
                        LEFT JOIN dbo.STATE st
                              ON st.STATE_ID = ad.STATE_ID
                        LEFT JOIN dbo.REGION rg
                              ON rg.REGION_ID = st.REGION_ID
                        LEFT JOIN dbo.COUNTRY cnt
                              ON cnt.COUNTRY_ID = st.COUNTRY_ID
                        INNER JOIN dbo.ENTITY stype
                              ON stype.ENTITY_ID = i.SITE_TYPE_ID
                        LEFT JOIN dbo.Time_Zone tz
                              ON tz.Time_Zone_Id = i.Time_Zone_Id
                  WHERE
                        ch.Site_Id = 0
                        AND ch.Sitegroup_Id > 0;

	-- Sending msg to process for portfolio client

	-- if new site is belongs to one of child client of portfolio client then send Message
      
      DECLARE @Message XML = ( SELECT
                                    SITE_ID AS Site_Id
                                   ,i.CLIENT_ID AS Client_Id
                                   ,'I' AS Op_Code
                               FROM
                                    INSERTED i
                                    INNER JOIN dbo.CLIENT c
                                          ON i.CLIENT_ID = c.CLIENT_ID
                               WHERE
                                    c.Portfolio_Client_Id > 0
                                    AND i.Portfolio_Client_Hier_Reference_Number IS NULL
            FOR
                               XML PATH('Site')
                                  ,ELEMENTS
                                  ,ROOT('Site_Info') );  

      IF @Message IS NOT NULL
            BEGIN

                  DECLARE @Conversation_Handle UNIQUEIDENTIFIER;    

                  BEGIN DIALOG CONVERSATION @Conversation_Handle
					FROM SERVICE [//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping]    
					TO SERVICE '//Change_Control/Service/CBMS/Portfolio_ClientHier_Mapping'
					ON CONTRACT [//Change_Control/Contract/Mapping_UnMapping_Site_To_Portfolio];    

                  SEND ON CONVERSATION @Conversation_Handle
					MESSAGE TYPE [//Change_Control/Message/Mapping_Site_To_Portfolio] (@Message);    

            END;
END;
;
GO
