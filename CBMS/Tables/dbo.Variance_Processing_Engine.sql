CREATE TABLE [dbo].[Variance_Processing_Engine]
(
[Variance_Processing_Engine_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Engine_Cd] [int] NOT NULL,
[Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Variance_processing_create] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Processing_Engine] ADD CONSTRAINT [PK_Variance_Processing_Engine_Id] PRIMARY KEY CLUSTERED  ([Variance_Processing_Engine_Id]) ON [DB_DATA01]
GO
