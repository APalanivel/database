CREATE TABLE [dbo].[IC_Supllier_Account_Changes_Batch]
(
[IC_Supllier_Account_Changes_Batch_Id] [int] NOT NULL IDENTITY(1, 1),
[Batch_Run_Date] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IC_Supllier_Account_Changes_Batch] ADD CONSTRAINT [pk_IC_Supllier_Account_Changes_Batch_Id] PRIMARY KEY NONCLUSTERED  ([IC_Supllier_Account_Changes_Batch_Id]) ON [DB_DATA01]
GO
