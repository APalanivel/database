CREATE TABLE [dbo].[Client_Commodity_Variance_Test_Config]
(
[Client_Commodity_Variance_Test_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Commodity_Id] [int] NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[Variance_Test_Type_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Commodity_Variance_Test_Config__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Client_Commodity_Variance_Test_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Commodity_Variance_Test_Config] ADD CONSTRAINT [PK_Client_Commodity_Variance_Test_Config] PRIMARY KEY CLUSTERED  ([Client_Commodity_Variance_Test_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Commodity_Variance_Test_Config] ADD CONSTRAINT [unc__Client_Commodity_Variance_Test_Config__Client_Commodity_Id__Start_Dt__End_Dt] UNIQUE NONCLUSTERED  ([Client_Commodity_Id], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
