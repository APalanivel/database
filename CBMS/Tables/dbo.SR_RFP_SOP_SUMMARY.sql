CREATE TABLE [dbo].[SR_RFP_SOP_SUMMARY]
(
[SR_RFP_SOP_SUMMARY_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_ACCOUNT_GROUP_ID] [int] NOT NULL,
[IS_BID_GROUP] [bit] NULL,
[CURRENT_VENDOR_NAME] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRICING_SUMMARY] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[START_DATE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[END_DATE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VOLUME] [decimal] (32, 16) NULL
) ON [DBData_Sourcing] TEXTIMAGE_ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table will store the system SOP master data', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_SOP_SUMMARY', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_SOP_SUMMARY] ADD 
CONSTRAINT [PK__SR_RFP_SOP_SUMMA__2A2D1845] PRIMARY KEY CLUSTERED  ([SR_RFP_SOP_SUMMARY_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
