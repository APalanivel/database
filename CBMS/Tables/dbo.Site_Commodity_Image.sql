CREATE TABLE [dbo].[Site_Commodity_Image]
(
[SITE_ID] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[CBMS_IMAGE_ID] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'images relating to site level cost and usage for a service month', 'SCHEMA', N'dbo', 'TABLE', N'Site_Commodity_Image', NULL, NULL
GO

ALTER TABLE [dbo].[Site_Commodity_Image] ADD CONSTRAINT [pk_Site_Commodity_Image] PRIMARY KEY NONCLUSTERED  ([SITE_ID], [Commodity_Id], [Service_Month]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******          
NAME:   tr_Site_Commodity_Image_Audit       

    
DESCRIPTION:  
	Populates the Site_Commodity_Image-Audit table wheen there is a change to the 
	Site_Commodity_Image table        
	
      
INPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------          

                
OUTPUT PARAMETERS:          
Name			DataType	Default		Description          
------------------------------------------------------------ 

         
USAGE EXAMPLES:          
------------------------------------------------------------ 

       
     
AUTHOR INITIALS:          
Initials	Name          
------------------------------------------------------------          
CMH			Chad Hattabaugh  
     
     
MODIFICATIONS           
Initials	Date	Modification          
------------------------------------------------------------          
CMH			10/06/2010	Created
*****/ 
CREATE TRIGGER [dbo].[tr_Site_Commodity_Image_Audit]
	ON [dbo].[Site_Commodity_Image] FOR INSERT, UPDATE, DELETE
AS 
BEGIN 
	SET NOCOUNT ON; 
	
	INSERT dbo.Site_Commodity_Image_Audit
	(	Audit_Function
		,Audit_Ts
		,Site_Id
		,Commodity_Id
		,Service_Month	) 
	SELECT
		CASE  
			WHEN d.Site_Id IS NULL THEN  1		-- Inserted  
			WHEN i.Site_Id IS NULL THEN  -1		-- Deleted  
			ELSE 0								-- Updated  
		END
		,GetDate() 
		,isnull(i.Site_Id, d.Site_Id) 
		,isnull(i.Commodity_Id, d.Commodity_Id) 
		,isnull(i.Service_Month, d.Service_Month)   
	FROM
		Inserted i 
		FULL OUTER JOIN Deleted d
			ON i.Site_Id = d.Site_id
				AND i.Commodity_Id = d.Commodity_Id
				AND i.Service_Month = d.Service_Month
END
GO

ALTER TABLE [dbo].[Site_Commodity_Image] ENABLE CHANGE_TRACKING
GO
