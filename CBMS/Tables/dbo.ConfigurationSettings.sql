CREATE TABLE [dbo].[ConfigurationSettings]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[SettingName] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[SettingValue] [nvarchar] (max) COLLATE Latin1_General_CI_AI NULL,
[IsConfigurable] [bit] NOT NULL CONSTRAINT [DF_ConfigurationSettings_IsConfigurable] DEFAULT ((0)),
[CorrelationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_ConfigurationSettings_CorrelationId] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConfigurationSettings] ADD CONSTRAINT [PK_dbo.ConfigurationSettings] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
