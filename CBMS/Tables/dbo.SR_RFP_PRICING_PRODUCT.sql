CREATE TABLE [dbo].[SR_RFP_PRICING_PRODUCT]
(
[SR_RFP_PRICING_PRODUCT_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[SR_RFP_ID] [int] NOT NULL,
[PRODUCT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PRODUCT_DESCRIPTION] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_11153563_410A_4FF6_843F_83E87B9635DA_1111075840] DEFAULT (newid()),
[Sr_Service_Condition_Template_Id] [int] NULL
) ON [DBData_Sourcing]
GO


ALTER TABLE [dbo].[SR_RFP_PRICING_PRODUCT] ADD
CONSTRAINT [fk_Sr_Service_Condition_Template__SR_RFP_PRICING_PRODUCT] FOREIGN KEY ([Sr_Service_Condition_Template_Id]) REFERENCES [dbo].[Sr_Service_Condition_Template] ([Sr_Service_Condition_Template_Id])
GO


EXEC sp_addextendedproperty N'MS_Description', N'This table would store the RFP level pricing products', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_PRICING_PRODUCT', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_PRICING_PRODUCT] ADD 
CONSTRAINT [PK__SR_RFP_PRICING_P__1AEAD4B5] PRIMARY KEY CLUSTERED  ([SR_RFP_PRICING_PRODUCT_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO


ALTER TABLE [dbo].[SR_RFP_PRICING_PRODUCT] ADD
CONSTRAINT [FK_SR_RFP_PRICING_PRODUCT_SR_RFP] FOREIGN KEY ([SR_RFP_ID]) REFERENCES [dbo].[SR_RFP] ([SR_RFP_ID])
GO
