CREATE TABLE [dbo].[SUPPLIER_ACCOUNT_METER_MAP]
(
[ACCOUNT_ID] [int] NOT NULL,
[METER_ID] [int] NOT NULL,
[IS_HISTORY] [bit] NULL CONSTRAINT [Set_To_Zero14] DEFAULT (0),
[METER_ASSOCIATION_DATE] [datetime] NULL,
[METER_DISASSOCIATION_DATE] [datetime] NULL,
[Contract_ID] [int] NOT NULL,
[Supplier_Account_Config_Id] [int] NOT NULL CONSTRAINT [df_SUPPLIER_ACCOUNT_METER_MAP__Supplier_Account_Config_Id] DEFAULT ((-1)),
[Is_Rolling_Meter] [bit] NOT NULL CONSTRAINT [df_SUPPLIER_ACCOUNT_METER_MAP__Is_Rolling_Meter] DEFAULT ((0)),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NULL
) ON [DBData_Contract_RateLibrary]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Contract_Meter_Info_Transfer

DESCRIPTION: Trigger on SUPPLIER_ACCOUNT_METER_MAP to send change data to the 
	Change control service. 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy


MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV			2015-12-14	Created
	HG			2017-01-13	MAINT-4789, state name column added to the meter message
	NR			2017-09-13	SE2017-251 -  Excluded the Utility Contract types.
	HG			2019-07-25	SE2017-733		Sending ACT Commodity name in place of CBMS Commodity name
	HG			2019-07-25	SE2017-733		Sending ACT Commodity name in place of CBMS Commodity name
******/
CREATE TRIGGER [dbo].[tr_Contract_Meter_Info_Transfer]
ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP]
FOR INSERT, UPDATE, DELETE
AS
      BEGIN

            DECLARE
                  @Conversation_Handle    UNIQUEIDENTIFIER
                , @Message_Contract_Meter XML
                , @From_Service           NVARCHAR(255)
                , @Target_Service         NVARCHAR(255)
                , @Target_Contract        NVARCHAR(255);

            DECLARE @Contract_Type_Id_List VARCHAR(MAX);

            SELECT
                  @Contract_Type_Id_List = ac.App_Config_Value
            FROM  dbo.App_Config ac
            WHERE ac.App_Config_Cd = 'Exclude_Contract_Type_Id_List'
                  AND   ac.Is_Active = 1;

            SET @Message_Contract_Meter =
                  (     SELECT
                                    CASE WHEN ins.Contract_ID IS NOT NULL
                                              AND     del.Contract_ID IS NULL
                                               THEN 'I'
                                         WHEN ins.Contract_ID IS NOT NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN 'U'
                                         WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN 'D'
                                    END AS Sys_Change_Operation
                                  , isnull(ins.Contract_ID, del.Contract_ID) AS Contract_Id
                                  , isnull(ins.METER_ID, del.METER_ID) AS Meter_Id
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_Number
                                    END AS Meter_Number
                                  , isnull(ins.METER_ASSOCIATION_DATE, del.METER_ASSOCIATION_DATE) AS Meter_Association_Dt
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  ins.METER_DISASSOCIATION_DATE
                                    END AS Meter_Disassociation_Dt
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_Country_Name
                                    END AS Country_Name
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_State_Name
                                    END AS State_Name
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  isnull(act_com.ACT_Commodity_XName, com.Commodity_Name)
                                    END AS Commodity_Name
                                  , @@SERVERNAME AS Server_Name
                        FROM        INSERTED ins
                                    FULL OUTER JOIN
                                    DELETED del
                                          ON del.Contract_ID = ins.Contract_ID
                                             AND del.METER_ID = ins.METER_ID
                                    INNER JOIN
                                    Core.Client_Hier_Account cha
                                          ON ( cha.Meter_Id = isnull(ins.METER_ID, del.METER_ID))
                                    INNER JOIN
                                    dbo.Commodity com
                                          ON cha.Commodity_Id = com.Commodity_Id
                                    INNER JOIN
                                    dbo.CONTRACT c
                                          ON ( c.CONTRACT_ID = isnull(ins.Contract_ID, del.Contract_ID))
                                    LEFT OUTER JOIN
                                    (dbo.Commodity_ACT_Commodity_Map com_act_map
                                     INNER JOIN
                                     dbo.ACT_Commodity act_com
                                           ON act_com.ACT_Commodity_Id = com_act_map.ACT_Commodity_Id)
                                          ON com_act_map.Commodity_Id = com.Commodity_Id
                        WHERE       c.Is_Published_To_EC = 1
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.ufn_split(@Contract_Type_Id_List, ',') us
                                    WHERE c.CONTRACT_TYPE_ID = us.Segments )
                        GROUP BY    CASE WHEN ins.Contract_ID IS NOT NULL
                                              AND     del.Contract_ID IS NULL
                                               THEN 'I'
                                         WHEN ins.Contract_ID IS NOT NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN 'U'
                                         WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN 'D'
                                    END
                                  , isnull(ins.Contract_ID, del.Contract_ID)
                                  , isnull(ins.METER_ID, del.METER_ID)
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_Number
                                    END
                                  , isnull(ins.METER_ASSOCIATION_DATE, del.METER_ASSOCIATION_DATE)
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  ins.METER_DISASSOCIATION_DATE
                                    END
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_Country_Name
                                    END
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  isnull(act_com.ACT_Commodity_XName, com.Commodity_Name)
                                    END
                                  , CASE WHEN ins.Contract_ID IS NULL
                                              AND     del.Contract_ID IS NOT NULL
                                               THEN NULL
                                         ELSE  cha.Meter_State_Name
                                    END
                        FOR XML PATH('Contract_Meter_Change'), ELEMENTS, ROOT('Contract_Meter_Changes'));

            IF @Message_Contract_Meter IS NOT NULL
                  BEGIN

                        SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                        -- Cycle through the targets in Change_Control Targt and sent the message to each
                        DECLARE cDialog CURSOR FOR(
                        SELECT
                              Target_Service
                            , Target_Contract
                        FROM  Service_Broker_Target
                        WHERE Table_Name = 'dbo.Supplier_Account_Meter_Map');
                        OPEN cDialog;
                        FETCH NEXT FROM cDialog
                        INTO
                              @Target_Service
                            , @Target_Contract;
                        WHILE @@Fetch_Status = 0
                              BEGIN

                                    EXEC Service_Broker_Conversation_Handle_Sel
                                          @From_Service
                                        , @Target_Service
                                        , @Target_Contract
                                        , @Conversation_Handle OUTPUT;
                                    SEND ON CONVERSATION
                                          @Conversation_Handle
                                    MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Change]
                                    (@Message_Contract_Meter);

                                    FETCH NEXT FROM cDialog
                                    INTO
                                          @Target_Service
                                        , @Target_Contract;
                              END;
                        CLOSE cDialog;
                        DEALLOCATE cDialog;

                  END;

      END;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_SAMM_Del_Client_Hier_Account
	
DESCRIPTION:   
	the records get deleted from Core.Client_Hier_Account table while deteing the record from SAMM table
 
 USAGE EXAMPLES:
------------------------------------------------------------
	

BEGIN TRAN
	DELETE SUPPLIER_ACCOUNT_METER_MAP
	WHERE METER_ID = 217558 AND ACCOUNT_ID = 289867

SELECT * FROM SUPPLIER_ACCOUNT_METER_MAP WHERE METER_ID = 217558 AND ACCOUNT_ID = 289867
SELECT * FROM Core.Client_Hier_Account WHERE METER_NUMBER = 'TEST_Meter'
SELECT * FROM Core.Client_Hier_Account WHERE ACCOUNT_NUMBER = 'TEST_Account_Sup'
SELECT * FROM Core.Client_Hier_Account WHERE METER_NUMBER = 'TEST_Meter'

ROLLBACK TRAN
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/20/2010	Created
	NR		2019-10-23   Added Supplier Config Id in join condition.

******/

CREATE TRIGGER [dbo].[tr_SAMM_Del_Client_Hier_Account]
ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP]
FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;

        DELETE
        cha
        FROM
            Core.Client_Hier_Account cha
            INNER JOIN DELETED d
                ON d.METER_ID = cha.Meter_Id
                   AND  d.ACCOUNT_ID = cha.Account_Id
                   AND  d.Supplier_Account_Config_Id = cha.Supplier_Account_Config_Id;
    END;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******        
NAME:        
 tr_SAMM_ins_Client_Hier_Account      
       
DESCRIPTION:         
 This will insert the new record into Core.Clinet_Hier table for supplier account whenever the new mapping insert into the SAMM table.      
 Client_Hier_id is based on the utility site_id      
       
 USAGE EXAMPLES:      
------------------------------------------------------------      
       
BEGIN TRAN      
 INSERT INTO      
    SUPPLIER_ACCOUNT_METER_MAP      
    (      
     ACCOUNT_ID,METER_ID,IS_HISTORY,METER_ASSOCIATION_DATE,METER_DISASSOCIATION_DATE,Contract_ID,Supplier_Account_Config_Id )      
    SELECT      
    1 AS ACCOUNT_ID      
      ,2 AS METER_ID      
      ,IS_HISTORY,METER_ASSOCIATION_DATE,METER_DISASSOCIATION_DATE,Contract_ID  ,Supplier_Account_Config_Id    
    FROM      
    SUPPLIER_ACCOUNT_METER_MAP      
    WHERE      
    METER_ID = 7519      
    AND ACCOUNT_ID = 4640      
      
 SELECT * FROM SUPPLIER_ACCOUNT_METER_MAP WHERE ACCOUNT_ID = 4640 and meter_id = 7518      
 SELECT * FROM Core.Client_Hier_Account WHERE ACCOUNT_ID = 4640 and meter_id = 7518      
      
ROLLBACK TRAN      
      
       
AUTHOR INITIALS:      
 Initials  Name      
------------------------------------------------------------      
 SKA   Shobhit Kumar Agrawal      
 RR    Raghu Reddy    
 AP    Athmaram Pabbathi      
 SP    Sandeep Pigilam
 NM		Nagaraju M    
    
 MODIFICATIONS:      
 Initials Date  Modification      
------------------------------------------------------------      
 SKA  08/17/2010 Created      
   09/22/2010 Added update statement for Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY      
   09/23/2010 Added update statement for Supplier_Account_Recalc_Type_Cd/Dsc      
   10/18/2010 Added update for Column Account_Vendor_Type_ID      
   10/28/2010 Added update for Column Account_Number_Search,Account_Group_ID & WATCHLIST-GROUP-INFO-ID    
      Delete the update statement for Account_RA_WatchList    
   11/23/2010 Added the statement for Account_Number_Search & Meter_Number_Search    
 RR   06/07/2011  Added statemament to populate Display_Account_Number    
 HG   07/12/2011 Added ISNULL check in the update statement which used to update the Display_Account_Number    
      (As this Display_Account_Number coulmn gets inserted with NULL we need this validation)    
 AP   05/04/2012    Fixed the case statement used of updating Display_Account_Number (ADLDT-202)    
        with ADLDT changes we are replacing the base tables/view with CH & CHA in the SP during these this issue was was identified during Testing by CTE QC Team.    
 AKR  2012-09-28   Modified the code to add Analyst_Mapping_Cd and Is_Broker_Account as a Part of POCO    
 SP   2014-08-08   MAINT-2920, modified to update the Last_Change_Ts column to help the error message processing objects in DVDEHub depend on this column value to re process the message.    
 HG   2015-07-17 CBMS Sourcing Enh - Changes made to update Alternate_Account_Column to CHA    
 NR   2017-03-07 Contract Placeholder.    
        - Update the DMO supplier account number end date as ?Unspecified? if DMO has open end date.    
        - Populate the Supplier_Account_End_Dt as 9999-12-31 if the DMO has open end date.    
        - Removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.    
NR   2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.      
NR   2019-07-15 Add Contract - Added Supplier_Account_Config table to get the supploer dates.    
RKV  2019-12-30 D20-1762  Removed ubm details
NM	2020-06-12 MAINT-10338 Retrieve Is_Data_Entry_Only column value from supplier account instead of utility account. 
******/

CREATE TRIGGER [dbo].[tr_SAMM_ins_Client_Hier_Account]
ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP]
FOR INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Legacy_Account_Dt DATETIME;


	SELECT
		@Legacy_Account_Dt = CONVERT(DATETIME, ac.App_Config_Value)
	FROM
		dbo.App_Config ac
	WHERE
		ac.App_Config_Cd = 'Add_Contract_Legacy_Account';


	DECLARE @Total_Supplier_Accoount_Config_Ids TABLE
	(
		Account_Id							 INT
		, Total_Supplier_Accoount_Config_Ids INT
	);

	INSERT INTO @Total_Supplier_Accoount_Config_Ids
		(
			Account_Id
			, Total_Supplier_Accoount_Config_Ids
		)
	SELECT
		i.ACCOUNT_ID
		, ISNULL(COUNT(DISTINCT sac.Supplier_Account_Config_Id), 0)
	FROM
		INSERTED i
		INNER JOIN dbo.Supplier_Account_Config sac
			ON sac.Account_Id = i.ACCOUNT_ID
	GROUP BY
		i.ACCOUNT_ID;

	INSERT INTO Core.Client_Hier_Account
		(
			Account_Type
			, Client_Hier_Id
			, Account_Id
			, Account_Number
			, Account_Number_Search
			, Account_Invoice_Source_Cd
			, Account_Is_Data_Entry_Only
			, Account_Not_Expected
			, Account_Not_Managed
			, Account_Service_level_Cd --10      
			, Account_Vendor_Id
			, Account_Vendor_Name
			, Account_Vendor_Type_ID
			, Account_Eligibility_Dt
			, Account_Group_ID
			--20      
			, Meter_Id
			, Meter_Number
			, Meter_Number_Search
			, Meter_Address_ID
			, Meter_Address_Line_1
			, Meter_Address_Line_2
			, Meter_City
			, Meter_ZipCode
			, Meter_State_Id
			, Meter_State_Name --30      
			, Meter_Country_Id
			, Meter_Country_Name
			, Meter_Geo_Lat
			, Meter_Geo_Long
			, Commodity_Id
			, Rate_Id
			, Rate_Name
			, Supplier_Account_begin_Dt
			, Supplier_Account_End_Dt
			, Supplier_Contract_ID --40      
			, Supplier_Meter_Association_Date
			, Supplier_Meter_Disassociation_Date
			, Supplier_Account_Recalc_Type_Cd
			, Supplier_Account_Recalc_Type_Dsc
			, Account_Analyst_Mapping_Cd
			, Account_Is_Broker
			, Alternate_Account_Number
			, Is_Invoice_Posting_Blocked
			, Last_Change_TS
			, Display_Account_Number
			, Supplier_Account_Config_Id
		)
	SELECT
		ent.ENTITY_NAME AS Account_Type
		, ch.Client_Hier_Id
		, supact.ACCOUNT_ID
		, supact.ACCOUNT_NUMBER
		, supact.Account_Number_Search
		, supact.INVOICE_SOURCE_TYPE_ID
		, ISNULL(supact.Is_Data_Entry_Only, 0) AS Is_Data_Entry_Only
		, supact.NOT_EXPECTED
		, supact.NOT_MANAGED
		, supact.SERVICE_LEVEL_TYPE_ID --10      
		, supact.VENDOR_ID
		, sven.VENDOR_NAME
		, sven.VENDOR_TYPE_ID
		, supact.ELIGIBILITY_DATE
		, supact.ACCOUNT_GROUP_ID
		--20      
		, i.METER_ID
		, met.METER_NUMBER
		, met.Meter_Number_Search
		, met.ADDRESS_ID
		, addr.ADDRESS_LINE1
		, addr.ADDRESS_LINE2
		, addr.CITY
		, addr.ZIPCODE
		, s.STATE_ID
		, s.STATE_NAME --30      
		, cty.COUNTRY_ID
		, cty.COUNTRY_NAME
		, addr.GEO_LAT
		, addr.GEO_LONG
		, r.COMMODITY_TYPE_ID
		, r.RATE_ID
		, r.RATE_NAME
		, sac.Supplier_Account_Begin_Dt AS Supplier_Account_begin_Dt
		, CASE WHEN ent.ENTITY_NAME = 'Utility' THEN
				   supact.Supplier_Account_End_Dt
			  ELSE
				  ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
		  END AS Supplier_Account_End_Dt
		, i.Contract_ID --40      
		, i.METER_ASSOCIATION_DATE
		, i.METER_DISASSOCIATION_DATE
		, supact.Supplier_Account_Recalc_Type_Cd
		, cd.Code_Value
		, supact.Analyst_Mapping_Cd
		, supact.Is_Broker_Account
		, supact.Alternate_Account_Number
		, supact.Is_Invoice_Posting_Blocked
		, GETDATE()
		, supact.ACCOUNT_NUMBER
		, i.Supplier_Account_Config_Id
	FROM
		INSERTED i
		INNER JOIN dbo.ACCOUNT supact
			ON supact.ACCOUNT_ID = i.ACCOUNT_ID
		INNER JOIN dbo.METER met
			ON met.METER_ID = i.METER_ID
		INNER JOIN dbo.ACCOUNT utiact
			ON utiact.ACCOUNT_ID = met.ACCOUNT_ID
		INNER JOIN Core.Client_Hier ch
			ON ch.Site_Id = utiact.SITE_ID
		INNER JOIN dbo.ENTITY ent
			ON supact.ACCOUNT_TYPE_ID = ent.ENTITY_ID
		INNER JOIN dbo.VENDOR sven
			ON sven.VENDOR_ID = supact.VENDOR_ID
		INNER JOIN dbo.RATE r
			ON met.RATE_ID = r.RATE_ID
		LEFT JOIN dbo.ADDRESS addr
			ON met.ADDRESS_ID = addr.ADDRESS_ID
		LEFT JOIN dbo.STATE s
			ON addr.STATE_ID = s.STATE_ID
		LEFT JOIN dbo.COUNTRY cty
			ON s.COUNTRY_ID = cty.COUNTRY_ID
		LEFT JOIN dbo.Code cd
			ON cd.Code_Id = supact.Supplier_Account_Recalc_Type_Cd
		INNER JOIN dbo.Supplier_Account_Config sac
			ON sac.Supplier_Account_Config_Id = i.Supplier_Account_Config_Id
	WHERE
		ent.ENTITY_NAME = 'SUPPLIER'
	GROUP BY
		ent.ENTITY_NAME
		, ch.Client_Hier_Id
		, supact.ACCOUNT_ID
		, supact.ACCOUNT_NUMBER
		, supact.Account_Number_Search
		, supact.INVOICE_SOURCE_TYPE_ID
		, ISNULL(supact.Is_Data_Entry_Only, 0)
		, supact.NOT_EXPECTED
		, supact.NOT_MANAGED
		, supact.SERVICE_LEVEL_TYPE_ID --10      

		, supact.VENDOR_ID
		, sven.VENDOR_NAME
		, sven.VENDOR_TYPE_ID
		, supact.ELIGIBILITY_DATE
		, supact.ACCOUNT_GROUP_ID
		--20      
		, i.METER_ID
		, met.METER_NUMBER
		, met.Meter_Number_Search
		, met.ADDRESS_ID
		, addr.ADDRESS_LINE1
		, addr.ADDRESS_LINE2
		, addr.CITY
		, addr.ZIPCODE
		, s.STATE_ID
		, s.STATE_NAME --30      
		, cty.COUNTRY_ID
		, cty.COUNTRY_NAME
		, addr.GEO_LAT
		, addr.GEO_LONG
		, r.COMMODITY_TYPE_ID
		, r.RATE_ID
		, r.RATE_NAME
		, sac.Supplier_Account_Begin_Dt
		, CASE WHEN ent.ENTITY_NAME = 'Utility' THEN
				   supact.Supplier_Account_End_Dt
			  ELSE
				  ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
		  END
		, i.Contract_ID --40      
		, i.METER_ASSOCIATION_DATE
		, i.METER_DISASSOCIATION_DATE
		, supact.Supplier_Account_Recalc_Type_Cd
		, cd.Code_Value
		, supact.Analyst_Mapping_Cd
		, supact.Is_Broker_Account
		, supact.Alternate_Account_Number
		, supact.Is_Invoice_Posting_Blocked
		, supact.ACCOUNT_NUMBER
		, i.Supplier_Account_Config_Id;


	WITH CTE_Display_Account_Number
	AS (   SELECT
			   CHA.Account_Id
			   , CASE WHEN CHA.Account_Type = 'Utility' THEN
						  CHA.Account_Number
					 ELSE
						 ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
						 + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
									CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
							   ELSE
								   CONVERT(
											  VARCHAR
											  , MIN(ISNULL(
															  CHA.Supplier_Meter_Association_Date
															  , CHA.Supplier_Account_begin_Dt
														  )
												   ), 101
										  )
						   END + '-'
						 + CASE WHEN supact.Supplier_Account_End_Dt IS NULL THEN
									'Unspecified'
							   WHEN MAX(ISNULL(CHA.Supplier_Meter_Disassociation_Date, CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
								   CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
							   ELSE
								   CONVERT(
											  VARCHAR
											  , MAX(ISNULL(
															  CHA.Supplier_Meter_Disassociation_Date
															  , CHA.Supplier_Account_End_Dt
														  )
												   ), 101
										  )
						   END + ')'
				 END AS Display_Account_Number
		   FROM
			   Core.Client_Hier_Account CHA
			   JOIN Inserted I
				   ON CHA.Account_Id = I.ACCOUNT_ID
			   JOIN dbo.Supplier_Account_Config supact
				   ON supact.Account_Id = I.ACCOUNT_ID
			   INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
				   ON tsaci.Account_Id = supact.Account_Id
		   WHERE
			   tsaci.Total_Supplier_Accoount_Config_Ids = 1 AND supact.Created_Ts <= @Legacy_Account_Dt
		   GROUP BY
			   CHA.Account_Id
			   , CHA.Account_Number
			   , CHA.Account_Type
			   , CHA.Account_Vendor_Name
			   , CHA.Supplier_Account_begin_Dt
			   , CHA.Supplier_Account_End_Dt
			   , supact.Supplier_Account_End_Dt)
	UPDATE
		CHA
	SET
		Display_Account_Number = DAN.Display_Account_Number
		, Last_Change_TS = GETDATE()
	FROM
		Core.Client_Hier_Account CHA
		JOIN CTE_Display_Account_Number DAN
			ON CHA.Account_Id = DAN.Account_Id
	WHERE
		ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;




	UPDATE
		CHA
	SET
		CHA.Display_Account_Number = CHA.Account_Number
		, Last_Change_TS = GETDATE()
	FROM
		Core.Client_Hier_Account CHA
		INNER JOIN Inserted I
			ON I.ACCOUNT_ID = CHA.Account_Id
		INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
			ON tsaci.Account_Id = I.ACCOUNT_ID
	WHERE
		tsaci.Total_Supplier_Accoount_Config_Ids > 1
		AND ISNULL(CHA.Display_Account_Number, '') <> ISNULL(CHA.Account_Number, '');
END;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.tr_SAMM_upd_Client_Hier_Account    
    
DESCRIPTION:    
 The new entry will be insert into core.client_hier_account when a meter is associated with new account,    
 while if the account is alreday associated with the some other meter then update the record    
    
 USAGE EXAMPLES:    
------------------------------------------------------------    
    
 BEGIN TRAN    
  UPDATE SUPPLIER_ACCOUNT_METER_MAP    
  SET METER_ID = 3928    
  WHERE    
  METER_ID = 217481    
  AND ACCOUNT_ID = 4638    
    
  SELECT * FROM SUPPLIER_ACCOUNT_METER_MAP WHERE ACCOUNT_ID = 4638    
  SELECT * FROM Core.Client_Hier_Account WHERE ACCOUNT_NUMBER = 'TEST_Account_Sup' --216881    
  SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID = 4638    
 ROLLBACK TRAN    
    
AUTHOR INITIALS:    
 Initials   Name    
------------------------------------------------------------    
 SKA   Shobhit Kumar Agrawal    
 HG   Harihara Suthan G    
 RR   Raghu Reddy    
 AP   Athmaram Pabbathi    
 SP   Sandeep Pigilam    
    
 MODIFICATIONS:    
 Initials Date  Modification    
------------------------------------------------------------    
 SKA  08/23/2010 Created    
   09/22/2010 Added update statement for Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY    
   09/23/2010 Added update statement for Supplier_Account_Recalc_Type_Cd/Dsc    
   10/18/2010 Added update for Column Account_Vendor_Type_ID    
   10/28/2010 Added update for Column Account_Number_Search,Account_Group_ID & WATCHLIST-GROUP-INFO_ID    
      Delete the update statement for Account_RA_WatchList    
   11/23/2010 Added the statement for Account_Number_Search & Meter_Number_Search    
 HG  02/17/2011 Script modified to fix MAINT-504 & regression issue reported on Contract update page    
       Is_Data_Entry_Only coulmn changed to pick the value of Supplier Account (earlier it used to pull the value of Utility)    
      Regression Issue     
       Meter_Id join was missed in the join between INSERTED/DELETED tables added it now.    
       (If update happening on Meter level in SAMM existing code will work fine , whenever a contract start date and end date gets changed need to update the records for all the accounts in the contracts to correct this meter_id join added)    
           
 RR  06/08/2011  Added update statememnt to populate CTE_Display_Account_Number    
 HG  07/12/2011 Added ISNULL check in the update statement which used to update the Display_Account_Number    
      (As this Display_Account_Number coulmn gets inserted with NULL we need this validation)     
     AP  05/04/2012    Fixed the case statement used of updating Display_Account_Number (ADLDT-202)    
        with ADLDT changes we are replacing the base tables/view with CH & CHA in the SP during these this issue was was identified during Testing by CTE QC Team.    
  SP     2014-08-08  MAINT-2920, modified to update the Last_Change_Ts column to help the error message processing objects in DVDEHub depend on this column value to re process the message.    
  HG  2015-07-17 CBMS Sourcing Enh - Changes made to update Alternate_Account_Column to CHA          
    
  NR  2017-03-07  Contract Placeholder.    
        - Update the DMO supplier account number end date as ?Unspecified? if DMO has open end date.    
        - Populate the Supplier_Account_End_Dt as 9999-12-31 if the DMO has open end date.    
        - Removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.    
    
    NR   2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.      
 NR   2019-07-15 Add Contract - Added Supplier_Account_Config table to get the supplier dates.    
RKV  2019-12-30 D20-1762  Removed ubm details   
******/

CREATE TRIGGER [dbo].[tr_SAMM_upd_Client_Hier_Account]
ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP]
FOR UPDATE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Legacy_Account_Dt DATETIME;


        SELECT
            @Legacy_Account_Dt = CONVERT(DATETIME, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Legacy_Account';


        DECLARE @Total_Supplier_Accoount_Config_Ids TABLE
              (
                  Account_Id INT
                  , Total_Supplier_Accoount_Config_Ids INT
              );

        INSERT INTO @Total_Supplier_Accoount_Config_Ids
             (
                 Account_Id
                 , Total_Supplier_Accoount_Config_Ids
             )
        SELECT
            i.ACCOUNT_ID
            , ISNULL(COUNT(DISTINCT sac.Supplier_Account_Config_Id), 0)
        FROM
            INSERTED i
            INNER JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = i.ACCOUNT_ID
        GROUP BY
            i.ACCOUNT_ID;


        MERGE INTO Core.Client_Hier_Account AS tgt
        USING (   SELECT
                        ent.ENTITY_NAME AS Account_Type
                        , ch.Client_Hier_Id
                        , supact.ACCOUNT_ID
                        , supact.ACCOUNT_NUMBER
                        , supact.Account_Number_Search
                        , supact.INVOICE_SOURCE_TYPE_ID
                        , ISNULL(supact.Is_Data_Entry_Only, 0) AS Is_Data_Entry_Only
                        , supact.NOT_EXPECTED
                        , supact.NOT_MANAGED
                        , supact.SERVICE_LEVEL_TYPE_ID
                        , supact.VENDOR_ID
                        , sven.VENDOR_NAME
                        , supact.ELIGIBILITY_DATE
                        , supact.ACCOUNT_GROUP_ID
                        , i.METER_ID
                        , met.METER_NUMBER
                        , met.Meter_Number_Search
                        , met.ADDRESS_ID
                        , addr.ADDRESS_LINE1
                        , addr.ADDRESS_LINE2
                        , addr.CITY
                        , addr.ZIPCODE
                        , s.STATE_ID
                        , s.STATE_NAME
                        , cty.COUNTRY_ID
                        , cty.COUNTRY_NAME
                        , addr.GEO_LAT
                        , addr.GEO_LONG
                        , r.COMMODITY_TYPE_ID
                        , r.RATE_ID
                        , r.RATE_NAME
                        , sac.Supplier_Account_Begin_Dt AS Supplier_Account_begin_Dt
                        , CASE WHEN ent.ENTITY_NAME = 'Utility' THEN sac.Supplier_Account_End_Dt
                              ELSE ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
                          END AS Supplier_Account_End_Dt
                        , i.Contract_ID AS Supplier_Contract_ID
                        , i.METER_ASSOCIATION_DATE AS Supplier_Meter_Association_Date
                        , i.METER_DISASSOCIATION_DATE AS Supplier_Meter_Disassociation_Date
                        , supact.Supplier_Account_Recalc_Type_Cd
                        , cd.Code_Value AS Supplier_Account_Recalc_Type_Dsc
                        , d.METER_ID AS Deleted_Meter_ID
                        , sven.VENDOR_TYPE_ID AS Account_Vendor_Type_ID
                        , supact.Alternate_Account_Number
                        , sac.Supplier_Account_Config_Id
                  FROM
                        INSERTED i
                        INNER JOIN DELETED d
                            ON i.ACCOUNT_ID = d.ACCOUNT_ID
                               AND  i.METER_ID = d.METER_ID
                        INNER JOIN dbo.ACCOUNT supact
                            ON supact.ACCOUNT_ID = i.ACCOUNT_ID
                        INNER JOIN dbo.METER met
                            ON met.METER_ID = i.METER_ID
                        INNER JOIN dbo.ACCOUNT utiact
                            ON utiact.ACCOUNT_ID = met.ACCOUNT_ID
                        INNER JOIN Core.Client_Hier ch
                            ON ch.Site_Id = utiact.SITE_ID
                        INNER JOIN dbo.ENTITY ent
                            ON supact.ACCOUNT_TYPE_ID = ent.ENTITY_ID
                        INNER JOIN dbo.VENDOR sven
                            ON sven.VENDOR_ID = supact.VENDOR_ID
                        INNER JOIN dbo.RATE r
                            ON met.RATE_ID = r.RATE_ID
                        LEFT JOIN dbo.ADDRESS addr
                            ON met.ADDRESS_ID = addr.ADDRESS_ID
                        LEFT JOIN dbo.STATE s
                            ON addr.STATE_ID = s.STATE_ID
                        LEFT JOIN dbo.COUNTRY cty
                            ON s.COUNTRY_ID = cty.COUNTRY_ID
                        LEFT JOIN dbo.Code cd
                            ON cd.Code_Id = supact.Supplier_Account_Recalc_Type_Cd
                        LEFT JOIN Core.Client_Hier_Account cah
                            ON cah.Meter_Id = i.METER_ID
                               AND  cah.Account_Id = i.ACCOUNT_ID
                        INNER JOIN dbo.Supplier_Account_Config sac
                            ON i.Supplier_Account_Config_Id = sac.Supplier_Account_Config_Id
                  GROUP BY
                      ent.ENTITY_NAME
                      , ch.Client_Hier_Id
                      , supact.ACCOUNT_ID
                      , supact.ACCOUNT_NUMBER
                      , supact.Account_Number_Search
                      , supact.INVOICE_SOURCE_TYPE_ID
                      , ISNULL(supact.Is_Data_Entry_Only, 0)
                      , supact.NOT_EXPECTED
                      , supact.NOT_MANAGED
                      , supact.SERVICE_LEVEL_TYPE_ID
                      , supact.VENDOR_ID
                      , sven.VENDOR_NAME
                      , supact.ELIGIBILITY_DATE
                      , supact.ACCOUNT_GROUP_ID
                      , i.METER_ID
                      , met.METER_NUMBER
                      , met.Meter_Number_Search
                      , met.ADDRESS_ID
                      , addr.ADDRESS_LINE1
                      , addr.ADDRESS_LINE2
                      , addr.CITY
                      , addr.ZIPCODE
                      , s.STATE_ID
                      , s.STATE_NAME
                      , cty.COUNTRY_ID
                      , cty.COUNTRY_NAME
                      , addr.GEO_LAT
                      , addr.GEO_LONG
                      , r.COMMODITY_TYPE_ID
                      , r.RATE_ID
                      , r.RATE_NAME
                      , sac.Supplier_Account_Begin_Dt
                      , CASE WHEN ent.ENTITY_NAME = 'Utility' THEN sac.Supplier_Account_End_Dt
                            ELSE ISNULL(sac.Supplier_Account_End_Dt, '9999-12-31')
                        END
                      , i.Contract_ID
                      , i.METER_ASSOCIATION_DATE
                      , i.METER_DISASSOCIATION_DATE
                      , supact.Supplier_Account_Recalc_Type_Cd
                      , cd.Code_Value
                      , d.METER_ID
                      , sven.VENDOR_TYPE_ID
                      , supact.Alternate_Account_Number
                      , sac.Supplier_Account_Config_Id) src
        ON (   src.ACCOUNT_ID = tgt.Account_Id
               AND  src.Deleted_Meter_ID = tgt.Meter_Id
               AND  src.Supplier_Account_Config_Id = tgt.Supplier_Account_Config_Id)
        WHEN MATCHED THEN UPDATE SET
                              Account_Vendor_Id = src.VENDOR_ID
                              , Account_Vendor_Name = src.VENDOR_NAME
                              , Account_Vendor_Type_ID = src.Account_Vendor_Type_ID
                              , Meter_Id = src.METER_ID
                              , Meter_Number = src.METER_NUMBER
                              , Meter_Number_Search = src.Meter_Number_Search
                              , Meter_Address_ID = src.ADDRESS_ID
                              , Meter_Address_Line_1 = src.ADDRESS_LINE1
                              , Meter_Address_Line_2 = src.ADDRESS_LINE2
                              , Meter_City = src.CITY
                              , Meter_ZipCode = src.ZIPCODE
                              , Meter_State_Id = src.STATE_ID
                              , Meter_State_Name = src.STATE_NAME
                              , Meter_Country_Id = src.COUNTRY_ID
                              , Meter_Country_Name = src.COUNTRY_NAME
                              , Meter_Geo_Lat = src.GEO_LAT
                              , Meter_Geo_Long = src.GEO_LONG
                              , Commodity_Id = src.COMMODITY_TYPE_ID
                              , Supplier_Account_begin_Dt = src.Supplier_Account_begin_Dt
                              , Supplier_Account_End_Dt = src.Supplier_Account_End_Dt
                              , Supplier_Contract_ID = src.Supplier_Contract_ID
                              , Supplier_Meter_Association_Date = src.Supplier_Meter_Association_Date
                              , Supplier_Meter_Disassociation_Date = src.Supplier_Meter_Disassociation_Date
                              , Supplier_Account_Recalc_Type_Cd = src.Supplier_Account_Recalc_Type_Cd
                              , Supplier_Account_Recalc_Type_Dsc = src.Supplier_Account_Recalc_Type_Dsc
                              , Alternate_Account_Number = src.Alternate_Account_Number
                              , Last_Change_TS = GETDATE()
        WHEN NOT MATCHED THEN INSERT (Account_Type
                                      , Client_Hier_Id
                                      , Account_Id
                                      , Account_Number
                                      , Account_Number_Search
                                      , Account_Invoice_Source_Cd
                                      , Account_Is_Data_Entry_Only
                                      , Account_Not_Expected
                                      , Account_Not_Managed
                                      , Account_Service_level_Cd            --10    
                                      , Account_Vendor_Id
                                      , Account_Vendor_Name
                                      , Account_Vendor_Type_ID
                                      , Account_Eligibility_Dt
                                      , Account_Group_ID
                                                                            --20    
                                      , Meter_Id
                                      , Meter_Number
                                      , Meter_Number_Search
                                      , Meter_Address_ID
                                      , Meter_Address_Line_1
                                      , Meter_Address_Line_2
                                      , Meter_City
                                      , Meter_ZipCode
                                      , Meter_State_Id
                                      , Meter_State_Name                    --30    
                                      , Meter_Country_Id
                                      , Meter_Country_Name
                                      , Meter_Geo_Lat
                                      , Meter_Geo_Long
                                      , Commodity_Id
                                      , Rate_Id
                                      , Rate_Name
                                      , Supplier_Account_begin_Dt
                                      , Supplier_Account_End_Dt
                                      , Supplier_Contract_ID                --40    
                                      , Supplier_Meter_Association_Date
                                      , Supplier_Meter_Disassociation_Date
                                      , Supplier_Account_Recalc_Type_Cd
                                      , Supplier_Account_Recalc_Type_Dsc    --44    
                                      , Alternate_Account_Number
                                      , Last_Change_TS
                                      , Display_Account_Number
                                      , Supplier_Account_Config_Id)
                              VALUES
                                  (src.Account_Type
                                   , src.Client_Hier_Id
                                   , src.ACCOUNT_ID
                                   , src.ACCOUNT_NUMBER
                                   , src.Account_Number_Search
                                   , src.INVOICE_SOURCE_TYPE_ID
                                   , src.Is_Data_Entry_Only
                                   , src.NOT_EXPECTED
                                   , src.NOT_MANAGED
                                   , src.SERVICE_LEVEL_TYPE_ID              --10    

                                   , src.VENDOR_ID
                                   , src.VENDOR_NAME
                                   , src.Account_Vendor_Type_ID
                                   , src.ELIGIBILITY_DATE
                                   , src.ACCOUNT_GROUP_ID
                                                                            --20    
                                   , src.METER_ID
                                   , src.METER_NUMBER
                                   , src.Meter_Number_Search
                                   , src.ADDRESS_ID
                                   , src.ADDRESS_LINE1
                                   , src.ADDRESS_LINE2
                                   , src.CITY
                                   , src.ZIPCODE
                                   , src.STATE_ID
                                   , src.STATE_NAME                         --30    
                                   , src.COUNTRY_ID
                                   , src.COUNTRY_NAME
                                   , src.GEO_LAT
                                   , src.GEO_LONG
                                   , src.COMMODITY_TYPE_ID
                                   , src.RATE_ID
                                   , src.RATE_NAME
                                   , src.Supplier_Account_begin_Dt
                                   , src.Supplier_Account_End_Dt
                                   , src.Supplier_Contract_ID               --40    
                                   , src.Supplier_Meter_Association_Date
                                   , src.Supplier_Meter_Disassociation_Date
                                   , src.Supplier_Account_Recalc_Type_Cd
                                   , src.Supplier_Account_Recalc_Type_Dsc   --44    
                                   , src.Alternate_Account_Number
                                   , GETDATE()
                                   , src.ACCOUNT_NUMBER
                                   , src.Supplier_Account_Config_Id);

        WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + CASE WHEN supact.Supplier_Account_End_Dt IS NULL THEN 'Unspecified'
                                    WHEN MAX(ISNULL(CHA.Supplier_Meter_Disassociation_Date, CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                        CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)), 101)
                                END + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN Inserted I
                        ON CHA.Account_Id = I.ACCOUNT_ID
                    INNER JOIN dbo.Supplier_Account_Config supact
                        ON supact.Account_Id = I.ACCOUNT_ID
                    INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                        ON tsaci.Account_Id = supact.Account_Id
               WHERE
                    tsaci.Total_Supplier_Accoount_Config_Ids = 1
                    AND supact.Created_Ts <= @Legacy_Account_Dt
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
                   , supact.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;


        UPDATE
            CHA
        SET
            CHA.Display_Account_Number = CHA.Account_Number
            , CHA.Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            INNER JOIN Inserted I
                ON I.ACCOUNT_ID = CHA.Account_Id
            INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                ON tsaci.Account_Id = I.ACCOUNT_ID
        WHERE
            tsaci.Total_Supplier_Accoount_Config_Ids > 1
            AND ISNULL(CHA.Display_Account_Number, '') <> ISNULL(CHA.Account_Number, '');


    END;;;
GO
ALTER TABLE [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ADD CONSTRAINT [SUPPLIER_ACCOUNT_METER_MAP_PK] PRIMARY KEY CLUSTERED  ([ACCOUNT_ID], [METER_ID], [Contract_ID], [Supplier_Account_Config_Id]) ON [DBData_Contract_RateLibrary]
GO
CREATE NONCLUSTERED INDEX [SUPPLIER_ACCOUNT_METER_MAP_N_1] ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ([ACCOUNT_ID], [IS_HISTORY]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SUPPLIER_ACCOUNT_METER_MAP__Contract_ID] ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ([Contract_ID], [ACCOUNT_ID]) INCLUDE ([METER_ASSOCIATION_DATE], [METER_DISASSOCIATION_DATE]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SUPPLIER_ACCOUNT_METER_MAP_1] ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ([METER_DISASSOCIATION_DATE], [ACCOUNT_ID], [METER_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_SUPPLIER_ACCOUNT_METER_MAP__ACCOUNT_ID__Meter_id] ON [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ([METER_ID], [ACCOUNT_ID], [Contract_ID]) INCLUDE ([METER_ASSOCIATION_DATE], [METER_DISASSOCIATION_DATE]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[SUPPLIER_ACCOUNT_METER_MAP] WITH NOCHECK ADD CONSTRAINT [ACC_SUPP_ACC_METER_MAP_FK] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
ALTER TABLE [dbo].[SUPPLIER_ACCOUNT_METER_MAP] ADD CONSTRAINT [METER_SUPP_ACC_METER_MAP_FK] FOREIGN KEY ([METER_ID]) REFERENCES [dbo].[METER] ([METER_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table maps the supplier account created for a contract to the meters of the contract', 'SCHEMA', N'dbo', 'TABLE', N'SUPPLIER_ACCOUNT_METER_MAP', NULL, NULL
GO
