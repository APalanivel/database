CREATE TABLE [dbo].[Client_Links]
(
[Client_Id] [int] NOT NULL,
[Link_Type_Cd] [int] NOT NULL,
[Link_URL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Link_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Client Relevant URLs displayed in Dv - only available for entry in Sustainability tab in CBMS', 'SCHEMA', N'dbo', 'TABLE', N'Client_Links', NULL, NULL
GO

ALTER TABLE [dbo].[Client_Links] ADD 
CONSTRAINT [pk_Client_Links] PRIMARY KEY CLUSTERED  ([Client_Id], [Link_Name]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
ALTER TABLE [dbo].[Client_Links] ADD
CONSTRAINT [fk_Client_Links_Client] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
