CREATE TABLE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map]
(
[Account_Invoice_Collection_Month_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Invoice_Begin_Dt] [date] NOT NULL,
[Invoice_End_Dt] [date] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_Month_Cu_Invoice_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map] ADD CONSTRAINT [pk_Account_Invoice_Collection_Month_Cu_Invoice_Map] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_Month_Id], [Cu_Invoice_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Invoice_Collection_Month_Cu_Invoice_Map__Cu_Invoice_Id] ON [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map] ([Cu_Invoice_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_Month_Cu_Invoice_Map] ADD CONSTRAINT [fk_Account_Invoice_Collection_Month__Account_Invoice_Collection_Month_Cu_Invoice_Map] FOREIGN KEY ([Account_Invoice_Collection_Month_Id]) REFERENCES [dbo].[Account_Invoice_Collection_Month] ([Account_Invoice_Collection_Month_Id])
GO
