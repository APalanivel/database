CREATE TABLE [dbo].[CBMS_Sitegroup_Rule]
(
[CBMS_Sitegroup_Rule_Id] [int] NOT NULL IDENTITY(1, 1),
[Cbms_Sitegroup_Id] [int] NOT NULL,
[Cbms_Rule_Filter_Id] [int] NOT NULL,
[Is_Inclusive] [bit] NOT NULL CONSTRAINT [DFCBMS_Sitegroup_RuleIs_Inclusive] DEFAULT ((0)),
[Display_Seq] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFCBMS_Sitegroup_RuleCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFCBMS_Sitegroup_RuleLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_Sitegroup_Rule] ADD CONSTRAINT [pk_CBMS_Sitegroup_Rule] PRIMARY KEY CLUSTERED  ([CBMS_Sitegroup_Rule_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CBMS_Sitegroup_Rule] ADD CONSTRAINT [fk_Cbms_Sitegroup__CBMS_Sitegroup_Rule] FOREIGN KEY ([Cbms_Sitegroup_Id]) REFERENCES [dbo].[Cbms_Sitegroup] ([Cbms_Sitegroup_Id])
GO
