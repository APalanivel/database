CREATE TABLE [dbo].[CU_EXCEPTION_DETAIL]
(
[CU_EXCEPTION_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CU_EXCEPTION_ID] [int] NOT NULL,
[EXCEPTION_TYPE_ID] [int] NOT NULL,
[EXCEPTION_STATUS_TYPE_ID] [int] NOT NULL,
[OPENED_DATE] [datetime] NOT NULL,
[IS_CLOSED] [bit] NOT NULL,
[CLOSED_REASON_TYPE_ID] [int] NULL,
[CLOSED_BY_ID] [int] NULL,
[CLOSED_DATE] [datetime] NULL,
[Account_ID] [int] NULL,
[Commodity_Id] [int] NULL
) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [IX_CU_EXCEPTION_DETAIL__IS_CLOSED__CU_EXCEPTION_ID__EXCEPTION_TYPE_ID] ON [dbo].[CU_EXCEPTION_DETAIL] ([IS_CLOSED], [CU_EXCEPTION_ID], [EXCEPTION_TYPE_ID]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Invoice Exception Detail including status and references CU_Exception_Closed_Reason', 'SCHEMA', N'dbo', 'TABLE', N'CU_EXCEPTION_DETAIL', NULL, NULL
GO

ALTER TABLE [dbo].[CU_EXCEPTION_DETAIL] ADD 
CONSTRAINT [PK_CU_EXCEPTION_DETAIL] PRIMARY KEY CLUSTERED  ([CU_EXCEPTION_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [cu_exception_detail__cu_exception_id__EXCEPTION_TYPE_ID__is_closed] ON [dbo].[CU_EXCEPTION_DETAIL] ([CU_EXCEPTION_ID], [EXCEPTION_TYPE_ID], [IS_CLOSED]) INCLUDE ([OPENED_DATE]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[CU_EXCEPTION_DETAIL] ADD
CONSTRAINT [FK_CU_EXCEPTION_DETAIL_CU_EXCEPTION] FOREIGN KEY ([CU_EXCEPTION_ID]) REFERENCES [dbo].[CU_EXCEPTION] ([CU_EXCEPTION_ID])
ALTER TABLE [dbo].[CU_EXCEPTION_DETAIL] ADD
CONSTRAINT [FK_CU_EXCEPTION_DETAIL_CU_EXCEPTION_TYPE] FOREIGN KEY ([EXCEPTION_TYPE_ID]) REFERENCES [dbo].[CU_EXCEPTION_TYPE] ([CU_EXCEPTION_TYPE_ID])



GO
GRANT SELECT ON  [dbo].[CU_EXCEPTION_DETAIL] TO [BPOReportRole]
GO
