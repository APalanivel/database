CREATE TABLE [dbo].[Counterparty_Client_Contact_Map]
(
[Counterparty_Id] [int] NOT NULL,
[Client_Id] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Counterparty_Client_Contact_Map] ADD CONSTRAINT [pk_Counterparty_Client_Contact_Map] PRIMARY KEY CLUSTERED  ([Counterparty_Id], [Client_Id], [Contact_Info_Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Counterparty_Client_Contact_Map] ADD CONSTRAINT [fk_Counterparty_Client_Contact_Map__Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[Counterparty_Client_Contact_Map] ADD CONSTRAINT [fk_Counterparty_Client_Contact_Map__Contact_Info_Id] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [dbo].[Counterparty_Client_Contact_Map] ADD CONSTRAINT [fk_Counterparty_Client_Contact_Map__Counterparty_Id] FOREIGN KEY ([Counterparty_Id]) REFERENCES [dbo].[RM_COUNTERPARTY] ([RM_COUNTERPARTY_ID])
GO
