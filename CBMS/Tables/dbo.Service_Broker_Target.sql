CREATE TABLE [dbo].[Service_Broker_Target]
(
[Table_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Target_Service] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Target_Contract] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Target service/contract where the base table changes needs to be pushed', 'SCHEMA', N'dbo', 'TABLE', N'Service_Broker_Target', NULL, NULL
GO

ALTER TABLE [dbo].[Service_Broker_Target] ADD CONSTRAINT [pk_Change_Control_Target] PRIMARY KEY CLUSTERED  ([Table_Name], [Target_Service]) ON [DB_DATA01]
GO
