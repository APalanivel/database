CREATE TABLE [Trade].[Deal_Ticket_Client_Contact]
(
[Deal_Ticket_Client_Contact_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Id] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Approval_Cbms_Image_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Contact__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Contact__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact] ADD CONSTRAINT [pk_Deal_Ticket_Client_Contact] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Contact_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Client_Contact__Contact_Info_Id] ON [Trade].[Deal_Ticket_Client_Contact] ([Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact] ADD CONSTRAINT [un_Deal_Ticket_Client_Contact__Deal_Ticket_Id__Contact_Info_Id] UNIQUE NONCLUSTERED  ([Deal_Ticket_Id], [Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact] ADD CONSTRAINT [fk_Contact_Info__Deal_Ticket_Client_Contact] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact] ADD CONSTRAINT [fk_Deal_Ticket__Deal_Ticket_Client_Contact] FOREIGN KEY ([Deal_Ticket_Id]) REFERENCES [Trade].[Deal_Ticket] ([Deal_Ticket_Id])
GO
