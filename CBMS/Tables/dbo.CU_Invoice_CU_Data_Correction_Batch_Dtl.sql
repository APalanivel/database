CREATE TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl]
(
[CU_Invoice_CU_Data_Correction_Batch_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[CU_Invoice_CU_Data_Correction_Batch_Id] [int] NOT NULL,
[From_Account_Id] [int] NOT NULL,
[To_Account_Id] [int] NULL,
[Cu_Invoice_Id] [int] NULL,
[Service_Month] [date] NULL,
[Action_Cd] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_CU_Invoice_CU_Data_Correction_Batch_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_CU_Invoice_CU_Data_Correction_Batch_Dtl__Last_Change_Ts] DEFAULT (getdate()),
[Error_Msg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl] ADD CONSTRAINT [pk_CU_Invoice_CU_Data_Correction_Batch_Dtl] PRIMARY KEY CLUSTERED  ([CU_Invoice_CU_Data_Correction_Batch_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl] ADD CONSTRAINT [un_CU_Invoice_CU_Data_Correction_Batch_Dtl] UNIQUE NONCLUSTERED  ([CU_Invoice_CU_Data_Correction_Batch_Id], [From_Account_Id], [To_Account_Id], [Cu_Invoice_Id], [Service_Month]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[CU_Invoice_CU_Data_Correction_Batch_Dtl] ADD CONSTRAINT [fk_CU_Invoice_CU_Data_Correction_Batch__CU_Invoice_CU_Data_Correction_Batch_Dtl] FOREIGN KEY ([CU_Invoice_CU_Data_Correction_Batch_Id]) REFERENCES [dbo].[CU_Invoice_CU_Data_Correction_Batch] ([CU_Invoice_CU_Data_Correction_Batch_Id])
GO
