CREATE TABLE [dbo].[seSupplyDemandOverview]
(
[SupplyDemandOverviewId] [int] NOT NULL IDENTITY(1, 1),
[YearNumber] [smallint] NOT NULL,
[YearLabel] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DryProduction] [int] NOT NULL,
[NetImports] [int] NOT NULL,
[SupplementalGasFuels] [int] NOT NULL,
[TotalGasSupply] [int] NOT NULL,
[ResidentialCommercial] [int] NOT NULL,
[Industrial] [int] NOT NULL,
[PowerGeneration] [int] NOT NULL,
[OtherConsumption] [int] NOT NULL,
[TotalConsumption] [int] NOT NULL,
[Imbalance] [int] NOT NULL,
[BalanceFactor] [int] NOT NULL,
[TotalChange] [int] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seSupplyDemandOverview', NULL, NULL
GO

ALTER TABLE [dbo].[seSupplyDemandOverview] ADD 
CONSTRAINT [PK_seSupplyDemandOverview] PRIMARY KEY CLUSTERED  ([SupplyDemandOverviewId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
