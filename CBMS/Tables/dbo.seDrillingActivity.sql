CREATE TABLE [dbo].[seDrillingActivity]
(
[DrillingActivityId] [int] NOT NULL IDENTITY(1, 1),
[ReportDate] [datetime] NOT NULL,
[Volume] [int] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated.  Last data 5/6/2009', 'SCHEMA', N'dbo', 'TABLE', N'seDrillingActivity', NULL, NULL
GO

ALTER TABLE [dbo].[seDrillingActivity] ADD 
CONSTRAINT [PK_seDrillingActivity] PRIMARY KEY CLUSTERED  ([DrillingActivityId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
