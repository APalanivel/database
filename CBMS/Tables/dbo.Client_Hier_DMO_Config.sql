CREATE TABLE [dbo].[Client_Hier_DMO_Config]
(
[Client_Hier_DMO_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[DMO_Start_Dt] [date] NOT NULL,
[DMO_End_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Hier_DMO_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Client_Hier_DMO_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_DMO_Config] ADD CONSTRAINT [pk_Client_Hier_DMO_Config] PRIMARY KEY NONCLUSTERED  ([Client_Hier_DMO_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Hier_DMO_Config] ADD CONSTRAINT [unc_Client_Hier_DMO_Config__Client_Hier_Id__Commodity_Id__IPO_Start_Dt__IPO_End_Dt] UNIQUE CLUSTERED  ([Client_Hier_Id], [Commodity_Id], [DMO_Start_Dt], [DMO_End_Dt]) ON [DB_DATA01]
GO
