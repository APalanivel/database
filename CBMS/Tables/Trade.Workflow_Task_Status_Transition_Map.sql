CREATE TABLE [Trade].[Workflow_Task_Status_Transition_Map]
(
[Workflow_Task_Status_Transition_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Task_Status_Map_Id] [int] NOT NULL,
[Workflow_Transition_Id] [int] NOT NULL,
[Descendant_Workflow_Status_Map_Id] [int] NOT NULL,
[Workflow_Transition_Level_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Task_Status_Transition_Map__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Task_Status_Transition_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Task_Status_Transition_Map] ADD CONSTRAINT [pk_Workflow_Task_Status_Transition_Map] PRIMARY KEY CLUSTERED  ([Workflow_Task_Status_Transition_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Task_Status_Transition_Map] ADD CONSTRAINT [un_Workflow_Task_Status_Transition_Map__Workflow_Task_Status_Map_Id__Workflow_Transition_Id] UNIQUE NONCLUSTERED  ([Workflow_Task_Status_Map_Id], [Workflow_Transition_Id]) ON [DB_DATA01]
GO
