CREATE TABLE [dbo].[REV]
(
[REV_ID] [int] NOT NULL IDENTITY(1, 1),
[REV_DATE] [datetime] NOT NULL,
[REV_DESCRIPTION] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Deprecated]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated.  Last Entry on 8/6/2007', 'SCHEMA', N'dbo', 'TABLE', N'REV', NULL, NULL
GO

ALTER TABLE [dbo].[REV] ADD 
CONSTRAINT [REV_PK] PRIMARY KEY CLUSTERED  ([REV_ID]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]

GO
