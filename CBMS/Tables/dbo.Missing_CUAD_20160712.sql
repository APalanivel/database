CREATE TABLE [dbo].[Missing_CUAD_20160712]
(
[Cost_Usage_Account_Dtl_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Bucket_Value] [decimal] (28, 10) NOT NULL,
[UOM_Type_Id] [int] NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[Created_By_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_Ts] [datetime] NOT NULL,
[Updated_By_Id] [int] NULL,
[ACCOUNT_ID] [int] NOT NULL,
[Row_Version] [timestamp] NOT NULL,
[Data_Source_Cd] [int] NULL,
[Client_Hier_Id] [int] NOT NULL
) ON [DB_DATA01]
GO
