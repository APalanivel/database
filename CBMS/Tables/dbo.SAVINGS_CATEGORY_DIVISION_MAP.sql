CREATE TABLE [dbo].[SAVINGS_CATEGORY_DIVISION_MAP]
(
[SAVINGS_CATEGORY_TYPE_ID] [int] NOT NULL,
[DIVISION_ID] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Users are required to select different categories of savings that the client (division) has approved Summit to ''count'' as savings.', 'SCHEMA', N'dbo', 'TABLE', N'SAVINGS_CATEGORY_DIVISION_MAP', NULL, NULL
GO

ALTER TABLE [dbo].[SAVINGS_CATEGORY_DIVISION_MAP] ADD 
CONSTRAINT [SAVINGS_CATEGORY_DIVISION_MAP_PK] PRIMARY KEY CLUSTERED  ([SAVINGS_CATEGORY_TYPE_ID], [DIVISION_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]


CREATE NONCLUSTERED INDEX [SAVINGS_CAT_DIVISION_MAP_N_2] ON [dbo].[SAVINGS_CATEGORY_DIVISION_MAP] ([DIVISION_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

ALTER TABLE [dbo].[SAVINGS_CATEGORY_DIVISION_MAP] ADD
CONSTRAINT [ENTITY_SAVINGS_CAT_DIVISION_MAP_FK] FOREIGN KEY ([SAVINGS_CATEGORY_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SAVINGS_CATEGORY_DIVISION_MAP] ADD
CONSTRAINT [DIVISION_SAVINGS_CAT_DIVISION_MAP_FK] FOREIGN KEY ([DIVISION_ID]) REFERENCES [dbo].[Division_Dtl] ([SiteGroup_Id])









GO
