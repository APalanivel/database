CREATE TABLE [DBAM].[REPL_IDENT_RANGE_MGMT]
(
[Table_name] [sys].[sysname] NOT NULL,
[Database_name] [sys].[sysname] NOT NULL,
[start_range] [int] NOT NULL,
[END_range] [int] NOT NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Manual method to manage & audit replication identity ranges.  Auto-management did not work for us.', 'SCHEMA', N'DBAM', 'TABLE', N'REPL_IDENT_RANGE_MGMT', NULL, NULL
GO

ALTER TABLE [DBAM].[REPL_IDENT_RANGE_MGMT] ADD CONSTRAINT [PK_REPL_IDENT_RANGE_MGMT__Table_name__Database_Name] PRIMARY KEY CLUSTERED  ([Table_name], [Database_name]) ON [DB_DATA01]
GO
