CREATE TABLE [dbo].[CEA_HOMEPAGE_CLIENTS_DISPLAY_LIST]
(
[CEA_HOMEPAGE_CLIENTS_DISPLAY_LIST_ID] [int] NOT NULL IDENTITY(1, 1),
[USER_INFO_ID] [int] NOT NULL,
[CLIENT_ID] [int] NOT NULL,
[CLIENT_DISPLAY_ORDER] [int] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'A way for the CEA users to set up a favorite order on their homepage, listing client names', 'SCHEMA', N'dbo', 'TABLE', N'CEA_HOMEPAGE_CLIENTS_DISPLAY_LIST', NULL, NULL
GO
