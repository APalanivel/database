CREATE TABLE [dbo].[Sr_Service_Condition_Category]
(
[Sr_Service_Condition_Category_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Category_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Required] [bit] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Category__Is_Required] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Category__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Category__Last_Change_Ts] DEFAULT (getdate()),
[Display_Seq] [int] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Category] ADD CONSTRAINT [pk_Sr_Service_Condition_Category] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Category_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Category] ADD CONSTRAINT [un_Sr_Service_Condition_Category__Category_Name] UNIQUE NONCLUSTERED  ([Category_Name]) ON [DB_DATA01]
GO
