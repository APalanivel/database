CREATE TABLE [dbo].[SR_RFP_ARCHIVE_ACCOUNT]
(
[SR_RFP_ARCHIVE_ACCOUNT_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_RFP_ACCOUNT_ID] [int] NOT NULL,
[ACCOUNT_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SITE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UTILITY_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SERVICE_LEVEL_NAME] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LP_SIZE] [decimal] (32, 16) NULL,
[CONTRACTING_ENTITY] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTRACTING_ENTITY_DETAILS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TARIFF_TRANSPORT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DUNS_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELIGIBLE_DATE] [datetime] NULL
) ON [DBData_Sourcing]
GO
ALTER TABLE [dbo].[SR_RFP_ARCHIVE_ACCOUNT] ADD CONSTRAINT [PK__SR_RFP_ARCHIVE_A__012B02B2] PRIMARY KEY CLUSTERED  ([SR_RFP_ARCHIVE_ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
ALTER TABLE [dbo].[SR_RFP_ARCHIVE_ACCOUNT] WITH NOCHECK ADD CONSTRAINT [FK_SR_RFP_ARCHIVE_ACCOUNT_SR_RFP_ACCOUNT] FOREIGN KEY ([SR_RFP_ACCOUNT_ID]) REFERENCES [dbo].[SR_RFP_ACCOUNT] ([SR_RFP_ACCOUNT_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the account level information for a RFP when a RFP account/group is closed. This table would store the archived information related to an account in CBMS, so that if these information change in the future, the archived info at the time of closing this RFP account can be referenced from this table.', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ARCHIVE_ACCOUNT', NULL, NULL
GO
