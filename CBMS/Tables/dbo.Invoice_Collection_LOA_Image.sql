CREATE TABLE [dbo].[Invoice_Collection_LOA_Image]
(
[Invoice_Collection_LOA_Image_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_LOA_Id] [int] NOT NULL,
[Cbms_Image_Id] [int] NOT NULL,
[LOA_Description] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_LOA_Image__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_LOA_Image__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA_Image] ADD CONSTRAINT [pk_Invoice_Collection_LOA_Image] PRIMARY KEY CLUSTERED  ([Invoice_Collection_LOA_Image_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA_Image] ADD CONSTRAINT [un_Invoice_Collection_LOA_Image__Invoice_Collection_LOA_Id__Cbms_Image_Id] UNIQUE NONCLUSTERED  ([Invoice_Collection_LOA_Id], [Cbms_Image_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_LOA_Image__Cbms_Image_Id] ON [dbo].[Invoice_Collection_LOA_Image] ([Cbms_Image_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_LOA_Image] ADD CONSTRAINT [fk_Invoice_Collection_LOA__Invoice_Collection_LOA_Image] FOREIGN KEY ([Invoice_Collection_LOA_Id]) REFERENCES [dbo].[Invoice_Collection_LOA] ([Invoice_Collection_LOA_Id])
GO
