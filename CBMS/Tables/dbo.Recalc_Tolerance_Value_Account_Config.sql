CREATE TABLE [dbo].[Recalc_Tolerance_Value_Account_Config]
(
[Recalc_Tolerance_Value_Account_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NULL,
[Commodity_Id] [int] NULL,
[Total_Cost_Lower_Range] [decimal] (28, 6) NULL,
[Total_Cost_Upper_Range] [decimal] (28, 6) NULL,
[Tolerance_Value] [decimal] (28, 6) NULL,
[Config_Start_Dt] [date] NULL,
[Config_End_Dt] [date] NULL,
[Value_Type_Cd] [int] NULL,
[Base_Currency_Id] [int] NULL,
[Comparison_Operation] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Recalc_Tolerance_Value_Account_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Recalc_Tolerance_Value_Account_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Recalc_Tolerance_Value_Account_Config] ADD CONSTRAINT [pk_Recalc_Tolerance_Value_Account_Config_Id] PRIMARY KEY NONCLUSTERED  ([Recalc_Tolerance_Value_Account_Config_Id]) ON [DB_DATA01]
GO
