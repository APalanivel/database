CREATE TABLE [dbo].[Invoice_Collection_Issue_Event]
(
[Invoice_Collection_Issue_Event_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Issue_Log_Id] [int] NOT NULL,
[Issue_Event_Type_Cd] [int] NOT NULL,
[Event_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Event__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Event__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Event] ADD CONSTRAINT [pk_Invoice_Collection_Issue_Event] PRIMARY KEY NONCLUSTERED  ([Invoice_Collection_Issue_Event_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Issue_Event__Invoice_Collection_Issue_Log_Id] ON [dbo].[Invoice_Collection_Issue_Event] ([Invoice_Collection_Issue_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Event] ADD CONSTRAINT [fk_Invoice_Collection_Issue_Log__Invoice_Collection_Issue_Event] FOREIGN KEY ([Invoice_Collection_Issue_Log_Id]) REFERENCES [dbo].[Invoice_Collection_Issue_Log] ([Invoice_Collection_Issue_Log_Id])
GO
