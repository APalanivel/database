CREATE TABLE [dbo].[Invoice_Collection_Account_Contact]
(
[Invoice_Collection_Account_Config_Id] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Is_Primary] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Contact__Is_Primary] DEFAULT ((0)),
[Is_Include_In_CC] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Contact__Is_CC] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Contact__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Account_Contact__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Contact] ADD CONSTRAINT [pk_Invoice_Collection_Account_Contact] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Account_Config_Id], [Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Account_Contact__Contact_Info_Id] ON [dbo].[Invoice_Collection_Account_Contact] ([Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Contact] ADD CONSTRAINT [fk_Contact_Info__Invoice_Collection_Account_Contact] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Contact] ADD CONSTRAINT [fk_Invoice_Collection_Account_Contact__Contact_Info_Id] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Contact] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Invoice_Collection_Account_Contact] FOREIGN KEY ([Invoice_Collection_Account_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config] ([Invoice_Collection_Account_Config_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Contact] ADD CONSTRAINT [fk_Invoice_Collection_Account_Contact__Invoice_Collection_Account_Config_Id] FOREIGN KEY ([Invoice_Collection_Account_Config_Id]) REFERENCES [dbo].[Invoice_Collection_Account_Config] ([Invoice_Collection_Account_Config_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'To store non login users mapped to Invoice_Collection_Account_Config Primary level', 'SCHEMA', N'dbo', 'TABLE', N'Invoice_Collection_Account_Contact', NULL, NULL
GO
