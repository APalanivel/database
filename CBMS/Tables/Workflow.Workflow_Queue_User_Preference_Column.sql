CREATE TABLE [Workflow].[Workflow_Queue_User_Preference_Column]
(
[Workflow_Queue_User_Preference_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[User_Info_Id] [int] NOT NULL,
[Workflow_Queue_Output_Column_Id] [int] NOT NULL,
[Display_Seq] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Workflow___Creat__5E16B864] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DF__Workflow___Last___5F0ADC9D] DEFAULT (getdate()),
[is_active] [bit] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_User_Preference_Column] ADD CONSTRAINT [pk_Workflow_Queue_User_Preference_Column] PRIMARY KEY CLUSTERED  ([Workflow_Queue_User_Preference_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_User_Preference_Column] ADD CONSTRAINT [uix_Workflow_Queue_User_Preference_Column__User_Info_Id__Workflow_Queue_Output_Column_Id] UNIQUE NONCLUSTERED  ([User_Info_Id], [Workflow_Queue_Output_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_User_Preference_Column] ADD CONSTRAINT [fk_Workflow_Queue_Output_Column__Workflow_Queue_User_Preference_Column] FOREIGN KEY ([Workflow_Queue_Output_Column_Id]) REFERENCES [Workflow].[Workflow_Queue_Output_Column] ([Workflow_Queue_Output_Column_Id])
GO
