CREATE TABLE [dbo].[RATE]
(
[RATE_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[VENDOR_ID] [int] NOT NULL,
[COMMODITY_TYPE_ID] [int] NOT NULL,
[RATE_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RATE_REQUIREMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Budget_Calc_Type_Cd] [int] NULL
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Rate_upd_Client_Hier_Account
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier_Account where ever any change happen in Rate table

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
BEGIN TRAN	
	UPDATE
		  RATE
	SET   
		 RATE_NAME = 'Trigger for coal'
	WHERE
		  RATE_ID = 17643

select rate_name from core.client_hier_account where rate_id = 	17643	  
ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri
	
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/19/2010	Created
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
	
Note:	
	Commodity & vendor can not be chnage from rate table

******/


CREATE TRIGGER [dbo].[tr_Rate_upd_Client_Hier_Account] ON [dbo].[RATE]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
      UPDATE
            cha
      SET   
            cha.Rate_Name = i.RATE_NAME
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                  ON i.RATE_ID = cha.RATE_ID
            INNER JOIN DELETED d
                  ON i.RATE_ID = d.RATE_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT i.Rate_Name
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT d.Rate_Name
                                                        FOR
                                                                  XML RAW ))
END 
;
GO
ALTER TABLE [dbo].[RATE] ADD CONSTRAINT [RATE_PK] PRIMARY KEY CLUSTERED  ([RATE_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE NONCLUSTERED INDEX [RATE_N_2] ON [dbo].[RATE] ([COMMODITY_TYPE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [RATE_N_3] ON [dbo].[RATE] ([RATE_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [RATE_N_1] ON [dbo].[RATE] ([VENDOR_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[RATE] ADD CONSTRAINT [COMMODITY_RATE_FK] FOREIGN KEY ([COMMODITY_TYPE_ID]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
ALTER TABLE [dbo].[RATE] ADD CONSTRAINT [VENDOR_RATE_FK] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table stores the Rate master information pubblished by utilities', 'SCHEMA', N'dbo', 'TABLE', N'RATE', NULL, NULL
GO
