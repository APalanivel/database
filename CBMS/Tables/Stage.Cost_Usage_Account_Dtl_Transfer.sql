CREATE TABLE [Stage].[Cost_Usage_Account_Dtl_Transfer]
(
[Cost_Usage_Account_Dtl_Transfer_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Bucket_Master_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Bucket_Value] [decimal] (28, 10) NULL,
[UOM_Type_Id] [int] NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[Created_By_Id] [int] NULL,
[Created_Ts] [datetime] NULL,
[Updated_Ts] [datetime] NULL,
[Updated_By_Id] [int] NULL,
[Data_Source_Cd] [int] NULL,
[Sys_Change_Operation] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Type_Cd] [int] NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Staging table to hold the data transferred from DVDEHub', 'SCHEMA', N'Stage', 'TABLE', N'Cost_Usage_Account_Dtl_Transfer', NULL, NULL
GO

ALTER TABLE [Stage].[Cost_Usage_Account_Dtl_Transfer] ADD CONSTRAINT [pk_Cost_Usage_Account_Dtl_Transfer] PRIMARY KEY NONCLUSTERED  ([Cost_Usage_Account_Dtl_Transfer_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
ALTER TABLE [Stage].[Cost_Usage_Account_Dtl_Transfer] ADD CONSTRAINT [uix_Cost_Usage_Account_Dtl_Transfer__Client_Hier_Id__Account_Id_Bucket_Master_Id] UNIQUE CLUSTERED  ([Client_Hier_Id], [Account_Id], [Bucket_Master_Id], [Service_Month]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
