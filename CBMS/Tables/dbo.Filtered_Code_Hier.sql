CREATE TABLE [dbo].[Filtered_Code_Hier]
(
[Filtered_Code_Hier_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Parent_Codeset_Filtered_Cd] [int] NOT NULL,
[Parent_Filtered_Code_Id] [int] NOT NULL,
[Child_Filtered_Code_Id] [int] NOT NULL,
[Child_Codeset_Filtered_Cd] [int] NOT NULL,
[Filtered_Code_Hier_Type_Id] [int] NOT NULL
) ON [DBData_Reference]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Child /Parent relationship for filtered dropdowns in RA', 'SCHEMA', N'dbo', 'TABLE', N'Filtered_Code_Hier', NULL, NULL
GO

ALTER TABLE [dbo].[Filtered_Code_Hier] ADD CONSTRAINT [pk_Filtered_Code_Hier] PRIMARY KEY CLUSTERED  ([Filtered_Code_Hier_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [Ix_Filtered_Code_Hier__Child_Codeset_Filtered_Cd__Child_Filtered_Code_Id] ON [dbo].[Filtered_Code_Hier] ([Child_Filtered_Code_Id], [Child_Codeset_Filtered_Cd]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [Ix_Filtered_Code_Hier__Filtered_Code_Hier_Type_Id] ON [dbo].[Filtered_Code_Hier] ([Filtered_Code_Hier_Type_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [Ix_Filtered_Code_Hier__ParentFilteredKeys__ChildFilteredKeys__Filtered_Code_Hier_Type_Id] ON [dbo].[Filtered_Code_Hier] ([Parent_Codeset_Filtered_Cd], [Parent_Filtered_Code_Id], [Child_Codeset_Filtered_Cd], [Child_Filtered_Code_Id], [Filtered_Code_Hier_Type_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE UNIQUE NONCLUSTERED INDEX [Un_Filtered_Code_Hier__ParentFilteredKeys__ChildFilteredKeys__Filtered_Code_Hier_Type_Id] ON [dbo].[Filtered_Code_Hier] ([Parent_Codeset_Filtered_Cd], [Parent_Filtered_Code_Id], [Child_Codeset_Filtered_Cd], [Child_Filtered_Code_Id], [Filtered_Code_Hier_Type_Id]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
CREATE NONCLUSTERED INDEX [Ix_Filtered_Code_Hier__Parent_Codeset_Filtered_Cd__Parent_Filtered_Code_Id] ON [dbo].[Filtered_Code_Hier] ([Parent_Filtered_Code_Id], [Parent_Codeset_Filtered_Cd]) WITH (FILLFACTOR=90) ON [DBData_Reference]
GO
ALTER TABLE [dbo].[Filtered_Code_Hier] ADD CONSTRAINT [fk_Filtered_Code__Filtered_Code_Hier__Child_Filtered_Code_Id__Child_Codeset_Filtered_Cd] FOREIGN KEY ([Child_Codeset_Filtered_Cd], [Child_Filtered_Code_Id]) REFERENCES [dbo].[Filtered_Code] ([Codeset_Filtered_Cd], [Filtered_Code_Id])
GO
ALTER TABLE [dbo].[Filtered_Code_Hier] ADD CONSTRAINT [fk_Filtered_Code_Hier_Type__Filtered_Code_Hier] FOREIGN KEY ([Filtered_Code_Hier_Type_Id]) REFERENCES [dbo].[Filtered_Code_Hier_Type] ([Filtered_Code_Hier_Type_Id])
GO
ALTER TABLE [dbo].[Filtered_Code_Hier] ADD CONSTRAINT [fk_Filtered_Code__Filtered_Code_Hier__Parent_Codeset_Filtered_Cd__Parent_Filtered_Code_Id] FOREIGN KEY ([Parent_Codeset_Filtered_Cd], [Parent_Filtered_Code_Id]) REFERENCES [dbo].[Filtered_Code] ([Codeset_Filtered_Cd], [Filtered_Code_Id])
GO
