CREATE TABLE [dbo].[Variance_Parameter_Baseline_Map]
(
[Variance_Baseline_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Parameter_Id] [int] NOT NULL,
[Baseline_Cd] [int] NOT NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of variance baseline to parameter', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Parameter_Baseline_Map', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Parameter_Baseline_Map] ADD 
CONSTRAINT [PK_Variance_Parameter_Baseline_Map] PRIMARY KEY CLUSTERED  ([Variance_Baseline_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
ALTER TABLE [dbo].[Variance_Parameter_Baseline_Map] ADD
CONSTRAINT [FK_Variance_ParameterVariance_Parameter_Baseline_Map] FOREIGN KEY ([Variance_Parameter_Id]) REFERENCES [dbo].[Variance_Parameter] ([Variance_Parameter_Id])
GO
