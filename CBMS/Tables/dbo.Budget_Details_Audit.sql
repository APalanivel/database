CREATE TABLE [dbo].[Budget_Details_Audit]
(
[Audit_Function] [smallint] NOT NULL,
[Audit_Ts] [datetime] NOT NULL,
[Budget_Detail_Id] [int] NOT NULL,
[Budget_Account_Id] [int] NOT NULL,
[Month_Identifier] [datetime] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks changes to Budget Details table', 'SCHEMA', N'dbo', 'TABLE', N'Budget_Details_Audit', NULL, NULL
GO
