CREATE TABLE [dbo].[Invoice_Collection_Issue_Attachment]
(
[Invoice_Collection_Issue_Log_Id] [int] NOT NULL,
[Cbms_Image_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Attachment__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Attachment] ADD CONSTRAINT [pk_Invoice_Collection_Issue_Attachment] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Issue_Log_Id], [Cbms_Image_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Issue_Attachment__Cbms_Image_Id] ON [dbo].[Invoice_Collection_Issue_Attachment] ([Cbms_Image_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Attachment] ADD CONSTRAINT [fk_Invoice_Collection_Issue_Log__Invoice_Collection_Issue_Attachment] FOREIGN KEY ([Invoice_Collection_Issue_Log_Id]) REFERENCES [dbo].[Invoice_Collection_Issue_Log] ([Invoice_Collection_Issue_Log_Id])
GO
