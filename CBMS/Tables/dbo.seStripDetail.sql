CREATE TABLE [dbo].[seStripDetail]
(
[StripDetailId] [int] NOT NULL IDENTITY(1, 1),
[StripHeaderId] [int] NOT NULL,
[Symbol] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Volume] [int] NULL,
[SortDate] [datetime] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seStripDetail', NULL, NULL
GO

ALTER TABLE [dbo].[seStripDetail] ADD 
CONSTRAINT [PK_seStripDetail] PRIMARY KEY CLUSTERED  ([StripDetailId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
