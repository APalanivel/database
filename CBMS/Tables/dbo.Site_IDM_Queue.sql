CREATE TABLE [dbo].[Site_IDM_Queue]
(
[Site_IDM_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [df_Site_IDM_Queue__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Site_IDM___Last___2F92337B] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Site_IDM_Queue] ADD CONSTRAINT [pk_Site_IDM_Queue] PRIMARY KEY CLUSTERED  ([Client_Hier_Id]) ON [DB_DATA01]
GO
