CREATE TABLE [dbo].[RM_BATCH_CONFIGURATION]
(
[RM_BATCH_CONFIGURATION_ID] [int] NOT NULL IDENTITY(1, 1),
[RM_BATCH_CONFIGURATION_MASTER_ID] [int] NOT NULL,
[CLIENT_ID] [int] NOT NULL,
[REPORT_LIST_ID] [int] NOT NULL,
[CORPORATE_REPORT] [bit] NOT NULL
) ON [DBData_Risk_MGMT]
ALTER TABLE [dbo].[RM_BATCH_CONFIGURATION] ADD
CONSTRAINT [FK_RM_BATCH_CONFIGURATION_CLIENT] FOREIGN KEY ([CLIENT_ID]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the client level child of batch configuration', 'SCHEMA', N'dbo', 'TABLE', N'RM_BATCH_CONFIGURATION', NULL, NULL
GO

ALTER TABLE [dbo].[RM_BATCH_CONFIGURATION] ADD 
CONSTRAINT [PK_RM_BATCH_CONFIGURATION] PRIMARY KEY CLUSTERED  ([RM_BATCH_CONFIGURATION_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]

ALTER TABLE [dbo].[RM_BATCH_CONFIGURATION] ADD
CONSTRAINT [FK_RM_BATCH_CONFIGURATION_REPORT_LIST] FOREIGN KEY ([REPORT_LIST_ID]) REFERENCES [dbo].[REPORT_LIST] ([REPORT_LIST_ID])
ALTER TABLE [dbo].[RM_BATCH_CONFIGURATION] ADD
CONSTRAINT [FK_RM_BATCH_CONFIGURATION_RM_BATCH_CONFIGURATION_MASTER] FOREIGN KEY ([RM_BATCH_CONFIGURATION_MASTER_ID]) REFERENCES [dbo].[RM_BATCH_CONFIGURATION_MASTER] ([RM_BATCH_CONFIGURATION_MASTER_ID])

GO
