CREATE TABLE [dbo].[Account_Not_Applicable_DMO_Config]
(
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Client_Hier_DMO_Config_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Not_Applicable_DMO_Config__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Not_Applicable_DMO_Config] ADD CONSTRAINT [pk_Account_Not_Applicable_DMO_Config] PRIMARY KEY CLUSTERED  ([Client_Hier_Id], [Account_Id], [Client_Hier_DMO_Config_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Not_Applicable_DMO_Config__Client_Hier_DMO_Config_Id] ON [dbo].[Account_Not_Applicable_DMO_Config] ([Client_Hier_DMO_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Not_Applicable_DMO_Config] ADD CONSTRAINT [fk_Client_Hier_DMO_Config__Account_Not_Applicable_DMO_Config] FOREIGN KEY ([Client_Hier_DMO_Config_Id]) REFERENCES [dbo].[Client_Hier_DMO_Config] ([Client_Hier_DMO_Config_Id])
GO
