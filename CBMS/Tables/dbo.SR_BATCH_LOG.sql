CREATE TABLE [dbo].[SR_BATCH_LOG]
(
[SR_BATCH_LOG_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_BATCH_MASTER_LOG_ID] [int] NOT NULL,
[BATCH_CATEGORY_TYPE_ID] [int] NOT NULL,
[BATCH_DURATION_TYPE_ID] [int] NOT NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store details of each batch process for sourcing', 'SCHEMA', N'dbo', 'TABLE', N'SR_BATCH_LOG', NULL, NULL
GO

ALTER TABLE [dbo].[SR_BATCH_LOG] ADD 
CONSTRAINT [PK__SR_BATCH_LOG__61B25759] PRIMARY KEY CLUSTERED  ([SR_BATCH_LOG_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_BATCH_LOG] ADD
CONSTRAINT [FK_SR_BATCH_LOG_ENTITY] FOREIGN KEY ([BATCH_CATEGORY_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SR_BATCH_LOG] ADD
CONSTRAINT [FK_SR_BATCH_LOG_ENTITY1] FOREIGN KEY ([BATCH_DURATION_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SR_BATCH_LOG] ADD
CONSTRAINT [FK_SR_BATCH_LOG_SR_BATCH_MASTER_LOG] FOREIGN KEY ([SR_BATCH_MASTER_LOG_ID]) REFERENCES [dbo].[SR_BATCH_MASTER_LOG] ([SR_BATCH_MASTER_LOG_ID])

GO
