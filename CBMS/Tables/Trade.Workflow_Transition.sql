CREATE TABLE [Trade].[Workflow_Transition]
(
[Workflow_Transition_Id] [int] NOT NULL IDENTITY(1, 1),
[Transition_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Transition_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Transition__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Transition__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Transition] ADD CONSTRAINT [pk_Workflow_Transition_] PRIMARY KEY CLUSTERED  ([Workflow_Transition_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Workflow_Transition] ADD CONSTRAINT [un_Workflow_Transition__Transition_Name] UNIQUE NONCLUSTERED  ([Transition_Name]) ON [DB_DATA01]
GO
