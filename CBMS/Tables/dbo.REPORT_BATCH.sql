CREATE TABLE [dbo].[REPORT_BATCH]
(
[REPORT_BATCH_ID] [int] NOT NULL IDENTITY(1, 1),
[BATCH_FILE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[USER_INFO_ID] [int] NOT NULL,
[BATCH_DATE] [datetime] NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Stored batch report data, but hasn''t been used since 3/18/2005', 'SCHEMA', N'dbo', 'TABLE', N'REPORT_BATCH', NULL, NULL
GO
