CREATE TABLE [dbo].[hvr_strsr_h_d_c0002_c0002]
(
[tbl_name] [varchar] (124) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_strsr__tbl_n__4C780BC2] DEFAULT (''),
[session_tstamp] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__hvr_strsr__sessi__4D6C2FFB] DEFAULT ('')
) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [hvr_strsr_h_d_c0002_c0002__x0] ON [dbo].[hvr_strsr_h_d_c0002_c0002] ([tbl_name]) ON [DB_DATA01]
GO
GRANT SELECT ON  [dbo].[hvr_strsr_h_d_c0002_c0002] TO [public]
GO
GRANT INSERT ON  [dbo].[hvr_strsr_h_d_c0002_c0002] TO [public]
GO
GRANT DELETE ON  [dbo].[hvr_strsr_h_d_c0002_c0002] TO [public]
GO
GRANT UPDATE ON  [dbo].[hvr_strsr_h_d_c0002_c0002] TO [public]
GO
