CREATE TABLE [dbo].[Notification_Msg_Dtl]
(
[Notification_Msg_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Notification_Msg_Queue_Id] [int] NOT NULL,
[Recipient_User_Id] [int] NOT NULL,
[Recipient_Email_Address] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Msg_Delivery_Status_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Notification_Msg_Dtl__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL,
[CC_Email_Address] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Recipient user id and email address and delivery status of system notifications', 'SCHEMA', N'dbo', 'TABLE', N'Notification_Msg_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[Notification_Msg_Dtl] ADD CONSTRAINT [pk_Notification_Msg_Dtl] PRIMARY KEY CLUSTERED  ([Notification_Msg_Dtl_Id]) ON [DB_DATA01]
GO

ALTER TABLE [dbo].[Notification_Msg_Dtl] ADD CONSTRAINT [fk_Notification_Msg_Queue__Notification_Msg_Dtl] FOREIGN KEY ([Notification_Msg_Queue_Id]) REFERENCES [dbo].[Notification_Msg_Queue] ([Notification_Msg_Queue_Id])
GO
