CREATE TABLE [dbo].[Xl_Bulk_Data_Process]
(
[Xl_Bulk_Data_Process_Id] [int] NOT NULL IDENTITY(1, 1),
[Xl_Process_Category_Cd] [int] NOT NULL,
[Xl_Process_Name_Cd] [int] NOT NULL,
[Xl_Process_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Default_For_Category] [bit] NOT NULL CONSTRAINT [df_Xl_Bulk_Data_Process__Is_Default_For_Category] DEFAULT ((0)),
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Xl_Bulk_Data_Process__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Xl_Bulk_Data_Process__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Bulk_Data_Process] ADD CONSTRAINT [pk_Xl_Bulk_Data_Process] PRIMARY KEY CLUSTERED  ([Xl_Bulk_Data_Process_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Xl_Bulk_Data_Process] ADD CONSTRAINT [un_Xl_Bulk_Data_Process__Xl_Process_Category_Cd__Xl_Process_Name_Cd] UNIQUE NONCLUSTERED  ([Xl_Process_Category_Cd], [Xl_Process_Name_Cd]) ON [DB_DATA01]
GO
