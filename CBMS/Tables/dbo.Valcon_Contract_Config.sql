CREATE TABLE [dbo].[Valcon_Contract_Config]
(
[Valcon_Contract_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[Valcon_Account_Config_Type_Cd] [int] NOT NULL,
[Is_Config_Complete] [bit] NOT NULL,
[Comment_Id] [int] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Valcon_Contract_Config__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Valcon_Contract_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Valcon_Contract_Config] ADD CONSTRAINT [PK_Valcon_Contract_Config__Valcon_Contract_Config_Id] PRIMARY KEY CLUSTERED  ([Valcon_Contract_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Valcon_Contract_Config] ADD CONSTRAINT [un_Valcon_Contract_Config__Contract_Id_Valcon_Account_Config_Type_Cd] UNIQUE NONCLUSTERED  ([Contract_Id], [Valcon_Account_Config_Type_Cd]) ON [DB_DATA01]
GO
