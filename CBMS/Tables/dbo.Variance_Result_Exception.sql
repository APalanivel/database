CREATE TABLE [dbo].[Variance_Result_Exception]
(
[Variance_Result_Exception_id] [int] NOT NULL IDENTITY(1, 1),
[Commodity_id] [int] NULL,
[Country_id] [int] NULL,
[Category_id] [int] NULL,
[Client_id] [int] NULL,
[Test_Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NULL,
[Created_Ts] [datetime2] NULL,
[Updated_user_id] [int] NULL,
[Last_Change_TS] [datetime2] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Result_Exception] ADD CONSTRAINT [PK_Variance_Result_Exception_id] PRIMARY KEY CLUSTERED  ([Variance_Result_Exception_id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Result_Exception] ADD CONSTRAINT [UC_Client_Country_Test_Description] UNIQUE NONCLUSTERED  ([Commodity_id], [Country_id], [Client_id], [Category_id], [Test_Description]) ON [DB_DATA01]
GO
