CREATE TABLE [dbo].[Invoice_Collection_Exception_Comment]
(
[Invoice_Collection_Exception_Comment_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Comment_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Exception_Comment__Created_Ts] DEFAULT (getdate()),
[Comment_Type_Cd] [int] NOT NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Exception_Comment] ADD CONSTRAINT [pk_Invoice_Collection_Exception_Comment] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Exception_Comment_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Exception_Comment__Invoice_Collection_Queue_Id] ON [dbo].[Invoice_Collection_Exception_Comment] ([Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Exception_Comment] ADD CONSTRAINT [fk_Invoice_Collection_Queue__Invoice_Collection_Exception_Comment] FOREIGN KEY ([Invoice_Collection_Queue_Id]) REFERENCES [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Queue_Id])
GO
