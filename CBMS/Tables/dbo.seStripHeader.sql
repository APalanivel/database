CREATE TABLE [dbo].[seStripHeader]
(
[StripHeaderId] [int] NOT NULL IDENTITY(1, 1),
[AccountId] [int] NOT NULL,
[StripName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExpirationDate] [datetime] NULL,
[UniformVolume] [bit] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seStripHeader', NULL, NULL
GO

ALTER TABLE [dbo].[seStripHeader] ADD 
CONSTRAINT [PK_seStripHeader] PRIMARY KEY CLUSTERED  ([StripHeaderId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
