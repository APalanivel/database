CREATE TABLE [dbo].[seContentType]
(
[ContentTypeId] [int] NOT NULL IDENTITY(1, 1),
[TypeLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [int] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seContentType', NULL, NULL
GO

ALTER TABLE [dbo].[seContentType] ADD 
CONSTRAINT [PK_seContentType] PRIMARY KEY CLUSTERED  ([ContentTypeId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
