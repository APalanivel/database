CREATE TABLE [dbo].[Invoice_Collection_Account_Config_Group]
(
[Invoice_Collection_Account_Config_Group_ID] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Account_Config_Group_name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL CONSTRAINT [DF_IC_Account_Config_Group_Created_User_Id] DEFAULT ((-1)),
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_IC_Account_Config_Group_Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config_Group] ADD CONSTRAINT [PK_Invoice_Collection_Account_Config_Group] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Account_Config_Group_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Account_Config_Group] ADD CONSTRAINT [UK_Account_Groupname] UNIQUE NONCLUSTERED  ([Invoice_Collection_Account_Config_Group_name]) ON [DB_DATA01]
GO
