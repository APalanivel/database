CREATE TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query]
(
[Workflow_Queue_Saved_Filter_Query_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Sub_Queue_Id] [int] NOT NULL,
[Saved_Filter_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Saved_Filter_Type_Cd] [int] NULL,
[Owner_User_Id] [int] NOT NULL,
[Expiration_Dt] [date] NOT NULL CONSTRAINT [df_Workflow_Queue_Saved_Filter_Query__Expiration_Dt] DEFAULT ('12/31/9999'),
[Master_Workflow_Queue_Saved_Filter_Query_Id] [int] NULL,
[Display_Seq] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Saved_Filter_Query__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Saved_Filter_Query__Last_Change_Ts] DEFAULT (getdate()),
[Is_Shared] [bit] NULL,
[Description] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query] ADD CONSTRAINT [pk_Workflow_Queue_Saved_Filter_Query] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Saved_Filter_Query_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query] ADD CONSTRAINT [un_Workflow_Queue_Saved_Filter_Query__Saved_Filter_Name__Saved_Filter_Type_Cd] UNIQUE NONCLUSTERED  ([Saved_Filter_Name], [Saved_Filter_Type_Cd], [Owner_User_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Saved_Filter_Query] ADD CONSTRAINT [fk_Workflow_Sub_Queue__Workflow_Queue_Saved_Filter_Query] FOREIGN KEY ([Workflow_Sub_Queue_Id]) REFERENCES [Workflow].[Workflow_Sub_Queue] ([Workflow_Sub_Queue_Id])
GO
