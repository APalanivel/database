CREATE TABLE [dbo].[Contract_Exception]
(
[Contract_Exception_Id] [int] NOT NULL IDENTITY(1, 1),
[Contract_Id] [int] NOT NULL,
[Exception_Type_Cd] [int] NOT NULL,
[Queue_Id] [int] NOT NULL,
[Exception_Status_Cd] [int] NOT NULL,
[Service_Desk_Ticket_XId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_Contract_Exception__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF_Contract_Exception__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contract_Exception] ADD CONSTRAINT [pk_Contract_Exception] PRIMARY KEY CLUSTERED  ([Contract_Exception_Id]) ON [DB_DATA01]
GO
