CREATE TABLE [dbo].[SR_SW_RFP_NO_BID]
(
[SR_SW_RFP_NO_BID_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[SR_ACCOUNT_GROUP_ID] [int] NOT NULL,
[IS_BID_GROUP] [bit] NULL,
[NO_BID_COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATED_BY] [int] NOT NULL,
[UPDATED_DATE] [datetime] NOT NULL,
[msrepl_tran_version] [uniqueidentifier] NOT NULL CONSTRAINT [MSrepl_tran_version_default_48EA73D0_77A1_437C_96DC_EC63684FF0B2_1591077550] DEFAULT (newid())
) ON [DBData_Sourcing]
GO


EXEC sp_addextendedproperty N'MS_Description', N'This table would store the no-bids on any account/group of a RFP, updated by the supplier contact from supplier website manage bid section', 'SCHEMA', N'dbo', 'TABLE', N'SR_SW_RFP_NO_BID', NULL, NULL
GO

ALTER TABLE [dbo].[SR_SW_RFP_NO_BID] ADD 
CONSTRAINT [PK__SR_SW_RFP_NO_BID__37871363] PRIMARY KEY CLUSTERED  ([SR_SW_RFP_NO_BID_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
CREATE NONCLUSTERED INDEX [IX_SR_SW_RFP_NO_BID__UPDATED_BY__SR_ACCOUNT_GROUP_ID] ON [dbo].[SR_SW_RFP_NO_BID] ([UPDATED_BY], [SR_ACCOUNT_GROUP_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
