CREATE TABLE [dbo].[dmlLookup]
(
[LookupId] [int] NOT NULL,
[Code] [uniqueidentifier] NOT NULL,
[LookupTypeId] [int] NOT NULL,
[LookupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SortOrder] [int] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'dmlLookup', NULL, NULL
GO
