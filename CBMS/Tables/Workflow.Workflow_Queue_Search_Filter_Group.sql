CREATE TABLE [Workflow].[Workflow_Queue_Search_Filter_Group]
(
[Workflow_Queue_Search_Filter_Group_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Saved_Filter_Query_Id] [int] NOT NULL,
[Filter_Group_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Owner_User_Info_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Search_Filter_Group__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Search_Filter_Group__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Group] ADD CONSTRAINT [pk_Workflow_Queue_Search_Filter_Group] PRIMARY KEY CLUSTERED  ([Workflow_Queue_Search_Filter_Group_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Group] ADD CONSTRAINT [un_Workflow_Queue_Search_Filter_Group__Workflow_Sub_Queue_Id__Filter_Group_Name__Owner_User_Info_Id] UNIQUE NONCLUSTERED  ([Workflow_Queue_Saved_Filter_Query_Id], [Filter_Group_Name], [Owner_User_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Search_Filter_Group] ADD CONSTRAINT [fk_Workflow_Queue_Saved_Filter_Query_Id_Workflow_Queue_Search_Filter_Group] FOREIGN KEY ([Workflow_Queue_Saved_Filter_Query_Id]) REFERENCES [Workflow].[Workflow_Queue_Saved_Filter_Query] ([Workflow_Queue_Saved_Filter_Query_Id])
GO
