CREATE TABLE [dbo].[Invoice_Collection_Queue_Event]
(
[Invoice_Collection_Queue_Event_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Event_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Event_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Queue_Event__Event_Ts] DEFAULT (getdate()),
[Event_By_User_Id] [int] NOT NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Queue_Event] ADD CONSTRAINT [pk_Invoice_Collection_Queue_Event] PRIMARY KEY NONCLUSTERED  ([Invoice_Collection_Queue_Event_Id]) ON [DB_DATA01]
GO
CREATE CLUSTERED INDEX [cix_Invoice_Collection_Queue_Event__Invoice_Collection_Queue_Id] ON [dbo].[Invoice_Collection_Queue_Event] ([Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
