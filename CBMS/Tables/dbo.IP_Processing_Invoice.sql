CREATE TABLE [dbo].[IP_Processing_Invoice]
(
[IP_Processing_Invoice_Id] [int] NOT NULL IDENTITY(1, 1),
[Processing_Cu_Invoice_Id] [int] NOT NULL,
[Cu_Invoice_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Is_Reported] [bit] NOT NULL,
[Is_Previously_Reported] [bit] NOT NULL,
[User_Info_ID] [int] NOT NULL,
[Last_Updated_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[IP_Processing_Invoice] ADD CONSTRAINT [pk_IP_Processing_Invoice_Id] PRIMARY KEY CLUSTERED  ([IP_Processing_Invoice_Id]) ON [DB_DATA01]
GO
