CREATE TABLE [dbo].[Invoice_Image_Migration_Batch]
(
[UBM_Batch_Master_Log_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Image_Migration_Batch__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Image_Migration_Batch__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Image_Migration_Batch] ADD CONSTRAINT [pk_Invoice_Image_Migration_Batch] PRIMARY KEY CLUSTERED  ([UBM_Batch_Master_Log_Id]) ON [DB_DATA01]
GO
