CREATE TABLE [Trade].[Rm_Budget_Current_Hedge_Position_Dtl]
(
[Rm_Budget_Current_Hedge_Position_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Rm_Budget_Id] [int] NOT NULL,
[Service_Month] [date] NOT NULL,
[Total_Forecast_Volume] [decimal] (28, 3) NOT NULL,
[Total_Volume_Hedged] [decimal] (28, 3) NULL,
[Hedged_Unit_Cost] [decimal] (28, 3) NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Current_Hedge_Position_Dtl] ADD CONSTRAINT [PK_Rm_Budget_Current_Hedge_Position_Dtl__Rm_Budget_Current_Hedge_Position_Dtl_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Current_Hedge_Position_Dtl_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Current_Hedge_Position_Dtl] ADD CONSTRAINT [FK_Rm_Budget_Current_Hedge_Position_Dtl__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
