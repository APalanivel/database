CREATE TABLE [dbo].[Invoice_Submission_Log]
(
[Invoice_Submission_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NULL,
[User_Info_Id] [int] NOT NULL,
[External_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[System_File_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment_Id] [int] NULL,
[Status_Cd] [int] NOT NULL,
[Invoice_Submission_Client_Location_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_Invoice_Submission_Log_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Updated_Ts] [datetime] NOT NULL CONSTRAINT [DF_Invoice_Submission_Log_Updated_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Submission_Log] ADD CONSTRAINT [PK_Invoice_Submission_Log] PRIMARY KEY CLUSTERED  ([Invoice_Submission_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Submission_Log] WITH NOCHECK ADD CONSTRAINT [fk_Invoice_Submission_Log] FOREIGN KEY ([Invoice_Submission_Client_Location_Id]) REFERENCES [dbo].[Invoice_Submission_Client_Location] ([Invoice_Submission_Client_Location_Id])
GO
