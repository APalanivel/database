CREATE TABLE [dbo].[Ec_Calc_Val_Period_UDF]
(
[Period_Cd] [int] NOT NULL,
[Period_UDF] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Period_UDF] ADD CONSTRAINT [PK_Ec_Calc_Val_Period_UDF__Period_Cd] PRIMARY KEY CLUSTERED  ([Period_Cd]) ON [DB_DATA01]
GO
