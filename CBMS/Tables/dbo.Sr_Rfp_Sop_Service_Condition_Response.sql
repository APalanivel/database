CREATE TABLE [dbo].[Sr_Rfp_Sop_Service_Condition_Response]
(
[Sr_Rfp_Sop_Service_Condition_Response_Id] [int] NOT NULL IDENTITY(1, 1),
[Sr_Rfp_Sop_Details_Id] [int] NOT NULL,
[Sr_Service_Condition_Category_Id] [int] NOT NULL,
[Category_Display_Seq] [int] NOT NULL,
[Sr_Service_Condition_Question_Id] [int] NOT NULL,
[Question_Display_Seq] [int] NOT NULL,
[Response_Text_Value] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sr_Rfp_Service_Condition_Template_Question_Map_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Rfp_Sop_Service_Condition_Response_Last_Change_Ts] DEFAULT (getdate()),
[Comment] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Sop_Service_Condition_Response] ADD CONSTRAINT [pk_Sr_Rfp_Sop_Service_Condition_Response] PRIMARY KEY CLUSTERED  ([Sr_Rfp_Sop_Service_Condition_Response_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Rfp_Sop_Service_Condition_Response] ADD CONSTRAINT [fk_Sr_Rfp_Sop_Service_Condition_Response__SR_RFP_SOP_DETAILS] FOREIGN KEY ([Sr_Rfp_Sop_Details_Id]) REFERENCES [dbo].[SR_RFP_SOP_DETAILS] ([SR_RFP_SOP_DETAILS_ID])
GO
ALTER TABLE [dbo].[Sr_Rfp_Sop_Service_Condition_Response] ADD CONSTRAINT [fk_Sr_Rfp_Sop_Service_Condition_Response__Sr_Service_Condition_Category] FOREIGN KEY ([Sr_Service_Condition_Category_Id]) REFERENCES [dbo].[Sr_Service_Condition_Category] ([Sr_Service_Condition_Category_Id])
GO
ALTER TABLE [dbo].[Sr_Rfp_Sop_Service_Condition_Response] ADD CONSTRAINT [fk_Sr_Rfp_Sop_Service_Condition_Response__Sr_Service_Condition_Question] FOREIGN KEY ([Sr_Service_Condition_Question_Id]) REFERENCES [dbo].[Sr_Service_Condition_Question] ([Sr_Service_Condition_Question_Id])
GO
