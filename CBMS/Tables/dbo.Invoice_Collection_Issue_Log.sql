CREATE TABLE [dbo].[Invoice_Collection_Issue_Log]
(
[Invoice_Collection_Issue_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Activity_Id] [int] NOT NULL,
[Invoice_Collection_Queue_Id] [int] NOT NULL,
[Collection_Start_Dt] [date] NOT NULL,
[Collection_End_Dt] [date] NOT NULL,
[Invoice_Collection_Issue_Type_Cd] [int] NOT NULL,
[Issue_Status_Cd] [int] NOT NULL,
[Issue_Entity_Owner_Cd] [int] NOT NULL,
[Issue_Owner_User_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Log__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Log__Last_Change_Ts] DEFAULT (getdate()),
[Type_Of_Issue_Owner_Cd] [int] NOT NULL,
[Is_Blocker] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Log__Is_Blocker] DEFAULT ((0)),
[Blocker_Action_Date] [date] NULL,
[Is_Auto_Linked] [bit] NOT NULL CONSTRAINT [df_Invoice_Collection_Issue_Log_Is_Auto_Linked] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Log] ADD CONSTRAINT [pk_Invoice_Collection_Issue_Log] PRIMARY KEY CLUSTERED  ([Invoice_Collection_Issue_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Log] ADD CONSTRAINT [un_Invoice_Collection_Issue_Log__Invoice_Collection_Activity_Id__Invoice_Collection_Queue_Id] UNIQUE NONCLUSTERED  ([Invoice_Collection_Activity_Id], [Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Issue_Log__Invoice_Collection_Queue_Id] ON [dbo].[Invoice_Collection_Issue_Log] ([Invoice_Collection_Queue_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Invoice_Collection_Issue_Log__Invoice_Collection_Queue_Id__Issue_Status_Cd] ON [dbo].[Invoice_Collection_Issue_Log] ([Invoice_Collection_Queue_Id], [Issue_Status_Cd]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Log] ADD CONSTRAINT [fk_Invoice_Collection_Activity__Invoice_Collection_Issue_Log] FOREIGN KEY ([Invoice_Collection_Activity_Id]) REFERENCES [dbo].[Invoice_Collection_Activity] ([Invoice_Collection_Activity_Id])
GO
ALTER TABLE [dbo].[Invoice_Collection_Issue_Log] ADD CONSTRAINT [fk_Invoice_Collection_Queue__Invoice_Collection_Issue_Log] FOREIGN KEY ([Invoice_Collection_Queue_Id]) REFERENCES [dbo].[Invoice_Collection_Queue] ([Invoice_Collection_Queue_Id])
GO
