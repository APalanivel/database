CREATE TABLE [dbo].[SR_RFP_ARCHIVE_METER_CONTRACTS]
(
[SR_RFP_ARCHIVE_METER_CONTRACTS_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_RFP_ACCOUNT_METER_MAP_ID] [int] NOT NULL,
[CONTRACT_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CONTRACT_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CURRENT_SUPPLIER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the contract  information for a RFP when a RFP account/group is closed. This table would store the archived information related to a contract in CBMS, so that if these information change in the future, the archived info at the time of closing a RFP account can be referenced from this table.', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ARCHIVE_METER_CONTRACTS', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_ARCHIVE_METER_CONTRACTS] ADD 
CONSTRAINT [PK__SR_RFP_ARCHIVE_M__04076F5D] PRIMARY KEY CLUSTERED  ([SR_RFP_ARCHIVE_METER_CONTRACTS_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_RFP_ARCHIVE_METER_CONTRACTS] ADD
CONSTRAINT [FK_SR_RFP_ARCHIVE_METER_CONTRACTS_SR_RFP_ACCOUNT_METER_MAP] FOREIGN KEY ([SR_RFP_ACCOUNT_METER_MAP_ID]) REFERENCES [dbo].[SR_RFP_ACCOUNT_METER_MAP] ([SR_RFP_ACCOUNT_METER_MAP_ID])

GO
