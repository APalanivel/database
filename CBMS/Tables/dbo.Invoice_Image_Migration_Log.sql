CREATE TABLE [dbo].[Invoice_Image_Migration_Log]
(
[CBMS_Image_Id] [int] NOT NULL,
[Status_Cd] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Image_Migration_Log__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Image_Migration_Log__Last_Change_Ts] DEFAULT (getdate()),
[UBM_Batch_Master_Log_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Image_Migration_Log] ADD CONSTRAINT [pk_Invoice_Image_Migration_Log] PRIMARY KEY CLUSTERED  ([CBMS_Image_Id]) ON [DB_DATA01]
GO
