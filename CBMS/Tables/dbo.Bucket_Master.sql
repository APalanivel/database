CREATE TABLE [dbo].[Bucket_Master]
(
[Bucket_Master_Id] [int] NOT NULL IDENTITY(100000, 1) NOT FOR REPLICATION,
[Commodity_Id] [int] NOT NULL,
[Bucket_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Category] [bit] NOT NULL CONSTRAINT [df_Bucket_Master_Is_Category] DEFAULT ((0)),
[Bucket_Type_Cd] [int] NOT NULL,
[Aggregation_Level_Cd] [int] NOT NULL,
[Is_Shown_on_Site] [bit] NOT NULL CONSTRAINT [df_Bucket_Master_Is_Shown_on_Site] DEFAULT ((0)),
[Is_Shown_On_Account] [bit] NOT NULL CONSTRAINT [df_Bucket_Master_is_Shown_On_Account] DEFAULT ((0)),
[Is_Shown_On_Invoice] [bit] NOT NULL CONSTRAINT [df_Bucket_Master_Is_Shown_On_Invoice] DEFAULT ((0)),
[Is_Required_For_Variance_Test] [bit] NULL CONSTRAINT [df__Bucket_Master__Is_Required_For_Variance_Test] DEFAULT ((0)),
[Sort_Order] [int] NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df__Bucket_Master__Is_Active] DEFAULT ((1)),
[Default_Uom_Type_Id] [int] NULL,
[External_Reference_Key] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Bucket_Stream_Cd] [int] NULL,
[Is_Reported] [bit] NOT NULL CONSTRAINT [df__Bucket_Master__Is_Reported] DEFAULT ((0)),
[Report_Decimal_Place] [int] NULL CONSTRAINT [df_Bucket_Master__Report_Decimal_Place] DEFAULT ((0))
) ON [DBData_Cost_Usage]
CREATE NONCLUSTERED INDEX [ix_Bucket_Master__Bucket_Name__Bucket_id] ON [dbo].[Bucket_Master] ([Bucket_Name], [Bucket_Master_Id]) INCLUDE ([Commodity_Id]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Bucket_Master] ADD 
CONSTRAINT [pk_Bucket_Master] PRIMARY KEY CLUSTERED  ([Bucket_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
CREATE NONCLUSTERED INDEX [ix_Bucket_Master__Bucket_id__Bucket_Name] ON [dbo].[Bucket_Master] ([Bucket_Master_Id], [Bucket_Name]) INCLUDE ([Commodity_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Bucket_Master__Commodity_ID] ON [dbo].[Bucket_Master] ([Commodity_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Bucket_Master__Bucket_Type_Cd] ON [dbo].[Bucket_Master] ([Bucket_Type_Cd]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', 'Contains cost and usage data , and demand buckets by commodity', 'SCHEMA', N'dbo', 'TABLE', N'Bucket_Master', NULL, NULL
GO
