CREATE TABLE [dbo].[Vendor_Contact_Map]
(
[VENDOR_ID] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Vendor_Contact_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Vendor_Contact_Map] ADD CONSTRAINT [pk_Vendor_Contact] PRIMARY KEY CLUSTERED  ([VENDOR_ID], [Contact_Info_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Vendor_Contact_Map] ADD CONSTRAINT [fk_Contact_Info__Vendor_Contact] FOREIGN KEY ([Contact_Info_Id]) REFERENCES [dbo].[Contact_Info] ([Contact_Info_Id])
GO
ALTER TABLE [dbo].[Vendor_Contact_Map] ADD CONSTRAINT [fk_VENDOR__Vendor_Contact] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])
GO
