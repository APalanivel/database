CREATE TABLE [dbo].[seCommodity]
(
[CommodityId] [int] NOT NULL IDENTITY(1, 1),
[Commodity] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Symbol] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Label] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TradingMonth] [datetime] NOT NULL,
[ExpirationDate] [datetime] NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seCommodity', NULL, NULL
GO

ALTER TABLE [dbo].[seCommodity] ADD 
CONSTRAINT [PK_seCommodity] PRIMARY KEY CLUSTERED  ([CommodityId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
