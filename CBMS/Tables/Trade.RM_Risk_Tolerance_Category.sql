CREATE TABLE [Trade].[RM_Risk_Tolerance_Category]
(
[RM_Risk_Tolerance_Category_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Risk_Tolerance_Category] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Client_Id] [int] NOT NULL CONSTRAINT [DFRM_Risk_Tolerance_CategoryClient_Id] DEFAULT ((-1)),
[Created_Ts] [datetime] NULL CONSTRAINT [DFRM_Risk_Tolerance_CategoryCreated_Ts] DEFAULT (getdate()),
[Last_Updated_By] [int] NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [DFRM_Risk_Tolerance_CategoryLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[RM_Risk_Tolerance_Category] ADD CONSTRAINT [Pk_RM_Risk_Tolerance_Category] PRIMARY KEY CLUSTERED  ([RM_Risk_Tolerance_Category_Id]) ON [DB_DATA01]
GO
