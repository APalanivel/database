CREATE TABLE [dbo].[Contact_Info]
(
[Contact_Info_Id] [int] NOT NULL IDENTITY(1, 1),
[Contact_Type_Cd] [int] NOT NULL,
[Contact_Level_Cd] [int] NOT NULL,
[First_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Last_Name] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email_Address] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax_Number] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Job_Position] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Location] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment_Id] [int] NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [DFContact_InfoIs_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFContact_InfoCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFContact_InfoLast_Change_Ts] DEFAULT (getdate()),
[Language_Cd] [int] NOT NULL,
[User_Info_Id] [int] NULL,
[Address] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Department_Name] [nvarchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mobile_Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State_Id] [int] NULL,
[Zipcode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Contact_Info] ADD CONSTRAINT [pk_Contact_Info] PRIMARY KEY CLUSTERED  ([Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Contact_Info__Contact_Type_Cd__Contact_Level_Cd__Is_Active] ON [dbo].[Contact_Info] ([Contact_Type_Cd], [Contact_Level_Cd], [Is_Active]) ON [DB_DATA01]
GO
