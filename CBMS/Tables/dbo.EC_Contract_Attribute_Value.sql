CREATE TABLE [dbo].[EC_Contract_Attribute_Value]
(
[EC_Contract_Attribute_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[EC_Contract_Attribute_Id] [int] NOT NULL,
[EC_Contract_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Contract_Attribute_Value__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Value] ADD CONSTRAINT [pk_EC_Contract_Attribute_Value] PRIMARY KEY NONCLUSTERED  ([EC_Contract_Attribute_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Value] ADD CONSTRAINT [unc_EC_Contract_Attribute_Value__EC_Contract_Attribute_Id__EC_Contract_Attribute_Value] UNIQUE CLUSTERED  ([EC_Contract_Attribute_Id], [EC_Contract_Attribute_Value]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Contract_Attribute_Value] ADD CONSTRAINT [fk_EC_Contract_Attribute__EC_Contract_Attribute_Value] FOREIGN KEY ([EC_Contract_Attribute_Id]) REFERENCES [dbo].[EC_Contract_Attribute] ([EC_Contract_Attribute_Id])
GO
