CREATE TABLE [dbo].[Utility_Dtl_Election]
(
[Utility_Dtl_Election_Id] [int] NOT NULL IDENTITY(1, 1),
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Election_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Enrollment_Period] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notification_Deadline] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Other_Stipulation] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'captures the EP or NG specific Annual Election information', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Election', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Election] ADD 
CONSTRAINT [pk_Utility_Dtl_Election] PRIMARY KEY CLUSTERED  ([Utility_Dtl_Election_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Election__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Election] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Election__Comment_ID] ON [dbo].[Utility_Dtl_Election] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Election] ADD
CONSTRAINT [fk_Utility_Dtl_Election__Vendor_Commodity_Map_Id] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Election] ADD
CONSTRAINT [fk_Utility_Dtl_Election__Comment_ID] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
