CREATE TABLE [dbo].[DV2_SESSION_INFO]
(
[SESSION_INFO_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IP_ADDRESS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[APP_SERVER_ID] [int] NULL,
[USER_INFO_ID] [int] NULL,
[CLIENT_ID] [int] NULL,
[LAST_UPDATED] [datetime] NOT NULL,
[User_Agent] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated in CBMS.  Original tables reside in DV2.', 'SCHEMA', N'dbo', 'TABLE', N'DV2_SESSION_INFO', NULL, NULL
GO
