CREATE TABLE [dbo].[Account_Commodity_Invoice_Recalc_Type]
(
[Account_Commodity_Invoice_Recalc_Type_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Commodity_ID] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[Invoice_Recalc_Type_Cd] [int] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Invoice_Reclac_Type__Created_Ts] DEFAULT (getdate()),
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Commodity_Invoice_Reclac_Type__Last_Change_Ts] DEFAULT (getdate()),
[Determinant_Source_Cd] [int] NULL,
[Source_Cd] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Commodity_Invoice_Recalc_Type] ADD CONSTRAINT [PK_Account_Commodity_Invoice_Reclac_Type_Id] PRIMARY KEY CLUSTERED  ([Account_Commodity_Invoice_Recalc_Type_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Commodity_Invoice_Recalc_Type] ADD CONSTRAINT [unc__Account_Commodity_Invoice_Reclac_Type__Account_Id__Commodity_ID__Start_Dt__End_Dt] UNIQUE NONCLUSTERED  ([Account_Id], [Commodity_ID], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Commodity_Invoice_Recalc_Type] ADD CONSTRAINT [fk__Account_Commodity_Invoice_Reclac_Type__Account_Id] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
