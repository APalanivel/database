CREATE TABLE [dbo].[Variance_Rule_Dtl]
(
[Variance_Rule_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Variance_Rule_Id] [int] NOT NULL,
[Variance_Consumption_Level_Id] [int] NOT NULL,
[Is_Data_Entry_Only] [bit] NOT NULL CONSTRAINT [DF__Variance___Is_Da__1271F526] DEFAULT ((0)),
[IS_Active] [bit] NOT NULL CONSTRAINT [DF__Variance___IS_Ac__1366195F] DEFAULT ((1)),
[Default_Tolerance] [decimal] (16, 4) NOT NULL,
[Test_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [IX_Variance_Rule_Dtl__Variance_Rule_Id__Variance_Consumption_Level_Id] ON [dbo].[Variance_Rule_Dtl] ([Variance_Rule_Id], [Variance_Consumption_Level_Id]) INCLUDE ([Default_Tolerance], [IS_Active], [Is_Data_Entry_Only], [Test_Description], [Variance_Rule_Dtl_Id]) ON [DBData_Invoice]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Variance rule details', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Rule_Dtl', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Rule_Dtl] ADD 
CONSTRAINT [PK_Variance_Rule_Dtl] PRIMARY KEY CLUSTERED  ([Variance_Rule_Dtl_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
ALTER TABLE [dbo].[Variance_Rule_Dtl] ADD
CONSTRAINT [FK_Variance_Consumption_LevelVariance_Rule_Dtl] FOREIGN KEY ([Variance_Consumption_Level_Id]) REFERENCES [dbo].[Variance_Consumption_Level] ([Variance_Consumption_Level_Id])
ALTER TABLE [dbo].[Variance_Rule_Dtl] ADD
CONSTRAINT [FK_Variance_RuleVariance_Rule_Dtl] FOREIGN KEY ([Variance_Rule_Id]) REFERENCES [dbo].[Variance_Rule] ([Variance_Rule_Id])
GO
