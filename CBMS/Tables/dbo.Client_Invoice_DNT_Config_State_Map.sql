CREATE TABLE [dbo].[Client_Invoice_DNT_Config_State_Map]
(
[Client_Invoice_DNT_Config_State_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Invoice_DNT_Config_Id] [int] NOT NULL,
[STATE_ID] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config_State_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_State_Map] ADD CONSTRAINT [pk_Client_Invoice_DNT_Config_State_Map] PRIMARY KEY CLUSTERED  ([Client_Invoice_DNT_Config_State_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_State_Map] ADD CONSTRAINT [un_Client_Invoice_DNT_Config_State_Map__Client_Invoice_DNT_Config_State_Map_Id__STATE_ID] UNIQUE NONCLUSTERED  ([Client_Invoice_DNT_Config_State_Map_Id], [STATE_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config_State_Map] ADD CONSTRAINT [RefSTATE2888] FOREIGN KEY ([STATE_ID]) REFERENCES [dbo].[STATE] ([STATE_ID])
GO
