CREATE TABLE [ETL].[Data_Conversion_Accounts_Stage]
(
[Site id] [int] NULL,
[Vendor ID] [int] NULL,
[Commodity] [int] NULL,
[ACCOUNT NUMBER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[METER] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rate ID] [int] NULL,
[Service LEVEL] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Consumption Level ID] [int] NULL,
[UBM Account ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEO] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Site Address] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is Default To Primary] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Purchase Method] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Consolidated_Billing_Posted_To_Utility] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email_Adddress] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Meter_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Recalc_Type] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Recalc_Type_Start_Dt] [date] NULL,
[Recalc_Type_End_Dt] [date] NULL,
[Alternate_Account_Number] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Staging table to bulk load the Utility accounts. Used by the data upload tool runs from tidal.', 'SCHEMA', N'ETL', 'TABLE', N'Data_Conversion_Accounts_Stage', NULL, NULL
GO
