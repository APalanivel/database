CREATE TABLE [dbo].[BUDGET_CONVERSION]
(
[CLIENT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DIVISION] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SITE] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCOUNT_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCOUNT_ID] [int] NOT NULL,
[MONTH_IDENTIFIER] [datetime] NOT NULL,
[BUDGET_USAGE] [decimal] (32, 16) NULL,
[MULTIPLIER] [decimal] (32, 16) NULL,
[VARIABLE] [decimal] (32, 16) NULL,
[ADDER] [decimal] (32, 16) NULL,
[GEN_OR_COMM] [decimal] (32, 16) NULL,
[TRANSPORTATION] [decimal] (32, 16) NULL,
[TRANSMISSION] [decimal] (32, 16) NULL,
[DISTRIBUTION] [decimal] (32, 16) NULL,
[OTHER_UNIT] [decimal] (32, 16) NULL,
[OTHER_FIXED] [decimal] (32, 16) NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Temporary table, not used in the application', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_CONVERSION', NULL, NULL
GO
