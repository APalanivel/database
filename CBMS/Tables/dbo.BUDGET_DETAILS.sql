CREATE TABLE [dbo].[BUDGET_DETAILS]
(
[BUDGET_DETAIL_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[BUDGET_ACCOUNT_ID] [int] NOT NULL,
[MONTH_IDENTIFIER] [smalldatetime] NOT NULL,
[VARIABLE] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VARIABLE_VALUE] [decimal] (32, 16) NULL,
[TRANSPORTATION] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSPORTATION_VALUE] [decimal] (32, 16) NULL,
[TRANSMISSION] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSMISSION_VALUE] [decimal] (32, 16) NULL,
[DISTRIBUTION] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISTRIBUTION_VALUE] [decimal] (32, 16) NULL,
[OTHER_BUNDLED] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OTHER_BUNDLED_VALUE] [decimal] (32, 16) NULL,
[OTHER_FIXED_COSTS] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OTHER_FIXED_COSTS_VALUE] [decimal] (32, 16) NULL,
[SOURCING_TAX] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOURCING_TAX_VALUE] [decimal] (32, 16) NULL,
[RATES_TAX] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATES_TAX_VALUE] [decimal] (32, 16) NULL,
[BUDGET_USAGE] [decimal] (32, 16) NULL,
[BUDGET_USAGE_TYPE_ID] [int] NULL,
[NYMEX_FORECAST] [decimal] (32, 16) NULL,
[IS_MANUAL_GENERATION] [bit] NULL,
[IS_MANUAL_SOURCING_TAX] [bit] NULL,
[TOTAL_COST] AS (((((((isnull([Variable_value],(0))+isnull([transportation_value],(0)))+isnull([transmission_value],(0)))+isnull([distribution_value],(0)))+isnull([other_bundled_value],(0)))+isnull([other_fixed_costs_value],(0)))+isnull([sourcing_tax_value],(0)))+isnull([rates_tax_value],(0))),
[Row_Version] [timestamp] NOT NULL
) ON [DBData_Budget]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.tr_Budget_Details_Audit

DESCRIPTION:

	Used to log DML actions happening on Budget_Details in to Budget_Details_Audit table.

	Audit_Function	Values
			1	- Insert
			0	- Update
		   -1   - Delete

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN
		UPDATE BUDGET_DETAILS
			SET Variable_Value = 13.1506000000000000
		WHERE
			Budget_Detail_id =  8064
			
		SELECT * FROM Budget_Details_Audit WHERE Budget_Detail_id = 8064


	ROLLBACK TRAN

	SELECT * FROM BUDGET_DETAILS
AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	06/03/2010	Created

******/

CREATE TRIGGER [dbo].[tr_Budget_Details_Audit]
	ON [dbo].[BUDGET_DETAILS]
	FOR INSERT, UPDATE, DELETE
AS
BEGIN

	SET NOCOUNT ON;

	INSERT dbo.Budget_Details_Audit
	(
		Audit_Function
		,Audit_Ts
		,Budget_Detail_Id
		,Budget_Account_Id
		,Month_Identifier
	)
	SELECT
		CASE
			WHEN d.Budget_Detail_Id IS NULL THEN 1		-- Inserted
			WHEN i.Budget_Detail_Id IS NULL THEN -1		-- Deleted
			ELSE 0										-- Updated
		END
		,GETDATE()
		,ISNULL(i.Budget_Detail_Id, d.Budget_Detail_Id)		
		,ISNULL(i.Budget_Account_Id, d.Budget_Account_Id)
		,ISNULL(i.Month_Identifier, d.Month_Identifier)
	FROM
		INSERTED i
		FULL OUTER JOIN DELETED d
			ON d.Budget_Detail_Id = i.Budget_Detail_Id

END
GO
ALTER TABLE [dbo].[BUDGET_DETAILS] ADD CONSTRAINT [XPKBUDGET_DETAILS] PRIMARY KEY CLUSTERED  ([BUDGET_DETAIL_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
CREATE NONCLUSTERED INDEX [IX_BUDGET_ACCOUNT] ON [dbo].[BUDGET_DETAILS] ([BUDGET_ACCOUNT_ID], [MONTH_IDENTIFIER]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[BUDGET_DETAILS] ADD CONSTRAINT [FK_BUDGET_ACCOUNT__BUDGET_DETAILS] FOREIGN KEY ([BUDGET_ACCOUNT_ID]) REFERENCES [dbo].[BUDGET_ACCOUNT] ([BUDGET_ACCOUNT_ID])
GO
ALTER TABLE [dbo].[BUDGET_DETAILS] WITH NOCHECK ADD CONSTRAINT [FK_BUDGET_DETAILS_ENTITY] FOREIGN KEY ([BUDGET_USAGE_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'At budget account level, budget components (values for transporation, distribution, etc)', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_DETAILS', NULL, NULL
GO
