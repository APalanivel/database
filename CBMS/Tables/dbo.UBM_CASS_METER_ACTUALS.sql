CREATE TABLE [dbo].[UBM_CASS_METER_ACTUALS]
(
[UTIL_BILL_HEADER_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUILDING_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACCOUNT_SERVICE_ID] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REF_METER_NO] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RATE_CODE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VENDOR_BILLING_DATE] [datetime] NULL,
[BILL_SERVICE_PERIOD_START_DATE] [datetime] NULL,
[BILL_SERVICE_PERIOD_END_DATE] [datetime] NULL,
[METER_LAST_READING] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[METER_CURRENT_READING] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEMAND_QUANTITY] [numeric] (32, 16) NULL,
[DEMAND_UOM] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USAGE_QUANTITY] [numeric] (32, 16) NULL,
[USAGE_UOM] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POWER_FACTOR] [numeric] (32, 16) NULL,
[READING_TYPE_CODE] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[READING_TYPE_DESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UBM_BATCH_MASTER_LOG_ID] [int] NULL,
[Determinant_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Ubm_Sub_Bucket_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_UBM]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the METER ACTUAL raw data of the CASS UBM CSV feed.  XML feed is in the UBM_Cass database, not CBMS.', 'SCHEMA', N'dbo', 'TABLE', N'UBM_CASS_METER_ACTUALS', NULL, NULL
GO



CREATE CLUSTERED INDEX [CX_UBM_BATCH_MASTER_LOG_ID] ON [dbo].[UBM_CASS_METER_ACTUALS] ([UBM_BATCH_MASTER_LOG_ID]) WITH (FILLFACTOR=80) ON [DBData_UBM]

CREATE NONCLUSTERED INDEX [IX_UBM_CASS_METER_ACTUALS_1] ON [dbo].[UBM_CASS_METER_ACTUALS] ([UTIL_BILL_HEADER_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_UBM_CASS_METER_ACTUALS] ON [dbo].[UBM_CASS_METER_ACTUALS] ([ACCOUNT_SERVICE_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]











GO
