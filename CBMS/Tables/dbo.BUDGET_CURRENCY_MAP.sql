CREATE TABLE [dbo].[BUDGET_CURRENCY_MAP]
(
[BUDGET_CURRENCY_MAP_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[BUDGET_ID] [int] NOT NULL,
[CURRENCY_UNIT_ID] [int] NOT NULL,
[CONVERSION_FACTOR] [decimal] (32, 16) NOT NULL
) ON [DBData_Budget]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.tr_Budget_Currency_Map_Audit

DESCRIPTION:

	Used to log DML actions happening on Budget_Currency_Map in to Budget_Currency_Map_Audit table.
	
	Audit Function values
			1 - Insert
			0 - Update
		   -1 - Delete

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN

		UPDATE
			Budget_Currency_Map
			SET Conversion_Factor = 0.0960450000000000
		WHERE
			Budget_Currency_Map_Id =  680

		SELECT * FROM BUDGET_CURRENCY_MAP_Audit WHERE Budget_Currency_Map_Id =  680

	ROLLBACK TRAN

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	06/03/2010	Created

******/

CREATE TRIGGER [dbo].[tr_Budget_Currency_Map_Audit]
	ON [dbo].[BUDGET_CURRENCY_MAP]
	FOR INSERT, UPDATE, DELETE
AS
BEGIN

	SET NOCOUNT ON;

	INSERT dbo.Budget_Currency_Map_Audit
	(
		Audit_Function
		,Audit_Ts
		,Budget_Currency_Map_Id
		,Budget_Id
		,Currency_Unit_Id
	)
	SELECT
		CASE
			WHEN d.Budget_Currency_Map_Id IS NULL THEN  1		-- Inserted
			WHEN i.Budget_Currency_Map_Id IS NULL THEN -1		-- Deleted
			ELSE 0												-- Updated
		END
		,GETDATE()
		,ISNULL(i.Budget_Currency_Map_Id, d.Budget_Currency_Map_Id)
		,ISNULL(i.Budget_Id, d.Budget_Id)
		,ISNULL(i.Currency_Unit_Id, d.Currency_Unit_Id)
	FROM
		INSERTED i
		FULL OUTER JOIN DELETED d
			ON d.Budget_Currency_Map_Id = i.Budget_Currency_Map_Id

END
GO
ALTER TABLE [dbo].[BUDGET_CURRENCY_MAP] ADD CONSTRAINT [XPKBUDGET_CURRENCY_MAP] PRIMARY KEY CLUSTERED  ([BUDGET_CURRENCY_MAP_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
CREATE NONCLUSTERED INDEX [IX_BUDGET_ID] ON [dbo].[BUDGET_CURRENCY_MAP] ([BUDGET_ID], [CONVERSION_FACTOR], [CURRENCY_UNIT_ID]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[BUDGET_CURRENCY_MAP] ADD CONSTRAINT [FK_BUDGET_CURRENCY_UNIT_ID_2] FOREIGN KEY ([CURRENCY_UNIT_ID]) REFERENCES [dbo].[CURRENCY_UNIT] ([CURRENCY_UNIT_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Allows for budget currency conversion factors specific to a budget', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_CURRENCY_MAP', NULL, NULL
GO
