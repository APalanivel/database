CREATE TABLE [dbo].[seContentPlacement]
(
[ContentPlacementId] [int] NOT NULL IDENTITY(1, 1),
[ContentPageId] [int] NOT NULL,
[PlacementLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seContentPlacement', NULL, NULL
GO

ALTER TABLE [dbo].[seContentPlacement] ADD 
CONSTRAINT [PK_seContentPlacement] PRIMARY KEY CLUSTERED  ([ContentPlacementId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
