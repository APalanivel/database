CREATE TABLE [dbo].[Cbms_Filter_Param]
(
[Cbms_Filter_Param_Id] [int] NOT NULL IDENTITY(1, 1),
[Cbms_Rule_Filter_Id] [int] NOT NULL,
[Param_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Default_Value] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Param_Ordinal] [int] NOT NULL,
[Is_Quoted] [bit] NOT NULL CONSTRAINT [DFCbms_Filter_ParamIs_Quoted] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Filter_Param] ADD CONSTRAINT [pk_Cbms_Filter_Param] PRIMARY KEY CLUSTERED  ([Cbms_Filter_Param_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Filter_Param] ADD CONSTRAINT [un_Cbms_Filter_Param__Cbms_Rule_Filter_Id__Param_Name] UNIQUE NONCLUSTERED  ([Cbms_Rule_Filter_Id], [Param_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Filter_Param] ADD CONSTRAINT [fk_Cbms_Rule_Filter__Cbms_Filter_Param] FOREIGN KEY ([Cbms_Rule_Filter_Id]) REFERENCES [dbo].[Cbms_Rule_Filter] ([Cbms_Rule_Filter_Id])
GO
