CREATE TABLE [dbo].[BUDGET_CONTRACT_BUDGET]
(
[BUDGET_CONTRACT_BUDGET_ID] [int] NOT NULL IDENTITY(1, 1),
[CONTRACT_ID] [int] NOT NULL,
[COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Budget]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Tracks that a contract has been used in a budget - but does not track the budget id.
Not sure that this is tied back to a specific budget at all, but just perhaps the months are pulled to a budget containing those months?', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_CONTRACT_BUDGET', NULL, NULL
GO

ALTER TABLE [dbo].[BUDGET_CONTRACT_BUDGET] ADD 
CONSTRAINT [XPKBUDGET_CONTRACT_BUDGET] PRIMARY KEY CLUSTERED  ([BUDGET_CONTRACT_BUDGET_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
CREATE NONCLUSTERED INDEX [IX_CONTRACT] ON [dbo].[BUDGET_CONTRACT_BUDGET] ([CONTRACT_ID]) ON [DB_INDEXES01]



GO
