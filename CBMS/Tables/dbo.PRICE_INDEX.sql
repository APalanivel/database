CREATE TABLE [dbo].[PRICE_INDEX]
(
[PRICE_INDEX_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[INDEX_ID] [int] NOT NULL,
[PRICING_POINT] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INDEX_DESCRIPTION] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_UNIT_ID] [int] NULL,
[VOLUME_UNIT_ID] [int] NULL,
[RA_Price_Point_Display_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Commodity_Id] [int] NOT NULL CONSTRAINT [DF__PRICE_IND__Commo__6503DE00] DEFAULT ((-1)),
[Price_Point_Short_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL CONSTRAINT [DF__PRICE_IND__Creat__7BB2392E] DEFAULT ((-1)),
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df__Price_Index__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df__Price_Index__Last_Change_Ts] DEFAULT (getdate())
) ON [DBData_Risk_MGMT]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Price_Index_Transmission

DESCRIPTION: Trigger on Price_Index to send change data to the 
	Change control service.

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Hariharasuthan Ganesan

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG			2016-02-02	Created to send the price_index changes to sys
******/
CREATE TRIGGER [dbo].[tr_Price_Index_Transmission] ON [dbo].[PRICE_INDEX]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN

      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message_Price_Index XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255);

      SET @Message_Price_Index = ( SELECT
                                    prc.Price_Index_Id
                                   ,prc.Index_Name
                                   ,prc.Price_Point_Short_Name AS CalculationAlias
								   ,prc.Commodity_Id
								   ,com.Commodity_Name
                                   ,prc.Uom_Id
                                   ,uom.ENTITY_NAME AS Uom_Name
                                   ,prc.Currency_Unit_Id
                                   ,cu.CURRENCY_UNIT_NAME AS Currency_Unit_Name
                                   ,prc.Sys_Change_Operation
                                   ,@@SERVERNAME AS Server_Name
                                   FROM
                                    ( SELECT
                                          ISNULL(ins.PRICE_INDEX_ID, del.PRICE_INDEX_ID) AS Price_Index_Id
                                         ,ins.PRICING_POINT AS Index_Name
                                         ,ins.Price_Point_Short_Name AS Price_Point_Short_Name
                                         ,ins.VOLUME_UNIT_ID AS Uom_Id
                                         ,ins.CURRENCY_UNIT_ID AS Currency_Unit_Id
                                         ,ins.Commodity_Id
                                         ,CASE WHEN ins.PRICE_INDEX_ID IS NULL
                                                    AND del.PRICE_INDEX_ID IS NOT NULL THEN 'D'
                                               WHEN ins.PRICE_INDEX_ID IS NOT NULL
                                                    AND del.PRICE_INDEX_ID IS NOT NULL THEN 'U'
                                               WHEN ins.PRICE_INDEX_ID IS NOT NULL
                                                    AND del.PRICE_INDEX_ID IS NULL THEN 'I'
                                          END AS Sys_Change_Operation
                                      FROM
                                          Inserted ins
                                          FULL JOIN Deleted del
                                                ON del.PRICE_INDEX_ID = ins.PRICE_INDEX_ID ) prc
                                    LEFT OUTER JOIN dbo.CURRENCY_UNIT cu
                                          ON cu.CURRENCY_UNIT_ID = prc.Currency_Unit_Id
                                    LEFT OUTER JOIN dbo.ENTITY uom
                                          ON uom.ENTITY_ID = prc.Uom_Id
                                    LEFT OUTER JOIN dbo.Commodity com
                                          ON com.Commodity_Id = prc.Commodity_Id
            FOR
                                   XML PATH('Price_Index_Change')
                                      ,ELEMENTS
                                      ,ROOT('Price_Index_Changes') ); 

      IF @Message_Price_Index IS NOT NULL
            BEGIN

                  SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Currency_And_Price_Change';

				  -- Cycle through the targets in Change_Control Targt and sent the message to each
                  DECLARE cDialog CURSOR
                  FOR
                  ( SELECT
                        Target_Service
                       ,Target_Contract
                    FROM
                        Service_Broker_Target
                    WHERE
                        Table_Name = 'dbo.Price_Index');

                  OPEN cDialog;    
                  FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;      
                  WHILE @@Fetch_Status = 0
                        BEGIN    
						
                              EXEC Service_Broker_Conversation_Handle_Sel
                                    @From_Service
                                   ,@Target_Service
                                   ,@Target_Contract
                                   ,@Conversation_Handle OUTPUT;
                              SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Transmission/Message/Cbms_Price_Index_Change] (@Message_Price_Index);

                              FETCH NEXT FROM cDialog INTO @Target_Service, @Target_Contract;
                        END;
                  CLOSE cDialog;
                  DEALLOCATE cDialog;

            END;

END;

;
GO

EXEC sp_addextendedproperty N'MS_Description', N'This table would store the index and pricing points', 'SCHEMA', N'dbo', 'TABLE', N'PRICE_INDEX', NULL, NULL
GO

ALTER TABLE [dbo].[PRICE_INDEX] ADD 
CONSTRAINT [PRICE_INDEX_PK] PRIMARY KEY CLUSTERED  ([PRICE_INDEX_ID]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
CREATE UNIQUE NONCLUSTERED INDEX [PRICE_INDEX_U_1] ON [dbo].[PRICE_INDEX] ([PRICE_INDEX_ID], [PRICING_POINT]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [PRICE_INDEX_N_1] ON [dbo].[PRICE_INDEX] ([INDEX_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]










GO
