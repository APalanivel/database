CREATE TABLE [Trade].[Financial_Counterparty_Client_Contact_Map]
(
[Financial_Counterparty_Client_Contact_Map_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[RM_COUNTERPARTY_ID] [int] NOT NULL,
[Contact_Info_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Financial_Counterparty_Client_Contact_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Financial_Counterparty_Client_Contact_Map] ADD CONSTRAINT [pk_Financial_Counterparty_Client_Contact_Map] PRIMARY KEY CLUSTERED  ([Financial_Counterparty_Client_Contact_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Financial_Counterparty_Client_Contact_Map] ADD CONSTRAINT [un_Financial_Counterparty_Client_Contact_Map__Client_Id__RM_COUNTERPARTY_ID__Contact_Info_Id] UNIQUE NONCLUSTERED  ([Client_Id], [RM_COUNTERPARTY_ID], [Contact_Info_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Financial_Counterparty_Client_Contact_Map__RM_COUNTERPARTY_ID] ON [Trade].[Financial_Counterparty_Client_Contact_Map] ([RM_COUNTERPARTY_ID]) ON [DB_DATA01]
GO
