CREATE TABLE [Trade].[Deal_Ticket_Client_Hier_TXN_Status]
(
[Deal_Ticket_Client_Hier_TXN_Status_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Ticket_Client_Hier_Workflow_Status_Id] [int] NOT NULL,
[Cbms_Trade_Number] [int] NOT NULL,
[Date_Data_Amended] [datetime] NOT NULL,
[Transaction_Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Trade_Price_Id] [int] NULL,
[Cbms_Counterparty_Id] [int] NULL,
[Id] [int] NULL,
[Assignee] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_TXN_Status_Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Hier_TXN_Status_Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_TXN_Status] ADD CONSTRAINT [pk_Deal_Ticket_Client_Hier_TXN_Status] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Hier_TXN_Status_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_TXN_Status] ADD CONSTRAINT [un_Deal_Ticket_Client_Hier_TXN_Status__Deal_Ticket_Client_Hier_Workflow_Status_Id] UNIQUE NONCLUSTERED  ([Deal_Ticket_Client_Hier_Workflow_Status_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_TXN_Status] ADD CONSTRAINT [fk_Deal_Ticket_Client_Hier_Workflow_Status] FOREIGN KEY ([Deal_Ticket_Client_Hier_Workflow_Status_Id]) REFERENCES [Trade].[Deal_Ticket_Client_Hier_Workflow_Status] ([Deal_Ticket_Client_Hier_Workflow_Status_Id])
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Hier_TXN_Status] ADD CONSTRAINT [fk_GDeal_Ticket_Client_Hier_TXN_Status__Trade_Price] FOREIGN KEY ([Trade_Price_Id]) REFERENCES [Trade].[Trade_Price] ([Trade_Price_Id])
GO
