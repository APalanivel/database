CREATE TABLE [dbo].[Variance_Extraction_Service_Log]
(
[Variance_Extraction_Service_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Hier_Id] [int] NOT NULL,
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Service_Month] [date] NULL,
[Error_Desc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Variance_Extraction_Service_Log__Created_Ts] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Variance_Extraction_Service_Log] ADD CONSTRAINT [pk_Variance_Extraction_Service_Log] PRIMARY KEY CLUSTERED  ([Variance_Extraction_Service_Log_Id]) ON [DB_DATA01]
GO
