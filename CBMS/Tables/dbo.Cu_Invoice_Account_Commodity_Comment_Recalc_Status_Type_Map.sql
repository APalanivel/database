CREATE TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map]
(
[Cu_Invoice_Account_Commodity_Comment_Id] [int] NULL,
[Recalc_Status_Type_Id] [int] NULL
) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map] ON [dbo].[Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map] ([Cu_Invoice_Account_Commodity_Comment_Id]) INCLUDE ([Recalc_Status_Type_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Account_Commodity_Comment_Recalc_Status_Type_Map] ADD CONSTRAINT [fk_Cu_Invoice_Account_Commodity_Comment] FOREIGN KEY ([Cu_Invoice_Account_Commodity_Comment_Id]) REFERENCES [dbo].[Cu_Invoice_Account_Commodity_Comment] ([Cu_Invoice_Account_Commodity_Comment_Id])
GO
