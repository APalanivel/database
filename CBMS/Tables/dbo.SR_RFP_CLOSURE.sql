CREATE TABLE [dbo].[SR_RFP_CLOSURE]
(
[SR_RFP_CLOSURE_ID] [int] NOT NULL IDENTITY(1, 1),
[SR_ACCOUNT_GROUP_ID] [int] NOT NULL,
[IS_BID_GROUP] [bit] NULL,
[CLOSE_ACTION_TYPE_ID] [int] NOT NULL,
[VENDOR_ID] [int] NULL,
[CONTRACT_ID] [int] NULL,
[BENCHMARK_INDEX_ADDER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENCHMARK_FIXED_WHOLESALE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SAVINGS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SAVINGS_COMMENTS] [varchar] (2200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_AWARDED_EMAIL_SENT] [bit] NULL,
[CLOSE_DATE] [datetime] NULL,
[SR_SUPPLIER_CONTACT_INFO_ID] [int] NULL,
[GENERAL_COMMENTS] [varchar] (2200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BENCHMARK_COMMENTS] [varchar] (2200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [bench_blank] DEFAULT ('')
) ON [DBData_Sourcing]
ALTER TABLE [dbo].[SR_RFP_CLOSURE] ADD
CONSTRAINT [FK_SR_RFP_CLOSURE_ENTITY] FOREIGN KEY ([CLOSE_ACTION_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[SR_RFP_CLOSURE] ADD
CONSTRAINT [FK_SR_RFP_CLOSURE_VENDOR] FOREIGN KEY ([VENDOR_ID]) REFERENCES [dbo].[VENDOR] ([VENDOR_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the RFP closure information', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_CLOSURE', NULL, NULL
GO

ALTER TABLE [dbo].[SR_RFP_CLOSURE] WITH NOCHECK ADD
CONSTRAINT [FK_SR_SUPPLIER_CONTACT_INFO_ID] FOREIGN KEY ([SR_SUPPLIER_CONTACT_INFO_ID]) REFERENCES [dbo].[SR_SUPPLIER_CONTACT_INFO] ([SR_SUPPLIER_CONTACT_INFO_ID]) NOT FOR REPLICATION
ALTER TABLE [dbo].[SR_RFP_CLOSURE] ADD 
CONSTRAINT [PK__SR_RFP_CLOSURE__40857097] PRIMARY KEY CLUSTERED  ([SR_RFP_CLOSURE_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
CREATE NONCLUSTERED INDEX [IX_SR_RFP_CLOSURE__IS_BID_GROUP] ON [dbo].[SR_RFP_CLOSURE] ([IS_BID_GROUP]) INCLUDE ([CLOSE_ACTION_TYPE_ID], [SR_ACCOUNT_GROUP_ID]) ON [DB_INDEXES01]







GO
