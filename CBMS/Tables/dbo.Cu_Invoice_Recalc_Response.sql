CREATE TABLE [dbo].[Cu_Invoice_Recalc_Response]
(
[Cu_Invoice_Recalc_Response_Id] [int] NOT NULL IDENTITY(1, 1),
[Recalc_Header_Id] [int] NOT NULL,
[Charge_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Determinant_Unit] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Determinant_Value] [decimal] (28, 10) NULL,
[Rate_Unit] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rate_Currency] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rate_Amount] [decimal] (28, 10) NULL,
[Net_Amount] [decimal] (28, 10) NULL,
[Calculator_Working] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response__Created_Ts] DEFAULT (getdate()),
[Bucket_Master_Id] [int] NULL,
[Rate_Uom_Type_Id] [int] NULL,
[Rate_Currency_Unit_Id] [int] NULL,
[Net_Amt_Currency_Unit_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Updated_User_Id] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response__Last_Change_Ts] DEFAULT (getdate()),
[Is_Hidden_On_RA] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response__Is_Hidden_On_RA] DEFAULT ((0)),
[Recalc_Response_Source_Cd] [int] NOT NULL,
[Is_Valid_Charge] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response_Is_Valid_Charge] DEFAULT ((1)),
[Charge_GUID] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Locked] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response__Is_Locked] DEFAULT ((0)),
[Is_Send_To_Sys] [bit] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Response__Is_Send_To_Sys] DEFAULT ((0)),
[Charge_Comment_Id] [int] NULL
) ON [DB_DATA01] TEXTIMAGE_ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Response] ADD CONSTRAINT [pk_Cu_Invoice_Recalc_Response] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Recalc_Response_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Cu_Invoice_Recalc_Response_Recalc_Header_Id] ON [dbo].[Cu_Invoice_Recalc_Response] ([Recalc_Header_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Response] ADD CONSTRAINT [fk_Recalc_Header__Cu_Invoice_Recalc_Response] FOREIGN KEY ([Recalc_Header_Id]) REFERENCES [dbo].[RECALC_HEADER] ([RECALC_HEADER_ID])
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Response] ENABLE CHANGE_TRACKING
GO
