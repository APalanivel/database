CREATE TABLE [dbo].[RM_Client_Workflow]
(
[RM_Client_Workflow_Id] [int] NOT NULL IDENTITY(1, 1),
[RM_Client_Hier_Onboard_Id] [int] NOT NULL,
[Hedge_Type_Id] [int] NOT NULL,
[Workflow_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_WorkflowCreated_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DFRM_Client_WorkflowLast_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Client_Workflow] ADD CONSTRAINT [pk_RM_Client_Workflow] PRIMARY KEY CLUSTERED  ([RM_Client_Workflow_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Client_Workflow] ADD CONSTRAINT [un_RM_Client_Workflow__RM_Client_Hier_Onboard_Id__edge_Type_Id] UNIQUE NONCLUSTERED  ([RM_Client_Hier_Onboard_Id], [Hedge_Type_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[RM_Client_Workflow] ADD CONSTRAINT [fk_RM_Client_Hier_Onboard__RM_Client_Workflow] FOREIGN KEY ([RM_Client_Hier_Onboard_Id]) REFERENCES [Trade].[RM_Client_Hier_Onboard] ([RM_Client_Hier_Onboard_Id])
GO
ALTER TABLE [dbo].[RM_Client_Workflow] ADD CONSTRAINT [fk_RM_Client_Workflow__Workflow_Id] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
ALTER TABLE [dbo].[RM_Client_Workflow] ADD CONSTRAINT [fk_Workflow__RM_Client_Workflow] FOREIGN KEY ([Workflow_Id]) REFERENCES [dbo].[Workflow] ([Workflow_Id])
GO
