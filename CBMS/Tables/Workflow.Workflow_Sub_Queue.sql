CREATE TABLE [Workflow].[Workflow_Sub_Queue]
(
[Workflow_Sub_Queue_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Id] [int] NOT NULL,
[Workflow_Sub_Queue_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Parent_Workflow_Sub_Queue_Id] [int] NULL,
[Display_Seq] [int] NOT NULL,
[Is_Default_View] [bit] NOT NULL,
[Permission_Info_Id] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Workflow___Creat__4595235D] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Workflow___Last___46894796] DEFAULT (getdate()),
[CSS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Sub_Queue] ADD CONSTRAINT [pk_Workflow_Sub_Queue] PRIMARY KEY CLUSTERED  ([Workflow_Sub_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Sub_Queue] ADD CONSTRAINT [uix_Workflow_Sub_Queue__Workflow_Queue_Id__Workflow_Sub_Queue_Name] UNIQUE NONCLUSTERED  ([Workflow_Queue_Id], [Workflow_Sub_Queue_Name]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Sub_Queue] ADD CONSTRAINT [fk_Workflow_Queue__Workflow_Sub_Queue] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
