CREATE TABLE [dbo].[EC_Meter_Attribute_Tracking]
(
[EC_Meter_Attribute_Tracking_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Meter_Id] [int] NOT NULL,
[EC_Meter_Attribute_Id] [int] NOT NULL,
[Start_Dt] [date] NOT NULL,
[End_Dt] [date] NULL,
[EC_Meter_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute_Tracking__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute_Tracking__Last_Change_Ts] DEFAULT (getdate()),
[Meter_Attribute_Type_Cd] [int] NULL CONSTRAINT [DF_Meter_Attribute_Type_Cd] DEFAULT ('-1')
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Tracking] ADD CONSTRAINT [pk_EC_Meter_Attribute_Tracking] PRIMARY KEY NONCLUSTERED  ([EC_Meter_Attribute_Tracking_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [IX_EC_Meter_Attribute_Tracking__EC_Meter_Attribute_Id__EC_Meter_Attribute_Value] ON [dbo].[EC_Meter_Attribute_Tracking] ([EC_Meter_Attribute_Id], [EC_Meter_Attribute_Value]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Tracking] ADD CONSTRAINT [unc_EC_Meter_Attribute_Tracking__Meter_Id__EC_Meter_Attribute_Id__Attribute_Value_Type_Cd__Start_Dt__End_Dt] UNIQUE NONCLUSTERED  ([Meter_Id], [EC_Meter_Attribute_Id], [Meter_Attribute_Type_Cd], [Start_Dt], [End_Dt]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Tracking] ADD CONSTRAINT [fk_EC_Meter_Attribute__EC_Meter_Attribute_Tracking] FOREIGN KEY ([EC_Meter_Attribute_Id]) REFERENCES [dbo].[EC_Meter_Attribute] ([EC_Meter_Attribute_Id])
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Tracking] ADD CONSTRAINT [fk_METER__EC_Meter_Attribute_Tracking] FOREIGN KEY ([Meter_Id]) REFERENCES [dbo].[METER] ([METER_ID])
GO
