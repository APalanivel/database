CREATE TABLE [dbo].[Invoice_Collection_Chase_Notification]
(
[Invoice_Collection_Chase_Notification_Log_Id] [int] NOT NULL IDENTITY(1, 1),
[Invoice_Collection_Chase_Log_Id] [int] NOT NULL,
[Notification_Msg_Queue_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Invoice_Collection_Chase_Notification__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Chase_Notification] ADD CONSTRAINT [pk_Invoice_Collection_Chase_Notification] PRIMARY KEY NONCLUSTERED  ([Invoice_Collection_Chase_Notification_Log_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Chase_Notification] ADD CONSTRAINT [un_Invoice_Collection_Chase_Notification] UNIQUE NONCLUSTERED  ([Invoice_Collection_Chase_Log_Id], [Notification_Msg_Queue_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Collection_Chase_Notification] ADD CONSTRAINT [fk_Invoice_Collection_Chase_Log__Invoice_Collection_Chase_Notification] FOREIGN KEY ([Invoice_Collection_Chase_Log_Id]) REFERENCES [dbo].[Invoice_Collection_Chase_Log] ([Invoice_Collection_Chase_Log_Id])
GO
