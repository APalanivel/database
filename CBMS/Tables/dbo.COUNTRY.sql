CREATE TABLE [dbo].[COUNTRY]
(
[COUNTRY_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[COUNTRY_NAME] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Row_Version] [timestamp] NOT NULL
) ON [DBData_CoreClient]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Country_upd_Client_Hier
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier table whenever any updates happen in Country table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		--SELECT * FROM COUNTRY WHERE COUNTRY_NAME = 'USA'
		SELECT CLIENT_HIER_ID,COUNTRY_NAME,COUNTRY_ID FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
				
		UPDATE COUNTRY
		SET COUNTRY_NAME = 'TEST'
		WHERE COUNTRY_ID = 4
		
		SELECT CLIENT_HIER_ID,COUNTRY_NAME,COUNTRY_ID FROM CORE.CLIENT_HIER WHERE CLIENT_ID = 10069 AND SITE_ID = 16995
		
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri
	
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/02/2010	Created
			08/31/2010  Added the Last_Change_TS column for update
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
******/

CREATE TRIGGER [dbo].[tr_Country_upd_Client_Hier] ON [dbo].[COUNTRY]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            Core.Client_Hier
      SET   
            COUNTRY_NAME = i.COUNTRY_NAME
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier ch
            INNER JOIN INSERTED i
                  ON i.COUNTRY_ID = ch.COUNTRY_ID
            INNER JOIN DELETED d
                  ON i.COUNTRY_ID = d.COUNTRY_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT i.COUNTRY_NAME
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT d.COUNTRY_NAME
                                                        FOR
                                                                  XML RAW ))            

END 
;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Country_upd_Client_Hier_Account
	
DESCRIPTION:   
	This will update the existing record of Core.Clinet_Hier_Account table whenever any updates happen in Country table.

 
 USAGE EXAMPLES:
------------------------------------------------------------
	
	BEGIN TRAN	
		SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID =3456 
				
		UPDATE COUNTRY
		SET COUNTRY_NAME = 'TEST'
		WHERE COUNTRY_ID = 4
		
		SELECT count(1) FROM CORE.CLIENT_HIER_ACCOUNT WHERE Meter_COUNTRY_NAME='usa'
		
		SELECT * FROM CORE.CLIENT_HIER_ACCOUNT WHERE ACCOUNT_ID =3456 
	ROLLBACK TRAN
	
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal
	BCH		Balaraju Chalumuri
	
 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/23/2010	Created
	BCH		2013-04-15  Replaced the BINARY_CHECKSUM funtion with HASHBYTES function.
******/

CREATE TRIGGER [dbo].[tr_Country_upd_Client_Hier_Account] ON [dbo].[COUNTRY]
      FOR UPDATE
AS
BEGIN
      SET NOCOUNT ON; 
	
      UPDATE
            cha
      SET   
            Meter_COUNTRY_NAME = i.COUNTRY_NAME
           ,Last_Change_TS = GETDATE()
      FROM
            Core.Client_Hier_Account cha
            INNER JOIN INSERTED i
                  ON i.COUNTRY_ID = cha.Meter_COUNTRY_ID
            INNER JOIN DELETED d
                  ON i.COUNTRY_ID = d.COUNTRY_ID
      WHERE
            HASHBYTES('SHA1', ( SELECT i.COUNTRY_NAME
                      FOR
                                XML RAW )) != HASHBYTES('SHA1', ( SELECT d.COUNTRY_NAME
                                                        FOR
                                                                  XML RAW ))            

END 
;
GO
ALTER TABLE [dbo].[COUNTRY] ADD CONSTRAINT [COUNTRY_PK] PRIMARY KEY CLUSTERED  ([COUNTRY_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
GO
CREATE UNIQUE NONCLUSTERED INDEX [COUNTRY_U_1] ON [dbo].[COUNTRY] ([COUNTRY_NAME]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]
GO
EXEC sp_addextendedproperty N'MS_Description', 'General list of Countries', 'SCHEMA', N'dbo', 'TABLE', N'COUNTRY', NULL, NULL
GO
