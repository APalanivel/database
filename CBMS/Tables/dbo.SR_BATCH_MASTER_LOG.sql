CREATE TABLE [dbo].[SR_BATCH_MASTER_LOG]
(
[SR_BATCH_MASTER_LOG_ID] [int] NOT NULL IDENTITY(1, 1),
[BATCH_START_DATE] [datetime] NOT NULL,
[BATCH_END_DATE] [datetime] NULL
) ON [DBData_Sourcing]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table is the master table for sourcing batch processes', 'SCHEMA', N'dbo', 'TABLE', N'SR_BATCH_MASTER_LOG', NULL, NULL
GO

ALTER TABLE [dbo].[SR_BATCH_MASTER_LOG] ADD 
CONSTRAINT [PK__SR_BATCH_MASTER___639A9FCB] PRIMARY KEY CLUSTERED  ([SR_BATCH_MASTER_LOG_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]

GO
