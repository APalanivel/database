CREATE TABLE [dbo].[BUDGET_DETAIL_COMMENTS]
(
[BUDGET_DETAIL_COMMENTS_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[BUDGET_ACCOUNT_ID] [int] NOT NULL,
[VARIABLE_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_VARIABLE_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_VA__61FE06CA] DEFAULT ((0)),
[TRANSPORTATION_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_TRANSPORTATION_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_TR__62F22B03] DEFAULT ((0)),
[DISTRIBUTION_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_DISTRIBUTION_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_DI__63E64F3C] DEFAULT ((0)),
[TRANSMISSION_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_TRANSMISSION_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_TR__64DA7375] DEFAULT ((0)),
[OTHER_BUNDLED_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_OTHER_BUNDLED_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_OT__65CE97AE] DEFAULT ((0)),
[OTHER_FIXED_COSTS_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_OTHER_FIXED_COSTS_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_OT__66C2BBE7] DEFAULT ((0)),
[SOURCING_TAX_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_SOURCING_TAX_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_SO__67B6E020] DEFAULT ((0)),
[RATES_TAX_COMMENTS] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_RATES_TAX_ON_DV] [bit] NOT NULL CONSTRAINT [DF__BUDGET_DE__IS_RA__68AB0459] DEFAULT ((0)),
[Row_Version] [timestamp] NULL
) ON [DBData_Budget]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******
NAME:
	dbo.tr_Budget_Detail_Comments_Audit

DESCRIPTION:

	Used to log DML actions happening on Budget_Details in to Budget_Details_Audit table.
	
	Audit Function values
			1 - Insert
			0 - Update
		   -1 - Delete

INPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

OUTPUT PARAMETERS:
	Name			DataType		Default	Description
------------------------------------------------------------

USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN

		UPDATE Budget_Detail_Comments
			SET variable_Comments = 'Feb-08 - Dec-08 budget calculated using ERCOT fixed forward pricing estimates for CY ''''08.'
		WHERE
			Budget_Detail_Comments_Id = 5548

		SELECT * FROM Budget_Detail_Comments_Audit WHERE budget_Detail_Comments_id = 5548

	ROLLBACK TRAN


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	HG			Harihara Suthan G

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	HG        	06/03/2010	Created

******/

CREATE TRIGGER [dbo].[tr_Budget_Detail_Comments_Audit]
	ON [dbo].[BUDGET_DETAIL_COMMENTS]
	FOR INSERT, UPDATE, DELETE
AS
BEGIN

	SET NOCOUNT ON;

	INSERT dbo.Budget_Detail_Comments_Audit
	(
		Audit_Function
		,Audit_Ts
		,Budget_Detail_Comments_Id
		,Budget_Account_Id
	)
	SELECT
		CASE
			WHEN d.Budget_Detail_Comments_Id IS NULL THEN  1		-- Inserted
			WHEN i.Budget_Detail_Comments_Id IS NULL THEN -1		-- Deleted
			ELSE 0													-- Updated
		END
		,GETDATE()
		,ISNULL(i.Budget_Detail_Comments_Id, d.Budget_Detail_Comments_Id)
		,ISNULL(i.Budget_Account_Id, d.Budget_Account_Id)
	FROM
		INSERTED i
		FULL OUTER JOIN DELETED d
			ON d.Budget_Detail_Comments_Id = i.Budget_Detail_Comments_Id

END
GO
ALTER TABLE [dbo].[BUDGET_DETAIL_COMMENTS] ADD CONSTRAINT [pk_BUDGET_DETAIL_COMMENTS] PRIMARY KEY NONCLUSTERED  ([BUDGET_DETAIL_COMMENTS_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
CREATE UNIQUE CLUSTERED INDEX [unc_BUDGET_DETAIL_COMMENTS_BUDGET_ACCOUNT_ID] ON [dbo].[BUDGET_DETAIL_COMMENTS] ([BUDGET_ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_Budget]
GO
ALTER TABLE [dbo].[BUDGET_DETAIL_COMMENTS] ADD CONSTRAINT [FK_BUDGET_ACCOUNT__BUDGET_DETAIL_COMMENTS] FOREIGN KEY ([BUDGET_ACCOUNT_ID]) REFERENCES [dbo].[BUDGET_ACCOUNT] ([BUDGET_ACCOUNT_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Budget comments at an account level, for each component (transportation, variable,distribution etc)', 'SCHEMA', N'dbo', 'TABLE', N'BUDGET_DETAIL_COMMENTS', NULL, NULL
GO
