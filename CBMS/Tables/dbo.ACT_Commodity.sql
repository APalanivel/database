CREATE TABLE [dbo].[ACT_Commodity]
(
[ACT_Commodity_Id] [int] NOT NULL IDENTITY(1, 1),
[ACT_Commodity_XName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_ACT_Commodity__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[ACT_Commodity] ADD CONSTRAINT [pk_ACT_Commodity] PRIMARY KEY NONCLUSTERED  ([ACT_Commodity_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[ACT_Commodity] ADD CONSTRAINT [un_ACT_Commodity__ACT_Commodity_XName] UNIQUE NONCLUSTERED  ([ACT_Commodity_XName]) ON [DB_DATA01]
GO
