CREATE TABLE [dbo].[Client_Invoice_DNT_Config]
(
[Client_Invoice_DNT_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[CLIENT_ID] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Effective_Start_Dt] [date] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config__Effective_Start_Dt] DEFAULT ('01-01-1900'),
[Effective_End_Dt] [date] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config__Effective_End_Dt] DEFAULT ('31-12-9999'),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Client_Invoice_DNT_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_DNT_Config] ADD CONSTRAINT [pk_Client_Invoice_DNT_Config] PRIMARY KEY CLUSTERED  ([Client_Invoice_DNT_Config_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Client_Invoice_DNT_Config__CLIENT_ID__Effective_Start_Dt__Effective_End_Dt] ON [dbo].[Client_Invoice_DNT_Config] ([CLIENT_ID], [Effective_Start_Dt], [Effective_End_Dt]) ON [DB_DATA01]
GO
