CREATE TABLE [dbo].[Sitegroup_Site]
(
[Sitegroup_id] [int] NOT NULL,
[Site_id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Sitegroup_Site__Last_Change_Ts] DEFAULT (getdate())
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Sites associated to a sitegroup', 'SCHEMA', N'dbo', 'TABLE', N'Sitegroup_Site', NULL, NULL
GO

ALTER TABLE [dbo].[Sitegroup_Site] ADD 
CONSTRAINT [pk_Sitegroup_Site] PRIMARY KEY CLUSTERED  ([Site_id], [Sitegroup_id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [ix_Sitegroup_Site_SiteGroup_Id] ON [dbo].[Sitegroup_Site] ([Sitegroup_id]) ON [DB_INDEXES01]



ALTER TABLE [dbo].[Sitegroup_Site] WITH NOCHECK ADD
CONSTRAINT [fk_SiteGroup_Site_Sitegroup] FOREIGN KEY ([Sitegroup_id]) REFERENCES [dbo].[Sitegroup] ([Sitegroup_Id]) NOT FOR REPLICATION
ALTER TABLE [dbo].[Sitegroup_Site] ADD
CONSTRAINT [fk_Sitegroup_Site_Site] FOREIGN KEY ([Site_id]) REFERENCES [dbo].[SITE] ([SITE_ID])


GO
