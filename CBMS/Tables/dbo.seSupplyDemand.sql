CREATE TABLE [dbo].[seSupplyDemand]
(
[SupplyDemandId] [int] NOT NULL IDENTITY(1, 1),
[DataTypeId] [int] NOT NULL,
[ReportYear] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportMonth] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnAccountedFor] [int] NOT NULL,
[Consumption] [int] NOT NULL,
[NetImports] [int] NOT NULL,
[StorageWithdrawal] [int] NOT NULL,
[DryProduction] [int] NOT NULL,
[SupplementalProduction] [int] NOT NULL,
[PlantFuel] [int] NOT NULL,
[Vehicles] [int] NOT NULL,
[Industrial] [int] NOT NULL,
[Commercial] [int] NOT NULL,
[Residential] [int] NOT NULL,
[Transportation] [int] NOT NULL,
[PowerGeneration] [int] NOT NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated', 'SCHEMA', N'dbo', 'TABLE', N'seSupplyDemand', NULL, NULL
GO

ALTER TABLE [dbo].[seSupplyDemand] ADD 
CONSTRAINT [PK_seSupplyDemand] PRIMARY KEY CLUSTERED  ([SupplyDemandId]) WITH (FILLFACTOR=80) ON [DBData_Risk_MGMT]
GO
