CREATE TABLE [dbo].[Time_Of_Use_Schedule_Term_Holiday]
(
[Time_Of_Use_Schedule_Term_Holiday_Id] [int] NOT NULL IDENTITY(1, 1),
[Holiday_Master_Id] [int] NOT NULL,
[Time_Of_Use_Schedule_Term_Id] [int] NOT NULL,
[Is_Rule_One_Active] [bit] NOT NULL CONSTRAINT [df_Time_Of_Use_Schedule_Term_Holiday__Is_Rule_One_Active] DEFAULT ((0)),
[Is_Rule_Two_Active] [bit] NOT NULL CONSTRAINT [df_Time_Of_Use_Schedule_Term_Holiday__Is_Rule_Two_Active] DEFAULT ((0)),
[Is_Manual_Override] [bit] NOT NULL CONSTRAINT [df_Time_Of_Use_Schedule_Term_Holiday__Is_Manual_Override] DEFAULT ((0)),
[Holiday_Dt] [date] NOT NULL
) ON [DBData_Contract_RateLibrary]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of terms to Holiday', 'SCHEMA', N'dbo', 'TABLE', N'Time_Of_Use_Schedule_Term_Holiday', NULL, NULL
GO

CREATE UNIQUE CLUSTERED INDEX [ucix_Time_Of_Use_Schedule_Term_Holiday__Time_Of_Use_Schedule_Term_Id__Holiday_Master_Id] ON [dbo].[Time_Of_Use_Schedule_Term_Holiday] ([Time_Of_Use_Schedule_Term_Id], [Holiday_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Holiday] ADD CONSTRAINT [pk_Time_Of_Use_Schedule_Term_Holiday] PRIMARY KEY NONCLUSTERED  ([Time_Of_Use_Schedule_Term_Holiday_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

CREATE NONCLUSTERED INDEX [ix_Time_Of_Use_Schedule_Term_Holiday__Holiday_Master_Id] ON [dbo].[Time_Of_Use_Schedule_Term_Holiday] ([Holiday_Master_Id]) WITH (FILLFACTOR=90) ON [DBData_Contract_RateLibrary]

ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Holiday] ADD
CONSTRAINT [fk_Holiday_Master__Time_Of_Use_Schedule_Term_Holiday] FOREIGN KEY ([Holiday_Master_Id]) REFERENCES [dbo].[Holiday_Master] ([Holiday_Master_Id])
ALTER TABLE [dbo].[Time_Of_Use_Schedule_Term_Holiday] ADD
CONSTRAINT [fk_Time_Of_Use_Schedule_Term__Time_Of_Use_Schedule_Term_Holiday] FOREIGN KEY ([Time_Of_Use_Schedule_Term_Id]) REFERENCES [dbo].[Time_Of_Use_Schedule_Term] ([Time_Of_Use_Schedule_Term_Id])
GO
