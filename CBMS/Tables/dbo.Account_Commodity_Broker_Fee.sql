CREATE TABLE [dbo].[Account_Commodity_Broker_Fee]
(
[Account_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL,
[Broker_Fee] [decimal] (28, 10) NOT NULL,
[Currency_Unit_Id] [int] NOT NULL,
[UOM_Entity_Id] [int] NOT NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Comission fees per account/commodity.  Used primarily by Sourcing.  Requirements by law to keep 7 years history.', 'SCHEMA', N'dbo', 'TABLE', N'Account_Commodity_Broker_Fee', NULL, NULL
GO

ALTER TABLE [dbo].[Account_Commodity_Broker_Fee] ADD 
CONSTRAINT [pk_Account_Commodity_Broker_Fee] PRIMARY KEY CLUSTERED  ([Account_Id], [Commodity_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
ALTER TABLE [dbo].[Account_Commodity_Broker_Fee] ADD
CONSTRAINT [fk_Account_Commodity_Broker_Fee__Account] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
ALTER TABLE [dbo].[Account_Commodity_Broker_Fee] ADD
CONSTRAINT [fk_Account_Commodity_Broker_Fee__Commodity] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
ALTER TABLE [dbo].[Account_Commodity_Broker_Fee] ADD
CONSTRAINT [fk_Account_Commodity_Broker_Fee__Currency_Unit] FOREIGN KEY ([Currency_Unit_Id]) REFERENCES [dbo].[CURRENCY_UNIT] ([CURRENCY_UNIT_ID])
GO
