CREATE TABLE [Trade].[Trade_Price]
(
[Trade_Price_Id] [int] NOT NULL IDENTITY(1, 1),
[Deal_Tickets_Price_XId] [int] NOT NULL,
[Trade_Price] [decimal] (28, 6) NULL,
[Mid_Price] [decimal] (28, 6) NULL,
[Market_Price] [decimal] (28, 6) NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF__Trade_Pri__Creat__470664F2] DEFAULT (getdate()),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Trade_Pri__Last___47FA892B] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Trade_Price] ADD CONSTRAINT [pk_Trade_Price] PRIMARY KEY CLUSTERED  ([Trade_Price_Id]) ON [DB_DATA01]
GO
