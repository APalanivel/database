CREATE TABLE [Workflow].[Workflow_Queue_Navigation_Link]
(
[Workflow_Queue_External_Link_Id] [int] NOT NULL IDENTITY(1, 1),
[Workflow_Queue_Id] [int] NOT NULL,
[Navigation_Link] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Navigation_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Workflow_Queue_Navigation_Link__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_Workflow_Queue_Navigation_Link__Last_Change_Ts] DEFAULT (getdate()),
[Permissinon_info_id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Navigation_Link] ADD CONSTRAINT [pk_Workflow_Queue_Navigation_Link] PRIMARY KEY CLUSTERED  ([Workflow_Queue_External_Link_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Workflow].[Workflow_Queue_Navigation_Link] ADD CONSTRAINT [FK__Workflow___Permi__0DA5CFF5] FOREIGN KEY ([Permissinon_info_id]) REFERENCES [dbo].[PERMISSION_INFO] ([PERMISSION_INFO_ID])
GO
ALTER TABLE [Workflow].[Workflow_Queue_Navigation_Link] ADD CONSTRAINT [fk_Workflow_Queue__Workflow_Queue_Navigation_Link] FOREIGN KEY ([Workflow_Queue_Id]) REFERENCES [Workflow].[Workflow_Queue] ([Workflow_Queue_Id])
GO
