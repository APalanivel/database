CREATE TABLE [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT]
(
[CU_INVOICE_DETERMINANT_ACCOUNT_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CU_INVOICE_DETERMINANT_ID] [int] NOT NULL,
[ACCOUNT_ID] [int] NULL,
[Determinant_Expression] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Determinant_Value] [decimal] (16, 4) NULL
) ON [DBData_Invoice]
CREATE NONCLUSTERED INDEX [IX_CU_INVOICE_DETERMINANT_ACCOUNT__ACCOUNT_ID__CU_INVOICE_DETERMINANT_ID] ON [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ([ACCOUNT_ID], [CU_INVOICE_DETERMINANT_ID]) ON [DB_INDEXES01]

GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of determinants per invoice to account', 'SCHEMA', N'dbo', 'TABLE', N'CU_INVOICE_DETERMINANT_ACCOUNT', NULL, NULL
GO

ALTER TABLE [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ADD 
CONSTRAINT [PK_CU_INVOICE_DETERMINANT_ACCOUNT_ID] PRIMARY KEY CLUSTERED  ([CU_INVOICE_DETERMINANT_ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
GO
ALTER TABLE [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ENABLE CHANGE_TRACKING
GO

CREATE NONCLUSTERED INDEX [ix_cu_invoice_determinant_account__cu_invoice_determinant_id] ON [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ([CU_INVOICE_DETERMINANT_ID]) INCLUDE ([ACCOUNT_ID], [CU_INVOICE_DETERMINANT_ACCOUNT_ID], [Determinant_Expression], [Determinant_Value]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_CU_INVOICE_DETERMINANT_ACCOUNT__Account_ID] ON [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ([ACCOUNT_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ADD
CONSTRAINT [FK_CU_INVOICE_DETERMINENT_ACCOUNT__ACCOUNT__ACCOUNT_ID] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
ALTER TABLE [dbo].[CU_INVOICE_DETERMINANT_ACCOUNT] ADD
CONSTRAINT [FK_CU_INVOICE_DETERMINENT_ACCOUNT__CU_INVOICE_DETERMINANT__CU_INVOICE_DETERMINENT_ID] FOREIGN KEY ([CU_INVOICE_DETERMINANT_ID]) REFERENCES [dbo].[CU_INVOICE_DETERMINANT] ([CU_INVOICE_DETERMINANT_ID])











GO
