CREATE TABLE [dbo].[Country_Commodity_Uom]
(
[Country_Commodity_Uom_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Commodity_Id] [int] NOT NULL,
[COUNTRY_ID] [int] NOT NULL,
[Default_Uom_Type_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Country_Commodity_Uom__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Country_Commodity_Uom__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
ALTER TABLE [dbo].[Country_Commodity_Uom] ADD 
CONSTRAINT [pk_Country_Commodity_Uom] PRIMARY KEY CLUSTERED  ([Country_Commodity_Uom_Id]) ON [DB_DATA01]
GO


ALTER TABLE [dbo].[Country_Commodity_Uom] ADD CONSTRAINT [un_Country_Commodity_Uom__Commodity_Id__COUNTRY_ID] UNIQUE NONCLUSTERED  ([Commodity_Id], [COUNTRY_ID]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Country_Commodity_Uom] ADD CONSTRAINT [fk_Commodity__Country_Commodity_Uom] FOREIGN KEY ([Commodity_Id]) REFERENCES [dbo].[Commodity] ([Commodity_Id])
GO
ALTER TABLE [dbo].[Country_Commodity_Uom] ADD CONSTRAINT [fk_Country__Country_Commodity_Uom] FOREIGN KEY ([COUNTRY_ID]) REFERENCES [dbo].[COUNTRY] ([COUNTRY_ID])
GO
