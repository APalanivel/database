CREATE TABLE [dbo].[Account_Invoice_Collection_UBM_Source_Dtl]
(
[Account_Invoice_Collection_UBM_Source_Dtl_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Invoice_Collection_Source_Id] [int] NOT NULL,
[UBM_Id] [int] NULL,
[UBM_Type_Cd] [int] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_UBM_Source_Dtl__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Invoice_Collection_UBM_Source_Dtl__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_UBM_Source_Dtl] ADD CONSTRAINT [pk_Account_Invoice_Collection_UBM_Source_Dtl] PRIMARY KEY CLUSTERED  ([Account_Invoice_Collection_UBM_Source_Dtl_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Account_IC_Ubm_Source_Dtl__Account_IC_Source_Id] ON [dbo].[Account_Invoice_Collection_UBM_Source_Dtl] ([Account_Invoice_Collection_Source_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [uix_Account_Invoice_Collection_UBM_Source_Dtl__Account_Invoice_Collection_Source_Id] ON [dbo].[Account_Invoice_Collection_UBM_Source_Dtl] ([Account_Invoice_Collection_Source_Id]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Account_Invoice_Collection_UBM_Source_Dtl] ADD CONSTRAINT [fk_Invoice_Collection_Account_Config__Account_Invoice_Collection_UBM_Source_Dtl] FOREIGN KEY ([Account_Invoice_Collection_Source_Id]) REFERENCES [dbo].[Account_Invoice_Collection_Source] ([Account_Invoice_Collection_Source_Id])
GO
EXEC sp_addextendedproperty N'MS_Description', N'In invoice collection process the type of Invoice Source UBM is captured in this table', 'SCHEMA', N'dbo', 'TABLE', N'Account_Invoice_Collection_UBM_Source_Dtl', NULL, NULL
GO
