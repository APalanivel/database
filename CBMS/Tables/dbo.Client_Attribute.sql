CREATE TABLE [dbo].[Client_Attribute]
(
[Client_Id] [int] NULL,
[Mark_Invoice_Exception_As_Priority] [bit] NOT NULL,
[Created_Uer_Id] [int] NULL,
[Created_Ts] [datetime] NULL CONSTRAINT [DF__Client_Attribute_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NULL,
[Updated_Ts] [datetime] NULL CONSTRAINT [DF__Client_Attribute_Updated_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Attribute] ADD CONSTRAINT [FK__Client_Attribute_Client_ID] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
