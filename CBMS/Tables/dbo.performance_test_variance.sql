CREATE TABLE [dbo].[performance_test_variance]
(
[Account_id] [int] NULL,
[Commodity_id] [int] NULL,
[service_month] [date] NULL,
[Test_Description] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Start_time] [datetime2] NULL,
[End_time] [datetime2] NULL
) ON [DB_DATA01]
GO
