CREATE TABLE [Trade].[Rm_Budget_Filter_Participant]
(
[Rm_Budget_Filter_Participant_Id] [int] NOT NULL IDENTITY(1, 1),
[Rm_Budget_Id] [int] NOT NULL,
[Filter_Participant_Id] [int] NOT NULL,
[RM_Budget_Filter_Participant_Type_Cd] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Rm_Budget_Filter_Participant__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Filter_Participant] ADD CONSTRAINT [PK_Rm_Budget_Filter_Participant__Rm_Budget_Filter_Participant_Id] PRIMARY KEY CLUSTERED  ([Rm_Budget_Filter_Participant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Rm_Budget_Filter_Participant] ADD CONSTRAINT [FK_Rm_Budget_Filter_Participant__Rm_Budget_Id] FOREIGN KEY ([Rm_Budget_Id]) REFERENCES [Trade].[Rm_Budget] ([Rm_Budget_Id])
GO
