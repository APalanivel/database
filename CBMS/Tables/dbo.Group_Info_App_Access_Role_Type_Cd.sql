CREATE TABLE [dbo].[Group_Info_App_Access_Role_Type_Cd]
(
[Group_Info_Id] [int] NOT NULL,
[App_Access_Role_Type_Cd] [int] NOT NULL
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Defines to which access role group can be included by default when this group is assigned to the client', 'SCHEMA', N'dbo', 'TABLE', N'Group_Info_App_Access_Role_Type_Cd', NULL, NULL
GO

ALTER TABLE [dbo].[Group_Info_App_Access_Role_Type_Cd] ADD CONSTRAINT [pk_Group_Info_App_Access_Role_Type_Cd_Map] PRIMARY KEY CLUSTERED  ([Group_Info_Id], [App_Access_Role_Type_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Group_Info_App_Access_Role_Type_Cd] ADD CONSTRAINT [fk_Group_Info_App_Access_Role_Type_Cd_Map__Group_Info] FOREIGN KEY ([Group_Info_Id]) REFERENCES [dbo].[GROUP_INFO] ([GROUP_INFO_ID])
GO
