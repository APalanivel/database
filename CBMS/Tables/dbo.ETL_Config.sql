CREATE TABLE [dbo].[ETL_Config]
(
[ConfigurationFilter] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfiguredValue] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PackagePath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConfiguredValueType] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Settings used by ETL processes', 'SCHEMA', N'dbo', 'TABLE', N'ETL_Config', NULL, NULL
GO
