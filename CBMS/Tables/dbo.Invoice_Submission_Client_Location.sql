CREATE TABLE [dbo].[Invoice_Submission_Client_Location]
(
[Invoice_Submission_Client_Location_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[Location_Path] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Location_Type_Cd] [int] NOT NULL,
[Location_Config] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[User_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Pass_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_Invoice_Submission_Client_Location_Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Updates_Ts] [datetime] NOT NULL CONSTRAINT [DF_Invoice_Submission_Client_Location_Updates_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_Submission_Client_Location] ADD CONSTRAINT [PK_Invoice_Submission_Client_Location] PRIMARY KEY CLUSTERED  ([Invoice_Submission_Client_Location_Id]) ON [DB_DATA01]
GO
CREATE UNIQUE NONCLUSTERED INDEX [unc_Invoice_Submission_Client_Location__Client_Id] ON [dbo].[Invoice_Submission_Client_Location] ([Client_Id]) ON [DB_DATA01]
GO
