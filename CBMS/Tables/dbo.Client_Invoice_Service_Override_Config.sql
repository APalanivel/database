CREATE TABLE [dbo].[Client_Invoice_Service_Override_Config]
(
[Client_Invoice_Service_Override_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NOT NULL,
[UBM_Id] [int] NOT NULL,
[Ubm_Service_Code] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Override_Commodity_Id] [int] NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df__Client_Invoice_Service_Override_Config__Is_Active] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL,
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_Service_Override_Config] ADD CONSTRAINT [PK_Client_Invoice_Service_Override_Config] PRIMARY KEY CLUSTERED  ([Client_Invoice_Service_Override_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_Service_Override_Config] ADD CONSTRAINT [Un_Client_Invoice_Service_Override_Config] UNIQUE NONCLUSTERED  ([Client_Id], [UBM_Id], [Ubm_Service_Code]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Client_Invoice_Service_Override_Config] ADD CONSTRAINT [FK_Client_Invoice_Service_Override_Config_Client_id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
GO
ALTER TABLE [dbo].[Client_Invoice_Service_Override_Config] ADD CONSTRAINT [FK_Client_Invoice_Service_Override_Config_Created_User_Id] FOREIGN KEY ([Created_User_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO
ALTER TABLE [dbo].[Client_Invoice_Service_Override_Config] ADD CONSTRAINT [FK_Client_Invoice_Service_Override_Config_Updated_User_Id] FOREIGN KEY ([Updated_User_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO
