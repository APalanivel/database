CREATE TABLE [dbo].[Sr_Service_Condition_Template_Product_Map]
(
[Sr_Service_Condition_Template_Product_Map_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Sr_Service_Condition_Template_Id] [int] NOT NULL,
[SR_Pricing_Product_Country_Map_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template_Product_Map__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template_Product_Map__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Product_Map] ADD CONSTRAINT [pk_Sr_Service_Condition_Template_Product_Map] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Template_Product_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Product_Map] ADD CONSTRAINT [un_Sr_Service_Condition_Template_Product_Map__Sr_Service_Condition_Template_Id__SR_Pricing_Product_Country_Map_Id] UNIQUE NONCLUSTERED  ([Sr_Service_Condition_Template_Id], [SR_Pricing_Product_Country_Map_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Product_Map] ADD CONSTRAINT [fk_SR_Pricing_Product_Country_Map__Sr_Service_Condition_Template_Product_Map] FOREIGN KEY ([SR_Pricing_Product_Country_Map_Id]) REFERENCES [dbo].[SR_Pricing_Product_Country_Map] ([SR_Pricing_Product_Country_Map_Id])
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template_Product_Map] ADD CONSTRAINT [fk_Sr_Service_Condition_Template__Sr_Service_Condition_Template_Product_Map] FOREIGN KEY ([Sr_Service_Condition_Template_Id]) REFERENCES [dbo].[Sr_Service_Condition_Template] ([Sr_Service_Condition_Template_Id])
GO
