CREATE TABLE [dbo].[User_Info_Client_App_Access_Role_Map]
(
[User_Info_Id] [int] NOT NULL,
[Client_App_Access_Role_Id] [int] NOT NULL,
[Assigned_User_Id] [int] NOT NULL,
[Assigned_Ts] [datetime] NOT NULL CONSTRAINT [DF__User_Info__Assig__0B2B687A] DEFAULT (getdate())
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Mapping of roles (collection of permission groups/functions) to the users', 'SCHEMA', N'dbo', 'TABLE', N'User_Info_Client_App_Access_Role_Map', NULL, NULL
GO

ALTER TABLE [dbo].[User_Info_Client_App_Access_Role_Map] ADD
CONSTRAINT [fk_Client_App_Access_Role__User_Info_Client_App_Access_Role_Map] FOREIGN KEY ([Client_App_Access_Role_Id]) REFERENCES [dbo].[Client_App_Access_Role] ([Client_App_Access_Role_Id])
ALTER TABLE [dbo].[User_Info_Client_App_Access_Role_Map] ADD
CONSTRAINT [fk_User_Info__User_Info_Client_App_Access_Role_Map] FOREIGN KEY ([User_Info_Id]) REFERENCES [dbo].[USER_INFO] ([USER_INFO_ID])
GO


SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO


/******
NAME:	tr_User_Info_Client_App_Access_Role_Map_Transfer

DESCRIPTION: Trigger to send changes for DV2 server through service_broker.


AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	DSC			Kaushik
	TP			Anoop
	
MODIFICATIONS

	Initials	Date			Modification
------------------------------------------------------------
	DSC			02/12/2014		Created
	DSC			03/05/2014		Added Update Event
	TP			01 Mar 2016		Modified to add Hub replication message	
	
******/
CREATE TRIGGER [dbo].[tr_User_Info_Client_App_Access_Role_Map_Transfer] ON [dbo].[User_Info_Client_App_Access_Role_Map]
      FOR INSERT, UPDATE, DELETE
AS
BEGIN 
	
      DECLARE
            @Conversation_Handle UNIQUEIDENTIFIER
           ,@Message XML
           ,@From_Service NVARCHAR(255)
           ,@Target_Service NVARCHAR(255)
           ,@Target_Contract NVARCHAR(255) 
	

      SET @Message = ( SELECT
                        i.User_Info_Id AS User_Info_Id
                       ,i.Client_App_Access_Role_Id AS Client_App_Access_Role_Id
                       ,i.Assigned_User_Id
                       ,i.Assigned_Ts
                       ,d.User_Info_Id AS Old_User_Info_Id
                       ,d.Client_App_Access_Role_Id AS Old_Client_App_Access_Role_Id
                       ,d.Assigned_User_Id AS Old_Assigned_User_Id
                       ,d.Assigned_Ts AS Old_Assigned_Ts
                       ,case WHEN i.Client_App_Access_Role_Id IS NOT NULL				-- One Column Check is sufficent 
                                  AND d.Client_App_Access_Role_Id IS NULL THEN 'I'
                             WHEN i.Client_App_Access_Role_Id IS NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'D'
                             WHEN i.Client_App_Access_Role_Id IS NOT NULL
                                  AND d.Client_App_Access_Role_Id IS NOT NULL THEN 'U'
                        END AS Op_Code
                       FROM
                        INSERTED i
                        FULL JOIN DELETED d
                              ON i.Client_App_Access_Role_Id = d.Client_App_Access_Role_Id
                                 AND i.User_Info_Id = d.User_Info_Id
            FOR
                       XML PATH('User_Info_Client_App_Access_Role_Map_Change')
                          ,ELEMENTS
                          ,ROOT('User_Info_Client_App_Access_Role_Map_Changes') )

      IF @Message IS NOT NULL 
            BEGIN
	
				
                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DV2/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/User_Info_Client_App_Access_Role_Map_Transfer'	

                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Info_Client_App_Access_Role_Map_Transfer] (@Message);

                  SET @From_Service = N'//Change_Control/Service/CBMS/User_Info_Transfer'
                  SET @Target_Service = N'//Change_Control/Service/DVDEHub/User_Info_Transfer'
                  SET @Target_Contract = N'//Change_Control/Contract/User_Info_Client_App_Access_Role_Map_Transfer'	

                  EXEC Service_Broker_Conversation_Handle_Sel 
                        @From_Service
                       ,@Target_Service
                       ,@Target_Contract
                       ,@Conversation_Handle OUTPUT;
                  SEND ON CONVERSATION @Conversation_Handle    
											MESSAGE TYPE [//Change_Control/Message/User_Info_Client_App_Access_Role_Map_Transfer] (@Message);



            END

END


GO


ALTER TABLE [dbo].[User_Info_Client_App_Access_Role_Map] ADD CONSTRAINT [pk_User_Info_Client_App_Access_Role_Map] PRIMARY KEY CLUSTERED  ([User_Info_Id], [Client_App_Access_Role_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_User_Info_Client_App_Access_Role_Map__Client_App_Access_Role_Id] ON [dbo].[User_Info_Client_App_Access_Role_Map] ([Client_App_Access_Role_Id]) ON [DB_INDEXES01]
GO
