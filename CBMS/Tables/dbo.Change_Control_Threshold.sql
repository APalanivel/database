CREATE TABLE [dbo].[Change_Control_Threshold]
(
[Table_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Change_Count_Threshold] [int] NOT NULL CONSTRAINT [df_Change_Control_Threshold__Change_Count_Threshold] DEFAULT ((0)),
[Current_Change_Count] [int] NOT NULL CONSTRAINT [df_Change_Control_Threshold__Current_Change_Count] DEFAULT ((0)),
[Time_Threshold_Mins] [int] NOT NULL CONSTRAINT [df_Change_Control_Threshold__Time_Threshold] DEFAULT ((5)),
[Last_Data_Push_Ts] [datetime] NOT NULL CONSTRAINT [df_Change_Control_Threshold__Last_Data_Push_Ts] DEFAULT (getdate()),
[Table_Change_Procedure] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Last_Version_Id] [bigint] NOT NULL CONSTRAINT [df_Change_Control_Threshold__Last_Version_Id] DEFAULT ((0))
) ON [DB_DATA01]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Used by internally developed Service Broker operations to trigger processing.', 'SCHEMA', N'dbo', 'TABLE', N'Change_Control_Threshold', NULL, NULL
GO

ALTER TABLE [dbo].[Change_Control_Threshold] ADD CONSTRAINT [pk_Broker_Change_Control] PRIMARY KEY CLUSTERED  ([Table_Name]) ON [DB_DATA01]
GO
