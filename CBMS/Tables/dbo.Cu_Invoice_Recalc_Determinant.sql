CREATE TABLE [dbo].[Cu_Invoice_Recalc_Determinant]
(
[Cu_Invoice_Recalc_Determinant_Id] [int] NOT NULL IDENTITY(1, 1),
[Recalc_Header_Id] [int] NOT NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NULL,
[Sub_Bucket_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Determinant_Value] [decimal] (28, 10) NULL,
[Uom_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Uom_Type_Id] [int] NULL,
[Expected_Slots] [int] NULL,
[Actual_Slots] [int] NULL,
[Missing_Slots] [int] NULL,
[Edited_Slots] [int] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Recalc_Determinant__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Determinant] ADD CONSTRAINT [pk_Cu_Invoice_Recalc_Determinant] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Recalc_Determinant_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Recalc_Determinant] ADD CONSTRAINT [fk_Recalc_Header__Cu_Invoice_Recalc_Determinant] FOREIGN KEY ([Recalc_Header_Id]) REFERENCES [dbo].[RECALC_HEADER] ([RECALC_HEADER_ID])
GO
