CREATE TABLE [Trade].[Deal_Ticket_Client_Contact_Client_Hier]
(
[Deal_Ticket_Client_Contact_Id] [int] NOT NULL,
[Client_Hier_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Deal_Ticket_Client_Contact_Client_Hier__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact_Client_Hier] ADD CONSTRAINT [pk_Deal_Ticket_Client_Contact_Client_Hier] PRIMARY KEY CLUSTERED  ([Deal_Ticket_Client_Contact_Id], [Client_Hier_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Deal_Ticket_Client_Contact_Client_Hier__Client_Hier_Id] ON [Trade].[Deal_Ticket_Client_Contact_Client_Hier] ([Client_Hier_Id]) ON [DB_DATA01]
GO
ALTER TABLE [Trade].[Deal_Ticket_Client_Contact_Client_Hier] ADD CONSTRAINT [fk_Deal_Ticket_Client_Contact__Deal_Ticket_Client_Contact_Client_Hier] FOREIGN KEY ([Deal_Ticket_Client_Contact_Id]) REFERENCES [Trade].[Deal_Ticket_Client_Contact] ([Deal_Ticket_Client_Contact_Id])
GO
