CREATE TABLE [dbo].[Cu_Invoice_Standing_Data_Exception]
(
[Cu_Invoice_Standing_Data_Exception_Id] [int] NOT NULL IDENTITY(1, 1),
[Cu_Invoice_Id] [int] NOT NULL,
[Exception_Type_Cd] [int] NOT NULL,
[Queue_Id] [int] NOT NULL,
[Exception_Status_Cd] [int] NOT NULL,
[Closed_By_User_Id] [int] NULL,
[Closed_Ts] [datetime] NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Standing_Data_Exception__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Cu_Invoice_Standing_Data_Exception__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Standing_Data_Exception] ADD CONSTRAINT [pk_Cu_Invoice_Standing_Data_Exception] PRIMARY KEY CLUSTERED  ([Cu_Invoice_Standing_Data_Exception_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix__Cu_Invoice_Standing_Data_Exception__Cu_Invoice_Id] ON [dbo].[Cu_Invoice_Standing_Data_Exception] ([Cu_Invoice_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix__Cu_Invoice_Standing_Data_Exception__Queue_Id_Exception_Type_Cd] ON [dbo].[Cu_Invoice_Standing_Data_Exception] ([Queue_Id], [Exception_Type_Cd]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cu_Invoice_Standing_Data_Exception] ADD CONSTRAINT [fk_Cu_Invoice__Cu_Invoice_Standing_Data_Exception] FOREIGN KEY ([Cu_Invoice_Id]) REFERENCES [dbo].[CU_INVOICE] ([CU_INVOICE_ID])
GO
