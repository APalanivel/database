CREATE TABLE [dbo].[Invoice_collection_New_Batch_Clients]
(
[Invoice_collection_New_Batch_Clients_Id] [int] NOT NULL IDENTITY(1, 1),
[Client_Id] [int] NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Invoice_collection_New_Batch_Clients] ADD CONSTRAINT [pk_Invoice_collection_New_Batch_Clients] PRIMARY KEY CLUSTERED  ([Invoice_collection_New_Batch_Clients_Id]) ON [DB_DATA01]
GO
