CREATE TABLE [dbo].[Account_Exception]
(
[Account_Exception_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Meter_Id] [int] NOT NULL,
[Exception_Type_Cd] [int] NOT NULL,
[Queue_Id] [int] NOT NULL,
[Exception_Status_Cd] [int] NOT NULL,
[Exception_By_User_Id] [int] NOT NULL,
[Exception_Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Exception__Exception_Created_Ts] DEFAULT (getdate()),
[Closed_By_User_Id] [int] NULL,
[Closed_Ts] [datetime] NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Account_Exception__Last_Change_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Commodity_Id] [int] NOT NULL CONSTRAINT [df_Account_Exception__Commodity_Id] DEFAULT ((-1)),
[Service_Desk_Ticket_XId] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Exception_Begin_Dt] [date] NULL,
[Exception_End_Dt] [date] NULL,
[Contract_Id] [int] NOT NULL CONSTRAINT [df_Account_Exception_Conrtract_Id] DEFAULT ((-1))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Account_Exception] ADD CONSTRAINT [pk_Account_Exception] PRIMARY KEY CLUSTERED  ([Account_Exception_Id]) ON [DB_DATA01]
GO
CREATE NONCLUSTERED INDEX [ix_Account_Exception__Account_Id__Commodity_Id__Meter_ID] ON [dbo].[Account_Exception] ([Account_Id], [Commodity_Id], [Meter_Id]) ON [DB_INDEXES01]
GO
CREATE NONCLUSTERED INDEX [IX_Account_Exception__Queue_Id__Exception_Type_Cd__Exception_Status_Cd] ON [dbo].[Account_Exception] ([Queue_Id], [Exception_Type_Cd], [Exception_Status_Cd]) ON [DB_INDEXES01]
GO
ALTER TABLE [dbo].[Account_Exception] ADD CONSTRAINT [fk_ACCOUNT__Account_Exception] FOREIGN KEY ([Account_Id]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])
GO
