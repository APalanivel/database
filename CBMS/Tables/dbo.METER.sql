CREATE TABLE [dbo].[METER]
(
[METER_ID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RATE_ID] [int] NOT NULL,
[PURCHASE_METHOD_TYPE_ID] [int] NULL,
[ACCOUNT_ID] [int] NOT NULL,
[ADDRESS_ID] [int] NOT NULL,
[METER_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_EXEMPT_STATUS] [decimal] (32, 16) NULL,
[IS_HISTORY] [bit] NULL,
[EXPIRATION_DATE] [datetime] NULL,
[COMMENTS] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_REVIEW_DATE] [datetime] NULL,
[TAX_EXEMPTION_ID] [int] NULL,
[Meter_Number_Search] AS (replace(replace(replace([Meter_Number],'/',space((0))),space((1)),space((0))),'-',space((0)))) PERSISTED,
[Meter_Type_Cd] [int] NULL
) ON [DBData_CoreClient]
ALTER TABLE [dbo].[METER] WITH NOCHECK ADD
CONSTRAINT [ACCOUNT_METER_FK] FOREIGN KEY ([ACCOUNT_ID]) REFERENCES [dbo].[ACCOUNT] ([ACCOUNT_ID])

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:	tr_Meter_Info_Change

DESCRIPTION: Trigger on METER to send change data to the 
	Change control service. 

AUTHOR INITIALS:
	Initials	Name
------------------------------------------------------------
	RKV         Ravi Kumar Vegesna
	NR			Narayana Reddy

MODIFICATIONS

	Initials	Date		Modification
------------------------------------------------------------
	RKV			2015-12-24	Created
	HG			2017-01-13	MAINT-4789, state name column added to the meter message
	HG			2017-02-03	Returning the Meter_id, Meter_Number column in camel case to match with the other meter messages going to SYS from CBMS
	NR			2017-09-13	SE2017-251 -  Excluded the Utility Contract types.
	HG			2019-07-25	SE2017-733		Sending ACT Commodity name in place of CBMS Commodity name

******/
CREATE TRIGGER [dbo].[tr_Meter_Info_Change]
ON [dbo].[METER]
FOR UPDATE
AS
      BEGIN

            DECLARE
                  @Conversation_Handle    UNIQUEIDENTIFIER
                , @Message_Contract_Meter XML
                , @From_Service           NVARCHAR(255)
                , @Target_Service         NVARCHAR(255)
                , @Target_Contract        NVARCHAR(255)
                , @Sys_Change_Operation   CHAR(1);

            DECLARE @Contract_Type_Id_List VARCHAR(MAX);

            SELECT
                  @Contract_Type_Id_List = ac.App_Config_Value
            FROM  dbo.App_Config ac
            WHERE ac.App_Config_Cd = 'Exclude_Contract_Type_Id_List'
                  AND   ac.Is_Active = 1;

            SET @Message_Contract_Meter =
                  (     SELECT
                                    'U' AS Sys_Change_Operation
                                  , NULL AS Contract_Id
                                  , ins.METER_ID AS Meter_Id
                                  , ins.METER_NUMBER AS Meter_Number
                                  , NULL Meter_Association_Dt
                                  , NULL AS Meter_Disassociation_Dt
                                  , cha.Meter_Country_Name AS Country_Name
                                  , cha.Meter_State_Name AS State_Name
                                  , isnull(act_com.ACT_Commodity_XName, com.Commodity_Name) AS Commodity_Name
                                  , @@SERVERNAME AS Server_Name
                        FROM        INSERTED ins
                                    INNER JOIN
                                    dbo.SUPPLIER_ACCOUNT_METER_MAP samm
                                          ON samm.METER_ID = ins.METER_ID
                                    INNER JOIN
                                    dbo.CONTRACT c
                                          ON samm.Contract_ID = c.CONTRACT_ID
                                    INNER JOIN
                                    Core.Client_Hier_Account cha
                                          ON ins.METER_ID = cha.Meter_Id
                                    INNER JOIN
                                    dbo.Commodity com
                                          ON c.COMMODITY_TYPE_ID = com.Commodity_Id
                                    FULL OUTER JOIN
                                    DELETED del
                                          ON del.METER_ID = ins.METER_ID
                                    LEFT OUTER JOIN
                                    (dbo.Commodity_ACT_Commodity_Map com_act_map
                                     INNER JOIN
                                     dbo.ACT_Commodity act_com
                                           ON act_com.ACT_Commodity_Id = com_act_map.ACT_Commodity_Id)
                                          ON com_act_map.Commodity_Id = com.Commodity_Id
                        WHERE       del.METER_NUMBER <> ins.METER_NUMBER
                                    AND   c.Is_Published_To_EC = 1
                                    AND   NOT EXISTS
                              (     SELECT
                                          1
                                    FROM  dbo.ufn_split(@Contract_Type_Id_List, ',') us
                                    WHERE c.CONTRACT_TYPE_ID = us.Segments )
                        GROUP BY    ins.METER_ID
                                  , ins.METER_NUMBER
                                  , cha.Meter_Country_Name
                                  , cha.Meter_State_Name
                                  , isnull(act_com.ACT_Commodity_XName, com.Commodity_Name)
                        FOR XML PATH('Contract_Meter_Change'), ELEMENTS, ROOT('Contract_Meter_Changes'));

            IF @Message_Contract_Meter IS NOT NULL
                  BEGIN

                        SET @From_Service = N'//Transmission/Service/CBMS/Cbms_Contract_Attribute_Change';

                        -- Cycle through the targets in Change_Control Targt and sent the message to each
                        DECLARE cDialog CURSOR FOR(
                        SELECT
                              Target_Service
                            , Target_Contract
                        FROM  Service_Broker_Target
                        WHERE Table_Name = 'dbo.Supplier_Account_Meter_Map');
                        OPEN cDialog;
                        FETCH NEXT FROM cDialog
                        INTO
                              @Target_Service
                            , @Target_Contract;
                        WHILE @@Fetch_Status = 0
                              BEGIN

                                    EXEC Service_Broker_Conversation_Handle_Sel
                                          @From_Service
                                        , @Target_Service
                                        , @Target_Contract
                                        , @Conversation_Handle OUTPUT;
                                    SEND ON CONVERSATION
                                          @Conversation_Handle
                                    MESSAGE TYPE [//Transmission/Message/Cbms_Contract_Meter_Change]
                                    (@Message_Contract_Meter);

                                    FETCH NEXT FROM cDialog
                                    INTO
                                          @Target_Service
                                        , @Target_Contract;
                              END;
                        CLOSE cDialog;
                        DEALLOCATE cDialog;

                  END;

      END;
GO




CREATE NONCLUSTERED INDEX [METER_N_5] ON [dbo].[METER] ([METER_NUMBER]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [IX_Meter__Meter_Number_Search] ON [dbo].[METER] ([Meter_Number_Search], [METER_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [METER_N_3] ON [dbo].[METER] ([PURCHASE_METHOD_TYPE_ID]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [METER_N_4] ON [dbo].[METER] ([RATE_ID]) ON [DB_INDEXES01]



GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the meter specific information for utility accounts', 'SCHEMA', N'dbo', 'TABLE', N'METER', NULL, NULL
GO

ALTER TABLE [dbo].[METER] ADD 
CONSTRAINT [METER_PK] PRIMARY KEY CLUSTERED  ([METER_ID]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]
CREATE NONCLUSTERED INDEX [IX_METER__Account_id__Address_id__Meter_id] ON [dbo].[METER] ([ACCOUNT_ID], [ADDRESS_ID], [METER_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_meter_account_id__rate_id] ON [dbo].[METER] ([ACCOUNT_ID], [RATE_ID]) INCLUDE ([METER_ID], [METER_NUMBER]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_meter_address_id__Meter_ID] ON [dbo].[METER] ([ADDRESS_ID], [METER_ID]) INCLUDE ([METER_NUMBER]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_meter__Meter_ID__address_id] ON [dbo].[METER] ([METER_ID], [ADDRESS_ID]) INCLUDE ([METER_NUMBER]) ON [DB_INDEXES01]



CREATE NONCLUSTERED INDEX [IX_Meter__Account_ID] ON [dbo].[METER] ([ACCOUNT_ID]) INCLUDE ([ADDRESS_ID], [RATE_ID]) ON [DB_INDEXES01]





CREATE UNIQUE NONCLUSTERED INDEX [METER_U_1] ON [dbo].[METER] ([METER_NUMBER], [ACCOUNT_ID]) WITH (FILLFACTOR=90) ON [DB_INDEXES01]




ALTER TABLE [dbo].[METER] ADD
CONSTRAINT [ADDRESS_METER_FK] FOREIGN KEY ([ADDRESS_ID]) REFERENCES [dbo].[ADDRESS] ([ADDRESS_ID])
ALTER TABLE [dbo].[METER] ADD
CONSTRAINT [ENTITY_METER_FK] FOREIGN KEY ([PURCHASE_METHOD_TYPE_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[METER] ADD
CONSTRAINT [ENTITY_METER_FK_ENTITY] FOREIGN KEY ([TAX_EXEMPTION_ID]) REFERENCES [dbo].[ENTITY] ([ENTITY_ID])
ALTER TABLE [dbo].[METER] ADD
CONSTRAINT [RATE_METER_FK] FOREIGN KEY ([RATE_ID]) REFERENCES [dbo].[RATE] ([RATE_ID])































GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******  
NAME:  
	tr_Meter_Del_Client_Hier_Account
	
DESCRIPTION:   
	While delete the meter record from meter table it will delete the entry from Core.Client_Hier_Account for that meter & Account
 
 USAGE EXAMPLES:
------------------------------------------------------------
	

BEGIN TRAN
	DELETE METER
	WHERE METER_ID = 167125 AND ACCOUNT_ID = 238695

SELECT * FROM METER WHERE METER_id = 167125


SELECT * FROM Core.Client_Hier_Account WHERE METER_id = 167125
SELECT * FROM Core.Client_Hier_Account WHERE ACCOUNT_NUMBER = 'Test_account'


ROLLBACK TRAN
	
AUTHOR INITIALS:
 Initials Name
------------------------------------------------------------
	SKA		Shobhit Kumar Agrawal

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	SKA		08/17/2010	Created

******/

CREATE TRIGGER [dbo].[tr_Meter_Del_Client_Hier_Account] ON [dbo].[METER]
      FOR DELETE
AS
BEGIN
      SET NOCOUNT ON ; 
	
      DELETE
            cha
      FROM
            core.Client_Hier_Account cha
            INNER JOIN DELETED d ON d.METER_ID = cha.Meter_Id
                                    AND d.ACCOUNT_ID = cha.ACCOUNT_ID
END 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******    
NAME:    
 dbo.tr_METER_ins_Client_Hier_Account  
   
DESCRIPTION:     
 This will insert/update the record into Core.Clinet_Hier_Account table whenever the new meter added into the meter table.  
 Client_Hier_id is based on the utility site_id  
   
 USAGE EXAMPLES:  
------------------------------------------------------------  
  
BEGIN TRAN  
INSERT INTO  
      METER  
      (  
       RATE_ID,PURCHASE_METHOD_TYPE_ID,ACCOUNT_ID,ADDRESS_ID,METER_NUMBER,TAX_EXEMPT_STATUS,IS_HISTORY,EXPIRATION_DATE  
      ,COMMENTS,TAX_REVIEW_DATE,TAX_EXEMPTION_ID )  
      SELECT  
  RATE_ID,PURCHASE_METHOD_TYPE_ID,289866,ADDRESS_ID,'TEST_Meter_Uti',TAX_EXEMPT_STATUS,IS_HISTORY,EXPIRATION_DATE  
  ,COMMENTS,TAX_REVIEW_DATE,TAX_EXEMPTION_ID  
      FROM  
            METER  
      WHERE  
            METER_ID = 217558  
  
SELECT * FROM METER WHERE METER_NUMBER = 'TEST_Meter_Uti'  
SELECT * FROM Core.Client_Hier_Account WHERE METER_NUMBER = 'TEST_Meter_Uti'  
  
ROLLBACK TRAN  
   
AUTHOR INITIALS:  
 Initials  Name  
------------------------------------------------------------  
 SKA   Shobhit Kumar Agrawal  
 RR   Raghu Reddy   
 AKR   Ashok Kumar Raju  
 SP   Sandeep Pigilam  
 NR   Narayana Reddy  
 SP          Srinivas Patchava  
  
 MODIFICATIONS:  
 Initials Date     Modification  
------------------------------------------------------------  
 SKA  08/17/2010  Created  
   09/22/2010  Added update statement for Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY  
   10/18/2010  Added update for Column Account_Vendor_Type_ID  
   10/28/2010  Added update for Column Account_Number_Search,Account_Group_ID & WATCHLIST-GROUP-INFO-ID  
      Delete the update statement for Account_RA_WatchList  
   11/23/2010  Added the statement for Account_Number_Search & Meter_Number_Search              
  
 RR  06/08/2011  Added update statememnt to populate CTE_Display_Account_Number  
 AKR  07/12/2011  Added ISNULL check in the update statement which used to update the Display_Account_Number(As this Display_Account_Number coulmn gets inserted with NULL we need this validation)   
    AP  05/04/2012  Fixed the case statement used of updating Display_Account_Number (ADLDT-202)  
      with ADLDT changes we are replacing the base tables/view with CH & CHA in the SP during these this issue was was identified during Testing by CTE QC Team.  
 AKR  2012-10-08  Modified the code to update Account_Analyst_Mapping_Code and Account_Is_Broker  
 RR  2013-08-13  MAINT-1903 Whenever the maximum meter disassociation date of the supplier account is greater than supplier accoung end Date, it is appending   
      the values with begin(Supplier_Account_Begin_Dt) date of the supplier account instead of supplier account end(Supplier_Accoung_End_Dt)date   
      for Display_Account_Number  
 SP  2014-08-08  MAINT-2920, modified to update the Last_Change_Ts column to help the error message processing objects in DVDEHub depend on this column value to re process the message.  
 HG  2015-07-17 CBMS Sourcing Enh - Changes made to update Alternate_Account_Column to CHA   
 RR  2015-10-06 Global Sourcing - Phase2 - Changes made to update Meter Type to CHA   
 RKV     2016-07-11  Maint-4056,Added to Supplier_Account_Recalc_Type_Cd to the insert Statement.  
 NR  2016-01-11 MAINT-4815 Added Template_Cu_Invoice_Id,Template_Cu_Invoice_Updated_User_Id,Template_Cu_Invoice_Last_Change_Ts.   
 NR  2017-03-07 Contract Placeholder.  
      - Update the DMO supplier account number end date as ?Unspecified? if DMO has open end date.  
      - Populate the Supplier_Account_End_Dt as 9999-12-31 if the DMO has open end date.  
      - Removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.  
 SP  2018-12-18 MAINT-8029 - Added Account_Not_Expected_Dt .  
 NR  2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.    
 NR  2019-08-20 Add Contract - Added  Supplier_Contract_ID as -1 for Utility accounts.  
******/

CREATE TRIGGER [dbo].[tr_Meter_ins_Client_Hier_Account]
ON [dbo].[METER]
FOR INSERT
AS
    BEGIN

        SET NOCOUNT ON;

        INSERT INTO Core.Client_Hier_Account
             (
                 Account_Type
                 , Client_Hier_Id
                 , Account_Id
                 , Account_Number
                 , Account_Number_Search
                 , Account_Invoice_Source_Cd
                 , Account_Is_Data_Entry_Only
                 , Account_Not_Expected
                 , Account_Not_Managed
                 , Account_Service_level_Cd --10  
                
                 , Account_Vendor_Id
                 , Account_Vendor_Name
                 , Account_Vendor_Type_ID
                 , Account_Eligibility_Dt
                 , Account_Group_ID
                                            --20  
                 , Meter_Id
                 , Meter_Number
                 , Meter_Number_Search
                 , Meter_Address_ID
                 , Meter_Address_Line_1
                 , Meter_Address_Line_2
                 , Meter_City
                 , Meter_ZipCode
                 , Meter_State_Id
                 , Meter_State_Name         --30  
                 , Meter_Country_Id
                 , Meter_Country_Name
                 , Meter_Geo_Lat
                 , Meter_Geo_Long
                 , Commodity_Id
                 , Rate_Id
                 , Rate_Name
                 , Account_Analyst_Mapping_Cd
                 , Account_Is_Broker
                 , Alternate_Account_Number
                 , Meter_Type_Cd
                 , Supplier_Account_Recalc_Type_Cd
                 , Template_Cu_Invoice_Id
                 , Template_Cu_Invoice_Updated_User_Id
                 , Template_Cu_Invoice_Last_Change_Ts
                 , Is_Invoice_Posting_Blocked
                 , Account_Not_Expected_Dt
                 , Supplier_Contract_ID
             )
        SELECT
            ent.ENTITY_NAME AS Account_Type
            , ch.Client_Hier_Id
            , i.ACCOUNT_ID
            , utiact.ACCOUNT_NUMBER
            , utiact.Account_Number_Search
            , utiact.INVOICE_SOURCE_TYPE_ID
            , ISNULL(utiact.Is_Data_Entry_Only, 0) AS Is_Data_Entry_Only
            , utiact.NOT_EXPECTED
            , utiact.NOT_MANAGED
            , utiact.SERVICE_LEVEL_TYPE_ID  --10  
            
            , utiact.VENDOR_ID
            , ven.VENDOR_NAME
            , ven.VENDOR_TYPE_ID
            , utiact.ELIGIBILITY_DATE
            , utiact.ACCOUNT_GROUP_ID
                                            --20  
            , i.METER_ID
            , i.METER_NUMBER
            , i.Meter_Number_Search
            , i.ADDRESS_ID
            , addr.ADDRESS_LINE1
            , addr.ADDRESS_LINE2
            , addr.CITY
            , addr.ZIPCODE
            , s.STATE_ID
            , s.STATE_NAME                  --30  
            , cty.COUNTRY_ID
            , cty.COUNTRY_NAME
            , addr.GEO_LAT
            , addr.GEO_LONG
            , r.COMMODITY_TYPE_ID
            , r.RATE_ID
            , r.RATE_NAME
            , utiact.Analyst_Mapping_Cd
            , utiact.Is_Broker_Account
            , utiact.Alternate_Account_Number
            , i.Meter_Type_Cd
            , utiact.Supplier_Account_Recalc_Type_Cd
            , utiact.Template_Cu_Invoice_Id
            , utiact.Template_Cu_Invoice_Updated_User_Id
            , utiact.Template_Cu_Invoice_Last_Change_Ts
            , utiact.Is_Invoice_Posting_Blocked
            , utiact.NOT_EXPECTED_DATE
            , -1 AS Supplier_Contract_ID
        FROM
            INSERTED i
            INNER JOIN dbo.ACCOUNT utiact
                ON utiact.ACCOUNT_ID = i.ACCOUNT_ID
            INNER JOIN Core.Client_Hier ch
                ON ch.Site_Id = utiact.SITE_ID
            INNER JOIN dbo.ENTITY ent
                ON utiact.ACCOUNT_TYPE_ID = ent.ENTITY_ID
            INNER JOIN dbo.RATE r
                ON i.RATE_ID = r.RATE_ID
            INNER JOIN dbo.VENDOR ven
                ON ven.VENDOR_ID = utiact.VENDOR_ID
            INNER JOIN dbo.ADDRESS addr
                ON i.ADDRESS_ID = addr.ADDRESS_ID
            INNER JOIN dbo.STATE s
                ON addr.STATE_ID = s.STATE_ID
            INNER JOIN dbo.COUNTRY cty
                ON s.COUNTRY_ID = cty.COUNTRY_ID
          

        ;WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + ISNULL(
                                    CASE WHEN MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                             CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
                                        ELSE
                                            CONVERT(
                                                VARCHAR
                                                , MAX(ISNULL(
                                                          CHA.Supplier_Meter_Disassociation_Date
                                                          , CHA.Supplier_Account_End_Dt)), 101)
                                    END, 'Unspecified') + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN Inserted I
                        ON CHA.Account_Id = I.ACCOUNT_ID
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;
    END;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******    
NAME:    
 dbo.tr_METER_upd_Client_Hier_Account  
   
DESCRIPTION:     
 The new entry will be insert into core.client_hier_account when a meter is associated with new account,   
 while if the account is alreday associated with the some other meter then update the record  
  
   
 USAGE EXAMPLES:  
------------------------------------------------------------  
   
 BEGIN TRAN   
  UPDATE METER  
  SET ADDRESS_ID = 678  
  WHERE   
  METER_NUMBER = 'TEST_Meter'  
    
 SELECT METER_ID,RATE_ID,PURCHASE_METHOD_TYPE_ID,ACCOUNT_ID,678 AS ADDRESS_ID,METER_NUMBER,TAX_EXEMPT_STATUS,IS_HISTORY,EXPIRATION_DATE FROM METER WHERE METER_NUMBER = 'TEST_Meter'  
 SELECT * FROM Core.Client_Hier_Account WHERE METER_NUMBER = 'TEST_Meter'  
 ROLLBACK TRAN  
   
AUTHOR INITIALS:  
 Initials   Name  
------------------------------------------------------------  
 SKA   Shobhit Kumar Agrawal  
 HG   Harihara Suthan G  
 RR   Raghu Reddy   
 AKR    Ashok Kumar Raju  
 AP   Athmaram Pabbathi  
 SP   Sandeep Pigilam  
 SP    Srinivas Patchava  
  
 MODIFICATIONS:  
 Initials Date     Modification  
------------------------------------------------------------  
 SKA  08/23/2010  Created  
   09/22/2010  Added update statement for Account_CONSOLIDATED_BILLING_POSTED_TO_UTILITY  
   10/18/2010  Added update for Column Account_Vendor_Type_ID  
   10/28/2010  Added update for Column Account_Number_Search,Account_Group_ID & WATCHLIST-GROUP-INFO-ID  
      Delete the update statement for Account_RA_WatchList  
   11/23/2010  Added the statement for Account_Number_Search & Meter_Number_Search        
 HG  01/07/2011  Changed the logic of Client_Hier_Id using act.Site_Id instead of Address.Address_Parent_id to have the unique logic across all the triggers.  
      (Site of a meter can be fetched in a two way   
       - Meter.Address_Id Join with Address.Address_Id and get the Address.Address_Parent_id as Site_id  
       - Meter.Account_id join with Account.Account_id get the Site_id   
         
      During the manual transition of moving account from one site to other site this Meter.Address_Id was not updated properly to avaoid the conflict made this change.  
        
 RR  06/08/2011  Added update statememnt to populate Display_Account_Number  
 AKR  07/12/2011  Added ISNULL check in the update statement which used to update the Display_Account_Number(As this Display_Account_Number coulmn gets inserted with NULL we need this validation)   
    AP  05/04/2012  Fixed the case statement used of updating Display_Account_Number (ADLDT-202)  
      with ADLDT changes we are replacing the base tables/view with CH & CHA in the SP during these this issue was was identified during Testing by CTE QC Team.  
 SP  2014-08-08  MAINT-2920, modified to update the Last_Change_Ts column to help the error message processing objects in DVDEHub depend on this column value to re process the message.  
          
 HG  2015-07-17 CBMS Sourcing Enh - Changes made to update Alternate_Account_Column to CHA   
 RR  2015-10-06 Global Sourcing - Phase2 - Changes made to update Meter Type to CHA   
 NR  2017-03-07 Contract Placeholder.  
      - Update the DMO supplier account number end date as ?Unspecified? if DMO has open end date.  
      - Populate the Supplier_Account_End_Dt as 9999-12-31 if the DMO has open end date.  
      - Removed CONSOLIDATED_BILLING_POSTED_TO_UTILITY column.  
 SP  2018-12-18 MAINT-8029 - Added Account_Not_Expected_Dt .  
 NR  2019-04-03 Data2.0 - Removed WATCHLIST-GROUP-INFO-ID  column from Account Table.    
 NR  2019-08-20 Add Contract - Added  Supplier_Contract_ID as -1 for Utility accounts.  
 NR  2020-01-02 MAINT-9712  Added group by clause.   
  
  
  
******/
CREATE TRIGGER [dbo].[tr_METER_upd_Client_Hier_Account]
ON [dbo].[METER]
FOR UPDATE
AS
    BEGIN

        SET NOCOUNT ON;

        MERGE INTO Core.Client_Hier_Account AS tgt
        USING (   SELECT
                        ISNULL(cah.Account_Type, ent.ENTITY_NAME) AS Account_Type
                        , ISNULL(ch.Client_Hier_Id, cah.Client_Hier_Id) AS Client_Hier_Id
                        , ISNULL(cah.Account_Id, i.ACCOUNT_ID) AS ACCOUNT_ID    --ACCOUNT ID CAN NEVER BE CHANGE FOR THE METER  
                        , ISNULL(cah.Account_Number, act.ACCOUNT_NUMBER) AS ACCOUNT_NUMBER
                        , ISNULL(cah.Account_Number_Search, act.Account_Number_Search) AS ACCOUNT_NUMBER_Search
                        , ISNULL(cah.Account_Invoice_Source_Cd, act.INVOICE_SOURCE_TYPE_ID) AS Account_Invoice_Source_Cd
                        , COALESCE(cah.Account_Is_Data_Entry_Only, act.Is_Data_Entry_Only, 0) AS Is_Data_Entry_Only
                        , ISNULL(cah.Account_Not_Expected, act.NOT_EXPECTED) AS Account_Not_Expected
                        , ISNULL(cah.Account_Not_Managed, act.NOT_MANAGED) AS Account_Not_Managed
                        , ISNULL(cah.Account_Service_level_Cd, act.SERVICE_LEVEL_TYPE_ID) AS Account_Service_level_Cd
                        , ISNULL(cah.Account_Group_ID, act.ACCOUNT_GROUP_ID) AS Account_Group_ID
                        , ven.VENDOR_ID
                        , ven.VENDOR_NAME
                        , ven.VENDOR_TYPE_ID
                        , ISNULL(cah.Account_Eligibility_Dt, act.ELIGIBILITY_DATE) AS Account_Eligibility_Dt
                        , i.METER_ID
                        , i.METER_NUMBER
                        , i.Meter_Number_Search
                        , i.ADDRESS_ID
                        , addr.ADDRESS_LINE1
                        , addr.ADDRESS_LINE2
                        , addr.CITY
                        , addr.ZIPCODE
                        , s.STATE_ID
                        , s.STATE_NAME
                        , cty.COUNTRY_ID
                        , cty.COUNTRY_NAME
                        , addr.GEO_LAT
                        , addr.GEO_LONG
                        , r.COMMODITY_TYPE_ID
                        , r.RATE_ID
                        , r.RATE_NAME
                        , cah.Meter_Id AS Account_Meter_ID
                        , act.Alternate_Account_Number
                        , i.Meter_Type_Cd
                        , ISNULL(cah.Account_Not_Expected_Dt, act.NOT_EXPECTED_DATE) AS Account_Not_Expected_Dt
                  FROM
                        INSERTED i
                        INNER JOIN DELETED d
                            ON i.METER_ID = d.METER_ID
                        INNER JOIN dbo.ACCOUNT act
                            ON act.ACCOUNT_ID = i.ACCOUNT_ID
                        INNER JOIN dbo.ENTITY ent
                            ON act.ACCOUNT_TYPE_ID = ent.ENTITY_ID
                        INNER JOIN dbo.ADDRESS addr
                            ON i.ADDRESS_ID = addr.ADDRESS_ID
                        INNER JOIN dbo.[STATE] s
                            ON addr.STATE_ID = s.STATE_ID
                        INNER JOIN dbo.COUNTRY cty
                            ON s.COUNTRY_ID = cty.COUNTRY_ID
                        INNER JOIN dbo.SITE st
                            ON st.SITE_ID = act.SITE_ID
                        INNER JOIN Core.Client_Hier ch
                            ON ch.Site_Id = st.SITE_ID
                        LEFT JOIN dbo.RATE r
                            ON i.RATE_ID = r.RATE_ID
                        LEFT JOIN dbo.VENDOR ven
                            ON r.VENDOR_ID = ven.VENDOR_ID
                        LEFT JOIN Core.Client_Hier_Account cah
                            ON cah.Meter_Id = i.METER_ID
                  GROUP BY
                      ISNULL(cah.Account_Type, ent.ENTITY_NAME)
                      , ISNULL(ch.Client_Hier_Id, cah.Client_Hier_Id)
                      , ISNULL(cah.Account_Id, i.ACCOUNT_ID)
                      , ISNULL(cah.Account_Number, act.ACCOUNT_NUMBER)
                      , ISNULL(cah.Account_Number_Search, act.Account_Number_Search)
                      , ISNULL(cah.Account_Invoice_Source_Cd, act.INVOICE_SOURCE_TYPE_ID)
                      , COALESCE(cah.Account_Is_Data_Entry_Only, act.Is_Data_Entry_Only, 0)
                      , ISNULL(cah.Account_Not_Expected, act.NOT_EXPECTED)
                      , ISNULL(cah.Account_Not_Managed, act.NOT_MANAGED)
                      , ISNULL(cah.Account_Service_level_Cd, act.SERVICE_LEVEL_TYPE_ID)
                      , ISNULL(cah.Account_Group_ID, act.ACCOUNT_GROUP_ID)
                      , ven.VENDOR_ID
                      , ven.VENDOR_NAME
                      , ven.VENDOR_TYPE_ID
                      , ISNULL(cah.Account_Eligibility_Dt, act.ELIGIBILITY_DATE)
                      , i.METER_ID
                      , i.METER_NUMBER
                      , i.Meter_Number_Search
                      , i.ADDRESS_ID
                      , addr.ADDRESS_LINE1
                      , addr.ADDRESS_LINE2
                      , addr.CITY
                      , addr.ZIPCODE
                      , s.STATE_ID
                      , s.STATE_NAME
                      , cty.COUNTRY_ID
                      , cty.COUNTRY_NAME
                      , addr.GEO_LAT
                      , addr.GEO_LONG
                      , r.COMMODITY_TYPE_ID
                      , r.RATE_ID
                      , r.RATE_NAME
                      , cah.Meter_Id
                      , act.Alternate_Account_Number
                      , i.Meter_Type_Cd
                      , ISNULL(cah.Account_Not_Expected_Dt, act.NOT_EXPECTED_DATE)) src
        ON (   src.ACCOUNT_ID = tgt.Account_Id
               AND  src.METER_ID = tgt.Meter_Id)
           OR   (   src.ACCOUNT_ID = tgt.Account_Id
                    AND Account_Meter_ID IS NULL)
        WHEN MATCHED THEN UPDATE SET
                              tgt.Meter_Number = src.METER_NUMBER
                              , tgt.Meter_Number_Search = src.Meter_Number_Search
                              , tgt.Meter_Address_ID = src.ADDRESS_ID
                              , tgt.Meter_Address_Line_1 = src.ADDRESS_LINE1
                              , tgt.Meter_Address_Line_2 = src.ADDRESS_LINE2
                              , tgt.Meter_City = src.CITY
                              , tgt.Meter_ZipCode = src.ZIPCODE
                              , tgt.Meter_State_Id = src.STATE_ID
                              , tgt.Meter_State_Name = src.STATE_NAME
                              , tgt.Meter_Country_Id = src.COUNTRY_ID
                              , tgt.Meter_Country_Name = src.COUNTRY_NAME
                              , tgt.Meter_Geo_Lat = src.GEO_LAT
                              , tgt.Meter_Geo_Long = src.GEO_LONG
                              , tgt.Commodity_Id = src.COMMODITY_TYPE_ID
                              , tgt.Rate_Id = src.RATE_ID
                              , tgt.Rate_Name = src.RATE_NAME
                              , tgt.Alternate_Account_Number = src.Alternate_Account_Number
                              , Last_Change_TS = GETDATE()
                              , tgt.Meter_Type_Cd = src.Meter_Type_Cd
                              , tgt.Account_Not_Expected_Dt = src.Account_Not_Expected_Dt
        WHEN NOT MATCHED THEN INSERT (Account_Type
                                      , Client_Hier_Id
                                      , Account_Id
                                      , Account_Number
                                      , Account_Number_Search
                                      , Account_Invoice_Source_Cd
                                      , Account_Is_Data_Entry_Only
                                      , Account_Not_Expected
                                      , Account_Not_Managed
                                      , Account_Service_level_Cd    --10  
                                      , Account_Vendor_Id
                                      , Account_Vendor_Name
                                      , Account_Vendor_Type_ID
                                      , Account_Eligibility_Dt
                                      , Account_Group_ID
                                                                    --20  
                                      , Meter_Id
                                      , Meter_Number
                                      , Meter_Number_Search
                                      , Meter_Address_ID
                                      , Meter_Address_Line_1
                                      , Meter_Address_Line_2
                                      , Meter_City
                                      , Meter_ZipCode
                                      , Meter_State_Id
                                      , Meter_State_Name            --30  
                                      , Meter_Country_Id
                                      , Meter_Country_Name
                                      , Meter_Geo_Lat
                                      , Meter_Geo_Long
                                      , Commodity_Id
                                      , Rate_Id
                                      , Rate_Name
                                      , Alternate_Account_Number
                                      , Meter_Type_Cd
                                      , Account_Not_Expected_Dt
                                      , Supplier_Contract_ID)
                              VALUES
                                  (src.Account_Type
                                   , src.Client_Hier_Id
                                   , src.ACCOUNT_ID
                                   , src.ACCOUNT_NUMBER
                                   , src.ACCOUNT_NUMBER_Search
                                   , src.Account_Invoice_Source_Cd
                                   , src.Is_Data_Entry_Only
                                   , src.Account_Not_Expected
                                   , src.Account_Not_Managed
                                   , src.Account_Service_level_Cd   --10  
                                   , src.VENDOR_ID
                                   , src.VENDOR_NAME
                                   , src.VENDOR_TYPE_ID
                                   , src.Account_Eligibility_Dt
                                   , src.Account_Group_ID
                                                                    --20  
                                   , src.METER_ID
                                   , src.METER_NUMBER
                                   , src.Meter_Number_Search
                                   , src.ADDRESS_ID
                                   , src.ADDRESS_LINE1
                                   , src.ADDRESS_LINE2
                                   , src.CITY
                                   , src.ZIPCODE
                                   , src.STATE_ID
                                   , src.STATE_NAME                 --30  
                                   , src.COUNTRY_ID
                                   , src.COUNTRY_NAME
                                   , src.GEO_LAT
                                   , src.GEO_LONG
                                   , src.COMMODITY_TYPE_ID
                                   , src.RATE_ID
                                   , src.RATE_NAME
                                   , src.Alternate_Account_Number
                                   , src.Meter_Type_Cd
                                   , src.Account_Not_Expected_Dt
                                   , -1);

        WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + ISNULL(
                                    CASE WHEN MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                             CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                        ELSE
                                            CONVERT(
                                                VARCHAR
                                                , MAX(ISNULL(
                                                          CHA.Supplier_Meter_Disassociation_Date
                                                          , CHA.Supplier_Account_End_Dt)), 101)
                                    END, 'Unspecified') + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN Inserted I
                        ON CHA.Account_Id = I.ACCOUNT_ID
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;
    END;;
GO
