CREATE TABLE [dbo].[RM_RISK_INSIGHT]
(
[RM_Risk_Insight_id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Insight_Date] [smalldatetime] NOT NULL,
[Insight_Title] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__RM_RISK_I__Insig__5F2D8438] DEFAULT (''),
[Author_id] [int] NOT NULL,
[Insight_Text] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DBData_Risk_MGMT] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Risk management Market Updates. ', 'SCHEMA', N'dbo', 'TABLE', N'RM_RISK_INSIGHT', NULL, NULL
GO

ALTER TABLE [dbo].[RM_RISK_INSIGHT] ADD 
CONSTRAINT [PK_RM_RISK_INSIGHT] PRIMARY KEY CLUSTERED  ([RM_Risk_Insight_id]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
