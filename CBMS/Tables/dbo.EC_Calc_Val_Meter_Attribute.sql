CREATE TABLE [dbo].[EC_Calc_Val_Meter_Attribute]
(
[EC_Calc_Val_Id] [int] NOT NULL,
[EC_Meter_Attribute_Id] [int] NOT NULL,
[EC_Meter_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Calc_Val_Meter_Attribute__Created_Ts] DEFAULT (getdate()),
[Update_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NULL CONSTRAINT [df_EC_Calc_Val_Meter_Attribute__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Calc_Val_Meter_Attribute] ADD CONSTRAINT [pk_EC_Calc_Val_Meter_Attribute] PRIMARY KEY CLUSTERED  ([EC_Calc_Val_Id], [EC_Meter_Attribute_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Calc_Val_Meter_Attribute] ADD CONSTRAINT [fk_EC_Calc_Val__EC_Calc_Val_Meter_Attribute] FOREIGN KEY ([EC_Calc_Val_Id]) REFERENCES [dbo].[EC_Calc_Val] ([EC_Calc_Val_Id])
GO
ALTER TABLE [dbo].[EC_Calc_Val_Meter_Attribute] ADD CONSTRAINT [fk_EC_Meter_Attribute__EC_Calc_Val_Meter_Attribute] FOREIGN KEY ([EC_Meter_Attribute_Id]) REFERENCES [dbo].[EC_Meter_Attribute] ([EC_Meter_Attribute_Id])
GO
