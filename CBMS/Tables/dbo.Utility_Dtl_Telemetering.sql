CREATE TABLE [dbo].[Utility_Dtl_Telemetering]
(
[Utility_Dtl_Telemetering_Id] [int] NOT NULL IDENTITY(1, 1),
[Question_Commodity_Map_Id] [int] NOT NULL,
[Vendor_Commodity_Map_Id] [int] NOT NULL,
[Industrial_Flag_Cd] [int] NOT NULL,
[Commercial_Flag_Cd] [int] NOT NULL,
[Comment_ID] [int] NULL
) ON [DBData_CoreClient]
GO
EXEC sp_addextendedproperty N'MS_Description', 'captures the ‘Telemetering Information’ section specific data', 'SCHEMA', N'dbo', 'TABLE', N'Utility_Dtl_Telemetering', NULL, NULL
GO

ALTER TABLE [dbo].[Utility_Dtl_Telemetering] ADD CONSTRAINT [unc_Utility_Dtl_Telemetering__Question_Commodity_Map_Id__Vendor_Commodity_Map_Id] UNIQUE CLUSTERED  ([Question_Commodity_Map_Id], [Vendor_Commodity_Map_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

ALTER TABLE [dbo].[Utility_Dtl_Telemetering] ADD CONSTRAINT [pk_Utility_Dtl_Telemetering] PRIMARY KEY NONCLUSTERED  ([Utility_Dtl_Telemetering_Id]) WITH (FILLFACTOR=90) ON [DBData_CoreClient]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Telemetering__Vendor_Commodity_Map_Id] ON [dbo].[Utility_Dtl_Telemetering] ([Vendor_Commodity_Map_Id]) ON [DB_INDEXES01]

CREATE NONCLUSTERED INDEX [ix_Utility_Dtl_Telemetering__Comment_ID] ON [dbo].[Utility_Dtl_Telemetering] ([Comment_ID]) ON [DB_INDEXES01]

ALTER TABLE [dbo].[Utility_Dtl_Telemetering] ADD
CONSTRAINT [fk_Question_Commodity_Map__Utility_Dtl_Telemetering] FOREIGN KEY ([Question_Commodity_Map_Id]) REFERENCES [dbo].[Question_Commodity_Map] ([Question_Commodity_Map_Id])
ALTER TABLE [dbo].[Utility_Dtl_Telemetering] ADD
CONSTRAINT [fk_VENDOR_COMMODITY_MAP__Utility_Dtl_Telemetering] FOREIGN KEY ([Vendor_Commodity_Map_Id]) REFERENCES [dbo].[VENDOR_COMMODITY_MAP] ([VENDOR_COMMODITY_MAP_ID])
ALTER TABLE [dbo].[Utility_Dtl_Telemetering] ADD
CONSTRAINT [fk_Comment__Utility_Dtl_Telemetering] FOREIGN KEY ([Comment_ID]) REFERENCES [dbo].[Comment] ([Comment_ID])




GO
