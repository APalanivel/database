CREATE TABLE [dbo].[EC_Meter_Attribute_Value]
(
[EC_Meter_Attribute_Value_Id] [int] NOT NULL IDENTITY(1, 1),
[EC_Meter_Attribute_Id] [int] NOT NULL,
[EC_Meter_Attribute_Value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute_Value__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_EC_Meter_Attribute_Value__Last_Change_Ts] DEFAULT (getdate()),
[Is_Default] [bit] NOT NULL CONSTRAINT [DF__EC_Meter___Is_De__4CBDDFA5] DEFAULT ((0))
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Value] ADD CONSTRAINT [pk_EC_Meter_Attribute_Value] PRIMARY KEY NONCLUSTERED  ([EC_Meter_Attribute_Value_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Value] ADD CONSTRAINT [unc_EC_Meter_Attribute_Value__EC_Meter_Attribute_Id__EC_Meter_Attribute_Value] UNIQUE CLUSTERED  ([EC_Meter_Attribute_Id], [EC_Meter_Attribute_Value]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[EC_Meter_Attribute_Value] ADD CONSTRAINT [fk_EC_Meter_Attribute__EC_Meter_Attribute_Value] FOREIGN KEY ([EC_Meter_Attribute_Id]) REFERENCES [dbo].[EC_Meter_Attribute] ([EC_Meter_Attribute_Id])
GO
