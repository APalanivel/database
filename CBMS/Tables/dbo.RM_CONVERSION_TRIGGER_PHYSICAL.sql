CREATE TABLE [dbo].[RM_CONVERSION_TRIGGER_PHYSICAL]
(
[CLIENT_ID] [int] NOT NULL,
[SITE_ID] [int] NOT NULL,
[CONTRACT_ID] [int] NULL,
[CONTRACT_NUMBER] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TRIGGER_MONTH] [datetime] NOT NULL,
[TRIGGER_SET_DATE] [datetime] NOT NULL,
[QUANTITY] [decimal] (18, 0) NOT NULL,
[TRIGGER_PRICE] [decimal] (32, 16) NOT NULL,
[BASIS_LOCATION] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPSIDE_DOWNSIDE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEAL_TICKET_NUMBER] [int] NOT NULL,
[FREQUENCY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UNIT] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IM_WS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IS_REJECTED] [bit] NULL
) ON [PRIMARY]
ALTER TABLE [dbo].[RM_CONVERSION_TRIGGER_PHYSICAL] ADD
CONSTRAINT [FK_RM_CONVERSION_TRIGGER_PHYSICAL_CLIENT] FOREIGN KEY ([CLIENT_ID]) REFERENCES [dbo].[CLIENT] ([CLIENT_ID])
ALTER TABLE [dbo].[RM_CONVERSION_TRIGGER_PHYSICAL] ADD
CONSTRAINT [FK_RM_CONVERSION_TRIGGER_PHYSICAL_SITE] FOREIGN KEY ([SITE_ID]) REFERENCES [dbo].[SITE] ([SITE_ID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table was used to convert RM Trigger Physical deals from a spreadsheet of data back in 2005. There is no current data in this table.', 'SCHEMA', N'dbo', 'TABLE', N'RM_CONVERSION_TRIGGER_PHYSICAL', NULL, NULL
GO
