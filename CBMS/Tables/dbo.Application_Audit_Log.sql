CREATE TABLE [dbo].[Application_Audit_Log]
(
[Client_Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Application_Name] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Audit_Function] [smallint] NOT NULL,
[Table_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Lookup_Value] [xml] NOT NULL,
[Event_By_User] [varchar] (81) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Execution_Ts] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE CLUSTERED INDEX [cix_Application_Audit_Log__Execution_Ts] ON [dbo].[Application_Audit_Log] ([Execution_Ts]) ON [PRIMARY]

GO
EXEC sp_addextendedproperty N'MS_Description', 'Captures audit trail for Delete Tools.', 'SCHEMA', N'dbo', 'TABLE', N'Application_Audit_Log', NULL, NULL
GO

ALTER TABLE [dbo].[Application_Audit_Log] ADD CONSTRAINT [CK__Applicati__Audit__6635E23A] CHECK (([Audit_Function]=(1) OR [Audit_Function]=(0) OR [Audit_Function]=(-1)))
GO
