CREATE TABLE [dbo].[CURRENCY_UNIT_COUNTRY_MAP]
(
[CURRENCY_UNIT_ID] [int] NOT NULL,
[COUNTRY_ID] [int] NOT NULL
) ON [DBData_Cost_Usage]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Junction between Currencies and the counties that used them', 'SCHEMA', N'dbo', 'TABLE', N'CURRENCY_UNIT_COUNTRY_MAP', NULL, NULL
GO

ALTER TABLE [dbo].[CURRENCY_UNIT_COUNTRY_MAP] ADD 
CONSTRAINT [PK_CURRENCY_UNIT_COUNTRY_MAP] PRIMARY KEY CLUSTERED  ([CURRENCY_UNIT_ID], [COUNTRY_ID]) WITH (FILLFACTOR=90) ON [DBData_Cost_Usage]
GO
