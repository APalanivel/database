CREATE TABLE [dbo].[Cbms_Rule_Filter]
(
[Cbms_Rule_Filter_Id] [int] NOT NULL IDENTITY(1, 1),
[Filter_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Filter_UDF] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Cbms_Filter_Class_Id] [int] NOT NULL,
[Lookup_Sp_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Rule_Filter] ADD CONSTRAINT [pk_Cbms_Rule_Filter] PRIMARY KEY CLUSTERED  ([Cbms_Rule_Filter_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Cbms_Rule_Filter] ADD CONSTRAINT [un_Cbms_Rule_Filter__Filter_Name] UNIQUE NONCLUSTERED  ([Filter_Name]) ON [DB_DATA01]
GO
