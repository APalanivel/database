CREATE TABLE [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Map]
(
[Ec_Calc_Val_Bucket_Map_Id] [int] NOT NULL,
[EC_Invoice_Sub_Bucket_Master_Id] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Ec_Calc_Val_Bucket_Sub_Bucket_Map__Created_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Map] ADD CONSTRAINT [pk_Ec_Calc_Val_Bucket_Map_Id__EC_Invoice_Sub_Bucket_Master_Id] PRIMARY KEY CLUSTERED  ([Ec_Calc_Val_Bucket_Map_Id], [EC_Invoice_Sub_Bucket_Master_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Map] ADD CONSTRAINT [fk_Ec_Calc_Val_Bucket_Sub_Bucket_Map__Ec_Calc_Val_Bucket_Map] FOREIGN KEY ([Ec_Calc_Val_Bucket_Map_Id]) REFERENCES [dbo].[Ec_Calc_Val_Bucket_Map] ([Ec_Calc_Val_Bucket_Map_Id])
GO
ALTER TABLE [dbo].[Ec_Calc_Val_Bucket_Sub_Bucket_Map] ADD CONSTRAINT [fk_Ec_Calc_Val_Bucket_Sub_Bucket_Map__EC_Invoice_Sub_Bucket_Master] FOREIGN KEY ([EC_Invoice_Sub_Bucket_Master_Id]) REFERENCES [dbo].[EC_Invoice_Sub_Bucket_Master] ([EC_Invoice_Sub_Bucket_Master_Id])
GO
