CREATE TABLE [dbo].[Sr_Service_Condition_Template]
(
[Sr_Service_Condition_Template_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Template_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template__Is_Active] DEFAULT ((0)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_Sr_Service_Condition_Template__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template] ADD CONSTRAINT [pk_Sr_Service_Condition_Template] PRIMARY KEY CLUSTERED  ([Sr_Service_Condition_Template_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Sr_Service_Condition_Template] ADD CONSTRAINT [un_Sr_Service_Condition_Template__Template_Name] UNIQUE NONCLUSTERED  ([Template_Name]) ON [DB_DATA01]
GO
