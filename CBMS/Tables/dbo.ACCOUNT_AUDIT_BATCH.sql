CREATE TABLE [dbo].[ACCOUNT_AUDIT_BATCH]
(
[ACCOUNT_AUDIT_BATCH_ID] [int] NOT NULL IDENTITY(1, 1),
[BATCH_DATE] [datetime] NOT NULL
) ON [DBData_Deprecated]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Was previously used for a process whereby analysts would randomly audit accounts for accuracy.  Degraded.', 'SCHEMA', N'dbo', 'TABLE', N'ACCOUNT_AUDIT_BATCH', NULL, NULL
GO

ALTER TABLE [dbo].[ACCOUNT_AUDIT_BATCH] ADD 
CONSTRAINT [PK_ACCOUNT_AUDIT_BATCH] PRIMARY KEY CLUSTERED  ([ACCOUNT_AUDIT_BATCH_ID]) WITH (FILLFACTOR=90) ON [DBData_Deprecated]

GO
