CREATE TABLE [dbo].[App_Global_Config_Output_Column]
(
[App_Global_Config_Output_Column_Id] [int] NOT NULL IDENTITY(1, 1),
[App_Global_Config_Id] [int] NOT NULL,
[Column_Display_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data_Source_Column_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Display_Seq] [int] NOT NULL,
[Date_Format] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Column_Header_CSS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Output_Column__Is_Active] DEFAULT ((1)),
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config_Output_Column__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [df_App_Global_Config_Output_Column__Last_Change_Ts] DEFAULT (getdate()),
[Is_Visible_To_User] [bit] NULL,
[Data_Source_Query] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Selection_Filter_Type_Cd] [int] NULL,
[Is_Editable] [bit] NOT NULL CONSTRAINT [df_App_Global_Config_Output_Column__Is_Editable] DEFAULT ((0)),
[Link_Type_CD] [int] NULL,
[Column_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Output_Column] ADD CONSTRAINT [pk_App_Global_Config_Output_Column] PRIMARY KEY CLUSTERED  ([App_Global_Config_Output_Column_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Output_Column] ADD CONSTRAINT [un_App_Global_Config_Output_Column__App_Global_Config_Id__Column_Name] UNIQUE NONCLUSTERED  ([App_Global_Config_Id], [Column_Display_Name]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[App_Global_Config_Output_Column] ADD CONSTRAINT [fk_App_Global_Config__App_Global_Config_Output_Column] FOREIGN KEY ([App_Global_Config_Id]) REFERENCES [dbo].[App_Global_Config] ([App_Global_Config_Id])
GO
