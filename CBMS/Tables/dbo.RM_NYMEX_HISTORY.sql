CREATE TABLE [dbo].[RM_NYMEX_HISTORY]
(
[MONTH_IDENTIFIER] [datetime] NOT NULL,
[SETTLEMENT_PRICE] [decimal] (32, 16) NULL
) ON [DBData_Risk_MGMT]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table would store the nymex settlement prices for the months as they keep getting settled', 'SCHEMA', N'dbo', 'TABLE', N'RM_NYMEX_HISTORY', NULL, NULL
GO

ALTER TABLE [dbo].[RM_NYMEX_HISTORY] ADD 
CONSTRAINT [PK_RM_NYMEX_HISTORY] PRIMARY KEY CLUSTERED  ([MONTH_IDENTIFIER]) WITH (FILLFACTOR=90) ON [DBData_Risk_MGMT]
GO
