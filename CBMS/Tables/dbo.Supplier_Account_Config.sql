CREATE TABLE [dbo].[Supplier_Account_Config]
(
[Supplier_Account_Config_Id] [int] NOT NULL IDENTITY(1, 1),
[Account_Id] [int] NOT NULL,
[Contract_Id] [int] NOT NULL,
[Supplier_Account_Begin_Dt] [date] NOT NULL,
[Supplier_Account_End_Dt] [date] NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL CONSTRAINT [DF_Supplier_Account_Config__Created_Ts] DEFAULT (getdate()),
[Updated_User_Id] [int] NOT NULL,
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF_Supplier_Account_Config__Last_Change_Ts] DEFAULT (getdate())
) ON [DB_DATA01]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.tr_Supplier_Account_Config_Upd_Client_Hier_Account

DESCRIPTION:
	The new entry will be insert into core.client_hier_account when a meter is associated with new account,
	while if the account is alreday associated with the some other meter then update the record

 USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN

	SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1655866
	SELECT * FROM dbo.Supplier_Account_Meter_Map sac WHERE sac.Account_Id =1655866
	 
	SELECT cha.Display_Account_Number FROM core.Client_Hier_Account cha  WHERE cha.Account_Id = 1655866 


			DELETE
samm
FROM
    dbo.Supplier_Account_Config samm
WHERE
    samm.ACCOUNT_ID = 1655866
    AND samm.Supplier_Account_Config_Id = 507673;

DELETE
samm
FROM
    dbo.SUPPLIER_ACCOUNT_METER_MAP samm
WHERE
    samm.ACCOUNT_ID = 1655866
    AND samm.Supplier_Account_Config_Id = 507673;
		 

	SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1655866
	SELECT * FROM dbo.Supplier_Account_Meter_Map sac WHERE sac.Account_Id =1655866
	 
	SELECT cha.Display_Account_Number FROM core.Client_Hier_Account cha  WHERE cha.Account_Id = 1655866 


	ROLLBACK TRAN

AUTHOR INITIALS:
 Initials		 Name
------------------------------------------------------------
 NR				Narayana Reddy

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	NR		2020-01-20	Add Contract - Created to update legacy account display account number when 
			time band is deleted for legacy accounts.

******/


CREATE TRIGGER [dbo].[tr_Supplier_Account_Config_Del_Cha_Display_Account_Number_Upd]
ON [dbo].[Supplier_Account_Config]
FOR DELETE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Legacy_Account_Dt DATETIME;


        SELECT
            @Legacy_Account_Dt = CONVERT(DATETIME, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Legacy_Account';


        DECLARE @Total_Supplier_Accoount_Config_Ids TABLE
              (
                  Account_Id INT
                  , Total_Supplier_Accoount_Config_Ids INT
              );

        INSERT INTO @Total_Supplier_Accoount_Config_Ids
             (
                 Account_Id
                 , Total_Supplier_Accoount_Config_Ids
             )
        SELECT
            i.Account_Id
            , ISNULL(COUNT(DISTINCT sac.Supplier_Account_Config_Id), 0)
        FROM
            Deleted i
            INNER JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = i.Account_Id
        GROUP BY
            i.Account_Id;



        WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + CASE WHEN supact.Supplier_Account_End_Dt IS NULL THEN 'Unspecified'
                                    WHEN MAX(ISNULL(CHA.Supplier_Meter_Disassociation_Date, CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                        CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)), 101)
                                END + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN Deleted I
                        ON CHA.Account_Id = I.Account_Id
                    INNER JOIN dbo.Supplier_Account_Config supact
                        ON supact.Account_Id = I.Account_Id
                    INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                        ON tsaci.Account_Id = supact.Account_Id
               WHERE
                    tsaci.Total_Supplier_Accoount_Config_Ids = 1
                    AND supact.Created_Ts <= @Legacy_Account_Dt
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
                   , supact.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;



        UPDATE
            CHA
        SET
            CHA.Display_Account_Number = CHA.Account_Number
            , CHA.Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            INNER JOIN Deleted I
                ON I.Account_Id = CHA.Account_Id
            INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                ON tsaci.Account_Id = I.Account_Id
        WHERE
            tsaci.Total_Supplier_Accoount_Config_Ids > 1
            AND ISNULL(CHA.Display_Account_Number, '') <> ISNULL(CHA.Account_Number, '');


    END;
    ;



    ;











GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******
NAME:
	dbo.tr_Supplier_Account_Config_Upd_Client_Hier_Account

DESCRIPTION:
	The new entry will be insert into core.client_hier_account when a meter is associated with new account,
	while if the account is alreday associated with the some other meter then update the record

 USAGE EXAMPLES:
------------------------------------------------------------

	BEGIN TRAN

	SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1655866 

	SELECT cha.Display_Account_Number FROM core.Client_Hier_Account cha  WHERE cha.Account_Id = 1655866 


		UPDATE Supplier_Account_Config
		SET Supplier_Account_End_Dt = '2021-11-29'
		WHERE
		 ACCOUNT_ID = 1655866
		 

		SELECT * FROM dbo.Supplier_Account_Config sac WHERE sac.Account_Id =1655866 

	SELECT cha.Display_Account_Number FROM core.Client_Hier_Account cha  WHERE cha.Account_Id = 1655866 

	ROLLBACK TRAN

AUTHOR INITIALS:
 Initials		 Name
------------------------------------------------------------
 NR				Narayana Reddy

 MODIFICATIONS:
 Initials	Date		Modification
------------------------------------------------------------
	NR		2019-07-15	Add Contract - Created.

******/


CREATE TRIGGER [dbo].[tr_Supplier_Account_Config_Upd_Client_Hier_Account]
ON [dbo].[Supplier_Account_Config]
FOR UPDATE
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @Legacy_Account_Dt DATETIME;


        SELECT
            @Legacy_Account_Dt = CONVERT(DATETIME, ac.App_Config_Value)
        FROM
            dbo.App_Config ac
        WHERE
            ac.App_Config_Cd = 'Add_Contract_Legacy_Account';


        DECLARE @Total_Supplier_Accoount_Config_Ids TABLE
              (
                  Account_Id INT
                  , Total_Supplier_Accoount_Config_Ids INT
              );

        INSERT INTO @Total_Supplier_Accoount_Config_Ids
             (
                 Account_Id
                 , Total_Supplier_Accoount_Config_Ids
             )
        SELECT
            i.Account_Id
            , ISNULL(COUNT(DISTINCT sac.Supplier_Account_Config_Id), 0)
        FROM
            INSERTED i
            INNER JOIN dbo.Supplier_Account_Config sac
                ON sac.Account_Id = i.Account_Id
        GROUP BY
            i.Account_Id;


        UPDATE
            cha
        SET
            cha.Supplier_Account_begin_Dt = ISNULL(i.Supplier_Account_Begin_Dt, d.Supplier_Account_Begin_Dt)
            , cha.Supplier_Account_End_Dt = COALESCE(i.Supplier_Account_End_Dt, d.Supplier_Account_End_Dt, '9999-12-31')
        FROM
            Inserted i
            INNER JOIN Deleted d
                ON i.Account_Id = d.Account_Id
                   AND  i.Contract_Id = d.Contract_Id
                   AND  i.Supplier_Account_Config_Id = d.Supplier_Account_Config_Id
            INNER JOIN Core.Client_Hier_Account cha
                ON cha.Account_Id = d.Account_Id
                   AND  cha.Supplier_Contract_ID = d.Contract_Id
                   AND  cha.Supplier_Account_Config_Id = i.Supplier_Account_Config_Id;





        WITH CTE_Display_Account_Number
        AS (
               SELECT
                    CHA.Account_Id
                    , CASE WHEN CHA.Account_Type = 'Utility' THEN CHA.Account_Number
                          ELSE
                              ISNULL(CHA.Account_Number, 'Blank for ' + CHA.Account_Vendor_Name) + ' ('
                              + CASE WHEN MIN(ISNULL(CHA.Supplier_Meter_Association_Date, CHA.Supplier_Account_begin_Dt)) < CHA.Supplier_Account_begin_Dt THEN
                                         CONVERT(VARCHAR, CHA.Supplier_Account_begin_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MIN(ISNULL(
                                                      CHA.Supplier_Meter_Association_Date
                                                      , CHA.Supplier_Account_begin_Dt)), 101)
                                END + '-'
                              + CASE WHEN supact.Supplier_Account_End_Dt IS NULL THEN 'Unspecified'
                                    WHEN MAX(ISNULL(CHA.Supplier_Meter_Disassociation_Date, CHA.Supplier_Account_End_Dt)) > CHA.Supplier_Account_End_Dt THEN
                                        CONVERT(VARCHAR, CHA.Supplier_Account_End_Dt, 101)
                                    ELSE
                                        CONVERT(
                                            VARCHAR
                                            , MAX(ISNULL(
                                                      CHA.Supplier_Meter_Disassociation_Date
                                                      , CHA.Supplier_Account_End_Dt)), 101)
                                END + ')'
                      END AS Display_Account_Number
               FROM
                    Core.Client_Hier_Account CHA
                    JOIN Inserted I
                        ON CHA.Account_Id = I.Account_Id
                    INNER JOIN dbo.Supplier_Account_Config supact
                        ON supact.Account_Id = I.Account_Id
                    INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                        ON tsaci.Account_Id = supact.Account_Id
               WHERE
                    tsaci.Total_Supplier_Accoount_Config_Ids = 1
                    AND supact.Created_Ts <= @Legacy_Account_Dt
               GROUP BY
                   CHA.Account_Id
                   , CHA.Account_Number
                   , CHA.Account_Type
                   , CHA.Account_Vendor_Name
                   , CHA.Supplier_Account_begin_Dt
                   , CHA.Supplier_Account_End_Dt
                   , supact.Supplier_Account_End_Dt
           )
        UPDATE
            CHA
        SET
            Display_Account_Number = DAN.Display_Account_Number
            , Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            JOIN CTE_Display_Account_Number DAN
                ON CHA.Account_Id = DAN.Account_Id
        WHERE
            ISNULL(CHA.Display_Account_Number, '') <> DAN.Display_Account_Number;



        UPDATE
            CHA
        SET
            CHA.Display_Account_Number = CHA.Account_Number
            , CHA.Last_Change_TS = GETDATE()
        FROM
            Core.Client_Hier_Account CHA
            INNER JOIN Inserted I
                ON I.Account_Id = CHA.Account_Id
            INNER JOIN @Total_Supplier_Accoount_Config_Ids tsaci
                ON tsaci.Account_Id = I.Account_Id
        WHERE
            tsaci.Total_Supplier_Accoount_Config_Ids > 1
            AND ISNULL(CHA.Display_Account_Number, '') <> ISNULL(CHA.Account_Number, '');


    END;
    ;



    ;










GO
ALTER TABLE [dbo].[Supplier_Account_Config] ADD CONSTRAINT [pk_Supplier_Account_Config] PRIMARY KEY NONCLUSTERED  ([Supplier_Account_Config_Id]) ON [DB_DATA01]
GO
ALTER TABLE [dbo].[Supplier_Account_Config] ADD CONSTRAINT [unc_Supplier_Account_Config_Account_Id__Contract_Id__SA_Start_Dt__SA_End_Dt] UNIQUE CLUSTERED  ([Account_Id], [Contract_Id], [Supplier_Account_Begin_Dt], [Supplier_Account_End_Dt]) ON [DB_DATA01]
GO
