CREATE TABLE [dbo].[SR_RFP_ACCOUNT_METER_MAP]
(
[SR_RFP_ACCOUNT_METER_MAP_ID] [int] NOT NULL IDENTITY(1, 1),
[METER_ID] [int] NOT NULL,
[SR_RFP_ACCOUNT_ID] [int] NOT NULL
) ON [DBData_Sourcing]
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT_METER_MAP] ADD CONSTRAINT [PK__SR_RFP_ACCOUNT_M__7E4E9607] PRIMARY KEY CLUSTERED  ([SR_RFP_ACCOUNT_METER_MAP_ID]) WITH (FILLFACTOR=90) ON [DBData_Sourcing]
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT_METER_MAP] ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_METER_MAP_METER] FOREIGN KEY ([METER_ID]) REFERENCES [dbo].[METER] ([METER_ID])
GO
ALTER TABLE [dbo].[SR_RFP_ACCOUNT_METER_MAP] WITH NOCHECK ADD CONSTRAINT [FK_SR_RFP_ACCOUNT_METER_MAP_SR_RFP_ACCOUNT] FOREIGN KEY ([SR_RFP_ACCOUNT_ID]) REFERENCES [dbo].[SR_RFP_ACCOUNT] ([SR_RFP_ACCOUNT_ID]) NOT FOR REPLICATION
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is the map table to store the meters selected for each account belonging to the particular RFP', 'SCHEMA', N'dbo', 'TABLE', N'SR_RFP_ACCOUNT_METER_MAP', NULL, NULL
GO
