CREATE TABLE [dbo].[App_Config]
(
[App_Config_Id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[App_Id] [int] NOT NULL,
[App_Config_Cd] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[App_Config_Value] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Is_Active] [bit] NOT NULL CONSTRAINT [df_App_Config_Is_Active] DEFAULT (1),
[Start_Dt] [datetime] NOT NULL CONSTRAINT [df_App_Config_Start_Dt] DEFAULT ('01/01/1900'),
[End_Dt] [datetime] NOT NULL CONSTRAINT [df_App_Config_End_Dt] DEFAULT ('12/31/2099')
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'Application Configuration Settings', 'SCHEMA', N'dbo', 'TABLE', N'App_Config', NULL, NULL
GO

ALTER TABLE [dbo].[App_Config] ADD CONSTRAINT [pk_App_Config] PRIMARY KEY CLUSTERED  ([App_Config_Id]) ON [PRIMARY]
GO
