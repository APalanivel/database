CREATE TABLE [dbo].[Variance_Rule]
(
[Variance_Rule_Id] [int] NOT NULL IDENTITY(1, 1),
[Operator_Cd] [int] NOT NULL,
[Positive_Negative] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Variance_Test_Id] [int] NOT NULL,
[Variance_Baseline_Id] [int] NOT NULL
) ON [DBData_Invoice]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Variance rule for variance test/baseline combination', 'SCHEMA', N'dbo', 'TABLE', N'Variance_Rule', NULL, NULL
GO

ALTER TABLE [dbo].[Variance_Rule] ADD 
CONSTRAINT [PK_Variance_Rule] PRIMARY KEY CLUSTERED  ([Variance_Rule_Id]) WITH (FILLFACTOR=90) ON [DBData_Invoice]
ALTER TABLE [dbo].[Variance_Rule] ADD
CONSTRAINT [FK_Variance_Parameter_Baseline_MapVariance_Rule] FOREIGN KEY ([Variance_Baseline_Id]) REFERENCES [dbo].[Variance_Parameter_Baseline_Map] ([Variance_Baseline_Id])
ALTER TABLE [dbo].[Variance_Rule] ADD
CONSTRAINT [FK_Variance_TestVariance_Rule] FOREIGN KEY ([Variance_Test_Id]) REFERENCES [dbo].[Variance_Test_Master] ([Variance_Test_Id])
GO
