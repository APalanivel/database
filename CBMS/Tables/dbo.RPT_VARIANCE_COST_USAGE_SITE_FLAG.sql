CREATE TABLE [dbo].[RPT_VARIANCE_COST_USAGE_SITE_FLAG]
(
[ACTIVE_TABLE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Legacy table pointing to the table above that is active.  Populated on Tuesday, Thursday & Saturday', 'SCHEMA', N'dbo', 'TABLE', N'RPT_VARIANCE_COST_USAGE_SITE_FLAG', NULL, NULL
GO
