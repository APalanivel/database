CREATE TABLE [dbo].[Client_Document_Category_Map]
(
[Client_Document_Category_Id] [int] NOT NULL,
[SSO_DOCUMENT_ID] [int] NOT NULL,
[Created_User_Id] [int] NOT NULL,
[Created_Ts] [datetime] NOT NULL
) ON [DB_DATA01]
CREATE NONCLUSTERED INDEX [ix_Client_Document_Category_Map__SSO_Document_ID] ON [dbo].[Client_Document_Category_Map] ([SSO_DOCUMENT_ID]) ON [DB_INDEXES01]

GO


ALTER TABLE [dbo].[Client_Document_Category_Map] ADD CONSTRAINT [PK_Client_Document_Category_Map] PRIMARY KEY CLUSTERED  ([Client_Document_Category_Id], [SSO_DOCUMENT_ID]) ON [DB_DATA01]
GO
