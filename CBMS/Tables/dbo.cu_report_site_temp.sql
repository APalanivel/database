CREATE TABLE [dbo].[cu_report_site_temp]
(
[session_uid] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'work tables used for generating data extracts', 'SCHEMA', N'dbo', 'TABLE', N'cu_report_site_temp', NULL, NULL
GO
