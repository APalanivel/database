CREATE TABLE [dbo].[Sub_Site]
(
[Sub_Site_Id] [int] NOT NULL CONSTRAINT [DF__Sub_Site__Sub_Si__2EF76596] DEFAULT ((0)),
[Sub_Site_Name] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Sub_Site__Sub_Si__2FEB89CF] DEFAULT (''),
[Sub_Site_Dsc] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Parent_Id] [int] NULL,
[Client_Hier_Id] [int] NOT NULL CONSTRAINT [DF__Sub_Site__Client__30DFAE08] DEFAULT ((0)),
[Add_Ts] [datetime] NULL,
[Add_User_Id] [int] NOT NULL CONSTRAINT [DF__Sub_Site__Add_Us__31D3D241] DEFAULT ((0)),
[Last_Change_Ts] [datetime] NOT NULL CONSTRAINT [DF__Sub_Site__Last_C__32C7F67A] DEFAULT ('01-jan-1900 00:00:00'),
[Last_Change_User_Id] [int] NOT NULL CONSTRAINT [DF__Sub_Site__Last_C__33BC1AB3] DEFAULT ((0)),
[Asset_Id] [int] NULL,
[Display_Seq] [int] NULL,
[Version_Of_Sub_Site_Id] [int] NOT NULL CONSTRAINT [DF__Sub_Site__Versio__34B03EEC] DEFAULT ((0))
) ON [DB_DATA01]
GO
CREATE UNIQUE CLUSTERED INDEX [sub_site__x0] ON [dbo].[Sub_Site] ([Sub_Site_Name], [Parent_Id], [Client_Hier_Id]) ON [DB_DATA01]
GO
