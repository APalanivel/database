CREATE TABLE [dbo].[TransformTypes]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[MappingType] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[ExecutorType] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL CONSTRAINT [DF_TransformTypes_ExecutorType] DEFAULT ('MapForceExecutor'),
[CorrelationId] [uniqueidentifier] NOT NULL,
[MappingSourceRoot] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[ExecutorInputFileRoot] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[ExecutorOutputFileRoot] [nvarchar] (max) COLLATE Latin1_General_CI_AI NOT NULL,
[PollInterval] [int] NOT NULL,
[CreateOutputSubfolder] [bit] NOT NULL CONSTRAINT [DF_TransformTypes_CreateOutputSubfolder] DEFAULT ((0)),
[OutputExtension] [nvarchar] (10) COLLATE Latin1_General_CI_AI NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TransformTypes] ADD CONSTRAINT [PK_dbo.TransformTypes] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
